#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  MinuteSetting - Minute Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, sequential setting that contains the minute of
//  the hour.  This class inherits from 'BatchSequentialSetting' and
//  provides the specific information needed for representation of the
//  minute.  This information includes the interval and range of this
//  setting's values, and this batch setting's new-patient value (see
//  'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains a sequential range class ('SequentialRange')
//  to manage the resolution and range of this setting.  This class has NO
//  dependent settings, or dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/MinuteSetting.ccv   25.0.4.0   19 Nov 2013 14:27:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Corrected incomplete comment.
//
//  Revision: 007   By: sah  Date:  18-Jun-1999    DR Number:  5440
//  Project:  ATC
//  Description:
//      Eliminated 'isChanged()' method, and added 'resetForcedChangeFlag()',
//      to do nothing.
//
//  Revision: 006   By: sah  Date:  15-Jun-1999    DR Number:  5429
//  Project:  ATC
//  Description:
//	Now overridding 'isChanged()' to ALWAYS return 'TRUE', thereby
//      forcing highlight mode.
//
//  Revision: 005   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.18.1.0) into Rev "BiLevel" (1.18.2.0)
//	Changed this sequential setting to allow its values to be cycled
//	through, rather than bounded.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "MinuteSetting.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  MinuteSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02194] -- The setting's range ...
//  $[02196] -- The setting's resolution ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

MinuteSetting::MinuteSetting(void)
 : BatchSequentialSetting(SettingId::MINUTE,
			  Setting::NULL_DEPENDENT_ARRAY_,  sequentialRange_, TRUE),
	 sequentialRange_(59,			// maxValue...
			  NULL_SETTING_BOUND_ID,	// maxConstraintId...
			  1,			// resolution...
			  NULL_SETTING_BOUND_ID,	// minConstraintId...
			  0)			// minValue...
{
  CALL_TRACE("MinuteSetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~MinuteSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

MinuteSetting::~MinuteSetting(void)
{
  CALL_TRACE("~MinuteSetting(void)");
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  resetForcedChangeFlag()  [virtual]
//
//@ Interface-Description
//  This method RESETS (to 'FALSE') this setting's internal flag that
//  indicates whether a forced change is being initiated (e.g., New-Patient
//  Setup or Transition Rules).
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is overridden to ensure that this setting's change-state is NOT reset.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
MinuteSetting::resetForcedChangeFlag(void)
{
  CALL_TRACE("resetForcedChangeFlag()");

  // do nothing...
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is ALWAYS changeable.
//
//  $[01320] -- date/time settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
MinuteSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this batch setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
MinuteSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  const SequentialValue  NEW_PATIENT_VALUE = 0;

  return(NEW_PATIENT_VALUE);
}   // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The "accepted" value of this setting is NEVER stored in the Accepted
//  Context, therefore always return 'TRUE'.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
MinuteSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  return(TRUE);
}

#endif // defined(SIGMA_DEVELOPMENT)


#if defined(SIGMA_DEVELOPMENT)

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
MinuteSetting::SoftFault(const SoftFaultID  softFaultID,
			      const Uint32       lineNumber,
			      const char*        pFileName,
			      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
                          MINUTE_SETTING, lineNumber, pFileName,
                          pPredicate);
} 

#endif // defined(SIGMA_DEVELOPMENT)
