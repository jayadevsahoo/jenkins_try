#ifndef ComplianceCalibration_HH
#define ComplianceCalibration_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ComplianceCalibration - Calibrates compliance compensation
//  for a given patient circuit.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ComplianceCalibration.hhv   25.0.4.0   19 Nov 2013 14:23:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: syw    Date: 02-Feb-2000     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Delete unused extern declarations.
//
//  Revision: 008  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 007  By:  quf    Date: 11-Nov-1997    DR Number: DCS 2625
//       Project:  Sigma (840)
//       Description:
//			Fixed to select a gas at the start of the test and use
//			the same gas for both the low and high flow phases.
//
//  Revision: 006  By:  quf    Date: 05-Nov-1997    DR Number: DCS 2602
//       Project:  Sigma (840)
//       Description:
//			Removed extern declaration of MIN_ADIABATIC_FACTOR and
//			MAX_ADIABATIC_FACTOR.
//
//  Revision: 005  By:  quf    Date: 11-Sep-1997    DR Number: DCS 2446
//       Project:  Sigma (840)
//       Description:
//			Adiabatic factor OOR Failure now occurs only if low limit is
//			reached by either factor.  If upper limit is exceeded, the
//			appropriate factor is truncated to its upper limit.  Added
//          method limitAdiabaticFactors_().
//
//  Revision: 004  By:  syw    Date: 11-Aug-1997    DR Number: DCS 2356
//       Project:  Sigma (840)
//       Description:
//			Added adiabatic factor calibration.
//
//  Revision: 003  By:  syw    Date: 08-Jul-1997    DR Number: DCS 2285
//       Project:  Sigma (840)
//       Description:
//			Removed	calculateHighFlowAndApproxTime_().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage


class ComplianceCalibration
{

  public:
    ComplianceCalibration(void);
    ~ComplianceCalibration(void);

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);
  
    void runTest(void);

  protected:

  private:
    ComplianceCalibration(const ComplianceCalibration&); // not implemented...
    void operator=(const ComplianceCalibration&);        // not implemented...

	Boolean measureCompliance_( Real32 flow, Real32& testedCompliance,
				Real32& complianceAt30cm, Uint32& pressurizeTime,
				Real32 &adiabaticFactor, GasType gasType = AIR) ;
	Boolean highComplianceTest_( Real32 compliance) ;
	Boolean lowComplianceTest_( Real32 compliance) ;
	void limitAdiabaticFactors_( Real32& lowFlowFactor, Real32& highFlowFactor);

};


#endif // ComplianceCalibration_HH 
