#ifndef	DownloadPortIds_HH
#define	DownloadPortIds_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: DownloadPortIds - List of TCP/UDP Communication port IDs
//       that identifies a specific socket within one system.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/DownloadPortIds.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 001  By:  <jhv>    Date:  06-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

enum PortId
{
#if defined(SIGMA_DEBUG)
	DOWNLOAD_SERVER_PORT   =  9000,
#else
	DOWNLOAD_SERVER_PORT   =  8000,
#endif //defined(SIGMA_DEBUG)

	LOWEST_PORT_ID			= DOWNLOAD_SERVER_PORT,
	DOWNLOAD_CLIENT_PORT,	
	SERVER_BROADCAST_PORT,
	CLIENT_BROADCAST_PORT,
    HIGHEST_PORT_ID = CLIENT_BROADCAST_PORT
};  

#endif	// DownloadPortIds_HH
