#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExpiratoryPausePhase -  Implements an Expiratory Pause Phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class BreathPhase.  Virtual
//      methods are implemented to do initialization before expiratory pause
//		can begin, to execute the expiratory pause every BD cycle, to wrap up
//      the pause phase to be executed at the end of expiratory phase.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements expiratory pause phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The exhalation valve is closed during this phase and the psols are
//		commanded  closed.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		Only limited to one instantiation.
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExpiratoryPausePhase.ccv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011  By: yyy     Date:  6-Jul-1999    DR Number: 5463
//  Project:  ATC
//  Description:
//      Lenghthen the Apnea interval by 100 ms to avoid apnea from
//		triggering when pause starts.
//
//  Revision: 010  By: yyy     Date:  29-Apr-1999    DR Number: 5367
//  Project:  ATC
//  Description:
//      Use same pause trigger for both inspiration and expiration
//		pause handles.
//
//  Revision: 009  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 008  By:  syw    Date:  07-Jul-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//          Bilevel initial version.
//			Interface with Maneuver class.
//
//  Revision: 007  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 006 By: iv   Date: 28-Jan-1997   DR Number: DCS 1339
//  	Project:  Sigma (R8027)
//		Description:
//			Enable the HighCircuitPressureInspTrigger at the start of the breath.
//
//  Revision: 005 By: syw   Date: 09-Oct-1996   DR Number: DCS 1339
//  	Project:  Sigma (R8027)
//		Description:
//			Limit exh valve command to
//				MIN_VALUE( PRESSURE_TO_SEAL + rExhPressureSensor.getFilteredValue(),
//				highCircuitPressure_)
//
//  Revision: 004 By: syw   Date: 16-Aug-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added method updateFlowControllerFlags.  Use FlowController::updatePsol( 0)
//			to close psols instead of Psol::updatePsol( 0).  Call
//			FlowController::newBreath().
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes:
//			- update methods to pass in proper arguments in
//			  ExhValveIController class.
//			- close PSOLS to zero instead of closeLevel.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ExpiratoryPausePhase.hh"

#include "MainSensorRefs.hh"
#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "ValveRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "PhasedInContextHandle.hh"
#include "ApneaInterval.hh"
#include "PressureSensor.hh"
#include "Trigger.hh"
#include "TimerBreathTrigger.hh"
#include "Psol.hh"
#include "ExhValveIController.hh"
#include "ControllersRefs.hh"
#include "BD_IO_Devices.hh"
#include "FlowController.hh"
#include "PausePressTrigger.hh"
#include "HighCircuitPressureInspTrigger.hh"
#include "ExpPauseManeuver.hh"
#include "ManeuverRefs.hh"
#include "ManeuverTimes.hh"

//@ End-Usage

//@ Constant: MAX_EXP_PAUSE_INTERVAL
// max time for expiratory pause in msec
#ifdef E600_840_TEMP_REMOVED
extern const Int32 MAX_EXP_PAUSE_INTERVAL ;
const Int32 MAX_EXP_PAUSE_INTERVAL = 20000 ;
#endif

//@ Constant: PAUSE_INTERVAL_EXTENSION
// pause extension in msec
extern const Int32 PAUSE_INTERVAL_EXTENSION ;
const Int32 PAUSE_INTERVAL_EXTENSION = 100 ;

//@ Constant: PRESSURE_TO_SEAL
// pressure above pat press for exhalation valve to seal
extern const Real32 PRESSURE_TO_SEAL ;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExpiratoryPausePhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called with the phase type argument.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

ExpiratoryPausePhase::ExpiratoryPausePhase( void)
 : BreathPhase(BreathPhaseType::EXPIRATORY_PAUSE) 	   	// $[TI1]
{
	CALL_TRACE("ExpiratoryPausePhase::ExpiratoryPausePhase( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExpiratoryPausePhase()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExpiratoryPausePhase::~ExpiratoryPausePhase(void)
{
	CALL_TRACE("ExpiratoryPausePhase::~ExpiratoryPausePhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
//      This method has a BreathTrigger& as an argument and has no return
//      value.  This method is called at the end of the expiratory pause
//      phase to wrap up the phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Cancel maneuver if trigId is not pause complete.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
ExpiratoryPausePhase::relinquishControl( const BreathTrigger& trigger)
{
   	CALL_TRACE("relinquishControl(BreathTrigger& trigger)");

	// use trigger to avoid compiler warning "trigger not used"
	Trigger::TriggerId trigId = trigger.getId() ;

	if (trigId != Trigger::PAUSE_COMPLETE)
	{
	   	// $[TI1]
		Maneuver::CancelManeuver();
	}	// implied else $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called every BD cycle during an expiratory pause to control the
//      exhalation valve to perform an expiratory pause.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The Psols are closed.  The exhalation valve is commanded to patient
//		pressure plus PRESSURE_TO_SEAL with a maximum of HCP.
// 		$[04301]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
ExpiratoryPausePhase::newCycle( void)
{
    CALL_TRACE("newCycle(void)");

   	// $[TI1]
	updateFlowControllerFlags_() ;

    RAirFlowController.updatePsol( 0.0F) ;
   	RO2FlowController.updatePsol( 0.0F) ;

	Real32	targetPressure = MIN_VALUE(
				PRESSURE_TO_SEAL + RExhPressureSensor.getFilteredValue(),
				highCircuitPressure_) ;

   	RExhValveIController.updateExhalationValve( targetPressure, 0.0F) ;

	elapsedTimeMs_ += CYCLE_TIME_MS ;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called at the beginning of the expiratory pause to initialize the
//      phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The apnea interval is extended to the maximum expiratory pause interval
//		(to avoid triggering apnea).  Maximum interval is used because
//		we do not know how long the pause will actually last.  The pause
//		timer is also set to the maximum and the timeout trigger is enabled.
//		The ExhValveIController and Psols are initalized via newBreath() method
//		calls.  The patient triggers are enabled via EnablePatientTriggers().
//		$[04120] $[04218] $[04219]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
ExpiratoryPausePhase::newBreath(void)
{
    CALL_TRACE("newBreath(void)");

	const BoundedValue& rHighCircuitPressure =
		PhasedInContextHandle::GetBoundedValue( SettingId::HIGH_CCT_PRESS) ;

	highCircuitPressure_ = rHighCircuitPressure.value ;

    RApneaInterval.extendInterval(MAX_EXP_PAUSE_INTERVAL + PAUSE_INTERVAL_EXTENSION);

    RExhValveIController.newBreath() ;
    RAirFlowController.newBreath() ;
   	RO2FlowController.newBreath() ;

    RPausePressTrigger.enable();
    RHighCircuitPressureInspTrigger.enable();

	elapsedTimeMs_ = 0 ;

   	if (RExpPauseManeuver.getManeuverState() == PauseTypes::PENDING)
   	{
	   	// $[TI1]
		// Activate the exp. pause maneuver
		RExpPauseManeuver.activate();
   	} 	// implied else $[TI2]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ExpiratoryPausePhase::SoftFault( const SoftFaultID  softFaultID,
                   				 const Uint32       lineNumber,
		   						 const char*        pFileName,
		  						 const char*        pPredicate)
{
  	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, EXPIRATORYPAUSEPHASE,
    	                     lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFlowControllerFlags_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to set the controller shutdown flag of the flow controller to
//		TRUE.  This method overloads the base class method.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set flags to true.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
ExpiratoryPausePhase::updateFlowControllerFlags_( void)
{
	CALL_TRACE("ExpiratoryPausePhase::updateFlowControllerFlags_( void)") ;

   	// $[TI1]
	RO2FlowController.setControllerShutdown( TRUE) ;
	RAirFlowController.setControllerShutdown( TRUE) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================






