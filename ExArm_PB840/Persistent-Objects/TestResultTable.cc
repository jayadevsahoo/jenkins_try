#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: TestResultTable - Test Result Table.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is the base class for all of the result tables.  This contains
//  the majority of the functionality for the result tables, where the
//  derived classes provide the arrays and size parameters.
//---------------------------------------------------------------------
//@ Rationale
//  This is a location to place common functionality amongst the
//  result tables.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A reference to the derived classes array of table entries is used,
//  to provide the functionality needed for the result tables.
//
//  This class protects against multiple threads by using a multi-reader,
//  single-writer semaphore.  This has the effect of a mutual-exclusion
//  semaphore for "write" access, while allowing multiple "read" accesses
//  through when no "write" access is grabbed.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/TestResultTable.ccv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Table
//
//  Revision: 014   By: rhj   Date: 15-July-2010     SCR Number: 6607
//  Project:  PROX
//  Description:
//      Added prox complete time to the overall result time.
//
//  Revision: 013   By: mnr   Date: 10-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      If user overrides the Overall SST status, do not apply that 
//      for PROX SST result.
//
//  Revision: 012  By:  rhj    Date:  05-May-2010    SCR Number: 6436 
//  Project:  PROX
//  Description:
//      Removed prox test from the overall SST results.
// 
//  Revision: 011  By:  rhj    Date:  15-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//      Added functionality to indicate a Single test EST step was 
//      performed.
//
//  Revision: 010  By:  rhj    Date:  04-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//		Added setAllTestsUndefined() and added a new parameter for 
//      CalcOverallTestResult.
//
//  Revision: 009  By:  sah    Date:  31-Mar-2000    DCS Number: 5680
//  Project:  NeoMode
//  Description:
//      Modified 'CalcOverallTestResult()' to ensure that the overall
//      result would will NOT be PASSED, when there is an incomplete
//      test.
//
//  Revision: 008  By:  sah    Date:  30-Mar-2000    DCS Number: 5652
//  Project:  NeoMode
//  Description:
//      Added call to 'resetTimeOfCurrResult()' when overridding ALL
//      tests (as via the "Development Options" subscreen).
//
//  Revision: 007  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 006  By:  sah    Date:  10-Oct-1997    DCS Number: 2172
//  Project:  Sigma (R8027)
//  Description:
//	Mapped requirement for overriding all EST tests to the
//	'overrideAllTests()' method.
//
//  Revision: 005  By:  sah    Date:  24-Sep-1997    DCS Number: 2513
//  Project:  Sigma (R8027)
//  Description:
//	Eliminated pre-processor variables around the 'overrideAllTests()'
//	for use by the Service Laptop.
//
//  Revision: 004  By:  sah    Date:  18-Jul-1997    DCS Number: 2246 & 2267
//  Project:  Sigma (R8027)
//  Description:
//	Changed 'overrideFailures_()' to a public method, and incorporated
//	logic to backout the last overall result diagnostic and replace it
//	with an "OVERRIDDEN" diagnostic.  Also, added same logic to the
//	'overrideAllTests()' method.
//
//  Revision: 003  By:  sah    Date:  23-Jun-1997    DCS Number: 2214 & 2246
//  Project:  Sigma (R8027)
//  Description:
//	Fixed problem with 'overrideAllTests()' (DEVELOPMENT ONLY), where
//	it was logging an EST completion diagnostic for SST.
//
//  Revision: 002  By:  sah    Date:  15-May-1997    DCS Number: 2112
//  Project:  Sigma (R8027)
//  Description:
//	Adding tracing of '[07040]'.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "TestResultTable.hh"

//@ Usage-Classes...
#include "Array_ResultTableEntry.hh"
#include "NovRamManager.hh"
#include "NovRamSemaphore.hh"
#include "NovRamUpdateManager.hh"
#include "EstDiagLog.hh"
#include "SstDiagLog.hh"
#include "SmManager.hh"
//@ End-Usage

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~TestResultTable()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this fault log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TestResultTable::~TestResultTable(void)
{
  CALL_TRACE("~TestResultTable()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  startingTest(testNumber)
//
//@ Interface-Description
//  The test given by 'testNumber' is starting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (testNumber < MAX_ENTRIES_)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TestResultTable::startingTest(const Uint testNumber)
{
  CALL_TRACE("startingTest(testNumber)");
  CLASS_PRE_CONDITION((testNumber < MAX_ENTRIES_));

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  pArrEntries_[testNumber].startingTest();

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  // activate the registered callback, if any...
  NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  intermediateResultLogged(testNumber, resultOfIntermediateStep)
//
//@ Interface-Description
//  An intermediate result, with a status given by 'resultOfIntermediateStep',
//  was logged for the test given by 'testNumber'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (testNumber < MAX_ENTRIES_)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TestResultTable::intermediateResultLogged(
				  const Uint         testNumber,
				  const TestResultId resultOfIntermediateStep
					 )
{
  CALL_TRACE("intermediateResultLogged(...)");
  CLASS_PRE_CONDITION((testNumber < MAX_ENTRIES_));

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  // notify the result table entry for test 'testNumber' of the intermediate
  // result...
  pArrEntries_[testNumber].intermediateResultLogged(resultOfIntermediateStep);

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  // activate the registered callback, if any...
  NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  completedTest(testNumber)
//
//@ Interface-Description
//  The test given by 'testNumber' completed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (testNumber < MAX_ENTRIES_)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TestResultTable::completedTest(const Uint testNumber)
{
  CALL_TRACE("completedTest(testNumber)");
  CLASS_PRE_CONDITION((testNumber < MAX_ENTRIES_));

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  pArrEntries_[testNumber].completedTest();

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  // activate the registered callback, if any...
  NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  overrideFailures()
//
//@ Interface-Description
//  Override all minor failures.  Then replace the last overall status
//  diagnostic with a diagnostic indicating this "OVERRIDDEN" status.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TestResultTable::overrideFailures(void)
{
  CALL_TRACE("overrideFailures()");

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  for (Uint idx = 0u; idx < MAX_ENTRIES_; idx++)
  {
	//do not override the PROX SST result
	if( idx == SmTestId::SST_TEST_PROX_ID )
		continue;
    pArrEntries_[idx].override();
  }

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  // activate the registered callback, if any...
  NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);

#if defined(SIGMA_GUI_CPU)
  DiagnosticCode   completionDiag;
  
  if (UPDATE_TYPE_ID_ == NovRamUpdateManager::EST_RESULT_TABLE_UPDATE)
  {   // $[TI1] -- replace the overall EST result diagnostic, with OVERRIDDEN...
    // "back-out" the diagnostic that contains the overall result previous
    // to the override...
    EstDiagLog::BackoutOverallResultDiag();

    completionDiag.setExtendedSelfTestCode(::OVERRIDDEN_TEST_ID,
					   ::MAX_EST_RESULTS);
  }
  else if (UPDATE_TYPE_ID_ == NovRamUpdateManager::SST_RESULT_TABLE_UPDATE)
  {   // $[TI2] -- replace the overall SST result diagnostic, with OVERRIDDEN...
    // "back-out" the diagnostic that contains the overall result previous
    // to the override...
    SstDiagLog::BackoutOverallResultDiag();

    completionDiag.setShortSelfTestCode(::OVERRIDDEN_TEST_ID,
				        ::MAX_SST_RESULTS);
  }
  else
  {
    AUX_CLASS_ASSERTION_FAILURE(UPDATE_TYPE_ID_);
  }

  NovRamManager::LogEstSstDiagnostic(completionDiag);
#endif // defined(SIGMA_GUI_CPU)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  overrideAllTests()
//
//@ Interface-Description
//  Change the results of ALL of the tests to the "OVERRIDDEN" status,
//  and log an overall result diagnostic indicating this overridden
//  state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[08056] -- Sigma shall set the overall EST result to OVERRIDDEN, upon
//              request through the serial interface.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TestResultTable::overrideAllTests(void)
{
  ResultEntryData  resultData;

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  for (Uint idx = 0u; idx < MAX_ENTRIES_; idx++)
  {   // $[TI1] -- this path is ALWAYS taken...
    pArrEntries_[idx].getResultData(resultData);

    if (resultData.getResultOfCurrRun() != ::NOT_APPLICABLE_TEST_RESULT_ID)
    {   // $[TI1.1] -- found an applicable test, set to "OVERRIDDEN"...
      resultData.setCompletionFlag(TRUE);
      resultData.setResultOfCurrRun(::OVERRIDDEN_TEST_ID);
      resultData.resetTimeOfCurrResult();

      pArrEntries_[idx].updateResultData(resultData);
    }   // $[TI1.2] -- by-pass this non-applicable test...
  }

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  // activate the registered callback, if any...
  NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);

#  if defined(SIGMA_GUI_CPU)
  DiagnosticCode   completionDiag;
  
  if (UPDATE_TYPE_ID_ == NovRamUpdateManager::EST_RESULT_TABLE_UPDATE)
  {   // $[TI2] -- log an EST overall result diagnostic...
    completionDiag.setExtendedSelfTestCode(::OVERRIDDEN_TEST_ID,
					   ::MAX_EST_RESULTS);
  }
  else if (UPDATE_TYPE_ID_ == NovRamUpdateManager::SST_RESULT_TABLE_UPDATE)
  {   // $[TI3] -- log an SST overall result diagnostic...
    completionDiag.setShortSelfTestCode(::OVERRIDDEN_TEST_ID,
				        ::MAX_SST_RESULTS);
  }
  else
  {
    AUX_CLASS_ASSERTION_FAILURE(UPDATE_TYPE_ID_);
  }

  NovRamManager::LogEstSstDiagnostic(completionDiag);
#  endif // defined(SIGMA_GUI_CPU)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setAllTestsUndefined_()
//
//@ Interface-Description
//  Change the results of ALL of the tests to the 
// "UNDEFINED_TEST_RESULT_ID" status.
//---------------------------------------------------------------------
//@ Implementation-Description
//  [LC07004] If the technician touches the SINGLE TEST button, 
//            if active, and presses ACCEPT
//  \a\ SINGLE TEST EST mode will begin and the highlighted test will be performed
//  \b\ The ventilator shall be placed into BD SERVICE REQUIRED state, 
//      even if the test passes or the equipment being tested is not installed.
//  \c\ The EST STATUS, at the end of any test, shall be NEVER RUN
//  \d\ All previous SINGLE STEP EST results for any other test are erased
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	TestResultTable::setAllTestsUndefined_(void)
{

	ResultEntryData  resultData;

	NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

	for (Uint idx = 0u; idx < MAX_ENTRIES_; idx++)
	{	// $[TI1] -- this path is ALWAYS taken...
		pArrEntries_[idx].getResultData(resultData);

		if (resultData.getResultOfCurrRun() != ::NOT_APPLICABLE_TEST_RESULT_ID)
		{
			resultData.setCompletionFlag(FALSE);
			resultData.setResultOfCurrRun(::UNDEFINED_TEST_RESULT_ID);
			resultData.setResultOfPrevRun(::UNDEFINED_TEST_RESULT_ID);
			resultData.resetTimeOfCurrResult();
			pArrEntries_[idx].updateResultData(resultData);
		}
	}

	NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

	// activate the registered callback, if any...
	NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);


}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setToNotApplicable(testNumber)
//
//@ Interface-Description
//  Set the status of test number 'testNumber' to "NOT APPLICABLE",
//  because 'testNumber' represents an EST GUI test.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (testNumber < MAX_ENTRIES_)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TestResultTable::setToNotApplicable(const Uint testNumber)
{
  CALL_TRACE("setToNotApplicable(testNumber)");
  CLASS_PRE_CONDITION((testNumber < MAX_ENTRIES_));

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  pArrEntries_[testNumber].setToNotApplicable();

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);
}   // $[TI1]


#if defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateEntries(testNumber)
//
//@ Interface-Description
//  Update the volatile entries of this result table with the new result
//  entries from BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (NovRamManager::IsVolatileAddr(this))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TestResultTable::updateEntries(
			 const AbstractArray(ResultTableEntry)& newBdEntries
			      )
{
  CALL_TRACE("updateEntries(newBdEntries)");
  CLASS_PRE_CONDITION((NovRamManager::IsVolatileAddr(this)));

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  for (Uint testNumber = 0u; testNumber < MAX_ENTRIES_; testNumber++)
  {
    pArrEntries_[testNumber] = newBdEntries[testNumber];
  }

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  // activate the registered callback, if any...
  NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  MergeResultTables(rMergedTable, arrGuiResults, arrBdResults,
//			       maxEntries)  [static]
//
//@ Interface-Description
//  This method is used to merge the results of two result tables (e.g.,
//  the GUI's and BD's result tables, respectively).  The entries are
//  returned in 'rMergedTable' as a sorted list of entries.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Iterate through each of the entries, for those BD entries that are
//  NOT clear (i.e., not "UNDEFINED" and not "NOT APPLICABLE") place in
//  'rMergedTable', otherwise place the GUI entry in the table (which
//  may be "UNDEFINED".
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
TestResultTable::MergeResultTables(
		      AbstractArray(ResultTableEntry)&       rMergedTable,
		      const AbstractArray(ResultTableEntry)& arrGuiResults,
		      const AbstractArray(ResultTableEntry)& arrBdResults,
		      const Uint                             maxEntries
			          )
{
  CALL_TRACE("MergeResultTables(rMergedTable, arrGuiResults, arrBdResults, maxEntries)");

  for (Uint idx = 0u; idx < maxEntries; idx++)
  {   // $[TI1] -- this path is ALWAYS taken...
    // it should NEVER be true that both entries are NOT cleared...
    SAFE_CLASS_ASSERTION((arrGuiResults[idx].isClear()  ||
			  arrBdResults [idx].isClear()));

    if (!arrBdResults[idx].isClear())
    {   // $[TI1.1]
      // 'arrBdResults[idx]' is not clear...
      rMergedTable[idx] = arrBdResults[idx];
    }
    else if (!arrGuiResults[idx].isClear())
    {   // $[TI1.2]
      // 'arrGuiResults[idx]' is not clear...
      rMergedTable[idx] = arrGuiResults[idx];
    }
    else
    {   // $[TI1.3]
      // both are clear, therefore indicate that in 'rMergedTable'...
      rMergedTable[idx].clear();
    }
  }  // end of for...
}

#endif // defined(SIGMA_GUI_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  CalcOverallTestResult(rOverallResult, arrGuiResults,
//				   arrBdResults, maxEntries)  [static]
//
//@ Interface-Description
//  Return the overall result of the current or last run.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  $[07040] -- if no test failures or alerts have occurred so far in an
//              active EST run, and ventilation was allwed prior to the
//              current run, and no non-EST failures have occurred that
//              would prevent ventilation, then aborting EST shall not
//              prevent normal ventilation.
//  $[LC07004]  If the technician touches the SINGLE TEST button...
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
	TestResultTable::CalcOverallTestResult(
										  ResultEntryData&                       rOverallResult,
#if defined(SIGMA_GUI_CPU)
										  const AbstractArray(ResultTableEntry)& arrGuiResults,
#endif // defined(SIGMA_GUI_CPU)
										  const AbstractArray(ResultTableEntry)& arrBdResults,
										  const Uint                             maxEntries,
										  const NovRamUpdateManager::UpdateTypeId  updateTypeId)
{
	CALL_TRACE("CalcOverallTestResult(rOverallResult, ...)");


	// initialize 'rOverallResult' to its default state...
	new (&rOverallResult) ResultEntryData();

	// In a single test mode, the overall result 
	// must return an UNDEFINED_TEST_RESULT_ID. 
	if (SmManager::IsSingleTestMode()  &&
		(updateTypeId == NovRamUpdateManager::EST_RESULT_TABLE_UPDATE)
	   )
	{
		return;
	}

#if defined(SIGMA_GUI_CPU)
	ResultEntryData  guiResultData;
#endif // defined(SIGMA_GUI_CPU)
	ResultEntryData  bdResultData;

	// used to determine the time of the latest result; set to earliest
	// possible time/date...
	TimeStamp  latestResultTime(TimeStamp::GetBeginEpoch());

	Uint idx;
	for (idx = 0u; idx < maxEntries; idx++)
	{	// $[TI1]

		// $[PX10002] A failure within the Proximal Flow System test 
		// shall not prevent entry into ventilation.
		if (updateTypeId == NovRamUpdateManager::SST_RESULT_TABLE_UPDATE &&
			idx == SmTestId::SST_TEST_PROX_ID)
		{
			if (!arrBdResults[idx].isClear())
			{	// -- BD's is NOT clear...
				arrBdResults[idx].getResultData(bdResultData);

				// Store the latest result time.
				if (bdResultData.getTimeOfResult() > latestResultTime)
				{
					latestResultTime = bdResultData.getTimeOfResult();
				}
			}
			continue;
		}

#if defined(SIGMA_GUI_CPU)
		// it should NEVER be true that both entries are NOT cleared...
		SAFE_CLASS_ASSERTION((arrGuiResults[idx].isClear()  ||
							  arrBdResults[idx].isClear()));

		if (!arrGuiResults[idx].isClear())
		{	// $[TI1.1] -- GUI's is NOT clear...
			arrGuiResults[idx].getResultData(guiResultData);

			if (guiResultData.getResultId() > rOverallResult.getResultId())
			{	// $[TI1.1.1]
				rOverallResult = guiResultData;
			}	// $[TI1.1.2]

			if (guiResultData.getTimeOfResult() > latestResultTime)
			{	// $[TI1.1.3]
				latestResultTime = guiResultData.getTimeOfResult();
			}	// $[TI1.1.4]

			if (guiResultData.getResultOfCurrRun() == ::INCOMPLETE_TEST_RESULT_ID  &&
				guiResultData.getResultOfPrevRun() == ::UNDEFINED_TEST_RESULT_ID)
			{	// $[TI1.1.5] -- this GUI test has never been completed...
				break;	// break out of for loop...
			}	// $[TI1.1.6] -- this GUI test has completed at least once...
		}
		else
#endif // defined(SIGMA_GUI_CPU)
			if (!arrBdResults[idx].isClear())
		{	// $[TI1.2] -- BD's is NOT clear...
			arrBdResults[idx].getResultData(bdResultData);

			if (bdResultData.getResultId() > rOverallResult.getResultId())
			{	// $[TI1.2.1]
				rOverallResult = bdResultData;
			}	// $[TI1.2.2]

			if (bdResultData.getTimeOfResult() > latestResultTime)
			{	// $[TI1.2.3]
				latestResultTime = bdResultData.getTimeOfResult();
			}	// $[TI1.2.4]

			if (bdResultData.getResultOfCurrRun() == ::INCOMPLETE_TEST_RESULT_ID  &&
				bdResultData.getResultOfPrevRun() == ::UNDEFINED_TEST_RESULT_ID)
			{	// $[TI1.2.5] -- this BD test has never been completed...
				break;	// break out of for loop...
			}	// $[TI1.2.6] -- this BD test has completed at least once...
		}
		else
		{	// $[TI1.3] -- both are clear (i.e., never run)...
#if defined(SIGMA_BD_CPU)
			arrBdResults[idx].getResultData(bdResultData);

			if (bdResultData.getResultOfCurrRun() != ::NOT_APPLICABLE_TEST_RESULT_ID)
			{	// $[TI1.3.1] -- there is an incomplete BD test...
				break;	// break out of for loop...
			}	// $[TI1.3.2] -- this "clear" BD test is NOT APPLICABLE; move on...
#elif defined(SIGMA_GUI_CPU)
			break;	// break out of for loop...
#endif // defined(SIGMA_BD_CPU)
		}	// end of else ([TI1.3])...
	}	// end of for...

	if (idx < maxEntries)
	{	// $[TI2] -- broke out of loop before processing all test results, due to
		//           an incomplete set of tests results; update overall test
		//           result...
		if (rOverallResult.isTestCompleted())
		{	// $[TI2.1] -- at least one previously-completed test...
			if (rOverallResult.getResultOfCurrRun() <= ::OVERRIDDEN_TEST_ID)
			{	// $[TI2.1.1]
				// set to 'INCOMPLETE', thereby forcing EST/SST...
				rOverallResult.setResultOfCurrRun(::INCOMPLETE_TEST_RESULT_ID);
			}	// $[TI2.1.2] -- previously-completed test has a FAILURE...
		}	// $[TI2.2] -- there are no previously-completed tests...
	}  // $[TI3] -- all results present, and processed...

	// make sure the overall result has a date/time of the latest event...
	rOverallResult.setTimeOfCurrResult(latestResultTime);
}


#if defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  HaveAllTestsPassed(arrResults, maxEntries)   [static]
//
//@ Interface-Description
//  Return 'TRUE' if the test results given in 'arrResults[]' don't
//  contain never run, incomplete, or failure statuses, otherwise return
//  'FALSE'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

Boolean
TestResultTable::HaveAllTestsPassed(
		      const AbstractArray(ResultTableEntry)& arrResults,
		      const Uint                             maxEntries
				   )
{
  CALL_TRACE("HasEstPassed(arrResults, maxEntries)");

  ResultEntryData  resultData;
  Boolean          hasEstPassed = TRUE;

  for (Uint idx = 0u; idx < maxEntries  &&  hasEstPassed; idx++)
  {   // $[TI1] -- always executes at least once...
    arrResults[idx].getResultData(resultData);

    const TestResultId  RESULT_ID = resultData.getResultId();

    switch (RESULT_ID)
    {
    case ::NOT_APPLICABLE_TEST_RESULT_ID :
    case ::NOT_INSTALLED_TEST_ID :
    case ::PASSED_TEST_ID :
    case ::OVERRIDDEN_TEST_ID :			// $[TI1.1]
      // do nothing...
      break;
    case ::UNDEFINED_TEST_RESULT_ID :
    case ::INCOMPLETE_TEST_RESULT_ID :
    case ::MINOR_TEST_FAILURE_ID :
    case ::MAJOR_TEST_FAILURE_ID :		// $[TI1.2]
      // found a test that has either never been run, or never completed,
      // or run with a non-overridden failure occuring...
      hasEstPassed = FALSE;
      break;
    case ::NUM_TEST_RESULT_IDS :
    default :
      AUX_CLASS_ASSERTION_FAILURE(RESULT_ID);
      break;
    };
  }   // end of for...

  return(hasEstPassed);
}



#endif // defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//  			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
TestResultTable::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, TEST_RESULT_TABLE,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  TestResultTable(pArrEntries, maxEntries, updateTypeId)
//
//@ Interface-Description
//  Construct a fault log that uses the array pointed to by 'pArrEntries'
//  for the log entries.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The array of entries that 'pArrEntries_' references is NOT
//  constructed, until AFTER this constructor has completed, therefore
//  it can't be used within this constructor.
//---------------------------------------------------------------------
//@ PreCondition
//  (updateTypeId == NovRamUpdateManager::EST_RESULT_TABLE_UPDATE  ||
//   updateTypeId == NovRamUpdateManager::SST_RESULT_TABLE_UPDATE)
//  (pArrEntries != NULL)
//  (maxEntries > 0)
//---------------------------------------------------------------------
//@ PostCondition
//  (isEmpty())
//@ End-Method
//=====================================================================

TestResultTable::TestResultTable(
		       ResultTableEntry*                       pArrEntries,
		       const Uint32                            maxEntries,
		       const NovRamUpdateManager::UpdateTypeId updateTypeId
				) : UPDATE_TYPE_ID_(updateTypeId),
				    MAX_ENTRIES_(maxEntries),
				    pArrEntries_(pArrEntries)

{
  CALL_TRACE("TestResultTable(pArrEntries)");
  SAFE_CLASS_PRE_CONDITION(
	  (updateTypeId == NovRamUpdateManager::EST_RESULT_TABLE_UPDATE  ||
	   updateTypeId == NovRamUpdateManager::SST_RESULT_TABLE_UPDATE)
	  		  );
  SAFE_CLASS_PRE_CONDITION((maxEntries > 0));
  SAFE_CLASS_PRE_CONDITION(
	    ((Byte*)pArrEntries_ == ((Byte*)this + sizeof(TestResultTable)))
	  		  );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  copyCurrEntries_(rCurrEntries)  [const]
//
//@ Interface-Description
//  Copy all of the current entries from this log into 'rCurrEntries'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TestResultTable::copyCurrEntries_(
			      AbstractArray(ResultTableEntry)& rCurrEntries
				 ) const
{
  CALL_TRACE("copyCurrEntries_(rCurrEntries)");
  SAFE_CLASS_PRE_CONDITION((rCurrEntries.getNumElems() >= MAX_ENTRIES_));

  NovRamSemaphore::RequestReadAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  for (Uint idx = 0u; idx < MAX_ENTRIES_; idx++)
  {
    rCurrEntries[idx] = pArrEntries_[idx];
  }

  NovRamSemaphore::ReleaseReadAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initTable_(maxEntries)  [const]
//
//@ Interface-Description
//  This is run as part of a system reset, where the NOVRAM may have already
//  been initialized before.  There are three states that a diagnostic
//  log can be in at this point:  totally valid, "correctable", and
//  "non-correctable".
//
//  If this log is completely valid, the log is left untouched and
//  'NovRamStatus::ITEM_VERIFIED' is returned.
//
//  If this log is NOT valid, but only the entries themselves are invalid
//  (i.e., the internal infrastructure members are fine), the entries are
//  "reset" to contain a diagnostic code that is specific for the
//  "correction" done here (i.e., the background fault ID 'TBD' is logged).
//  For this situation the "corrections" leave this log in a completely
//  valid state, and 'NovRamStatus::ITEM_RECOVERED' is returned.
//
//  if this log's internal infrastructure is corrupted (e.g., the "head"
//  or "tail" indeces), the log is deemed "non-correctable" and a full
//  intialization is needed.  For this situation the log is left untouched,
//  and 'NovRamStatus::ITEM_INITIALIZED' is returned to indicate that a
//  full initialization is needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The infrastructure members include those members that can only be
//  initialized via an explicit constructor call (i.e., the constant
//  'MAX_ENTRIES_', and the reference 'pArrEntries_').  If any of these
//  data members are deemed invalid, a full initialization of the log is
//  needed to get back to a valid state.
//
//  When the infrastructure members are deemed valid, the only thing left is
//  the log entries, themselves.  Each entry that is invalid, is "corrected".
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
TestResultTable::initTable_(const Uint32 maxEntries)
{
  CALL_TRACE("initTable_(maxEntries)");

  NovRamStatus::InitializationId  initStatusId;
  Boolean                         isInitNeeded;

  // $[TI1] (TRUE)  $[TI2] (FALSE)...
  isInitNeeded = (
	UPDATE_TYPE_ID_ < NovRamUpdateManager::LOW_RESULT_TABLE_VALUE   ||
	UPDATE_TYPE_ID_ > NovRamUpdateManager::HIGH_RESULT_TABLE_VALUE  ||
	MAX_ENTRIES_ != maxEntries  					||
	(Byte*)pArrEntries_ != ((Byte*)this + sizeof(TestResultTable))
	         );

  if (!isInitNeeded)
  {   // $[TI3]
    initStatusId = NovRamStatus::ITEM_VERIFIED;

    NovRamStatus::InitializationId  entryStatusId;

    for (Uint idx = 0u; idx < MAX_ENTRIES_; idx++)
    {   // $[TI3.1] -- ALWAYS executes...
      // this verifies that each entry is in a valid state, and will
      // "correct" those entries that aren't...
      entryStatusId = pArrEntries_[idx].initialize();

      if (entryStatusId > NovRamStatus::ITEM_VERIFIED)
      {   // $[TI3.1.1] -- this entry was "corrected"...
	initStatusId = NovRamStatus::ITEM_RECOVERED;
      }   // $[TI3.1.2] -- this entry was NOT "corrected"...
    }   // end of for...
  }
  else
  {   // $[TI4] -- a full initialization is needed...
    initStatusId = NovRamStatus::ITEM_INITIALIZED;
  }

  return(initStatusId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifyTable_(maxEntries)  [const]
//
//@ Interface-Description
//  Verify the integrity of each of the "active" entries of this log.
//  If all of the entries between the "head" and "tail" indices
//  (inclusive) are determined to be valid, 'NovRamStatus::ITEM_VALID' is
//  returned.  If the entries are not valid, 'NovRamStatus::ITEM_INVALID'
//  is returned.
//
//  The derived class passes 'maxEntries' which is the constant, stored
//  in Flash EPROM, that represents the maximum number of entries
//  allowed in this diagnostic code log.  This parameter is used to
//  verify the integrity of the "head" and "tail" indices.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NovRamStatus::VerificationId
TestResultTable::verifyTable_(const Uint32 maxEntries) const
{
  CALL_TRACE("verifyTable_(maxEntries)");

  NovRamSemaphore::RequestReadAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  Boolean  isValid;

  // verify that 'MAX_ENTRIES_' is equal to the value stored in FLASH,
  // and that the "head" and "tail" indices are within range of the
  // given maximum value...
  isValid = (
    UPDATE_TYPE_ID_ >= NovRamUpdateManager::LOW_RESULT_TABLE_VALUE   &&
    UPDATE_TYPE_ID_ <= NovRamUpdateManager::HIGH_RESULT_TABLE_VALUE  &&
    MAX_ENTRIES_ == maxEntries					     &&
    (const Byte*)pArrEntries_ == ((const Byte*)this + sizeof(TestResultTable))
	    );   // $[TI1]  $[TI2]

  if (isValid)
  {   // $[TI3]
    // the maximum value and indices are valid, now check the entries,
    // themselves...
    for (Uint idx = 0u; isValid  &&  idx < MAX_ENTRIES_; idx++)
    {
       isValid = (pArrEntries_[idx].isValid());
    }
  }   // $[TI4] -- invalid class members...

  NovRamSemaphore::ReleaseReadAccess(NovRamSemaphore::RESULT_TABLE_ACCESS);

  // convert the boolean 'isValid' to a status...
  return((isValid) ? NovRamStatus::ITEM_VALID		// $[TI5]
  		   : NovRamStatus::ITEM_INVALID);	// $[TI6]
}


//=====================================================================
//
//  External Functions...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Free-Function:  operator<<(ostr, testResultCodeTable)
//
// Interface-Description
//  Output the contents of the diagnostic code log given by 'testResultCodeTable'
//  to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Free-Function
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const TestResultTable& testResultTable)
{
  CALL_TRACE("operator<<(ostr, testResultTable)");

  cout << "TestResultTable {" << endl;
  cout << "  UPDATE_TYPE_ID_ = " << testResultTable.UPDATE_TYPE_ID_  << endl;
  cout << "  MAX_ENTRIES_    = " << testResultTable.MAX_ENTRIES_     << endl;

  for (Uint idx = 0u; idx < testResultTable.MAX_ENTRIES_; idx++)
  {
    ostr << "  [" << idx << "] = " << testResultTable.pArrEntries_[idx]
    	 << endl;
  }
 
  cout << "};\n" << endl;

  return(ostr);
}

#endif // defined(SIGMA_DEVELOPMENT)
