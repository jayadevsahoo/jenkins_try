//----------------------------------------------------------------------------
//            Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------
/// @file MemoryHandle.hh
#ifndef MEMORY_HANDLE_HH_
#define MEMORY_HANDLE_HH_


#include "SigmaTypes.hh"
#include "FaultHandlerMacros.hh"


typedef Uint16 MemHandle;
typedef void*  MemPtr;

/// @class MemoryHandle
/// @detail The MemoryHandle class was brought in from PB980 to handle a specific
/// issue associated with PB840 code on new hardwares. The PB840 code used 24-bit 
/// pointers, and passed them around in IPC messages in MessageQueue, usually along
/// with a 8-bit message id. This works in 840, because the queue messages can have
/// upto 32-bits in size. The new hardware (like PB980 or PB880) now use 32-bit 
/// pointers. So the queue messages will now have to be at least 40-bits (32bit data
/// + 8bit id). However, increasing the queue message sizes is going to result in
/// code changes in a lot of areas, and we are trying to avoid it. 
///
/// The solution to this problem, in PB980, was the MemoryHandle class. This class 
/// provided two APIs CreateHandle() and GetMemoryPtr(). The CreateHandle() API took
/// a memory pointer, stored it in an array and returned a handle which was just 16-bit.
/// The code that was originally passing the memory pointers in queue messages, started
/// inserting these memory handles into the messages. The receiver used the other
/// GetMemoryPtr() API to retrieve the actual pointer, based on the 16-bit handle it
/// gets from the message. The implementation of CreateHandle() API used a monotously
/// increasing handle every time a new pointer was inserted - i.e. the new pointer is 
/// stored at the slot right after where the last pointer was inserted. This made the 
/// insertion simpler and faster, but it meant that after 64K pointers (16-bit handles), 
/// no more pointers can be inserted.
///
/// This implementation of MemHandle uses the basic framework from PB980. It adds a 
/// new DeleteMemoryPtr() API for clients to indicate when a handle/pointer is no longer
/// needed. The CreateHandle() implementation has changed so as to insert on the 
/// first available slot, instead of the slot after last-inserted slot - i.e. there is
/// no more monotonously increasing handles. This allows the class to store 64K
/// pointers at any given time. If all slots are in use, and someone releases a handle
/// using DeleteMemoryPtr() API, it allows insertion of one more pointer.
///
class MemoryHandle
{
public:
	/// Create a handle to the memory pointer passed in. If a handle
	/// already exists for this memory pointer, the existing handle is returned.
	/// Since the handle is limited to 16bits, this class can handle only a max
	/// 64k pointers (in fact, 64k-1, because the handle 0xffff is reserved for
	/// error return)
	/// \param[in] memPtr - the pointer to memory to create a handle to
	/// \return A 16bit handle. MemoryHandle::MAX_HANDLES upon error.
	static MemHandle  CreateHandle( MemPtr memPtr );

	/// Gets the memory pointer referred to by the handle. If the handle
	/// does not exist then MEM_PTR_NULL is returned.
	/// \param[in] handle - the handle referring to memory
	/// \return the pointer to memory if the handle is valid, otherwise MEM_PTR_NULL.
	static MemPtr     GetMemoryPtr( MemHandle handle );

	/// Deletes memory pointer referred to by the handle. If the handle exists
	/// \param[in] handle - the handle referring to memory
	static void		DeleteMemoryPtr( MemHandle handle ); 

	static void  SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL);

	enum moduleid
	{
	 MEMORY_HANDLE = 1
	};
	static const Uint32 MAX_HANDLES = 0xFFFF;

private:
	static MemPtr m_PointerArray[MAX_HANDLES];
	static Boolean m_isInitialized;
};

#endif // MEMORY_HANDLE_HH_
