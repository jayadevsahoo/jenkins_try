#ifndef KeyHandlers_HH
#define KeyHandlers_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: KeyHandlers - A class whose purpose is as a repository for
// various offscreen key handler classes such as ExpiratoryPauseHandler,
// ManualInspirationHandler, ScreenBrightnessHandler, etc. 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/KeyHandlers.hhv   25.0.4.0   19 Nov 2013 14:08:02   pvcs  $
//
//@ Modification-Log
//
//
// 
//  Revision: 005   By: rpr    Date: 22-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Applied code review comments.
// 
//  Revision: 004   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 003  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd	Date:  12-MAY-95    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//             Integration baseline.
//====================================================================

#include "AlarmSilenceResetHandler.hh" 
#include "AlarmVolumeHandler.hh" 
#include "ExpiratoryPauseHandler.hh"
#include "InspiratoryPauseHandler.hh"
#include "ManualInspirationHandler.hh"
#include "HundredPercentO2Handler.hh"
#include "ScreenContrastHandler.hh"
#include "ScreenBrightnessHandler.hh"
#include "ScreenLockHandler.hh"
#include "GuiAppClassIds.hh"


class KeyHandlers
{
public:
	// Default constructor
    KeyHandlers(void);						// Empty 

	// Method to query screenLockHandler object 
	inline ScreenLockHandler &getScreenLockHandler(void);

	// Method to query alarmSilenceResetHandler object 
	inline AlarmSilenceResetHandler &getAlarmSilenceResetHandler(void);

	// Method to query HundredPercentO2Handler object 
	inline HundredPercentO2Handler &getHundredPercentO2Handler(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL,
						  const char*       pPredicate = NULL);
private:
	~KeyHandlers(void);						// Empty

	// Copy constructor
	KeyHandlers(const KeyHandlers &);		// not implemented

	// Assignment operator
	void operator=(const KeyHandlers &);	// not implemented

	//@ Data-Member: alarmSilenceResetHandler_
	// Member which handles the Alarm Silence and Reset keys.
	AlarmSilenceResetHandler alarmSilenceResetHandler_;
 
	//@ Data-Member: alarmVolumeHandler_
	// Member which handles the Alarm Volume key.
	AlarmVolumeHandler alarmVolumeHandler_;
 
	//@ Data-Member: expiratoryPauseHandler_
	// Member which handles the Expiratory Pause key.
	ExpiratoryPauseHandler expiratoryPauseHandler_; 

	//@ Data-Member: inspiratoryPauseHandler_                             
	// Member which handles the Inspiratory Pause key.                    
	InspiratoryPauseHandler inspiratoryPauseHandler_;

	//@ Data-Member: manualInspirationHandler_
	// Member which handles the Manual Inspiration key.
	ManualInspirationHandler manualInspirationHandler_;

	//@ Data-Member: hundredPercentO2Handler_ 
	// Member which handles the Hundred Percent O2 key.
	HundredPercentO2Handler hundredPercentO2Handler_;

	//@ Data-Member: screenContrastHandler_
	// Member which handles the Screen Contrast key.
	ScreenContrastHandler screenContrastHandler_;

	//@ Data-Member: screenBrightnessHandler_
	// Member which handles the Screen Brightness key.
	ScreenBrightnessHandler screenBrightnessHandler_;

	//@ Data-Member: screenLockHandler_
	// Member which handles the Screen Lock key.
	ScreenLockHandler screenLockHandler_;
};

// Inline methods
#include "KeyHandlers.in"

#endif
