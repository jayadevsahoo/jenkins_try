#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: Library - Provides global new/delete override operators
//  and array_init, array_dest, and  pure_virtual_called functions.
//  VRTX copyright information is also located in this module.
//---------------------------------------------------------------------
//@ Interface-Description
//  Overrides the new and delete operators. There is no dynamic allocation
//  of memory (heap free store) allowed in Sigma. If an attempt is made to
//  allocate or deallocate memory an assertion will cause termination of
//  the calling task. The functions array_init, array_dest, and 
//  pure_virtual_called are provided to satisfy the compiler in resolving
//  symbols. The VRTX copyright information is contained here for
//  licensing requirements.
//---------------------------------------------------------------------
//@ Rationale
//  All needed memory is reserved ahead of time.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Will utilize assertions to prevent dynamic allocation and deallocation
//  of memory.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with pre and
//  post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/Library.ccv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004    By: gdc              Date: 27-Mar-2008  SCR Number: 6407
//    Project: TREND2
//    Description:
//       Return NULL pointer from new() to resolve compiler warning. 
//       Change copyright to "extern" instead of "static" to resolve
//       compiler warning.
//
//  Revision: 003    By: hct              Date: 24-APR-1997  DR Number: 1994
//    Project: Sigma (R8027)
//    Description:
//       Deleted requirement numbers 00441 and 00442.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
#include "OsFoundation.hh"

#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: operator new
//
//@ Interface-Description
//  This function overrides the library version global new operator.  
//  Use of this function is not allowed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dynamic memory allocation (heap free store) is prohibited in 
//  Sigma, this function immediately asserts and terminates the 
//  calling task.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Free-Function
//=====================================================================
void * operator new(unsigned int)
{
    FREE_ASSERTION(FALSE, OS_FOUNDATION, OsFoundation::MEMORY);
	return NULL;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: operator delete
//
//@ Interface-Description
//  Function overrides the library version global delete operator.  
//  Use of this function is not allowed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since dynamic memory allocation (heap space) is prohibited in 
//  Sigma, this function immediately asserts and terminates the 
//  calling task.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Free-Function
//=====================================================================
void operator delete(void *)
{
    FREE_ASSERTION(FALSE, OS_FOUNDATION, OsFoundation::MEMORY);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: __array_init
//
//@ Interface-Description
//  Function overrides the library version _array_init function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Function returns with no processing.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Free-Function
//=====================================================================
extern "C" void  __array_init(void) 
{
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: __array_dest
//
//@ Interface-Description
//  Function overrides the library version _array_dest function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns with no processing.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Free-Function
//=====================================================================
extern "C" void  __array_dest(void) 
{
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: __pure_virtual_called
//
//@ Interface-Description
//  This function overrides the library version _pure_virtual_called 
//  function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns with no processing.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Free-Function
//=====================================================================
extern "C" void  __pure_virtual_called(void) 
{
}

//Copyright Notice. 
//Under our licensing agreement with Ready Systems, Puritan-Bennett
//is required to place a copyright notice either in ASCII form
//embedded within the firmware or to place a sticker on the EPROM
//itself.  Puritan-Bennett has chosen to use the first approach.
//The copyright notice is embedded as a "const char" string.

const char* copyright =

                   "SILICON SOFTWARE\n"
    "copyright (c) 1989 Ready Systems Corp.  All rights reserved.\n"  
    "Unpublished-rights reserved under the copyright laws of the "
    "United States.\n"
                  "RESTRICTED RIGHTS LEGEND\n"
    "Use, duplication or disclosure by the Goverment is subject to\n"
    "restrictions as set forth in subparagraph (c)(1)(ii) of the Rights\n"
    "in Technical Data & Computer Software clause at DFARS 252,227-7013.\n"
    "READY SYSTEMS, 470 POTRERO AVENUE, SUNNEYVALE, CA 94086\n";

