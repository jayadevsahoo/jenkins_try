#include "stdafx.h"

//====================================================================
//This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Bitmap - This is a graphical bitmap class.
//---------------------------------------------------------------------
//@ Interface-Description
// This class implements a graphical bitmap whose appearance is based on one of
// the images encapsulated within the Image class.  These images are
// represented by ImageDescriptors.  The constructor takes a reference to an
// ImageDescriptor to initialize the object.  The setImage() member function
// allows the user to select a new image for the bitmap object.  This class is
// derived from the Drawable class, and thus must provide a draw() method.
// Other controls, for color, position, visibility, and so on are inherited
// from Drawable.  The color attribute is used only for monochrome bitmaps.
//---------------------------------------------------------------------
//@ Rationale
// This class is kept separate from Image so many Bitmaps can share one
// ImageDescriptor.  This eliminates the problem of having many copies of the
// same raw data.
//---------------------------------------------------------------------
//@ Implementation-Description
// Internally this class stores a pointer to an ImageDescriptor.  Whenever this
// object is drawn, the raw data is read from the ImageDescriptor.  Although a
// bitmap can change which image it represents, it is assumed that the content
// of each image does not vary over the lifetime of the Bitmap.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Bitmap.ccv   25.0.4.0   19 Nov 2013 14:11:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: gdc    Date:  21-May-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 008   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 007   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 006   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 005  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 004  By: sah       Date: 06-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid, by removing all semaphore requests/releases from
//      all of the 'draw()' methods.
//
//  Revision: 003  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 002  By: mpm      Date: 29-Mar-1996  DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added calls to GuiFoundation::RequestDisplayAccess() and ReleaseDisplayAccess()
//      in order to allow shared access to the display by separate drawing tasks.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Bitmap.hh"

//@ Usage-Classes
#include "BaseContainer.hh"
#include "Container.hh"
//@ End-Usage

#include "DisplayContext.hh"
#include "GraphicsContext.hh"


//@ Code...

//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Bitmap
//
//@ Interface-Description
// Constructs the bitmap using the given "rDibInfo" as the Image to draw.
// Attributes such as position are set to default by Drawable's constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method sets the bitmap's image to the passed image by calling
// the public setImage() method.  This sets the pBitmapImage_ member variable
// to point to the passed image.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Bitmap::Bitmap(DibInfo & rDibInfo)
{
	CALL_TRACE("Bitmap::Bitmap(DibInfo&)");

	//				  								$[TI1]
	//
	// Setup the image.
	//
	setImage(rDibInfo);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Bitmap
//
//@ Interface-Description
//	Destroy object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Bitmap::~Bitmap(void)
{
	CALL_TRACE("Bitmap::~Bitmap()");
	//				  								$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setImage
//
//@ Interface-Description
// Changes the Image for this Bitmap. Note that the passed reference
// "rDibInfo" does not refer to an Image; rather, it is a reference to an
// ImageDescriptor.  References or pointers to an Image do not make
// sense here, because there are no instances of the Image class; it is fully
// static, and no instances are possible.  The Image class is simply a wrapper
// for a set of ImageDescriptors.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method sets the private pBitmapImage_ pointer to the address of
// the passed rDibInfo reference.  Since the new image may be a different size
// than the old one, it is necessarry to update the area_ (inherited from
// Drawable); the changing() method of Drawable is called before this is
// done, so that the next redraw will work properly.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Bitmap::setImage(DibInfo & rDibInfo)
{
	CALL_TRACE("Bitmap::setImage(DibInfo&)");

	pBitmapImage_ = &rDibInfo;			// $[TI1]

	// Call Drawable::changing() to alert the redraw framework that the Drawable's
	// area_ is about to change:
	//
	changing();

	// Update Drawable::area_ when the Bitmap image changes.
	//
	setWidth(pBitmapImage_->biHeader.biWidth);
	setHeight(pBitmapImage_->biHeader.biHeight);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: draw [virtual]
//
//@ Interface-Description
// Draws the bitmap.  The ImageDescriptor type supports both monochrome and
// color bitmaps.  "On" bits in monochrome bitmaps are drawn in the color set
// via the setColor() call; "Off" bits are "transparent".  For color bitmaps,
// the color attribute is ignored.  The passed Area parameter (clip area) is
// ignored; this method relies upon the clipping feature of the low-level
// graphics library in the VGA-Graphics-Driver susbsystem to avoid rendering
// outside of the clip area.  A call to a low-level routine to specify the clip
// area is made in Container::draw().  Requires the Bitmap to be
// visible.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls VGA driver routine DisplayContext::DisplayBitmap() to draw the bitmap.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Bitmap::draw(const Area&)
{
	CALL_TRACE("Bitmap::draw(const Area&)");

	DisplayContext & rDc = getBaseContainer()->getDisplayContext();
	GraphicsContext & rGc = getBaseContainer()->getGraphicsContext();

	// If the bitmap is a color bitmap, the color parameter is ignored, because
	// the colors are completely specified in the bitmap.

	//				  								$[TI1]
	rGc.setForegroundColor( getColor() );
	rDc.drawBitmap(rGc,
					  *pBitmapImage_,
					  getParentContainer()->convertToRealX(area_.getX1()),
					  getParentContainer()->convertToRealY(area_.getY1()) );

}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Bitmap::SoftFault(const SoftFaultID  softFaultID,
				  const Uint32       lineNumber,
				  const char*        pFileName,
				  const char*        pPredicate)
{
	CALL_TRACE("Bitmap::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, GUI_FOUNDATION_BITMAP,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
