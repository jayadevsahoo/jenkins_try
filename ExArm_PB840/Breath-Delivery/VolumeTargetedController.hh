#ifndef VolumeTargetedController_HH
#define VolumeTargetedController_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VolumeTargetedController - Implements a volume targeted trajectory
//				controller.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/VolumeTargetedController.hhv   10.7   08/17/07 09:45:36   pvcs  
//
//@ Modification-Log
//
//  Revision: 001  By:  syw    Date:  03-Sep-2000    DR Number: 5755
//       Project:  VTPC
//       Description:
//             Initial version
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

//@ End-Usage

class VolumeTargetedController {
  public:
    VolumeTargetedController( void) ;
    ~VolumeTargetedController( void) ;

	inline Uint32 getSuspectCount( void) const ;
	inline void reset( void) ;
	inline Real32 getCPatient( void) const ;
	inline Real32 getCPatientFiltered( void) const ;
		
	Real32 getPressureTrajectory( const Real32 volumeTarget,
								  const Real32 alpha,
								  const Real32 kp,
								  const Real32 lowerLimit) ;

    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;

  protected:

  private:
    VolumeTargetedController( const VolumeTargetedController&) ;  	// not implemented...
    void   operator=( const VolumeTargetedController&) ; 			// not implemented...

    //@ Data-Member: breathNumber_
    // number of volume targeted (VTPC or VS) breaths since test breath
    Uint32 breathNumber_ ;

	//@ Data-Member: prevVolumeTarget_
	// previous volume setting
	Real32 prevVolumeTarget_ ;

	//@ Data-Member: suspectCounter_
	// suspect counter
	Uint32 suspectCounter_ ;

	//@ Data-Member: cPatient_
	// calculated patient compliance
	Real32 cPatient_ ;
	
	//@ Data-Member: cPatientFiltered_
	// filtered patient compliance
	Real32 cPatientFiltered_ ;

	//@ Data-Member: resetOccurred_
	// set if controller has been reseted.
	Boolean resetOccurred_ ;

	//@ Data-Member: prevTarget_
	// previous pressure trajectory
	Real32 prevTarget_ ;

} ;

// Inlined methods
#include "VolumeTargetedController.in"

#endif // VolumeTargetedController_HH 

