#include "stdafx.h"
#if	defined(SIGMA_GUI_CPU)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DiagnosticFault - The class that contains textual description
//	of diagnostic faults and diagnostic tests that are displayed by the
// Diagnostic Code logs and SST Results Logs.
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DiagnosticFault.ccv   25.0.4.0   19 Nov 2013 14:07:42   pvcs  $ 
//
//@ Modification-Log  
//
//  Revision: 027  By: mnr    Date: 24-Mar-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      SST support for PROX added.
//
//  Revision: 026  By: rhj    Date: 17-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//     Added new entries for PROX errors. 
//
//  Revision: 025  By: gdc    Date:  18-Aug-2009   SCR Number: 6417
//  Project:  XB
//  Description:
//     Added Safety-Net disgnostic messages for revised BD monitor.
//
//  Revision: 024  By: gdc    Date:  26-May-2007   SCR Number: 6330
//  Project:  Trend
//  Description:
//     Removed SUN prototype code.
//
//  Revision: 023  By: gdc    Date:  20-Mar-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//     Added entry for Compact Flash errors. Added entries for
//	   compact flash service mode test.
//
//  Revision: 022  By: hlg    Date:  07-Sep-2001   DR Number: 5954
//  Project:  GUIComms
//  Description:
//     Replaced generic GUI serial port errors with GUI serial port
//     errors that are unique for each serial port.
//
//  Revision: 021  By: quf    Date:  12-Jul-2001   DR Number: 5493
//  Project:  GUIComms
//  Description:
//     Added new entries for GUI serial port errors. 
//
//  Revision: 020  By: hhd    Date:  21-Jun-1999   DR Number: 5376
//  Project:  ATC
//  Description:
//     Added new entry for Battery background error. 
//
//  Revision: 019  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 018  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 017  By:  sah	Date:  08-May-1998    DR Number:  5041
//  Project:  Sigma (R8027)
//  Description:
//      Added two new system event codes, for tracking NOVRAM failures.
//
//  Revision: 016  By:  hhd	   Date:  22-OCT-1997    DR Number:  2288
//    Project:  Sigma (R8027)
//    Description:
//      Restored reference to SV_WYE_NOT_BLOCKED. 
//
//  Revision: 015  By:  yyy    Date:  01-Oct-97    DR Number: DCS 2543
//    Project:  Sigma (840)
//    Description:
//      Updated error msg for SV_BK_TOUCH_SCREEN_RESUME.
//
//  Revision: 015  By:  yyy    Date:  01-Oct-97    DR Number: DCS 2204
//    Project:  Sigma (840)
//    Description:
//      Updated error msg for battery test.
//
//  Revision: 014  By:  hhd	   Date:  30-SEP-1997    DR Number:  2288
//    Project:  Sigma (R8027)
//    Description:
//      Removed SV_WYE_NOT_BLOCKED. 
//
//  Revision: 013  By:  sah    Date:  30-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Replaced the system event ID for a diagnostic log clearance,
//      with an event ID for a task-control transition timeout.
//
//  Revision: 012  By: yyy    Date:  29-Sep-1997    DR Number: DCS 1920
//      Project:  Sigma (840)
//      Description:
//        Added a new BkEventName: BK_TOUCH_SCREEN_BLOCKED_INFO.
//
//  Revision: 011  By:  yyy    Date:  09-Sep-97    DR Number: 2445
//    Project:  Sigma (R8027)
//    Description:
//      Added new error conditions for SST circuit resistance test
//
//  Revision: 010  By:  yyy    Date:  08-Sep-97    DR Number: 2364
//    Project:  Sigma (R8027)
//    Description:
//      Added test names to the new EST tests.
//
//  Revision: 009  By:  yyy    Date:  06-Aug-97    DR Number: 2130
//    Project:  Sigma (R8027)
//    Description:
//      Rearrange the error code between SST tests.
//
//  Revision: 008  By:  yyy    Date:  30-JUL-97    DR Number: 2205
//    Project:  Sigma (R8027)
//    Description:
//      Updated the error for exh valve velocity transducer test.
//
//  Revision: 007  By:  yyy    Date:  29-JUL-97    DR Number: 2013
//    Project:  Sigma (R8027)
//    Description:
//      Updated the error for Nurse Call test.
//
//  Revision: 006  By:  yyy    Date:  29-JUL-97    DR Number: 2216
//    Project:  Sigma (R8027)
//    Description:
//      Added condition 10, and 11 (SV_LOW_EXPIRATORY_FILTER_DELTA_P) for
//		Sst filter test.
//
//  Revision: 005  By:  yyy    Date:  29-JUL-97    DR Number: 2266
//    Project:  Sigma (R8027)
//    Description:
//      Added condition 6 for EST leak test.
//
//  Revision: 004  By:  hhd    Date:  25-JUNE-97    DR Number: 2233
//    Project:  Sigma (R8027)
//    Description:
//      Added BK_DATAKEY_SIZE_ERROR.
//
//  Revision: 003  By:  yyy    Date:  27-MAY-97    DR Number: 2042
//    Project:  Sigma (R8027)
//    Description:
//      Removed SV_SPEAKER_DISCONNECTED and SV_RESONANCE_TEST, changed
//		SV_SELF_TEST_FAILED to SV_SAAS_TEST_FAILED.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2004
//    Project:  Sigma (R8027)
//    Description:
//      Changed BK_COMPR_CHECKSUM to BK_COMPR_BAD_DATA.  Added BK_COMPR_UPDATE_SN,
//		and BK_COMPR_UPDATE_PM_HRS error strings.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "DiagnosticFault.hh"

#include "CommFaultId.hh"
#include "Background.hh"
#include "PostErrors.hh"
#include "Array_CodeLogEntry_MAX_CODE_LOG_ENTRIES.hh"
#include "CodeLogEntry.hh"
#include "DiagnosticCode.hh"
#include "NovRamManager.hh"
#include "MiscStrs.hh"
#include "SmStatusId.hh"
#include "limits.h"
#include "string.h"
#include <stdio.h>
// Fault messages for EXTENDED SELF TESTS
EstSstTestFaultStruct DiagnosticFault::EstTestFaultStrs[] =
{
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_AIR_PRESSURE_NOT_DETECTED},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_O2_PRESSURE_NOT_DETECTED},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_6,
								&MiscStrs::SV_O2_PRESSURE_DETECTED},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_7,
								&MiscStrs::SV_AIR_PSOL_LEAK},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_8,
								&MiscStrs::SV_AIR_PRESSURE_DETECTED},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_9,
								&MiscStrs::SV_O2_PSOL_LEAK},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_10,
								&MiscStrs::SV_SAFETY_V_PRESSURE_RELIEF_FAILURE},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_11,
								&MiscStrs::SV_O2_PRESSURE_NOT_DETECTED},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_12,
								&MiscStrs::SV_COMPRESSOR_PRESSURE_DETECTED},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_13,
								&MiscStrs::SV_AIR_PSOL_LEAK},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_14,
								&MiscStrs::SV_O2_PSOL_LEAK},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_15,
								&MiscStrs::SV_AIR_ZERO_FLOW_CHECK_FAILED},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_16,
								&MiscStrs::SV_O2_ZERO_FLOW_CHECK_FAILED},
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, SmStatusId::CONDITION_17,
								&MiscStrs::SV_EXP_ZERO_FLOW_CHECK_FAILED},
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_1, 
								&MiscStrs::SV_KEY_ACCEPT_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_2, 
								&MiscStrs::SV_KEY_CLEAR_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_3, 
								&MiscStrs::SV_KEY_INSP_PAUSE_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_4, 
								&MiscStrs::SV_KEY_EXP_PAUSE_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_5, 
								&MiscStrs::SV_KEY_MAN_INSP_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_6, 
								&MiscStrs::SV_KEY_HUNDRED_PERCENT_O2_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_7, 
								&MiscStrs::SV_KEY_INFO_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_8, 
								&MiscStrs::SV_KEY_ALARM_RESET_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_9, 
								&MiscStrs::SV_KEY_ALARM_SILENCE_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_10, 
								&MiscStrs::SV_KEY_ALARM_VOLUME_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_11, 
								&MiscStrs::SV_KEY_SCREEN_BRIGHTNESS_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_12, 
								&MiscStrs::SV_KEY_SCREEN_CONTRAST_FAILURE },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, SmStatusId::CONDITION_13, 
								&MiscStrs::SV_KEY_SCREEN_LOCK_FAILURE },
	{ SmTestId::EST_TEST_GUI_KNOB_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_BAD_KNOB },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_1, 
								&MiscStrs::SV_BAD_GUI_HIGH_ALARM_LED },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_BAD_GUI_MEDIUM_ALARM_LED },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_BAD_GUI_LOW_ALARM_LED },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_4, 
								&MiscStrs::SV_BAD_GUI_NORMAL_LED },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_5, 
								&MiscStrs::SV_BAD_GUI_BATT_BACKUP_LED },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_6, 
								&MiscStrs::SV_BAD_GUI_ON_BATT_PWR_LED },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_7, 
								&MiscStrs::SV_BAD_GUI_COMP_RDY_LED },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_8, 
								&MiscStrs::SV_BAD_GUI_COMP_OP_LED },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_9, 
								&MiscStrs::SV_BAD_GUI_100_PCT_O2_LED },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_10, 
								&MiscStrs::SV_BAD_GUI_ALARM_SILENCE_LED },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, SmStatusId::CONDITION_11, 
								&MiscStrs::SV_BAD_GUI_SCREEN_LOCK_LED },
	{ SmTestId::EST_TEST_BD_LAMP_ID, SmStatusId::CONDITION_1, 
								&MiscStrs::SV_BAD_BD_STATUS_LEDS },
	{ SmTestId::EST_TEST_BD_LAMP_ID, SmStatusId::CONDITION_2, 
								&MiscStrs::SV_BAD_VENT_INOP_LED },
	{ SmTestId::EST_TEST_BD_LAMP_ID, SmStatusId::CONDITION_3, 
								&MiscStrs::SV_BAD_SVO_LED },
	{ SmTestId::EST_TEST_BD_LAMP_ID, SmStatusId::CONDITION_4, 
								&MiscStrs::SV_BAD_LOSS_OF_GUI_LED },
	//To do E600 tests needs to be revisted for sound class
	{ SmTestId::EST_TEST_GUI_AUDIO_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_SAAS_TEST_FAILED},
	{ SmTestId::EST_TEST_GUI_NURSE_CALL_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_NURSE_CALL_STUCK_ON},
	{ SmTestId::EST_TEST_GUI_NURSE_CALL_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_NURSE_CALL_STUCK_OFF},
	{ SmTestId::EST_TEST_BD_AUDIO_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_BAD_ALARM_CABLE},
	{ SmTestId::EST_TEST_BD_AUDIO_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_BAD_POWER_FAIL_CAP },
	{ SmTestId::EST_TEST_BD_AUDIO_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_BAD_POWER_FAIL_CAP },
	{ SmTestId::EST_TEST_BD_AUDIO_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_BAD_BD_AUDIO},
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_BAD_O2_FLOW_SENSOR},
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_O2_PSOL_CURRENT_OUT_OF_RANGE},
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_BAD_AIR_FLOW_SENSOR},
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_AIR_PSOL_CURRENT_OUT_OF_RANGE},
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, SmStatusId::CONDITION_6,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_O2_FLOW},
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, SmStatusId::CONDITION_7,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_AIR_FLOW},
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, SmStatusId::CONDITION_8,
								&MiscStrs::SV_O2_ZERO_FLOW_CHECK_FAILED},
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, SmStatusId::CONDITION_9,
								&MiscStrs::SV_AIR_ZERO_FLOW_CHECK_FAILED},
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, SmStatusId::CONDITION_10,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_O2_FLOW},
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, SmStatusId::CONDITION_11,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_AIR_FLOW},
	{ SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_INSP_AUTOZERO_OUT_OF_RANGE},
	{ SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_EXH_AUTOZERO_OUT_OF_RANGE},
	{ SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_FAILED_TO_REACH_TEST_PRESSURE},
	{ SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_CROSS_CHECK_FAILED},
	{ SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_BAD_INSP_AUTOZERO_SOLE_INSP_PRESS_SENSOR },
	{ SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_6,
								&MiscStrs::SV_BAD_EXH_AUTOZERO_SOLE_EXH_PRESS_SENSOR },
	{ SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_7,
								&MiscStrs::SV_CROSS_CHECK_FAILED},
	{ SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_8,
								&MiscStrs::SV_FAILED_TO_REACH_TEST_PRESSURE},
	{ SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_10,
								&MiscStrs::SV_NO_AC_POWER_CONNECTED},
	{ SmTestId::EST_TEST_SAFETY_SYSTEM_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_SAFETY_VALVE_OCCLUDED },
	{ SmTestId::EST_TEST_SAFETY_SYSTEM_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_BAD_SAFETY_VALVE_LOOPBAK_CURR_DRIVER },
	{ SmTestId::EST_TEST_SAFETY_SYSTEM_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_BAD_INSP_CHECK_VALVE},
	{ SmTestId::EST_TEST_SAFETY_SYSTEM_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_BAD_INSP_CHECK_VALVE},
	{ SmTestId::EST_TEST_SAFETY_SYSTEM_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_NO_FLOW},
	{ SmTestId::EST_TEST_EXH_VALVE_SEAL_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_BAD_EXH_VALVE_SEAL}, 
	{ SmTestId::EST_TEST_EXH_VALVE_SEAL_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_EXH_VALVE_TEMP_OOR}, 
	{ SmTestId::EST_TEST_EXH_VALVE_SEAL_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_EXH_FLOW}, 
	{ SmTestId::EST_TEST_EXH_VALVE_SEAL_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_EXP_VALVE_NOT_CALIBRATED}, 
	{ SmTestId::EST_TEST_EXH_VALVE_SEAL_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_BAD_EXH_VALVE_SEAL}, 
	{ SmTestId::EST_TEST_EXH_VALVE_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_BAD_CALIBR_EXH_VALVE }, 
	{ SmTestId::EST_TEST_EXH_VALVE_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_EXP_VALVE_NOT_CALIBRATED}, 
	{ SmTestId::EST_TEST_EXH_VALVE_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_EXH_FLOW}, 
	{ SmTestId::EST_TEST_SM_LEAK_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_LEAK_FAILURE },
	{ SmTestId::EST_TEST_SM_LEAK_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_PRESSURE},
	{ SmTestId::EST_TEST_SM_LEAK_ID, SmStatusId::CONDITION_6,
								&MiscStrs::SV_TEST_CIRCUIT_NOT_CONNECTED},
	{ SmTestId::EST_TEST_EXH_HEATER_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_AIR_FLOW},
	{ SmTestId::EST_TEST_EXH_HEATER_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_BAD_EXH_HEATER },
	{ SmTestId::EST_TEST_EXH_HEATER_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_BAD_EXH_HEATER },
	{ SmTestId::EST_TEST_GENERAL_ELECTRONICS_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_CHAN_VAL_OOR },
	{ SmTestId::EST_TEST_GUI_TOUCH_ID, SmStatusId::CONDITION_1, 
								&MiscStrs::SV_GUI_TOUCH_FATAL_ERROR},
	{ SmTestId::EST_TEST_GUI_TOUCH_ID, SmStatusId::CONDITION_2, 
								&MiscStrs::SV_GUI_TOUCH_BLOCKED_BEAM},
	{ SmTestId::EST_TEST_GUI_TOUCH_ID, SmStatusId::CONDITION_3, 
								&MiscStrs::SV_GUI_TOUCH_TOO_HIGH_AMBIENT},
	{ SmTestId::EST_TEST_GUI_TOUCH_ID, SmStatusId::CONDITION_4, 
								&MiscStrs::SV_GUI_TOUCH_SCREEN_NOT_RESPONDING},
	{ SmTestId::EST_TEST_GUI_SERIAL_PORT_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_BAD_GUI_SER_PORT },
	{ SmTestId::EST_TEST_GUI_SERIAL_PORT_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_BAD_GUI_SER_PORT },
	{ SmTestId::EST_TEST_GUI_SERIAL_PORT_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_BAD_GUI_SER_PORT },
	{ SmTestId::EST_TEST_BATTERY_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_BATTERY_NOT_CHARGED },
	{ SmTestId::EST_TEST_BATTERY_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_BATTERY_NOT_DISCHARGING },
	{ SmTestId::EST_TEST_BATTERY_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_BAD_BPS },
	{ SmTestId::EST_TEST_BATTERY_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_BATTERY_NOT_CHARGING },
	{ SmTestId::RESTART_BD_ID, SmStatusId::NULL_CONDITION_ITEM,
								&MiscStrs::SV_RESTART_BD},
	{ SmTestId::RESTART_GUI_ID, SmStatusId::NULL_CONDITION_ITEM,
								&MiscStrs::SV_RESTART_GUI},
	{ SmTestId::COMPACT_FLASH_TEST_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_CF_INTERFACE_ERROR},
	{ SmTestId::COMPACT_FLASH_TEST_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_CF_FLASH_ERROR}
};


// Fault messages for SHORT SELF TESTS
EstSstTestFaultStruct DiagnosticFault::SstTestFaultStrs[] =
{
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_1, 
								&MiscStrs::SV_NO_FLOW}, 
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_OCCLUDED_INSP_LIMB }, 
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_3, 
								&MiscStrs::SV_OCCLUDED_INSP_LIMB }, 
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_UNABLE_REACH_MIN_PEAK_FLOW},
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_OCCLUDED_EXP_LIMB},
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_6,
								&MiscStrs::SV_OCCLUDED_EXP_LIMB},
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_7,
								&MiscStrs::SV_UNABLE_REACH_MIN_PEAK_FLOW},
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_8,
								&MiscStrs::SV_INSP_LIMB_RESISTANCE_LOW},
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_9,
								&MiscStrs::SV_EXP_LIMB_RESISTANCE_LOW},
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_10,
								&MiscStrs::SV_UNABLE_REACH_MIN_PEAK_FLOW},
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_11,
								&MiscStrs::SV_UNABLE_REACH_MIN_PEAK_FLOW},
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_12,
								&MiscStrs::SV_WYE_NOT_BLOCKED},
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_13,
								&MiscStrs::SV_INSP_LIMB_RESISTANCE_LOW},
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, SmStatusId::CONDITION_14,
								&MiscStrs::SV_EXP_LIMB_RESISTANCE_LOW},
	{ SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_NO_FLOW },
	{ SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_OCC_EXH_COMPARTMENT},
	{ SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_OCC_EXH_FILTER },
	{ SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_OCC_EXH_FILTER},
	{ SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, SmStatusId::CONDITION_6,
								&MiscStrs::SV_CIRCUIT_NOT_DISCONNECTED},
	{ SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, SmStatusId::CONDITION_7,
								&MiscStrs::SV_CIRCUIT_NOT_RECONNECTED},
	{ SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, SmStatusId::CONDITION_8,
								&MiscStrs::SV_OCC_EXH_COMPARTMENT},
	{ SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, SmStatusId::CONDITION_10,
								&MiscStrs::SV_LOW_EXPIRATORY_FILTER_DELTA_P},
	{ SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, SmStatusId::CONDITION_11,
								&MiscStrs::SV_LOW_EXPIRATORY_FILTER_DELTA_P},
	{ SmTestId::SST_TEST_CIRCUIT_LEAK_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_LEAK_FAILURE },
	{ SmTestId::SST_TEST_CIRCUIT_LEAK_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_LEAK_FAILURE },
	{ SmTestId::SST_TEST_CIRCUIT_LEAK_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_PRESSURE},
	{ SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_INSP_AUTOZERO_OUT_OF_RANGE},
	{ SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_EXH_AUTOZERO_OUT_OF_RANGE},
	{ SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_FAILED_TO_REACH_TEST_PRESSURE},
	{ SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_CROSS_CHECK_FAILED},
	{ SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_BAD_INSP_AUTOZERO_SOLE_INSP_PRESS_SENSOR },
	{ SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_6,
								&MiscStrs::SV_BAD_EXH_AUTOZERO_SOLE_EXH_PRESS_SENSOR },
	{ SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_7,
								&MiscStrs::SV_CROSS_CHECK_FAILED},
	{ SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID, SmStatusId::CONDITION_8,
								&MiscStrs::SV_FAILED_TO_REACH_TEST_PRESSURE},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_BAD_O2_FLOW_SENSOR},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_O2_PSOL_CURRENT_OUT_OF_RANGE},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_BAD_AIR_FLOW_SENSOR},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_AIR_PSOL_CURRENT_OUT_OF_RANGE},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_GAS_NOT_CONNECTED},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_6,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_O2_FLOW},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_7,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_AIR_FLOW},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_8,
								&MiscStrs::SV_O2_ZERO_FLOW_CHECK_FAILED},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_9,
								&MiscStrs::SV_AIR_ZERO_FLOW_CHECK_FAILED},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_10,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_O2_FLOW},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_11,
								&MiscStrs::SV_UNABLE_TO_ESTABLISH_AIR_FLOW},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_12,
								&MiscStrs::SV_O2_PRESSURE_NOT_DETECTED},
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, SmStatusId::CONDITION_13,
								&MiscStrs::SV_AIR_PRESSURE_NOT_DETECTED},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_BAD_UNABLE_PRESSURIZE_CIRCUIT},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_BAD_UNABLE_PRESSURIZE_CIRCUIT},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_HIGH_FLOW_TIME_TOO_LOW},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_COMPL_CALIB_FAILURE},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_COMPL_CALIB_FAILURE},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_6,
								&MiscStrs::SV_COMPL_LOW},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_7,
								&MiscStrs::SV_COMPL_LOW},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_8,
								&MiscStrs::SV_COMPL_CALIB_FAILURE},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_9,
								&MiscStrs::SV_COMPL_CALIB_FAILURE},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_10,
								&MiscStrs::SV_COMPL_LOW},
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, SmStatusId::CONDITION_11,
								&MiscStrs::SV_COMPL_LOW},
    // PROX SST
	{ SmTestId::SST_TEST_PROX_ID, SmStatusId::CONDITION_1,
								&MiscStrs::SV_PX_COMM_ERROR},
	{ SmTestId::SST_TEST_PROX_ID, SmStatusId::CONDITION_2,
								&MiscStrs::SV_PX_SENSOR_ERROR},
	{ SmTestId::SST_TEST_PROX_ID, SmStatusId::CONDITION_3,
								&MiscStrs::SV_PX_DEMO_MODE_ERROR},
	{ SmTestId::SST_TEST_PROX_ID, SmStatusId::CONDITION_4,
								&MiscStrs::SV_PX_BAROMETIC_PRESS_CROSS_CHK_ERROR},
	{ SmTestId::SST_TEST_PROX_ID, SmStatusId::CONDITION_5,
								&MiscStrs::SV_PX_AUTO_ZERO_AND_LEAK_ERROR},
	{ SmTestId::SST_TEST_PROX_ID, SmStatusId::CONDITION_6,
								&MiscStrs::SV_PX_PURGE_ERROR},
	{ SmTestId::SST_TEST_PROX_ID, SmStatusId::CONDITION_7,
								&MiscStrs::SV_PX_PRESS_CROSS_CHK_ERROR},
	{ SmTestId::SST_TEST_PROX_ID, SmStatusId::CONDITION_8,
								&MiscStrs::SV_PX_FLOW_SENSOR_CROSS_CHK_ERROR},
	{ SmTestId::SST_TEST_PROX_ID, SmStatusId::CONDITION_9,
								&MiscStrs::SV_GAS_NOT_CONNECTED},
	{ SmTestId::SST_TEST_PROX_ID, SmStatusId::CONDITION_10,
								&MiscStrs::PX_PROX_TEST_SKIPPED}

};


// Fault messages for NETWORK APPLICATION
TestFaultStruct DiagnosticFault::CommFaultStrs[] = 
{
	{ NETWORKAPP_EPERM, &MiscStrs::SV_NETWORKAPP_EPERM },
	{ NETWORKAPP_ENOENT, &MiscStrs::SV_NETWORKAPP_ENOENT },
	{ NETWORKAPP_ESRCH, &MiscStrs::SV_NETWORKAPP_ESRCH },
	{ NETWORKAPP_EINTR, &MiscStrs::SV_NETWORKAPP_EINTR },
	{ NETWORKAPP_EIO, &MiscStrs::SV_NETWORKAPP_EIO },
	{ NETWORKAPP_ENXIO, &MiscStrs::SV_NETWORKAPP_ENXIO },
	{ NETWORKAPP_E2BIG, &MiscStrs::SV_NETWORKAPP_E2BIG },
	{ NETWORKAPP_ENOEXEC, &MiscStrs::SV_NETWORKAPP_ENOEXEC },
	{ NETWORKAPP_EBADF, &MiscStrs::SV_NETWORKAPP_EBADF },
	{ NETWORKAPP_ECHILD, &MiscStrs::SV_NETWORKAPP_ECHILD },
	{ NETWORKAPP_EAGAIN, &MiscStrs::SV_NETWORKAPP_EAGAIN },
	{ NETWORKAPP_ENOMEM, &MiscStrs::SV_NETWORKAPP_ENOMEM },
	{ NETWORKAPP_EACCES, &MiscStrs::SV_NETWORKAPP_EACCES },
	{ NETWORKAPP_EFAULT, &MiscStrs::SV_NETWORKAPP_EFAULT },
	{ NETWORKAPP_ENOTBLK, &MiscStrs::SV_NETWORKAPP_ENOTBLK },
	{ NETWORKAPP_EBUSY, &MiscStrs::SV_NETWORKAPP_EBUSY },
	{ NETWORKAPP_EEXIST, &MiscStrs::SV_NETWORKAPP_EEXIST },
	{ NETWORKAPP_EXDEV, &MiscStrs::SV_NETWORKAPP_EXDEV },
	{ NETWORKAPP_ENODEV, &MiscStrs::SV_NETWORKAPP_ENODEV },
	{ NETWORKAPP_ENOTDIR, &MiscStrs::SV_NETWORKAPP_ENOTDIR },
	{ NETWORKAPP_EISDIR, &MiscStrs::SV_NETWORKAPP_EISDIR },
	{ NETWORKAPP_EINVAL, &MiscStrs::SV_NETWORKAPP_EINVAL },
	{ NETWORKAPP_ENFILE, &MiscStrs::SV_NETWORKAPP_ENFILE },
	{ NETWORKAPP_EMFILE, &MiscStrs::SV_NETWORKAPP_EMFILE },
	{ NETWORKAPP_ENOTTY, &MiscStrs::SV_NETWORKAPP_ENOTTY },
	{ NETWORKAPP_ETXTBSY, &MiscStrs::SV_NETWORKAPP_ETXTBSY },
	{ NETWORKAPP_EFBIG, &MiscStrs::SV_NETWORKAPP_EFBIG },
	{ NETWORKAPP_ENOSPC, &MiscStrs::SV_NETWORKAPP_ENOSPC },
	{ NETWORKAPP_ESPIPE, &MiscStrs::SV_NETWORKAPP_ESPIPE },
	{ NETWORKAPP_EROFS, &MiscStrs::SV_NETWORKAPP_EROFS },
	{ NETWORKAPP_EMLINK, &MiscStrs::SV_NETWORKAPP_EMLINK },
	{ NETWORKAPP_EPIPE, &MiscStrs::SV_NETWORKAPP_EPIPE },
	{ NETWORKAPP_EDOM, &MiscStrs::SV_NETWORKAPP_EDOM },
	{ NETWORKAPP_ERANGE, &MiscStrs::SV_NETWORKAPP_ERANGE },
	{ NETWORKAPP_EWOULDBLOCK, &MiscStrs::SV_NETWORKAPP_EWOULDBLOCK },
	{ NETWORKAPP_EINPROGRESS, &MiscStrs::SV_NETWORKAPP_EINPROGRESS },
	{ NETWORKAPP_EALREADY, &MiscStrs::SV_NETWORKAPP_EALREADY },
	{ NETWORKAPP_ENOTSOCK, &MiscStrs::SV_NETWORKAPP_ENOTSOCK },
	{ NETWORKAPP_EDESTADDRREQ, &MiscStrs::SV_NETWORKAPP_EDESTADDRREQ },
	{ NETWORKAPP_EMSGSIZE, &MiscStrs::SV_NETWORKAPP_EMSGSIZE },
	{ NETWORKAPP_EPROTOTYPE, &MiscStrs::SV_NETWORKAPP_EPROTOTYPE },
	{ NETWORKAPP_ENOPROTOOPT, &MiscStrs::SV_NETWORKAPP_ENOPROTOOPT },
	{ NETWORKAPP_EPROTONOSUPPORT, &MiscStrs::SV_NETWORKAPP_EPROTONOSUPPORT },
	{ NETWORKAPP_ESOCKTNOSUPPORT, &MiscStrs::SV_NETWORKAPP_ESOCKTNOSUPPORT },
	{ NETWORKAPP_EOPNOTSUPP,  &MiscStrs::SV_NETWORKAPP_EOPNOTSUPP },
	{ NETWORKAPP_EPFNOSUPPORT, &MiscStrs::SV_NETWORKAPP_EPFNOSUPPORT },
	{ NETWORKAPP_EAFNOSUPPORT, &MiscStrs::SV_NETWORKAPP_EAFNOSUPPORT },
	{ NETWORKAPP_EADDRINUSE, &MiscStrs::SV_NETWORKAPP_EADDRINUSE },
	{ NETWORKAPP_EADDRNOTAVAIL, &MiscStrs::SV_NETWORKAPP_EADDRNOTAVAIL },
	{ NETWORKAPP_ENETDOWN, &MiscStrs::SV_NETWORKAPP_ENETDOWN },
	{ NETWORKAPP_ENETUNREACH, &MiscStrs::SV_NETWORKAPP_ENETUNREACH },
	{ NETWORKAPP_ENETRESET, &MiscStrs::SV_NETWORKAPP_ENETRESET },
	{ NETWORKAPP_ECONNABORTED, &MiscStrs::SV_NETWORKAPP_ECONNABORTED },
	{ NETWORKAPP_ECONNRESET, &MiscStrs::SV_NETWORKAPP_ECONNRESET },
	{ NETWORKAPP_ENOBUFS, &MiscStrs::SV_NETWORKAPP_ENOBUFS },
	{ NETWORKAPP_EISCONN, &MiscStrs::SV_NETWORKAPP_EISCONN },
	{ NETWORKAPP_ENOTCONN, &MiscStrs::SV_NETWORKAPP_ENOTCONN },
	{ NETWORKAPP_ESHUTDOWN, &MiscStrs::SV_NETWORKAPP_ESHUTDOWN },
	{ NETWORKAPP_ETOOMANYREFS, &MiscStrs::SV_NETWORKAPP_ETOOMANYREFS },
	{ NETWORKAPP_ETIMEDOUT, &MiscStrs::SV_NETWORKAPP_ETIMEDOUT },
	{ NETWORKAPP_ECONNREFUSED, &MiscStrs::SV_NETWORKAPP_ECONNREFUSED },
	{ NETWORKAPP_ELOOP, &MiscStrs::SV_NETWORKAPP_ELOOP },
	{ NETWORKAPP_ENAMETOOLONG, &MiscStrs::SV_NETWORKAPP_ENAMETOOLONG },
	{ NETWORKAPP_EHOSTDOWN, &MiscStrs::SV_NETWORKAPP_EHOSTDOWN },
	{ NETWORKAPP_EHOSTUNREACH, &MiscStrs::SV_NETWORKAPP_EHOSTUNREACH },
	{ NETWORKAPP_INVALID_COMM_DATA , &MiscStrs::SV_NETWORKAPP_INVALID_COMM_DATA},
	{ NETWORKAPP_DATA_TOO_LARGE, &MiscStrs::SV_NETWORKAPP_DATA_TOO_LARGE},
	{ NETWORKAPP_INVALID_COMM_MSGID, &MiscStrs::SV_NETWORKAPP_INVALID_COMM_MSGID},
	{ NETWORKAPP_NO_ACK_MESSAGE, &MiscStrs::SV_NETWORKAPP_NO_ACK_MESSAGE },
	{ NETWORKAPP_MAX_ACTIVE_MSG_DELAY, &MiscStrs::SV_NETWORKAPP_MAX_ACTIVE_MSG_DELAY },
	{ NETWORKAPP_BLOCKIO_NO_ACCEPT_CALLBACK, &MiscStrs::SV_NETWORKAPP_BLOCKIO_NO_ACCEPT_CALLBACK },
	{ NETWORKAPP_BLOCKIO_SOCKET_ALREADY_OPENED, &MiscStrs::SV_NETWORKAPP_BLOCKIO_SOCKET_ALREADY_OPENED },
	{ NETWORKAPP_WRONG_INPUT_COUNT, &MiscStrs::SV_NETWORKAPP_WRONG_INPUT_COUNT },
	{ NETWORKAPP_OUT_OF_ACKBUFFER, &MiscStrs::SV_NETWORKAPP_OUT_OF_ACKBUFFER },
	{ NETWORKAPP_MSG_TRANSMISSION_FAILED, &MiscStrs::SV_NETWORKAPP_MSG_TRANSMISSION_FAILED },	
	{ NETWORKAPP_CONNECT_TIMEOUT, &MiscStrs::SV_NETWORKAPP_CONNECT_TIMEOUT},
	{ NETWORKAPP_RECV_TIMEOUT, &MiscStrs::SV_NETWORKAPP_RECV_TIMEOUT},
	{ STWARE_LOG_ERROR, &MiscStrs::SV_STWARE_LOG_ERROR },
	{ STWARE_LOG_DIAGNOSTIC, &MiscStrs::SV_STWARE_LOG_DIAGNOSTIC },
	{ STWARE_IN_CONTROL_UNLINK_INIFADDR_IFP, &MiscStrs::SV_STWARE_IN_CONTROL_UNLINK_INIFADDR_IFP }, 
	{ STWARE_IN_CONTROL_UNLINK_INIFADDR_LIST, &MiscStrs::SV_STWARE_IN_CONTROL_UNLINK_INIFADDR_LIST },
	{ STWARE_IN_CKSUM_C_OUT_OF_DATA, &MiscStrs::SV_STWARE_IN_CKSUM_C_OUT_OF_DATA },
	{ STWARE_XSRN_ADDROUTE_MASK, &MiscStrs::SV_STWARE_XSRN_ADDROUTE_MASK },
	{ STWARE_XSRN_DELETE_INCONSISTENT, &MiscStrs::SV_STWARE_XSRN_DELETE_INCONSISTENT },
	{ STWARE_XSRN_DELETE_NO_ANNOTATION, &MiscStrs::SV_STWARE_XSRN_DELETE_NO_ANNOTATION },
	{ STWARE_XSRN_DELETE_NO_US, &MiscStrs::SV_STWARE_XSRN_DELETE_NO_US },
	{ STWARE_XSRN_DELETE_ORPHANED_MASK, &MiscStrs::SV_STWARE_XSRN_DELETE_ORPHANED_MASK },
	{ STWARE_UDP_USRREQ_UNEXPECTED_CONTROL_DATA, &MiscStrs::SV_STWARE_UDP_USRREQ_UNEXPECTED_CONTROL_DATA },
	{ STWARE_XS_STARTUP_NO_UNIT, &MiscStrs::SV_STWARE_XS_STARTUP_NO_UNIT },
	{ STWARE_NO_MEMORY_FOR_IFADDR, &MiscStrs::SV_STWARE_NO_MEMORY_FOR_IFADDR },
	{ STWARE_LOOUTPUT_CANNOT_HANDLE_IF, &MiscStrs::SV_STWARE_LOOUTPUT_CANNOT_HANDLE_IF},
	{ STWARE_XSRN_INSERT_COMING_OUT, &MiscStrs::SV_STWARE_XSRN_INSERT_COMING_OUT },
	{ STWARE_MCOPYDATA_NEGATIVE_OFFSET, &MiscStrs::SV_STWARE_MCOPYDATA_NEGATIVE_OFFSET },
	{ STWARE_MCOPYDATA_NULL_DATA_PTR, &MiscStrs::SV_STWARE_MCOPYDATA_NULL_DATA_PTR },
	{ STWARE_SBDROP_LEN_TOO_LARGE1, &MiscStrs::SV_STWARE_SBDROP_LEN_TOO_LARGE1 },
	{ STWARE_SBDROP_LEN_TOO_LARGE2, &MiscStrs::SV_STWARE_SBDROP_LEN_TOO_LARGE2 },
	{ STWARE_SBCOMPRESS_NO_EOR, &MiscStrs::SV_STWARE_SBCOMPRESS_NO_EOR },
	{ STWARE_ARPINPUT_DUPLICATE_IP_ADDR, &MiscStrs::SV_STWARE_ARPINPUT_DUPLICATE_IP_ADDR },
	{ STWARE_XS_REGISTERIF_INVALID_TYPE, &MiscStrs::SV_STWARE_XS_REGISTERIF_INVALID_TYPE },
	{ STWARE_XS_REGISTERIF_INVALID_FAMILY, &MiscStrs::SV_STWARE_XS_REGISTERIF_INVALID_FAMILY },
	{ STWARE_XS_DRIVIOCTL_INIT_FAILURE, &MiscStrs::SV_STWARE_XS_DRIVIOCTL_INIT_FAILURE },
	{ STWARE_XS_GOODETHERRARP_SHORT_RARP_PKT, &MiscStrs::SV_STWARE_XS_GOODETHERRARP_SHORT_RARP_PKT },
	{ STWARE_XS_GOODETHERRARP_WRONG_HWTYPE, &MiscStrs::SV_STWARE_XS_GOODETHERRARP_WRONG_HWTYPE },
	{ STWARE_XS_GOODETHERRARP_SHORT_RARP_PKT2, &MiscStrs::SV_STWARE_XS_GOODETHERRARP_SHORT_RARP_PKT2 },
	{ STWARE_XS_GOODETHERRARP_INVALID_PROTOCOL , &MiscStrs::SV_STWARE_XS_GOODETHERRARP_INVALID_PROTOCOL },
	{ STWARE_M_COPYM_LEN_TOO_LARGE, &MiscStrs::SV_STWARE_M_COPYM_LEN_TOO_LARGE },
	{ STWARE_M_COPYM_OFFSET_TOO_LARGE, &MiscStrs::SV_STWARE_M_COPYM_OFFSET_TOO_LARGE },
	{ STWARE_M_COPYDATA_OUT_OF_MBUF, &MiscStrs::SV_STWARE_M_COPYDATA_OUT_OF_MBUF },
	{ STWARE_M_COPYDATA_LEN_TOO_LARGE, &MiscStrs::SV_STWARE_M_COPYDATA_LEN_TOO_LARGE },
	{ STWARE_M_COPYDATA_OFFSET_NEGATIVE, &MiscStrs::SV_STWARE_M_COPYDATA_OFFSET_NEGATIVE },
	{ STWARE_M_COPYDATA_NULL_POINTER, &MiscStrs::SV_STWARE_M_COPYDATA_NULL_POINTER },
	{ STWARE_MXS_MBGET_OUT_OF_MBUF, &MiscStrs::SV_STWARE_MXS_MBGET_OUT_OF_MBUF },
	{ STWARE_M_CLALLOC_OUT_OF_CLICK, &MiscStrs::SV_STWARE_M_CLALLOC_OUT_OF_CLICK },
	{ STWARE_MBUF_MULTI_MFREE, &MiscStrs::SV_STWARE_MBUF_MULTI_MFREE },
	{ STWARE_MALLOC_FAILED, &MiscStrs::SV_STWARE_MALLOC_FAILED},
	{ STWARE_DUMMY_ERROR1, &MiscStrs::DIAG_UNDEFINED_ERROR},
	{ STWARE_DUMMY_ERROR2, &MiscStrs::DIAG_UNDEFINED_ERROR},
	{ STWARE_INFINITE_MBUF_LINK, &MiscStrs::SV_STWARE_INFINITE_MBUF_LINK },
	{ DCI_PARITY_ERROR_PORT1, &MiscStrs::SV_DCI_PARITY_ERROR },
	{ DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT1, &MiscStrs::SV_DCI_INPUT_BUFFER_OVERFLOW_ERROR },
	{ DCI_NON_SPECIFIC_ERROR_PORT1, &MiscStrs::SV_DCI_NON_SPECIFIC_ERROR },
	{ DCI_UNKNOWN_ERROR_PORT1, &MiscStrs::SV_DCI_UNKNOWN_ERROR },
	{ DCI_PARITY_ERROR_PORT2, &MiscStrs::SV_DCI_PARITY_ERROR },
	{ DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT2, &MiscStrs::SV_DCI_INPUT_BUFFER_OVERFLOW_ERROR },
	{ DCI_NON_SPECIFIC_ERROR_PORT2, &MiscStrs::SV_DCI_NON_SPECIFIC_ERROR },
	{ DCI_UNKNOWN_ERROR_PORT2, &MiscStrs::SV_DCI_UNKNOWN_ERROR },
	{ DCI_PARITY_ERROR_PORT3, &MiscStrs::SV_DCI_PARITY_ERROR },
	{ DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT3, &MiscStrs::SV_DCI_INPUT_BUFFER_OVERFLOW_ERROR },
	{ DCI_NON_SPECIFIC_ERROR_PORT3, &MiscStrs::SV_DCI_NON_SPECIFIC_ERROR },
	{ DCI_UNKNOWN_ERROR_PORT3, &MiscStrs::SV_DCI_UNKNOWN_ERROR },
	{ SAFETYNET_CHECKSUM_ERROR, &MiscStrs::SV_SAFETYNET_CHECKSUM_ERROR },
	{ SAFETYNET_CYCLE_TIME_ERROR, &MiscStrs::SV_SAFETYNET_CYCLE_TIME_ERROR },
	{ SAFETYNET_INTERVAL_TIME_ERROR, &MiscStrs::SV_SAFETYNET_INTERVAL_TIME_ERROR },
	{ SAFETYNET_SYNC_ACQUIRED, &MiscStrs::SV_SAFETYNET_SYNC_ACQUIRED },
	{ SAFETYNET_SYNC_LOST, &MiscStrs::SV_SAFETYNET_SYNC_LOST }
};

// Table of EST test ids and test names. 
EstSstTestNameStruct DiagnosticFault::EstTestNameStrs[] =
{
	{ SmTestId::EST_TEST_GAS_SUPPLY_ID, &MiscStrs::SM_GAS_SUPPLY_TEST_ID },
	{ SmTestId::EST_TEST_GUI_KEYBOARD_ID, &MiscStrs::SM_GUI_KEYBOARD_TEST_ID },
	{ SmTestId::EST_TEST_GUI_KNOB_ID, &MiscStrs::SM_GUI_KNOB_TEST_ID },
	{ SmTestId::EST_TEST_GUI_LAMP_ID, &MiscStrs::SM_GUI_LAMP_TEST_ID }, 
	{ SmTestId::EST_TEST_BD_LAMP_ID, &MiscStrs::SM_BD_LAMP_TEST_ID },
	{ SmTestId::EST_TEST_GUI_AUDIO_ID, &MiscStrs::SM_GUI_AUDIO_TEST_ID },
	{ SmTestId::EST_TEST_BD_AUDIO_ID, &MiscStrs::SM_BD_AUDIO_TEST_ID },
	{ SmTestId::EST_TEST_FS_CROSS_CHECK_ID, &MiscStrs::SM_FS_CROSS_CHECK_TEST_ID },
	{ SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID, &MiscStrs::SM_CIRCUIT_PRESSURE_TEST_ID },
	{ SmTestId::EST_TEST_SAFETY_SYSTEM_ID, &MiscStrs::SM_SAFETY_SYSTEM_TEST_ID },
	{ SmTestId::EST_TEST_EXH_VALVE_SEAL_ID, &MiscStrs::SM_EXH_VALVE_SEAL_TEST_ID },
	{ SmTestId::EST_TEST_EXH_VALVE_ID, &MiscStrs::SM_EXH_VALVE_TEST_ID },
	{ SmTestId::EST_TEST_SM_LEAK_ID, &MiscStrs::SM_SM_LEAK_TEST_ID },
	{ SmTestId::EST_TEST_EXH_HEATER_ID, &MiscStrs::SM_EXH_HEATER_TEST_ID },
	{ SmTestId::EST_TEST_GENERAL_ELECTRONICS_ID, &MiscStrs::SM_GENERAL_ELECTRONICS_TEST_ID },
	{ SmTestId::EST_TEST_GUI_TOUCH_ID, &MiscStrs::SM_GUI_TOUCH_TEST_ID },
	{ SmTestId::EST_TEST_GUI_SERIAL_PORT_ID, &MiscStrs::SM_GUI_SERIAL_PORT_TEST_ID },
	{ SmTestId::EST_TEST_GUI_NURSE_CALL_ID, &MiscStrs::SM_GUI_NURSE_CALL_TEST_ID },
	{ SmTestId::EST_TEST_BATTERY_ID, &MiscStrs::SM_BATTERY_TEST_ID }
};

// Table of SST test ids and test names.
EstSstTestNameStruct DiagnosticFault::SstTestNameStrs[] =
{
	{ SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID, &MiscStrs::SM_CIRCUIT_RESISTANCE_TEST_ID },
	{ SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, &MiscStrs::SM_SST_FILTER_TEST_ID },
	{ SmTestId::SST_TEST_CIRCUIT_LEAK_ID, &MiscStrs::SM_SST_LEAK_TEST_ID },
	{ SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID, &MiscStrs::SM_CIRCUIT_PRESSURE_TEST_ID },
	{ SmTestId::SST_TEST_FLOW_SENSORS_ID, &MiscStrs::SM_SST_FS_CC_TEST_ID },
	{ SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, &MiscStrs::SM_COMPLIANCE_CALIB_TEST_ID },
	{ SmTestId::SST_TEST_PROX_ID, &MiscStrs::SM_SST_PROX_STRING_ID }
};



// Fault messages for STARTUP (POST and INIT)
TestFaultStruct DiagnosticFault::StartupFaultStrs[] = 
{
	{ POST_ERROR_PROCESSOR_INITIALIZATION, &MiscStrs::SV_POST_PROCESSOR_INITIALIZATION },
	{ POST_ERROR_INTEGER_UNIT, 				&MiscStrs::SV_POST_INTEGER_UNIT_TEST },
	{ POST_ERROR_DRAM_REFRESH_TIMER, 		&MiscStrs::SV_POST_DRAM_REFRESH_TIMER_TEST },
	{ POST_ERROR_DRAM_STACK, 				&MiscStrs::SV_POST_KERNEL_DRAM_TEST },
	{ POST_ERROR_BOOT_EPROM, 				&MiscStrs::SV_POST_BOOT_EPROM_CHECKSUM_TEST },
	{ POST_ERROR_PHASE_2, 					&MiscStrs::SV_POST_PHASE_2_TEST},
	{ POST_ERROR_ADDRESS_MODE, 				&MiscStrs::SV_POST_ADDRESSING_MODE_TEST },
	{ POST_ERROR_NOVRAM, 					&MiscStrs::SV_POST_KERNEL_NOVRAM_TEST },
	{ POST_ERROR_ROLLING_THUNDER, 			&MiscStrs::SV_POST_ROLLING_THUNDER_TEST },
	{ POST_ERROR_INTERRUPT, 				&MiscStrs::SV_POST_INTERRUPT_CONTROLLER_TEST },
	{ POST_ERROR_TOD, 						&MiscStrs::SV_POST_TIME_OF_DAY_CLOCK_TEST },
	{ POST_ERROR_TIMER, 					&MiscStrs::SV_POST_TIMER_TEST },
	{ POST_ERROR_WATCHDOG, 					&MiscStrs::SV_POST_WATCHDOG_TIMER_TEST },
	{ POST_ERROR_EEPROMCHECKSUM, 			&MiscStrs::SV_POST_EEPROM_CHECKSUM_TEST },

	{ POST_ERROR_MMU, &MiscStrs::SV_POST_MEMORY_MANAGEMENT_UNIT_TEST },
	{ POST_ERROR_BTO, &MiscStrs::SV_POST_BUS_TIMER_TEST },
	{ POST_ERROR_NMI_REGISTER, &MiscStrs::SV_POST_NMI_REGISTER_TEST },
	{ POST_ERROR_DRAM_MEMORY, &MiscStrs::SV_POST_DRAM_TEST },
	{ POST_ERROR_LAN_SELFTEST_START, &MiscStrs::SV_POST_LAN_SELFTEST_START },
	{ POST_ERROR_LAN_SELFTEST_END, &MiscStrs::SV_POST_LAN_SELFTEST_END },
	{ POST_ERROR_UNEXPECTED_RESET_UMPIRE, &MiscStrs::SV_POST_UNEXPECTED_RESET_UMPIRE_TEST },
	{ POST_ERROR_POST_NOVRAM, &MiscStrs::SV_POST_NOVRAM},
	{ POST_ERROR_FPU , &MiscStrs::SV_POST_ERROR_FPU },
	{ POST_ERROR_DRAM_PARITY, &MiscStrs::SV_POST_ERROR_DRAM_PARITY },
	//To do E600 needs to be revisted for sound class
	{ POST_ERROR_SAASTEST_START, &MiscStrs::SV_POST_SAASTEST_START },
	{ POST_ERROR_SAASTEST_END, &MiscStrs::SV_POST_SAASTEST_END },
	{ POST_ERROR_RESONANCE_START, &MiscStrs::SV_POST_ERROR_RESONANCE_START },
	{ POST_ERROR_RESONANCE_END, &MiscStrs::SV_POST_ERROR_RESONANCE_END },
	{ POST_ERROR_VENTINOP, 		&MiscStrs::SV_POST_VENTINOP },
	{ POST_ERROR_ANALOG_INTERFACE_PCB, &MiscStrs::SV_POST_ANALOG_INTERFACE_PCB_TEST },
	{ POST_ERROR_ADC, 				&MiscStrs::SV_POST_ADC_TEST },
	{ POST_ERROR_DAC, 				&MiscStrs::SV_POST_DAC_TEST },
	{ POST_ERROR_ANALOG_DEVICES, 	&MiscStrs::SV_POST_ANALOG_DEVICES_TEST },
	{ POST_ERROR_SERIAL_DEVICE, 	&MiscStrs::SV_POST_ERROR_SERIAL_DEVICE },
	{ POST_ERROR_SAFESTATE_SYSTEM , &MiscStrs::SV_POST_SAFESTATE_SYSTEM_TEST },
	{ POST_ERROR_SERVICE_SWITCH, 	&MiscStrs::SV_SERVICE_SWITCH_STUCKED },
	{ POST_ERROR_AC_VOLTAGE	, 		&MiscStrs::SV_AC_VOLTAGE_TEST },
	{ POST_ERROR_DOWNLOAD_BOOT, 	&MiscStrs::SV_POST_DOWNLOAD_BOOT },
	{ POST_ERROR_SIGMA_OS_BOOT, 	&MiscStrs::SV_POST_SIGMA_OS_BOOT },
	{ POST_ERROR_PBMON_BOOT, 		&MiscStrs::SV_POST_PBMON_BOOT },
	{ POST_ERROR_APPLICATION_BOOT,  &MiscStrs::SV_POST_APPLICATION_BOOT }
};



// Fault messages for  BACKGROUND
TestFaultStruct DiagnosticFault::BackgroundFaultStrs[] = 
{
	{ BK_NO_EVENT, &MiscStrs::SV_BK_NO_EVENT },
	{ BK_SV_SWITCHED_SIDE_OOR, &MiscStrs::SV_BK_SV_SWITCHED_SIDE_OOR },
	{ BK_EXH_FLOW_OOR, &MiscStrs::SV_BK_EXH_FLOW_OOR },
	{ BK_O2_PSOL_CURRENT_OOR, &MiscStrs::SV_BK_O2_PSOL_CURRENT_OOR },
	{ BK_AIR_PSOL_CURRENT_OOR, &MiscStrs::SV_BK_AIR_PSOL_CURRENT_OOR },
	{ BK_EXH_MOTOR_CURR_OOR, &MiscStrs::SV_BK_EXH_MOTOR_CURR_OOR },
	{ BK_EXH_VLV_COIL_TEMP_OOR, &MiscStrs::SV_BK_EXH_VLV_COIL_TEMP_OOR },
	{ BK_EXH_PRESS_OOR, &MiscStrs::SV_BK_EXH_PRESS_OOR },
	{ BK_INSP_PRESS_OOR, &MiscStrs::SV_BK_INSP_PRESS_OOR },
	{ BK_AIR_FLOW_OOR_HIGH, &MiscStrs::SV_BK_AIR_FLOW_OOR_HIGH},
	{ BK_AIR_FLOW_OOR_LOW, &MiscStrs::SV_BK_AIR_FLOW_OOR_LOW},
	{ BK_AIR_FLOW_TEMP_OOR, &MiscStrs::SV_BK_AIR_FLOW_TEMP_OOR },
	{ BK_O2_FLOW_OOR_HIGH, &MiscStrs::SV_BK_O2_FLOW_OOR_HIGH},
	{ BK_O2_FLOW_OOR_LOW, &MiscStrs::SV_BK_O2_FLOW_OOR_LOW},
	{ BK_O2_FLOW_TEMP_OOR, &MiscStrs::SV_BK_O2_FLOW_TEMP_OOR },
	{ BK_EXH_FLOW_TEMP_OOR, &MiscStrs::SV_BK_EXH_FLOW_TEMP_OOR },
	{ BK_BD_10V_SUPPLY_MON_OOR, &MiscStrs::SV_BK_BD_10V_SUPPLY_MON_OOR },
	{ BK_BD_12V_FAIL_OOR, &MiscStrs::SV_BK_BD_12V_FAIL_OOR },
	{ BK_BD_15V_FAIL_OOR, &MiscStrs::SV_BK_BD_15V_FAIL_OOR },
	{ BK_NEG_15V_FAIL_OOR, &MiscStrs::SV_BK_NEG_15V_FAIL_OOR },
	{ BK_GUI_12V_FAIL_OOR, &MiscStrs::SV_BK_GUI_12V_FAIL_OOR },
	{ BK_GUI_5V_FAIL_OOR, &MiscStrs::SV_BK_GUI_5V_FAIL_OOR },
	{ BK_BD_5V_FAIL_OOR, &MiscStrs::SV_BK_BD_5V_FAIL_OOR },
	{ BK_O2_PSOL_STUCK, &MiscStrs::SV_BK_O2_PSOL_STUCK},
	{ BK_AIR_PSOL_STUCK, &MiscStrs::SV_BK_AIR_PSOL_STUCK},
	{ BK_AIR_PSOL_STUCK_OPEN, &MiscStrs::SV_BK_AIR_PSOL_STUCK_OPEN },
	{ BK_O2_PSOL_STUCK_OPEN, &MiscStrs::SV_BK_O2_PSOL_STUCK_OPEN },
	{ BK_ATM_PRESS_OOR, &MiscStrs::SV_BK_ATM_PRESS_OOR },
	{ BK_O2_SENSOR_OOR, &MiscStrs::SV_BK_O2_SENSOR_OOR },
	{ BK_O2_SENSOR_OOR_RESET, &MiscStrs::SV_BK_O2_SENSOR_OOR_RESET},
	{ BK_SVO_CURRENT_OOR, &MiscStrs::SV_BK_SVO_CURRENT_OOR },
	{ BK_PI_STUCK, &MiscStrs::SV_BK_PI_STUCK},
	{ BK_PE_STUCK, &MiscStrs::SV_BK_PE_STUCK},
	{ BK_INSP_AUTOZERO_FAIL, &MiscStrs::SV_BK_INSP_AUTOZERO_FAIL },
	{ BK_EXH_AUTOZERO_FAIL, &MiscStrs::SV_BK_EXH_AUTOZERO_FAIL },
	{ BK_POWER_FAIL_CAP, &MiscStrs::SV_BK_POWER_FAIL_CAP },
	{ BK_ALARM_CABLE, &MiscStrs::SV_BK_ALARM_CABLE },
	{ BK_ADC_FAIL_HIGH, &MiscStrs::SV_BK_ADC_FAIL_HIGH },
	{ BK_ADC_FAIL_LOW, &MiscStrs::SV_BK_ADC_FAIL_LOW },
	{ BK_ADC_LOOPBACK_FAIL, &MiscStrs::SV_BK_ADC_LOOPBACK_FAIL },
	{ BK_TOUCH_SCREEN_FAIL, &MiscStrs::SV_BK_TOUCH_SCREEN_FAIL },
	{ BK_TOUCH_SCREEN_BLOCKED, &MiscStrs::SV_BK_TOUCH_SCREEN_BLOCKED },
	{ BK_TOUCH_SCREEN_RESUME, &MiscStrs::SV_BK_TOUCH_SCREEN_RESUME },
	{ BK_AC_SWITCH_STUCK, &MiscStrs::SV_BK_AC_SWITCH_STUCK },
	{ BK_BD_NOVRAM_CHECKSUM, &MiscStrs::SV_BK_BD_NOVRAM_CHECKSUM },
	{ BK_BD_TOD_FAIL, &MiscStrs::SV_BK_BD_TOD_FAIL },
	{ BK_GUI_TOD_FAIL, &MiscStrs::SV_BK_GUI_TOD_FAIL },
	{ BK_GUI_NOVRAM_CHECKSUM, &MiscStrs::SV_BK_GUI_NOVRAM_CHECKSUM },
	{ BK_BPS_VOLTAGE_OOR , &MiscStrs::SV_BK_BPS_VOLTAGE_OOR },
	{ BK_BPS_CURRENT_OOR, &MiscStrs::SV_BK_BPS_CURRENT_OOR },
	{ BK_BPS_MODEL_OOR, &MiscStrs::SV_BK_BPS_MODEL_OOR },
	{ BK_EXH_HEATER_OOR, &MiscStrs::SV_BK_EXH_HEATER_OOR },
	{ BK_GUI_STUCK_KEY, &MiscStrs::SV_BK_GUI_STUCK_KEY },
	{ BK_BD_EEPROM_CHECKSUM, &MiscStrs::SV_BK_BD_EEPROM_CHECKSUM },
	{ BK_GUI_EEPROM_CHECKSUM, &MiscStrs::SV_BK_GUI_EEPROM_CHECKSUM },
	//To do E600 needs to be revisted for sound class
	{ BK_GUI_SAAS_COMM_FAIL, &MiscStrs::SV_BK_GUI_SAAS_COMM_FAIL },
	{ BK_COMPR_ELAPSED_TIMER, &MiscStrs::SV_BK_COMPR_ELAPSED_TIMER },
	{ BK_COMPR_BAD_DATA, &MiscStrs::SV_BK_COMPR_BAD_DATA },
	{ BK_LOSS_OF_GUI_COMM, &MiscStrs::SV_BK_LOSS_OF_GUI_COMM },
	{ BK_LOSS_OF_BD_COMM, &MiscStrs::SV_BK_LOSS_OF_BD_COMM },
	{ BK_RESUME_GUI_COMM, &MiscStrs::SV_BK_RESUME_GUI_COMM },
	{ BK_RESUME_BD_COMM, &MiscStrs::SV_BK_RESUME_BD_COMM },
	{ BK_EST_REQUIRED, &MiscStrs::SV_BK_EST_REQUIRED },
	//To do E600 tests needs to be revisted for sound class
	{ BK_GUI_SAAS_AUDIO_FAIL, &MiscStrs::SV_BK_GUI_SAAS_AUDIO_FAIL },
	{ BK_LV_REF_OOR, &MiscStrs::SV_BK_LV_REF_OOR },
	{ BK_SV_CURRENT_OOR, &MiscStrs::SV_BK_SV_CURRENT_OOR },
    { BK_MON_ALARMS_FAIL, &MiscStrs::SV_BK_MON_ALARMS_FAIL },
    { BK_MON_APNEA_ALARM_FAIL, &MiscStrs::SV_BK_MON_APNEA_ALARM_FAIL},
    { BK_MON_APNEA_INT_FAIL, &MiscStrs::SV_BK_MON_APNEA_INT_FAIL},
    { BK_MON_HIP_FAIL, &MiscStrs::SV_BK_MON_HIP_FAIL},
    { BK_MON_BREATH_TIME_FAIL, &MiscStrs::SV_BK_MON_BREATH_TIME_FAIL},
    { BK_MON_INSP_TIME_FAIL, &MiscStrs::SV_BK_MON_INSP_TIME_FAIL},
    { BK_MON_NO_DATA, &MiscStrs::SV_BK_MON_NO_DATA},
    { BK_MON_O2_MIXTURE_FAIL, &MiscStrs::SV_BK_MON_O2_MIXTURE_FAIL},
    { BK_MON_DATA_CORRUPTED, &MiscStrs::SV_BK_MON_DATA_CORRUPTED},
    { BK_DATAKEY_UPDATE_FAIL, &MiscStrs::SV_BK_DATAKEY_UPDATE_FAIL},
    { BK_TASK_MONITOR_FAIL, &MiscStrs::SV_BK_TASK_MONITOR_FAIL},
    { BK_GUI_WAVEFORM_DROP, &MiscStrs::SV_BK_GUI_WAVEFORM_DROP},
    { BK_BD_WAVEFORM_DROP, &MiscStrs::SV_BK_BD_WAVEFORM_DROP},
    { BK_FORCED_VENTINOP, &MiscStrs::SV_BK_FORCED_VENTINOP},
	{ BK_BD_MULT_MAIN_CYCLE_MSGS, &MiscStrs::SV_BK_BD_MULT_MAIN_CYCLE_MSGS},
	{ BK_BD_MULT_SECOND_CYCLE_MSGS, &MiscStrs::SV_BK_BD_MULT_SECOND_CYCLE_MSGS},
	{ BK_WATCHDOG_FAILURE, &MiscStrs::SV_BK_WATCHDOG_FAILURE},
	{ BK_INIT_RESUME_GUI_COMM, &MiscStrs::SV_BK_INIT_RESUME_GUI_COMM },
	{ BK_INIT_RESUME_BD_COMM, &MiscStrs::SV_BK_INIT_RESUME_BD_COMM },
	{ BK_INIT_LOSS_GUI_COMM, &MiscStrs::SV_BK_INIT_LOSS_GUI_COMM },
	{ BK_INIT_LOSS_BD_COMM, &MiscStrs::SV_BK_INIT_LOSS_BD_COMM },
	{ BK_COMPR_UPDATE_SN, &MiscStrs::SV_BK_COMPR_UPDATE_SN },
	{ BK_COMPR_UPDATE_PM_HRS, &MiscStrs::SV_BK_COMPR_UPDATE_PM_HRS },
	{ BK_DATAKEY_SIZE_ERROR, &MiscStrs::SV_BK_DATAKEY_SIZE_ERROR },
	{ BK_TOUCH_SCREEN_BLOCKED_INFO, &MiscStrs::SV_BK_TOUCH_SCREEN_BLOCKED },
	{ BK_TOUCH_SCREEN_RESUME_INFO, &MiscStrs::SV_BK_TOUCH_SCREEN_RESUME },
	{ BK_BPS_EVENT_INFO, &MiscStrs::SV_BK_BPS_EVENT_INFO },
	{ BK_COMPACT_FLASH_ERROR_INFO, &MiscStrs::SV_BK_COMPACT_FLASH_ERROR_INFO },
	{ BK_PROX_ERROR_INFO, &MiscStrs::SV_BK_PROX_ERROR_INFO }
};


// Fault messages for software traps.
TestFaultStruct DiagnosticFault::TrapFaultStrs[] = 
{
	{ ACCESS_FAULT_ID, &MiscStrs::SV_TRAP_ACCESS_FAULT },
    { ADDRESS_ERROR_ID, &MiscStrs::SV_TRAP_ADDRESS_ERROR},
    { ILLEGAL_INSTRUCTION_ID, &MiscStrs::SV_TRAP_ILLEGAL_INSTRUCTION },
    { INTEGER_DIVIDE_BY_ZERO, &MiscStrs::SV_TRAP_INTEGER_DIVIDE_BY_ZERO },
    { PRIVILEGE_VIOLATION_ID, &MiscStrs::SV_TRAP_PRIVILEGE_VIOLATION },
    { FORMAT_ERROR_ID, &MiscStrs::SV_TRAP_FORMAT_ERROR },
    { UNINITIALIZED_INTERRUPT, &MiscStrs::SV_TRAP_UNINITIALIZED_INTERRUPT }
};


// Table of system events and report messages.
TestFaultStruct DiagnosticFault::SystemEventStrs[] = 
{
	{ DiagnosticCode::TASK_CONTROL_TRANSITION_TIMEOUT, &MiscStrs::DLOG_TRANSITION_TIMEOUT },
	{ DiagnosticCode::REAL_TIME_CLOCK_UPDATE_PENDING, &MiscStrs::DLOG_DATE_TIME_BEFORE_CHANGE },
	{ DiagnosticCode::REAL_TIME_CLOCK_UPDATE_DONE, &MiscStrs::DLOG_DATE_TIME_CHANGED },
	{ DiagnosticCode::DIAG_LOG_ENTRY_CORRECTED, &MiscStrs::DLOG_CORRUPTED },
	{ DiagnosticCode::WATCHDOG_TIMER_FAILED, &MiscStrs::DLOG_WATCHDOG_TIMER_FAILED },
	{ DiagnosticCode::BD_CLOCK_SYNC_PENDING, &MiscStrs::DLOG_DATE_TIME_BEFORE_SYNC },
	{ DiagnosticCode::BD_CLOCK_SYNC_DONE, &MiscStrs::DLOG_DATE_TIME_SYNCHRONIZED },
	{ DiagnosticCode::VENT_IN_OP_LATCHED, &MiscStrs::DLOG_VENT_IN_OP_LATCHED },
	{ DiagnosticCode::INTENTIONAL_RESET_FORCED, &MiscStrs::DLOG_INTENTIONAL_RESET_FORCED },
	{ DiagnosticCode::SETTINGS_XACTION_FAILED, &MiscStrs::DLOG_SETTINGS_XACTION_FAILED },
	{ DiagnosticCode::SETTINGS_XACTION_SUCCEEDED, &MiscStrs::DLOG_SETTINGS_XACTION_SUCCEEDED },
	{ DiagnosticCode::SETTINGS_STATE_MISMATCH, &MiscStrs::DLOG_SETTINGS_SYNC_MISMATCH },
	{ DiagnosticCode::APP_NOVRAM_ACCESS_FAILURE, &MiscStrs::DLOG_NOVRAM_ACCESS_FAULT },
	{ DiagnosticCode::APP_NOVRAM_RESTORE_FAILURE, &MiscStrs::DLOG_NOVRAM_RESTORE_FAULT }
};



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTestEventStr 		[static]
//
//@ Interface-Description
//	Called by getLogEntryColumn() method of the same class to display the error
//	message.  This method is passed two arguments:  faultId which
//	represents the Diagnostic Fault Id.  pFaultStruct is the address
// 	of the error message table.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Searches in the error table for the entry which has matching 'faultId' (input value).
// Returns the message string in the current entry, if found.  
//	Otherwise, return the predefined string.
//---------------------------------------------------------------------
//@ PreCondition
// 	The table has to exist (non NULL)  
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
StringId
DiagnosticFault::GetTestEventStr(
				Uint16 faultId, TestFaultStruct pFaultStruct[], Uint16 arrSize)
{
	CALL_TRACE("DiagnosticFault::GetTestEventStr(Uint16 faultId,"
							"TestFaultStruct pFaultStruct[], Uint16 arrSize)");
#ifdef SIGMA_DEVELOPMENT
	SAFE_CLASS_ASSERTION( (pFaultStruct != NULL) && (arrSize > 0) );
#endif
	int count = 0;

	while (count < arrSize)
    {										// $[TI1]	
		if (pFaultStruct[count].testFaultId == faultId)
		{									// $[TI2]
	    	return (pFaultStruct[count].testFaultName[GuiApp::GetLanguageIndex()]);
		}
		else
		{									// $[TI3]
			count++;
		}
    }										// $[TI4]
    return( MiscStrs::DIAG_UNDEFINED_ERROR ) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEstTestEventStr 	[static]
//
//@ Interface-Description
//	Called by DiagCodeLogSubScreen's getLogEntryColumn() method to display the error
//	message.  This method is passed along two arguments:  testNumber which
//	represents the test Id and conditionNumber is the error condition of the test. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	Searches in the EST error table for the id that matches the given 
//	fault id and error condition.  Returns the string in the current entry, if found.  
//	Otherwise, return the predefined string.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
StringId
DiagnosticFault::GetEstTestEventStr(
						Uint16 testNumber, Uint16 conditionNumber)
{
	CALL_TRACE("DiagnosticFault::GetEstTestEventStr(Uint16 testNumber,"
								"Uint16 conditionNumber");
#ifdef SIGMA_DEVELOPMENT
	SAFE_CLASS_ASSERTION	( (testNumber < SmTestId::EST_TEST_MAX)
											&& (conditionNumber > 0) );
#endif

	EstSstTestFaultStruct *pFaultStruct = EstTestFaultStrs;	
	int arrSize = sizeof(EstTestFaultStrs) / sizeof(EstSstTestFaultStruct);
	int count = 0;

	while (count < arrSize)
    {										// $[TI1]
		if ( (pFaultStruct[count].testNumber == testNumber) && 
			 (pFaultStruct[count].conditionNumber == conditionNumber) )
		{									// $[TI2]
#ifdef SIGMA_DEVELOPMENT
			SAFE_CLASS_ASSERTION(pFaultStruct[count].conditionName[GuiApp::GetLanguageIndex()] != NULL);
#endif
	    	return (pFaultStruct[count].conditionName[GuiApp::GetLanguageIndex()]);
		}
		else
		{									// $[TI3]
			count++;
		}
    }										// $[TI4]
    return( MiscStrs::DIAG_UNDEFINED_TEST_CASE) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSstTestEventStr 	[static]
//
//@ Interface-Description
//	Called by DiagCodeLogSubScreen's getLogEntryColumn() method to display the error
//	message.  This method is passed along two arguments:  testNumber which
//	represents the test Id and conditionNumber is the error condition of the test. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	Searches in the SST error table for the id that matches the given 
//	fault id and error condition.  Returns the string in the current entry, if found.  
//	Otherwise, return the predefined string.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
StringId
DiagnosticFault::GetSstTestEventStr(
						Uint16 testNumber, Uint16 conditionNumber)
{
	CALL_TRACE("DiagnosticFault::GetSstTestEventStr(Uint16 testNumber,"
								"Uint16 conditionNumber");

#ifdef SIGMA_DEVELOPMENT
	SAFE_CLASS_ASSERTION	( (testNumber < SmTestId::SST_TEST_MAX)
					&& (conditionNumber > 0) );
#endif

	EstSstTestFaultStruct *pFaultStruct = SstTestFaultStrs;	
	int arrSize = sizeof(SstTestFaultStrs) / sizeof(EstSstTestFaultStruct);
	int count = 0;

	while (count < arrSize)
    {										// $[TI1]
		if ( (pFaultStruct[count].testNumber == testNumber) && 
			 (pFaultStruct[count].conditionNumber == conditionNumber) )
		{									// $[TI2]

#ifdef SIGMA_DEVELOPMENT
			SAFE_CLASS_ASSERTION(pFaultStruct[count].conditionName[GuiApp::GetLanguageIndex()] != NULL);
#endif
	    	return (pFaultStruct[count].conditionName[GuiApp::GetLanguageIndex()]);
		}
		else
		{									// $[TI3]
			count++;
		}
    }										// $[TI4]
    return( MiscStrs::DIAG_UNDEFINED_TEST_CASE) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SearchDiagCodeString [private]
//
//@ Interface-Description
//	Called by the class itself 
// Input:	the testType (EST or SST)
//				testId
//					
//	Output:	displayString which contains diagnostic code message display string.
//				firstStepNumber:	step number of the first diagnostic code found.
//				secondStepNumber:	step number of the second diagnostic code found (if applied)
//				
//	This method returns the total of diagnostic codes found.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First, retrieve the Diagnostics Code Log entries from NovRam.
//	Using this array, query NovRam for the list of all entries matching the
// given test id.  This list contains the diagnostics information we need
//	to display.  Then we start looping through this list.  If we detect that
//	this test is not installed at the presence, we simply return an empty
//	count and string.  Otherwise, we compose a list of all diagnostic codes
//	for the given test.   We return the their total count plus the first
//	and second step (condition) number, tailored to the needs of the GUI display.
//---------------------------------------------------------------------
//@ PreCondition
//	displayString must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
 
Uint16
DiagnosticFault::SearchDiagCodeString(Uint16 testType, 
						Uint16 testId, 
						wchar_t *displayString,
						Uint8 &firstStepNumber, Uint8 &secondStepNumber)
{
	CALL_TRACE("DiagnosticFault::SearchDiagCodeString(Uint16 testType,"
		"Uint16 testId, char displayString[],"
		"Uint8 &firstStepNumber Uint8 &secondStepNumber)");


	CLASS_ASSERTION(displayString != NULL);


	Uint8 stepNumber, failureLevel;
	static const wchar_t* Space = L" ";  
	Uint16 strCount = 0;	// The number of diagnostic string returned.
	wchar_t diagCodeString[8];  
	DiagnosticCode diagCode;
	DiagnosticCode::ExtendedSelfTestBits estBits;
	DiagnosticCode::ShortSelfTestBits sstBits;


	firstStepNumber = secondStepNumber = UCHAR_MAX;

	// Get array of EST/SST diagnostic entries from NovRam
	FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES) logEntries;

	// Call NovRamManager' method to get the array of test diagnostics for
	// a given test id.
	if (testType == DiagnosticCode::EXTENDED_SELF_TEST_DIAG)
	{										// $[TI1]
		NovRamManager::GetEstTestDiagnostics(testId, logEntries);
	}
	else if (testType == DiagnosticCode::SHORT_SELF_TEST_DIAG)
	{										// $[TI2]	
		NovRamManager::GetSstTestDiagnostics(testId, logEntries);
	}	


	// Search in the diagnostic log for all entries with matching testId
	for (int i = 0; (i < logEntries.getNumElems()) && 
									(!logEntries[i].isClear()); i++)
	{										// $[TI3]
		diagCode = logEntries[i].getDiagnosticCode();

		// Return the step number for the first diagnostic code matched
		if (testType == DiagnosticCode::EXTENDED_SELF_TEST_DIAG)
		{									// $[TI4]
			estBits = diagCode.getExtendedSelfTestCode();
			failureLevel = estBits.failureLevel;
			stepNumber = estBits.stepNumber;
		}
		else if (testType == DiagnosticCode::SHORT_SELF_TEST_DIAG)
		{									// $[TI5]
			sstBits = diagCode.getShortSelfTestCode();
			failureLevel = sstBits.failureLevel;
			stepNumber = sstBits.stepNumber;
		}
		else
		{
#ifdef SIGMA_DEVELOPMENT
			SAFE_CLASS_ASSERTION_FAILURE();		// Should never happen
#endif
		}

		// If test is not installed at present time, simply return a 0 count.
		if (failureLevel == NOT_INSTALLED_TEST_ID)
		{									// $[TI6]
			return 0;
		}									// $[TI7]

		diagCode.getDiagCodeString(diagCodeString);
		wcscat(displayString, diagCodeString);  
		wcscat(displayString, Space);          
		++strCount;

		if (strCount == 1)
		{									// $[TI8]
			firstStepNumber = stepNumber;
		}
		else if (strCount == 2)
		{									// $[TI9]
			secondStepNumber = stepNumber;
		}
	}

	if (strCount == 0)
	{										// $[TI10]
		displayString[0] = L'\0';
	}										// $[TI11]
	return (strCount);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	FormatString		[private]
//
//@ Interface-Description
//	Format a diagnostic code message to be displayed on the GUI's result tables.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Call GetSstTestEventStr() or GetEstTestEventStr() to get the error text then concatenate 
//	it to the returned string.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
 
void 
DiagnosticFault::FormatString(Uint16 diagType, wchar_t *resultString, wchar_t *fromString, 
										Uint16 testId, Uint8 stepNumber)
{
	wcscpy(resultString, fromString);	
	wcscat(resultString, L":");
	wcscat(resultString, L" ");
	if (diagType == DiagnosticCode::EXTENDED_SELF_TEST_DIAG)
	{		// $[TI1]
		wcscat(resultString, GetEstTestEventStr(testId, stepNumber));
	}
	else if (diagType == DiagnosticCode::SHORT_SELF_TEST_DIAG)
	{		// $[TI2]
		wcscat(resultString, GetSstTestEventStr(testId, stepNumber));
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetCode	[private]
//
//@ Interface-Description
// This function gets the first token (non-space) from a given string.
//	It returns the address of the next character in the input stream. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	We parse the given string character by character, looking for the next 
//	string token in this string.  We return the string token via the
//	parameter list, the start of the next non-blank character in the main
//	string
//---------------------------------------------------------------------
//@ PreCondition
//	Both strings must be non-null, and the given string size must be > 0.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
wchar_t *
DiagnosticFault::GetCode(wchar_t *theString, wchar_t *returnStr, int strSize)
{
	CALL_TRACE("DiagnosticFault::GetCode(char *theString, char *returnStr,"
												"int strSize)");

	CLASS_ASSERTION( (theString != NULL) && (returnStr != NULL) 
											&& (strSize > 0) );

	int  count = 0;			// collective count as more characters are added
	wchar_t *tmp1 = theString, *tmp2 = returnStr;

	// Find first token from theString and assign it to returnStr
	for (;
		(*tmp1 != L' ') && (*tmp1 != L'\0') && (count < strSize-1); tmp1++)
	{	// $[TI1]
		*tmp2 = *tmp1;
		count++;
		tmp2++;
	}	// $[TI2]
	*tmp2 = L'\0';

	// We want to skip trailing blanks
	while (*tmp1 == L' ')
	{	// $[TI3]
		tmp1++;
	}	// // $[TI4]

	return tmp1;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ComposeDisplayDiagStrings [static]
//
//@ Interface-Description
//	Called by GUI
// Input: The diagnostic type (EST/SST ) and the testId. 
//	Output: displayString1 and displayString2.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Call SearchDiagCodeString() to search in the EST/SST diagnostic log for
// entries which match the given test id.  Then format the display strings 
// to be the output.
//--------------------------------------------------------------------- 
//@ PreCondition
// Input displayString1 (and displayString2 if EST) must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
DiagnosticFault::ComposeDisplayDiagStrings(Uint16 diagType, Uint16 testId, 
				wchar_t *displayString1, wchar_t *displayString2)
{
	CALL_TRACE("DiagnosticFault::ComposeDisplayDiagStrings("
		"Uint16 diagType, Uint16 testId,"
		"char displayString1[], char displayString2[])");

	CLASS_ASSERTION(displayString1 != NULL);

	if (diagType == DiagnosticCode::EXTENDED_SELF_TEST_DIAG)
	{		// $[TI1]
		CLASS_ASSERTION(displayString2 != NULL);
	}		// $[TI2]


	Uint8 firstStepNumber, secondStepNumber;	// condition number
	const int MAX_STRING = 256;
	wchar_t  *currPtr;
	wchar_t diagStr[MAX_STRING];
	wchar_t displayString[MAX_STRING];	

	diagStr[0] = L'\0';
	displayString[0] = L'\0';
	firstStepNumber = secondStepNumber = UCHAR_MAX;

	Uint16 count = SearchDiagCodeString(diagType, testId, 
							displayString, 
							firstStepNumber, secondStepNumber);

	// No such entries found in the diagnostic log, simply return empty strings 
	if (count == 0)
	{								// $[TI3]
		// Empty strings
		displayString1[0] = L'\0';
		if (displayString2 != NULL)
		{							// $[TI4]
			displayString2[0] = L'\0';
		}							// $[TI5]
	}								// $[TI6]
	// Only 1 diagnostic code, format the display string to be returned
	else if (count == 1)
	{								// $[TI7]
		FormatString(diagType, displayString1, displayString, testId, firstStepNumber);
		if (displayString2 != NULL)
		{							// $[TI8]
			displayString2[0] = L'\0';
		}							// $[TI9]
	}
	else if (count >= 2)	// Format two diagnostic code into display strings
	{								// $[TI10]
		if (diagType == DiagnosticCode::EXTENDED_SELF_TEST_DIAG)
		{							// $[TI11]
			currPtr = GetCode(displayString, diagStr, MAX_STRING);
			if (count == 2)
			{						// $[TI12]
				// Format first string 
				FormatString(diagType, displayString1, diagStr, testId, firstStepNumber); 
				GetCode(currPtr, diagStr, MAX_STRING);
				FormatString(diagType, displayString2, diagStr, testId, secondStepNumber); 
			}
			else // count > 2
			{						// $[TI13]
				FormatString(diagType, displayString1, diagStr, testId, firstStepNumber); 
				wcscpy(displayString2, currPtr);
			}
		}	
		else if (diagType == DiagnosticCode::SHORT_SELF_TEST_DIAG)
		{							// $[TI14]
			// Only display string shows both codes
			wcscpy(displayString1, displayString);
		}
		else
		{
#ifdef SIGMA_DEVELOPMENT
			SAFE_CLASS_ASSERTION_FAILURE();
#endif
		}
	}
	return;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetCommTestEventStr [static]
//
//@ Interface-Description
//  Called by any application which wants to get the Network Communications
//  sub-system's error text given a Network Communications error number.
//	 Input: Network Communications error number.
//	 Output: Error event string.
//---------------------------------------------------------------------
//@ Implementation-Description: 
//	 Invoke method DiagnosticFault::GetTestEventStr() to search for the event name.
// 	Return the string found.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

StringId 
DiagnosticFault::GetCommTestEventStr(Uint16 faultId)
{
	Uint16 arrSize = sizeof(CommFaultStrs) / sizeof(TestFaultStruct);	
#ifdef DEBUG
	printf("arrSize= %d, NUM_COMM_FAULT_IDS = %d\n", arrSize, NUM_COMM_FAULT_IDS);
#endif

	// Make sure GUI-Apps diagnostic database is in sync with other subsystems.
#ifdef SIGMA_DEVELOPMENT
	SAFE_CLASS_ASSERTION( arrSize == (NUM_COMM_FAULT_IDS-1)); // See CommFaultId.hh
#endif

	return(DiagnosticFault::GetTestEventStr(faultId, CommFaultStrs, arrSize));
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSystemEventStr [static]
//
//@ Interface-Description
//  Called by any application which wants to get the  
//  System event string corresponding to a System Event Id 
//  Input:  System Event Id
//  Output: System Event String
//---------------------------------------------------------------------
//@ Implementation-Description: 
//	Search the System Event String table using the System Event Id.
//	Return the string found.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

StringId 
DiagnosticFault::GetSystemEventStr(Uint16 systemEventId)
{
	Uint16 arrSize = sizeof(SystemEventStrs) / sizeof(TestFaultStruct);	

	return(DiagnosticFault::GetTestEventStr(systemEventId, SystemEventStrs, 
														arrSize));
												// $[TI1]
															
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetStartupTestEventStr [static]
//
//@ Interface-Description
// Called by any application which wants to get the STARTUP
// sub-system's error text given a STARTUP error number.
// Input:  STARTUP error number. 
// Output: Error event string.
//---------------------------------------------------------------------
//@Implementation-Description:
//	Invoke method DiagnosticFault::GetTestEventStr() to search for the event
//	text in the stored table, given the fault id.  Return the string found.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

StringId 
DiagnosticFault::GetStartupTestEventStr(Uint16 faultId)
{
	Uint16 arrSize = sizeof(StartupFaultStrs) / sizeof(TestFaultStruct);	

	return(DiagnosticFault::GetTestEventStr(faultId, StartupFaultStrs, 
														arrSize));
												// $[TI1]
															
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBackgroundFaultStr [static]
//
//@ Interface-Description
// Called by any application which wants to get the BACKGROUND sub-system 
// error text given a BACKGROUND error number.
//	Input: BACKGROUND error number.
//	Output: Error event string.
//---------------------------------------------------------------------
//@ Implementation-Description: 
//	Invoke method DiagnosticFault::GetTestEventStr() to search for the event
//	text in the stored table, given the fault id.    Return the string found.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

StringId 
DiagnosticFault::GetBackgroundFaultStr(Uint16 faultId)
{
	Uint16 arrSize = sizeof(BackgroundFaultStrs) / sizeof(TestFaultStruct);

#ifdef DEBUG 
	TRACE2("arrSize= %d, BK_MAX_EVENT= %d\n", arrSize, BK_MAX_EVENT);
#endif
#ifdef SIGMA_DEVELOPMENT
	// Make sure GUI-Apps diagnostic database is in sync with other subsystems.
	SAFE_CLASS_ASSERTION( arrSize == BK_MAX_EVENT );
#endif

	return(DiagnosticFault::GetTestEventStr(faultId, BackgroundFaultStrs, arrSize));
												// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSstTestNameStr [static]
//
//@ Interface-Description
// Called by any application which wants to get the SST test name
// given a SST test number.
//	Input: SST test number.
//	Output: Sst test name.
//---------------------------------------------------------------------
//@	Implementation-Description: 
//	Retrieve the test name in the stored table given the test id.
// Return the string found.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

StringId 
DiagnosticFault::GetSstTestNameStr(Uint16 testNumber)
{
	CALL_TRACE("DiagnosticFault::GetSstTestNameStr(Uint16 testNumber)");

#ifdef SIGMA_DEVELOPMENT
	SAFE_CLASS_ASSERTION((testNumber >= SmTestId::SST_TEST_START_ID) && 
					(testNumber <= SmTestId::SST_TEST_MAX));
#endif

    Uint16 arrSize = sizeof(SstTestNameStrs) / sizeof(EstSstTestNameStruct);
	int count = 0;

    while (count < arrSize)
    {										// $[TI1]
		if (SstTestNameStrs[count].testId == testNumber)
		{									// $[TI2]
	    	return (SstTestNameStrs[count].testName[GuiApp::GetLanguageIndex()]);
		}
		else
		{									// $[TI3]
		    count++;
		}
    }										// $[TI4]
    return( MiscStrs::DIAG_UNDEFINED_ERROR ) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEstTestNameStr [static]
//
//@ Interface-Description
// Called by any application which wants to get the EST test name
// given an EST test number.
//	Input: EST test number.
//	Output: EST test name.
//---------------------------------------------------------------------
//@ Implementation-Description: 
//	Retrieve the test name in the stored table given the test id.
// Return the string found.
//---------------------------------------------------------------------
//@ PreCondition
//	Test number must be in the valid range [EST_TEST_START_ID, EST_TEST_MAX]
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

StringId 
DiagnosticFault::GetEstTestNameStr(Uint16 testNumber)
{
#ifdef SIGMA_DEVELOPMENT
	SAFE_CLASS_ASSERTION((testNumber >= SmTestId::EST_TEST_START_ID) && 
					(testNumber <= SmTestId::EST_TEST_MAX));
#endif

    Uint16 arrSize = sizeof(EstTestNameStrs) / sizeof(EstSstTestNameStruct);
	int count = 0;

    while (count < arrSize)
    {										// $[TI1]
		if (EstTestNameStrs[count].testId == testNumber)
		{									// $[TI2]
		    return (EstTestNameStrs[count].testName[GuiApp::GetLanguageIndex()]);
		}
		else
		{									// $[TI3]
		    count++;
		}
    }										// $[TI4]
    return( MiscStrs::DIAG_UNDEFINED_ERROR ) ;
}

#endif	//defined(SIGMA_GUI_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiagnosticFault::SoftFault(const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, DIAGNOSTICFAULT,
                            lineNumber, pFileName, pPredicate);
}
