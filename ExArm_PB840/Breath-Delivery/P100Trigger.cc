#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: P100Trigger - Trigger to enter P100 breath phase.
//---------------------------------------------------------------------
//@ Interface-Description
// This class triggers into the P100 pause phase based on a net flow
// transition from exhalation to inspiration. Upon triggering, the 
// scheduler starts the P100 occlusion (pause) breath phase.
//---------------------------------------------------------------------
//@ Rationale
// This class performs the calculations to determine if the 
// P100 breath phase can be activated.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class is derived from PatientTrigger. 
//---------------------------------------------------------------------
//@ Fault-Handling
// not applicable
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/P100Trigger.ccv   25.0.4.0   19 Nov 2013 13:59:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//=====================================================================

#include "P100Trigger.hh"
#include "ControllersRefs.hh"
#include "PatientTrigger.hh"
#include "MainSensorRefs.hh"
#include "FlowSensor.hh"
#include "ExhFlowSensor.h"
#include "ExhalationPhase.hh"
#include "PhaseRefs.hh"
//#include "PeepController.hh"

//@ Usage-Classes

//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: P100Trigger()  
//
//@ Interface-Description
//	Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize triggerId_ using PatientTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

P100Trigger::P100Trigger(void) : 
	PatientTrigger(Trigger::P100_MANEUVER),
	prevNetFlow_(0.0),
	elapsedTime_(0),
	flowThreshold_(0.0)

{
	CALL_TRACE("P100Trigger()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~P100Trigger()
//
//@ Interface-Description  
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

P100Trigger::~P100Trigger(void)
{
	CALL_TRACE("~P100Trigger()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable()
//
//@ Interface-Description
//	This method has no arguments and returns nothing.  This method is called to
//	enable the trigger.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The base class enable() method is called and data members are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
P100Trigger::enable(void)
{
	CALL_TRACE("P100Trigger::enable(void)") ;
	 
	// $[RM12090] When the P100 maneuver becomes armed, the P100 inspiratory trigger shall be enabled.
	// $[RM24003] Enable the P100 flow trigger.

	elapsedTime_ = 0;

	PatientTrigger::enable() ;
}

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
P100Trigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, NETFLOWINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
// This method takes no parameters.  If the trigger condition has occured,
// the method returns true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
// All of the following conditions must be met to trigger:
//  1. Exhalation flow controller target <= 1.0
//  2. Exhalation time > 200 ms
//  3. Net flow >= 0.0.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
P100Trigger::triggerCondition_(void)
{
	CALL_TRACE("triggerCondition_()");

    // $[RM12306] The maneuver shall transition to active when the P100 inspiratory trigger conditions (see Control Specification) have been met.
	// $[RM24009] \4\ The P100 trigger conditions...

	Real32 netFlow = RAirFlowSensor.getValue() + RO2FlowSensor.getValue() 
						- RExhFlowSensor.getDryValue()
						- RExhFlowSensor.getValue();

	elapsedTime_ += ::CYCLE_TIME_MS;

	// ignore trigger during first 200 ms of exhalation as too noisy to avoid false triggering
	Boolean isTriggered = 
		RExhalationPhase.getFlowTarget() <= 1.0 && elapsedTime_ > 200 && netFlow >= 0.0;

	return(isTriggered);

}



//=====================================================================
//
//  Private Methods...
//
//=====================================================================
