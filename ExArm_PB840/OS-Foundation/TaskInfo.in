#ifndef TaskInfo_IN
#define TaskInfo_IN
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TaskInfo - Used to store and retrieve Task data.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/TaskInfo.inv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================


//@ Usage-Classes
#include "Sys_Init.hh"
//@ End-Usage

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetName()
//
//@ Interface-Description
//  Returns a constant character pointer to the Task name.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline const char * 
TaskInfo::GetName(void) const
{
    return pName_;	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetState()
//
//@ Interface-Description
//  Returns the last entered state of the Task, not the actual VRTX32
//  state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline TaskState 
TaskInfo::GetState(void) const
{
    return state_;	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetIndex()
//
//@ Interface-Description
//  Returns the task index in TaskTableDef. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Int32 
TaskInfo::GetIndex(void) const
{
    return taskIdx_;	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBasePriority()
//
//@ Interface-Description
// Returns the priority given to this task at initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Int32 
TaskInfo::GetBasePriority(void) const
{
    return basePriority_;	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetStack()
//
//@ Interface-Description
// Returns a pointer to the beginning of the task's stack area.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Byte *
TaskInfo::GetStack(void) const
{
    return pStack_;	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetStackSize()
//
//@ Interface-Description
// Returns the size of the task's stack area.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline size_t
TaskInfo::GetStackSize(void) const
{
    return stackSize_;	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetFlags()
//
//@ Interface-Description
//  Returns the current flags.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Uint32 
TaskInfo::GetFlags(void) const 
{
    return flags_;	// $[TI1]
}

#ifdef	SIGMA_DEVELOPMENT
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDebugFlags()
//
//@ Interface-Description
//  Returns the current DebugFlags.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Uint32 
TaskInfo::GetDebugFlags(void) const 
{
    return debugFlags_;
}
#endif  // SIGMA_DEVELOPMENT

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEntryPtr()
//
//@ Interface-Description
//  Returns a constant function pointer that points to the function
//  associated with this task.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline EntryPtr 
TaskInfo::GetEntryPtr(void) const
{
    return entryPoint_;	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetQueue()
//
//@ Interface-Description
//  Takes no arguments and returns the ID of the queue used for Sys-Init
//  messages for this task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the value of the private constant queue_.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline IpcId
TaskInfo::GetQueue(void) const
{
    return queue_;	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetMonitorPeriod()
//
//@ Interface-Description
//  Takes no arguments and returns the Task-Monitor reporting
//  period for this task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the value of the private constant monitorPeriod_.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Uint32
TaskInfo::GetMonitorPeriod(void) const
{
    return monitorPeriod_;	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetMonitorQueue()
//
//@ Interface-Description
//  Takes no arguments and returns the queue ID for Task-Monitor messages
//  for this task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the value of the private constant monitorQueue_.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline IpcId
TaskInfo::GetMonitorQueue(void) const
{
    return monitorQueue_;      // $[TI1]
}

#ifdef	SIGMA_DEVELOPMENT
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDebugFlags()
//
//@ Interface-Description
//  Takes an Uint32  and sets it to the new debug flags of the object.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline void 
TaskInfo::setDebugFlags(const Uint32 debugFlags)
{
    debugFlags_ = debugFlags;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isDebugOn()
//
//@ Interface-Description
// Takes as input an Int32 that represents a bit mask with the bits
// of interest turned on.
//---------------------------------------------------------------------
//@ Implementation-Description
// The input mask is &-ed with the debug flags that were given in the
// Application initialization file for the calling task.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Boolean
TaskInfo::isDebugOn(const Uint32 mask) const
{
    if ( Sys_Init::IsTaskingOn() == FALSE )
    {
      return(FALSE);
    }
    else
    {
      return( (debugFlags_ & mask) != 0 );
    }
}
#endif // SIGMA_DEVELOPMENT 
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsFlagOn()
//
//@ Interface-Description
// Takes as input an Int32 that represents a bit mask with the bits
// of interest turned on.
//---------------------------------------------------------------------
//@ Implementation-Description
// The input mask is &-ed with the flags that were given in the
// Application initialization file for the calling task.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Boolean
TaskInfo::IsFlagOn(const Uint32 mask) const
{
    return( (flags_ & mask) != 0 );
    			// $[TI1] (T)
			// $[TI2] (F)
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTaskInfoByIndex_() [private]
//
//@ Interface-Description
// Takes a task index and returns a non-constant reference to
// the Task object associated with this index.
//---------------------------------------------------------------------
//@ Implementation-Description
// It simply returns the appropriate offset into the Task array
// after checking that the index (or ID) is valid.
//---------------------------------------------------------------------
//@ PreCondition
//    (IsValidId_(taskId) == TRUE)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline TaskInfo& 
TaskInfo::GetTaskInfoByIndex_(const Int32 idx)
{
    SAFE_CLASS_PRE_CONDITION(TaskInfo::IsValidId_(taskId));
    return PTaskInfoObjects_[idx];			// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTaskInfo()
//
//@ Interface-Description
// Takes a task index which is the internal ID (i.e. index which we use
// to reference the task in the task array and table)
//---------------------------------------------------------------------
//@ Implementation-Description
// It simply calls GetTaskInfoByIndex_ which returns the appropriate
// offset into the Task array
//---------------------------------------------------------------------
//@ PreCondition
//  TaskInfo::Initialized_ == TRUE
//  Valid taskId checked in GetTaskInfoByIndex_(taskId).
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline TaskInfo& 
TaskInfo::GetTaskInfo(const Int32 index)
{			
	CLASS_PRE_CONDITION(TaskInfo::Initialized_ == TRUE)
	return TaskInfo::GetTaskInfoByIndex_(index);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FindThreadIndex()
//
//@ Interface-Description
// Helper function. Takes an actual OS task ID and returns the corresponding 
// internal task index of this task (if found!)
//---------------------------------------------------------------------
//@ Implementation-Description
// If the thread ID is found in the task info array, the corresponding
// internal ID (index) is returned; otherwise, (-1) is returned to indicate error
//---------------------------------------------------------------------
//@ PreCondition
//  The caller checks for TaskInfo::Initialized_ == TRUE
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Int32
TaskInfo::FindThreadIndex(Uint32 thrdId)
{
    for (Uint32 idx=1; idx <= GetTaskCount(); idx++)
    {
        CLASS_ASSERTION( TaskInfo::IsValidIndex_(idx) );
        if(TaskInfo::PTaskInfoObjects_[idx].osTaskId_ == thrdId)
			return idx;
    }
    
    AUX_CLASS_ASSERTION_FAILURE(thrdId)
    
    return -1;	//we won't get here if the assertion is enabled..
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsValidIndex_() [private]
//
//@ Interface-Description
// Takes a task id and returns TRUE if it is valid and FALSE
// if it is not.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Valid task Ids are between 1 and MAX_NUM_TASKS(inclusive).  
//  These Ids are assigned in the Create() method.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Boolean TaskInfo::IsValidIndex_(const Int32 idx)
{
    return ( (idx > 0 && idx <= TaskInfo::MAX_NUM_TASKS) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTaskCount()
//
//@ Interface-Description
// Returns the current task count.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns value of private data TaskCount_.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Uint32
TaskInfo::GetTaskCount(void)
{
    return TaskCount_;	// $[TI1]
}

#endif // TaskInfo_IN 


