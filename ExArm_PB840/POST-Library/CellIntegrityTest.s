;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================


;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: CellIntegrityTest.s - POST DRAM Cell Integrity Test
;
;---------------------------------------------------------------------
;@ Interface-Description
;
;  Free Functions: 
;     CellIntegrityTest  - performs the Fast Exchange test on the
;                          specified range of DRAM.
;
;---------------------------------------------------------------------
;@ Rationale
;    This module provides a common memory data pattern test for both
;    Kernel and POST.
;---------------------------------------------------------------------
;@ Implementation-Description
;    The cell integrity test is designed to use no DRAM for local
;    variables since the Kernel uses this function to test DRAM prior
;    to using it.  Local variables including the calling parameters
;    use registers instead of DRAM.
;---------------------------------------------------------------------
;@ Fault-Handling
;    A failure of any test in this file is considered a Major Fault and
;    causes an immediate suspension of testing.  The processor is put
;    into a state from which it cannot exit without a power cycle or reset.
;---------------------------------------------------------------------
;  Restrictions
;    The 68040 data cache must be OFF for all memory tests
;    to insure that all memory accesses result in DRAM accesses.  The 
;    instruction cache is turned ON to increase performance for 
;    this test and implicitly testi the instruction cache.
;---------------------------------------------------------------------
;@ Invariants
;    None
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
; @(#) $Header:   /840/Baseline/POST-Library/vcssrc/CellIntegrityTest.s_v   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
;
;@ Modification-Log
;
;  Revision 003   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
;    Project:   Sigma   (R8027)
;    Description:
;       Added testable item annotations.
;
;  Revision: 002    By: Gary Cederquist  Date: 16-MAY-1997  DR Number: 2121
;    Project: Sigma (R8027)
;    Description:
;       Removed assembler options specified in source in favor of command
;       line options.
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================

                XDEF        CellIntegrityTest
                SECTION     code,,C

               INCLUDE  kernel.inc

;--------- Data for the Cell Integrity Test  -------------------------
                ALIGN   16
Pattern:        DCB.L   8,$ffffffff             ;initial memory fill pattern
regList:        REG     d2/d3/d4/d5/d6/a1/a2/a5 ;registers used in movem


;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: CellIntegrityTest - Performs a Memory Cell Integrity Test
;
;@ Interface-Description
;
; This tests the ability of each bit in each longword to hold a one and a
; a zero.  This is the most time consuming part of the memory test, so an
; attempt has been made to optimize it for speed.  The test first writes
; 1's to memory then adds a 1 to each long word which leaves a zero in
; in memory (thereby clearing memory).  $[11012] $[11039] $[11074]
;
; Implementation note: This module is designed to test 256 bytes each time
;                      through the inner loop.  Therefore; the loop count
;                      for 16 Mbytes fits in 16 bits
;                       (16384*1024/256 = 65,566). 65536 is loop count + 1
;                      If the number of bytes tested each time through the
;                      loop is reduced or the amount of memory tested is 
;                      increased; be careful that the iteration loop count
;                      does not overflow 16 bits.
;
;                      Remember; the dbne instruction exits the loop when
;                                the or.l's have a non-zero result, or the
;                                loop count transitions to -1 ($FFFF).
;                                Therefore an initial value of $FFFF will
;                                do 65536 loops, or 65536*256 = 16 Mbytes.
;
;    Inputs:  d0.l - number of bytes to test
;                 (bytes tested = 256 * floor(d0.l/256)), max = 16 Mbytes
;             a0.l - test starting address (must be a multiple of 256)
;             a6.l - points to the subroutine return location 
;             
;
;    Outputs: d7.l - PASSED = test passed, else MAJOR = test failed
;
;---------------------------------------------------------------------
;@ Implementation-Description
;
;
;  REGISTER USAGE
;
;  Register Modified: d0,d1,d2,d3,d4,d5,d6,d7,a0,a1,a2,a4,a5,a6
;
;---------------------------------------------------------------------
;@ PreCondition
;    None
;---------------------------------------------------------------------
;@ PostCondition
;    None
;
;@ End-Free-Function
;=====================================================================
;>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Entry Point for Internal Use and External Use
; void CellIntegrityTest() {
CellIntegrityTest:
                                             UNIT_TEST_POINT POSTUT_9_1
                move.l  d0,d1

                andi.l  #$000000ff,d1    ;if not a multiple of 256, fail
                bne     testFailed
                asr.l   #8,d0            ;loop does 256 bytes each iteration
                tst.l   d0               ;if(testing less than 256 bytes) fail
                beq     testFailed
                                         ; // $[TI1.1] failed
                                         ; // $[TI1.2] passed
                subq.l  #1,d0            ;loop_count = requested_loops - 1
                move.l  d0,d1
                andi.l  #$ffff0000,d1    ;if(loop_count > 16 bits) fail
                bne     testFailed
                                         ; // $[TI2.1] failed
                                         ; // $[TI2.2] passed
                movem.l Pattern,regList  ;fill the registers with a pattern
                                             UNIT_TEST_POINT POSTUT_9_4
                move.l  a0,a4            ;a4 used for zero test portion

CellLoop:       STROBE_WATCHDOG          ;keep the watchdog from timing out
                movem.l regList,(a0)     ;fill 256 bytes with the pattern
                movem.l regList,(32,a0)
                movem.l regList,(64,a0)
                movem.l regList,(96,a0)
                movem.l regList,(128,a0)
                movem.l regList,(160,a0)
                movem.l regList,(192,a0)
                movem.l regList,(224,a0)

                                             UNIT_TEST_POINT POSTUT_9_2
                REPT    64
                addq.l  #1,(a0)+         ;  add one to the 0xffffffff that
                bne     testFailed       ;  was set, result should be 0
                                         ; // $[TI3.1] failed
                                         ; // $[TI3.2] passed
                ENDR

                                             UNIT_TEST_POINT POSTUT_9_3

                move.l  (a4)+,d7         ;get the first long word
                REPT    63               ;  verify that the 0's written
                or.l    (a4)+,d7         ;  above are still in memory
                ENDR

                dbne    D0,CellLoop      ;test or.l's and loop count
                bne.s   testFailed ;if the or.l's != 0, then failed
                                         ; // $[TI4.1] failed
                                         ; // $[TI4.2] passed

;**************************************************************************
;************ Test completed - return to POST calling routine  ************
testPassed:     moveq.l   #PASSED,d7
; // $[TI5.1]
;<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Exit Point
                jmp     (a6)

;**************************************************************************
;************ Exit point for Tests that failed  ***************************
testFailed:     moveq.l #MAJOR,D7
; // $[TI5.2]
;<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Exit Point
                jmp     (A6)

; }
                END
