//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1998, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename: PbTimer.c - VRTX Timer Device Driver for PB FPGA Timers
//
//---------------------------------------------------------------------
//@ Interface-Description
//
//  This file contains the VRTX device driver for the Puritan-Bennett
//  developed FPGA-based system timers. These timers were developed
//  as part of the Cost Reduction program to replace the Intel 8254
//  timers. This device driver implements all the functions required 
//  of timer device drivers by the VRTX Operation System. It is based
//  on the timer device driver template provided by Microtec Research.
//
//---------------------------------------------------------------------
//@ Rationale
//  This module contains the VRTX device driver for the PB FPGA timers.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This module provides all the functions required by the VRTX OS
//  to implement a timer device driver.
//
//  The names of functions and typedefs are unchanged from the names 
//  provided by the Microtec template for this device driver. This is
//  done purposely to maintain commonality with the other VRTX device
//  drivers.
//---------------------------------------------------------------------
//@ Fault Handling
//  Faults are handled by the VRTX and 840 exception handlers.
//---------------------------------------------------------------------
//@ Restrictions
//    none
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Sigma-Bsp/vcssrc/PbTimer.c_v   25.0.4.0   19 Nov 2013 14:29:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist  Date: 14-Dec-1998  DR Number: 5311
//    Project: 840 Cost Reduction
//    Description:
//       Initial version.
//
//=====================================================================


//=====================================================================
//
//  The following is Microtec's original instructions for the timer
//  device driver template. It is left here for documentation purposes.
//
//*****************************************************************************
// TIFORM.c                                                             
//                                                                           
//    This file contains all of the device-dependent, board-independent      
//    functions to control a hardware timer of name TIFORM.
//
//    Instructions for Creating a device driver from this template:
//
//    [ Refer to the timer abstract model document, tmodel.txt
//      for derrivation of formulas. ]
//
//    Select the hardware model of the timer
//
//       * Compile tiform.c without modification.  There should be no errors.
//         The define M68K was used to ensure compilation.  This may cause
//         an error since it is defined as a part of some makefiles.  If
//         this is the case, comment out the define.
//       * Copy tiform.h to a file named after the device. i.e. TIFORM.h
//       * Copy tiform.c to a file named after the device. i.e. TIFORM.c
//       * Edit TIFORM.h
//       * The string TIFORM should be replaced by the name of the hardware
//         timer that this device driver is being written for.
//       * save TIFORM.h
//       * Edit TIFORM.c
//       * The string TIFORM should be replaced by the name of the hardware
//         timer that this device driver is being written for.
//       * If your device has a prescaler uncomment the line that represents
//         the type of prescaler.  Make certain to define only one type of
//         prescaler.  Default: MODULO_N_PRESCALER.  If your device supports
//         fixed values only, define FIXED_PRESCALER and supply the appropiate
//         code in set_ns_per_count and get_ns_per_count functions.  
//       * Determine the type of counter that represents your timer.  Uncomment
//         the line that represents your counter.  Make certain to define only
//         one type of counter.  Default: MODULO_N_COUNTER
//       * Determine the size of the counter.   Uncomment the TIMER_MAX
//         value that corresponds to your timer.  If your maximum value is
//         not supplied, then create a define.  Default: 16 bits
//       * Compile the file.  There should be no errors.
//       * You now have chosen the specific model of your timer.
//       * Search the file for any occurance of TIFORM (to ensure all functions
//         have been modified)
//       * Compile the file.  There should be no errors.
//
//    Supply the hardware specific timer values
//
//       * define the following six macros where the input/output value
//         for each of these macros/functions is an unsigned long.
//
//          TIFORM_prescaler_set_preload(n)
//                sets the current preload value of the prescaler
//          TIFORM_prescaler_get_preload() 
//                returns the current preload value of the prescaler
//          TIFORM_counter_set_preload(n)  
//                sets the current preload value of the counter
//          TIFORM_counter_get_preload()   
//                returns the current preload value of the counter
//          TIFORM_counter_set_current(n)
//                sets the current counter value
//       * Compile the file.
//
//    Supply the initialization code
//
//       * in the TIFORM_init function, supply and additional code required
//         by the device to prepare the device to operate.  If specific
//         timer values are required, standard defaults should be:
//
//          prescaler: ns_per_count = 1000   (1us)
//          counter:   set_preload  = 10     (10us)
//
//    Supply the interrupt functions
//
//       * The interrupt functions are useful, but not absolutely necessary.
//         If a "shadow" interrupt state is maintained, this will be
//         sufficient.
//
//       * In the TIFORM_disable routine, at the location indicated by the
//         instruction, disable the timer interrupts.
//
//       * In the TIFORM_enable routine, at the location indicated by the
//         instruction, enable the timer interrupts.
//
//       * In the TIFORM_interrupt_status routine, follow the instructions
//         in the function to complete the routine.
//
//
//    Supply the test function
//
//       * if any device specific interrupt code is necessary, supply that code
//         int the TIFORM_test function.
//                                                                           
// History:
//
//    Date        Name              Description
//    ---------------------------------------------------------------------- 
//    08-07-92    Donn Rochette     First Version, mo332pit.c
//    09-09-92    Roger Cole        Reformatted file, renamed to tiform
//    09-09-92    Roger Cole        Converted into a template file for all
//                                  timers
//    11-24-92    Roger Cole        Added support for various timer models
//    03-10-93    Roger Cole        Redefined names of functions to clarify
//
//*****************************************************************************
//
//======== END MICROTEC INSTRUCTIONS =========================================

#include "ansiprot.h"   // ANSI prototypes 
#include "logio.h"      // int enable/disable 
#include "timer.h"      // generic timer specific values 
#include "vbsp.h"       // VBSP return codes and typedefs 
#include "PbTimer.h"    // specific definitions and prototypes 

// ** types of counters

#define DOWN_COUNTER

// ** types of prescalers

#define NO_PRESCALER 

// ** timer maximum values/size of timer

#define TIMER_MAX  0x3FFFFF    // 22 bits 


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_install
//
//@ Interface-Description
//  >Von
//   This function installs the data structures for the hardware timer
//  
//   Inputs:
//             pCfg             pointer to timer device configuration.
//             pDev             pointer to timer device descriptor.
//   Outputs:
//             none
//  
//   Return codes:
//             VBSP_SUCCESS     The call succeeded
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//
//@ End-Free-Function
//=====================================================================

vbsp_return_t 
PbTimer_install(PbTimer_config_t *pCfg, timer_dev_desc_t *pDev)
{
  // $[TI1]

  // set up the config table 
  // linked to the additional RAM space for this device 
  pDev->config = pCfg->base_config;  

  // install the register map 
  pDev->map = (void *)&pCfg->regs;

  // initialize the external board function fops entry 
  pDev->externio = pCfg->externio;

  // initialize the specific variables 
  *(PbTimer_specific_t *)pDev->config.specific = pCfg->specific_cfg;

  // set up the fops table 
  // These functions MUST be defined! 
  pDev->fops.init         = PbTimer_init;
  pDev->fops.test         = PbTimer_test;
  pDev->fops.get_count    = PbTimer_get_ns_per_count;
  pDev->fops.set_count    = PbTimer_set_ns_per_count;
  pDev->fops.get_preload  = PbTimer_get_rollover; // old to new name translation
  pDev->fops.set_preload  = PbTimer_set_rollover; // old to new name translation
  pDev->fops.get_rollover = PbTimer_get_max;      // old to new name translation
  pDev->fops.read_timer   = PbTimer_read_timer;
  pDev->fops.int_enable   = PbTimer_enable;
  pDev->fops.int_disable  = PbTimer_disable;
  pDev->fops.int_info_get = PbTimer_interrupt_status;

  return(VBSP_SUCCESS);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_init
//
//@ Interface-Description
//  >Von
//   This routine initializes the timer for a preloadic          
//   interrupt.  Interrupts are enabled by this routine.
//  
//   Inputs:                                                                   
//             pDev           pointer to device descriptor
//   Outputs:                                                                  
//             none                                                            
//                                                                             
//   Return codes:                                                             
//             VBSP_SUCCESS             The call succeeded                     
//             VBSP_INVALID_PERIOD      bad timer preload                       
//             VBSP_INVALID_PRIORITY    bad interrupt priority value           
//             VBSP_INVALID_VECTOR      bad interrupt vector number            
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Free-Function
//=====================================================================

vbsp_return_t 
PbTimer_init(timer_dev_desc_t *pDev)
{
  unsigned long cnt;

  PbTimer_reg_map_t *pRegs = (PbTimer_reg_map_t *)pDev->map;

  // check preload value 
  if (pDev->config.preload > TIMER_MAX)
  {													// $[TI1.1]
	cnt = TIMER_MAX + 1;
  }
  else
  {													// $[TI1.2]
	cnt = pDev->config.preload;
  }

  *pRegs->pTimerPreloadReg = cnt;
  *pRegs->pTimerControlReg = PBTIMER_EN;

  return(VBSP_SUCCESS);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_disable
//
//@ Interface-Description
//  >Von
//  Disable interrupts from the timer.
//
//  Inputs:                                                                   
//           pDev           pointer to device descriptor
//
//  Outputs:                                                                  
//           none.                                                           
//                                                                           
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function simply sets the shadow register to interrupt disabled
//  since there is no requirement to disable timer interrupts. This 
//  procedure is per Microtec instructions.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Free-Function
//=====================================================================

vbsp_return_t 
PbTimer_disable(timer_dev_desc_t *pDev)
{
  // $[TI1]
  BOARD_SPEC_INT_DISABLE(pDev, 0); 
  ((PbTimer_specific_t*)(pDev->config.specific))->shadowIntReg =
	  LOGIO_INT_DISABLED;

  return(VBSP_SUCCESS);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_enable
//
//@ Interface-Description
//  >Von
//  Enables interrupts from the timer.
//
//  Inputs:
//            pDev           pointer to device descriptor
//
//  Outputs:
//            none.
//
//  Return codes:
//            VBSP_SUCCESS    The call succeeded
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function simply sets the shadow register to interrupt enabled
//  since there is no requirement to disable timer interrupts. This 
//  procedure is per Microtec instructions.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Free-Function
//=====================================================================

vbsp_return_t 
PbTimer_enable(timer_dev_desc_t *pDev)
{
  // $[TI1]
  BOARD_SPEC_INT_ENABLE(pDev, 0); 
  ((PbTimer_specific_t*)(pDev->config.specific))->shadowIntReg
		= LOGIO_INT_ENABLED;

  return(VBSP_SUCCESS);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_interrupt_status
//
//@ Interface-Description
//  >Von
//   Get interrupt status, returns either interrupts enabled or interrupts
//   disabled.
//
//   Inputs:
//             pDev           pointer to device descriptor
//   Outputs:
//             *pStatus
//                  LOGIO_INT_ENABLED    - if interrupts are enabled
//                  LOGIO_INT_DISABLED   - if interrupts are disabled
//   Return codes:
//             VBSP_SUCCESS    The call succeeded
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  The timer cannot return the state of its interrupts so we 
//  return the interrupt state contained in the shadow register.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Free-Function
//=====================================================================

vbsp_return_t 
PbTimer_interrupt_status(timer_dev_desc_t *pDev, int *pStatus)
{
  if ( ((PbTimer_specific_t*)(pDev->config.specific))->shadowIntReg
	  == LOGIO_INT_ENABLED )
  { 												// $[TI1.1]
	*pStatus = (unsigned char)LOGIO_INT_ENABLED;
  }
  else
  { 												// $[TI1.2]
	*pStatus = (unsigned char)LOGIO_INT_DISABLED;
  }

  return( VBSP_SUCCESS );
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_set_ns_per_count
//
//@ Interface-Description
//  >Von
//   Set the timer count value
//
//   The clock period is fixed for the PB timer. This function sets 
//   the *pNsPerCount parameter to the base clock period and returns.
//                                                                           
//   Inputs:                                                                   
//             pDev           pointer to device descriptor
//             *pNsPerCount   return parameter
//   Outputs:                                                                  
//             none.
//   Return codes:                                                             
//             VBSP_SUCCESS
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//
//@ End-Free-Function
//=====================================================================
vbsp_return_t 
PbTimer_set_ns_per_count(timer_dev_desc_t *pDev, unsigned long *pNsPerCount)
{
  // $[TI1]
  *pNsPerCount = pDev->config.clock_period;
  return(VBSP_SUCCESS);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_get_ns_per_count
//
//@ Interface-Description
//  >Von
//   Get the timer count.
//   This function returns the base count value in nanoseconds.
//
//   Inputs:
//             pDev           pointer to device descriptor
//             *pNsPerCount   configuration parameters
//   Outputs:
//             none.
//   Return codes:
//             VBSP_SUCCESS    The call succeeded
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Free-Function
//=====================================================================

vbsp_return_t 
PbTimer_get_ns_per_count(timer_dev_desc_t *pDev, unsigned long *pNsPerCount)
{
  // $[TI1]
  *pNsPerCount = pDev->config.clock_period;
  return(VBSP_SUCCESS);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_get_rollover
//
//@ Interface-Description
//  >Von
//   Gets the timer rollover value.
//
//   Inputs:
//             pDev           pointer to device descriptor
//             *pRollover     timer rollover value return parameter
//   Outputs:
//             none.
//   Return codes:
//             VBSP_SUCCESS   The call succeeded
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//
//@ End-Free-Function
//=====================================================================

vbsp_return_t 
PbTimer_get_rollover(timer_dev_desc_t *pDev, unsigned long *pRollover)
{
  // $[TI1]
  PbTimer_reg_map_t *pRegs = (PbTimer_reg_map_t *)pDev->map;
  *pRollover = *pRegs->pTimerPreloadReg;

  return(VBSP_SUCCESS);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_set_rollover
//
//@ Interface-Description
//  >Von
//   Sets the timer rollover value.
//
//   Inputs:
//             pDev           pointer to device descriptor
//             *pRollover     timer rollover configuration parameter
//   Outputs:
//             none.
//   Return codes:
//             VBSP_SUCCESS   The call succeeded
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Free-Function
//=====================================================================

vbsp_return_t 
PbTimer_set_rollover(timer_dev_desc_t *pDev, unsigned long *pRollover)
{
  PbTimer_reg_map_t *pRegs = (PbTimer_reg_map_t *)pDev->map;

  if (*pRollover > TIMER_MAX)
  {													// $[TI1.1]
	*pRollover = TIMER_MAX + 1;
  }													// $[TI1.2]

  pDev->config.preload = *pRollover;
  *pRegs->pTimerPreloadReg = *pRollover;

  return(VBSP_SUCCESS);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_get_max
//
//@ Interface-Description
//  >Von
//    Get the timer maximum value.
//
//    Inputs:
//              pDev           pointer to device descriptor 
//              *pMax          return rollover value
//    Outputs:
//              none.
//    Return codes:
//              VBSP_SUCCESS   The call succeeded
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Free-Function
//=====================================================================

vbsp_return_t 
PbTimer_get_max(timer_dev_desc_t *pDev, unsigned long *pMax)
{
  // $[TI1]
  *pMax = TIMER_MAX;
  return(VBSP_SUCCESS);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_read_timer
//
//@ Interface-Description
//  >Von
//   Read the current count value
//
//   Inputs:
//             pDev           pointer to device descriptor
//             *pCount        count value                        
//   Outputs:
//             none.
//   Return codes:
//             VBSP_SUCCESS   The call succeeded
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//
//@ End-Free-Function
//=====================================================================

vbsp_return_t 
PbTimer_read_timer(timer_dev_desc_t *pDev, unsigned long *pCount)
{
  // $[TI1]
  PbTimer_reg_map_t *pRegs = (PbTimer_reg_map_t *)pDev->map;
  *pCount = *pRegs->pTimerPreloadReg - *pRegs->pTimerCountReg;
  return(VBSP_SUCCESS);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: PbTimer_test
//
//@ Interface-Description
//  >Von
//   Interrupt service routine for the timer.
// 
//   This function returns true since the interrupt is tied directly to
//   the interrupting timer device.
// 
//   Inputs:
//             pEntry         pointer to interrupt entry
//   Outputs:
//             none.
//   Return codes: 
//             0 - no interrupt occurred (not applicable for PbTimer)
//             1 - interrupt occurred
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Free-Function
//=====================================================================

int 
PbTimer_test(logio_interrupt_entry_t *pEntry)  
{
  // $[TI1]
  timer_dev_desc_t *pDev = (timer_dev_desc_t *)pEntry->id->data;

  // INTERRUPT_START(pDev,0);  not necessary -- comment left for documentation
  // CLEAR INTERRUPTS HERE IF NECESSARY 

  INTERRUPT_END(pDev,0);
  return( 1 );
}
