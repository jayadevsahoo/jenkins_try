#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LaptopMessageHandler -  Low-level utility routines for formatting
// the outgoing messages.
//---------------------------------------------------------------------
//@ Interface-Description
// A class wrapper for static methods and string buffers that are used
// to do laptop message manipulation.  The interface is quite simple,
// SetMessageNo(), SetCommand(), SetSwRevisionCommand(), 
// SetSerialNumberCommand(), SetSwOptionsCommand(),
// SetRunTimeCommand(), SetDataCommand(), and SetLogCommand() shall construct
// the message to meet the LaptopEvent's format requirement.  GetBufferIndex()
// and GetBuffer() are the common utilities used to build up the messages.
//---------------------------------------------------------------------
//@ Rationale
// This code contains useful routines for commonly used latop message
// formatting requirements.
//---------------------------------------------------------------------
//@ Implementation-Description
// See individual methods for particular implementation descriptions.
//---------------------------------------------------------------------
//@ Fault-Handling
// Usual use of asserts to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LaptopMessageHandler.ccv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: gdc    Date: 26-May-2007   SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 006  By: gdc    Date: 22-Jan-2001   DR Number: 5493
//  Project:  GuiComms
//  Description:
//      Implemented SMCP on additional serial ports.
//
//  Revision: 005  By: dosman    Date: 26-Feb-1999   DR Number: 5322
//  Project:  ATC
//  Description:
//	Implemented changes for ATC.
//	Removed underscore from name of public method.
//	Added method to send software options to remote PC.
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By: gdc      Date: 24-Nov-1997  DR Number: 2605
//    Project:  Sigma (R8027)
//    Description:
//     Changed interface to GUI-Serial-Interface as part of its 
//     restructuring.
//
//  Revision: 002  By:  hhd	   Date:  18-SEP-97    DR Number: 2506
//       Project:  Sigma (R8027)
//       Description:
//			   Incremented bufferIndex by 1 when endstring is added to the log data buffer.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "LaptopMessageHandler.hh"

//@ Usage-Classes
#include "LaptopEventRegistrar.hh"
#include "MsgQueue.hh"
#include "SerialInterface.hh"
#include <stdio.h>
#include <string.h>
//@ End-Usage

//@ Code...

//
// Initializations of static member data.
//
char LaptopMessageHandler::OutgoingBuffer_[LaptopMessageHandler::MAX_BUFF_SIZE];
Uint16 LaptopMessageHandler::BufferIndex_=0;
Uint8 LaptopMessageHandler::MessageNo_=0;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ClearBuffer [public, static]
//
//@ Interface-Description
//	Clear the outgoing message buffer.  This method shall be used before we
//  construct any laptop message to guarantee a fresh clean buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the OutgoingBuffer.  Set the buffer index to 0.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::ClearBuffer(void)
{
	CALL_TRACE("LaptopMessageHandler::ClearBuffer(void)");

	for (Uint16 i = 0; i < LaptopMessageHandler::MAX_STRING_SIZE; i++)
	{												// $[TI1]
		OutgoingBuffer_[i] = '\0';
	}												// $[TI2]

	BufferIndex_ = 0;
													
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetMessageNo [public, static]
//
//@ Interface-Description
//	Build the message number part of the laptop message.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Increment the "MessageNo_", set the outgoing message buffer with
//  "MessageNo_", and update the "BufferIndex_".
//	Message string is NULL-terminated.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::SetMessageNo(void)
{
	CALL_TRACE("LaptopMessageHandler::SetMessageNo(void)");

	MessageNo_++;

	OutgoingBuffer_[BufferIndex_=LaptopEventId::OUTGOING_MSG_NUMBER_IDX] = MessageNo_;

	BufferIndex_++;
	OutgoingBuffer_[BufferIndex_] = '\0';
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetCommand [public, static]
//
//@ Interface-Description
//	Build the command part of the laptop message with "commandId" and
//  "commandValue".
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the outgoing message buffer with "commandId", if "commandValie" is 
//  available, then set the "commandValie" in the outgoing message buffer.
//	Message string is NULL-terminated.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::SetCommand(Int commandId, Int commandValue)
{
	CALL_TRACE("LaptopMessageHandler::SetCommand(Int commandId, Int commandValue)");

	// Set the command ID
	OutgoingBuffer_[BufferIndex_=LaptopEventId::OUTGOING_COMMAND_ID_IDX] = commandId;

	if (commandValue != -1)
	{												// $[TI1]
		// Set the command value
		OutgoingBuffer_[BufferIndex_=LaptopEventId::OUTGOING_COMMAND_VALUE_IDX] = commandValue;
	}												// $[TI2]

	BufferIndex_++;
	OutgoingBuffer_[BufferIndex_] = '\0';
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetSwRevisionCommand [public, static]
//
//@ Interface-Description
//	Build the software revision message with two input strings: "guiRevision"
//  and "bdRevision".
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the outgoing message buffer with LaptopEventId::SOFTWARE_VERSION_COMMAND.
//  Format the software revision string.  Assign the software revision
//  string to the outgoing message buffer.
//	Message string is NULL-terminated.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::SetSwRevisionCommand(const char *guiRevision, const char *bdRevision)
{
	CALL_TRACE("LaptopMessageHandler::SetSwRevisionCommand(char *guiRevision, char *bdRevision)");

	char buffer[LaptopMessageHandler::MAX_STRING_SIZE];

	// Build the command ID for outgoingBuffer.
	OutgoingBuffer_[LaptopEventId::OUTGOING_COMMAND_ID_IDX]	=
						LaptopEventId::SOFTWARE_VERSION_COMMAND;

	// Build the software revision string.
	_snprintf(buffer, sizeof(buffer)-1, "GUI:%s, BD:%s", guiRevision, bdRevision);
	buffer[sizeof(buffer)-1] = '\0';
	// Copy the software revision string to OutgoingBuffer_.
	memcpy((char*)&OutgoingBuffer_[LaptopEventId::OUTGOING_SW_REVISION_VALUE_IDX],
				(char*)buffer, strlen(buffer));

	BufferIndex_ = LaptopEventId::OUTGOING_SW_REVISION_VALUE_IDX + strlen(buffer) + 1;
	OutgoingBuffer_[BufferIndex_] = '\0';

                                                           // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetSerialNumberCommand [public, static]
//
//@ Interface-Description
//	Build the configuration message with input strings: "guiKernel"
//  "bdKernel", "guiSN", "bdSN", and "compressorSN".
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the outgoing message buffer with LaptopEventId::UNIT_SERIAL_NUMBER_COMMAND.
//  Format the serial number revision string with the passed arguments.
//  Assign the serial number revision string to the outgoing message buffer.
//	Message string is NULL-terminated.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::SetSerialNumberCommand(const char *guiKernel,
										   const char *bdKernel,
										   const char *guiSN,
										   const char *bdSN,
										   const char *compressorSN)
{
	CALL_TRACE("LaptopMessageHandler::SetSerialNumberCommand(char *, char *)");

	char buffer[LaptopMessageHandler::MAX_STRING_SIZE];

	// Build the command ID for outgoingBuffer.
	OutgoingBuffer_[LaptopEventId::OUTGOING_COMMAND_ID_IDX]	=
						LaptopEventId::UNIT_SERIAL_NUMBER_COMMAND;

	// Build the software revision value to outgoingBuffer.
	_snprintf(buffer, sizeof(buffer)-1, "GUI Post:%s, BD Post:%s, GUI SN:%s, BD SN:%s, CompressorSN:%s",
			guiKernel, bdKernel, guiSN, bdSN, compressorSN);

	buffer[sizeof(buffer)-1] = '\0';

	// Copy the serial number information string to OutgoingBuffer_.
	memcpy((char *)&OutgoingBuffer_[LaptopEventId::OUTGOING_UNIT_SERIAL_NUMBER_VALUE_IDX],
				(char *)buffer, strlen(buffer));


	BufferIndex_ = LaptopEventId::OUTGOING_UNIT_SERIAL_NUMBER_VALUE_IDX + strlen(buffer) + 1;
	OutgoingBuffer_[BufferIndex_] = '\0';

                                                           // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetSwOptionsCommand [public, static]
//
//@ Interface-Description
//	Build the ventilator software options configuration message 
//	with input string: "ventilatorSoftwareOptionsString".
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the outgoing message buffer with 
//	LaptopEventId::SOFTWARE_OPTIONS_COMMAND.
//  Format the software options string with the passed arguments.
//  Assign the software options string to the outgoing message buffer.
//	Message string is NULL-terminated.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::SetSwOptionsCommand(const char *ventilatorSoftwareOptionsString)
{
	CALL_TRACE("LaptopMessageHandler::SetSwOptionsCommand(char *)");

	char buffer[LaptopMessageHandler::MAX_STRING_SIZE];

	// Build the command ID for outgoingBuffer.
	OutgoingBuffer_[LaptopEventId::OUTGOING_COMMAND_ID_IDX]	=
						LaptopEventId::SOFTWARE_OPTIONS_COMMAND;

	// Build the software options value to outgoingBuffer.
	_snprintf(buffer, sizeof(buffer)-1, "Software Options:%s", ventilatorSoftwareOptionsString);
	buffer[sizeof(buffer)-1] = '\0';

	// Copy the serial number information string to OutgoingBuffer_.
	memcpy((char *)&OutgoingBuffer_[LaptopEventId::OUTGOING_SOFTWARE_OPTIONS_VALUE_IDX],
				(char *)buffer, strlen(buffer));

	BufferIndex_ = LaptopEventId::OUTGOING_SOFTWARE_OPTIONS_VALUE_IDX + strlen(buffer) + 1;
	OutgoingBuffer_[BufferIndex_] = '\0';

  	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetRunTimeCommand [public, static]
//
//@ Interface-Description
//	Build the configuration message with input values: "compressorValue"
//  and "systemValue".
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the outgoing message buffer with LaptopEventId::UNIT_RUNTIME_INFO_COMMAND.
//  Format the runtime information string with the passed arguments.
//  Assign the runtime information string to the outgoing message buffer.
//	Message string is NULL-terminated.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::SetRunTimeCommand(Real32 compressorValue,
									Real32 systemValue)
{
	CALL_TRACE("LaptopMessageHandler::SetRunTimeCommand(Real32 compressorValue,	Real32 systemValue)");

	// Build the command ID for outgoingBuffer.
	OutgoingBuffer_[LaptopEventId::OUTGOING_COMMAND_ID_IDX]	=
						LaptopEventId::UNIT_RUNTIME_INFO_COMMAND;

	// Copy the compressor runtime information to OutgoingBuffer_.
	memcpy((char *)&OutgoingBuffer_[LaptopEventId::OUTGOING_COMPRESSOR_RUN_TIME_VALUE_IDX],
				(char *) &compressorValue, sizeof(Real32));

	// Copy the system runtime information to OutgoingBuffer_.
	memcpy((char *) &OutgoingBuffer_[LaptopEventId::OUTGOING_SYSTEM_RUN_TIME_VALUE_IDX],
				(char *) &systemValue, sizeof(Real32));


	BufferIndex_ = LaptopEventId::OUTGOING_SYSTEM_RUN_TIME_VALUE_IDX + sizeof(Real32);

	OutgoingBuffer_[BufferIndex_+1] = '\0';

                                                           // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetDataCommand [public, static]
//
//@ Interface-Description
//	Build the data message with input values: "dataIndex" and "dataValue".
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the outgoing message buffer with LaptopEventId::DATA_COMMAND.
//  Format the data information string with the passed arguments.
//  Assign the data information string to the outgoing message buffer.
//	Message string is NULL-terminated.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::SetDataCommand(Int dataIndex, Real32 dataValue)
{
	CALL_TRACE("LaptopMessageHandler::SetDataCommand(Int dataIndex, Real32 dataValue)");

	// Build the command ID for outgoingBuffer.
	OutgoingBuffer_[LaptopEventId::OUTGOING_COMMAND_ID_IDX]	= LaptopEventId::DATA_COMMAND;

	// Copy the dataIndex information to OutgoingBuffer_.
	memcpy((char *) &OutgoingBuffer_[LaptopEventId::OUTGOING_DATA_POINT_IDX],
                                (char*) &dataIndex, sizeof(Int));

	// Copy the dataValue information to OutgoingBuffer_.
	memcpy((char *) &OutgoingBuffer_[LaptopEventId::OUTGOING_DATA_VALUE_IDX],
                                (char *) &dataValue, sizeof(Real32));

	BufferIndex_ = LaptopEventId::OUTGOING_DATA_VALUE_IDX + sizeof(Real32);
	OutgoingBuffer_[BufferIndex_] = '\0';

                                                           // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetLogCommand [public, static]
//
//@ Interface-Description
//	Build the log message with input values: "commandId" and "logMsg".
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the outgoing message buffer with the passed argument "commandId".
//  Format the log string with the passed arguments "logMsg".
//	Message string is NULL-terminated.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::SetLogCommand(Int commandId, char *logMsg)
{
	CALL_TRACE("LaptopMessageHandler::SetLogCommand(Int commandId, char *logMsg)");

	OutgoingBuffer_[LaptopEventId::OUTGOING_COMMAND_ID_IDX] 	= commandId;

	if (logMsg != NULL)
	{                                                           // $[TI1]
		strcpy((char *) &OutgoingBuffer_[LaptopEventId::OUTGOING_LOG_DATA_IDX], logMsg);
		BufferIndex_ += strlen(logMsg);
	}
	else
	{                                                           // $[TI2]
		BufferIndex_ = LaptopEventId::OUTGOING_LOG_DATA_IDX;
	}

	OutgoingBuffer_[BufferIndex_++] = '\0';
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBufferIndex [public, static]
//
//@ Interface-Description
//  Returns the BufferIndex_ member.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the value of BufferIndex_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int16
LaptopMessageHandler::GetBufferIndex(void)
{
	CALL_TRACE("LaptopMessageHandler::GetBufferIndex(void)");

	return(BufferIndex_);
	                                                           // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBuffer [public, static]
//
//@ Interface-Description
//  Returns a pointer to the OutgoingBuffer_ member.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the address of OutgoingBuffer_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

char *
LaptopMessageHandler::GetBuffer(void)
{
	CALL_TRACE("LaptopMessageHandler::GetBuffer(void)");

	return((char *) &OutgoingBuffer_);
	                                                           // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SerialOutputEventHappened [public, static]
//
//@ Interface-Description
//  This method is called to inform GUI Serial-IO subsystem of a new message
//  is coming with "serialCommandCodes".  The actual message "msgBuffer" and
//  the size of the message "length" to the GUI Serial-IO subsystem shall be
//  post to its internal buffer for later process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Request to download the "msgBuffer" to GUI-Serial-Interface subsystem's
//  internal buffer.  Followed by posting the "serialCommandCodes" event to
//  the SERIAL_INTERFACE_Q.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::SerialOutputEventHappened(
						SerialInterface::SerialCommandCodes serialCommandCodes,
						char *msgBuffer, Uint16 length)
{
	CALL_TRACE("LaptopMessageHandler::SerialOutputEventHappened(serialCommandCodes, msgBuffer, length)");

	// Inform the GUI-Serial-Interface subsystem of the requesting of
	// the put operation
	Uint byteCount = SerialInterface::Write( msgBuffer, length, 0, 
							LaptopEventRegistrar::GetControlPort() );
	CLASS_ASSERTION( byteCount == length );
														// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LaptopMessageHandler::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("LaptopMessageHandler::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LAPTOPMESSAGEHANDLER,
							lineNumber, pFileName, pPredicate);
}
