/*****************************************************************************/
/*   Module Name:     panapi.h                                              
/*   Purpose:       840 Ventilator Final Test                             
/*                                                                          
/*                                                                           
/*   Requirements:  This module implements requirements detailed in        
/*          <72361-93>                                                             
/*                                                                           
/*   $Revision::   1.1     $                                                    
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.

Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _VENT_PANEL_API_
#define _VENT_PANEL_API_

#define AC     1
#define SIMV   2
#define SPONT  3
#define PC     4
#define VC     5
#define PTRIG  6
#define VTRIG  7
#define SQUARE 8

#define D_ON   1
#define D_OFF  0

enum{
	F_VOLUME = 1,
	F_BREATH,
	F_FLOW,
	F_TPL,
	F_PSENS,
	F_PEEP,
	F_O2,
	F_PI,
	F_P, 
	F_TI,
	F_MODE,
	F_IBW,
	F_MANTYPE,
	F_TRIGGER,
	MAX_FLASH,
}; 

int  InitPerfPanel(int *panelId, int *done); 
void FlashOn(int control);
void ResetFlashOn(int control);    
void SetModeDisplay(int display, int mode);
void SetManTypeDisplay(int display, int mode); 
void SetTriggerTypeDisplay(int display, int mode); 
void SetIdealBodyWeightDisplay(int display, int reading); 
void SetBreathPerMinuteDisplay(int display, double reading);
void SetVolumeDisplay(int display, int reading);
void SetMaxFlowDisplay(int display, double reading);  
void SetTplDisplay(int display, double reading); 
void SetWaveFormDisplay(int display, int type); 
void SetPsensDisplay(int display, double reading); 
void SetO2Display(int display, int reading);
void SetPEEPDisplay(int display, double reading);
void SetTiDisplay(int display, double reading); 
void SetPiDisplay(int display, double reading);
void SetPDisplay(int display, int reading);
int  DisplaySST(void); 
void SetInstructionBox(int display, char *title, char *inst); 

#endif // #ifndef _VENT_PANEL_API_
