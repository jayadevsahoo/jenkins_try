#ifndef MemoryStructs_HH
#define MemoryStructs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================
// Filename:	MemoryStructs - Data type definitions of NormalVentMemory,
// ServiceModeMemory and ScreenMemoryUnion. 
//====================================================================
// 
//@ Version
// @(#) $$
//
//@ Modification-Log
//  Revision: 001  By:  hhd    Date:  15-NOV-99    DR Number: 5327
//       Project:  Neo-Mode
//       Description:
//		Initial version.
//====================================================================

// Code
#include "LowerScreen.hh"
#include "UpperScreen.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceUpperScreen.hh"
extern Uint ScreenMemory[];

// The following structures are declared so that we can end up with a
// union (ScreenMemoryUnion) whose size will be large enough to hold either
// the normal ventilation upper and lower screens or the service mode upper
// and lower screens.  This memory is reused for constructing normal
// ventilation and service mode screens (memory conservation!).
struct NormalVentMemory
{
    char lowerScreen[sizeof (LowerScreen)];
	char upperScreen[sizeof (UpperScreen)];
};
struct ServiceModeMemory
{
	char lowerScreen[sizeof (ServiceLowerScreen)];
	char upperScreen[sizeof (ServiceUpperScreen)];
};
union ScreenMemoryUnion
{
	NormalVentMemory  normalVentMemory;
	ServiceModeMemory serviceModeMemory;
};

#endif // MemoryStructs_HH
