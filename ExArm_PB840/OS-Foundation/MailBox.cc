#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MailBox - A specialized queue derived from IpcQueue class.
//---------------------------------------------------------------------
//@ Interface-Description
//  The MailBox class can be used for inter-task communication and
//  synchronization using 32 bit messages.  A MailBox object
//  is created with an Identification number, length and name as defined 
//  in the IpcTableDef.cc. The MailBox class is basically the same
//  as a IpcQueue of length 1, with an added overWrite() function.
//  All public functions available to a IpcQueue object are accessible
//  to a MailBox object. After a MailBox object is created, 32 bit
//  messages can be safely transmitted between tasks, using putMsg() or 
//  overWrite() to place a message in the MailBox, and using pendForMsg() 
//  or acceptMsg() to retrieve messages from the MailBox.  
//
//  Note: A full MailBox is not a fatal error.
//---------------------------------------------------------------------
//@ Rationale
//  The MailBox class provides a simple and safe way to synchronize
//  tasks, by passing messages when buffering of messages is not 
//  necessary.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The MailBox class publicly inherits from the IpcQueue class to take
//  advantage of the IpcQueue interface and functionality.  The MailBox
//  class is simply a IpcQueue with a length of one, and the added 
//  overWrite() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with pre and 
//  post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//  The MailBox class can not allow two messages to exist in a MailBox.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/MailBox.ccv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
#include "Sigma.hh"
#include "OsFoundation.hh"
#include "MailBox.hh"
#include "Task.hh"

//@ Usage-Classes
#include "IpcQueue.hh"
#include "IpcTable.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MailBox()
//
//@ Interface-Description
// Constructs a MailBox object with an I.D. from the IpcTableDef.cc. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the IpcQueue constructor for this qId.
//---------------------------------------------------------------------
//@ PreCondition
//  The qId will be checked for validity in IpcQueue.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
MailBox::MailBox(const Int32 qId) : IpcQueue(Ipc::MAILBOX, qId)
{
    error_ = Ipc::OK;
    			// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MailBox()  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
MailBox::~MailBox(void)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
// This method creates all the IpcQueues of type MAILBOX in the
// IpcTable[] array.  It calls the protected method IpcQueue::Create_
// to actually create the VRTX32 queue.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls IpcQueue::Create_ with the id, length, and wait order specified
//  for each entry in IpcTable defined as a MAILBOX type. Wait order is
//  defaulted to PRIORITY_ORDER.
//---------------------------------------------------------------------
//@ PreCondition
//    (IpcTable::IsInitialized() == TRUE)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void MailBoxList::Initialize(void)
{
    CLASS_PRE_CONDITION( IpcTable::IsInitialized() );
 
    for (Int32 i=0; i<IpcTable::GetNumEntries(); i++)
    {
      const IpcTableEntry& ipcEntry = IpcTable::GetIpcTableEntry(i);
      if (ipcEntry.type == Ipc::MAILBOX)
      {						// $[TI1]
         IpcQueueList::Create_(&ipcEntry);
      }						// $[TI2]
    }
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: overWrite()
//
//@ Interface-Description
// Takes a constant Int32 message as input, and replaces the current message
// (if any) in the MailBox with the input message. Returns Ipc::OK 
// or Ipc::Q_FULL.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls acceptMsg() to clear the MailBox and then calls
// putMsg() to put the input message into the MailBox.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32 MailBox::overWrite(const Int32 msg)
{
    Int32 tmp;
    acceptMsg(tmp);
    Int32 err = putMsg(msg);
    // Ipc::Q_FULL is acceptable; it just means that another task
    // put a message in the MailBox between the acceptMsg() and the
    // putMsg().
    CLASS_ASSERTION(err == Ipc::OK || err == Ipc::Q_FULL);//TODO define Q_FULL for Windows!
    return ( Ipc::OK );					// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: putMsg()
//
//@ Interface-Description
//  Takes a constant Int32 msg.
//  Puts the message in the mailbox depending on the success of
//  the operation. It returns Ipc::OK or Ipc::Q_FULL.
//  A queue full error return is non-fatal.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls WriteMsgQueue for "this" queue object handle
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32 MailBox::putMsg(const Int32 msg)
{
//if ported to WinCE
#if defined(_WIN32_WCE) || defined(WIN32)
 	HANDLE txHandle = getHandle()->txQhandle;
	AUX_CLASS_ASSERTION(txHandle != NULL, QID_)
	if(!WriteMsgQueue(txHandle, (LPVOID)&msg, IpcQueue::IPC_MSG_SIZE, Ipc::NO_WAIT, 0))
	{
		error_ = GetLastError();
		error_ = mapOsIpcError(error_);
	}
	else
	{
		error_ = Ipc::OK;
	}
#else
	error_ = Ipc::TIMED_OUT;
#endif
	Uint32 auxErrorCode = error_ | QID_<<24;
    AUX_CLASS_ASSERTION((error_ == Ipc::OK || error_ == Ipc::Q_FULL), auxErrorCode );//TODO define Q_FULL for Windows!
 
    return error_;						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PutMsg()
//
//@ Interface-Description
//  Puts the message in the mailbox.
//  Takes a constant Int32 msg and a constant Int32 qId reference and
//  returns Int32; Ipc::OK or Ipc::Q_FULL.
//  A queue full error return is non-fatal.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls WriteMsgQueue for the queue object handle corresponding the the
//  passed ID in the argument
//---------------------------------------------------------------------
//@ PreCondition
//  The call will return Ipc::BAD_QID if the queue does not
//  exist. This error code will cause an assertion.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32 MailBoxList::PutMsg(const Int32 qId, const Int32 msg)
{
	//if ported to WinCE
#if defined(_WIN32_WCE) || defined(WIN32)
    Int32 error = Ipc::OK;
	
	//MailBox targetMailBox(qId);
	//HANDLE txHandle = targetMailBox.getHandle()->txQhandle;
	HANDLE txHandle = IpcQueueList::GetQueueById(qId)->txQhandle; //this is faster than instanciating a MailBox
	AUX_CLASS_ASSERTION(txHandle != NULL, qId)
	if(!WriteMsgQueue(txHandle, (LPVOID)&msg, IpcQueue::IPC_MSG_SIZE, Ipc::NO_WAIT, 0))
	{      
		error = GetLastError();
		error = IpcQueue::mapOsIpcError(error);
	}
#else
	error = Ipc::TIMED_OUT;
#endif
	Uint32 auxErrorCode = error | qId<<24;
    AUX_CLASS_ASSERTION( (error == Ipc::OK || error == Ipc::Q_FULL), auxErrorCode );//TODO define Q_FULL for Windows!
    return error;  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
MailBox::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::MAILBOX,
      lineNumber, pFileName, pPredicate);
}
