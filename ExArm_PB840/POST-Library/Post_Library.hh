#ifndef Post_Library_HH
#define Post_Library_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Post_Library - Common POST functions.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/Post_Library.hhv   25.0.4.0   19 Nov 2013 14:16:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 06-JUN-1997  DR Number: 2183
//    Project: Sigma (R8027)
//    Description:
//       Added sense information as part of auxiliary results.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

#include "SigmaTypes.hh"
#include "PostDefs.hh"

//@ Usage-Classes
//@ End-Usage

typedef void (*Handler)(void);

class Post_Library
{
  public:
    static void  Initialize(void);

  private:
    Post_Library(const Post_Library&);		// not implemented...
    Post_Library(void);				        // not implemented...
    ~Post_Library(void);				        // not implemented...
    void  operator=(const Post_Library&);		// not implemented...
};

//@ Begin-Free-Declarations

void      inline SetPostStep(Uint8 testNumber);
void      inline SetErrorCode(Uint8 code);
void      inline SetSense(Uint32 sense);
Uint32    inline GetSense(void);

void      SetTestResult(Uint8 result);
void      MajorPostFailure(void);
void      SwitchAlarms(Boolean state);
void      SwitchLeds(Boolean state);

// defined in InstallHandler.s
Handler   InstallHandler(Uint vector, Handler handler);

// defined in ClearMemory.s
Boolean   ClearMemory(Uint32 * pStart, Uint32 length);
//@ End-Free-Declarations


#include "Post_Library.in"

#endif // Post_Library_HH
