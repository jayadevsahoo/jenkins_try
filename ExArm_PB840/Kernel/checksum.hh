#ifndef checksum_HH
#define checksum_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: checksum.hh
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/checksum.hhv   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "kpost.hh"
#include "cktable.hh"

//@ Begin-Free-Declarations

#ifdef E600_840_TEMP_REMOVED
extern Criticality FlashChecksumTest(void);
extern Criticality BootEpromTest(void);
#endif

//@ End-Free-Declarations

#endif  // checksum_HH
