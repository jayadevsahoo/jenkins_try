//------------------------------------------------------------------------------
//                   Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file pset.hh
///
/// This file contains an fixed block allocator used with the std::set
/// types so that any dynamic storage requested by the STL library is obtained
/// from a fixed memory pool and not the global heap. The class plist
/// is used in place of std::list. Use plist exactly the same as
/// std::set except that the maximum string size is limited to the pool
///	size limit.
///
/// Fixed block allocators are used to eliminate dynamic storage allocation
/// heap fragmentation issues.
///
/// Example usage:
///
///	pset<pstring*> testset;
///	pstring teststr = "test str";
///	testset.insert(&teststr);
///	pset<pstring*>::iterator retIt = testset.find(&teststr);
//------------------------------------------------------------------------------
#ifndef PSET_H
#define PSET_H

//------------------------------------------------------------------------------
// I N C L U D E S
//------------------------------------------------------------------------------
#include "singlepool_allocator.hh"
#include <set>

template<class _Kty,
	class _Pr = std::less<_Kty>,
	class _Alloc = singlepool_allocator<_Kty> >
	class pset
		: public std::set<_Kty, _Pr, _Alloc>
	{
	};

template<class _Kty,
	class _Pr = std::less<_Kty>,
	class _Alloc = singlepool_allocator<_Kty> >
	class pmultiset
		: public std::multiset<_Kty, _Pr, _Alloc>
	{
	};

#endif // PSET_H

