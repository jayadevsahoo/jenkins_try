#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: ServiceModeAgent - Interface Agent to the Service-Mode data.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the interface to Service-Mode subsystem data
//  sent by the BD to the GUI.  It is instantiated by Task-Control
//  in the BdReadyMessage and accessed by applications that 
//  retrieve the ServiceModeAgent reference from the message.
//---------------------------------------------------------------------
//@ Rationale
//  The ServiceModeAgent class encapsulates the data from the 
//  Service-Mode subsystem that is sent to the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The ServiceModeAgent is constructed with the current states 
//  maintained by the Service-Mode subsystem.
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ServiceModeAgent.ccv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 008   By:  quf   Date:  16-Oct-1997   DR Number: DCS 2567
//       Project:  Sigma (840)
//       Description:
//			Removed fio2 calibration status.
//
//  Revision: 007   By:  quf   Date:  24-Sep-1997   DR Number: DCS 2410
//       Project:  Sigma (840)
//       Description:
//			Added private data member atmPresXducerCalStatus_ and
//			access method getAtmPresXducerCalStatus(), updated constructors.
//
//  Revision: 006   By:  quf   Date:  18-Sep-1997   DR Number: DCS 1861, 2525
//       Project:  Sigma (840)
//       Description:
//			Added private data member ventInopTestStatus_ and
//			access method getVentInopTestStatus(), updated constructors,
//			eliminated vent inop test Boolean data members and access methods.
//
//  Revision: 005   By:  quf   Date:  15-Sep-1997   DR Number: DCS 2385
//       Project:  Sigma (840)
//       Description:
//			Added private data member flowSensorCalStatus_ and
//			access method getFlowSensorCalStatus(), updated constructors.
//
//  Revision: 004  By:  quf    Date:  11-Aug-97    DR Number: DCS 2107
//       Project:  Sigma (840)
//       Description:
//			Due to change from Boolean to 3-state EV cal novram flag,
//          exhValveRequired_ was changed to exhValveCalStatus_,
//          isExhValveCalRequired() was changed to getExhValveCalStatus(),
//          fixed constructor and copy constructor.
//
//  Revision: 003  By:  quf    Date:  16-Jul-97    DR Number: DCS 2066
//       Project:  Sigma (840)
//       Description:
//             Moved "SST required" calculation from ServiceModeAgent::isSstRequired()
//             to ServiceMode::IsSstRequired().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//            isSstRequired() now defined for GUI only.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "ServiceModeAgent.hh"

//@ Usage-Classes

#include "NovRamManager.hh"

#if defined(SIGMA_BD_CPU)
#include "VentObjectRefs.hh"
#include "ForceVentInopTest.hh"
#include "ServiceModeNovramData.hh"
#include "SmRefs.hh"
#endif  // defined(SIGMA_BD_CPU)

#ifdef SIGMA_DEBUG
#include "stdio.h"
#endif  // SIGMA_DEBUG


//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


#if defined(SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceModeAgent [constructor]
//
//@ Interface-Description
//  Constructor.  No arguments.  Initializes data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize data members.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ServiceModeAgent::ServiceModeAgent(void)
    : flowSensorInfoRequired_(!ServiceMode::IsFlowSensorInfoValid())
     ,flowSensorCalStatus_(ServiceModeNovramData::GetFlowSensorCalStatus())
     ,atmPresXducerCalStatus_(ServiceModeNovramData::GetAtmPresXducerCalStatus())
     ,exhValveCalStatus_(ServiceMode::GetExhValveCalStatus())
     ,ventInopTestStatus_(RForceVentInopTest.getStatus())
	 ,isSstRequired_( ServiceMode::IsSstRequired())
{
// $[TI1]
    CALL_TRACE("ServiceModeAgent(void)");

    NovRamManager::GetOverallEstResult(bdEstOverallResult_);
    NovRamManager::GetOverallSstResult(bdSstOverallResult_);
}

#endif  // defined(SIGMA_BD_CPU)


#if defined(SIGMA_GUI_CPU) && defined(SIGMA_UNIT_TEST)
// Required for GUI unit test ONLY in order to test the copy
// constructor
ServiceModeAgent::ServiceModeAgent(void)
    : flowSensorInfoRequired_(FALSE)
	 ,flowSensorCalStatus_(ServiceMode::PASSED)
     ,atmPresXducerCalStatus_(ServiceMode::FAILED)
     ,exhValveCalStatus_(ServiceMode::PASSED)
     ,ventInopTestStatus_(ServiceMode::FAILED)
	 ,isSstRequired_(TRUE)
{

}

#endif  // defined(SIGMA_GUI_CPU) && defined(SIGMA_UNIT_TEST)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceModeAgent [copy constructor]
//
//@ Interface-Description
//  Copy constructor.  Makes a copy of the argument ServiceModeAgent object.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Create a copy of the argument ServiceModeAgent object.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ServiceModeAgent::ServiceModeAgent(const ServiceModeAgent& rServiceModeAgent)
    : flowSensorInfoRequired_(rServiceModeAgent.flowSensorInfoRequired_)
     ,flowSensorCalStatus_(rServiceModeAgent.flowSensorCalStatus_)
     ,atmPresXducerCalStatus_(rServiceModeAgent.atmPresXducerCalStatus_)
     ,exhValveCalStatus_(rServiceModeAgent.exhValveCalStatus_)
     ,bdEstOverallResult_(rServiceModeAgent.bdEstOverallResult_)
     ,bdSstOverallResult_(rServiceModeAgent.bdSstOverallResult_)
     ,ventInopTestStatus_(rServiceModeAgent.ventInopTestStatus_)
	 ,isSstRequired_( rServiceModeAgent.isSstRequired_)
{
// $[TI2]
    CALL_TRACE("ServiceModeAgent(const ServiceModeAgent&)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceModeAgent [destructor]
//
//@ Interface-Description
//  Destructor for the ServiceModeAgent.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ServiceModeAgent::~ServiceModeAgent(void)
{
    CALL_TRACE("~ServiceModeAgent(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ==== 
//@ Method:  isSstRequired
//                                                                      
//@ Interface-Description                                               
//	This method has no arguments and returns the SST required
//	status.
//--------------------------------------------------------------------- 
//@ Implementation-Description                                          
//	Return the Boolean value isSstRequired_.
//--------------------------------------------------------------------- 
//@ PreCondition                                                        
//   none                                                               
//--------------------------------------------------------------------- 
//@ PostCondition                                                       
//   none                                                               
//@ End-Method                                                          
//===================================================================== 

#if defined(SIGMA_GUI_CPU)

Boolean
ServiceModeAgent::isSstRequired (void) const
{
// $[TI1]
	CALL_TRACE("ServiceModeAgent::isSstRequired (void) const");

	return( isSstRequired_);
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
ServiceModeAgent::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, SERVICEMODEAGENT, 
                          lineNumber, pFileName, pPredicate);
}

#endif  // defined(SIGMA_GUI_CPU)


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================
