#ifndef MainSettingsArea_HH
#define MainSettingsArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MainSettingsArea - The area on the Lower Screen containing
// the main setting buttons and the breath control settings (displaying modes
// and IBW).  This class is part of the GUI-Apps Framework cluster.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MainSettingsArea.hhv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By: rhj     Date:  10-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 009  By: gdc     Date:  27-May-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Removed Insp Too Long alarm for NIV. Added High Ti spont alert.
//
//  Revision: 008  By: gdc     Date:  15-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - NIV1
//      added NIV indicator on status line
//
//  Revision: 007  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added button for new volume support setting
//
//  Revision: 006  By:  hhd	   Date:  07-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//      Added batch change capability to the MainSettingsArea, whereby
//      multiple settings can be changed and accepted as a group.
//
//  Revision: 005  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/MainSettingsArea.hhv   1.9.1.0   07/30/98 10:15:26   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiEventTarget.hh"

//@ Usage-Classes
#include "Container.hh"
#include "DiscreteSettingButton.hh"
#include "IeRatioSettingButton.hh"
#include "Line.hh"
#include "NumericSettingButton.hh"
#include "TextField.hh"
#include "TimingSettingButton.hh"
#include "ContextObserver.hh"
#include "Box.hh"
//@ End-Usage

class MainSettingsArea : public Container, public GuiEventTarget,
	                                public GuiTimerTarget,
									public ContextObserver
{
public:
	MainSettingsArea(void);
	~MainSettingsArea(void);

	void initialize(void);		// Initialize Main Settings Area
	void setToFlat(void);		// Flatten all buttons
	void setToUp(void);			// Unflatten all buttons
	void setBlank(Boolean blankFlag);

	// GuiEventTarget virtual method
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// ContextObserver virtual method...
    virtual void  batchSettingUpdate(
							 const Notification::ChangeQualifier qualifierId,
							 const ContextSubject*               pSubject,
							 const SettingId::SettingIdType      settingId
									);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	MainSettingsArea(const MainSettingsArea&);		// not implemented...
	void   operator=(const MainSettingsArea&);	// not implemented...

	void updateArea_(void);			// Formulates the display

	//@ Data-Member: isBlank_
	// Tells whether the Main Settings Area is blank or not.  If it is blank
	// then no buttons are displayed but the black status line and the button
	// area below are still shown.  Defaults to TRUE.
	Boolean isBlank_;

	//@ Data-Member: nivBackground_
	// The yellow background for the vent-type and mode in NIV
	Box nivBackground_;

	//@ Data-Member: ventTypeText_
	// A text field for displaying the current vent type. 
	TextField ventTypeText_;

	//@ Data-Member: modeText_
	// A text field for displaying the current Mode.
	TextField modeText_;

	//@ Data-Member: mandatoryTypeText_
	// A text field for displaying the current Mandatory Type.
	TextField mandatoryTypeText_;

	//@ Data-Member: spontTypeText_
	// A text field for displaying the current Spont Type.
	TextField spontTypeText_;

	//@ Data-Member: triggerTypeText_
	// A text field for displaying the current Trigger Type.
	TextField triggerTypeText_;

	//@ Data-Member: ibwNumeric_
	// A numeric field for displaying the value of IBW.
	NumericField ibwNumeric_;

	//@ Data-Member: kilogramText_
	// The text field which displays `Kg' for the IBW value.
	TextField kilogramText_;

	//@ Data-Member: buttonContainer_
	// The area which contains the button.  It is defined as the area
	// below the status line.
	Container buttonContainer_;

	//@ Data-Member: expiratoryTimeButton_
	// The Expiratory Time main setting button.
	TimingSettingButton expiratoryTimeButton_;

	//@ Data-Member: peepLowTimeButton_
	// The Peep Low Time setting button
	TimingSettingButton peepLowTimeButton_;

	//@ Data-Member: flowAccelerationButton_
	// The Flow Acceleration setting button.
	NumericSettingButton flowAccelerationButton_;

	//@ Data-Member: flowPatternButton_
	// The Flow Pattern main setting button.
	DiscreteSettingButton flowPatternButton_;

	//@ Data-Member: flowSensitivityButton_
	// The Flow Sensitivity main setting button.
	NumericSettingButton flowSensitivityButton_;

	//@ Data-Member: expiratorySensitivityButton_
	// The Expiratory Sensitivity main setting button.
	NumericSettingButton expiratorySensitivityButton_;

	//@ Data-Member: ieRatioButton_
	// The I:E Ratio main setting button.
	IeRatioSettingButton ieRatioButton_;

	//@ Data-Member: hlRatioButton_
	// The peep high:peep low Ratio setting button
	IeRatioSettingButton hlRatioButton_;

	//@ Data-Member: inspiratoryPressureButton_
	// The Inspiratory Pressure main setting button.
	NumericSettingButton inspiratoryPressureButton_;

	//@ Data-Member: inspiratoryTimeButton_
	// The Inspiratory Time main setting button.
	TimingSettingButton inspiratoryTimeButton_;

	//@ Data-Member: peepHighTimeButton_
	// The Peep High Time setting button
	TimingSettingButton peepHighTimeButton_;

	//@ Data-Member: oxygenPercentageButton_
	// The Oxygen Percentage main setting button.
	NumericSettingButton oxygenPercentageButton_;

	//@ Data-Member: peakFlowButton_
	// The Peak Flow main setting button.
	NumericSettingButton peakFlowButton_;

	//@ Data-Member: peepButton_
	// The Peep main setting button.
	NumericSettingButton peepButton_;

	//@ Data-Member: peepLowButton_
	// the Peep Low setting button.
	NumericSettingButton peepLowButton_;

	//@ Data-Member: peepHighButton_
	// The Peep High setting button.
	NumericSettingButton peepHighButton_;

	//@ Data-Member: plateauTimeButton_
	// The Plateau Time main setting button.
	TimingSettingButton plateauTimeButton_;

	//@ Data-Member: pressureSensitivityButton_
	// The Pressure Sensitivity main setting button.
	NumericSettingButton pressureSensitivityButton_;

	//@ Data-Member: pressureSupportButton_
	// The Pressure Support main setting button.
	NumericSettingButton pressureSupportButton_;

	//@ Data-Member: respiratoryRateButton_
	// The Respiratory Rate main setting button.
	NumericSettingButton respiratoryRateButton_;

	//@ Data-Member: tidalVolumeButton_
	// The Tidal Volume main setting button.
	NumericSettingButton tidalVolumeButton_;

	//@ Data-Member: percentSupportButton_
	// The Percent Support main setting button.
	NumericSettingButton percentSupportButton_;

	//@ Data-Member: volumeSupportButton_
	// The Volume Support main setting button.
	NumericSettingButton volumeSupportButton_;

	//@ Data-Member: highSpontInspTimeButton_
	// The High Spontaneous Inspiratory Time main setting button.
	TimingSettingButton highSpontInspTimeButton_;


	//@ Data-Member: leftWingtip_
	// Left-hand "wingtip" line for the fake status bar. 
	Line leftWingtip_;

	//@ Data-Member: leftWing_
	// Left-hand "wing" line for the fake status bar.
	Line leftWing_;

	//@ Data-Member: rightWing_
	// Right-hand "wing" line for the fake status bar.
	Line rightWing_;

	//@ Data-Member: rightWingtip_
	// Right-hand "wingtip" line for the fake status bar.
	Line rightWingtip_;

	//@ Data-Member: leakCompText_
	// A text field for displaying the leak compenstation ventilation 
	TextField leakCompText_;

	//@ Data-Member: DisplayLeakComp_
	// Used to toogle the displaying leak compenstation ventilation and ventilation mode 
	static Boolean DisplayLeakComp_;

	enum { NUM_SETTING_BUTTONS_ = 24 };

	//@ Data-Member: arrMainSettingButtons_
	// Array of setting buttons.
	SettingButton*  arrMainSettingButtons_[NUM_SETTING_BUTTONS_ + 1];
};


#endif // MainSettingsArea_HH 
