//                           
#ifndef BreathDataPacket_HH
#define BreathDataPacket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct : BreathDataPacket - a structure definition of a patient data
//   packet that is generated at the end of each breath.
//
//   RealTimeDataPackeet - a structure definition of a patient data packet
//    that is generated at the end of each inspiration phase of a breath.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/BreathDataPacket.hhv   25.0.4.0   19 Nov 2013 14:17:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 016   By:   rhj    Date: 05-March-2009    SCR Number: 6454
//  Project:  840S
//  Description:
//       Added inspiredLungVolAlarm to the RealTimeDataPacket struct.
// 
//  Revision: 015   By:   rhj    Date: 07-July-2008    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 014   By: gdc      Date:  20-Feb-2007    SCR Number: 6345
//  Project:  RESPM
//  Description:
//		Removed C20/C related code.
//
//  Revision: 013   By: gdc      Date:  16-May-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project related changes.
//
//  Revision: 012   By: gdc      Date:  10-Jun-2005    SCR Number: 6170
//  Project:  NIV
//  Description:
//		Added processing to detect NIV high inspiratory time and generate
//		patient-data callback for displaying alert.
//
//  Revision: 011   By: gdc      Date:  16-Feb-2005    SCR Number: 6144
//  Project:  NIV
//  Description:
//		Added vent-type (NIV/INVASIVE) to real-time data packet.
//
//  Revision: 010   By: yakovb   Date:  19-Mar-2002    DR Number: 5863
//  Project:  VCP
//  Description:
//      Handle VC+ mandatory messages.
//
//  Revision: 009   By: syw   Date:  15-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      * added 'spontInspTime' to the real-time data packet
//      * added 'spontInspTime', and 'spontTiTtotRatio' to the breath data
//        packet
//
//  Revision: 008  By:  sah    Date:  17-Jul-00    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      * added 'dynamicCompliance', and 'dynamicResistance' to the breath
//        data packet
//
//  Revision: 007  By:  jja    Date:  20-Apr-00    DR Number: 5708 
//	Project:  NeoMode
//	Description:
//		Revise Pmean to be an averaged value instead of  
//              single breath.
//
//  Revision: 006  By:  jja    Date:  06-Apr-00    DR Number: 5692 
//	Project:  NeoMode
//	Description:
//		Switched peakCircuitPressure from end of breath to 
//              end of insp structures.
//
//  Revision: 005  By:  syw    Date:  17-Aug-99    DR Number: 5471, 5481 
//	Project:  ATC
//	Description:
//		Added breathTypeDisplay to all structures
//
//  Revision: 004  By:  hct    Date:  20-Jan-99    DR Number: 5322 
//	Project:  ATC
//	Description:
//		Add SupportTypeValue.
//
//  Revision: 003  By:  syw    Date:  19-Jan-99    DR Number: 5322 
//	Project:  ATC
//	Description:
//		TC initial version.
//
//  Revision: 002  By:  syw    Date:  28-Jan-98    DR Number: 
//	Project:  BiLevel (R8027)
//	Description:
//		BiLevel initial version.
//		Added phaseType to RealTimeDataPacket and BreathDataPacket.
//		Defined BiLevelMandDataPacket and BiLevelMandExhDataPacket.
//
//  Revision: 001  By:  jhv    Date:  20-Jan-95    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================
#include "SocketWrap.hh"
#include "Sigma.hh"
#include "PatientDataMgr.hh"
#include "BreathType.hh"
#include "BreathPhaseType.hh"
#include "TimeStamp.hh"
#include "SupportTypeValue.hh"
#include "MandTypeValue.hh"
#include "VentTypeValue.hh"
#include "Trigger.hh"

// @Type: RealTimeDataPacket
struct   RealTimeDataPacket
{
	BreathType  breathType;
	Real32 endInspPressure;
	BreathPhaseType::PhaseType phaseType ;
	Real32 inspiredlungVolume ;
	SupportTypeValue::SupportTypeValueId supportType;
	MandTypeValue::MandTypeValueId mandType;
	BreathType breathTypeDisplay ;
	Real32  peakCircuitPressure;
	Real32	spontInspTime;
	VentTypeValue::VentTypeValueId ventType;
	Trigger::TriggerId breathTriggerId;
	Real32  dynamicCompliance;
	Real32  dynamicResistance;
	Real32  peakSpontInspFlow;
	Real32  inspiredLungVolAlarm;

	inline void convHtoN()
	{
		SocketWrap sock;
		breathType = (BreathType)sock.HTONL((Uint32)breathType);
		endInspPressure = sock.HTONF(endInspPressure);
		phaseType = (BreathPhaseType::PhaseType)sock.HTONL((Uint32)phaseType);
		inspiredlungVolume = sock.HTONF(inspiredlungVolume);
		supportType = (SupportTypeValue::SupportTypeValueId)sock.HTONL((Uint32)supportType);
		mandType = (MandTypeValue::MandTypeValueId)sock.HTONL((Uint32)mandType);
		breathTypeDisplay = (BreathType)sock.HTONL((Uint32)breathTypeDisplay);
		peakCircuitPressure = sock.HTONF(peakCircuitPressure);
		spontInspTime = sock.HTONF(spontInspTime);
		ventType = (VentTypeValue::VentTypeValueId)sock.HTONL((Uint32)ventType);
		breathTriggerId = (Trigger::TriggerId)sock.HTONL((Uint32)breathTriggerId);
		dynamicCompliance = sock.HTONF(dynamicCompliance);
		dynamicResistance = sock.HTONF(dynamicResistance); 
		peakSpontInspFlow = sock.HTONF(peakSpontInspFlow);
		inspiredLungVolAlarm = sock.HTONF(inspiredLungVolAlarm);
	}

	inline void convNtoH()
	{
		SocketWrap sock;
		breathType = (BreathType)sock.NTOHL((Uint32)breathType);
		endInspPressure = sock.NTOHF(endInspPressure);
		phaseType = (BreathPhaseType::PhaseType)sock.NTOHL((Uint32)phaseType);
		inspiredlungVolume = sock.NTOHF(inspiredlungVolume);
		supportType = (SupportTypeValue::SupportTypeValueId)sock.NTOHL((Uint32)supportType);
		mandType = (MandTypeValue::MandTypeValueId)sock.NTOHL((Uint32)mandType);
		breathTypeDisplay = (BreathType)sock.NTOHL((Uint32)breathTypeDisplay);
		peakCircuitPressure = sock.NTOHF(peakCircuitPressure);
		spontInspTime = sock.NTOHF(spontInspTime);
		ventType = (VentTypeValue::VentTypeValueId)sock.NTOHL((Uint32)ventType);
		breathTriggerId = (Trigger::TriggerId)sock.NTOHL((Uint32)breathTriggerId);
		dynamicCompliance = sock.NTOHF(dynamicCompliance);
		dynamicResistance = sock.NTOHF(dynamicResistance); 
		peakSpontInspFlow = sock.NTOHF(peakSpontInspFlow);
		inspiredLungVolAlarm = sock.NTOHF(inspiredLungVolAlarm);
	}
};

// @Type: BreathDataPacket
struct   BreathDataPacket
{
	TimeStamp   time;
	BreathType  breathType;
	Real32  exhaledTidalVolume;
	Real32  breathDuration;
	Real32  endExpiratoryPressure;
	Real32  ieRatio;
	Real32	mandFraction ;
	Real32  dynamicCompliance;
	Real32  dynamicResistance;
	BreathType previousBreathType;
	BreathPhaseType::PhaseType phaseType ;
	BreathType breathTypeDisplay ;
	Real32	spontInspTime;
	Real32	spontTiTtotRatio;
	Real32  peakExpiratoryFlow;
	Real32  endExpiratoryFlow;
    Real32  percentLeak;
	Real32  exhLeakRate;
	Uint    isLeakDetected; 
    Real32  inspLeakVol;

	inline void convHtoN()
	{
		SocketWrap sock;
		time.convHtoN();
		breathType = (BreathType)sock.HTONL((Uint32)breathType);
		exhaledTidalVolume = sock.HTONF(exhaledTidalVolume);
		breathDuration = sock.HTONF(breathDuration);
		endExpiratoryPressure = sock.HTONF(endExpiratoryPressure);
		ieRatio = sock.HTONF(ieRatio);
		mandFraction = sock.HTONF(mandFraction);
		dynamicCompliance = sock.HTONF(dynamicCompliance);
		dynamicResistance = sock.HTONF(dynamicResistance);
		previousBreathType = (BreathType)sock.HTONL((Uint32)previousBreathType);
		phaseType = (BreathPhaseType::PhaseType)sock.HTONL((Uint32)phaseType);
		breathTypeDisplay = (BreathType)sock.HTONL((Uint32)breathTypeDisplay);
		spontInspTime = sock.HTONF(spontInspTime);
		spontTiTtotRatio = sock.HTONF(spontTiTtotRatio);
		peakExpiratoryFlow = sock.HTONF(peakExpiratoryFlow);
		endExpiratoryFlow = sock.HTONF(endExpiratoryFlow);
		percentLeak = sock.HTONF(percentLeak);
		exhLeakRate = sock.HTONF(exhLeakRate);
		isLeakDetected = sock.HTONL(isLeakDetected); 
		inspLeakVol = sock.HTONF(inspLeakVol);
	}

	inline void convNtoH()
	{
		SocketWrap sock;
		time.convNtoH();
		breathType = (BreathType)sock.NTOHL((Uint32)breathType);
		exhaledTidalVolume = sock.NTOHF(exhaledTidalVolume);
		breathDuration = sock.NTOHF(breathDuration);
		endExpiratoryPressure = sock.NTOHF(endExpiratoryPressure);
		ieRatio = sock.NTOHF(ieRatio);
		mandFraction = sock.NTOHF(mandFraction);
		dynamicCompliance = sock.NTOHF(dynamicCompliance);
		dynamicResistance = sock.NTOHF(dynamicResistance);
		previousBreathType = (BreathType)sock.NTOHL((Uint32)previousBreathType);
		phaseType = (BreathPhaseType::PhaseType)sock.NTOHL((Uint32)phaseType);
		breathTypeDisplay = (BreathType)sock.NTOHL((Uint32)breathTypeDisplay);
		spontInspTime = sock.NTOHF(spontInspTime);
		spontTiTtotRatio = sock.NTOHF(spontTiTtotRatio);
		peakExpiratoryFlow = sock.NTOHF(peakExpiratoryFlow);
		endExpiratoryFlow = sock.NTOHF(endExpiratoryFlow);
		percentLeak = sock.NTOHF(percentLeak);
		exhLeakRate = sock.NTOHF(exhLeakRate);
		isLeakDetected = sock.NTOHL(isLeakDetected); 
		inspLeakVol = sock.NTOHF(inspLeakVol);
	}
};

struct BiLevelMandDataPacket
{
	BreathType  breathType;
	Real32 endExpiratoryPressure;
	Real32  peakCircuitPressure;
	BreathPhaseType::PhaseType phaseType ;
	BreathType breathTypeDisplay ;

	inline void convHtoN()
	{
		SocketWrap sock;
		breathType = (BreathType)sock.HTONL((Uint32)breathType);
		endExpiratoryPressure = sock.HTONF(endExpiratoryPressure);
		peakCircuitPressure = sock.HTONF(peakCircuitPressure);
		phaseType = (BreathPhaseType::PhaseType)sock.HTONL((Uint32)phaseType);
		breathTypeDisplay = (BreathType)sock.HTONL((Uint32)breathTypeDisplay);
	}

	inline void convNtoH()
	{
		SocketWrap sock;
		breathType = (BreathType)sock.NTOHL((Uint32)breathType);
		endExpiratoryPressure = sock.NTOHF(endExpiratoryPressure);
		peakCircuitPressure = sock.NTOHF(peakCircuitPressure);
		phaseType = (BreathPhaseType::PhaseType)sock.NTOHL((Uint32)phaseType);
		breathTypeDisplay = (BreathType)sock.NTOHL((Uint32)breathTypeDisplay);
	}
} ;

struct BiLevelMandExhDataPacket
{
	BreathType  breathType;
	BreathPhaseType::PhaseType phaseType ;
	BreathType breathTypeDisplay ;
	Real32  peakCircuitPressure;

	inline void convHtoN()
	{
		SocketWrap sock;
		breathType = (BreathType)sock.HTONL((Uint32)breathType);
		peakCircuitPressure = sock.HTONF(peakCircuitPressure);
		phaseType = (BreathPhaseType::PhaseType)sock.HTONL((Uint32)phaseType);
		breathTypeDisplay = (BreathType)sock.HTONL((Uint32)breathTypeDisplay);
	}

	inline void convNtoH()
	{
		SocketWrap sock;
		breathType = (BreathType)sock.NTOHL((Uint32)breathType);
		peakCircuitPressure = sock.NTOHF(peakCircuitPressure);
		phaseType = (BreathPhaseType::PhaseType)sock.NTOHL((Uint32)phaseType);
		breathTypeDisplay = (BreathType)sock.NTOHL((Uint32)breathTypeDisplay);
	}
} ;

#endif // BreathDataPacket_HH 





