#include "stdafx.h"
// ****************************************************************************
//
//  UtilityFunctions.cpp - This contains misc global functions which are not
//          associated with a class.
//
// ****************************************************************************
#include "UtilityFunctions.h"

//#############################################################################
//	Function:	BinarySearchFloat()
//	
//	Inputs:	
//			Value	- Value to search for in source table 
//			pData	- Source table
//			iHi		- Highest index for source table
//
//	Outputs:
//			piLeft	- Left Index
//			piRight	- Right Index
//
//#############################################################################
void BinarySearchFloat( uint32_t *piLeft, uint32_t *piRight, float Value,
					   const float *pData, uint32_t iHi)
{
	/* Find the location of current input value in the data table. */
	*piLeft = 0U;
	*piRight = iHi;
	if (Value <= pData[0] ) {
		/* Less than or equal to the smallest point in the table. */
		*piRight = 0U;
	}
	else if (Value >= pData[iHi] ) {
		/* Greater than or equal to the largest point in the table. */
		*piLeft = iHi;
	}
	else {
		/* Do a binary search. */
		while (( *piRight - *piLeft ) > 1U )
		{
			/* Get the average of the left and right indices using to Floor rounding. */
			uint32_t i = (*piLeft + *piRight) >> 1;

			/* Move either the right index or the left index so that */
			/*  LeftDataPoint <= CurrentValue < RightDataPoint */
			if (Value < pData[i] ) 
				*piRight = i;
			else 
				*piLeft = i;
		}
	}
}

//#############################################################################
//	Function:	LookUpFloat()
//	
//	Inputs:	pYData	- Destination table
//			Value	- Value to search for in source table 
//			pUData	- Source table
//			iHi		- Highest index for source table
//
//	Outputs:
//			pY		- Returned value
//
//#############################################################################
void LookUpFloat( float *pY, const float *pYData, float Value,
				 const float *pUData, uint32_t iHi)
{
	uint32_t iLeft;
	uint32_t iRight;
	BinarySearchFloat( &iLeft, &iRight, Value, pUData, iHi);

	float lambda;
	if (pUData[iRight] > pUData[iLeft] ) {
		float den = pUData[iRight];
		den = den - pUData[iLeft];		
		float num = Value;
		num = num - pUData[iLeft];
		lambda = num / den;
	} 
	else {
	  lambda = 0.0;
	}

	float yLeftCast = pYData[iLeft];
	float yRightCast = pYData[iRight];
	yLeftCast += lambda * ( yRightCast - yLeftCast );
	(*pY) = yLeftCast;
}






