#ifndef LaptopEventCascade_HH
#define LaptopEventCascade_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LaptopEventCascade - A LaptopEventTarget which stores a list
// of LaptopEventTargets.  Allows multiple LaptopEventTargets to
// register for laptop request events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LaptopEventCascade.hhv   25.0.4.0   19 Nov 2013 14:08:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd		Date:  22-MAY-95	DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "LaptopEventTarget.hh"

//@ Usage-Classes
#include "GuiAppClassIds.hh"
//@ End-Usage


class LaptopEventCascade : public LaptopEventTarget
{
public:
	LaptopEventCascade(void);
	~LaptopEventCascade(void);

	// Other LaptopEventTarget's register with this method.
	void addTarget(LaptopEventTarget *pTarget);

	// Setting change notices are communicated through here.
	void laptopRequestHappened(LaptopEventId::LTEventId eventId, Uint8 *pMsgData);
	void laptopFailureHappened(SerialInterface::SerialFailureCodes
													serialFailureCodes,
													Uint8 eventId);
	
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	LaptopEventCascade(const LaptopEventCascade&);	// not implemented...
	void operator=(const LaptopEventCascade&);		// not implemented...

	enum
	{
		//@ Constant: LPT_MAX_TARGETS_
		// The maximum number of target pointers that can be stored by this
		// cascade
		LPT_MAX_TARGETS_ = 5
	};

    // @ Data-Member: targets_
    // The array which holds pointers to all of the targets.
    LaptopEventTarget *targets_[LPT_MAX_TARGETS_];

    //@ Data-Member: numTargets_
    // The number of targets stored in 'targets_[]'.
    Uint numTargets_;
};


#endif // LaptopEventCascade_HH 
