#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  TrendCursorPositionSetting - Trend Cursor Position Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a non-batch, bounded setting that stores the cursor's 
//  position for the Trend table or trend graph.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TrendCursorPositionSetting.ccv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: rhj    Date:  05-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Initial version
//=====================================================================

#include "TrendCursorPositionSetting.hh"
#include "Plot1TypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage
//@ Code...
//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

static const BoundedInterval  LAST_NODE_ =
{
	-359.0f,	// absolute minimum...
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	0.0f,		// absolute maximum...
	1.0f,		// resolution...
	ONES,		// precision...
	&::LAST_NODE_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: TrendCursorPositionSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, the value interval
//  get initialized in this method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendCursorPositionSetting::TrendCursorPositionSetting(void)
	: NonBatchBoundedSetting(SettingId::TREND_CURSOR_POSITION,
							 Setting::NULL_DEPENDENT_ARRAY_,
							 &::INTERVAL_LIST_,
							 TREND_CURSOR_POSITION_MAX_ID,// maxConstraintId...
							 TREND_CURSOR_POSITION_MIN_ID)// minConstraintId...
{
	CALL_TRACE("TrendCursorPositionSetting()");
}  

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~TrendCursorPositionSetting()  [Virtual Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendCursorPositionSetting::~TrendCursorPositionSetting(void)	
{
	CALL_TRACE("~TrendCursorPositionSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	TrendCursorPositionSetting::getApplicability(
												const Notification::ChangeQualifier
												) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	return(Applicability::CHANGEABLE);
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  The purpose of this method is to return the setting's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method returns a constant parameter.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	TrendCursorPositionSetting::getDefaultValue(void) const
{
	CALL_TRACE("getDefaultValue()");

	BoundedValue  defaultValue;

	defaultValue.value     = 0.0f;
	defaultValue.precision = ONES;

	return(defaultValue); 
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	TrendCursorPositionSetting::SoftFault(const SoftFaultID  softFaultID,
										  const Uint32       lineNumber,
										  const char*        pFileName,
										  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							TREND_CURSOR_POSITION_SETTING, lineNumber,
							pFileName, pPredicate);
}
