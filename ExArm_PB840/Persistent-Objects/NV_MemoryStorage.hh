

#ifndef NV_MEMORY_STORAGE_HH
#define NV_MEMORY_STORAGE_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2014, Covidien
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NV_MemoryStorage - Nonvolatile memory storage
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a a mechanism to store data structures that are
//  intended to be non-volatile (NV). In the original PB840, there were
//  battery backed up static RAM (NOVRAM) and Flash that were used to
//  store data during power off.
//
//  In the 880, there is no NOVRAM and the Flash is the WinCE file system.
//
//  All NV structures will be stored in the WinCE file system.
//
//  Objects of this class are instantiated in the NV_MemoryStorageThread's
//  Initialize method. This method is called during system initialization,
//  prior to starting the various threads. Once initialization is complete,
//  the NV_MemoryStorageThread provides execution context for these objects
//  to update
//---------------------------------------------------------------------
//@ End-Preamble
//

#include "sigma.hh"
#include <string>

class NV_MemoryStorage
{

    friend class NV_MemoryStorageThread;

    public:
		NV_MemoryStorage(void **addressOfAccessPointer, Uint32 memorySize, const std::string& fileName, bool dynamicUpdate);
        ~NV_MemoryStorage();



    protected:
        Uint8	*buffer_;
        Uint8	*bufferCopy_;
        bool    dynamicUpdate_;
        Uint32  memorySize_;

		std::string primaryDataFileName_;
		std::string primaryCrcFileName_;
		std::string backupDataFileName_;
        std::string backupCrcFileName_;

        void CheckForStorageUpdate_();        // Called periodically by NV_MemoryStorageThread
        bool Load_();
		bool LoadFromFile_(const std::string& dataFileName, const std::string& crcFileName);

        // Returns true is valid store occurred,
        // stores from a specific location
        bool Store_();
        bool StoreToFile_(const std::string& dataFileName, const std::string& crcFileName);

		void CreateFileNames_(const std::string& _FileName);

		//Utility method to read a data buffer to a file, given name and data buffer
		bool LoadDataFromFile_(const std::string& fileName, void* buffer, Uint32 bufferSize);
		//Utility method to write a data buffer to a file, given name and data buffer
		bool WriteDataToFile_(const std::string& fileName, const void* buffer, Uint32 bufferSize);

    private:
        NV_MemoryStorage();                         // Don't want this called!


};




#endif

