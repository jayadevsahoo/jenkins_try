#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================

#include "BatteryInfoBroadcaster.hh"
#include "Task.hh"
#include "PortIds.hh"
#include "ExternalBattery.hh"
#include "MiscSensorRefs.hh"
#include "VentObjectRefs.hh"
#include "LinearSensor.hh"
#include "BatteryInfo.hh"
#include "SystemBattery.hh"

static const char*  BATTERYINFO_BROADCAST_ADDR = "192.168.0.255";

UdpCommunication BatteryInfoBroadcaster::UdpComm_;

BatteryInfoBroadcaster::BatteryInfoBroadcaster(void)
{
}

BatteryInfoBroadcaster::~BatteryInfoBroadcaster(void)
{
}

// Read data from battery. If not able to read data from EBM board
// consider as Battery Not Installed. 
void BatteryInfoBroadcaster::ReadEBMData_(
                                    ExternalBatteryInfo & externalBatteryInfo)
{
    memset( &externalBatteryInfo, 0, sizeof(externalBatteryInfo) );

    ExternalBattery* pExternalBattery = ExternalBattery::getInstance();
    externalBatteryInfo.isInstalled = pExternalBattery->isInstalled();
    
    bool readOK = true;

    // If EBM is not installed to Vent, we'll ignore the other data
    // reading. 
    if ( externalBatteryInfo.isInstalled )
    {
        // Read State Of Charge
        readOK = pExternalBattery->readStateOfCharge( 
                                            externalBatteryInfo.stateOfCharge );
        if ( !readOK )
        {
            externalBatteryInfo.isInstalled    = false;
            return;
        }

        // Read Remaining Capacity
        readOK = pExternalBattery->readRemainingCapacity( 
                                        externalBatteryInfo.remainingCapacity );
        if ( !readOK )
        {
            externalBatteryInfo.isInstalled    = false;
            return;
        }

        // Read Full Charge Capacity
        readOK = pExternalBattery->readFullChargeCapacity( 
                                        externalBatteryInfo.fullChargeCapacity );
        if ( !readOK )
        {
            externalBatteryInfo.isInstalled    = false;
            return;
        }

        // Read Average Current
        readOK = pExternalBattery->readAverageCurrent( 
                                            externalBatteryInfo.averageCurrent );
        if ( !readOK )
        {
            externalBatteryInfo.isInstalled    = false;
            return;
        }

        // Read Voltage
        readOK = pExternalBattery->readVoltage( externalBatteryInfo.voltage );
        if ( !readOK )
        {
            externalBatteryInfo.isInstalled    = false;
            return;
        }

        // Read Temperature
        readOK = pExternalBattery->readTemperature( 
                                            externalBatteryInfo.temperature );
        if ( !readOK )
        {
            externalBatteryInfo.isInstalled    = false;
            return;
        }

        // Read Instantaneous Current
        pExternalBattery->readInstantaneousCurrent( 
                                    externalBatteryInfo.instantaneousCurrent );
        if ( !readOK )
        {
            externalBatteryInfo.isInstalled    = false;
            return;
        }

        // Check if EBM is the power source
        externalBatteryInfo.isPowerSource = pExternalBattery->isPowerSource();
        
        // Check if EBM is under charging
        externalBatteryInfo.isCharging    = pExternalBattery->isCharging();
    }
}

void BatteryInfoBroadcaster::ReadSystemBatteryData_(
                                        SystemBatteryInfo & systemBatteryInfo)
{
    systemBatteryInfo.voltage = RSystemBatteryVoltage.getValue(); 
    systemBatteryInfo.status = RSystemBattery.getBpsStatus();
    systemBatteryInfo.stateOfCharge = RSystemBattery.getStateOfCharge();
}

void BatteryInfoBroadcaster::InitializeBroadcast_()
{
    UdpComm_.stop();
    UdpComm_.setup(
        UdpCommunication::UDP_SEND, 0, BATTERYINFO_BROADCAST_ADDR, SWAT_PC_UDP_PORT);
}

void BatteryInfoBroadcaster::BroadcastTask()
{
    // Initialize the broadcast
    InitializeBroadcast_();

    BatteryInfo     batteryInfo;
    DWORD           bytesWritten = 0;
    bool            forever = true;

    while ( forever )
    {
        ReadEBMData_( batteryInfo.ebi );
        ReadSystemBatteryData_( batteryInfo.sbi );

         // Set battery information magic number
        batteryInfo.magicNumber  = BATTERYINFO_MAGIC_NUMBER;
        
        batteryInfo.convHtoN();

        bool writeSuccess =  UdpComm_.writeData((Uint8*)&batteryInfo,
                                        sizeof(batteryInfo), &bytesWritten);
        
        // Not able to write data, re-initiate UDP Communication
        if ( !writeSuccess )
        {
            InitializeBroadcast_();
        }
        
        Task::Delay( 0, 500 );
    }
}
