#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OffKeyPanel - Generic OffKey handler class which allows
// the viewing of the progress of the key events.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains textual fields, event id associated with
// this offKey panel, a pointer to a callBack function to handle cancel
// request, and a pointer to the sub-screen that is associated with this
// panel.
// activatePanel() 
// showPanel()
// deactivatePanel()
// resetTitle()
// registerCallbackPtr()
// buttonDownHappened()
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/OffKeyPanel.ccv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: rrush    Date: 21-Jan-2009   SCR Number:  6435
//  Project:  840S
//  Description:
//		Corrected order of setText and positionInContainer calls for
//		panelTitle_.
//  
//  Revision: 005  By: hhd	Date: 27-Apr-2000   DCS Number:  5657, 5629
//  Project:  Baseline
//  Description:
//		Modified to allow the Alarm Silence progress bar and button to be displayed
//	during Settings Locked out event.
//
//  Revision: 004  By: sah	Date: 10-Apr-2000   DCS Number:  5702
//  Project:  NeoMode
//  Description:
//      As part of the translation effort, the CANCEL button dimensions
//      had to be changed to allow for ALL languages to fit.
//
//  Revision: 003  By: sah	Date: 10-Apr-2000   DCS Number:  5683
//  Project:  NeoMode
//  Description:
//      As part of this DCS, removed unneeded parameters from 'activatePanel()',
//      to go along with the removal of the corresponding unneeded strings.
//
//  Revision: 002  By:  hhd	   Date:  08-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//		Modified to provide additional off-screen key management, whereby 100% O2
//		and Alarm Silence can be cancelled.
//
//  Revision: 001  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//		(hhd) 29-Nov-99	Modified to fix bugs.
//=====================================================================

#include "OffKeyPanel.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "OffKeysSubScreen.hh"
#include "GuiApp.hh"
//@ End-Usage

//=====================================================================
//
//		Static Data...
//
//=====================================================================

// Initialize static constants.
static const Int32 BUTTON_HEIGHT_ = 32;
static const Int32 BUTTON_WIDTH_ = 80;
static const Int32 PANEL_TITLE_Y_ = 8;

static const Int32 PANEL_MSG_Y_ = PANEL_TITLE_Y_ + 21;
static const Int32 PANEL_COMMAND_Y_ = PANEL_MSG_Y_ + 21;
static const Int32 BUTTON_X_ = 0;
static const Int32 BUTTON_Y_ = PANEL_COMMAND_Y_ + 21;
static const Int32 BUTTON_BORDER_ = 3;

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OffKeyPanel()  [Constructor]
//
//@ Interface-Description
// Constructs a OffKeyPanel.  Passed the following parameters:
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize all data members.  Setup the selectRowButtons_[] array to
// point to each select button.  Register for callbacks to each button.
// Setup the colors of drawables that need it.
//---------------------------------------------------------------------
//@ PreCondition
// The pTarget parameter must be non-null.  The number of rows and columns
// must be within the maximum limits.  The row height must be 2 or greater.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OffKeyPanel::OffKeyPanel(Uint32 xOrg, Uint32 yOrg, Uint32 width, Uint32 height,
						 Uint16 numButtons, StringId  panelTitle, SubScreen *pFocusSubScreen,
						 EventData::EventId eventId,
						 UserEventCallbackPtr pEventCallBack) :
	pFocusSubScreen_(pFocusSubScreen),
	pUserEventCallBack_(pEventCallBack),
	eventId_(eventId),
	panelMsg_(),
	cancelButton_(BUTTON_X_, BUTTON_Y_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_,
			Button::LIGHT, BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER,
			MiscStrs::CANCEL_MANEUVER_BUTTON_TITLE),
	panelTitle_(panelTitle)
{
	CALL_TRACE("OffKeyPanel::OffKeyPanel(void)");

	//
	// Size and position the panel.
	//
	setWidth(width);
	setHeight(height);
	setX(xOrg);
	setY(yOrg);
	setFillColor(Colors::MEDIUM_BLUE);
	
	addDrawable(&panelTitle_);
	panelTitle_.positionInContainer(GRAVITY_CENTER);
	panelTitle_.setY(PANEL_TITLE_Y_);
	panelTitle_.setColor(Colors::WHITE);
	panelTitle_.setShow(FALSE);

	addDrawable(&panelMsg_);
	panelMsg_.positionInContainer(GRAVITY_CENTER);
	panelMsg_.setY(PANEL_MSG_Y_);
	panelMsg_.setShow(FALSE);
	panelMsg_.setColor(Colors::WHITE);

	addDrawable(&cancelButton_);
	cancelButton_.positionInContainer(GRAVITY_CENTER);
	cancelButton_.setShow(FALSE);

	// Register for callbacks from cancel button
	cancelButton_.setButtonCallback(this);

	/*****************************************************/
	// The following is designed for future maneuver usage.
	// The user should derived a maneuverOffKeyPanel from this class
	// and add the following data members to this derived class.
	/*****************************************************/
	// Setup the selectRowButtons_ array

	//selectButtons_[0]  = &select0Button_;
	//SAFE_CLASS_ASSERTION(0 == MAX_BUTTONS_ - 1);

	// Make this object be a button target of all displayable row select buttons
	//for (int idx = 0; idx < numButtons; idx++)
	//{
	//	addDrawable(selectButtons_[idx]);
	//	selectButtons_[idx]->setButtonCallback(this);
	//	selectButtons_[idx]->setShow(FALSE);
	//}
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OffKeyPanel()  [Destructor]
//
//@ Interface-Description
// OffKeyPanel destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OffKeyPanel::~OffKeyPanel(void)
{
	CALL_TRACE("OffKeyPanel::~OffKeyPanel(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activatePanel
//
//@ Interface-Description
// Called by OffKeys subScreen to display the offKey panel info.
//---------------------------------------------------------------------
//@ Implementation-Description
// Update the panelTitle_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
OffKeyPanel::activatePanel(StringId msg, Boolean inManeuver, Boolean displayCancelButton, 
							Boolean redraw)
{
	CALL_TRACE("OffKeyPanel::activatePanel(StringId msg, Boolean inManeuver, Boolean displayCancelButton, Boolean redraw)");

	if (displayCancelButton)
	{										// $[TI1.2.1]
		// Make sure all buttons are in the up state
		cancelButton_.setToUp();
	}										// $[TI1.2.2]
	cancelButton_.setShow(displayCancelButton && redraw);

	// Display the title/msg depending on which type of activity is occuring.
	if (inManeuver)
	{										 // $[TI1.2.3]
		panelMsg_.setText(msg);
		panelMsg_.positionInContainer(GRAVITY_CENTER);
		panelMsg_.setShow(redraw);
	}
	else
	{										  // $[TI1.2.4]
		panelTitle_.setText(msg);
	}

	panelTitle_.positionInContainer(GRAVITY_CENTER);
	panelTitle_.setShow(redraw);

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: showPanel
//
//@ Interface-Description
// Called by OffKeys subScreen and AlarmOffKeyPanel to display the offKey panel info.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the Show flags of panel cancel button, message and title to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//
//=====================================================================

void
OffKeyPanel::showPanel(Boolean redrawMode, Boolean displayCancelButton)
{
	CALL_TRACE("OffKeyPanel::showPanel(Boolean redrawMode,"
									"Boolean displayCancelButton)");

   	cancelButton_.setShow(redrawMode && displayCancelButton);
	panelMsg_.setShow(redrawMode);
	panelTitle_.setShow(redrawMode);
	// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivatePanel
//
//@ Interface-Description
// Called by OffKeys subScreen to deactivate the offKey panel info.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize the panelTitle_ and hide it.
// $[NE01012] Automatic dismissal of Offscreen Key Status SubScreen... 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OffKeyPanel::deactivatePanel(void)
{
	CALL_TRACE("OffKeyPanel::deactivatePanel(void)");

	panelMsg_.setShow(FALSE);
	panelTitle_.setShow(FALSE);
	cancelButton_.setToUp();
	cancelButton_.setShow(FALSE);
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetTitle
//
//@ Interface-Description
// Allowing us to set the title message.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the titleMsg for the panelTitle_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OffKeyPanel::resetTitle(StringId titleMsg)
{
	CALL_TRACE("OffKeyPanel::resetTitle(StringId titleMsg)");

	panelTitle_.setText(titleMsg);
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerCallbackPtr
//
//@ Interface-Description
// Allows setting up the callBack routine for handling cancel
// request.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the pUserEventCallBack_ pointer as callBack method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OffKeyPanel::registerCallbackPtr(UserEventCallbackPtr pEventCallback, StringId panelMsg)
{
	CALL_TRACE("OffKeyPanel::registerCallbackPtr(UserEventCallbackPtr pEventCallback, StringId panelMsg)");

	CLASS_ASSERTION(pEventCallback != NULL);
	
	panelTitle_.setText(panelMsg);

	pUserEventCallBack_ = pEventCallback;
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when the the buttons is pressed down.
//---------------------------------------------------------------------
//@ Implementation-Description
// If it is pressed down by the operator and it is the cancel button then
// execute the callBack routine.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OffKeyPanel::buttonDownHappened(Button *pButton, Boolean byOperatorAction)
{
	CALL_TRACE("OffKeyPanel::buttonDownHappened(Button *pButton, Boolean byOperatorAction)");


	CLASS_ASSERTION(pUserEventCallBack_ != NULL);
	
	// Only react to down events that come from the operator
	if (byOperatorAction)
	{											// $[TI1.1]
		if (pButton == (Button *) &cancelButton_)
		{										// $[TI1.1.1]
			(*pUserEventCallBack_)();


		}										// $[TI1.1.2]
	}											// $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
OffKeyPanel::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, OFFKEYPANEL,
  				lineNumber, pFileName, pPredicate);
}

