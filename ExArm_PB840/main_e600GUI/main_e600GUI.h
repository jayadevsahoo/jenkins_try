// main_e600GUI.h : main header file for the main_e600GUI application
//			
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"			

// Cmain_e600GUIApp:
// See main_e600GUI.cpp for the implementation of this class
//

class Cmain_e600GUIApp : public CWinApp
{
public:
	Cmain_e600GUIApp();
	CDC* ptrLowerCDC_; 
	CDC* ptrUpperCDC_; 
	
	void InitializeLowerViewCDC();   
	void InitializeUpperViewCDC();	 
    void GUISystemInitalization(void);

private:
    BOOL CreateFrmView(); 
// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
public:


	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();
};

extern Cmain_e600GUIApp theApp;
