// softd.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "softd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CSoftdApp

BEGIN_MESSAGE_MAP(CSoftdApp, CWinApp)
	//{{AFX_MSG_MAP(CSoftdApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSoftdApp construction

CSoftdApp::CSoftdApp()
{
	/* No Special init required */
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSoftdApp object

CSoftdApp BootPApp;

/////////////////////////////////////////////////////////////////////////////
// CSoftdApp initialization

BOOL CSoftdApp::InitInstance()
{
	if(!AfxSocketInit()){
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	return TRUE;
}

//////////////////////////////////////////////////////////////////////
void CSoftdApp::EndServerSession()
{	
	Server.EndSession(); 
}

//////////////////////////////////////////////////////////////////////
int CSoftdApp::StartServer(BootInfo *startUp)
{
	int result = 0;

	Server.SetGuiFileName(startUp->guiFileName,startUp->guiId);
	Server.SetBdFileName(startUp->bdFileName,startUp->bdId);
	Server.SetFilePath(startUp->filePath);
	Server.SetBootPCallback(startUp->pBootPCallback);
	Server.SetTftpCallback(startUp->pTftpCallback);

	if(startUp->multiHomed == 1){
		Server.SetAltAddress(startUp->altAddress);
	}

	result = Server.GoLive();

	return(result);
}

//////////////////////////////////////////////////////////////////////
int CSoftdApp::GetLastError()
{
	return(Server.GetLastError());
}

////////////////////////////////////////////////////////////////////////////
// C function interface

//////////////////////////////////////////////////////////////////////
DllExport void StartSession(BootInfo *startUp, int *results)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState( ));

	(*results) = BootPApp.StartServer(startUp);

	return;
}

//////////////////////////////////////////////////////////////////////
DllExport void EndSession(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState( ));

	BootPApp.EndServerSession();

	return;
}

//////////////////////////////////////////////////////////////////////
DllExport int GetSessionLastError(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState( ));

	return(BootPApp.GetLastError());
}

