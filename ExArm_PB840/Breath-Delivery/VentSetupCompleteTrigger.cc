#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VentSetupCompleteTrigger - Checks for patient setup.
//---------------------------------------------------------------------
//@ Interface-Description
//	This trigger is used by the SafetyPcvScheduler. The trigger will notify
//	scheduler when the patient set up has been completed during safety pcv.
//---------------------------------------------------------------------
//@ Rationale
//	This trigger notifies the active scheduler when patient
//	setup is complete.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This class contains a boolean state variable to keep track of whether
//	this trigger is enabled or not.  If the trigger is not enabled,
//	it will always return a state of false. If the patient setup is completed 
//	return true, else return false.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VentSetupCompleteTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "VentSetupCompleteTrigger.hh"

//@ Usage-Classes
#include "BreathMiscRefs.hh"

#include "PhasedInContextHandle.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VentSetupCompleteTrigger
//
//@ Interface-Description
//	Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The triggerId is set by the base class constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VentSetupCompleteTrigger::VentSetupCompleteTrigger(void)
: ModeTrigger(Trigger::VENT_SETUP_COMPLETE)
{
    CALL_TRACE("VentSetupCompleteTrigger(void)");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VentSetupCompleteTrigger() 
//
//@ Interface-Description 
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VentSetupCompleteTrigger::~VentSetupCompleteTrigger(void)
{
  CALL_TRACE("~VentSetupCompleteTrigger()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
VentSetupCompleteTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, VENTSETUPCOMPLETETRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has
//    occured, the method returns true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method checks if patient setup has been completed. If the setup is
//    completed returns true, else returns false.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
VentSetupCompleteTrigger::triggerCondition_(void)
{
    CALL_TRACE("triggerCondition_(void)");

    // TRUE: $[TI1]
    // FALSE: $[TI2]
    return (PhasedInContextHandle::AreBdSettingsAvailable());
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
