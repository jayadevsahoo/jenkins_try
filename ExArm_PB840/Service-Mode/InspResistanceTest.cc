#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  InspResistanceTest - Test inspiratory and expiratory limb
//	resistance
//---------------------------------------------------------------------
//@ Interface-Description
//	This class implements the SST circuit resistance test.
//	Besides testing inspiratory and expiratory limb resistances
//	at a single test flow, it characterizes the limb resistances over
//	a range of flows for use during ventilation.
//
//	NOTE: this test is now called the "Circuit Resistance Test".
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the SST circuit resistance test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() performs the circuit resistance test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/InspResistanceTest.ccv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012  By: rhj    Date: 14-June-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added PROX resistances.
// 
//  Revision: 011  By: rhj    Date: 06-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added prox prompts      
// 
//  Revision: 010  By: yab    Date: 17-Apr-2002     DR Number: 5739
//  Project:  VCP
//  Description:
//      Modified flow from 60lpm to 30lpm for Neo circuit in isWyeBlocked().
//
//  Revision: 009  By: syw    Date: 02-Feb-2000     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Remove reference to PatientCctTypeValue.hh
//
//  Revision: 008  By: sah    Date: 30-Jun-1999     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Incorporation of NeoMode Project requirements.
//
//  Revision: 007  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 006  By:  quf    Date: 03-Nov-1997    DR Number: DCS 2602
//       Project:  Sigma (840)
//       Description:
//			Fixed runTest() to set the resistance-valid flag TRUE for each
//			NOVRAM resistance object only when test completes without
//			any FAILUREs.
//
//  Revision: 005  By:  quf    Date: 10-Oct-1997    DR Number: DCS 2262
//       Project:  Sigma (840)
//       Description:
//			Added comment to class-level Interface-Description indicating
//			alternate names for this test.
//
//  Revision: 004  By:  quf    Date: 08-Sep-1997    DR Number: DCS 2445
//       Project:  Sigma (840)
//       Description:
//            Changed adult and ped low resistance FAILURE criteria and
//            added low resistance ALERT criteria, changed ped occlusion
//            ALERT criteria.
//
//  Revision: 003  By:  quf    Date: 05-Aug-1997    DR Number: DCS 2130
//       Project:  Sigma (840)
//       Description:
//            Moved total circuit resistance measurement here from the
//            Expiratory Filter Test (SstFilterTest.cc).  Added an
//            initial wye blocked check during test repeat only.  Added
//            testFlow_, setTestFlow_(), and isWyeBlocked_().
//            Recoded runTest(), measureInspLimbResistance_() and
//            evaluateExhLimbResistance_() for easier code maintenance
//            and unit testing.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "InspResistanceTest.hh"

//@ Usage-Classes

#include "SmConstants.hh"
#include "SmRefs.hh"
#include "SmFlowController.hh"
#include "SmExhValveController.hh"
#include "SmUtilities.hh"
#include "SmManager.hh"
#include "SstFilterTest.hh"
#include "SmGasSupply.hh"

#include "ValveRefs.hh"
#include "MainSensorRefs.hh"
#include "PressureSensor.hh"
#include "FlowSensor.hh"

#include "PhasedInContextHandle.hh"
#include "PatientCctTypeValue.hh"

#include "OsTimeStamp.hh"
#include "Task.hh"
#include "NovRamManager.hh"
#include "ProxTest.hh"

#if defined(SIGMA_DEVELOPMENT)
# include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"
# include "InspResistanceTest_UT.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage


//@ Code...


// limb occlusion FAILURE criteria, adult circuit
const Real32 ADULT_LIMB_OCCL_FAILURE =   12.5;  // cmH2O

// limb occlusion ALERT criteria, adult circuit
const Real32 ADULT_LIMB_OCCL_ALERT =      8.5;  // cmH2O

// limb low resistance FAILURE criteria, adult circuit
const Real32 ADULT_LIMB_LOW_RESISTANCE_FAILURE = 0.2;  // cmH2O

// limb low resistance ALERT criteria, adult circuit
const Real32 ADULT_LIMB_LOW_RESISTANCE_ALERT   = 0.6;  // cmH2O


// limb occlusion FAILURE criteria, ped circuit
const Real32 PED_LIMB_OCCL_FAILURE =      7.5;  // cmH2O

// limb occlusion ALERT criteria, ped circuit
const Real32 PED_LIMB_OCCL_ALERT =        5.5;  // cmH2O

// limb low resistance FAILURE criteria, ped circuit
const Real32 PED_LIMB_LOW_RESISTANCE_FAILURE = 0.2;  // cmH2O

// limb low resistance ALERT criteria, ped circuit
const Real32 PED_LIMB_LOW_RESISTANCE_ALERT   = 0.5;  // cmH2O


// limb occlusion FAILURE criteria, neo circuit
const Real32 NEO_LIMB_OCCL_FAILURE =      3.5;  // cmH2O

// limb occlusion FAILURE criteria with PROX, neo circuit
const Real32 PROX_NEO_LIMB_OCCL_FAILURE =      6.5;  // cmH2O

// limb occlusion ALERT criteria, neo circuit
const Real32 NEO_LIMB_OCCL_ALERT =        100.0 ;  // no alert for neonate... failure only

// limb low resistance FAILURE criteria, neo circuit
const Real32 NEO_LIMB_LOW_RESISTANCE_FAILURE = 0.2;  // cmH2O

// limb low resistance ALERT criteria, neo circuit
const Real32 NEO_LIMB_LOW_RESISTANCE_ALERT   = 0.0;  // no alert for neonate... failure only


// test flow for limb delta P measurement, adult circuit
const Real32 ADULT_CIRCUIT_TEST_FLOW = 60.0;  // lpm

// test flow for limb delta P measurement, ped circuit
const Real32 PED_CIRCUIT_TEST_FLOW = 30.0;  // lpm

// test flow for limb delta P measurement, neo circuit
const Real32 NEO_CIRCUIT_TEST_FLOW = 10.0;  // lpm

#ifdef SIGMA_NEO_UNIT_TEST
extern Real32 ResistancePeakFlow1 ;
extern Real32 ResistancePeakFlow2 ;
extern Boolean ResistanceFailure1 ;
extern Boolean ResistanceFailure2 ;
extern Boolean ResistanceFailure3 ;
extern Real32 InspOcclFailureDeltaP ;
extern Real32 ExhOcclFailureDeltaP ;
#endif  // SIGMA_NEO_UNIT_TEST

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: InspResistanceTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
// none
//-----------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=======================================================================
InspResistanceTest::InspResistanceTest( void)
{
	CALL_TRACE("InspResistanceTest::InspResistanceTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~InspResistanceTest()  [Destructor]
//
//@ Interface-Description
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
InspResistanceTest::~InspResistanceTest( void)
{
	CALL_TRACE("InspResistanceTest::~InspResistanceTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runTest()
//
//@ Interface-Description
//	This method performs the insp/exp limb resistance test.  No arguments,
//	no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	$[10006]
//	During test repeat only, prompt to block the wye and verify it's blocked.
//	Measure total circuit resistance slope and offset while flow is ramped
//	from 0.  Prompt to unblock the wye.  Measure inspiratory limb delta P at
//	the required test flow and check that it's in range.  Measure
//	the inspiratory limb resistance slope and offset while flow
//	is ramped from 0.  Calculate the expiratory limb resistance
//	by subtracting the insp resistance from the total circuit resistance.
//	Calculate the expiratory limb delta P at the same flow used for the
//	insp limb delta P by using the formula
//	exp limb delta P = flow^2 * exp limb slope + flow * exp limb offset,
//	then check that it's in range.  Store the insp and exp limb resistance
//	slopes and offsets for use during ventilation.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================
void
InspResistanceTest::runTest( void)
{
	RETAILMSG ( TRUE, ( L"InspResistanceTest\r\n") );

	CALL_TRACE("InspResistanceTest::runTest( void)") ;

#ifdef SM_TEST
	Int32 selection;
	printf("Select circuit type, 0 = Adult, 1 = Ped, 2 = Neo: ");
	scanf("%d", &selection);

	if (selection < 0 || selection > 2)
	{
		printf("Illegal value, default to Adult\n");
		selection = 1;
	}
	
	printf("Circuit type = %s\n", (selection == 0) ? "Adult"
				      : ((selection == 1) ? "Ped" : "Neo"));

	PhasedInContextHandle::SetDiscreteValue(SettingId::PATIENT_CCT_TYPE, 
											(PatientCctTypeValue::Id)selection);
#endif  // SM_TEST

	Boolean isNeoCircuit = FALSE;

	// get current circuit type from Settings-Validation


	const DiscreteValue  CIRCUIT_TYPE = 
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

//Neonantal values are given as initial ones to remove warnings.
	Real32  minPeakFlowAlertLimit = NEO_ALERT_MIN_PEAK_FLOW;
	Real32  minPeakFlowFailLimit = NEO_FAILURE_MIN_PEAK_FLOW;

	switch (CIRCUIT_TYPE)
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			// $[TI13]
			minPeakFlowAlertLimit = NEO_ALERT_MIN_PEAK_FLOW;
			minPeakFlowFailLimit  = NEO_FAILURE_MIN_PEAK_FLOW;
			isNeoCircuit = TRUE;
			break;
			
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			// $[TI14]
			minPeakFlowAlertLimit = PED_ALERT_MIN_PEAK_FLOW;
			minPeakFlowFailLimit  = PED_FAILURE_MIN_PEAK_FLOW;
			break;
			
		case PatientCctTypeValue::ADULT_CIRCUIT :
			// $[TI15]
			minPeakFlowAlertLimit = ADULT_ALERT_MIN_PEAK_FLOW;
			minPeakFlowFailLimit  = ADULT_FAILURE_MIN_PEAK_FLOW;
			break;
			
		default :
			// unexpected circuit type...
			AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
			break;
	}

	setTestFlow_(CIRCUIT_TYPE);

	RSmUtilities.autozeroPressureXducers();

	// Phase 0: block wye prompt and verification (ONLY for test repeat)
	Boolean operatorExit = FALSE;
	Boolean noFailures = TRUE;
	if (RSmManager.isTestRepeat())
	{
	// $[TI1]

		SmPromptId::PromptId  smPromptId1;
		if (RProxTest.isProxTestEnabled() && isNeoCircuit)
		{
			smPromptId1  = SmPromptId::PX_BLOCK_SENSOR_OUTPUT_PROMPT;
		}
		else
		{
			smPromptId1  = SmPromptId::BLOCK_WYE_PROMPT;
		}


		if (RSmUtilities.messagePrompt (smPromptId1))
		{	
		// $[TI1.1]
#ifdef SIGMA_UNIT_TEST
			if (!InspResistanceTest_UT::WyeBlocked)
#else
			if (!isWyeBlocked_())
#endif  // SIGMA_UNIT_TEST
			{
			// $[TI1.1.1]
				// wye not blocked
				RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_FAILURE,
						SmStatusId::CONDITION_12);
				noFailures = FALSE;
			}
			// $[TI1.1.2]
			// implied else
		}
		else
		{
		// $[TI1.2]
			operatorExit = TRUE;
		}
		// end else
	}
	// $[TI2]
	// implied else

	// Phase 1: total resistance characterization
	Resistance totalResistance;
	if (!operatorExit && noFailures)
	{
	// $[TI3]
		// perform total circuit resistance characterization
#ifdef SIGMA_UNIT_TEST
		totalResistance.setSlope(3.0);
		Real32 peakFlow = InspResistanceTest_UT::TotalResistancePeakFlow;
#else
#ifdef SIGMA_NEO_UNIT_TEST
		Real32 peakFlow = ResistancePeakFlow1 ;
		noFailures = ResistanceFailure1 ;
#else
		Real32 peakFlow = totalResistance.measureCircuitResistance();
		RETAILMSG ( TRUE, ( L"Peak insp flow: %d\r\n", (Uint32)peakFlow ) );

#endif  // SIGMA_NEO_UNIT_TEST
#endif  // SIGMA_UNIT_TEST

		if (peakFlow < minPeakFlowFailLimit)
		{
		// $[TI3.1]
			// Didn't reach minimum FAILURE peak flow 
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_10);
			 noFailures = FALSE;
		}
		else if (peakFlow < minPeakFlowAlertLimit)
		{
		// $[TI3.2]
			// Didn't reach minimum ALERT peak flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_11);
		}
		// $[TI3.3]
		// implied else
	}
	// $[TI4]
	// implied else

	// Phase 2: measure insp limb resistance at fixed flow
	if (!operatorExit && noFailures)
	{
	// $[TI5]

		SmPromptId::PromptId  smPromptId2;
		if (RProxTest.isProxTestEnabled() && isNeoCircuit)
		{
			smPromptId2  = SmPromptId::PX_UNBLOCK_SENSOR_OUTPUT_PROMPT;
		}
		else
		{
			smPromptId2  = SmPromptId::UNBLOCK_WYE_PROMPT;
		}

		if (RSmUtilities.messagePrompt (smPromptId2))
		{
		// $[TI5.1]
#ifdef SIGMA_UNIT_TEST
			noFailures = TRUE;
#else
			noFailures = measureInspLimbResistance_(CIRCUIT_TYPE);
#endif  // SIGMA_UNIT_TEST
		}
		else
		{
		// $[TI5.2]
			operatorExit = TRUE;
		}
		// end else
	}
	// $[TI6]
	// implied else
	
#ifdef SIGMA_NEO_UNIT_TEST
		noFailures = ResistanceFailure2 ;
#endif  // SIGMA_NEO_UNIT_TEST

	// Phase 3: insp limb resistance characterization
	Resistance inspResistance;
	if ( !operatorExit && noFailures)
	{
	// $[TI7]
		// wait for residual pressure to decay
		Task::Delay (0, 200);

		RETAILMSG (TRUE, ( L"Closing the exhalation valve\r\n" ) );
	   	RSmExhValveController.close();

#ifdef SIGMA_UNIT_TEST
		inspResistance.setSlope(1.0);
		Real32 peakFlow = InspResistanceTest_UT::InspResistancePeakFlow;
#else
#ifdef SIGMA_NEO_UNIT_TEST
		Real32 peakFlow = ResistancePeakFlow2 ;
		noFailures = ResistanceFailure3 ;
#else
		Real32 peakFlow = inspResistance.measureCircuitResistance();

		RETAILMSG ( TRUE, ( L"Peak insp flow: %d\r\n", (Uint32)peakFlow ) );
#endif // SIGMA_NEO_UNIT_TEST
#endif  // SIGMA_UNIT_TEST

	   	RSmExhValveController.open();

		if (peakFlow < minPeakFlowFailLimit)
		{			
		// $[TI7.1]
			// Didn't reach minimum FAILURE peak flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_4);
			noFailures = FALSE;
		}
		else if (peakFlow < minPeakFlowAlertLimit)
		{			
		// $[TI7.2]
			// Didn't reach minimum ALERT peak flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_7);
		}
		// $[TI7.3]
		// implied else

	}
	else
	{
		RETAILMSG ( TRUE, ( L"Resistance failed\r\n" ) );
	}
	// $[TI8]
	// implied else

	// Phase 4: exh limb resistance calculation
	if (!operatorExit && noFailures)
	{
	// $[TI9]
		// calculate exh resistance by subtracting
		// insp resistance from total resistance
		Resistance exhResistance = totalResistance - inspResistance;

		// check if exh limb resistance is in acceptable range
#ifdef SIGMA_UNIT_TEST
		if (InspResistanceTest_UT::ExhLimbResistanceOk)
#else
		if (evaluateExhLimbResistance_(exhResistance, CIRCUIT_TYPE))
#endif
		{
		// $[TI9.1]
			// No FAILUREs occurred: update NOVRAM resistance objects

			// set the resistance-valid flags TRUE
			inspResistance.setResistanceValid(TRUE);
			exhResistance.setResistanceValid(TRUE);

			// store the updated insp and exh resistance objects
			NovRamManager::UpdateInspResistance(inspResistance);
			NovRamManager::UpdateExhResistance(exhResistance);

			RETAILMSG ( TRUE, ( L"Resistance passed\r\n" ) );
		}
		else
		{
			RETAILMSG ( TRUE, ( L"Resistance failed\r\n" ) );
		}
		// $[TI9.2]
		// implied else

#ifdef SIGMA_DEBUG
	printf("Insp resistance slope  = %f\n", inspResistance.getSlope());
	printf("Insp resistance offset = %f\n", inspResistance.getOffset());
	printf("Exh resistance slope   = %f\n", exhResistance.getSlope());
	printf("Exh resistance offset  = %f\n", exhResistance.getOffset());
#endif  // SIGMA_DEBUG

	}
	// $[TI10]
	// implied else

	if (operatorExit)
	{
	// $[TI11]
		RSmManager.sendOperatorExitToServiceData();
	}
	// $[TI12]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
InspResistanceTest::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("InspResistanceTest::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, INSPRESISTANCETEST,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: isWyeBlocked_()
//
//@ Interface-Description
//	This method takes no arguments and returns a Boolean indicating if
//	patient circuit wye is detected to be blocked.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Start a flow and allow a fixed time for pressure to reach the target
//	pressure, indicating blocked wye.  If pressure is reached within
//	the timeout, return TRUE, else return FALSE.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================
Boolean
InspResistanceTest::isWyeBlocked_(void)
{
	CALL_TRACE("InspResistanceTest::isWyeBlocked_(void)");

	const Uint32 TARGET_PRESSURE = 100;  // cmH2O
	const Real32 FLOW = 60.0;  // lpm
	// max pressurize time, 12 ml/cm max compliance and 60 lpm flow:
	// 100 cmH2O * 12 ml/cm * (1/60 lpm) * 60 sec/min = 1200 msec
	// use 2000 msec to allow sufficient margin
	const Real32 FLOW_NEO = 30.0;  // lpm
	// max pressurize time, 12 ml/cm max compliance and 30 lpm flow:
	// 100 cmH2O * 3 ml/cm * (1/30 lpm) * 60 sec/min = 600 msec
	// use 2000 msec to allow sufficient margin and for consistancy 
	// with adult and pediatric
	const Uint32 MAX_PRESSURIZE_TIME = 2000;  // msec

	// close exh valve 
	RSmExhValveController.close();

	// get current circuit type from Settings-Validation
	const DiscreteValue  CIRCUIT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

    if(CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
	{
        //start 30lpm flow to build pressure
	    RSmFlowController.establishFlow( FLOW_NEO, DELIVERY, EITHER);
	}
	else
    { 
        //start 60lpm flow to build pressure
        RSmFlowController.establishFlow( FLOW, DELIVERY, EITHER);
	}

	// wait for target pressure to be reached or until timeout
	OsTimeStamp timer;
	Uint32 currentTime = 0;
	Real32 expPressure = 0.0;
	while ( expPressure < TARGET_PRESSURE &&
			currentTime < MAX_PRESSURIZE_TIME )
	{
	// $[TI1]
		Task::Delay( 0, 10);
		expPressure = RExhPressureSensor.getValue();
		currentTime = timer.getPassedTime();
	}

	// stop flow and open exh valve
	RSmFlowController.stopFlowController();
	RSmExhValveController.open();

	// allow time for pressure and flow to decay
	Task::Delay(1);

	// if timeout was reached, then wye is not blocked
	Boolean wyeIsBlocked;
	if (currentTime >= MAX_PRESSURIZE_TIME)
	{
	// $[TI2]
		wyeIsBlocked = FALSE;
	}
	else
	{
	// $[TI3]
		wyeIsBlocked = TRUE;
	}

	return( wyeIsBlocked);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: measureInspLimbResistance_()
//
//@ Interface-Description
//	This method measures the insp limb delta P for a given test flow.
//	Takes circuit type as argument and returns a Boolean indicating an
//	acceptable insp limb delta P was measured.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Determine the failure/alert criteria based on circuit type.
//	Establish the desired test flow then measure the inspiratory pressure.
//	If stable test flow was not reached generate FAILURE,
//	else measure the insp pressure and generate insp limb delta P FAILURE
//	ALERT if appropriate.
//	If FAILURE was generated, return FALSE, else return TRUE.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================
Boolean
InspResistanceTest::measureInspLimbResistance_(DiscreteValue circuitType)
{
	CALL_TRACE("InspResistanceTest::measureInspLimbResistance_( \
											DiscreteValue circuitType )");
	
	Boolean noFailures = TRUE;
	
	//Neonantal values are given as initial ones to remove warnings.

	Real32 inspOcclFailureDeltaP = NEO_LIMB_OCCL_FAILURE;
	Real32 inspOcclAlertDeltaP = NEO_LIMB_OCCL_ALERT;
	Real32 inspLowFailureDeltaP = NEO_LIMB_LOW_RESISTANCE_FAILURE;
	Real32 inspLowAlertDeltaP = NEO_LIMB_LOW_RESISTANCE_ALERT;

	switch (circuitType)
	{
	case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		// $[TI2]
		inspOcclFailureDeltaP = PED_LIMB_OCCL_FAILURE;
		inspOcclAlertDeltaP   = PED_LIMB_OCCL_ALERT;
		inspLowFailureDeltaP  = PED_LIMB_LOW_RESISTANCE_FAILURE;
		inspLowAlertDeltaP    = PED_LIMB_LOW_RESISTANCE_ALERT;
		break;
	case PatientCctTypeValue::ADULT_CIRCUIT :
		// $[TI1]
		inspOcclFailureDeltaP = ADULT_LIMB_OCCL_FAILURE;
		inspOcclAlertDeltaP   = ADULT_LIMB_OCCL_ALERT;
		inspLowFailureDeltaP  = ADULT_LIMB_LOW_RESISTANCE_FAILURE;
		inspLowAlertDeltaP    = ADULT_LIMB_LOW_RESISTANCE_ALERT;
		break;
	case PatientCctTypeValue::NEONATAL_CIRCUIT :
		// $[TI5]
		if (RProxTest.isProxTestEnabled())
		{
			inspOcclFailureDeltaP = PROX_NEO_LIMB_OCCL_FAILURE;
		}
		else
		{
			inspOcclFailureDeltaP = NEO_LIMB_OCCL_FAILURE;
		}

		inspOcclAlertDeltaP   = NEO_LIMB_OCCL_ALERT;
		inspLowFailureDeltaP  = NEO_LIMB_LOW_RESISTANCE_FAILURE;
		inspLowAlertDeltaP    = NEO_LIMB_LOW_RESISTANCE_ALERT;
		break;
	default :
		// unexpected circuit type...
		AUX_CLASS_ASSERTION_FAILURE(circuitType);
		break;
	}

#ifdef SIGMA_UNIT_TEST
	InspResistanceTest_UT::InspOcclFailureDeltaP = inspOcclFailureDeltaP;
#endif  // SIGMA_UNIT_TEST

#ifdef SIGMA_NEO_UNIT_TEST
	InspOcclFailureDeltaP = inspOcclFailureDeltaP ;
#endif  // SIGMA_NEO_UNIT_TEST

   	RSmExhValveController.close();
    RSmFlowController.establishFlow( testFlow_, DELIVERY, EITHER);
	if (RSmFlowController.waitForStableFlow())
	{
	// $[TI3]
		// measure insp limb delta P at the required test flow
		RSmManager.signalTestPoint();
		Real32 inspPressure = RInspPressureSensor.getValue();
		RSmManager.sendTestPointToServiceData( inspPressure);

		if (inspPressure > inspOcclFailureDeltaP)
		{
			wchar_t msg[1024];
			memset( msg, 0, 1024 );
			swprintf(msg, L"Insp pressure: %f, inspOcclFailureDeltaP: %f\r\n", 
				inspPressure, inspOcclAlertDeltaP);
			RETAILMSG( TRUE, (msg) );

		// $[TI3.1]
			// insp limb occlusion FAILURE
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_2);
			noFailures = FALSE;
		}
		else if (inspPressure < inspLowFailureDeltaP)
		{
		// $[TI3.2]
			// insp limb resistance low FAILURE
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_8);
			noFailures = FALSE;
		}
		else if (inspPressure > inspOcclAlertDeltaP)
		{
		// $[TI3.3]
			// insp limb occlusion ALERT
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_3);
		}
		else if (inspPressure < inspLowAlertDeltaP)
		{
		// $[TI3.5]
			// insp limb resistance low ALERT
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_13);
		}
		// $[TI3.4]
		// implied else
	}
	else
	{
	// $[TI4]
		// can't establish flow
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_1);
		noFailures = FALSE;
	}
	// end else

	RSmExhValveController.open();
	RSmFlowController.stopFlowController();

	return( noFailures);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: evaluateExhLimbResistance_()
//
//@ Interface-Description
//	This method is used to check if the expiratory limb delta P is
//	within range.  Two arguments: Resistance object reference and
//	circuit type.  Returns a Boolean indicating an acceptable exh limb
//	delta P was calculated.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Compare the expiratory delta P to the appropriate adult or pediatric
//	bounds.  Generate a FAILURE or ALERT as appropriate, then return
//	FALSE if a FAILURE occurred, return TRUE otherwise.
//-----------------------------------------------------------------------
//@ PreCondition
//	testFlow_ == ADULT_CIRCUIT_TEST_FLOW or PED_CIRCUIT_TEST_FLOW
//		or NEO_CIRCUIT_TEST_FLOW
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================
Boolean
InspResistanceTest::evaluateExhLimbResistance_( Resistance& exhResistance,
											DiscreteValue circuitType)
{
	CALL_TRACE("InspResistanceTest::evaluateExhLimbResistance_( \
			Resistance& exhResistance, DiscreteValue circuitType)");

	CLASS_PRE_CONDITION( testFlow_ == ADULT_CIRCUIT_TEST_FLOW ||
							testFlow_ == PED_CIRCUIT_TEST_FLOW ||
							testFlow_ == NEO_CIRCUIT_TEST_FLOW );

	//Neonantal values are given as initial ones to remove warnings.

	Real32 expOcclFailureDeltaP = NEO_LIMB_OCCL_FAILURE;
	Real32 expOcclAlertDeltaP = NEO_LIMB_OCCL_ALERT;
	Real32 expLowFailureDeltaP = NEO_LIMB_LOW_RESISTANCE_FAILURE;
	Real32 expLowAlertDeltaP = NEO_LIMB_LOW_RESISTANCE_ALERT;

	switch (circuitType)
	{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			// $[TI2]
			expOcclFailureDeltaP = PED_LIMB_OCCL_FAILURE;
			expOcclAlertDeltaP   = PED_LIMB_OCCL_ALERT;
			expLowFailureDeltaP  = PED_LIMB_LOW_RESISTANCE_FAILURE;
			expLowAlertDeltaP    = PED_LIMB_LOW_RESISTANCE_ALERT;
			break;
			
		case PatientCctTypeValue::ADULT_CIRCUIT :
			// $[TI1]
			expOcclFailureDeltaP = ADULT_LIMB_OCCL_FAILURE;
			expOcclAlertDeltaP   = ADULT_LIMB_OCCL_ALERT;
			expLowFailureDeltaP  = ADULT_LIMB_LOW_RESISTANCE_FAILURE;
			expLowAlertDeltaP    = ADULT_LIMB_LOW_RESISTANCE_ALERT;
			break;
			
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			// $[TI6]
			expOcclFailureDeltaP = NEO_LIMB_OCCL_FAILURE;
			expOcclAlertDeltaP   = NEO_LIMB_OCCL_ALERT;
			expLowFailureDeltaP  = NEO_LIMB_LOW_RESISTANCE_FAILURE;
			expLowAlertDeltaP    = NEO_LIMB_LOW_RESISTANCE_ALERT;
			break;
			
		default :
			// unexpected circuit type...
			AUX_CLASS_ASSERTION_FAILURE(circuitType);
			break;
	}

	Boolean noFailures;

	Real32 slope = exhResistance.getSlope();
	Real32 offset = exhResistance.getOffset();

	// calculate exp limb delta P at the same test flow
	// used for insp limb delta P measurement
	Real32 exhLimbDeltaP = slope * testFlow_ * testFlow_ +
										offset * testFlow_;

	RSmManager.sendTestPointToServiceData( exhLimbDeltaP);

#ifdef SIGMA_UNIT_TEST
	InspResistanceTest_UT::ExhOcclFailureDeltaP = expOcclFailureDeltaP;
#endif  // SIGMA_UNIT_TEST

#ifdef SIGMA_NEO_UNIT_TEST
	ExhOcclFailureDeltaP = expOcclFailureDeltaP;
#endif  // SIGMA_NEO_UNIT_TEST

	if (exhLimbDeltaP > expOcclFailureDeltaP)
	{
	// $[TI3]
		// Exp limb occlusion FAILURE
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_5);
		noFailures = FALSE;
	}
	else if (exhLimbDeltaP < expLowFailureDeltaP)
	{
	// $[TI5]
		// Exp limb low resistance FAILURE
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_9);
		noFailures = FALSE;
	}
	else
	{
	// $[TI4]
		// Failures not detected, check for Alerts
		noFailures = TRUE;

		if (exhLimbDeltaP > expOcclAlertDeltaP)
		{
		// $[TI4.1]
			// Exp limb occlusion ALERT
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_6);
		}
		else if (exhLimbDeltaP < expLowAlertDeltaP)
		{
		// $[TI4.3]
			// Exp limb low resistance ALERT
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_14);
		}
		// $[TI4.2]
		// implied else
	}
	// end else

	return( noFailures);
}



// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: setTestFlow_()
//
//@ Interface-Description
// This method sets the test flow used to check insp/exp limb delta P.
// Takes circuit type setting as argument, no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Set test flow based on circuit type setting.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================
void
InspResistanceTest::setTestFlow_(DiscreteValue circuitType)
{
	CALL_TRACE("InspResistanceTest::setTestFlow_(DiscreteValue circuitType)");

	switch (circuitType)
	{
	case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
	// $[TI2]
		testFlow_ = PED_CIRCUIT_TEST_FLOW;
		break;

	case PatientCctTypeValue::ADULT_CIRCUIT :
	// $[TI1]
		testFlow_ = ADULT_CIRCUIT_TEST_FLOW;
		break;

	case PatientCctTypeValue::NEONATAL_CIRCUIT :
	// $[TI3]
		testFlow_ = NEO_CIRCUIT_TEST_FLOW;
		break;

	default :
		// unexpected circuit type value...
		AUX_CLASS_ASSERTION_FAILURE(circuitType);
		break;
	}
}
