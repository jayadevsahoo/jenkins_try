// END_OF_BREATH_DATA_set.h : Declaration of the C_END_OF_BREATH_DATA_set

#pragma once

// code generated on Thursday, December 10, 2009, 4:18 PM

class C_END_OF_BREATH_DATA_set : public CRecordset
{
public:
	C_END_OF_BREATH_DATA_set(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(C_END_OF_BREATH_DATA_set)

// Field/Param Data

// The string types below (if present) reflect the actual data type of the
// database field - CStringA for ANSI datatypes and CStringW for Unicode
// datatypes. This is to prevent the ODBC driver from performing potentially
// unnecessary conversions.  If you wish, you may change these members to
// CString types and the ODBC driver will perform all necessary conversions.
// (Note: You must use an ODBC driver version that is version 3.5 or greater
// to support both Unicode and these conversions).

	LONGLONG	m_event_sequence_id;
	CStringA	m_breathType;
	float	m_exhaledTidalVolume;
	float	m_breathDuration;
	float	m_endExpiratoryPressure;
	float	m_ieRatio;
	float	m_mandFraction;
	float	m_dynamicCompliance;
	float	m_dynamicResistance;
	CStringA	m_phaseType;
	CStringA	m_previousBreathType;
	CStringA	m_breathTypeDisplay;
	float	m_spontInspTime;
	float	m_spontTiTtotRatio;
	float	m_peakExpiratoryFlow;
	float	m_endExpiratoryFlow;

// Overrides
	// Wizard generated virtual function overrides
	public:
	virtual CString GetDefaultConnect();	// Default connection string

	virtual CString GetDefaultSQL(); 	// default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX support

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};


