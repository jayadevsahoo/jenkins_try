#include "stdafx.h"

#include "NetworkUtest.hh"

//ONLY FOR UNIT TESTING, ON BD SIDE..
#if defined(RUN_NETWORK_UNIT_TEST) && defined (SIGMA_BD_CPU)
#pragma once
#include "ComputedInspPauseDataPacket.hh"
#include "PavBreathDataPacket.hh"
#include "PavPlateauDataPacket.hh"
#include "PavStartupDataPacket.hh"
#include "AveragedBDPacket.hh"
#include "WaveformData.hh"
#include "PauseTypes.hh"
//#include "BreathDataPacket.hh"

static Boolean isLittleEndian = TRUE;	//transmitter CPU (BD in this case) endianness

void NetworkUtest::runEndiannessUtest()
{
	if(FALSE == testAveragedBreathData())
		DEBUGMSG(TRUE, (_T("\n testAveragedBreathData failed --- \n")));
	else
		DEBUGMSG(TRUE, (_T("\n testAveragedBreathData PASS +++ \n")));

	if(FALSE == testEndBreathData())
		DEBUGMSG(TRUE, (_T("\n testEndBreathData failed --- \n")));
	else
		DEBUGMSG(TRUE, (_T("\n testEndBreathData PASS +++ \n")));

	if(FALSE == testWaveformData())
		DEBUGMSG(TRUE, (_T("\n testWaveformData failed --- \n")));
	else
		DEBUGMSG(TRUE, (_T("\n testWaveformData PASS +++ \n")));

	if(FALSE == testBiLevelMandData())
		DEBUGMSG(TRUE, (_T("\n testBiLevelMandData failed --- \n")));
	else
		DEBUGMSG(TRUE, (_T("\n testBiLevelMandData PASS +++ \n")));

	if(FALSE == testBiLevelMandExhData())
		DEBUGMSG(TRUE, (_T("\n testBiLevelMandExhData failed --- \n")));
	else
		DEBUGMSG(TRUE, (_T("\n testBiLevelMandExhData PASS +++ \n")));

	if(FALSE == testComputedInspPauseData())
		DEBUGMSG(TRUE, (_T("\n testComputedInspPauseData failed --- \n")));
	else
		DEBUGMSG(TRUE, (_T("\n testComputedInspPauseData PASS +++ \n")));

	if(FALSE == testPavBreathData())
		DEBUGMSG(TRUE, (_T("\n testPavBreathData failed --- \n")));
	else
		DEBUGMSG(TRUE, (_T("\n testPavBreathData PASS +++ \n")));

	if(FALSE == testPavPlateauData())
		DEBUGMSG(TRUE, (_T("\n testPavPlateauData failed --- \n")));
	else
		DEBUGMSG(TRUE, (_T("\n testPavPlateauData PASS +++ \n")));

	if(FALSE == testPavStartupData())
		DEBUGMSG(TRUE, (_T("\n testPavStartupData failed --- \n")));
	else
		DEBUGMSG(TRUE, (_T("\n testPavStartupData PASS +++ \n")));

	if(FALSE == testEndExpPauseData())
		DEBUGMSG(TRUE, (_T("\n testEndExpPauseData failed --- \n")));
	else
		DEBUGMSG(TRUE, (_T("\n testEndExpPauseData PASS +++ \n")));
}

template <typename Type> 
static Int testBdPacketTxRxEndianness(Type* srcData, Type* xmitMsg, Int size)
{
	//Do integrity check first
	if(!srcData || !xmitMsg || size<= 0)
		return 0;
	if(memcmp(xmitMsg, srcData, size) != 0)
		return 0; //initial xmit msg should match source data

	xmitMsg->convHtoN();//convert before xmitting on network
	Int isDifferent = memcmp(xmitMsg, srcData, size);
	//if Little Endian, data should not match after HtoN. 
	//If Big Endian, hton should not change data
	if( (isLittleEndian && !isDifferent) || (!isLittleEndian && isDifferent) )
	{
		return 0; //error!
	}
	//Received the transmit msg
	Type rxMsg;
	memcpy((void*)&rxMsg, (const void*)xmitMsg, size);//receive message into rx buffer
	if(memcmp(&rxMsg, xmitMsg, size) != 0)
		return 0; //copy error!
	rxMsg.convNtoH();
	//the original and the received should be identical after all conversions
	if(memcmp(&rxMsg, srcData, size) != 0)
		return 0; //error!
	return 1;//pass
}


Int NetworkUtest::testRTBreathData()
{
	//RealTimeDataPacket data;	//original (source) data
	//data.endInspPressure = 1.1f;
	//data.inspiredlungVolume = 1.2f;
	//data.phaseType = BreathPhaseType::PhaseType::INSPIRATORY_PAUSE;
	//RealTimeDataPacket xmitMsg = data;	//xmitted msg

	//return testBdPacketTxRxEndianness(&data, &xmitMsg, sizeof(RealTimeDataPacket));
	return 0; //for now
}

Int NetworkUtest::testAveragedBreathData()
{
	AveragedBDPacket data;	//original (source) data
	data.exhaledMinuteVol = 1.1f;
	data.exhaledSpontMinuteVol = 1.2f;
	data.totalRespRate = 14;
	AveragedBDPacket xmitMsg = data;	//xmitted msg

	return testBdPacketTxRxEndianness(&data, &xmitMsg, sizeof(AveragedBDPacket));
}

Int NetworkUtest::testEndBreathData()
{
	//BreathDataPacket data;	//original (source) data
	//data.dynamicCompliance = 1.1f;
	//data.endExpiratoryFlow = 1.2f;
	//data.breathType = BreathType::SPONT;
	//BreathDataPacket xmitMsg = data;	//xmitted msg

	//return testBdPacketTxRxEndianness(&data, &xmitMsg, sizeof(BreathDataPacket));
	return 0; //for now
}

Int NetworkUtest::testWaveformData(void)
{
	WaveformData data;
	data.bdPhase = BreathPhaseType::PhaseType::EXHALATION;
	data.isApneaActive = 1;
	data.lungFlow = 1.3f;
	WaveformData xmitMsg = data;

	return testBdPacketTxRxEndianness(&data, &xmitMsg, sizeof(WaveformData));
}

Int NetworkUtest::testBiLevelMandData()
{
	//BiLevelMandDataPacket data;	//original (source) data
	//data.endExpiratoryPressure = 1.1f;
	//data.peakCircuitPressure = 1.2f;
	//data.breathType = BreathType::SPONT;
	//BiLevelMandDataPacket xmitMsg = data;	//xmitted msg

	//return testBdPacketTxRxEndianness(&data, &xmitMsg, sizeof(BiLevelMandDataPacket));
	return 0; //for now
}

Int NetworkUtest::testBiLevelMandExhData()
{
	//BiLevelMandExhDataPacket data;	//original (source) data
	//data.peakCircuitPressure = 1.1f;
	//data.phaseType = BreathPhaseType::PhaseType::INSPIRATION;
	//data.breathType = BreathType::SPONT;
	//BiLevelMandExhDataPacket xmitMsg = data;	//xmitted msg

	//return testBdPacketTxRxEndianness(&data, &xmitMsg, sizeof(BiLevelMandExhDataPacket));
	return 0; //for now
}

Int NetworkUtest::testComputedInspPauseData()
{
	ComputedInspPauseDataPacket data;	//original (source) data
	data.staticCompliance = 1.1f;
	data.staticResistance = 1.2f;
	ComputedInspPauseDataPacket xmitMsg = data;	//xmitted msg

	return testBdPacketTxRxEndianness(&data, &xmitMsg, sizeof(ComputedInspPauseDataPacket));
}
Int NetworkUtest::testPavBreathData()
{
	PavBreathDataPacket data;	//original (source) data
	data.intrinsicPeep = 1.1f;
	data.pavState = (PavState::Id)2;
	data.totalWorkOfBreathing = 2.3f;
	PavBreathDataPacket xmitMsg = data;	//xmitted msg

	return testBdPacketTxRxEndianness(&data, &xmitMsg, sizeof(PavBreathDataPacket));
}

Int NetworkUtest::testPavPlateauData()
{
	PavPlateauDataPacket data;	//original (source) data
	data.lungCompliance = 1.1f;
	data.pavState = (PavState::Id)2;
	data.totalResistance = 2.3f;
	PavPlateauDataPacket xmitMsg = data;	//xmitted msg

	return testBdPacketTxRxEndianness(&data, &xmitMsg, sizeof(PavPlateauDataPacket));
}

Int NetworkUtest::testPavStartupData()
{
	PavStartupDataPacket data;	//original (source) data
	data.pavState = (PavState::Id)2;
	PavStartupDataPacket xmitMsg = data;	//xmitted msg

	return testBdPacketTxRxEndianness(&data, &xmitMsg, sizeof(PavStartupDataPacket));
}

Int NetworkUtest::testEndExpPauseData()
{
	SocketWrap sock;
	PauseTypes::PpiState data;	//original (source) data
	data = PauseTypes::STABLE;
	PauseTypes::PpiState xmitMsg = data;	//xmitted msg
	if(memcmp(&xmitMsg, &data, sizeof(PauseTypes::PpiState)) != 0)
		return 0; //initial xmit msg should match source data
	xmitMsg = (PauseTypes::PpiState)sock.HTONL((Uint32)xmitMsg);//convert before xmitting on network
	Int isDifferent = memcmp(&xmitMsg, &data, sizeof(PauseTypes::PpiState));
	//if Little Endian, data should not match after HtoN. 
	//If Big Endian, hton should not change data
	if( (isLittleEndian && !isDifferent) || (!isLittleEndian && isDifferent) )
	{
		return 0; //error!
	}
	//Received the transmit msg
	PauseTypes::PpiState rxMsg = xmitMsg;//receive message into rx buffer
	if(memcmp(&rxMsg, &xmitMsg, sizeof(PauseTypes::PpiState)) != 0)
		return 0; //copy failed
	rxMsg = (PauseTypes::PpiState)sock.NTOHL((Uint32)rxMsg);
	//the original and the received should be identical after all conversions
	if(memcmp(&rxMsg, &data, sizeof(PauseTypes::PpiState)) != 0)
		return 0; //error!
	return 1;//pass
}


#endif // RUN_NETWORK_UNIT_TEST && (SIGMA_BD_CPU)
