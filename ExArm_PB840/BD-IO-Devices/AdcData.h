#ifndef ADC_DATA_H
#define ADC_DATA_H

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================
#include "Sigma.hh"
#include "BD_IO_Devices.hh"
#include "AnalogInputs.h"

static const Uint8	NUM_LONG_WORDS_PER_CHANNEL		= 2;
static const Uint8	NUM_ADC_DATA					= ADC_NUMBER_OF_CHANNELS * NUM_LONG_WORDS_PER_CHANNEL;

class AdcData_t
{
public:	
	AdcData_t();
	~AdcData_t();

	void operator = (const AdcData_t&);
	
	uint32_t data[NUM_ADC_DATA];

	bool IsValid(Uint16 input);
	AdcCounts GetValue(Uint16 input);
};


#endif