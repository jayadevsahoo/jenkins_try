#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Filename: HelpStrs.cc - Contains the definitions of all the help menus
// and help text for displaying the help system for normal ventilation
// and service mode.
//---------------------------------------------------------------------
//@ Interface-Description
// All language specific help text is defined in this class.  One global
// variable is exported from this file, NormalVentHelpMenu.  This is the
// highest level help menu for the GUI during normal ventilation (note that the
// system does not currently display help during service mode).  This variable
// points to an array of HelpMenuTopics with each HelpMenuTopic pointing to
// either a submenu or a screen of help text.  Thus, from this high level menu
// a tree of submenus can be formed with menu options eventually ending in a
// screen of help text.
//---------------------------------------------------------------------
//@ Rationale
// The idea is that the HelpSubScreen is completely data-driven from the
// contents of this file.  The content of each help menu and help screen
// is completely specified within this file thus giving the help system more
// flexibility and also localizing all help text in one file to make it easier
// to translate to different languages.
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the two structures defined in HelpStrs.hh, HelpMenuTopic and HelpMenu
// along with the TextAreaLine structure defined in TextArea.hh, we build up
// static instances of these structures or arrays of these structures
// culminating in the definition of the NormalVentHelpMenu structure.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/HelpStrs.ccv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 019   By:   Boris Kuznetsov      Date: 03-May-2003       DR Number: 6036
//  Project:   Russian
//  Description:
//      Addded include file for PolishHelpStrs
//
//  Revision: 018   By:   S. Peters      Date: 16-Oct-2002       DR Number: 6029
//  Project:   Polish
//  Description:
//      Addded include file for PolishHelpStrs
//
//  Revision: 017  By:  gdc	   Date:  11-Sep-1998  DCS Number: 5162
//  Project:  Color
//  Description:
//		Including JapaneseHelpStrs.in for JAPANESE instead of 
//      EnglishHelpStrs.in.
//
//  Revision: 016  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/HelpStrs.ccv   1.21.1.0   07/30/98 10:13:38   gdc
//
//   Revision 015   By:  clw    Date: 09-Jul-1998        DR Number:  xxxx
//   Project:   Japanese        
//   Description:
//       Peer Review rework - portugese help had been set to English, this has now been undone.
//
//  Revision: 014  By:  hhd        Date:  15-JAN-98    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//           Included PortugeseHelpStrs.in
//
//  Revision: 013  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//   Revision 012   By:  clw    Date: 16-Apr-1998        DR Number:  xxxx
//   Project:   Sigma   (R8027)
//   Description:
//       Added support for Japanese language.  Currently this uses the
//       same help text as the English build.
//
//   Revision 011   By:  sah    Date: 15-Jan-1998        DR Number:  5004
//   Project:   Sigma   (R8027)
//   Description:
//       Removed include of now-obsoleted 'Symbols.hh'.
//
//  Revision: 010  By:  hhd	   Date:  23-DEC-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//		 	Included SpanishHelpStrs.in
//
//  Revision: 009  By:  hhd	   Date:  03-DEC-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//		 	Included ItalianHelpStrs.in
//
//  Revision: 008  By:  hhd	   Date:  25-NOV-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//		 	Included GermanHelpStrs.in
//
//  Revision: 008  By:  hhd	   Date:  04-NOV-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//		 	Included FrenchHelpStrs.in
//
//  Revision: 007  By:  clw    Date:  23-OCT-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//             Corrected problems noted in peer review: some wording fixes in the file header,
//             plus added the screen-layout diagram button to several help topic screens.
//
//  Revision: 006  By:  clw    Date:  21-OCT-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//             Removed all language-specific strings.  These are now in separate
//             .in files, one for each language.  Also - eliminated the HelpStrs class
//             to reduce build interdependency.  Previously it only included a subset of
//             the help strings anyway.
//             Code defining HelpText structs formerly included a screen title line; these
//             are now defined as StringId's in the language-dependent file.
//
//  Revision: 005  By:  clw    Date:  14-OCT-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//             Removed unused strings ALARM_HANDLING_HELP_TOPICS_ and
//             SHORT_SELF_TEST_HELP_TOPICS_.
//
//  Revision: 004  By:  clw    Date:  14-OCT-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//             Removed unused string PATIENT_DATA_HELP_TOPICS_
//
//  Revision: 003  By:  clw    Date:  10-OCT-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//             Removed redundant help screen titles 
//
//  Revision: 002  By:  clw    Date:  07-AUG-97    DR Number: 1639
//       Project:  Sigma (R8027)
//       Description:
//             Implement changes from label review and correct problems
//             noted in DCS 1639.
//
//  Revision: 001  By:  mpm    Date:  13-JUL-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "HelpStrs.hh"

//@ Usage-Classes
//@ End-Usage

#ifdef SIGMA_ASIA
	#include "AsiaHelpStrs.in"
#elif SIGMA_EUROPE
	#include "EuropeHelpStrs.in"
#elif SIGMA_CHINESE
	#include "ChineseHelpStrs.in"
#elif SIGMA_ENGLISH
	#include "EnglishHelpStrs.in"
#elif SIGMA_FRENCH
	#include "FrenchHelpStrs.in"
#elif SIGMA_GERMAN
	#include "GermanHelpStrs.in"
#elif SIGMA_ITALIAN
	#include "ItalianHelpStrs.in"
#elif SIGMA_JAPANESE
	#include "JapaneseHelpStrs.in"
#elif SIGMA_POLISH
	#include "PolishHelpStrs.in"
#elif SIGMA_PORTUGUESE
	#include "PortugueseHelpStrs.in"
#elif SIGMA_RUSSIAN
	#include "RussianHelpStrs.in"
#elif SIGMA_SPANISH
	#include "SpanishHelpStrs.in"
#endif


//---------------------------------------------------------------------
//		M E N U S
//---------------------------------------------------------------------

// The style in this file is to define higher level menus first.  This means
// we must predeclare any lower level data members used in higher level data
// objects.  To implement the forward declaration, the "extern" keyword is used.
//
// IMPORTANT: the use of "extern" does NOT mean that these symbols are intended
// to be accessed externally!
//
// Only the NormalVentHelpMenu actually needs to be externally accessed.  This is
// accomplished via an extern statement in HelpStrs.hh.


// Here we declare the array of help topics for the highest level
// menu, NormalVentHelpMenu.
extern HelpMenuTopic NormalVentHelpTopics[];

// The global top level menu, NormalVentHelpMenu
HelpMenu NormalVentHelpMenu =
{
	&HELP_TITLE_MAIN_MENU_, ::NormalVentHelpTopics
};

// Declare all submenus for NormalVentHelpTopics
extern HelpMenu BasicHelpMenu;
extern HelpMenu GettingStartedMenu;
extern HelpMenu AlarmHandlingMenu;
extern HelpMenu ShortSelfTestMenu;
extern HelpMenu ServiceMenu;

HelpText NewPatientSetupHelpText =
{
	HELP_NO_DIAGRAM,
	NewPatientSetupHelpLines,
	&HELP_TITLE_NEW_PATIENT_SETUP_
};

HelpText PatientDataHelpText =
{
	HELP_DIAGRAM1,
	PatientDataHelpLines,
	&HELP_TITLE_PATIENT_DATA_
};

HelpText GraphicsHelpText =
{
	HELP_NO_DIAGRAM,
	GraphicsHelpLines,
	&HELP_TITLE_GRAPHICS_
};


HelpText AlarmHandlingHelpText =
{
	HELP_DIAGRAM1,
	AlarmHandlingHelpLines,
	&HELP_TITLE_ALARM_HANDLING_
};

HelpText ShortSelfTestHelpText =
{
	HELP_NO_DIAGRAM,
	ShortSelfTestHelpLines,
	&HELP_TITLE_SHORT_SELF_TEST_
};

// Define the help topics for the main menu.
HelpMenuTopic NormalVentHelpTopics[] =
{
	{ &HELP_TOPIC_A_FEW_BASICS_,
		NULL,
		&BasicHelpMenu},

	{ &HELP_TOPIC_GETTING_STARTED_,
		NULL,
		&GettingStartedMenu},

	{ &HELP_TOPIC_NEW_PATIENT_SETUP_,
		&NewPatientSetupHelpText,
		NULL},

	{ &HELP_TOPIC_PATIENT_DATA_,
		&PatientDataHelpText,
		NULL},

	{ &HELP_TOPIC_GRAPHICS_,
		&GraphicsHelpText,
		NULL},

	{ &HELP_TOPIC_ALARM_HANDLING_,
		&AlarmHandlingHelpText,
		NULL},

	{ &HELP_TOPIC_SHORT_SELF_TEST_,
		&ShortSelfTestHelpText,
		NULL},

	{ &HELP_TOPIC_SERVICE_,
		NULL,
		&ServiceMenu},

	// DO NOT REMOVE.  THIS MUST BE THE LAST ENTRY IN THIS ARRAY.
	{ NULL, NULL, NULL }
};

extern HelpMenuTopic BasicHelpTopics[];
HelpMenu BasicHelpMenu =
{
	&HELP_TITLE_BASIC_MENU_, BasicHelpTopics
};

extern HelpMenuTopic GettingStartedHelpTopics[];
HelpMenu GettingStartedMenu =
{
	&HELP_TITLE_GETTING_STARTED_MENU_, GettingStartedHelpTopics
};

extern HelpMenuTopic ServiceHelpTopics[];
HelpMenu ServiceMenu =
{
	&HELP_TITLE_SERVICE_MENU_, ServiceHelpTopics
};


HelpText	ScreenHelpText =
{
	HELP_DIAGRAM1,
	ScreensHelpLines,
	&HELP_TITLE_SCREENS_HELP_
};

HelpText	SoundHelpText =
{
	HELP_NO_DIAGRAM,
	SoundsHelpLines,
	&HELP_TITLE_SOUNDS_
};

HelpText	OffscreenKeysHelpText =
{
	HELP_NO_DIAGRAM,
	OffscreenKeysHelpLines,
	&HELP_TITLE_OFFSCREEN_KEYS_
};

HelpText	OffscreenIndicatorsHelpText =
{
	HELP_NO_DIAGRAM,
	OffscreenIndicatorsHelpLines,
	&HELP_TITLE_OFFSCREEN_INDICATORS_
};


HelpText	OnScreenSymbolsHelpText =
{
	HELP_NO_DIAGRAM,
	OnScreenSymbolsHelpLines,
	&HELP_TITLE_ONSCREEN_SYMBOLS_
};

HelpMenuTopic BasicHelpTopics[] = 
{

	{ &HELP_TOPIC_SCREENS_,
		&ScreenHelpText,
		NULL},
	{ &HELP_TOPIC_SOUNDS_,
		&SoundHelpText,
		NULL},
	{ &HELP_TOPIC_OFFSCRN_KEYS_AND_INDICATORS_,
		&OffscreenKeysHelpText,
		NULL},
	{ &HELP_TOPIC_OFFSCRN_INDICATORS_,
		&OffscreenIndicatorsHelpText,
		NULL},
	{ &HELP_TOPIC_ONSCREEN_SYMBOLS_,
		&OnScreenSymbolsHelpText,
		NULL},

	// DO NOT REMOVE.  THIS MUST BE THE LAST ENTRY IN THIS ARRAY.
	{ NULL, NULL, NULL }
};



HelpText PrimarySettingsHelpText =
{
	HELP_DIAGRAM1,
	PrimarySettingsHelpLines,
	&HELP_TITLE_PRIMARY_SETTING_
};

HelpText BatchSettingsHelpText =
{
	HELP_DIAGRAM1,
	BatchSettingsHelpLines,
	&HELP_TITLE_BATCH_SETTINGS_
};

HelpText ChangingSettingsHelpText =
{
	HELP_DIAGRAM1,
	ChangingSettingsHelpLines,
	&HELP_TITLE_CHANGING_SETTINGS_
};

HelpText OtherVentilatorSettingsHelpText =
{
	HELP_NO_DIAGRAM,
	OtherVentilatorSettingsHelpLines,
	&HELP_TITLE_OTHER_SETTINGS_
};

HelpText ConstantDuringRateChangeHelpText =
{
	HELP_DIAGRAM2,
	ConstantDuringRateChangeHelpLines,
	&HELP_TITLE_CONSTANT_DURING_RATE_CHANGE_
};

HelpText ApneaSettingsHelpText =
{
	HELP_DIAGRAM1,
	ApneaSettingsHelpLines,
	&HELP_TITLE_APNEA_SETTINGS_
};

HelpText AlarmSettingsHelpText =
{
	HELP_DIAGRAM1,
	AlarmSettingsHelpLines,
	&HELP_TITLE_ALARM_SETTINGS_
};

HelpMenuTopic GettingStartedHelpTopics[] = 
{
	{ &HELP_TOPIC_CHANGING_SETTINGS_,
		&ChangingSettingsHelpText,
		NULL},

	{ &HELP_TOPIC_PRIMARY_SETTINGS_,
		&PrimarySettingsHelpText,
		NULL},

	{ &HELP_TOPIC_BATCH_SETTINGS_,
		&BatchSettingsHelpText,
		NULL},

	{ &HELP_TOPIC_OTHER_VENT_SETTINGS_,
		&OtherVentilatorSettingsHelpText,
		NULL},

	{ &HELP_TOPIC_CONST_DURING_RATE_CHANGE_,
		&ConstantDuringRateChangeHelpText,
		NULL},

	{ &HELP_TOPIC_APNEA_SETTINGS_,
		&ApneaSettingsHelpText,
		NULL},

	{ &HELP_TOPIC_ALARM_SETTINGS_,
		&AlarmSettingsHelpText,
		NULL},
	// DO NOT REMOVE.  THIS MUST BE THE LAST ENTRY IN THIS ARRAY.
	{ NULL, NULL, NULL }
};


HelpText GeneralInformationHelpText =
{
	HELP_NO_DIAGRAM,
	GeneralInformationHelpLines,
	&HELP_TITLE_GENERAL_INFO_
};

HelpText O2SensorCalibrationHelpText=
{
	HELP_NO_DIAGRAM,
	O2SensorCalibrationHelpLines,
	&HELP_TITLE_O2_SENSOR_CALIB_
};

HelpText CleaningSterilizationHelpText =
{
	HELP_NO_DIAGRAM,
	CleaningSterilizationHelpLines,
	&HELP_TITLE_CLEANING_STERILIZATION_
};

HelpText PeriodicMaintenanceHelpText =
{
	HELP_NO_DIAGRAM,
	PeriodicMaintenanceHelpLines,
	&HELP_TITLE_PERIODIC_MAINT_
};

HelpText ServiceTechnicianHelpText =
{
	HELP_NO_DIAGRAM,
	ServiceTechnicianHelpLines,
	&HELP_TITLE_SERVICE_TECHNICIAN_
};

HelpMenuTopic ServiceHelpTopics[] = 
{
	{ &HELP_TOPIC_GENERAL_INFO_,
		&GeneralInformationHelpText,
		NULL
	},

	{ &HELP_TOPIC_O2_SENSOR_CALIBRATION_,
		&O2SensorCalibrationHelpText,
		NULL
	},

	{ &HELP_TOPIC_CLEANING_AND_STERILIZATION_,
		&CleaningSterilizationHelpText,
		NULL
	},

	{ &HELP_TOPIC_PERIODIC_MAINTENANCE_,
		&PeriodicMaintenanceHelpText,
		NULL
	},

	{ &HELP_TOPIC_SERVICE_TECH_,
		&ServiceTechnicianHelpText,
		NULL
	},
	// DO NOT REMOVE.  THIS MUST BE THE LAST ENTRY IN THIS ARRAY.
	{ NULL, NULL, NULL }
};



