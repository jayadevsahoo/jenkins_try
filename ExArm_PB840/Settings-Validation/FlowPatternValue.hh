
#ifndef FlowPatternValue_HH
#define FlowPatternValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  FlowPatternValue - Values of Flow Pattern Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/FlowPatternValue.hhv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage

struct FlowPatternValue
{
  //@ Type:  FlowPatternValueId
  // All of the possible values of the Flow Pattern Setting.
  enum FlowPatternValueId
  {
    // $[02049] -- values of the range of Apnea Flow Pattern Setting...
    // $[02125] -- values of the range of Flow Pattern Setting...
    SQUARE_FLOW_PATTERN,
    RAMP_FLOW_PATTERN,

    TOTAL_FLOW_PATTERNS
  };
};


#endif // FlowPatternValue_HH 
