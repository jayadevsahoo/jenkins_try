#ifndef TestData_HH
#define TestData_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TestData - Compute processed sensor data
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/TestData.hhv   25.0.4.0   19 Nov 2013 14:23:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  syw    Date: 11-Aug-1997    DR Number: DCS 2356
//       Project:  Sigma (840)
//       Description:
//          Defined peakPexh_ data member and resetPeakPexh() and getPeakPexh()
//			methods.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date: 21-Aug-1995    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

class TestData
{
  public:

    TestData(void);
    ~TestData(void);

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);

    void newCycle (void);
    void calculateFlowOffset(void);

    inline void resetDeliveredVolume(void);
    inline Real32 getDeliveredVolume(void);
    inline Real32 getNetDeliveredVolume(void);
	inline void resetPeakPexh( void) ;
	inline Real32 getPeakPexh( void) const ;
	inline Real32 getAveExhPress( void ) const { return aveExhPress_; }
	inline Real32 getAveExhDrvPress ( void ) const { return aveExhDrvPress_; }
		
  protected:

  private:
    TestData(const TestData&);	        // not implemented...
    void   operator=(const TestData&);	// not implemented...

    //@ Data-Member:  deliveredVolume_
    // gas volume delivered (air and O2)
    Real32		deliveredVolume_;

    //@ Data-Member:  netDeliveredVolume_
    // gas volume delivered (air and O2) minus the volume exiting
    // the exhalation valve
    Real32		netDeliveredVolume_;

    //@ Data-Member:  flowOffset_
    // expiratory flow offset
    Real32		flowOffset_;

	//@ Data-Member: peakPexh_
	// peak pressure seen by exh pressure sensor
	Real32 peakPexh_ ;

	//@ Data-Member: aveExhDrvPress_
	// average of three samples of the exh valve drv pressure
	Real32 aveExhDrvPress_ ;

	//@ Data-Member: aveExhPress_
	// average of three samples of the exh valve pressure
	Real32 aveExhPress_;
};


// Inlined methods...
#include "TestData.in"

#endif // TestData_HH 
