
#ifndef Array_ResultTableEntry_MAX_TEST_ENTRIES_HH
#define Array_ResultTableEntry_MAX_TEST_ENTRIES_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Array_ResultTableEntry_MAX_TEST_ENTRIES - Fixed Array of 'MAX_TEST_ENTRIES' Elements of Type 'ResultTableEntry'.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedArray.H_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Collection.hh"
#include "Collection.hh"

//@ Usage-Classes
#include "Array_ResultTableEntry.hh"
//@ End-Usage


class Array_ResultTableEntry_MAX_TEST_ENTRIES : public Array_ResultTableEntry
{
  public:
    Array_ResultTableEntry_MAX_TEST_ENTRIES(const Array_ResultTableEntry_MAX_TEST_ENTRIES& array);
    Array_ResultTableEntry_MAX_TEST_ENTRIES(void);
    virtual ~Array_ResultTableEntry_MAX_TEST_ENTRIES(void);

    inline void  operator=(const Array_ResultTableEntry_MAX_TEST_ENTRIES& array);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  protected:
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    //@ Data-Member:  pBlock_
    // A fixed array of 'MAX_TEST_ENTRIES' elements.
    ResultTableEntry  pBlock_[MAX_TEST_ENTRIES];
};


// Inlined methods...
#include "Array_ResultTableEntry_MAX_TEST_ENTRIES.in"


#endif  // Array_ResultTableEntry_MAX_TEST_ENTRIES_HH
