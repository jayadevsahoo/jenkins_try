#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Flash Memory - Utilities to erase, write to flash memory.
//---------------------------------------------------------------------
//@ Interface-Description
//		Provides interface to erase and to write to flash memory.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//		The FlashWriteEnable bit must be enabled before erasing or writing
//		to flash memory.  Only the first block of flash will be allowed to
//		be accessed by this class.  To ensure reliability of the write,
//		the flash should be erased before writing.  The flash erase will
//		erase the contents of one block.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlashMemory.ccv   25.0.4.0   19 Nov 2013 13:54:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: rhj    Date:  11-April-2006    DR Number: 6192
//  Project:  XENA
//  Description:
//      Added support for the Spansion Flash memory chip.
//      These changes only supports the XENA GUI board with the
//      non-XENA BD board.
//
//  Revision: 003 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002 By: syw    Date: 22-May-1997   DR Number: DCS 2165
//  	Project:  Sigma (R8027)
//		Description:
//			Moved TI labels.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "FlashMemory.hh"

//@ Usage-Classes

#include "Task.hh"

//@ End-Usage

//TODO E600_LL: Need to review this file with all #ifdef E600_840_TEMP_REMOVED

#ifdef SIGMA_DEBUG
#include <stdio.h>
#endif // SIGMA_DEBUG

//@ Code...

// Initialize the static flash block address. 
Uint32 * FlashMemory::FlashBlockAddress_[(MAX_TOTAL_NUM_SUB_BLOCKS+1)] =
{
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

// Default the static initialization flag to false.
Boolean  FlashMemory::initializationFlag_ = FALSE;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  This static method takes no argument and returns no value.  
//  It calls the FlashProgrammer Initialize function to determine
//  which flash memory chip is currently installed.  
//  Also, it initializes the beginning address of the each flash memory
//  sub-block into an array.
//---------------------------------------------------------------------
//@ Implementation-Description
//  FlashBlockAddress_ is initialized by the FlashProgrammer::
//  GetFlashBlockAddress.  It also sets initializationFlag to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
FlashMemory::Initialize(void)
{
#ifdef E600_840_TEMP_REMOVED
    CALL_TRACE("FlashMemory::Initialize(void)");

    // Determines which flash memory device functions will be used.
    FlashProgrammer::Initialize();

    for(Uint8 index = 0; index <= FlashProgrammer::GetTotalSubBlocks(); index++)
    {        
        FlashBlockAddress_[index] = FlashProgrammer::GetFlashBlockAddress(index);
    }
    initializationFlag_ = TRUE;
#endif
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	EraseFlash
//
//@ Interface - Description
//		This method has the starting address and the ending address of flash
//		to erase.  This method returns the error status of the erase with
//		0 indicating no error.  This method will erase all blocks starting
//		with the block that the starting addresss is in up to the block that
//		the ending address is in.   
//-----------------------------------------------------------------------
//@ Implementation - Description
//		Determine which blocks the start and end address reside in and erase
//		the blocks.
//-----------------------------------------------------------------------
//@ PreCondition
//		end >= start
//		start >= FlashBlockAddress_[0] && start < FlashBlockAddress_[1]
//		end >= FlashBlockAddress_[0] && end < FlashBlockAddress_[1]
//      Must execute initialize static function first before executing this
//      method.
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================
Uint32
FlashMemory::EraseFlash( Uint32 *start, Uint32 *end )
{
	CALL_TRACE("FlashMemory::EraseFlash( Uint32 *start, Uint32 *end )") ;

   	Uint32 rtnStatus = 0 ;
#ifdef E600_840_TEMP_REMOVED
   	Uint32 volatile *address ;
	CLASS_PRE_CONDITION( end >= start) ;
    CLASS_PRE_CONDITION(initializationFlag_);
   	// $[TI1]

#if defined(SIGMA_PRODUCTION)

	CLASS_PRE_CONDITION( start >= FlashBlockAddress_[0] && start < FlashBlockAddress_[1]) ;
	CLASS_PRE_CONDITION( end >= FlashBlockAddress_[0] && end < FlashBlockAddress_[1]) ;

  	address = FlashBlockAddress_[0] ;
    rtnStatus = FlashProgrammer::FlashEraseBlock(address) ;

#else

	CLASS_PRE_CONDITION( start >= FlashBlockAddress_[0] && start < FlashBlockAddress_[MAX_TOTAL_NUM_SUB_BLOCKS + 1 -1]) ;
	CLASS_PRE_CONDITION( end >= FlashBlockAddress_[0] && end < FlashBlockAddress_[MAX_TOTAL_NUM_SUB_BLOCKS + 1-1]) ;
	
	Uint32 startIndex ;
   	Uint32 endIndex ;
   
   	for (startIndex=MAX_TOTAL_NUM_SUB_BLOCKS + 1-2; start < FlashBlockAddress_[startIndex]; startIndex--)
    	;
	
   	for (endIndex=MAX_TOTAL_NUM_SUB_BLOCKS + 1-2; end < FlashBlockAddress_[endIndex] ; endIndex--)
      	;

	// erase complete block that fall between start and end
   	for	(Uint32 ii=startIndex; ii <= endIndex; ii++)
   	{
	   	address = FlashBlockAddress_[ii] ;
        rtnStatus = FlashProgrammer::FlashEraseBlock(address);
   	}

#endif // defined(SIGMA_PRODUCTION)
#endif

	return( rtnStatus) ;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	FlashWrite
//
//@ Interface - Description
//		This method has the flash write address and the Uint32 data as arguments.
//		This method returns the error status of the write with 0 indicating no
//		error.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		Validate writeAddress and then write out data to flash.
//-----------------------------------------------------------------------
//@ PreCondition
//		writeAddress >= FlashBlockAddress_[0] && writeAddress < FlashBlockAddress_[1]
//      Must execute initialize static function first before executing this
//      method.
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================
Uint32
FlashMemory::FlashWrite( Uint32 *writeAddress, Uint32 data)
{
#ifdef E600_840_TEMP_REMOVED
	CALL_TRACE("FlashMemory::FlashWrite( Uint32 *writeAddress, Uint32 data)") ;

   	// $[TI1]
   	
#if defined(SIGMA_PRODUCTION)
    CLASS_PRE_CONDITION(initializationFlag_);

	CLASS_PRE_CONDITION( writeAddress >= FlashBlockAddress_[0] && writeAddress < FlashBlockAddress_[1]) ;

#endif // defined(SIGMA_PRODUCTION)

    return( FlashProgrammer::FlashWriteWord( writeAddress, data)) ;
#endif
	return 0;
}

//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: BackupCheckSumTable
//
//@ Interface - Description
//       Backup the checksum table from Flash into a temporary memory
//       space.
//-----------------------------------------------------------------------
//@ Implementation - Description
//       Calls FlashProgrammer::CopyCheckSumTable to copy the checksum
//       table from Flash into a temporary memory space.
//-----------------------------------------------------------------------
//@ PreCondition
//      Must execute initialize static function first before executing this
//      method.
//-----------------------------------------------------------------------
//@ PostCondition
//       none
//@ End - Method
//=======================================================================
void
FlashMemory::BackupCheckSumTable()
{
#ifdef E600_840_TEMP_REMOVED
    CALL_TRACE("FlashMemory::CopyCheckSumTable()") ;
    CLASS_PRE_CONDITION(initializationFlag_);

    FlashProgrammer::CopyCheckSumTable();
#endif
}

//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: RestoreCheckSumTable
//
//@ Interface - Description
//      Restores the checksum table into Flash.
//-----------------------------------------------------------------------
//@ Implementation - Description
//       Calls FlashProgrammer::WriteCheckSumTable to write the checksum
//       table from the temporary memory space to Flash.
//-----------------------------------------------------------------------
//@ PreCondition
//      Must execute initialize static function first before executing this
//      method.
//-----------------------------------------------------------------------
//@ PostCondition
//      none
//@ End - Method
//=======================================================================
Uint32
FlashMemory::RestoreCheckSumTable()
{
#ifdef E600_840_TEMP_REMOVED
    CALL_TRACE("FlashMemory::WriteCheckSumTable()") ;
    CLASS_PRE_CONDITION(initializationFlag_);

    return( FlashProgrammer::WriteCheckSumTable());
#endif
	return 0;
}


#ifdef SIGMA_DEBUG

void
FlashMemory::DumpFlash( Uint32 *start, Uint32 *end )
{
	Uint32 index ;
	
	for (index=0; start + index <= end; index++)
	{
		if (!(index % 8))
			printf( "\n%8.8lX ", start+index) ;
		printf( "%8.8lX ", *(start+index)) ;
	}
}

void
FlashMemory::VerifyFlash( Uint32 *start, Uint32 *end, Uint32 data)
{
   	Uint32 volatile *address = start ;
   	
	for ( ; address <= end; address++)
	{
		Uint32 volatile readData = *address ;

		if (readData != data)
			printf( "Error: %8.8lX  %8.8lX vs. %8.8lX\n", address, readData, data) ;
	}  
}

void
FlashMemory::FlashTestDriver( void)
{
	Uint32 eraseError = 0 ;
	Uint32 writeError = 0 ;

	for (Uint32 jj=0; jj < MAX_TOTAL_NUM_SUB_BLOCKS + 1; jj++)
		printf( "Block %ld: %8.8lX\n", jj, FlashBlockAddress_[jj]) ;

	SetFlashWriteEnable() ;
	Task::Delay(0,20) ;
		
	for (Uint32 index=0; index < MAX_TOTAL_NUM_SUB_BLOCKS + 1-1; index++)
	{	
		eraseError =0 ;
		writeError = 0 ;
	
//		DumpFlash( FlashBlockAddress_[index], FlashBlockAddress_[index+1] - 1) ;

		printf( "\nErasing Flash\n") ;
	
		eraseError = EraseFlash( FlashBlockAddress_[index], FlashBlockAddress_[index+1] - 1 ) ;

		if (!eraseError)
		{
//			DumpFlash( FlashBlockAddress_[index], FlashBlockAddress_[index+1] - 1) ;

			printf( "Verifying Erase\n") ;
			VerifyFlash( FlashBlockAddress_[index], FlashBlockAddress_[index+1] - 1, 0xffffffff ) ;

			printf( "Writing Flash\n") ;

			for (Uint32 ii=0; ii < FlashBlockAddress_[index+1] - FlashBlockAddress_[index] && !writeError; ii++)
			{
				writeError = FlashWrite( FlashBlockAddress_[index] + ii, (Uint32)FlashBlockAddress_[index] + ii * 4) ;
			}

			if (!writeError)
			{
				printf( "Verifying Write\n") ;
				for (ii=0; ii < FlashBlockAddress_[index+1] - FlashBlockAddress_[index]; ii++)
				{
					VerifyFlash( FlashBlockAddress_[index] + ii, FlashBlockAddress_[index] + ii, (Uint32)FlashBlockAddress_[index] + ii*4) ;
				}
			}
			else
			{
				printf( "FAILURE: Write error code = %8.8lX\n", writeError) ;
			}
		}
		else
		{
			printf( "FAILURE: Erase error code = %8.8lX\n", eraseError) ;
		}
	}
	ClearFlashWriteEnable() ;
	Task::Delay( 0, 20) ;
}

#endif // SIGMA_DEBUG


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
FlashMemory::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("FlashMemory::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, FLASHMEMORY,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

