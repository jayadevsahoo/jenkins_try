#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Covidien
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Covidien Corporation.
//
//     Copyright (c) 1997, Covidien Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: ProxiInterface - Protocol Driver for the Proximal Sensor serial protocol.
//---------------------------------------------------------------------
//@ Interface-Description
//  Provides the Proximal Sensor protocol driver
//  for the 840 ventilator. This class contains the methods that
//  receive and decode incoming commands from the proximal sensor over the serial
//  port. It also contains the interface for outputing data to the proximal sensor
//  through the serial port.
//---------------------------------------------------------------------
//@ Rationale
//  Contains proximal sensor command and data processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness of 
//  parameterized data. Incoming commands are verified for correctness
//  and an error report issued for commands in error.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ProxiInterface.ccv   25.0.4.0   19 Nov 2013 13:54:08   pvcs  $
//
//  Revision: 010   By: rhj    Date: 31-Mar-2011   SCR Number: 6630
//  Project:  PROX
//  Description:
//      Added disablePurge() method in the processRequests_() method
//      to ensure that the prox board disables purge.
// 
//  Revision: 009   By: rhj    Date: 16-Feb-2011   SCR Number: 6745 
//  Project:  PROX
//  Description:
//      Changed prox's maxChargeTime from 800ms to 2 secs.
//
//  Revision: 008   By: gdc   Date: 17-Jan-2011    SCR Number: 6733
//  Project:  PROX
//  Description:
//      Included VCO2 study code for development reference.
// 
//  Revision: 007   By: gdc   Date: 17-Jan-2011    SCR Number: 6732
//  Project:  PROX
//  Description:
//      Resolved unpredictable order of evaluation of expressions with
//      post-increment operator by using indexing operator for all data
//      from proximal sensor.
// 
//  Revision: 006   By: rpr   Date: 27-Dec-2010    SCR Number: 6711 and 6710
//  Project:  PROX
//  Description:
//      Unrecognized DPIs are skipped appropriately and floating point variable
//      truncation fixed.
// 
//  Revision: 005   By: rhj   Date: 14-Dec-2010    SCR Number: 6656
//  Project:  PROX
//  Description:
//      Added a flag to prevent barametric pressures to be updated
//      during Demo mode.
//  
//  Revision: 004   By: rhj   Date: 16-Nov-2010    SCR Number: 6622
//  Project:  PROX
//  Description:
//      Added stopProx();
//  
//  Revision: 003   By: rpr   Date: 29-Sept-2010    SCR Number: 6687
//  Project:  PROX
//  Description:
//      Fixes getting continuous serial data without any commands from PROX. 
// 
//  Revision: 002  By:  rpr    Date: 28-Jul-2010   SCR Number: 6602
//  Project:  PROX
//  Description:
//      removed resetting mercury module if revision string was not 
//		initially retrieved, just reset and clear serial channel
// 
//  Revision: 001 	By: gdc 	Date: 16 May 2008   SCR Number: 6436
//	Project:  PROX
//	Description:
//		Initial version 
//=====================================================================

#include "Sigma.hh"
#include "ProxiInterface.hh"
#include "IpcIds.hh"
#include "BdSerialPort.hh"
#include "Mercury.hh"
#include "Task.hh"
#include "TaskInfo.hh"
#include "TaskControlAgent.hh"
#include "OsTimeStamp.hh"
#include "BigSerialNumber.hh"
#include "MathUtilities.hh"
#include "PatientCctTypeValue.hh"
#include "PhasedInContextHandle.hh"
#include <string.h>

#if defined( SYSLOG_ENABLED )
    #include <syslog.h>
    #define SYSLOG(A) syslog A
    #define OPENLOG(A) openlog A
#else
    #define SYSLOG(A)
    #define OPENLOG(A)
#endif // defined( SYSLOG_ENABLED )


//@ End-Usage

//@ Code...

//Globals

static const Real32 ADULT_VOLUME_RESOLUTION = 1.0;  // ml
static const Real32 NEO_VOLUME_RESOLUTION = 0.1;  // ml

Boolean ProxiInterface::isProxBoardInstalled_ = FALSE;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProxiInterface [constructor]
//
//@ Interface-Description
//  Constructor for the ProxiInterface class. Instantiates the Proximal sensor
//  protocol interface on the specified SerialPort.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ProxiInterface::ProxiInterface( BdSerialPort & rSerialPort,
                                BdSerialInterface::PortNum portNum ) :
BdSerialInterface( rSerialPort, portNum ),
isFlowRequested_(FALSE),
isPressureRequested_(FALSE),
isCO2Requested_(FALSE),
isVolumeRequested_(FALSE),
isAccumPressureRequested_(FALSE),
volumeResolution_(0.0),
currentCycle_(0),
nextReadCycle_(1),
totalWaveformPkts_(0),
missedWaveformPkts_(0),
shortPkts_(0),
invalidChecksums_(0),
badHdrs_(0),
expectedPacketCount_(0),
totalBytesRead_(0),
isDataFresh_(FALSE),
isPurgeParamsTransmitting_(FALSE),
isUpdateNeeded_(FALSE),
isDumpAccumalatorRequested_(FALSE),
isHostZeroRequested_(FALSE),
isZeroBusy_(FALSE),
isModuleResetRequested_(FALSE),
isGasConditionUpdateNeeded_(FALSE),
isGasConditionsTransmitting_(FALSE),
pumpOn_(FALSE),
isRevStrReady_(FALSE),
isSerialNumReady_(FALSE),
isOemIdReady_(FALSE),
isPurgeIntervalUpdateRequested_(FALSE)
{
    Uint16 index;

    accumlatorPressure_.value = 0;
    flow_.value = 0.0;
    pressure_.value = 0.0;
    volume_.value = 0.0;
    co2_.value = 0.0;
    reportedZeroTime_.data = 0;
    currentResevoirPressure_.value = 0;
    resumeRequest_ = FALSE;
    suspendRequest_ = FALSE;
    resumePurgeRequest_ = FALSE;
    suspendPurgeRequest_ = FALSE;
    isProxSuspended_ = FALSE;
    isManualPurgeRequested_ = FALSE;

    manualTargetPressure_ = 6;
    manualMaxPressure_ = manualTargetPressure_ + 2;

    idString_[0] = '\0';

    purgeStatus_.byte1 = 0;
    purgeStatus_.byte2 = 0;
    purgeStatus_.byte3 = 0;
    flowStatusByte1_ = 1;

    zeroStatus_ = ZERO_IDLE;
    sensorType_ = Mercury::NEONATAL_FLOW_SENSOR;

    transferGasConditionBuffer_.fio2 = 21;
    transferGasConditionBuffer_.anesthetic = 0;
    transferGasConditionBuffer_.gasBalance = 0;
    transferGasConditionBuffer_.inspGasTemperature = 35;
    transferGasConditionBuffer_.expGasParam = 0;
    transferGasConditionBuffer_.inspHumidity = 50;


    transferPurgeBuffer_.mode = ProxiInterface::PROX_MANUAL_MODE;
    transferPurgeBuffer_.maxWaitTime = 10.0;
    transferPurgeBuffer_.resTargetPressure = 3.0;
    transferPurgeBuffer_.resMaxPressure =  transferPurgeBuffer_.resTargetPressure + 2.0f;
    transferPurgeBuffer_.cycleRepCount = 4;
    transferPurgeBuffer_.releaseValveOpenTime = 0.075f;
    transferPurgeBuffer_.releaseValveCloseTime = 0.125f;
    transferPurgeBuffer_.intervalTime = 180;
    targetPurgeInterval_ = 180;
    transferPurgeBuffer_.maxChargeTime = 2.0f;

    mainRevision_.pStr = "NOT INSTALLED";
    boardSerialNumber_ = 0;

    for (index =  0; index < PROX_MAX_REV_STR_SIZE; index++)
    {
        mainRevisionStr_[index] = '\0';
    }


    readState_ = COMMAND;

    maxRead_ = 0;

    enablePurgeTest_ = FALSE;
    sstTargetPressure_ = 0.0f;

    enableSquareWaveformDemoMode_ = FALSE;
    disableSquareWaveformDemoMode_ = FALSE;
    setProxManualPurge_ = FALSE;

    memset(portBuffer_, 0, sizeof(portBuffer_));

    cmdDex_ = 0;
    nextDex_ = 0;
    bytesRead_ = 0;
    commandFound_ = FALSE;
    nbfFound_ = FALSE;
    pktSize_ = 0;
    numCmds_ = 0;

    barometricPressure_.value = 0;
    isGetOEMIdRequested_  = FALSE;

    airwayPressureOOR_ = 0;
    flowOOR_ = 0;

    enableContinuousMode_ = FALSE;
    disableContinuousMode_ = FALSE;

    isProxReady_ = FALSE;

    inspiredVolumeFactor_ = 0.0f;
    expiredVolumeFactor_ = 0.0f;
    atmPressure_ = 0.0f;
    hmeFactorTest_ = 0.0f;
    hmeFactor_ = 0.0f;
    proxCorrectionFactor_ = 0.0f;

    isProxInDemoMode_ = FALSE;
    isProxBoardInstalled_ = rSerialPort_.GetCTS();

#if defined(VCO2_STUDY)
    mercurySoftwareOptions_ = 0;
    isCapnostatZeroed_ = FALSE;
    isCapnostatZeroRequested_ = FALSE;
    capnostatStatus_.data = 0;
    endTidalCO2_.value = 0;
    inspiredCO2_.value = 0;
    volumeCO2ExpiredPerMinute_.value = 0;
    mixedExpiredCO2_.value = 0;
    startOfInspirationMark_.value = 0;
    startOfExpirationMark_.value = 0;
    inspiredTime_.value = 0;
    expiredTime_.value = 0;
    mercurySoftwareOptions_ = 0;
    previousCapnostatStatus_ = 0;
    expiredTime_.state = DATA_UNINITIALIZED;
    inspiredTime_.state = DATA_UNINITIALIZED;
    expiratoryVolume_.state = DATA_UNINITIALIZED;
    inspiratoryVolume_.state = DATA_UNINITIALIZED;
    endTidalCO2_.state = DATA_UNINITIALIZED;
    inspiredCO2_.state = DATA_UNINITIALIZED;
    volumeCO2ExpiredPerMinute_.state = DATA_UNINITIALIZED;
    mixedExpiredCO2_.state = DATA_UNINITIALIZED;
#endif // defined(VCO2_STUDY)
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProxiInterface [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ProxiInterface::~ProxiInterface()   
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isDataFresh
//
//@ Interface-Description
//  returns variable isDataFresh indicating new waveform data has been
//  received
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isDataFresh(void) const
{
    return(isDataFresh_);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFlow
//
//@ Interface-Description
//  returns flow value for prox sensor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getFlow(void) const
{
    return flow_.value;   
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPressure
//
//@ Interface-Description
//  returns pressure value for prox sensor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getPressure(void) const
{
    return pressure_.value;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getBarometricPressure
//
//@ Interface-Description
//  returns barometric pressure
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getBarometricPressure(void) const
{
    return barometricPressure_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAccumatorPressure
//
//@ Interface-Description
//  returns accumulator or resovoir prssure used to purge line clear
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getAccumatorPressure(void) const
{
    return accumlatorPressure_.value;
} 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getBoardSerialNumber
//
//@ Interface-Description
//  returns serial number of prox board
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32 ProxiInterface::getBoardSerialNumber(void) const
{
    return(boardSerialNumber_);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getResevoirPressure
//
//@ Interface-Description
// returns resevour pressure from waveform packets
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getResevoirPressure(void) const
{
    return currentResevoirPressure_.value;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendPacket_
//
//@ Interface-Description
//  sends serial packet stream to proximal interface
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::sendPacket_(void* buffer, const int numbytes)
{
    // wait a long time to send the message since the sensor may be busy
    Uint32 bytesSent = write(buffer, numbytes, 30000);

    if (bytesSent < (Uint32) numbytes)
    {
        SYSLOG((LOG_ERR, "failed to send packet"));
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processGetAndSetPkt_
//
//@ Interface-Description
// Processes instruments settings received from prox interface
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::processGetAndSetPkt_(unsigned char * msg, int pktSize)
{

    int index;
    unsigned char *pByte = msg + 2;        // waveform data starts after cmd and nbf
    char isb = *pByte++;
	UNUSED_SYMBOL(pktSize); 

    switch (isb)
    {
    case Mercury::MAX_INPUT_LINE_WAIT_TIME:
        {
            purgeParams_.maxWaitTime = ((pByte[0] << 7) + pByte[1]) / 100.0f;
            pByte += 2;
        }
        break;

    case Mercury::OEM_ID:
        {
            for (index = 0; index < PROX_MAX_ID_STRING - 1; index++)
            {
                idString_[index] = *pByte++;
            }
            idString_[index] = '\0';
            isOemIdReady_ = TRUE;
        }
        break;

    case Mercury::ZERO_PURGE_MODE:
        {
            ;//do nothing
        }
        break;
    case Mercury::RESERVOIR_TARGET_PRESSURE:
        {
            purgeParams_.resTargetPressure = ((pByte[0] << 7) + pByte[1]) / 100.0f;
            pByte += 2;
        }
        break;
    case Mercury::RESERVOIR_MAX_PRESSURE:
        {
            purgeParams_.resMaxPressure = ((pByte[0] << 7) + pByte[1]) / 100.0f;
            pByte += 2;
        }
        break;
    case Mercury::CYCLE_REPETITION_COUNT:
        {
            purgeParams_.cycleRepCount =  *pByte++;
        }
        break;
    case Mercury::RELEASE_VALVE_OPEN_TIME:
        {
            purgeParams_.releaseValveOpenTime = ((pByte[0] << 7) + pByte[1]) / 1000.0f;
            pByte += 2;
        }
        break;
    case Mercury::RELEASE_VALVE_CLOSE_TIME:
        {
            purgeParams_.releaseValveCloseTime = ((pByte[0] << 7) + pByte[1]) / 1000.0f;
            pByte += 2;
        }
        break;
    case Mercury::PURGE_INTERVAL_TIME:
        {
            purgeParams_.intervalTime =  (pByte[0] << 7) + pByte[1];
            pByte += 2;
        }
        break;
    case Mercury::RESERVOIR_PRESSURE_TRANSDUCER_OFFSET:
        {
            purgeParams_.resPressureTransOffset = ((pByte[0] << 7) + pByte[1]) / 1000.0f;
            pByte += 2;
        }
        break;
    case Mercury::MAX_CHARGE_TIME:
        {
            purgeParams_.maxChargeTime = ((pByte[0] << 7) + (pByte[1])) / 100.0f;
            pByte += 2;
        }
        break;
    case Mercury::MODULE_SERIAL_NUMBER:
        {
            boardSerialNumber_ =  (pByte[0] << 28) + (pByte[1] << 21) + 
                                  (pByte[2] << 14) + (pByte[3] << 7) + pByte[4];
            pByte += 5;
            isSerialNumReady_ = TRUE;
        }
        break;
#if defined(VCO2_STUDY)
    case Mercury::INSTALLED_SOFTWARE_OPTIONS:
        mercurySoftwareOptions_  = (pByte[0] << 16) + (pByte[1] << 8) + pByte[2];
        pByte += 3;
        break;
    case Mercury::SET_GET_ETCO2_TIME_PERIOD:
    case Mercury::SET_GET_VCO2_AVERAGING_TIME:
        pByte += 1;
        break;
#endif // defined(VCO2_STUDY)

    default:
        break;
    }



}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addWord_
//
//@ Interface-Description
//  Helper function to build serial data streams. Adds one word to serial
//  stream and returns the number of bytes added to stream.  Output can 
//  be used to increment index to buffering data.  Output is always 2.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
int ProxiInterface::addWord_(unsigned char* pBuf, const short value)
{
    *pBuf++ = value >> 8;
    *pBuf   = (unsigned char)value;
    return 2;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processWaveformPkt_
//
//@ Interface-Description
//  Processes wave form packets from prox interface
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void ProxiInterface::processWaveformPkt_(unsigned char* msg, int pktSize)
{
    unsigned char *pByte = msg + 2;  // waveform data starts after cmd and nbf

    static const Int ACCUM_PRESSURE_OFFSET = 0;
    static const Int FLOW_OFFSET = 100000;
    static const Int PRESSURE_OFFSET = 8192;
    static const Int VOLUME_OFFSET = 8192;
    static const Int CO2_OFFSET = 1000;
    static const Real32 FLOW_RESOLUTION = 0.01; // L/min
    static const Real32 PRESSURE_RESOLUTION = 0.05;  // cmH2O
    static const Real32 CO2_RESOLUTION = 0.01; // mmHg
    static const Real32 ACCUM_PRESSURE_RESOLUTION = 0.01 ;
    static const Real32 FLOW_LOWER_BOUND = -180;
    static const Real32 FLOW_UPPER_BOUND = 180.0;
    static const Real32 AIRWAY_PRESSURE_LOWER_BOUND = -120.0;
    static const Real32 AIRWAY_PRESSURE_UPPER_BOUND = 120.0;


#if defined(VCO2_STUDY)
    // clear the SOI/SOE marks before reading the new DPIs
    startOfInspirationMark_.value = 0.0f;
    startOfExpirationMark_.value = 0.0f;
#endif

    Int flow;
    Int pressure;
    Int co2;
    Int volume;
    Int accumPressure;
    Real32 tempFlow;
    Real32 tempPressure;
    static Real32 previousFlow = 0.0;
    static Real32 previousPressure = 0.0;
    /******** Order Matters !!!!! ********/
    if (isFlowRequested_)
    {
        *pByte &= 0x07;    // BUG workaround
        flow  = *pByte++ << 14;
        flow += *pByte++ << 7;
        flow += *pByte++;
        flow -= FLOW_OFFSET;
        tempFlow =  flow * FLOW_RESOLUTION;
        if ( tempFlow >= FLOW_LOWER_BOUND && tempFlow <= FLOW_UPPER_BOUND )
        {
            flow_.value = tempFlow;
            previousFlow = tempFlow;
            flowOOR_ = 0;
        }
        else
        {
            flow_.value = previousFlow;
            flowOOR_++;
        }
        flow_.updateTime = currentUpdateTime_;
    }
    if (isPressureRequested_)
    {
        pressure  = *pByte++ << 7;
        pressure += *pByte++;
        pressure -= PRESSURE_OFFSET;
        tempPressure =  pressure * PRESSURE_RESOLUTION;
        if (tempPressure >= AIRWAY_PRESSURE_LOWER_BOUND && tempPressure <= AIRWAY_PRESSURE_UPPER_BOUND)
        {
            pressure_.value = tempPressure;
            previousPressure = tempPressure;
            airwayPressureOOR_ = 0;
        }
        else
        {
            pressure_.value = previousPressure;
            airwayPressureOOR_++;

        }
        pressure_.updateTime = currentUpdateTime_;
    }
    if (isCO2Requested_)
    {
        co2  = *pByte++ << 7;
        co2 += *pByte++;
        co2 -= CO2_OFFSET;
        co2_.value = co2 * CO2_RESOLUTION;
        // if the reading is less than zero but within the accuracy spec (2 mmHg)
        // then zero the reading
        if (co2_.value < 0.0 && co2_.value > -2.0)
        {
            co2_.value = 0.0;
        }
        co2_.updateTime = currentUpdateTime_;
    }
    if (isVolumeRequested_)
    {
        volume  = *pByte++ << 7;
        volume += *pByte++;
        volume -= VOLUME_OFFSET;
        volume_.value = volume * volumeResolution_;
        volume_.updateTime = currentUpdateTime_;
    }
    if (isAccumPressureRequested_)
    {
        accumPressure  = *pByte++ << 7;
        accumPressure += *pByte++;
        accumPressure -= ACCUM_PRESSURE_OFFSET;
        accumlatorPressure_.value = accumPressure * ACCUM_PRESSURE_RESOLUTION;
        accumlatorPressure_.updateTime = currentUpdateTime_;
    }


    isDataFresh_ = TRUE;

    unsigned char dpih = *pByte;

    int packetCounter = (dpih & 0x78) >> 3;
    if (packetCounter != expectedPacketCount_)
    {
        // waveforms missed is a minimum number of waveforms missed
        // the total number is indeterminant since the incoming packet
        // count is limited to a range of 0..15
        ++missedWaveformPkts_;
    }
    expectedPacketCount_ = (packetCounter + 1) & 0xf;

    while (pByte < msg + pktSize - 2)
    {
        short dataParameterIndex = ((pByte[0] & 0x07) << 8) + pByte[1];
        pByte += 2;

        switch (dataParameterIndex)
        {
        case Mercury::NO_PACKET_DATA_AVAIL:
            break;
        case Mercury::FLOW_STATUS:
            flowStatusByte1_ = *pByte;
            flowStatus_.data = (pByte[0] << 8) + pByte[1];
            pByte += 2;
            flowStatus_.updateTime = currentUpdateTime_;
            sensorType_ = Mercury::SensorType(flowStatus_.data & Mercury::FLOW_SENSOR_TYPE_MASK);
            break;
        case Mercury::BAROMETRIC_PRESSURE:
            // If Prox demo mode is in progress
            // do not update the barometric pressure
            // since it has fake values.
            if (!isProxInDemoMode_)
            {
                barometricPressure_.value =  (Real32) ((pByte[0] << 7) + pByte[1]);
                barometricPressure_.updateTime = currentUpdateTime_;
            }
            pByte += 2;
            break;
        case Mercury::CURRENT_RESERVOIR_PRESSURE: 
            // data is expected to be of integer format with a resolution of 100
            currentResevoirPressure_.value = ((pByte[0] << 7) + pByte[1]) / 100.0f;
            pByte += 2;
            currentResevoirPressure_.updateTime = currentUpdateTime_;
            break;
        case Mercury::ENHANCED_PURGE_STATUS:  //3 byte purge status
            purgeStatus_.byte1 = *pByte++;
            purgeStatus_.byte2 = *pByte++;
            purgeStatus_.byte3 = *pByte++;
            purgeStatus_.updateTime = currentUpdateTime_;
            break;

        case Mercury::REPORTED_ZERO_INTERVAL: //2 bytes
            reportedZeroTime_.data = (pByte[0] << 7) + pByte[1];
            pByte += 2;
            reportedZeroTime_.updateTime = currentUpdateTime_;

            break;

#if defined(VCO2_STUDY)
        case Mercury::CAPNOSTAT5_CO2_PRIORITY_STATUS:
            if (previousCapnostatStatus_ == Mercury::CO2_SENSOR_WARM_UP 
                && *pByte == Mercury::CO2_SENSOR_OK)
            {
                isCapnostatZeroed_ = FALSE;
                isCapnostatZeroRequested_ = TRUE;
            }
            else if (previousCapnostatStatus_ == Mercury::CO2_SENSOR_OK
                     && *pByte == Mercury::CO2_ZERO_IN_PROGRESS)
            {
                // zero in progress
            }
            else if (previousCapnostatStatus_ == Mercury::CO2_ZERO_IN_PROGRESS
                     && *pByte == Mercury::CO2_SENSOR_OK)
            {
                isCapnostatZeroed_ = TRUE;
            }
            capnostatStatus_.data = *pByte;
            capnostatStatus_.updateTime = currentUpdateTime_;
            previousCapnostatStatus_ = *pByte++;
            break;
        case Mercury::START_OF_INSPIRATION_MARK:
            startOfInspirationMark_.value = (pByte[0] * 128 + pByte[1]) / 1000.0; 
            pByte += 2;
            startOfInspirationMark_.updateTime = currentUpdateTime_;
            break;
        case Mercury::START_OF_EXPIRATION_MARK:
            startOfExpirationMark_.value = (pByte[0] * 128 + pByte[1]) / 1000.0; 
            pByte += 2;
            startOfExpirationMark_.updateTime = currentUpdateTime_;
            break;
        case Mercury::INSPIRATORY_VOLUME:
            if (pByte[0] == 0x7f && pByte[1] == 0x7f && pByte[2] == 0x7f)
            {
                inspiratoryVolume_.state = DATA_INVALID;
            }
            else if (pByte[0] & 0x40)
            {
                inspiratoryVolume_.state = DATA_QUESTIONABLE;
            }
            else
            {
                inspiratoryVolume_.state = DATA_VALID;
            }
            inspiratoryVolume_.value = ((pByte[0] & 0x1f) * 16384 + pByte[1] * 128 + pByte[2]) / 10.0; 
            pByte += 3;
            inspiratoryVolume_.updateTime = currentUpdateTime_;
            printf("inspiratoryVolume = %d", inspiratoryVolume_.value);
            break;
        case Mercury::EXPIRATORY_VOLUME:
            if (pByte[0] == 0x7f && pByte[1] == 0x7f && pByte[2] == 0x7f)
            {
                expiratoryVolume_.state = DATA_INVALID;
            }
            else if (pByte[0] & 0x40)
            {
                expiratoryVolume_.state = DATA_QUESTIONABLE;
            }
            else
            {
                expiratoryVolume_.state = DATA_VALID;
            }
            expiratoryVolume_.value = ((pByte[0] & 0x1f) * 16384 + pByte[1] * 128 + pByte[2]) / 10.0; 
            pByte += 3;
            expiratoryVolume_.updateTime = currentUpdateTime_;
            break;
        case Mercury::END_TIDAL_CO2:
            if (pByte[1] == 0x7f && pByte[2] == 0x7f)
            {
                endTidalCO2_.state = DATA_INVALID;
            }
            else if (pByte[0] & 0x40)
            {
                endTidalCO2_.state = DATA_QUESTIONABLE;
            }
            else
            {
                endTidalCO2_.state = DATA_VALID;
            }
            endTidalCO2_.value = ((pByte[1] & 0x1f) * 128 + pByte[2]) / 10.0; 
            pByte += 3;
            endTidalCO2_.updateTime = currentUpdateTime_;
            break;
        case Mercury::INSPIRED_CO2:
            if (pByte[1] == 0x7f && pByte[2] == 0x7f)
            {
                inspiredCO2_.state = DATA_INVALID;
            }
            else if (pByte[0] & 0x40)
            {
                inspiredCO2_.state = DATA_QUESTIONABLE;
            }
            else
            {
                inspiredCO2_.state = DATA_VALID;
            }
            inspiredCO2_.value = ((pByte[1] & 0x1f) * 128 + pByte[2]) / 10.0; 
            pByte += 3;
            inspiredCO2_.updateTime = currentUpdateTime_;
            break;
        case Mercury::VOLUME_CO2_EXPIRED_PER_MINUTE: 
            if (pByte[0] == 0x7f && pByte[1] == 0x7f && pByte[2] == 0x7f)
            {
                volumeCO2ExpiredPerMinute_.state = DATA_INVALID;
            }
            else if (pByte[0] & 0x40)
            {
                volumeCO2ExpiredPerMinute_.state = DATA_QUESTIONABLE;
            }
            else
            {
                volumeCO2ExpiredPerMinute_.state = DATA_VALID;
            }
            volumeCO2ExpiredPerMinute_.value = ((pByte[0] & 0x1f) * 16384 + pByte[1] * 128 + pByte[2]) / 10.0; 
            pByte += 3;
            volumeCO2ExpiredPerMinute_.updateTime = currentUpdateTime_;
            break;
        case Mercury::MIXED_EXPIRED_CO2:
            if (pByte[1] == 0x7f && pByte[2] == 0x7f)
            {
                mixedExpiredCO2_.state = DATA_INVALID;
            }
            else if (pByte[1] & 0x40)
            {
                mixedExpiredCO2_.state = DATA_QUESTIONABLE;
            }
            else
            {
                mixedExpiredCO2_.state = DATA_VALID;
            }
            mixedExpiredCO2_.value = ((pByte[1] & 0x1f) * 128 + pByte[2]) / 10.0; 
            pByte += 3;
            mixedExpiredCO2_.updateTime = currentUpdateTime_;
            break;
        case Mercury::INSPIRED_TIME:
            if (pByte[0] == 0x7f && pByte[1] == 0x7f)
            {
                inspiredTime_.state = DATA_INVALID;
            }
            else
            {
                inspiredTime_.state = DATA_VALID;
            }
            inspiredTime_.value = ((pByte[0] & 0x1f) * 128 + pByte[1]) / 100.0; 
            pByte += 2;
            inspiredTime_.updateTime = currentUpdateTime_;
            break;
        case Mercury::EXPIRED_TIME:
            if (pByte[0] == 0x7f && pByte[1] == 0x7f)
            {
                expiredTime_.state = DATA_INVALID;
            }
            else
            {
                expiredTime_.state = DATA_VALID;
            }
            expiredTime_.value = ((pByte[0] & 0x1f) * 128 + pByte[1]) / 100.0; 
            pByte += 2;
            expiredTime_.updateTime = currentUpdateTime_;
            break;
#endif // defined(VCO2_STUDY)

        default:
            // unrecognized DPI so advance the message pointer to the end of the
            // packet to prevent us from processing random data as DPIs
            pByte = msg + pktSize - 2;
            break;
        }
    }

    ++totalWaveformPkts_;

}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  processCmd_( Byte * buffer)
//
//@ Interface-Description
//  Processes serial data stream from prox interface
//  msg is pointer to start of serial stream
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void ProxiInterface::processCmd_( Byte * msg)
{

    unsigned char& cmd = msg[CMD_INDEX];
    unsigned char& nbf = msg[NBF_INDEX];
    unsigned char& wmv = msg[4];
    unsigned char& wmb1 = msg[5];
    unsigned char& wmb2 = msg[6];
    int pktSize = 2 + nbf;

    currentUpdateTime_.now();

    // process the command
    switch (cmd)
    {
#if defined(VCO2_STUDY)
    case Mercury::CAPNOSTAT5_ROOM_AIR_ZERO:
        capnostatZsb_.data = msg[3];
        capnostatZsb_.updateTime = currentUpdateTime_;
        break;
#endif
    case Mercury::GET_SET_INSTRUMENT_SETTINGS:
        {
            processGetAndSetPkt_(msg,pktSize);
        }
        break;
    case Mercury::GET_SOFTWARE_REVISION:
        {
            unsigned char* pFrom = msg + 3;
            char* pString = mainRevisionStr_;
            for (int ix=0; ix < MIN_VALUE(nbf-2,(Uint8)sizeof(mainRevisionStr_)-1); ix++)
            {
                *pString++ = *pFrom++;
            }

            *pString = '\0'; //null-terminate the string

            mainRevision_.pStr = mainRevisionStr_;
            mainRevision_.updateTime = currentUpdateTime_;
            isRevStrReady_ = TRUE;
        }
        break;
    case Mercury::WAVEFORM_AND_PARAMETER_DATA:
        {
            processWaveformPkt_(msg, pktSize);
        }
        break;
    case Mercury::START_WAVEFORM_MODE:
        {
            if (wmv == 1) // Waveform mode valid
            {
                // let the receiver know what data to expect
                isFlowRequested_          = wmb2 & Mercury::FLOW_WAVEFORM;
                isPressureRequested_      = wmb2 & Mercury::PRESSURE_WAVEFORM;
                isVolumeRequested_        = wmb2 & Mercury::VOLUME_WAVEFORM;
                isCO2Requested_           = wmb2 & Mercury::CO2_WAVEFORM;
                isAccumPressureRequested_ = wmb1 & Mercury::ACCUM_PRESSURE_WAVEFORM;
            }
        }
        break;
    case Mercury::GET_SET_AIRWAY_GAS_CONDITIONS:
    case Mercury::STOP_CONTINUOUS_MODE:
        {
            // do nothing for now
        }
        break;
    case Mercury::NACK_ERROR:
        {
            // ignore for now
        }
        break;
    default:
        badHdrs_ = cmd;
        break;
    }

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fixUpbuffer_
//
//@ Interface-Description
//  copy the left over data to the start of the buffer.  If data begins
//  to overflow input buffer for the prox interface data is moved to 
//  the begining of buffer.  Extra padding is accounted for at the end
//  of the buffer to allow for this.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::fixUpbuffer_(void)
{
    // copy the left over data to the start of the buffer
    if (numCmds_ >= NUMBUFFSMASK)
    {
        if (totalBytesRead_ > 0)
        {
            memcpy(&portBuffer_[0],  &portBuffer_[cmdDex_ + pktSize_], totalBytesRead_);
            nextDex_ = totalBytesRead_;
        }
        else
        {
            nextDex_ = 0;
        }
        cmdDex_ = 0;
        numCmds_ = 0;
    }
    else
    {
        cmdDex_ = cmdDex_ + pktSize_;
    }

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//  Called every 5ms by main BD cycle to process prox serial data streams
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::newCycle(void)
{

    // Check if the Prox board is physically installed.
    // If it doesn't exist, return.
    if (!isProxBoardInstalled_)
    {
        return;
    }

    if (TaskControlAgent::GetBdState() != STATE_SST)
    {
        if (++currentCycle_ < nextReadCycle_)
        {
            isDataFresh_ = FALSE;
            return;
        }
    }
    else
    {
        isDataFresh_ = FALSE;
    }

    // sensor updates waveforms at 10ms cycles so no need to update for another 10ms 
    nextReadCycle_ = currentCycle_ + 2;

    // get contents of logio buffer
    bytesRead_ = read(&portBuffer_[nextDex_], LOGIO_CHARBUF_SIZE, 0);     

    // no data to process
    if (bytesRead_ <= 0)
    {
        return;
    }

    nextDex_ += bytesRead_;
    totalBytesRead_ += bytesRead_;

    maxRead_ = MAX_VALUE((Uint32)maxRead_, bytesRead_);

    // protect against getting serial data with no commands
    // do not allow cmdDex_ to increment past end of buffer
    // reset all buffer variables 
    if (cmdDex_ >= PORTBUFFERSIZE - MAX_PKT_SIZE)
    {
        cmdDex_  = 0;
        numCmds_ = 0;
        nextDex_ = 0;
        nextReadCycle_ = 0;
        currentCycle_ = 0;
        // indicate no good data coming in
        ++invalidChecksums_;
        return;
    }

    while (totalBytesRead_ >= MIN_PKT_SIZE)
    {
        // find the first command response
        if (!commandFound_)
        {
            while (totalBytesRead_ >= MIN_PKT_SIZE)
            {
                if ((portBuffer_[cmdDex_] & PROX_CMD_MASK) &&
                    !(portBuffer_[cmdDex_ + NBF_INDEX] & PROX_CMD_MASK)
                   )
                {
                    commandFound_ = TRUE;
                    break;
                }
                totalBytesRead_--;
                cmdDex_++;
            }
        }

        if (!nbfFound_)
        {
            if (totalBytesRead_ < MIN_PKT_SIZE)
            {
                ++shortPkts_;
                nextReadCycle_ = currentCycle_ + 1;
                break;
            }

            nbfFound_ = TRUE;
            pktSize_ = MIN_VALUE(portBuffer_[cmdDex_ + NBF_INDEX] + 2, MAX_PKT_SIZE);
        }

        if (totalBytesRead_ < pktSize_)
        {
            ++shortPkts_;
            nextReadCycle_ = currentCycle_ + 1;
            break;
        }

        totalBytesRead_ -= pktSize_;
        commandFound_ = FALSE;
        nbfFound_ = FALSE;

        // numCmds_ goes from  0 to NUMBUFFSMASK
        numCmds_ = (numCmds_ + 1) & NUMBUFFSMASK;

        if (isChecksumValid_(&portBuffer_[cmdDex_], pktSize_))
        {
            processCmd_(&portBuffer_[cmdDex_]);
        }
        else
        {
            ++invalidChecksums_;
        }

        fixUpbuffer_();

    } // while (totalBytesRead_ >= MIN_PKT_SIZE) ... 
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetModule_
//
//@ Interface-Description
//  Sends a reset module command to prox interface
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::resetModule_(void)
{
    static const int NUMBYTES = 3;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    msg[CMD_INDEX] = Mercury::RESET_MERCURY_MODULE;
    msg[NBF_INDEX] = NBF;       // NBF - number of bytes to follow this byte
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: copyMainRevStr
//
//@ Interface-Description
//  Makes a copy of the main revision string
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ProxiInterface::copyMainRevStr(BigSerialNumber& mainRevStr)
{
    BigSerialNumber revStr(mainRevision_.pStr);
    mainRevStr = revStr;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSoftwareRevision_
//
//@ Interface-Description
//  Sends command to prox interface to request software revision.
//  Serial data is built once for efficiency.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::getSoftwareRevision_(const Mercury::RevisionFormat revisionFormat)
{
    static const int NUMBYTES = 4;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    msg[CMD_INDEX] = Mercury::GET_SOFTWARE_REVISION; //$CA
    msg[NBF_INDEX] = NBF;                       // NBF - number of bytes to follow this byte
    msg[2] = revisionFormat;                 // Revision Format = 0 (Main Software Revision)
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFromProxIE
//
//@ Interface-Description
// Return a boolean to represent I/E from Prox, requires nothing
// mirrors I/E that is sent from ventilator
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//	None
//@ End-Method
//=====================================================================
Boolean ProxiInterface::getFromProxIE(void)
{
    // I/E is represeted by bit 0
    return( purgeStatus_.byte2 & PURGE_ENABLE_HIGH_MASK );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getOEMId_
//
//@ Interface-Description
//  Requests OEM id from prox.  Command is built once for effiency.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::getOEMId_(void)
{
    static const int NUMBYTES = 4;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;   
    msg[NBF_INDEX] = NBF;                   // NBF - number of bytes to follow this byte
    msg[2] = Mercury::OEM_ID;                                
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSensorType
//
//@ Interface-Description
//  returns proximal sensor type
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Mercury::SensorType ProxiInterface::getSensorType(void)
{
    return( sensorType_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAirwayGasConditions_
//
//@ Interface-Description
//  Setup up airway gas conditions for prox board.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setAirwayGasConditions_(void)
{
    static const int NUMBYTES = 11;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    isGasConditionsTransmitting_ =  TRUE;

    SYSLOG((LOG_DEBUG, "Sending Airway Conditions"));

    msg[CMD_INDEX] = Mercury::GET_SET_AIRWAY_GAS_CONDITIONS;
    msg[NBF_INDEX] = NBF;                // NBF - number of bytes to follow this byte
    msg[2] = 0;
    msg[3] = transferGasConditionBuffer_.fio2; // FiO2 - monitored value (monitored)
    msg[4] = 0;                  // anesthetic agent HB
    msg[5] = 0;                  // anesthetic agent LB
    msg[6] = 0;                  // Balance Gas Byte (0= N2, 1 = N2O, 2 = Helium)
    msg[7] = transferGasConditionBuffer_.inspHumidity;       // inspiratory humidity (setting)
    msg[8] = transferGasConditionBuffer_.inspGasTemperature;  // inspiratory temperature
    msg[9] = transferGasConditionBuffer_.expGasParam;         // expiratory gas temp/humidity - exp gas equals insp gas
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);

    isGasConditionsTransmitting_ = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startContinuousMode_
//
//@ Interface-Description
//  Sends start of wave form message to prox
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::startContinuousMode_(void)
{
    unsigned char msg[MAX_PKT_SIZE];

    int ix = 0;

    msg[ix++] = Mercury::START_WAVEFORM_MODE;   //0x90
    msg[ix++] = 0;                               // NBF - set after building command
    msg[ix++] = 1;                          //0x01
    msg[ix++] = Mercury::W100D100;          //0x02
    //msg[ix++] = Mercury::W200D200; 

    SYSLOG((LOG_DEBUG, "Sending START ContinousMode"));

    if (TaskControlAgent::GetBdState() == STATE_SST)
    {
        msg[ix++] = Mercury::WMB1_ACC;         // also includes Accum pressure this will consume a waveform entry
    }
    else
    {
        msg[ix++] = Mercury::WMB1;
    }
#if defined(VCO2_STUDY)
    msg[ix++] = Mercury::FLOW_WAVEFORM | Mercury::PRESSURE_WAVEFORM | Mercury::CO2_WAVEFORM;

    ix += addWord_(&msg[ix], Mercury::REPORTED_ZERO_INTERVAL);
    ix += addWord_(&msg[ix], Mercury::ENHANCED_PURGE_STATUS);
    ix += addWord_(&msg[ix], Mercury::CURRENT_RESERVOIR_PRESSURE);
    ix += addWord_(&msg[ix], Mercury::BAROMETRIC_PRESSURE);
    ix += addWord_(&msg[ix], Mercury::INSPIRATORY_VOLUME);
    ix += addWord_(&msg[ix], Mercury::EXPIRATORY_VOLUME);
    ix += addWord_(&msg[ix], Mercury::START_OF_INSPIRATION_MARK);
    ix += addWord_(&msg[ix], Mercury::START_OF_EXPIRATION_MARK);
    ix += addWord_(&msg[ix], Mercury::INSPIRED_TIME);
    ix += addWord_(&msg[ix], Mercury::EXPIRED_TIME);
    ix += addWord_(&msg[ix], Mercury::CAPNOSTAT5_CO2_PRIORITY_STATUS);
    ix += addWord_(&msg[ix], Mercury::VOLUME_CO2_EXPIRED_PER_MINUTE);
    ix += addWord_(&msg[ix], Mercury::END_TIDAL_CO2);
    ix += addWord_(&msg[ix], Mercury::INSPIRED_CO2);
    ix += addWord_(&msg[ix], Mercury::MIXED_EXPIRED_CO2);
#else
    //msg[ix++] = Mercury::FLOW_WAVEFORM | Mercury::PRESSURE_WAVEFORM | Mercury::VOLUME_WAVEFORM;
    msg[ix++] = Mercury::FLOW_WAVEFORM | Mercury::PRESSURE_WAVEFORM;

    ix += addWord_(&msg[ix], Mercury::REPORTED_ZERO_INTERVAL);
    ix += addWord_(&msg[ix], Mercury::ENHANCED_PURGE_STATUS);
    ix += addWord_(&msg[ix], Mercury::CURRENT_RESERVOIR_PRESSURE);

    if (TaskControlAgent::GetBdState() == STATE_SST)
    {
        ix += addWord_(&msg[ix], Mercury::BAROMETRIC_PRESSURE);
    }
#endif


    msg[NBF_INDEX] = ix - 1;                // NBF - number of bytes to follow this byte
    addChecksum_(ix++, msg);

    sendPacket_(msg, ix);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enablePurge_
//
//@ Interface-Description
//  Enable custom purge on the prox board.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::enablePurge_(void)
{
    SYSLOG((LOG_DEBUG, "Resuming Custom Purge"));

    static const int NUMBYTES = 5;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    msg[CMD_INDEX] = Mercury::CUSTOM_PURGE;  
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = 0;
    msg[3] = Mercury::RESUME_CUSTOM_PURGE;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disablePurge_
//
//@ Interface-Description
//  Disable custom purge on the prox board.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::disablePurge_(void)
{
    SYSLOG((LOG_DEBUG, "Suspending Custom Purge"));

    static const int NUMBYTES = 5;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    msg[CMD_INDEX] = Mercury::CUSTOM_PURGE;  
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = 0;
    msg[3] = Mercury::SUSPEND_CUSTOM_PURGE;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: stopContinuousMode_
//
//@ Interface-Description
//  Send prox board command to stop wave form packets
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::stopContinuousMode_(void)
{
    static const int NUMBYTES = 3;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    SYSLOG((LOG_DEBUG, "Sending STOP ContinousMode"));

    msg[CMD_INDEX] = Mercury::STOP_CONTINUOUS_MODE;      // 0xC9
    msg[NBF_INDEX] = NBF;                   // NBF - number of bytes to follow this byte
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: suspend
//
//@ Interface-Description
//  Sets suspendRequest_
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::suspend(void)
{
    suspendRequest_ =  TRUE ;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resume
//
//@ Interface-Description
// Sets resumeRequest_
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::resume(void)
{
    resumeRequest_ =  TRUE ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: suspendPurge
//
//@ Interface-Description
// Sets suspendPurgeRequest_
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::suspendPurge(void)
{
    suspendPurgeRequest_ =  TRUE ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resumePurge
//
//@ Interface-Description
// Sets resumePurgeRequest_
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::resumePurge(void)
{
    resumePurgeRequest_ =  TRUE ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setManualPurge
//
//@ Interface-Description
//  Sends prox board a manual purge request.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setManualPurge_(PurgeMode mode)
{
    static const int NUMBYTES = 6;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    char db1;
    char db2;

    SYSLOG((LOG_DEBUG, " Setting Manual Purge Mode: %d", mode));

    if (mode == PROX_AUTO_PURGE_MODE)
    {
        db1 = 01;
        db2 = 01;
    }
    else
    {
        db1 = 02;
        db2 = 02;
    }

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xCE
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::ZERO_PURGE_MODE; 
    msg[3] = db1;
    msg[4] = db2;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: verifyPurgeParams
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32 ProxiInterface::verifyPurgeParams(PurgeParams &params)
{
    Uint32 missing = 0;
    Uint16 index;

    for (index = Mercury::MIN_PURGE_ISB; index < Mercury::MAX_PURGE_ISB ; index++)
    {
        switch (index)
        {
        case Mercury::ZERO_PURGE_MODE:
            if (params.mode != purgeParams_.mode)
            {
                missing |= 0x01;  
            }
            break;
        case Mercury::MAX_INPUT_LINE_WAIT_TIME:
            if (IsEquivalent(params.maxWaitTime, purgeParams_.maxWaitTime, HUNDREDTHS))
            {
                missing |= 0x02;
            }
            break;
        case Mercury::RESERVOIR_TARGET_PRESSURE:
            if (IsEquivalent(params.resTargetPressure,purgeParams_.resTargetPressure, HUNDREDTHS))
            {
                missing |= 0x04;
            }
            break;
        case Mercury::RESERVOIR_MAX_PRESSURE:
            if (IsEquivalent(params.resMaxPressure, purgeParams_.resMaxPressure, HUNDREDTHS))
            {
                missing |= 0x08;
            }
            break;
        case Mercury::CYCLE_REPETITION_COUNT:
            if (params.cycleRepCount != purgeParams_.cycleRepCount)
            {
                missing |= 0x10;
            }
            break;
        case Mercury::RELEASE_VALVE_OPEN_TIME:
            if (IsEquivalent(params.releaseValveOpenTime, purgeParams_.releaseValveOpenTime,THOUSANDTHS ))
            {
                missing |= 0x20;
            }
            break;
        case Mercury::RELEASE_VALVE_CLOSE_TIME:
            if (IsEquivalent(params.releaseValveCloseTime, purgeParams_.releaseValveCloseTime, THOUSANDTHS))
            {
                missing |= 0x40;
            }
            break;
        case Mercury::PURGE_INTERVAL_TIME:
            if (params.intervalTime != purgeParams_.intervalTime)
            {
                missing |= 0x80;
            }
            break;
        case Mercury::RESERVOIR_PRESSURE_TRANSDUCER_OFFSET:
            if (params.resPressureTransOffset != purgeParams_.resPressureTransOffset)
            {
                ;// missing |= 0x200;
            }
            break;
        case Mercury::MAX_CHARGE_TIME:
            if (IsEquivalent(params.maxChargeTime, purgeParams_.maxChargeTime, HUNDREDTHS))
            {
                missing |= 0x400;
            }
            break;

        default:
            break;


        }
    }

    return(missing);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateGasConditions
//
//@ Interface-Description
//  Updates the gas conditions in use.  Typically called when condtions change.
//  (i.e. when user changes O2 etc)
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::updateGasConditions( GasConditions &conditions )
{
    Boolean isBusy = TRUE;
    if (!isGasConditionsTransmitting_)
    {
        transferGasConditionBuffer_.fio2 = conditions.fio2;
        transferGasConditionBuffer_.anesthetic = conditions.anesthetic;
        transferGasConditionBuffer_.gasBalance = conditions.gasBalance;
        transferGasConditionBuffer_.inspHumidity = conditions.inspHumidity;
        transferGasConditionBuffer_.inspGasTemperature = conditions.inspGasTemperature;
        transferGasConditionBuffer_.expGasParam = conditions.expGasParam;

        isGasConditionUpdateNeeded_ = TRUE;
        isBusy = FALSE;
    }

    return(isBusy);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePurgeParams
//
//@ Interface-Description
//  Updates the purge parameters to be used in prox
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::updatePurgeParams(PurgeParams &params , Boolean forceAll)
{
    Boolean isBusy = TRUE;
    if (!isPurgeParamsTransmitting_)
    {

        transferPurgeBuffer_.cycleRepCount = params.cycleRepCount;
        transferPurgeBuffer_.mode = params.mode;
        transferPurgeBuffer_.maxWaitTime = params.maxWaitTime;
        transferPurgeBuffer_.resTargetPressure = params.resTargetPressure;
        transferPurgeBuffer_.resMaxPressure = params.resMaxPressure;
        transferPurgeBuffer_.releaseValveOpenTime = params.releaseValveOpenTime;
        transferPurgeBuffer_.releaseValveCloseTime = params.releaseValveCloseTime;
        transferPurgeBuffer_.intervalTime = params.intervalTime;
        transferPurgeBuffer_.resPressureTransOffset = params.resPressureTransOffset;
        transferPurgeBuffer_.maxChargeTime = params.maxChargeTime;

        isUpdateNeeded_ = TRUE;
        isBusy = FALSE;
        forceDownAllPurgeParams_ = forceAll;
    }

    return(isBusy);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: requestManualPurge
//
//@ Interface-Description
//  When requesting manual purge target pressure and max target pressure
//  are set.  Because the prox board does not employ sofisticated
//  control algorithms max pressure is really not useful.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::requestManualPurge(Real32 targetPressure, Real32 maxPressure)
{
    manualTargetPressure_ = targetPressure;
    manualMaxPressure_ =  maxPressure;
    isManualPurgeRequested_ = TRUE;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: executeManualPurge_
//
//@ Interface-Description
//  Performs steps to perform a manual purge
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::executeManualPurge_( void )
{
    disablePurge_();
    Task::Delay(1);

    setResTargetPressure_(manualTargetPressure_);
    Task::Delay(1);

    setResMaxPressure_(manualMaxPressure_);
    Task::Delay(1);

    enablePurge_();

    transmitDumpAccumalator_(TRUE);

    //worst case i/E 30 seconds
    Task::Delay(30);

    //restore 
    disablePurge_();
    Task::Delay(1);

    setResTargetPressure_(transferPurgeBuffer_.resTargetPressure);
    Task::Delay(1);

    setResMaxPressure_(transferPurgeBuffer_.resMaxPressure);
    Task::Delay(1);

    enablePurge_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: transmitPurgeParams_
//
//@ Interface-Description
//  transmits purge paramaters to prox interface
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::transmitPurgeParams_(void)
{
    Uint16 index;


    isPurgeParamsTransmitting_ = TRUE;

    disablePurge_();
    Task::Delay(1);

    for (index = Mercury::MIN_PURGE_ISB; index < Mercury::MAX_PURGE_ISB ; index++)
    {
        Task::Delay(1);
        switch (index)
        {
        case Mercury::ZERO_PURGE_MODE:
            if ((transferPurgeBuffer_.mode != purgeParams_.mode) || forceDownAllPurgeParams_)
            {
                setManualPurge_(transferPurgeBuffer_.mode);
            }
            break;
        case Mercury::MAX_INPUT_LINE_WAIT_TIME:
            if (IsEquivalent(transferPurgeBuffer_.maxWaitTime, purgeParams_.maxWaitTime, HUNDREDTHS) || forceDownAllPurgeParams_)
            {
                setMaxInputLineWait_(transferPurgeBuffer_.maxWaitTime);
            }
            break;
        case Mercury::RESERVOIR_TARGET_PRESSURE:
            if (IsEquivalent(transferPurgeBuffer_.resTargetPressure,purgeParams_.resTargetPressure, HUNDREDTHS) || forceDownAllPurgeParams_)
            {
                setResTargetPressure_(transferPurgeBuffer_.resTargetPressure);
            }
            break;
        case Mercury::RESERVOIR_MAX_PRESSURE:
            if (IsEquivalent(transferPurgeBuffer_.resMaxPressure, purgeParams_.resMaxPressure, HUNDREDTHS)|| forceDownAllPurgeParams_)
            {
                setResMaxPressure_(transferPurgeBuffer_.resMaxPressure);
            }
            break;
        case Mercury::CYCLE_REPETITION_COUNT:
            if ((transferPurgeBuffer_.cycleRepCount != purgeParams_.cycleRepCount) || forceDownAllPurgeParams_)
            {
                setCycleRepCount_(transferPurgeBuffer_.cycleRepCount);
            }
            break;
        case Mercury::RELEASE_VALVE_OPEN_TIME:
            if (IsEquivalent(transferPurgeBuffer_.releaseValveOpenTime, purgeParams_.releaseValveOpenTime,THOUSANDTHS ) || forceDownAllPurgeParams_)
            {
                setValveOpenTime_(transferPurgeBuffer_.releaseValveOpenTime);
            }
            break;
        case Mercury::RELEASE_VALVE_CLOSE_TIME:
            if (IsEquivalent(transferPurgeBuffer_.releaseValveCloseTime, purgeParams_.releaseValveCloseTime, THOUSANDTHS) || forceDownAllPurgeParams_)
            {
                setValveCloseTime_(transferPurgeBuffer_.releaseValveCloseTime);
            }
            break;
        case Mercury::PURGE_INTERVAL_TIME:
            if ((transferPurgeBuffer_.intervalTime != purgeParams_.intervalTime)|| forceDownAllPurgeParams_)
            {
                setIntervalTime_(transferPurgeBuffer_.intervalTime);
            }
            break;
        case Mercury::MAX_CHARGE_TIME:
            if (IsEquivalent(transferPurgeBuffer_.maxChargeTime, purgeParams_.maxChargeTime, HUNDREDTHS) || forceDownAllPurgeParams_)
            {
                setMaxChargeTime_(transferPurgeBuffer_.maxChargeTime);
            }
            break;
        default:
            break;
        }
    }

    enablePurge_();
    forceDownAllPurgeParams_ =  FALSE;
    isPurgeParamsTransmitting_ = FALSE;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableManualPurge_
//
//@ Interface-Description
//  Performs steps to enable manual purges
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::enableManualPurge_()
{
    disablePurge_();
    Task::Delay(1);
    setManualPurge_(PROX_MANUAL_MODE );  
    Task::Delay(1);
    enablePurge_();
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setMaxInputLineWait_
//
//@ Interface-Description
//  Sends instrument settings command for maximum input wait time
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setMaxInputLineWait_(Real32 target)
{
    static const int NUMBYTES = 6;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    SYSLOG((LOG_DEBUG, " Sending Max Line Time: %f", target));

    Uint32  param = target * 100;
    Uint8 db1 = param / 128;
    Uint8 db2 = param % 128;

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::MAX_INPUT_LINE_WAIT_TIME;  
    msg[3] = db1; 
    msg[4] = db2;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setIntervalTime_
//
//@ Interface-Description
//  Sends prox board the desired purge interval to be used
//  Most of command is built once for effiency
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setIntervalTime_(Uint16 target)
{
    static const int NUMBYTES = 6;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    SYSLOG((LOG_DEBUG, " Sending Interval Time: %d ", target ));

    Uint8 db1 = target / 128;
    Uint8 db2 = target % 128;

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::PURGE_INTERVAL_TIME;
    msg[3] = db1; 
    msg[4] = db2;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValveOpenTime
//
//@ Interface-Description
// Build and sends the release value open time message
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setValveOpenTime_(Real32 target)
{
    static const int NUMBYTES = 6;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    SYSLOG((LOG_DEBUG, " Sending Valve Open Time: %f", target));

    Uint32  param = target * 1000;
    Uint8 db1 = param / 128;
    Uint8 db2 = param % 128;

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::RELEASE_VALVE_OPEN_TIME;
    msg[3] = db1; 
    msg[4] = db2;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValveCloseTime
//
//@ Interface-Description
// Build and sends the release value close time message
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setValveCloseTime_(Real32 target)
{
    static const int NUMBYTES = 6;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    SYSLOG((LOG_DEBUG, " Sending Valve Close Time: %f ", target));

    Uint32  param = target * 1000;
    Uint8 db1 = param / 128;
    Uint8 db2 = param % 128;

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::RELEASE_VALVE_CLOSE_TIME;   
    msg[3] = db1; 
    msg[4] = db2;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFlowZeroValveOpenTime
//
//@ Interface-Description
// Build and sends the  FLOW_ZERO_VALVE_OPEN_TIME message
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setFlowZeroValveOpenTime_(Real32 target)
{
    static const int NUMBYTES = 6;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    SYSLOG((LOG_DEBUG, " Sending Flow Zero Time: %f", target));

    Uint32  param = target * 100;
    Uint8 db1 = param / 128;
    Uint8 db2 = param % 128;

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::FLOW_ZERO_VALVE_OPEN_TIME; 
    msg[3] = db1; 
    msg[4] = db2;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setMaxChargeTime
//
//@ Interface-Description
// Build and sends the  MAX_CHARGE_TIME message
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setMaxChargeTime_(Real32 target)
{
    static const int NUMBYTES = 6;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    Uint32  param = target * 100;
    Uint8 db1 = param / 128;
    Uint8 db2 = param % 128;

    SYSLOG((LOG_DEBUG, " Sending Max Purge Time: %f", target));

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::MAX_CHARGE_TIME;  
    msg[3] = db1; 
    msg[4] = db2;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setResMaxPressure_
//
//@ Interface-Description
// Build and sends the RESERVOIR_MAX_PRESSURE message
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setResMaxPressure_(Real32 target)
{
    static const int NUMBYTES = 6;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    Uint32  param = target * 100;
    Uint8 db1 = param / 128;
    Uint8 db2 = param % 128;

    SYSLOG((LOG_DEBUG, " Sending Res Max Pressure: %f", target));

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::RESERVOIR_MAX_PRESSURE; 
    msg[3] = db1; 
    msg[4] = db2;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setResTargetPressure_
//
//@ Interface-Description
// Build and sends the RESERVOIR_TARGET_PRESSURE message
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setResTargetPressure_(Real32 target)
{
    static const int NUMBYTES = 6;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    Uint32  param = (target * 100);
    Uint8 db1 = (param / 128);
    Uint8 db2 = (param % 128);

    SYSLOG((LOG_DEBUG, " Sending Pressure target: %f ", target));

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::RESERVOIR_TARGET_PRESSURE;                          
    msg[3] = db1; 
    msg[4] = db2;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCycleRepCount_
//
//@ Interface-Description
// Build and sends the CYCLE_REPETITION_COUNT message
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setCycleRepCount_(Uint8 count)
{
    static const int NUMBYTES = 5;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    SYSLOG((LOG_DEBUG, " Sending Rep Count %d", count));

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::CYCLE_REPETITION_COUNT;
    msg[3] = count; 
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFlowStatus
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32 ProxiInterface::getFlowStatus(void)
{
    return(flowStatusByte1_);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isPurgeError
//
//@ Interface-Description
//  returns true if purge status returns an error
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isPurgeError(void)
{
    Boolean status = FALSE;
    if (purgeStatus_.byte1 == CUSTOM_PURGE_ERROR)
    {
        status = TRUE;
    }

    return(status);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isPurgeActive
//
//@ Interface-Description
//  returns true if purging activity is active
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isPurgeActive(void)
{
    Boolean status = FALSE;
    if (purgeStatus_.byte1 == CUSTOM_PURGE_DISCHARGING ||
        purgeStatus_.byte1 == CUSTOM_PURGE_SETTLING)
    {
        status = TRUE;
    }

    return(status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getReservoirError
//
//@ Interface-Description
//  returns reservoir status
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint8 ProxiInterface::getReservoirError(void)
{
    Uint8 error = 0;
    //Increase Error  bit 2, DecreaseError bit 3, Inhalation bit 4
    error = (purgeStatus_.byte2 & (RESERVOIR_PRESSURE_INCREASE_ERROR_MASK |
                                   RESERVOIR_PRESSURE_DECREASE_ERROR_MASK | 
                                   RESERVOIR_INHALATION_DISCHARGE_ERROR_MASK ) );
    return(error);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getZeroStatus
//
//@ Interface-Description
//  returns status from auto zero activity
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ProxiInterface::ZeroStatus ProxiInterface::getZeroStatus(void)
{
    ZeroStatus status = ZERO_IDLE ;

    if (purgeStatus_.byte1 == CUSTOM_PURGE_ZERO_INPROGRESS)
    {
        status = ZERO_IN_PROGRESS;
    }
    else if (purgeStatus_.byte2 & 0x02)
    {
        status =  ZERO_REQUIRED;   
    }

    return(status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isZeroActive
//
//@ Interface-Description
//  returns true if auto zero is in progress
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isZeroActive(void)
{
    return(purgeStatus_.byte1 == CUSTOM_PURGE_ZERO_INPROGRESS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isZeroWaiting
//
//@ Interface-Description
//  returns true if prox boards desires to perform a antozero
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isZeroWaiting(void)
{
    return(purgeStatus_.byte1 == CUSTOM_PURGE_WAIT_ZERO_LINE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPurgeErrorByte
//
//@ Interface-Description
//  returns purge error byte from prox board
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint8 ProxiInterface::getPurgeErrorByte(void)
{
    return(purgeStatus_.byte3);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: performZero
//
//@ Interface-Description
// Sets isHostZeroRequested_
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void  ProxiInterface::performZero(void)
{
    isHostZeroRequested_ = TRUE;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dumpAccumalator
//
//@ Interface-Description
// Dumps accumulator, Turns pump off normally
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::dumpAccumalator(Boolean pumpOn)
{
    isDumpAccumalatorRequested_ = TRUE;
    pumpOn_ = pumpOn;

}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: transmitZeroRequest_
//
//@ Interface-Description
//  Transmits autozero request
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::transmitZeroRequest_(void)
{
    static const int NUMBYTES = 5;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    SYSLOG((LOG_DEBUG, "Send request to Zero" ));

    msg[CMD_INDEX] = Mercury::CUSTOM_PURGE;  
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = 0;                               //PDB alway 0
    msg[3] = Mercury::INIT_SYNCED_ZERO;   
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getBoardSerialNumber_
//
//@ Interface-Description
// Builds and sends serial number request message
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::getBoardSerialNumber_(void)
{
    static const int NUMBYTES = 4;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    SYSLOG((LOG_DEBUG, "Requesting Serial Number"));

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;                   // NBF - number of bytes to follow this byte
    msg[2] = Mercury::MODULE_SERIAL_NUMBER;           //Serial Number
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: transmitDumpAccumalator_
//
//@ Interface-Description
// Builds and sends dump accumulator message
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::transmitDumpAccumalator_(Boolean pumpOn)
{
    static const int NUMBYTES = 5;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    Uint8 pmb = Mercury::INIT_DISCHARGE_RESERVOIR;
    if (pumpOn)
    {
        pmb = Mercury::INIT_MANUAL_PURGE;
    }

    SYSLOG((LOG_DEBUG, "Send dump Accum"));

    msg[CMD_INDEX] = Mercury::CUSTOM_PURGE;  
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = 0;                               //PDB alway 0
    msg[3] = pmb;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isProxBoardInstalled
//
//@ Interface-Description
//  returns true if the prox board is installed
//---------------------------------------------------------------------
//@ Implementation-Description
// If the CTS line is 1 board is installed else if it is 0 board is not
// installed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isProxBoardInstalled(void)
{

	//BdSerialPort & rSerialPort = BdSerialInterface::GetSerialPort(BdSerialInterface::SERIAL_PORT_1);
    //isProxBoardInstalled_ = rSerialPort.GetCTS();

    return isProxBoardInstalled_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processRequests_
//
//@ Interface-Description
//  Sets up proxboard and Processes requests from it 
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::processRequests_(void)
{
    OPENLOG(("proxi", LOG_PID, LOG_USER));
    SYSLOG((LOG_INFO, "starting proximal interface"));

    // if no Prox board just sleep for ever
    if (!isProxBoardInstalled_)
    {
        // sleep forever
		for(;;)
        {
            Task::Delay(60,0);
        }
    }

    isProxReady_ = FALSE;
    rSerialPort_.reset();
    rSerialPort_.flushReceiveBuffer();
    Task::Delay(1);
    resetModule_(); 
    Task::Delay(10);  //time required for prox board to come out of reset 
                      // reported by Respironics


#if !defined(VCO2_STUDY)
    Boolean once = TRUE;
    // if we are not in SST and circuit is not NEO sleep as well
    while ( (TaskControlAgent::GetBdState() != STATE_SST)  && 
            PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE) != PatientCctTypeValue::NEONATAL_CIRCUIT)
    {
        SYSLOG((LOG_INFO, "stopContinuousMode_  GetBdState  NEONATAL_CIRCUIT"));
        stopProx_(once);
        once = FALSE;

        // sleep for 60sec
        Task::Delay(60,0);  
    }
#endif // !defined(VCO2_STUDY)

    SYSLOG((LOG_INFO, "isRevStrReady_ 1 = %d", isRevStrReady_));

    int tmr = 0;
    while (*mainRevisionStr_ == '\0')
    {
        SYSLOG((LOG_INFO, "stopContinuousMode_  mainRevisionStr_ "));
        stopContinuousMode_();
        Task::Delay(2);  //allow prox to respond
		disablePurge_(); // Suspend purge
		Task::Delay(1);
        SYSLOG((LOG_DEBUG, "bytes=%d, maxread=%d, wfpkts=%d, wfmiss=%d, ckerrs=%d, shortpkts=%d, lastbadhdr=%02xh, TaskControlAgent::GetBdState()= %d", \
                totalBytesRead_, maxRead_, totalWaveformPkts_, missedWaveformPkts_, invalidChecksums_, shortPkts_, badHdrs_, TaskControlAgent::GetBdState()));

        getSoftwareRevision_(Mercury::MAIN_SOFTWARE_REVISION);
        Task::Delay(2);  //allow prox to respond

        SYSLOG((LOG_INFO, "isRevStrReady_ 2 = %d", isRevStrReady_));

        // restart serial port if we can't get rev string after a while
        if (tmr++ >= 2)
        {
            SYSLOG((LOG_INFO, "trying again to get revision string"));
            tmr = 0;
            rSerialPort_.reset();
            rSerialPort_.flushReceiveBuffer();
        }

        maxRead_ = 0;
    }

    SYSLOG((LOG_INFO, "isRevStrReady_ 3 = %d", isRevStrReady_));
    SYSLOG((LOG_INFO, "starting waveforms"));

    //General Note: commands that have large responses give a delay in order not to over run the LOG_IO buffer.

    getBoardSerialNumber_();

    Task::Delay(1);
    getOEMId_();
    Task::Delay(1);

    setAirwayGasConditions_();

#if defined(VCO2_STUDY)
    Task::Delay(0,100);
    getMercurySoftwareOptions_();
    Task::Delay(0,100);
    setETCO2TimePeriod(Mercury::ETCO2_ONE_BREATH_PERIOD);
    Task::Delay(0,100);
    setVCO2AveragingTime(Mercury::VCO2_ONE_BREATH_AVERAGE);
    Task::Delay(0,100);
#endif

    Int32 cycleCount = 0;

    if (TaskControlAgent::GetBdState() == STATE_SST || 
        TaskControlAgent::GetBdState() == STATE_SERVICE )
    {
        disablePurge_();
    }
    else
    {
        enableManualPurge_();
    }


    for (;;)
    {
#if !defined(VCO2_STUDY)
        Boolean runOnce = TRUE;
        while (PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE) != PatientCctTypeValue::NEONATAL_CIRCUIT)
        {
            stopProx_(runOnce);

            // sleep for 60 secs
            Task::Delay(60,0);  

            runOnce = FALSE;
        }
#endif

        isProxReady_ = TRUE;
        Task::Delay(1);

        if (isModuleResetRequested_)
        {
            resetModule_();
            isModuleResetRequested_ = FALSE;
        }

        if (resumeRequest_)
        {
            startContinuousMode_();
            enablePurge_();
            resumeRequest_ = FALSE;
            isProxSuspended_ = FALSE;
        }

        if (suspendRequest_)
        {
            SYSLOG((LOG_INFO, "stopContinuousMode_  suspendRequest_"));
            stopContinuousMode_();
            disablePurge_();
            suspendRequest_ = FALSE;
            isProxSuspended_ = TRUE;
        }

        if (resumePurgeRequest_)
        {
            enablePurge_();
            resumePurgeRequest_ = FALSE;
        }

        if (suspendPurgeRequest_)
        {
            disablePurge_();
            suspendPurgeRequest_ = FALSE;
        }

        if (isUpdateNeeded_)
        {
            transmitPurgeParams_();
            isUpdateNeeded_ = FALSE;
        }

        if (isDumpAccumalatorRequested_)
        {
            transmitDumpAccumalator_(pumpOn_);
            isDumpAccumalatorRequested_ = FALSE;
        }

        if (isGasConditionUpdateNeeded_)
        {
            //SYSLOG((LOG_DEBUG, "inspiredVolumeFactor = %f | expiredVolumeFactor = %f ", inspiredVolumeFactor_, expiredVolumeFactor_));
            //SYSLOG((LOG_DEBUG, "atmPressure = %f | HME_FACTOR_TEST = %f | HME_FACTOR = %f", atmPressure_, hmeFactorTest_, hmeFactor_));
            //SYSLOG((LOG_DEBUG, "PROX_CORRECTION_FACTOR = %f", proxCorrectionFactor_));

            setAirwayGasConditions_();
            isGasConditionUpdateNeeded_ = FALSE;
        }

        if (enableContinuousMode_)
        {
            startContinuousMode_();
            enableContinuousMode_ = FALSE;
        }

        if (disableContinuousMode_)
        {
            SYSLOG((LOG_INFO, "stopContinuousMode_  disableContinuousMode_"));
            stopContinuousMode_();
            disableContinuousMode_ = FALSE;
        }

        if (isHostZeroRequested_)
        {
            if (!isZeroBusy_)
            {
                isZeroBusy_ = TRUE;
                transmitZeroRequest_();
                isHostZeroRequested_ = FALSE;
                isZeroBusy_ = FALSE;
            }

        }

        if (isManualPurgeRequested_)
        {
            executeManualPurge_();
            isManualPurgeRequested_ = FALSE;
        }

        if (enablePurgeTest_)
        {
            purgeTest_();
            enablePurgeTest_ = FALSE;
        }

        if (disableSquareWaveformDemoMode_)
        {
            setSquareWaveformDemoMode(FALSE);
            disableSquareWaveformDemoMode_ = FALSE;
            Task::Delay(1);
            isProxInDemoMode_ = FALSE;
        }

        if (enableSquareWaveformDemoMode_)
        {
            isProxInDemoMode_ = TRUE;
            setSquareWaveformDemoMode(TRUE);
            enableSquareWaveformDemoMode_ = FALSE;
        }

        if (setProxManualPurge_)
        {
            setManualPurge_(PROX_MANUAL_MODE );  
            setProxManualPurge_ = FALSE;
        }

        if (isGetOEMIdRequested_)
        {
            getOEMId_();
            isGetOEMIdRequested_ = FALSE;
        }

        if ( isPurgeIntervalUpdateRequested_ )
        {
            setIntervalTime_(targetPurgeInterval_);
            isPurgeIntervalUpdateRequested_ = FALSE;
        }

#if defined(VCO2_STUDY)
        if (isCapnostatZeroRequested_)
        {
            zeroCapnostat_();
            isCapnostatZeroRequested_ = FALSE;
        }
#endif

        if (purgeStatus_.reportTime < purgeStatus_.updateTime)
        {
            purgeStatus_.reportTime = purgeStatus_.updateTime;


#if defined( SYSLOG_ENABLED )
            static char byte1 = purgeStatus_.byte1;
            static char byte2 = purgeStatus_.byte2;
            static char byte3 = purgeStatus_.byte3;

            if (byte1 !=  purgeStatus_.byte1 || byte2 != purgeStatus_.byte2 || byte3 != purgeStatus_.byte3)
            {
                byte1 =  purgeStatus_.byte1;
                byte2 =  purgeStatus_.byte2;
                byte3 =  purgeStatus_.byte3;
                SYSLOG((LOG_INFO, "PurgeStatus 1)-> %08x, 2)-> %08x, 3)-> %08x ",purgeStatus_.byte1, purgeStatus_.byte2, purgeStatus_.byte3));
            }
#endif // defined( SYSLOG_ENABLED )
        }

        if (reportedZeroTime_.reportTime < reportedZeroTime_.updateTime)
        {
            reportedZeroTime_.reportTime = reportedZeroTime_.updateTime;
#if defined( SYSLOG_ENABLED )
            static Uint32 rtime = (Uint32)reportedZeroTime_.data;
            if (rtime != reportedZeroTime_.data)
            {
                SYSLOG((LOG_INFO, "AutoZero Time in --> %d", reportedZeroTime_.data));
                rtime = (Uint32) reportedZeroTime_.data;
            }

#endif // defined( SYSLOG_ENABLED )

        }


        if (flowStatus_.reportTime < flowStatus_.updateTime)
        {
            flowStatus_.reportTime = flowStatus_.updateTime;
            co2_.reportTime = co2_.updateTime;


#if defined( SYSLOG_ENABLED )
            static Uint32 fStatus = flowStatus_.data;
            if ( fStatus != flowStatus_.data)
            {
                SYSLOG((LOG_INFO, ">>>> Flow Status: %04xh, Serial # %d ", flowStatus_.data, boardSerialNumber_));
                fStatus = flowStatus_.data;
            }
#endif // defined( SYSLOG_ENABLED )

            switch (Mercury::SensorType(flowStatus_.data & Mercury::FLOW_SENSOR_TYPE_MASK))
            {
            case Mercury::ADULT_FLOW_CO2_SENSOR:
            case Mercury::PEDIATRIC_FLOW_CO2_SENSOR:
            case Mercury::ADULT_FLOW_SENSOR:
                volumeResolution_ = ADULT_VOLUME_RESOLUTION;
                break;
            case Mercury::NEONATAL_FLOW_SENSOR:
            case Mercury::NEONATAL_FLOW_CO2_SENSOR:
                volumeResolution_ = NEO_VOLUME_RESOLUTION;
                break;
            case Mercury::UNPLUGGED_SENSOR:
            case Mercury::INVALID_SENSOR:
                // set device alert condition
                break;
            default:
                ;//Do not assert
            }
        }

        if (++cycleCount % 5 == 0)
        {

#if defined( SYSLOG_ENABLED )
            //SYSLOG((LOG_DEBUG, "bytes=%d, maxread=%d, wfpkts=%d, wfmiss=%d, ckerrs=%d, shortpkts=%d, lastbadhdr=%02xh",\
            //	   totalBytesRead_, maxRead_, totalWaveformPkts_, missedWaveformPkts_, invalidChecksums_, shortPkts_, badHdrs_));
            //SYSLOG((LOG_DEBUG, "inspiredVolumeFactor = %f | expiredVolumeFactor = %f ", inspiredVolumeFactor_, expiredVolumeFactor_));
            //SYSLOG((LOG_DEBUG, "atmPressure = %f | HME_FACTOR_TEST = %f | HME_FACTOR = %f", atmPressure_, hmeFactorTest_, hmeFactor_));
            //SYSLOG((LOG_DEBUG, "PROX_CORRECTION_FACTOR = %f", proxCorrectionFactor_));
#endif // defined( SYSLOG_ENABLED )
            maxRead_ = 0;

            if (flow_.reportTime == flow_.updateTime && !isProxSuspended_)
            {
                // waveforms not updating -- restart the sensor
                SYSLOG((LOG_INFO, "Reseting Serial port & starting proximal sensor"));

                rSerialPort_.reset();
                startContinuousMode_();
                Task::Delay(1);
                enableManualPurge_();
                setAirwayGasConditions_();
            }
            flow_.reportTime = flow_.updateTime;
        }


        if (barometricPressure_.reportTime < barometricPressure_.updateTime)
        {
            barometricPressure_.reportTime = barometricPressure_.updateTime;
            SYSLOG((LOG_INFO, "Patm = %.0f mmHg", getBarometricPressure()));
        }

        if (mainRevision_.reportTime < mainRevision_.updateTime)
        {
            mainRevision_.reportTime = mainRevision_.updateTime;
            SYSLOG((LOG_INFO, "Software Revision: %s", mainRevision_.pStr));
        }

    } // forever loop
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  addChecksum_
//
//@ Interface-Description
// Calculate checksum value from the character buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// Compute the checksum for outgoing packet and append it at the end of 
// the packet before send to the proximal sensor.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void ProxiInterface::addChecksum_(short size, unsigned char buffer[])
{
    char chksum = 0;

    for (short i=0; i < size; i++)
    {
        chksum = (char) (chksum + buffer[i]);
    }

    chksum = (char) ((-chksum) & 0x7F);
    buffer[size] = chksum;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isChecksumValid
//
//@ Interface-Description
//  Verifies checksum on specified packet.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isChecksumValid_(unsigned char *pBuffer, short size)
{
    int chksum = *pBuffer++;
    size--;

    for (; size > 0; size--)
    {
        if (*pBuffer & PROX_CMD_MASK)
        {
            return FALSE;   // invalid data type
        }
        chksum += *pBuffer++;
    }
    chksum &= 0x7F;

    return(chksum == 0);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isSensorReadingCycleError
//
//@ Interface-Description
//  Verifies checksum on specified packet.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
Boolean  ProxiInterface::isSensorReadingCycleError(Uint32 delta)
{
    Boolean status = FALSE;
    Uint32 flowResult  = ABS_VALUE(flow_.reportTime - flow_.updateTime);
    Uint32 pressureResult = ABS_VALUE(pressure_.reportTime - pressure_.updateTime);
    if ((flowResult > delta) || (pressureResult > delta))
    {
        status = TRUE;
    }

    return(status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isRevStrReady
//
//@ Interface-Description
//  Returns Boolean isRevStrReady_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isRevStrReady()
{
    return isRevStrReady_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isSerialNumReady
//
//@ Interface-Description
//  Returns Boolean isSerialNumReady_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isSerialNumReady()
{
    return isSerialNumReady_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void    ProxiInterface::SoftFault(  const SoftFaultID softFaultID
                                    , const Uint32      lineNumber
                                    , const char *      pFileName
                                    , const char *      pPredicate )
{
    FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES,PROXI_INTERFACE_CLASS,
                            lineNumber, pFileName, pPredicate);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isOemIdReady
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   isOemIdReady_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member isOemIdReady_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isOemIdReady(void)
{   
    return(isOemIdReady_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isCommunicationUp
//
//@ Interface-Description
//  returns true if covidien string id is recieve from prox board.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isCommunicationUp(void)
{
    Boolean isCommunicationUp = FALSE;

    const char COVIDIEN_STR[PROX_MAX_ID_STRING] = "COVIDIEN           "; 
    const Uint8 MAX_DELAY = 10;

    // Check the oem Id 
    if (isOemIdReady_)
    {
        if (strncmp(idString_, COVIDIEN_STR,PROX_MAX_ID_STRING ) == 0)
        {
            isCommunicationUp = TRUE;
        }
    }
    else
    {

        // Try one more time to query the oem id
        // Request for the OEM Id
        isGetOEMIdRequested_ = TRUE;

        Uint8 cycleCount = 0;
        // Wait for the prox board
        while (!isOemIdReady_ && cycleCount++ < MAX_DELAY)
        {
            Task::Delay(0,500);
        }

        // When the oem id is populated,
        // compare the covidien_str to verify 
        // the correct prox board.
        if (isOemIdReady_)
        {
            if (strncmp(idString_, COVIDIEN_STR,PROX_MAX_ID_STRING ) == 0)
            {
                isCommunicationUp = TRUE;
            }
        }
    }

    return isCommunicationUp;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSquareWaveformDemoMode
//
//@ Interface-Description
//  sets up prox board to send a demo mode message.  Used to determine
//  if communications is occuring properly with prox since know message
//  formats will be transmitted.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setSquareWaveformDemoMode(Boolean enable)
{
    static const int NUMBYTES = 5;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;               // NBF - number of bytes to follow this byte
    msg[2] = Mercury::DEMO_MODE;

    if (enable)
    {
        msg[3] = Mercury::SQUARE_WAVE_TEST_MODE; 
    }
    else
    {
        msg[3] = Mercury::DEMO_OFF; 
    }
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: purgeTest
//
//@ Interface-Description
//  Perform purge test
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::purgeTest_(void)
{

    disablePurge_();
    Task::Delay(1);

    setResMaxPressure_(sstTargetPressure_ + 2.0f);
    Task::Delay(1);

    setResTargetPressure_(sstTargetPressure_);
    Task::Delay(1);

    setMaxChargeTime_(2.0f);
    Task::Delay(1);

    enablePurge_();
    Task::Delay(1);

    transmitDumpAccumalator_(TRUE);
    Task::Delay(1);



}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableSquareWaveformDemoMode
//
//@ Interface-Description
// Sets enableSquareWaveformDemoMode_ 
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::enableSquareWaveformDemoMode(void)
{   
    enableSquareWaveformDemoMode_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableSquareWaveformDemoMode
//
//@ Interface-Description
// Sets disableSquareWaveformDemoMode_
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::disableSquareWaveformDemoMode(void)
{   
    disableSquareWaveformDemoMode_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setManualPurge
//
//@ Interface-Description
// Set setProxManualPurge_
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setManualPurge(void)
{   
    setProxManualPurge_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enablePurgeTest
//
//@ Interface-Description
// Enable purge with targetPressure
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::enablePurgeTest(Real32 targetPressure)
{   
    sstTargetPressure_ = targetPressure;
    enablePurgeTest_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isWaitingForPurgeEnable
//
//@ Interface-Description
// returns true if purge is waiting to be performed.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isWaitingForPurgeEnable(void)
{
    Boolean status = FALSE;
    if (purgeStatus_.byte1 == CUSTOM_PURGE_WAITING)
    {
        status = TRUE;
    }

    return(status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isProxIdleModeActive
//
//@ Interface-Description
//  returns true is prox board status is idle
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isIdleModeActive(void)
{
    Boolean status = FALSE;
    if (purgeStatus_.byte1 == CUSTOM_PURGE_IDLE)
    {
        status = TRUE;
    }

    return(status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setGasParams
//
//@ Interface-Description
//  Sets up gas parameters based on air source
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setGasParams( GasType testGas )
{

    if (testGas == AIR)
    {
        transferGasConditionBuffer_.fio2 = 21;
        transferGasConditionBuffer_.anesthetic = 0;
        transferGasConditionBuffer_.gasBalance = 0;
        transferGasConditionBuffer_.inspGasTemperature = 22;
        transferGasConditionBuffer_.expGasParam = 0;
        transferGasConditionBuffer_.inspHumidity = 20;
    }
    else if (testGas == O2)
    {

        transferGasConditionBuffer_.fio2 = 100;
        transferGasConditionBuffer_.anesthetic = 0;
        transferGasConditionBuffer_.gasBalance = 0;
        transferGasConditionBuffer_.inspGasTemperature = 22;
        transferGasConditionBuffer_.expGasParam = 0;
        transferGasConditionBuffer_.inspHumidity = 20;
    }

    isGasConditionUpdateNeeded_ = TRUE;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePurgeInterval
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the flag isPurgeIntervalUpdateRequested_ to TRUE and store
//  the passed in value of targetTime to targetPurgeInterval_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::updatePurgeInterval(Uint16 targetTimeInSecs)
{
    targetPurgeInterval_ = targetTimeInSecs;
    isPurgeIntervalUpdateRequested_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFlowOOR
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  return the class for flow OOR occurences
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32 ProxiInterface::getFlowOOR(void)
{
    return(flowOOR_);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPressureOOR
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//   return the class for pressure OOR occurences
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32 ProxiInterface::getPressureOOR(void)
{
    return(airwayPressureOOR_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableContinuousMode
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets enableContinuousMode_ to TRUE and allows processRequest() 
//  to call startContinuousMode_()
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::enableContinuousMode(void)
{   
    enableContinuousMode_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableContinuousMode
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets disableContinuousMode_ to TRUE and allows processRequest()
//  to call startContinuousMode_()
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::disableContinuousMode(void)
{   
    disableContinuousMode_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isProxReady
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
// return flag to indicate prox started is completed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isProxReady(void)
{
    return(isProxReady_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isProxSuspended
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
// return flag to indicate prox is suspended.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::isProxSuspended(void)
{
    return(isProxSuspended_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setProxCorrectionFactor()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set proxCorrectionFactor_
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setProxCorrectionFactor(Real32 proxCorrectionFactor)
{
    proxCorrectionFactor_ = proxCorrectionFactor;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHmeFactor()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set hmeFactor_
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setHmeFactor(Real32 hmeFactor)
{
    hmeFactor_ = hmeFactor;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHmeFactorTest()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set hmeFactorTest_
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setHmeFactorTest(Real32 hmeFactorTest)
{
    hmeFactorTest_ = hmeFactorTest;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAtmPressure()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set atmPressure_
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setAtmPressure(Real32 atmPressure)
{
    atmPressure_ = atmPressure;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setExpiredVolumeFactor()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set expiredVolumeFactor_
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setExpiredVolumeFactor(Real32 expiredVolumeFactor)
{
    expiredVolumeFactor_ = expiredVolumeFactor;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setInspiredVolumeFactor()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set inspiredVolumeFactor_
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setInspiredVolumeFactor(Real32 inspiredVolumeFactor)
{
    inspiredVolumeFactor_ = inspiredVolumeFactor;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: stopProx(Boolean runOnce)
//
//@ Interface-Description
//  Stops the mercury module from purging and sending continuous data.
//  The argument runOnce allows to stop prox from any action.  If it is
//  false, it just do a task delay.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls stopContinuousMode_(), gets board information, and suspend the
//  purge by calling disablePurge_()
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void ProxiInterface::stopProx_(Boolean runOnce)
{ 
    if (runOnce)
    {
        stopContinuousMode_();
        Task::Delay(2);  //allow prox to respond

        getSoftwareRevision_(Mercury::MAIN_SOFTWARE_REVISION);
        Task::Delay(2);  //allow prox to respond

        getBoardSerialNumber_();

        Task::Delay(1);
        getOEMId_();
        Task::Delay(1);

        disablePurge_();
    }

}

#if defined(VCO2_STUDY)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: zeroCapnostat_
//
//@ Interface-Description
//  Builds and sends the CAPNOSTAT5_ROOM_AIR_ZERO message
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::zeroCapnostat_(void)
{
    SYSLOG((LOG_DEBUG,"Zeroing capnostat5"));

    static const int NUMBYTES = 4;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    msg[CMD_INDEX] = Mercury::CAPNOSTAT5_ROOM_AIR_ZERO;
    msg[NBF_INDEX] = NBF;                   // NBF - number of bytes to follow this byte
    msg[2] = 0x2;                        // 02h per command specification
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMercurySoftwareOptions_
//
//@ Interface-Description
//  Builds and sends the GET_SET_INSTRUMENT_SETTINGS message to 
//  retrieve the installed software options from the Mercury Module.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::getMercurySoftwareOptions_(void)
{
    static const int NUMBYTES = 4;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;                // NBF - number of bytes to follow this byte
    msg[2] = Mercury::INSTALLED_SOFTWARE_OPTIONS;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setETCO2TimePeriod
//
//@ Interface-Description
//  Builds and sends the GET_SET_INSTRUMENT_SETTINGS message to 
//  set the end tidal CO2 volume time period to the specified time.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setETCO2TimePeriod(const Mercury::ETCO2TimePeriod period)
{
    static const int NUMBYTES = 5;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;                // NBF - number of bytes to follow this byte
    msg[2] = Mercury::SET_GET_ETCO2_TIME_PERIOD;
    msg[3] = period;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setVCO2AveragingTime
//
//@ Interface-Description
//  Builds and sends the GET_SET_INSTRUMENT_SETTINGS message to 
//  set the CO2 volume averaging period to the specified period.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ProxiInterface::setVCO2AveragingTime(const Mercury::VCO2AveragingPeriod period)
{
    static const int NUMBYTES = 5;
    static const int NBF = NUMBYTES - 2;
    unsigned char msg[NUMBYTES];

    msg[CMD_INDEX] = Mercury::GET_SET_INSTRUMENT_SETTINGS;       // 0xD4
    msg[NBF_INDEX] = NBF;                // NBF - number of bytes to follow this byte
    msg[2] = Mercury::SET_GET_VCO2_AVERAGING_TIME;
    msg[3] = period;
    addChecksum_(NUMBYTES-1, msg);

    sendPacket_(msg, NUMBYTES);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getVolume
//
//@ Interface-Description
//  Returns the last volume waveform value read from the proximal flow
//  sensor.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getVolume(void) const
{
    return volume_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCO2
//
//@ Interface-Description
//  Returns the last CO2 waveform value read from the proximal flow
//  sensor.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getCO2(void) const
{
    return co2_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getInspiredTime
//
//@ Interface-Description
//  Returns the last inspired time value read from the proximal flow
//  sensor.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getInspiredTime() const
{
    return inspiredTime_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getExpiredTime
//
//@ Interface-Description
//  Returns the last expired time value read from the proximal flow
//  sensor.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getExpiredTime() const
{
    return expiredTime_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getStartOfInspirationMark
//
//@ Interface-Description
//  Returns the start of inspiration mark read from the proximal
//  flow sensor. This is the time elapsed since the sensor
//  recognized start of inspiration. Returns zero on cycles when this 
//  value is not reported from the sensor.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getStartOfInspirationMark() const
{
    return startOfInspirationMark_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getStartOfExpirationMark
//
//@ Interface-Description
//  Returns the start of expiration mark read from the proximal
//  flow sensor. This is the time elapsed since the sensor
//  recognized start of expiration. Returns zero on cycles when this 
//  value is not reported from the sensor.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getStartOfExpirationMark() const
{
    return startOfExpirationMark_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getExhVolume
//
//@ Interface-Description
//  Returns the last exhaled volume value read from the proximal
//  flow sensor. 
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getExhVolume(void) const
{
    return expiratoryVolume_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getInspVolume
//
//@ Interface-Description
//  Returns the last inspired volume value read from the proximal
//  flow sensor. 
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getInspVolume(void) const
{
    return inspiratoryVolume_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMercurySoftwareOptions
//
//@ Interface-Description
//  Returns the software options installed on and reported by the 
//  Mercury module.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32 ProxiInterface::getMercurySoftwareOptions(void) const
{
    return mercurySoftwareOptions_;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCapnostatStatus
//
//@ Interface-Description
//  Returns the status value returned by the Mercury Module for the 
//  Capnostat 5. This is also known as the prioritized CO2 status.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32 ProxiInterface::getCapnostatStatus() const
{
    return capnostatStatus_.data;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getEndTidalCO2
//
//@ Interface-Description
//  Returns the end tidal CO2 last reported by the Capnostat 5 through
//  the Mercury Module.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getEndTidalCO2() const
{
    return endTidalCO2_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getInspiredCO2
//
//@ Interface-Description
//  Returns the inspired CO2 volume last reported by the Capnostat 5
//  through the Mercury Module.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getInspiredCO2() const
{
    return inspiredCO2_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getVolumeCO2ExpiredPerMinute
//
//@ Interface-Description
//  Returns the volume of CO2 expired per minute as reported by the 
//  Capnostat 5 through the Mercury Module.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getVolumeCO2ExpiredPerMinute() const
{
    return volumeCO2ExpiredPerMinute_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMixedExpiredCO2
//
//@ Interface-Description
//  Returns the mixed expired CO2 value as reported by the Capnostat 5 
//  through the Mercury Module.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 ProxiInterface::getMixedExpiredCO2() const
{
    return mixedExpiredCO2_.value;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getDataValid
//
//@ Interface-Description
//  Returns TRUE if all of the volumetric and timing data from the 
//  Mercury Module is valid. Otherwise, returns FALSE. Used to indicate
//  whether or not the data collected by the Waveforms program on the 
//  PC is valid.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ProxiInterface::getDataValid() const
{
    return(expiredTime_.state == DATA_VALID
           && inspiredTime_.state == DATA_VALID
           && expiratoryVolume_.state == DATA_VALID
           && inspiratoryVolume_.state == DATA_VALID
           && endTidalCO2_.state == DATA_VALID
           && inspiredCO2_.state == DATA_VALID
           && volumeCO2ExpiredPerMinute_.state == DATA_VALID
           && mixedExpiredCO2_.state == DATA_VALID);
}
#endif // defined(VCO2_STUDY)
