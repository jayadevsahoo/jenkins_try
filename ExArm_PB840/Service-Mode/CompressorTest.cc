#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CompressorTest - Test the compressor
//---------------------------------------------------------------------
//@ Interface-Description
//
//	This class implements the compressor test, which tests the
//	compressor timer, compressor pressure switch, the compressor
//	operating states, and the ability of the compressor to maintain
//	pressure during a worst-case breath delivery.
//
//	Note: compressor leak is separately tested in the CompressorLeakTest
//	class.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the compressor test.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method runTest() is responsible for running the compressor test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/CompressorTest.ccv   25.0.4.0   19 Nov 2013 14:23:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: quf    Date: 13-Oct-1999     DR Number: 5540
//  Project:  ATC
//  Description:
//	STANDBY mode test modified to eliminate possibility
//	of false failures at high altitude.
//	DISABLED mode test shortened to partly offset the increased
//	RUN and STANDBY mode test times.
//	Challenge waveform test modified to account for variability of
//	compressor flow delivery based on Patm.
//
//  Revision: 006  By: dosman    Date: 26-Feb-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	Implemented changes to remove compiler warnings for unit test.
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  quf    Date: 20-Nov-1997    DR Number: DCS 2644
//       Project:  Sigma (840)
//       Description:
//			Fixed runTest() to use a more reliable method of preventing
//	 		the Background Maintenance Task from accessing the compressor
//			serial EEPROM during this test.
//
//  Revision: 003  By:  quf    Date: 15-Sep-1997    DR Number: DCS 2281
//       Project:  Sigma (840)
//       Description:
//			Fixed "challenge waveform" test parameters based on latest
//			compressor specs.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "CompressorTest.hh"

//@ Usage-Classes

#include "Task.hh"
#include "ValveRefs.hh"
#include "VentObjectRefs.hh"
#include "MainSensorRefs.hh"
#include "SmRefs.hh"
#include "Compressor.hh"
#include "FlowSensor.hh"
#include "Psol.hh"
#include "SafetyNet.hh"
#include "BackgroundMaintApp.hh"
#include "ExhalationValve.hh"
// E600 BDIO #include "CompressorTimer.hh"

#include "TestData.hh"
#include "SmGasSupply.hh"
#include "SmManager.hh"
#include "SmFlowController.hh"
#include "SmUtilities.hh"
#include "SmConstants.hh"

#if defined(SIGMA_DEVELOPMENT)
# include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "SmUnitTest.hh"
# include "FakeIo.hh"
# include "FakeControllers.hh"
# include "CompressorTest_UT.hh"
# include "FakeTask.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//@ Constant:
// additional delay time for run mode test
const Uint32 RUN_TEST_DELAY = 5000;  // msec


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CompressorTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.  No arguments.  Sets up private data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set savedCompressorTime = 0.
//	set timerWorking_ = TRUE.
//	set pressureSwitchWorking_ = TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
CompressorTest::CompressorTest(void) :
				savedCompressorTime_(0),
				timerWorking_(TRUE),
				pressureSwitchWorking_(TRUE)
{
// $[TI1]
	CALL_TRACE("CompressorTest::CompressorTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CompressorTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CompressorTest::~CompressorTest(void)
{
	CALL_TRACE("CompressorTest::~CompressorTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest()
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.  It is used
//  to test the compressor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09148]  $[09076]
//	Check if compressor is present, if not, send "not installed"
//	message to gui and exit.
//	Check if AC power is being used.  If not:
//		Prompt operator to connect AC power.
//		If AC is still not connected, generate ALERT and exit.
//	If wall air is detected, prompt operator to disconnect it then
//	recheck wall air presence.  If it's still detected, generate
//	ALERT and exit.
//	Perform RUN mode tests, generate appropriate ALERTs if applicable.
//	Perform DISABLED mode test, generate appropriate ALERTs if applicable.
//	Perform STANDBY mode test, generate appropriate ALERTs if applicable.
//	Perform "challenge waveform" test, generate appropriate ALERTs if applicable.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
CompressorTest::runTest(void)
{
	CALL_TRACE("CompressorTest::runTest(void)");

	Boolean operatorExit = FALSE;
	Boolean acDetected = TRUE;
	Boolean wallAirDisconnected = TRUE;

	timerWorking_ = TRUE;
	pressureSwitchWorking_ = TRUE;

	// If compressor is not present
	if (!RCompressor.checkCompressorInstalled())
	{
	// $[TI1]
		// Compressor option not installed
		RSmManager.sendOptionNotInstalledToServiceData();
	}
	else
	{
	// $[TI2]
		// stop bkg maint task from updating compressor operational time
		BackgroundMaintApp::IssueNewSemphCommand(
								BackgroundMaintApp::REQUEST_TO_SUSPEND) ;
		while ( BackgroundMaintApp::RetrieveCurrentSemphStatus()
					!= BackgroundMaintApp::TASK_SUSPENDED)
		{
		// $[TI2.11]
			Task::Delay( 0, 20) ;

# ifdef SIGMA_UNIT_TEST
// added for unit testing, otherwise we're stuck in this loop
			SmUnitTest::SuspendBkgMaintAppTask();
# endif  // SIGMA_UNIT_TEST
		}

#ifdef SIGMA_UNIT_TEST
		SmUtilities::TestResult acResult = CompressorTest_UT::AcResult;
#else
		SmUtilities::TestResult acResult = RSmUtilities.checkAcPower();
#endif  // SIGMA_UNIT_TEST

		// First verify that AC power is connected
		switch (acResult)
		{
			case SmUtilities::AC_CONNECTED:
			// $[TI2.1]
				// AC power is connected
				break;

			case SmUtilities::AC_NOT_CONNECTED:
			// $[TI2.2]
				// Running on BPS power, can't test compressor
				RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_ALERT,
						SmStatusId::CONDITION_2);

				acDetected = FALSE;
				break;

			case SmUtilities::OPERATOR_EXIT:
			// $[TI2.3]
				operatorExit = TRUE;
				break;

			default:
			// $[TI2.4]
				CLASS_ASSERTION_FAILURE();
				break;
		}
		// end switch

		// If wall air is connected, prompt to disconnect it and
		// verify it was disconnected
		if ( !operatorExit && acDetected &&
			 RSmGasSupply.isWallAirPresent())
		{
		// $[TI2.5]
			if ( !RSmUtilities.messagePrompt(
						SmPromptId::DISCONNECT_AIR_PROMPT) )
			{
			// $[TI2.5.1]
				operatorExit = TRUE;
			}
			else
			{
			// $[TI2.5.2]
				// operator hit the ACCEPT key

				// Bleed trapped wall air
				RSmUtilities.drainWallAir();

				// Re-check wall air presence
				if (RSmGasSupply.isWallAirPresent())
				{
				// $[TI2.5.2.1]
					// Wall air still detected
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_ALERT,
						SmStatusId::CONDITION_1);

					wallAirDisconnected = FALSE;
				}
				// $[TI2.5.2.2]
				// implied else
			}
			// end else
		}
		// $[TI2.6]
		// implied else

		if (!operatorExit && acDetected && wallAirDisconnected)
		{
		// $[TI2.7]
			// Perform RUN mode tests
			runModeTest_();

			// Perform DISABLED mode tests
			disabledModeTest_();

			// Perform STANDBY test
			standbyModeTest_();

			// Perform challenge waveform test
			challengeWaveformTest_();
		}
		// $[TI2.8]
		// implied else

		if (operatorExit)
		{
		// $[TI2.9]
			RSmManager.sendOperatorExitToServiceData();
		}
		// $[TI2.10]
		// implied else

		// let bkg maint task resume updating compressor operational time
		BackgroundMaintApp::IssueNewSemphCommand( BackgroundMaintApp::TASK_RESUME);
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
CompressorTest::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SERVICE_MODE, COMPRESSORTEST,
		                      lineNumber, pFileName, pPredicate);

}
// runTest


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  runModeTest_
//
//@ Interface-Description
//	This method performs the RUN mode test.  No arguments, no
//	return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: the compressor was previously set to RUN mode
//	automatically when operator disconnected wall air.
//
//	Measure the initial compressor time.
//	Wait for the required test time, then read the final compressor
//	time.  If the elapsed time = final time - initial time is out of
//	range, generate an ALERT and set timerWorking_ = FALSE.
//	Check if compressor air pressure is present.  If not, generate
//	an ALERT and set pressureSwitchWorking_ = FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
CompressorTest::runModeTest_(void)
{
	CALL_TRACE("CompressorTest::runModeTest_(void)");

	// Calculate compressor RUN test timing bounds
	Uint32 minRunTestTime = (COMPRESSOR_FILL_TIME +
				RUN_TEST_DELAY) / 1000 - 1;  // sec

	Uint32 maxRunTestTime = (COMPRESSOR_FILL_TIME +
				RUN_TEST_DELAY) / 1000 + 1;  // sec

	// Read the initial compressor time, run for
	// (max fill time + margin), then read the
	// final compressor time
	readInitialTime_();

	Task::Delay( 0, COMPRESSOR_FILL_TIME + RUN_TEST_DELAY);

	Uint32 elapsedTime = measureElapsedTime_();

	RSmManager.sendTestPointToServiceData( (Real32)elapsedTime);

	// Compressor should have ran for the entire time
	if ( elapsedTime < minRunTestTime ||
		 elapsedTime > maxRunTestTime )
	{
	// $[TI1]
		// Timer didn't increment properly
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_4);

		timerWorking_ = FALSE;
	}
	// $[TI2]
	// implied else

	if (!RSmGasSupply.isCompressorAirPresent())
	{
	// $[TI3]
		// Compressor pressure absent unexpectedly
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_3);

		pressureSwitchWorking_ = FALSE;
	}
	// $[TI4]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  disabledModeTest_
//
//@ Interface-Description
//	This method performs the DISABLED mode test.  No arguments, no
//	return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Disable the automatic compressor control and force compressor
//	to DISABLED state.
//	Drain the compressor accumulator. If compressor pressure is
//	detected after draining, generate an ALERT and set
//	pressureSwitchWorking_ = FALSE.
//	Read initial compressor time.
//	Wait for the required test time, then read the final compressor
//	time.  If elapsed time = final time - initial time is out of range,
//	generate an ALERT and set timerWorking_ = FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
CompressorTest::disabledModeTest_(void)
{
	CALL_TRACE("CompressorTest::disabledModeTest_(void)");

	Uint32 testTime = 5000;  // msecs

	// disable compressor
	RSmGasSupply.disableCompressorAndControl();

	// Drain compressor accumulator
	RSmUtilities.drainAir();

	if (RSmGasSupply.isCompressorAirPresent())
	{
	// $[TI1]
		// Compressor air present unexpectedly
		// Minor failure, Condition 6
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_6);

		pressureSwitchWorking_ = FALSE;
	}
	// $[TI2]
	// implied else

	// Verify compressor timer is inactive
	readInitialTime_();

	Task::Delay( 0, testTime);

	Uint32 elapsedTime = measureElapsedTime_();

	RSmManager.sendTestPointToServiceData( (Real32)elapsedTime);

	// Compressor timer should have incremented 0 + margin
	if (elapsedTime > 1)
	{
	// $[TI3]
		// Timer incremented unexpectedly
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_7);

		timerWorking_ = FALSE;
	}
	// $[TI4]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  standbyModeTest_
//
//@ Interface-Description
//	This method performs the STANDBY mode test.  No arguments, no
//	return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assumptions: compressor is in DISABLED mode, automatic compressor
//	control is off and accumulator is empty from the DISABLED mode test.
//
//	If either the RUN or STANDBY mode timer test failed, generate an
//	ALERT and exit.
//	Manually command the compressor to STANDBY mode.
//	Read initial compressor time.
//	Wait for the required test time, then read the final compressor
//	time.  If elapsed time = final time - initial time is out of range,
//	generate an ALERT.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
CompressorTest::standbyModeTest_(void)
{
	CALL_TRACE("CompressorTest::standbyModeTest_(void)");

	// Can only test STANDBY state with a working timer
	if (timerWorking_)
	{
	// $[TI1]
		// Calculate timing bounds for STANDBY mode test
		Uint32 minStandbyTestTime = 3;  // sec
		Uint32 maxStandbyTestTime =
			(COMPRESSOR_FILL_TIME * 2) / 1000 - 5;  // sec

		// Command the compressor to STANDBY
		RCompressor.enableCompressor();
		RCompressor.setCompressorMotorReady();

		// Allow compressor to recharge
		readInitialTime_();

		Task::Delay( 0, COMPRESSOR_FILL_TIME * 2);

		Uint32 elapsedTime = measureElapsedTime_();

		RSmManager.sendTestPointToServiceData((Real32) elapsedTime);

		// Verify timer incremented within bounds
		if ( elapsedTime < minStandbyTestTime ||
			 elapsedTime > maxStandbyTestTime )
		{
		// $[TI1.1]
			// Elapsed time out of bounds =
			// STANDBY mode test failed
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_9);
		}
		// $[TI1.2]
		// implied else
	}
	else
	{
	// $[TI2]
		// Timer not working correctly, so
		// can't test STANDBY mode
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_8);
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  challengeWaveformTest_
//
//@ Interface-Description
//	This method performs the "challenge waveform" test.  No arguments,
//	no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If either the RUN or STANDBY mode compressor pressure switch test
//	failed, generate an ALERT and exit.
//	Re-enable compressor controller.
//	Set the exhalation valve to simulate patient pressure.
//	Begin simulated breath cycles using the "challenge waveform."
//		Activate flow then delay to simulate inspiration.
//		Shut off flow.
//		If compressor pressure is not present, generate an ALERT,
//		open the exhalation valve and exit.
//		Delay to simulate exhalation.
//		Repeat for ~1 minute total.
//	Open the exhalation valve.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
CompressorTest::challengeWaveformTest_(void)
{
	CALL_TRACE("CompressorTest::challengeWaveformTest_(void)");

	// Assume compressor is in STANDBY state

	// Constants for calculating the challenge waveform:
	// Tinsp
	const Uint32 INSP_TIME = 820;  // msec
	// slope of compressor minute volume (BTPS) vs Patm
	const Real32 MINUTE_VOL_SLOPE  = 0.030261;  // lpm/cmH2O
	// offset of compressor minute volume (BTPS) vs Patm
	const Real32 MINUTE_VOL_OFFSET = 18.723;    // lpm
	// maximum compressor delivered volume (BTPS) per breath
	const Real32 MAX_VOL_PER_BREATH = 2.74;  // liters
	// maximum compressor flow (BTPS)
	const Real32 MAX_FLOW_BTPS = 200.0;  // lpm
	// time conversion constants
	const Uint32 SECS_PER_MIN = 60;
	const Uint32 MSECS_PER_SEC = 1000;


	// Calculate challenge waveform parameters:
	// atmospheric pressure
	const Real32 ATM_PRESSURE =
		RSmUtilities.atmPressureBounded();  // cmH2O
	// compressor minute volume (BTPS)
	const Real32 MINUTE_VOLUME =
		ATM_PRESSURE * MINUTE_VOL_SLOPE + MINUTE_VOL_OFFSET; // lpm
	// resp rate
	const Real32 RESP_RATE = MINUTE_VOLUME  / MAX_VOL_PER_BREATH;
	// breath period rounded to nearest 10 msec
	const Uint32 BREATH_PERIOD =
		(Uint32) (SECS_PER_MIN * MSECS_PER_SEC / RESP_RATE + 5.0) /
		10 * 10;  // msec
	// Texp
	const Uint32 EXP_TIME = BREATH_PERIOD - INSP_TIME;
	// # of breath cycles for approx. 1-minute test = resp rate
	// rounded to nearest integer value
	const Uint32 NUM_OF_BREATH_CYCLES = (Uint32)(RESP_RATE + 0.5);
	// inspiratory flow (STPD)
	const Real32 MAX_FLOW_STPD =
		RSmUtilities.btpsToStpd( MAX_FLOW_BTPS, ATM_PRESSURE);

	const Real32 PRESSURE = 45.0;  // cmH2O

	// Turn back on the compressor controller
	RSmGasSupply.enableCompressorAndControl();

	// Can't perform test unless compressor pressure switch is working
	if (pressureSwitchWorking_)
	{
	// $[TI1]
		// Wait for compressor to fully recharge
		Task::Delay( 0, COMPRESSOR_FILL_TIME);

		// Set the exh valve
		RExhalationValve.updateValve( PRESSURE, ExhalationValve::NORMAL);

		// Perform challenge waveform breath cycles for about 1 min
		for (Uint32 numCycles = 0; numCycles < NUM_OF_BREATH_CYCLES; numCycles++)
		{
		// $[TI1.1]
			// simulate inspiratory phase
			RSmFlowController.establishFlow( MAX_FLOW_STPD, DELIVERY, AIR);

			Task::Delay( 0, INSP_TIME);

			if(!RSmGasSupply.isCompressorAirPresent())
			{
			// $[TI1.1.1]
				// Compressor pressure dropped below switch threshold
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT,
					SmStatusId::CONDITION_11);

				RSmFlowController.stopFlowController();
				break;
			}
			// $[TI1.1.2]
			// implied else

			// simulate expiratory phase
			RSmFlowController.stopFlowController();

			Task::Delay( 0, EXP_TIME - SM_CYCLE_TIME);

		}
		// end for

		RSmUtilities.openExhValve();
	}
	else
	{
	// $[TI2]
		// Pressure switch not working, can't perform challenge
		// waveform test
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_10);
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: readInitialTime_()
//
//@ Interface-Description
//  This method reads the compressor usage timer and stores it.
//  It takes no	arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Read the compressor timer and store the value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
CompressorTest::readInitialTime_(void)
{
// $[TI1]
	CALL_TRACE("CompressorTest::readInitialTime_(void)");

    savedCompressorTime_ = 0; // E600 BDIO RCompressorTimer.readActiveCounter();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: measureElapsedTime_()
//
//@ Interface-Description
//  This method reads the compressor usage timer.  It takes no
//	arguments and returns the difference between the current time
//  and the time stored by readInitialTime_().
//---------------------------------------------------------------------
//@ Implementation-Description
//	Read the compressor timer, subtract the current value from the
//	previously stored value and return the difference.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32
CompressorTest::measureElapsedTime_(void)
{
// $[TI1]
	CALL_TRACE("CompressorTest::measureElapsedTime_(void)");

    Uint32 currentTime = 0; // E600 BDIO RCompressorTimer.readActiveCounter();

	return (currentTime - savedCompressorTime_);
}
