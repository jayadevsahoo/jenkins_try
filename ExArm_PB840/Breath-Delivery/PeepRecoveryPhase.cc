#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PeepRecoveryPhase - Implements pressure support inspiration
//		ventilation to recover patient pressure to peep.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from PsvPhase class which is derived from
//		PressureBasePhase class.  No public methods are defined by this
//		class since base class methods are used. Protected virtual methods
//		are implemented to support the base class.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms for peep recovery.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The data members values from the base class are defined	in the pure
//		virtual methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PeepRecoveryPhase.ccv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Removed getExhSens_() method as dead code - never called.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By: syw   Date:  23-Apr-1998    DR Number: 5075
//  	Project:  Sigma (840)
//		Description:
//			Eliminate callback call in constructor since PsvPhase will
//			register the callback.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  syw    Date:  06-May-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

#include "PeepRecoveryPhase.hh"

#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "O2Mixture.hh"

//@ End-Usage

//@ Code...


//	$[04322] 
//@ Constant: PEEP_RECOVERY_FAP
// fap during peep recovery
static const Real32 PEEP_RECOVERY_FAP = 50.0F ;	

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PeepRecoveryPhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//		The RO2Mixture object should be constructed before this class is
//		constructed.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called.  Register breath phase to
//		O2Mixture.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PeepRecoveryPhase::PeepRecoveryPhase( void) : PsvPhase() // $[TI1]
{
	CALL_TRACE("PeepRecoveryPhase::PeepRecoveryPhase( void)") ;
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PeepRecoveryPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PeepRecoveryPhase::~PeepRecoveryPhase(void)
{
	CALL_TRACE("PeepRecoveryPhase::~PeepRecoveryPhase(void)") ;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PeepRecoveryPhase::SoftFault( const SoftFaultID  softFaultID,
                   	 const Uint32       lineNumber,
					 const char*        pFileName,
		   			 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PEEPRECOVERYPHASE,
                             lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineEffectivePressureAndBiasOffset_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called by the base class, PressureBasePhase, to determine the
//		effective pressure and the bias offset. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		The bias offset = 1.5.  The effective pressure is bias offset.
//		$[04322] 
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PeepRecoveryPhase::determineEffectivePressureAndBiasOffset_( void)
{
	CALL_TRACE("PeepRecoveryPhase::determineEffectivePressureAndBiasOffset_( void)") ;
	
  	// $[TI1]
	biasOffset_ = 1.5F ;
	effectivePressure_ = biasOffset_ ;
}
		
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineFlowAccelerationPercent_( void)
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called by the base class, PressureBasePhase, to determine the
//		flow acceleration percentage.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The flow acceleration percentage is PEEP_RECOVERY_FAP ;
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PeepRecoveryPhase::determineFlowAccelerationPercent_( void)
{
	CALL_TRACE("PeepRecoveryPhase::determineFlowAccelerationPercent_( void)") ;

  	// $[TI1]
	fap_ = PEEP_RECOVERY_FAP ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineMirrorGain_( void)
//
//@ Interface-Description
//      This method has no arguments and returns the mirror gain.  This method
//      is called to obtain the mirrorGain.  This method must be called AFTER
//      the desired pressure filter and the trajectory are determined.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The mirror gain is dependent on peep value
//		$[04322] 
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
PeepRecoveryPhase::determineMirrorGain_( void)
{
	CALL_TRACE("PressureBasedPhase::determineMirrorGain_( void)") ;
	
  	// $[TI1]
	Real32 mirrorGain = 1.0F / ( 1.5F + peep_) ;

	return( mirrorGain) ;
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
