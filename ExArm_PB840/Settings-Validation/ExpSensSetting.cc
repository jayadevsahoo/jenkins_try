#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ExpSensSetting - Expiratory Sensitivity setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the percentage of peak
//  inspiratory flow that causes a switch from inspiration to exhalation,
//  when the flow is dropping at the end of inspiration.  This class
//  inherits from 'BoundedSetting' and provides the specific information
//  needed for representation of expiratory sensitivity.  This information
//  includes the interval and range of this setting's values, and this batch
//  setting's new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, or dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ExpSensSetting.ccv   25.0.4.0   19 Nov 2013 14:27:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 004   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ExpSensSetting.hh"
#include "ModeValue.hh"
#include "SupportTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02111] -- The setting's range ...
//  $[02113] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	1.0f,		// absolute minimum...
	0.0f,		// unused...
	TENTHS,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	80.0f,		// absolute maximum...
	1.0f,		// resolution...
	ONES,		// precision...
	&::LAST_NODE_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ExpSensSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, the method initializes
//  the value interval range.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ExpSensSetting::ExpSensSetting(void)
: BatchBoundedSetting(SettingId::EXP_SENS,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  EXP_SENS_MAX_ID,	 // maxConstraintId...
					  EXP_SENS_MIN_ID)	 // minConstraintId...
{
	CALL_TRACE("ExpSensSetting()");
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ExpSensSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ExpSensSetting::~ExpSensSetting(void)
{
	CALL_TRACE("~ExpSensSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	ExpSensSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

	DiscreteValue  modeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			modeValue = pMode->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			modeValue = pMode->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	return((modeValue == ModeValue::SIMV_MODE_VALUE     ||
			modeValue == ModeValue::BILEVEL_MODE_VALUE  ||
			modeValue == ModeValue::SPONT_MODE_VALUE ||
			modeValue == ModeValue::CPAP_MODE_VALUE)
		   ? Applicability::CHANGEABLE		// $[TI3]
		   : Applicability::INAPPLICABLE);	// $[TI4]
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, toValue, fromValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'fromValue' to 'toValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[PA02001]  -- transitioning to/from PA...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ExpSensSetting::acceptTransition(const SettingId::SettingIdType,
									 const DiscreteValue,
									 const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, toValue, fromValue)");

	// store the new-patient value into the adjusted context...
	setAdjustedValue(getNewPatientValue());

	// activation of transition rule must be italicized...
	setForcedChangeFlag();
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant for the new patient value.
//
//  $[02112] The setting's new-patient value ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	ExpSensSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	BoundedValue  newPatient;

	const Setting*  pSpontType =
		SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

	const DiscreteValue  SPONT_TYPE_VALUE = pSpontType->getAdjustedValue();

	switch ( SPONT_TYPE_VALUE )
	{
		case SupportTypeValue::OFF_SUPPORT_TYPE :
		case SupportTypeValue::PSV_SUPPORT_TYPE :
		case SupportTypeValue::ATC_SUPPORT_TYPE :
		case SupportTypeValue::VSV_SUPPORT_TYPE :	  // $[TI1]
			newPatient.value = 25.0f;
			break;
		case SupportTypeValue::PAV_SUPPORT_TYPE :	  // $[TI2]
			newPatient.value =  3.0f;
			break;
		default :
			// unexpected patient spont type value...
			AUX_CLASS_ASSERTION_FAILURE(SPONT_TYPE_VALUE);
			break;
	}

	newPatient.precision = ONES;

	return(newPatient);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ExpSensSetting::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							EXP_SENS_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, sets this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02111] -- The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ExpSensSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	BoundedRange::ConstraintInfo  constraintInfo;

	//-------------------------------------------------------------------
	//  update maximum constraint...
	//-------------------------------------------------------------------

	constraintInfo.value  = getAbsoluteMaxValue_();
	constraintInfo.id     = EXP_SENS_MAX_ID;
	constraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMaxConstraint(constraintInfo);

	//-------------------------------------------------------------------
	//  update minimum constraint...
	//-------------------------------------------------------------------

	constraintInfo.value  = getAbsoluteMinValue_();
	constraintInfo.id     = EXP_SENS_MIN_ID;
	constraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMinConstraint(constraintInfo);
}  // $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Use this to return the absolute maximum value that is currently available
//  from this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02111] -- The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	ExpSensSetting::getAbsoluteMaxValue_(void) const
{
	CALL_TRACE("getAbsoluteMaxValue_()");

	Real32  absoluteMaxValue;

	const Setting*  pSpontType =
		SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

	const DiscreteValue  SPONT_TYPE_VALUE = pSpontType->getAdjustedValue();

	switch ( SPONT_TYPE_VALUE )
	{
		case SupportTypeValue::OFF_SUPPORT_TYPE :
		case SupportTypeValue::PSV_SUPPORT_TYPE :
		case SupportTypeValue::ATC_SUPPORT_TYPE :
		case SupportTypeValue::VSV_SUPPORT_TYPE :	  // $[TI1]
			absoluteMaxValue = ::INTERVAL_LIST_.value;	// absolute maximum...
			break;
		case SupportTypeValue::PAV_SUPPORT_TYPE :	  // $[TI2]
			absoluteMaxValue = 10.0f;
			break;
		default :
			// unexpected patient spont type value...
			AUX_CLASS_ASSERTION_FAILURE(SPONT_TYPE_VALUE);
			break;
	}

	return(absoluteMaxValue);
}
