#ifndef CycleTimer_HH
#define CycleTimer_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: CycleTimer - Responsible to invoke the periodic functions
//  of the Breath-Delivery subsystem.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/CycleTimer.hhv   25.0.4.0   19 Nov 2013 13:59:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  17-Apr-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

//@ Usage-Classes
#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
//@ End-Usage

class CycleTimer 
{
  public:

    static void SignalNewCycle(void);
    static void SignalNewSecondaryCycle(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);
 
  protected:

  private:
    CycleTimer(void);        // Declared but not implemented
    ~CycleTimer(void);       // Declared but not implemented
    CycleTimer(const CycleTimer&);       // Declared but not implemented
    void operator=(const CycleTimer&);   // Declared but not implemented
};


#endif // CycleTimer_HH 
