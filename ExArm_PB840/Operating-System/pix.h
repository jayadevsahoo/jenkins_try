#ifndef _PIX_H
#define _PIX_H
//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2009, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Filename: pix.h - position independent code and data
//---------------------------------------------------------------------
//@ Version-Information
//  @(#) $Header:   /840/Baseline/Operating-System/vcssrc/pix.h_v   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  27-Jan-2009    SCR Number: 6461 
//  Project:  840S
//  Description:
//	Initial version.
//=====================================================================

//   @(#)   pix.h   7.3 ;  3/30/92   13:57:19 
//**********************************************************
// THIS INFORMATION IS PROPRIETARY TO MICROTEC RESEARCH, INC.                                  
//----------------------------------------------------------
// Copyright (c) 1991-1993  Microtec Research, Inc.
// All rights reserved
//**********************************************************

//**  File pix.h 
// Position independent code and data.

#if  _PID || _PIC

typedef void (*PIXFPTR)();

	#define PIX_REF(ptr) (*((int*)(((char*)ptr + (int)_pix_start_of_data )) )) 
	#define PIX_ADD(ptr) ((char*)ptr +(int)_pix_start_of_data)
	#define PIX_SUB(ptr) ((char*)ptr -(int)_pix_start_of_data)

extern char* _pix_start_of_data ;

void init_pix_code_data();

#else // _PID || _PIC

	#define PIX_REF(ptr) *ptr
	#define PIX_ADD(ptr) ptr
	#define PIX_SUB(ptr) ptr

#endif // _PID || _PIC

#endif // _PIX_H
