
#ifndef AcceptedContextHandle_HH
#define AcceptedContextHandle_HH

//====================================================================
// This is a  proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AcceptedContextHandle - Handle to the Accepted Settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/AcceptedContextHandle.hhv   25.0.4.0   19 Nov 2013 14:27:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "SettingClassId.hh"
#include "SettingId.hh"

//@ Usage-Classes
#include "BoundedValue.hh"
#include "DiscreteValue.hh"
#include "SequentialValue.hh"
//@ End-Usage


class AcceptedContextHandle
{
  public:
    static BoundedValue   GetBoundedValue(
				  const SettingId::SettingIdType boundedId
					 );
    static DiscreteValue  GetDiscreteValue(
				  const SettingId::SettingIdType discreteId
				  	  );
    static SequentialValue  GetSequentialValue(
				const SettingId::SettingIdType sequentialId
					      );

#if defined(SIGMA_DEVELOPMENT)
    static void  SetBoundedValue(const SettingId::SettingIdType boundedId,
				 const BoundedValue& boundedValue);
    static void  SetDiscreteValue(const SettingId::SettingIdType discreteId,
				  const DiscreteValue discreteValue);
    static void  SetSequentialValue(
    				const SettingId::SettingIdType sequentialId,
    				const SequentialValue sequentialValue
				   );
#endif // defined(SIGMA_DEVELOPMENT)

    static void   SoftFault(const SoftFaultID softFaultID,
			    const Uint32      lineNumber,
			    const char*       pFileName  = NULL, 
			    const char*       pPredicate = NULL);
  
  private:
    AcceptedContextHandle(const AcceptedContextHandle&);// not implemented...
    AcceptedContextHandle(void);		        // not implemented...
    ~AcceptedContextHandle(void);                       // not implemented...
    void  operator=(const AcceptedContextHandle&);      // not implemented...
};


#endif // AcceptedContextHandle_HH 
