
#ifndef DListR_Alarm_HH
#define DListR_Alarm_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: DListR_Alarm - Abstract Doubly-Linked List of 'Alarm' References.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractDListR.H_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "FoundationClassId.hh"

class Alarm;  // forward declaration...

//@ Usage-Classes
#include "DListC_MemPtr.hh"
//@ End-Usage


class DListR_Alarm
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&                  ostr,
				       const DListR_Alarm& dList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

    //@ Friend:  DIterR_Alarm
    // 'DIterR_Alarm' needs access to the protected methods of this
    // class.
    friend class DIterR_Alarm;

    //@ Friend:  SortR_Alarm
    // 'SortR_Alarm' needs access to the protected methods of this
    // class.
    friend class SortR_Alarm;

  public:
    virtual ~DListR_Alarm(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);
    inline SigmaStatus  goPrev (void);
    inline SigmaStatus  goLast (void);

    SigmaStatus  search(Alarm&                 item,
			const SearchFrom      startFrom = FROM_FIRST,
			const SearchDirection direction = FORWARD,
			const EquivResolution foundWhen = EQUIV_OBJECTS);

    inline void  insert(Alarm&                newItem,
		        const InsertionPlace insertPlace = AFTER_CURRENT);
    inline void  append(Alarm& newItem);
    inline void  prepend(Alarm& newItem);

    inline SigmaStatus  removeCurrent(void);
    SigmaStatus  removeItem(Alarm&                item,
			    const EquivResolution foundWhen = EQUIV_OBJECTS);
    inline void         clearList(void);

    inline const Alarm&  currentItem(void) const;
    inline Alarm&        currentItem(void);

    inline void  operator=(const DListR_Alarm& dList);

    Boolean  isEquivTo(const DListR_Alarm& dList,
		       const EquivResolution     equivWhen) const;

    inline Boolean  operator==(const DListR_Alarm& dList) const;
    inline Boolean  operator!=(const DListR_Alarm& dList) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
    inline DListR_Alarm(DListC_MemPtr* pList);

    // return a pointer to the node that is a match of 'item', starting
    // the search from 'pStartNode'...
    const DNodeC_MemPtr*  findNode_(
				    Alarm&                       item,
				    const DNodeC_MemPtr* pStartNode,
				    const SearchDirection       direction,
				    const EquivResolution       foundWhen
					  ) const;

    inline Boolean  isLegalNode_(const DNodeC_MemPtr* pNode) const;

    inline const DNodeC_MemPtr*  getFirst_  (void) const;
    inline const DNodeC_MemPtr*  getCurrent_(void) const;
    inline const DNodeC_MemPtr*  getLast_   (void) const;

    inline DNodeC_MemPtr*  getFirst_  (void);
    inline DNodeC_MemPtr*  getCurrent_(void);
    inline DNodeC_MemPtr*  getLast_   (void);

    inline void  setCurrent_(DNodeC_MemPtr* pNewCurr);

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    DListR_Alarm(const DListR_Alarm&);	// not implemented...
    DListR_Alarm(void);			  	// not implemented...

    //@ Data-Member:  rPtrList_
    // A reference to the abstract list of pointers.
    DListC_MemPtr&  rPtrList_;
};


// Inlined methods...
#include "DListR_Alarm.in"


#endif  // DListR_Alarm_HH
