#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmLogTabButton - A TabButton whose icon visually indicates 
// whether or not there are unviewed alarm log entries.
//---------------------------------------------------------------------
//@ Interface-Description
// A specialized TabButton which indicates whether or not there are unviewed
// alarm log entries.  The button indicates whether or not there are unviewed
// alarm log entries by displaying two different bitmap (icon) configurations.
// This class interacts with the Alarms subsystem to determine whether or not
// there are unviewed alarm log entries.
//
// The updateDisplay() method is inherited from TabButton and is called anytime
// the alarm log bitmap needs to change.
//---------------------------------------------------------------------
//@ Rationale
// This specialized TabButton handles the unique display requirements of
// the Alarm Log screen select button.
//---------------------------------------------------------------------
//@ Implementation-Description
// If this button is in the up state, it displays either one or two
// bitmaps.  If there is one bitmap, an alarm log "clipboard" icon, this
// means that there are no unviewed alarm log entries.  If there are two
// bitmaps, the "clipboard" icon plus an "alert" icon, this means that
// there are unviewed alarm log entries.  In the down state, this button
// always displays only the "clipboard" icon.
//
// This class provides an updateDisplay() method which decides whether
// to display one or two bitmaps.  The updateDisplay() method fetches
// alarm log data from the Alarms subsystem.
//
// This class also redefines the virtual downHappened_() method in the
// parent in order to handle the bitmap change which occurs when the
// button goes from the up state to the down state.  Also, the
// upHappened_() method is redefined to save the state of the alarm log
// at the time the alarm log subscreen is dismissed.
//---------------------------------------------------------------------
//@ Fault-Handling
// Standard assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmLogTabButton.ccv   25.0.4.0   19 Nov 2013 14:07:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 007   By: gdc    Date:  26-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 006   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
//	As RLowerScreen, RUpperScreen, RServiceLowerScreen and RServiceUpperScreen
//	have become members of their corresponding classes, class scopes for these
//	reference variables must change from GuiApp to their respective classes, i.e,
//	LowerScreen, UpperScreen, ServiceLowerScreen and ServiceUpperScreen.
//
//	as class scope for RLowerScreen, RUpperScreen

//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 004  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision:  002  By: yyy   Date:  22-May-1997      DR Number: DCS 1917
//    Project:    Sigma (R8027)
//    Description:
//      Update the alarm log counts and last alarm timer from the
//		AlarmLogSubScreen so these two number always sync with the values
//		in AlarmLogSubScreen.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "AlarmLogTabButton.hh"

#include "Sigma.hh"

//@ Usage-Classes
#include "AlarmEvent.hh"
#include "Array_AlarmLogEntry_MAX_ALARM_ENTRIES.hh"
#include "Image.hh"
#include "NovRamManager.hh"
#include "UpperSubScreenArea.hh"
#include "AlarmLogSubScreen.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CLIPBOARD_NORMAL_X_ = 27;
static const Int32 CLIPBOARD_ALERT_X_ = 43;
static const Int32 CLIPBOARD_Y_ = 1;
static const Int32 ALERT_X_ = 10;
static const Int32 ALERT_Y_ = 5;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmLogTabButton  [Constructor]
//
//@ Interface-Description
// Constructor.  Passed the following arguments:
// >Von
//	xOrg			The X coordinate of the button.
//	yOrg			The Y coordinate of the button.
//	width			Width of the button.
//	height			Height of the button.
//	buttonType		Type of the button, either Button::LIGHT,
//					Button::LIGHT_NON_LIT or Button::DARK.
//	bevelSize		Width of the button's internal border.
//	cornerType		Tells whether its corners are rounded or not, can be
//					Button::ROUND or Button::SQUARE.
//	title			The string displayed as the button's title.
//	pSubScreen		The subscreen which is selected by pressing this button.
//>Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Passes constructor arguments through to the TabButton constructor.
// Initializes all members and initializes the positions of all
// graphical objects.  Adds all graphical objects to the parent
// container for display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmLogTabButton::AlarmLogTabButton(Uint16 xOrg, Uint16 yOrg,
						 Uint16 width, Uint16 height,
						 Button::ButtonType buttonType, Uint8 bevelSize,
						 Button::CornerType cornerType,
						 StringId title, SubScreen *pSubScreen) :
	TabButton(xOrg, yOrg, width, height, buttonType, bevelSize,
					cornerType, title, pSubScreen),
	clipboardBitmap_(Image::RAlarmLogIcon),
	alertBitmap_(Image::RAlertIcon),
	numLogEntries_(-1)
{
	CALL_TRACE("AlarmLogTabButton::AlarmLogTabButton(xOrg, yOrg, width, height, "
			   "buttonType, bevelSize, cornerType, title, pSubScreen, shortcutId)");

	//
	// Position then add bitmaps to this button.
	//
	clipboardBitmap_.setX(CLIPBOARD_NORMAL_X_);
	clipboardBitmap_.setY(CLIPBOARD_Y_);
	addLabel(&clipboardBitmap_);

	alertBitmap_.setX(ALERT_X_);
	alertBitmap_.setY(ALERT_Y_);
	addLabel(&alertBitmap_);
	alertBitmap_.setShow(FALSE);

	//
	// Set time stamp marker to bogus, uninitialized value.
	//
	lastTimeStamp_.invalidate();						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmLogTabButton  [Destructor]
//
//@ Interface-Description
// Destructor.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmLogTabButton::~AlarmLogTabButton(void)
{
	CALL_TRACE("AlarmLogTabButton::~AlarmLogTabButton(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay [public, virtual]
//
//@ Interface-Description
// Decides whether to display one or both button bitmaps, based on whether or
// not there are unviewed alarm log entries.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method is called in response to an alarm update message.  If
// this button is currently in the "up" state and the "alert" bitmap is
// currently not shown, then check the alarm log to see whether or not a
// new entry was made.  If so, display the "alert" bitmap, shifting the
// "clipboard" bitmap aside to make room.
//
// This redefines the base class virtual function TabButton::updateDisplay().
//
// $[01070] Alarm Log button indicates if there's new alarm events by ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmLogTabButton::updateDisplay(void)
{
	CALL_TRACE("AlarmLogTabButton::updateDisplay(void)");

    if ((UP == getState_()) && (FALSE == alertBitmap_.getShow()))
	{													// $[TI1]
		//
		// Access the alarm log entries.
		//
		FixedArray(AlarmLogEntry,MAX_ALARM_ENTRIES) logEntries;
		NovRamManager::GetAlarmLogEntries(logEntries);

		if (!logEntries[0].isClear())
		{												// $[TI2]
			Int32 numLogEntries = 0;
			// $[TI2]
			// Count the current number of log entries
			for (numLogEntries = 0; numLogEntries < MAX_ALARM_ENTRIES;
															numLogEntries++)
			{											// $[TI9.1]
				if (logEntries[numLogEntries].isClear())
				{										// $[TI3]
					break;
				}										// $[TI4]
			}											// $[TI9.2]

			//
			// Do quick test to see if alarm log changed size, then if necessary
			// do a fine-grained test to see if the time stamp of the newest log
			// entry is the same as the saved time stamp.
			//
			const TimeStamp latestTimeStamp =
					AlarmEvent(logEntries[0].getAlarmAction()).getTimeStamp();

			if ((numLogEntries_ != numLogEntries) ||
				(lastTimeStamp_ != latestTimeStamp))
			{											// $[TI5]
				clipboardBitmap_.setX(CLIPBOARD_ALERT_X_);
				alertBitmap_.setShow(TRUE);
			}											// $[TI6]
		}												// $[TI7]
	}													// $[TI8]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: downHappened_ [protected, virtual]
//
//@ Interface-Description
// Modify the button bitmap (icon) display to show only the "clipboard"
// bitmap.  The 'isByOperatorAction' parameter is TRUE when this method
// is called as a result of a user action, but it is not used here
// during display processing.
//---------------------------------------------------------------------
//@ Implementation-Description
// Hide the "alert" bitmap and restore the "clipboard" bitmap to its
// normal (centered) position.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmLogTabButton::downHappened_(Boolean isByOperatorAction)
{
	CALL_TRACE("AlarmLogTabButton::downHappened_(isByOperatorAction)");

	//
	// Perform any default TabButton processing.
	//
	TabButton::downHappened_(isByOperatorAction);

	//
	// Shift the clipboard bitmap back to its centered position and
	// hide the alert bitmap.
	//
	clipboardBitmap_.setX(CLIPBOARD_NORMAL_X_);
	alertBitmap_.setShow(FALSE);						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: upHappened_ [protected, virtual]
//
//@ Interface-Description
// Capture the number of alarm log entries and the time stamp of the
// latest alarm log entry at the time the alarm log subscreen is
// dismissed.  The 'isByOperatorAction' parameter is TRUE when this
// method is called as a result of a user action, but it is not used
// here during display processing.
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the internal 'lastTimeStamp_' and 'numLogEntries_' values,
// based on the current state of the alarm log.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmLogTabButton::upHappened_(Boolean isByOperatorAction)
{
	CALL_TRACE("AlarmLogTabButton::upHappened_(Boolean isByOperatorAction)");

	//
	// Perform any default TabButton processing.
	//
	TabButton::upHappened_(isByOperatorAction);

	numLogEntries_ = -1;
	lastTimeStamp_.invalidate();

	//
	// Access the alarm log entries.  If there is at least one entry in the
	// log, then set the list position to the first (newest) entry in the
	// log so that we can get the timestamp of the first entry.
	//
	FixedArray(AlarmLogEntry,MAX_ALARM_ENTRIES) logEntries;
	NovRamManager::GetAlarmLogEntries(logEntries);

	if (!logEntries[0].isClear())
	{													// $[TI1]
		UpperSubScreenArea::GetAlarmLogSubScreen()->
							getAlarmLogInfo(numLogEntries_, lastTimeStamp_);
	}													// $[TI4]
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmLogTabButton::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("AlarmLogTabButton::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMLOGTABBUTTON,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

