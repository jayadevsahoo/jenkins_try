#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DiagLogMenuSubScreen - Diagnostic Log Menu allows access to the 
//	Diagnostic Log subscreens. 
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the SubScreen and LogTarget classes.
// It contains a SubScreenTitleArea object for the subscreen title, a
// ScrollableLog object which displays the entries of the diagnostic
// logs in a scrollable and selectable format.  It also has constants which 
// define the log's location, dimension and format.
// 	Specifically, there is one select button for each of the following 
// log types. 
// >Von
//	1.	System Diagnostic log.
//	3.	Communication Diagnostic log.
//	7.	SST/EST Diagnostic log.
// >Voff
// This class collaborates with the UpperSubScreenArea to activate the
// subscreens associated with the select buttons contained in its menu. 
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// The getLogEntryColumn() method is called to retrieve the contents of any
// newly displayed rows. When the user selects a new entry or deselects the 
// current entry, the method currentLogEntryChangeHappened() is called.
//---------------------------------------------------------------------
//@ Rationale
// Created to provide a navigation interface to the Diagnostic Log subscreens. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Each ScrollableLog select button is labeled with the title of the subscreen
// associated with that button.  
// There is tricky problem with the Other Screens screen select button
// (contained by the UpperScreenSelectArea).  That is, when a new "other"
// subscreen is activated from this Other Screens subscreen, the default
// deactivation behavior for the Other Screens subscreen pops up the Other
// Screens screen select button.  However, this is not the desired result -- we
// want the Other Screens screen select button to remain selected when a new
// "other" subscreen is activated.  Thus, the Other Screens screen select
// button must be "manually" pushed down (selected) when a new "other" subscreen
// is activated.  See the currentLogEntryChangeHappened() method for details.
//
// The ScrollableLog menu and the subscreen title are added to the Container 
// when it is constructed.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DiagLogMenuSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  10-Sep-97    DR Number: 1923
//    Project:  Sigma (R8027)
//    Description:
//      Changed the name of communications Diagnostic log to the system
//		information log.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

//
// Sigma includes.
//
#include "DiagLogMenuSubScreen.hh"

//@ Usage-Classes
#include "UpperScreen.hh"
#include "ServiceUpperScreen.hh"
#include "MiscStrs.hh"
#include "UpperSubScreenArea.hh"
#include "ScrollableLog.hh"
#include "UpperSubScreenArea.hh"
#include "DiagCodeLogSubScreen.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 MENU_X_ = 55;
static const Int32 MENU_Y_ = 77;
static const Int32 MENU_ROW_HEIGHT_ = 38;
static const Int32 MENU_NUM_ROWS_ = 3;
static const Int32 MENU_NUM_COLUMNS_ = 2;
static const Int32 MENU_COLUMN1_WIDTH_ = 462;
static const Int32 MENU_COLUMN2_WIDTH_ = 60;
static const Int32 DL_SYSTEM_ENTRY_ = 0;
static const Int32 DL_SYS_INFO_ENTRY_ = 1;
static const Int32 DL_EST_SST_ENTRY_ = 2;
static const Int32 MAX_DIAGNOSTIC_LOGS_ = MENU_NUM_ROWS_;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DiagLogMenuSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members; Sets the position, size and color of the subscreen
// area.  Add the subscreen title object to the parent container for display.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DiagLogMenuSubScreen::DiagLogMenuSubScreen(SubScreenArea *pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleArea_(MiscStrs::DLOG_MENU_SUBSCREEN_TITLE, SubScreenTitleArea::SSTA_CENTER),
	pMenu_(NULL)
{
	CALL_TRACE("DiagLogMenuSubScreen::DiagLogMenuSubScreen(pSubScreenArea)");

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	// Add screen title object.
	//
	addDrawable(&titleArea_);

	MenuEntryTopics_[0] = MiscStrs::DLOG_SYS_TITLE;
	MenuEntryTopics_[1] = MiscStrs::DLOG_SYSTEM_INFO_TITLE;
	MenuEntryTopics_[2] = MiscStrs::DLOG_EST_SST_TITLE;		//	$[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DiagLogMenuSubScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DiagLogMenuSubScreen::~DiagLogMenuSubScreen(void)
{
	CALL_TRACE("DiagLogMenuSubScreen::~DiagLogMenuSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate() [virtual]
//
//@ Interface-Description
// Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the position, the format of and number of entries in the 
// ScrollableLog menu.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DiagLogMenuSubScreen::activate(void)
{
	CALL_TRACE("DiagLogMenuSubScreen::activate(void)");

	// Create the scrollable log
	pMenu_ = ScrollableLog::New(this, MENU_NUM_ROWS_, MENU_NUM_COLUMNS_,
									MENU_ROW_HEIGHT_, FALSE);

	// Setup menu stuff				   
	pMenu_->setX(MENU_X_);				  
	pMenu_->setY(MENU_Y_);				  
	pMenu_->setUserSelectable(TRUE);			

	// Setup column info for the menu's ScrollableLog     
	pMenu_->setColumnInfo(1, MiscStrs::DLOG_TEST_TYPE_LABEL, 
							MENU_COLUMN1_WIDTH_, LEFT, 
							10, TextFont::NORMAL, TextFont::FOURTEEN);
	pMenu_->setColumnInfo(0, MiscStrs::DLOG_TEST_SELECT_LABEL, 
							MENU_COLUMN2_WIDTH_, LEFT, 
							10, TextFont::THIN, TextFont::TEN, TextFont::TEN);

	// Inform menu_ of the number of entries in the menu (this action
	// refreshes the menu if it's currently displayed.
	pMenu_->setEntryInfo(0, MAX_DIAGNOSTIC_LOGS_);

	addDrawable(pMenu_);
	
	// Activate the menu
	pMenu_->activate();										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate() [virtual]
//
//@ Interface-Description
// Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DiagLogMenuSubScreen::deactivate(void)
{
	CALL_TRACE("DiagLogMenuSubScreen::deactivate(void)");

	// Remove scrollable log from the display
	removeDrawable(pMenu_);									//	$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
//	Called when the ScrollableLog needs to update the contents of one of its 
//	cells.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First, turn off CheapText option because no cheap text is used here.
//	Next, for the given column in the ScrollableLog menu, set the appropriate
//	column title.
//---------------------------------------------------------------------
//@ PreCondition
//  Entry and column numbers has to be within bounds set. 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiagLogMenuSubScreen::getLogEntryColumn(Uint16 entry, Uint16 column,
		Boolean &rIsCheapText, StringId &rString1, StringId &rString2, StringId &, StringId &)
{
	CALL_TRACE("DiagLogMenuSubScreen::getLogEntryColumn(Uint16 entry, Uint16 column, "
				"Boolean &rIsCheapText, StringId &rString1, "
				"StringId &rString2 StringId & StringId &)");

	CLASS_PRE_CONDITION((entry < MAX_DIAGNOSTIC_LOGS_) && (column < MENU_NUM_COLUMNS_));

	// No cheap text is used for any menu information
	rIsCheapText = FALSE;

	switch (column)
	{						     
	case 1:					      // $[TI1] 
		// Column 0, the Topic column.  Only 1 line of text is used so
		// set rString1 to the correct help topic.	
		rString1 =  MenuEntryTopics_[entry]; 
		rString2 = NULL;				  
		break;					    

	case 0:												// 	$[TI2]
		// Column 1, the Select column.  Contains only selection buttons so
		// no text is displayed.  Set both rString1 and rString2 to null.
		rString1 = NULL_STRING_ID;
		rString2 = NULL_STRING_ID;
	break;

    default:
		SAFE_CLASS_ASSERTION_FAILURE();	    // Cannot be reached.
	break;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: currentLogEntryChangeHappened
//
//@ Interface-Description
//	Called when the user selects a new entry or deselects the current entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Debounce the selection button then remove the ScrollableLog menu from 
//	display.  Retrieve the DiagCodeLogSubScreen subscreen.  Set the Log Type
//	of this subscreen to be the log entry chosen by the user.  Next, fetch
//	the Upper Other Screens tab button; activate the DiagCodeLogSubScreen and 
//	associate this tab button with it.  Finally, force the tab button back down
//	since activating a new subscreen causes a previously selected button to 
// 	automatically pop up.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiagLogMenuSubScreen::currentLogEntryChangeHappened(Uint16 changedEntry, Boolean)
{
    CALL_TRACE("DiagLogMenuSubScreen::currentLogEntryChangeHappened("
				    "Uint16 changedEntry, Boolean isSelected)");

	// User selected a menu topic.  First debounce the selection.
	pMenu_->clearSelectedEntry();

	TabButton*  pTabButton = NULL;					// Selected tab button
	DiagCodeLogSubScreen*   pSubscreen = NULL;      // Subscreen to be activated

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{	// Service Mode									// $[TI1]
		pSubscreen = UpperSubScreenArea::GetDiagCodeLogSubScreen();
		pTabButton = ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->
										getDiagResultTabButton();
	}
	else									// Normal ventilation Mode
	{														//	$[TI2]
		// Fetch the subscreen associated with the pressed button.
		pSubscreen = UpperSubScreenArea::GetDiagCodeLogSubScreen();
		pTabButton = UpperScreen::RUpperScreen.getUpperScreenSelectArea()->
										getUpperOtherScreensTabButton();
	}
	CLASS_ASSERTION(pSubscreen != NULL);
	CLASS_ASSERTION(pTabButton != NULL);

	switch(changedEntry)
	{	
	case DL_SYSTEM_ENTRY_:								// $[TI3.1]
		pSubscreen->setLogType(DiagCodeLogSubScreen::DL_SYSTEM);
		break;

	case DL_SYS_INFO_ENTRY_:							  		// $[TI3.2]
		pSubscreen->setLogType(DiagCodeLogSubScreen::DL_SYS_INFO);
		break; 

	case DL_EST_SST_ENTRY_:								// $[TI3.3]
		pSubscreen->setLogType(DiagCodeLogSubScreen::DL_EST_SST);
		break;

	default:
		break;
	}

	//
	// Activate the subscreen and force the Upper Other Screens tab button
	// down. $[01063]
	//							
	getSubScreenArea()->activateSubScreen(pSubscreen, pTabButton);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiagLogMenuSubScreen::SoftFault(const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, DIAGLOGMENUSUBSCREEN,
                            lineNumber, pFileName, pPredicate);
}
