#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhPressureSensorTest - BD Safety Net Test for the detection
//                            of exhalation pressure sensor reading OOR
//---------------------------------------------------------------------
//@ Interface-Description
//  The checkCriteria() method of this class is called from the
//  newCycle() method of the SafetyNetTestMediator class.  The
//  checkCriteria() method invokes methods in the Sensor class to
//  obtain the value of the expiratory and inspiratory pressure
//  sensor readings.
//---------------------------------------------------------------------
//@ Rationale
//  Used to verify that the expiratory pressure is reading within the
//  specified tolerance.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The safety net test is defined in the checkCriteria() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/ExhPressureSensorTest.ccv   10.7   08/17/07 09:36:38   pvcs  
//
//@ Modification-Log
//
//  Revision: 005  By: syw     Date:  06-Jan-2000    DR Number: 5590
//  Project:  NeoMode
//  Description:
//      In checkCriteria(), avoid checking condition if during an autozero.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code when fault is detected.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  20-Sep-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "ExhPressureSensorTest.hh"
#include "MainSensorRefs.hh"

//@ Usage-Classes
#include "BreathRecord.hh"
#include "PressureSensor.hh"
#include "BreathMiscRefs.hh"
#include "BreathPhaseScheduler.hh"
#include "PressureXducerAutozero.hh"
//@ End-Usage

//@ Code...

#if defined( SIGMA_SAFETY_NET_DEBUG )
#include <stdio.h>
#endif // defined( SIGMA_SAFETY_NET_DEBUG )

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhPressureSensorTest()  
//
//@ Interface-Description
//  Default Constructor.  Passes two arguments to the base class
//  constructor to define the error code and time criterion.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The backgndEventId_ and maxCyclesCriteriaMet_ data members are
//  set by the base class constructor.  The background event id is
//  a unique identifier that is placed in the diagnostic log if the
//  background check detects a problem.  The maximum Cycles that the
//  criteria can be met before the Background subsystem is notified
//  is MAX_PRESSURE_OOR_CYCLES.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

ExhPressureSensorTest::ExhPressureSensorTest( void ):
SafetyNetTest( BK_EXH_PRESS_OOR, MAX_PRESSURE_OOR_CYCLES )
{
    // $[TI1]    
    CALL_TRACE("ExhPressureSensorTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExhPressureSensorTest() 
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

ExhPressureSensorTest::~ExhPressureSensorTest( void )
{
    CALL_TRACE("~ExhPressureSensorTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCriteria()
//
//@ Interface-Description
//  This method takes no argument and returns the a BkEventName.
//  This method is responsible for detecting the exhalation pressure 
//  sensor reading OOR.
//  If the background check criteria have been met for the required
//  number of cycles, the backgndEventId for the class is returned;
//  otherwise BK_NO_EVENT is returned to indicate to the caller that
//  the Background subsystem should not be notified.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method first checks the status of the backgndCheckFailed_
//  data member.  If it is TRUE, the test is no longer run since
//  this Background event is not auto-resettable and the Background
//  subsystem only needs to be informed of an event once.  In this
//  case, BK_NO_EVENT is returned to the caller.
//
//  Otherwise, the following test is performed:
//
//      ( Exhalation Pressure < MIN_PRES_TEST_LIMIT
//        and
//        Inspiratory Pressure > PRESSURE_SENSOR_STUCK_CRITERIA )
//        OR
//      Exhalation Pressure > MAX_PRES_TEST_LIMIT
//
//  $[06070] $[06071]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
ExhPressureSensorTest::checkCriteria( void )
{
    CALL_TRACE("ExhPressureSensorTest::checkCriteria( void )") ;

    BkEventName rtnValue = BK_NO_EVENT;

	const Real32 exhPressure = RExhPressureSensor.getValue();
	const Real32 inspPressure = RInspPressureSensor.getValue();
    // Only run the check if it hasn't already failed
    // Check to see not in OSC scheduler since occlusion may 
    // cause the exhalation pressure sensor to appear OOR 
	// if a suctioning manuever is taking place ignore checking for
	// background failure for the next 10 seconds
	// allows for transient spikes to settle and the pressure transducers
	// to track closer together again.
    if ( ( ! backgndCheckFailed_ ) &&
    		! RPressureXducerAutozero.getIsAutozeroInProgress() &&
         ( (BreathPhaseScheduler::GetCurrentScheduler()).getId()
            != SchedulerId::OCCLUSION ) &&
		  !IsSuctioning(exhPressure, inspPressure) )
    {

        if ( ( ( exhPressure < MIN_PRES_TEST_LIMIT ) &&
               ( inspPressure > PRESSURE_SENSOR_STUCK_CRITERIA ) ) ||
             ( exhPressure > MAX_PRES_TEST_LIMIT ) )
        {
            if ( ++numCyclesCriteriaMet_ >= maxCyclesCriteriaMet_ )
            {
                errorCode_ = (Uint16)(exhPressure*100.0F);
                backgndCheckFailed_ = TRUE;
                rtnValue = backgndEventId_;
            }
        }
        else  // criteria not met
        {
            numCyclesCriteriaMet_ = 0;
        }
    }
    else  // criteria not met
    {
        numCyclesCriteriaMet_ = 0;
    }
    
    return( rtnValue );
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
ExhPressureSensorTest::SoftFault( const SoftFaultID  softFaultID,
                                     const Uint32       lineNumber,
                                     const char*        pFileName,
                                     const char*        pPredicate )
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

    FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, EXHPRESSURESENSORTEST,
                             lineNumber, pFileName, pPredicate );
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


