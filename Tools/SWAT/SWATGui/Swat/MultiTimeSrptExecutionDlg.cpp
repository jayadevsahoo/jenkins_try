// MultiTimeSrptExecutionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Swat.h"
#include "MultiTimeSrptExecutionDlg.h"


// CMultiTimeSrptExecutionDlg dialog

IMPLEMENT_DYNCREATE(CMultiTimeSrptExecutionDlg, CDHtmlDialog)

CMultiTimeSrptExecutionDlg::CMultiTimeSrptExecutionDlg(CWnd* pParent /*=NULL*/)
	: CDHtmlDialog(CMultiTimeSrptExecutionDlg::IDD, CMultiTimeSrptExecutionDlg::IDH, pParent)
{

}

CMultiTimeSrptExecutionDlg::~CMultiTimeSrptExecutionDlg()
{
}

void CMultiTimeSrptExecutionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDHtmlDialog::DoDataExchange(pDX);
}

BOOL CMultiTimeSrptExecutionDlg::OnInitDialog()
{
	CDHtmlDialog::OnInitDialog();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

BEGIN_MESSAGE_MAP(CMultiTimeSrptExecutionDlg, CDHtmlDialog)
END_MESSAGE_MAP()

BEGIN_DHTML_EVENT_MAP(CMultiTimeSrptExecutionDlg)
	DHTML_EVENT_ONCLICK(_T("ButtonOK"), OnButtonOK)
	DHTML_EVENT_ONCLICK(_T("ButtonCancel"), OnButtonCancel)
END_DHTML_EVENT_MAP()



// CMultiTimeSrptExecutionDlg message handlers

HRESULT CMultiTimeSrptExecutionDlg::OnButtonOK(IHTMLElement* /*pElement*/)
{
	OnOK();
	return S_OK;
}

HRESULT CMultiTimeSrptExecutionDlg::OnButtonCancel(IHTMLElement* /*pElement*/)
{
	OnCancel();
	return S_OK;
}
