#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BinaryCommand - digital output interface class
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides an interface to the system digital I/Os.  It 
//  allow its user to toggle the desired bit on the designated
//  register high or low. 
//---------------------------------------------------------------------
//@ Rationale
//  Low-level device driver utility.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Toggles the desired bit at the designated register high or low.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BinaryCommand.ccv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002  By: syw    Date:  19-Dec-1996    DR Number: DCS 1603
//  	Project:  Sigma (R8027)
//		Description:
//			Changed Register to BitAccessGpio.  Added setBit() method.
//			Changed bitMask_ to bit_.  Added latchBit() method.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BinaryCommand.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BinaryCommand()
//
//@ Interface-Description
//  	Constructor.  This method has a mask and a bit access register
//		as arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called.  The mask is converted into a
//		bit location with bit 0 as the LSB.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

BinaryCommand::BinaryCommand( const ApplicationDigitalOutputs_t bit, BitAccessGpio& bitAccess)
              : rBitAccessGpio_(bitAccess)
{
	CALL_TRACE("BinaryCommand::BinaryCommand( const Uint16 mask, BitAccessGpio& reg)") ;
	
	bit_ = bit ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BinaryCommand()  [Destructor]
//
//@ Interface-Description
//  	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

BinaryCommand::~BinaryCommand(void)
{
    CALL_TRACE("BinaryCommand::~BinaryCommand(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setBit
//
//@ Interface-Description
//  This method accepts a bit state (on or off) as an arguement and returns
//	no value.  This method requests to set the designated bit to high or low.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method sets the designated bit to high or low.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void 
BinaryCommand::setBit( BitAccessGpio::BitState bitState)
{
	CALL_TRACE("BinaryCommand::setBit( BitState bitState)") ;

	if (bitState == BitAccessGpio::ON)
	{
	    // $[TI1]
	    rBitAccessGpio_.requestBitUpdate( bit_, BitAccessGpio::ON);
	}
	else if (bitState == BitAccessGpio::OFF)
	{
	    // $[TI2]
	    rBitAccessGpio_.requestBitUpdate( bit_, BitAccessGpio::OFF) ;
	}
	else
	{
		CLASS_PRE_CONDITION( bitState == BitAccessGpio::ON || bitState == BitAccessGpio::OFF) ;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: latchBit
//
//@ Interface-Description
//  This method accepts a bit state (on or off) as an arguement and returns
//	no value.  This method requests to latche the designated bit to high or low.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method latches the designated bit to high or low.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void 
BinaryCommand::latchBit( BitAccessGpio::BitState bitState)
{
	CALL_TRACE("BinaryCommand::latchBit( BitState bitState)") ;

	if (bitState == BitAccessGpio::ON)
	{
	    // $[TI1]
	    rBitAccessGpio_.enableLatch( bit_, BitAccessGpio::ON);
	}
	else if (bitState == BitAccessGpio::OFF)
	{
	    // $[TI2]
	    rBitAccessGpio_.enableLatch( bit_, BitAccessGpio::OFF) ;
	}
	else
	{
		CLASS_PRE_CONDITION( bitState == BitAccessGpio::ON || bitState == BitAccessGpio::OFF) ;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition 
//  None.
//@ End-Method 
//===================================================================== 

void
BinaryCommand::SoftFault( const SoftFaultID  softFaultID,
                          const Uint32       lineNumber,
                          const char*        pFileName,
                          const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES, BINARYCOMMAND,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================

