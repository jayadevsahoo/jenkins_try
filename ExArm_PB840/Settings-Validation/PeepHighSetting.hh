
#ifndef PeepHighSetting_HH
#define PeepHighSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PeepHighSetting - PEEP High Level for BiLevel Breaths
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PeepHighSetting.hhv   25.0.4.0   19 Nov 2013 14:27:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 003   By: dosman  Date:  11-Nov-1998    DR Number: 5247
//  Project:  BILEVEL
//  Description:
//  	Changed from DoubleIntervalRange to SingleIntervalRange
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: dosman  Date:  08-Jan-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial version for BiLevel.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
//@ End-Usage


class PeepHighSetting : public BatchBoundedSetting 
{
  public:
    PeepHighSetting(void); 
    virtual ~PeepHighSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
                                   const DiscreteValue            newValue,
                                   const DiscreteValue            currValue);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual void  updateConstraints_(void);

  private:
    PeepHighSetting(const PeepHighSetting&);	   // not implemented...
    void  operator=(const PeepHighSetting&);   // not implemented...
};


#endif // PeepHighSetting_HH 
