#if !defined(AFX_SEQUENCESET_H__617D1250_54A5_11D3_9C97_004005519CCD__INCLUDED_)
#define AFX_SEQUENCESET_H__617D1250_54A5_11D3_9C97_004005519CCD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SequenceSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSequenceSet DAO recordset

class CSequenceSet : public CDaoRecordset
{
public:
	CSequenceSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSequenceSet)

// Field/Param Data
	//{{AFX_FIELD(CSequenceSet, CDaoRecordset)
	long	m_ID;
	COleDateTime	m_time;
	CString	m_User;
	long	m_P_F;
	CString	m_UUTPN;
	CString	m_UUTBDSN;
	CString	m_UUTBDRunTime;
	CString	m_UUTRev;
	CString	m_BDPost;
	CString	m_GUISN;
	CString	m_GUIPost;
	CString	m_Saas;
	CString	m_CompSN;
	CString	m_CompRunTime;
	CString	m_PTSSN;
	CString	m_PTSVer;
	CString	m_PTSLastCal;
	CString	m_PTSNextCal;
	BOOL	m_PTSOutofCal;
	CString	m_VentAsset;
	CString	m_WorkOrder;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSequenceSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEQUENCESET_H__617D1250_54A5_11D3_9C97_004005519CCD__INCLUDED_)
