#ifndef OperatingParamEventName_HH
#define OperatingParamEventName_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename: OperatingParamEventName - Enumerated names for the Operating
//        parameter events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/OperatingParamEventName.hhv   25.0.4.0   19 Nov 2013 13:51:24   pvcs  $
//
//@ Modification-Log  
//
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct    Date: 05/22/95         DR Number:   7
//      Project:   Sigma   (R8027)
//      Description:
//         Added events to support new battery alarms as per changes 
//         to Non-Technical Alarm text and structure as per changes to SRS.
//  
//   Revision 003   By:   hct    Date: 08/08/95         DR Number:   571
//      Project:   Sigma   (R8027)
//      Description:
//         Added event ALARM_BROADCAST_EN to support Alarm information 
//         broadcasts.
//  
//   Revision 004   By:   hct    Date: 01/11/96         DR Number:   649
//      Project:   Sigma   (R8027)
//      Description:
//         Changed conditional compilation label from SIGMA_DEVELOPMENT to
//         SIGMA_SIMULATOR.
//  
//   Revision 005   By:   gbs      Date: 05/17/96         DR Number:   995
//      Project:   Sigma   (R8027)
//      Description:
//         Update header comments.
//         These changes do not affect Unit Test.
//  
//   Revision 006   By:   hct      Date: 11/04/96         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Added events HUNDRED_PERCENT_O2_EN, NOT_HUNDRED_PERCENT_O2_EN.
//  
//   Revision 007   By:   hct    Date: 09/29/97         DR Number:  1475
//      Project:   Sigma   (R8027)
//      Description:
//         Add VOLUME LIMIT alarm.
//  
//   Revision 008   By:   hct      Date: 01/13/99       DR Number: 5322
//      Project:   Sigma   (R8O27)
//      Description:
//         Add ATC functionality.
//
//  Revision: 009   By: syw   Date:  07-Aug-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//      Add PAV events.
//
//  Revision: 010   By: hct   Date:  30-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//      Add VTPC events.
//
//  Revision: 011   By: hct   Date:  11-SEP-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//      Changed VTPC events.
//
//  Revision: 012   By:   hct    Date: 24-OCT-2000      DR Number: 5755
//  Project:  VTPC
//  Description:
//      Differentiate between spont and mand breaths for VTPC alarms.
//
//   Revision 013   By:   heatherw  Date: 03/07/2002    DR Number:   5790
//   Project:   VCP aka VCP
//   Description:
//       Added new alarm event name: INSPIRED_MAND_VOLUME_LIMIT_EN
//       Modified INSPIRED_VOLUME_LIMIT_EN to be 
//       INSPIRED_SPONT_VOLUME_LIMIT_EN.
//
//   Revision 014   By:   gdc  Date:  02/21/2005    DR Number:   6144
//   Project:  NIV1
//   Description:
//       DCS 6144 - NIV1
//       Modifications to support NIV.
//       Removed obsolete low insp pressure event.
//       Added vent-type setting change event.
//
//   Revision 015   By:   rhj    Date: 05-March-2007      DR Number: 6359
//   Project:  RESPM
//   Description:
//       Resolve merge conflict for RM to Baseline 
//
//   Revision 016   By:   rhj    Date: 26-Jan-2009       SCR Number:  6452
//   Project:  840S
//   Description:
//       Added  DISCONNECT_WITH_COMPRESSOR_EN.
// 
//   Revision 017   By:   rhj    Date: 17-Feb-2010       SCR Number: 6436
//   Project:  PROX
//   Description:
//       Added PROX_ALARM_OFF_EN and PROX_ALARM_ON_EN.
//
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
//@ End-Usage

//@ Type: OperatingParameterEventName
// Enumeration OperatingParameterEventName enumerates the event messages
// which are sent to the Alarm task via the OperatingParameterEventQueue.
enum OperatingParameterEventName 
{
   EVENT_NAME_NULL = -1,
   WALL_AIR_PRESENT_EN,
   NOT_WALL_AIR_PRESENT_EN,
   IN_APNEA_VENTILATION_EN,
   NOT_IN_APNEA_VENTILATION_EN, 
   INSP_TIME_TOO_LONG_EN, 
   END_OF_BREATH_EN, 
   BTAT_1_EN,      NOT_BTAT_1_EN,  BTAT_2_EN,      NOT_BTAT_2_EN,  BTAT_3_EN,  
   NOT_BTAT_3_EN,  BTAT_4_EN,      NOT_BTAT_4_EN,  BTAT_5_EN,      NOT_BTAT_5_EN,
   BTAT_6_EN,      NOT_BTAT_6_EN,  BTAT_7_EN,      NOT_BTAT_7_EN,  BTAT_8_EN, 
   NOT_BTAT_8_EN,  BTAT_9_EN,      NOT_BTAT_9_EN,  BTAT_10_EN,     NOT_BTAT_10_EN,
   BTAT_11_EN,     NOT_BTAT_11_EN, BTAT_12_EN,     NOT_BTAT_12_EN, BTAT_13_EN,
   NOT_BTAT_13_EN, BTAT_14_EN,     NOT_BTAT_14_EN, BTAT_15_EN,     NOT_BTAT_15_EN,
   BTAT_16_EN,     NOT_BTAT_16_EN, BTAT_17_EN,     NOT_BTAT_17_EN,
   HIGH_CIRCUIT_PRESSURE_EN, 
   COMP_AIR_PRESENT_EN,
   NOT_COMP_AIR_PRESENT_EN,
   COMP_OPTION_PRESENT_EN,
   NOT_COMP_OPTION_PRESENT_EN,
   FIO2_HIGH_EN,
   FIO2_NORMAL_EN,
   FIO2_LOW_EN, 
   TRIGGER_SETTING_CHANGE_EN, 
   MINOR_POST_FAULT_EN, 
   NOT_MINOR_POST_FAULT_EN, 
   DISCONNECT_EN,
   DISCONNECT_WITH_COMPRESSOR_EN,
   NOT_DISCONNECT_EN,
   STARTUP_DISCONNECT_EN,
   NOT_STARTUP_DISCONNECT_EN,
   WALL_O2_PRESENT_EN, 
   NOT_WALL_O2_PRESENT_EN, 
   O2_MONITOR_INSTALLED_EN,
   NOT_O2_MONITOR_INSTALLED_EN,
   O2_PERCENT_SETTING_CHANGE_EN, 
   INTERNAL_BATTERY_POWER_EN, 
   NOT_INTERNAL_BATTERY_POWER_EN,
   BATTERY_TIME_LEFT_LT_2_MIN_EN,
   NOT_BATTERY_TIME_LEFT_LT_2_MIN_EN,
   NON_FUNCTIONAL_BATTERY_EN,
   NOT_NON_FUNCTIONAL_BATTERY_EN,
   CHARGE_TIME_GT_8_HRS_EN,
   NOT_CHARGE_TIME_GT_8_HRS_EN,
   PROCEDURE_ERROR_EN,
   NOT_PROCEDURE_ERROR_EN,
   LOW_AC_POWER_EN, 
   NOT_LOW_AC_POWER_EN,
   SEVERE_OCCLUSION_EN, 
   NOT_SEVERE_OCCLUSION_EN, 
   AVERAGED_PATIENT_DATA_EN, 
   VENT_PRESSURE_EN, 
   LOSS_PAT_DATA_MON_EN,
   NOT_LOSS_PAT_DATA_MON_EN,
   HUNDRED_PERCENT_O2_EN,
   NOT_HUNDRED_PERCENT_O2_EN,
   VOLUME_LIMIT_EN,
   INSPIRED_SPONT_VOLUME_LIMIT_EN, 
   INSPIRED_SPONT_PRESSURE_LIMIT_EN,
   INSPIRED_MAND_VOLUME_LIMIT_EN,
   COMPENSATION_LIMIT_EN,
   PAV_STARTUP_EN,
   NOT_PAV_STARTUP_EN,
   PAV_ASSESSMENT_EN,
   NOT_PAV_ASSESSMENT_EN,
   VOLUME_NOT_DELIVERED_VC_EN,
   VOLUME_NOT_DELIVERED_VS_EN,
   MANUAL_RESET_EN,
   ALARM_BROADCAST_EN,
   LOI_LED_ON_EN,
   LOI_LED_OFF_EN,
   BD_ALARM_ON_EN,
   BD_ALARM_OFF_EN, 
   VENT_TYPE_SETTING_CHANGE_EN,
   PROX_ALARM_OFF_EN,
   PROX_ALARM_ON_EN,
#ifdef SIGMA_SIMULATOR
   CLEAR_ALARM_LOG_EN, 
   ALARM_SILENCE_EN, 
   END_ALARM_SILENCE_EN, 
   DISPLAY_ALARM_LOG_EN, 
   DISPLAY_CURRENT_ALARMS_EN,
#endif  // SIGMA_SIMULATOR
   MAX_NUM_OPERATING_PARAMETER_EVENT_NAMES 
        };

#endif  // OperatingParamEventName_HH
