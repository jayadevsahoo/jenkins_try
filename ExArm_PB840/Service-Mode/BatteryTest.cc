#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BatteryTest -- Perform system battery test
//---------------------------------------------------------------------
//@ Interface-Description
//
//  The purpose of this class is to test the Backup Power Supply (BPS).
//	If a BPS is not installed, the test will exit immediately indicating
//	a "not installed" status.  If a BPS is installed, then it will
//	first be tested to verify battery is fully charged.  If fully charged,
//	the BPS is then tested under discharge.  If this passes, the BPS
//	is finally tested to verify that it is recharging.
//---------------------------------------------------------------------
//@ Rationale
//  This test ensures that the BPS, if installed, is working properly.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  The public method runTest() is provided for the battery test.
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/BatteryTest.ccv   25.0.4.0   19 Nov 2013 14:23:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  syw    Date: 28-Sep-1998    DR Number: DCS 5181
//      Project:  BiLevel
//     	Description:
//			eliminate function declaration to enableCompressorAndControl(),
//			call it instead.
//
//  Revision: 001  By:  mad    Date:  15-Mar-1996    DR Number: DCS 2204
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "BatteryTest.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmManager.hh"
#include "SmUtilities.hh"
#include "SmFlowController.hh"
#include "SmPressureController.hh"
#include "SmExhValveController.hh"
#include "SmGasSupply.hh"

#include "Task.hh"
#include "OsTimeStamp.hh"

#include "MiscSensorRefs.hh"
#include "ValveRefs.hh"
#include "VentObjectRefs.hh"
#include "Psol.hh"
#include "Solenoid.hh"

#ifdef SIGMA_DEBUG
#include <stdio.h>		// for printf()
#endif // SIGMA_DEBUG

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"
# include "BatteryTest_UT.hh"

# include "FakeTask.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage


//@ Code...

// min voltage for "early" voltage tests
const Real32 MIN_INIT_VOLTS_DISCHARGING = 23.5;  // V

// min voltage for "late" voltage tests
const Real32 MIN_FINAL_VOLTS_DISCHARGING = 22.8;  // V

// max voltage drop allowed
const Real32 MAX_DELTA_VOLTAGE = 2.0;  // V

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: BatteryTest
//
//@ Interface-Description
//  	Constructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=======================================================================
BatteryTest::BatteryTest( void)
{ 
	CALL_TRACE("BatteryTest::BatteryTest( void)") ;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~BatteryTest
//
//@ Interface-Description
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
BatteryTest::~BatteryTest( void)
{
	CALL_TRACE("BatteryTest::~BatteryTest( void)") ;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runTest
//
//@ Interface-Description
//
//	This method performs the battery test.  No arguments, no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	$[09188]
//	Phase 1: test if BPS is installed.
//	Phase 2: test if battery is fully charged.
//	Phase 3: unplug AC power and test battery as it discharges.
//	Phase 4: connect AC power and test if battery is recharging.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
void
BatteryTest::runTest( void)
{
	CALL_TRACE("BatteryTest::void runTest( void)") ;

	// Check if a battery is installed
	SmUtilities::TestResult bpsStatus1 =
#ifdef SIGMA_UNIT_TEST
										BatteryTest_UT::BpsStatus1;
#else
										RSmUtilities.getBpsStatus();
#endif  // SIGMA_UNIT_TEST

	if (bpsStatus1 == SmUtilities::NOT_INSTALLED)
	{
	// $[TI1]
		// No battery installed
		RSmManager.sendOptionNotInstalledToServiceData();
	}
	else if (bpsStatus1 != SmUtilities::CHARGED)
	{
	// $[TI2]
		// Battery not fully charged
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_1);
	}
	else
	{
	// $[TI3]
		// BPS is installed and charged, so continue

		// disable the compressor
		RSmGasSupply.disableCompressorAndControl();

		// prompt to unplug AC
		if (!RSmUtilities.messagePrompt(SmPromptId::UNPLUG_AC_PROMPT))
		{
		// $[TI3.1]
			RSmManager.sendOperatorExitToServiceData();
		}
		else
		{
		// $[TI3.2]
			// apply additional system load
			const Int16 PSOL_COUNT = 0x500;
			RAirPsol.updatePsol(PSOL_COUNT);
			RO2Psol.updatePsol(PSOL_COUNT);
			const Int16 EV_COUNT = 0x700;
			RSmUtilities.commandExhValve(EV_COUNT);
			RSmUtilities.closeSafetyValve();

			// Allow time for power source change to take effect
			Task::Delay(2);

			// check battery while it's discharging -- if it passes,
			// check to make sure battery can switch to charging state
			Boolean dischargeTestPassed =
#ifdef SIGMA_UNIT_TEST
								BatteryTest_UT::DischargeTestPassed;
#else
								checkDischarging_();
#endif  // SIGMA_UNIT_TEST

			// command PSOLs closed and exh valve open, keep safety valve closed
			RAirPsol.updatePsol(0);
			RO2Psol.updatePsol(0);
			RSmUtilities.openExhValve();

			if ( !RSmUtilities.messagePrompt(
					SmPromptId::CONNECT_AC_PROMPT) )
			{
			// $[TI3.2.1]
				RSmManager.sendOperatorExitToServiceData();
			}
			else if (dischargeTestPassed)
			{
			// $[TI3.2.2]
				// Allow time for power source change to take effect
				Task::Delay(2);

				SmUtilities::TestResult bpsStatus2 =
#ifdef SIGMA_UNIT_TEST
									BatteryTest_UT::BpsStatus2;
#else
									 RSmUtilities.getBpsStatus();
#endif  // SIGMA_UNIT_TEST

				if (bpsStatus2 != SmUtilities::CHARGING)
				{
				// $[TI3.2.2.1]
					// Battery charging test failed
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_ALERT,
						SmStatusId::CONDITION_4);
				}
				// $[TI3.2.2.2]
				// implied else
			}
			// $[TI3.2.3]
			// implied else: operator exit or discharge test failed
		}
		// end else

		// enable compressor
		RSmGasSupply.enableCompressorAndControl();
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BatteryTest::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("BatteryTest::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, BATTERYTEST,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: checkDischarging_
//
//@ Interface-Description
//	$[09160]
//  This method is used to test the battery under load (i.e. switch system to
//  battery power).  No arguments.  The test pass/fail status is returned.
//-----------------------------------------------------------------------
//@ Implementation-Description
//  If bps status != ON_BATTERY_POWER (system running on AC power)
//		generate an ALERT, skip to END.
//  Create additional load by energizing safety and exh valves and PSOLs.
//	Start marking time (T=0 secs).
//	Measure voltage (Vinit) at T=30 secs and if Vinit < Vinit_min,
//		generate an ALERT and skip to END.
//	Repeat voltage testing every 30 secs until T=300 secs:
//		If (next voltage reading - Vinit) falls below the delta voltage min,
//			exit loop and generate an ALERT.
//		Else if next voltage reading < Vfinal_min,
//			exit loop and generate an ALERT.
//	END:
//	Restore AC power.
//	Return TRUE if test passed, FALSE if not.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Boolean
BatteryTest::checkDischarging_( void)
{
	CALL_TRACE("BatteryTest::checkDischarging_( void)");

	const Int32 MAX_ITERATIONS = 9;
	const Uint32 BATT_SAMPLING_TIME = 30000;  // msec
	Real32 newVoltage = 0.0;
	Real32 deltaVoltage = 0.0;
	Uint32 delaySinceLastRead = 0;
	Uint32 lastReadTime = 0;
	Int32 counter = 0;
	Boolean testPassed = TRUE;

	// check if system is running on BPS power
	SmUtilities::TestResult bpsStatus =
#ifdef SIGMA_UNIT_TEST
									BatteryTest_UT::BpsStatus1;
#else
									RSmUtilities.getBpsStatus();
#endif  // SIGMA_UNIT_TEST

	if (bpsStatus != SmUtilities::ON_BATTERY_POWER)
	{
	// $[TI1]
		// not running on batttery power
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_2);

		testPassed = FALSE;
	}
	else
	{
	// $[TI2]
		// verified that system is running on BPS power, so proceed

		// initialize timer
		// NOTE: T=0 secs
		OsTimeStamp timer;
	
		// delay 1 sampling interval then take initial voltage sample
		// NOTE: T=30 secs
		Task::Delay( 0, BATT_SAMPLING_TIME);
		const Real32 INIT_VOLTAGE = RSystemBatteryVoltage.getValue();
		// store the time at which voltage was read to allow
		// store the current time for delay compensation
		lastReadTime = timer.getPassedTime();

		// NOTE: sending data to Service Data has a built-in delay, so
		// need to compensate the next sampling delay
		RSmManager.sendTestPointToServiceData(
									SmDataId::BATTERY_VOLTAGE1, INIT_VOLTAGE);

		// check initial voltage before continuing
		if (INIT_VOLTAGE < MIN_INIT_VOLTS_DISCHARGING)
		{
		// $[TI2.1]
			// Battery discharge test failed
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_3);

			testPassed = FALSE;
		}
		else			
		{
		// $[TI2.2]
			// initial voltage OK, proceed

			// setup loop counter then loop for the rest of the samples @
			// T=60, 90, ...,300 secs
			counter = 0;
			do
			{
			// $[TI2.2.1]
				// compensate the sampling delay by subtracting the delay which
				// already occurred since the last data read
				delaySinceLastRead = timer.getPassedTime() - lastReadTime;
				Task::Delay( 0, BATT_SAMPLING_TIME - delaySinceLastRead);
			
				// store the current time for delay compensation
				lastReadTime = timer.getPassedTime();
				// store old voltage, get new voltage, compute delta
				newVoltage = RSystemBatteryVoltage.getValue();
				deltaVoltage =  INIT_VOLTAGE - newVoltage;

				// NOTE: sending data to Service Data has a built-in delay, so
				// need to compensate the sampling delay
				RSmManager.sendTestPointToServiceData(
										SmDataId::BATTERY_VOLTAGE2, newVoltage);

				RSmManager.sendTestPointToServiceData(
								SmDataId::BATTERY_DELTA_VOLTAGE, deltaVoltage);

				// increment counter
				counter++;
#ifdef SIGMA_UNIT_TEST
				// force a single iteration only during unit test
				counter = MAX_ITERATIONS;
#endif  // SIGMA_UNIT_TEST

			}  while ( counter < MAX_ITERATIONS &&
						newVoltage >= MIN_FINAL_VOLTS_DISCHARGING &&
						deltaVoltage <= MAX_DELTA_VOLTAGE );

#ifdef SIGMA_DEBUG
			Uint32 elapsedTime = timer.getPassedTime();
			printf("Discharge test time = %u msec\n", elapsedTime);
#endif // SIGMA_DEBUG

			// check voltage and delta voltage criteria
			if ( deltaVoltage > MAX_DELTA_VOLTAGE ||
					newVoltage < MIN_FINAL_VOLTS_DISCHARGING )
			{
			// $[TI2.2.2]
				// Battery discharge test failed
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT,
					SmStatusId::CONDITION_3);

				testPassed = FALSE;
			}
			// $[TI2.2.3]
			// implied else
		}
		// end else
	}
	// end else

	return( testPassed);
}
