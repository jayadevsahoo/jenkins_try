//
//  File:  sdk_ser.h
//
#ifndef __SER_H
#define __SER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <winioctl.h>

//------------------------------------------------------------------------------

#define IOCTL_SER_READ                 CTL_CODE(FILE_DEVICE_SERIAL_PORT,50,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define IOCTL_SER_WRITE                CTL_CODE(FILE_DEVICE_SERIAL_PORT,51,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define IOCTL_UART_REGDUMP             CTL_CODE(FILE_DEVICE_SERIAL_PORT,52,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define IOCTL_CHANGE_DEBUG             CTL_CODE(FILE_DEVICE_SERIAL_PORT,53,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define IOCTL_RESET_UART               CTL_CODE(FILE_DEVICE_SERIAL_PORT,54,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define IOCTL_DISPATCH_THREAD_PRIORITY CTL_CODE(FILE_DEVICE_SERIAL_PORT,55,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define IOCTL_ISTRXDMA_THREAD_PRIORITY CTL_CODE(FILE_DEVICE_SERIAL_PORT,56,METHOD_BUFFERED,FILE_ANY_ACCESS)
//------------------------------------------------------------------------------
#define IfDebugMask(MaskPointer,Bit)  (MaskPointer->DebugMask&Bit?TRUE:FALSE)
#define MASK_DEBUG_HWTXINTR           (1 << 0)
#define MASK_DEBUG_HWXMITCOMCHAR      (1 << 1)
#define MASK_DEBUG_DOTXDATA           (1 << 2)
#define MASK_DEBUG_EVALUATEEVENTFLAG  (1 << 3)
#define MASK_DEBUG_SER_READ           (1 << 4)
#define MASK_DEBUG_SER_WRITE          (1 << 5)
#define MASK_DEBUG_HWRXINTR           (1 << 6)
#define MASK_DEBUG_HWRXDMAINTR        (1 << 7)
#define MASK_DEBUG_HWGETIRQTYPE       (1 << 8)

#ifdef __cplusplus
}
#endif

#endif
