#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendTableSubScreen - Trend Table sub-screen
//---------------------------------------------------------------------
//@ Interface-Description 
//  The TrendTableSubScreen is a TrendSubScreen that displays a
//  tabular view of the trending data. It contains a table showing a
//  data/time column, six columns of trend data and a column for trend
//  events. The table shows seven rows of data with each row showing the
//  trended values for the selected trend parameters for the date and
//  time shown.
// 
//  This class contains the setting buttons and scrollable menus to
//  select up to six different trend parameters and a timescale
//  selection button and menu. It contains a trend cursor slider that
//  allows the user to scroll quickly through the data selecting a
//  single sample time along the time axis for which the selected row is
//  highlighted in the table. The table also provides a scrollbar to
//  allow the user to scroll through the data table using the scrollbar
//  button scrolling mechanism.
// 
//  The UpperSubScreenArea instantiates this class. The trending tab
//  on the upper screen activates this sub-screen or the
//  TrendGraphsSubScreen when the tab button is pressed down. The
//  TrendSubScreen controls which trend sub-screen is activated by
//  setting the sub-screen pointer in the tab button to the last
//  TrendSubScreen selected by the operator. The graphical view is
//  selected as the initial (default) view of the trend data.
// 
//  This class is a TrendSubScreen that provides definitions for the 
//  pure virtual methods isAnyButtonPressed_, requestTrendData_,
//  updateCursorPosition, disableScrolling and update to provide the
//  TrendSubScreen functionality. It also overloads many of the callback
//  virtual methods such as valueUpdate defined in TrendSubScreen to
//  provide specialized processing of these events. In most cases, the
//  overloaded method also calls the base class method to process these
//  events as well. In this way, the base class method provides the same
//  "common" processing of these events for all TrendSubScreens.
// 
//  This class is a LogTarget that inherits the getLogEntryColumn method
//  that is called by the TrendingLog to provide it with the textual
//  data for each cell in the the table. This method accesses the
//  TrendDataSet from the TrendDataSetAgent to get the trend data
//  requested, format it to specifications and return it the the
//  TrendingLog that formats it using LogCell for presentation on the
//  display.
// 
//  This class is a TrendEventTarget that inherits the trendDataReady
//  method. This class registers with the TrendDataSetAgent class for
//  callbacks to this method when new trend data is available from the
//  agent.
// 
//---------------------------------------------------------------------
//@ Rationale 
//  Encapsulates the display objects and methods specific to the Trend
//  Table sub-screen.
// 
//---------------------------------------------------------------------
//@ Implementation-Description 
//  The TrendTableSubScreen is a TrendSubScreen that is directly
//  instantiated in the UpperSubScreenArea. As a TrendSubScreen it
//  passes references to several of its contained Drawables to the base
//  class during construction. In this way, the TrendSubScreen base
//  class has access to these objects without having to instantiate
//  the objects as protected members in the base class and then require
//  the derived class to move and resize them according to its specific
//  requirements. In some cases, the object attributes can only be
//  defined in the constructor so this requires the object be
//  instantiated in the derived class or add "set" accessor methods to
//  change the object's constructed attributes. Instead of implementing
//  a new set of the "set" methods, we decided to instantiate in the
//  derived class and pass references to the base class.
// 
//  This class contains many objects that use the DirectDraw feature of
//  the graphics library. DirectDraw allows for drawing objects directly
//  in the frame buffer without going through the standard
//  GUI-Foundations repaint cycle. This provided the necessary
//  performance enhancements for the graphical elements of the trending
//  sub-screen, but placed much of the burden for managing the
//  overlapping graphics elements and exposure events back on the
//  application. This complicated the implementation of this class
//  considerably. This class uses numerous flags to assure that one
//  direct draw graphic doesn't write over another and when it does, the
//  underlying graphic is repainted when exposed. This implementation
//  could be simplified with an enhanced graphics library that managed
//  DirectDraw graphics more transparently.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  No special fault handling strategies apart from the usual assertions
//  and pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions 
//	none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Modification-Log
//
//  Revision: 002  By: gdc      Date:  11-Aug-2008   SCR Number: 6439
//  Project:  840S
//  Description:
//      Removed touch screen "drill-down" work-around.
//
//  Revision: 001  By:  gdc	   Date:  30-Jun-2007    DCS Number: 6237
//  Project:  TREND
//  Description:
//		Initial version.
//
//=====================================================================

#include "TrendTableSubScreen.hh"

//@ Usage-Classes
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "MiscStrs.hh"
#include "SettingId.hh"
#include "SettingSubject.hh"
#include "TextUtil.hh"
#include "TrendDataSetAgent.hh"
#include "TrendFormatData.hh"
#include "TrendingLog.hh"
#include "TrendSelectMenuItems.hh"
#include "TrendSelectValue.hh"
#include "TrendTimeScaleMenuItems.hh"
#include "UpperSubScreenArea.hh"

// TODO E600 removed
//#include "Ostream.hh"

//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BUTTON_BORDER_ = 2;

static const Int32 TREND_BUTTON_X_ = 1;
static const Int32 TREND_BUTTON_Y_ = 1;
static const Int32 TREND_BUTTON_WIDTH_ = 60;
static const Int32 TREND_BUTTON_HEIGHT_ = 28;

static const Int32 TABLE_ROW_HEIGHT_ = 27;
static const Int32 TABLE_BUTTON_X_ = 1;
static const Int32 TABLE_BUTTON_Y_ = 53;

static const Int32 TIME_COLUMN_WIDTH_ = 55;
static const Int32 SELECT_COLUMN_WIDTH_ = 80;
static const Int32 EVENT_DETAIL_COLUMN_WIDTH_ = 75;

static const Int32 TABLE_BUTTON_HEIGHT_ = 32;
static const Int32 TABLE_X_ = 1;
static const Int32 TABLE_Y_ = TABLE_BUTTON_Y_ + TABLE_BUTTON_HEIGHT_;
static const Int32 NUM_TABLE_ROWS_ = 7;
static const Int32 NUM_TABLE_COLUMNS_ = 8;
static const Int32 TIME_COLUMN_ = 0;
static const Int32 FIRST_SELECT_COLUMN_ = 1;
static const Int32 NUM_SELECT_COLUMNS_ = 6;
static const Int32 EVENT_DETAIL_COLUMN_ = 7;

static const Int32 TREND_TABLE_EVENT_SCROLL_X_ = 358;
static const Int32 TREND_TABLE_EVENT_SCROLL_Y_ = 166;
static const Int32 TREND_EVENT_SCROLLBAR_WIDTH_ = 10;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendTableSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen (the Upper subscreen
// area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all buttons and the subscreen title.  Adds all buttons
// and the subscreen title object to the parent container for display.
// Registers for button callbacks.
//
// $[01190] The Upper Other Screens subscreen should display ...
//---------------------------------------------------------------------
//@ PreCondition
// The given 'pSubScreenArea' pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendTableSubScreen::TrendTableSubScreen(SubScreenArea* pSubScreenArea) 
:   TrendSubScreen(         pSubScreenArea, 
							graphScreenNavButton_, 
							printButton_, 
							printingText_, 
							eventDetailButton_,
							presetsButton_
				  ),
	graphScreenNavButton_(  TREND_BUTTON_X_, 
							TREND_BUTTON_Y_,
							TREND_BUTTON_WIDTH_, 
							TREND_BUTTON_HEIGHT_,
							Button::LIGHT, BUTTON_BORDER_, Button::SQUARE,
							Button::NO_BORDER, MiscStrs::TREND_GRAPH_SCREEN_NAV_TITLE,
							NULL_STRING_ID, GRAVITY_CENTER),
	presetsButton_(         TREND_BUTTON_X_ + TREND_BUTTON_WIDTH_,
							TREND_BUTTON_Y_, 
							TREND_BUTTON_WIDTH_,
							TREND_BUTTON_HEIGHT_,
							Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
							Button::NO_BORDER,
							MiscStrs::TREND_PRESET_BUTTON_TITLE,
							SettingId::TREND_PRESET, NULL,
							MiscStrs::TREND_PRESET_BUTTON_HELP_MSG, 8),
	printButton_(           TREND_BUTTON_X_ + 2 * TREND_BUTTON_WIDTH_, 
							TREND_BUTTON_Y_,
							TREND_BUTTON_WIDTH_, 
							TREND_BUTTON_HEIGHT_,
							Button::LIGHT, BUTTON_BORDER_, Button::SQUARE,
							Button::NO_BORDER, MiscStrs::TREND_PRINT_BUTTON_TITLE),
	printingText_(          MiscStrs::PRINTING_TITLE),
	timeScaleButton_(       TABLE_BUTTON_X_, TABLE_BUTTON_Y_,
							TIME_COLUMN_WIDTH_, TABLE_BUTTON_HEIGHT_, Button::LIGHT,
							BUTTON_BORDER_, Button::ROUND,
							Button::NO_BORDER, NULL,
							SettingId::TREND_TIME_SCALE, 
							ScrollableMenu::GetTrendTimeScaleMenu(),
							TrendTimeScaleMenuItems::GetMenuItems(),
							TABLE_BUTTON_X_,
							TABLE_BUTTON_Y_ + TABLE_BUTTON_HEIGHT_),
	trendSelectButton1_(    TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_, 
							TABLE_BUTTON_Y_,
							SELECT_COLUMN_WIDTH_, TABLE_BUTTON_HEIGHT_, Button::LIGHT,
							BUTTON_BORDER_, Button::ROUND,
							Button::NO_BORDER, NULL,
							SettingId::TREND_SELECT_1, 
							ScrollableMenu::GetTrendSelectMenu(),
							TrendSelectMenuItems::GetMenuItems(),
							TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_,
							TABLE_BUTTON_Y_ + TABLE_BUTTON_HEIGHT_),
	trendSelectButton2_(    TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + SELECT_COLUMN_WIDTH_, 
							TABLE_BUTTON_Y_,
							SELECT_COLUMN_WIDTH_, TABLE_BUTTON_HEIGHT_, Button::LIGHT,
							BUTTON_BORDER_, Button::ROUND,
							Button::NO_BORDER, NULL,
							SettingId::TREND_SELECT_2, 
							ScrollableMenu::GetTrendSelectMenu(),
							TrendSelectMenuItems::GetMenuItems(),
							TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + SELECT_COLUMN_WIDTH_, 
							TABLE_BUTTON_Y_ + TABLE_BUTTON_HEIGHT_),
	trendSelectButton3_(    TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + (2 *SELECT_COLUMN_WIDTH_), 
							TABLE_BUTTON_Y_,
							SELECT_COLUMN_WIDTH_, TABLE_BUTTON_HEIGHT_, Button::LIGHT,
							BUTTON_BORDER_, Button::ROUND,
							Button::NO_BORDER, NULL,
							SettingId::TREND_SELECT_3, 
							ScrollableMenu::GetTrendSelectMenu(),
							TrendSelectMenuItems::GetMenuItems(),
							TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + (2 *SELECT_COLUMN_WIDTH_), 
							TABLE_BUTTON_Y_ + TABLE_BUTTON_HEIGHT_),
	trendSelectButton4_(    TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + (3 *SELECT_COLUMN_WIDTH_), 
							TABLE_BUTTON_Y_,
							SELECT_COLUMN_WIDTH_, TABLE_BUTTON_HEIGHT_, Button::LIGHT,
							BUTTON_BORDER_, Button::ROUND,
							Button::NO_BORDER, NULL,
							SettingId::TREND_SELECT_4, 
							ScrollableMenu::GetTrendSelectMenu(),
							TrendSelectMenuItems::GetMenuItems(),
							TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + (3 *SELECT_COLUMN_WIDTH_), 
							TABLE_BUTTON_Y_ + TABLE_BUTTON_HEIGHT_),
	trendSelectButton5_(    TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + (4 *SELECT_COLUMN_WIDTH_), 
							TABLE_BUTTON_Y_,
							SELECT_COLUMN_WIDTH_, TABLE_BUTTON_HEIGHT_, Button::LIGHT,
							BUTTON_BORDER_, Button::ROUND,
							Button::NO_BORDER, NULL,
							SettingId::TREND_SELECT_5, 
							ScrollableMenu::GetTrendSelectMenu(),
							TrendSelectMenuItems::GetMenuItems(),
							TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + (4 *SELECT_COLUMN_WIDTH_), 
							TABLE_BUTTON_Y_ + TABLE_BUTTON_HEIGHT_),
	trendSelectButton6_(    TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + (5 *SELECT_COLUMN_WIDTH_), 
							TABLE_BUTTON_Y_,
							SELECT_COLUMN_WIDTH_, TABLE_BUTTON_HEIGHT_, Button::LIGHT,
							BUTTON_BORDER_, Button::ROUND,
							Button::NO_BORDER, NULL,
							SettingId::TREND_SELECT_6, 
							ScrollableMenu::GetTrendSelectMenu(),
							TrendSelectMenuItems::GetMenuItems(),
							TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + (5 *SELECT_COLUMN_WIDTH_), 
							TABLE_BUTTON_Y_ + TABLE_BUTTON_HEIGHT_),
	eventDetailButton_(     TABLE_BUTTON_X_ + TIME_COLUMN_WIDTH_ + (6 *SELECT_COLUMN_WIDTH_), 
							TABLE_BUTTON_Y_,
							EVENT_DETAIL_COLUMN_WIDTH_, TABLE_BUTTON_HEIGHT_, 
							Button::LIGHT, BUTTON_BORDER_, 
							Button::ROUND, Button::NO_BORDER, 
							MiscStrs::TREND_EVENT_DETAIL_LARGE),
	trendCursorSlider_(     this),
	trendingLog_(           *(this), NUM_TABLE_ROWS_, NUM_TABLE_COLUMNS_, TABLE_ROW_HEIGHT_),
	isDataRequestPending_(  FALSE),
	isCallbackActive_(      FALSE)
{
	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::BLACK);

	printingText_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
	printingText_.setShow(FALSE);
	addDrawable(&printingText_);

	TrendDataSetAgent::RegisterTarget(this);  

	// point the presets menu to the preset strings set up in the base class
	presetsButton_.setValueInfo(TrendSubScreen::PresetsTypeInfo_);

	addDrawable(&graphScreenNavButton_);
	addDrawable(&printButton_);

	// Set Callbacks
	printButton_.setButtonCallback(this);
	graphScreenNavButton_.setButtonCallback(this);

	trendCursorSlider_.setX(printButton_.getX() + printButton_.getWidth() + 4);
	trendCursorSlider_.setY(TREND_BUTTON_Y_);
	trendCursorSlider_.setShowCursorLine(FALSE);
	trendCursorSlider_.setShowAxis(TRUE);
	addDrawable(&trendCursorSlider_);

	// setup the trending log 
	trendingLog_.setX(TABLE_X_);
	trendingLog_.setY(TABLE_Y_);
	// set date/time column
	trendingLog_.setColumnInfo(TIME_COLUMN_, NULL, TIME_COLUMN_WIDTH_, CENTER,
							   0, TextFont::NORMAL, TextFont::TEN, TextFont::EIGHT);
	// set trend select columns
	for (Int16 i = 0; i < NUM_SELECT_COLUMNS_; i++)
	{
		trendingLog_.setColumnInfo(FIRST_SELECT_COLUMN_+i, NULL, SELECT_COLUMN_WIDTH_, CENTER,
								   0, TextFont::NORMAL, TextFont::TEN, TextFont::EIGHT);
	}

	// set event detail column
	trendingLog_.setColumnInfo(EVENT_DETAIL_COLUMN_, NULL, EVENT_DETAIL_COLUMN_WIDTH_, CENTER,
							   0, TextFont::NORMAL, TextFont::TEN, TextFont::EIGHT);

	trendingLog_.enableFastScroll(GuiTimerId::TREND_TABLE_TIMER);

	addDrawable(&trendingLog_);

	// set up log 
	trendingLog_.setEntryInfo(0, TrendDataMgr::MAX_ROWS );

	// add select buttons on top of table
	Int8 idx = 0;
	arrSettingButtonPtrs_[idx++] = &trendSelectButton1_;
	arrSettingButtonPtrs_[idx++] = &trendSelectButton2_;
	arrSettingButtonPtrs_[idx++] = &trendSelectButton3_;
	arrSettingButtonPtrs_[idx++] = &trendSelectButton4_;
	arrSettingButtonPtrs_[idx++] = &trendSelectButton5_;
	arrSettingButtonPtrs_[idx++] = &trendSelectButton6_;
	arrSettingButtonPtrs_[idx++] = &timeScaleButton_;
	arrSettingButtonPtrs_[idx++] = &presetsButton_;
	arrSettingButtonPtrs_[idx] = NULL;

	AUX_CLASS_ASSERTION((idx <= NUM_TREND_TABLE_SETTING_BUTTONS_), idx);

	// Add the buttons to the subscreen
	for (idx = 0u; arrSettingButtonPtrs_[idx] != NULL; idx++)
	{
		arrSettingButtonPtrs_[idx]->setButtonCallback(this);
		arrSettingButtonPtrs_[idx]->usePersistentFocus();
		arrSettingButtonPtrs_[idx]->setSingleSettingAcceptEnabled(TRUE);
		addDrawable(arrSettingButtonPtrs_[idx]);
	}
	trendSelectButton1_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	trendSelectButton2_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	trendSelectButton3_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	trendSelectButton4_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	trendSelectButton5_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	trendSelectButton6_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	timeScaleButton_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	presetsButton_.setValueTextPosition(::GRAVITY_BOTTOM);
	presetsButton_.setDropDownTextSize(8);

	eventDetailButton_.setButtonCallback(this);
	addDrawable(&eventDetailButton_);

	// set persistent 
	eventDetailButton_.usePersistentFocus();

	// the first table column is the date/time so this is just a placeholder
	Int32 tableColumn = 0;
	datasetColumn_[tableColumn++] = -1;
	// The first three columns of the dataset are the same for the graphics 
	// and table sub-screens. The last three are unique to the table subscreen.
	datasetColumn_[tableColumn++] = 0;
	datasetColumn_[tableColumn++] = 1;
	datasetColumn_[tableColumn++] = 2;
	datasetColumn_[tableColumn++] = 3;
	datasetColumn_[tableColumn++] = 4;
	datasetColumn_[tableColumn++] = 5;
	datasetColumn_[tableColumn++] = -1; // Last column is a place holder for events.
	CLASS_ASSERTION(tableColumn == NUM_TREND_TABLE_COLUMNS_);

	// pre-position event detail window
	eventDetailLog_.setX(TREND_TABLE_EVENT_SCROLL_X_);
	eventDetailLog_.setY( TREND_TABLE_EVENT_SCROLL_Y_);

	TrendDataSetAgent::RegisterTarget(this);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendTableSubScreen  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendTableSubScreen::~TrendTableSubScreen(void)
{
	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate() [virtual]
//
//@ Interface-Description
//	This SubScreen virtual method prepares the subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class activate method. Activates all buttons and the
//  cursor slider. Requests updated trend data from the Trend-Database.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendTableSubScreen::activate(void)
{
	TrendSubScreen::activate();

	trendCursorSlider_.activate();
	trendingLog_.activate();

	// Activate all buttons
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::activate);

	// reset the column erased states
	for (Uint32 col=0; col<NUM_TREND_TABLE_COLUMNS_; col++)
	{
		isTableColumnErased_[col] = FALSE;
	}
	areAllTableColumnsErased_ = FALSE;

	// refresh screen if data exists
	requestTrendData();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate() [virtual]
//
//@ Interface-Description
//  This SubScreen virtual method prepares the subscreen for removal
//  from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Deactivates the trending log, the cursor slider and all settings
//  buttons. Calls the base class deactivate.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendTableSubScreen::deactivate(void)
{
	trendingLog_.deactivate(); 

	trendCursorSlider_.deactivate();

	// deactivate all setting buttons...
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::deactivate);

	TrendSubScreen::deactivate();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
//  Called when any registered setting has changed.
//
//  qualifierId		The context in which the change happened, either
//                  ACCEPTED or ADJUSTED.
// 
//  pSubject		The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Called when any of the observed trend setting parameters have
//  changed. Marks for erasure the table column of the changed trend
//  parameter. If the time scale has changed, flags for erasure
//  of all the table columns. We flag columns for erasure instead of
//  erasing them here because the DirectDraw pop-up menu is overwritten
//  by erasing the column. The actual erase operation is done right
//  before requesting new trend data in requestTrendData_. We
//  erase the column data before requesting new data from the database
//  so the setting displayed isn't mismatched with old columnar data
//  from the previous setting.
// 
//  Forwards this event onto the TrendSubScreen base class for handling
//  setting changes common to all TrendSubScreens.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendTableSubScreen::valueUpdate(const Notification::ChangeQualifier qualifierId,
									  const SettingSubject* pSubject)
{
	if (!isVisible())
	{
		return;
	}

	const SettingId::SettingIdType  SETTING_ID = pSubject->getId();

	if (qualifierId == Notification::ACCEPTED)
	{

		if (SETTING_ID == SettingId::TREND_SELECT_1 && !isTableColumnErased_[1])
		{
			//trendingLog_.setShowColumn(1, FALSE);
			isTableColumnErased_[1] = TRUE;
		}
		else if (SETTING_ID == SettingId::TREND_SELECT_2 && !isTableColumnErased_[2])
		{
			//trendingLog_.setShowColumn(2, FALSE);
			isTableColumnErased_[2] = TRUE;
		}
		else if (SETTING_ID == SettingId::TREND_SELECT_3 && !isTableColumnErased_[3])
		{
			//trendingLog_.setShowColumn(3, FALSE);
			isTableColumnErased_[3] = TRUE;
		}
		else if (SETTING_ID == SettingId::TREND_SELECT_4 && !isTableColumnErased_[4])
		{
			//trendingLog_.setShowColumn(4, FALSE);
			isTableColumnErased_[4] = TRUE;
		}
		else if (SETTING_ID == SettingId::TREND_SELECT_5 && !isTableColumnErased_[5])
		{
			//trendingLog_.setShowColumn(5, FALSE);
			isTableColumnErased_[5] = TRUE;
		}
		else if (SETTING_ID == SettingId::TREND_SELECT_6 && !isTableColumnErased_[6])
		{
			//trendingLog_.setShowColumn(6, FALSE);
			isTableColumnErased_[6] = TRUE;
		}
		else if (SETTING_ID == SettingId::TREND_TIME_SCALE && !areAllTableColumnsErased_)
		{
			for (Uint32 col=0; col<NUM_TREND_TABLE_COLUMNS_; col++)
			{
				if (!isTableColumnErased_[col])
				{
					//trendingLog_.setShowColumn(col, FALSE);
					isTableColumnErased_[col] = TRUE;
				}
			}
			eventDetailButton_.setToFlat();
			areAllTableColumnsErased_ = TRUE;
		}

	}

	TrendSubScreen::valueUpdate(qualifierId,pSubject);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
//  This ButtonTarget virtual method is called when any button registed
//  with this sub-screen is pressed up by the operator
//  (byOperatorAction is TRUE) or when Button::setToUp() is called.
//---------------------------------------------------------------------
//@ Implementation-Description
// 
//  When any of the trend parameter selection buttons are pressed up
//  then this method reenables table refresh (by setting
//  isCallbackActive_ FALSE).
// 
//  If the event detail button is pressed up then deactive the window
//  and remove it from this sub-screen.
// 
//  Upon receiving the button up event, if a trend parameter setting has
//  changed, then attempt to make a request for new trend data. If
//  there's been no change in the trend parameters then refresh the
//  screen with the current data. The pop-up menus have exposed the
//  underlying plots that must now be refreshed.
// 
//  Passes the button up event on to the TrendSubScreen base class for
//  processing common to all TrendSubScreens.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendTableSubScreen::buttonDownHappened(Button *pButton, Boolean byOperatorAction)
{
#ifdef TRENDTABLESUBSCREEN_DEBUG
	cout << "TrendTableSubScreen::buttonDownHappened\n";
#endif


	// Was the Graph Subscreen button pressed?
	if (pButton == &graphScreenNavButton_)
	{
	    // There is no reason to set this button to up
        // when we go into another subscreen.
		//pButton->setToUp();
		displayGraphs_();
	}
	else if ((pButton == &timeScaleButton_) ||
			 (pButton == &trendSelectButton1_) ||
			 (pButton == &trendSelectButton2_) ||
			 (pButton == &trendSelectButton3_) ||
			 (pButton == &trendSelectButton4_) ||
			 (pButton == &trendSelectButton5_) ||
			 (pButton == &trendSelectButton6_) ||
			 (pButton == &presetsButton_)  || 
			 (pButton == &eventDetailButton_))
	{
		isCallbackActive_ = TRUE;

		if (pButton == &eventDetailButton_)
		{
			isEventButtonDown_ = TRUE;
			updateEventDetailWindow_();

			if (eventDetailLog_.isScrollbarVisible())
			{
				eventDetailLog_.setX(TREND_TABLE_EVENT_SCROLL_X_);
			}
			else
			{
				eventDetailLog_.setX(TREND_TABLE_EVENT_SCROLL_X_ + TREND_EVENT_SCROLLBAR_WIDTH_);
			}

			addDrawable(&eventDetailLog_);
			eventDetailLog_.activate();
		}

	}

	// let the base class process the event as well
	TrendSubScreen::buttonDownHappened(pButton, byOperatorAction);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
//  This ButtonTarget virtual method is called when any button registed
//  with this sub-screen is pressed up by the operator
//  (byOperatorAction is TRUE) or when Button::setToUp() is called.
//---------------------------------------------------------------------
//@ Implementation-Description
// 
//  When any of the trend parameter selection buttons are pressed up
//  this method reenables sub-screen refresh (by setting
//  isCallbackActive_ FALSE).
// 
//  If the event detail button is pressed up then deactive the window
//  and remove it from this sub-screen.
// 
//  Upon receiving the button up event, if a trend parameter setting has
//  changed, then attempt to make a request for new trend data. If
//  there's been no change in the trend parameters then refresh the
//  screen with the current data. The pop-up menus have exposed the
//  underlying plots that must now be refreshed.
// 
//  Passes the button up event on to the TrendSubScreen base class for
//  processing common to all TrendSubScreens.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void TrendTableSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
	if ((pButton == &timeScaleButton_) ||
		(pButton == &trendSelectButton1_) ||
		(pButton == &trendSelectButton2_) ||
		(pButton == &trendSelectButton3_) ||
		(pButton == &trendSelectButton4_) ||
		(pButton == &trendSelectButton5_) ||
		(pButton == &trendSelectButton6_) ||
		(pButton == &eventDetailButton_) ||
		(pButton == &presetsButton_))
	{
		isCallbackActive_ = FALSE;

		if ((pButton == &eventDetailButton_) && isEventButtonDown_)
		{
			isEventButtonDown_ = FALSE;
			eventDetailLog_.deactivate();
			removeDrawable( &eventDetailLog_ );
		}

		if (isSettingChanged_)
		{
			// request new trend data
			if (requestTrendData() == TrendSubScreen::REQUEST_ACCEPTED)
			{
				isSettingChanged_ = FALSE;
			}
		}
		else
		{
			// just refresh the screen without updating the dataset
			update();
		}

	}

	// let the base class process the event as well
	TrendSubScreen::buttonUpHappened(pButton, byOperatorAction);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: trendDataReady
//
//@ Interface-Description
// This method should be redefined in derived classes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendTableSubScreen::trendDataReady(TrendDataSet& rTrendDataSet) 
{

	graphScreenNavButton_.setToUp();
	timeScaleButton_.setToUp();
	trendSelectButton1_.setToUp();
	trendSelectButton2_.setToUp();
	trendSelectButton3_.setToUp();
	trendSelectButton4_.setToUp();
	trendSelectButton5_.setToUp();
	trendSelectButton6_.setToUp();
	presetsButton_.setToUp();
	eventDetailButton_.setToUp();
	trendCursorSlider_.enableCursor();
	trendingLog_.setUserScrollable(TRUE);


	isDataRequestPending_ = FALSE;

	// update corrects the state of the eventDetailButton_
	update();

	// show the data now that it's updated
	for (Uint32 col=0; col<NUM_TREND_TABLE_COLUMNS_; col++)
	{
		if (isTableColumnErased_[col])
		{
			trendingLog_.setShowColumn(col, TRUE);
			isTableColumnErased_[col] = FALSE;
		}
	}

	areAllTableColumnsErased_ = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
//  Called to refresh the contents of the trend table with the contents
//  of the current TrendDataSet.
// 
//---------------------------------------------------------------------
//@ Implementation-Description 
// 
//  In general, this method is called to refresh the table any time a
//  button on the sub-screen is popped up. This manual refresh is
//  required because the pop-up menus and the event detail window
//  activated by this sub-screen's buttons are drawn directly into the
//  frame buffer. The background repaint mechanism contained in
//  GUI-Foundations does not work with objects drawn directly in the
//  frame buffer because these objects are never included in a list of
//  changed Drawables in the BaseContainer so the underlying objects
//  never receive a call to be refreshed.
// 
//  Several conditions inhibit refreshing the table. These conditions
//  include: when an active pop-up menu or event log overlays the
//  table, when the user is scrolling through the table, and while
//  receiving new trend data.
// 
//  When refresh is inhibited, this method starts the display retry
//  timer so the refresh can be tried again later.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


void TrendTableSubScreen::update(void)
{
	if (!isVisible())
	{
		return;
	}

	if (isCallbackActive_)
	{
		// to prevent refresh when a menu overlays the graphs
		GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DISPLAY_RETRY_TIMER);
	}
	if (trendCursorSlider_.isCursorActive() || trendingLog_.isScrolling())
	{
		// To prevent refresh while scrolling the table with the cursor or
		// scrollbar. This prevents multiple refresh requests caused by the
		// event detail button popping up/down while scrolling.
		GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DISPLAY_RETRY_TIMER);
	}
	else if (isDataRequestPending_)
	{
		// to prevent multiple refresh when buttons are set up during trendDataReady
		GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DISPLAY_RETRY_TIMER);
	}
	else
	{
		// cancel the retry timer in case we're able to refresh before the timer expires
		GuiTimerRegistrar::CancelTimer(GuiTimerId::TREND_DISPLAY_RETRY_TIMER);

		// reset and refresh the entire log
		trendingLog_.setEntryInfo(rTrendDataSetAgent_.getCursorPosition(),  TrendDataMgr::MAX_ROWS );
		updateCursorPosition();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
//  Called by TrendingLog when it needs to retrieve the text content of
//  a particular cell in the trend table.
// 
//	Passed the following parameters:
// 
// >Von
//  entryNumber         The index (dataset row) of the log entry
// 
//  columnNumber		The column number of the log entry
// 
//	rIsCheapText		Output parameter. A reference to a flag which
//                      determines whether the text content of the log
//                      entry is specified as cheap text or not.
// 
//	rString1            String id of the first string. Can be
//                      set to NULL_STRING_ID to indicate an empty
//                      entry column.
// 
//	rString2            String id of the second string.
// 
//  rString3            String id of the third string.  Not used if
//                      rIsCheapText is FALSE.
// 
//	rString1Message     A help message that will be displayed in the
//                      Message Area when rString1 is touched.  This
//                      only works when rIsCheapText is TRUE.
// 
// >Voff
//	We retrieve information for the appropriate column, make sure it's
//	formatted correctly and return it to TrendingLog.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Depending on which column is requested, this method retrieves data
//	for the selected entry (row) and column from the current
//	TrendDataSet. It uses the TrendDataSetAgent to access the current
//	TrendDataSet.
// 
//	This method uses TrendFormatData::FormatData to format the numeric
//	trend data to its specifications.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void TrendTableSubScreen::getLogEntryColumn(Uint16 entry, 
											Uint16 tableColumn,
											Boolean &rIsCheapText, 
											StringId &rString1, 
											StringId &rString2,
											StringId &rString3, 
											StringId &rString1Message)
{
	static wchar_t tmpBuffer1[40];
	static wchar_t tmpBuffer2[40];
	static wchar_t tmpBuffer3[40];
	Uint16 row = entry;

	// Default the two string output parameters to point to the static
	// character buffers.
	rString1 = &tmpBuffer1[0];
	rString2 = &tmpBuffer2[0];
	rString3 = &tmpBuffer3[0];
	rString1Message = NULL_STRING_ID;
	tmpBuffer1[0] = L'\0';
	tmpBuffer2[0] = L'\0';
	tmpBuffer3[0] = L'\0';

	const TrendDataSet& rTrendDataSet = rTrendDataSetAgent_.getTrendDataSet();

	if (isTableColumnErased_[tableColumn])
	{
		swprintf( tmpBuffer1, L" ");
		rString2 = NULL_STRING_ID;
		rString3 = NULL_STRING_ID;
		rIsCheapText = FALSE;
	}
	else if (tableColumn == TIME_COLUMN_)
	{
		// Column 0, the Time column.
		const TimeStamp & rTimeStamp = rTrendDataSet.getTrendTimeStamp(row);

		if (rTimeStamp.isInvalid())
		{
			swprintf( tmpBuffer1, L"------" );
			rString2 = NULL_STRING_ID;
			rString3 = NULL_STRING_ID;
			rIsCheapText = FALSE;
		}
		else
		{
			// First line of text is the time, second line is the date
			rIsCheapText = FALSE;
			TextUtil::FormatTime(tmpBuffer1, L"%t", rTimeStamp);
			TextUtil::FormatTime(tmpBuffer2, L"%P", rTimeStamp);
			rString3 = NULL_STRING_ID;
			rIsCheapText = FALSE;
		}
	}
	else if (tableColumn == EVENT_DETAIL_COLUMN_)
	{
		const TrendEvents & rEvents = rTrendDataSet.getTrendEvents( row );

		Int32 numEvents = 0;

		// extract the event ids 
		// this array should be sized to the maximum events to display in the popup
		// this should also be moved to the TrendSubScreen base class so we can use
		// it to display the event pop-up window.
		TrendEvents::EventId eventId[TrendEvents::TOTAL_EVENT_IDS]; 

		Int32 idx = 0;

		// extract the user events
		for (idx = TrendEvents::BEGIN_USER_EVENT_ID; 
			(idx <= TrendEvents::END_USER_EVENT_ID) && (numEvents < countof(eventId)); 
			idx++)
		{
			if (rEvents.isEventSet(TrendEvents::EventId(idx)))
			{
				eventId[numEvents++] = TrendEvents::EventId(idx);
			}
		}
		// extract the auto-events
		for (idx = TrendEvents::BEGIN_AUTO_EVENT_ID; 
			(idx <= TrendEvents::END_AUTO_EVENT_ID) && (numEvents < countof(eventId)); 
			idx++)
		{
			if (rEvents.isEventSet(TrendEvents::EventId(idx)))
			{
				eventId[numEvents++] = TrendEvents::EventId(idx);
			}
		}

		if (numEvents == 0)
		{
			swprintf(tmpBuffer1, L" ");
		}
		else if (numEvents == 1)
		{
			swprintf(tmpBuffer1, L" %d", 
					TrendFormatData::FormatEvent(eventId[0]));
		}
		else if (numEvents == 2)
		{
			swprintf(tmpBuffer1, L" %d %d",
					TrendFormatData::FormatEvent(eventId[0]),
					TrendFormatData::FormatEvent(eventId[1]));
		}
		else if (numEvents == 3)
		{
			swprintf(tmpBuffer1, L" %d %d %d", 
					TrendFormatData::FormatEvent(eventId[0]),
					TrendFormatData::FormatEvent(eventId[1]),
					TrendFormatData::FormatEvent(eventId[2]));
		}
		else if (numEvents > 3)
		{
			swprintf(tmpBuffer1, L" %d %d %d +", 
					TrendFormatData::FormatEvent(eventId[0]),
					TrendFormatData::FormatEvent(eventId[1]),
					TrendFormatData::FormatEvent(eventId[2]));
		}

		rString2 = NULL_STRING_ID;
		rString3 = NULL_STRING_ID;
		rIsCheapText = FALSE;
	}
	else
	{
		// data columns
		const TrendDataValues& rTdv = rTrendDataSet.getTrendDataValues(datasetColumn_[tableColumn]);
		const TrendValue & rTrendValue = rTdv.getValue( row );
		TrendSelectValue::TrendSelectValueId colId = rTdv.getId();

		if (colId == TrendSelectValue::TREND_DATA_NOT_AVAILABLE)
		{
			// option not enabled therefore data not available
			swprintf( tmpBuffer1, L"N/A");
			rIsCheapText = FALSE;
		}
		else if (colId == TrendSelectValue::TREND_EMPTY ||
				 !rTrendValue.isValid)
		{
			swprintf( tmpBuffer1, L"------" );
			rIsCheapText = FALSE;
		}
		else
		{
			TrendFormatData::FormatData(tmpBuffer1, colId, rTrendValue.data.realValue);
			rIsCheapText = FALSE;
		}
		rString2 = NULL_STRING_ID;
		rString3 = NULL_STRING_ID;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: requestTrendData_(void) [protected]
//
//@ Interface-Description
//  Constructs the trendDataSet parameters and send them to Trend Data
//  Manager to handle the request.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Disables the user interface by making all sub-screen buttons flat
//  before signaling the TrendDataSetAgent to update its dataset. Erases
//  the column of data that's changing. Disables the user interface 
//  so the user isn't looking at data as it's changing. Database
//  retrieval time is perceptable but acceptably short (~1 second). This
//  could be enhanced by double buffering the requests and dataset
//  responses.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void TrendTableSubScreen::requestTrendData_()
{
	// make menus/cursor/data unavailable until database request completes
	graphScreenNavButton_.setToFlat();
	timeScaleButton_.setToFlat();
	trendSelectButton1_.setToFlat();
	trendSelectButton2_.setToFlat();
	trendSelectButton3_.setToFlat();
	trendSelectButton4_.setToFlat();
	trendSelectButton5_.setToFlat();
	trendSelectButton6_.setToFlat();
	presetsButton_.setToFlat();
	eventDetailButton_.setToFlat();
	trendCursorSlider_.disableCursor();
	trendingLog_.setUserScrollable(FALSE);

	// Clearing the column data should be done when the setting changes
	// but the direct draw graphic is over the columns when the setting
	// does change. This is the best place to clear the column until we
	// can integrate direct draw graphics into the graphics library and 
	// exposure event processing.
	for (Uint32 col=0; col<NUM_TREND_TABLE_COLUMNS_; col++)
	{
		if (isTableColumnErased_[col])
		{
			trendingLog_.setShowColumn(col,FALSE);
		}
	}

	rTrendDataSetAgent_.requestTrendData(); 

	isDataRequestPending_ = TRUE;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isAnyButtonPressed_
//
//@ Interface-Description 
//  This TrendSubScreen pure virtual method is called by the
//  TrendSubScreen before starting a trend database request to assure
//  that the user interface is inactive.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method checks the button state from the Button class instead of
//  using some sort of semaphore in the buttonDownHappened callback
//  since the callback occurs after the button actually goes down
//  thereby opening a window between the time the first button goes up
//  and the second goes down when a data request could be started with
//  the user interface still active. Using the button state eliminates
//  this window.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean TrendTableSubScreen::isAnyButtonPressed_(void) const
{
	return((timeScaleButton_.getButtonState() == Button::DOWN) ||
		   (trendSelectButton1_.getButtonState() == Button::DOWN) ||
		   (trendSelectButton2_.getButtonState() == Button::DOWN) ||
		   (trendSelectButton3_.getButtonState() == Button::DOWN) ||
		   (trendSelectButton4_.getButtonState() == Button::DOWN) ||
		   (trendSelectButton5_.getButtonState() == Button::DOWN) ||
		   (trendSelectButton6_.getButtonState() == Button::DOWN) ||
		   (presetsButton_.getButtonState() == Button::DOWN) ||
		   (eventDetailButton_.getButtonState() == Button::DOWN) ||
		   (trendCursorSlider_.isCursorActive())
		  );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateCursorPosition
//
//@ Interface-Description 
//  This method is called to update the table drawables that change with
//  the position of the trending cursor. This method updates the table
//  by selecting the row in the table corresponding to the cursor's
//  location in the dataset. It also updates the event drawables setting
//  the event detail button up if there are any trend events stored at
//  that location.
// 
//  The TrendSubScreen calls this method when it receives a setting
//  change callback for the trend cursor position.
// 
//  The update method calls this method when refreshing the table.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  The TrendDataSetAgent provides the current cursor position on the
//  screen relative to newest data at the top of the table. This method
//  passes this cursor position along to the TrendingLog as the selected
//  row. With this information, the TrendingLog scrolls to the selected
//  row and highlights it.
// 
//  Note: Although the TrendingLog handles the table formatting and
//  scrolling operations, it uses this class to retrieve the data values
//  for each table location. It does this through this class's
//  getLogEntryColumn method.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  Since this method retrieves the cursor location from the
//  TrendDataSet, the TrendDataSetAgent::valueUpdate setting callback
//  must be called before this method to update those variables when the
//  trend cursor setting changes. The order of instantiation guarantees
//  this sequencing since the TrendSubScreen instantiates the
//  TrendDataSet as part of its construction before it registers for the
//  settings callback.
// 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//===================================================================== 

void TrendTableSubScreen::updateCursorPosition(void)
{
	// if a data request is pending, wait for the callback since this method
	// only scrolls to a position in the current table instead of refreshing
	// all of the data.  Wait for the trendDataReady callback before 
	// repositioning the cursor.
	if (!isVisible() || isDataRequestPending_)
	{
		return;
	}


	Int32 row = rTrendDataSetAgent_.getCursorPosition();

	// the TrendDataSetAgent::valueUpdate must be called before this
	// subscreen valueUpdate to update the cursor position first
	trendingLog_.setSelectedEntry(row);

	const TrendDataSet& rTrendDataSet = rTrendDataSetAgent_.getTrendDataSet();

	if (rTrendDataSet.getTrendEvents(row).isAnyEventSet())
	{
		if (eventDetailButton_.getButtonState() == Button::FLAT)
		{
			eventDetailButton_.setToUp();
		}
	}
	else
	{
		eventDetailButton_.setToFlat();                       
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableScrolling
//
//@ Interface-Description
//  This method is called to disable scrolling through the trend data
//  using the trend cursor or the table scrollbar. The TrendSubScreen
//  base class calls this method when any one trend parameter changes.
//  This prevents the user from looking through data that may not match
//  the current set of trend parameters shown on the display. It also
//  forces the user to accept any trend parameter changes before
//  pressing the scrolling cursor button that will grab focus and
//  postpone the dataset update required to get the data in sync with
//  the displayed settings.
//---------------------------------------------------------------------
//@ Implementation-Description 
//	Disables the trend cursor slider and the scrollbar.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendTableSubScreen::disableScrolling(void)
{
	trendCursorSlider_.disableCursor();
	trendingLog_.setUserScrollable(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================
void TrendTableSubScreen::SoftFault(const SoftFaultID  softFaultID,
									const Uint32       lineNumber,
									const char*        pFileName,
									const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDTABLESUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
