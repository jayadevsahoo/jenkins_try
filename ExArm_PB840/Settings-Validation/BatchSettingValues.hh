
#ifndef BatchSettingValues_HH
#define BatchSettingValues_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: BatchSettingValues - Manager of the Batch Setting Values.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BatchSettingValues.hhv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

//@ Usage-Classes
#include "SettingValue.hh"
#include "SettingValueMgr.hh"

#if defined(SIGMA_DEVELOPMENT)
class  Ostream;   // forward declaration...
#endif // defined(SIGMA_DEVELOPMENT)
//@ End-Usage


class BatchSettingValues : public SettingValueMgr
{
#if defined(SIGMA_DEVELOPMENT)
    friend Ostream&  operator<<(Ostream&                 ostr,
    				const BatchSettingValues& batchValues);
#endif // defined(SIGMA_DEVELOPMENT)

  public:
    BatchSettingValues(const BatchSettingValues& batchValues);
    BatchSettingValues(SettingValueMgr::ChangeCallbackPtr pInitCallback);
    BatchSettingValues(void);
    ~BatchSettingValues(void);

    SettingValue     getSettingValue(
				const SettingId::SettingIdType settingId
				    ) const;
    BoundedValue     getBoundedValue(
				const SettingId::SettingIdType boundedId
				    ) const;
    DiscreteValue    getDiscreteValue(
				const SettingId::SettingIdType discreteId
				     ) const;
    SequentialValue  getSequentialValue(
				const SettingId::SettingIdType sequentialId
				       ) const;
  
    void  setSettingValue   (const SettingId::SettingIdType settingId,
			     const SettingValue&            newValue);
    void  setBoundedValue   (const SettingId::SettingIdType boundedId,
			     const BoundedValue&            newValue);
    void  setDiscreteValue  (const SettingId::SettingIdType discreteId,
			     const DiscreteValue            newValue);
    void  setSequentialValue(const SettingId::SettingIdType sequentialId,
			     const SequentialValue          newValue);

    void  operator=(const BatchSettingValues& batchValues);
    void  copyFrom (const BatchSettingValues& batchValues);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    //@ Data-Member:  arrBatchValues_
    // Fixed array containing the values for each of the batch settings.
    SettingValue  arrBatchValues_[::NUM_BATCH_SETTING_IDS];
};


#endif // BatchSettingValues_HH 
