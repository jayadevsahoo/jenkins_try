#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CycleTimer - Responsible to invoke the periodic functions
//  of the Breath-Delivery subsystem.
//---------------------------------------------------------------------
//@ Interface-Description
//	The class provides methods that implement the functionality of the
//  main and the secondary breath delivery cycles.
//---------------------------------------------------------------------
//@ Rationale
//	The main and the secondary cycles are parts of periodic tasks that
//  restart every fixed amount of time.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This class invokes the newCycle methods of the objects that are
//  required to	be updated every BD cycle and every BD secondary cycle.
//  A call from the periodic task - BdExecutiveTask invokes the periodic
//  sequence of events that are characterized by this class.
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a
//---------------------------------------------------------------------
//@ Restrictions
// This is a static class and should never be instantiated
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/CycleTimer.ccv   25.0.4.1   20 Nov 2013 17:34:46   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 018  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software. The 5 ms clock check is still performed.
//
//  Revision: 017   By: gdc    Date:  07-Feb-2011   SCR Number: 6733
//  Project:  PROX
//  Description:
//	    Added VCO2 study code for development reference.
//
//  Revision: 016   By: erm    Date:  5-Oct-2010    DR Number: 6436
//  Project:  PROX
//  Description:
//	    Remove BdSignal Collect() must manually include into dev builds
//
//  Revision: 015   By: erm    Date:  5-May-2010    DR Number: 6436
//  Project:  PROX
//  Description:
//		Add ProxManager to SignalNewCycle
//
//
//  Revision: 014   By: gdc   Date:  12-Oct-2009    DR Number: 6147
//  Project:  XB0074
//  Description:
//		XB0074 project-related:
//		BDMonitoredData newCycle is called regardless of communication state.
//
//  Revision: 013   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added call to DynamicMechanics.newCycle() and modified the
//		interface to Maneuver::NewCycle().
//
//  Revision: 012   By: syw   Date:  19-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//			Call RPavManager.newCycle() ;
//
//  Revision: 011   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added BdSignal::Collect() if development options are allowed
//
//  Revision: 010  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//          Add call to EventFilter.newSecondaryCycle()
//
//  Revision: 009  By: yyy     Date:  5-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added LundData.newCycle() call every 5 ms cycle by SignalNewCycle().
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By:  yyy    Date:  01-Oct-1998    DCS Number:
//  Project:  BiLevel
//  Description:
//			BiLevel initial version.
//
//  Revision: 006  By:  hct    Date: 17-Jul-1997    DR Number: DCS 2178
//       Project:  Sigma (R8027)
//       Description:
//	   Added call to RBdMonitoredData.updateSequenceNumber() to increment
//         the cycle sequence number on every cycle whether communications
//         is up and the GUI is online or not.  BdMonitoredData compares the
//         BD generated sequence number against the GUI generated sequence
//         number.  If these numbers don't match, the BD Monitor tests which
//         rely on sequenced data are not run.
//
//  Revision: 005  By:  hct    Date: 17-Jul-1997    DR Number: DCS 2291
//       Project:  Sigma (R8027)
//       Description:
//	   Added test to make sure communications is up before calling
//         RBdMonitoredData.newCycle().
//
//  Revision: 004  By:  syw    Date: 23-Jun-1997    DR Number: DCS 2244
//       Project:  Sigma (R8027)
//       Description:
//			Added call to RVentStatus.newCycle().
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  sp    Date:  17-Apr-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "CycleTimer.hh"

#include "MainSensorRefs.hh"
#include "MiscSensorRefs.hh"
#include "TimersRefs.hh"
#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "BreathMiscRefs.hh"
#include "VentObjectRefs.hh"
#include "Fio2Monitor.hh"
#include "RegisterRefs.hh"
#include "SafetyNetRefs.hh"
#include "ControllersRefs.hh"

//@ Usage-Classes
#include "VentStatus.hh"
#include "BreathTriggerMediator.hh"
#include "ModeTriggerMediator.hh"
#include "TimerMediator.hh"
#include "BreathPhaseScheduler.hh"
#include "BreathPhase.hh"
#include "BreathSet.hh"
#include "ApneaInterval.hh"
#include "SensorMediator.hh"
#include "GasSupply.hh"
#include "PowerSource.hh"
#include "SystemBattery.hh"
#include "BdAlarms.hh"
#include "SafetyNetTestMediator.hh"
#include "ExhHeaterController.hh"
#include "BdMonitorRefs.hh"
#include "TimeMonitor.hh"
#include "NetworkApp.hh"
#include "TaskControlAgent.hh"
#include "PressureXducerAutozero.hh"
#include "BitAccessGpioMediator.hh"
#include "Maneuver.hh"
#include "LungData.hh"
#include "DynamicMechanics.hh"
#include "PavManager.hh"
#include "EventFilter.hh"
#include "BDIORefs.hh"
#include "Arts.hh"
#include "BdSignal.hh"
#include "SoftwareOptions.hh"
#include "ProxManager.hh"
#include "SwatHelper.hh"
#include "BDIOTest.h"

//@ End-Usage

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SignalNewCycle
//
//@ Interface-Description
//	This method takes no parameters and has no return value.
//	It initiates BD cycle based activities by invoking the newCycle
//  method in the appropriate objects.
//  The order in which these methods are invoked is important.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method collaborates with the objects that have to run every BD
//  cycle.
//---------------------------------------------------------------------
//@ PreCondition
//	None
//---------------------------------------------------------------------
//@ PostCondition
//	None
//@ End-Method
//=====================================================================
void
CycleTimer::SignalNewCycle(void)
{
    CALL_TRACE("SignalNewCycle()");

	BreathPhase *pBreathPhase;
    RTimerMediator.newCycle();

// Order of the following method calls are important
// ------------------------------------------
    RSensorMediator.newCycle();

    if(BdioTest.newCycle())
		return;
	
// Order of the following newCycle methods is important since power supply
// state may effect the compressor operation
// ------------------------------------------
	// Gas supply status is dependent on compressor status
// E600 BDIO	RCompressor.newCycle();
	RGasSupply.newCycle();
//-------------------------------------------

    // $[04117]
    RModeTriggerMediator.newCycle();
    RBreathTriggerMediator.newCycle();

	pBreathPhase = BreathPhase::GetCurrentBreathPhase();
    pBreathPhase->newCycle();
    RPressureXducerAutozero.newCycle() ;

    (RBreathSet.getCurrentBreathRecord())->newCycle();

	RLungData.newCycle() ;
	RPavManager.newCycle() ;
	RDynamicMechanics.newCycle();

    Maneuver::NewCycle();

    pBreathPhase->phaseInSettings();

	BreathPhaseScheduler::NewCycle();
	UiEvent::NewCycle();
	RBdAlarms.newCycle();

	RTimeMonitor.newCycle();

	Arts::UpdateOutputs() ;

    // The following method should run AFTER all requestes to change register
    // status have been submitted.
    RBitAccessGpio.newCycle() ;

    // must be called after RBitAccessGpio.newCycle()
	RVentStatus.newCycle() ;

	RProxManager.newCycle();

	if (SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions())
	{
		BdSignal::Collect() ;
#ifdef INTEGRATION_TEST_ENABLE
        swat::update();
        swat::g_clBatchJobManager.RunCycle();
#endif // INTEGRATION_TEST_ENABLE
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SignalNewSecondaryCycle
//
//@ Interface-Description
//
//	This method takes no parameters and has no return value.
//	It initiates the BD secondary cycle based activities by invoking the
//  newCycle method in the appropriate objects.
//  The order in which these methods are invoked is important.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	The method collaborates with the objects that have to run every secondary
//  BD cycle.
//  The order in which these methods are invoked is important.
//---------------------------------------------------------------------
//@ PreCondition
//	None
//---------------------------------------------------------------------
//@ PostCondition
//	None
//@ End-Method
//=====================================================================

void
CycleTimer::SignalNewSecondaryCycle( void )
{
    CALL_TRACE("SignalNewSecondaryCycle()");

    // $[00422]
	RSensorMediator.newSecondaryCycle();

	// Acquire data for the read registers
// TODO E600 BDIO	RPowerSupplyReadPort.newCycle();

// TODO E600 BDIO	RSafetyNetStatusPort.newCycle();

	REventFilter.newSecondaryCycle() ;

// Order of the following newCycle methods is important
// since power supply state may effect the compressor operation
// ------------------------------------------
	
    // System battery operation is dependent on power source operation 
    RPowerSource.newCycle();
    RSystemBattery.newCycle();

//-------------------------------------------

	RFio2Monitor.newCycle();

// TODO E600 	RExhHeaterController.newCycle() ;

// TODO E600 	RSafetyNetTestMediator.newCycle();

	RApneaInterval.newCycle();

	// $[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
CycleTimer::SoftFault(const SoftFaultID  softFaultID,
		      const Uint32       lineNumber,
		      const char*        pFileName,
		      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, CYCLETIMER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
