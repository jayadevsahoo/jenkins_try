#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LogTarget -  A target for using the ScrollableLog class.  Provides
// callbacks to ScrollableLog for retrieving log text retrieval and for
// communicating row selection/deselection events.
//---------------------------------------------------------------------
//@ Interface-Description
// Any class interested in using the ScrollableLog class must inherit
// from LogTarget.  LogTarget provides two callback routines,
// getLogEntryColumn() and currentLogEntryChangeHappened(), which are
// intrinsic to the operation of ScrollableLog.
//
// getLogEntryColumn() provides a method by which ScrollableLog can
// retrieve the textual contents of any entry in a log and
// currentLogEntryChangeHappened() is called by ScrollableLog whenever
// the user selects or deselects a row of the log.
//---------------------------------------------------------------------
//@ Rationale
// Needed to operate ScrollableLog correctly.
//---------------------------------------------------------------------
//@ Implementation-Description
// Both virtual callback methods are defined but do nothing.  It is
// up to the class which inherits from LogTarget to redefine these
// methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// This class should not be instantiated directly.  This class has use only
// when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LogTarget.ccv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "LogTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of a log entry.  Passed the following parameters:
// >Von
//	entryNumber			The index of the log entry
//	columnNumber		The column number of the log entry
//	rIsCheapText		Output parameter.  A reference to a flag which
//						determines whether the text content of the log
//						entry is specified as cheap text or not.
//	rString1			String id of the first string. Can be
//						set to NULL_STRING_ID to indicate an empty entry
//						column.
//	rString2			String id of the second string.
//	rString3			String id of the third string.  Not used if
//						rIsCheapText is FALSE.
//	rString1Message		A help message that will be displayed in the
//						Message Area when rString1 is touched.  This only
//						works when rIsCheapText is TRUE.
// >Voff
// If LogTarget desires to specify the contents of an entry column exactly
// then it sets rIsCheapText to TRUE and puts the cheap text into rString1.
// If it wants ScrollableLog to auto format the entry column then it can
// set rIsCheapText to FALSE and then set rString1 and/or rString2,
// depending on whether it wants a one or two-line auto formatted display.
// If rString1 and/or rString2 are not used then they should be set to
// NULL_STRING_ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LogTarget::getLogEntryColumn(Uint16, Uint16, Boolean &, StringId &, StringId &,
                              StringId &, StringId &)
{
	CALL_TRACE("LogTarget::getLogEntryColumn(Uint16, Uint16, Boolean &, "
                                             "StringId &, StringId &, StringId &, StringId &)");

	// Do nothing
}														// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: currentLogEntryChangeHappened
//
//@ Interface-Description
// When the users selects or deselects an entry of the log then ScrollableLog
// calls this method.  The first parameter specifies the entry and the
// second is a flag, 'isSelected', which will be TRUE if the entry was
// selected, FALSE if the entry was deselected.
//
// Note: when a user selects a new entry while another entry was previously
// selected there is only one call made to this method, for the new entry
// being selected.  There is no call made informing LogTarget that the
// old selection was automatically deselected.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LogTarget::currentLogEntryChangeHappened(Uint16, Boolean)
{
        CALL_TRACE("LogTarget::currentLogEntryChangeHappened(Uint16, Boolean)");

        // Do nothing	
}														// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LogTarget()  [Constructor, Protected]
//
//@ Interface-Description
// Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LogTarget::LogTarget(void)
{
        CALL_TRACE("LogTarget::LogTarget(void)");

        // Do nothing
}														// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LogTarget()  [Destructor, Protected]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LogTarget::~LogTarget(void)
{
        CALL_TRACE("LogTarget::~LogTarget(void)");

        // Do nothing
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LogTarget::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LOGTARGET,
				lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
