#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  PatientCctTypeSetting - Patient Circuit Type Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the current type of
//  circuit that is used to ventilate the patient.  This class inherits from
//  'DiscreteSetting' and provides the specific information needed for
//  the representation of patient circuit type.  This information includes
//  the set of values that this setting can have (see
//  'PatientCctTypeValue.hh'), and this setting's new-patient value.
//
//  This setting -- along with humidification type setting -- is unique
//  in that it is a batch setting that can be changed during Service Mode.
//  In fact, patient circuit type can ONLY be changed during Service Mode.
//  It also has its value stored in two different locations within NOVRAM:
//  one with the accepted values (see 'AcceptedContext'), the other place
//  is for storing the value of this setting the last time SST was run.
//  This class does not need to provide any additional behavior for either
//  of these unique aspects of this setting.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PatientCctTypeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		isEnabledValue() updated to support NeoMode Lockout option.
//
//  Revision: 007   By: sah   Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      * changed 'isEnabledValue()' to a public method, to provide
//        support for the new drop-down menus
//
//  Revision: 006   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Incorporated initial specifications for NeoMode Project.
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  simplified 'getNewPatientValue()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "PatientCctTypeSetting.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SoftwareOptions.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  PatientCctTypeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02213] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PatientCctTypeSetting::PatientCctTypeSetting(void)
	  : BatchDiscreteSetting(SettingId::PATIENT_CCT_TYPE,
				 Setting::NULL_DEPENDENT_ARRAY_,
				 PatientCctTypeValue::TOTAL_CIRCUIT_TYPES)
{
  CALL_TRACE("PatientCctTypeSetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~PatientCctTypeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PatientCctTypeSetting::~PatientCctTypeSetting(void)
{
  CALL_TRACE("~PatientCctTypeSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Patient circuit type is ALWAYS changeable.
//
//  $[01269] -- SST settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
PatientCctTypeSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  isEnabledValue()  [const, virtual]
//
//@ Interface-Description
//  Is this setting's value given by 'value', a currently-enabled value?
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02213] -- values of the range of Patient Circuit Type Setting...
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
Boolean
PatientCctTypeSetting::isEnabledValue(const DiscreteValue value) const
{
  Boolean  isEnabled;

  switch (value)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    // this optional value is only enabled when its option is active...
    // $[TI1.1] (TRUE)  $[TI1.2] (FALSE)...
    isEnabled = (SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE));
    break;

  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI2]
    // [02282] these standard values are enabled only when NEO_MODE_LOCKOUT is not active
    isEnabled = !( SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_LOCKOUT) );
    break;

  case PatientCctTypeValue::TOTAL_CIRCUIT_TYPES :
  default :
    // unexpected circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(value);
    break;
  }
 
  return(isEnabled);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
PatientCctTypeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  const AcceptedContext&  rAcceptedContext = ContextMgr::GetAcceptedContext();

  // use the setting's value set during SST Setup...
  const BoundedValue  NEW_PATIENT =
		rAcceptedContext.getSettingValue(SettingId::PATIENT_CCT_TYPE);

  return(NEW_PATIENT);
}  // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PatientCctTypeSetting::SoftFault(const SoftFaultID  softFaultID,
				 const Uint32       lineNumber,
				 const char*        pFileName,
				 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          PATIENT_CIRCUIT_TYPE_SETTING, lineNumber, pFileName,
                          pPredicate);
}
