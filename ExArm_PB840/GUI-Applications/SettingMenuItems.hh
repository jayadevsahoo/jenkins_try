#ifndef SettingMenuItems_HH
#define SettingMenuItems_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SettingMenuItems -  A LogTarget for setting menu items
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SettingMenuItems.hhv   25.0.4.0   19 Nov 2013 14:08:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc    Date: 15-Jan-2007   SCR Number: 6237
//  Project:  TREND
//  Description:
//      Initial version.
//
//====================================================================

//@ Usage-Classes
#include "GuiAppClassIds.hh"
#include "DiscreteSettingButton.hh"
#include "LogTarget.hh"
//@ End-Usage

class SettingMenuItems : public LogTarget
{
public:
	virtual DiscreteSettingButton::ValueInfo& getValueInfo(void) const;

	// LogTarget virtual
	virtual void getLogEntryColumn(Uint16 entryNumber, 
								   Uint16 columnNumber,
								   Boolean &rIsCheapText, 
								   StringId &rString1,
								   StringId &rString2, 
								   StringId &rString3,
								   StringId &rString1Message);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	SettingMenuItems(DiscreteSettingButton::ValueInfo * pMenuInfo);
	virtual ~SettingMenuItems(void);

private:
	// these methods are purposely declared, but not implemented...
	SettingMenuItems(const SettingMenuItems&);	 // not implemented...
	void   operator=(const SettingMenuItems&);	 // not implemented...

	///@Data-Member: pMenuInfo_
	// pointer to the ValueInfo struct containing the menu entry strings
	// used by DiscretSettingButton to retrieve the button's "value text"
	DiscreteSettingButton::ValueInfo* pMenuInfo_;
};

#endif // SettingMenuItems_HH
