#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== C L A S S   D E S C R I P T I O N ====
//@ Class: BackgroundCycleTimer - Maintains the scheduling and error
//                                reporting of a Background test 
//---------------------------------------------------------------------
//@ Interface-Description
//  This class' newCycle() method is designated as the main interface.
//  The basic frequency that newCycle() is called is assumed by the
//  caller.  This class will execute the Background test based on the
//  parameters defined in the constructor.  When an error status is
//  returned by the Background test, this error status is relayed to
//  Background::ReportBkEvent().
//---------------------------------------------------------------------
//@ Rationale
//  A generic template for maintaining the schedule and error reporting
//  of Background test.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When a BackgroundCycleTimer object is created, a function pointer
//  to a test is attached along with the scheduling parameters.
//  Each time a newCycle() runs, it increments a count that is compared
//  to the maximum count.  When the limit is reached the test attached
//  to the object is called.  If the test fails, a Background event
//  is generated.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A 
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundCycleTimer.ccv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004    By: iv   Date: 11-Mar-1998    DR Number: DCS 5041
//    Project: Sigma (840)
//    Description:
//       Added error code report for BK event in newCycle().
//
//  Revision: 003   By: syw    Date: 21-Nov-1997   DR Number: DCS 2637 
//    Project:  Sigma (R8027)
//    Description:
//		Added condition that state must be STATE_ONLINE to call
//		ReportBkEvent().
//
//  Revision: 002   By: iv    Date: 29-Apr-1997   DR Number: None 
//    Project:  Sigma (R8027)
//    Description:
//      Rework per unit test peer review.
//
//  Revision: 001   By: by   Date: 12-Dec-1995   DR Number: None
//    Project:  Sigma (840)
//    Description:
//      Initial version.
//=====================================================================

#include "BackgroundCycleTimer.hh"

//@ Usage-Classes

#include "TaskControlAgent.hh"

// Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BackgroundCycleTimer  [constructor]
//
//@ Interface-Description
//  Constructor.  This constructor accepts three arguments,
//  a TestNamePtr, an Int32 for test frequency, and an Int32 for
//  the start time (T0:time zero).
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the members currentTime_, testFrequency_, and pTestToRun_. 
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BackgroundCycleTimer::BackgroundCycleTimer( const TestNamePtr pTestname,
                                            Int32 testFrequency,
                                            Int32 initialDelay )
                     : pTestToRun_( pTestname ),
                       testFrequency_( testFrequency ),
                       currentTime_( initialDelay )
{
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~BackgroundCycleTimer  [destructor]
//
//@ Interface-Description
//  Standard Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Not implemented.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BackgroundCycleTimer::~BackgroundCycleTimer()
{

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  newCycle 
//
//@ Interface-Description
//  This method accepts no parameter and returns no value.
//
//  This method keeps tract of the schedule for a Background Test.
//  It executes the Background test when time has expired and
//  reports a Background event when one occurs.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method increments the currentTime_ and compares it to the
//  testFrequency_ limit.  If the time limit has been reached, the
//  associated test is run.  If the return parameter indicates an error
//  occured a background event is declared.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
BackgroundCycleTimer::newCycle( void )
{
    BkEventName testStatus;

    ++currentTime_;

    if ( currentTime_ >= testFrequency_ )
    {
    // $[TI1]

        Uint16 errorCode = 0U; 
        testStatus = pTestToRun_(errorCode);      // Its time to run a test.

        if ( testStatus != BK_NO_EVENT && TaskControlAgent::GetMyState() == STATE_ONLINE)
        {
        // $[TI1.1]
            // Report the failure
            Background::ReportBkEvent( testStatus, errorCode );
        }
        // $[TI1.2]

        // reset the private count
        currentTime_ = 0;
    }
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
BackgroundCycleTimer::SoftFault( const SoftFaultID  softFaultID,
                                 const Uint32       lineNumber,
                                 const char*        pFileName,
                                 const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SAFETY_NET, BACKGROUNDCYCLETIMER,
                          lineNumber, pFileName, pPredicate );
}
