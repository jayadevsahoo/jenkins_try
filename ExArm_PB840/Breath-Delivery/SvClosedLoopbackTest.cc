#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: SvClosedLoopbackTest - BD Safety Net Range Test for the
//               detection of closed safety valve loopback current OOR
//---------------------------------------------------------------------
//@ Interface-Description
//  The checkCriteria() method of this class is called from the
//  newCycle() method of the SafetyNetTestMediator class.  The
//  checkCriteria() method invokes the getCount() method in the
//  Sensor class to obtain the safety valve loopback current.
//  It invokes the getCurrentState() method in the Solenoid class
//  to determine when the safety valve is closed.
//---------------------------------------------------------------------
//@ Rationale
//  Used to determine if the safety valve is closed when it is commanded.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The safety net test is defined in the checkCriteria() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SvClosedLoopbackTest.ccv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By: iv    Date:  29-May-1998    DR Number: 5041
//  	Project:  Sigma (840)
//		Description:
//			Added unit test code.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  05-Sep-1995    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "SvClosedLoopbackTest.hh"
#include "BreathMiscRefs.hh"
#include "MiscSensorRefs.hh"
#include "ValveRefs.hh"
#include "SchedulerId.hh"

//@ Usage-Classes
#include "BreathPhaseScheduler.hh"
#include "LinearSensor.hh"
#include "Solenoid.hh"

#ifdef SIGMA_UNIT_TEST
    extern Int32 UnitTestNum;
#endif // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SvClosedLoopbackTest()
//
//@ Interface-Description
//  Default Constructor.  Invokes the base class constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The pSafetyNetSensorData_ attribute is set to point to the
//  safety net sensor data for the safety valve loopback current.
//---------------------------------------------------------------------
//@ PreCondition
//  The Sensor object for the SV loopback current has already been
//  constructed and the pointer to the safety net data is not NULL
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

SvClosedLoopbackTest::SvClosedLoopbackTest( void ): SafetyNetRangeTest ()
{
    // $[TI1]
    CALL_TRACE("SvClosedLoopbackTest(void)");

    // Set up the pointer to the SafetyNetSensorData object for the SV
    // loopback current

	//TODO E600_LL: to be reviewed; this file might be no longer applicable
	//pSafetyNetSensorData_ = RSafetyValveCurrent.getSafetyNetSensorDataAddr();

	//CLASS_PRE_CONDITION( pSafetyNetSensorData_ != NULL );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SvClosedLoopbackTest()
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

SvClosedLoopbackTest::~SvClosedLoopbackTest( void )
{
    CALL_TRACE("~SvClosedLoopbackTest(void)");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCriteria()
//
//@ Interface-Description
//  This method takes no arguments and returns a BkEventName.
//  This method is responsible for detecting a safety valve loopback
//  current OOR condition.
//  If the background check criteria have been met for the required
//  number of cycles, the backgndEventId for the class is returned;
//  otherwise BK_NO_EVENT is returned to indicate to the caller that
//  the Background subsystem should not be notified.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The current scheduler ID and breath phase are retrieved. If the
//  vent is using a scheduler that has the safety valve closed and
//  if the SV has had time to close (as determined by the breath phase),
//  the getCount() method is called to get the safety valve loopback
//  current reading.
//
//  This method invokes the getSchedulerId() and GetPhaseType() methods
//  of the current breath record to determine if the safety valve is
//  closed -- SV is only closed during specific schedulers after it
//  has been given time to physically close.
//
//  The method sensorReadingRangeCheck() is then called with
//  the sensor reading, which is compared to its min/max values.
//  The method getBackgndId() of the SafetyNetSensorData class is then
//  called to determine if the check failed or not.
//
//  $[06015] $[06155]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
SvClosedLoopbackTest::checkCriteria( void )
{
    CALL_TRACE("SvClosedLoopbackTest::checkCriteria (void)") ;

    BkEventName rtnValue = BK_NO_EVENT;

    // SV is closed in AC, Spont, SIMV, Apnea and Safety PCV once the
    // valve has been given time to close
    if ( Solenoid::CLOSED == RSafetyValve.getCurrentState() )
    {
    // $[TI1]

        // Get the Safety Valve Loopback current in counts
        AdcCounts safetyValveLoopbackCounts = 0; // E600 BDIORSafetyValveCurrent.getCount();

#ifdef SIGMA_UNIT_TEST
if ( UnitTestNum == 5)
{
    safetyValveLoopbackCounts = 333U;
}
#endif // SIGMA_UNIT_TEST

        pSafetyNetSensorData_->sensorReadingRangeCheck(safetyValveLoopbackCounts);

        // getBackgndEventId returns BK_NO_EVENT if there is no Background
        // event to report or if it was previously reported
        rtnValue = pSafetyNetSensorData_->getBackgndEventId();

    }
    else    // SV is not closed
    {
    // $[TI2]
        pSafetyNetSensorData_->resetNumCyclesOor();
    }

    return( rtnValue );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SvClosedLoopbackTest::SoftFault( const SoftFaultID softFaultID,
                                 const Uint32      lineNumber,
                                 const char*       pFileName,
                                 const char*       pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, SVCLOSEDLOOPBACKTEST,
                           lineNumber, pFileName, pPredicate );
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


