

#ifndef BreathDatumHandle_IN
#define BreathDatumHandle_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  BreathDatumHandle -  Provides a dedicated data access handle 
//          for each specific breath data item type.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/BreathDatumHandle.inv   25.0.4.0   19 Nov 2013 14:17:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  30-Jan-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  getDiscreteValue()
//
//@ Interface-Description
//    Returns the latest discrete data value of the breath item type
//    that this object handles.
//--------------------------------------------------------------------
//@ Implementation-Description
//      Get the actual data from the data processing agent for the breath
//    item type being handled by this handle.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline const     DiscreteBreathDatum&
BreathDatumHandle::getDiscreteValue()
{
  CALL_TRACE("BreathDatumHandle::getDiscreteValue()");
  SAFE_CLASS_PRE_CONDITION(pAgent_);

  return( pAgent_->getDiscreteDatum());                    // $[TI1]
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  getBoundedValue()
//
//@ Interface-Description
//    Returns the latest data value of a bounded breath data type that
//    this handle represents.
//--------------------------------------------------------------------
//@ Implementation-Description
//   The actual breath data is obtained from the data processing object.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline const BoundedBreathDatum &
BreathDatumHandle::getBoundedValue()
{
  CALL_TRACE("BreathDatumHandle::getBoundedValue()");
  SAFE_CLASS_PRE_CONDITION(pAgent_);

  return(pAgent_->getBreathDatum()); // $[TI1]
}
#endif // BreathDatumHandle_IN 
