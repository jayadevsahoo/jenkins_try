

#ifndef NV_MEMORY_STORAGE_THREAD_HH
#define NV_MEMORY_STORAGE_THREAD_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NV_MemoryStorageThread - Nonvolatile memory storage thread
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides execution context for the NV_MemoryStorage class
//  objects.
//
//  When the Initialize method is called during system initialization,
//  (prior to starting the various threads), NV_MemoryStorage class objects
//  are instantiated.
//  Once this thread is running, it provides execution context for those
//  objects by calling their CheckForRequiredStorageUpdate method.
//---------------------------------------------------------------------
//@ End-Preamble
//

// Forward Declararions
class NV_MemoryStorage;

class NV_MemoryStorageThread
{
    public:
        static void Initialize();                           // Called prior to starting any threads or
                                                            // initializing much of anything
        static void Shutdown();                             // Forces data to be written and cleans up allocated classes

        static void Thread();

		static void UpdateStorageObjects();

    protected:

		static const int MAX_NUMBER_OR_NV_STORAGE_AREAS = 10;        // If this isn't enough, just increase the value

        // This array holds pointers to the instances of the NV_MemoryStorage. This allows the
        // thread to iterate through them to check for the need to update to the file system.
        static NV_MemoryStorage * pNV_MemoryStorage_[MAX_NUMBER_OR_NV_STORAGE_AREAS];

        static bool initialized_;                            // Set true once initialization is complete.

    private:
        NV_MemoryStorageThread();                           // Not to be created
        ~NV_MemoryStorageThread();



};


#endif

