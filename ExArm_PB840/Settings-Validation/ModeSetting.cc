#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  ModeSetting - Mode Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the ventilation mode
//  (i.e., "assist-controlled", "spontaneous", or a hybrid of the other two).
//  This setting is also a Main Control Setting, and, therefore, activates
//  the Transition Rules of the settings that are dependent on this setting's
//  transitions.  This class inherits from 'DiscreteSetting' and provides
//  the specific information needed for the representation of mandatory type.
//  This information includes the set of values that this setting can have
//  (see 'ModeValue.hh'), this setting's activation of Transition Rules
//  (see 'calcTransition()'), and this batch setting's new-patient value.
//
//  Because this is a Main Control Setting, the changing of this setting
//  is done differently than the other settings.  The direct changing of a
//  Main Control Setting does not update the settings that are dependent on
//  its value -- unlike those settings that have dependent settings and
//  override 'calcDependentValues_()'.  The updating of the "dependent"
//  settings of Main Control Settings is done by the Adjusted Context AFTER
//  all of the Main Control Settings have been changed and approved by the
//  operator.  This update is done via the Transition Rules defined for this
//  setting (see 'calcTransition()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ModeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 023  By:  rhj    Date:  22-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Disable PROX under BiLevel and NIV.
//
//  Revision: 022   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 021  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option. Disable CPAP is NeoMode
//		update option is not enabled.
//
//  Revision: 020   By: gdc    Date: 31-Dec-2008    SCR Number: 6177
//  Project:  840S
//  Description:
//      Restructured class to correctly revert to the user-selected value 
// 		when an "observed" setting has forced a change to the value of
// 		this setting. Changed getNewPatient method to provide default value 
// 		for	this setting based on the settings it depends upon. Implemented
// 		derived method setAdjustedValue to save the user-selected value for
// 		so it can be used by valueUpdate in handling changes to higher level
// 		settings such as vent-type.
//
//  Revision: 019   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 018   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 017  By: rhj    Date: 10-Nov-2008    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 016  By: gdc    Date: 24-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Reworked per code review.  
//   
//  Revision: 015  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      NIV mods. Added SRS requirement numbers.  
//
//  Revision: 014  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//   
//  Revision: 013  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      * corrected inadequate SRS mapping
//      * changed 'isEnabledValue()' to a public method, to provide
//        support for the new drop-down menus
//
//  Revision: 012   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added support for percent support's transition rules
//
//  Revision: 011   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 010   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  incorporated circuit-specific new-patient values
//
//  Revision: 009   By: sah   Date:  19-Jan-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Modified 'isEnabledValue_()' method to use new 'SettingOptions'
//	functionality.
//
//  Revision: 008   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  simplified 'calcTransition()' method implementation
//	*  added 'acceptTransition()' method due to new Transition Rule
//	   implementation
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 007   By: dosman    Date:  11-Nov-1998    DR Number: 5252
//  Project:  BILEVEL
//  Description:
//	Added SRS requirement number.
//
//  Revision: 006   By: sah    Date:  29-Sep-1998    DR Number: 5052
//  Project:  BILEVEL
//  Description:
//      Remove redundant processing of apnea correction from
//	'calcTransition()'.
//
//  Revision: 005   By: dosman    Date:  30-Apr-1998    DR Number: 4
//  Project:  BILEVEL
//  Description:
//      added virtual method to be able to determine if a ModeValue
//      is currently enabled
//
//  Revision: 004   By: dosman    Date:  07-Apr-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getNewPatientValue()
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: dosman    Date:  07-Jan-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial version for BiLevel.
//	Added code to handle transition rules going into bilevel
//	and coming from bilevel for peephigh and PI
//	Added transition rules to inform inspiratory time
//      of transition from bilevel 
//      (because absolute max become more strict)
//      and to bilevel 
//      (because absolute max becomes more loose)
//	Added transition rules to inform I:E 
//      of transition from bilevel 
//      (because absolute max become more strict)
//      and to bilevel 
//      (because absolute max is loosened).
//	Added transition rules to inform MandType
//      of transition to bilevel 
//      (because it should be ensured to be PCV).
//      Added requirement numbers for tracing to SRS. 
//      Made sure that IeRatio and InspTime have their maximum
//      values updated when transitioning out of SPONT
//      (and into BILEVEL) because respiratory rate is calculated
//      at that time so those values need to be updated.(3/10/98)
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "ModeSetting.hh"
#include "ModeValue.hh"
#include "PatientCctTypeValue.hh"
#include "VentTypeValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
//@ End-Usage


//@ Code...
static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"A/C", ModeValue::AC_MODE_VALUE},
	{"SIMV", ModeValue::SIMV_MODE_VALUE},
	{"SPONT", ModeValue::SPONT_MODE_VALUE},
	{"BILEVEL", ModeValue::BILEVEL_MODE_VALUE},
	{"CPAP", ModeValue::CPAP_MODE_VALUE},
	{NULL , NULL}
};


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ModeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02200] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ModeSetting::ModeSetting(void)
: BatchDiscreteSetting(SettingId::MODE,
					   Setting::NULL_DEPENDENT_ARRAY_,
					   ModeValue::TOTAL_MODE_VALUES),
	isChangeForced_(FALSE),
	operatorSelectedValue_(NO_SELECTION_)
{
	setLookupTable(ParameterLookup_);
}	// $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~ModeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ModeSetting::~ModeSetting(void)
{
	CALL_TRACE("~ModeSetting(void)");
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setAdjustedValue(newAdjustedValue)  [virtual]
//
//@ Interface-Description
//  This virtual method is called to set the adjusted value of this 
//  setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the Setting base class method to actually store the new value
//  in the adjusted context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ModeSetting::setAdjustedValue(const SettingValue& newValue)
{
	AUX_CLASS_ASSERTION(isEnabledValue(DiscreteValue(newValue)), DiscreteValue(newValue));

	// call the base class to set the value into the adjusted context
	Setting::setAdjustedValue(newValue);

	if (!isChangeForced_)
	{
		operatorSelectedValue_ = newValue;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is ALWAYS changeable.
//
//  $[01080] -- main control setting applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id ModeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id applicabilityId = Applicability::CHANGEABLE;

	if ( qualifierId == Notification::ADJUSTED && 
		 ContextMgr::GetAdjustedContext().getAllSettingValuesKnown() &&
		 SettingsMgr::GetSettingPtr(SettingId::IBW)->isChanged() )
	{
		applicabilityId = Applicability::VIEWABLE;
	}

	return applicabilityId;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcTransition(toValue, fromValue)  [virtual]
//
//@ Interface-Description
//  The purpose of this method is to inform the settings effected
//  by a state transition of this setting. So they can adjust
//  their value(s) appropriately.
//
//  This method deals with state transitions where-as dependent settings
//  calculations typically deal with bounded settings and incremental
//  changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Inform the settings effected by a state transition of this setting.
//  So they can adjust their value(s) appropriately.
//
//  $[02200]   -- valid modes to be handled are ...
//  $[02025]   -- transitioning from A/C...
//  $[TC02000] -- transitioning from SIMV...
//  $[02026]   -- transitioning from SPONT...
//  $[BL02000] -- transitioning from BILEVEL...
//---------------------------------------------------------------------
//@ PreCondition
//  (toValue != fromValue)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void ModeSetting::calcTransition(const DiscreteValue toValue,
								 const DiscreteValue fromValue)
{
	CALL_TRACE("calcTransition(toValue, fromValue)");

	// $[LC02009]
	SettingsMgr::GetSettingPtr(SettingId::LEAK_COMP_ENABLED)->
		acceptTransition(getId(), toValue, fromValue);
	SettingsMgr::GetSettingPtr(SettingId::DISCO_SENS)->
		acceptTransition(getId(), toValue, fromValue);
	SettingsMgr::GetSettingPtr(SettingId::PROX_ENABLED)->
		acceptTransition(getId(), toValue, fromValue);

	switch ( fromValue )
	{
		case ModeValue::AC_MODE_VALUE :	  // $[TI1]
			{ // $[02025] Setting changes for transition from A/C...
				SettingsMgr::GetSettingPtr(SettingId::PRESS_SUPP_LEVEL)->
					acceptTransition(getId(), toValue, fromValue);
				SettingsMgr::GetSettingPtr(SettingId::PERCENT_SUPPORT)->
					acceptTransition(getId(), toValue, fromValue);

				switch ( toValue )
				{
					case ModeValue::SIMV_MODE_VALUE :
					case ModeValue::SPONT_MODE_VALUE :	  // $[TI1.1]
						// do nothing (handled above)
						break;
					case ModeValue::BILEVEL_MODE_VALUE :  // $[TI1.2]
						SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HL_RATIO)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::CPAP_MODE_VALUE :
						SettingsMgr::GetSettingPtr(SettingId::FLOW_ACCEL_PERCENT)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MAND_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_SPONT_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MINUTE_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HIGH_EXH_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HIGH_EXH_MINUTE_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::APNEA_INTERVAL)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::AC_MODE_VALUE :
					default :
						// unexpected "to" mode value...
						AUX_CLASS_ASSERTION_FAILURE(toValue);
						break;
				}
			}
			break;

		case ModeValue::SIMV_MODE_VALUE : // $[TI2]
			{ // $[TC02000] Setting changes for transition from SIMV...
				switch ( toValue )
				{
					case ModeValue::AC_MODE_VALUE :
					case ModeValue::SPONT_MODE_VALUE :	  // $[TI2.1]
						SettingsMgr::GetSettingPtr(SettingId::FLOW_ACCEL_PERCENT)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::BILEVEL_MODE_VALUE :  // $[TI2.2]
						SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HL_RATIO)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::CPAP_MODE_VALUE :
						SettingsMgr::GetSettingPtr(SettingId::FLOW_ACCEL_PERCENT)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MAND_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_SPONT_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MINUTE_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HIGH_EXH_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HIGH_EXH_MINUTE_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::APNEA_INTERVAL)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::SIMV_MODE_VALUE :
					default :
						// unexpected "to" mode value...
						AUX_CLASS_ASSERTION_FAILURE(toValue);
						break;
				}
			}
			break;

		case ModeValue::SPONT_MODE_VALUE :	  // $[TI3]
			{ // $[02026] Setting changes for transition from SPONT...
				switch ( toValue )
				{
					case ModeValue::AC_MODE_VALUE :
					case ModeValue::SIMV_MODE_VALUE : // $[TI3.1]
						SettingsMgr::GetSettingPtr(SettingId::RESP_RATE)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::EXP_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::IE_RATIO)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::BILEVEL_MODE_VALUE :  // $[TI3.2]
						SettingsMgr::GetSettingPtr(SettingId::RESP_RATE)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HL_RATIO)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::CPAP_MODE_VALUE :
						SettingsMgr::GetSettingPtr(SettingId::FLOW_ACCEL_PERCENT)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MAND_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_SPONT_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MINUTE_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HIGH_EXH_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HIGH_EXH_MINUTE_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::APNEA_INTERVAL)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::SPONT_MODE_VALUE :
					default :
						// unexpected "to" mode value...
						AUX_CLASS_ASSERTION_FAILURE(toValue);
						break;
				}
			}
			break;

		case ModeValue::BILEVEL_MODE_VALUE :  // $[TI4]
			{ // $[BL02000] Setting changes for transition from BILEVEL...
				SettingsMgr::GetSettingPtr(SettingId::INSP_PRESS)->
					acceptTransition(getId(), toValue, fromValue);
				SettingsMgr::GetSettingPtr(SettingId::PEEP)->
					acceptTransition(getId(), toValue, fromValue);
				SettingsMgr::GetSettingPtr(SettingId::INSP_TIME)->
					acceptTransition(getId(), toValue, fromValue);

				switch ( toValue )
				{
					case ModeValue::AC_MODE_VALUE :
					case ModeValue::SIMV_MODE_VALUE :	  // $[TI4.1]
						SettingsMgr::GetSettingPtr(SettingId::EXP_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::IE_RATIO)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::SPONT_MODE_VALUE :	  // $[TI4.2]
						// do nothing (handled above)
						break;
					case ModeValue::CPAP_MODE_VALUE :
						SettingsMgr::GetSettingPtr(SettingId::FLOW_ACCEL_PERCENT)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MAND_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_SPONT_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MINUTE_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HIGH_EXH_TIDAL_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HIGH_EXH_MINUTE_VOL)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::APNEA_INTERVAL)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::BILEVEL_MODE_VALUE :
					default :
						// unexpected "to" mode value...
						AUX_CLASS_ASSERTION_FAILURE(toValue);
						break;
				}
			}
			break;

		case ModeValue::CPAP_MODE_VALUE :
			{ 
				// transitions for all transitions out of CPAP
				SettingsMgr::GetSettingPtr(SettingId::FLOW_ACCEL_PERCENT)->
					acceptTransition(getId(), toValue, fromValue);
				SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MAND_TIDAL_VOL)->
					acceptTransition(getId(), toValue, fromValue);
				SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_SPONT_TIDAL_VOL)->
					acceptTransition(getId(), toValue, fromValue);
				SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MINUTE_VOL)->
					acceptTransition(getId(), toValue, fromValue);
				SettingsMgr::GetSettingPtr(SettingId::HIGH_EXH_TIDAL_VOL)->
					acceptTransition(getId(), toValue, fromValue);
				SettingsMgr::GetSettingPtr(SettingId::HIGH_EXH_MINUTE_VOL)->
					acceptTransition(getId(), toValue, fromValue);
				SettingsMgr::GetSettingPtr(SettingId::APNEA_INTERVAL)->
					acceptTransition(getId(), toValue, fromValue);

				// $[LC02002] Setting changes for transition from CPAP type SPONT mode...
				switch ( toValue )
				{
					case ModeValue::AC_MODE_VALUE :
					case ModeValue::SIMV_MODE_VALUE : // $[TI3.1]
						SettingsMgr::GetSettingPtr(SettingId::RESP_RATE)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::EXP_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::IE_RATIO)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::BILEVEL_MODE_VALUE :  // $[TI3.2]
						SettingsMgr::GetSettingPtr(SettingId::RESP_RATE)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW_TIME)->
							acceptTransition(getId(), toValue, fromValue);
						SettingsMgr::GetSettingPtr(SettingId::HL_RATIO)->
							acceptTransition(getId(), toValue, fromValue);
						break;
					case ModeValue::SPONT_MODE_VALUE :
						break;
					case ModeValue::CPAP_MODE_VALUE :
					default :
						// unexpected "to" mode value...
						AUX_CLASS_ASSERTION_FAILURE(toValue);
						break;
				}

			}
			break;

		default :
			// unexpected "from" mode value...
			AUX_CLASS_ASSERTION_FAILURE(fromValue);
			break;
	};
	SettingsMgr::GetSettingPtr(SettingId::HIGH_CCT_PRESS)->
							acceptTransition(getId(), toValue, fromValue);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  isEnabledValue()  [const, virtual]
//
//@ Interface-Description
//  Is this setting's value given by 'value', a currently-enabled value?
//---------------------------------------------------------------------
//@ Implementation-Description
// $[BL04079]
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean ModeSetting::isEnabledValue(const DiscreteValue value) const
{
	Boolean  isEnabled;

	const DiscreteValue  VENT_TYPE_VALUE = 
		SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();

	const Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);
	const DiscreteValue  PATIENT_CCT_TYPE_VALUE = pSetting->getAdjustedValue();

	switch ( value )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :
		case ModeValue::SPONT_MODE_VALUE :			  // $[TI1]
			// these standard values are ALWAYS enabled...
			isEnabled = TRUE;
			break;

		case ModeValue::BILEVEL_MODE_VALUE :	  // $[TI2]
			// bilevel option must be enabled
			if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL) )
			{		// $[TI2.1]
				// only available using invasive vent type
				isEnabled = (VENT_TYPE_VALUE == VentTypeValue::INVASIVE_VENT_TYPE);
			}
			else
			{
				// $[TI2.2]
				isEnabled = FALSE;
			}
			break;

		case ModeValue::CPAP_MODE_VALUE :
			isEnabled = SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_UPDATE) &&
						(PATIENT_CCT_TYPE_VALUE == PatientCctTypeValue::NEONATAL_CIRCUIT) &&
						(VENT_TYPE_VALUE == VentTypeValue::NIV_VENT_TYPE);
			break;

		case ModeValue::TOTAL_MODE_VALUES :
		default :
			// unexpected mode value...
			AUX_CLASS_ASSERTION_FAILURE(value);
			break;
	}
	return(isEnabled);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Returns this setting's new-patient (default) value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a default setting value for mode based on the 
//  current circuit type.
//  $[02201] -- new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue ModeSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	DiscreteValue newPatientValue;

	const Setting*  pPatientCctType = SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);
	const DiscreteValue  PATIENT_CCT_TYPE_VALUE = pPatientCctType->getAdjustedValue();
	const DiscreteValue  CURRENT_MODE = getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			if (CURRENT_MODE == ModeValue::CPAP_MODE_VALUE)
			{
				newPatientValue = ModeValue::SPONT_MODE_VALUE;
			}
			else
			{
				newPatientValue = ModeValue::SIMV_MODE_VALUE;
			}
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2]
			newPatientValue = ModeValue::AC_MODE_VALUE;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(newPatientValue);
}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  resetState()  [virtual]
//
//@ Interface-Description
//  Reset the state of this setting, preparing it for changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void ModeSetting::resetState(void)
{
	CALL_TRACE("resetState()");

	// forward up to base class...
	Setting::resetState();

	operatorSelectedValue_ = getAcceptedValue();
}	// $[TI1]

//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to vent type and IBW value changes by updating mode
//  applicability and value.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[NI02010]\a\ -- for NIV, if Mode=BILEVEL then set to AC
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


void ModeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
							  const SettingSubject*               pSubject)
{
	if ( qualifierId == Notification::ADJUSTED )
	{
		const SettingId::SettingIdType SETTING_ID = pSubject->getId();
		if (SETTING_ID == SettingId::VENT_TYPE)
		{
			const DiscreteValue CURRENT_VALUE = getAdjustedValue();

			isChangeForced_ = TRUE;
			// attempt to revert to the operator selection first if enabled
			if (operatorSelectedValue_ != NO_SELECTION_ && isEnabledValue(operatorSelectedValue_))
			{
				if (CURRENT_VALUE != operatorSelectedValue_)
				{
					setAdjustedValue(operatorSelectedValue_);
				}
			}
			else
			{
				setAdjustedValue(getNewPatientValue());
			}
			// else - no change required
			isChangeForced_ = FALSE;

			updateApplicability(qualifierId);
		}
		else if (SETTING_ID == SettingId::IBW)
		{
			updateApplicability(qualifierId);
		}
		else
		{
			AUX_CLASS_ASSERTION_FAILURE(SETTING_ID);
		}
	}  // -- don't care about accepted value changes...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean ModeSetting::doRetainAttachment(const SettingSubject*) const
{
	CALL_TRACE("doRetainAttachment(pSubject)");

	return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor Vent Type and IBW values therefore
//  this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void ModeSetting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	// monitor changes in vent type value...
	attachToSubject_(SettingId::VENT_TYPE, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::IBW, Notification::VALUE_CHANGED);
}  // $[TI1]




//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void ModeSetting::SoftFault(const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							MODE_SETTING, lineNumber, pFileName,
							pPredicate);
}
