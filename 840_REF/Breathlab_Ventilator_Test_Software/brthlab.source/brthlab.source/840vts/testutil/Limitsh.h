/*****************************************************************************/
/*   Module Name:    Limitsh.h                                        
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _LIMITS_72361_H
#define _LIMITS_72361_H

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include "ldb.h"

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#define OXYGEN           0
#define AIR              1
#define PEEP             2
#define PRESSURE         5
#define VOLUME           8
#define MAX_LIMIT_NAME  25
#define MAX_NAME_SIZE   20

/***********************************************/
/************ STRUCTURE DEFINITIONS ************/
/***********************************************/

struct TestTarget
{
	int    name;    /* Ldb index to description */
	double target;  /* target value  */
};

static struct TestTarget OxiTestTarget[] =
{
	/* Name */         /* Target */
	   Ldb_oxygen_30,     0.30,
	   Ldb_oxygen_90,     0.90,
};

struct VolumeStruct
{
  int  inspTime;
  int  target;
  char trigType[10];
  int  gas;
};

struct TargetLimits
{
	double target;      /* setting to be tested */
	double lowerLimit;  /* lower limit for comparing actual with target */
	double upperLimit;  /* upper limit for comparing actual with target */
}; 



static const double MetLimitHigh = 3.2;
static const double MetLimitLow  = 0.1;     
static const double MetOffLimit  = 0.05;

static struct TestTarget PeepTestTarget[] =
{
  /* Name */         /* target */
     Ldb_peep_5,         5.0,
     Ldb_peep_25,       25.0,
     Ldb_peep_45,       45.0,
};

static struct TestTarget PressureTestTarget[] =
{
	/* Name */       /* target */
	   Ldb_pres_5,       5.0,
	   Ldb_pres_90,     90.0,
};

// Circuit Pressure reported by PTS shall not be qualified in accordance with test specifications 
// so as to be consistent with manufacturing test software.  By order of Project Manager 
// with agreement by manufacturing test engineering in peer review 09/12/2000.
//static struct TargetLimits const CircuitPressureLimits[] = 
//{
//	/* Target*/  /* LowerLimit */  /* UpperLimit */ 
//	    10.0,        9.23,             10.78,      
//	    50.0,       45.34,             54.67,      
//	   100.0,       93.71,            106.29,      
//};

static struct TestTarget CpInspTestTarget[] =
{
	/* Name */       /* target */
	   Ldb_cpi_10,       10.0,
	   Ldb_cpi_50,       50.0,
	   Ldb_cpi_100,     100.0, 
};

static struct TestTarget CpExhTestTarget[] =
{
	/* Name */       /* target */
	   Ldb_cpe_10,       10.0,
	   Ldb_cpe_50,       50.0,
	   Ldb_cpe_100,     100.0, 
};

static const double InspUpperAccuracy = 0.0798;
static const double InspUpperOffset   = 0.18;
static const double InspLowerAccuracy = 0.0766;
static const double InspLowerOffset   = 0.20;

static const double ExhUpperAccuracy  = 0.0808;
static const double ExhUpperOffset    = 0.20;
static const double ExhLowerAccuracy  = 0.0756;
static const double ExhLowerOffset    = 0.28;

static const double OxygenAccuracyLow30    = 0.2630;
static const double OxygenAccuracyHigh30   = 0.3386; 

static const double OxygenAccuracyLow90    = 0.8474;
static const double OxygenAccuracyHigh90   = 0.9542; 

static const double PeepAccuracy      = 0.0475;
static const double PeepOffset        = 2.04;

static const double PressureAccuracy  = 0.0325;
static const double PressureOffset    = 3.04;

static const double CircuitPressureAccuracy =  0.0385;
static const double CircuitPressureOffset   =  0.39;

static const double O2_Level_Volume         = 50.0;

static const double Volume25Accuracy =   0.33;
static const double VolumeAccuracy   =   0.13;
static const double VolumeOffset     =  10;


static struct VolumeStruct Volume[] =
{
	// Volume test settings shall not match test specifications 
	// so as to be consistent with manufacturing test software.  By order of Project Manager
	// with agreement by manufacturing test engineering in peer review 09/12/2000.
	/* inspTime */  /* target */  /* trig */    /* Gas */
	   200,              25,         "PressT",     /*AIR*/ OXYGEN,
	   200,              25,         /*"FlowT"*/"PressT",      AIR,  
	   200,              25,         "FlowT",      /*OXYGEN*/ AIR,
	  1600,             200,         "FlowT",      AIR,
	  1600,             200,         "PressT",     AIR,
	   800,             600,         "PressT",     AIR,
	   800,             600,         "FlowT",      AIR,
	  1260,            2500,         "FlowT",      AIR,
	  1260,            2500,         "PressT",     AIR,
};

 static struct TargetLimits const GasFlowLimits[] = 
{
	/* Target*/  /* LowerLimit */  /* UpperLimit */ 
	     1.0,         0.26,             1.79,      
	     8.0,         6.40,             9.72,      
	    60.0,        52.03,            68.61,      
	   150.0,       131.01,           170.54,     
	   200.0,       174.88,           227.16,     
};


#endif // #ifndef _LIMITS_72361_H
