 
#ifndef O2Mixture_HH
#define O2Mixture_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: O2Mixture - determines the required O2 mix
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/O2Mixture.hhv   25.0.4.0   19 Nov 2013 13:59:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013  By:  rpr    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 012   By: rpr    Date: 23-Jan-2009    SCR Number: 6435  
//  Project:  840S
//  Description:
//      Modified for code review comments.
// 
//  Revision: 011   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 010  By:  syw	   Date:  06-Jan-2000    DCS Number: 5327, 5606
//  Project:  NeoMode
//  Description:
//		Initial version.
//		Use 40% for idle flow and OSC if NEO. 
//		Handle restoring o2% when gas supply is recovered during OSC.
//
//  Revision: 009  By: yyy     Date:  5-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added updateCycleToCycleCorrection_(), resetMixErrorSum() methods.
//
//  Revision: 008  By: iv      Date:  18-Mar-1999    DR Number: 5352
//  Project:  ATC
//  Description:
//      Add a flag mixResetRequired_.
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      ATC initial release.
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 004  By:  iv    Date:  04-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changes for unit test.
//
//  Revision: 003 By:  iv   Date:   15-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface
//             and for Alarm-Analysis interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "TimerTarget.hh"
#  include "UiEvent.hh"

//@ Usage-Classes
class BreathPhase;
class GasSupply;
class BreathPhaseScheduler;
class IntervalTimer;
//@ End-Usage

//@ constant: MAX_NUM_PHASES
// An arbitrary number big enough to accomodate all breath phases 
static const Int16 MAX_NUM_PHASES = 20;

extern const Int32 HUNDRED_PERCENT_DURATION_MS;

class O2Mixture: public TimerTarget
{
	public:

	enum GasMix {ALL_O2 = 0, ALL_AIR, AIR_O2};
	O2Mixture(IntervalTimer& rO2Timer, IntervalTimer& rMixTimer);
	virtual ~O2Mixture(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

    static EventData::EventStatus Request100PercentO2(const EventData::EventId id,
													 const Boolean eventStatus);
	static void SetGasSupplyStatus(const GasSupply& gasSupply);
	inline static void Set100PercentRequested(const Boolean requestStatus);
	inline static void SetNeocalO2SensorRequested(const Boolean requestStatus);
    inline Real32 getAppliedO2Percent(void) const;
    inline Real32 getTargetO2Percent(void) const;
    Real32 calculateO2Fraction(Real32 desiredFlow=0.0);
	inline static Boolean Is100PercentO2Requested(void);
    void determineO2Mix(const BreathPhaseScheduler & scheduler, BreathPhase& rPhase);
    virtual void timeUpHappened(const IntervalTimer& timer);
	void registerBreathPhaseCallBack(BreathPhase * const pBreathPhase);
	inline void resetMixErrorSum(void);

  protected:

  private:
    O2Mixture(const O2Mixture&);		// not implemented...
    void   operator=(const O2Mixture&);	// not implemented...
	O2Mixture(void);  // not implemented...

	void setGasSupplyStatus_(const GasSupply& gasSupply);
	void resetMixErrorSum_(void);
	void calculateBreathMix_(BreathPhase& rPhase);
	Real32 updateCycleToCycleCorrection_(Real32 desiredFlow);
	Real32 neonatalMixHandle_(void) ;
    void cancelO2Request_(void);

	// returns TRUE if option is enabled and neo ciruit is in use
	Boolean isPlus20Enabled_(void);

    //@ Data-Member: targetO2Percent_
    // The O2 percent targeted.
    Real32 targetO2Percent_;

    //@ Data-Member: appliedO2Percent_
    // The O2 percent currently in effect.
    Real32 appliedO2Percent_;

    //@ Data-Member:  isCalO2Sensor_
    // specifies whether or not to calibrate O2 sensor.
    Boolean isCalO2Sensor_;

    //@ Data-Member:  IsNeoCalO2Sensor_
    // force calibration of O2 sensor in neonatal environment
	// forced by the MoreSettingsSubscreen Enable/Disable/Cal
    static Boolean IsNeoCalO2Sensor_;

    //@ Data-Member: mixBeforeCalibration_
    // The O2 percent at the start of 100% O2 event.
    Real32 mixBeforeCalibration_;

    //@ Data-Member:  isO2Available_
    // specifies whether or not the ventilator can deliver O2.
    Boolean isO2Available_;

    //@ Data-Member:  isAirAvailable_
    // specifies whether or not the ventilator can deliver air.
    Boolean isAirAvailable_;

    //@ Data-Member:  Is100PercentO2_
    // The state of 100% o2 delivery - as a result of user request 
    static Boolean Is100PercentO2_;

    //@ Data-Member:  isDisconnected_
    // specifies whether or not the ventilator is disconnected.
	Boolean isDisconnected_;

    //@ Data-Member:  isOccluded_
    // specifies whether or not the ventilator is occluded.
	Boolean isOccluded_;

	//@ Data-Member:& rO2Timer_
	// a reference to the timer server
	IntervalTimer& rO2Timer_;

	//@ Data-Member:& rNCPAPO2Timer_
	// a reference to the timer server controls the interval of the 
	// determineO2Mix is called when in NCPAP
	IntervalTimer& rNCPAPO2Timer_;

	//@ Data-Member: pBreathPhase_ []
	// a pointer to an array of pointers to BreathPhase
	BreathPhase *pBreathPhase_[MAX_NUM_PHASES];

	//@ Data-Member: phaseIndex_
	// tracks the number of breath phases registered for callback
	Int16 phaseIndex_;

	//@ Data-Member: mixResetRequired_
	// flag to reset mix calculations at start of inspiration when change
	// in setting was detected during previous inspiration.
	Boolean mixResetRequired_;

	//@ Data-Member: mixErrorSum_
	// sum of all the mix errors
	Real32 mixErrorSum_;

	//@ Data-Member: readyToCorrect_
	// flag to hold off mix correction
	Boolean readyToCorrect_;

};


// Inlined methods...
#include "O2Mixture.in"


#endif // O2Mixture_HH 
