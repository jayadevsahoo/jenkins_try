#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SstLeakSubScreen - The subscreen is activated when running SST 
// leak test.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains textual fields, and a LeakGauge object.
// This class inherits Service Data callbacks from the abstract ServiceDataTarget
// class.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// method setValue() provides a way for SstTestSubScreen to update the
// leak data through graphing on the upper screen area.
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the Sst Leak subscreen in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The implementation is quite simple.  One leak gauge graph is displayed.
// Whenever the leak data is passed down from SstTest subscreen to this object,
// we update the display to reflect the change.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SstLeakSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SstLeakSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "SubScreenArea.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SstLeakSubScreen()  [Constructor]
//
//@ Interface-Description
//  Creates the Sst Leak subscreen.  Passed a pointer to the subscreen
//  area which creates it (LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sizes, positions and colors the subscreen container.
//  Creates the leak gauge displayed in the subscreen and adds them
//  to the container.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstLeakSubScreen::SstLeakSubScreen(SubScreenArea *pSubScreenArea) :
		SubScreen(pSubScreenArea),
		titleArea_(MiscStrs::SST_LEAK_TEST_SUBSCREEN_TITLE),
		leakGauge_(50.0, 90.0, 
				MiscStrs::SST_LEAK_PASS_MSG, MiscStrs::SST_LEAK_FAIL_MSG,
				75.0, 55.0, 
				MiscStrs::SST_LEAK_ALERT_MSG)
{
	CALL_TRACE("SstLeakSubScreen::SstLeakSubScreen(SubScreenArea "
													"*pSubScreenArea)");

#if defined FORNOW
printf("SstLeakSubScreen::() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Size and position the text displayable containers
	leakGauge_.setX(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH/2);
	leakGauge_.setY(38);
	
	// Add the following drawables
	addDrawable(&leakGauge_);
	addDrawable(&titleArea_);
		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SstLeakSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys SstLeakSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstLeakSubScreen::~SstLeakSubScreen(void)
{
	CALL_TRACE("SstLeakSubScreen::~SstLeakSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Called by our subscreen area before this subscreen is to be displayed
//  allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We simpily activate the leakGauge.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstLeakSubScreen::activate(void)
{
	CALL_TRACE("SstLeakSubScreen::activate(void)");

#if defined FORNOW	
printf("SstLeakSubScreen::activate()\n");
#endif	// FORNOW

	// Activate the leak gauge.
	leakGauge_.activate();
		// $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Called by our subscreen area just after this subscreen is removed from
//  the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstLeakSubScreen::deactivate(void)
{
	CALL_TRACE("SstLeakSubScreen::deactivate(void)");

#if defined FORNOW
printf("SstLeakSubScreen::deactivate() at line  %d\n", __LINE__);
#endif	// FORNOW

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValue
//
//@ Interface-Description
//  Inform the leakGauge object to update the new leak datum.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call leakGauge's setValue() method to update the leak datum.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstLeakSubScreen::setValue(Real32 value)
{
	CALL_TRACE("SstLeakSubScreen::setValue(Real32 value)");
	leakGauge_.setValue(value);
		// $[TI1]
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SstLeakSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
			SSTLEAKSUBSCREEN, lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
