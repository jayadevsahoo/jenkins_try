//------------------------------------------------------------------------------
//                   Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file xallocator.h
//------------------------------------------------------------------------------
#ifndef XALLOCATOR_HH

#include <stddef.h>

extern "C" void *xalloc(size_t size);
extern "C" void xfree(void* ptr);
extern "C" void *xrealloc(void *ptr, size_t size);
extern "C" void xallocator_init();
#endif // XALLOCATOR_HH
