#ifndef DownloadTaskCons_HH
#define DownloadTaskCons_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996-1997, Nellcor/Puritan-Bennett Corporation
//=====================================================================
 
// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: DownloadTaskCons - Defines all the constant values used
//		for the download tasks.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/DownloadTaskCons.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  jhv      Date:  23-Feb-96    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//=====================================================================
 
enum TaskMode
{
	SUPERVISOR_MODE = 0x1,
	USER_MODE = 0x2
};

enum TaskPri
{
	STACKWARE_PRIORITY= 10,
	TFTP_CLIENT_PRIORITY = 30,
#ifdef SIGMA_UNIT_TEST
	TESTMAIN_PRIORITY = 40,
	TESTUSER_PRIORITY = 40,
#endif // SIGMA_UNIT_TEST
	FLASH_BURNER_PRIORITY = 20,
    LOWEST_PRIORITY = 255
};

enum TaskId
{
	STACKWARE_TASK_ID = 1,
	LOWEST_TASK_ID = STACKWARE_TASK_ID,
	TFTP_CLIENT_ID,
#ifdef SIGMA_UNIT_TEST
	TESTMAIN_ID,
	TESTUSER_ID,
#endif // SIGMA_UNIT_TEST
	FLASH_BURNER_ID,
	HIGHEST_TASK_ID = FLASH_BURNER_ID,
	NUM_TASKS = HIGHEST_TASK_ID
};

enum TASK_LIMITS
{
	STACK_MAGIC_COOKIE = 0xEF,
#ifdef SIGMA_UNIT_TEST
	TASK_STACK_SIZE = 64*1024
#else
	TASK_STACK_SIZE = 32*1024
#endif // SIGMA_UNIT_TEST
};
#endif   // DownloadTaskCons_HH
