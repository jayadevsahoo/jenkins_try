// ShowMsgContinueDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Swat.h"
#include "ShowMsgContinueDlg.h"


// CShowMsgContinueDlg dialog

IMPLEMENT_DYNAMIC(CShowMsgContinueDlg, CDialog)

CShowMsgContinueDlg::CShowMsgContinueDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CShowMsgContinueDlg::IDD, pParent)
{

}

CShowMsgContinueDlg::~CShowMsgContinueDlg()
{
}

void CShowMsgContinueDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CShowMsgContinueDlg, CDialog)
END_MESSAGE_MAP()


// CShowMsgContinueDlg message handlers

void CShowMsgContinueDlg::ShowString(CString strMessage)
{

	CWnd* wnd = GetDlgItem(IDC_STATIC_MSG);
	wnd->SetWindowTextA(strMessage);

}

void CShowMsgContinueDlg::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
 
  delete this;

	CDialog::PostNcDestroy();
}
