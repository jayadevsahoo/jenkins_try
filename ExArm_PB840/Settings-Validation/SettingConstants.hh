#ifndef SettingConstants_HH
#define SettingConstants_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SettingConstants - Subsystem-Wide Constants.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingConstants.hhv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		DEFINED_TIDAL_VOL_NEONATAL_MIN_SQUARE_FLOW_PATTERN_VALUE and
// 		TIDAL_VOL_NEONATAL_MIN_SQUARE_FLOW_PATTERN_VALUE added.
//
//  Revision: 014   By: mnr    Date: 21-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		IBW_ABSOLUTE_MIN_NEO_VALUE added.
//
//  Revision: 013   By: mnr    Date: 10-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//	Added more #defines and Setting Constants for distinct lower tidal volume 
//  bounds based on MAND TYPE.
//
//  Revision: 012  By: mnr     Date: 24-Jun-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//   	Lowered the Neonatal tidal vol min value to 2.0f.
// 
//  Revision: 011   By: gdc    Date: 26-Jan-2009    SCR Number: 6461
//  Project:  840S
//  Description:
//      Implemented #define workaround for compiler bug initializing
//      static const structs.
//
//  Revision: 010   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 009  By: gdc    Date:  24-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      Corrected comment per code review.
//   
//  Revision: 008  By: gdc    Date:  18-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 007  By: gfu    Date:  11-Aug-2004    DR Number: 6132
//  Project:  PAV3
//  Description:
//      Modified code per SDCR #6132.  Also added keyword "Log" to the file header
//      as requested during code review.
//
//  Revision: 006  By: sah    Date:  07-Nov-2000    DR Number: 5789
//  Project:  VTPC
//  Description:
//      Added new peak flow min/max constants for use internally, and by
//      Breath-Delivery.
//
//  Revision: 005   By: sah    Date: 09-Feb-2000    DR Number: 5635
//  Project:  NeoMode
//  Description:
//      Eliminated IBW NEONATAL soft-bound constant.
//
//  Revision: 004   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode project-related changes:
//      *  added new circuit-based constants
//      *  obsoleted patient type setting
//
//  Revision: 003   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new ATC-specific constants
//
//  Revision: 002   By: dosman    Date:  24-Dec-1997    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//	Added symbolic constants.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//            Initial version 
//  
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
#include "SequentialValue.hh"
//@ End-Usage


struct SettingConstants
{
  public:

    //====================================================================
    //  Pressure-Based Breath Constants...
    //====================================================================

    //@ Constant:  MAX_ALLOWED_PRESSURE_SETTING_VALUE
    // Maximum allowed pressure setting value, 
    // to avoid reaching actual maximum pressure in tube
    static const Real32  MAX_ALLOWED_PRESSURE_SETTING_VALUE;

    //@ Constant:  MIN_NON_MAND_PRESS_BUFF
    // Minimum difference in pressure between PEEP and High Circuit
    // Pressure allowed.
    static const Real32  MIN_NON_MAND_PRESS_BUFF;

    //@ Constant: MIN_MAND_PRESS_DELTA 
    // Minimum difference in pressure between PEEP and a mandatory pressure
    // level which could be either PEEP_HIGH (in BiLevel) which is defined
    // from an absolute zero pressure or INSP_PRESS (in any other mode) which
    // is defined with PEEP as the zero.
    static const Real32  MIN_MAND_PRESS_DELTA;

    //@ Constant: MIN_MAND_PRESS_BUFF
    // Minimum difference in pressure between HIGH CCT PRESSURE
    // and a pressure setting (pressure support or inspiratory pressure)
    // that is determined with PEEP as the zero.
    static const Real32  MIN_MAND_PRESS_BUFF;

    //@ Constant: DEFAULT_MAND_PRESS_DELTA
    // 
    static const Real32  DEFAULT_MAND_PRESS_DELTA;

    //@ Constant:  NP_PEEP_VALUE
    // Default new patient PEEP value
    static const Real32  NP_PEEP_VALUE;

    //@ Constant:  LOW_PRESS_PEEP_DELTA
    // Default low circuit pressure delta above PEEP
    // LowCctPressure = PEEP + LOW_PRESS_PEEP_DELTA
    static const Real32  LOW_PRESS_PEEP_DELTA;

    //====================================================================
    //  Apnea and non-Apnea Respiratory Rate Constants...
    //====================================================================

    //@ Constant:  NP_RESP_RATE_*_VALUE
    // These are the new-patient values of apnea and non-apnea respiratory rate.
    static const Real32  NP_RESP_RATE_NEO_VALUE;
    static const Real32  NP_RESP_RATE_PED_VALUE;
    static const Real32  NP_RESP_RATE_ADULT_VALUE;


    //====================================================================
    //  Ideal Body Weight Constants...
    //====================================================================

    //@ Constant:  IBW_ABSOLUTE_MIN_VALUE
    // IBW's absolute minimum, regardless of circuit type.
    static const Real32  IBW_ABSOLUTE_MIN_VALUE;

	//@ Constant:  IBW_ABSOLUTE_MIN_NEO_VALUE
	// IBW's absolute minimum for NEO_MODE_UPDATE option, 
    // regardless of circuit type.
	static const Real32  IBW_ABSOLUTE_MIN_NEO_VALUE;

    //@ Constant:  IBW_ABSOLUTE_MAX_VALUE
    // IBW's absolute maximum, irregardless of circuit type.
    static const Real32  IBW_ABSOLUTE_MAX_VALUE;


    //@ Constant:  IBW_NEO_CCT_HARD_MAX_VALUE
    // IBW's hard maximum with a NEONATAL circuit type.
    static const Real32  IBW_NEO_CCT_HARD_MAX_VALUE;


    //@ Constant:  IBW_PED_CCT_HARD_MIN_VALUE
    // IBW's hard minimum with a PEDIATRIC circuit type.
    static const Real32  IBW_PED_CCT_HARD_MIN_VALUE;

    //@ Constant:  IBW_PED_CCT_SOFT_MIN_VALUE
    // IBW's soft minimum with a PEDIATRIC circuit type.
    static const Real32  IBW_PED_CCT_SOFT_MIN_VALUE;

    //@ Constant:  IBW_PED_CCT_SOFT_MAX_VALUE
    // IBW's soft maximum with a PEDIATRIC circuit type.
    static const Real32  IBW_PED_CCT_SOFT_MAX_VALUE;

    //@ Constant:  IBW_PED_CCT_HARD_MAX_VALUE
    // IBW's hard maximum with a PEDIATRIC circuit type.
    static const Real32  IBW_PED_CCT_HARD_MAX_VALUE;


    //@ Constant:  IBW_ADULT_CCT_HARD_MIN_VALUE
    // IBW's hard minimum with a ADULT circuit type.
    static const Real32  IBW_ADULT_CCT_HARD_MIN_VALUE;

    //@ Constant:  IBW_ADULT_CCT_SOFT_MIN_VALUE
    // IBW's soft minimum with a ADULT circuit type.
    static const Real32  IBW_ADULT_CCT_SOFT_MIN_VALUE;


    //====================================================================
    //  Peak Inspiratory Flow Constants...
    //====================================================================

    //@ Constant:  NP_PEAK_FLOW_IBW_SCALE
    // This is the scaling value used by the peak inspiratory flows to
    // determine their IBW-based new-patient value.
    static const Real32  NP_PEAK_FLOW_NEO_IBW_SCALE;
    static const Real32  NP_PEAK_FLOW_PED_IBW_SCALE;
    static const Real32  NP_PEAK_FLOW_ADULT_IBW_SCALE;

    static const Real32  MIN_PEAK_FLOW_NEO_VALUE;
    static const Real32  MAX_PEAK_FLOW_NEO_VALUE;
    static const Real32  MIN_PEAK_FLOW_PED_ADULT_VALUE;
    static const Real32  MAX_PEAK_FLOW_PED_VALUE;
    static const Real32  MAX_PEAK_FLOW_ADULT_VALUE;

    //====================================================================
    //  Tidal Volume Constants...
    //====================================================================

    //@ Constant:  MAX_TIDAL_VOL_IBW_SCALE
    // This is the scaling value used by the tidal volumes to determine their
    // IBW-based upper limit.
    static const Real32  MAX_TIDAL_VOL_IBW_SCALE;

    //@ Constant:  MIN_TIDAL_VOL_IBW_SCALE
    // This is the scaling value used by the tidal volumes to determine their
    // IBW-based lower limit.
    static const Real32  MIN_TIDAL_VOL_IBW_SCALE;

    //@ Constant:  NP_TIDAL_VOL_IBW_SCALE
    // This is the scaling value used by the tidal volumes to determine their
    // IBW-based new-patient value.
    static const Real32  NP_TIDAL_VOL_IBW_SCALE;

    //@ Constant:  TIDAL_VOL_STANDARD_MIN_VALUE;
    // This is the standard (non-NeoMode) absolute minimum tidal volume value.
    static const Real32  TIDAL_VOL_STANDARD_MIN_VALUE;

    //@ Constant:  TIDAL_VOL_NEONATAL_MIN_VALUE_NON_VC_PLUS;
    // This is the neonatal (NeoMode active) absolute minimum tidal volume
    // value for NON VC PLUS (VC and PC)
    static const Real32  TIDAL_VOL_NEONATAL_MIN_VALUE_NON_VC_PLUS;

	//@ Constant:  TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS;
    // This is the neonatal (NeoMode active) absolute minimum tidal volume
    // value for VC+.
    static const Real32  TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS;

	//@ Constant:  TIDAL_VOL_NEONATAL_MIN_VALUE_MISC;
    // This is the neonatal (NeoMode active) absolute minimum tidal volume
    // value for alarm limits etc.
    static const Real32  TIDAL_VOL_NEONATAL_MIN_VALUE_MISC;

	//@ Constant:  TIDAL_VOL_NEONATAL_MIN_SQUARE_FLOW_PATTERN_VALUE
    // This is the neonatal (NeoMode active) absolute minimum tidal volume
    // value for Square flow pattern.
    static const Real32  TIDAL_VOL_NEONATAL_MIN_SQUARE_FLOW_PATTERN_VALUE;

    //@ Constant:  MAX_TIDAL_VOL_SOFT_IBW_SCALE;
    // This is the IBW-based upper soft limit factor.
    static const Real32  MAX_TIDAL_VOL_SOFT_IBW_SCALE;

    //@ Constant:  MIN_TIDAL_VOL_SOFT_IBW_SCALE;
    // This is the IBW-based lower soft limit factor.
    static const Real32  MIN_TIDAL_VOL_SOFT_IBW_SCALE;


    //====================================================================
    //  High Exhaled Minute Volume Constants...
    //====================================================================

    //@ Constant:  NP_HIGH_EXH_MINUTE_VOL_NEO_SCALE
    // This is the scaling value used by the high exhaled minute volume to
    // determine its NEONATAL-based, new-patient value.
    static const Real32  NP_HIGH_EXH_MINUTE_VOL_NEO_SCALE;

    //@ Constant:  NP_HIGH_EXH_MINUTE_VOL_PED_SCALE
    // This is the scaling value used by the high exhaled minute volume to
    // determine its PEDIATRIC-based, new-patient value.
    static const Real32  NP_HIGH_EXH_MINUTE_VOL_PED_SCALE;

    //@ Constant:  NP_HIGH_EXH_MINUTE_VOL_ADULT_SCALE
    // This is the scaling value used by the high exhaled minute volume to
    // determine its ADULT-based, new-patient value.
    static const Real32  NP_HIGH_EXH_MINUTE_VOL_ADULT_SCALE;


    //====================================================================
    //  Low Exhaled Minute Volume Constants...
    //====================================================================

    //@ Constant:  NP_LOW_EXH_MINUTE_VOL_NEO_SCALE
    // This is the scaling value used by the low exhaled minute volume to
    // determine its NEONATAL-based, new-patient value.
    static const Real32  NP_LOW_EXH_MINUTE_VOL_NEO_SCALE;

    //@ Constant:  NP_LOW_EXH_MINUTE_VOL_PED_SCALE
    // This is the scaling value used by the low exhaled minute volume to
    // determine its PEDIATRIC-based, new-patient value.
    static const Real32  NP_LOW_EXH_MINUTE_VOL_PED_SCALE;

    //@ Constant:  NP_LOW_EXH_MINUTE_VOL_ADULT_SCALE
    // This is the scaling value used by the low exhaled minute volume to
    // determine its ADULT-based, new-patient value.
    static const Real32  NP_LOW_EXH_MINUTE_VOL_ADULT_SCALE;


    //====================================================================
    //  High Exhaled Tidal Volume Constants...
    //====================================================================

    //@ Constant:  NP_HIGH_EXH_TIDAL_VOL_IBW_SCALE
    // This is the scaling value used by the high exhaled tidal volume to
    // determine its IBW-based, new-patient value.
    static const Real32  NP_HIGH_EXH_TIDAL_VOL_IBW_SCALE;

    //====================================================================
    //  High Inspired Tidal Volume Constants...
    //====================================================================

	static const Real32  NP_HIGH_INSP_TIDAL_VOL_IBW_SCALE;
	static const Real32  MIN_HIGH_INSP_TIDAL_VOL_IBW_SCALE;

    //====================================================================
    //  Low Exhaled Mandatory Tidal Volume Constants...
    //====================================================================

    //@ Constant:  NP_LOW_MAND_TIDAL_VOL_IBW_SCALE
    // This is the scaling value used by the low exhaled mandatory tidal
    // volume to determine its IBW-based, new-patient value.
    static const Real32  NP_LOW_MAND_TIDAL_VOL_IBW_SCALE;


    //====================================================================
    //  Low Exhaled Spontaneous Tidal Volume Constants...
    //====================================================================

    //@ Constant:  NP_LOW_SPONT_TIDAL_VOL_IBW_SCALE
    // This is the scaling value used by the low exhaled spontaneous tidal
    // volume to determine its IBW-based, new-patient value.
    static const Real32  NP_LOW_SPONT_TIDAL_VOL_IBW_SCALE;


    //====================================================================
    //  Alarm Limit Constants...
    //====================================================================

    //@ Constant:  UPPER_ALARM_LIMIT_OFF
    // This is the value that indicates that a upper alarm is turned "off".
    static const Real32  UPPER_ALARM_LIMIT_OFF;

    //@ Constant:  LOWER_ALARM_LIMIT_OFF
    // This is the value that indicates that a lower alarm is turned "off".
    static const Real32  LOWER_ALARM_LIMIT_OFF;


    //====================================================================
    //  Display Setting Range Constants...
    //====================================================================

    //@ Constant:  MAX_DISPLAY_CONTRAST_SCALE
    // Maximum display contrast scale value.
    static const SequentialValue  MAX_DISPLAY_CONTRAST_SCALE;

    //@ Constant:  MIN_DISPLAY_CONTRAST_SCALE
    // Minimum display contrast scale value.
    static const SequentialValue  MIN_DISPLAY_CONTRAST_SCALE;

    //@ Constant:  MAX_DISPLAY_CONTRAST_DELTA
    // Maximum display contrast delta value.
    static const SequentialValue  MAX_DISPLAY_CONTRAST_DELTA;

    //@ Constant:  MIN_DISPLAY_CONTRAST_DELTA
    // Minimum display contrast delta value.
    static const SequentialValue  MIN_DISPLAY_CONTRAST_DELTA;

    //@ Constant:  MAX_DISPLAY_BRIGHTNESS
    // Maximum display brightness value.
    static const SequentialValue  MAX_DISPLAY_BRIGHTNESS;

    //@ Constant:  MIN_DISPLAY_BRIGHTNESS
    // Minimum display brightness value.
    static const SequentialValue  MIN_DISPLAY_BRIGHTNESS;


    //====================================================================
    //  For External Subsystem Use Only...
    //====================================================================

    //@ Constant:  MIN_TUBE_ID_VALUE
    // Minimum tube id value.
    static const Real32  MIN_TUBE_ID_VALUE;

    //@ Constant:  TOTAL_TUBE_ID_VALUES
    // Number of tube id value.
    // $[TC02030] -- range of tube id values...
    // $[TC02033] -- resolution of tube id...
    enum { TOTAL_TUBE_ID_VALUES = 12 };


    //====================================================================
    //  General Arithmetic Constants...
    //====================================================================

    //@ Constant:  MSEC_TO_MIN_CONVERSION
    // Millisecond to minute conversion factor.
    static const Real32  MSEC_TO_MIN_CONVERSION;

    //=====================================================================
    //  PAV support Constants...
    //=====================================================================

    // New patient default percent support values
   
    static const Real32 NEW_PATIENT_PERCENT_SUPP_PAV;
    static const Real32 NEW_PATIENT_PERCENT_SUPP_NON_PAV;
   
    // Soft and  hard bounds for percent support setting in PA mode
    
    static const Real32 MAX_SOFT_PERCENT_SUPP_PAV;
    static const Real32 ABS_MAX_PERCENT_SUPP_PAV;
   
    // Min percent support value for ATC mode
   
    static const Real32 ABS_MIN_PERCENT_SUPP_ATC;
    
};


//=====================================================================
//
//	WARNING:
//
//  	The following constants must be defined using #define
//  	instead of static const since these values are used to
//  	initialize const structs. The MRI compiler does not 
//  	always resolve "static const" inside const structs at
//  	compile time when using "static const" and creates a static
//  	initializer that attempts to initialize the struct located
//  	in const memory which results in an access fault attempting
//  	to write to read-only memory. Using #define is a workaround 
//  	for this problem.
//
//=====================================================================

// any obscure -- NOT within legal ranges of any setting -- positive
// value will do...  
#define DEFINED_UPPER_ALARM_LIMIT_OFF  Real32(1000000.0f)

// any obscure -- NOT within legal ranges of any setting -- negative
// value will do...
#define DEFINED_LOWER_ALARM_LIMIT_OFF  Real32(-1000.0f)

// $[NE02000] minimum tidal volume for neonates
#define DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_NON_VC_PLUS (3.0f)

#define DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS (2.0f)

#define DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC (5.0f)

#define DEFINED_TIDAL_VOL_NEONATAL_MIN_SQUARE_FLOW_PATTERN_VALUE (3.4f)

#endif // SettingConstants_HH 
