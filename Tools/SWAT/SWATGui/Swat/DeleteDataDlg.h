#pragma once
#include "afxwin.h"
#include "resource.h"


// CDeleteDataDlg dialog

class CDeleteDataDlg : public CDialog
{
	DECLARE_DYNAMIC(CDeleteDataDlg)

public:
	CDeleteDataDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDeleteDataDlg();

// Dialog Data
	enum { IDD = IDD_DELETE_DATA };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	

	int m_nTimeMinutes;
	virtual BOOL OnInitDialog();
};
