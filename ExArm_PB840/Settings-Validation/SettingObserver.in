
#ifndef SettingObserver_IN
#define SettingObserver_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class:  SettingObserver - SettingObserver Class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingObserver.inv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//=====================================================================

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//================= M E T H O D   D E S C R I P T I O N ===============
//@ Method:  getSubjectPtr_(subjectId)  [const]
//
//@ Interface-Description
//  Return a pointer to this observer's subject, that is identified by
//  'subjectId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline SettingSubject*
SettingObserver::getSubjectPtr_(
			  const SettingId::SettingIdType subjectId
			       ) const
{
  CALL_TRACE("getSubjectPtr_(subjectId)");
  return(SettingsMgr::GetSettingSubjectPtr(subjectId));
}  // $[TI1]


#endif // SettingObserver_IN 
