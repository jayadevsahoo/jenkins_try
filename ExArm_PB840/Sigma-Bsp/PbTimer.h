#ifndef _PbTimer_H_
#define _PbTimer_H_
 
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1998, Puritan-Bennett Corporation
//=====================================================================
 
 
//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: PbTimer.h - VRTX Timer Device Driver for PB FPGA Timers
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Sigma-Bsp/vcssrc/PbTimer.h_v   25.0.4.0   19 Nov 2013 14:29:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist  Date: 15-DEC-1998  DR Number: 5311
//    Project: 840 Cost Reduction
//    Description:
//       Initial version
//
//=====================================================================

//=== MICROTEC COMMENT ================================================
//
//**************************************************************************
//
//		Copyright (c) 1993-1996 MICROTEC RESEARCH INCORPORATED.
//
//	All rights reserved. MICROTEC RESEARCH's source code is an unpublished
//	work and the use of a copyright notice does not imply otherwise.
//	This source code contains confidential, trade secret material of
//	MICROTEC RESEARCH. Any attempt or participation in deciphering,
//	decoding, reverse engineering or in any way altering the source code
//	is strictly prohibited, unless the prior written consent of
//	MICROTEC RESEARCH is obtained.
//
//**************************************************************************


//**************************************************************************
// tiform.h                                                             
//                                                                           
// History:
//
//    Date        Name              Description
//    ---------------------------------------------------------------------- 
//    08-07-92    Donn Rochette     First Version, tiform.c
//    09-09-92    Roger Cole        Reformatted file, renamed to tiform
//    09-09-92    Roger Cole        Converted into a template file for all
//                                  timers
//
//**************************************************************************

//=== ORIGINAL MICROTEC COMMENT =======================================

#include "timer.h"

// register addresses for the timer 

typedef struct
{
  // define timer device registers here 
  volatile unsigned long *pTimerPreloadReg;
  volatile unsigned long *pTimerCountReg;
  volatile unsigned char *pTimerControlReg;
} PbTimer_reg_map_t;

typedef struct 
{
  // define driver specific variables here 
  volatile char shadowIntReg;
} PbTimer_specific_t;

// timer device descriptor 

typedef struct 
{
  timer_config_t     base_config;
  extern_fops_t      externio;
  PbTimer_reg_map_t  regs;
  PbTimer_specific_t specific_cfg;
} PbTimer_config_t;

// device specific definitions

#define PBTIMER_CLK          (8000000)
#define PBTIMER_NS_PER_CLK   (1e9/PBTIMER_CLK)
#define PBTIMER_EN           (0x01)

// function prototypes 

extern vbsp_return_t 
PbTimer_install(PbTimer_config_t *pCfg, timer_dev_desc_t *pDev);

extern vbsp_return_t 
PbTimer_init(timer_dev_desc_t *pDev);
 
extern vbsp_return_t 
PbTimer_disable(timer_dev_desc_t *pDev);
 
extern vbsp_return_t 
PbTimer_enable(timer_dev_desc_t *pDev);
 
extern vbsp_return_t 
PbTimer_interrupt_status(timer_dev_desc_t *pDev, int *pStatus);

extern vbsp_return_t 
PbTimer_status(timer_dev_desc_t *pDev, int *pStatus);

extern vbsp_return_t 
PbTimer_set_ns_per_count(timer_dev_desc_t *pDev, unsigned long *pNsPerCount);

extern vbsp_return_t 
PbTimer_get_ns_per_count(timer_dev_desc_t *pDev, unsigned long *pNsPerCount);

extern vbsp_return_t 
PbTimer_set_rollover(timer_dev_desc_t *pDev, unsigned long *pRollover);

extern vbsp_return_t 
PbTimer_get_rollover(timer_dev_desc_t *pDev, unsigned long *pRollover);

extern vbsp_return_t 
PbTimer_get_max( timer_dev_desc_t *pDev, unsigned long *pMax);

extern vbsp_return_t 
PbTimer_read_timer(timer_dev_desc_t *pDev, unsigned long *pCount);

extern vbsp_return_t 
PbTimer_set_timer(timer_dev_desc_t *pDev, unsigned long *pCount);

extern int 
PbTimer_test(logio_interrupt_entry_t *pEntry);


#endif 
