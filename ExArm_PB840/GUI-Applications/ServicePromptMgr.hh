#ifndef ServicePromptMgr_HH
#define ServicePromptMgr_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServicePromptMgr - The central control for all Service prompt area
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServicePromptMgr.hhv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  29-AUG-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


//@ Usage-Classes
#include "PromptArea.hh"
#include "GuiAppClassIds.hh"
class GuiStateTarget;
//@ End-Usage


class ServicePromptMgr
{
public:
	ServicePromptMgr(void);	
	virtual ~ServicePromptMgr(void);
	
	void setTestPrompt(PromptArea::PromptType promptType, PromptArea::PromptPriority priority,
									StringId stringId);
	void restoreTestPrompt(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	ServicePromptMgr(const ServicePromptMgr&);	// not implemented...
	void operator=(const ServicePromptMgr&);		// not implemented...

	//@ Data-Member: stringIds_
	// A two-dimensional array storing the string ids of each different
	// prompt type for each prompt priority.
	StringId stringIds_[PromptArea::PA_NUMBER_OF_PRIORITIES]
						[PromptArea::PA_NUMBER_OF_TYPES];
};


#endif // ServicePromptMgr_HH 
