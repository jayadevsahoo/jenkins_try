#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TextField - This class defines objects which can appear as character
// strings on the screen.
//---------------------------------------------------------------------
//@ Interface-Description
// The TextField class manages the display of text on the screen.  The
// application code specifies a string of text at construction time.  The class
// provides methods allowing the application to change the text and the
// text color.  The TextField constructor also provides the capability to
// initialize a NULL message string for application code to generate array of
// TextField.
//
// The text string is expected to contain embedded formatting information,
// using a protocol described in the Sigma GUI Foundation Software Design
// Specification.  The formatting embedded in this string can specify font
// sizes and styles.  It also can specify where to render the text within its
// screen area.  This format is called the "Message String" format.
//
// The TextField class is derived from the Drawable class, and thus is "aware"
// of the screen management framework.
//
// Even though this is a Drawable, its width and height should never be set
// explicitly.  They are calculated from the screen area which will be used
// when the current text string is rendered.  The width and height are
// automatically updated when the text string is changed.  To prevent this from
// being done inadvertently, the setWidth() and setHeight() methods inherited
// from Drawable are redefined in this class; if called, they generate an error
// via a CLASS_ASSERTION.
//
// This class does not support automatic wrapping of text into multiple lines.
// Instead, it relies on the embedded formatting to specify explicit position
// information for the start of each line.
//
// To allow applications to accurately determine the absolute height of a
// cheap text string, i.e. the number of pixels counting from the uppermost
// drawn pixel of the text to the lowermost drawn pixel, there is a method
// provided called getUnusedVerticalHeight().  This returns the number of
// pixels of vertical white space at the top of the TextField.  Substracting
// this number from the value returned from getHeight() will give the
// absolute height of the cheap text.
//---------------------------------------------------------------------
//@ Rationale
// This class was created to satisfy the need for an object to represent text
// on the screen.  Keeping the formatting information embedded in the text
// allows convenient substitution of strings for different languages.  This
// avoids the need for code changes when text position and size must be "tuned"
// to get a piece of text to fit on the screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// At construction time, the application specifies a pointer to the text to be
// displayed.  The setText() method can be used later to specify a new pointer;
// the constructor simply calls setText(), passing the pointer as a parameter.
// The setText() method copies the supplied text into the private string_[]
// array.
//
// Whenever the text to be displayed is changed, the private updateArea()
// method is called.  This method calls the private renderText_() method with
// the setArea flag set to true - this sets the area_ member but does not draw
// anything on the screen.  The area_ member variable is inherited from
// ancestor class Drawable.
//
// TextField accepts calls to its draw() method from the screen management
// framework.  Changes to a TextField will not result in changes on the
// screen until the application calls the BaseContainer::repaint() method,
// signaling the framework to update the screen.
//
// TextField::draw() is responsible for rendering the text onto the screen. It
// does this by calling the private renderText_() method (with the "setArea"
// parameter set to FALSE).  This method parses through the text string,
// extracting and applying the embedded formatting information.  It calls the
// low-level VGA routine VgaFont::TextOut() to render the text on the screen.
// It changes screen position and font based on embedded commands.
//
// The "Message String" format provides the ability to embed "substrings" in
// the string to be rendered.  These are enclosed in braces.  Format commands
// included in a substring only apply within the substring.  Substrings may be
// embedded within substrings. The renderText_() method deals with substrings
// by calling itself recursively when they are encountered.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/TextField.ccv   25.0.4.0   19 Nov 2013 14:11:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date: 18-Aug-2009    SCR Number: 6147
//  Project:  XB
//  Description:
//      Modified to allow for processing of square brackets inside text 
// 		strings used in syslog message strings.
//
//  Revision: 010   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 009   By: gdc    Date:  01-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Implemented positionInContainer for specialized positioning of 
//  text fields. Added TEXT_FIELD_ASSERTION macros to dump the text
//  string prior to asserting using the normal CLASS_ASSERTION macros.
// 
//  Revision: 008   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 020   By: gdc    Date:  06-Dec-2006    SCR Number: 6237
//  Project:  Trend
//  Description:
//	Reset origin of TextField if either vertices if negative when
// 	entering renderText_. Area accumulation does not work properly
// 	when either TextField Area's coordinates are negative.
// 	Area formed during renderText_ now self corrects to avoid 
// 	assertion. Added TextField error logging capabilities.
//
//  Revision: 019   By: B. Kuznetsov  Date:  05-May-2003    SCR Number: 6036 
//  Project: Russian
//  Description:
//  Changed the position of the function calling getAlternateFontId 
//	to get alternative font 
//
//  Revision: 018   By: S. Peters    Date:  16-Oct-2002    DCS Number: 6029 
//  Project: Polish
//  Description:
//    Added some statement to take care of Alternate or Polish languages.
//    The added code segment looks for the A as format specifier. 
//    Just before the character is printed out it switches to the alternate 
//    character matrix.  
//
//  Revision: 017   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 016   By: sah    Date:  15-Jun-2000    DCS Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  removed assertion in 'renderText()' to allow for better 
//         debugging of ill-formatted messages
//
//  Revision: 015   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 014   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 013  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color 
//  Description:
//      Initial release.
//
//  Revision: 012  By:  clw    Date:  28-May-1998    DCS Number:
//  Project:  Japanese
//  Description:
//      Updated renderText to auto-correct text-too-high condition.
//      Updated subscriptDrop method to support 16-point text size.
//      Added support for new Japanese style.  This implements a
//      new cheap-text operator "J".
//
//  Revision: 011  By:  sah    Date:  15-Jan-1998    DCS Number: 5004
//  Project:  Sigma (R8027)
//  Description:
//      No longer need run-time processing of symbols; now done at
//      compile time.
//
//  Revision: 010  By:  sah    Date:  27-Oct-1997    DCS Number: 2581
//  Project:  Sigma (R8027)
//  Description:
//      Changed two 'static' arrays in 'renderText_()' to stack-based
//		variables, to eliminate a multiple-thread contention.  This
//		affects the stack usage for both the GUI Apps Task and the GUI
//		Bar Task.
//
//  Revision: 009  By: sah       Date: 06-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid, by removing all semaphore requests/releases from
//      all of the 'draw()' methods.
//
//  Revision: 008  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 007  By: clw      Date: 28-Apr-1997   DCR Number: <none>
//    Project:  Sigma (R8027)
//    Description:
//      Added missing Testable Item markers, removed one that was in SUN prototype code
//      This was done as part of the prep for peer inspection, and update of unit test
//
//  Revision: 006  By: gdc      Date: 04-May-1996   DCR Number: 981
//    Project:  Sigma (R8027)
//    Description:
//      Correct minor syntax errors found by new compiler
//
//  Revision: 005  By: mpm      Date: 29-Mar-1996   DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added calls to GuiFoundation::RequestDisplayAccess() and ReleaseDisplayAccess()
//      in order to allow shared access to the display by separate drawing tasks.
//
//  Revision: 004  By: mpm      Date: 06-Dec-1995   DCR Number: 548
//    Project:  Sigma (R8027)
//    Description:
//      Fixed bug where empty cheap text string, e.g. "{p=12,N:}", wasn't setting
//      the unusedVerticalHeight_ variable correctly.
//
//  Revision: 003  By: mpm      Date: 25-Aug-1995   DCR Number: 548
//    Project:  Sigma (R8027)
//    Description:
//      Added the getUnusedVerticalHeight() feature.
//
//  Revision: 002  By: yyy      Date: 25-Aug-1995   DCR Number: 1962
//    Project:  Sigma (R8027)
//    Description:
//      Correct error in getValue_() that caused failure when embedded
//      number had leading zero.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "TextField.hh"
#include <string.h>

//@ Usage-Classes
#include "BaseContainer.hh"
#include "TextFont.hh"
#include "DisplayContext.hh"
#include "GraphicsContext.hh"
#include "VgaRectangle.hh"
#include "VgaFontData.hh"

#include "VgaFontData.hh"    //used for VgaFontData::getAlternateFontId


//@ End-Usage

//@ Macro:  TEXT_FIELD_ASSERTION_FAILURE()
// This macro provides a forced assertion identified for the TextField class.
#define TEXT_FIELD_ASSERTION_FAILURE()	\
{	\
	dumpTextString_(); \
	CLASS_ASSERTION_FAILURE(); \
}

//@ Macro:  AUX_TEXT_FIELD_ASSERTION_FAILURE(auxErrorCode)
// This macro provides a forced assertion identified for the TextField class.
#define AUX_TEXT_FIELD_ASSERTION_FAILURE(auxErrorCode)	\
{	\
	dumpTextString_(); \
	AUX_CLASS_ASSERTION_FAILURE(auxErrorCode); \
}

//@ Macro:  TEXT_FIELD_ASSERTION(boolExpr)
// This macro provides a test of the assertion identified by 'boolExpr' for
// the TextField class.
#define TEXT_FIELD_ASSERTION(boolExpr)	\
{	\
	if (!(boolExpr))	\
	{	\
		dumpTextString_(); \
		CLASS_ASSERTION(boolExpr); \
	}	\
}

//@ Code...
//
// Initialization of static data members...
//

// This is the greatest depth of recursion allowed in text rendering.
const Int TextField::MAX_RECURSION_DEPTH = 20;

// This is the greatest offset allowed to be able to derivated from the
// VGA screen coordinate.
const Int TextField::MAX_VGA_OFFSET = 100;


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TextField
//
//@ Interface-Description
// This constructor initializes the TextField object.  The passed "pString"
// pointer must point to the text to be displayed, or it must be NULL.  The
// text to be displayed (if any) must be in the "Message String" format.  This
// text is copied into internal private storage.  The size of the TextField is
// automatically set to just enclose the specified text.
//---------------------------------------------------------------------
//@ Implementation-Description
// The passed "pString" pointer is passed to the setText() method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
TextField::TextField(const wchar_t* pString)
:	unusedVerticalHeight_(0),
	baselineFontSize_(0)
{
	CALL_TRACE("TextField::TextField(const wchar_t *pString)");
	wmemset(string_,L'\0',MAX_STRING_LENGTH+1);
	setText(pString);									// $[TI1]
}

	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TextField
//
//@ Interface-Description
// This is a default constructor with no argument passed.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
// It serves as the default constructor that initializes a NULL message string.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
TextField::TextField(void)
:	unusedVerticalHeight_(0),
	baselineFontSize_(0)
{
	CALL_TRACE("TextField::TextField(void)");
	wmemset(string_,L'\0',MAX_STRING_LENGTH+1);
	setText(NULL);									// $[TI2]
}

	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TextField() 
//
//@ Interface-Description
// Destructor. Destroys object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TextField::~TextField(void)
{
	CALL_TRACE("~TextField()");
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setText
//
//@ Interface-Description
// Changes the text to be displayed.  The passed "pString" pointer must point
// to the text to be displayed, or it must be NULL.  The text to be displayed
// (if any) must be in the "Message String" format.  This text is copied into
// internal private storage.  The size of the TextField is automatically set to
// just enclose the specified text.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the passed "pString" is NULL, sets the private string_ array to a null
// string.  Otherwise, the text pointed to by pString is copied into the
// string_ array until a null character is encountered.  This change may affect
// the state of the screen, so the changing() method is called to force this
// object to be redrawn at the next repaint.  Since the size of the TextField
// is based on its content, and the content may have changed, the updateArea_()
// method is called to recalculate the TextField's area_.
//---------------------------------------------------------------------
//@ PreCondition
//	"pString" must be NULL, or it must point to a text string of length
// less than or equal to MAX_STRING_LENGTH.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
TextField::setText(const wchar_t* pString)
{
	CALL_TRACE("TextField::setText(const char * pString)");

	// pre-condition checks
	if (!((pString == NULL) || (wcslen(pString) <= MAX_STRING_LENGTH)))
	{
		// dump the first 80 characters of the string parameter and assert
#if 0
		char tmpBuffer[80];
		cout << "invalid TextField: " << strncpy_s(tmpBuffer, pString, sizeof(tmpBuffer)-1) << endl;
#endif
		CLASS_PRE_CONDITION((pString == NULL) || (wcslen(pString) <= MAX_STRING_LENGTH));
	}

	if (pString != NULL)
	{	//													$[TI1]
		wcscpy_s(string_,MAX_STRING_LENGTH, pString); 

	}
	else
	{	//													$[TI2]	
		string_[0] = L'\0';
	}

	changing();		// Since this saves the old area - must call
					// BEFORE area is changed.
	updateArea_();	// Updates area of field based on contained text.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHeight	[virtual]
//
//@ Interface-Description
// Redefines the inherited Drawable::setHeight() method to generate an error.
// The passed parameter is ignored.
// Since the area occupied by a TextField is fully determined by its text,
// based on the height and width needed to render the text, this class has
// taken control of the height and width of the inherited area.  If the
// application was allowed to change it, it could be changed into an illegal
// state.
//---------------------------------------------------------------------
//@ Implementation-Description
// This redefinition effectively prevents the application from changing the
// height of the inherited screen area.
//
// An error is generated via the TEXT_FIELD_ASSERTION_FAILURE macro.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
TextField::setHeight(Int)
{
	CALL_TRACE("TextField::setHeight(Int height)");

	TEXT_FIELD_ASSERTION_FAILURE();
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setWidth	[virtual]
//
//@ Interface-Description
// Redefines the inherited Drawable::setWidth() method to generate an error.
// The passed parameter is ignored.
// Since the area occupied by a TextField is fully determined by its text,
// based on the height and width needed to render the text, this class has
// taken control of the height and width of the inherited area.  If the
// application was allowed to change it, it could be changed into an illegal
// state.
//---------------------------------------------------------------------
//@ Implementation-Description
// This redefinition effectively prevents the application from changing the
// width of the inherited screen area.
//
// An error is generated via the TEXT_FIELD_ASSERTION_FAILURE macro.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
TextField::setWidth(Int)
{
	CALL_TRACE("TextField::setWidth(Int width)");

	TEXT_FIELD_ASSERTION_FAILURE();
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: draw	[virtual]
//
//@ Interface-Description
// This draw routine renders the text on the VGA display.  It overrides the
// pure virtual method inherited from Drawable.  The passed clipArea parameter
// is ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method calls the renderText_() method to render the string stored
// in the private string_ array (if any).  This call is the first of what 
// may be a series of recursive calls.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextField::draw(const Area &)
{
	CALL_TRACE("TextField::draw(const Area &)");

	//													$[TI1]
	// The following call renders the string_ and updates the display.
	validateAndRenderTextField_(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: positionInContainer [virtual]
//
//@ Interface-Description
//  Positions the TextField in its parent container as specified by the
//  gravity parameter. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Repositions the object in its parent container using setX/setY.
//  Text is centered vertically such that the character baseline to the 
//  top of the character is centered and the descender is positioned 
//  below the baseline. The baseline is determined when the text is
//  rendered. The first font size encountered in the cheap text string
//  is considered the Y distance from the top of the character to its 
//  base.
//---------------------------------------------------------------------
//@ PreCondition
//  pParentContainer_ not NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
TextField::positionInContainer(const Gravity gravity)
{
	CALL_TRACE("positionInContainer()");

	// a hard-coded margin is not optimal, but matches the pre-Trending implementation
	// this could be enhanced with a parameterized margin to allow positioning of
	// the text field such that the descender is never positioned below the container
	static const Int BOTTOM_MARGIN = 3;
	Container* pParentContainer = getParentContainer();

	TEXT_FIELD_ASSERTION( pParentContainer );

	// ascent is always based on the "normal" font style so all font
	// styles will be positioned similarly
	Int ascent = 0;
	Int maxAscent = 0;

	if (baselineFontSize_ != 0)
	{
	
		ascent = TextFont::GetAscent(TextFont::Size(baselineFontSize_), TextFont::NORMAL);
		maxAscent = TextFont::GetMaxAscent(TextFont::Size(baselineFontSize_), TextFont::NORMAL);

	}

	switch (gravity)
	{
	case ::GRAVITY_LEFT :
	case ::GRAVITY_CENTER :
	case ::GRAVITY_RIGHT :
		// the base class can handle horizontal justification
		Drawable::positionInContainer(gravity);
		break;
	case ::GRAVITY_TOP :
		setY(0);
		break;
	case ::GRAVITY_VERTICAL_CENTER :
		// Text is centered vertically such that the character baseline to the 
		// top of a normal sized character is centered and the descender is positioned 
		// below the baseline. Since the baseline is positioned maxAscent pixels from
		// the top of the character, we move the character up by the difference of
		// the maximum ascent and the normal ascent. The client provides a large
		// enough container to accomodate the max ascent plus descent plus some margin.
		setY( (pParentContainer->getHeight() - ascent) / 2 - (maxAscent - ascent));
		break;

	case ::GRAVITY_BOTTOM :
		// a hard-coded margin is not optimal, but matches the pre-Trending implementation
		// this could be enhanced with a parameterized margin to allow positioning of
		// the text field such that the descender is never positioned below the container
		{
		static const Int BOTTOM_MARGIN = 3;
		setY( pParentContainer->getHeight() - maxAscent - BOTTOM_MARGIN);
		}
		break;

	default :
		// unexpected 'gravity' value...
		AUX_TEXT_FIELD_ASSERTION_FAILURE( gravity );
		break;
	};

	// negative origins which are not allowed are corrected during sizing and rendering
}


//=====================================================================
//
// Private methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: validateAndRenderTextField_
//
//@ Interface-Description
// This routine renders the text on the VGA display when the "setArea" is set
// to FALSE.  Or this routine sets the area_() member of Drawable to
// surround the text that would be rendered when the "setArea" is set to TRUE.
//---------------------------------------------------------------------
//@ Implementation-Description
// This function uses renderText_() in the setArea mode to set the area_.
// The text in the string_ array must be in the "Message String" format (see
// GuiFoundation SDS for reference".
// Note that the closing brace is requred to be the *last* character in
// the string.  This is done to help find mismatched braces.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
TextField::validateAndRenderTextField_(Boolean setArea)
{
	CALL_TRACE("TextField::validateAndRenderTextField_(Boolean setArea)");
	
	Int startX = 0;
	Int startY = 0;
	Int startIndex = 0;

	// The following call updates the area_ member of drawable.
	if (string_[0] != L'\0')
	{	//												$[TI1]
		// The first character of any renderable string must be an open brace.
		TEXT_FIELD_ASSERTION( string_[0] == L'{');

		Int len = wcslen(string_);	// get the string length.
		TEXT_FIELD_ASSERTION( len <= MAX_STRING_LENGTH);
		
		// The last character of any renderable string must be a close brace.
		TEXT_FIELD_ASSERTION( string_[len-1] == L'}');

		// Initialize the unused vertical height to the maximum height of
		// a cheap text string.  This value will be reduced to the correct
		// value during renderText_.
		unusedVerticalHeight_ = GuiFoundation::SCREEN_HEIGHT_PIXELS + 2 * TextField::MAX_VGA_OFFSET;

		// initialize baseline font size that will be updated when a size
		// is encountered in the cheap text string
		baselineFontSize_ = 0;

		// If setArea is TRUE, then update the area_.
		// The last (7th) parameter (TRUE) causes this call to update the area
		// without rendering.
		// If setArea is FALSE, the draw the text string_.
		// The last (7th) parameter (FALSE) causes this call to render the
		// string_ and draw to the display.
		// 
		renderText_(string_, startIndex, startX, startY, getColor(), 0, setArea);

		// Check to see if unusedVerticalHeight_ was modified.  If not then
		// set it to zero.  This happens, for example, when the cheap text is
		// empty, e.g. "{p=12,y=12:}".
		if (unusedVerticalHeight_ > area_.getHeight())
		{	//											$[TI1.1]
			unusedVerticalHeight_ = 0;
		}	//											$[TI1.2]

		// The closing brace should be the last character in the string;
		// otherwise, we might have unmatched braces.
		TEXT_FIELD_ASSERTION( string_[startIndex+1] == L'\0');
	
    }
	else
	{	//												$[TI2]
		// No unused vertical height for an empty string.
		unusedVerticalHeight_ = 0;
		baselineFontSize_ = 0;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: renderText_
//
//@ Interface-Description
// This recursive function takes the currrent position in the string and
// renders the substrings at this level. If it finds an open curly brace then
// this functon is called recursively to handle the next level of nesting.
//
// >Von
// Summary of passed parameters:
//  buffer[]	Contains string to render.
//  rBufIndex	index to current position in buffer.
//  rX			X position within field where next char will be rendered.
//					Gets bigger as each char is rendered, left-to-right.
//					This is a reference to an external variable - the
//					value is preserved from the last call.
//  rY			Y position within field where next char will be rendered.
//					Stays fixed unless explicitly chenged.
//					This is a reference to an external variable - the
//					value is preserved from the last call.
//  color		color of rendered text.
//  yOffset		vert shift; used for subscripts, etc. Reverts after this call.
//  setArea		TRUE=don't render, just set area_; FALSE=the reverse (default).
//  size		Initial point size, default = NO_SIZE for the outer call
//					(overridden by embedded	"p=<size>" command).
//  style		Normal (default), italic, or "THIN" (less bold than normal), or Japanese.
// >Voff
//
// This function operates in two modes. One mode is set by having the "setArea"
// parameter equal to false. In that mode the text is rendered. If setArea is
// true then the purpose of this function is not to display the text but to
// figure out how much space the text would take up if it were displayed.  In
// this mode, the area_ member inherited from Drawable is set.  This is
// required so the area can be tracked for repainting later.  NOTE: in this
// mode, the area_'s width and height should always be set to the smallest
// possible in the outer call before this method is called.
//
// This method requires the supplied text to conform to the "Message String"
// format described in the GUI Foundations SDS document.  This format comprises
// an outer set of open and close braces {}, containing a string which includes
// 2 sections separated by a colon character.  The section before (and
// including) the "control section" contains commands which specify the point
// size, style, position, and color of the section that follows the colon the
// "text section".  The text to the right of the colon is the text to be
// rendered.  It may include substrings enclosed in braces; these also must
// contain a control section and a text section, as described above.  Recursive
// calls allow braced substrings to be nested as deeply as needed. 
//
// In addition, special characters can be specified by including a single
// backslash followed by an octal number representing the appropriate ASCII
// code.  The C++ pre-processor will convert the octal code into the
// corresponding ASCII code.  These are used mainly in the pre-defined symbol
// substrings.
//
// The above method can't be used to insert brace or square bracket characters
// "[, ], {, or }" into the text of a message, because this would not prevent
// the renderText_() method from treating them as braces and brackets.
// Therefore, renderText_() provides a way to remove special syntactical
// meaning from any character.  It simply accepts any character following a
// backslash and immediately places it into the output string.  Note that the
// user must use TWO backslash characters, because the C++ pre-processor will
// remove one of them.
//
// A point size other than NO_SIZE must be specified before the first
// colon using the p=size command or the passed size the first time this method
// is called; if none is found, an error will result.
//
// It is assumed that the caller of this method (not counting recursive
// calls) has verified that the length of the string is <= MAX_STRING_LENGTH
// and that the first and last characters are open and close braces.
// Upon return from the outer call to this routine, the caller should
// verify that the closing brace (matching the open brace) is the last
// character in the string; otherwise, there might be mismatched braces.
//
// This method uses an internal static variable to monitor the depth of
// recurion; for this reason, it should not be called reentrantly.
//---------------------------------------------------------------------
//@ Implementation-Description
// This function reads the control sequence (before the colon) to determine the
// font to use, the color, and the position to start rendering.  It then
// displays the string as far as the next open or closed curly brace.  If it
// reaches a close curley brace it returns.  If it reaches an open curley
// brace then it calls itself recursively to render the sub-string.  After
// the recursive call returns then the current level renders the string from
// the current position in the string to the next curley brace, and so on
// until a close curley brace is reached.
//
// SPECIAL NOTES FOR JAPANESE:
// To support Japanese, an additional style has been added. This is turned on
// by the cheap text format specifier "J".  While in this style, the ASCII chars
// from the string to be rendered are used to index the Japanese font for the given
// point size, where the corresponding Japanese characters are stored.  To allow
// more than 256 characters, a system of "escape characters" has been implemented.
// The most common Japanese characters will be packed into the "primary" font for the
// specified point size; less common characters will be preceded by one of three
// possible escape characters, each diverting the font id to a different alternate
// font set for that point size.  The number of font sets per point size is set in
// the VGA-Graphics-Driver subsystem class VgaFontData.hh.  The conversion of
// translator-supplied Japanese strings into the correct ASCII mappings (possibly
// with escape characters) is handled by a seperate tool.  This tool set also
// generates the Japanese fonts.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void 
TextField::renderText_(
			const wchar_t buffer[], Int &rBufIndex, Int &rX,
			Int &rY, Colors::ColorS color, Int yOffset, Boolean setArea, 
			Int size, TextFont::Style style)
{
	CALL_TRACE("TextField::renderText_(const char buffer[], Int &rBufIndex, "
			"Int &rX, Int &rY, Colors::ColorS color, "
			"Int yOffset, Boolean setArea, Int size, TextFont::Style style)");
	
	// area accumulation does not work with negative area origins so reset them here
	if (getX() < 0)
	{
#if 0
		{
			cout << "TextField : this=" << hex << Uint(this) << endl;
			cout << "          : negative X origin reset" << dec << endl;
			cout << "          : x=" << getX() << endl;
			cout << "          : string_=" << string_ << endl;
		}
#endif
		setX(0);
	}
	if (getY() < 0)
	{
#if 0
		{
			cout << "TextField : this=" << hex << Uint(this) << endl;
			cout << "          : negative Y origin reset" << dec << endl;
			cout << "          : y=" << getY() << endl;
			cout << "          : string_=" << string_ << endl;
		}
#endif
		setY(0);
	}

	Boolean alternateChar = FALSE;  //a flag to determine to switch to the Alternate

	FontID font = TextFont::INVALID_FONT_ID;	// no font is specified yet
	
	if (size != Int(TextFont::NO_SIZE))
	{	//												$[TI1]
		// Determine the font, based on the specified point size and style.
		font = TextFont::GetFont((TextFont::Size) size, style);
		TEXT_FIELD_ASSERTION(font != TextFont::INVALID_FONT_ID);
	}
	//													$[TI2]

	rBufIndex++; // skip opening brace character

	// Save current size and style - this will be used below
	//  to check for size or style change (font change).
	Int oldSize = size;
	TextFont::Style oldStyle = style;

	// loop thru command portion of string (text preceding the ':'),
	//  extracting formatting data.  This consists of pairs of single-char
	//  commands followed by numbers.
	
	const Int MAX_X = GuiFoundation::SCREEN_WIDTH_PIXELS +
						TextField::MAX_VGA_OFFSET;
	const Int MIN_X = -TextField::MAX_VGA_OFFSET;
	const Int MAX_Y = GuiFoundation::SCREEN_HEIGHT_PIXELS +
						TextField::MAX_VGA_OFFSET;
	const Int MIN_Y = -TextField::MAX_VGA_OFFSET;

	const Int MAX_REL_X = GuiFoundation::SCREEN_WIDTH_PIXELS +
							TextField::MAX_VGA_OFFSET;
	const Int MIN_REL_X = -MAX_REL_X;
	
	const Int MAX_REL_Y = GuiFoundation::SCREEN_HEIGHT_PIXELS +
							TextField::MAX_VGA_OFFSET;
	
	const Int MIN_REL_Y = -MAX_REL_Y;
	while (buffer[rBufIndex] != L':')
	{	//												$[TI3]
		TEXT_FIELD_ASSERTION(buffer[rBufIndex] != L'\0');

 		wchar_t command = buffer[rBufIndex];
		
		rBufIndex ++;

		TEXT_FIELD_ASSERTION(rBufIndex <= MAX_STRING_LENGTH);
		TEXT_FIELD_ASSERTION(buffer[rBufIndex] != L'\0');

		// Now process command:
		switch (command)
		{											// $[TI3.1]
		case L' ':									// $[TI3.1.1]
		case L',':									// $[TI3.1.2]
			break;
			
		case L'p':	// "Point size": sets text height.
													// $[TI3.1.3]
			size = getValue_(buffer, rBufIndex,
								(Int)TextFont::MIN_SIZE,
								(Int)TextFont::MAX_SIZE );
			break;
			
		case L'c' :	// Sets text color.  See Colors class for codes.
													// $[TI3.1.4]
			color = (Colors::ColorS)getValue_(buffer, rBufIndex, 0,
											((Int)Colors::NUM_COLORS)-1 );
			break;
			
		case L'x' :	// Sets absolute x text position within parent Container.
					// This value persists after this renderText() call
					// returns, since rX is a reference.
													// $[TI3.1.5]
			rX = getValue_(buffer, rBufIndex, MIN_X, MAX_X);
			break;
			
		case L'y' :	// sets absolute y text position within parent Container.
					// This value persists after this renderText() call
					// returns, since rY is a reference.
													// $[TI3.1.6]
			rY = getValue_(buffer, rBufIndex, MIN_Y, MAX_Y);
			break;
			
		case L'X' :	// sets relative x text position within parent Container.
					// This value persists after this renderText() call
					// returns, since rX is a reference.
													// $[TI3.1.7]
			rX += getValue_(buffer, rBufIndex, MIN_REL_X, MAX_REL_X);
			break;
			
		case L'Y' :	// sets relative y text position within parent Container.
					// This value persists after this renderText() call
					// returns, since rY is a reference.
													// $[TI3.1.8]
			rY += getValue_(buffer, rBufIndex, MIN_REL_Y, MAX_REL_Y);
			break;
			
		case L'o':	// Sets temporary y offset - reverts to previous settng
					// when this call of renderText() returns.
													// $[TI3.1.9]
			yOffset = getValue_(buffer, rBufIndex, MIN_REL_Y, MAX_REL_Y);
			break;
			
		case L'S':	// Subscript: sets text size and yOffset to values
					// appropriate for a subscript for the current text
					// size.
					// Note that these params are passed as references -
					// the following call changes them both.
													// $[TI3.1.10]
			subscriptDrop_(yOffset, size);
			break;

        case L'A':  //alternate characters such as Polish.  
                    // Note: can be combined with any other format specifiers N,I,T 
             alternateChar = TRUE;

             break;

		// Note: the following text styles are not combinable.
			
		case L'N':	// Sets "Normal" (bold) text style.
													// $[TI3.1.11]
			style = TextFont::NORMAL;
			break;
			
		case L'T':	// Sets "Thin" (not bold) text style.
													// $[TI3.1.12]
			style = TextFont::THIN;
			break;
			
		case L'I':	// Sets "Italic" text style.	// $[TI3.1.13]
			style = TextFont::ITALIC;
			break;
			
		case L'J':	// Sets "Japanese" text style.	// $[TI3.1.14]
			style = TextFont::JAPANESE;
			break;
			
		default :
			// THIS SHOULD NEVER HAPPEN, but if it did, rather than
			// assert, keep going, but change the color to a blinking
			// color to that the improperly-formatted message can be
			// easily spotted and corrected...
			color = Colors::WHITE_ON_BLACK_BLINK_FAST;
			break;
		}	// end switch
	}	// end while not ':'
													// $[TI4]
	// Size is initially set to TextFont::NO_SIZE for calls other than
	// recursive ones; if a command to set the size has not been encountered
	// prior to the first colon, an error is generated.

	TEXT_FIELD_ASSERTION(size != Int(TextFont::NO_SIZE));

	// If size or style has been changed, update the font:
	if (size != oldSize || style != oldStyle)
	{	//												$[TI5]
		// Determine the font, based on the specified point size and style.
        //  Note: For alternate font sets just use the 'regular  font set id.
        //  If it is an alternate id, then later the Id will be offset to alternate font set id.
		font = TextFont::GetFont((TextFont::Size) size, style);
		TEXT_FIELD_ASSERTION(font != TextFont::INVALID_FONT_ID);

		// if we don't have a baseline font size yet then set it to the current size
		if (baselineFontSize_ == 0)
		{
			baselineFontSize_ = size;
		}
	}
	//													$[TI6]
	


	rBufIndex++;   // skip the ':'
	TEXT_FIELD_ASSERTION(buffer[rBufIndex] != L'\0');

	// =========================================================
	// Process the string itself, now that the controls are done

	while (buffer[rBufIndex] != L'}')
	{	//												$[TI7]
		wchar_t subString[MAX_STRING_LENGTH+1];
		
		Int subStringIndex = 0;

		// Extract substring to be rendered, up to the next open or close
		// brace, or to the next open bracket:
		
		while 	(	(buffer[rBufIndex] != L'}') &&
					(buffer[rBufIndex] != L'{') &&
					(buffer[rBufIndex] != L'[')
				)
		{	//											$[TI7.1]	

			// If a backslash is encountered, grab the following character
			// regardless of what it is.  This removes any syntactical meaning
			// from the escaped character, allowing characters such as braces
			// and square brackets to be inserted in the message.
			// Note that the user must use TWO backslashes before the
			// character to be inserted, because the C++ pre-processor
			// will remove one of them.

			if (buffer[rBufIndex] == L'\134')	// is a backslash character
			{	//										$[TI7.1.1]
				rBufIndex ++;	// skip over the backslash
				TEXT_FIELD_ASSERTION(buffer[rBufIndex] != L'\0');

			}

			//											$[TI7.1.2]
			// An ASCII character is expected to follow the backslash.
			subString[subStringIndex] = buffer[rBufIndex];
			rBufIndex ++;
			TEXT_FIELD_ASSERTION(buffer[rBufIndex] != L'\0');
			
			subStringIndex ++;
		}
		//												$[TI7.2]

		subString[subStringIndex] = L'\0';	// Terminate the subString

		if (subString[0] != L'\0')
		{	//											$[TI7.3]
			// Determine position of text within area_.  This call sets
			//  Rect rect to the bounding box around the text for the substring.
			VgaRect rect;
			//TODO E600 - for Japanese
			//if (TextFont::JAPANESE == style)
			//{	//										$[TI7.3.10]
				// A special call is needed for Japanese due to the need to handle
				// alternate fonts (based on escape characters)
				/*VgaFont::TextRectJ(area_.getX1() + rX, area_.getY1() + rY+yOffset,
							  font, subString, &rect);*/ 
			//}
			//else
			{	//										$[TI7.3.5]
				if (TRUE == alternateChar)  // an alternate format specifier was found (e.g. Polish).
				{
					// Get the alternate font id
					font = VgaFontData::getAlternateFontId(font); 
				}
				VgaFontData::TextRect(area_.getX1() + rX, area_.getY1() + rY+yOffset, font, subString, &rect);

			}

			// The area occupied by the substring must not extend above the
			//  top of the area_.
			//  Note that y0 is the top edge for a rect, while y1 is the
			//  top edge for an Area.
            //  This y offset moves the text to a legal location.
			Int yDelta = 0;
            if (rect.y0 < area_.getY1())
            {	//										$[TI7.3.6]
                yDelta = area_.getY1() - rect.y0;  // move down to correct problem
            }	//										$[TI7.3.7]
 

			// If the starting X value of the text begins before the X
			// coordinate we requested (this happens if the first character
			// of the string contains a negative "hotspot" X) then shift
			// the text to the right the correct number of pixels so that
			// it begins drawing at the desired X coordinate. 'xDelta' is
			// the required offset.
			//											$[TI7.3.3], $[TI7.3.4]
			Int xDelta = (rect.x0 >= area_.getX1()) ? 0 :
													  area_.getX1() - rect.x0;

			if (setArea)
			{	//										$[TI7.3.1]
				// in setArea mode this function just accumulates the areas.
				Area a;

				Int  x0 = rect.x0 + xDelta;
				Int  y0 = rect.y0 + yDelta;
				Int  x1 = rect.x1 + xDelta;
				Int  y1 = rect.y1 + yDelta;

				if (x0 <= x1 && y0 <= y1)
				{
					a.set(x0, y0, x1, y1);
				}
				else
				{
#if 0
					{
						cout << "malformed area: this      = " << hex << Uint(this) << endl;
						cout << dec;
						cout << "              : buffer    = " << buffer << endl;
						cout << "              : rBufIndex = " << rBufIndex << endl;
						cout << "              : subString = " << subString << endl;
						cout << "              : font      = " << font << endl;
						cout << "              : size      = " << size << endl;
						cout << "              : style     = " << style << endl;
						cout << "              : rect.x0   = " << rect.x0 << endl;
						cout << "              : rect.y0   = " << rect.y0 << endl;
						cout << "              : rect.x1   = " << rect.x1 << endl;
						cout << "              : rect.y1   = " << rect.y1 << endl;
						cout << "              : area.x0   = " << area_.getX1() << endl;
						cout << "              : area.y0   = " << area_.getY1() << endl;
						cout << "              : area.x1   = " << area_.getX2() << endl;
						cout << "              : area.y1   = " << area_.getY2() << endl;
					}
#endif
					a.set(0, 0, 0, 0);
				}
				//Area a(rect.x0 + xDelta, rect.y0 + yDelta, rect.x1 + xDelta, rect.y1 + yDelta);

				// This updates the TextField's area_ (inherited from
				// Drawable).
				area_.makeMerge(a, area_);

				// See if the number of unused vertical pixels should be
				// decreased due to a substring being positioned higher up
				// in the TextField.
				if (rect.y0 - area_.getY1() < unusedVerticalHeight_)
				{	//									$[TI7.3.1.1]
					// New value for unusedVerticalHeight_
					unusedVerticalHeight_ = rect.y0 - area_.getY1();
				}	//									$[TI7.3.1.2]
			}
			else
			{	//										$[TI7.3.2]
				DisplayContext& rDc = getBaseContainer()->getDisplayContext();
				GraphicsContext& rGc = getBaseContainer()->getGraphicsContext();

				rGc.setForegroundColor( color );

				// TODO E600 - Render the substring on the screen.
				//if (TextFont::JAPANESE == style)
				//{	//										$[TI7.3.8]
					// A special call is needed for Japanese due to
					// the need to handle alternate font sets based on escape chars
				//	rDc.drawJapaneseText(rGc, getRealX() + rX + xDelta,
				//				 getRealY() + rY+yOffset+yDelta,
				//				 font, subString);
				//}
				//else
				{	//          $[TI7.3.9]
					RgbQuad rgb = Colors::toStandardColor(color);
					rDc.setTextColor(rgb);
					int subStringHeight = 0; //TODO E600 - Inorder to, revert to previous implementation- set its value to "0", need work on "rect.y1 - rect.y0;" formula.
                    rDc.drawText(rGc, getRealX() + rX + xDelta,
                                     getRealY() + rY+yOffset+yDelta-subStringHeight,
                                     font, subString);
				}
			}

			// Update the x position:
			rX += (rect.x1 - rect.x0) + 1;

		}	// end of if subString[0] != '\0'		// $[TI7.4]

		// ===================================================
		// Now that the substring has been rendered, check what caused the
		// substring to end - it might have encountered the start of
		// a nested substring.  This is handled by recursive
		// calls.  Or, it might have hit the end of the string ('}').

		// no longer support embedded symbol processing (DCS #5004)...
		TEXT_FIELD_ASSERTION((buffer[rBufIndex] != L'['));

		if (buffer[rBufIndex] == L'{')		// Check for nested substring:
		{	//											$[TI7.6]
			// Process the nested string as a substring, using recursive call:
			renderText_(buffer, rBufIndex, rX, rY, color, yOffset,
						setArea, size, style);
						
			TEXT_FIELD_ASSERTION(buffer[rBufIndex] == L'}');
			
			rBufIndex ++;
			TEXT_FIELD_ASSERTION(buffer[rBufIndex] != L'\0');
		}
		//												$[TI7.7]
	} // end while not '}'								// $[TI8]

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: subscriptDrop_
//
//@ Interface-Description
// This method adjusts the text point size and vertical offset to produce a
// subscript.  References to size and yOffset are passed in as parameters;
// based on the initial size, new values for both size and yOffset are
// computed.
//---------------------------------------------------------------------
//@ Implementation-Description
// This routine contains an internal table of subscript point sizes
// which correspond to each "normal", non-subscript point size.  The
// offset is simply half of the subscript point size.  Only certain
// non-subscript point sizes are supported -- attempts to use
// unsupported non-subscript point sizes results in an error.
//---------------------------------------------------------------------
//@ PreCondition
//  (rSize >= 0 && rSize < countof(SubscriptTable_))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TextField::subscriptDrop_(Int &rYOffset, Int &rSize)
{
	CALL_TRACE("TextField::subscriptDrop_(Int &rYOffset, Int &rSize)");

	static Int SubscriptTable_[] = { 
		0, // 0
		0, // 1
		0, // 2
		0, // 3
		0, // 4
		0, // 5
		6, // 6
		0, // 7
		6, // 8
		0, // 9
		6, // 10 
		0, // 11
		8, // 12
		0, // 13
		8, // 14
		0, // 15
		8, // 16
		0, // 17
		8, // 18
		0, // 19
		0, // 20
		0, // 21
		0, // 22
		0, // 23
		12 // 24
	}; 

	TEXT_FIELD_ASSERTION(rSize >= 0 && rSize < countof(SubscriptTable_));

	rSize = SubscriptTable_[rSize];					// $[TI1]

	TEXT_FIELD_ASSERTION(rSize != 0);

	// Increment the current yOffset (via the reference) to drop
	// the subscript.
	rYOffset += rSize / 2;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getValue_
//
//@ Interface-Description
// This function extracts a base 10 number from the passed buffer[], at offset
// rBufIndex.  It asserts that the extracted Int value is within the range
// specified.  It also asserts that the number does not overflow while being
// extracted.  The min and max values default to MIN_INT16_VALUE and
// MAX_INT16_VALUE respectively.  A single leading equal sign is allowed but
// not required.  At least one digit must be present.  The index is left
// pointing at the first non-numeric character after the extracted
// number; this must not be a null ('\0').
//---------------------------------------------------------------------
//@ Implementation-Description
// Extracts characters from buffer one at a time, converts them to decimal
// digits, and accumulates the resulting value into numValue.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
Int
TextField::getValue_(const wchar_t buffer[], Int &rBufIndex,
					Int minValue, Int maxValue)
{
	CALL_TRACE("getValue_(const wchar_t buffer[], Int &rBufIndex, "
			"Int minValue, Int maxValue)");

	// if an equal sign is present, skip over it:
	if (buffer[rBufIndex] == '=')
	{	//												$[TI1]
		rBufIndex ++;
		TEXT_FIELD_ASSERTION(buffer[rBufIndex] != '\0');
	}
	//													$[TI2]
													
	// now extract the number:
		
	Boolean isNegative = FALSE;
	
	if (buffer[rBufIndex] == L'-')	// check for minus sign
	{	//												$[TI3]
		isNegative = TRUE;
		rBufIndex ++;
		TEXT_FIELD_ASSERTION(buffer[rBufIndex] != L'\0');
	}
	//													$[TI4]

	// There must be at least one digit present:
	TEXT_FIELD_ASSERTION(isDigit_(buffer[rBufIndex]));

	Int numValue = 0;
	Int prevValue = -1;


	while (isDigit_(buffer[rBufIndex]))
	{	//												$[TI5]
		numValue = numValue * 10 + (buffer[rBufIndex] - L'0');

		// Check for wrap-around:
		TEXT_FIELD_ASSERTION(numValue >= prevValue);
		
		rBufIndex++;
		TEXT_FIELD_ASSERTION(buffer[rBufIndex] != L'\0');

		prevValue = numValue;
	}

	if (isNegative)
	{	//												$[TI6]
		numValue = - numValue;
	}
	//													$[TI7]
	
	TEXT_FIELD_ASSERTION(numValue >= minValue && numValue <= maxValue);

	return (numValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ("CLASS_PRE_CONDITION()", etc.). The "softFaultID" and
//	"lineNumber" are essential pieces of information.  The "pFileName"
//	and "pPredicate" strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
TextField::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, TEXT_FIELD, lineNumber,
  pFileName, pPredicate);

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateArea_
//
//@ Interface-Description
// This function should be called whenever the string to be rendered (in
// string_[]) changes. It sets the area_() member of Drawable to surround
// the text that would be rendered if this text were drawn.
//---------------------------------------------------------------------
//@ Implementation-Description
// This function uses validateAndRenderTextField_() in the setArea mode to
// valide the string_ and set the area_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TextField::updateArea_(void)
{
    CALL_TRACE("TextField::updateArea_(void)");
 
    // Initialize area_ to empty.  Must call setWidth() & setHeight() via
    // Drawable, because this class has redefined them to prevent the
    // application from explicitly setting width and height.
 
    //                                                  $[TI1]
    Drawable::setWidth(1);
    Drawable::setHeight(1);

    // The following call updates the area_ member of drawable.
    validateAndRenderTextField_(TRUE);
    
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dumpTextString_
//
//@ Interface-Description
// This method is called by the TEXT_FIELD_ASSERTION macros to print the 
// this TextField's text string to the console.
//---------------------------------------------------------------------
//@ Implementation-Description
// Uses cout to print the private string_ to the console. Terminates 
// the private string_ with a NULL to assure NULL termination before
// calling cout to print the string.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TextField::dumpTextString_(void)
{
    CALL_TRACE("TextField::dumpTextString_(void)");

	RETAILMSG(1, (L"invalid TextField: %s\n", string_));
}

