#ifndef HelpStrs_HH
#define HelpStrs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: HelpStrs.hh - Contains the structure declarations necessary
// to form a help menu and text system for display by HelpSubScreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/HelpStrs.hhv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 004  By:  clw    Date:  23-OCT-97    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Rework resulting from peer review. Added missing type comments.
//
//  Revision: 003  By:  clw    Date:  21-oct-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated the HelpStrs class. Many strings were not included in this
//             class previously - and by eliminating it, this header file is less likely
//             to change, thus reducing build interdependency.
//
//  Revision: 002  By:  clw    Date:  10-oct-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//             Removed unused help titles.
//
//  Revision: 001  By:  mpm    Date:  13-JUL-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "TextArea.hh"
#include "GuiAppClassIds.hh"

enum DiagramId
{
	HELP_NO_DIAGRAM,
	HELP_DIAGRAM1,
	HELP_DIAGRAM2
};

struct HelpMenu;  // forward declaration


//@ Type: HelpText
// A structure holding the information describing a help topic text screen.
// This includes an optional diagram button identified by DiagramId, a pointer
// to an array of TextAreaLine objects (pHelpTextLines), and a pointer to a
// StringId object that contains the title of the screen.  Although referred to
// as a screen, the topic text might actually be more than will fit on one
// screen; this is automatically paged into as many screens as needed.

struct HelpText
{
	DiagramId 		helpDiagram;
	TextAreaLine 	*pHelpTextLines;
	StringId		*pHelpTextTitle;
};


//@ Type: HelpMenuTopic
// A structure holding the information describing an entry in a help menu.
// Each menu item has a title, P_TOPIC, plus either a pointer to a text screen
// pHelpText or a pointer to a submenu pSubMenu.  One and only one out of
// pHelpText and pSubMenu must be set to NULL since a help topic can either
// point to a set of help text or to a submenu, not both.

struct HelpMenuTopic
{
	StringId		*P_TOPIC;
	HelpText		*pHelpText;
	HelpMenu 		*pSubMenu;
};


//@ Type: HelpMenu
// A HelpMenu structure consists of a title, P_TITLE, and a pointer to an
// array of help topics, pHelpTopics.

struct HelpMenu
{
	StringId		*P_TITLE;
	HelpMenuTopic 	*pHelpTopics;
};

// Top-level menu for displaying the help system for GUI-Apps during normal
// ventilation.
extern HelpMenu NormalVentHelpMenu;

#endif // HelpStrs_HH 

