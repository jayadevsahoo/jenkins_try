#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: GuiTimeout - GUI State: Timeout.
//---------------------------------------------------------------------
//@ Interface-Description
//  The GuiTimeout class is a FsmState used by the GuiFsm finite state
//  machine.  GUI task control enters this state when communications
//  cannot be established with the BD within the timeout period
//  established by the StartupTimer class.  Transitioning to the
//  TIMEOUT state allows the operator to display the diagnostic
//  information even though the BD may not be communicating.  This
//  class contains methods to process events while in the TIMEOUT
//  state.  Upon receiving a BdReadyMessage, this state will reboot the
//  GUI to sync up with the BD which is apparently now alive.  There
//  are no state transitions out of the TIMEOUT state other than
//  rebooting the GUI and restarting the finite state machine.
//
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the GUI Timeout state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  GuiTimeout is derived from the FsmState class.  It overrides the
//  virtual method receiveBdReady to provide state specific behavior
//  when it receives a BdReadyMessage.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiTimeout.ccv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "GuiTimeout.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "TaskControlAgent.hh"
#include "BdReadyMessage.hh"
#include "TaskAgentManager.hh"
#include "Reset.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//TODO E600 added GUI only
#if defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiTimeout [default constructor]
//
//@ Interface-Description
//  GuiTimeout default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiTimeout::GuiTimeout()
{
    CALL_TRACE("GuiTimeout()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiTimeout [destructor]
//
//@ Interface-Description
//  GuiTimeout class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiTimeout::~GuiTimeout()
{
    CALL_TRACE("~GuiTimeout()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

const char*
GuiTimeout::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "GuiTimeout";
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with
//  this GuiOnline state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the ONLINE state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SigmaState
GuiTimeout::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_TIMEOUT;
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveBdReady
//
//@ Interface-Description
//  The receiveBdReady method processes a BdReadyMessage received while
//  in the GuiTimeout state.  Upon receiving this message, this method
//  will reset the GUI to resynchronize with the BD.  The
//  processor is reset to attempt a transition through the START state
//  to the ONLINE, SERVICE, or INOP states.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
GuiTimeout::receiveBdReady(const BdReadyMessage& ) const
{
    CALL_TRACE("receiveBdReady(const BdReadyMessage&)");


    Reset::Initiate(INTENTIONAL, Reset::GUITIMEOUT);
    // $[TI1]
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
GuiTimeout::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_GUITIMEOUT,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================




#endif //defined(SIGMA_GUI_CPU)
