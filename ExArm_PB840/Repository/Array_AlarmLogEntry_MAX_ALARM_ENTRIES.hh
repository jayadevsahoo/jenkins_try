
#ifndef Array_AlarmLogEntry_MAX_ALARM_ENTRIES_HH
#define Array_AlarmLogEntry_MAX_ALARM_ENTRIES_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Array_AlarmLogEntry_MAX_ALARM_ENTRIES - Fixed Array of 'MAX_ALARM_ENTRIES' Elements of Type 'AlarmLogEntry'.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedArray.H_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Collection.hh"
#include "Collection.hh"

//@ Usage-Classes
#include "Array_AlarmLogEntry.hh"
//@ End-Usage


class Array_AlarmLogEntry_MAX_ALARM_ENTRIES : public Array_AlarmLogEntry
{
  public:
    Array_AlarmLogEntry_MAX_ALARM_ENTRIES(const Array_AlarmLogEntry_MAX_ALARM_ENTRIES& array);
    Array_AlarmLogEntry_MAX_ALARM_ENTRIES(void);
    virtual ~Array_AlarmLogEntry_MAX_ALARM_ENTRIES(void);

    inline void  operator=(const Array_AlarmLogEntry_MAX_ALARM_ENTRIES& array);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  protected:
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    //@ Data-Member:  pBlock_
    // A fixed array of 'MAX_ALARM_ENTRIES' elements.
    AlarmLogEntry  pBlock_[MAX_ALARM_ENTRIES];
};


// Inlined methods...
#include "Array_AlarmLogEntry_MAX_ALARM_ENTRIES.in"


#endif  // Array_AlarmLogEntry_MAX_ALARM_ENTRIES_HH
