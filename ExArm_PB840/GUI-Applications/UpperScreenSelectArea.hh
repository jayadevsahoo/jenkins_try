#ifndef UpperScreenSelectArea_HH
#define UpperScreenSelectArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: UpperScreenSelectArea - Area which contains screen select
// buttons for Waveforms, More Data, Alarm Log, More Alarms, Other
// Screens, Events, and Trending.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/UpperScreenSelectArea.hhv   25.0.4.0   19 Nov 2013 14:08:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: rhj   Date:  01-May-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Trend related changes. 
//
//  Revision: 004   By: rhj   Date:  20-Sept-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Added the ability to get the More Data Tabs Button.
//
//  Revision: 003  By:  syw	   Date:  26-Aug-1998    DCS Number: 
//  Project:  BiLevel
//  Description:
//		Initial version.  Added "Software Property of NPB" message.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "Bitmap.hh"
#include "Box.hh"
#include "TabButton.hh"
#include "AlarmLogTabButton.hh"
#include "MoreAlarmsTabButton.hh"
#include "WaveformsTabButton.hh"
#include "GuiAppClassIds.hh"
#include "TrendEventTarget.hh"
#include "TrendEventRegistrar.hh"
//@ End-Usage

class UpperScreenSelectArea : public Container, public TrendEventTarget
{
public:
	UpperScreenSelectArea(void);
	~UpperScreenSelectArea(void);

	void initialize(void);
	void updateButtonDisplay(void);
	inline WaveformsTabButton* getWaveformsTabButton(void);
	inline TabButton* getUpperOtherScreensTabButton(void);
    inline TabButton* getMoreDataTabButton(void);
	inline TabButton* getTrendsTabButton(void);

    // Inheriting classes will be informed of trend manager status
    virtual void trendManagerStatus( Boolean isEnabled, Boolean isRunning );

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);



protected:

private:
	// these methods are purposely declared, but not implemented...
	UpperScreenSelectArea(const UpperScreenSelectArea&);	// not implemented...
	void operator=(const UpperScreenSelectArea&);			// not implemented...

	//@ Data-Member: waveformsTabButton_
	// Screen select button for the Waveforms subscreen.
	WaveformsTabButton waveformsTabButton_;

	//@ Data-Member: moreDataTabButton_
	// Screen select button for the More Data subscreen.
	TabButton moreDataTabButton_;

	//@ Data-Member: alarmLogTabButton_
	// Screen select button for the Alarm Log subscreen.
	AlarmLogTabButton alarmLogTabButton_;

	//@ Data-Member: moreAlarmsTabButton_
	// Screen select button for the More Alarms subscreen.
	MoreAlarmsTabButton moreAlarmsTabButton_;

	//@ Data-Member: upperOtherScreensTabButton_
	// Screen select button for the Upper Other Screens subscreen.
	TabButton upperOtherScreensTabButton_;

	//@ Data-Member: mainSettingsTabButton_
	// Screen select button for the Upper Main Settings subscreen
	TabButton mainSettingsTabButton_;

	//@ Data-Member: waveformsBitmap_
	// Bitmap for the Waveforms screen select button.
	Bitmap waveformsBitmap_;

	//@ Data-Member: trendBitmap_
	// Bitmap for the trend screen select button.
	Bitmap trendBitmap_;

	//@ Data-Member: moreDataBitmap_
	// Bitmap for the More Data screen select button.
	Bitmap moreDataBitmap_;

	//@ Data-Member: upperOtherScreensBitmap_
	// Bitmap for the Upper Other Screens screen select button.
	Bitmap upperOtherScreensBitmap_;

	//@ Data-Member: topLineBox_
	// Box used for displaying white border at the top of this area,
	// coincident with the bottom border for the UpperSubScreenArea.
	Box topLineBox_;

	//@ Data-Member: trendsTabButton_
	// Screen select button for the Trends subscreen.
	TabButton  trendsTabButton_;

    //@ Data-Member: eventTabButton_
	// Screen select button for the Event subscreen.
	TabButton  eventTabButton_;

	enum
	{
		NUM_TAB_BUTTONS_ = 8
    };

	// The subscreen buttons.
	TabButton*  arrTabButtonPtrs_[NUM_TAB_BUTTONS_ + 1];


};

// Inlined methods
#include "UpperScreenSelectArea.in"

#endif // UpperScreenSelectArea_HH 
