#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2002, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SerialLoopbackTestSubScreen - Serial Loopback Test Subscreen
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen is activated by selecting Serial Loopback Test
// button in the ServiceLowerOtherScreen.  This test is only for GUIs with
// 3 serial ports and tests that ports 2 and 3 are active by transmitting
// a message from port 2 to port 3 and then from port 3 to port 2.
// Only one instance of this class is created by LowerSubScreenArea.  The
// interface is pretty generic, as a SubScreen it defines the activate()
// and deactivate() methods and then receives button events
// through via the usual means (i.e., the buttonDownHappened(),
// buttonUpHappened(), adjustPanelClearPressHappened and adjustPanelAcceptPressHappened()) 
//---------------------------------------------------------------------
//@ Rationale
// Required for user interface to the Serial Loopback Test.
//---------------------------------------------------------------------
//@ Implementation-Description
// The main operation of this class is to wait for button down and up events,
// received via buttonUpHappened() ,buttonDownHappened(), adjustPanelClearPressHappened()
// and adjustPanelAcceptPressHappened().  It also waits for test result data and
// test commands received from Service-Data and dispatched to the virtual
// methods processTestPrompt(), processTestKeyAllowed(), 
// processTestResultStatus(), processTestResultCondition(), and 
// processTestData().  
// On the other hand, button events are caught by the button callback methods. 
// The display of this subscreen is organized into many different screen
// layouts corresponding to different stages of the test process.
// Pressing down the Start Test button causes the test process
// to begin.  The user's responses to the prompts displayed are what drives
// the process forward.
//
// The ServiceDataRegistrar is used to register for service data events.
// All other functionality is inherited from the SubScreenArea class.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only be
// displayed in the LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SerialLoopbackTestSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  quf    Date:  23-Jan-2002    DR Number: 5984
//  Project: GuiComms
//  Description:
//  Initial coding.
//=====================================================================

#include "SerialLoopbackTestSubScreen.hh"

#include "MiscStrs.hh"
#include "PromptStrs.hh"
#include "SmTestId.hh"
#include "LowerScreen.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceLowerScreenSelectArea.hh"

#if defined FAKE_SM
#	include "FakeServiceModeManager.hh"
#   define SmManager FakeServiceModeManager
#else		
#	include "SmManager.hh"
#endif	// FAKE_SM

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "PromptArea.hh"
#include "Sound.hh"
class SubScreenArea;
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Uint16 BUTTON_X_ = 520;
static const Uint16 BUTTON_Y_ = 367;
static const Uint16 BUTTON_WIDTH_ = 110;
static const Uint16 BUTTON_HEIGHT_ = 32;
static const Uint8  BUTTON_BORDER_ = 3;
static const Int32 STATUS_LABEL_X_ = 379;
static const Int32 STATUS_LABEL_Y_ = 0;
static const Int32 STATUS_WIDTH_ = 255;
static const Int32 STATUS_HEIGHT_ = 31;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SerialLoopbackTestSubScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the Serial Loopback Test Subscreen.  The given `pSubScreenArea' is 
// the SubScreenArea in which it will be displayed (in this case the lower
// subscreen area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Creates, positions, and initializes all the graphical components of this
// subscreen:  title and a number of static and dynamic textual items.
// Also, register for callback for the Start Test button.
// $[GC07000] The Serial Port Loopback subscreen shall provide a button..
// $[GC07001] Once the test has been started, the Serial Port Loopback...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SerialLoopbackTestSubScreen::SerialLoopbackTestSubScreen(SubScreenArea *pSubScreenArea) :
	SubScreen(pSubScreenArea),
	testStatus_(STATUS_LABEL_X_, STATUS_LABEL_Y_,
				STATUS_WIDTH_, STATUS_HEIGHT_,
				MiscStrs::EXH_V_CAL_STATUS_LABEL,
				NULL_STRING_ID),
	errorHappened_(FALSE),
	keyAllowedId_(-1),
	startTestButton_(BUTTON_X_,				BUTTON_Y_,
					BUTTON_WIDTH_,			BUTTON_HEIGHT_,
					Button::DARK, 			BUTTON_BORDER_,
					Button::SQUARE,			Button::NO_BORDER,
					MiscStrs::START_TEST_TAB_BUTTON_LABEL),
	guiTestMgr_(this),
	titleArea_(MiscStrs::SERIAL_LOOPBACK_TEST_SUBSCREEN_TITLE)
{
	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Register for callbacks for all buttons in which we are interested.
	startTestButton_.setButtonCallback(this);

	// Add the title area
	addDrawable(&titleArea_);

	// Add the buttons.
	addDrawable(&startTestButton_);

	// Add status line text objects to main container
	addDrawable(&testStatus_);

	// Initialize the promptName, promptId arrays and prompts after an error
	// condition has occured
	setPromptTable_();

	guiTestMgr_.setupTestResultDataArray();
	guiTestMgr_.setupTestCommandArray();
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SerialLoopbackTestSubScreen  [Destructor]
//
//@ Interface-Description
//  Destroys the Serial Loopback Test subscreen.  Needs to do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SerialLoopbackTestSubScreen::~SerialLoopbackTestSubScreen(void)
{
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just before this subscreen is displayed
//  on the screen.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The first thing to do is to register the subscreen with Gui Test
//  manager for test events.  Then we call the generic screen layout
//  method to display the initial subscreen configuration.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::activate(void)
{
// $[TI1]
	//
	// Register all general possible test results for test events, but first
	// we have to initialize ServiceDataRegistrar.
	//
	guiTestMgr_.registerTestResultData();
	guiTestMgr_.registerTestCommands();

	// Let the service mode  manager knows the type of Service mode.
	SmManager::DoFunction(SmTestId::SET_TEST_TYPE_MISC_ID);

	layoutScreen_(TEST_SETUP);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just after this subscreen has been
//  removed from the screen.  No parameters are needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We perform any necessary cleanup.  First signal to Gui Test manager
//  about the end of test.  Release the AdjustPanel focus, then clear
//  the prompt area.  The Start Test button is shown in up position.
//  Finally, the LowerScreen Select area is shown.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::deactivate(void)
{
	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);


	// Pop up the button.
	startTestButton_.setToUp();

	// Activate lower subscreen area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when start buttons in this subscreen is
// pressed down (because this button registers a callback).
// Passed a pointer to the button as well as 'byOperatorAction', a flag which
// indicates whether the operator pressed the button down or whether the
// setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::buttonDownHappened(Button *pButton,
									Boolean byOperatorAction)
{
	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	if (pButton == &startTestButton_)
	{		// $[TI3]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();

		//
		// Set all prompts 
		//
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								PromptStrs::SM_KEY_ACCEPT_P);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_HIGH,
								NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_LOW,
								NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH,
								PromptStrs::OTHER_SCREENS_CANCEL_S);

		// Set the next test status.
		testStateId_ = START_BUTTON_PRESSED;
	}		// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when any button in this subscreen is deselected.
// Passed a pointer to the button as well as 'byOperatorAction', 
// which indicates whether the operator pressed the button up or whether the 
// setToUp() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
//  There is a possibility that this event occurs after this subscreen is
//  removed from the screen (when this subscreen is deactivated and the
//  Adjust Panel focus is lost).  In this case we do nothing, simply return.
//  Otherwise, we clear the Adjust Panel focus, dehighlight the prompt area
//  and change the display state id. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::buttonUpHappened(Button *pButton, 
													Boolean byOperatorAction)
{
	// Check if the event is operator-initiated
	if (byOperatorAction)
	{													// $[TI1]
		// Clear the all prompts
		// Clear the Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Make sure the prompt area's background is dehighlighted.
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY, 
				PromptArea::PA_HIGH, NULL_STRING_ID);

		// Set the next test status.
		testStateId_ = UNDEFINED_TEST_STATE;
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.  It is
//  normally called when the operator presses down the Accept key to signal the
//  accepting of continuing the test process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First reset the Adjust Panel focus, then clear the prompt area.
//  Next, depending on the display state id, we layout the screen accordingly.
//  The state id is set to UNDEFINED_TEST_STATE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::adjustPanelAcceptPressHappened(void)
{
	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Make sure the prompt area's background is dehighlighted.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_HIGH, NULL_STRING_ID); 

	switch(testStateId_)
	{
	case START_BUTTON_PRESSED:				// $[TI1]
		// layout the screen then signal Service Mode to
		// start the test
		layoutScreen_(TEST_START_ACCEPTED);
		startTest_(SmTestId::SERIAL_LOOPBACK_TEST_ID);
		break;

	case PROMPT_ACCEPT_BUTTON_PRESSED:		// $[TI2]
		// layout the screen then signal Service Mode to
		// continue the test
		layoutScreen_(TEST_PROMPT_ACCEPTED_CLEARED);
		SmManager::OperatorAction( SmTestId::SERIAL_LOOPBACK_TEST_ID,
										  SmPromptId::KEY_ACCEPT );
		break;

	default:								// $[TI3]
		// Make the Invalid Entry sound
		Sound::Start(Sound::INVALID_ENTRY);
		break;
	}

	testStateId_ = UNDEFINED_TEST_STATE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_
//
//@ Interface-Description
// Lays out the lower screen into one of three display configurations.
//---------------------------------------------------------------------
//@ Implementation-Description
// Update prompt messages, Start Test button display, test status display, and
// screen select area display as a function of the display configuration.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::layoutScreen_(TestScreenId testScreenId)
{
	switch (testScreenId)
	{
	case TEST_SETUP:				// $[TI1]
		// Set all prompts
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_TOUCH_START_TEST_TO_BEGIN_P);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_LOW, NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);

		// Show the startTestButton_
		startTestButton_.setShow(TRUE);

		// Hide the test status
		testStatus_.setShow(FALSE);

		break;

	case TEST_START_ACCEPTED:	// $[TI2]
	{
		// Clear all prompts
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_LOW, NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, NULL_STRING_ID);

		// Pop up the startTestButton_.
		startTestButton_.setToUp();

		// Hide the start button.
		startTestButton_.setShow(FALSE);

		// Hide the lower screen select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

		break;
	}	

	case TEST_PROMPT_ACCEPTED_CLEARED:	// $[TI3]

		// Set Primary prompt to "Please Wait...."
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_PLEASE_WAIT_P);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPromptTable_
//
//@ Interface-Description
//  Initialize the promptId_, and promptName_ tables.
//  The promptId_ array contains Service-Mode prompt ids.
//  The promptName_ table contains translation text of Service-Mode prompt code.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::setPromptTable_(void)
{
	Int testIndex = 0;

	// First clear each entry in the array.
	for (testIndex = 0; testIndex < MAX_TEST_PROMPTS; testIndex++)
	{
	// $[TI1]
		promptId_[testIndex]	= SmPromptId::NULL_PROMPT_ID;
		promptName_[testIndex] 	= NULL_STRING_ID;
	}

	testIndex = 0;

	// Fill in the prompt table entries
	promptId_[testIndex] = SmPromptId::CONNECT_CABLE_PROMPT;
	promptName_[testIndex++] = PromptStrs::CONNECT_CABLE_P;

	// Make sure total test count is within the maximum allowed.
	CLASS_ASSERTION(testIndex == MAX_TEST_PROMPTS);
}


//============================ m e t h o d   d e s c r i p t i o n ====
//@ Method: processTestPrompt
//
//@ Interface-Description
//  Process the prompt command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First, we grab the Adjust Panel focus, then we update the prompt area
//  and the status area.  Next, we update the test state id.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::processTestPrompt(Int command)
{
	// Grab Adjust Panel focus
	AdjustPanel::TakeNonPersistentFocus(this, TRUE);

	// Disable the knob sound.
	AdjustPanel::DisableKnobRotateSound();

	// Error checking the prompt received 
	Int idx ;
	for (idx = 0;
			 idx < MAX_TEST_PROMPTS && promptId_[idx] != command;
			 idx++);
	CLASS_ASSERTION(idx < MAX_TEST_PROMPTS);

	// Set primary prompt to the corresponding message.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								promptName_[idx]);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
								PromptArea::PA_HIGH,
								NULL_STRING_ID); 

	switch (keyAllowedId_)
	{
	case SmPromptId::ACCEPT_KEY_ONLY:				// $[TI1]
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY, 
								PromptArea::PA_HIGH,
								PromptStrs::EXH_V_CAL_PROMPT_ACCEPT_S);
		break;

	default:										// $[TI2]
		CLASS_ASSERTION_FAILURE();			// Illegal event
		break;
	}

	// Display the overall testing status on the service status area
	testStatus_.setStatus(MiscStrs::EXH_V_CAL_WAITING_MSG);
	testStatus_.setShow(TRUE);

	// Set the next test status
	testStateId_ = PROMPT_ACCEPT_BUTTON_PRESSED;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//  Process the test result status given by Service Mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
// TEST_START: no action.
// TEST_ALERT or TEST_FAILURE: set errorHappened_ TRUE to indicate test fault.
// TEST_END: setup for test to be restarted.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	switch (resultStatus)
	{
	case SmStatusId::TEST_END:			// $[TI1]
		setupForRestart_();
		break;

	case SmStatusId::TEST_START:		// $[TI2]
		break;

	case SmStatusId::TEST_ALERT:		// $[TI3]
	case SmStatusId::TEST_FAILURE:		// $[TI4]
		errorHappened_ = TRUE;
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//  Process test result condition passed by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this subscreen is designed not to display test result conditions,
//  this remains an empty method.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::processTestResultCondition(Int resultCondition)
{
	// Empty method
	// SerialLoopbackTest doesn't display test result conditions
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//  Process keyAllowed command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Store the key allowed ID
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::processTestKeyAllowed(Int command)
{
	// Remember the expected user key response ID
	keyAllowedId_ = command;
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator release the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the test
// process.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call GuiTestManager's virtual method to do the job.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::adjustPanelRestoreFocusHappened(void)
{
	guiTestMgr_.restoreTestPrompt();
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  Process data given by Service-Mode
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this subscreen is designed not to display test data,
//  this remains an empty method
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	// Empty method
	// SerialLoopbackTest doesn't display test data
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTest_
//
//@ Interface-Description
//	Start the indicated Service Mode test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Reset the errorHappened_ flag then tell Service Mode to do the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)
{
	// Reset status flag
	errorHappened_ = FALSE;

	// Start the test.
	SmManager::DoTest(currentTestId);
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupForRestart_
//
//@ Interface-Description
//	Prepare the screen for users to restart the test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Show the Lower Select area and START TEST button; Display the status
//	and prompts.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::setupForRestart_(void)
{
	if (errorHappened_)
	{                                       // $[TI1]
		// Display the serial loopback failed status message.
		testStatus_.setStatus(MiscStrs::SERIAL_LOOPBACK_TEST_FAILED_MSG);
	}
	else
	{                                       // $[TI2]
		// Display the serial loopback completed status message.
		testStatus_.setStatus(MiscStrs::SERIAL_LOOPBACK_TEST_COMPLETE_MSG);
	}

	// Activate the lower screen 's select area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);

	// Set primary prompt to start test.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, 
					PromptStrs::EXH_V_CAL_TOUCH_START_TEST_TO_BEGIN_P);

	// Erase the advisory prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_HIGH,
					NULL_STRING_ID);

	// Set the secondary prompt to To cancel touch other screen.
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, 
					PromptStrs::OTHER_SCREENS_CANCEL_S);

	// Redisplay start button.
	startTestButton_.setShow(TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SerialLoopbackTestSubScreen::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							SERIALLOOPBACKTESTSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
