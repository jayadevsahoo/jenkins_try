#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: O2FlowSensorTest - BD Safety Net Test for the detection of
//                            O2 flow sensor reading OOR  
//---------------------------------------------------------------------
//@ Interface-Description
//  The checkCriteria() method of this class is called from the
//  newCycle() method of the SafetyNetTestMediator class.  The
//  checkCriteria() method invokes methods in the Sensor class
//  to obtain the O2 Flow reading.
//---------------------------------------------------------------------
//@ Rationale
//  Used to determine if the O2 Flow Sensor reading OOR 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The safety net test is defined in the checkCriteria() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/O2FlowSensorTest.ccv   10.7   08/17/07 09:39:12   pvcs  
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code when fault is detected.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  20-Sep-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "O2FlowSensorTest.hh"
#include "MainSensorRefs.hh"
#include "FlowSensor.hh"

//@ Usage-Classes
#if defined ( SIGMA_SAFETY_NET_DEBUG )
#include <stdio.h>
#endif // defined ( SIGMA_SAFETY_NET_DEBUG )
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: O2FlowSensorTest()  
//
//@ Interface-Description
//  Default Constructor.  Passes two arguments to the base class
//  contructor to define the error code and the time criterion.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The backgndEventId_ and maxCyclesCriteriaMet_ data members are
//  set by the base class constructor.  The background event id is
//  a unique identifier that is placed in the diagnostic log if the
//  background check detects a problem.
//
//  This base class constructor is passed null values because this
//  class conducts two O2 flow sensor tests, high and low.
//  Hence, data member numCyclesOorHigh_ is used to keep track of the
//  number of cycles the o2 flow sensor reads OOR-high.  The
//  data member numCyclesOorLow_ is used to keep track of the number
//  of cycles the o2 flow sensor reads OOR-low.  These two data
//  members are both initialized to 0.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

O2FlowSensorTest::O2FlowSensorTest( void ) :
SafetyNetTest( BK_NO_EVENT, 0 )
{
    // $[TI1]
    CALL_TRACE("O2FlowSensorTest( void )");

    numCyclesOorHigh_ = 0;
    numCyclesOorLow_ = 0;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~O2FlowSensorTest() 
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

O2FlowSensorTest::~O2FlowSensorTest(void)
{
    CALL_TRACE("~O2FlowSensorTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCriteria()
//
//@ Interface-Description
//  This method takes no arguments and returns the a BkEventName.
//  This method is responsible for detecting the O2 flow sensor
//  reading OOR.
//  If the background check criteria have been met for the required
//  number of cycles, the backgndEventId for the class is returned;
//  otherwise BK_NO_EVENT is returned to indicate to the caller that
//  the Background subsystem should not be notified.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method first checks the status of the backgndCheckFailed_
//  data member.  If it is TRUE, the test is no longer run since
//  Background events are not auto-resettable and the Background
//  subsystem only needs to be informed of an event once.  In this
//  case, BK_NO_EVENT is returned to the caller.
//
//  If the check has not already failed, the value of the
//  O2 Flow signal is retrieved from the Sensor objects.
//
//  The method calls the getValue() method for the O2 Flow Sensor
//  flow signal.
//
//  The following two criteria is tested:
//
//      (1)  O2 Flow Sensor > MAX_FLOW_TEST_LIMIT
//      (2)  O2 Flow Sensor < MIN_FLOW_TEST_COUNT_LIMIT
//
//  If either of the condition is met, its associated counter is 
//  incremented and compared to the maximum allowable cycle for 
//  that test to determine if the background subsystem needs to
//  be informed.
//
//  $[06107] $[06108]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
O2FlowSensorTest::checkCriteria( void )
{
    CALL_TRACE("O2FlowSensorTest::checkCriteria( void )") ;

    BkEventName rtnValue = BK_NO_EVENT;

    // Don't run the check if it has already failed
    if ( ! backgndCheckFailed_ )
    {
    // $[TI1]

        if ( RO2FlowSensor.getValue() > MAX_FLOW_TEST_LIMIT )
        {
        // $[TI1.1]
            // Increment numCyclesOorHigh_ and compare to the max allowed
            if ( ++numCyclesOorHigh_ >= MAX_FLOW_HIGH_OOR_CYCLES )
            {
            // $[TI1.1.1]
                errorCode_ = (Uint16)(RO2FlowSensor.getValue()*100.0F);
                backgndCheckFailed_ = TRUE;
                rtnValue = BK_O2_FLOW_OOR_HIGH;
            }
            // $[TI1.1.2]
        }
        else if ( RO2FlowSensor.getCount() < MIN_FLOW_TEST_COUNT_LIMIT )
        {
        // $[TI1.2]
            numCyclesOorHigh_ = 0;
 
            if ( ++numCyclesOorLow_ >= MAX_FLOW_LOW_OOR_CYCLES )
            {
            // $[TI1.2.1]
                errorCode_ = (Uint16)(RO2FlowSensor.getValue()*100.0F);
                backgndCheckFailed_ = TRUE;
                rtnValue = BK_O2_FLOW_OOR_LOW;
            }
            // $[TI1.2.2]
        }
        else   // criteria not met
        {
        // $[TI1.3]
            numCyclesOorHigh_ = 0;
            numCyclesOorLow_ = 0;
        }
    }
    // $[TI2]

    return( rtnValue );
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
O2FlowSensorTest::SoftFault( const SoftFaultID  softFaultID,
                             const Uint32       lineNumber,
                             const char*        pFileName,
                             const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, O2FLOWSENSORTEST,
                           lineNumber, pFileName, pPredicate );
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================




