// BPSocket.cpp : implementation file
//

#include "stdafx.h"
#include "softd.h"
#include "BPSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BootPSocket

//////////////////////////////////////////////////////////////////////
BootPSocket::BootPSocket()
{
	bpServer = NULL;

	return;
}

//////////////////////////////////////////////////////////////////////
BootPSocket::~BootPSocket()
{
	bpServer = NULL;

	return;
}

//////////////////////////////////////////////////////////////////////
BootPSocket::BootPSocket(BootPServer *server)
{
	bpServer = server;

	return;
}

// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(BootPSocket, CAsyncSocket)
	//{{AFX_MSG_MAP(BootPSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// BootPSocket member functions

//////////////////////////////////////////////////////////////////////
void BootPSocket::OnReceive(int nErrorCode) 
{
	CAsyncSocket::OnReceive(nErrorCode);

	if(bpServer){
		bpServer->ProcessMsg();
	}

	return;
}

