#ifndef BackgroundFaultHandler_HH
#define BackgroundFaultHandler_HH
//===========================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//===========================================================================

//========================== H E A D E R   D E S C R I P T I O N ============
//  Class: BackgroundFaultHandler - independent task for reporting and
//                                  processing of system failures.
//---------------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundFaultHandler.hhv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By: iv    Date:  07-Jan-1998    DR Number: DCS 5000
//    Project:  Sigma (840)
//    Description:
//       Added a Uint16 default argument to methods ProcessBkEvent() and
//       LogDiagnosticCodeUtil_().
//
//  Revision: 003   By: by   Date: 28-Feb-1997   DR Number: None
//    Project:  Sigma (840)
//    Description:
//       When a BTAT9 and BTAT11 both exist, the alarm type is
//       upgraded to a BTAT1.
//
//  Revision: 002   By: by   Date: 18-Feb-1997   DR Number: 1771
//    Project:  Sigma (840)
//    Description:
//       Modified the arming of EST Required device alert on power-up
//       instead of when an EST Required to reset alert occurs.
//       As a result, ArmEstRequiredAlert_() is obsolete.
//
//  Revision: 001    By: by   Date: 11-Nov-1996 DR Number: None
//    Project: Sigma (840)
//    Description:
//       Initial version
//
//===========================================================================

#include "Sigma.hh"
#include "Background.hh"
#include "BtatEventState.hh"
#include "ChangeStateMessage.hh"
#include "GuiReadyMessage.hh"
#include "TaskControlAgent.hh"

static const Uint8 MAX_MINOR_BTAT_LIST_ITEMS = 18;
static const Uint8 MAX_EST_REQUIRED_BTAT_LIST_ITEMS = 12;
 
class BackgroundFaultHandler
{
    public:

        typedef struct
        {
            OperatingParameterEventName alarmType;
            TestResultId                failureType;
            Boolean                     multipleLoggingRequired;
            char*                       pDebugText; // Used for vconsole output
        } BkEventType;

        static void inline ChangeStateCallback( const ChangeStateMessage& rMessage );

        static inline OperatingParameterEventName
        GetAssociatedBtat( const enum BkEventName eventName );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL, 
			       const char*       pPredicate = NULL );

        static void Initialize( void ); 

        static void ProcessQueueTask( void ); 

        static void ProcessBkEvent(const enum BkEventName eventName,
                                   const Uint16 errorCode = 0u);

        static void ReissueActiveBtat( void );
        
        static void GuiReadyCallback( const GuiReadyMessage& rMessage );

    protected:

    private:

        BackgroundFaultHandler( void );  // Constructor, not implemented
        ~BackgroundFaultHandler( void ); // Destructor, not implemented

        static void ReissueActiveBtat_( void );

        static void LogDiagnosticCodeUtil_(const enum BkEventName eventId,
                                           const Uint16 errorCode = 0u);

        static void SystemResponseToBtat_( const OperatingParameterEventName );

        static void ProcessReversibleBtat_( const OperatingParameterEventName, 
                                            BtatEventState& rBtatEventStat );

        static void DeclareVentInop_( void );

        //@ Data-Member: BK_EVENT_ASSOCIATION_TABLE_ 
        //  a error code table containing the
        //  definition of all background events 
        static const BkEventType BK_EVENT_DEFINITON_TABLE_[ BK_MAX_EVENT ];

        //@ Data-Member: BkEventStatusTable_
        //  stores current statuses of all Background Events
        //  when a Background event has occurred, its associated
        //  cell will be set to TRUE
        static Boolean BkEventStatusTable_[ BK_MAX_EVENT ];

        //@ Data-Member: MINOR_FAULT_BTAT_LIST_ 
        //  contains a list of minor BTATs
        static const OperatingParameterEventName
        MINOR_FAULT_BTAT_LIST_[ MAX_MINOR_BTAT_LIST_ITEMS ];

        //@ Data-Member: EST_REQUIRED_BTAT_LIST_ 
        //  contains a list of BTATs that require to pass EST to reset 
        static const OperatingParameterEventName
        EST_REQUIRED_BTAT_LIST_[ MAX_EST_REQUIRED_BTAT_LIST_ITEMS ];

#if defined( SIGMA_SAFETY_NET )
    public:
        static const char* GetBkEventName( const enum BkEventName eventName );
#endif // defined( SIGMA_SAFETY_NET )

};

// Inlined methods
#include "BackgroundFaultHandler.in"

#endif    // #ifndef BackgroundFaultHandler_HH

