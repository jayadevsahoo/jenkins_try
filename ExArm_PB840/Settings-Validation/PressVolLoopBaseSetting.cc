#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  PressVolLoopBaseSetting - Pressure-Volume Waveform Loop Baseline
//          Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a non-batch, bounded setting that contains the pressure-volume
//  loop baseline value (in cmH2O) above or below PEEP.  This class inherits
//  from 'NonBatchBoundedSetting' and provides the specific information
//  needed for representation of Loop Baseline.  This information includes
//  the interval and range of this setting's values, this non-batch setting's
//  default value (see 'getDefaultValue()'), and the contraints of this
//  setting.
//
//  This setting is dependent on the value of PEEP.  This dependency is
//  different than most dependent relationships, because this setting is
//  a non-batch setting, whereas PEEP is a batch setting; when non-batch
//  settings are changed thier "accepted" value is updated, but when batch
//  settings are changed their "adjusted" value is updated -- their
//  "accepted" value is not updated until the user explicitly accepts the
//  adjusted batch.  Because of this dependency, this setting defines an
//  'acceptPrimaryChange()' method.  However, because of the differences
//  between batch and non-batch changes, it is not PEEP that activates
//  that method, the Accepted Context will activate the method upon changes
//  to PEEP.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings nor dynamic constraints, but it
//  does have dynamic bounds (see 'updateConstraints_()').
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PressVolLoopBaseSetting.ccv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: sah   Date:  18-Apr-2000    DR Number: 5704
//  Project:  NeoMode
//  Description:
//      Changed to fixed upper and lower constraints, therefore no
//      need for 'updateConstraints_()' method.
//
//  Revision: 006   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  modified 'acceptPrimaryChange()' and 'updateContrsints()'
//	   methods to use new PEEP-low setting, when appropriate
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.10.1.0) into Rev "BiLevel" (1.10.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 002   By: sah    Date: 28 Oct 1996    DR Number: 761 & 1486
//  Project: Sigma (R8027)
//  Description:
//	Modified to look at PEEP's accepted value, rather than adjusted
//	value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "PressVolLoopBaseSetting.hh"
#include "Plot1TypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[01171] -- The setting's range ...
static const BoundedInterval  LAST_NODE_ =
  {
    -10.0f,		// absolute minimum...
    0.0f,		// unused...
    TENTHS,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    55.0f,		// absolute maximum...
    0.1f,		// resolution...
    TENTHS,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PressVolLoopBaseSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, the value interval
//  get initialized in this method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PressVolLoopBaseSetting::PressVolLoopBaseSetting(void)
    : NonBatchBoundedSetting(SettingId::PRESS_VOL_LOOP_BASE,
			     Setting::NULL_DEPENDENT_ARRAY_,
			     &::INTERVAL_LIST_,
			     PRESS_VOL_LOOP_BASE_MAX_ID,// maxConstraintId...
			     PRESS_VOL_LOOP_BASE_MIN_ID)// minConstraintId...
{
  CALL_TRACE("PressVolLoopBaseSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PressVolLoopBaseSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PressVolLoopBaseSetting::~PressVolLoopBaseSetting(void)
{
  CALL_TRACE("~PressVolLoopBaseSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
PressVolLoopBaseSetting::getApplicability(
			  const Notification::ChangeQualifier
					 ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pPlot1Type =
			  SettingsMgr::GetSettingPtr(SettingId::PLOT1_TYPE);

  const DiscreteValue  PLOT1_TYPE_VALUE = pPlot1Type->getAcceptedValue();

  return((PLOT1_TYPE_VALUE == Plot1TypeValue::PRESSURE_VS_VOLUME)
	   ? Applicability::CHANGEABLE		// $[TI1]
	   : Applicability::INAPPLICABLE);	// $[TI2]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  The purpose of this method is to return the setting's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method returns a constant parameter.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
PressVolLoopBaseSetting::getDefaultValue(void) const
{
  CALL_TRACE("getDefaultValue()");

  BoundedValue  defaultValue;
  
  defaultValue.value     = 0.0f;
  defaultValue.precision = TENTHS;

  return(defaultValue);   // $[TI1]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned, and this setting's value is "clipped"
//  to that bound.  Otherwise, a value of 'NULL' is returned to indicate no
//  dependent bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When PEEP or PEEP low changes, this method is called by the Accepted
//  Context (see 'AcceptedContext::acceptAdjustedBatch_()').
//
//  $[02002] -- new dependent settings based on primary settings...
//
//  NOTE:  since this setting is NOT a batch setting, it needs to base
//	   its bound on PEEP's accepted, rather than adjusted, value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
PressVolLoopBaseSetting::acceptPrimaryChange(
				    const SettingId::SettingIdType primaryId
					    )
{
  CALL_TRACE("acceptPrimaryChange(primaryId)");

  const Setting*  pPeep    = SettingsMgr::GetSettingPtr(SettingId::PEEP);
  const Setting*  pPeepLow = SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW);

  Real32  peepValue;

  if (pPeep->getApplicability(Notification::ACCEPTED) != 
						Applicability::INAPPLICABLE)
  {  // $[TI1] -- PEEP is applicable...
    peepValue = BoundedValue(pPeep->getAcceptedValue()).value;
  }
  else
  {  // $[TI2] -- PEEP low must be applicable...
    SAFE_CLASS_ASSERTION(
	  (pPeepLow->getApplicability(Notification::ACCEPTED) != 
						Applicability::INAPPLICABLE));
    peepValue = BoundedValue(pPeepLow->getAcceptedValue()).value;
  }

  // update this setting's dynamic constraints -- which are based on the
  // PEEP value...
  updateConstraints_();

  BoundedValue  loopBaseline = getAcceptedValue();

  if (peepValue != loopBaseline.value)
  {   // $[TI3]
    loopBaseline.value = peepValue;

    getBoundedRange_().warpValue(loopBaseline);

    // store the new P-V loop baseline value...
    setAcceptedValue(loopBaseline);
  }   // $[TI4] -- P-V loop baseline is unchanged...

  return(NULL);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PressVolLoopBaseSetting::SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName,
				   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  PRESS_VOL_LOOP_BASE_SETTING, lineNumber,
			  pFileName, pPredicate);
}
