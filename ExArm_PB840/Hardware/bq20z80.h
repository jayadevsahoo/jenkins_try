
//#############################################################################
//
//  bq20z80.h - Header file for bq20z80_t class.
//
//
//#############################################################################

#ifndef BQ20Z80_H

#define BQ20Z80_H

#include "I2C.h"
#include "Sigma.hh"


static const uint8_t COMMAND_CODE_READ_WRITE_BATTERY_MODE       = 0x03;
static const uint8_t COMMAND_CODE_READ_TEMPERATURE              = 0x08;
static const uint8_t COMMAND_CODE_READ_CURRENT                  = 0x0a;
static const uint8_t COMMAND_CODE_READ_RELATIVE_STATE_OF_CHARGE = 0x0d;
static const uint8_t COMMAND_CODE_READ_FULL_CHARGE_CAPACITY     = 0x10;
static const uint8_t COMMAND_CODE_READ_BATTERY_STATUS           = 0x16;
static const uint8_t COMMAND_CODE_READ_BATTERY_TIME             = 0x12;  // read the average time to empty information

struct Bq20z80Data_t
{
    uint16_t    Charge;                             // %
    bool        ChargeValid;

    uint16_t    FullChargeCapacity;                 // %
    bool        FullChargeCapacityValid;

    int16_t     Current;                            // mA
    bool        CurrentValid;

    uint16_t    BatteryStatus;                      // See data sheet
    bool        BatteryStatusValid;

    int16_t     BatteryTemperature;                 // tenths, celsius
    bool        BatteryTemperatureValid;

    uint16_t    BatteryTime;                        // unit is min, see data sheet
    bool        BatteryTimeValid;
};

class Bq20z80_t : public I2C
{
	public:
        Bq20z80_t(HANDLE FileHandle, uint8_t BusAddress, I2cBaudIndex_t BaudIndex);
        ~Bq20z80_t(){}

        void GetBatteryData(Bq20z80Data_t *BatteryData);
        bool GetRelativeStateOfCharge(uint16_t *ChargeState);
        bool GetBatteryStatus(uint16_t *BatteryStatus);
        bool GetFullChargeCapacity(uint16_t *ChargeCapacity);
        bool GetCurrent(int16_t *Current);

};

#endif

