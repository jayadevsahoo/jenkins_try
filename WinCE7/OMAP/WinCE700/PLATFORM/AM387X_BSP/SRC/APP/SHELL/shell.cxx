/*
===============================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
===============================================================================
*/
#include <windows.h>
#include <bsp.h>
#include <initguid.h>
//#include <twl.h>
//#include <bus.h>			//edited out for testing purposes
//#include <display_drvesc.h>	
#pragma warning(push)
#pragma warning(disable: 4201)
#pragma warning(disable:4100)
#pragma warning (disable:4127)
#pragma warning (disable: 6001)
#include <svsutil.hxx>
#pragma warning(pop)
#include <auto_xxx.hxx>
#include <usbfnioctl.h>
#include <string.h>
//#include <devload.h>
//#include <hdmi.h>			//edited out for testing purposes
#include <memtxapi.h>
#include "utils.h"
//#include <i2cproxy.h>
#include <proxyapi.h>
//#include <map.h>
#include "oalex.h"
//#include "oal_clock.h"

//#include "pmxproxy.h"

// disable PREFAST warning for use of EXCEPTION_EXECUTE_HANDLER
#pragma warning (disable: 6320)

// enables cacheinfo command to display OAL cache info, used for testing only
#define CACHEINFO_ENABLE        FALSE

#define MESSAGE_BUFFER_SIZE     280

EXTERN_C const GUID APM_SECURITY_GUID;

//-----------------------------------------------------------------------------

typedef VOID (*PFN_FmtPuts)(WCHAR *s, ...);
typedef WCHAR* (*PFN_Gets)(WCHAR *s, int cch);

//-----------------------------------------------------------------------------

#if 0
WCHAR const* GetVddText(int i);
WCHAR const* GetDpllText(int i);
WCHAR const* GetDpllClockText(int i);
WCHAR const* GetDeviceClockText(int i);
UINT FindClockIdByName(WCHAR const *szName);
OMAP_DEVICE FindDeviceIdByName(WCHAR const *szName);
WCHAR const* FindDeviceNameById(OMAP_DEVICE clockId);
WCHAR const* FindClockNameById(UINT clockId, UINT nLevel);
BOOL FindClockIdByName(WCHAR const *szName, UINT *pId, UINT *pLevel);
#endif

static BOOL Help(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL InReg8(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL InReg16(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL InReg32(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL OutReg8(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL OutReg16(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL OutReg32(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL Fill32(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);

static BOOL DumpMailboxReg(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL DumpMailboxIrqReg(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL CpuIdle(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL Reboot(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );

#if 0
static BOOL InI2C(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL OutI2C(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL Device_xxx(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL Observe(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL SetContrast(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL OPMode(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);

static BOOL Dump_RegisterGroup(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL TouchScreenCalibrate(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL ScreenRotate(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
static BOOL Display(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );
static BOOL TvOut(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );
static BOOL DVIControl(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );
static BOOL InterruptLatency(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );
static BOOL PowerDomain(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );

extern BOOL ProfileDvfs(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );
extern BOOL ProfileInterruptLatency(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );
extern BOOL ProfileSdrcStall(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );
extern BOOL ProfileWakeupAccuracy(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );
static BOOL HalProfile(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );
#endif

static LONG   GetDec(LPCWSTR string);
static UINT32 GetHex(LPCWSTR string);

#if 0
static void Dump_PRCM(PFN_FmtPuts pfnFmtPuts);
static void Dump_PRCMStates(PFN_FmtPuts pfnFmtPuts);
static void Dump_GPMC(PFN_FmtPuts pfnFmtPuts);
static void Dump_SDRC(PFN_FmtPuts pfnFmtPuts);
static void Dump_DSS(PFN_FmtPuts pfnFmtPuts);
static void Dump_ContextRestore(PFN_FmtPuts pfnFmtPuts);
static void Dump_EFuse(PFN_FmtPuts pfnFmtPuts);

static BOOL SetSlaveAddress(HANDLE hI2C, DWORD address, DWORD mode);
static DWORD WriteI2C(HANDLE hI2C, UINT8  subaddr, VOID*  pBuffer, DWORD  count, DWORD *pWritten);
static DWORD ReadI2C(HANDLE hI2C, UINT8  subaddr, VOID*  pBuffer, DWORD  count, DWORD *pRead);

#if CACHEINFO_ENABLE
static BOOL ShowCacheInfo(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts );
#endif

static BOOL GetNEONStat(ULONG argc,LPWSTR args[],PFN_FmtPuts pfnFmtPuts);

#endif
static BOOL GetOSVersion(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
BOOL SetUSBFn(ULONG argc,LPWSTR args[],PFN_FmtPuts pfnFmtPuts);

HANDLE GetProxyDriverHandle()
{
    static HANDLE _hProxy = NULL;
    if (_hProxy == NULL){
        _hProxy = CreateFile(L"PXY1:", GENERIC_WRITE|GENERIC_READ,
                    0, NULL, OPEN_EXISTING, 0, NULL
                    );
        if (_hProxy == INVALID_HANDLE_VALUE){
            _hProxy = NULL;
        }
    }

    return _hProxy;
}

//-----------------------------------------------------------------------------

typedef struct {
    DWORD       dwStart;
    DWORD       dwSize;
    void       *pv;
} MEMORY_REGISTER_ENTRY;


static MEMORY_REGISTER_ENTRY _pRegEntries[] = {
    { 0,                            0,                          NULL}
};

void* MmMapIoSpace_Proxy( PHYSICAL_ADDRESS PhysAddr, ULONG size, BOOL bNotUsed )
{
    MEMORY_REGISTER_ENTRY *pRegEntry = _pRegEntries;
    void *pAddress = NULL;
    UINT64 phSource;
    UINT32 sourceSize, offset;
    BOOL rc;
    VIRTUAL_COPY_EX_DATA data;

	UNREFERENCED_PARAMETER(bNotUsed);

    // Check if we can use common mapping for device registers (test is
    // simplified as long as we know that we support only 32 bit addressing).
    //
    if (PhysAddr.HighPart == 0) {
        while (pRegEntry->dwSize > 0){
            if (pRegEntry->dwStart <= PhysAddr.LowPart &&
                (pRegEntry->dwStart + pRegEntry->dwSize) > PhysAddr.LowPart){

                // check if memory is already mapped to the current process space
                rc = TRUE;
                if (pRegEntry->pv == NULL){
                    // reserve virtual memory and map it to a physical address
                    pRegEntry->pv = VirtualAlloc(0, pRegEntry->dwSize, MEM_RESERVE,
                                        PAGE_READWRITE | PAGE_NOCACHE);

                    if (pRegEntry->pv == NULL){
                        DEBUGMSG(TRUE, (L"ERROR: MmMapIoSpace_Proxy failed reserve registers memory\r\n"));
                        goto cleanUp;
                    }

                    // Populate IOCTL parameters
                    data.idDestProc = GetCurrentProcessId();
                    data.pvDestMem = pRegEntry->pv;
                    data.physAddr = pRegEntry->dwStart;
                    data.size = pRegEntry->dwSize;
                    rc = DeviceIoControl(GetProxyDriverHandle(), IOCTL_VIRTUAL_COPY_EX,
                            &data, sizeof(VIRTUAL_COPY_EX_DATA), NULL, 0,
                            NULL, NULL);

                }

                if (!rc){
                    DEBUGMSG(TRUE, (L"ERROR: MmMapIoSpace_Proxy failed allocate registers memory\r\n"));
                    VirtualFree(pRegEntry->pv, 0, MEM_RELEASE);
                    pRegEntry->pv = NULL;
                    goto cleanUp;
                }

                // Calculate offset
                offset = PhysAddr.LowPart - pRegEntry->dwStart;
                pAddress = (void*)((UINT32)pRegEntry->pv + offset);
                break;
            }

            // check next register map entry
            ++pRegEntry;
        }
    }

    if (pAddress == NULL){
        phSource = PhysAddr.QuadPart & ~(PAGE_SIZE - 1);
        sourceSize = size + (PhysAddr.LowPart & (PAGE_SIZE - 1));

        pAddress = VirtualAlloc(0, sourceSize, MEM_RESERVE, PAGE_READWRITE | PAGE_NOCACHE);
        if (pAddress == NULL){
            DEBUGMSG(TRUE, (L"ERROR: MmMapIoSpace_Proxy failed reserve memory\r\n"));
            goto cleanUp;
        }

        // Populate IOCTL parameters
        data.idDestProc = GetCurrentProcessId();
        data.pvDestMem = pAddress;
        data.physAddr = (UINT)phSource;
        data.size = sourceSize;
        rc = DeviceIoControl(GetProxyDriverHandle(), IOCTL_VIRTUAL_COPY_EX,
                &data, sizeof(VIRTUAL_COPY_EX_DATA), NULL, 0,NULL, NULL);

        if (!rc){
            DEBUGMSG(TRUE, (L"ERROR: MmMapIoSpace_Proxy failed allocate memory\r\n"));
            VirtualFree(pAddress, 0, MEM_RELEASE);
            pAddress = NULL;
            goto cleanUp;
        }
        pAddress = (void*)((UINT)pAddress + (UINT)(PhysAddr.LowPart & (PAGE_SIZE - 1)));
    }

cleanUp:
    return pAddress;
}

//-----------------------------------------------------------------------------
void MmUnmapIoSpace_Proxy( void *pAddress, UINT  count )
{
	UNREFERENCED_PARAMETER(pAddress);
	UNREFERENCED_PARAMETER(count);
}

//-----------------------------------------------------------------------------

static struct {
    LPCWSTR cmd;
    BOOL (*pfnCommand)(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts);
} s_cmdTable[] = {
    { L"?", Help },
    { L"in8", InReg8 },
    { L"in16", InReg16 },
    { L"in32", InReg32 },
    { L"out8", OutReg8 },
    { L"out16", OutReg16 },
    { L"out32", OutReg32 },
    { L"fill32", Fill32 },
    { L"mailbox", DumpMailboxReg},
    { L"mailboxirq", DumpMailboxIrqReg},
    { L"usbFnSet", SetUSBFn },
    { L"OSversion", GetOSVersion},    
    { L"cpuidle", CpuIdle},        
    { L"reboot", Reboot},
};

//-----------------------------------------------------------------------------

BOOL ParseCommand( LPWSTR cmd, LPWSTR cmdLine, PFN_FmtPuts pfnFmtPuts, PFN_Gets pfnGets )
{
    BOOL rc = FALSE;
    LPWSTR argv[64];
    int argc = 64;
    ULONG ix;

	UNREFERENCED_PARAMETER(pfnGets);

    // Look for command
    for (ix = 0; ix < dimof(s_cmdTable); ix++)
        if (wcscmp(cmd, s_cmdTable[ix].cmd) == 0) break;

	if (ix >= dimof(s_cmdTable)) goto cleanUp;

    // Divide command line to token
    if (cmdLine != NULL)
        CommandLineToArgs(cmdLine, &argc, argv);
    else
        argc = 0;

    // Call command
    pfnFmtPuts(L"\r\n");
    __try {
        if (!s_cmdTable[ix].pfnCommand(argc, argv, pfnFmtPuts)) {
            pfnFmtPuts(L"\r\n");
            Help(0, NULL, pfnFmtPuts);
        }
    } __except (EXCEPTION_EXECUTE_HANDLER) {
        pfnFmtPuts(L"exception: please check addresses and values\r\n");
    }
    pfnFmtPuts(L"\r\n");
    rc = TRUE;

cleanUp:
    return rc;
}

//-----------------------------------------------------------------------------

BOOL Help( ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts )
{
	UNREFERENCED_PARAMETER(args);
	UNREFERENCED_PARAMETER(argc);

    pfnFmtPuts(L"  in8    address [size] -> read 8-bit registers\r\n");
    pfnFmtPuts(L"  in16   address [size] -> read 16-bit registers\r\n");
    pfnFmtPuts(L"  in32   address [size] -> read 32-bit registers\r\n");
    pfnFmtPuts(L"  out8   address value  -> write 8-bit register\r\n");
    pfnFmtPuts(L"  out16  address value  -> write 16-bit register\r\n");
    pfnFmtPuts(L"  out32  address value  -> write 32-bit register\r\n");
    pfnFmtPuts(L"  fill32 address size value  -> Fill specified address range with 32bit value\r\n");
    pfnFmtPuts(L"  mailbox -> Dump all mailbox registers\r\n");
    pfnFmtPuts(L"  mailboxirq -> Dump all mailbox registers except the mailbox msg regs\r\n");
    pfnFmtPuts(L"  usbFnSet [serial/rndis/storage [<storage_device_name ex: DSK1:>]] -> Change current USB Function Client. For storage, you can optionally specify device name (along with the colon at the end)\r\n");  
    pfnFmtPuts(L"  OSversion -> Get OS version information\r\n");    
    pfnFmtPuts(L"  cpuidle -> display amount of time spent in OEMIdle\r\n");        
    pfnFmtPuts(L"  reboot -> software reset of device\r\n");
    pfnFmtPuts(L"\r\n");
    pfnFmtPuts(L"    Address, size and value are hex numbers.\r\n");
    return TRUE;
}


//-----------------------------------------------------------------------------

BOOL
GetOSVersion(
    ULONG argc,
    LPWSTR args[],
    PFN_FmtPuts pfnFmtPuts
    )
{
    BOOL rc = TRUE;
    OSVERSIONINFO osv;
	
    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(args);
    UNREFERENCED_PARAMETER(pfnFmtPuts);

    osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

    if(GetVersionEx(&osv))
    {
        pfnFmtPuts(L" OSMajor=%d, OSMinor=%d\r\n", osv.dwMajorVersion, osv.dwMinorVersion);        
    }
    else
    {
        pfnFmtPuts(L" GetVersionEx return error=%d\r\n", GetLastError());        
    }
    return rc;
}

HANDLE GetUfnController( )
{
    HANDLE hUfn = NULL;
    union {
        BYTE rgbGuidBuffer[sizeof(GUID) + 4]; // +4 since scanf writes longs
        GUID guidBus;
    } u = { 0 };
    LPGUID pguidBus = &u.guidBus;
    LPCTSTR pszBusGuid = _T("E2BDC372-598F-4619-BC50-54B3F7848D35");

    // Parse the GUID
    int iErr = _stscanf(pszBusGuid, SVSUTIL_GUID_FORMAT, SVSUTIL_PGUID_ELEMENTS(&pguidBus));
    if (iErr == 0 || iErr == EOF)
        return INVALID_HANDLE_VALUE;

    // Get a handle to the bus driver
    DEVMGR_DEVICE_INFORMATION di;
    memset(&di, 0, sizeof(di));
    di.dwSize = sizeof(di);
    ce::auto_handle hf = FindFirstDevice(DeviceSearchByGuid, pguidBus, &di);

    if (hf != INVALID_HANDLE_VALUE) {
        hUfn = CreateFile(di.szBusName, GENERIC_READ, FILE_SHARE_READ, NULL,
            OPEN_EXISTING, 0, NULL);
    } else {
        NKDbgPrintfW(_T("No available UsbFn controller!\r\n"));
    }

    return hUfn;
}


DWORD ChangeClient( HANDLE hUfn, LPCTSTR pszNewClient )
{

    if(hUfn == INVALID_HANDLE_VALUE || pszNewClient == NULL)
        return ERROR_INVALID_PARAMETER;


    DWORD dwRet = ERROR_SUCCESS;
    UFN_CLIENT_NAME ucn;
    _tcscpy(ucn.szName, pszNewClient);
    BOOL fSuccess = DeviceIoControl(hUfn, IOCTL_UFN_CHANGE_CURRENT_CLIENT, &ucn, sizeof(ucn), NULL, 0, NULL, NULL);

    if (fSuccess) {
        UFN_CLIENT_INFO uci;
        memset(&uci, 0, sizeof(uci));

        DWORD cbActual;
        fSuccess = DeviceIoControl(hUfn, IOCTL_UFN_GET_CURRENT_CLIENT, NULL, 0, &uci, sizeof(uci), &cbActual, NULL);
        if(fSuccess == FALSE || _tcsicmp(uci.szName, pszNewClient) != 0)
            return ERROR_GEN_FAILURE;

        if (uci.szName[0]) {
            RETAILMSG(1, (L"Changed to client \"%s\"\r\n", uci.szName));
        } else {
            RETAILMSG(1, (L"There is now no current client\r\n"));
        }
    } else {
        dwRet = GetLastError();
        RETAILMSG(1, (L"Could not change to client \"%s\" error %d\r\n", pszNewClient, dwRet));
    }

    return dwRet;
}

BOOL SetUSBFn( ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts )
{
    BOOL rc = TRUE;    

    ce::auto_handle hUfn = GetUfnController();
    if (hUfn == INVALID_HANDLE_VALUE) {        
        return FALSE;
    }

    if (argc>0){
        if (wcscmp(args[0], L"serial") == 0){            
            ChangeClient(hUfn,_T("serial_class"));
        } else if (wcscmp(args[0], L"rndis") == 0) {
            ChangeClient(hUfn,_T("RNDIS"));
        } else if (wcscmp(args[0], L"storage") == 0) { 
            if (argc>1) {
                TCHAR   szRegPath[MAX_PATH] = _T("\\Drivers\\USB\\FunctionDrivers\\Mass_Storage_Class");            
                DWORD dwTemp;
                HKEY hKey = NULL;
                LPWSTR pszDeviceName = args[1];
                DWORD status;
                
                if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, szRegPath, 0, NULL, 0, 0,
                                   NULL, &hKey, &dwTemp) != ERROR_SUCCESS) {
                    return FALSE;
                }
                status = RegSetValueEx(hKey, _T("DeviceName"), 0, REG_SZ, (PBYTE)pszDeviceName,
                                        sizeof(WCHAR)*(wcslen(pszDeviceName) + 1));
                RegCloseKey(hKey);
            }

            ChangeClient(hUfn,_T("mass_storage_class"));
        } else {
            pfnFmtPuts(L"Invalid USB Function type\r\n");
            rc = FALSE;
        }
    }
    return rc;
}

//-----------------------------------------------------------------------------
BOOL InReg8( ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts )
{
    BOOL rc = FALSE;
    UINT32 address = 0;
	UINT32 count = 0;
    PHYSICAL_ADDRESS pa;
    UINT8 *pAddress = NULL;
    WCHAR line[80];
    UINT8 data;
    UINT32 ix, ip;

    if (argc < 1){
        pfnFmtPuts(L"Missing address!\r\n");
        goto cleanUp;
    }

    address = GetHex(args[0]);
	count   = (argc > 1) ? GetHex(args[1]) : 1;

    pa.QuadPart = address;
    pAddress = (UINT8*)MmMapIoSpace_Proxy(pa, count * sizeof(UINT8), FALSE);
    if (pAddress == NULL){
        pfnFmtPuts(L"Failed map physical address 0x%08x to virtual address!\r\n",address);
        goto cleanUp;
    }

    for (ix = 0, ip = 0; ix < count; ix++) {
        data = INREG8(&pAddress[ix]);
        if ((ix & 0x0F) == 0) {
            StringCchPrintf(&line[ip], dimof(line) - ip, L"%08x:", address + ix);
            ip += lstrlen(&line[ip]);
        }
        StringCchPrintf( &line[ip], dimof(line) - ip, L" %02x", data);
        ip += lstrlen(&line[ip]);
        if ((ix & 0x0F) == 0x0F) {
            pfnFmtPuts(line);
            pfnFmtPuts(L"\r\n");
            ip = 0;
        }
    }
    if (ip > 0){
        pfnFmtPuts(line);
        pfnFmtPuts(L"\r\n");
    }

    rc = TRUE;

cleanUp:
    if (pAddress != NULL) MmUnmapIoSpace_Proxy(pAddress, count * sizeof(UINT8));
    return rc;
}

//-----------------------------------------------------------------------------
BOOL InReg16( ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts )
{
    BOOL rc = FALSE;
    UINT32 address = 0;
	UINT32 count = 0;
    PHYSICAL_ADDRESS pa;
    UINT8 *pAddress = NULL;
    WCHAR line[80];
    UINT16 data;
    UINT32 ix, ip;

    if (argc < 1){
        pfnFmtPuts(L"Missing address!\r\n");
        goto cleanUp;
    }

    address = GetHex(args[0]);
	count   = (argc > 1) ? GetHex(args[1]) : 1;

    pa.QuadPart = address;
    pAddress = (UINT8*)MmMapIoSpace_Proxy(pa, count * sizeof(UINT16), FALSE);
    if (pAddress == NULL){
        pfnFmtPuts(L"Failed map physical address 0x%08x to virtual address!\r\n",address);
        goto cleanUp;
    }

    for (ix = 0, ip = 0; ix < count; ix += sizeof(UINT16)) {
        data = INREG16((UINT32)pAddress + ix);
        if ((ix & 0x0F) == 0){
            StringCchPrintf(&line[ip], dimof(line) - ip, L"%08x:", address + ix);
            ip += lstrlen(&line[ip]);
        }
        StringCchPrintf(&line[ip], dimof(line) - ip, L" %04x", data);
        ip += lstrlen(&line[ip]);
        if (((ix + sizeof(UINT16)) & 0x0F) == 0){
            pfnFmtPuts(line);
            pfnFmtPuts(L"\r\n");
            ip = 0;
        }
    }
    if (ip > 0){
        pfnFmtPuts(line);
        pfnFmtPuts(L"\r\n");
    }

    rc = TRUE;

cleanUp:
    if (pAddress != NULL) MmUnmapIoSpace_Proxy(pAddress, count * sizeof(UINT16));
    return rc;
}

//-----------------------------------------------------------------------------
BOOL InReg32( ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts )
{
    BOOL rc = FALSE;
    UINT32 address = 0;
	UINT32 count = 0;
    PHYSICAL_ADDRESS pa;
    UINT8 *pAddress = NULL;
    WCHAR line[80];
    UINT32 data;
    UINT32 ix, ip;

    if (argc < 1) {
        pfnFmtPuts(L"Missing address!\r\n");
        goto cleanUp;
    }

    address = GetHex(args[0]);
	count   = (argc > 1) ? GetHex(args[1]) : 1;

    pa.QuadPart = address;
    pAddress = (UINT8*)MmMapIoSpace_Proxy(pa, count * sizeof(UINT32), FALSE);
    if (pAddress == NULL){
        pfnFmtPuts(L"Failed map physical address 0x%08x to virtual address!\r\n",
            address);
        goto cleanUp;
    }

    for (ix = 0, ip = 0; ix < count; ix += sizeof(UINT32)){
        data = INREG32((UINT32)pAddress + ix);
        if ((ix & 0x0F) == 0){
            StringCchPrintf(&line[ip], dimof(line) - ip, L"%08x:", address + ix);
            ip += lstrlen(&line[ip]);
        }
        StringCchPrintf(&line[ip], dimof(line) - ip, L" %08x", data);
        ip += lstrlen(&line[ip]);
        if (((ix + sizeof(UINT32)) & 0x0F) == 0){
            pfnFmtPuts(line);
            pfnFmtPuts(L"\r\n");
            ip = 0;
        }
    }
    if (ip > 0){
		pfnFmtPuts(line);
        pfnFmtPuts(L"\r\n");
    }

    rc = TRUE;

cleanUp:
    if (pAddress != NULL) MmUnmapIoSpace_Proxy(pAddress, count * sizeof(UINT32));
    return rc;
}

//-----------------------------------------------------------------------------

BOOL OutReg8( ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts )
{
    BOOL rc = FALSE;
    UINT32 address, value;
    PHYSICAL_ADDRESS pa;
    UINT8 *pAddress = NULL;

    if (argc < 2) {
        pfnFmtPuts(L"Address and value required!\r\n");
        goto cleanUp;
    }

    address = GetHex(args[0]);
    value = GetHex(args[1]);

    if (value > 0x0100){
        pfnFmtPuts(L"Value must be in 0x00 to 0xFF range!\r\n");
        goto cleanUp;
    }

    pa.QuadPart = address;
    pAddress = (UINT8*)MmMapIoSpace_Proxy(pa, sizeof(UINT8), FALSE);
    if (pAddress == NULL){
        pfnFmtPuts(L"Failed map physical address 0x%08x to virtual address!\r\n",
            address);
        goto cleanUp;
    }

    OUTREG8(pAddress, value);
    pfnFmtPuts(L"Done, read back: 0x%02x\r\n", INREG8(pAddress));

    rc = TRUE;

cleanUp:
    if (pAddress != NULL) MmUnmapIoSpace_Proxy(pAddress, sizeof(UINT8));
    return rc;
}

//-----------------------------------------------------------------------------
BOOL OutReg16( ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts )
{
    BOOL rc = FALSE;
    UINT32 address, value;
    PHYSICAL_ADDRESS pa;
    UINT16 *pAddress = NULL;

    if (argc < 2) {
        pfnFmtPuts(L"Address and value required!\r\n");
        goto cleanUp;
	}

    address = GetHex(args[0]);
    value = GetHex(args[1]);

    if (value > 0x00010000) {
        pfnFmtPuts(L"Value must be in 0x0000 to 0xFFFF range!\r\n");
        goto cleanUp;
    }

    pa.QuadPart = address;
    pAddress = (UINT16*)MmMapIoSpace_Proxy(pa, sizeof(UINT16), FALSE);
    if (pAddress == NULL){
        pfnFmtPuts(L"Failed map physical address 0x%08x to virtual address!\r\n",
            address);
        goto cleanUp;
    }

    OUTREG16(pAddress, value);
    pfnFmtPuts(L"Done, read back: 0x%04x\r\n", INREG16(pAddress));

    rc = TRUE;

cleanUp:
    if (pAddress != NULL) MmUnmapIoSpace_Proxy(pAddress, sizeof(UINT16));
    return rc;
}

//-----------------------------------------------------------------------------
BOOL OutReg32( ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts )
{
    BOOL rc = FALSE;
    UINT32 address, value;
    PHYSICAL_ADDRESS pa;
    UINT32 *pAddress = NULL;

    if (argc < 2){
        pfnFmtPuts(L"Address and value required!\r\n");
        goto cleanUp;
    }

    address = GetHex(args[0]);
    value = GetHex(args[1]);

    pa.QuadPart = address;
    pAddress = (UINT32*)MmMapIoSpace_Proxy(pa, sizeof(UINT32), FALSE);
    if (pAddress == NULL){
        pfnFmtPuts(L"Failed map physical address 0x%08x to virtual address!\r\n",
            address);
        goto cleanUp;
    }

    OUTREG32(pAddress, value);
    pfnFmtPuts(L"Done, read back: 0x%08x\r\n", INREG32(pAddress));
    rc = TRUE;

cleanUp:
    if (pAddress != NULL) MmUnmapIoSpace_Proxy(pAddress, sizeof(UINT32));
    return rc;
}

//-----------------------------------------------------------------------------
BOOL Fill32( ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts )
{
    BOOL rc = FALSE;
    UINT32 address, size, value, i;
    PHYSICAL_ADDRESS pa;
    UINT32 *pAddress = NULL;

    if (argc < 3) {
        pfnFmtPuts(L"Address, Size and value required!\r\n");
        goto cleanUp;
    }

    address = GetHex(args[0]);
    size = GetHex(args[1]);
    value = GetHex(args[2]);

    pa.QuadPart = address;
    pAddress = (UINT32*)MmMapIoSpace_Proxy(pa, size * sizeof(UINT32), FALSE);
    if (pAddress == NULL) {
        pfnFmtPuts(L"Failed map physical address 0x%08x to virtual address!\r\n",
            address );
        goto cleanUp;
    }

    for (i = 0; i < size; i++){
        OUTREG32(pAddress, value);
        pAddress++;
    }

    pfnFmtPuts(L"Fill done\r\n");
    rc = TRUE;

cleanUp:
    if (pAddress != NULL) MmUnmapIoSpace_Proxy(pAddress, size * sizeof(UINT32));
    return rc;
}

//-----------------------------------------------------------------------------
LONG GetDec( LPCWSTR string )
{
    LONG result;
    UINT32 ix = 0;
    BOOL bNegative = FALSE;

    result = 0;
    while (string != NULL) {
        if ((string[ix] >= L'0') && (string[ix] <= L'9')){
            result = (result * 10) + (string[ix] - L'0');
            ix++;
        } else if( string[ix] == L'-') {
            bNegative = TRUE;
            ix++;
        } else {
            break;
        }
    }
    return (bNegative) ? -result : result;
}   

//-----------------------------------------------------------------------------

UINT32 GetHex( LPCWSTR string )
{
    UINT32 result;
    UINT32 ix = 0;

    result = 0;
    while (string != NULL){
        if ((string[ix] >= L'0') && (string[ix] <= L'9')){
            result = (result << 4) + (string[ix] - L'0');
            ix++;
        } else if ((string[ix] >= L'a') && (string[ix] <= L'f')) {
            result = (result << 4) + (string[ix] - L'a' + 10);
            ix++;
        } else if (string[ix] >= L'A' && string[ix] <= L'F') {
            result = (result << 4) + (string[ix] - L'A' + 10);
            ix++;
        } else {
            break;
        }
    }
    return result;
}

#define DISPLAY_REGISTER_VALUE(base, offset, name)  StringCchPrintf(szBuffer, MESSAGE_BUFFER_SIZE, L#name); \
                                      pfnFmtPuts(szBuffer); \
                                      StringCchPrintf(szBuffer, MESSAGE_BUFFER_SIZE, L": Addr=0x%08x, Offset=0x%04x, Value=0x%08x\r\n", (0x480C8000+offset), offset, INREG32((UINT32 *)((UINT32)base + (UINT32)offset))); \
                                      pfnFmtPuts(szBuffer);

static BOOL DumpMailboxReg(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts)
{
    UINT32 *   pMailboxBase;
    PHYSICAL_ADDRESS pa;
    WCHAR szBuffer[MESSAGE_BUFFER_SIZE];

    pa.QuadPart = 0x480C8000;
    pMailboxBase = (UINT32 *)MmMapIoSpace_Proxy(pa, 0x1000, FALSE);
    StringCchPrintf(szBuffer, MESSAGE_BUFFER_SIZE, L"pMailboxBase =0x%x, sample val=0x%x\r\n", pMailboxBase, INREG32((UINT32)pMailboxBase + (UINT32)0x10));
    pfnFmtPuts(szBuffer);

    DISPLAY_REGISTER_VALUE(pMailboxBase, 0,      "Mailbox Revision        ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x10,   "Mailbox Sysconfig       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x40,   "Mailbox Message 0       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x44,   "Mailbox Message 1       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x48,   "Mailbox Message 2       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x4c,   "Mailbox Message 3       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x50,   "Mailbox Message 4       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x54,   "Mailbox Message 5       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x58,   "Mailbox Message 6       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x5c,   "Mailbox Message 7       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x60,   "Mailbox Message 8       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x64,   "Mailbox Message 9       ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x68,   "Mailbox Message 10      ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x6c,   "Mailbox Message 11      ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x80,   "Mailbox FifoStatus 0    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x84,   "Mailbox FifoStatus 1    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x88,   "Mailbox FifoStatus 2    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x8c,   "Mailbox FifoStatus 3    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x90,   "Mailbox FifoStatus 4    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x94,   "Mailbox FifoStatus 5    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x98,   "Mailbox FifoStatus 6    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x9c,   "Mailbox FifoStatus 7    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xa0,   "Mailbox FifoStatus 8    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xa4,   "Mailbox FifoStatus 9    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xa8,   "Mailbox FifoStatus 10   ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xac,   "Mailbox FifoStatus 11   ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xc0,   "Mailbox MsgStatus 0     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xc4,   "Mailbox MsgStatus 1     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xc8,   "Mailbox MsgStatus 2     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xcc,   "Mailbox MsgStatus 3     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xd0,   "Mailbox MsgStatus 4     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xd4,   "Mailbox MsgStatus 5     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xd8,   "Mailbox MsgStatus 6     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xdc,   "Mailbox MsgStatus 7     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xe0,   "Mailbox MsgStatus 8     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xe4,   "Mailbox MsgStatus 9     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xe8,   "Mailbox MsgStatus 10    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xec,   "Mailbox MsgStatus 11    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x100,  "Mailbox IrqStatus_Raw 0 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x110,  "Mailbox IrqStatus_Raw 1 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x120,  "Mailbox IrqStatus_Raw 2 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x130,  "Mailbox IrqStatus_Raw 3 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x104,  "Mailbox IrqStatus_Clr 0 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x114,  "Mailbox IrqStatus_Clr 1 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x124,  "Mailbox IrqStatus_Clr 2 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x134,  "Mailbox IrqStatus_Clr 3 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x108,  "Mailbox IrqEnable_Set 0 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x118,  "Mailbox IrqEnable_Set 1 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x128,  "Mailbox IrqEnable_Set 2 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x138,  "Mailbox IrqEnable_Set 3 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x10c,  "Mailbox IrqEnable_Clr 0 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x11c,  "Mailbox IrqEnable_Clr 1 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x12c,  "Mailbox IrqEnable_Clr 2 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x13c,  "Mailbox IrqEnable_Clr 3 ");
    return TRUE;
}

static BOOL DumpMailboxIrqReg(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts)
{
    UINT32 *   pMailboxBase;
    PHYSICAL_ADDRESS pa;
    WCHAR szBuffer[MESSAGE_BUFFER_SIZE];

    pa.QuadPart = 0x480C8000;
    pMailboxBase = (UINT32 *)MmMapIoSpace_Proxy(pa, 0x1000, FALSE);
    StringCchPrintf(szBuffer, MESSAGE_BUFFER_SIZE, L"pMailboxBase =0x%x, sample val=0x%x\r\n", pMailboxBase, INREG32((UINT32)pMailboxBase + (UINT32)0x10));
    pfnFmtPuts(szBuffer);

    DISPLAY_REGISTER_VALUE(pMailboxBase, 0,      "Mailbox Revision        ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x80,   "Mailbox FifoStatus 0    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x84,   "Mailbox FifoStatus 1    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x88,   "Mailbox FifoStatus 2    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x8c,   "Mailbox FifoStatus 3    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x90,   "Mailbox FifoStatus 4    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x94,   "Mailbox FifoStatus 5    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x98,   "Mailbox FifoStatus 6    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x9c,   "Mailbox FifoStatus 7    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xa0,   "Mailbox FifoStatus 8    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xa4,   "Mailbox FifoStatus 9    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xa8,   "Mailbox FifoStatus 10   ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xac,   "Mailbox FifoStatus 11   ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xc0,   "Mailbox MsgStatus 0     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xc4,   "Mailbox MsgStatus 1     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xc8,   "Mailbox MsgStatus 2     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xcc,   "Mailbox MsgStatus 3     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xd0,   "Mailbox MsgStatus 4     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xd4,   "Mailbox MsgStatus 5     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xd8,   "Mailbox MsgStatus 6     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xdc,   "Mailbox MsgStatus 7     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xe0,   "Mailbox MsgStatus 8     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xe4,   "Mailbox MsgStatus 9     ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xe8,   "Mailbox MsgStatus 10    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0xec,   "Mailbox MsgStatus 11    ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x100,  "Mailbox IrqStatus_Raw 0 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x110,  "Mailbox IrqStatus_Raw 1 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x120,  "Mailbox IrqStatus_Raw 2 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x130,  "Mailbox IrqStatus_Raw 3 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x104,  "Mailbox IrqStatus_Clr 0 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x114,  "Mailbox IrqStatus_Clr 1 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x124,  "Mailbox IrqStatus_Clr 2 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x134,  "Mailbox IrqStatus_Clr 3 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x108,  "Mailbox IrqEnable_Set 0 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x118,  "Mailbox IrqEnable_Set 1 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x128,  "Mailbox IrqEnable_Set 2 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x138,  "Mailbox IrqEnable_Set 3 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x10c,  "Mailbox IrqEnable_Clr 0 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x11c,  "Mailbox IrqEnable_Clr 1 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x12c,  "Mailbox IrqEnable_Clr 2 ");
    DISPLAY_REGISTER_VALUE(pMailboxBase, 0x13c,  "Mailbox IrqEnable_Clr 3 ");
    return TRUE;
}

#define REG_SHELL_PATH TEXT("Software\\Texas Instruments\\Shell")
BOOL CpuIdle(ULONG argc, LPWSTR args[], PFN_FmtPuts pfnFmtPuts)
{
    
    DWORD _idleLast = 0;
    DWORD _tickLast = 0;
    DWORD idle;
    DWORD tick;
    WCHAR szBuffer[MESSAGE_BUFFER_SIZE];
    
    HKEY hKey = NULL;    
    DWORD size;
    DWORD dwType = REG_DWORD;

	UNREFERENCED_PARAMETER(args);
	UNREFERENCED_PARAMETER(argc);

    //get the last parameters
    if (ERROR_SUCCESS == RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPWSTR)REG_SHELL_PATH, 0, NULL, 
                    REG_OPTION_NON_VOLATILE, 0, NULL, &hKey, NULL))
    {  
        size=sizeof(DWORD);
        if (ERROR_SUCCESS != RegQueryValueEx(hKey, L"CPUIdle_LastTick", 0, &dwType, (BYTE*)&_tickLast, &size))
        {   
            _tickLast=0;
        }                
        size=sizeof(DWORD);
        if (ERROR_SUCCESS != RegQueryValueEx(hKey, L"CPUIdle_LastIdle", 0, &dwType, (BYTE*)&_idleLast, &size))
        {   
            _idleLast=0;
        }        
    }   

    idle = GetIdleTime();
    tick = GetTickCount();

    StringCchPrintf(szBuffer, MESSAGE_BUFFER_SIZE, L"Curr: idle count=0x%08X, tick count=0x%08X\r\n", idle, tick);
    pfnFmtPuts(szBuffer);
    
    StringCchPrintf(szBuffer, MESSAGE_BUFFER_SIZE, L"Last: idle count=0x%08X, tick count=0x%08X\r\n", _idleLast, _tickLast);
    pfnFmtPuts(szBuffer);

    StringCchPrintf(szBuffer, MESSAGE_BUFFER_SIZE, L"The difference from last check is...\r\n");
    pfnFmtPuts(szBuffer);

    StringCchPrintf(szBuffer, MESSAGE_BUFFER_SIZE, L"idle delta=0x%08X, tick delta=0x%08X\r\n", (idle - _idleLast), (tick - _tickLast));
    pfnFmtPuts(szBuffer);

    StringCchPrintf(szBuffer, MESSAGE_BUFFER_SIZE, L"cpu load is %f%%\r\n", (1.0f - ((float)(idle - _idleLast)/(float)(tick - _tickLast))) * 100.0f);
    pfnFmtPuts(szBuffer);

    _idleLast = idle;
    _tickLast = tick;

    //store the last parameters
    if (hKey!=NULL) 
    {
        size=sizeof(DWORD);
        RegSetValueEx(hKey,
                    L"CPUIdle_LastTick",
                    0,
                    dwType,
                    (BYTE*)&_tickLast,
                    size);
        size=sizeof(DWORD);
        RegSetValueEx(hKey,
                    L"CPUIdle_LastIdle",
                    0,
                    dwType,
                    (BYTE*)&_idleLast,
                    size);
        RegCloseKey(hKey);
    }
    return TRUE;
}

//------------------------------------------------------------------------------

BOOL
Reboot(
    ULONG argc,
    LPWSTR args[],
    PFN_FmtPuts pfnFmtPuts
    )
{
    BOOL rc = FALSE;

	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(args);

    pfnFmtPuts( TEXT("Rebooting device\r\n") );    

    rc = KernelIoControl( IOCTL_HAL_REBOOT, NULL, 0, NULL, 0, NULL );
    if( rc == FALSE )
    {
        pfnFmtPuts(L"IOCTL_HAL_REBOOT  failed\r\n");
        goto cleanUp;
    }

cleanUp:
    return rc;
}


//1163
