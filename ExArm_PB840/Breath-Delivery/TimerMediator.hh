#ifndef TimerMediator_HH
#define TimerMediator_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TimerMediator - updates all the timers that are on its list
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/TimerMediator.hhv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 009   By: rpr    Date: 22-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support code review comments.
// 
//  Revision: 008  By:  rpr   Date:  17-Oct-2008    SCR Number: 6435 
//  Project:  840S
//  Description:
//  	Increased NUM_TIMERS to account for RO2MixTimer Timer ref 
//		in support of +20
//
//  Revision: 007   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM maneuver timers.
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By:  syw   Date:  06-Jan-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//       		BiLevel Baseline.  Changed NUM_TIMERS to 16.
//
//  Revision: 004  By:  sp    Date:  29-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection Rework.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  kam   Date:  06-Nov-1995    DR Number:600 
//       Project:  Sigma (R8027)
//       Description:
//             Increased NUM_TIMERS to account for SmSwitchConfirmationTimer
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
class IntervalTimer;

//@ End-Usage

//@ Constant: NUM_TIMERS
//number of interval timers
static const Int32 NUM_TIMERS = 20;

class TimerMediator 
{
  public:
    TimerMediator(void);
    ~TimerMediator(void);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL,
						  const char*       pPredicate = NULL);

    void newCycle(void);
  
  protected:

  private:
    TimerMediator(const TimerMediator&);          // Declared but not implemented
    void operator=(const TimerMediator&);   // Declared but not implemented

    //@ Data-Member: pTimerList_
    // A collection of pointers to all the timers references 
    IntervalTimer* pTimerList_[NUM_TIMERS];

#ifdef SIGMA_UNIT_TEST
public:
   IntervalTimer* getTimerList1(void);
#endif // SIGMA_UNIT_TEST
};


#endif //TimerMediator_HH
