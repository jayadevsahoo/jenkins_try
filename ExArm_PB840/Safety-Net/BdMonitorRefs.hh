
#ifndef BdMonitorRefs_HH
#define BdMonitorRefs_HH

//=====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett Inc. of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor Puritan Bennett Inc. of California.
//
//            Copyright (c) 1996, Nellcor Puritan Bennett Inc.
//=====================================================================

//====================================================================
// Class: BdMonitorRefs - External references for the BD run time safety
//  net tests.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BdMonitorRefs.hhv   25.0.4.1   20 Nov 2013 16:09:24   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 003  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software. 
// 
//  Revision: 002  By:  mnr   Date:  03-Dec-2010    SCR Number: 6671
//  Project:  Xena2
//  Description:
// 		BreathTimeMonitor obsoleted.
// 
//  Revision: 001  By:  kam   Date:  26-Jan-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================


class TimeMonitor;
extern TimeMonitor& RTimeMonitor;

#  if defined(SIGMA_GUI_CPU) || defined(SIGMA_UNIT_TEST) || defined(SIGMA_COMMON)

class DataCorruptionInfo;
extern DataCorruptionInfo& RDataCorruptionInfo;

#  endif

#endif // BdMonitorRefs_HH 

