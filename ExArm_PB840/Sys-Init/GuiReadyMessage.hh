#ifndef GuiReadyMessage_HH
#define GuiReadyMessage_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  GuiReadyMessage - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiReadyMessage.hhv   25.0.4.0   19 Nov 2013 14:33:52   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 29-SEP-1997 DR Number: 2513
//      Project:   Sigma   (R8027)
//      Description:
//         Added GUI FLASH serial number for DataKey restore operation.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
#include "TaskControlMessage.hh"
#include "SigmaState.hh"
#include "PostAgent.hh"
#include "SerialNumber.hh"
//@ End-Usage

 
class GuiReadyMessage : public TaskControlMessage {
  public:
 
    GuiReadyMessage(enum SigmaState state, Boolean service_required);
    GuiReadyMessage(const GuiReadyMessage& rGuiReady);

    ~GuiReadyMessage();

    const char* nameOf(void) const;
    void dispatch(const AppContext& context) const;

    inline SigmaState     getState(void) const;
    inline Boolean        isServiceRequired(void) const;

    inline const PostAgent&     getPostAgent(void) const;
    inline const SerialNumber&  getSerialNumber(void) const;

    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);
 
  private:
    GuiReadyMessage();                        // declared only
    void operator=(const GuiReadyMessage&);   // declared only

    //@ Data-Member:    state_
    //  Reports the node is ready in this state.
    SigmaState       state_;

    //@ Data-Member:    serviceRequired_
    //  Indicates if the node requires service.
    Boolean          serviceRequired_;

    //@ Data-Member:    postAgent_
    //  Contains the PostAgent object from the reporting node
    //  providing access to various states
    const PostAgent  postAgent_;

    //@ Data-Member:    serialNumber_
    //  Contains the SerialNumber found in GUI FLASH
    const SerialNumber  serialNumber_;

};
 
#include "GuiReadyMessage.in"

#endif // GuiReadyMessage_HH
