#pragma once

#include "swat.h"
#include "explorer.h"
// CPowerResetDlg dialog

//enum power_page_status_t {NOT_LOGGED_IN, LOGGED_IN, LOGGIN_ERROR, TIME_OUT, ERROR_GETTING_STATE};

class CPowerResetDlg : public CDialog
{
	DECLARE_DYNAMIC(CPowerResetDlg)

public:
	CPowerResetDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPowerResetDlg();
	bool bToSwitchOnOROff;

// Dialog Data
	enum { IDD = IDD_POWER_RESET_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	int nSeconds;
	CExplorer m_webCtrl;
	bool bSwitchedOn;
	bool bSwitchedOff;
	bool bSwitched;
	virtual BOOL OnInitDialog();
	DECLARE_EVENTSINK_MAP()
	void DocumentCompleteExplorerReset(LPDISPATCH pDisp, VARIANT* URL);
	void* ElemFromID( LPWSTR szID, IID nTypeIID );
	bool LogIn();
	bool CheckLogin();
	bool SwitchOnOff(bool bOnOff);
	power_page_status_t m_SwitchState;
	power_page_status_t GetStatus();
	bool ClickLogInError();



	
};
