#include "stdafx.h"
// UpperBaseContainerView.cpp : implementation file
//

#ifdef SIGMA_GUIPC_CPU
#include "ExArm.h"
#else
#include "main_e600GUI.h"
#endif
#include "UpperBaseContainerView.h"

#include "GuiFoundation.hh"
#include "GuiApp.hh"
// UpperBaseContainerView

IMPLEMENT_DYNCREATE(CUpperBaseContainerView, BaseContainerView)

CUpperBaseContainerView::CUpperBaseContainerView()
{

}

CUpperBaseContainerView::~CUpperBaseContainerView()
{
}

// UpperBaseContainerView drawing
void CUpperBaseContainerView::OnDraw(CDC* pDC)
{
#ifdef SIGMA_GUIPC_CPU
	// Invalidate the base containers
	GuiFoundation::InvalidateBaseContainers();

	// Fire UPPER & LOWER repaint events to GUI-App's queue
	GuiApp::RaiseGuiEventPriority(GuiApp::GUI_REPAINT_UPPER_EVENT,
		GuiApp::GUI_SECOND_HIGHEST_PRIORITY);

	GuiApp::RaiseGuiEventPriority(GuiApp::GUI_REPAINT_LOWER_EVENT,
		GuiApp::GUI_SECOND_HIGHEST_PRIORITY);
#endif
}

// UpperBaseContainerView diagnostics

#ifdef _DEBUG
void CUpperBaseContainerView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CUpperBaseContainerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// UpperBaseContainerView message handlers

void CUpperBaseContainerView::CreateUpperBaseViewCDC(){

#ifdef SIGMA_GUIPC_CPU
	if(((CExArmApp *)AfxGetApp())->ptrUpperCDC_ == NULL) {
		((CExArmApp *)AfxGetApp())->ptrUpperCDC_ = new CClientDC(this);
	}
#else
	if(((Cmain_e600GUIApp *)AfxGetApp())->ptrUpperCDC_ == NULL) {
		((Cmain_e600GUIApp *)AfxGetApp())->ptrUpperCDC_ = new CClientDC(this);
	}
#endif

}
