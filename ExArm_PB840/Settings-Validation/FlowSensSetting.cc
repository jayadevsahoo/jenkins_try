#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  FlowSensSetting - Flow Sensitivity Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the rate of flow (in
//  liters-per-minute) inhaled by a patient that triggers the ventilator to
//  deliver a mandatory or spontaneous breath -- when trigger type is set to
//  "FLOW".  This class inherits from 'BoundedSetting' and provides the
//  specific information needed for representation of flow sensitivity.
//  This information includes the interval and range of this setting's
//  values, and this batch setting's new-patient value (see
//  'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, or dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/FlowSensSetting.ccv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 21-Jan-2009   SCR Number: 6435
//  Project:  840S
//  Description:
//	Default flow sensitivity for neonates changed to 0.5 LPM.
//
//  Revision: 004   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//      *  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  incorporated circuit-specific new-patient values and
//         ranges
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "FlowSensSetting.hh"
#include "TriggerTypeValue.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02129] -- The setting's range ...
//  $[02131] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	0.1f,		// absolute minimum...
	0.0f,		// unused...
	TENTHS,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	20.0f,		// absolute maximum...
	0.1f,		// resolution...
	TENTHS,		// precision...
	&::LAST_NODE_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: FlowSensSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, the value interval
//  get initialized.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FlowSensSetting::FlowSensSetting(void)
: BatchBoundedSetting(SettingId::FLOW_SENS,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  FLOW_SENS_MAX_ID,	// upperBoundId ..
					  FLOW_SENS_MIN_ID)	// lowerBoundId ...
{
	CALL_TRACE("FlowSensSetting()");
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~FlowSensSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FlowSensSetting::~FlowSensSetting(void)
{
	CALL_TRACE("~FlowSensSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	FlowSensSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pTriggerType =
		SettingsMgr::GetSettingPtr(SettingId::TRIGGER_TYPE);

	DiscreteValue  triggerTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			triggerTypeValue = pTriggerType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			triggerTypeValue = pTriggerType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	return((triggerTypeValue == TriggerTypeValue::FLOW_TRIGGER)
		   ? Applicability::CHANGEABLE		// $[TI3]
		   : Applicability::INAPPLICABLE);	// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Calculate the setting's new patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02130] -- new-patient formula ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	FlowSensSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	BoundedValue  newPatient;

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			newPatient.value = 0.5f;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			newPatient.value = 2.0f;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			newPatient.value = 3.0f;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	newPatient.precision = TENTHS;

	return(newPatient);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	FlowSensSetting::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							FLOW_SENS_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	FlowSensSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	BoundedRange::ConstraintInfo  constraintInfo;

	//-------------------------------------------------------------------
	// update maximum constraint...
	//-------------------------------------------------------------------

	constraintInfo.id     = ::FLOW_SENS_MAX_ID;
	constraintInfo.value  = getAbsoluteMaxValue_();
	constraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMaxConstraint(constraintInfo);

	//-------------------------------------------------------------------
	// update minimum constraint...
	//-------------------------------------------------------------------

	constraintInfo.id     = ::FLOW_SENS_MIN_ID;
	constraintInfo.value  = getAbsoluteMinValue_();
	constraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMinConstraint(constraintInfo);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute minimum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic minimum constraint.
//
//  $[02129] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	FlowSensSetting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");

	Real32  absoluteMinValue;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			absoluteMinValue = 0.1f;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2]
			absoluteMinValue = 0.2f;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMinValue);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute maximum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic maximum constraint.
//
//  $[02129] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	FlowSensSetting::getAbsoluteMaxValue_(void) const
{
	CALL_TRACE("getAbsoluteMaxValue_()");

	Real32  absoluteMaxValue;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			absoluteMaxValue = 10.0f;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2]
			absoluteMaxValue = 20.0f;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMaxValue);
}
