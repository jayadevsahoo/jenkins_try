/*****************************************************************************/
/*   Module Name:   report.c                                                 */
/*   Purpose:       API for manipulating the Crystal Reports interface       */
/*                                                                           */
/*                                                                           */
/*   Requirements:                                                           */
/*                                                                           */
/*   Modification History:                                                   */
/*                                                                           */
/*   Description of Change                  By                Date           */
/*   ---------------------                  -----             ------         */
/*   Changed the Alarm840.Log               RHJ               29 Feb,2008    */
/*   to Alrm840.LOG                                                          */
/*                                                                           */
/*   Add the ability to view Alarm Logs     RHJ               27 Nov,2007    */
/*   DisplayReportCB function is modified.                                   */
/*                                                                           */
/*   Display Report function is modified.   Lakshmi(CGS)      01 Dec,2000    */
/*                                                                           */
/*   Selected Reports to preview or         Lakshmi(CGS)      01 Dec,2000    */
/*   delete functions are added.                                             */
/*                                                                           */
/*   PrintTestReport funciton is removed.   Dwarkanath(CGS)   05 Oct,2000    */
/*                                                                           */
/*   Critical Observation during Phase1     Kathirvel(CGS)    27 Sep,2000    */
/* 	 is incorportated.                                                       */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*   $Revision::                                                             */
/*                                                                           */
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <formatio.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>

#include "cpvt_db.h"
#include "cpvtmain.h" // gPanelHandle[]
#include "cvicpreh.h"
#include "dbapi.h"
#include "helpinit.h"
#include "ldb.h"
#include "mainpnls.h"
#include "MsgPopup.h"
#include "registry.h"
#include "version.h"
#include "report.h"


/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#define MAX_ASCII_TEXT_LEN 255

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

static int ReportPanel;
static int SelectReportPanel;
struct SSNdate SNDate1;
extern int Months[];

void DisplayTestPanel(void);
void LaunchNotepadReport(char *fileName);

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:    CheckForNoNameLog                                                */
/*                                                                           */
/* Purpose:  needs description                                               */
/*                                                                           */
/* Example Call:   CheckForNoNameLog(char** path_name,                       */
/*                                   char** file_name,                       */
/*                                   LOGTYPES log);                          */
/*                                                                           */
/* Input:    char**   path_name - name of path where log file is located     */
/*           char**   file_name - name of log file                           */
/*           LOGTYPES log       - selected report                            */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void CheckForNoNameLog(char** path_name, char** file_name, LOGTYPES log)
{
	int dirtypath = 0;
	int dirtyfile = 0;
	char defpath[LOG_MAX_PATH];
	char deffile[LOG_MAX_PATH];

	if(strlen(*path_name) <= 0){
		free(*path_name);
		*path_name = NULL;
	}
	if(strlen(*file_name) <= 0){
		free(*file_name);
		*file_name = NULL;
	}
	if(*path_name == NULL){
		*path_name = malloc(LOG_MAX_PATH);	
		dirtypath = 1;
	}
	if(*file_name == NULL){
		*file_name = malloc(LOG_MAX_PATH);
		dirtyfile = 1;
	}

	switch(log){
		case DIAG_LOG:
			strcpy(defpath, LDBf(Ldb_DiagLog_Def_Path));
			strcpy(deffile, LDBf(Ldb_DiagLog_Def_File));
			break;
		case INFO_LOG:
			strcpy(defpath, LDBf(Ldb_InfoLog_Def_Path));
			strcpy(deffile, LDBf(Ldb_InfoLog_Def_File));
			break;
		case TEST_LOG:
			strcpy(defpath, LDBf(Ldb_TestLog_Def_Path));
			strcpy(deffile, LDBf(Ldb_TestLog_Def_File));
			break;
        case ALARM_LOG:
            strcpy(defpath, LDBf(Ldb_AlarmLog_Def_Path));
            strcpy(deffile, LDBf(Ldb_AlarmLog_Def_File));
            break;
	}

	if(dirtypath){
		strcpy(*path_name, defpath);
	}
	if(dirtyfile){
		strcpy(*file_name, deffile);
	}

	return;
}


/*****************************************************************************/
/* Name:    HandleLogPrint                                                   */
/*                                                                           */
/* Purpose:  Print the specified log file.                                   */
/*                                                                           */
/* Example Call:   int HandleLogPrint(LOGTYPES log, char* file_name);        */
/*                                                                           */
/* Input:    LOGTYPES log       - selected report                            */
/*           char*    file_name - name of log file                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   VAL_EXISTING_FILE_SELECTED  - report generation was successful  */
/*			 !VAL_EXISTING_FILE_SELECTED - error in generating report        */
/*                                                                           */
/*****************************************************************************/
int HandleLogPrint(LOGTYPES log, char* filename)
{
	char* path_name;
	char* file_name;
	int file_begin;
	int selstat;

	path_name = (char *)malloc(LOG_MAX_PATH); // Modified the statement due to Critical Observation.
	file_name = (char *)malloc(LOG_MAX_PATH); // Modified the statement due to Critical Observation.
	

	// retrive the last report directory
	if(TRUE/*GetCSEEnabled()*/){
		filename = strcpy(filename, LDBf(Ldb_CSE_Dir));
		if(SetDir(LDBf(Ldb_CSE_Dir)) == -1){
			MakeDir(LDBf(Ldb_CSE_Dir));
		}
		
		switch(log){
			case DIAG_LOG:
				filename = strcat(filename, LDBf(Ldb_CSE_DiagLog));
				break;
			case TEST_LOG:
				filename = strcat(filename, LDBf(Ldb_CSE_TestLog));
				break;
			case INFO_LOG:
				filename = strcat(filename, LDBf(Ldb_CSE_InfoLog));
				break;
            case ALARM_LOG:
                filename = strcat(filename, LDBf(Ldb_CSE_AlarmLog));
                break;
		}
		selstat = VAL_EXISTING_FILE_SELECTED;
	}
	else{
		switch(log){
			case DIAG_LOG:
				GetLogInfo(path_name, LOG_DIAGPATH_KEY);
				GetLogInfo(file_name, LOG_DIAGFILE_KEY);
				CheckForNoNameLog(&path_name, &file_name, log);
				selstat = FileSelectPopup(path_name, file_name, "",
				                          LDBf(Ldb_Select_Diag_File),
				                          VAL_OK_BUTTON, 0, 0, 1, 0, filename);
				file_begin = FindPattern(filename, 0, strlen(filename), "\\", 0, 1);
				if(file_begin != -1){
					SetLogInfo(filename+file_begin+1, LOG_DIAGFILE_KEY);
					filename[file_begin] = '\0';
					SetLogInfo(filename, LOG_DIAGPATH_KEY);
					filename[file_begin] = '\\';
				}
				break;
			case INFO_LOG:
				GetLogInfo(path_name, LOG_INFOPATH_KEY);
				GetLogInfo(file_name, LOG_INFOFILE_KEY);
				CheckForNoNameLog(&path_name, &file_name, log);
				selstat = FileSelectPopup(path_name, file_name, "",
				                          LDBf(Ldb_Select_SysInfo_file),
				                          VAL_OK_BUTTON, 0, 0, 1, 0, filename);
				file_begin = FindPattern(filename, 0, strlen(filename), "\\", 0, 1);
				if(file_begin!=-1){
					SetLogInfo(filename+file_begin+1, LOG_INFOFILE_KEY);
					filename[file_begin] = '\0';
					SetLogInfo(filename, LOG_INFOPATH_KEY);
					filename[file_begin] = '\\';
				}
				break;
			case TEST_LOG:
				GetLogInfo(path_name, LOG_TESTPATH_KEY);
				GetLogInfo(file_name, LOG_TESTFILE_KEY);
				CheckForNoNameLog(&path_name, &file_name, log);
				selstat = FileSelectPopup(path_name, file_name, "",
				                          LDBf(Ldb_Select_testlog_file),
				                          VAL_OK_BUTTON, 0, 0, 1, 0, filename);
				file_begin = FindPattern(filename, 0, strlen(filename), "\\", 0, 1);
				if(file_begin != -1){
					SetLogInfo(filename + file_begin + 1, LOG_TESTFILE_KEY);
					filename[file_begin] = '\0';
					SetLogInfo(filename, LOG_TESTPATH_KEY);
					filename[file_begin] = '\\';
				}
				break;
            case ALARM_LOG:
                GetLogInfo(path_name, LOG_ALARMPATH_KEY);
                GetLogInfo(file_name, LOG_ALARMFILE_KEY);
                CheckForNoNameLog(&path_name, &file_name, log);
                selstat = FileSelectPopup(path_name, file_name, "",
                                          LDBf(Ldb_Select_alarmlog_file),
                                          VAL_OK_BUTTON, 0, 0, 1, 0, filename);
                file_begin = FindPattern(filename, 0, strlen(filename), "\\", 0, 1);
                if(file_begin != -1){
                    SetLogInfo(filename + file_begin + 1, LOG_ALARMFILE_KEY);
                    filename[file_begin] = '\0';
                    SetLogInfo(filename, LOG_ALARMPATH_KEY);
                    filename[file_begin] = '\\';
                }
                break;
		} // end switch(log)
	}
	free(path_name);
	free(file_name);

	return selstat;
}


/*****************************************************************************/
/* Name:    LaunchNotepadReport                                              */
/*                                                                           */
/* Purpose:  Launches Windows Notepad preset to specified filename           */
/*                                                                           */
/* Example Call:    LaunchNotepadReport("c:\\filename");                     */
/*                                                                           */
/* Input:    char* file_name - name of log file                              */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
void LaunchNotepadReport(char *fileName)
{
	char tempstr[255];

	sprintf(tempstr, "Notepad.exe %s", fileName);
	LaunchExecutable(tempstr);

	return;
}


/*****************************************************************************/
/* Name:    DisplayLogReport                                                 */
/*                                                                           */
/* Purpose:  needs description                                               */
/*                                                                           */
/* Example Call:   void DisplayLogReport(char* file_name);                   */
/*                                                                           */
/* Input:    char* file_name - name of log file                              */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void DisplayLogReport(char* filename)
{
	FILE* LOGFile;
	char buffer[254];
	char Info1[MAX_INFO_LEN];
	char Info2[MAX_INFO_LEN];
	char Info3[MAX_INFO_LEN];
	char Info4[MAX_INFO_LEN];
	char Info5[MAX_INFO_LEN];

	if(RegGetInfoFields(Info1, Info2, Info3, Info4, Info5) == -1){
		CPVTMessagePopup(LDBf(Ldb_Error), LDBf(Ldb_Cant_read_registry), 
		                 OK_ONLY_DLG, NO_HELP);
		return;
	}
	else{
		SetInfo(Info1, Info2, Info3, Info4, Info5, 0);
	}
	LOGFile = fopen(filename, "r");
	if(LOGFile == NULL){
		sprintf(buffer, LDBf(Ldb_Cant_Open_file), filename);
		CPVTMessagePopup(LDBf(Ldb_Error), buffer, OK_ONLY_DLG, NO_HELP);
		return;
	}
	SetMouseCursor(VAL_HOUR_GLASS_CURSOR);
	while(fgets(buffer, 254, LOGFile)){
		WriteLogLine(buffer);
	}
	fclose(LOGFile);
	SetMouseCursor(VAL_DEFAULT_CURSOR);

	#ifdef CRPE_ENABLED
	DisplayReportPreview("c:\\brthlab\\840vts\\report\\logreport.rpt", NULL);
	#endif
	ClearLog();

	return;
}


/*****************************************************************************/
/* Name:    RptCheckItem                                                     */
/*                                                                           */
/* Purpose:  Makes report options check boxes function like radio buttons    */
/*                                                                           */
/* Example Call:   void RptCheckItem(int panel, int control);                */
/*                                                                           */
/* Input:    int panel   - needs description                                 */
/*           int control - the control to set                                */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void RptCheckItem(int panel, int control)
{
	// Each control in the radio button set has an if else group.
	// If it was commited check it otherwise uncheck it.
	// This way there is always one checked.
	if(control == REPORTOPTS_STDCSE)
		SetCtrlVal(panel, REPORTOPTS_STDCSE, 1);
	else
		SetCtrlVal(panel, REPORTOPTS_STDCSE, 0);

	if(control == REPORTOPTS_TESTRESULTS)
		SetCtrlVal(panel, REPORTOPTS_TESTRESULTS, 1);
	else
		SetCtrlVal(panel, REPORTOPTS_TESTRESULTS, 0);

	if(control == REPORTOPTS_DIAGLOG)
		SetCtrlVal(panel, REPORTOPTS_DIAGLOG, 1);
	else
		SetCtrlVal(panel, REPORTOPTS_DIAGLOG, 0);

	if(control == REPORTOPTS_INFOLOG)
		SetCtrlVal(panel, REPORTOPTS_INFOLOG, 1);
	else
		SetCtrlVal(panel, REPORTOPTS_INFOLOG, 0);

	if(control == REPORTOPTS_TESTLOG)
		SetCtrlVal(panel, REPORTOPTS_TESTLOG, 1);
	else
		SetCtrlVal(panel, REPORTOPTS_TESTLOG, 0);

    if(control == REPORTOPTS_ALARMLOG)
    {
		SetCtrlVal(panel, REPORTOPTS_ALARMLOG, 1);
    }
	else
    {
		SetCtrlVal(panel, REPORTOPTS_ALARMLOG, 0);
    }

	return;
}


/*****************************************************************************/
/* Name:    DisplayReport                                                    */
/*                                                                           */
/* Purpose:  Launches the Crystal reports viewer                             */
/*                                                                           */
/*                                                                           */
/* Example Call:   void DisplayReport();                                     */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void DisplayReport(void)
{
	int tempPanelHandle;
	int tempCntrlHandle;
	char filename[MAX_PATHNAME_LEN];
	int checked;

	ReportPanel = LoadPanel(0, LDBf(Ldb_AdminpanelFile), REPORTOPTS);
	SetCtrlAttribute(ReportPanel, REPORTOPTS_OK,   ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(ReportPanel, REPORTOPTS_EXIT, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

	#ifdef CRPE_ENABLED
	//if(!IsReportAvailable())
	#endif
	//	SetCtrlAttribute(ReportPanel, REPORTOPTS_TESTRESULTS, ATTR_DIMMED, TRUE);
	//RFH: for now let's support launching notepad
	//#ifndef CRPE_ENABLED
	//	SetCtrlAttribute(ReportPanel, REPORTOPTS_DIAGLOG, ATTR_DIMMED, TRUE);
	//	SetCtrlAttribute(ReportPanel, REPORTOPTS_TESTLOG, ATTR_DIMMED, TRUE);
	//	SetCtrlAttribute(ReportPanel, REPORTOPTS_INFOLOG, ATTR_DIMMED, TRUE);
	//#endif

	InstallPopup(ReportPanel);
//	while(TRUE){
//		GetUserEvent(1, &tempPanelHandle, &tempCntrlHandle);
//		ProcessSystemEvents();
//		if(tempPanelHandle == ReportPanel){
//			switch(tempCntrlHandle){
//				case(REPORTOPTS_OK):
//					// Handle CSE specific version: Just launch NOTEPAD on specified file
//					if(TRUE/*GetCSEEnabled()*/){
//						GetCtrlVal(ReportPanel, REPORTOPTS_TESTRESULTS, &checked);
//						if(checked){
//						//to display the list of records to preview or delete
//							DisplaySelectReports();
//						}
//			 			GetCtrlVal(ReportPanel, REPORTOPTS_STDCSE, &checked);
//			 			if(checked){
//							LaunchNotepadReport("c:\\brthlab\\840vts\\logs\\72362.log");
//						}
//
//						GetCtrlVal(ReportPanel, REPORTOPTS_DIAGLOG, &checked);
//						if(checked){
//							LaunchNotepadReport("c:\\brthlab\\840vts\\logs\\Sys840.log");
//						}
//
//						GetCtrlVal(ReportPanel, REPORTOPTS_INFOLOG, &checked);
//						if(checked){
//							LaunchNotepadReport("c:\\brthlab\\840vts\\logs\\Info840.log");
//						}
//
//						GetCtrlVal(ReportPanel, REPORTOPTS_TESTLOG, &checked);
//						if(checked){
//							LaunchNotepadReport("c:\\brthlab\\840vts\\logs\\St840.log");
//						}
//					}
//					break;
//				case(REPORTOPTS_EXIT):
//					DiscardPanel(ReportPanel);
//					return;
//				case(REPORTOPTS_STDCSE):
//				case(REPORTOPTS_TESTRESULTS):
//				case(REPORTOPTS_DIAGLOG):
//				case(REPORTOPTS_INFOLOG):
//				case(REPORTOPTS_TESTLOG):
//					RptCheckItem(ReportPanel, tempCntrlHandle);
//					break;
//			} // end switch(tempCntrlHandle)
//			tempPanelHandle = -1;
//		} // end if(tempPanelHandle == ReportPanel)
//	} // end while(TRUE)
	//return;
}


/*****************************************************************************/
/*****************************************************************************/
int CVICALLBACK DisplayReportCB(int panel, int control, int event,
                       void *callbackData, int eventData1, int eventData2)
{
	int checked;

	switch(event){
		case EVENT_COMMIT:
			switch(control){
				case(REPORTOPTS_TESTRESULTS):
				case(REPORTOPTS_STDCSE):
				case(REPORTOPTS_DIAGLOG):
				case(REPORTOPTS_INFOLOG):
				case(REPORTOPTS_TESTLOG):
                case(REPORTOPTS_ALARMLOG):
					RptCheckItem(panel, control);
					break;
				case(REPORTOPTS_OK):
					// Handle CSE specific version: Just launch NOTEPAD on specified file
					if(TRUE/*GetCSEEnabled()*/){
						GetCtrlVal(ReportPanel, REPORTOPTS_TESTRESULTS, &checked);
						if(checked){
						//to display the list of records to preview or delete
							DisplaySelectReports();
						}
						GetCtrlVal(ReportPanel, REPORTOPTS_STDCSE, &checked);
						if(checked){
							LaunchNotepadReport("c:\\brthlab\\840vts\\logs\\72362.log");
						}
			 			
						GetCtrlVal(ReportPanel, REPORTOPTS_DIAGLOG, &checked);
						if(checked){
							LaunchNotepadReport("c:\\brthlab\\840vts\\logs\\Sys840.log");
						}
			 			
						GetCtrlVal(ReportPanel, REPORTOPTS_INFOLOG, &checked);
						if(checked){
							LaunchNotepadReport("c:\\brthlab\\840vts\\logs\\Info840.log");
						}
			 			
						GetCtrlVal(ReportPanel, REPORTOPTS_TESTLOG, &checked);
						if(checked){
							LaunchNotepadReport("c:\\brthlab\\840vts\\logs\\St840.log");
						}

                        GetCtrlVal(ReportPanel, REPORTOPTS_ALARMLOG, &checked);
						if(checked){
							LaunchNotepadReport("c:\\brthlab\\840vts\\logs\\Alrm840.log");
						}
					}
					break;
				case(REPORTOPTS_EXIT):
					DiscardPanel(ReportPanel);
					break;
			} // end switch(control)
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:    DisplaySelectReports                                             */
/*                                                                           */
/* Purpose:  Displays a panel containing a list of reports to be selected    */
/*           for preview or to be deleted.                                   */
/*                                                                           */
/*                                                                           */
/* Example Call:   void DisplaySelectReports(void);                          */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
//static int DisplayReportsSelectDone = FALSE;
void DisplaySelectReports(void)
{
//	int tempPanelHandle;
//	int tempCntrlHandle;
//	int iStatus,iRecCnt; 
	
	SelectReportPanel = LoadPanel (0, LDBf(Ldb_AdminpanelFile), SELECTRPT);
	SetCtrlAttribute(SelectReportPanel, SELECTRPT_PREVIEW, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(SelectReportPanel, SELECTRPT_DELETE,  ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(SelectReportPanel, SELECTRPT_EXIT,    ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	
	//to display the Select report to preview or delete dialog
	InsertReportsList();
//	DisplayReportsSelectDone = FALSE;
	InstallPopup(SelectReportPanel);
//	while(DisplayReportsSelectDone == FALSE){
//		GetUserEvent(0, &tempPanelHandle, &tempCntrlHandle);
//		ProcessSystemEvents();
//		if(tempPanelHandle == SelectReportPanel){
//			switch(tempCntrlHandle){

//			case(SELECTRPT_PREVIEW):
//					//to preview selected reports
//					PreviewReports();
//					break;
							
//			case(SELECTRPT_EXIT):
//					// cancels all selections and return to report dialog
//		 			DiscardPanel(SelectReportPanel);
//					return;
					
//			case(SELECTRPT_DELETE):
//					//to delete selected reports
//					iStatus=DeleteReports();
//					break;
					
//			case(SELECTRPT_LISTRECORDS):
//					//check is there any checked item in the list box
//					GetNumCheckedItems (SelectReportPanel, SELECTRPT_LISTRECORDS, &iRecCnt);
//					switch(iRecCnt)
//					{
//						case 0:
//							SetCtrlAttribute (SelectReportPanel, SELECTRPT_DELETE, ATTR_DIMMED, 1);
//							SetCtrlAttribute (SelectReportPanel, SELECTRPT_PREVIEW, ATTR_DIMMED,1);
//							break;
//						
//						default:
//							SetCtrlAttribute (SelectReportPanel, SELECTRPT_DELETE, ATTR_DIMMED, 0);
//							SetCtrlAttribute (SelectReportPanel, SELECTRPT_PREVIEW, ATTR_DIMMED,0);
//							break;
//					}	
//					break;
			
//			default:
//					break;
					
//			}
//		}	 
//	}
}


/*****************************************************************************/
/*****************************************************************************/
int CVICALLBACK DisplaySelectReportsCB(int panel, int control, int event,
                     void *callbackData, int eventData1, int eventData2)
{
	int iStatus, iRecCnt;

	switch(event){
		case EVENT_COMMIT:
			switch(control){
				case(SELECTRPT_PREVIEW):
					//to preview selected reports
					PreviewReports();
					break;

				case(SELECTRPT_EXIT):
					// cancels all selections and return to report dialog
		 			DiscardPanel(SelectReportPanel);
					break;

				case(SELECTRPT_DELETE):
					//to delete selected reports
					iStatus = DeleteReports();
					break;

				case(SELECTRPT_LISTRECORDS):
					//check is there any checked item in the list box
					GetNumCheckedItems(SelectReportPanel, SELECTRPT_LISTRECORDS, &iRecCnt);
					switch(iRecCnt){
						case 0:
							SetCtrlAttribute(SelectReportPanel, SELECTRPT_DELETE,  ATTR_DIMMED, 1);
							SetCtrlAttribute(SelectReportPanel, SELECTRPT_PREVIEW, ATTR_DIMMED, 1);
							break;

						default:
							SetCtrlAttribute(SelectReportPanel, SELECTRPT_DELETE,  ATTR_DIMMED, 0);
							SetCtrlAttribute(SelectReportPanel, SELECTRPT_PREVIEW, ATTR_DIMMED, 0);
							break;
					} // end switch(iRecCnt)
					break;

				default:
					break;
			} // end switch(control)
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:    DeleteReports                                                    */
/*                                                                           */
/* Purpose:  Deletes the selected reports from the Database                  */
/*                                                                           */
/*                                                                           */
/* Example Call:   void DeleteReports(void);                                 */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
int DeleteReports(void)
{
	int iYesNo, iRecCnt, iIndex, iCheckItem, iSeqID, iStatus;

	iYesNo = CPVTConfirmPopup(LDBf(Ldb_Reports_Delete_title), LDBf(Ldb_Reports_Sure_Delete), 
	                          OK_CANCEL_DLG, NO_HELP);
	switch(iYesNo){
		case 1:
			//get number of reports in the
			GetNumListItems(SelectReportPanel, SELECTRPT_LISTRECORDS, &iRecCnt);
			for(iIndex = 0; iIndex < iRecCnt; iIndex++){
				//if any report is checked then delete from the database and list box 
				IsListItemChecked(SelectReportPanel, SELECTRPT_LISTRECORDS, iIndex, &iCheckItem);
				if(iCheckItem){
					GetValueFromIndex(SelectReportPanel, SELECTRPT_LISTRECORDS, iIndex, &iSeqID);
					iStatus = DeleteSelectedSequence(iSeqID);
					if(!iStatus){
						return 0;
					}
					DeleteListItem(SelectReportPanel, SELECTRPT_LISTRECORDS, iIndex, 1);
					GetNumListItems(SelectReportPanel, SELECTRPT_LISTRECORDS, &iRecCnt);

					if(!iRecCnt){
						SetCtrlAttribute(SelectReportPanel, SELECTRPT_DELETE,  ATTR_DIMMED, 1);
						SetCtrlAttribute(SelectReportPanel, SELECTRPT_PREVIEW, ATTR_DIMMED, 1);
						return 1;
					}
					else{
						if(!iIndex){
							iIndex--;
						}
					}
				}
			}
			//to check default to be recent report
			IsListItemChecked(SelectReportPanel, SELECTRPT_LISTRECORDS, 0, &iCheckItem);
			if(!iCheckItem){
				CheckListItem(SelectReportPanel, SELECTRPT_LISTRECORDS, 0, 1);
			}
			break;

   		case 0:
			break;
		
		default:
			break;
	}
	return 1;
}


/*****************************************************************************/
/* Name:    PreviewReports                                                   */
/*                                                                           */
/* Purpose:  Launches the Crystal Report Preview.                            */
/*                                                                           */
/*                                                                           */
/* Example Call:   void PreviewReports(void);                                */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void PreviewReports(void) 
{
	int iIndex, iRecCnt, iCheckItem;
	int iSeqID;
	char Info1[MAX_INFO_LEN];
	char Info2[MAX_INFO_LEN];
	char Info3[MAX_INFO_LEN];
	char Info4[MAX_INFO_LEN];
	char Info5[MAX_INFO_LEN];

	//clear info table
	ClearInfoSet();
	GetNumListItems(SelectReportPanel, SELECTRPT_LISTRECORDS, &iRecCnt);
	for(iIndex = 0; iIndex < iRecCnt; iIndex++){
		IsListItemChecked(SelectReportPanel, SELECTRPT_LISTRECORDS, iIndex, &iCheckItem);
		if(iCheckItem){
			GetValueFromIndex(SelectReportPanel, SELECTRPT_LISTRECORDS, iIndex, &iSeqID);
			if(RegGetInfoFields(Info1, Info2, Info3, Info4, Info5) == -1){
				SetInfo("", "", "", "", "", iSeqID);
			}
			else{
				SetInfo(Info1, Info2, Info3, Info4, Info5, iSeqID);
			}
		}
	}
	#ifdef CRPE_ENABLED
	DisplayReportPreview(LDBf(Ldb_rpt_FileName), LDBf(Ldb_DB_Name));
	#endif

	return;
}


/*****************************************************************************/
/* Name:     InsertReportsList                                               */
/*                                                                           */
/* Purpose:  insert the reports in list box control.                         */
/*                                                                           */
/*                                                                           */
/* Example Call:   void InsertReportsList(void);                             */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           SNDate1- A structure used to hold                               */
/*                    the sequence id, UUT serial Number and date            */
/*                                                                           */
/*                                                                           */
/* Return:   none                                                            */
/*                                                                           */
/*****************************************************************************/
void InsertReportsList(void)
{
	int iId, iIndex, iStatus, iRecCnt = 1, iHour;
	char cBuf[300], cBuf1[8], cBuf2[8] = "AM";


	// disable the delete preview buttons	
	SetCtrlAttribute(SelectReportPanel, SELECTRPT_DELETE,  ATTR_DIMMED, 1);
	SetCtrlAttribute(SelectReportPanel, SELECTRPT_PREVIEW, ATTR_DIMMED, 1);
	//clear the list box
	ClearListCtrl (SelectReportPanel, SELECTRPT_LISTRECORDS);

	//insert the reports in to the list box
	SNDate1 = SelectJetSeqSN(iRecCnt);
	iIndex = 0;
	while(SNDate1.ID != 0){
		strcpy(cBuf, LDBf(Months[atoi(strtok(SNDate1.Date, "/"))-1])); 
		strcat(cBuf, ". ");			     
		strcpy(cBuf1, strtok(NULL, "/"));
		if(strlen(cBuf1) == 1){
			strcat(cBuf, "0");
		}
		strcat(cBuf, cBuf1);
		strcat(cBuf, ", ");
		strcat(cBuf, strtok(NULL, " "));
		strcat(cBuf, "  ");
		strcpy(cBuf1, strtok(NULL, ":"));
		//to display time in standard format
		iHour = atoi(cBuf1)-12;
		if(iHour >= 0){
			strcpy(cBuf2, "PM");
			if(!iHour){
				iHour = atoi(cBuf1);
			}
		}
		else{
			strcpy(cBuf2, "AM");
			if(iHour == -12){
				iHour = 12;
			}
			else{
				iHour = atoi(cBuf1);
			}
		}
		sprintf(cBuf1, "%d", iHour);

		if(strlen(cBuf1) == 1){
			strcat(cBuf, "0");
		}
		strcat(cBuf, cBuf1);
		strcat(cBuf, ":");
		strcpy(cBuf1, strtok(NULL, ":"));
		if(strlen(cBuf1) == 1){
			strcat(cBuf, "0");
		}
		strcat(cBuf, cBuf1);
		strcat(cBuf, ":");
		strcpy(cBuf1, strtok(NULL, " "));
		if(strlen(cBuf1) == 1){
			strcat(cBuf, "0");
		}
		strcat(cBuf, cBuf1);
		strcat(cBuf, " ");
		strcat(cBuf, cBuf2);
		strcat(cBuf, "  ");
		strcat(cBuf, SNDate1.UUTSN);
		InsertListItem(SelectReportPanel, SELECTRPT_LISTRECORDS, iIndex, cBuf, SNDate1.ID);
		IsListItemChecked(SelectReportPanel, SELECTRPT_LISTRECORDS, 0, &iStatus);
		if(!iStatus){
			CheckListItem(SelectReportPanel, SELECTRPT_LISTRECORDS, 0, 1);
			SetCtrlAttribute(SelectReportPanel, SELECTRPT_DELETE,  ATTR_DIMMED, 0);
			SetCtrlAttribute(SelectReportPanel, SELECTRPT_PREVIEW, ATTR_DIMMED, 0);
		}

		iIndex = iIndex +1;
		iRecCnt = iRecCnt +1;
		SNDate1 = SelectJetSeqSN(iRecCnt);
	} // end while(SNDate1.ID != 0)

	return;
}
