#include "stdafx.h"
//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#include"Actions.h"
#include"BatchExecuter.h"
#include<stdio.h>

#include"XReal.h"
#include"VirtualSensor.h"
#include"SwatBuffer.h"
#include"SwatGui.h"

namespace swat
{

    /////////////////////////////////////////////////////////////
    /// @class WaitEvent
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::waitevent  (unsigned int iEventId, unsigned int iTimeExpire);
    /// <ul>
    /// <li>iEventId    GetUIArg(0)
    /// <li>iTimeExpire GetUIArg(1)
    /// </ul>
    /////////////////////////////////////////////////////////////
    int WaitEvent::waitevent_continue(BatchExecuter *pCMgr)
    {

        WaitState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ws;

        //If Event has occured.
        if (current_context.m_iEventStatus
            != EventManager::getEventState(current_context.m_iEventId))
        {
            return BatchExecuter::STATE_CORETURN_CONTINUE;
        }

        if (current_context.m_bEventHasTimeout)
        {
            //If timeout has occured
            if (current_context.m_WaitTime <= BatchExecuter::m_iTime)
            {
                return BatchExecuter::STATE_CORETURN_TIMEOUT;
            }
        }

        return BatchExecuter::STATE_WAIT;
    }

    inline int WaitEvent::doAction(BatchExecuter *pCMgr)
    {

        unsigned int event_id = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0);

        unsigned int time = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(1);

        //If not able to push the context, return STATE_ERROR
        if (pCMgr->pushContext(waitevent_continue) == false)
        {
            return BatchExecuter::STATE_ERROR;
        }

        WaitState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ws;

        current_context.m_bEventHasTimeout = (time != 0);
        current_context.m_WaitTime = BatchExecuter::m_iTime + time;
        current_context.m_iEventId = event_id;
        current_context.m_iEventStatus = EventManager::getEventState(event_id);

        return BatchExecuter::STATE_WAIT;
    }

    /////////////////////////////////////////////////////////////
    /// @class WaitTime
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::waittime   (unsigned int iTime );
    /// <ul>
    /// <li>iTime GetUIArg(0)
    /// </ul>
    /// @note ME - 08/08/2010 Tested and Reviewed
    /////////////////////////////////////////////////////////////
    int WaitTime::waittime_continue(BatchExecuter *pCMgr)
    {

        WaitState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ws;

        // If timeout has occured.
        if (current_context.m_WaitTime <= BatchExecuter::m_iTime)
        {
            return BatchExecuter::STATE_CORETURN_CONTINUE;
        }

        return BatchExecuter::STATE_WAIT;
    }

    inline int WaitTime::doAction(BatchExecuter *pCMgr)
    {

        unsigned int time = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0);

        if (time == 0)
        {
            return BatchExecuter::STATE_CONTINUE;
        }

        //If not able to push the context, return STATE_ERROR
        if (pCMgr->pushContext(waittime_continue) == false)
        {
            return BatchExecuter::STATE_ERROR;
        }

        WaitState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ws;

        current_context.m_WaitTime = BatchExecuter::m_iTime + time;

        return BatchExecuter::STATE_WAIT;
    }

    /////////////////////////////////////////////////////////////
    /// @class RepeatN
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::repeatn    (unsigned int iCount);
    /// <ul>
    /// <li>iCount GetUIArg(0)
    /// </ul>
    /// @note ME - 08/08/2010 Tested and Reviewed
    /////////////////////////////////////////////////////////////
    inline int RepeatN::doAction(BatchExecuter *pCMgr)
    {
        MarkState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ms;

        unsigned int iArg = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0);

        current_context.iMarkCount++;

        // If iArg counts have occured.
        if (current_context.iMarkCount >= iArg)
        {
            return BatchExecuter::STATE_CORETURN_CONTINUE;
        }

        pCMgr->m_iBatchCurrentIndex = current_context.m_iMark;
        return BatchExecuter::STATE_CONTINUE;
    }

    /////////////////////////////////////////////////////////////
    /// @class RepeatTime
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::repeattime (unsigned int iTime );
    /// <ul>
    /// <li>iTime GetUIArg(0)
    /// </ul>
    /// @note ME - 08/08/2010 Tested and Reviewed
    /////////////////////////////////////////////////////////////
    inline int RepeatTime::doAction(BatchExecuter *pCMgr)
    {
        MarkState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ms;

        TimeStamp iArg = current_context.m_WaitTime
            + pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0);

        //If Timeout has occured
        if (BatchExecuter::m_iTime >= iArg)
        {
            return BatchExecuter::STATE_CORETURN_CONTINUE;
        }

        pCMgr->m_iBatchCurrentIndex = current_context.m_iMark;
        return BatchExecuter::STATE_CONTINUE;
    }

    /////////////////////////////////////////////////////////////
    /// @class RepeatEvent
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::repeatevent(unsigned int iEventId, unsigned int iTimeExpire);
    /// <ul>
    /// <li>iEventId     GetUIArg(0)
    /// <li>iTimeExpire  GetUIArg(1)
    /// </ul>
    /////////////////////////////////////////////////////////////
    inline int RepeatEvent::doAction(BatchExecuter *pCMgr)
    {
        MarkState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ms;

        unsigned int iEventId = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0);

        //Capture first event id not already captured
        if (current_context.bEventCaptured == false)
        {
            current_context.bEventCaptured = true;
            current_context.i_EventState = EventManager::getEventState(iEventId);
        }

        //If Event has occured
        else if (current_context.i_EventState != EventManager::getEventState(iEventId))
        {
            return BatchExecuter::STATE_CORETURN_CONTINUE;
        }

        //If timeout value was set
        if (pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(1) != 0)
        {
            TimeStamp iArg = current_context.m_WaitTime
                + pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(1);

            //If Timeout occured
            if (BatchExecuter::m_iTime >= iArg)
            {
                return BatchExecuter::STATE_CORETURN_TIMEOUT;
            }
        }

        //Do it over again
        pCMgr->m_iBatchCurrentIndex = current_context.m_iMark;
        return BatchExecuter::STATE_CONTINUE;

    }

    /////////////////////////////////////////////////////////////
    /// @class MarkEvent
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::mark (unsigned int iEventId);
    /// <ul>
    /// <li>iEventId GetUIArg(0)
    /// </ul>
    /////////////////////////////////////////////////////////////
    inline int MarkEvent::doAction(BatchExecuter *pCMgr)
    {
        unsigned int iEventId = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0);

        //If not able to push the context, return STATE_ERROR.
        if (pCMgr->pushContext(DispatchContext::continueAction) == false)
        {
            return BatchExecuter::STATE_ERROR;
        }

        MarkState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ms;
        current_context.m_iMark = pCMgr->m_iBatchCurrentIndex;
        current_context.iMarkCount = 0;
        current_context.m_WaitTime = BatchExecuter::m_iTime;

        current_context.bEventCaptured = true;
        current_context.i_EventState = EventManager::getEventState(iEventId);

        //Copy index from previous state.
        if (pCMgr->m_iCurrentContextIndex > 1)
        {
            current_context.m_buffer_index
                = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex - 1].ms.m_buffer_index;
        }

        return BatchExecuter::STATE_CONTINUE;

    }

    /////////////////////////////////////////////////////////////
    /// @class Mark
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::mark();
    /// @note ME - 08/08/2010 Tested and Reviewed
    /////////////////////////////////////////////////////////////
    inline int Mark::doAction(BatchExecuter *pCMgr)
    {

        //If not able to push the context, return STATE_ERROR
        if (pCMgr->pushContext(DispatchContext::continueAction) == false)
        {
            return BatchExecuter::STATE_ERROR;
        }

        MarkState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ms;
        current_context.m_iMark = pCMgr->m_iBatchCurrentIndex;
        current_context.iMarkCount = 0;
        current_context.m_WaitTime = BatchExecuter::m_iTime;
        current_context.bEventCaptured = false;

        //Copy index from previous state.
        if (pCMgr->m_iCurrentContextIndex > 1)
        {
            current_context.m_buffer_index
                = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex - 1].ms.m_buffer_index;
        }

        return BatchExecuter::STATE_CONTINUE;
    }

    /////////////////////////////////////////////////////////////
    /// @class SetSensor
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::setsensor  (unsigned int iSensorId, float fValue);
    /// <ul>
    /// <li>iSensorId GetUIArg(0)
    /// <li>fValue    GetFArg (1)
    /// </ul>
    /// @note ME - 08/08/2010 Tested and Reviewed
    /////////////////////////////////////////////////////////////
    inline int SetSensor::doAction(BatchExecuter *pCMgr)
    {
        XRealBase::set(pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0),
                       pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetFArg(1));

        return BatchExecuter::STATE_CONTINUE;
    }


    /////////////////////////////////////////////////////////////
    /// @class SetRawSensor
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::setsensor  (unsigned int iSensorId, int iValue);
    /// <ul>
    /// <li>iSensorId GetUIArg(0)
    /// <li>iValue    GetFArg (1)
    /// </ul>
    /////////////////////////////////////////////////////////////
    inline int SetRawSensor::doAction(BatchExecuter *pCMgr)
    {
        XRawBase::set(pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0),
                       pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(1));

        return BatchExecuter::STATE_CONTINUE;
    }


    /////////////////////////////////////////////////////////////
    /// @class AddSensor
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::setsensor  (unsigned int iSensorId, float fValue);
    /// <ul>
    /// <li>iSensorId GetUIArg(0)
    /// <li>fValue    GetFArg (1)
    /// </ul>
    /// @note ME - 08/08/2010 Tested and Reviewed
    /////////////////////////////////////////////////////////////
    inline int AddSensor::doAction(BatchExecuter *pCMgr)
    {
        XRealBase::add(pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0),
                       pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetFArg(1));

        return BatchExecuter::STATE_CONTINUE;
    }


    /////////////////////////////////////////////////////////////
    /// @class AddRawSensor
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::setsensor  (unsigned int iSensorId, int iValue);
    /// <ul>
    /// <li>iSensorId GetUIArg(0)
    /// <li>iValue    GetIArg (1)
    /// </ul>
    /////////////////////////////////////////////////////////////
    inline int AddRawSensor::doAction(BatchExecuter *pCMgr)
    {
        XRawBase::add(pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0),
                       pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetIArg(1));

        return BatchExecuter::STATE_CONTINUE;
    }


	// Simulate a touch on GUI
    inline int SimulateGuiTouch::doAction(BatchExecuter *pCMgr)
    {

		int x = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetIArg(0);
		int y = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetIArg(1);

		SwatGui::GetInstance()->simulateTouch( x, y );

        return BatchExecuter::STATE_CONTINUE;
    }


	// Simulate a Bezel key press on GUI
	inline int SimulateGuiBezelKey::doAction(BatchExecuter *pCMgr)
    {

		int keyId = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetIArg(0);
		int onOff = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetIArg(1);

		SwatGui::GetInstance()->simulateBezelKey( keyId, onOff );

        return BatchExecuter::STATE_CONTINUE;
    }


    /////////////////////////////////////////////////////////////
    /// @class UnsetSensor
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::unsetsensor(unsigned int iSensorId);
    /// <ul>
    /// <li>iSensorId GetUIArg(0)
    /// </ul>
    /// @note ME - 08/08/2010 Tested and Reviewed
    /////////////////////////////////////////////////////////////
    inline int UnsetSensor::doAction(BatchExecuter *pCMgr)
    {
        XRealBase::unset(pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0));
        return BatchExecuter::STATE_CONTINUE;
    }


    /////////////////////////////////////////////////////////////
    /// @class UnsetRawSensor
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::unsetsensor(unsigned int iSensorId);
    /// <ul>
    /// <li>iSensorId GetUIArg(0)
    /// </ul>
    /// @note ME - 08/08/2010 Tested and Reviewed
    /////////////////////////////////////////////////////////////
    inline int UnsetRawSensor::doAction(BatchExecuter *pCMgr)
    {
        XRawBase::unset(pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0));
        return BatchExecuter::STATE_CONTINUE;
    }



    /////////////////////////////////////////////////////////////
    /// @class SignalEvent
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::signalevent(unsigned int iEventId);
    /// <ul>
    /// <li>iEventId GetUIArg(0)
    /// </ul>
    /// @note ME - 08/08/2010 Tested and Reviewed
    /////////////////////////////////////////////////////////////
    inline int SignalEvent::doAction(BatchExecuter *pCMgr)
    {
        unsigned int event_id = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0);

        EventManager::signalEvent(event_id);

        return BatchExecuter::STATE_CONTINUE;
    }

    /////////////////////////////////////////////////////////////
    /// @class ResetAll
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::resetall();
    /////////////////////////////////////////////////////////////
    inline int ResetAll::doAction(BatchExecuter *pCMgr)
    {
        return BatchExecuter::STATE_RESETALL;
    }

    /////////////////////////////////////////////////////////////
    /// @class SetSensorBufferValue
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::setsensor  (unsigned int iSensorId);
    /// <ul>
    /// <li>iSensorId GetUIArg(0)
    /// </ul>
    /////////////////////////////////////////////////////////////
    inline int SetSensorBufferValue::doAction(BatchExecuter *pCMgr)
    {
        MarkState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ms;

        XRealBase::set(pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetUIArg(0),
                       SwatBuffer::getBufferData(current_context.m_buffer_index));

        current_context.m_buffer_index++;
        return BatchExecuter::STATE_CONTINUE;
    }

    /////////////////////////////////////////////////////////////
    /// @class SetBufferIndex
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::setindex   (int iIndex);
    /// <ul>
    /// <li>iIndex  GetIArg(0)
    /// </ul>
    /////////////////////////////////////////////////////////////
    inline int SetBufferIndex::doAction(BatchExecuter *pCMgr)
    {
        MarkState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ms;

        current_context.m_buffer_index
            = pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetIArg(0);

        return BatchExecuter::STATE_CONTINUE;
    }

    /////////////////////////////////////////////////////////////
    /// @class SkipBufferIndex
    /// Impliments functionality of the batch routine,
    /// void BatchBuilder::skipindex  (int iIndex);
    /// <ul>
    /// <li>iIndex GetIArg(0)
    /// </ul>
    /////////////////////////////////////////////////////////////
    inline int SkipBufferIndex::doAction(BatchExecuter *pCMgr)
    {
        MarkState &current_context = pCMgr->m_Context[pCMgr->m_iCurrentContextIndex].ms;

        current_context.m_buffer_index
            += pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetIArg(0);

        return BatchExecuter::STATE_CONTINUE;
    }

    /////////////////////////////////////////////////////////////
    /// @class DispatchContext
    /// This class must be at the end of this file in order
    /// for inline to work.
    ///
    /// This class can be optimised for performance.
    /////////////////////////////////////////////////////////////
    int DispatchContext::continueAction(BatchExecuter *pCMgr)
    {

        // As long as the following conditions are met, continue to process
        // batch commands.
        // <ul>
        // <li>There are batch commands to run.
        // <li>The returned result from each action is BatchExecuter::STATE_CONTINUE.
        // <li>The batch commands are valid.
        // <li>The batch command BatchCommand::CMD_END has not been received.
        // </ul>
        //
        // When the command BatchCommand::CMD_END is processed or there are no more commands to process,
        // BatchExecuter::STATE_END is returned.
        // When the command is not valid, BatchExecuter::STATE_INVALID_BATCH_COMMAND is returned.
        for (int iRetval; pCMgr->m_iBatchCurrentIndex < pCMgr->m_iBatchLength; pCMgr->m_iBatchCurrentIndex++)
        {
            switch (pCMgr->m_pclBatchCommand[pCMgr->m_iBatchCurrentIndex].GetCommandId())
            {

                // Returns
                //  BatchExecuter::STATE_CONTINUE
                case BatchCommand::CMD_SETSENSOR:
                    iRetval = SetSensor::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_ADDSENSOR:
                    iRetval = AddSensor::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_UNSETSENSOR:
                    iRetval = UnsetSensor::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_SETSENSORBUFFERVALUE:
                    iRetval = SetSensorBufferValue::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_SETINDEX:
                    iRetval = SetBufferIndex::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_SKIPINDEX:
                    iRetval = SkipBufferIndex::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_SIGNALEVENT:
                    iRetval = SignalEvent::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_MARKEVENT:
                    iRetval = MarkEvent::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_MARK:
                    iRetval = Mark::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_SETRAWSENSOR:
                    iRetval = SetRawSensor::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_UNSETRAWSENSOR:
                    iRetval = UnsetRawSensor::doAction(pCMgr);
                    continue;

                case BatchCommand::CMD_ADDRAWSENSOR:
                    iRetval = AddRawSensor::doAction(pCMgr);
                    continue;

				case BatchCommand::CMD_SIMULATE_TOUCH:
					iRetval = SimulateGuiTouch::doAction(pCMgr);
					continue;

				case BatchCommand::CMD_SIMULATE_BEZEL:
					iRetval = SimulateGuiBezelKey::doAction(pCMgr);
					continue;
                    // Returns
                    //  BatchExecuter::STATE_CONTINUE,
                    //  BatchExecuter::STATE_CORETURN_CONTINUE
                case BatchCommand::CMD_REPEATN:
                    iRetval = RepeatN::doAction(pCMgr);
                    break;

                case BatchCommand::CMD_REPEATTIME:
                    iRetval = RepeatTime::doAction(pCMgr);
                    break;

                    // Returns
                    //  BatchExecuter::STATE_CORETURN_CONTINUE,
                    //  BatchExecuter::STATE_CORETURN_TIMEOUT,
                    //  BatchExecuter::STATE_CONTINUE
                case BatchCommand::CMD_REPEATEVENT:
                    iRetval = RepeatEvent::doAction(pCMgr);
                    break;

                    // Returns
                    //  BatchExecuter::STATE_CONTINUE, if time to wait is zero
                    //  BatchExecuter::STATE_WAIT,
                case BatchCommand::CMD_WAITTIME:
                    iRetval = WaitTime::doAction(pCMgr);
                    break;

                    // Returns
                    //  BatchExecuter::STATE_WAIT;
                case BatchCommand::CMD_WAITEVENT:
                    iRetval = WaitEvent::doAction(pCMgr);
                    break;

                    // Returns
                    //  Other values
                case BatchCommand::CMD_RESETALL:
                    iRetval = ResetAll::doAction(pCMgr);
                    break;

                case BatchCommand::CMD_END:
                    iRetval = BatchExecuter::STATE_END;
                    break;

                default:
                    iRetval = BatchExecuter::STATE_INVALID_BATCH_COMMAND;
                    break;
            }

            if (iRetval != BatchExecuter::STATE_CONTINUE)
            {
                return iRetval;
            }

        }

        return BatchExecuter::STATE_END;
    }

    int DispatchContext::doAction(BatchExecuter *pCMgr)
    {

        //If not able to push the context, return STATE_ERROR
        if (pCMgr->pushContext(DispatchContext::continueAction) == false)
        {
            return BatchExecuter::STATE_ERROR;
        }

        return BatchExecuter::STATE_CONTINUE;
    }

}
