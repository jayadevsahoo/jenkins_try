#ifndef TrendDbSampleRm_HH
#define TrendDbSampleRm_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbSampleRm
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Trend-Database/vcssrc/TrendDbSampleRm.hhv   10.7   08/17/07 11:02:38   pvcs  
//
//@ Modification-Log
//  Revision: 001  By:  ksg    Date:  17-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
// 
//=====================================================================

#include "TrendDbSampleData.hh"



class TrendDbSampleRm : public TrendDbSampleData
{
public:    
	TrendDbSampleRm( PatientDataId::PatientItemId id,
					 TrendSelectValue::TrendSelectValueId trendId,
					 TrendEvents::EventId eventId );
	virtual ~ TrendDbSampleRm();
	virtual void setValue( const TrendValue & newValue,
						   TrendTimeScaleValue::TrendTimeScaleValueId timescale = TrendTimeScaleValue::TREND_TIME_SCALE_1HR );
	virtual void compressData( TrendTimeScaleValue::TrendTimeScaleValueId timescale );

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
protected:

	virtual void clear_( void );

private:
	TrendDbSampleRm( const TrendDbSampleRm& );	// not implemented
	void operator=( const TrendDbSampleRm& );	// not implemented

	//@ Data-Member: eventId_
	// The event which will accept the pending RM value
	TrendEvents::EventId eventId_;
};

#endif // TrendDbSampleRm_HH



