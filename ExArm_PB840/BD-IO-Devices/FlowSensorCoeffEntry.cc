#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FlowSensorCoeffEntry - Defines the information structure for
//      the flow calibration entry as stored in the FlowSensorCalInfo object.
//---------------------------------------------------------------------
//@ Interface-Description
//      The class contains a template for the flow sensor coefficients.
//      The FlowSensorCalInfo is a friend of this class and can directly
//		access the members of this entry.
//---------------------------------------------------------------------
//@ Rationale
//      This class defines the flow sensor coefficient entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This class contains the 3 coefficients (A, B, and C) for
//      converting a voltage into a flow.  It also contains the
//      voltage at which these coefficients were defined.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlowSensorCoeffEntry.ccv   26.0.1.0   04 Feb 2013 10:36:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 005 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004 By: syw    Date: 07-Apr-1998   DR Number: none
//  	Project:  Sigma (840)
//		 	BiLevel initial version.  Added formatting to write() method.
//
//  Revision: 003  By: syw    Date:  25-Jan-1996    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added also SIGMA_DEBUG to the write() method.  Modify write
//			to use printf intead of cout.  Include stdio.h
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added setFlowSensorCoeffEntry() method for service mode usage.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================
#include "FlowSensorCoeffEntry.hh"

#ifdef SIGMA_DEBUG
#include <stdio.h>
#include "PrintQueue.hh"
#endif // SIGMA_DEBUG

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlowSensorCoeffEntry()
//
//@ Interface-Description
//      Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

FlowSensorCoeffEntry::FlowSensorCoeffEntry( void)
{
	CALL_TRACE("FlowSensorCoeffEntry::FlowSensorCoeffEntry( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlowSensorCoeffEntry()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

FlowSensorCoeffEntry::~FlowSensorCoeffEntry( void)
{
	CALL_TRACE("FlowSensorCoeffEntry::~FlowSensorCoeffEntry( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=()
//
//@ Interface-Description
//      operator=.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Sets all elements equal to the object passed in.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void
FlowSensorCoeffEntry::operator=( const FlowSensorCoeffEntry& coeffEntry)
{
	CALL_TRACE("FlowSensorCoeffEntry::operator=( \
				const FlowSensorCoeffEntry& coeffEntry)") ;

    if (this != &coeffEntry)
    {
	    // $[TI1.1]
	    calVoltage_ = coeffEntry.calVoltage_ ;
    	coeffA_ = coeffEntry.coeffA_ ;
	    coeffB_ = coeffEntry.coeffB_ ;
    	coeffC_ = coeffEntry.coeffC_ ;
    }   // implied else $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFlowSensorCoeffEntry
//
//@ Interface-Description
//		This method takes calVoltage, coeffA, coeffB, coeffC as arguments
//		and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Sets all elements equal to the object passed in.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void 
FlowSensorCoeffEntry::setFlowSensorCoeffEntry( const Real32 volt,
		const Real32 a, const Real32 b, const Real32 c)
{
	CALL_TRACE("FlowSensorCoeffEntry::setFlowSensorCoeffEntry( \
				const Real32 volt, const Real32 a, const Real32 b, \
				const Real32 c)") ;

    // $[TI1]
    
    calVoltage_ = volt ;
    coeffA_ = a ;
    coeffB_ = b ;
    coeffC_ = c ;
}

#ifdef SIGMA_DEBUG

void FlowSensorCoeffEntry::write( void)
{
	CALL_TRACE("FlowSensorCoeffEntry::write( void)") ;

	sprintf( string, "V = %e    A = %e  B = %e  C = %e\n",
			calVoltage_, coeffA_, coeffB_, coeffC_) ;
	PrintQueue::SendString( string) ;

	printf( "V = %e    A = %e  B = %e  C = %e\n",
			calVoltage_, coeffA_, coeffB_, coeffC_) ;
	
}

#endif // SIGMA_DEBUG


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
FlowSensorCoeffEntry::SoftFault(const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    
    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, FLOWSENSORCOEFFENTRY, lineNumber,
                             pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================



