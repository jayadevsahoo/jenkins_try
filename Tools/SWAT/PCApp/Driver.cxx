//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Driver.cxx
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#define _CRT_SECURE_NO_WARNINGS

#include "ICConfiguration.h"
#include "ISettingsCapture.h"
#include <sys/types.h>

#include <Winsock2.h>
#include <ws2tcpip.h>

#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <string>
#include "pav.h"

#ifndef socklen_t
#define socklen_t unsigned int
#endif

//-------------------------------------
// C O D E
//-------------------------------------

/** 
 * Connect to remote server.
 * 
 * @param cli_ip    A string containing the IP address of server.
 * @param cli_port  An integer containing the port number to connect to.
 * 
 * @return          A Socket descriptor.
 * 
 */
int
connect_to_server (char *cli_ip, short cli_port) {
    struct sockaddr_in cli_sa;
    int cli_socket;
    int iRet;

    /* Create the socket */
    cli_socket = socket (AF_INET, SOCK_STREAM, 0);
    if (cli_socket < 0) {
        perror ("connect_to_server:- socket");
        exit (-1);
    }

    /*Build socketaddt struct */
    memset(&cli_sa, 0,sizeof (cli_sa));
    cli_sa.sin_family = AF_INET;
    cli_sa.sin_port = htons (cli_port);

	cli_sa.sin_addr.S_un.S_addr = inet_addr (cli_ip);
#if 0
    iRet = inet_pton((INT)AF_INET, (PCSTR)cli_ip, (PVOID) &cli_sa.sin_addr);
    if (iRet == -1) {
        perror ("connect_to_server:- inet_pton");
        exit (-1);
    }
#endif
    iRet =
        connect (cli_socket, (struct sockaddr *) &cli_sa,
                 (socklen_t) sizeof (cli_sa));

    if (iRet == -1) {
        perror ("connect_to_server:- connect");
        exit (-1);
    }

    return cli_socket;
}


int
readblock (int sock, char *buff, size_t len) {
    int rcnt = 0;
    int tcnt;

    while (len) {
        tcnt = recv (sock, buff + rcnt, len, 0);
        if (tcnt <= 0) {
            break;
        }
        rcnt += tcnt;
        len -= tcnt;
    }
    return rcnt;
}



int
writeblock (int sock, char *buff, size_t len) {
    int rcnt = 0;
    int tcnt;

    while (len) {
        tcnt = send (sock, buff + rcnt, len, 0);
        if (tcnt <= 0) {
            break;
        }
        rcnt += tcnt;
        len -= tcnt;
    }
    return rcnt;
}


int
sendContext (IConfiguration * pX, char *pServerName, char *pPort) {

    int recv;

    char tBuffer[1000];
    char buffer[1000];


    memset (buffer, 0, sizeof (buffer));
    int iLen = pX->makeContext (buffer, 2000);



    /* Open transaction socket */
    int serv_socket = connect_to_server ((char *) pServerName, atoi (pPort));

    /* RECEIVE BD's response to the OPEN... */

    recv = readblock (serv_socket, tBuffer, 8);
    recv = readblock (serv_socket, tBuffer + 8, 4);


    /* Step #1:  SEND the first message of the four-way transaction... */
    recv = writeblock (serv_socket, buffer, 8);
    recv = writeblock (serv_socket, buffer + 8, iLen - 8);


    /* Step #2:  RECEIVE the first response from the BD... */
    recv = readblock (serv_socket, tBuffer, 8);
    recv = readblock (serv_socket, tBuffer + 8, iLen - 8);


    /* Step #3:  SEND the third message of the four-way transaction... */
    recv = writeblock (serv_socket, (char *)peer0_2, 8);
    recv = writeblock (serv_socket, (char *)peer0_3, 4);

    /* Step #4:  RECEIVE the final response from the BD... */
    recv = readblock (serv_socket, tBuffer, 8);
    recv = readblock (serv_socket, tBuffer + 8, 4);

    /*CLOSE Settings Transaction Socket... */
    closesocket (serv_socket);

    return 0;

}















bool
helpMsg () {
    fprintf (stdout,
             "Usage: PCApp -xBASECONFIG [options] [[PARM=VALUE],...]\n"
             "\n"
             "This program allows for the creation and modification of a\n"
             "settings context.\n"
             "Options:\n"
             "    -x BASECONFIG      Required config file that contains a mapping between\n"
             "                       Parameter name and parameter value.                 \n"
             "    -y CONFIGOUT       Create a config file from the current context.      \n"
             "    -c CONTEXT         Context file containing current config.             \n"
             "    -o CONTEXTOUT      New context file based on current context modle.    \n"
             "    -s host:port       Send settings to host on port                       \n"
             "    -l                 Show currnet config                                 \n"
             "    -h                 Help                                                \n"
             "    -C                 Capture settings                                    \n"
             "    -r                 Capture settings continusly                         \n"
             "\n");

    return true;
}


#include <stdio.h>

#define NULL	0
#define EOF	(-1)
#define ERR(s, c)	if(opterr){\
	char errbuf[2];\
	errbuf[0] = c; errbuf[1] = '\n';\
	fputs(argv[0], stdout);\
	fputs(s, stdout);\
	fputc(c, stdout);}
	//(void) write(2, argv[0], (unsigned)strlen(argv[0]));\
	//(void) write(2, s, (unsigned)strlen(s));\
	//(void) write(2, errbuf, 2);}

int	opterr = 1;
int	optind = 1;
int	optopt;
char	*optarg;

int
getopt(int argc, char **argv, char *opts)
{
	static int sp = 1;
	register int c;
	register char *cp;

	if(sp == 1)
		if(optind >= argc ||
		   argv[optind][0] != '-' || argv[optind][1] == '\0')
			return(EOF);
		else if(strcmp(argv[optind], "--") == NULL) {
			optind++;
			return(EOF);
		}
	optopt = c = argv[optind][sp];
	if(c == ':' || (cp=strchr(opts, c)) == NULL) {
		ERR(": illegal option -- ", c);
		if(argv[optind][++sp] == '\0') {
			optind++;
			sp = 1;
		}
		return('?');
	}
	if(*++cp == ':') {
		if(argv[optind][sp+1] != '\0')
			optarg = &argv[optind++][sp+1];
		else if(++optind >= argc) {
			ERR(": option requires an argument -- ", c);
			sp = 1;
			return('?');
		} else
			optarg = argv[optind++];
		sp = 1;
	} else {
		if(argv[optind][++sp] == '\0') {
			sp = 1;
			optind++;
		}
		optarg = NULL;
	}
	return(c);
}

IConfiguration *pX;

int ReceiveData(void *pThis, char *pData, int iLength){
     pX->readContext ((char *)pData, iLength);
    (*((unsigned int *)pThis)) = 0;
    return 0;
}

#if 0
char g_buffer[2000];

int main(){
    int iSize=404;
    IConfiguration *pX= getConfigurationInstance();
;

    bool bRetval = pX->loadXMLDocument ("setting_id.xml");

    pX->makeContext(g_buffer, sizeof(g_buffer)) ;

    while(1){
      
        bool ret=pX->readContext (g_buffer, iSize) ;
        if(ret)
        pX->printContext ();

        else exit(-1);
    }


}
#endif

 /* Standard error macro for reporting API errors */ 
 #define PERR(bSuccess, api){if(!(bSuccess)) printf("%s:Error %d from %s on line %d\n", __FILE__, GetLastError(), api, __LINE__);}



 void cls( HANDLE hConsole )
 {
    COORD coordScreen = { 0, 0 };    /* here's where we'll home the
                                        cursor */ 
    BOOL bSuccess;
    DWORD cCharsWritten;
    CONSOLE_SCREEN_BUFFER_INFO csbi; /* to get buffer info */ 
    DWORD dwConSize;                 /* number of character cells in
                                        the current buffer */ 

    /* get the number of character cells in the current buffer */ 

    bSuccess = GetConsoleScreenBufferInfo( hConsole, &csbi );
    PERR( bSuccess, "GetConsoleScreenBufferInfo" );
    dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

    /* fill the entire screen with blanks */ 

    bSuccess = FillConsoleOutputCharacter( hConsole, (TCHAR) ' ',
       dwConSize, coordScreen, &cCharsWritten );
    PERR( bSuccess, "FillConsoleOutputCharacter" );

    /* get the current text attribute */ 

    bSuccess = GetConsoleScreenBufferInfo( hConsole, &csbi );
    PERR( bSuccess, "ConsoleScreenBufferInfo" );

    /* now set the buffer's attributes accordingly */ 

    bSuccess = FillConsoleOutputAttribute( hConsole, csbi.wAttributes,
       dwConSize, coordScreen, &cCharsWritten );
    PERR( bSuccess, "FillConsoleOutputAttribute" );

    /* put the cursor at (0, 0) */ 

    bSuccess = SetConsoleCursorPosition( hConsole, coordScreen );
    PERR( bSuccess, "SetConsoleCursorPosition" );
    return;
 }


  HWND GetConsoleHwnd(void)
   {
       #define MY_BUFSIZE 1024 // Buffer size for console window titles.
       HWND hwndFound;         // This is what is returned to the caller.
       char pszNewWindowTitle[MY_BUFSIZE]; // Contains fabricated
                                           // WindowTitle.
       char pszOldWindowTitle[MY_BUFSIZE]; // Contains original
                                           // WindowTitle.

       // Fetch current window title.

       GetConsoleTitle(pszOldWindowTitle, MY_BUFSIZE);

       // Format a "unique" NewWindowTitle.

       wsprintf(pszNewWindowTitle,"%d/%d",
                   GetTickCount(),
                   GetCurrentProcessId());

       // Change current window title.

       SetConsoleTitle(pszNewWindowTitle);

       // Ensure window title has been updated.

       Sleep(40);

       // Look for NewWindowTitle.

       hwndFound=FindWindow(NULL, pszNewWindowTitle);

       // Restore original window title.

       SetConsoleTitle(pszOldWindowTitle);

       return(hwndFound);
   }



int
main
(int argc, char *argv[]) {

    WSADATA wsaData;



    
    /* initilizing Windows Socket */
    WSAStartup(MAKEWORD(2, 2), &wsaData);


    /* getopt var'a */
    int c;
    std::string xmlConfigInFile;
    std::string contextConfigInFile;

    std::string xmlConfigOutFile;
    std::string contextConfigOutFile;
    std::string hostPort;
    bool bHelp = false;
    bool bSend = false;
    bool bList = false;

    bool bXmlConfigInFile       = false;
    bool bContextConfigInFile   = false;
    bool bXmlConfigOutFile      = false;
    bool bContextConfigOutFile  = false;
    bool bCaptureSettings       = false;
    bool bCaptureContinues      = false;


    opterr = 0;

    while ((c = getopt (argc, argv, "o:s:x:c:hlCr")) != -1) {
        switch (c) {
        case 'x':
            xmlConfigInFile = optarg;
            bXmlConfigInFile = true;
            break;

        case 'y':
            xmlConfigOutFile = optarg;
            bXmlConfigOutFile = true;
            break;

        case 'c':
            contextConfigInFile = optarg;
            bContextConfigInFile = true;
            break;

        case 'C':
            bCaptureSettings  = true;
            break;

        case 'r':
            bCaptureContinues = true;
            break;

        case 'o':
            contextConfigOutFile = optarg;
            bContextConfigOutFile = true;
            break;

        case 's':
            bSend = true;
            hostPort = optarg;
            break;

        case 'l':
            bList = true;
            break;

        case 'h':
            bHelp = true;
            break;

        case '?':
            if (optopt == 'x' || optopt == 'y' || optopt == 'c'
                || optopt == 'o')
                fprintf (stdout, "Option -%c requires an argument.\n\n",
                         optopt);
            else if (isprint (optopt))
                fprintf (stdout, "Unknown option `-%c'.\n\n", optopt);
            else
                fprintf (stdout, "Unknown option character `\\x%x'.\n\n",
                         optopt);

            bHelp = 1;
        }
        if (bHelp)
            break;
    }

    if (bXmlConfigInFile == false) {
        fprintf (stdout, "Required option -x missing.\n\n");
        bHelp = true;
    }

    if (bHelp) {
        helpMsg ();
        return -1;
    }

    pX= getConfigurationInstance();

    bool bRetval = pX->loadXMLDocument (xmlConfigInFile.c_str ());

    if (bRetval == false) {
        fprintf (stdout, "Failed to load, \"%s\".\n\n",
                 xmlConfigInFile.c_str ());
        helpMsg ();
        return -1;
    }

    if (bContextConfigInFile) {
        bool bRetval = pX->loadContextFile (contextConfigInFile.c_str ());
        if (bRetval == false) {
            fprintf (stdout, "failed to load, \"%s\".\n\n",
                     contextConfigInFile.c_str ());
            helpMsg ();
            return -1;
        }
    }



    if(bCaptureContinues && bCaptureSettings && bSend) {
        unsigned int lock = 1;
        bSend = false;
        
        char buffer[300];
        strcpy (buffer, hostPort.c_str ());
        char *pHost = strtok (buffer, ":");
        char *pPort = strtok (NULL, "");

        ISettingsCapture *pSc = getSettingsCaptureInstance(pHost, ReceiveData, &lock );
        
        if(pSc == NULL) 
        {
            fprintf(stdout,"Could not set up capture device.\n");
            helpMsg ();
            return -1;
        }

        if (bList) {
            cls(GetStdHandle(STD_OUTPUT_HANDLE));
        }


        while(1){


             

            while(lock)
            {
                Sleep(250);
            }
            lock = 1;


            //Write back to context
            if (bContextConfigOutFile) {
                pX->writeContextFile (contextConfigOutFile.c_str ());
            }


            if (bList) {
                cls(GetStdHandle(STD_OUTPUT_HANDLE));
                pX->printContext ();
            }


        }

        try{
            pSc->destroy();
        }
        catch( ...){
            fprintf(stdout,"Caught an error\n");
        }
    }






    if(bCaptureSettings && bSend) {
        unsigned int lock = 1;
        bSend = false;
        
        char buffer[300];
        strcpy (buffer, hostPort.c_str ());
        char *pHost = strtok (buffer, ":");
        char *pPort = strtok (NULL, "");

        ISettingsCapture *pSc = getSettingsCaptureInstance(pHost, ReceiveData, &lock );
        
        if(pSc == NULL) 
        {
            fprintf(stdout,"Could not set up capture device.\n");
            helpMsg ();
            return -1;
        }


        while(lock)
        {
            Sleep(100);
        }
        Sleep(500);

        try{
            pSc->destroy();
        }
        catch( ...){
            fprintf(stdout,"Caught an error\n");
        }
    }


    //Set parameter
    for (int i = optind; i < argc; i++) {
        char *pName = strtok (argv[i], "=");
        char *pValue = strtok (NULL, "");

        if (pName && pValue) {
            if (pX->setParam (pName, pValue) == false) {
                fprintf (stdout, "Could not set \"%s=%s\".\n\n", pName, pValue);
                helpMsg ();
                return -1;
            }
        }

    }


    //Write back to context
    if (bContextConfigOutFile) {
        pX->writeContextFile (contextConfigOutFile.c_str ());
    }


    if (bList) {
        pX->printContext ();
    }


    if (bSend) {
        char buffer[300];
        strcpy (buffer, hostPort.c_str ());
        char *pHost = strtok (buffer, ":");
        char *pPort = strtok (NULL, "");

        if (pHost && pPort) {
            sendContext (pX, pHost, pPort);
        }
        else {
            fprintf (stdout, "Invalid host.\n\n");
            helpMsg ();
            return -1;
        }

    }

    pX->destroy();

    return 0;
}

