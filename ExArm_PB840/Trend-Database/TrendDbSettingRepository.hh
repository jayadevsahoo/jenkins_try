#ifndef TrendDbSettingRepository_HH
#define TrendDbSettingRepository_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbSettingRepository
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSettingRepository.hhv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
//  Revision: 001  By:  ksg    Date:  5-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
// 
//=====================================================================

#include "ContextObserver.hh"
#include "TrendDataSet.hh"
#include "TrendDbSampleNonAvg.hh"	
#include "TrendDbConfig.hh"
#include "TrendDbTable.hh"
#include "TrendDbSettingRecord.hh"

// forward declare
class TimeStamp;

class TrendDbSettingRepository : public ContextObserver
{
public:

	TrendDbSettingRepository();
	~TrendDbSettingRepository();

	SigmaStatus initialize( void );

	void registerTarget( void );

	virtual void batchSettingUpdate(const Notification::ChangeQualifier qualId,
								    const ContextSubject *              pContextSub,
								    const SettingId::SettingIdType      setId );
	virtual void nonBatchSettingUpdate(const Notification::ChangeQualifier qualId,
									   const ContextSubject *              pContextSub,
									   const SettingId::SettingIdType      setId );

	void sample( Uint32 seqNumber, const TimeStamp & trendTimeStmap );

	Boolean isValidContext( const SettingId::SettingIdType setId );

	TrendDataSet::Status processRequest( TrendDataSet * pTrendDataSet );

	Boolean hasSettingIds( TrendDataSet * pTrendSet );

	void resyncAll( void );

	void resetDatabase( void );

	void sampleSimulatedData( Int32 seqPosNumber,
							  const TimeStamp & trendTimeStamp,
							  const TrendValue * pTrendValues );

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
private:

	TrendDbSettingRepository( const TrendDbSettingRepository & );
	void operator=( const TrendDbSettingRepository & );

	Int32 findPosition_( Int32 seqNumber );

	Int16 loadRecordData_( TrendDbTableBuffer & inputBuffer,
						   TrendDataSet * pTrendDataSet,
						   Uint16 firstRow );

	SigmaStatus loadTableIndex_( void );

	SigmaStatus updateTable_( Int32 seqNumber, const TimeStamp & timeStamp );

	//@ Data-Member: refreshArray_
	// Array used to know if a value is in need for updating
	Boolean isRefreshedArray_[SettingId::TOTAL_SETTING_IDS];

	//@ Data-Member: setExpSens_
	// Trend sample collection for Exp Sens setting
	TrendDbSampleNonAvg     setExpSensSample_;

	//@ Data-Member: setRespRateSample_
	// Trend sample collection for Resportiory Rate (f) sample
	TrendDbSampleNonAvg     setRespRateSample_;

	//@ Data-Member: setPeakInspFlowSample_
	// Trend sample collection for Peak Inspirotory flow (Vdot-max) setting
	TrendDbSampleNonAvg     setPeakInspFlowSample_;

	//@ Data-Member: setIERatioSample_
	// Trend sample collecting the I:E setting
	TrendDbSampleNonAvg     setIeRatioSample_;

	//@ Data-Member: setO2PercentSample_
	// Trend sample collection for O2 percent setting
	TrendDbSampleNonAvg     setO2PercentSample_;

	//@ Data-Member: setTcSupportSample_
	// Trend sample collection for TC support setting
	TrendDbSampleNonAvg     setTcSupportSample_;

	//@ Data-Member: setPeepSample_
	// Trend sample collection for PEEP setting
	TrendDbSampleNonAvg     setPeepSample_;

	//@ Data-Member: setPeepHighSample_
	// Trend sample collection for PEEP High setting
	TrendDbSampleNonAvg     setPeepHighSample_;

	//@ Data-Member: setPeepLowSample_
	// Trend sample collection for PEEP Low setting
	TrendDbSampleNonAvg     setPeepLowSample_;

	//@ Data-Member: setInspPressureSample_
	// Trend sample collection for Inspiratory Pressure setting
	TrendDbSampleNonAvg     setInspPressureSample_;

	//@ Data-Member: setPressSupportSample_
	// Trend sample collection for Pressure support setting
	TrendDbSampleNonAvg     setPressSupportSample_;

	//@ Data-Member: setFlowAccelSample_
	// Trend sample collection for Flow Acceleration Rate setting
	TrendDbSampleNonAvg     setFlowAccelSample_;

	//@ Data-Member: setFlowSensSample_
	// Trend sample collection for Flow Sensitivity setting
	TrendDbSampleNonAvg     setFlowSensSample_;

	//@ Data-Member: setPressSensSample_
	// Trend sample collection for Pressure Sensitivity setting
	TrendDbSampleNonAvg     setPressSensSample_;

	//@ Data-Member: setPeepHighTimeSample_
	// Trend sample collection for the Peep High Time setting
	TrendDbSampleNonAvg     setPeepHighTimeSample_;

	//@ Data-Member: setThTlRatioSample_
	// Trend sample collection for Th:Tl Ratio setting
	TrendDbSampleNonAvg     setThTlRatioSample_;

	//@ Data-Member: setInspTimeSample_
	// Trend sample collection for Inspiratory Time Setting
	TrendDbSampleNonAvg     setInspTimeSample_;

	//@ Data-Member: setPeepLowTimeSample_
	// Trend sample collection for Peep Low Time setting
	TrendDbSampleNonAvg     setPeepLowTimeSample_;

	//@ Data-Member: setTidalVolumeSample_
	// Trend sample collection for Tidial Volume (VC) setting
	TrendDbSampleNonAvg     setTidalVolumeSample_;

	//@ Data-Member: setVolumeSupportSample_
	// Trend sample collection for Volume Support (VS)
	TrendDbSampleNonAvg     setVolumeSupportSample_;


	//@ Data-Member: dbConfig_
	// Configuration of the Setting Trend database
	TrendDbConfig           dbConfig_;

	//@ Data-Member: patientRecord_
	// Used to access the fields of the record
	TrendDbSettingRecord    settingRecord_;

	//@ Data-Member: compactFlashInterface_
	// Interface to the compact flash
	CompactFlash &          compactFlashInterface_;

	//@ Data-Member: table_
	//  Trend table for the settings
	//  Settings use just one table which is used when a setting is changed
	//  upto 10 seconds
	TrendDbTable            table_;

	// Define the size of the index table to be the setting's capacity plus
	// some to cover the fact that capacity is on block boundries and therefore
	// can be larger then set by at most a block and that there also could be a
	// block in RAM, all of this can give the setting's table a little extra in
	// capacity (two extra blocks with 3 records each for 6 extra records)
	enum
	{
		TABLE_INDEX_CAPACITY = TrendDataMgr::TREND_SETTING_DB_CAPACITY + 6
	};

	//@ Data-Member: tableIndex_
	// Used to find the position for a given seq number
	Uint32 tableIndex_[ TABLE_INDEX_CAPACITY ];
};

#endif // TrendDbSettingRepository_HH
