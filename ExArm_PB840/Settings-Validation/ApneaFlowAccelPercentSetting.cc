#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaFlowAccelPercentSetting - Apnea Flow Acceleration Percentage
//          Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the percentage of
//  acceleration of inspiratory pressure generated by the ventilator when
//  in pressure-based apnea ventilation.  Higher values of apnea flow
//  acceleration percent produce inspiratory pressure trajectories of
//  shorter time.  This class inherits from 'BatchBoundedSetting' and
//  provides the specific information needed for representation of
//  flow acceleration percentage.  This information includes this batch
//  setting's new-patient value (see 'getNewPatientValue()'), and the
//  constraints of this setting.
//
//  This setting is a constant setting, and, as such, never has an
//  applicability of CHANGEABLE.  The 'calcNewValue()' method defined
//  by one of the base classes ensures that non-CHANGEABLE settings
//  are never changed.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings, and never needs to update its
//  bound values, therefore 'calcDependentValues_()' and 'updateConstraints_()'
//  are NOT overridden.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaFlowAccelPercentSetting.ccv   25.0.4.0   19 Nov 2013 14:27:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed now-unneeded 'calcNewValue()'
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaFlowAccelPercentSetting.hh"
#include "MandTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02047] -- The setting's range ...
static const BoundedInterval  LAST_NODE_ =
  {
    50.0f,		// absolute minimum...
    0.0f,		// unused...
    ONES,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    50.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaFlowAccelPercentSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BatchBoundedSetting.  The method passes to
//  BatchBoundedSetting it's default information. After initializing this
//  setting's base class, the single interval range is initialized.
//
//  Since this setting's value remains constant, the constraint IDs are set
//  to 'NULL_SETTING_BOUND_ID'.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaFlowAccelPercentSetting::ApneaFlowAccelPercentSetting(void)
   : BatchBoundedSetting(SettingId::APNEA_FLOW_ACCEL_PERCENT,
			 Setting::NULL_DEPENDENT_ARRAY_,
			 &::INTERVAL_LIST_,
			 NULL_SETTING_BOUND_ID,	// maxConstraintId...
			 NULL_SETTING_BOUND_ID)	// minConstraintId...
{
  CALL_TRACE("ApneaFlowAccelPercentSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaFlowAccelPercentSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaFlowAccelPercentSetting::~ApneaFlowAccelPercentSetting(void)
{
  CALL_TRACE("~ApneaFlowAccelPercentSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaFlowAccelPercentSetting::getApplicability(
				const Notification::ChangeQualifier qualifierId
					  ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pApneaMandType =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_MAND_TYPE);

  DiscreteValue  apneaMandTypeValue;

  switch (qualifierId)
  {
  case Notification::ACCEPTED :		// $[TI1]
    apneaMandTypeValue = pApneaMandType->getAcceptedValue();
    break;
  case Notification::ADJUSTED :		// $[TI2]
    apneaMandTypeValue = pApneaMandType->getAdjustedValue();
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(qualifierId);
    break;
  }

  return((apneaMandTypeValue == MandTypeValue::PCV_MAND_TYPE)
	   ? Applicability::VIEWABLE		// $[TI3]
	   : Applicability::INAPPLICABLE);	// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a Bounded value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaFlowAccelPercentSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  // $[02048] The setting's new-patient value ...

  BoundedValue  newPatient;
  
  newPatient.value     = 50.0f;
  newPatient.precision = ONES;

  return(newPatient);  // $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaFlowAccelPercentSetting::SoftFault(const SoftFaultID  softFaultID,
		                        const Uint32       lineNumber,
			                const char*        pFileName,
			                const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  APNEA_FLOW_ACCEL_PERCENT_SETTING, lineNumber,
			  pFileName, pPredicate);
}
