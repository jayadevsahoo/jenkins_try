/*****************************************************************************/
/*   Module Name:   tt.h                                     
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Modified InitTips to not allocate      Dave Richardson   23 Nov, 2002
/*   and abandon memory
/*
/*   $Revision::   1.2      $                                                
/*
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/tooltips/tt.h-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:37:10   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   23 Nov 2002 12:26:48   Richardson
 * Updated per SCR # 31

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

#ifndef _TT_H
#define _TT_H

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <toolbox.h>

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#define TOOLTIP_BOUND 5
#define TOOLTIP_COLOR_BG 0x00FFFFAA

/***********************************************/
/************ STRUCTURE DEFINITIONS ************/
/***********************************************/

typedef struct tag_TipListNode {
	int PanelID;
	int CtrlID;
	char* CtrlConst;
	char* CtrlStr;
} TipListNode;

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

extern ListType TipList;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

int CVICALLBACK ToolTipTimer(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2);

void TTStringInsert(void);
void InitTips(int panel);
void SwitchPanels(int Newpanel);
void SwitchPanelsWithConst(int Newpanel, int NewPanelConst);
int MatchTip(int panel, int x, int y);
void PauseTips(void);
void UnPauseTips(void);
char* strdup(char* source);

#endif // #ifndef _TT_H
