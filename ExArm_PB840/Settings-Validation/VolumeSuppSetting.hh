
#ifndef VolumeSuppSetting_HH
#define VolumeSuppSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  VolumeSuppSetting - Volume Support Setting
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/VolumeSuppSetting.hhv   25.0.4.0   19 Nov 2013 14:27:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  09-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added this new setting to support VS
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
//@ End-Usage


class VolumeSuppSetting : public BatchBoundedSetting 
{
  public:
    VolumeSuppSetting(void); 
    virtual ~VolumeSuppSetting(void);

    virtual Applicability::Id  getApplicability(
				 const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
				   const DiscreteValue            newValue,
				   const DiscreteValue            currValue);

    virtual SettingValue  getNewPatientValue(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);
 
  protected:
    virtual void  updateConstraints_(void);

    virtual void  findSoftMinMaxValues_(Real32& rSoftMinValue,
					Real32& rSoftMaxValue) const;

    virtual Real32  getAbsoluteMinValue_(void) const;

  private:
    // not implemented...
    VolumeSuppSetting(const VolumeSuppSetting&);
    void  operator=(const VolumeSuppSetting&);
};


#endif // VolumeSuppSetting_HH 
