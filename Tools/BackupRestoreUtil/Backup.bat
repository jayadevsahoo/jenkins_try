@Echo OFF
rem *************************************************************
rem : This batch file can Backup the files. It dynamically create the temp.txt file.
rem : This text files consist of ftp commands to transfer the files. Backup will be 
rem : stored in the GUI and BD folder created in current script folder.
rem *************************************************************
@Echo OFF
rem *******Clean up : Remove old dir and files *********************
cls
del GUI\*.* /Q
del BD\*.* /Q

rmdir GUI
rmdir BD

rem ******Create new Backup : Create dir and copy files ****************
mkdir GUI
mkdir BD

rem ****** Create GUI temp text file with ftp commands  ***************
echo anonymous>>temp.txt
echo abc@covidien.com>>temp.txt
echo cd ProgramFiles>>temp.txt
echo lcd GUI>>temp.txt
echo mget *.dat>>temp.txt
echo mget *.crc>>temp.txt
echo mget *.tbl>>temp.txt
echo disconnect>>temp.txt
echo quit>>temp.txt

rem ****** Connect to GUI and backup files ************************
ftp -i -s:temp.txt 192.168.0.2

del temp.txt

rem ****** Create GUI temp text file with ftp commands  *************
echo anonymous>>temp.txt
echo abc@covidien.com>>temp.txt
echo cd ProgramFiles>>temp.txt
echo lcd BD>>temp.txt
echo mget *.dat>>temp.txt
echo mget *.crc>>temp.txt
echo mget *.tbl>>temp.txt
echo disconnect>>temp.txt
echo quit>>temp.txt

rem ****** Connect to BD and backup files *********************
ftp -i -s:temp.txt 192.168.0.3

del temp.txt


rem *********** Script Ends *******************************