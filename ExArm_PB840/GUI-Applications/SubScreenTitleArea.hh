#ifndef SubScreenTitleArea_HH
#define SubScreenTitleArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SubScreenTitleArea - Used for displaying the title of a
// sub-screen with the title surrounded by a bounding box.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SubScreenTitleArea.hhv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "Line.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class SubScreenTitleArea : public Container
{
public:
	//@ Type: Justification
	// An enumerated type listing the different justifications possible for a
	// SubScreenTitleArea, SSTA_LEFT, SSTA_RIGHT and SSTA_CENTER.  Used to 
	// specify whether the title is displayed at the top left, top right or
	// center of the subscreen.
	enum Justification
	{
		SSTA_LEFT = 0,
		SSTA_RIGHT,
		SSTA_CENTER
	};

	SubScreenTitleArea(StringId title = NULL_STRING_ID,
					   Justification justification = SSTA_LEFT);
	~SubScreenTitleArea(void);


	void setTitle(StringId title);
	virtual void addDrawable(Drawable* pDrawable);
	void clear(void);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	SubScreenTitleArea(const SubScreenTitleArea&);	// not implemented...
	void operator=(const SubScreenTitleArea&);		// not implemented...

	void updateArea_(void);				// Resize area according to any changes

	//@ Data-Member: justification_
	// Specifies the justification, SSTA_LEFT or SSTA_RIGHT, of the title area.
	Justification justification_;

	//@ Data-Member: titleText_
	// The text field for displaying the title.
	TextField titleText_;

	//@ Data-Member: bottomLine_
	// The white line displayed below the title.
	Line bottomLine_;

};

#endif // SubScreenTitleArea_HH 
