#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: WaveformsTabButton - A specialized version of TabButton which
// is responsible for changing the color of the joining tab according
// to which Waveforms screen is being displayed.
//---------------------------------------------------------------------
//@ Interface-Description
// A specialized TabButton which joins to the Waveforms subscreen.
// The button must change the color of the top line above the button to
// either black or medium grey, depending on whether the Waveforms
// subscreen is currently displaying waveforms (on a black background)
// or the plot setup screen (on a medium grey background)!
//
// The updateDisplay() method is inherited from TabButton and is called anytime
// the Waveforms subscreen switches between the waveform display and the
// plot setup screen.
//---------------------------------------------------------------------
//@ Rationale
// This specialized TabButton handles the unique display requirements of
// the Waveforms subscreen select button.
//---------------------------------------------------------------------
//@ Implementation-Description
// The updateDisplay() method asks the Waveforms subscreen what state it is
// in, i.e. is it displaying waveforms or the plot setup screen, and sets
// the color of the top line accordingly.  updateDisplay() is called
// by the Upper Screen select area at the appropriate times.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// n/a
//---------------------------------------------------------------------
//@ Invariants
// n/a
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformsTabButton.ccv   25.0.4.0   19 Nov 2013 14:08:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

//
// Sigma includes.
//
#include "WaveformsTabButton.hh"
#include "UpperScreen.hh"
#include "UpperSubScreenArea.hh"
#include "WaveformsSubScreen.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WaveformsTabButton  [Constructor]
//
//@ Interface-Description
// Constructor.  Passed the following arguments:
// >Von
//	xOrg			The X coordinate of the button.
//	yOrg			The Y coordinate of the button.
//	width			Width of the button.
//	height			Height of the button.
//	buttonType		Type of the button, either Button::LIGHT,
//					Button::LIGHT_NON_LIT or Button::DARK.
//	bevelSize		Width of the button's internal border.
//	cornerType		Tells whether its corners are rounded or not, can be
//					Button::ROUND or Button::SQUARE.
//	title			The string displayed as the button's title.
//	pSubScreen		The subscreen which is selected by pressing this button.
//>Vof
//---------------------------------------------------------------------
//@ Implementation-Description
// Passes constructor arguments through to the TabButton constructor.
// Initializes all members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformsTabButton::WaveformsTabButton(Uint16 xOrg, Uint16 yOrg,
								 Uint16 width, Uint16 height,
								 Button::ButtonType buttonType,
								 Uint8 bevelSize,
								 Button::CornerType cornerType,
								 StringId title, SubScreen *pSubScreen) :
	TabButton(xOrg, yOrg, width, height, buttonType, bevelSize,
							cornerType, title, pSubScreen)
{
	CALL_TRACE("WaveformsTabButton::WaveformsTabButton(xOrg, yOrg, width, "
				"height, buttonType, bevelSize, cornerType, title, pSubScreen, shortcutId)");
}														// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~WaveformsTabButton  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformsTabButton::~WaveformsTabButton(void)
{
	CALL_TRACE("WaveformsTabButton::~WaveformsTabButton(void)");

	// Do nothing.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay [public, virtual]
//
//@ Interface-Description
// Decides which color the area above this button should be displayed in
// so as to make a clean join with the Waveforms subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Queries the Waveform subscreen to see if it's displaying waveforms or
// not.  Waveform display will cause the "tab" to be colored black,
// otherwise it will be colored medium grey.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsTabButton::updateDisplay(void)
{
	CALL_TRACE("WaveformsTabButton::updateDisplay(void)");

	if (UpperSubScreenArea::GetWaveformsSubScreen()->isOnWaveDisplay())
	{													// $[TI1]
		setTopLineBoxColor_(Colors::BLACK);
	}
	else
	{													// $[TI2]
		setTopLineBoxColor_(Colors::MEDIUM_BLUE);
	}
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformsTabButton::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("WaveformsTabButton::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, WAVEFORMSTABBUTTON,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
