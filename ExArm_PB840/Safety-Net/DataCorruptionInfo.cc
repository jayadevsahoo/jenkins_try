#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett Inc of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor Puritan Bennett Inc of California.
//
//            Copyright (c) 1996, Nellcor Puritan Bennett Inc
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DataCorruptionInfo - used as an interface to NovRamManager.
//---------------------------------------------------------------------
//@ Interface-Description
//    The data corruption information must persist through powerfails.  This
//    class provides the interface between the BdMonitor subsystem and the 
//    Persistent-Objects subsystem.
//---------------------------------------------------------------------
//@ Rationale
//    If three instances of corrupted raw signal data have been detected
//    in the last 24 hours, the GUI shall log an event in the system
//    diagnostic log and place the ventilator into Vent Inop.
//---------------------------------------------------------------------
//@ Implementation-Description
//    When the BdMonitor subsystem encounters invalid encoded data, the
//    DataCorruptionInfo::update method is called (unless the ventilator
//    is already in a forcedVentInop condition).  If MAX_DATA_CORRUPTIONS
//    corruptions have been detected in the last twenty-four hours,
//    the ventilator is forced into the vent inop state.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/DataCorruptionInfo.ccv   25.0.4.0   19 Nov 2013 14:20:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  by    Date:  12-Dec-1995    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//  
//=====================================================================

#include "DataCorruptionInfo.hh"
#include "NovRamManager.hh"
//@ Usage-Classes

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DataCorruptionInfo()  [Default Constructor]
//
//@ Interface-Description
//    Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Deactivate the corruptionData_ items in the local array.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

DataCorruptionInfo::DataCorruptionInfo(void)
{
  CALL_TRACE("DataCorruptionInfo()");

  for (Int32 ii = 0; ii < MAX_DATA_CORRUPTIONS; ii++)
  {
    corruptionData_[ii].active = FALSE;
  }
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DataCorruptionInfo(corruptionInfo)  [Copy Constructor]
//
//@ Interface-Description
//    Copy constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Copy the contents of the passed parameter corruptionInfo.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

DataCorruptionInfo::DataCorruptionInfo(const DataCorruptionInfo& corruptionInfo)
{
  CALL_TRACE("DataCorruptionInfo(corruptionInfo)");

  operator=(corruptionInfo);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DataCorruptionInfo()  [Destructor]
//
//@ Interface-Description
//    Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

DataCorruptionInfo::~DataCorruptionInfo(void)
{
  CALL_TRACE("~DataCorruptionInfo()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(corruptionInfo)  [Assignment Operator]
//
//@ Interface-Description
//    Assignment operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

void
DataCorruptionInfo::operator=(const DataCorruptionInfo& rCorruptionInfo)
{
  CALL_TRACE("operator=(rCorruptionInfo)");

  if (this != &rCorruptionInfo) // $[TI1]
  {   
    for (Int32 ii = 0; ii < MAX_DATA_CORRUPTIONS; ii++)
    {
      corruptionData_[ii].active =
          rCorruptionInfo.corruptionData_[ii].active;
      corruptionData_[ii].time =
          rCorruptionInfo.corruptionData_[ii].time;
    }     
  }   // $[TI2]
}

#if defined(SIGMA_GUI_CPU) || defined(SIGMA_UNIT_TEST) || defined(SIGMA_COMMON)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  clear_(void)
//
//@ Interface-Description
//    This method takes no parameters and returns void.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Reset the corruption data counting mechanism by setting the
//    active field in the corruptionData_ array to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

void
DataCorruptionInfo::clear_(void)
{
  CALL_TRACE("clear_(void)");

  for (Int32 ii = 0; ii < MAX_DATA_CORRUPTIONS; ii++)
  {
    corruptionData_[ii].active = FALSE;
  }
// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  update(void)
//
//@ Interface-Description
//  This method takes no parameters and returns void.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If MAX_DATA_CORRUPTIONS instances of raw ADC channel data have been 
//  detected in the last 24 hours, an event is logged to the System 
//  Diagnostic log and the ventilator is placed into Vent Inop and a value
//  of TRUE is returned. $[06171]
//
//  This method is called by BdMonitoredData::verifyEncodedData_ if corrupted
//  data is encountered.  If there are already MAX_DATA_CORRUPTIONS - 1
//  active corruptionData_ items in the corruptionData_ array and none 
//  is greater than ONE_DAY_IN_HOURS old, force vent inop, else activate
//  an inactive item.  If there are already MAX_DATA_CORRUPTIONS - 1 valid 
//  CorruptionDatas, throw out the oldest and insert the new.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean
DataCorruptionInfo::update(void)
{
  CALL_TRACE("update(void)");

  Int32 numDataCorruptions = 0;
  Boolean forcedVentInop = FALSE;

  NovRamManager::GetDataCorruptionInfo(*this);

  // Determine the current number of data corruptions by adding up the number
  // of active corruptionData_ array items.
// TODO E600
  Int32 ii = 0;
  for (ii = 0; ii < MAX_DATA_CORRUPTIONS; ii++)
  {
    if (corruptionData_[ii].active) // $[TI1]
    {
      numDataCorruptions++;
    } // else $[TI2]
  }

  // If there less than MAX_DATA_CORRUPTIONS - 1 active items in the
  // array, set the item in position numDataCorruptions to the current
  // operational time and set it active.
  if (numDataCorruptions < MAX_DATA_CORRUPTIONS - 1) // $[TI3]
  {
    // Put the current timestamp into the corruptionData_ array.
    corruptionData_[numDataCorruptions].time =
        Post::GetOperationalTime();
    corruptionData_[numDataCorruptions].active = TRUE;
  }
  else // $[TI4]
  {
    // If the oldest timestamp is older than 24 hours, then shift all of
    // the data and declare no fault.
    if (Post::GetOperationalTime() - corruptionData_[0].time > 
        ONE_DAY_IN_HOURS) // $[TI4.1]
    {
      for (ii = 0; ii < MAX_DATA_CORRUPTIONS - 2; ii++)
      {
	corruptionData_[ii].time = corruptionData_[ii+1].time;
      }

      // Put the current operational time in the latest element
      corruptionData_[ii].time = Post::GetOperationalTime();
    }
    else // $[TI4.2]
    {
      // max faults in 24 hours
      // log error and go VIO
      clear_();
      Background::ReportBkEvent (BK_MON_DATA_CORRUPTED);
      forcedVentInop = TRUE;
    }
  }
  NovRamManager::UpdateDataCorruptionInfo(*this);

  return(forcedVentInop);
}

#endif // defined(SIGMA_GUI_CPU) || defined(SIGMA_UNIT_TEST) || defined(SIGMA_COMMON)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
DataCorruptionInfo::SoftFault (const SoftFaultID  softFaultID,
                          const Uint32       lineNumber,
                          const char*        pFileName,
                          const char*        pPredicate)
{
  CALL_TRACE("SoftFault (softFaultID, lineNumber, pFileName, pPredicate)") ;
  	
  FaultHandler::SoftFault (softFaultID, SAFETY_NET, DATACORRUPTIONINFO,
                          lineNumber, pFileName, pPredicate) ;
}
