#ifndef SWATMESSAGE_H
#define SWATMESSAGE_H

#include "uuid.h"

//namespace swat{

    class SwatMessage
    {
        public:
            static const uuid_t_ EXECUTESCRIPT;
            static const uuid_t_ LOADBUFFER;   
            static const uuid_t_ ENABLESWAT;   
            static const uuid_t_ DISABLESWAT;  
    };

//}
#endif

