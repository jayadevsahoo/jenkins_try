#ifndef ApneaSetupSubScreen_HH
#define ApneaSetupSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ApneaSetupSubScreen - The subscreen in the Lower screen
// selected by pressing the Apnea Setup button.  Displays and allows
// the user to adjust the current Apnea vent modes and settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ApneaSetupSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 005   By: sah   Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  using new DropDownSettingButton class for mand type
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


#include "BatchSettingsSubScreen.hh"

//@ Usage-Classes
#include "ApneaCorrectionStatus.hh"
#include "BreathTimingDiagram.hh"
#include "DropDownSettingButton.hh"
#include "NumericSettingButton.hh"
#include "NumericLimitSettingButton.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "TimingSettingButton.hh"
//@ End-Usage

class ApneaSetupSubScreen : public BatchSettingsSubScreen
{
public:
	ApneaSetupSubScreen(SubScreenArea *pSubScreenArea);
	~ApneaSetupSubScreen(void);

	// SubScreen virtual methods
	virtual void acceptHappened(void);

	// ButtonTarget virtual methods
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
												Boolean byOperatorAction);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// BatchSettingsSubScreen virtual method
	virtual void adjustPanelRestoreFocusHappened(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// BatchSettingsSubScreen virtual method...
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									  );

private:
	// these methods are purposely declared, but not implemented...
	ApneaSetupSubScreen(void);							// not implemented...
	ApneaSetupSubScreen(const ApneaSetupSubScreen&);	// not implemented...
	void   operator=(const ApneaSetupSubScreen&);		// not implemented...

	void layoutScreen_(void);						// Layout screen 1 or 2

	void setApneaSetupTabTitleText_( StringId stringId );

	//@ Data-Member: onScreen1_
	// A boolean which tells us whether we're in the first or second screen
	// of Apnea Setup.
	Boolean onScreen1_;

	//@ Data-Member: statusBar_
	// The status bar which displays the breath control settings.
	Container statusBar_;

	//@ Data-Member: mainSettingsAreaContainer_
	// A container which holds all the buttons displayed in the "main settings
	// area" of screen 2 and whose fill color forms a background for the
	// buttons.
	Container mainSettingsAreaContainer_;

	//@ Data-Member: changeMandTypeButton_
	// The Change Mandatory type button, which allows the operator to
	// go to Screen 2 of Apnea Setup in order to change the Mandatory Type.
	// Can only be done once a session.
	TextButton changeMandTypeButton_;

	//@ Data-Member: continueButton_
	// The continue button, which allows the operator to get from Screen
	// 2 back to Screen 1 of Apnea Setup.
	TextButton continueButton_;

	//@ Data-Member: mandatoryTypeButton_
	// The Apnea Mandatory Type setting button.
	DropDownSettingButton mandatoryTypeButton_;

	//@ Data-Member: flowPatternButton_
	// The Apnea Flow Pattern setting button.
	DiscreteSettingButton flowPatternButton_;

	//@ Data-Member: inspiratoryPressureButton_
	// The Apnea Inspiratory Pressure setting button.
	NumericSettingButton inspiratoryPressureButton_;

	//@ Data-Member: inspiratoryTimeButton_
	// The Apnea Inspiratory Time setting button.
	TimingSettingButton inspiratoryTimeButton_;

	//@ Data-Member: intervalButton_
	// The Apnea Interval button.
	//TimingSettingButton intervalButton_;
	NumericLimitSettingButton intervalButton_;

	//@ Data-Member: oxygenPercentageButton_
	// The Apnea Oxygen Percentage button.
	NumericSettingButton oxygenPercentageButton_;

	//@ Data-Member: peakFlowButton_
	// The Apnea Peak Flow setting button.
	NumericSettingButton peakFlowButton_;

	//@ Data-Member: respiratoryRateButton_
	// The Apnea Respiratory Rate setting button.
	NumericSettingButton respiratoryRateButton_;

	//@ Data-Member: tidalVolumeButton_
	// The Apnea Tidal Volume setting button.
	NumericSettingButton tidalVolumeButton_;

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member: modeTitle_
	// The text for the "fake" apnea Mode title displayed in the status bar.
	TextField modeTitle_;
	
	//@ Data-Member: mandatoryTypeTitle_
	// The text for the Mandatory Type title displayed in the status bar.
	TextField mandatoryTypeTitle_;
	
	//@ Data-Member: leftWingtip_
	// Left-hand "wingtip" line for the Mandatory Type on the status bar. 
	Line leftWingtip_;

	//@ Data-Member: leftWing_
	// Left-hand "wing" line for the Mandatory Type on the status bar.
	Line leftWing_;

	//@ Data-Member: rightWing_
	// Right-hand "wing" line for the Mandatory Type on the status bar.
	Line rightWing_;

	//@ Data-Member: rightWingtip_
	// Right-hand "wingtip" line for the Mandatory Type on the status bar.
	Line rightWingtip_;

	//@ Data-Member: breathTimingDiagram_
	// The breath timing diagram displayed on the first screen.
	BreathTimingDiagram breathTimingDiagram_;

	//@ Data-Member: correctionStatus_
	// The apnea correction status returned when we inform the Settings
	// subsystem that we are adjusting apnea settings.  Tells us if they
	// auto-corrected anything.
	ApneaCorrectionStatus correctionStatus_;

	//@ Data-Member: apneaSetupText_
	// The text displayed in the settings area indicating apnea setup
	TextField apneaSetupText_;

	enum { MAX_SCREEN1_SETTING_BUTTONS_ = 8 };
	enum { MAX_SCREEN2_SETTING_BUTTONS_ = 1 };

	//@ Data-Member: arrScreen1BtnPtrs_
	SettingButton*  arrScreen1BtnPtrs_[MAX_SCREEN1_SETTING_BUTTONS_ + 1];

	//@ Data-Member: arrScreen2BtnPtrs_
	SettingButton*  arrScreen2BtnPtrs_[MAX_SCREEN2_SETTING_BUTTONS_ + 1];
	
};

#endif // ApneaSetupSubScreen_HH 
