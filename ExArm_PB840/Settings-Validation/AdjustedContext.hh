
#ifndef AdjustedContext_HH
#define AdjustedContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AdjustedContext - Context for the Adjusted Batch Settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/AdjustedContext.hhv   25.0.4.0   19 Nov 2013 14:27:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: jja    Date: 05-Apr-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//      Added new 'getAllSettingValuesKnown()' method, and new
//      'allSettingValuesKnown_' data member.
//
//  Revision: 005   By: sah    Date: 05-May-2000    DR Number: 5718
//  Project:  NeoMode
//  Description:
//      Added new 'BatchState' enum, 'getBatchState()' method, and
//      'batchState_' data member.
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  now derived off of 'ContextSubject' base class
//	*  added new 'isSettingChanged()' virtual method
//	*  added new 'newPatientSetupFlag_' flag
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date: 29-Sep-1997    DR Number: 2410
//  Project: Sigma (R8027)
//  Description:
//	Added new interface method for the adjustment of the new
//	Atmospheric Pressure Setting.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Integration baseline.
//
//====================================================================

#include "ApneaCorrectionStatus.hh"
#include "BatchAdjustPhase.hh"
#include "TemplateMacros.hh"

//@ Usage-Classes
#include "BatchValue.hh"
#include "BatchSettingValues.hh"
#include "ContextSubject.hh"
//@ End-Usage


class AdjustedContext : public ContextSubject
{
  public:
    //@ Type:  BatchState
    // State of adjustment.
    enum BatchState
    {
      NEW_PATIENT_BATCH,
      VENT_SETUP_BATCH,
      APNEA_BATCH,
      GENERIC_BATCH,
      SST_BATCH,
      TIME_DATE_BATCH,
      DCI_BATCH,
      SERVICE_BATCH,
      ATM_PRESS_CAL,

      TOTAL_BATCH_STATES
    };

    AdjustedContext(void);
    ~AdjustedContext(void);

    inline BatchState  getBatchState(void) const;

    void                   adjustNewPatientBatch(
					    const BatchAdjustPhase phase
						);
    void                   adjustVentSetupBatch (
					    const BatchAdjustPhase phase
						);
    ApneaCorrectionStatus  adjustApneaSetupBatch(
					    const BatchAdjustPhase phase,
					    const Boolean          isReEntry
						);

    inline void  adjustGenericBatch     (void);
    inline void  adjustSstBatch         (void);
    void         adjustTimeDateBatch    (void);
    inline void  adjustDciSetupBatch    (void);
    inline void  adjustServiceSetupBatch(void);
    void         adjustAtmPressureCal   (const Real32 atmPressureValue);

    void  recoverStoredBatch(void);

    ApneaCorrectionStatus  correctApneaSettings(void);

    inline const BatchSettingValues&  getAdjustedValues(void) const;

    inline BoundedValue         getBoundedValue (
				   const SettingId::SettingIdType boundedId
				    		)  const;
    inline DiscreteValue        getDiscreteValue(
				   const SettingId::SettingIdType discreteId
				    		)  const;
    inline SequentialValue      getSequentialValue(
				   const SettingId::SettingIdType sequentialId
						  )  const;

    inline void  setSettingValue (const SettingId::SettingIdType settingId,
				  const SettingValue&            newValue);
    inline void  setBoundedValue (const SettingId::SettingIdType boundedId,
				  const BoundedValue&            newValue);
    inline void  setDiscreteValue(const SettingId::SettingIdType discreteId,
				  const DiscreteValue            newValue);
    inline void  setSequentialValue(
				const SettingId::SettingIdType sequentialId,
				const SequentialValue          newValue
				   );

    inline Boolean getAllSettingValuesKnown(void) const;
    
    //-----------------------------------------------------------------
    // virtual methods inherited from ContextSubject class...
    //-----------------------------------------------------------------

    inline Boolean  isInNewPatientSetupMode (void) const;
    inline void     clearNewPatientSetupFlag(void);

    virtual Boolean  isSettingChanged(
			      const SettingId::SettingIdType settingId
				     ) const;

    virtual Boolean  areAnyBatchSettingsChanged(void) const;

    virtual SettingValue  getSettingValue(
			      const SettingId::SettingIdType settingId
					 ) const;


#if defined(SIGMA_DEVELOPMENT)
    Boolean  areAllValuesValid(void) const;
#endif  // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

     
  private:
    AdjustedContext(const AdjustedContext&);	// not implemented...
    void  operator=(const AdjustedContext&);	// not implemented...

    static void  RegisterSettingChange_(
				    const SettingId::SettingIdType batchId
				       );

    void  adjustAcceptedBatch_(void);

    //@ Data-Member:  batchState_
    // This data member identifies the last batch state entered.
    BatchState  batchState_;

    //@ Data-Member:  newPatientSetupFlag_
    // This flag indicates whether we're in the process of setting up a
    // new patient, or not.
    Boolean  newPatientSetupFlag_;

    //@ Data-Member:  proposedBatchValues_
    // The adjusted batch setting values.
    BatchSettingValues  proposedBatchValues_;
    
    //@ Data-Member:  allSettingValuesKnown_
    // Flag indicating when all settings values are known 
    // following new patient initialzation.
    Boolean   allSettingValuesKnown_ ;

};


// Inlined methods
#include "AdjustedContext.in"


#endif // AdjustedContext_HH 
