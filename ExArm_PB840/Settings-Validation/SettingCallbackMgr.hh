#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

#ifndef SettingCallbackMgr_HH
#define SettingCallbackMgr_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SettingCallbackMgr - Manager of callbacks to external subsystems.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingCallbackMgr.hhv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  obsoleted this class from use on the GUI; new observer/subject
//	   mechanism now used on the GUI
//	*  as part of the GUI obsolescence, removed unneeded methods and
//	   array dimensions
//
//  Revision: 003   By: sah  Date:  23-Nov-1998    DR Number: 5238
//  Project:  BILEVEL
//  Description:
//	Added method for clearing of all registered changes.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

#include "SettingId.hh"
#include "ContextId.hh"

//@ Usage-Classes
//@ End-Usage


class SettingCallbackMgr
{
  public:
    static void  Initialize(void);

    //@ Type:  SettingCallbackPtr
    // Callback for changes of a given setting within a given context.
    typedef void  (*SettingCallbackPtr)(SettingId::SettingIdType settingId);

    static void  RegisterSettingCallback(
			  const SettingId::SettingIdType         settingId,
			  SettingCallbackMgr::SettingCallbackPtr pNewCallback
				        );
    static void  ActivateSettingCallback(
				  const SettingId::SettingIdType settingId
				        );

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

    //@ Constant:  MAX_ALLOWABLE_CALLBACKS
    // This constant identifies the maximum number of callbacks that can
    // be registered for one selection (i.e., maximum number for the context
    // callbacks and maximum number for each of the setting callbacks).
    enum { MAX_ALLOWABLE_CALLBACKS = 4 };

  private:
    //@ Data-Member:  ArrCallback_
    // Static, two-dimensional array of callback pointers that are to be
    // called whenever any setting changes within the Pending Context.  The
    // first dimension is the number of batch settings -- one "row" for
    // each setting, while the second dimension is the maximum number of
    // callbacks that can be registered for a single setting.
    static SettingCallbackPtr  ArrCallbackPtrs_[
					::NUM_BD_SETTING_IDS
					       ][
				SettingCallbackMgr::MAX_ALLOWABLE_CALLBACKS
					        ];
};


#endif // SettingCallbackMgr_HH 

#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
