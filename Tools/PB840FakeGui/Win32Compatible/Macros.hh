#pragma once

#define CLASS_PRE_CONDITION(A) ASSERT(A)
#define CLASS_ASSERTION(A) ASSERT(A)
#define CLASS_POST_CONDITION(A,B) ASSERT(A)
#define SAFE_CLASS_PRE_CONDITION(A) ASSERT(A)
#define AUX_CLASS_PRE_CONDITION(A,B) ASSERT(A)
#define SAFE_CLASS_ASSERTION(A) ASSERT(A)

//@ Macro:  countof(array)
// Return the number of elements in an array
#define countof(array)		(sizeof(array) / sizeof(*array))

#define CALL_TRACE(A) printf(A)

// redefine all these socket API to compatible with Winsock2.h
#define MT_socket socket
#define MT_ioctl ioctlsocket
#define MT_setsockopt setsockopt
#define MT_close closesocket
#define MT_select select
#define MT_recv recv
#define MT_sendmsg WSASendMsg
#define MT_bind bind
#define MT_listen listen
#define MT_accept accept
#define MT_connect connect
#define MT_getsockopt getsockopt


#define WM_PRINTF_LOG (WM_APP + 0x0010)
#define WM_PUSH_SETTING (WM_APP + 0x0011)