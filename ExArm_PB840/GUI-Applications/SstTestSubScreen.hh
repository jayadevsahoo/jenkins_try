#ifndef SstTestSubScreen_HH
#define SstTestSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: SstTestSubScreen - The subscreen is activated after the user selects
// PROCEED in the SstSetupSubScreen and presses the Accept key.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SstTestSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:32   pvcs  $
//
//@ Modification-Log  
//
//  Revision: 010   By: mnr   Date: 04-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      SST_TEST_PX_REMOVE_INSP_FILTER_PROMPT added to SmPromptId.
//
//  Revision: 009   By: mnr   Date: 13-APR-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Redundant SstTestId enum removed. Using the one from SmTestId.hh.
//
//  Revision: 008   By: mnr   Date: 24-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      PROX SST related updates.
// 
//  Revision: 007   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 006  By:  hhd	   Date:  27-Apr-1999    DCS Number: 5322
//  Project:  ATC
//  Description:
//		Initial version.
//
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 004  By:  yyy    Date: 08-Oct-1997    DR Number: DCS 2326, 2443
//  Project:  Sigma (840)
//  Description:
//		Added PromptId CONNECT_HUMIDIFIER_PROMPT and deleted
//		SST_TEST_CONNECT_INSP_FILTER promptId.
//
//  Revision: 003  By:  yyy    Date:  20-Aug-97    DR Number: 1988
//    Project:  Sigma (R8027)
//    Description:
//      Added handles for different pressure unit display.
//
//  Revision: 002  By:  yyy    Date:  22-JUNE-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      Renamed sstStatus_ to sstStatusStr_ to identify it as a string
//		rather than a status field for translation.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "GuiTimerTarget.hh"
#include "SubScreen.hh"
#include "AdjustPanelTarget.hh"
#include "GuiTestManagerTarget.hh"
#include "LogTarget.hh"
#include "NovRamEventTarget.hh"

//@ Usage-Classes
#include "Array_ResultTableEntry_MAX_TEST_ENTRIES.hh"
#include "GuiTestManager.hh"
#include "NovRamManager.hh"
#include "NovRamUpdateManager.hh"
#include "ResultTableEntry.hh"
#include "ScrollableLog.hh"
#include "SmPromptId.hh"
#include "SmStatusId.hh"
#include "SmTestId.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "UpperSubScreenArea.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class SstTestSubScreen : public SubScreen, public GuiTimerTarget,
							public AdjustPanelTarget,
							public LogTarget,
							public NovRamEventTarget,
							public GuiTestManagerTarget
{
public:
	SstTestSubScreen(SubScreenArea *pSubScreenArea);
	~SstTestSubScreen(void);

	// Redefine SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// Redefine LogTarget methods
	virtual void getLogEntryColumn(Uint16 entryNumber, Uint16 columnNumber,
						Boolean &rIsCheapText, StringId &rString1,
						StringId &rString2, StringId &rString3,
						StringId &rString1Message);
	
	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
												Boolean byOperatorAction);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelRestoreFocusHappened(void);
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);

	// NovRamEventTarget virtual methods
	virtual void novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId
											updateId);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// GuiTestManagerTarget virtual method
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);
	
	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	SstTestSubScreen(void);							// not implemented..
	SstTestSubScreen(const SstTestSubScreen&);	// not implemented..
	void operator=(const SstTestSubScreen&);			// not implemented..

	//@ Type: SstScreenId
	// Lists the possible screen layout scenarios.
	enum SstScreenId
	{
		SST_NO_SCREEN,
		SST_SETUP_SCREEN,			// Initial screen set to previous SST result.
		SST_START_SCREEN,			// do the rest of SST test.
		SST_EXIT_SCREEN,			// exit the all the SST test.
		SST_TEST_NO_SCREEN		
	};

	void layoutScreen_(SstScreenId sstScreenId);
	void setSstTestNameTable_(void);
	void setSstTestPromptTable_(void);
	void setSstTestStatusTable_(void);
	void formatStatusOutcome_(ResultEntryData resultData, wchar_t *tmpBuffer, Boolean isOutcome);  
	void startTest_(void);
	void nextTest_(void);
	void repeatOrNextTest_(void);
	void displayPausingRequest_(void);
	void displayFailure_(void);
	void setupUpperScreenLayout_(int sstCurrentTestIndex);
	void setDataFormat_(SmTestId::ServiceModeTestId testId);
	void displayPrompt_(void);
	void displaySstFinishedScreen_(void);
	void displaySstResultOutcome_(void);
	Int getNextState_(Int newEvent);
	void executeState_(void);
	void setupFallbackState_(void);

	
	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member: exitButton_
	// The Exit button is used for exiting the SST test mode.
	TextButton exitButton_;

	//@ Data-Member: repeatButton_
	// The Repeat button is used for repeating the SST test mode.
	TextButton repeatButton_;

	//@ Data-Member: nextButton_
	// The Next button is used for proceeding to the next SST test mode.
	TextButton nextButton_;

	//@ Data-Member: overrideButton_
	// The Override button is used to override the minor failure found in the
	//  SST test mode.
	TextButton overrideButton_;

	//@ Data-Member: restartButton_
	// The Restart button is used to restart the SST test.
	//  SST test mode.
	TextButton restartButton_;

	//@ Data-Member: sstSubScreenDivideLine_
	// The line divides the SST Lower Sub-screen Area in to two logical area.
	Line sstSubScreenDivideLine_;

	//@ Data-Member: pCurrentButton_
	// The pCurrentButton_ is used for remembering the previously button
	// which was pressed by the user.
	Button *pCurrentButton_;

	//@ Data-Member: sstStatusStr_
	// A container for displaying the current SST status.
	TextField sstStatusStr_;

	//@ Data-Member: sstCompletedLabel_
	// A text field for displaying the current SST completion label.
	TextField sstCompletedLabel_;

	//@ Data-Member: sstOutcomeLabel_
	// A text field for displaying the current SST outcome label.
	TextField sstOutcomeLabel_;

	//@ Data-Member: sstCompletedValue_
	// A text field for displaying the current SST completion Value.
	TextField sstCompletedValue_;

	//@ Data-Member: sstOutcomeValue_
	// A text field for displaying the current SST outcome Value.
	TextField sstOutcomeValue_;

	//@ Data-Member: pLog_
	// A ScrollableLog field for handling the table display.
	 ScrollableLog *pLog_;

	//@ Data-Member: pSstLeakSubScreen_
	// Address points to the SstLeakSubScreen.
	SstLeakSubScreen *pSstLeakSubScreen_;

	//@ Data-Member: pSstProxSubScreen_
	// Address points to the SstProxSubScreen.
	SstProxSubScreen *pSstProxSubScreen_;

	//@ Data-Member: pUpperSubScreenArea_
	// Address points to UpperSubScreenArea
	UpperSubScreenArea *pUpperSubScreenArea_;
	
	//@ Data-Member: unitId_
	// The unit used based on settings.
	StringId unitId_;

	//@ Type: sstPromptId
	// Lists the possible SST prompt ID.
	enum SstPromptId
	{
		SST_TEST_CONNECT_O2_PROMPT,
		SST_TEST_CONNECT_AIR_PROMPT,
		SST_TEST_UNBLOCK_WYE_PROMPT,
		SST_TEST_BLOCK_WYE_PROMPT,
		SST_TEST_IS_HUMIDIFIER_WET_PROMPT,
		SST_TEST_ATTACH_CIRCUIT_PROMPT,
		SST_TEST_CONCT_TUBE_BFR_EXH_FLTR_PROMPT,
		SST_TEST_DISCNCT_TUBE_BFR_EXH_FLTR_PROMPT,
		SST_TEST_CONNECT_HUMIDIFIER_PROMPT,
		SST_TEST_PROX_SENSOR_PROMPT,                  
		SST_TEST_CONNECT_PROX_SENSOR_PROMPT,
		SST_TEST_REMOVE_INSP_FILTER_PROMPT,
		SST_TEST_BLOCK_EXP_PORT_PROMPT,
		SST_TEST_PX_REMOVE_INSP_FILTER_PROMPT,
		SST_TEST_PX_REMOVE_EXP_LIMB_PROMPT,
		SST_TEST_PX_UNBLOCK_PROX_SENSOR_OUTPUT_PROMPT,
		SST_TEST_PX_BLOCK_SENSOR_OUTPUT_PROMPT,
		SST_TEST_PX_UNBLOCK_EXP_PORT_PROMPT,
		NUM_SST_TEST_PROMPT_MAX
	};

	//@ Type: sstRowId
	// Lists the the number of rows can be displayed
	enum SstRowId
	{
		ROW_1,
		ROW_2,
		ROW_3,
		ROW_4,
		ROW_5,
		ROW_6,
		ROW_7,
		SST_DISPLAY_ROW_MAX
	};

	//@ Type: sstRowId
	// Lists the the number of columns can be displayed
	enum SstColId
	{
		COL_1_TIME,
		COL_2_TEST,
		COL_3_DIAG_CODE,
		COL_4_RESULT,
		SST_DISPLAY_COL_MAX
	};

	//@ Type: sstEventId
	// Lists the possible state events.
	enum SstEventId
	{
		UNDEFINED_EVENT=-1,

		// Button initiated event
		EXIT_EVENT 				= 0,
		REPEAT_EVENT 			= 1,
		NEXT_EVENT 				= 2,
		OVERRIDE_EVENT 			= 3,
		RESTART_EVENT 			= 4,

		// Keyboard initiated event
		ACCEPT_EVENT 			= 5,
		CLEAR_EVENT 			= 6,

		// Service mode initiated event
		USER_PROMPT_EVENT 		= 7,
		END_ALERT_EVENT 		= 8,
		END_FAILURE_EVENT 		= 9,
		USER_END_EVENT 			= 10,
		
		// Timer initiated event
		TIMEOUT_EVENT 			= 11,

		// Program automatically initiated event
		AUTO_END_OF_TEST_EVENT 	= 12,
		AUTO_EVENT 				= 13,
		SST_EVENT_NUM
	};
	
	//@ Type: sstStateId
	// Lists the possible states.
	enum SstStateId
	{
		UNDEFINED_NEXT_TEST=-1,
		INITIAL_STATE 				= 0,
		READY_TO_TEST_STATE 		= 1,
		DO_TEST_STATE 				= 2,
		PROMPT_STATE 				= 3,
		END_ALERT_STATE 			= 4,
		END_FAILURE_STATE 			= 5,
		TEST_PASS_STATE 			= 6,
		EXECUTE_PROMPT_STATE 		= 7,
		EXIT_REQUEST_STATE 			= 8,
		REPEAT_STATE 				= 9,
		NEXT_STATE 					= 10,
		OVERRIDE_STATE 				= 11,
		RESTART_STATE 				= 12,
		DO_NEXT_STATE 				= 13,
		END_OF_SST_STATE 			= 14,
		PAUSE_REQUEST_STATE 		= 15,
		EXIT_BEFORE_PROMPT_STATE 	= 16,
		END_EXIT_ALERT_STATE 		= 17,
		END_EXIT_FAILURE_STATE 		= 18,
		END_EXIT_TEST_STATE 		= 19,
		EXIT_SST_STATE 				= 20,
		EXIT_FALLBACK_STATE 		= 21,
		RESTART_SST_STATE 			= 22,
		EXECUTE_EXIT_SST_STATE 		= 23,
		EXECUTE_RESTART_STATE 		= 24,
		EXECUTE_REPEAT_STATE 		= 25,
		SST_STATE_NUM
	};

	//@ Data-Member: testManager_
	// An object which look after rendering the service mode test.
	GuiTestManager testManager_;
	
	//@ Data-Member: sstCurrentTestIndex_
	// The currently processing SST test item ids.
	Int sstCurrentTestIndex_;
	
	//@ Data-Member: maxServiceModeTest_
	// The maximum number of test in the SST test item ids.
	Int maxServiceModeTest_;
	
	//@ Data-Member: sstTestId_
	// An array which stores the SST test item ids.
	SmTestId::ServiceModeTestId sstTestId_[SmTestId::SST_TEST_MAX];

	//@ Data-Member: sstTestName_
	// An array which stores the SST test item names text.
	StringId sstTestName_[SmTestId::SST_TEST_MAX];

	//@ Data-Member: sstTestPromptId_
	// An array which stores the SST test prompt ids.
	SmPromptId::PromptId sstTestPromptId_[NUM_SST_TEST_PROMPT_MAX];
	
	//@ Data-Member: sstTestPromptName_
	// An array which stores the prompt test item names text.
	StringId sstTestPromptName_[NUM_SST_TEST_PROMPT_MAX];

	//@ Data-Member: sstTestData_
	// An array which stores the SST test data text.
	wchar_t sstTestData_[SmTestId::SST_TEST_MAX][256];  

	//@ Data-Member: sstResultTable_
	// An array which stores the EST ResultTableEntry information.
	FixedArray(ResultTableEntry,MAX_TEST_ENTRIES) sstResultTable_;
	
	//@ Data-Member: sstStatusDecodingTable_
	// An array which stores the decoding information from Service Mode to
	// Persistent object.
	Int sstStatusDecodingTable_[SmStatusId::NUM_TEST_DATA_STATUS];

	//@ Data-Member: clearLogEntry_
	// An boolean flag indicating to keep or clear the log entry.
	Boolean clearLogEntry_;

	//@ Data-Member: promptId_
	// Hold the Service Mode prompt ID.
	Int promptId_;

	//@ Data-Member: keyAllowedId_
	// Hold the Service Mode key combinations allowed for the operator.
	SmPromptId::KeysAllowedId keyAllowedId_;

	//@ Data-Member: conditionId_
	// Hold the Service Mode condition ID.
	Int conditionId_;

	//@ Data-Member: userKeyPressedId_
	// Hold the operator pressed key Id.
	SmPromptId::ActionId userKeyPressedId_;

	//@ Data-Member: isThisSstTestCompleted_
	// Hold the test completed information.
	Boolean isThisSstTestCompleted_;

	//@ Data-Member: mostSevereStatusId_
	// Hold the most severed status ID.
	Int mostSevereStatusId_;

	//@ Data-Member: overallStatusId_
	// Hold the overall status ID.
	Int overallStatusId_;

	//@ Data-Member:  previousState_
	// Previous state.
	Int previousState_;
	
	//@ Data-Member:  nextState_
	// Current state.
	Int nextState_;
	
	//@ Data-Member:  fallBackState_
	// FallBack state.
	Int fallBackState_;
	
	//@ Data-Member: overrideRequested_
	// Flag to Hold the override request
	Boolean	overrideRequested_;
	
	//@ Data-Member: repeatTestRequested_
	// Flag to Hold the repeat request
	Boolean	repeatTestRequested_;
	
	//@ Data-Member: sstStateTable_
	// State table for SST test.
	Int16 sstStateTable_[SST_STATE_NUM][SST_EVENT_NUM];

	//@ Data-Member:  RecursionCount_
	// Recursion Count_.
	Int16 RecursionCount_;
	
	//@ Data-Member:  newEvent_
	// New event.
	Int newEvent_;
	
};

#endif // SstTestSubScreen_HH 
