#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmtted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2008, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LeakCompMgr - A central location to manage leak compensation
//                       related data.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class manages all the leak compensation related data 
//  for detecting and calculating leak.  This class is updated every 
//  BD cycle, on transition from inspiration to exhalation, and 
//  exhalation to inspiration.  It also calculates inspiratory leak, 
//  exhalation leak rate, and percent leak. 
//---------------------------------------------------------------------
//@ Rationale
//   This class provides leak compensation related data used by 
//   controllers and triggering algorithms in the breath delivery subsystem.  
//---------------------------------------------------------------------
//@ Implementation-Description
//   All data members which are used in leak compensation algorithms are 
//   updated every cycle to cycle.  This class calculates inspiratory leak, 
//  exhalation leak rate, and percent leak which gets populated in 
//  BreathData.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  only one instance is allowed.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/LeakCompMgr.ccv   25.0.4.0   19 Nov 2013 13:59:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013   By: rhj   Date:  25-Oct-2010    SCR Number: 6694
//  Project:  PROX
//  Description:
//      Changed the SummationBuffer getSum() to getCurrentSum() and
//      the SummationBuffer getAverage() to getCurrentAverage()
//
//  Revision: 012   By: rhj   Date:  25-Oct-2010    SCR Number: 6694
//  Project:  PROX
//  Description:
//      Fixed the peep optimization changes and the checkExhalationFlow 
//      method which was calculating the sum incorrectly.
//
//  Revision: 011   By: rhj   Date:  05-May-2010    SCR Number: 6436  
//  Project:  PROX
//  Description:
//      Optimize leakcomp algorithms by minimizing the use of 
//      math api calls.
//
//  Revision: 010   By: rhj   Date:  12-May-2009    SCR Number:  6511 
//  Project:  840S2
//  Description:
//      isLeakCompEnabled_ is now being checked during a 
//      non-breathing mode and added isBreathingMode method to 
//      to this class.
// 
//  Revision: 009   By: rhj   Date:  13-Apr-2009    SCR Number:  6497 
//  Project:  840S2
//  Description:
//      Prevent leak compensation calculating every BD cycle during 
//      a non-breathing mode.
// 
//  Revision: 008   By: rhj   Date: 13-Mar-2009     SCR Number:  6482     
//  Project:  840S2
//  Description:
//      Added additional checks to the circuit disconnect algorithms
//      when leaks exceed the disconnect sensitivity.
//
//  Revision: 007   By: rhj   Date:  12-Mar-2009    SCR Number:  6492 
//  Project:  840S2
//  Description:
//		Enhanced Leak Compensation algorithms to handle no leaks when 
//      exhalation leak rate is underestimated.
//
//  Revision: 004   By: rhj   Date:  13-Feb-2009    SCR Number: 6473
//  Project:  840S
//  Description:
//		AverageNetFlowLeak and averageExhPressure are now reset every
//      newCycle of this class.
// 
//  Revision: 003   By: rhj   Date:  11-Feb-2009    SCR Number: 6471
//  Project:  840S
//  Description:
//		Enhance backup cycling criterion for high leaks.  
// 
//  Revision: 002   By: rhj   Date:  19-Jan-2009    SCR Number: 6455 & 6452
//  Project:  840S
//  Description:
//		Enhanced Leak Compensation algorithms to handle no leaks and
//      added a limit to the disconnect sensitivity when a compressor
//      is active.
// 
//  Revision: 001   By: rhj   Date:  18-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//		Initial version.
//
//=====================================================================

#include "LeakCompMgr.hh"
#include <math.h>
#include "PhasedInContextHandle.hh"
#include "SettingId.hh"
#include "LeakCompEnabledValue.hh"
#include "NetFlowInspTrigger.hh"
#include "PatientDataMgr.hh"
#include "ModeValue.hh"
#include "TriggersRefs.hh"
#include "PressureBasedPhase.hh"
#include "TriggerTypeValue.hh"
#include "PressureXducerAutozero.hh"
#include "MainSensorRefs.hh"
#include "NovRamManager.hh"
#include "PeepController.hh"
#include "ControllersRefs.hh"
#include "BreathMiscRefs.hh"
#include "MainSensorRefs.hh"
#include "FlowController.hh"
#include "PressureXducerAutozero.hh"
#include "SchedulerRefs.hh"
#include "PressureSensor.hh"
#include "ExhFlowSensor.hh"
#include "PhaseRefs.hh"
#include "ExhalationPhase.hh"
#include "ControllersRefs.hh"
#include "BreathPhaseScheduler.hh"
#include "BiLevelScheduler.hh"
#include "PatientCctTypeValue.hh"
#include "PeepController.hh"
#include "LungData.hh"
#include "BreathRecord.hh"
#include "Compressor.hh"
#include "VentObjectRefs.hh"
#include "MathUtilities.hh"
#include "StaticMechanicsBuffer.hh"


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LeakCompMgr() 
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Initialize all data members
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

LeakCompMgr::LeakCompMgr(Real32 lPerMinToMl) :  
L_PER_MIN_TO_ML_(lPerMinToMl),
	netLeakFlowBuffer_(StaticMechanicsBuffer::GetNetLeakFlowBuffer(),
					   StaticMechanicsBuffer::GetNetLeakFlowBufferSize()),

    exhWyePressureBuffer_(StaticMechanicsBuffer::GetExhWyePressureBuffer(),
						  StaticMechanicsBuffer::GetExhWyePressBufferSize())

{
	CALL_TRACE("LeakCompMgr()");

	// $[LC24001]
	leakCompPhases_ = LeakCompMgr::LEAK_COMP_STARTUP;
	prevLeakVolume_ = 0.0F;
	prevInspLeakVolume_  = 0.0F;
	inspLeakVol_  = 0.0F;
	netVolume_ = 0.0F;
	k1Gain_ = 0.0F;
	k2Gain_ = 0.0F;
	pressureSum1_ = 0.0F;
	pressureSum2_ = 0.0F;
	prevPressureSum1_ = 0.0F;
	prevPressureSum2_ = 0.0F;
	leakFlow_  = 0.0F;
	leakCompIndex_ = 0;
	isSteadyLeak_ = FALSE;
	avgSteadyLeakRate_    = 0.0F;
	avgSteadyExpPressure_ = 0.0F;
	pressureSlope1_ = 0.0F;
	pressureSlope2_ = 0.0F;
	flowSlope1_ = 0.0F;
	flowSlope2_ = 0.0F;
	isPressureAndFlowSteady_ = FALSE;
	currentFilExhPressureData_ = 0.0F;
	leakCompIndex2_ = 0;
	k1AndK2Counter_ = 0;
	filteredLeakFlow_ = 0.0F;
	currentBreathPhaseType_ = BreathPhaseType::NON_BREATHING;


	effectivePressure_ = 0.0F;
	pressureTrajectory_ = 0.0F;
	maxPreviousLeakVolume_= 0.0F;
	maxLeakVolume_= 0.0F;
	maxK1Gain_= 0.0F;
	percentLeak_ = 0.0F;
	prevK1Gain_   = 0.0F;
	averageNetFlowLeak_ = 0.0F;
	averageExhPressure_ = 0.0F;
	sqrExhPress_            = 0.0F;
	finalK1Gain_              = 0.0F;
	prevExhTime_            = 0;
	prevInspTime_           = 0;
	isLeakDetected_        = FALSE;
	exhLeakRate_           = 0.0F;
	partialSteadyAvgPress_      = 0.0F;
	partialPrevLeakVolume_         = 0.0F;
	isPartialSteadyLeak_       = FALSE;
	ieTransition_          = FALSE;
	partialAvgSteadyLeakRate_   = 0.0F;
	partialPressureSum1_        = 0.0F;
	partialPressureSum2_        = 0.0F;
	prevFilteredNetFlow_    = 0.0F;
	estimatedLeakError_    = 0.0F;
	k2Counter_    = 1;
	prevK2Gain_            = 0.0F;
	prevAverageExhPressureData_ = 0.0F;
	netFlow_ = 0.0F;

	for (Uint32 index = 0; index < MAX_LEAK_COMP_DATA; index++)
	{
		filteredNetLeakFlow_[index] = 0.0F;
		filteredExhPressure_[index] = 0.0F;
	}

	exhPressure_ = 0.0f;
    filteredNetFlow_ = 0.0f;

	NovRamManager::GetInspResistance(rInspResistance_);
	NovRamManager::GetExhResistance(rExhResistance_);


	// Defaulted to the maximum disconnect senstivity value of neonatal
	discoSensitivity_ = 10.0f;
	isDiscoSensLimitByComp_ = FALSE;
	exhFlowOffset_ = 0.0f;
	isExhLeakHigh_ = FALSE;
	isBreathingMode_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LeakCompMgr()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LeakCompMgr::~LeakCompMgr(void) 
{
	CALL_TRACE("~LeakCompMgr()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newInspiration
//
//@ Interface-Description
//   This method is called on transition to a new inspiration. 
//---------------------------------------------------------------------
//@ Implementation-Description
// $[LC24022] - Reset leak compensation parameters once at the 
//  onset of a new inspiration
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void LeakCompMgr::newInspiration(void)
{

	// Save the current pressure sum 1 and 2 calculations.
	prevPressureSum1_ = pressureSum1_;
	prevPressureSum2_ = pressureSum2_;

	// If leak compensation is enabled, and the current mode
	// is not BILEVEL, reset pressure sum 1 and 2 to zero.
	if (modeValue_ != ModeValue::BILEVEL_MODE_VALUE
		&& isLeakCompEnabled_
	   )
	{
		pressureSum1_ = 0.0F;
		pressureSum2_ = 0.0F;
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newExhalation
//
//@ Interface-Description
//   This method sets the previous exhalation time from the given 
//   parameter and has no return value.  It must be called on the 
//    same BD cycle as the transition to exhalation.  
//---------------------------------------------------------------------
//@ Implementation-Description
//   Resets the steady leak flag and flow and pressure stability flag.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void LeakCompMgr::newExhalation(Real32 exhTime)
{
	prevExhTime_ = exhTime;
	isSteadyLeak_ = FALSE;			  // Reset steady leak flag
	isPressureAndFlowSteady_ = FALSE; // Reset flow and pressure stability condition
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setInspTime
//
//@ Interface-Description
//  	This method takes a inspTime argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//   sets data member prevInspTime_ to the value passed in.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
void LeakCompMgr::setInspTime(Uint32 inspTime)
{
	prevInspTime_ = inspTime;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isEnabled
//
//@ Interface-Description
//   This method takes no parameters and returns data member
//   isLeakCompEnabled_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member isLeakCompEnabled_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean LeakCompMgr::isEnabled(void)
{
	return isLeakCompEnabled_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle(void)
//
//@ Interface-Description
//   This method takes no parameters, and has no return value.  
//   It is called every BD cycle.  Leak compensation data members are 
//   updated depending upon the phase type (inspiration or exhalation). 
//   If the phase type, is non-breathing, then this method will not update.  
//---------------------------------------------------------------------
//@ Implementation-Description
//   All data members which are used in leak compensation algorithms are 
//   updated every cycle to cycle.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void LeakCompMgr::newCycle(void)
{
	const Real32 LEAK_ALPHA = 0.94F;
	const Uint32 DELTA_INDEX_1 = 8;     
	const Real32 MAX_NET_FLOW = 300.0f;

	modeValue_ = PhasedInContextHandle::GetDiscreteValue(SettingId::MODE);
	circuitType_ = PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  LEAK_COMP_ENABLED_VALUE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::LEAK_COMP_ENABLED);

	isLeakCompEnabled_ = LEAK_COMP_ENABLED_VALUE == LeakCompEnabledValue::LEAK_COMP_ENABLED;

	const SchedulerId::SchedulerIdValue  SCHED_ID = BreathPhaseScheduler::GetCurrentScheduler().getId();

	isBreathingMode_ = (SCHED_ID >= SchedulerId::FIRST_BREATHING_SCHEDULER &&  
						   SCHED_ID <= SchedulerId::LAST_BREATHING_SCHEDULER);

	if ( isLeakCompEnabled_ && isBreathingMode_)
	{

		Real32 inspFlow = RO2FlowSensor.getValue() + RAirFlowSensor.getValue();
		Real32 exhDryFlow = RExhFlowSensor.getDryValue();

		// Store measured netflow and exhalation pressure into a buffer
		// $[LC24005] $[LC24008] $[LC24016]
		filteredNetFlow_ =  prevFilteredNetFlow_ * LEAK_ALPHA + (inspFlow - exhDryFlow) * (1-LEAK_ALPHA); 

		prevFilteredNetFlow_ = filteredNetFlow_ ;

		netLeakFlowBuffer_.updateBuffer(filteredNetFlow_);

		// $[LC24009]
	    exhPressure_ =  BreathRecord::GetPressWyeExpEst();

		// When autozero occurs, replace the exhalation pressure
		// to a calculated exhalation pressure.
		if (RPressureXducerAutozero.getIsAutozeroInProgress() &&
			RPressureXducerAutozero.getAutozeroSide() == PressureXducerAutozero::EXH)
		{
			exhPressure_ =   RInspPressureSensor.getValue()
											 - rInspResistance_.calculateDeltaP( inspFlow )   
											 - rExhResistance_.calculateDeltaP( exhDryFlow ) ;
		}

		// Remove negative exhalation pressure from the buffer.
		if (exhPressure_ < 0.0F)
		{
			exhPressure_ = 0.0F;
		}

		exhWyePressureBuffer_.updateBuffer(exhPressure_);

		// Used for leak algorithms. 
		sqrExhPress_ = pow( exhPressure_, 0.5F );   

		// $[LC24010] Calculate Qleak 
		leakFlow_ = ( (k1Gain_ * sqrExhPress_) + (k2Gain_ * exhPressure_ * sqrExhPress_) );

		// $[LC24011] During inspiration, Qleak's maximum value is the measured inpiratory flow.
		if (leakFlow_ > inspFlow &&
			BreathRecord::GetPhaseType() == BreathPhaseType::INSPIRATION)
		{
			leakFlow_ = inspFlow;
		}

		// $[LC24012] Calculate filtered Qleak
		filteredLeakFlow_ = (1 - LEAK_ALPHA) * filteredLeakFlow_ + LEAK_ALPHA * leakFlow_;

		// $[LC24017] Calculate the average netflow and exhalation pressure
		// $[LC24019]
        averageNetFlowLeak_ = netLeakFlowBuffer_.getCurrentAverage();
        averageExhPressure_ = exhWyePressureBuffer_.getCurrentAverage();

		// Every 25ms, calculate flow and pressure slopes for
		// exhalation and inhalation.
		if ((leakCompIndex_ == ((MAX_LEAK_COMP_DATA/2) - 1) ) ||  
			(leakCompIndex_ == (MAX_LEAK_COMP_DATA - 1)))
		{

			// Save the current Average Net flow and exhalation pressure 
			// into a second buffer.
			filteredNetLeakFlow_[leakCompIndex2_] = averageNetFlowLeak_;
			filteredExhPressure_[leakCompIndex2_] = averageExhPressure_;


			// Save the current filtered exhalation pressure.
			currentFilExhPressureData_ = filteredExhPressure_[leakCompIndex2_];


			Uint32 leakCompIndex3 = 0;
			Uint32 leakCompIndex4 = 0;

			// Calculate a new index based on the current index minus delta.
			if (leakCompIndex2_ > DELTA_INDEX_1)
			{
				leakCompIndex3 = (leakCompIndex2_ - DELTA_INDEX_1);
			}
			else
			{
				leakCompIndex3 = (MAX_LEAK_COMP_DATA - (DELTA_INDEX_1 - leakCompIndex2_) ) %MAX_LEAK_COMP_DATA;

			}


			// Calculate a new index based on the current index minus half of delta.
			if (leakCompIndex2_ > (DELTA_INDEX_1 /2))
			{
				leakCompIndex4 = (leakCompIndex2_ - (DELTA_INDEX_1/2));
			}
			else
			{
				leakCompIndex4 = (MAX_LEAK_COMP_DATA - ((DELTA_INDEX_1/2) - leakCompIndex2_) ) %MAX_LEAK_COMP_DATA;

			}

			// $[LC24021]-- Calculate exhalation flow and pressure slopes
			flowSlope1_ = (filteredNetLeakFlow_[leakCompIndex2_] - filteredNetLeakFlow_[leakCompIndex4]);
			flowSlope2_ = (filteredNetLeakFlow_[leakCompIndex4] - filteredNetLeakFlow_[leakCompIndex3]);
			pressureSlope1_ = (filteredExhPressure_[leakCompIndex2_] - filteredExhPressure_[leakCompIndex4]);
			pressureSlope2_ = (filteredExhPressure_[leakCompIndex4] - filteredExhPressure_[leakCompIndex3]);

			// Increment index
			leakCompIndex2_ ++;

			// Reset index back to zero if the current index is
			// greater than the MAX_LEAK_COMP_DATA.
			if (leakCompIndex2_ > (MAX_LEAK_COMP_DATA - 1))
			{
				leakCompIndex2_ = 0;
			}
		}


		// $[LC24013] -- Modify the net flow calculation...
		netFlow_ = exhDryFlow - inspFlow + filteredLeakFlow_;

		// Limit net flow
		if (netFlow_ >  MAX_NET_FLOW)
		{
			netFlow_ = MAX_NET_FLOW;
		}
		else if (netFlow_ < -MAX_NET_FLOW)
		{
			netFlow_ = -MAX_NET_FLOW;
		}
	}
	else // if leak compensation is disabled,
	{
		// reset the leak compensation variables.
		k1Gain_ = 0.0F;
		k2Gain_ = 0.0F;
		finalK1Gain_  = 0.0F;
		pressureSum1_ = 0.0F;
		pressureSum2_ = 0.0F;
		netVolume_ = 0.0F;
		prevLeakVolume_ = 0.0F;
		inspLeakVol_ = 0.0F;
		exhLeakRate_ = 0.0F;
		isLeakDetected_ = FALSE;
		prevFilteredNetFlow_ = 0.0f;
		filteredNetFlow_ = 0.0f;
        exhPressure_ = 0.0f;
		prevAverageExhPressureData_ = 0.0f;
		RExhalationPhase.resetLeakAdd();
		RPeepController.resetLeakAdd();
        netLeakFlowBuffer_.reset();
        exhWyePressureBuffer_.reset();

	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNetFlow
//
//@ Interface-Description
//   This method takes no parameters and returns data member
//   netFlow_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member netFlow_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32 LeakCompMgr::getNetFlow()
{
	return netFlow_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isExhFlowFinished
//
//@ Interface-Description
//   Evaluate if exhalation flow is finished. If so, return true, else
//   return false.
//---------------------------------------------------------------------
//@ Implementation-Description
//   If the pressure and flow are steady and the amount of leak 
//   added to the flow target is greater than 5 then return true.
//   Otherwise return false if the above conditions are not met.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean LeakCompMgr::isExhFlowFinished(void)
{
	Boolean flag = ((RExhalationPhase.getLeakAdd() > 5.0f)  &&
					(isPressureAndFlowSteady_)
				   );

	return flag;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateAvgExhPress(void)
//
//@ Interface-Description
//  	This method takes no argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Sets data member prevAverageExhPressureData_ to the 
//   class member averageExhPressure_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
void LeakCompMgr::updateAvgExhPress(void)
{

	prevAverageExhPressureData_ = averageExhPressure_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateAvgExhPress (Real32 peep)
//
//@ Interface-Description
//   This method takes a peep argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Sets data member prevAverageExhPressureData_ to the value passed in.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
void LeakCompMgr::updateAvgExhPress( Real32 peep)
{

	prevAverageExhPressureData_ = peep;
}





//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update(void)
//
//@ Interface-Description
//   This method takes no parameters, and has no return value.  
//   It is called every BD cycle.  Leak compensation data members are 
//   updated depending upon the phase type (inspiration or exhalation). 
//   If the phase type, is non-breathing, then this method will not update.  
//---------------------------------------------------------------------
//@ Implementation-Description
//   All data members which are used in leak compensation algorithms are 
//   updated every cycle to cycle.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void LeakCompMgr::update()
{

	const Real32 LEAK_OFFSET = 5.0F;
	const Real32 STABILITY_LIMIT = 0.2F; 
	const Real32 DELTA_PEEP = 1.0F;
	const Real32 LEAK_ADJUST_LIMIT = 1.0F;
	const Real32 COMPRESSOR_MAX_DISCO_SENS = 25.0F;

	BreathPhaseType::PhaseType phaseType = BreathRecord::GetPhaseType();
	Real32 inspFlow = RO2FlowSensor.getValue() + RAirFlowSensor.getValue();

	discoSensitivity_ = PhasedInContextHandle::GetBoundedValue(SettingId::DISCO_SENS).value;
    isDiscoSensLimitByComp_ = FALSE;

	if (isLeakCompEnabled_ && isBreathingMode_)
	{


		Real32 sqrAvgExhPress = pow(MAX_VALUE(averageExhPressure_,0.3F)  , 0.5F );

		// Limit the disconnect sensitivity to COMPRESSOR_MAX_DISCO_SENS, if 
		// the compressor is running, Leak Compensation is enabled and the 
		// current setting of disconnect sensitivity is greater than COMPRESSOR_MAX_DISCO_SENS. 
		// $[LC02014]
		if ( (discoSensitivity_ > COMPRESSOR_MAX_DISCO_SENS) // E600 BDIO  &&
// E600 BDIO			 (RCompressor.getCompressorStatus() == Compressor::RUN)
		   )
		{
		   discoSensitivity_ = COMPRESSOR_MAX_DISCO_SENS; 
		   isDiscoSensLimitByComp_ = TRUE;
		}


		// $[LC24003] -- Create a new breath phase parameter, LeakBPhase and define it as follows...
		if (modeValue_ != ModeValue::BILEVEL_MODE_VALUE)
		{
			currentBreathPhaseType_ = phaseType;
		}
		else
		{
			if (BreathRecord::IsHighPeep())
			{
				currentBreathPhaseType_ = BreathPhaseType::INSPIRATION;
			}
			else
			{
				currentBreathPhaseType_ = BreathPhaseType::EXHALATION;
			}

		}

		DiscreteValue triggerType = PhasedInContextHandle::GetDiscreteValue( SettingId::TRIGGER_TYPE) ;


		Boolean pressureTargetCondition     =  FALSE;
		Boolean pressureTrajectoryCondition =  FALSE;

		Real32 peep = 1.0F; 
        Real32 sqrPeep = 0.0f;
		Real32 sqrPrevAvgExhPressData = 0.0f;

		Boolean stabilityPressureCondition = FALSE;

		// $[LC24023]
		if (currentBreathPhaseType_ == BreathPhaseType::INSPIRATION)
		{
			switch (leakCompPhases_)
			{
			case LEAK_COMP_STARTUP:
				leakCompPhases_ = LEAK_COMP_INHALATION;
				k1Gain_ = 0.0F;
				k2Gain_ = 0.0F;
				finalK1Gain_  = 0.0F;
				pressureSum1_ = 0.0F;
				pressureSum2_ = 0.0F;
				netVolume_ = 0.0F;
				prevLeakVolume_ = 0.0F;
				inspLeakVol_ = 0.0F;
				exhLeakRate_ = 0.0F;
				break;
			case LEAK_COMP_INHALATION:
				// $[LC24006] 
				netVolume_ = netVolume_ + filteredNetFlow_;
				inspLeakVol_ = inspLeakVol_ + leakFlow_;
				pressureSum1_ = pressureSum1_ + sqrExhPress_;
				pressureSum2_ = pressureSum2_ + exhPressure_ * sqrExhPress_;
				maxLeakVolume_ = maxLeakVolume_ + maxK1Gain_ * sqrExhPress_ + k2Gain_ * sqrExhPress_ * exhPressure_;



				pressureTargetCondition =
					(RPeep.getActualPeep() + effectivePressure_  - currentFilExhPressureData_ <= 0.5); 
				pressureTrajectoryCondition = 
					(RPeep.getActualPeep() + effectivePressure_  - pressureTrajectory_ <= 0.5); 


				if (modeValue_ != ModeValue::BILEVEL_MODE_VALUE)
				{
					RPeepController.setBiasFlowDelta(0.0F);                    

					const DiscreteValue MAND_TYPE = PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);

					if (MAND_TYPE != MandTypeValue::VCV_MAND_TYPE)
					{
						if (pressureTrajectoryCondition && pressureTargetCondition)
						{
							stabilityPressureCondition = TRUE;
						}
					}
				}

				// Check for stability. If it is stable, recalculate k1
				if ((ABS_VALUE(flowSlope1_) < MAX_VALUE(STABILITY_LIMIT, MIN_VALUE(inspFlow * 0.01f, 0.5f))) &&
					(ABS_VALUE(flowSlope2_) < MAX_VALUE(STABILITY_LIMIT, MIN_VALUE(inspFlow * 0.01f, 0.5f)))  &&
					(ABS_VALUE(pressureSlope1_) < STABILITY_LIMIT) && (ABS_VALUE(pressureSlope2_) < STABILITY_LIMIT) &&
					( stabilityPressureCondition) &&
					(circuitType_ != PatientCctTypeValue::NEONATAL_CIRCUIT) &&
                    (averageExhPressure_ > 0.0f)
				   )
				{

					if (ABS_VALUE(leakFlow_ - averageNetFlowLeak_ )  > LEAK_ADJUST_LIMIT)
					{
						if (ABS_VALUE(leakFlow_ - averageNetFlowLeak_ )  > LEAK_OFFSET)
						{
							k1AndK2Counter_ ++;
						}

						// Recalculate k1
						k1Gain_ = (averageNetFlowLeak_  /sqrAvgExhPress ); 
						
                        k2Gain_ = 0.001;
						
                        // Limit K1
						if (k1Gain_ < 0.0f)
						{
							k1Gain_ = 0.01f;
						}

						if (averageNetFlowLeak_ < 0.3F)
						{
							k1Gain_ = 0.0F;
							k2Gain_ = 0.0F;
						}

						// Recalculate leak flow data.
						leakFlow_ = ( k1Gain_ * sqrExhPress_)  + k2Gain_ * sqrExhPress_ * exhPressure_  ;

						finalK1Gain_ = k1Gain_;

						// Adjust during exhalation at high peep in BILEVEL.
						if (phaseType != BreathPhaseType::INSPIRATION)
						{

						    Real32 actualPeep = MAX_VALUE(RPeep.getActualPeep(), 0.3F);
							Real32 sqrActualPeep = pow(actualPeep , 0.5f);

							RPeepController.setBiasFlowDelta(leakFlow_ -  ((k1Gain_ * sqrActualPeep ) 
																		   + k2Gain_ * (sqrActualPeep * actualPeep ) )); 
							RPeepController.setFilteredBiasFlowDelta(0.0F);
						}
					}
				}


				break;
			case LEAK_COMP_EXHALATION:

				// $[LC24007] -- Save accumulator for the just finished breath
				prevLeakVolume_ = netVolume_;

				if ((circuitType_ == PatientCctTypeValue::NEONATAL_CIRCUIT) &&
					(prevLeakVolume_ < 0.0F))
				{
					prevLeakVolume_ = 0.0F;
				}
				else if (prevLeakVolume_ < 250.0F)
				{
					prevLeakVolume_ = 0.0F;
				}

				maxPreviousLeakVolume_ = maxLeakVolume_ ;


				if (modeValue_ == ModeValue::BILEVEL_MODE_VALUE)
				{
					peep = PhasedInContextHandle::GetBoundedValue(SettingId::PEEP_LOW).value;
				}
				else
				{
					peep = PhasedInContextHandle::GetBoundedValue(SettingId::PEEP).value;
				}

				if (peep < 0.5F)
				{
					peep = 0.1F;
				}

				sqrPeep = pow(peep, 0.5F);

				// Used for disconnect
				maxK1Gain_ = ( discoSensitivity_ /  sqrPeep) - k2Gain_ * sqrPeep * peep;


				// Limit K2 gain if the value is negative.
				if (maxK1Gain_ < 0.0f)
				{
					maxK1Gain_ = discoSensitivity_ /  sqrPeep;					
				}

				// Calculate K1 & K2
				calculateK1K2Leak_();


				// Reset leak comp variables.
				netVolume_ = filteredNetFlow_;
				leakCompPhases_ = LEAK_COMP_INHALATION;

				if (modeValue_ == ModeValue::BILEVEL_MODE_VALUE)
				{
					pressureSum1_ = 0.0F;
					pressureSum2_ = 0.0F;
				}

				maxLeakVolume_ =  (maxK1Gain_ * sqrExhPress_) + k2Gain_ * sqrExhPress_ * exhPressure_; 
                
				inspLeakVol_ = ( (k1Gain_ * sqrExhPress_) + (k2Gain_ * exhPressure_ * sqrExhPress_) );

				break;
			default:
				// Assertion; Unknown Leak Compensation phase.
				AUX_CLASS_PRE_CONDITION(FALSE,leakCompPhases_);
				break;

			}
		}
		else if (currentBreathPhaseType_ == BreathPhaseType::EXHALATION) // $[LC24024]
		{
			switch (leakCompPhases_)
			{
			case LEAK_COMP_STARTUP:

				leakCompPhases_ = LEAK_COMP_STARTUP;
				k1Gain_ = 0.0F;
				k2Gain_ = 0.0F;
				finalK1Gain_  = 0.0F;
				pressureSum1_ = 0.0F;
				pressureSum2_ = 0.0F;
				netVolume_ = 0.0F;
				prevLeakVolume_ = 0.0F;
				break;
			case LEAK_COMP_INHALATION:
				// $[LC24006] 
				netVolume_ = netVolume_ + filteredNetFlow_;
				maxLeakVolume_ = maxLeakVolume_ + maxK1Gain_ * sqrExhPress_ + k2Gain_ * sqrExhPress_ * exhPressure_;

				pressureSum1_ = pressureSum1_ + sqrExhPress_;
				pressureSum2_ = pressureSum2_ + exhPressure_ * sqrExhPress_;
				leakCompPhases_ = LEAK_COMP_EXHALATION;



				// $[LC24042] Calculate Inspiratory Leak Volume.
				prevInspLeakVolume_ = inspLeakVol_ * L_PER_MIN_TO_ML_;

				if (modeValue_ == ModeValue::BILEVEL_MODE_VALUE)
				{
					prevAverageExhPressureData_  = PhasedInContextHandle::GetBoundedValue(SettingId::PEEP_LOW).value;
				}


				if (prevAverageExhPressureData_ < 0.1F)
				{
					prevAverageExhPressureData_ = 0.1F;
				}

				//TODO E600 changed 0.5 to 0.5f to avoid compiler confusion
				sqrPrevAvgExhPressData = pow(prevAverageExhPressureData_, 0.5f);

				// $[LC24042] Calculate Exhalation Leak Rate
			    exhLeakRate_ =  (k1Gain_ * sqrPrevAvgExhPressData + k2Gain_ * sqrPrevAvgExhPressData * prevAverageExhPressureData_);

				// Used for the leak detected message which displays under RM subscreens.    
				if (exhLeakRate_ > 1.0F)
				{
					isLeakDetected_ = TRUE;
				}
				else
				{
					isLeakDetected_ = FALSE;
				}

				// Calculate Percent Leak
				if ((prevInspLeakVolume_ + RLungData.getInspLungVolume()) >= 0.001F)
				{
					percentLeak_ = (prevInspLeakVolume_ / (prevInspLeakVolume_ + RLungData.getInspLungVolume() ) ) * 100;

				}
				else
				{
					percentLeak_ = 0.0F;
				}

				ieTransition_ = FALSE;
				isPartialSteadyLeak_ = FALSE;
                
                // Reset exhalation flow offset
				exhFlowOffset_ = 0.0f;
				break;

			case LEAK_COMP_EXHALATION:
				// $[LC24006] 
				netVolume_ = netVolume_ + filteredNetFlow_;
				maxLeakVolume_ = maxLeakVolume_ + maxK1Gain_ * sqrExhPress_ + k2Gain_ *  sqrExhPress_ * exhPressure_;


				pressureSum1_ = pressureSum1_ + sqrExhPress_;
				pressureSum2_ = pressureSum2_ + exhPressure_ * sqrExhPress_;   


				if (triggerType == TriggerTypeValue::PRESSURE_TRIGGER)
				{
					pressureTargetCondition =   
						(  currentFilExhPressureData_ > DELTA_PEEP); 

				}
				else
				{
					pressureTargetCondition = TRUE;
				}

				// Check for stability.  If it is stable, recalculate k1
				if ((ABS_VALUE(flowSlope1_) < MAX_VALUE(STABILITY_LIMIT, MIN_VALUE(inspFlow * 0.01f, 0.5f))) &&
					(ABS_VALUE(flowSlope2_) < MAX_VALUE(STABILITY_LIMIT, MIN_VALUE(inspFlow * 0.01f, 0.5f)))  && 
					(ABS_VALUE(flowSlope1_ + flowSlope2_) < MAX_VALUE(STABILITY_LIMIT, MIN_VALUE(inspFlow * 0.01f, 0.5f))) &&
					(ABS_VALUE(pressureSlope1_) < STABILITY_LIMIT) && (ABS_VALUE(pressureSlope2_) < STABILITY_LIMIT) &&
					pressureTargetCondition &&
					(BreathRecord::GetTrueExhTime() > 400)  &&
					(averageNetFlowLeak_ > (-inspFlow *0.02f)) 
				   )
				{

                    if (averageExhPressure_ >= 0.0f)
                    {
                        isPressureAndFlowSteady_ = TRUE;                   

                        // Calculate exhalation flow offset.
    					exhFlowOffset_ = RPeepController.getLeakAdd() - averageNetFlowLeak_ ;
    
    					if (ABS_VALUE(leakFlow_ - averageNetFlowLeak_ )  > LEAK_ADJUST_LIMIT)
    					{
    						if (ABS_VALUE(leakFlow_ - averageNetFlowLeak_ )  > LEAK_OFFSET)
    						{
    							k1AndK2Counter_ ++;
    						}
    
    						// Recalculate K1
    						k1Gain_ = (averageNetFlowLeak_  / sqrAvgExhPress);
    						
                            k2Gain_ = 0.001;
    
    						// Limit K1
    						if (k1Gain_ < 0.0f)
    						{
    							k1Gain_ = 0.01f;
    						}
    
    						// Limit K1
    
                            Real32 maxK1Gain =  ( (discoSensitivity_ + 5) / pow(MAX_VALUE(RPeep.getActualPeep(),0.3F), 0.5F));
    
    						if (k1Gain_ > maxK1Gain)
    						{
    							k1Gain_ = maxK1Gain;
    						}
    						
    						// Set K1 and K2 to zero when there is minimal leaks.
    						if (((circuitType_ == PatientCctTypeValue::NEONATAL_CIRCUIT) && (k1Gain_ < 0.0F)) || 
    							((circuitType_ != PatientCctTypeValue::NEONATAL_CIRCUIT) &&  (averageNetFlowLeak_ < 0.3F) 
    							 && (netVolume_ < 250.0F )))
    						{
    							k1Gain_ = 0.0F;
    							k2Gain_ = 0.0F;
    						}
    
    						// Save K1
    						finalK1Gain_ = k1Gain_;
    
    						// Recalculate leak flow data.
    						leakFlow_ = ( k1Gain_ * sqrExhPress_)  + k2Gain_ * sqrExhPress_ * exhPressure_  ;
    
    						// Adjust bias flow if current value is more than required.
    						if ((leakFlow_ - RPeepController.getLeakAdd()) < 0.0f)
    						{
    							RPeepController.setBiasFlowDelta(leakFlow_ - RPeepController.getLeakAdd());                    
    							RPeepController.setFilteredBiasFlowDelta(0.0F); 
    						}
    
    					}

                    }
                    else
                    {
                          netLeakFlowBuffer_.reset();
                          exhWyePressureBuffer_.reset();
                    }


				}
				else
				{
					isPressureAndFlowSteady_ = FALSE;                   
				}

				// Find the end of exhalation
				checkExhalationFlow_();

				// During Bilevel low peep when an exhalation phase of a spont
				// breath is not completed, save current data for k1 & k2 calculation.
				if (modeValue_ == ModeValue::BILEVEL_MODE_VALUE)
				{

					if (phaseType == BreathPhaseType::EXHALATION)
					{
						ieTransition_ = FALSE;

					}
					else if ((phaseType == BreathPhaseType::INSPIRATION) &&
							 (ieTransition_ == FALSE) &&
							 (isSteadyLeak_)
							)
					{
						ieTransition_ = TRUE;
						partialSteadyAvgPress_ = avgSteadyExpPressure_;
						partialPrevLeakVolume_    = netVolume_;
						isPartialSteadyLeak_ = TRUE;
						partialAvgSteadyLeakRate_ = avgSteadyLeakRate_;
						partialPressureSum1_ = pressureSum1_;
						partialPressureSum2_ = pressureSum2_;
					}

				}
				break;
			default:
				// Assertion; Unknown Leak Compensation phase.
				AUX_CLASS_PRE_CONDITION(FALSE,leakCompPhases_);
				break;

			}
		}

		// Increment index
		leakCompIndex_ ++;

		// Reset index if it is greater than MAX_LEAK_COMP_DATA
		if (leakCompIndex_ > (MAX_LEAK_COMP_DATA - 1))
		{
			leakCompIndex_ = 0;
		}

	}
	else
	{
		leakCompPhases_ = LEAK_COMP_STARTUP;
	}
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: computeCompliance(Real32 prevComputeCompliance)
//
//@ Interface-Description
//  This method takes a previous computed compliance and 
//  computes a new compliance volume if certain conditions are met.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If Leak Compensation is enabled, and the current circuit type is 
//  neonatal, K1 > 0 and K2 > 0.15, calculate a new compliance volume.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32 LeakCompMgr::computeCompliance(Real32 prevComputeCompliance)
{

	Real32 computeCompliance = prevComputeCompliance;

	// Calculate a new compliance volume, if Leak Compensation is enabled,
	// and the circuit type is neonatal. $[LC24034]
	if ((isLeakCompEnabled_) && 
		(circuitType_ == PatientCctTypeValue::NEONATAL_CIRCUIT) &&
		(k1Gain_ > 0.0F) &&
		(k2Gain_ > 0.15F) 
	   )
	{
		computeCompliance = computeCompliance * 0.5F;
	}


	return computeCompliance;
}





//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateK1K2Leak_
//
//@ Interface-Description
//  $[LC24038] This method calculates k1Gain_ and k2Gain_ when Leak 
//  Comp is enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If exhalation flow and proximal (exhalation) pressure slope 
//  are stable, calculate k1Gain_ and k2Gain_.  If stability was not
//  met, calculate estimated leak error then calculate k1Gain_ and K2Leak.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void LeakCompMgr::calculateK1K2Leak_()
{

	CALL_TRACE("LeakCompMgr::calculateK1K2Leak_()") ;

	// Low level leak limit is used to decrease the inaccuracy 
    // of the flow sensor when there is a leak.    
    const Real32 LOW_LEVEL_LEAK_LIMIT = 0.4f;


	// If Leak Compensation is enabled, calculate K1 and K2.
	if (isLeakCompEnabled_)
	{

		isExhLeakHigh_ = FALSE;

		// Reset K1 and K2 if Leak is zero or less than 20mL
		if ((prevLeakVolume_ * L_PER_MIN_TO_ML_) < 20.0f)
		{
			prevK1Gain_ = 0.0F;
			prevK2Gain_ = 0.0F;
		}
		else
		{
			// Save K1 and K2
			prevK1Gain_ = k1Gain_;
			prevK2Gain_ = k2Gain_;  
		}

		// Calculated estimated total leak error in mL.
		estimatedLeakError_ = (prevLeakVolume_ - (k1Gain_ * prevPressureSum1_) - (k2Gain_ * prevPressureSum2_)) * L_PER_MIN_TO_ML_;

		// Limit error to 2000mL
		if (estimatedLeakError_ > 2000.0f)
		{
			estimatedLeakError_ = 2000.0f;
		}


		// Reset K1 and K2 to calculate new values
		k1Gain_ = 0.0F;
		k2Gain_ = 0.0F;

		// In Bilevel, set the initial leak comp variables from 
		// the previous calculated leak comp variables during exhalation.
		if (isPartialSteadyLeak_ && !isSteadyLeak_ &&
			(modeValue_ == ModeValue::BILEVEL_MODE_VALUE)            
		   )
		{
			avgSteadyExpPressure_   = partialSteadyAvgPress_;
			prevLeakVolume_  =  partialPrevLeakVolume_ ;
			avgSteadyLeakRate_      = partialAvgSteadyLeakRate_ ;
			prevPressureSum1_       = partialPressureSum1_ ;
			prevPressureSum2_       = partialPressureSum2_ ;
		}

		//TODO E600 changed 0.5 to 0.5f to avoid compiler confusion
        Real32 sqrAvgSteadyExpPress = pow(avgSteadyExpPressure_, 0.5f);
		Real32 temp = prevPressureSum1_ *  sqrAvgSteadyExpPress * avgSteadyExpPressure_ - (prevPressureSum2_ * sqrAvgSteadyExpPress);



		// To prevent division by zero.
		if (prevPressureSum1_ <= 0.001F)
		{
			prevPressureSum1_ = 0.1F;

		}


		// Check stability
		Boolean stabilityCondition = (isSteadyLeak_ || isPartialSteadyLeak_) && (ABS_VALUE(temp) >= 0.001F);

		// When pressure and flow steady conditions are satisfied,
		// calculate K1 and K2.
		if (stabilityCondition)
		{
			Real32 num1 = prevLeakVolume_ * sqrAvgSteadyExpPress * avgSteadyExpPressure_
						  - prevPressureSum2_ * avgSteadyLeakRate_;
			Real32 num2 =  prevPressureSum1_ * avgSteadyLeakRate_ - prevLeakVolume_ * sqrAvgSteadyExpPress;
			
			k1Gain_ = num1 / temp;
			k2Gain_ = num2 / temp;


			// Adjust when the new K1 and K2 are too
			// high.
			if (k1AndK2Counter_ > 0)
			{
				k1Gain_ = prevK1Gain_;
				k2Gain_ = 0.001f;
			}


			// Recalculate K1 and K2 if values are negative.
			if (k1Gain_ < 0.0f || k2Gain_ < 0.0f)
			{
				k2Gain_ =  0.001f;
 		        k1Gain_ = (prevLeakVolume_ )/ prevPressureSum1_;

				if (k1Gain_ < 0.0f)
				{
					k1Gain_ = 0.01f;
				}

			}

			// Save K1 for biasflow calculations
			finalK1Gain_ = k1Gain_;


			if (k2Gain_ >= 0.0f)
			{
				prevK2Gain_ = k2Gain_ / 2.0f ;
			}

			k2Counter_ = 1;

			// Under steady state condition, check if the actual
			// leak rate during exhalation exceeded 90% of the 
			// disconnect sensitivity.
			if (avgSteadyLeakRate_ > (0.9f * discoSensitivity_))
			{
               isExhLeakHigh_ = TRUE;
			}

		}
		else
		{

			// Calculate inspiratory and expiratory tidal volume difference 
			Real32 tidalVolDiff = ( RBreathData.getInspiredLungVolume()  - RBreathData.getExhaledTidalVolume())/ L_PER_MIN_TO_ML_;

			// Enhancement in case of very low leak levels to prevent
			// autocycle
			if (exhLeakRate_ < LOW_LEVEL_LEAK_LIMIT)
			{
				prevLeakVolume_ = MAX_VALUE(prevLeakVolume_,tidalVolDiff );
			}

			// If no steady state was detected, no information is available 
			// regarding steady exhalation leak and therefore this condition
			// is non-binding. The disconnect decision is made only 
			// based on the maximum volume comparison.
			isExhLeakHigh_ = TRUE;

			// Reset average steady expiratory pressure 
			// if steady leak was not detected.
			avgSteadyExpPressure_ = 0.0f;

			// If estimated leak error is less than 100mL or if 
			// previous leak volume is too small, keep the previous K2.
			if ( (ABS_VALUE(estimatedLeakError_) < 100.0f) || 
				 ((prevLeakVolume_ * L_PER_MIN_TO_ML_) < 100.0f))
			{
				k2Gain_ = prevK2Gain_ ;

			}
			else
			{
				// If the error is positive, increment K2. 
				// Otherwise if error is negative, reduce K2 by half.
				if (estimatedLeakError_ > 0.0F)
				{
					k2Gain_ = prevK2Gain_ + 0.001f * (estimatedLeakError_ / (500.0f * k2Counter_));
					k2Counter_ ++;

					// Limit K2 counter to 1000
					if (k2Counter_ > 1000)
					{
						k2Counter_ = 1000;
					}
				}
				else
				{
					k2Gain_ = prevK2Gain_ / 2.0f;
				}
			}

			// Limit K2  
			if (k2Gain_ > 0.5f)
			{
				k2Gain_ = 0.5f;
			}

			// Calculate K1
			if ((prevLeakVolume_ < (1.0f/L_PER_MIN_TO_ML_)) && 
			    (tidalVolDiff < 0.0f))
			{
				k1Gain_ = k1Gain_ - 0.1f;
			}
			else
			{
				k1Gain_ = (prevLeakVolume_ - (k2Gain_ * prevPressureSum2_) )/ prevPressureSum1_;

			}


			// Enhance K1 gain at very small leak rates
			if (( (tidalVolDiff * L_PER_MIN_TO_ML_) >  5.0f ) && (exhLeakRate_ < LOW_LEVEL_LEAK_LIMIT))
			{
                k1Gain_  = MAX_VALUE(k1Gain_, 0.1f); 
			}

			// Adjust when the new K1 and K2 are too
			// high.
			if (k1AndK2Counter_ > 0)
			{
				k1Gain_ = prevK1Gain_;
				k2Gain_ = 0.001f;
			}

			// Recalculate K1 and K2 if values are negative.
			if (k1Gain_ < 0.0f || k2Gain_ < 0.0f)
			{
				k2Gain_ =  0.001f;
				k1Gain_ = (prevLeakVolume_)/ prevPressureSum1_;

				if (k1Gain_ < 0.0f)
				{
					k1Gain_ = 0.01f;
				}

			}

			// Adjust biasflow if stability condition was not met.
			if (((prevExhTime_ + prevInspTime_) > 200.0F) &&
				(prevLeakVolume_ > 0.0F)  &&
				(modeValue_ != ModeValue::BILEVEL_MODE_VALUE) &&
				(circuitType_ == PatientCctTypeValue::NEONATAL_CIRCUIT) &&
				(BreathRecord::GetEndDryExhFlow() < BASE_FLOW_MIN )
			   )
			{
				//TODO E600 changed 0.5 to 0.5f to avoid compiler confusion
				finalK1Gain_ = (prevLeakVolume_ / ( (prevExhTime_ + prevInspTime_) * pow(MAX_VALUE(RPeep.getActualPeep(),0.3F), 0.5f) )) * 5.0F;
			}
			else
			{
				finalK1Gain_ = k1Gain_;
			}

		}



	}

	k1AndK2Counter_ = 0;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkExhalationFlow_
//
//@ Interface-Description
//  This function checks exhalation flow and proximal (exhalation) 
//  pressure slope for stability and end of active exhalation.  
// $[LC24037] 
//---------------------------------------------------------------------
//@ Implementation-Description
//  When Leak Comp is enabled and exhalation flow and pressure are 
//  stable, calculate average steady leak rate and average steady 
//  expiratory pressure.  
//---------------------------------------------------------------------
//@ PreCondition
//  none
//
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void LeakCompMgr::checkExhalationFlow_()
{
	Real32 netFlowAccum  = 0.0F;
	Real32 exhWyePressAccum = 0.0F;

	CALL_TRACE("LeakCompMgr::checkExhalationFlow_()") ;


	// If Leak Compensation is enabled, check exhalation flow for triggering
	// and spirometry.
	if (isLeakCompEnabled_)
	{
		if ((BreathRecord::IsExhFlowFinished() && (NetFlowInspTrigger::STEADY == RNetFlowInspTrigger.getCycleState())) || isPressureAndFlowSteady_)
		{
			netFlowAccum      = netLeakFlowBuffer_.getCurrentSum();
			exhWyePressAccum  = exhWyePressureBuffer_.getCurrentSum(); 

			if (exhWyePressAccum < 0.001F)
			{
				exhWyePressAccum = 0.1F;
			}

			// Calculate average steady leak rate.
			avgSteadyLeakRate_    = netFlowAccum  / MAX_LEAK_COMP_DATA;

			if (avgSteadyLeakRate_ < 0.3F)
			{
				avgSteadyLeakRate_ = 0.0F;
			}

			// Calculate average steady expiratory pressure.
			avgSteadyExpPressure_ = exhWyePressAccum / MAX_LEAK_COMP_DATA;

			if (avgSteadyExpPressure_ < 0.001F)
			{
				avgSteadyExpPressure_ = 0.1F;
			}


			isSteadyLeak_ = TRUE;

		}
	}
	else
	{
		isSteadyLeak_ = FALSE;

	}

}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLeakFlow
//
//@ Interface-Description
//   This method takes no parameters. It returns the value of the 
//   current leak flow for this breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member leakFlow_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getLeakFlow(void) 
{   
	CALL_TRACE("LeakCompMgr::getLeakFlow()");

	return(leakFlow_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getK1Gain
//
//@ Interface-Description
//   This method takes no parameters. It returns the value of the 
//   K1 leak for this breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member k1Gain_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getK1Gain(void) 
{   
	CALL_TRACE("LeakCompMgr::getK1Gain()");

	return(k1Gain_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getK2Gain
//
//@ Interface-Description
//   This method takes no parameters. It returns the value of the 
//   K2 leak for this breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member k2Gain_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getK2Gain(void) 
{   
	CALL_TRACE("LeakCompMgr::getK2Gain()");

	return(k2Gain_);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLeakCompPhases
//
//@ Interface-Description
//   This method takes no parameters. It returns the current leak 
//   compensation phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member leakCompPhases_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getLeakCompPhases(void) 
{   
	CALL_TRACE("LeakCompMgr::getLeakCompPhases()");

	return(leakCompPhases_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPrevLeakVolume
//
//@ Interface-Description
//   This method takes no parameters. It returns the total leak 
//   volume calculated from the previous breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member prevLeakVolume_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getPrevLeakVolume(void) 
{   
	CALL_TRACE("LeakCompMgr::getPrevLeakVolume()");

	return(prevLeakVolume_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNetVolume
//
//@ Interface-Description
//   This method takes no parameters. It returns net amount of 
//   volume per breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member netVolume_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getNetVolume(void) 
{   
	CALL_TRACE("LeakCompMgr::getNetVolume()");

	return(netVolume_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAvgSteadyLeakRate
//
//@ Interface-Description
//   This method takes no parameters. It returns the value of the 
//   Average steady leak rate for this breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member avgSteadyLeakRate_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getAvgSteadyLeakRate(void) 
{   
	CALL_TRACE("LeakCompMgr::getAvgSteadyLeakRate()");

	return(avgSteadyLeakRate_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isSteadyLeak
//
//@ Interface-Description
//   This method takes no parameters and returns data member
//   isSteadyLeak_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member isSteadyLeak_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean
	LeakCompMgr::isSteadyLeak(void) 
{   
	CALL_TRACE("LeakCompMgr::IsSteadyLeak()");

	return(isSteadyLeak_);
}


//=====================================================================
//@ Method: getFlowSlope1
//
//@ Interface-Description
//   This method takes no parameters. It returns the value of the 
//   first flow slope for this breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member FlowSlope1_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getFlowSlope1(void) 
{   
	CALL_TRACE("LeakCompMgr::getFlowSlope1()");

	return(flowSlope1_);
}



//=====================================================================
//@ Method: getPressureSlope1
//
//@ Interface-Description
//   This method takes no parameters. It returns the value of the 
//   first pressure slope for this breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member PressureSlope1_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getPressureSlope1(void) 
{   
	CALL_TRACE("LeakCompMgr::getPressureSlope1()");

	return(pressureSlope1_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFilteredLeakFlow
//
//@ Interface-Description
//   This method takes no parameters and returns data member
//   filteredLeakFlow_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member filteredLeakFlow_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getFilteredLeakFlow(void) 
{   CALL_TRACE("LeakCompMgr::getFilteredLeakFlow()");

	// $[TI1]
	return(filteredLeakFlow_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setEffectivePressure
//
//@ Interface-Description
//  	This method takes a effectivePressure argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//   sets data member effectivePressure_ to the value passed in.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
void LeakCompMgr::setEffectivePressure(Real32 effectivePressure)
{
	CALL_TRACE("LeakCompMgr::setEffectivePressure(Real32 effectivePressure)");
	effectivePressure_ = effectivePressure;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPressureTrajectory
//
//@ Interface-Description
//  	This method takes a pressureTrajectory argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//   sets data member pressureTrajectory_ to the value passed in.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
void LeakCompMgr::setPressureTrajectory(Real32 pressureTrajectory)
{
	CALL_TRACE("LeakCompMgr::SetPressureTrajectory(Real32 pressureTrajectory)");
	pressureTrajectory_ = pressureTrajectory;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPercentLeak
//
//@ Interface-Description
//   This method takes no parameters and returns data member
//   percentLeak_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member percentLeak_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getPercentLeak(void)
{
	CALL_TRACE("LeakCompMgr::getPercentLeak()");

	return percentLeak_;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMaxPreviousLeakVolume
//
//@ Interface-Description
//   This method takes no parameters and returns data member
//   maxPreviousLeakVolume_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member maxPreviousLeakVolume_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getMaxPreviousLeakVolume(void)
{
	CALL_TRACE("LeakCompMgr::getMaxPreviousLeakVolume()");

	// $[TI1]
	return maxPreviousLeakVolume_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetLeakCompParms
//
//@ Interface-Description
//   This method takes no parameters and resets
//   Leak Compensation Parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Sets the  members finalK1Gain_, k1Gain_ and  k2Gain_ to zero.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
void
	LeakCompMgr::resetLeakCompParms(void)
{
	CALL_TRACE("LeakCompMgr::resetLeakCompParms()");

	finalK1Gain_ = 0.0F;
	k1Gain_    = 0.0F;
	k2Gain_    = 0.0F;
	leakCompPhases_ = LEAK_COMP_STARTUP;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFinalK1Gain
//
//@ Interface-Description
//   This method takes no parameters. It returns the value of the 
//   final K1 gain for this breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member finalK1Gain_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32
	LeakCompMgr::getFinalK1Gain(void) 
{   
	CALL_TRACE("LeakCompMgr::getFinalK1Gain()");

	return(finalK1Gain_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getExhLeakRate
//
//@ Interface-Description
//   This method takes no parameters. It returns the value of the 
//   exhalation leak rate for this breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member exhLeakRate_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32 
	LeakCompMgr::getExhLeakRate(void)
{
	return exhLeakRate_;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isLeakDetected
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   isLeakDetected_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member isLeakDetected_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean
	LeakCompMgr::isLeakDetected( void)
{
	CALL_TRACE("LeakCompMgr::isLeakDetected( void)") ;

	// $[TI1]
	return isLeakDetected_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getInspLeakVol
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   prevInspLeakVolume_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member prevInspLeakVolume_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32 
	LeakCompMgr::getInspLeakVol(void)
{
	return prevInspLeakVolume_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getDiscoSensitivity
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   discoSensitivity_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member discoSensitivity_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32 
	LeakCompMgr::getDiscoSensitivity(void)
{
	return discoSensitivity_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isDiscoSensLimitByComp
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   isDiscoSensLimitByComp_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member isDiscoSensLimitByComp_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean 
	LeakCompMgr::isDiscoSensLimitByComp(void)
{
	return isDiscoSensLimitByComp_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getExhFlowOffset
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   exhFlowOffset_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member exhFlowOffset_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32 
	LeakCompMgr::getExhFlowOffset(void)
{
	return exhFlowOffset_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAvgSteadyExpPressure
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   avgSteadyExpPressure_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member avgSteadyExpPressure_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32 
	LeakCompMgr::getAvgSteadyExpPressure(void)
{
	return(avgSteadyExpPressure_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isDisconnectDetected
//
//@ Interface-Description
///  This method takes no parameters and returns true if disconnect
///  is detected otherwise it returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[LC24039]-- When the Leak Compensation is ON, the Circuit Disconnect condition...
//  
//  Disconnect is detected based on the following conditions:
//    1) Previous Leak Volume > Max Previous Leak Volume AND
//    2) Exhalation leak must exceed the disconnect sensitivity setting
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean 
    LeakCompMgr::isDisconnectDetected(void)
{

   return( (prevLeakVolume_ > maxPreviousLeakVolume_) && isExhLeakHigh_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isPressureAndFlowSteady
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   isPressureAndFlowSteady_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member isPressureAndFlowSteady_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean
	LeakCompMgr::isPressureAndFlowSteady(void) 
{   
	CALL_TRACE("LeakCompMgr::isPressureAndFlowSteady()");

	return(isPressureAndFlowSteady_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isBreathingMode
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   isBreathingMode_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member isBreathingMode_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean
	LeakCompMgr::isBreathingMode(void) 
{   
	CALL_TRACE("LeakCompMgr::isBreathingMode()");

	return(isBreathingMode_);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAverageNetFlowLeak
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   averageNetFlowLeak_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member averageNetFlowLeak_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32 LeakCompMgr::getAverageNetFlowLeak()
{
    return averageNetFlowLeak_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAverageExhPressure
//
//@ Interface-Description
///  This method takes no parameters and returns data member
//   averageExhPressure_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the  data member averageExhPressure_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32 LeakCompMgr::getAverageExhPressure()
{
    return averageExhPressure_;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
	LeakCompMgr::SoftFault(const SoftFaultID  softFaultID,
						   const Uint32       lineNumber,
						   const char*        pFileName,
						   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, LEAKCOMPMGR,
							lineNumber, pFileName, pPredicate);
}





