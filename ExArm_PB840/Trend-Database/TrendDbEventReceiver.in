#ifndef TrendDbEventReceiver_IN
#define TrendDbEventReceiver_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
// Class: TrendDbEventReceiver
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbEventReceiver.inv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  ksg    Date:  17-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPatientConnected()
//
//@ Interface-Description
//  Set the patient connected status
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::setPatientConnected( Boolean patientConnected )
{
	isPatientConnected_ = patientConnected;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isPatientConnected()
//
//@ Interface-Description
//  Get the patient connected status
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendDbEventReceiver::isPatientConnected( void ) const
{
	return( isPatientConnected_ );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPatientOcclusion()
//
//@ Interface-Description
//  Set the patient occlusion status
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::setPatientOcclusion( Boolean patientOcclusion )
{
	isPatientCircuitOccluded_ = patientOcclusion;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isPatientOcclusion()
//
//@ Interface-Description
//  Get the patient occlusion status
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendDbEventReceiver::isPatientCircuitOccluded( void ) const
{
	return( isPatientCircuitOccluded_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPatientApnea()
//
//@ Interface-Description
//  Set the patient occlusion status
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::setPatientApnea( Boolean patientApnea )
{
	isPatientApnea_ = patientApnea;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isPatientApnea()
//
//@ Interface-Description
//  Get the patient apnea status
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendDbEventReceiver::isPatientApnea( void ) const
{
	return( isPatientApnea_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPatientSetupComplete()
//
//@ Interface-Description
//  Set the patient setup complete status
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::setPatientSetupComplete( Boolean patientSetupComplete )
{
	isPatientSetupComplete_ = patientSetupComplete;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isPatientSetupComplete()
//
//@ Interface-Description
//  Get the patient setup complete status
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendDbEventReceiver::isPatientSetupComplete( void ) const
{
	return( isPatientSetupComplete_ );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getUserEvents
//
//@ Interface-Description
//  Returns the 32-bit vector corresponding to the User Events
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the User events from the frozen set
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDbEventReceiver::getUserEvents(void) const
{
	return( eventSetData_[frozenEventSet_].eventBits_[USER_EVENT] );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAutoEvents
//
//@ Interface-Description
//  Returns the 32-bit vector corresponding to the Auto Events
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the Auto event from the frozen set
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDbEventReceiver::getAutoEvents(void) const
{
	return( eventSetData_[frozenEventSet_].eventBits_[AUTO_EVENT] );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getEventValue
//
//@ Interface-Description
//  Returns the value for the given event Id
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the event value from the frozen set
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
const TrendValue & TrendDbEventReceiver::getEventValue( TrendEvents::EventId event) const
{
	return( eventSetData_[frozenEventSet_].eventValue_[event] );
}

#endif // TrendDbEventReceiver_IN

