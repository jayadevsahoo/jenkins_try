#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: BufferPool - Buffer Pool Interface Class to VRTX Buffer Services.
//---------------------------------------------------------------------
//@ Interface-Description
//  The BufferPool class provides allocation and deallocation of 
//  statically defined memory blocks using the VRTX partition management
//  functions sc_pcreate(), sc_gblock() and sc_rblock().  The 
//  calling application constructs a BufferPool object with the 
//  specified number and size buffers.  This creates the appropriate
//  VRTX memory partition.  The allocate and free methods use the 
//  VRTX sc_gblock() and sc_rblock() respectively.  This class provides
//  some buffer overwrite protection and multiple "free" protection by 
//  maintaining an allocation flag at the beginning of the buffer.  
//  The free method asserts if the application attempts to free a 
//  buffer that is not allocated.
//---------------------------------------------------------------------
//@ Rationale
//  BufferPool provides a class wrapper for the VRTX partition 
//  management functions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The VRTX memory management functions are used to allocate and free
//  buffers from buffer pool memory.  The class constructor calls
//  the VRTX library function sc_pcreate() to create a memory partition.
//  The Allocate function calls sc_gblock() to get a block from the
//  buffer pool.  The Free function calls sc_rblock() to release the
//  memory block back to the buffer pool.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/BufferPool.ccv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 002   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001    By: Gary Cederquist  Date: 15-APR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "BufferPool.hh"
#include "IpcIds.hh"
//TODO E600 remove
//#include "vrtxil.h"
//@ Usage-Classes
#include "Ipc.hh"
// TODO E600 remove
//#include "Ostream.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

// Buffer struct defines the Buffer object contained in the BufferPool.
// Includes both the data area and the free list pointer.
struct Buffer
{
    Uint            flag;
    Uint            data;
};
 
const Uint            AVAIL      =  0;
const Uint            ALLOC      =  0x7f7f7f7f;

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BufferPool [constructor]
//
//@ Interface-Description
//  Sets up the BufferPool as a VRTX memory partition.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls VRTX sc_pcreate() to create the memory partition.
//  Iterates through the BufferPool_ initializing each buffer
//  flag to AVAIL.
//---------------------------------------------------------------------
//@ PreCondition
//  sizeBuffer >= sizeof(Uint)
//  sizeBuffer % sizeof(Uint) == 0
//  sizePool % sizeBuffer == 0
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BufferPool::BufferPool( Uint poolId, 
                        Uint * pBufferPool, 
                        Uint sizePool, 
                        Uint sizeBuffer)
    : poolId_(poolId),
      pBufferPool_(pBufferPool),
      sizePool_(sizePool),
      sizeBuffer_(sizeBuffer)
{
    CALL_TRACE("BufferPool(Uint, void *, Uint, Uint)");
 
    AUX_CLASS_PRE_CONDITION(sizeBuffer >= sizeof(Uint), sizeBuffer);
    AUX_CLASS_PRE_CONDITION(sizeBuffer % sizeof(Uint) == 0, sizeBuffer);
    AUX_CLASS_PRE_CONDITION(sizePool % sizeBuffer == 0, sizeBuffer);

    //TODO E600 port this
/*    Int32  err = 0;
    sc_pcreate( poolId_, (char*)pBufferPool_,
                (long)sizePool_,
                (long)sizeBuffer_, &err);
    AUX_CLASS_ASSERTION(err == Ipc::OK, err);

    for ( Uint* pBlock = pBufferPool_;
          pBlock < pBufferPool_ + sizePool_ / sizeof(Uint);
          pBlock += sizeBuffer_ / sizeof(Uint))
    {
        *pBlock = AVAIL;
    }*/
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BufferPool [destructor]
//
//@ Interface-Description
//  BufferPool destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BufferPool::~BufferPool(void)
{
    CALL_TRACE("~BufferPool");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: allocate
//
//@ Interface-Description
//  This method allocates a free buffer from the BufferPool. 
//  The size of the memory request is specified to this method which 
//  verifies the requested size does not exceed the size of a buffer.
//  If a buffer is available, this method returns a pointer to the
//  buffer data area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the VRTX sc_gblock() to get a memory block from the memory
//  partition created in Initialize().  Sets the buffer flag to ALLOC.
//---------------------------------------------------------------------
//@ PreCondition
//  nbytes <= sizeBuffer_ - sizeof(Uint)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void *
BufferPool::allocate(Uint nbytes)
{
    AUX_CLASS_PRE_CONDITION( nbytes <= sizeBuffer_ - sizeof(Uint), nbytes );

    //TODO E600 port
    return new char[nbytes];// (void*)NULL;
/*    Int32  err = 0;
    Uint * pBlock = (Uint *)sc_gblock(poolId_, &err);
    AUX_CLASS_ASSERTION(err == Ipc::OK, err);

    *pBlock = ALLOC;

    return pBlock+1;*/
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: free
//
//@ Interface-Description
//  This method deallocates a Buffer previously allocated
//  by the allocate method.  It accepts the memory pointer returned by
//  the allocate method.  After verifying the address is an 
//  allocated buffer, it returns the buffer to the free list. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls VRTX sc_rblock() to release the memory block back to the
//  buffer pool.
//---------------------------------------------------------------------
//@ PreCondition
//  *--pBlock == ALLOC
//  to ensure memory being freed is a BufferPool Buffer.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BufferPool::freeThePool(void * pObject)
{
    //TODO E600 port
	char *pch = (char *)pObject;
	delete []pch;
//    if (pObject != NULL) // $[TI1]
//    {
//        Uint * pBlock = (Uint*)pObject;
//
//        // backup pointer and check "allocated" flag
//        --pBlock;
//
//#ifdef SIGMA_DEVELOPMENT
//        if (*pBlock != ALLOC)
//        {
//            cout << "INVALID FREE OPERATION" << endl;
//            cout << "pBlock       = " << (void*)pBlock << endl;
//            cout << "pBufferPool_ = " << pBufferPool_ << endl;
//            cout << "sizePool_    = " << (void*)sizePool_ << endl;
//            cout << "sizeBuffer_  = " << (void*)sizeBuffer_ << endl;
//        }
//#endif
//
//        // check "allocated" flag
//        AUX_CLASS_PRE_CONDITION(*pBlock == ALLOC, (Uint)pBlock);
//     
//        // clear "allocated" flag
//        *pBlock = AVAIL;
//        Int32  err = 0;
//        sc_rblock(poolId_, (char*)pBlock, &err);
//        AUX_CLASS_ASSERTION(err == Ipc::OK, err);
//    }  
    // else $[TI2]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getFreeBufferCount
//
//@ Interface-Description
//  This method returns the current count of free buffers.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Scans the BufferPool_ for available buffers.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
Int32
BufferPool::getFreeBufferCount(void) const
{
    Int32   freeBufferCount = 0;

    for ( Uint* pBlock = pBufferPool_;
          pBlock < pBufferPool_ + sizePool_ / sizeof(Uint);
          pBlock += sizeBuffer_ / sizeof(Uint))
    {
        if (*pBlock == AVAIL) 
        {   // $[TI1.1]
            ++freeBufferCount;
        }
        // $[TI1.2]
    }
    return freeBufferCount;
    // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getTotalBufferCount
//
//@ Interface-Description
//  This method returns the total number of buffers in the BufferPool.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns value of local NUM_BUFFERS.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
Int32
BufferPool::getTotalBufferCount(void) const
{
    return sizePool_ / sizeBuffer_;
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
BufferPool::SoftFault(const SoftFaultID  softFaultID,
                      const Uint32       lineNumber,
                      const char*        pFileName,
                      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, OS_FOUNDATION,
                          OsFoundation::BUFFERPOOL, 
                          lineNumber, pFileName, pPredicate);
}

 
//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================


