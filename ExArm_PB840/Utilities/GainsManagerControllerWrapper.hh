//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsManagerControllerWrapper.hh
/// @brief Definition of the GainsManagerControllerWrapper class
//----------------------------------------------------------------------------
#ifndef GAINSMANAGERCONTROLLERWRAPPER_HH_
#define GAINSMANAGERCONTROLLERWRAPPER_HH_

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "SigmaTypes.hh"
#include <string>

//----------------------------------------------------------------------------
// GAINS MANAGER TOUCH POINT for adding gains, constants, variables
// for manipulation.
//----------------------------------------------------------------------------

namespace GainsManagerNS
{
    //----------------------------------------------------------------------------
    // C L A S S   D E C L A R A T I O N
    // Declare this class as part of the GainsManagerNS namespace
    //----------------------------------------------------------------------------
    /// @class GainsManagerControllerWrapper
    /// @brief Interface class to provide set/get methods for global objects
    ///
    /// This class provides static methods to allow the GainsManager to
    /// access global objects. The static methods facilitate the use
    /// of function pointers which are used to abstract away the details of
    /// each controller that the GainsManager is managing.
    //-------------------------------------------------------------------------
    class GainsManagerControllerWrapper;
};

//-------------------------------------
// C L A S S    D E F I N I T I O N
//-------------------------------------
class GainsManagerNS::GainsManagerControllerWrapper
{
public:
    //--------------------------------------------------------------------
    /// Sets the ki gain into the Peep Controller
    /// @param[in] r - the gain to set
    //--------------------------------------------------------------------
	static void   setPeepKi( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the ki gain from the Peep Controller
    /// @return the gain
    //--------------------------------------------------------------------
	static Real32 getPeepKi();

    //--------------------------------------------------------------------
    /// Sets the A factor into the Flow Controller
    /// @param[in] r - the factor to set
    //--------------------------------------------------------------------
	static void   setAFactor( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the A factor from the Flow Controller
    /// @return the factor
    //--------------------------------------------------------------------
	static Real32 getAFactor();

    //--------------------------------------------------------------------
    /// Sets the B factor into the Flow Controller
    /// @param[in] r - the factor to set
    //--------------------------------------------------------------------
	static void   setBFactor( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the B factor from the Flow Controller
    /// @return the factor
    //--------------------------------------------------------------------
	static Real32 getBFactor();

    //--------------------------------------------------------------------
    /// Sets the C factor into the Flow Controller
    /// @param[in] r - the factor to set
    //--------------------------------------------------------------------
	static void   setCFactor( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the C factor from the Flow Controller
    /// @return the factor
    //--------------------------------------------------------------------
	static Real32 getCFactor();

    //--------------------------------------------------------------------
    /// Sets the D factor into the Flow Controller
    /// @param[in] r - the factor to set
    //--------------------------------------------------------------------
	static void   setDFactor( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the D factor from the Flow Controller
    /// @return the factor
    //--------------------------------------------------------------------
	static Real32 getDFactor();

    //--------------------------------------------------------------------
    /// Sets the Kp gain into the Pressure or PID Pressure Controller
    /// @param[in] r - the gain to set
    //--------------------------------------------------------------------
    static void   setPressureKp( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the Kp gain from the Pressure or PID Pressure Controller
    /// @return the gain
    //--------------------------------------------------------------------
    static Real32 getPressureKp();

    //--------------------------------------------------------------------
    /// Sets the Ki gain into the Pressure or PID Pressure Controller
    /// @param[in] r - the gain to set
    //--------------------------------------------------------------------
    static void   setPressureKi( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the Ki gain from the Pressure or PID Pressure Controller
    /// @return the gain
    //--------------------------------------------------------------------
    static Real32 getPressureKi();

    //--------------------------------------------------------------------
    /// Sets the Kp gain into the Pressure or PID Pressure Controller
    /// @param[in] r - the gain to set
    //--------------------------------------------------------------------
    static void   setPidKp( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the Kp gain from PID Pressure Controller
    /// @return the gain
    //--------------------------------------------------------------------
    static Real32 getPidKp();

    //--------------------------------------------------------------------
    /// Sets the Ki gain into the PID Pressure Controller
    /// @param[in] r - the gain to set
    //--------------------------------------------------------------------
    static void   setPidKi( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the Ki gain from the PID Pressure Controller
    /// @return the gain
    //--------------------------------------------------------------------
    static Real32 getPidKi();

	//--------------------------------------------------------------------
    /// Sets the Kd gain into the PID Pressure Controller
    /// @param[in] r - the gain to set
    //--------------------------------------------------------------------
    static void   setPidKd( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the Kd gain from the PID Pressure Controller
    /// @return the gain
    //--------------------------------------------------------------------
    static Real32 getPidKd();

};

#endif // GAINSMGRCONTROLLERWRAPPER_HH_
