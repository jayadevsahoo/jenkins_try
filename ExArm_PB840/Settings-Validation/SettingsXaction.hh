
#ifndef SettingsXaction_HH
#define SettingsXaction_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SettingsXaction - Settings Transaction Class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingsXaction.hhv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.  Also,
//	while working on the initial restructuring for adding soft-bound
//	capability, I noticed some unneeded UNIT_TEST blocks.
//  
//  Revision: 002   By: sah    Date: 14-Jul-1997    DR Number: 2263
//  Project: Sigma (R8027)
//  Description:
//	Modified mechansism that initiates transactions and monitors
//	failed transactions to eliminate race condition.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "SettingId.hh"
#include "SettingXmitData.hh"
#include "NetworkMsgId.hh"

//@ Usage-Classes
class  ChangeStateMessage;  // forward declaration...

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
class  BdReadyMessage;    // forward declaration...
class  BatchSettingValues;  // forward declaration...
#elif defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
class  ChangeStateMessage;    // forward declaration...
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
#include "InterTaskMessage.hh"
//@ End-Usage


class SettingsXaction
{
  public:
    //@ Type:  XactionMsg
    // Enumerator of queue message IDs.
    enum XactionMsg
    {
      XACTION_INITIATION_MSG = ::FIRST_APPLICATION_MSG
    };

    static void  TaskDriver(void);

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
    //================================================================
    //  GUI-only public interface...
    //================================================================

    static void  Initiate(const SettingsXactionId xactionId);

#ifdef SIGMA_DEBUG
    // DEBUG ONLY
    static Uint FailSettingsTransaction;
#endif 

#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

#if defined(SIGMA_UNIT_TEST)
    // used by Unit Tests to turn on/off the initiation of a Settings'
    // Transaction...
    static Boolean  DoInitiateXaction;
#endif // defined(SIGMA_UNIT_TEST)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    SettingsXaction(const SettingsXaction&);		// not implemented...
    SettingsXaction(void);				// not implemented...
    ~SettingsXaction(void);				// not implemented...
    void  operator=(const SettingsXaction&);		// not implemented...


    //================================================================
    //  Ubiquitous private interface...
    //================================================================

    static void  ChangeStateCallback_(
				  const ChangeStateMessage& changeStateMsg
				     );

    static void  ProcessFailedXmit_(const Int xactionSocketId);
    static void  ProcessFailedRecv_(const Int xactionSocketId);

    static inline Uint  CalcXactionBlockSize_(const Uint numXmitElems);

    static inline void  SetupQueueMsg_(
				    InterTaskMessage&       rQueueMsg,
				    const SettingsXactionId xactionState,
				    const Uint16            xactionInfo
				      );

    static inline SettingsXactionId  GetXactionState_(
    					const InterTaskMessage& queueMsg
						     );
    static inline Uint16             GetXactionInfo_(
    					const InterTaskMessage& queueMsg
						    );

	static Int  RecvSettingXactionBlockMsg_(Int socketId,
											XmitDataMsgId msgId,
											SettingXactionBlock *pData,
											Uint32 nBytes,
											Uint waitDuration);
	static const Int	XmitSettingXactionBlockMsg_(Int socketId,
											 XmitDataMsgId msgId,
											 const SettingXactionBlock *pData,
											 Uint32 nBytes);


#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
    //================================================================
    //  GUI-only private interface...
    //================================================================

    //@ Type:  XactionStatusId_
    enum XactionStatusId_
    {
      XACTION_SUCCESSFUL,
      OPEN_SOCKET_FAILURE,
      XMIT_FAILURE,
      RECV_FAILURE
    };

    static void     BdReadyCallback_    (const BdReadyMessage& bdReadyMsg);
    static Boolean  BeginClientXaction_ (const SettingsXactionId xactionId);
    static void     ReportXactionStatus_(
				    const XactionStatusId_ xactionStatusId
					);

#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
    //================================================================
    //  BD-only private interface...
    //================================================================

    static void  EstablishConnection_(Int xactionSocketId);
    static void  BeginServerXaction_ (const Int xactionSocketId);

#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

    //@ Data-Member:  FailedXactionId_
    // Used to store the type of transaction requested, when that
    // transaction fails to complete.
    static SettingsXactionId  FailedXactionId_;

    //@ Constant:  INVALID_SOCKET_ID_
    // Contains an invalid socket identifier.
    static const Int  INVALID_SOCKET_ID_;

    //@ Constant:  MAX_ELEMS_BLOCK_SIZE_
    // Static constant that identifies the maximum transaction block size
    // that can be transmitted/received (based on the transmission block
    // and the maximum number of settings that can be transmitted, at one
    // time).
    static const Uint  MAX_ELEMS_BLOCK_SIZE_;

    //@ Constant:  ZERO_ELEMS_BLOCK_SIZE_
    // Static constant that identifies the transaction block size when NO
    // elements are transmitted/received.
    static const Uint  ZERO_ELEMS_BLOCK_SIZE_;

    //@ Constant:  BLOCK_RECV_WAIT_TIME_
    // Static constant that identifies the amount of time (in milliseconds)
    // that a blocked receive will wait.
    static const Uint  BLOCK_RECV_WAIT_TIME_;
};


// Inlined methods...
#include "SettingsXaction.in"


#endif // SettingsXaction_HH 
