#ifndef ServiceModeNovramData_HH
#define ServiceModeNovramData_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class: ServiceModeNovramData - Service Mode data stored in novram
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ServiceModeNovramData.hhv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//   Revision: 001  By: quf    Date: 15-Sep-1997   DR Number: DCS 2385, 2410, 2525
//      Project:   Sigma (840)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 

//@ Usage-Classes

#include "ServiceMode.hh"

//@ End-Usage

class ServiceModeNovramData
{
  public:
    ServiceModeNovramData();
    ~ServiceModeNovramData();
    ServiceModeNovramData(const ServiceModeNovramData& rSmNovramData);
    ServiceModeNovramData& operator=(const ServiceModeNovramData&);

#ifdef SIGMA_BD_CPU
	static void SetFlowSensorCalStatus(const ServiceMode::TestStatus status);
	static ServiceMode::TestStatus GetFlowSensorCalStatus(void);

	static void SetExhValveCalStatus(const ServiceMode::TestStatus status);
	static ServiceMode::TestStatus GetExhValveCalStatus(void);

	static void SetAtmPresXducerCalStatus(const ServiceMode::TestStatus status);
	static ServiceMode::TestStatus GetAtmPresXducerCalStatus(void);

	static void SetVentInopTestPassed(const Boolean testPassed);
	static Boolean IsVentInopTestPassed(void);

	static void SetVentInopTestPhaseId(const Int16 phaseId);
	static Int16 GetVentInopTestPhaseId(void);

#endif  // SIGMA_BD_CPU

    static void     SoftFault(const SoftFaultID softFaultID,
							  const Uint32      lineNumber,
							  const char*       pFileName  = NULL,
							  const char*       pPredicate = NULL);

  private:

// *** NOTE: All data must be common to both BD and GUI! ***

    //@ Data-Member:    flowSensorCalStatus_
    //  Flow sensor calibration status flag
	ServiceMode::TestStatus flowSensorCalStatus_;
 
    //@ Data-Member:    exhValveCalStatus_
    //  exhalation valve calibration status flag
	ServiceMode::TestStatus exhValveCalStatus_;

    //@ Data-Member:    atmPresXducerCalStatus_
    //  atmospheric pressure transducer  calibration status flag
	ServiceMode::TestStatus atmPresXducerCalStatus_;
 
    //@ Data-Member:	ventInopTestPassed_
    //  vent inop test status flag (TRUE = passed)
	Boolean ventInopTestPassed_;

    //@ Data-Member:	ventInopTestPhaseId_
    //  vent inop test phase ID
	Int16 ventInopTestPhaseId_;

};


#endif // ServiceModeNovramData_HH
