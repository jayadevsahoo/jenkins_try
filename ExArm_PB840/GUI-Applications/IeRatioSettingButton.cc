#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: IeRatioSettingButton - A setting button which implements the
// setting display particular to the I to E Ratio, i.e. 1:num or num:1.
//---------------------------------------------------------------------
//@ Interface-Description
// The I:E Ratio Setting button (which inherits from SettingButton) consists of
// a title (I:E) displayed along with the I:E Ratio setting value, either in
// the format 1:num or num:1.  If the I:E Ratio value has not been modified by
// the operator then the value is displayed in normal text, otherwise the 'num'
// part of the display is written in italic text.
//
// Once created, the I:E Ratio Setting Button operates without intervention
// except for needing an activate() call before being displayed (to sync its
// display with the current I:E Ratio setting value).
//
// Only one interface method is defined, updateDisplay_(), which is called
// (via SettingButton) when the button must update its displayed setting value.
//---------------------------------------------------------------------
//@ Rationale
// This class is really a specialization of NumericSettingButton but it is
// needed because the setting value display format changes according to whether
// the I:E Ratio value is above or below 1.0.
//---------------------------------------------------------------------
//@ Implementation-Description
// After construction, most of the operation of this setting button is handled
// by the inherited SettingButton class.  All IeRatioSettingButton really needs
// to do is to react to changes of the I:E Ratio setting which it controls and
// update the numeric value display accordingly.  If the I:E Ratio value is
// less than 1 then it is displayed in the format 1:num, where num is the
// reciprocal of the I:E Ratio.  Otherwise the value is displayed in the format
// num:1 where num actually equals the I:E Ratio.
//
// Only one interface method is defined, updateDisplay_(), which is called (via
// SettingButton) when the button must update its displayed setting value.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/IeRatioSettingButton.ccv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012  By:  hhd	   Date:  24-May-1999    DR Number: 5369 
//    Project:  Sigma (R8027)
//    Description:
//		Removed references to Button::setButtonType() (empty) method and the
//		local variable 'buttonType' used as its argument.
//
//  Revision: 010  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 009  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 007  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/IeRatioSettingButton.ccv   1.16.1.0   07/30/98 10:13:44   gdc
//
//  Revision: 006  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 005  By:  hhd	   Date:  15-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Cleaned up.
//
//  Revision: 004  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 003  By:  hhd    Date:  22-AUG-97    DR Number: 2321
//       Project:  Sigma (R8027)
//       Description:
//				Cleaned up code modification made for DCS 2321.
//
//  Revision: 002  By:  hhd    Date:  20-AUG-97    DR Number: 2321
//       Project:  Sigma (R8027)
//       Description:
//			Added testable items.
//
//  Revision: 001  By:  hhd    Date:  06-AUG-97    DR Number: 2321
//       Project:  Sigma (R8027)
//       Description:
//         Converted decimal point to comma for all European but German language.
//=====================================================================

#include "IeRatioSettingButton.hh"

#include "GuiTimerRegistrar.hh"
#include <math.h>
#include "TextUtil.hh"
#include "PromptArea.hh"

//@ Usage-Classes
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IeRatioSettingButton()  [Constructor]
//
//@ Interface-Description
// Creates an I:E Ratio Setting Button. Passed the following arguments:
// >Von
//	xOrg			The X co-ordinate of the button.
//	yOrg			The Y co-ordinate of the button.
//	width			Width of the button.
//	height			Height of the button.
//	buttonType		Type of the button, either Button::LIGHT,
//					Button::LIGHT_NON_LIT or Button::DARK.
//	bevelSize		Width of the button's internal border.
//	cornerType		Tells whether its corners are rounded or not, can be
//					Button::ROUND or Button::SQUARE.
//	borderType		The border type (whether it has an external border or not).
//	valuePointSize	The point size of the I:E Ratio value display.
//	valueCenterX	The X coordinate around which the the I:E Ratio value field
//					will be centered within the button.
//	valueCenterY	The Y coordinate around which the the I:E Ratio value field
//					will be centered within the button.
//	titleText		The string displayed as the title of the button's numeric
//					value.
//	settingId		The setting whose value the button displays and modifies.
//	pFocusSubScreen	The sub-screen which receives Accept key events from the
//					button, normally the sub-screen which contains the button.
//	messageId		The help message displayed in the Message Area when this
//					button is pressed down.
//	isMainSetting	Flag which specifies whether this setting button is a
//					"Main" setting.  Main setting buttons have slightly
//					different functionality than normal setting buttons.
//>Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Most of the passed-in parameters are passed straight on to the inherited
// SettingButton.  We also store the point size and centering coordinates
// of the for the I:E Ratio value display.  The text field used for
// displaying the I:E Ratio value is added to the label container of
// this button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

IeRatioSettingButton::IeRatioSettingButton(Uint16 xOrg, Uint16 yOrg,
					Uint16 width, Uint16 height, Button::ButtonType buttonType,
					Uint8 bevelSize, Button::CornerType cornerType,
					Button::BorderType borderType,
					TextFont::Size valuePointSize,
					Uint16 valueCenterX, Uint16 valueCenterY,
					StringId titleText, SettingId::SettingIdType settingId,
					SubScreen *pFocusSubScreen, StringId messageId,
					Boolean isMainSetting) :
		SettingButton(xOrg, yOrg, width, height, buttonType, bevelSize,
					  cornerType, borderType, titleText, settingId,
					  pFocusSubScreen, messageId, isMainSetting),
		valuePointSize_(valuePointSize),
		valueCenterX_(valueCenterX),
		valueCenterY_(valueCenterY)
{
	CALL_TRACE("IeRatioSettingButton(...)");

	// Add the text field used
	addLabel(&valueText_);		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~IeRatioSettingButton  [Destructor]
//
//@ Interface-Description
// Destroys an IeRatioSettingButton.  Nothing special to do.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

IeRatioSettingButton::~IeRatioSettingButton(void)
{
	CALL_TRACE("IeRatioSettingButton::~IeRatioSettingButton(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: applicabilityUpdate
//
//@ Interface-Description
//  Called when applicability of the setting id is changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update the display of this button according to its applicability.
//---------------------------------------------------------------------
//@ PreCondition
//  qualifierId is ACCEPTED or ADJUSTED.
//	setting id is valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
IeRatioSettingButton::applicabilityUpdate(
                              const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject
									     )
{
	CALL_TRACE("applicabilityUpdate(qualifierId, pSubject)");

	SAFE_AUX_CLASS_PRE_CONDITION((SUBJECT_ID_ == pSubject->getId()),
								 ((SUBJECT_ID_ << 16) | pSubject->getId()));
	CLASS_PRE_CONDITION((qualifierId == Notification::ACCEPTED  ||
						 qualifierId == Notification::ADJUSTED));

	if ((!isThisMainSetting_()  &&  qualifierId != Notification::ADJUSTED)  ||
		(isThisMainSetting_()   &&  qualifierId != Notification::ACCEPTED))
	{  // $[TI1]
		// either this is NOT a main setting and its NOT an ADJUSTED change, OR
		// it IS a main setting and its NOT an ACCEPTED change...
		return;
	}  // $[TI2]

	const Applicability::Id  NEW_APPLICABILITY =
									pSubject->getApplicability(qualifierId);

	switch (NEW_APPLICABILITY)
	{
	case Applicability::CHANGEABLE :					// $[TI3]
		if (!isThisMainSetting_())
		{		// $[TI3.1]
			if (activityState_ == NO_ACTIVITY)
			{  // $[TI3.1.1] -- applic change NOT due to activation mechanism,
			   //             must be due to change in Constant Parm's value...
				// when a timing setting button becomes newly applicable due to
				// a change to Constant Parm setting, the button needs to be
				// set to the down state ($[CL01001])...
				setToDown();
			}
			else
			{  // $[TI3.1.2] -- applic change due to activation mechanism...
				setToUp();
			}
		}	   	// $[TI3.2]

		setShow(TRUE);
		break;

	case Applicability::VIEWABLE :
	case Applicability::INAPPLICABLE :					// $[TI4]
		// when timing settings are in the VIEWABLE state, they are only to
		// be used within the breath timing bar, therefore all timing setting
		// buttons should be hidden when VIEWABLE...
		setShow(FALSE);
		break;

	default :
		// unexpected applicability id...
		AUX_CLASS_ASSERTION_FAILURE(NEW_APPLICABILITY);
		break;
	}

	if (getShow())
	{	   	// $[TI5]

		// Update our display according to the new setting value.
		updateDisplay_(qualifierId, pSubject);
	}	   	// $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [Protected, virtual]
//
//@ Interface-Description
// Called when the display of this setting button needs to be updated with
// the latest setting value.  Passed a setting context id informing us the
// setting context from which we should retrieve the new setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
// Retrieve the new I:E Ratio value from the Settings-Validation subsystem
// and display it.  The value is stored in the following manner: if the
// value is greater than zero then we display the I:E ratio as "value : 1"
// otherwise if it's less than zero we display "1 : -value".
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
IeRatioSettingButton::updateDisplay_(Notification::ChangeQualifier qualifierId,
									 const SettingSubject*         pSubject)
{
	CALL_TRACE("updateDisplay_(qualifierId, pSubject)");

	BoundedValue        ieRatioValue;
	Boolean             isSettingChanged = FALSE;
	wchar_t                charStyle        = L'N';

	if (qualifierId == Notification::ACCEPTED)
	{													// $[TI1]
		ieRatioValue = pSubject->getAcceptedValue();
	}
	else if (qualifierId == Notification::ADJUSTED)
	{													// $[TI2]
		ieRatioValue = pSubject->getAdjustedValue();

		if (pSubject->isChanged())
		{												// $[TI2.1]
			isSettingChanged = TRUE;
			charStyle        = L'I';
		}												// $[TI2.2]
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(qualifierId);
	}

	// If the I:E Ratio value is less than zero then we display in the
	// 1:num format, else it's num:1.  Round the value to the correct precision.
	if (ieRatioValue.value < 0.0)
	{													// $[TI3]
		// Formulate the cheap text for the 1:num display
		swprintf(valueString_, L"{p=%d,y=%d,%c:1:%.*f}",
					valuePointSize_, valuePointSize_, charStyle,
					- ieRatioValue.precision,
					- ieRatioValue.value );
	}
	else
	{													// $[TI4]
		// Formulate the cheap text for the num:1 display
		swprintf(valueString_, L"{p=%d,y=%d,%c:%.*f:1}",
					valuePointSize_, valuePointSize_, charStyle,
					- ieRatioValue.precision,
					ieRatioValue.value );
	}

	valueText_.setHighlighted(isSettingChanged);

	// Set the displayed text
	valueText_.setText(valueString_);

	// Center the text
	valueText_.setX(valueCenterX_ - (valueText_.getWidth() / 2));
	valueText_.setY(valueCenterY_ - (valueText_.getHeight() / 2));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
IeRatioSettingButton::SoftFault(const SoftFaultID  softFaultID,
							    const Uint32       lineNumber,
							    const char*        pFileName,
							    const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, IERATIOSETTINGBUTTON,
									lineNumber, pFileName, pPredicate);
}
