
#ifndef WaveformSet_HH
#define WaveformSet_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: WaveformSet - Set of waveform records
//---------------------------------------------------------------------
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/WaveformSet.hhv   25.0.4.0   19 Nov 2013 14:00:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  sp    Date:  17-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "WaveformRecord.hh"

//@ End-Usage

static const Int32 NUM_WAVEFORM_SET = 5;
class WaveformSet {
  public:
    WaveformSet(void);
    ~WaveformSet(void);

    inline void clearAll(void);
    inline void resetCurrentIndex(void);
    inline Boolean isFull(void);
    inline const WaveformRecord* getNextWaveformRecord(void);
    inline Boolean add(const WaveformRecord& waverec);

    static void SoftFault(const SoftFaultID softFaultID,
  const Uint32      lineNumber,
  const char*       pFileName  = NULL, 
  const char*       pPredicate = NULL);
  
  protected:

  private:
    WaveformSet(const WaveformSet&);		// not implemented...
    void   operator=(const WaveformSet&);	// not implemented...

    //@ Data-Member:  waveformRecord_
    // an array of waveform records
    WaveformRecord  waveformRecord_[NUM_WAVEFORM_SET];	

    //@ Data-Member:  currentRecord_
    //index to current record
    Int32 currentRecord_;

    //@ Data-Member:  numRecords_
    //The number of waveform records currently stored in the set
    Int32 numRecords_;

};


// Inlined methods...
#include "WaveformSet.in"


#endif // WaveformSet_HH 
