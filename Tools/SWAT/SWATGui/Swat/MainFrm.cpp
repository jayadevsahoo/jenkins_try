// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Swat.h"
#include "MainFrm.h"
#include "SwatView.h"
#include <mmsystem.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_MESSAGE(WM_NEW_LOG_RCVD, &CMainFrame::OnMessage_logRcvd)
	ON_MESSAGE(NEW_SETTINGS_RCVD, &CMainFrame::OnMessage_NewSettingsRcvd)
	ON_MESSAGE(WM_CRITICAL_ERROR_RCVD, &CMainFrame::OnMessage_CriticalErrorRcvd)
	ON_MESSAGE(WM_SHOW_MSG, &CMainFrame::OnMessage_ShowMessage)
	ON_MESSAGE(WM_HIDE_MSG, &CMainFrame::OnMessage_HideMessage)
	ON_MESSAGE(WM_BREATH_PHASE_RCVD, &CMainFrame::OnMessage_BreathPhase)
	/*ON_MESSAGE(WM_TURN_ON_OFF_VENT, &CMainFrame::OnMessage_TurnOnOffVent)*/
	ON_MESSAGE(WM_REPORT_LOG_RCVD, &CMainFrame::OnMessage_ReportLog)

	ON_WM_WINDOWPOSCHANGED()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	m_pMainView = NULL;
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;


	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

/*	if (!m_wndDlgBar.Create(this, IDR_MAINFRAME, 
		CBRS_ALIGN_TOP, AFX_IDW_DIALOGBAR))
	{
		TRACE0("Failed to create dialogbar\n");
		return -1;		// fail to create
	}

	if (!m_wndReBar.Create(this) ||
		!m_wndReBar.AddBar(&m_wndToolBar) ||      
		!m_wndReBar.AddBar(&m_wndDlgBar))
	{
		TRACE0("Failed to create rebar\n");
		return -1;      // fail to create
	}
	*/

	if (!m_wndReBar.Create(this) ||
		!m_wndReBar.AddBar(&m_wndToolBar))
	{
		TRACE0("Failed to create rebar\n");
		return -1;      // fail to create
	}
	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Remove this if you don't want tool tips
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
	

	ModifyStyle(WS_MAXIMIZEBOX, 0, SWP_FRAMECHANGED); 
	SetWindowPos(&CWnd::wndTop , ((CSwatApp*)AfxGetApp())->nSWAT_XOpen_Position, 50 ,
											900, 1000, SWP_FRAMECHANGED);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{

	if( !CFrameWnd::PreCreateWindow(cs) )
  return FALSE;
 // TODO: Modify the Window class or styles here by modifying
 //  the CREATESTRUCT cs

 cs.style &= ~WS_THICKFRAME;
 cs.style &= ~(LONG) FWS_ADDTOTITLE;


 return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers



LRESULT CMainFrame::OnMessage_logRcvd(WPARAM wParam, LPARAM lParam)
{
//	UNREFERENCED_PARAMETER(wParam);

	int nMsgType = wParam;
	CString* pstr = reinterpret_cast<CString*>(lParam);


	//str.GetLength();

	CString str(*pstr);
	if(m_pMainView){
	m_pMainView->RefreshStatusWindow( str, nMsgType );
	}

	delete pstr;
	

   return 0;


}
/*
LRESULT CMainFrame::OnMessage_TurnOnOffVent(WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(wParam);
	
	if(m_pMainView)
	{
		m_pMainView->TurnOnOffVent((bool)lParam);
	}


   return 0;


}*/
LRESULT CMainFrame::OnMessage_NewSettingsRcvd(WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(wParam);
	CString* pstr = reinterpret_cast<CString*>(lParam);

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString str(*pstr);
	pApp->GetNewSettings(str);
	
	delete pstr;

	pApp->m_bNewSettingsRcvd = true;

   return 0;


}


LRESULT CMainFrame::OnMessage_CriticalErrorRcvd(WPARAM wParam, LPARAM lParam)
{

	UNREFERENCED_PARAMETER(wParam);
	CString* pstr = reinterpret_cast<CString*>(lParam);

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString str(*pstr);
	str.TrimRight();

	pApp->LogStatus(str, REPORT_RED); //critical errors are shown in the status window as well
	if(pApp->m_bRun == true) //if script running, stop it.
	{
		pApp->m_bRun = false;
		pApp->m_ScriptResults.m_nErrors++;
		pApp->LogStatus("Error occured in SWAT/Vent interaction. Script stop triggered.");
	}

	if(pApp->m_bPopErrorMsg) //if pop up enabled
	{
		str = "Critical Error Occured: \r\n" + 
					str + "\r\n"
				"Please fix the error and restart SWAT.";
		AfxMessageBox(str,MB_ICONSTOP);

	}
	delete pstr;
	return 0;
}


LRESULT CMainFrame::OnMessage_ShowMessage(WPARAM wParam, LPARAM lParam)
{

	UNREFERENCED_PARAMETER(wParam);
	CString* pstr = reinterpret_cast<CString*>(lParam);

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString str(*pstr);
	str.TrimRight();

	//::PlaySound("c:\\SWAT\\chimes.wav",NULL,SND_FILENAME|SND_ASYNC);

	pApp->LogStatus("USER ACTION REQD: " + str);

	pApp->ShowContinueMessage(str);

	delete pstr;
	
	return 0;
}
LRESULT CMainFrame::OnMessage_HideMessage(WPARAM wParam, LPARAM lParam)
{
	
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->ClearMessage();
		
	return 0;

}

LRESULT CMainFrame::OnMessage_BreathPhase(WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(wParam);
	CString* pstr = reinterpret_cast<CString*>(lParam);

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString str(*pstr);
	str.TrimRight();

	if(m_pMainView){
	m_pMainView->ShowBreathPhase( str );
	}

	delete pstr;

//	pApp->LogStatus(str);
	
	return 0;

}

LRESULT CMainFrame::OnMessage_ReportLog(WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(wParam);
	CString* pstr = reinterpret_cast<CString*>(lParam);

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString str(*pstr);
	str.TrimRight();

	if(m_pMainView)
	{
		pApp->LogReportMessage(str, 0 );
			
	}

	delete pstr;
	
	return 0;
}


void CMainFrame::OnWindowPosChanged(WINDOWPOS* lpwndpos)
{
	CFrameWnd::OnWindowPosChanged(lpwndpos);
	// TODO: Add your message handler code here

	((CSwatApp*)AfxGetApp())->nSWAT_XClose_Position = lpwndpos->x;

}
