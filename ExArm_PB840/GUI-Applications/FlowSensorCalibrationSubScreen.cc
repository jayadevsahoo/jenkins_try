#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FlowSensorCalibrationSubScreen - Activated by selecting Flow Sensor
// Calibration in the Service Lower Other Screen after users press/release the
// Lower Other screen tab button.
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen is activated by selecting Flow Sensor Calibration
// button in the ServiceLowerOtherScreen. 
// Only one instance of this class is created by LowerSubScreenArea.  The
// interface is pretty generic, as a SubScreen it defines the activate()
// and deactivate() methods and then receives button events
// through via the usual means (i.e., the buttonDownHappened(),
// buttonUpHappened() and adjustPanelAcceptPressHappened()) 
//---------------------------------------------------------------------
//@ Rationale
// Groups the components of the Flow Sensor Calibration subscreen in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The main operation of this class is to wait for button down and up events,
// received via buttonUpHappened() ,buttonDownHappened() and 
// adjustPanelAcceptPressHappened().  It also waits for test result data and
// test commands received from Service-Data and dispatched to the virtual
// methods processTestPrompt(), processTestKeyAllowed(), 
// processTestResultStatus(), processTestResultCondition(), and 
// processTestData().  
// On the other hand, button events are caught by the button callback methods. 
// The display of this subscreen is organized into many different screen
// layouts corresponding to different stages of the test process.
// Pressing down the Start Test button causes the exhalation calibration process
// to begin.  The user's responses to the prompts displayed are what drives
// the process forward.
//
// The ServiceDataRegistrar is used to register for servicd data events.
// All other functionality is inherited from the SubScreenArea class.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only be
// displayed in the LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/FlowSensorCalibrationSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:48   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 006   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  12/15/99
//
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 004  By:  yyy    Date: 16-Oct-1997    DR Number: DCS 2565
//  Project:  Sigma (840)
//  Description:
//		Mapped new SRS.
//
//  Revision: 003  By:  yyy    Date:  25-SEP-1997    DR Number:   2385
//       Project:  Sigma (R8027)
//       Description:
//             Use IsFlowSensorCalPassed() to access flow sensor cal status.
//			   10-Oct-1997 Fixed cal failed message.
//
//  Revision: 002  By:  gdc    Date:  23-SEP-1997    DR Number:   2513
//       Project:  Sigma (R8027)
//       Description:
//             Cleaned up Service-Mode interface.
//
//  Revision: 001  By:  yyy    Date:  29-AUG-1997    DR Number:   2279
//       Project:  Sigma (R8027)
//       Description:
//             Initial coding.
//=====================================================================

#include "FlowSensorCalibrationSubScreen.hh"

#include "MiscStrs.hh"
#include "PromptStrs.hh"
#include "SmTestId.hh"
#include "LowerScreen.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceLowerScreenSelectArea.hh"
#include "ServiceUpperScreen.hh"
#include "UpperSubScreenArea.hh"

#if defined FAKE_SM
#	include "FakeServiceModeManager.hh"
#   define SmManager FakeServiceModeManager
#else		
#	include "SmManager.hh"
#endif	// FAKE_SM

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "PromptArea.hh"
#include "Sound.hh"
#include "Colors.hh"
class SubScreenArea;
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Uint16 BUTTON_X_ = 520;
static const Uint16 BUTTON_Y_ = 367;
static const Uint16 BUTTON_WIDTH_ = 110;
static const Uint16 BUTTON_HEIGHT_ = 32;
static const Uint8  BUTTON_BORDER_ = 3;
static const Int32 STATUS_LABEL_X_ = 379;
static const Int32 STATUS_LABEL_Y_ = 0;
static const Int32 STATUS_WIDTH_ = 255;
static const Int32 STATUS_HEIGHT_ = 31;
static const Int32 ERR_DISPLAY_AREA_X1_ = 115;
static const Int32 ERR_DISPLAY_AREA_Y1_ = 100;
static const Int32 ERR_DISPLAY_ROW_WIDTH_ = 20;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlowSensorCalibrationSubScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the Flow Sensor Calibration Subscreen.  The given `pSubScreenArea' is 
// the SubScreenArea in which it will be displayed (in this case the upper 
// subscreen area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Creates, positions, and initializes all the graphical components of this
// subscreen:  title and a number of static and dynamic textual items.
//  Also, register for callback for the Start Test button.
//
// $[07070] Flow sensor calibration subscreen shall provide a button labeled...
// $[07071] Flow sensor calibration subscreen shall provide a display of ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

FlowSensorCalibrationSubScreen::FlowSensorCalibrationSubScreen(SubScreenArea *pSubScreenArea) :
	SubScreen(pSubScreenArea),
	calStatus_(STATUS_LABEL_X_, STATUS_LABEL_Y_,
				STATUS_WIDTH_, STATUS_HEIGHT_,
				MiscStrs::EXH_V_CAL_STATUS_LABEL,
				NULL_STRING_ID),
	errorHappened_(FALSE),
	errorIndex_(0),
	keyAllowedId_(-1),
	userKeyPressedId_(SmPromptId::NULL_ACTION_ID),
	startTestButton_(BUTTON_X_,				BUTTON_Y_,
					BUTTON_WIDTH_,			BUTTON_HEIGHT_,
					Button::DARK, 			BUTTON_BORDER_,
					Button::SQUARE,			Button::NO_BORDER,
					MiscStrs::START_TEST_TAB_BUTTON_LABEL),
	guiTestMgr_(this),
	titleArea_(MiscStrs::FLOW_SENSOR_CALIBRATION_SUBSCREEN_TITLE)

{
	CALL_TRACE("FlowSensorCalibrationSubScreen::FlowSensorCalibrationSubScreen(pSubScreenArea)");

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	for (int i=0; i<MAX_CONDITION_ID; i++)
	{
		errDisplayArea_[i].setColor(Colors::WHITE);
		errDisplayArea_[i].setY(ERR_DISPLAY_AREA_Y1_+(i*ERR_DISPLAY_ROW_WIDTH_));
		errDisplayArea_[i].setX(ERR_DISPLAY_AREA_X1_);
		addDrawable(&errDisplayArea_[i]);
	}

	// Register for callbacks for all buttons in which we are interested.
	startTestButton_.setButtonCallback(this);

	// Add the title area
	addDrawable(&titleArea_);

	// Add the buttons.
	addDrawable(&startTestButton_);

	// Add status line text objects to main container
	addDrawable(&calStatus_);

	// Initialize the promptName, promptId arrays and prompts after an error
	// condition has occured
	setPromptTable_();

	guiTestMgr_.setupTestResultDataArray();
	guiTestMgr_.setupTestCommandArray();
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FlowSensorCalibrationSubScreen  [Destructor]
//
//@ Interface-Description
//  Destroys the Enter Service subscreen.  Needs to do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

FlowSensorCalibrationSubScreen::~FlowSensorCalibrationSubScreen(void)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::~FlowSensorCalibrationSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just before this subscreen is displayed
//  on the screen.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The first thing to do is to update the test status with result 
//  from the last Service run, if any.  Next, register the subscreen with
//  Gui Test manager for test events.  Then we call the generic screen layout
//  method to display the initial subscreen configuration.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::activate(void)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::activate(void)");


	if (GuiApp::IsFlowSensorCalPassed())
	{		// $[TI1]
		calStatus_.setStatus(MiscStrs::EXH_V_CAL_COMPLETE_MSG);
	}
	else
	{		// $[TI2]		
		calStatus_.setStatus(MiscStrs::EXH_V_CAL_FAILED_MSG);
	}

	//
	// Register all general possible test results for test events, but first
	// we have to initialize ServiceDataRegistrar.
	//
	guiTestMgr_.registerTestResultData();
	guiTestMgr_.registerTestCommands();

	// Let the service mode  manager knows the type of Service mode.
	SmManager::DoFunction(SmTestId::SET_TEST_TYPE_MISC_ID);

	layoutScreen_(CALIBRATION_SETUP);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just after this subscreen has been
//  removed from the screen.  No parameters are needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We perform any necessary cleanup.  First signal to Gui Test manager
//  about the end of test.  Release the AdjustPanel focus, then clear
//  the prompt area.  The Start Test button is shown in up position.
//  Finally, the LowerScreen Select area is shown.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::deactivate(void)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::deactivate(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);


	// Pop up the button.
	startTestButton_.setToUp();

	// Activate lower subscreen area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when start buttons in Flow Sensor Calibration subscreen is
// pressed down (because this button registers a callback).
// Passed a pointer to the button as well as 'byOperatorAction', a flag which
// indicates whether the operator pressed the button down or whether the
// setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::buttonDownHappened(Button *pButton,
									Boolean byOperatorAction)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::buttonDownHappened(Button *pButton, "
											"Boolean byOperatorAction)");
	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	if (pButton == &startTestButton_)
	{		// $[TI3]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();

		//
		// Set all prompts 
		//
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								PromptStrs::SM_KEY_ACCEPT_P);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_HIGH,
								NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_LOW,
								NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH,
								PromptStrs::OTHER_SCREENS_CANCEL_S);

		// Set the next test status.
		testStateId_ = START_BUTTON_PRESSED;
	}		// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when any button in Flow Sensor Calibration subscreen is 
// deselected.  Passed a pointer to the button as well as 'byOperatorAction', 
// which indicates whether the operator pressed the button up or whether the 
// setToUp() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
//  There is a possibility that this event occurs after this subscreen is
//  removed from the screen (when this subscreen is deactivated and the
//  Adjust Panel focus is lost).  In this case we do nothing, simply return.
//  Otherwise, we clear the Adjust Panel focus, dehighlight the prompt area
//  and change the display state id. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::buttonUpHappened(Button *pButton, 
													Boolean byOperatorAction)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::buttonUpHappened(Button *pButton, "
										"Boolean byOperatorAction)");

	// Check if the event is operator-initiated
	if (byOperatorAction)
	{													// $[TI1]
		// Clear the all prompts
		// Clear the Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Make sure the prompt area's background is dehighlighted.
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY, 
				PromptArea::PA_HIGH, NULL_STRING_ID);

		// Set the next test status.
		testStateId_ = UNDEFINED_TEST_STATE;
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.  It is
//  normally called when the operator presses down the Accept key to signal the
//  accepting of continuing the test process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First reset the Adjust Panel focus, then clear the prompt area.
//  Next, depending on the display state id, we layout the screen accordingly.
//  The state id is set to UNDEFINED_TEST_STATE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::adjustPanelAcceptPressHappened(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Make sure the prompt area's background is dehighlighted.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_HIGH, NULL_STRING_ID); 

	switch(testStateId_)
	{
	case START_BUTTON_PRESSED:				// $[TI1]

		layoutScreen_(CALIBRATION_START_ACCEPTED);

		break;

	case PROMPT_ACCEPT_BUTTON_PRESSED:		// $[TI2]
		// Set user key pressed id.
		userKeyPressedId_ = SmPromptId::KEY_ACCEPT;
		
		// ACCEPT key being pressed in response to the test PROMPT.
		layoutScreen_(CALIBRATION_PROMPT_ACCEPTED_CLEARED);
		break;

	default:								// $[TI3]
		// Make the Invalid Entry sound
		Sound::Start(Sound::INVALID_ENTRY);
		break;
	}

	testStateId_ = UNDEFINED_TEST_STATE;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_
//
//@ Interface-Description
// Sets the subscreen into one of three display configurations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If screen id is CALIBRATION_SETUP, we update the prompt area and 
//  show the Start Test button.
//  If screen id is CALIBRATION_START_ACCEPTED, we reset the error flag,
//  and error display area, update the prompt area, hide the start test
//  button and the lower select area.  Then we start up the test.
//  The status area is also updated.
//  If screen id is CALIBRATION_PROMPT_ACCEPTED_CLEARED, we update the prompt, status,
//  then communicate this state to Service Mode manager.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::layoutScreen_(CalScreenId calScreenId)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::layoutScreen_(CalScreenId calScreenId)");

	switch (calScreenId)
	{
	case CALIBRATION_SETUP:				// $[TI1]
		// Setup for transfer flash calibration information to GUI board.
		titleArea_.setTitle(MiscStrs::FLOW_SENSOR_CALIBRATION_SUBSCREEN_TITLE);

		// Set all prompts
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_TOUCH_START_TEST_TO_BEGIN_P);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_LOW, NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);

		// Show the startTest button
		startTestButton_.setShow(TRUE);

		break;

	case CALIBRATION_START_ACCEPTED:	// $[TI2]
	{
		// Set all prompts
		// Pop up the startTestButton_.
		startTestButton_.setToUp();

		// Hide the start button.
		startTestButton_.setShow(FALSE);

		// Hide the lower screen select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

		startTest_(SmTestId::FS_CALIBRATION_ID);

		break;
	}	

	case CALIBRATION_PROMPT_ACCEPTED_CLEARED:	// $[TI3]

		// Set Primary prompt to "Please Wait...."
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_PLEASE_WAIT_P);

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

		// Display the overall testing status on the service status area
		calStatus_.setStatus(MiscStrs::EXH_V_CAL_RUNNING_MSG);

		// Signal the Service Test Manager to continue the test.
#ifndef FAKE_SM
		if (userKeyPressedId_ != SmPromptId::NULL_ACTION_ID)
#endif	// !FAKE_SM
		{								// $[TI4]
			SmManager::OperatorAction(SmTestId::FS_CALIBRATION_ID,
						  userKeyPressedId_);
		}								// $[TI5]

		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPromptTable_
//
//@ Interface-Description
//  Initialize the promptId_, promptName_ and conditionText_ tables.  
//  The promptId_ array contains Service-Mode prompt ids.
//  The promptName_ table contains translation text of Service-Mode prompt code.
//  The conditionText_ table contains translation text of Service-Mode
//  error conditions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::setPromptTable_(void)
{
	Int testIndex = 0;

	CALL_TRACE("FlowSensorCalibrationSubScreen::setPromptTable_(void)");

	// First clear each entry in the array.
	for (testIndex = 0; testIndex < MAX_CAL_PROMPTS; testIndex++)
	{
		promptId_[testIndex]	= SmPromptId::NULL_PROMPT_ID;
		promptName_[testIndex] 	= NULL_STRING_ID;
	}

	testIndex = 0;

	promptId_[testIndex] = SmPromptId::CONNECT_AC_PROMPT;
	promptName_[testIndex++] = PromptStrs::SM_CONNECT_AC_P;

	promptId_[testIndex] = SmPromptId::REMOVE_INSP_FILTER_PROMPT;
	promptName_[testIndex++] = PromptStrs::SM_REMOVE_INSP_FILTER_P;

	promptId_[testIndex] = SmPromptId::CONNECT_AIR_PROMPT;
	promptName_[testIndex++] = PromptStrs::SM_CONNECT_AIR_P;

	promptId_[testIndex] = SmPromptId::CONNECT_O2_PROMPT;
	promptName_[testIndex++] = PromptStrs::SM_CONNECT_O2_P;

#ifdef SIGMA_DEVELOPMENT

	// Make sure total test count is within the maximum allowed.
	SAFE_CLASS_ASSERTION(testIndex == MAX_CAL_PROMPTS);

	// Do some safe assertions that all entries in the array have been filled
	for (testIndex = 0; testIndex < MAX_CAL_PROMPTS; testIndex++)
	{
		SAFE_CLASS_ASSERTION(promptId_[testIndex] != SmPromptId::NULL_PROMPT_ID);
		SAFE_CLASS_ASSERTION(promptName_[testIndex] != NULL_STRING_ID);
	}
#endif // SIGMA_DEVELOPMENT
											// $[TI1]
}

//============================ m e t h o d   d e s c r i p t i o n ====
//@ Method: processTestPrompt_
//
//@ Interface-Description
//  Process the prompt command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First, we grab the Adjust Panel focus, then we update the prompt area
//  and the status area.  Next, we update the screen display id.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::processTestPrompt(Int command)");

#if defined FORNOW
printf("alibrationSetupSubScreen::processTestPrompt() command=%d\n", command);
#endif	// FORNOW

	// Grab Adjust Panel focus
	AdjustPanel::TakeNonPersistentFocus(this, TRUE);

	// Disable the knob sound.
	AdjustPanel::DisableKnobRotateSound();

	Int idx;
	// Error checking the prompt received 
	for (idx = 0;
			 idx < MAX_CAL_PROMPTS && promptId_[idx] != command;
			 idx++);
	SAFE_CLASS_ASSERTION(idx < MAX_CAL_PROMPTS);

	// Set primary prompt to the corresponding message.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								promptName_[idx]);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
								PromptArea::PA_HIGH,
								NULL_STRING_ID); 

	switch (keyAllowedId_)
	{
	case SmPromptId::ACCEPT_KEY_ONLY:				// $[TI1]
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY, 
								PromptArea::PA_HIGH,
								PromptStrs::EXH_V_CAL_PROMPT_ACCEPT_S);
		break;

	default:										// $[TI2]
		CLASS_ASSERTION_FAILURE();			// Illegal event
		break;
	}
	



	// Display the overall testing status on the service status area
	calStatus_.setStatus(MiscStrs::EXH_V_CAL_WAITING_MSG);

	// Set the next test status
	testStateId_ = PROMPT_ACCEPT_BUTTON_PRESSED;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//  Process the test result status given by Service Mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dispatch the given command and make the corresponding layoutScreen_
//  call to update the screen display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::processTestResultStatus_"
								"(Int resultStatus Int testResultId)");

#if defined FORNOW
printf("alibrationSetupSubScreen::processTestResultStatus() resultStatus=%d\n", resultStatus);
#endif	// FORNOW

	switch (resultStatus)
	{
	case SmStatusId::TEST_END:			// $[TI1]
		nextTest_();
		break;

	case SmStatusId::TEST_START:		// $[TI2]
		break;

	case SmStatusId::TEST_ALERT:		// $[TI3]
	case SmStatusId::TEST_FAILURE:		// $[TI4]
		errorHappened_ = TRUE;
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//  Process test result condition passed by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Display the text for the corresponding result condition on the screen
//  and update the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//  Result condition has to be in the range [0, MAX_CONDITION_ID]
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::processTestResultCondition(Int resultCondition)");

	SAFE_CLASS_ASSERTION((resultCondition >= 0) && (resultCondition <= MAX_CONDITION_ID));

#if defined FORNOW
printf("FlowSensorCalibrationSubScreen::processTestResultCondition() resultCondition=%d\n", resultCondition);
#endif	// FORNOW

	if (resultCondition > 0)
	{						// $[TI1]
		errDisplayArea_[errorIndex_].setText(conditionText_[resultCondition-1]);
		errDisplayArea_[errorIndex_].setShow(TRUE);
		errorIndex_++;

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_SEE_OP_MANUAL_A);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);
	}						// $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//  Process keyAllowed command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//	For exhalation calibration, this data is ignored.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::processTestKeyAllowed(Int command)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::processTestKeyAllowed(Int command)");

#if defined FORNOW
printf("FlowSensorCalibrationSubScreen::processTestKeyAllowed() command=%d\n", command);
#endif	// FORNOW

	// Remember the expected user key response ID
	keyAllowedId_ = command;
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator release the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the Calibration
// process.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call GuiManager's virtual method to do the job.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::adjustPanelRestoreFocusHappened(void)");

#if defined FORNOW
printf("adjustPanelRestoreFocusHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	guiTestMgr_.restoreTestPrompt();
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  Process data given by Service-Mode
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this subscreen is designed not to display test data,
//  this remains an empty method
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::processTestData(Int dataIndex, TestResult *pResult)");
	// Empty method
	// Flow Sensor Calibration SubScreen doesn't display test data
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setErrorTable_
//
//@ Interface-Description
// 	Sets the error text in the error table.  Any condition that arises
//	has a unique id which is used as an index into this table of condition
//	text.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assign condition text (non-cheap text) to error table.
//	We also initialize the error display area's show flag.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::setErrorTable_(void)
{
	Int testIndex = 0;

	CALL_TRACE("FlowSensorCalibrationSubScreen::setErrorTable_(void)");


#if defined FORNOW
printf("FlowSensorCalibrationSubScreen::setErrorTable_() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Reset status flag
	errorHappened_ = 0;
	errorIndex_ = 0;


	if (currentTestId_ ==SmTestId::FS_CALIBRATION_ID)
	{											// $[TI1]
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_AC_POWER_NOT_CONNECTED;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_NO_AIR_CONNECTED;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_NO_O2_CONNECTED;
		conditionText_[testIndex++] = MiscStrs::CANNOT_ACHIEVE_MIN_AIR_FLOW;
		conditionText_[testIndex++] = MiscStrs::CANNOT_ACHIEVE_MIN_O2_FLOW;
		conditionText_[testIndex++] = MiscStrs::AIR_OFFSET_OOR;
		conditionText_[testIndex]   = MiscStrs::O2_OFFSET_OOR;
	}
	else
	{
		CLASS_ASSERTION_FAILURE();
	}

	// Make sure total test count is within the maximum allowed.
	CLASS_ASSERTION(testIndex <= MAX_CONDITION_ID);

	for (int i=0; i < MAX_CONDITION_ID; i++)
	{	
		errDisplayArea_[i].setShow(FALSE);
	}	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTest_
//
//@ Interface-Description
//	Preparation step for a test to start.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the condition text (error) table.  Display prompts and status.
//	Finally, tell Service Mode Manager to do the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)");

#if defined FORNOW
printf("startTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	currentTestId_ = currentTestId;

	// Set the appropriated error message table.
	setErrorTable_();

	// Set all prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_TESTING_P);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, NULL_STRING_ID); 

	// Display the overall testing status on the service status area
	calStatus_.setStatus(MiscStrs::EXH_V_CAL_RUNNING_MSG);

	// Start the test.
	SmManager::DoTest(currentTestId);
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nextTest_
//
//@ Interface-Description
//	Handles the test and interface control at the end of a Calibration.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Display the appropriate status message based on the test result.
//	The screen goes back to the initial	test's pre-start state.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::nextTest_(void)
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::nextTest_(void)");

#if defined FORNOW
printf("nextTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	setupForRestartCalTest_();

	if (errorHappened_)
	{                                       // $[TI1]
		// Display the calibration failed status message.
		calStatus_.setStatus(MiscStrs::EXH_V_CAL_FAILED_MSG);
		GuiApp::SetFlowSensorCalPassedFlag(FALSE);
	}
	else
	{                                       // $[TI2]
		// Display the calibration completed status message.
		calStatus_.setStatus(MiscStrs::EXH_V_CAL_COMPLETE_MSG);
		GuiApp::SetFlowSensorCalPassedFlag(TRUE);
	}
	
	ServiceUpperScreen::RServiceUpperScreen.getUpperSubScreenArea()->updateVentTestSummaryScreen();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupForRestartCalTest_
//
//@ Interface-Description
//	Prepare the screen for users to restart the test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Show the Lower Select area and START TEST button; Display the status
//	and prompts.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::setupForRestartCalTest_(void)
{
	// Activate the lower screen 's select area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);

	// Set primary prompt to start test.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, 
					PromptStrs::EXH_V_CAL_TOUCH_START_TEST_TO_BEGIN_P);

	// Erase the advisory prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_HIGH,
					NULL_STRING_ID);

	// Set the secondary prompt to To cancel touch other screen.
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, 
					PromptStrs::OTHER_SCREENS_CANCEL_S);

	// Redisplay start button.
	startTestButton_.setShow(TRUE);
													// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
FlowSensorCalibrationSubScreen::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("FlowSensorCalibrationSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							FLOWSENSORCALIBRATIONSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
