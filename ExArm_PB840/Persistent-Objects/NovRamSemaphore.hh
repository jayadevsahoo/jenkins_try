
#ifndef NovRamSemaphore_HH
#define NovRamSemaphore_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  NovRamSemaphore - NOVRAM Semaphore Class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamSemaphore.hhv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 003  By:  sah    Date:  30-Sep-1997    DCS Number: 2530
//  Project:  Sigma (R8027)
//  Description:
//     Provide additional semaphore for use by the internal double buffer
//     instance of 'NovRamSequenceNum'.
//
//  Revision: 002  By:  sah    Date:  25-Jun-1997    DCS Number: 1908
//  Project:  Sigma (R8027)
//  Description:
//     Provide additional semaphores for those double-buffered items that
//     are accessed by time-critical tasks.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "PersistentObjsId.hh"
#include "IpcIds.hh"

//@ Usage-Classes
class  MutEx;
//@ End-Usage


class NovRamSemaphore
{
  public:
    //@ Type:  AccessIdType
    // This enumerator defines the different access semaphores available.
    enum AccessIdType
    {
      DIAG_LOG_ACCESS,
      ALARM_LOG_ACCESS,
      RESULT_TABLE_ACCESS,
      SEQUENCE_NUM_ACCESS,

      // double-buffered data items...
      DOUBLE_BUFF_ACCESS,
#if defined(SIGMA_GUI_CPU)
      DATA_CORRUPTION_INFO_ACCESS,	// accessed by BD-Monitor Task...
#elif defined(SIGMA_BD_CPU)
      BD_SYSTEM_STATE_ACCESS,		// accessed by BD-Control Task...
      FIO2_CAL_INFO_ACCESS,		// accessed by BD-Secondary Task...
#endif // defined(SIGMA_GUI_CPU)
      SEQ_NUM_BUFFER_ACCESS,

      NUM_ACCESS_TYPES
    };

    static void  Initialize(void);

    static void  RequestReadAccess(
			    const NovRamSemaphore::AccessIdType accessId
				  );
    static void  ReleaseReadAccess(
			    const NovRamSemaphore::AccessIdType accessId
				  );

    static void  RequestWriteAccess(
			    const NovRamSemaphore::AccessIdType accessId
			    	   );
    static void  ReleaseWriteAccess(
			    const NovRamSemaphore::AccessIdType accessId
			    	   );

    static inline Boolean  IsBlockingEnabled(void);
    static inline void     EnableBlocking   (void);
    static inline void     DisableBlocking  (void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

    //@ Type:  SemaphoreInfo
    // A struct to store the needed information for each of the access
    // semaphore types.
    struct SemaphoreInfo
    {
      Uint   readAccessCount;
      IpcId  mutexId;
    };

  private:
#if defined(SIGMA_UNIT_TEST)
  // allow public access during unit testing...
  public:
#endif // defined(SIGMA_UNIT_TEST)
    NovRamSemaphore(const NovRamSemaphore&);	// not implemented...
    NovRamSemaphore(void);			// not implemented...
    ~NovRamSemaphore(void);			// not implemented...
    void  operator=(const NovRamSemaphore&);	// not implemented...

    //@ Data-Member:  ArrSemaphoreInfo_
    // A static array of counts of the number of "read" accesses open, for
    // each data item type.
    static SemaphoreInfo  ArrSemaphoreInfo_[NUM_ACCESS_TYPES];

    //@ Data-Member:  RCounterMutEx_
    // Mutual-exlusion semaphore used to protect the read-access counter
    // array.
    static MutEx&  RCounterMutEx_;

    //@ Data-Member:  BlockingEnabledFlag_
    // Static boolean that stores whether blocking is enabled, or not.
    static Boolean  BlockingEnabledFlag_;
};


// Inlined methods...
#include "NovRamSemaphore.in"


#endif // NovRamSemaphore_HH 
