#include "stdafx.h"
#ifdef SIGMA_BD_CPU
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VcvPhase - Implements volume control ventilation inspiration.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class BreathPhase.  Two instances
//		of this class exist at any given time.  One instance for normal settings
//		and one for apnea settings.  Only one instance can be active at any
//		given time.  Plateau phase is also implemented in this class.
//
//		Virtual methods are implemented to initialize the VcvPhase, to perform
//		VcvPhase activities each volume controlled BD cycle, to allow for
//		internal clean up and graceful termination of the breath phase,
//		and to get the btps/compliance compensated volume that is to be
//		delivered.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms for volume based inspiration
//---------------------------------------------------------------------
//@ Implementation-Description
//		VcvPhase main responsibility is to calculate the flow trajectory.
//		The flow trajectory is used by the volume controllers to control
//		the PSOL to deliver the desired flow.  A pressure trajectory is
//		also computed dynamically as input to the exhalation valve
//		controller.
//
//		Other responsibilities are:
//		- managing the volume controller
//		- monitor VCV cycles vs. BD cycles
//		- Managing compliance compensating for volume delivery
//		- control the plateau portion of inspiration
//		- enable the triggers that terminate a mandatory inspiration
//
//		Static data members are implemented to manage the VCV phases
//		(apnea and non-apnea) as one value for determining setting
//		changes and compliance pressures.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		Only two instances of this class may be created
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VcvPhase.ccv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 033  By: rhj    Date:  18-Mar-2010   SCR Number: 6558  
//  Project:  NEO
//  Description:
//		Added a guard to prevent negative desired flows.
//
//  Revision: 032  By: gfu    Date:  22-Apr-2002    DR Number: 5918
//  Project:  VTPC
//  Description:
//		Add comment to indicate functioning of getComplLimitFactor is dependent upon table construction.
//
//  Revision: 031  By: gfu    Date:  19-Mar-2002    DR Number: 5996
//  Project:  VTPC
//  Description:
//		Make getComplLimitFactor a public method.
//
//  Revision: 030  By: sah    Date:  07-Nov-2000    DR Number: 5789
//  Project:  VTPC
//  Description:
//		Handle min and max flow limits for test breath.
//
//  Revision: 029  By: jja     Date:  17-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		Added VTPC
//
//  Revision: 028  By: syw     Date:  09-Mar-2000    DR Number: 5684
//  Project:  NeoMode
//  Description:
//		Added call to FlowController::DeterminePsolLookupFlags() and added the
//		results of these flags as arguments to FlowController::updatePsol().
//
//  Revision: 027  By: sah     Date:  22-Sep-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  added neonatal-specific compliance factor calculations
//
//  Revision: 026  By: syw     Date:  13-Jul-1999    DR Number: 5473
//  Project:  ATC
//  Description:
//		limit cCPeakFlow_ >= 0.0
//
//  Revision: 025  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 024  By:  syw   Date:  13-Oct-1998    DR Number: DCS 5190
//       Project:  Sigma (840)
//       Description:
//			Removed ACTIVE_PENDING checking.
//
//  Revision: 023  By:  syw   Date:  16-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (840)
//       Description:
//			Update peep change status.
//
//  Revision: 022  By:  syw    Date:  07-Jul-1998    DR Number: DCS 5158
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.
//		 	Interface with insp pause maneuver.
//
//  Revision: 021  By: syw    Date:  22-Oct-1997    DR Number: DCS 2564
//  	Project:  Sigma (840)
//		Description:
//			Replace patientType with PatientCctType.
//			
//  Revision: 020  By: syw    Date:  29-Sep-1997    DR Number: DCS 1475
//  	Project:  Sigma (840)
//		Description:
//			Added call to RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_VOLUME_LIMIT)
//			when compliance limitted.
//			
//  Revision: 020  By: syw    Date:  25-Sep-1997    DR Number: DCS 1135
//  	Project:  Sigma (840)
//		Description:
//			Call BreathRecord::SetFirstCycleOfPlateauFlag() during
//			first cycle of plateau
//
//  Revision: 019  By: syw   Date:  15-Sep-1997    DR Number: 2430
//  	Project:  Sigma (840)
//		Description:
//			Added requirement traceability.
//
//  Revision: 018  By: syw   Date:  15-Aug-1997    DR Number: 2388
//  	Project:  Sigma (840)
//		Description:
//			Remove 4 breath running average.
//
//  Revision: 017  By: syw   Date:  12-Aug-1997    DR Number: 2356
//  	Project:  Sigma (840)
//		Description:
//			Use adiabatic factor in compliance compensation algorithm.
//
//  Revision: 016  By: syw   Date:  28-Jul-1997    DR Number: 2330
//  	Project:  Sigma (840)
//		Description:
//			Use inspTime_ and end exh pressure to determine the compliance volume
//			for end end pressure.
//
//  Revision: 015  By: syw   Date:  08-Jul-1997    DR Number: 2284
//  	Project:  Sigma (840)
//		Description:
//			Change DEAD_BAND_MS to 7.0.  Change CLOSE_TIME_MS to 0 if
//			plateau is off.  Use the average of the last 4 LastValidEipForCc_
//			for the eip do determine compliance volume. 
//
//  Revision: 014  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 013  By:  syw    Date:  27-Mar-1997    DR Number: DCS 1780, 1827
//       Project:  Sigma (840)
//       Description:
//             Changed compliance volume code.
//
//  Revision: 012  By:  iv    Date:  01-Feb-1997    DR Number: NONE
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 011 By: syw   Date: 07-Nov-1996   DR Number: DCS 1502 
//  	Project:  Sigma (R8027)
//		Description:
//			Added lastErrorSumUpdated_ flag to handle the updating of the
//			last error sum update instead of using plateau time.  The last
//			error sum update was not being called if plateau != 0 and the
//			VcvPhase was terminated early (due to disconnect).
//
//  Revision: 010 By: syw   Date: 29-Oct-1996   DR Number: DCS 1476 
//  	Project:  Sigma (R8027)
//		Description:
//			LastValidEipForCc_ updated using BreathRecord::GetAirwayPressure().
//			Compliance volume algorithm if pressures are < 0.
//
//  Revision: 009 By: syw   Date: 16-Oct-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Change flowCommandLimit_ to ADULT_MAX_FLOW independent of patient
//			type.
//
//  Revision: 008 By: syw   Date: 18-Sep-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Reset volume controller if there is a mix change.
//
//  Revision: 007 By: syw   Date: 16-Aug-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added call to updateFlowControllerFlags and setControllerShutdown
//			so that the flow controller can now command a zero command.
//
//  Revision: 006 By: sp    Date: 6-May-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Add unit test method setTotalVolumeMl.
//
//  Revision: 005 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Use pressure measurement to store LastValidEipForCc_
//			instead of BreathRecords value.
//
//  Revision: 004 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes:
//			- use explicit zero to close psol instead of cloaseLevel.
//			- eliminated setActiveControllers().
//			- added CLOSE_TIME impact to cCPeafFlow_ calculation.
//
//  Revision: 003 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By:  kam   Date:  29-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "VcvPhase.hh"

#include "ControllersRefs.hh"
#include "TriggersRefs.hh"
#include "BreathMiscRefs.hh"
#include "ValveRefs.hh"
#include "MainSensorRefs.hh"
#include "PhaseRefs.hh"

//@ Usage-Classes

#include "BD_IO_Devices.hh"
#include "VolumeController.hh"
#include "CircuitCompliance.hh"
#include "Btps.hh"
#include "PressureSensor.hh"
#include "FlowSensor.hh"
#include "MathUtilities.hh"
#include "ExhValveIController.hh"
#include "BreathSet.hh"
#include "BreathRecord.hh"
#include "TimerBreathTrigger.hh"
#include "Psol.hh"
#include "UiEvent.hh"
#include "PhasedInContextHandle.hh"
#include "ExhalationPhase.hh"
#include "BdAlarms.hh"
#include "ManeuverRefs.hh"
#include "InspPauseManeuver.hh"
#include "Peep.hh"
#include "VolumeTargetedManager.hh"
#include "BreathPhaseScheduler.hh"
#include "SettingConstants.hh"

//@ End-Usage

//@ Constant: DEAD_BAND_MS
// specifies the deadband time for the Psol $[04155]
static const Real32 DEAD_BAND_MS = 7.0F ;

//@ Constant: MIN_TO_SEC_CF
// minute to seconds conversion factor
static const Real32 MIN_TO_SEC_CF = 60.0F ;

//@ Constant: CLOSE_TIME_MS
// closing time of Psol
static const Real32 CLOSE_TIME_MS = 15.0F ;

//@ Code...

// $[04043] $[04049]
// initialization of static variables
Real32 VcvPhase::LastValidEipForCc_ = 0.0F ; 	// reset CC on power up
DiscreteValue VcvPhase::FlowPattern_ = FlowPatternValue::SQUARE_FLOW_PATTERN ;
Real32 VcvPhase::InspDeliveryTimeMs_ = -1.0F ; 	// to force isControllerReset to TRUE
Real32 VcvPhase::PrevAirCcPeakFlow_ = 0.0F ;
Real32 VcvPhase::PrevO2CcPeakFlow_ = 0.0F ;
Boolean VcvPhase::HcpOrHvpOccured_ = FALSE ;
Boolean VcvPhase::MixChangeOccurred_ = TRUE ;
Real32 VcvPhase::BeginningMix_ = 100.0F ;
Boolean VcvPhase::IsPlateauActive_ = FALSE;


struct ComplLimitFactorInfo_
{
	Real32  ibwValue;
	Real32  factorValue;
};

struct ComplLimitFactorRange_
{
	Real32                 highIbwValue;
	ComplLimitFactorInfo_  limitFactorTable_[10];
};

// $[04041]
// Caution:  method getComplLimitFactor is designed around the structure of this table.  If
// this table is changed, check method logic too.
static const ComplLimitFactorRange_  AdultComplianceLimitFactorTable_[] =
{
	{
		13.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{ 7.0f, 5.0f},
			{ 7.5f, 5.0f},
			{ 8.0f, 5.0f},
			{ 8.5f, 5.0f},
			{ 9.0f, 5.0f},
			{ 9.5f, 5.0f},
			{10.0f, 5.0f},
			{11.0f, 4.92f},
			{12.0f, 4.84f},
			{13.0f, 4.76f}
		}
	},
	{
		23.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{14.0f, 4.68f},
			{15.0f, 4.6f},
			{16.0f, 4.52f},
			{17.0f, 4.44f},
			{18.0f, 4.36f},
			{19.0f, 4.28f},
			{20.0f, 4.2f},
			{21.0f, 4.12f},
			{22.0f, 4.04f},
			{23.0f, 3.96f},
		}
	},
	{
		33.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{24.0f, 3.88f},
			{25.0f, 3.8f},
			{26.0f, 3.72f},
			{27.0f, 3.64f},
			{28.0f, 3.56f},
			{29.0f, 3.48f},
			{30.0f, 3.4f},
			{31.0f, 3.378333f},
			{32.0f, 3.356667f},
			{33.0f, 3.335f}
		}
	},
	{
		43.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{34.0f, 3.313333f},
			{35.0f, 3.291667f},
			{36.0f, 3.27f},
			{37.0f, 3.248333f},
			{38.0f, 3.226667f},
			{39.0f, 3.205f},
			{40.0f, 3.183333f},
			{41.0f, 3.161667f},
			{42.0f, 3.14f},
			{43.0f, 3.118333f}
		}
	},
	{
		65.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{44.0f, 3.096667f},
			{45.0f, 3.075f},
			{46.0f, 3.053333f},
			{47.0f, 3.031667f},
			{48.0f, 3.01f},
			{49.0f, 2.988333f},
			{50.0f, 2.966667f},
			{55.0f, 2.858333f},
			{60.0f, 2.75f},
			{65.0f, 2.736111f}
		}
	},
	{
		130.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{ 70.0f, 2.722222f},
			{ 75.0f, 2.708333f},
			{ 80.0f, 2.694444f},
			{ 85.0f, 2.680556f},
			{ 90.0f, 2.666667f},
			{ 95.0f, 2.652778f},
			{100.0f, 2.638889f},
			{110.0f, 2.611111f},
			{120.0f, 2.583333f},
			{130.0f, 2.555556f}
		}
	},
	{
		150.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{140.0f, 2.527778f},
			{150.0f, 2.5f},
			{150.0f, 2.5f},
			{150.0f, 2.5f},
			{150.0f, 2.5f},
			{150.0f, 2.5f},
			{150.0f, 2.5f},
			{150.0f, 2.5f},
			{150.0f, 2.5f},
			{150.0f, 2.5f}
		}
	},
} ; 


// $[04041]
static const ComplLimitFactorRange_  PedComplianceLimitFactorTable_[] =
{
	{
		8.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{3.5f, 5.0f},
			{4.0f, 5.0f},
			{4.5f, 5.0f},
			{5.0f, 5.0f},
			{5.5f, 5.0f},
			{6.0f, 5.0f},
			{6.5f, 5.0f},
			{7.0f, 5.0f},
			{7.5f, 5.0f},
			{8.0f, 5.0f}
		}
	},
	{
		16.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{ 8.5f, 5.0f},
			{ 9.0f, 5.0f},
			{ 9.5f, 5.0f},
			{10.0f, 5.0f},
			{11.0f, 3.5f},
			{12.0f, 3.1f},
			{13.0f, 2.86f},
			{14.0f, 2.78f},
			{15.0f, 2.7f},
			{16.0f, 2.686667f}
		}
	},
	{
		26.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{17.0f, 2.673333f},
			{18.0f, 2.66f},
			{19.0f, 2.646667f},
			{20.0f, 2.633333f},
			{21.0f, 2.62f},
			{22.0f, 2.606667f},
			{23.0f, 2.593333f},
			{24.0f, 2.58f},
			{25.0f, 2.566667f},
			{26.0f, 2.553333f}
		}
	},
	{
		35.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{27.0f, 2.54f},
			{28.0f, 2.526667f},
			{29.0f, 2.513333f},
			{30.0f, 2.5f},
			{31.0f, 2.5f},
			{32.0f, 2.5f},
			{33.0f, 2.5f},
			{34.0f, 2.5f},
			{35.0f, 2.5f},
			{35.0f, 2.5f}
		}
	},
} ; 


// $[04041]
static const ComplLimitFactorRange_  NeoComplianceLimitFactorTable_[] =
{
	{
		1.4f,				// highIbwValue
		{					// limitFactorTable_[]
			{0.5f, 10.0f},
			{0.6f, 10.0f},
			{0.7f, 10.0f},
			{0.8f,  9.333333f},
			{0.9f,  8.407407f},
			{1.0f,  7.666667f},
			{1.1f,  7.060606f},
			{1.2f,  6.555556f},
			{1.3f,  6.128205f},
			{1.4f,  5.761905f}
		}
	},
	{
		2.4f,				// highIbwValue
		{					// limitFactorTable_[]
			{1.5f, 5.444444f},
			{1.6f, 5.166667f},
			{1.7f, 4.921569f},
			{1.8f, 4.703704f},
			{1.9f, 4.508772f},
			{2.0f, 4.333333f},
			{2.1f, 4.174603f},
			{2.2f, 4.030303f},
			{2.3f, 3.898551f},
			{2.4f, 3.777778f}
		}
	},
	{
		3.4f,				// highIbwValue
		{					// limitFactorTable_[]
			{2.5f, 3.666667f},
			{2.6f, 3.564103f},
			{2.7f, 3.469136f},
			{2.8f, 3.380952f},
			{2.9f, 3.298851f},
			{3.0f, 3.222222f},
			{3.1f, 3.150538f},
			{3.2f, 3.083333f},
			{3.3f, 3.020202},
			{3.4f, 2.960784f}
		}
	},
	{
		8.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{3.5f, 2.904762f},
			{4.0f, 2.666667f},
			{4.5f, 2.5f},
			{5.0f, 2.5f},
			{5.5f, 2.5f},
			{6.0f, 2.5f},
			{6.5f, 2.5f},
			{7.0f, 2.5f},
			{7.5f, 2.5f},
			{8.0f, 2.5f}
		}
	},
	{
		12.0f,				// highIbwValue
		{					// limitFactorTable_[]
			{ 8.5f, 2.5f},
			{ 9.0f, 2.5f},
			{ 9.5f, 2.5f},
			{10.0f, 2.5f},
			{11.0f, 2.5f},
			{12.0f, 2.5f},
			{12.0f, 2.5f},
			{12.0f, 2.5f},
			{12.0f, 2.5f},
			{12.0f, 2.5f}
		}
	},
} ; 


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VcvPhase()
//
//@ Interface-Description
//		Constructor.  The tidal volume, min inspiration flow, plateau time,
//		flow pattern, and insp time id's are passed as arguments.  This method
//		returns nothing.  The rO2Mixture object should be constructed before
//		this class is constructed.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called with the phase type argument.
//		The setting ID's are initialized with the arguments passed in.
//		Register breath phase to O2Mixture.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
VcvPhase::VcvPhase(	const SettingId::SettingIdType tidalVolumeId,
					const SettingId::SettingIdType minInspFlowId,
					const SettingId::SettingIdType plateauTimeId,
					const SettingId::SettingIdType flowPatternId,
					const SettingId::SettingIdType inspTimeId)
					: BreathPhase(BreathPhaseType::INSPIRATION)		// $[TI1]
{
	CALL_TRACE( "VcvPhase::VcvPhase( const SettingId::SettingIdType tidalVolumeId, \
									 const SettingId::SettingIdType minInspFlowId, \
				 					 const SettingId::SettingIdType plateauTimeId, \
				 					 const SettingId::SettingIdType flowPatternId, \
				 					 const SettingId::SettingIdType inspTimeId)") ;

	// $[TI2]
	tidalVolumeId_ 	= tidalVolumeId ;
	minInspFlowId_ 	= minInspFlowId ;
	plateauTimeId_ 	= plateauTimeId ;
	flowPatternId_ 	= flowPatternId ;
	inspTimeId_		= inspTimeId ;

	RO2Mixture.registerBreathPhaseCallBack( this) ;

	tempCktCompFactor_ = 1.0;
	tempCktCompFactorSet_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VcvPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
VcvPhase::~VcvPhase( void)
{
	CALL_TRACE("VcvPhase::~VcvPhase( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
//		This method has BreathTrigger& as an argument and has no return
// 		value.  This method is called at the beginning of exhalation to wrap
// 		up inspiration phase in preparation for the next inspiration phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//      If the trigger to this breath was a manual insp trigger, inform GUI.
//		The end inspiration pressure is stored for compliance compensation
//		calculations if breath was not terminated by specific triggers.
//		The summers are updated for the last interval if they haven't been
//		already updated by newCycle() since plateau was zero.
// 		$[04037] $[04038] $[04043] $[04044] $[04046] $[04047] $[04048]
// 		$[04157] $[04161]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
VcvPhase::relinquishControl( const BreathTrigger& trigger)
{
	CALL_TRACE("VcvPhase::relinquishControl( BreathTrigger& trigger)") ;
	
	Trigger::TriggerId startingTrigid = PBreathTrigger_->getId() ;
	
	// if the trigger to this breath was a manual insp trigger, indicate completion
	// of phase delivery to GUI

	if (startingTrigid == Trigger::OPERATOR_INSP)
	{
		// $[TI1.1]
		((UiEvent&)RManualInspEvent).setEventStatus( EventData::COMPLETE) ;
	}	// implied else $[TI1.2]
	
	Trigger::TriggerId terminatingTrigId = trigger.getId() ;
	
	// update pressure for compliance compensation if no HCP, no HVP, no occlusion,
	// no disconnect, and no SVO.  An occlusion, disconnect or SVO are detected if
	// terminatingTrigId = IMMEDIATE_BREATH.  

	HcpOrHvpOccured_ = FALSE ;

	if (terminatingTrigId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP
				|| terminatingTrigId == Trigger::HIGH_VENT_PRESSURE_EXP)
	{
		// $[TI2.2]
		HcpOrHvpOccured_ = TRUE ;
	}
	else if (terminatingTrigId != Trigger::IMMEDIATE_BREATH)
	{
		// $[TI2.1]
		// LastValidEipForCc_ is end insp pressure (includes plateau pressure if active)
		Real32 press = 0.5F * (RInspPressureSensor.getValue()
						+ RExhPressureSensor.getValue()) ;
		
		LastValidEipForCc_ = 0.5F * (BreathRecord::GetAirwayPressure() + press) ;
	}	// impled else $[TI2.3]

	if (lastErrorSumUpdated_ == FALSE)
	{
		// $[TI3.1]
    	Real32 desiredFlow = calculateDesiredFlow_() ;
    	Real32 desiredO2Flow = desiredFlow * RO2Mixture.calculateO2Fraction() ;
	    Real32 desiredAirFlow = desiredFlow - desiredO2Flow ;
      	
      	RO2VolumeController.updateLastErrorSum( desiredO2Flow) ;
	    RAirVolumeController.updateLastErrorSum( desiredAirFlow) ;
	    intervalNumber_ ++ ;
   	}	// implied else $[TI3.2]
   	
	// $[VC24017]
	RVolumeTargetedManager.setEndPlateauPress( RExhPressureSensor.getValue()) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
// 		This method has no arguments and has no return value.  This method is
// 		called every BD cycle during volume control ventilation phase to control
// 		the PSOLs and exhalation valve to deliver volume based inspirations.
//---------------------------------------------------------------------
//@ Implementation-Description
//		A timer is implemented to ensure that the volume controller is
//		updated every VOLUME_CYCLE_TIME_MS msecs.  Plateaus are controlled
//		every VOLUME_CYCLE_TIME_MS instead of every CYCLE_TIME_MS since
//		the resolution on plateau times is 100 msecs.  ImmediateBreathTrigger
//		is enabled 1 BD cycle before the end of inspiration to ensure
//		proper termination of inspiration.  When plateau is first detected,
//		activate the insp. pause maneuver.  During a plateau, the error
//		sums are update on the first plateau cycle.  To avoid updating
//		the error sums again when relinquishControl is executed, the flag
//		lastErrorSumUpdated_ is set and checked in relinquishControl to avoid
//		the update.
//		$[04025] $[04026] $[04150] $[04157] $[04160] $[04163] 
//		$[04165] $[04166] $[04279] 
//---------------------------------------------------------------------
//@ PreCondition
//		elapsedTimeMs_ < inspTimeMs_
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
VcvPhase::newCycle(void)
{
	CALL_TRACE("VcvPhase::newCycle(void)") ;
	
	CLASS_PRE_CONDITION( elapsedTimeMs_ < inspTimeMs_) ;

	// enable trigger one cycle (CYCLE_TIME_MS) before the end of inspiration 
   	if (elapsedTimeMs_ >= plateauTimeMs_
	   		 + InspDeliveryTimeMs_ - CYCLE_TIME_MS)
	{
		// $[TI1.1]
   		((BreathTrigger&)RImmediateExpTrigger).enable() ;
   	}	// implied else $[TI1.2]
   
   	// controlTimerMs_ is initialized to VOLUME_CYCLE_TIME_MS by newBreath()
   	controlTimerMs_ += CYCLE_TIME_MS ;

	Real32 desiredFlow = 0.0F ;
	Real32 	desiredAirFlow = 0.0F ;
	Real32 desiredO2Flow = 0.0F ;
	
   	if (controlTimerMs_ >= VOLUME_CYCLE_TIME_MS) 
   	{
		// $[TI2.1]
	   	// the following is executed every VOLUME_CYCLE_TIME_MS
	   	
		updateFlowControllerFlags_() ;
		
      	controlTimerMs_ = 0 ;

		if (!IsEquivalent( RO2Mixture.getTargetO2Percent(), BeginningMix_, HUNDREDTHS))
		{
			// $[TI3.1]
			MixChangeOccurred_ = TRUE ;
		}	// implied else $[TI3.2]
		
      	if (elapsedTimeMs_ < InspDeliveryTimeMs_)
      	{
			// $[TI4.1]
      		// volume control code
	      	desiredFlow = calculateDesiredFlow_() ;
      	
   	  		desiredO2Flow = desiredFlow * RO2Mixture.calculateO2Fraction() ;
	      	desiredAirFlow = desiredFlow - desiredO2Flow ;

      		intervalNumber_++ ;

			Boolean useAirLookup = FALSE ;
			Boolean useO2Lookup = FALSE ;

			VolumeController::DeterminePsolLookupFlags( desiredAirFlow, desiredO2Flow,
												useAirLookup, useO2Lookup) ;

	   		RAirVolumeController.updatePsol( desiredAirFlow, useAirLookup) ;
   			RO2VolumeController.updatePsol( desiredO2Flow, useO2Lookup) ;
    	}
    	else
    	{
			// $[TI4.2]
    		// plateau code
			if (IsEquivalent( (Real32)elapsedTimeMs_, InspDeliveryTimeMs_, HUNDREDTHS))
	      	{
				// $[TI5.1]
				SAFE_CLASS_ASSERTION( !IsEquivalent( plateauTimeMs_, 0.00, HUNDREDTHS)) ;

				// $[VC24017]
				RVolumeTargetedManager.setBeginPlateauPress( RExhPressureSensor.getValue()) ;
				
	      		// execute during first plateau cycle
		      	desiredFlow = calculateDesiredFlow_() ;
      	
		   	  	desiredO2Flow = desiredFlow * RO2Mixture.calculateO2Fraction() ;
      			desiredAirFlow = desiredFlow - desiredO2Flow ;
      			intervalNumber_++ ;

	      		RAirVolumeController.updateLastErrorSum( desiredAirFlow) ;
				RO2VolumeController.updateLastErrorSum( desiredO2Flow) ;
				lastErrorSumUpdated_ = TRUE ;

				BreathRecord::SetFirstCycleOfPlateauFlag() ;

				if (RInspPauseManeuver.getManeuverState() == PauseTypes::PENDING &&
					RInspPauseManeuver.isManeuverAllowed())
				{
					// $[TI6.1]
					// $[BL04009] :a start inspiratoyr pause phase at beginning of
					// plateau
					// Activate the insp. pause maneuver
					RInspPauseManeuver.activate();

	               	// notify the user that inspiratory pause is active:
    	            RInspiratoryPauseEvent.setEventStatus(EventData::PLATEAU_ACTIVE);
				}	// implied else $[TI6.2]

				IsPlateauActive_ = TRUE;
	      	}	// implied else $[TI5.2]
			// use flow controller to shut psols
			RAirFlowController.setControllerShutdown( TRUE) ;
			RO2FlowController.setControllerShutdown( TRUE) ;
	   	  	desiredO2Flow = 0.0F ;
   			desiredAirFlow = 0.0F ;
   			
	   		RAirFlowController.updatePsol( desiredAirFlow) ;
   			RO2FlowController.updatePsol( desiredO2Flow) ;
		}
   	}	// implied else $[TI2.2]
   	
	elapsedTimeMs_ += CYCLE_TIME_MS ;

	Real32	targetPressure = MIN_VALUE( PRESSURE_TO_SEAL
											+ RExhPressureSensor.getFilteredValue(),
										highCircuitPressure_ + PRESSURE_ABOVE_HCP) ;
   	RExhValveIController.updateExhalationValve( targetPressure, 0.0F) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
// 		This method has no arguments and has no return value.  This method is
// 		called at the beginning of each volume based inspiration to update
// 		parameters needed for inspiration.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Determine if the controller needs to be reset.  Obtain new
//		values from settings.  Calculate compliance compensated peak
//		flow for the desired waveshape.  Initialize the volume controllers to
//		their proper states.  Initialize the volume controllers and exhalation
//		valve controllers with their newBreath() methods.  Enable the high
//		pressure triggers.
//		$[04031] $[04034] $[04040] $[04082] $[04152] $[04153] $[04155]
//		$[04156] $[04159] $[04078] $[04085] $[04091] $[04096] $[04167]
//		$[04258]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
VcvPhase::newBreath( void)
{
	CALL_TRACE("VcvPhase::newBreath( void)") ;
	 	
   	Boolean isControllerReset = checkResetConditions_() ;

	flowCommandLimit_ = ADULT_MAX_FLOW ;

	// update settings for volume phase

   	// round inspTimeMs_ down to nearest VOLUME_CYCLE_TIME_MS.  Volume lost due to
   	// rounding will be compensated for with higher peak flow.
   	inspTimeMs_	= (Real32)(PhasedInContextHandle::GetBoundedValue( inspTimeId_).value
   								/ VOLUME_CYCLE_TIME_MS)
   						* VOLUME_CYCLE_TIME_MS ;

	if (PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE) ==
			MandTypeValue::VCP_MAND_TYPE &&
		BreathPhaseScheduler::GetCurrentScheduler().getId() != SchedulerId::APNEA)
	{
		// $[TI8]
		// $[VC24014]
		// Do a test VCV breath using modified VTPC settings
		tidalVolumeMl_ 	= PhasedInContextHandle::GetBoundedValue( tidalVolumeId_).value ;
		FlowPattern_ 	= FlowPatternValue::RAMP_FLOW_PATTERN ;
		plateauTimeMs_ 	= MIN_VALUE( 200.0F, inspTimeMs_ - 200.0F) ;

		Real32 peakFlow = tidalVolumeMl_ * 2.0F * 60.0F / (inspTimeMs_ - plateauTimeMs_)  ;

	    const DiscreteValue  CIRCUIT_TYPE =
    	    PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE) ;

		Real32 minPeakFlow = 0.0 ;
		
	    switch (CIRCUIT_TYPE)
    	{
    		case PatientCctTypeValue::ADULT_CIRCUIT :
				// $[TI13]
				minPeakFlow = SettingConstants::MIN_PEAK_FLOW_PED_ADULT_VALUE ;
	        	break ;

	    	case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
				// $[TI14]
				minPeakFlow = SettingConstants::MIN_PEAK_FLOW_PED_ADULT_VALUE ;
		        break ;
	        
		    case PatientCctTypeValue::NEONATAL_CIRCUIT :
				// $[TI15]
				minPeakFlow = SettingConstants::MIN_PEAK_FLOW_NEO_VALUE ;
		        break ;

	    	default :
    	    	AUX_CLASS_ASSERTION_FAILURE( CIRCUIT_TYPE) ;
	    	    break ;
		}
		
		if (peakFlow > flowCommandLimit_)
		{
			// $[TI9]
			peakFlow = flowCommandLimit_ ;
		}
		else if (peakFlow < minPeakFlow)
		{
			// $[TI12]
			peakFlow = minPeakFlow ;

			// adjust plateau time to make up for the min flow

			Real32 tempQMin = MAX_VALUE( 0.8F, 0.1F * peakFlow) ;
			Real32 actualInspTime = tidalVolumeMl_ * 2.0F * 60.0F / ( tempQMin + peakFlow) ;

			plateauTimeMs_ = inspTimeMs_ - actualInspTime ;
		}
		// $[TI10]
		qMin_ = MAX_VALUE( 0.8F, 0.1F * peakFlow) ;
	}
	else
	{
		// $[TI11]
		// Do a normal VCV breath using VCV settings
		tidalVolumeMl_ 	= PhasedInContextHandle::GetBoundedValue( tidalVolumeId_).value ;
		qMin_			= PhasedInContextHandle::GetBoundedValue( minInspFlowId_).value ;
		FlowPattern_ 	= PhasedInContextHandle::GetDiscreteValue( flowPatternId_) ;
		plateauTimeMs_ 	= PhasedInContextHandle::GetBoundedValue( plateauTimeId_).value ;
	}
	
   	highCircuitPressure_ 	= PhasedInContextHandle::GetBoundedValue( SettingId::HIGH_CCT_PRESS).value ;
	BeginningMix_ = RO2Mixture.getTargetO2Percent() ;
								   	
	IsPlateauActive_ = FALSE;
   	InspDeliveryTimeMs_ = inspTimeMs_ - plateauTimeMs_ ;
   	
	//////////////////////////////////////////////////////////////////////
	// calculate compliance volume
	// TODO E600_LL: if circuit compensation debug flag is set, use the temporary version of compliance volume calculation;
	// this version is to be removed
	if(tempCktCompFactorSet_)
	{
		Real32	endExhPressure = BreathRecord::GetEndExpPressureComp() ;
		Real32 deltaPressure = LastValidEipForCc_ - endExhPressure ;
		if(deltaPressure <= 0.0)
			inspCompVolumeMl_ = 0;
		else
			inspCompVolumeMl_ = tempCktCompFactor_ * deltaPressure;
	}
	else
	{
		updateComplianceVolume_() ;
	}
	//////////////////////////////////////////////////////////////////////
	
    Real32 inspBtpsCf = Btps::GetInspBtpsCf() ;

    // TODO E600_LL: force HME_HUMIDIFIER case for now
    bool status = TRUE;
	if (status) //PhasedInContextHandle::GetDiscreteValue( SettingId::HUMID_TYPE) == HumidTypeValue::HME_HUMIDIFIER)
    {
		// $[TI2.1]
		totalVolumeMl_ = (tidalVolumeMl_ * inspBtpsCf) + inspCompVolumeMl_ ;
	}
	/*else
    {
		// $[TI2.2]
		totalVolumeMl_ = (tidalVolumeMl_ + inspCompVolumeMl_) * inspBtpsCf ;
	}*/

	intervalNumber_ = 0 ;
    elapsedTimeMs_ = 0 ;
    controlTimerMs_ = VOLUME_CYCLE_TIME_MS ; // to induce activation of VolumeController
	MixChangeOccurred_ = FALSE ;
	lastErrorSumUpdated_ = FALSE ;
	
	// calculate the number of VCV (VOLUME_CYCLE_TIME_MS ms) cycles from the inspiration
	// delivery time to the nearest VOLUME_CYCLE_TIME_MS ms.  calculate the compliance
	// compensated peak flow using the number of VCV cycles

   	Real32	baseFlow = RAirFlowSensor.getValue() + RO2FlowSensor.getValue() ;

	Real32 	compensatedQminVolumeMl ;
	Real32	denom ;
	Real32 dtPlusDeadBandMs = VOLUME_CYCLE_TIME_MS + DEAD_BAND_MS ;

	Real32 actualCloseTimeMs = CLOSE_TIME_MS ;

	if (IsEquivalent( plateauTimeMs_, 0.00, HUNDREDTHS))
	{
	   	// $[TI7.1]
		actualCloseTimeMs = 0.0 ;
	}  	// implied else $[TI7.2]
	
	switch( FlowPattern_)
    {
		case FlowPatternValue::SQUARE_FLOW_PATTERN:
			// $[TI3.1]
       		numberVcvCycles_ = (Int32)(InspDeliveryTimeMs_ / VOLUME_CYCLE_TIME_MS) ;
	       	cCPeakFlow_ = (2.0F * MIN_TO_SEC_CF * totalVolumeMl_
	       							- baseFlow * dtPlusDeadBandMs
	       						  )
	       						  / ( (2.0F * numberVcvCycles_ - 1.0F)
	       						  		 * VOLUME_CYCLE_TIME_MS
                     					 + 2.0F * actualCloseTimeMs - DEAD_BAND_MS
                     		   	    ) ;
			break ;

       	case FlowPatternValue::RAMP_FLOW_PATTERN:
			// $[TI3.2]
       		numberVcvCycles_ = (Int32)(InspDeliveryTimeMs_ / VOLUME_CYCLE_TIME_MS) ;

       		compensatedQminVolumeMl = qMin_
       				 * ( (Real32)numberVcvCycles_ * (Real32)numberVcvCycles_
       				 		* (Real32)VOLUME_CYCLE_TIME_MS - DEAD_BAND_MS
       				 		+ 2.0F * (Real32)numberVcvCycles_ * actualCloseTimeMs
       				   ) ;
       										    
       		denom = ((Real32)numberVcvCycles_ - 1.0F)
       				 	* ((Real32)numberVcvCycles_
       				 	* (Real32)VOLUME_CYCLE_TIME_MS - DEAD_BAND_MS) ;
       					 
       		cCPeakFlow_ = ( (Real32)numberVcvCycles_
       							* ( 2.0F * MIN_TO_SEC_CF * totalVolumeMl_
       								 	- baseFlow * dtPlusDeadBandMs
       						      )	- compensatedQminVolumeMl
       					  ) / denom ;
       		break ;

       	default:
           	CLASS_PRE_CONDITION( FlowPattern_ == FlowPatternValue::SQUARE_FLOW_PATTERN
           						 || FlowPattern_ == FlowPatternValue::RAMP_FLOW_PATTERN) ;
    }

#ifdef SIGMA_UNIT_TEST_DCS_5473
	cCPeakFlow_ = -100.0 ;
#endif // SIGMA_UNIT_TEST_DCS_5473	

    if (cCPeakFlow_ > flowCommandLimit_)
    {
		// $[TI4.1]
    	cCPeakFlow_ = flowCommandLimit_ ;
    }
    else if (cCPeakFlow_ < 0.0)
    {
		// $[TI4.3]
    	cCPeakFlow_ = 0.0 ;
    }
    // implied else $[TI4.2]

	//	The following manages the ControllerType of the air/O2VolumeController
	//	with the following algorithm:
	//
	//	The controllerType resets to FF_PLUS_I if isControllerReset is TRUE.  The next
	//	breath will bump the controllerType to INIT_SUMMERS_FF_PLUS_I.  Each subsequent
	//	breath thereafter will either set the controllerType to FF_PLUS_SUMMERS if
	//  BreathLikenessStatus is TRUE or back to INIT_SUMMERS_FF_PLUS_I if
	//	BreathLikenssStatus is FALSE.  updateErrorBand() is called on the second breath
	//	after the controllerType is set to FF_PLUS_I.

	// 	Note: PrevO2CcPeakFlow_ and PrevAirCcPeakFlow_ are used when ControllerType =
	//	INIT_SUMMERS_FF_PLUS_I or FF_PLUS_SUMMERS.  PrevO2CcPeakFlow_ and PrevAirCcPeakFlow_
	//	are initialized when ControllerType = FF_PLUS_I. 

	if (isControllerReset)	
	{
		// $[TI5.1]
		VolumeController::SetControllerType( VolumeController::FF_PLUS_I) ;
	}
	else
	{
		// $[TI5.2]
		if (VolumeController::GetControllerType() == VolumeController::FF_PLUS_I)
		{
			// $[TI6.1]
			VolumeController::SetControllerType( VolumeController::INIT_SUMMERS_FF_PLUS_I) ;
		}
		else if (VolumeController::GetBreathLikenessStatus() == TRUE)
		{
			// $[TI6.2]
			VolumeController::SetControllerType( VolumeController::FF_PLUS_SUMMERS) ;

			RO2VolumeController.updateErrorBand( PrevO2CcPeakFlow_) ;
			RAirVolumeController.updateErrorBand( PrevAirCcPeakFlow_) ;
		}
		else
		{
			// $[TI6.3]
			VolumeController::SetControllerType( VolumeController::INIT_SUMMERS_FF_PLUS_I) ;
			RO2VolumeController.updateErrorBand( PrevO2CcPeakFlow_) ;
			RAirVolumeController.updateErrorBand( PrevAirCcPeakFlow_) ;
		}
	}

	Real32 o2Fraction = RO2Mixture.calculateO2Fraction() ;
   	Real32 airFraction = 1.0F - o2Fraction ;

	// compute new PrevAirCcPeakFlow_ and PrevO2CcPeakFlow_		   			   	
    PrevAirCcPeakFlow_ = cCPeakFlow_ * airFraction ;
    PrevO2CcPeakFlow_ = cCPeakFlow_ * o2Fraction ;

	// the following must be called before VolumeController::newBreath() is called
	RAirVolumeController.setPeakDesiredFlow( PrevAirCcPeakFlow_) ;
	RO2VolumeController.setPeakDesiredFlow( PrevO2CcPeakFlow_) ;
	
    RO2VolumeController.newBreath() ;
    RAirVolumeController.newBreath() ;

    // flow controller used during plateau
    RO2FlowController.newBreath() ;
    RAirFlowController.newBreath() ;

    RExhValveIController.newBreath() ;

	((Trigger&)RHighVentPressureExpTrigger).enable() ;
	((Trigger&)RHighCircuitPressureExpTrigger).enable() ;

    RPeep.setPeepChange( FALSE) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
VcvPhase::SoftFault( const SoftFaultID  softFaultID,
                   	 const Uint32       lineNumber,
		   			 const char*        pFileName,
		   			 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, VCVPHASE,
                             lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkResetConditions_()
//
//@ Interface-Description
//		This method has no arguments and returns the Boolean of whether to
//		reset the controllers.  This method is to be called before the
//		inspiratory time, plateau time,	inspiratory delivery time, and flow
//		pattern are updated with current settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The InspDeliveryTimeMs_ (previous breath's value) is compared against
//		the present setting.  The FlowPattern_ (previous breath's waveshape) is
//		also compared against the present setting.  If either are different or
//		the HcpOrHvpOccured_ is TRUE or there was a mix change, the
//		isControllerReset flag is set TRUE else FALSE.  On power up, the initial
//		values for the InspDeliveryTimeMs_ is defined by the static initialization
//		to -1 to ensure that the values are NOT the same as the settings.
//		This will always cause isControllerReset to be TRUE, which is desired
//		upon power up. 
//		$[04159] $[04160] $[04161] $[04079]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Boolean
VcvPhase::checkResetConditions_( void)
{
	CALL_TRACE("VcvPhase::checkResetConditions_( void)") ;

	Boolean rtnValue = FALSE ;
	
   	Real32 newPlateauTimeMs = PhasedInContextHandle::GetBoundedValue( plateauTimeId_).value ;

   	// round newInspTimeMs down to nearest VOLUME_CYCLE_TIME_MS as done in newBreath()
   	Real32 newInspTimeMs = (Real32)(PhasedInContextHandle::GetBoundedValue( inspTimeId_).value
									   	 / VOLUME_CYCLE_TIME_MS)
   								* VOLUME_CYCLE_TIME_MS ;
   	
   	Real32 newInspDeliveryTimeMs = newInspTimeMs - newPlateauTimeMs ;

   	if (!IsEquivalent( InspDeliveryTimeMs_, newInspDeliveryTimeMs, TENTHS) ||
        	(PhasedInContextHandle::GetDiscreteValue( flowPatternId_) != FlowPattern_) ||
			!IsEquivalent( RO2Mixture.getTargetO2Percent(), BeginningMix_, HUNDREDTHS) ||
        	MixChangeOccurred_ || HcpOrHvpOccured_)
    {
		// $[TI1.1]
    	rtnValue = TRUE ;
    }	// implied else $[TI1.2]

  	return( rtnValue) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateComplianceVolume_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to update the compliance volume based on the end insp pressure.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The inspCompVolumeMl_ is the difference in compliance volume at
//		the pressure at the end of inspiration (LastValidEipForCc_) minus
//		the compliance volume at the pressure at the end of exhalation.
//		Compliance volume must be >= 0.0 and <= a maximum compliance volume
//		based on IBW. 
// 		$[04037] $[04038] $[04041] $[04043] $[04330]
//---------------------------------------------------------------------
//@ PreCondition
//		patientCircuitType == PatientCctTypeValue::ADULT_CIRCUIT ||
//			PatientCctTypeValue::PEDIATRIC_CIRCUIT
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
VcvPhase::updateComplianceVolume_( void)
{
	CALL_TRACE("VcvPhase::updateComplianceVolume_( void)") ;
		
	Real32	endExhPressure = BreathRecord::GetEndExpPressureComp() ;

	if (LastValidEipForCc_ <= 0.0)
	{
		// $[TI5.1]
		inspCompVolumeMl_ = 0.0 ;
		AdiabaticFactor_ = 1.0 ;
	}
	else
	{
		// $[TI5.2]
		Real32 deltaPressure = LastValidEipForCc_ - endExhPressure ;

		Real32 tempComplVol = RCircuitCompliance.getComplianceVolume( deltaPressure,(Uint32)inspTimeMs_) ;
		
		Real32 tempFactor = 1.0 ;
	
		if (FlowPattern_ == FlowPatternValue::SQUARE_FLOW_PATTERN && 
			IsEquivalent( plateauTimeMs_, 0.00, HUNDREDTHS))
		{
		   	// $[TI6.1]
			tempFactor = RCircuitCompliance.getAdiabaticFactor((Uint32) inspTimeMs_) ;
		}  	// implied else $[TI6.2]

		AdiabaticFactor_ = (tidalVolumeMl_ + tempComplVol * tempFactor)
							/ (tidalVolumeMl_ + tempComplVol) ;
							
		deltaPressure = (LastValidEipForCc_ - endExhPressure) * AdiabaticFactor_ ;

		if (PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE) ==
				MandTypeValue::VCP_MAND_TYPE &&
			BreathPhaseScheduler::GetCurrentScheduler().getId() != SchedulerId::APNEA)
		{
			// $[TI7]
			// $[VC24014]
			deltaPressure = 15.0 ;
		}
		// $[TI8]
		
		inspCompVolumeMl_ =	RCircuitCompliance.getComplianceVolume( deltaPressure, (Uint32)inspTimeMs_) ;
	}

	const Real32  COMPL_LIMIT_FACTOR = getComplLimitFactorValue();
	
	const Real32  MAX_COMPL_VOLUME = tidalVolumeMl_ * COMPL_LIMIT_FACTOR;
	
	if (inspCompVolumeMl_ < 0.0F)
	{
		// $[TI4.1]
		inspCompVolumeMl_ = 0.0F ;
	}
	else if (inspCompVolumeMl_ > MAX_COMPL_VOLUME)
	{
		// $[TI4.2]
		inspCompVolumeMl_ = MAX_COMPL_VOLUME;
		RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_VOLUME_LIMIT) ;
	}	// implied else $[TI4.3]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateDesiredFlow_( void)
//
//@ Interface-Description
//		This method has no arguments and returns the desired flow. This method
//		is called to determine the desired flow for the current interval number
//		based on the flow pattern.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The desired flow is computed dynamically based on a square or ramp
//		trajectory.
// 		$[04155] $[04156]
//---------------------------------------------------------------------
//@ PreCondition
//		flowPattern == FlowPatternValue::SQUARE_FLOW_PATTERN
//			|| FlowPatternValue::RAMP_FLOW_PATTERN
//		numberVcvCycles_ > 0
//---------------------------------------------------------------------
//@ PostCondition
//		0.0 <= desiredFlow <= flowCommandLimit_
//
//@ End-Method
//=====================================================================
Real32
VcvPhase::calculateDesiredFlow_(void) 
{
	CALL_TRACE("calculateDesiredFlow_(void)") ;
	
    Real32 desiredFlow = 0 ;

	switch( FlowPattern_)
   	{
    	case FlowPatternValue::SQUARE_FLOW_PATTERN:
			// $[TI1.1]
      		desiredFlow = cCPeakFlow_ ;
			break ;

      	case FlowPatternValue::RAMP_FLOW_PATTERN:
			// $[TI2.1]
      		CLASS_PRE_CONDITION( numberVcvCycles_ > 0) ;

			desiredFlow = cCPeakFlow_ - 
					(cCPeakFlow_ - qMin_) * intervalNumber_ / numberVcvCycles_ ;

			// Exclude extreme settings under which the extended delivery time 
			// causes negative desired flow.			
			if ((intervalNumber_ > numberVcvCycles_) && (desiredFlow < 0.0f))
			{
				desiredFlow = 0.0f;
			}
      		break ;

      default:
      	CLASS_PRE_CONDITION( (FlowPattern_ == FlowPatternValue::SQUARE_FLOW_PATTERN) || 
                             (FlowPattern_ == FlowPatternValue::RAMP_FLOW_PATTERN) )
   }

	CLASS_POST_CONDITION( desiredFlow <= flowCommandLimit_, VcvPhase) ;
	CLASS_POST_CONDITION( desiredFlow >= 0.0F, VcvPhase) ;

   	return( desiredFlow) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getComplLimitFactorValue()
//
//@ Interface-Description
//		Return the compliance limit factor value based on 'ibwValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The compliance limit factor value is not only dependent on the
//		passed-in IBW value, but also on the current circuit type.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32
VcvPhase::getComplLimitFactorValue(void) 
{
	CALL_TRACE("VcvPhase::getComplLimitFactorValue(void)") ;
		
    const DiscreteValue  CIRCUIT_TYPE =
        PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE) ;

	const ComplLimitFactorRange_*  pComplLimitFactorInfo = NULL;

    switch (CIRCUIT_TYPE)
    {
	    case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			// $[TI2]
			pComplLimitFactorInfo = ::PedComplianceLimitFactorTable_ ;
	        break ;
	        
    	case PatientCctTypeValue::ADULT_CIRCUIT :
			// $[TI1]
			pComplLimitFactorInfo = ::AdultComplianceLimitFactorTable_ ;
        	break ;

	    case PatientCctTypeValue::NEONATAL_CIRCUIT :
			// $[TI3]
			pComplLimitFactorInfo = ::NeoComplianceLimitFactorTable_ ;
	        break ;

    	default :
        	AUX_CLASS_ASSERTION_FAILURE( CIRCUIT_TYPE) ;
	        break ;
    }

	const Real32 IBW_VALUE =
				PhasedInContextHandle::GetBoundedValue(SettingId::IBW).value ;

	Real32 complLimitFactor = 2.5f ;  // set to reasonable default value...

	for ( ; pComplLimitFactorInfo->highIbwValue != 0.0f; pComplLimitFactorInfo++)
	{
		if (pComplLimitFactorInfo->highIbwValue >= IBW_VALUE)
		{
			// $[TI4]
			const ComplLimitFactorInfo_*  pFactorRange =
									pComplLimitFactorInfo->limitFactorTable_ ;

			for (; pFactorRange->ibwValue < IBW_VALUE; pFactorRange++)
			{
				// do nothing...
			}

			complLimitFactor = pFactorRange->factorValue ;
			
			// break out of outer loop...
			break ;
		}	// $[TI5]
	}

   	return( complLimitFactor) ;
}

#endif // SIGMA_BD_CPU
