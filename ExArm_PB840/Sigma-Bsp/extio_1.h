///************************************************************************/
///* THIS INFORMATION IS PROPRIETARY TO                                   */
///* MICROTEC - A Mentor Graphics Company                                 */
///*----------------------------------------------------------------------*/
///* Copyright (c) 1993-1997 Microtec - A Mentor Graphics Company         */
///* All rights reserved                                                  */
///************************************************************************/
///*
//**The software source code contained in this file and in the related
//**header files is the property of Microtec. and may only be
//**used under the terms and conditions defined in the Microtec
//**license agreement. You may modify this code as needed but you must 
//**retain the Microtec copyright notices, including this statement.
//**Some restrictions may apply when using this software with non-Microtec
//**software.  In all cases, Microtec reserves all rights.
//*/
///************************************************************************/
///*
// *
// *	Module Name:		%M%
// *
// *	Identification:		%Z% %I% %M%
// *
// *	Date:			%G%  %U%
// *
// * 
// */
//
//
//#ifndef _EXTERNIO_H_
//#define _EXTERNIO_H_
//#include "vbsp.h"
////#include "compiler.h"
////#include "logio.h"
//
//
//#ifdef _MCC386
//
//#define	_inb	inp
//#define	_inw	inpw
//#define	_ind	inpd
//#define	_outb	outp
//#define	_outw	outpw
//#define	_outd	outpd
//#endif
//
//#ifdef SPARC
//
//typedef struct {
//   void (*reg_write_b)(_ANSIPROT3(BYTE *, LONG, BYTE )); 
//   /* and initialized PER DEVICE*/
//   
//   void (*reg_write_w)(_ANSIPROT3(WORD *, LONG, WORD));
//   /* these function pointers */
//   
//   void (*reg_write_l)(_ANSIPROT3(LONG *, LONG, LONG));
//   /* are used to call functions*/
//   
//   BYTE (*reg_read_b)( _ANSIPROT2(BYTE *, LONG)); /* that may contain board */
//   WORD (*reg_read_w)( _ANSIPROT2(WORD *, LONG));  /* specific code */
//   LONG (*reg_read_l)( _ANSIPROT2(LONG *, LONG));
//   vbsp_return_t (*interrupt_start)(_ANSIPROT2(void *,	unsigned char));
//   vbsp_return_t (*interrupt_end)(_ANSIPROT2(void *,unsigned char));
//   vbsp_return_t (*brd_spec_int_disable)(_ANSIPROT2
//					 (void *,logio_int_entry_t *));
//   vbsp_return_t (*brd_spec_int_enable)(_ANSIPROT2
//					(void *,logio_int_entry_t *));
//   /* the following pointer should be used to add extra device
//    * specific information needed that all devices may not need
//    */
//   void *specific;
//} extern_fops_t;
//
///* Put the following lines in your device driver's .h file, replacing "DEVICE"
//   with the driver's name.
//   
//   #ifdef SPARC
//   #define REG_WRITE_B(devid,address,data) DEVICE_write_b(address,data)
//   #define REG_WRITE_W(devid,address,data) DEVICE_write_w(address,data)
//   #define REG_WRITE_L(devid,address,data) DEVICE_write_l(address,data)
//   #define REG_READ_B(devid,address)       DEVICE_read_b(address)
//   #define REG_READ_W(devid,address)       DEVICE_read_w(address)
//   #define REG_READ_L(devid,address)       DEVICE_read_l(address)
//   #endif
//   
//   */
//
//#else /* not SPARC */
//
//typedef struct {
//    void (*reg_write_b)(_ANSIPROT2(BYTE *, BYTE )); 
//    /* and initialized PER DEVICE*/
//    
//    void (*reg_write_w)(_ANSIPROT2(WORD *, WORD)); 
//    /* these function pointers */
//    
//    void (*reg_write_l)(_ANSIPROT2(LONG *, LONG));
//    /* are used to call functions*/
//   
//    BYTE (*reg_read_b)( _ANSIPROT1(BYTE *)); /* that may contain board */
//    WORD (*reg_read_w)( _ANSIPROT1(WORD *));  /* specific code */
//    LONG (*reg_read_l)( _ANSIPROT1(LONG *));
//    vbsp_return_t (*interrupt_start)(_ANSIPROT2(void *,	unsigned char));
//    vbsp_return_t (*interrupt_end)(_ANSIPROT2(void *,unsigned char));
//    vbsp_return_t (*brd_spec_int_disable)(_ANSIPROT2
//					  (void *,logio_int_entry_t *));
//    vbsp_return_t (*brd_spec_int_enable)(_ANSIPROT2
//					 (void *,logio_int_entry_t *));
//    /* the following pointer should be used to add extra device
//     * specific information needed that all devices may not need
//     */
//    void *specific;
//} extern_fops_t;
//
//#ifdef USE_FUNCTIONS
//
//#if defined(INTELx86) 
//
//#define REG_WRITE_B(devid,address,data)\
//if (devid->externio.reg_write_b)\
//(*devid->externio.reg_write_b)(address,data);\
//else\
//_outb((unsigned int)address,data);
//#define REG_WRITE_W(devid,address,data)\
//if (devid->externio.reg_write_w)\
//(*devid->externio.reg_write_w)(address,data);\
//else\
//_outw((unsigned int)address,data);
//#define REG_WRITE_L(devid,address,data)\
//if (devid->externio.reg_write_l)\
//(*devid->externio.reg_write_l)(address,data);\
//else\
//_outd((unsigned int)address,data);
//#define REG_READ_B(devid,address)    ((devid->externio.reg_read_b)?(*devid->externio.reg_read_b)((BYTE *)address):_inb((unsigned int)address))
//#define REG_READ_W(devid,address)    ((devid->externio.reg_read_w)?(*devid->externio.reg_read_w)((WORD *)address): _inw((unsigned int)address))
//#define REG_READ_L(devid,address)    ((devid->externio.reg_read_l)?(*devid->externio.reg_read_l)((LONG *)address): _ind((unsigned int)address))
//
//#else /* INTELx86 */
//
//#define REG_WRITE_B(devid,address,data)\
//if (devid->externio.reg_write_b)\
//(*devid->externio.reg_write_b)(address,data);\
//else\
//*(BYTE*)address=(BYTE)data;
//#define REG_WRITE_W(devid,address,data)\
//if (devid->externio.reg_write_w)\
//(*devid->externio.reg_write_w)(address,data);\
//else\
//*(WORD*)address=(WORD)data;
//#define REG_WRITE_L(devid,address,data)\
//if (devid->externio.reg_write_l)\
//(*devid->externio.reg_write_l)(address,data);\
//else\
//*(LONG*)address=(LONG)data;
//#define REG_READ_B(devid,address)    ((devid->externio.reg_read_b)?(*devid->externio.reg_read_b)((BYTE *)address): (BYTE)*address)
//#define REG_READ_W(devid,address)    ((devid->externio.reg_read_w)?(*devid->externio.reg_read_w)((WORD *)address): (WORD)*address)
//#define REG_READ_L(devid,address)    ((devid->externio.reg_read_l)?(*devid->externio.reg_read_l)((LONG *)address): (LONG)*address)
//
//#endif /* INTELx86 */
//
//#else /* USE FUNCTIONS */
//
//#if defined(INTELx86)
//
//#define REG_WRITE_B(devid,address,data)  _outb((unsigned int)address,data);
//#define REG_WRITE_W(devid,address,data)  _outw((unsigned int)address,data);
//#define REG_WRITE_L(devid,address,data)  _outd((unsigned int)address,data);
//#define REG_READ_B(devid,address)        _inb((unsigned int)address)
//#define REG_READ_W(devid,address)        _inw((unsigned int)address)
//#define REG_READ_L(devid,address)        _ind((unsigned int)address)
//
//#else /*INTEL */
//
//#define REG_WRITE_B(devid,address,data) *(BYTE*)address=(BYTE)data
//#define REG_WRITE_W(devid,address,data) *(WORD*)address=(WORD)data
//#define REG_WRITE_L(devid,address,data) *(LONG*)address=(LONG)data
//#define REG_READ_B(devid,address)       ((BYTE)*address)
//#define REG_READ_W(devid,address)       ((WORD)*address)
//#define REG_READ_L(devid,address)       ((LONG)*address)
//
//#endif /*INTEL */
//#endif /* USE FUNCTIONS */
//#endif /* SPARC */
//
//#define INTERRUPT_START(dev, type) \
//if ((dev)->externio.interrupt_start)\
//(*(dev)->externio.interrupt_start)(((void*)dev), (type))
//
//#define INTERRUPT_END(dev, type) \
//if ((dev)->externio.interrupt_end)\
//(*(dev)->externio.interrupt_end)(((void*)dev), (type))
//
//#define BOARD_SPEC_INT_DISABLE(dev, intrpt) \
//if ((dev)->externio.brd_spec_int_disable) \
//(*(dev)->externio.brd_spec_int_disable)(((void*)dev), (intrpt))
//
//#define BOARD_SPEC_INT_ENABLE(dev, intrpt) \
//if ((dev)->externio.brd_spec_int_enable) \
//(*(dev)->externio.brd_spec_int_enable)(((void*)dev), (intrpt))
//
//#define INT_PKT_RX      0x01
//#define INT_PKT_TX      0x02
//#define INT_PKT_ERR     0x03
//#define INT_PKT_INIT    0x04
//
//#define INT_TTY_RX      0x05
//#define INT_TTY_TX      0x06
//#define INT_TTY_ERR     0x07
//#define INT_TTY_INIT    0x08
//
//#endif
//
//
