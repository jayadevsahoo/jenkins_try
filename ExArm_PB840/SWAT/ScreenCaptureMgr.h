////////////////Copyright (c) 2014 Covidien/////////////////////////////////////
/*******************************************************************************
bitbucket.org/covidien/e600.git
Name:		ScreenCaptureMgr.h

Purpose:
	Header file for SWAT proxy of ScreenCaptureMgr class.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 10-MAR-2014
PROJECT: 880 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version. Screen capture manager is used to manage the screen
    capture, it is a part of SWAT subsystem. Will be called from SWAT handler
    or SWAT agent.

END OF REVISIONS

*******************************************************************************/
#pragma once

#include <vector>
#include <string>
#include <algorithm>

using namespace std;

#include <Windows.h>
#include <tchar.h>

#include "SWATTypes.h"

typedef std::basic_string<TCHAR> tstring;

class ScreenCaptureMgr
{
public:

    ScreenCaptureMgr(void);
    ~ScreenCaptureMgr(void);

    // initialize the manager according to image buffer directory
    int     initialize( void );

    // clear the image buffer folder
    bool    clear( void );

    /*
       SWAT client will use FTP to download capture files from vent
       this function is used to return a FTP address to SWAT client
       */
    int     queryScreenshotFileName(int index, TCHAR *rScreenAddr);

    // return the default capture image directory
    const TCHAR*  getScreenshotsDirectory();

    // return the size of the capture file buffer directory
    SWATStatus    getBufferSize( int& bufferSize );

    SWATStatus    saveScreenToFile(int x = 0,
					                      int y = 0,
					                      int w = 0,
					                      int h = 0,
					                      const TCHAR *pBmpFileName = NULL ); 

protected:

    // add a screen capture file to this manager
    int     appendToBuffer_( const tstring& sFileName );


    // is initialization ok? 
    inline bool    isInitialized_();


    void validateParams_(const int nFullScreenWidth, 
                                const int nFullScreenHeight,
				                int &x, int &y, int &w, int &h, 
				                const TCHAR *pBmpFileNameIn,
				                TCHAR *szBmpFileNameOut);

    inline bool isScreenshotExists_( const tstring& fileName );

    // screen capture storage folder
    const TCHAR*      screenShotsDir_;

    // if initialization error?
    int              initializedError_;

    // vector array used to store filename
	vector<tstring>   bitmapList_;

};