#ifndef LinearRegression_HH
#define LinearRegression_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LinearRegression - class to support linear regression.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/LinearRegression.hhv   10.7   08/17/07 09:38:10   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  05-Mar-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "SummationBuffer.hh"

//@ End-Usage

class LinearRegression {
  public:
    LinearRegression( Real32 *pX, Real32 *pXy, Real32 *pY, Real32 *pX2, Uint32 bufferSize ) ;
    ~LinearRegression( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							 const Uint32      lineNumber,
							 const char*       pFileName  = NULL, 
							 const char*       pPredicate = NULL) ;

	void updateValues( const Real32 xValue, const Real32 yVale) ;
	Real32 getEstimateYValue( const Real32 xValue) ;
	Real32 getSlope( void) ;
	void reset( void) ;
		
  protected:

  private:
    LinearRegression( void) ;						// not implemented...
    LinearRegression( const LinearRegression&) ;  	// not implemented...
    void   operator=( const LinearRegression&) ; 	// not implemented...

	//@ Data-Member: x_
	// summation of all the x values
	SummationBuffer x_ ;

	//@ Data-Member: xy_
	// summation of all the x*y values
	SummationBuffer xy_ ;

	//@ Data-Member: y_
	// summation of all the y values
	SummationBuffer y_ ;

	//@ Data-Member: x2_
	// summation of all the x^2 values
	SummationBuffer x2_ ;
} ;


#endif // LinearRegression_HH 
