#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: BdFsm - BD Finite State Machine.
//---------------------------------------------------------------------
//@ Interface-Description
//  The BdFsm contains the static instantiations of the valid 
//  FsmStates for the BD Task-Control.  This class instantiates a 
//  single instance for each valid FsmState and provides static 
//  accessor functions to these states. 
//---------------------------------------------------------------------
//@ Rationale
//  The BdFsm contains the FsmState instantiations for BD task control.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each FsmState instantiation is defined in a static function which
//  defines a concrete FsmState as its static data.  The first time the
//  static function is called, the FsmState is constructed and its
//  reference returned.  On subsequent calls to the static function,
//  only the reference is returned since the object is already
//  constructed.  The Initialize entry point provides controlled 
//  static initialization by calling each static member function
//  containing function static.  There is only one instantiation for
//  each FsmState which are accessed using the BdFsm accessor methods. 
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/BdFsm.ccv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "BdFsm.hh"
#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif

//@ Usage-Classes
#include "InitConditions.hh"
#include "BdInop.hh"
#include "BdOnline.hh"
#include "BdService.hh"
#include "BdStart.hh"
#include "BdSst.hh"
#include "TaskControlAgent.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  BdFsm class initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Initialize method calls each BdFsm method containing function  
//  static to initialize their static object.  Subsequent calls to these
//  methods return a reference to the constructed static object.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdFsm::Initialize(void)
{
    CALL_TRACE("Initialize(void)");

    // controlled initialization of statics
    BdFsm::GetBdInopState();
    BdFsm::GetBdOnlineState();
    BdFsm::GetBdServiceState();
    BdFsm::GetBdStartState();
    BdFsm::GetBdSstState();
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBdInopState
//
//@ Interface-Description
//  The GetBdInopState returns a reference to the BdInop state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static BdInop state is constructed and a reference to it
//  returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BdInop&
BdFsm::GetBdInopState(void)
{
    CALL_TRACE("GetBdInopState(void)");
    static const BdInop state;
    return state;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBdOnlineState
//
//@ Interface-Description
//  The GetBdOnlineState returns a reference to the BdOnline state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static BdOnline state is constructed and a reference to it
//  returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


const BdOnline&
BdFsm::GetBdOnlineState(void)
{
    CALL_TRACE("BdOnlineState(void)");
    static const BdOnline state;
    return state; 
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBdServiceState
//
//@ Interface-Description
//  The GetBdServiceState returns a reference to the BdService state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static BdService state is constructed and a reference to it
//  returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BdService&
BdFsm::GetBdServiceState(void)
{
    CALL_TRACE("BdServiceState(void)");
    static const BdService state;
    return state;
    // $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBdStartState
//
//@ Interface-Description
//  The GetBdStartState returns a reference to the BdStart state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static BdStart state is constructed and a reference to it
//  returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BdStart&
BdFsm::GetBdStartState(void)
{
    CALL_TRACE("BdStartState(void)");
    static const BdStart state;
    return state;
    // $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBdSstState
//
//@ Interface-Description
//  The GetBdSstState returns a reference to the BdSst state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static BdSst state is constructed and a reference to it
//  returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BdSst&
BdFsm::GetBdSstState(void)
{
    CALL_TRACE("BdSstState(void)");
    static const BdSst state;
    return state; 
    // $[TI1]
}


 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BdFsm::SoftFault(const SoftFaultID  softFaultID,
                 const Uint32       lineNumber,
                 const char*        pFileName,
                 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT, 
                          Sys_Init::ID_BDFSM, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================





