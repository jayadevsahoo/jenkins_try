#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  PeepLowSetting - Low Positive End-Expiratory Pressure  
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the positive pressure
//  applied to the patient's circuit during a BiLevel exhalation.  This class
//  inherits from 'BatchBoundedSetting' and provides the specific information
//  needed for representation of PEEP-low.  This information includes the
//  interval and range of this setting's values, and this batch setting's
//  new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, or dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PeepLowSetting.ccv   25.0.4.0   19 Nov 2013 14:27:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 003   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 002   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//=====================================================================

#include "PeepLowSetting.hh"
#include "ModeValue.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[TC02017] -- The setting's range ...
//  $[TC02020] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	0.0f,		// absolute minimum...
	0.0f,		// unused...
	TENTHS,		// unused...
	NULL
};
static const BoundedInterval  NODE1_ =
{
	20.0f,		// change-point...
	0.5f,		// resolution...
	TENTHS,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	45.0f,		// absolute maximum...
	1.0f,		// resolution...
	ONES,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PeepLowSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BatchBoundedSetting.  The method passes to
//  BatchBoundedSetting it's default information. Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeepLowSetting::PeepLowSetting(void)
: BatchBoundedSetting(SettingId::PEEP_LOW,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  PEEP_LOW_MAX_ID,	// maxConstraintId...
					  PEEP_LOW_MIN_ID)	// minConstraintId...
{
	CALL_TRACE("PeepLowSetting()");
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PeepLowSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeepLowSetting::~PeepLowSetting(void)
{
	CALL_TRACE("~PeepLowSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability(qualifierId)  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	PeepLowSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id  applicability;

	if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL) )
	{  // $[TI1] -- BILEVEL is an active option...
		const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

		DiscreteValue  modeValue;

		switch ( qualifierId )
		{
			case Notification::ACCEPTED :		// $[TI1.1]
				modeValue = pMode->getAcceptedValue();
				break;
			case Notification::ADJUSTED :		// $[TI1.2]
				modeValue = pMode->getAdjustedValue();
				break;
			default :
				AUX_CLASS_ASSERTION_FAILURE(qualifierId);
				break;
		}

		applicability = (modeValue == ModeValue::BILEVEL_MODE_VALUE)
						? Applicability::CHANGEABLE	   // $[TI1.3]
						: Applicability::INAPPLICABLE; // $[TI1.4]
	}
	else
	{  // $[TI2] -- BILEVEL is not an active option...
		applicability = Applicability::INAPPLICABLE;
	}

	return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant for the new patient value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	PeepLowSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	BoundedValue  newPatient;

	// $[TC02018] The setting's new-patient value ...
	newPatient.value     = 3.0f;
	newPatient.precision = TENTHS;

	return(newPatient);	 // $[TI1]
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  This method is only called during transition of mode
//	to BiLevel
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02025]\e\   -- transitioning from A/C to BiLevel
//  $[TC02000]\b\ -- transitioning from SIMV to BiLevel
//  $[02026]\d\   -- transitioning from SPONT to BiLevel
//---------------------------------------------------------------------
//@ PreCondition
//  ((settingId == SettingId::MODE)           &&
//   ((currValue == ModeValue::SIMV_MODE_VALUE)  ||
//    (currValue == ModeValue::AC_MODE_VALUE)  ||
//    (currValue == ModeValue::CPAP_MODE_VALUE)  ||
//    (currValue == ModeValue::SPONT_MODE_VALUE))  &&
//   (newValue == ModeValue::BILEVEL_MODE_VALUE))
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
	PeepLowSetting::acceptTransition(const SettingId::SettingIdType settingId,
									 const DiscreteValue            newValue,
									 const DiscreteValue        currValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	AUX_CLASS_PRE_CONDITION((settingId == SettingId::MODE), settingId);
	AUX_CLASS_PRE_CONDITION(((currValue == ModeValue::SIMV_MODE_VALUE)  ||
							 (currValue == ModeValue::SPONT_MODE_VALUE) ||
							 (currValue == ModeValue::CPAP_MODE_VALUE) ||
							 (currValue == ModeValue::AC_MODE_VALUE) ),
							currValue);
	AUX_CLASS_PRE_CONDITION((newValue == ModeValue::BILEVEL_MODE_VALUE),
							newValue);

	const Setting*  pPeep = SettingsMgr::GetSettingPtr(SettingId::PEEP);

	BoundedValue  newPeepLow;

	newPeepLow = pPeep->getAdjustedValue();

	// store the value...
	setAdjustedValue(newPeepLow);

	// activation of Transition Rule MUST be italicized...
	setForcedChangeFlag();
}	// $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	PeepLowSetting::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							PEEP_LOW_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//
//  $[TC02017] -- PEEP low's range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	PeepLowSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	BoundedRange::ConstraintInfo  maxConstraintInfo;

	BoundedValue  maxLimit;

	// reset this range's maximum constraint to allow for "warping" of a
	// new maximum value...
	getBoundedRange_().resetMaxConstraint();

	//===================================================================
	// set up PEEP low's absolute maximum bound that is based on mode
	//===================================================================

	// start off with the absolute maximum bound value and ID...
	maxConstraintInfo.value = getAbsoluteMaxValue_();
	maxConstraintInfo.id    = PEEP_LOW_MAX_ID;

	//===================================================================
	// calculate PEEP low's maximum bound that is based on high circuit
	// pressure...
	//===================================================================

	const Setting*  pHighCctPress =
		SettingsMgr::GetSettingPtr(SettingId::HIGH_CCT_PRESS);

	const Real32  HIGH_CCT_PRESS_VALUE =
		BoundedValue(pHighCctPress->getAdjustedValue()).value;

	maxLimit.value = (HIGH_CCT_PRESS_VALUE -
					  SettingConstants::MIN_NON_MAND_PRESS_BUFF);

	if ( maxLimit.value < maxConstraintInfo.value )
	{	// $[TI1]
		// warp the calculated maximum value to a lower "click" boundary...
		getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

		// the high circuit pressure-based maximum is more restrictive than
		// 'maxConstraintInfo.value', therefore save its value and id as the maximum
		// bound value and id...
		maxConstraintInfo.id    = PEEP_LOW_MAX_BASED_PCIRC_ID;
		maxConstraintInfo.value = maxLimit.value;
	}  // $[TI2] -- high cct. pressure's absolute max is more restrictive...


	const Setting*  pPressSupp =
		SettingsMgr::GetSettingPtr(SettingId::PRESS_SUPP_LEVEL);

	if ( pPressSupp->getApplicability(Notification::ADJUSTED) !=
		 Applicability::INAPPLICABLE )
	{	// $[TI3] -- pressure support is applicable...
		const Real32  PRESS_SUPP_VALUE =
			BoundedValue(pPressSupp->getAdjustedValue()).value;

		//===================================================================
		// calculate PEEP low's maximum bound that is based on high circuit pressure
		// and pressure support...
		//===================================================================

		maxLimit.value = (HIGH_CCT_PRESS_VALUE - PRESS_SUPP_VALUE -
						  SettingConstants::MIN_MAND_PRESS_BUFF);

		if ( maxLimit.value < maxConstraintInfo.value )
		{	// $[TI3.1]
			// warp the calculated maximum values to a lower "click" boundary...
			getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

			// the high circuit pressure-based maximum is more restrictive than
			// 'maxConstraintInfo.value', therefore save its value and id as the
			// maximum bound value and id...
			maxConstraintInfo.id    = PEEP_LOW_MAX_BASED_PCIRC_PSUPP_ID;
			maxConstraintInfo.value = maxLimit.value;
		}	// $[TI3.2] -- 'maxConstraintInfo.value' is more restrictive...


		//===================================================================
		// calculate PEEP low's maximum bound that is based on pressure support...
		//===================================================================

		maxLimit.value = (SettingConstants::MAX_ALLOWED_PRESSURE_SETTING_VALUE -
						  PRESS_SUPP_VALUE);

		if ( maxLimit.value < maxConstraintInfo.value )
		{	// $[TI3.3]
			// warp the calculated maximum values to a lower "click" boundary...
			getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

			// the pressure support level-based maximum is more restrictive than
			// 'maxConstraintInfo.value', therefore save its value and id as the
			// maximum bound value and id...
			maxConstraintInfo.id    = PEEP_LOW_MAX_BASED_PSUPP_ID;
			maxConstraintInfo.value = maxLimit.value;
		}	// $[TI3.4] -- 'maxConstraintInfo.value' is more restrictive...
	}	// $[TI4] -- pressure support level is not applicable...


	//===================================================================
	// calculate PEEP low's maximum bound that is based on PEEP_HIGH
	//===================================================================

	const Setting*  pPeepHigh = SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH);

	const Real32  PEEP_HIGH_VALUE =
		BoundedValue(pPeepHigh->getAdjustedValue()).value;

	maxLimit.value = (PEEP_HIGH_VALUE -
					  SettingConstants::MIN_MAND_PRESS_DELTA);

	if ( maxLimit.value < maxConstraintInfo.value )
	{	// $[TI5]
		// warp the calculated maximum values to a lower "click" boundary...
		getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

		// the PEEP_HIGH -based maximum is more restrictive than
		// 'maxConstraintInfo.value', therefore save its value and id as the
		// maximum bound value and id...
		maxConstraintInfo.id    = PEEP_LOW_MAX_BASED_PEEPH_ID;
		maxConstraintInfo.value = maxLimit.value;
	}	// $[TI6] -- 'maxConstraintInfo.value' is more restrictive...


	// this setting has no maximum soft bounds...
	maxConstraintInfo.isSoft = FALSE;

	// update the maximum bound to the most restrictive bound...
	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}
