#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ServicePromptMgr - The central control for all Service prompt area
//---------------------------------------------------------------------
//@ Interface-Description
// Objects which handle Service prompt change.  It remembers the previous
// prompt so we can restore the previous prompt if it had been replaced by
// other prompts.
//---------------------------------------------------------------------
//@ Rationale
// This class is necessary for Service Mode because we want to restore
// a test prompt when it is replaced by an user prompt (eg key board
// prompt etc).
//---------------------------------------------------------------------
//@ Implementation-Description
// A instance of this class can be created by a test class.  The class 
// uses the stringIds_[] as the centerpiece.  This is an array of string
// pointers.  Any time, the test prompt needs to be displayed we call
// setTestPrompt() to store this prompt into the stringIds_[] array and
// inform the PromptArea to display this prompt.  Whenever the user prompt
// is removed due to lose of AdjustPanel target, the restoreTestPrompt()
// will be invoked to restored the previous test prompt is there is any.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// None. 
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServicePromptMgr.ccv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  29-AUG-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ServicePromptMgr.hh"

//@ Usage-Classes
#include "GuiApp.hh"
//@ End-Usage

//=====================================================================
//
//		Static Data...
//
//=====================================================================


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServicePromptMgr   [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes stringIds_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServicePromptMgr::ServicePromptMgr(void)
{
	CALL_TRACE("ServicePromptMgr::ServicePromptMgr(void)");

	// Initialize the local prompt strings.
	for (Int row = 0; row < PromptArea::PA_NUMBER_OF_PRIORITIES; row++)
	{
		for (Int col = 0; col < PromptArea::PA_NUMBER_OF_TYPES; col++)
		{
			stringIds_[row][col] = NULL_STRING_ID;
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServicePromptMgr  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  N/A
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServicePromptMgr::~ServicePromptMgr(void)
{
	CALL_TRACE("ServicePromptMgr::~ServicePromptMgr(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTestPrompt
//
//@ Interface-Description
// Called by any test when a prompt is requested.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Store the prompt into the stringIds_[].  Inform PromptArea to display
//  the prompt.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServicePromptMgr::setTestPrompt(PromptArea::PromptType promptType,
								PromptArea::PromptPriority priority,
								StringId stringId)
{
	CALL_TRACE("ServicePromptMgr::setTestPrompt(PromptType promptType, "
							"PromptPriority priority, StringId stringId)");

#if defined FORNOW
printf("ServicePromptMgr::setTestPrompt at line  %d, priority = %d, promptType = %d, stringId = %s\n",
						 __LINE__, priority, promptType, stringId);
#endif	// FORNOW

	stringIds_[priority][promptType] = stringId;
	
	// Set the prompt.
	GuiApp::PPromptArea->setPrompt(promptType, priority, stringId);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: restoreTestPrompt
//
//@ Interface-Description
// Called by test subscreen when adjustPanelRestoreFocusHappened() method
// is called.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If there was a high priority primary prompt stored in stringIds_[], then
//  we inform the PromptArea to display the high priority prompts (from the
//  stringIds_[]).  Otherwise, we inform the PromptArea to display the low priority
//  prompts (from the stringIds_[]).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServicePromptMgr::restoreTestPrompt(void)
{
	CALL_TRACE("ServicePromptMgr::restoreTestPrompt(void)");

#if defined FORNOW
printf("ServicePromptMgr::restoreTestPrompt at line  %d\n", __LINE__);
#endif	// FORNOW

	if (stringIds_[PromptArea::PA_HIGH][PromptArea::PA_PRIMARY] != NULL_STRING_ID)
	{
		// Restore the Primary prompts
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					 PromptArea::PA_HIGH, stringIds_[PromptArea::PA_HIGH][PromptArea::PA_PRIMARY]);
			
		// Restore the Advisory prompts
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					 PromptArea::PA_HIGH, stringIds_[PromptArea::PA_HIGH][PromptArea::PA_ADVISORY]);

		// Restore the Secondary prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_HIGH, stringIds_[PromptArea::PA_HIGH][PromptArea::PA_SECONDARY]);
	}
	else
	{
		// Restore the Primary prompts
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					 PromptArea::PA_LOW, stringIds_[PromptArea::PA_LOW][PromptArea::PA_PRIMARY]);
			
		// Restore the Advisory prompts
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					 PromptArea::PA_LOW, stringIds_[PromptArea::PA_LOW][PromptArea::PA_ADVISORY]);

		// Restore the Secondary prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, stringIds_[PromptArea::PA_LOW][PromptArea::PA_SECONDARY]);
	}
}




#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ServicePromptMgr::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("ServicePromptMgr::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SERVICEPROMPTMGR,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
