#ifndef BdSst_HH
#define BdSst_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  BdSst - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/BdSst.hhv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 

#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
#include "FsmState.hh"
//@ End-Usage

class BdSst : public FsmState
{
  public:
    BdSst();
    ~BdSst();

    const char* nameOf(void) const;
    SigmaState getSigmaState(void) const;
    void receiveCommUp(const CommUpMessage& message) const;
    void receiveGuiReady(const GuiReadyMessage& message) const;
	void receiveChangeState(const ChangeStateMessage& message) const;

    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);

 
  private:
    BdSst(const BdSst&);              // not implemented
    void operator=(const BdSst&);         // not implemented
};
 

#endif // BdSst_HH
