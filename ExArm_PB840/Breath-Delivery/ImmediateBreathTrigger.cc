#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ImmediateBreathTrigger - Triggers immediately upon being enabled.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers a change in the active breath phase on the next 
//    BD cycle.  Its trigger condition always evaluates to true.  This
//    trigger is kept on a list contained in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    This class is used to implement an immediate change in the active
//    breath phase on the next BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of 
//    whether this trigger is enabled or not.  If the trigger is not 
//    enabled, it will always return a state of false.  If enabled, and
//    on the active trigger list in the BreathTriggerMediator,it 
//    will always return true.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ImmediateBreathTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ImmediateBreathTrigger.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ImmediateBreathTrigger
//
//@ Interface-Description
//	This constructor takes triggerId as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ using BreathTrigger constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ImmediateBreathTrigger::ImmediateBreathTrigger(
	const Trigger::TriggerId triggerId
) : BreathTrigger(triggerId)
{
  CALL_TRACE("ImmediateBreathTrigger(Trigger::TriggerId triggerId)");
  // $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ImmediateBreathTrigger() 
//
//@ Interface-Description 
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ImmediateBreathTrigger::~ImmediateBreathTrigger(void)
{
  CALL_TRACE("~ImmediateBreathTrigger()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ImmediateBreathTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, IMMEDIATEBREATHTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  It always returns true.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method always returns true.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
ImmediateBreathTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");
 
  // $[TI1]
  return (TRUE);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
