#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Filename:  BoundedValue - Type Definition of 'BoundedValue'.
//---------------------------------------------------------------------
//@ Interface-Description
//  This type defines the structure of bounded values.  These values
//  provide a storage type for the values of all bounded settings.  These
//  values are defined as a floating-point/precision pair.  Every bounded
//  setting value is represented by a floating-point number, along with
//  the precision, or resolution, of that number.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a generic bounded setting storage type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This type is a structure containing a floating-point value and a
//  precision enumerator.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BoundedValue.ccv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "BoundedValue.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  External Functions...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

#include "SettingConstants.hh"

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function:  operator<<(ostr, boundedValue)
//
// Interface-Description
//  Print the contents of 'boundedValue' to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Free-Function
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const BoundedValue& boundedValue)
{
  CALL_TRACE("::operator<<(ostr, boundedValue)");

  if (boundedValue.value != SettingConstants::UPPER_ALARM_LIMIT_OFF &&
      boundedValue.value != SettingConstants::LOWER_ALARM_LIMIT_OFF)
  {
    ostr << '<' << boundedValue.value;
    ostr << ", " << boundedValue.precision << '>';
  }
  else
  {
    ostr << "OFF";
  }

  return (ostr);
}

#endif  // defined(SIGMA_DEVELOPMENT)
