
#ifndef TestResultTable_HH
#define TestResultTable_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  TestResultTable - Result Table.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/TestResultTable.hhv   25.0.4.0   19 Nov 2013 14:18:56   pvcs  $
//
//@ Modification-Table
//
//  Revision: 006  By:  rhj    Date:  16-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//		Added singleTestLogEnabled_ data member and EnableSingleTestLog
//      function.
//
//  Revision: 005  By:  rhj    Date:  04-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//		Added setAllTestsUndefined() and added a new parameter for 
//      CalcOverallTestResult.
//
//  Revision: 004  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 003  By:  sah    Date:  24-Sep-1997    DCS Number: 2513
//  Project:  Sigma (R8027)
//  Description:
//	Eliminated pre-processor variables around the 'overrideAllTests()'
//	for use by the Service Laptop.
//
//  Revision: 002  By:  sah    Date:  18-Jul-1997    DCS Number: 2246
//  Project:  Sigma (R8027)
//  Description:
//	Changed 'overrideFailures_()' to a public method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "TemplateMacros.hh"
#include "NovRamStatus.hh"

//@ Usage-Classes
#include "ResultTableEntry.hh"
#include "NovRamUpdateManager.hh"

class AbstractArray(ResultTableEntry);

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif // defined(SIGMA_DEVELOPMENT)
//@ End-Usage


class TestResultTable
{
#if defined(SIGMA_DEVELOPMENT)
    friend Ostream&  operator<<(Ostream&               ostr,
    				const TestResultTable& testResultTable);
#endif // defined(SIGMA_DEVELOPMENT)

  public:
    ~TestResultTable(void);

    void  startingTest(const Uint testNumber);
    void  intermediateResultLogged(
			       const Uint         testNumber,
			       const TestResultId resultOfIntermediateStep
			       	  );
    void  completedTest(const Uint testNumber);
    void  overrideFailures(void);
    void  overrideAllTests(void);

    void  setToNotApplicable(const Uint testNumber);

#if defined(SIGMA_GUI_CPU)
    void  updateEntries(const AbstractArray(ResultTableEntry)& newBdEntries);
#endif // defined(SIGMA_GUI_CPU)

    static void  MergeResultTables(
		      AbstractArray(ResultTableEntry)&       rMergedTable,
		      const AbstractArray(ResultTableEntry)& arrGuiResults,
		      const AbstractArray(ResultTableEntry)& arrBdResults,
		      const Uint                             maxEntries
			          );

    static void  CalcOverallTestResult(
		      ResultEntryData&                       rOverallResult,
#if defined(SIGMA_GUI_CPU)
		      const AbstractArray(ResultTableEntry)& arrGuiResults,
#endif // defined(SIGMA_GUI_CPU)
		      const AbstractArray(ResultTableEntry)& arrBdResults,
		      const Uint                             maxEntries,
              const NovRamUpdateManager::UpdateTypeId  updateTypeId
				      );

#if defined(SIGMA_GUI_CPU)
    static Boolean  HaveAllTestsPassed(
		      const AbstractArray(ResultTableEntry)& arrResults,
		      const Uint                             maxEntries
				      );
#endif // defined(SIGMA_GUI_CPU)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  protected:
    TestResultTable(ResultTableEntry*                       pArrEntries,
    		    const Uint                              maxEntries,
		    const NovRamUpdateManager::UpdateTypeId updateTypeId);

    void  copyCurrEntries_(
		      AbstractArray(ResultTableEntry)& rCurrEntries
			  ) const;

    NovRamStatus::InitializationId  initTable_  (const Uint maxEntries);
    NovRamStatus::VerificationId    verifyTable_(const Uint maxEntries) const;
    void  setAllTestsUndefined_(void);

  private:
    TestResultTable(const TestResultTable&);	// not implemented...
    TestResultTable(void);			// not implemented...
    void  operator=(const TestResultTable&);	// not implemented...

    //@ Constant:  UPDATE_TYPE_ID_
    // This identifies the update ID of this result table.
    const NovRamUpdateManager::UpdateTypeId  UPDATE_TYPE_ID_;

    //@ Constant:  MAX_ENTRIES_
    // The maximum number of entries allowed in this fault log.
    const Uint  MAX_ENTRIES_;

    //@ Data-Member:  pArrEntries_
    // A pointer to the derived class's array of log entries.
    ResultTableEntry* const  pArrEntries_;

};


#endif // TestResultTable_HH 
