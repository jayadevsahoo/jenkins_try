#ifndef PDTimerId_HH
#define PDTimerId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: PDTimerId - Enumerated ids for the Patient Data timers.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PDTimerId.hhv   25.0.4.0   19 Nov 2013 14:17:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By:   rhj    Date: 09-March-2009    SCR Number: 6454
//  Project:  840S
//  Description:
//      Added INSP_TIDAL_VOL_ALARM_TIMER.
//  
//  Revision: 004   By: rhj    Date:  17-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Changed INSP_SPONT_TIDAL_VOL_TIMER to INSP_TIDAL_VOL_TIMER.
//
//  Revision: 003   By: rhj    Date:  03-Oct-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Added Cdyn, Rdyn, C20/c and PSF timers.
//
//  Revision: 002  By:  hct    Date:  19-JAN-99     DR Number: 5322
//       Project:  ATC
//       Description:
//             ATC baseline.
//
//  Revision: 001  By:  jhv    Date:  26-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//
//====================================================================

class PDTimerId
{
public:
	enum PDTimerIdType
	{
		//For Patient Data subsystem
		SPONT_TIDAL_VOL_TIMER,
		MAND_TIDAL_VOL_TIMER,
		INSP_TIDAL_VOL_TIMER,
		DYNAMIC_COMPLIANCE_TIMER,
		DYNAMIC_RESISTANCE_TIMER,
		C20_TO_C_RATIO_TIMER,
		PEAK_SPONT_INSP_FLOW_TIMER,
        INSP_TIDAL_VOL_ALARM_TIMER,
#if		defined(SIGMA_DEVELOPMENT)
		WAVEFORM_DATA_TIMER,
		END_BREATH_DATA_TIMER,
		PEEND_DATA_TIMER,
		REALTIME_DATA_TIMER,
		AVERAGE_DATA_TIMER,
#endif 	// defined(SIGMA_DEVELOPMENT)
		NUM_TIMER_IDS
	};
};

#endif // PDTimerId_HH 
