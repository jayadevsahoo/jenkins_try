#ifndef	SerialBuffer_HH
# define SerialBuffer_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  SerialBuffer - Serial-Interface Serial Data Buffer
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SerialBuffer.hhv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003    By: quf       Date: 16-Aug-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//  Introduced encapsulated version of class IDs.
//
//  Revision: 002    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//  Revision  001	By: gdc	Date: X/XX/96	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "IpcIds.hh"

//@ Usage-Classes
//@ End-Usage

class MsgQueue;

class SerialBuffer
{
  public:
	SerialBuffer(  IpcId mutexId
                 , Uint * pPacketMemory
                 , Uint packetSize
                 , Uint numPackets 
	             , Uint ** pPacketMap );

	virtual ~SerialBuffer();

	virtual Uint read( void * pMemory, Uint count, Uint milliseconds = 0 );
	virtual Uint write( const void * pMemory, Uint count, Uint milliseconds = 0 );

	static void  SoftFault(const SoftFaultID softFaultID,
					       const Uint32	lineNumber,
					       const char *	pFileName = NULL,
					       const char *	pPredicate = NULL);

  protected:

  private:
	Boolean      isEmpty_(void) const;
	Boolean      isFull_(void) const;
    Uint **      inc_( Uint ** pPacket );
	void         idleFunction_(void) const;

    //@ Data-Member: mutexId_
    //  Shared resource protection
	IpcId        mutexId_;

    //@ Data-Member: pPacketMemory_
    //  Pointer to contiguous memory containing the message packets
	Uint *       pPacketMemory_;

    //@ Data-Member: packetSize_
    //  Size in bytes of each packet in packet memory
	Uint         packetSize_;

    //@ Data-Member: numPackets_
    //  Count of packets in packet memory
	Uint         numPackets_;

	//@ Data-Member: pPacketMap_
	//  Pointer to the message packet map containing pointers to each
	//  message packet in packet memory. The packet map is a
	//  circular buffer of packet pointers used to queue data between
	//  producer and consumer.
	Uint **      pPacketMap_;

    //@ Data-Member: pPStartPacket_
    //  Pointer to the packet map pointer of the next packet to be read.
	Uint **      pPStartPacket_;

    //@ Data-Member: pPEndPacket_
    //  Pointer to the packet map pointer of the next packet to be written.
	Uint **      pPEndPacket_;

    //@ Data-Member: packetCount_
    //  The number of packets currently allocated in packet memory.
	Uint         packetCount_;
};

#endif	// ! SerialBuffer_HH
