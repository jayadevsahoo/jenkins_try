#include "stdafx.h"
//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#include "MessageProcessor.h"

#include "EventManager.h"
#include "SwatState.h"
#include  "SwatMessage.h"
#include "BatchCommand.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "VirtualSensor.h"
#include "SocketWrap.hh"

namespace swat
{

    BatchMemoryManager MessageProcessor::m_clBatchMemoryManager(NMEMORY_SLOTS);
    bool (* MessageProcessor::m_fcall)(BatchCommandInfo *);


    MessageProcessor::MessageProcessor(bool (*fcall)(BatchCommandInfo *) )
    {
        m_fcall=fcall;
    }


    bool MessageProcessor::ProcessPacket(char *buf, int iMsgLen)
    {
        if( (unsigned int) iMsgLen < sizeof(NetworkMessageHeader))
        {
            //Invalid message header
            return false;
        }
        
        // Convert bytes order for network message header
        NetworkMessageHeader* pNetMsgHeader = (NetworkMessageHeader*)buf;
        SocketWrap sockWrap;
        pNetMsgHeader->iLength = sockWrap.NTOHL( pNetMsgHeader->iLength );
        pNetMsgHeader->iOffset = sockWrap.NTOHL( pNetMsgHeader->iOffset );

        if( ((NetworkMessageHeader *)buf)->iLength == (unsigned int)iMsgLen )
        {

            ProcessMessage( buf+sizeof(NetworkMessageHeader), iMsgLen-sizeof(NetworkMessageHeader));
            return true;
        }
        else
        {
            //Invalid message header
            return false;
        }
    }




    void MessageProcessor::ProcessMessage(char *pIn, int  iMsgLen )
    {
        int  iExpectedLen;

        //Make sure that the message meets the min requirments.
        if((unsigned int)iMsgLen<sizeof(CommandInfo))
        {
            //Droped message
            //
            EventManager::signalEvent(EventManager::MESSAGE_DROPPED);
            EventManager::signalEvent(EventManager::MESSAGE_INVALID_LENGTH);
            return;
        }



        //If ENABLESWAT set SwatState::bEnabled = true
        if(memcmp(((BatchCommandInfo *)pIn)->clCommandInfo.TypeUUID,SwatMessage::ENABLESWAT,sizeof(SwatMessage::ENABLESWAT))==0)
        {
            SwatState::bEnabled = true;
            return;
        }

        //If swat is not enables, return
        if(SwatState::bEnabled == false)
        {
            EventManager::signalEvent(EventManager::MESSAGE_DROPPED);
            EventManager::signalEvent(EventManager::SWAT_DISABLED);
            return;
        }

        BatchCommandInfo* pBatchCommandInfo = (BatchCommandInfo*)pIn;
        pBatchCommandInfo->convNtoH();

        if(memcmp(((BatchCommandInfo *)pIn)->clCommandInfo.TypeUUID,SwatMessage::EXECUTESCRIPT,sizeof(SwatMessage::EXECUTESCRIPT))==0)
        {
            // Go through all BatchCommand and convert the byte order of their
            // members if the count of BatchCommand great than zero
            if ( pBatchCommandInfo->iNCMD > 0 )
            {
                BatchCommand* pBatchCommand = (BatchCommand*)(pIn + sizeof(BatchCommandInfo));
                for ( int i = 0; i < pBatchCommandInfo->iNCMD; ++i )
                {
                    pBatchCommand->convNtoH();
                    pBatchCommand ++;
                }
            }

            iExpectedLen = sizeof( BatchCommandInfo ) + ((BatchCommandInfo *)pIn)->iNCMD*sizeof(BatchCommand) ;

            //Check length
            if(iExpectedLen != iMsgLen)
            {
                EventManager::signalEvent(EventManager::MESSAGE_DROPPED);
                EventManager::signalEvent(EventManager::MESSAGE_INVALID_LENGTH);
                return;
            }

            BatchCommandInfo *p = (BatchCommandInfo *)m_clBatchMemoryManager.allocate(iExpectedLen);

            if(p==NULL)
            {
                EventManager::signalEvent(EventManager::MESSAGE_DROPPED);
                EventManager::signalEvent(EventManager::MEMORY_NO_ALLOCATE);
            }
            else
            {
                memcpy(p,pIn,iExpectedLen);

                if(m_fcall(p)==false)
                {
                    EventManager::signalEvent(EventManager::MESSAGE_DROPPED);
                    EventManager::signalEvent(EventManager::QUEUE_NO_ALLOCATE);
                    BatchMemoryManager::free(p);
                }
            }
        }

        else if(memcmp(((BatchCommandInfo *)pIn)->clCommandInfo.TypeUUID,SwatMessage::DISABLESWAT,sizeof(SwatMessage::DISABLESWAT))==0)
        {
            SwatState::bEnabled = false;
            VirtualSensorBuffer::reset();
        }
        else if(memcmp(((BatchCommandInfo *)pIn)->clCommandInfo.TypeUUID,SwatMessage::LOADBUFFER,sizeof(SwatMessage::LOADBUFFER))==0)
        {


        }
        else
        {
            EventManager::signalEvent(EventManager::MESSAGE_DROPPED);
            EventManager::signalEvent(EventManager::MESSAGE_INVALID_UUID);
        }

    }

}

