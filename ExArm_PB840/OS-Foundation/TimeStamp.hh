#ifndef TimeStamp_HH
#define TimeStamp_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TimeStamp - A time marker object which represents wall
//  clock time and related functions.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/TimeStamp.hhv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 005   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 004   By: gdc   Date: 04-Jan-2000  DR Number: 5588
//  Project:  NeoMode
//  Description:
//      Replaced global reference with public access method.
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number:1943/1944
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.  Optimized julianDay_ function and
//       removed superfluous operators.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================
#include "SocketWrap.hh"
#include "Sigma.hh"
#include "TimeDay.hh"
#include "OsFoundation.hh"

//@ Usage-Classes
//@ End-Usage

class TimeStamp
{
  public:
	TimeStamp(void);
	TimeStamp(const TimeStamp& rTime);
	TimeStamp(const struct TimeOfDay& rTOD);
	TimeStamp( const Int32 year, const Int32 month, const Int32 dayOfMonth, 
	           const Int32 hour, const Int32 minute, const Int32 second, 
		       const Int32 hsec=0);

	~TimeStamp(void);

	static void Initialize(void);
    static TimeStamp Now(void);

	static const TimeStamp& GetRNullTimeStamp(void);

	void now(void);

    struct TimeOfDay getTimeOfDay() const;

    static const TimeStamp& GetBeginEpoch();

	Int32 operator-(const TimeStamp& rTime) const;
	void operator=(const TimeStamp& rTime);

	Boolean operator<(const TimeStamp& rTime) const;
	Boolean operator<=(const TimeStamp& rTime) const;
	Boolean operator>(const TimeStamp& rTime) const;
	Boolean operator>=(const TimeStamp& rTime) const;
	Boolean operator==(const TimeStamp& rTime) const;
	Boolean operator!=(const TimeStamp& rTime) const;

	inline void invalidate(void);
	inline Boolean isInvalid(void) const;
	inline Uint32 getYear(void) const;
	inline Uint32 getMonth(void) const;
	inline Uint32 getDayOfMonth(void) const;
	inline Uint32 getHour(void) const;
	inline Uint32 getMinutes(void) const;
	inline Uint32 getSeconds(void) const;
	inline Uint32 getHundredths(void) const;
	inline Boolean isBounded(void) const;

	//These APIs are to convert the endiannes of TimeStamp.
	//Only year_ (16 bit) needs conversion..
	//Use when transmitting a TimeStamp object in a network message 
	inline void convHtoN();
	inline void convNtoH();

	static void SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL, 
			      const char*       pPredicate = NULL);

  protected:

  private:
	static TimeStamp& GetRNullTimeStamp_(void);

	//@ Type: TimeConvType
	// Enumerated type that can be used to convert milliseconds to day, 
	// hour, minute, seconds and hundreths of seconds.
	enum TimeConvType
	{
	  MSEC_PER_DAY = 86400000,
	  MSEC_PER_HOUR = 3600000,
	  MSEC_PER_MIN = 60000,
	  MSEC_PER_SEC = 1000,
	  MSEC_PER_HSEC = 10
	};


    Uint32 julianDay_() const;

    Uint32 getMilSec_() const;

	//@ Data-Member: year_
	// 00 to 99 (2000-2099)	because TimeOfDay year is limited to 99
    Uint8 year_;	

	//@ Data-Member: month_
	// 1-12
    Uint8  month_;	

	//@ Data-Member: dayOfMon_
	// 1-31, obviously month-dependent
    Uint8  dayOfMon_;	

	//@ Data-Member: hour_
	// 0-23
	Uint8  hour_;		

	//@ Data-Member: min_
	// 00-59
	Uint8  min_;	

	//@ Data-Member: sec_
	// 00-59
	Uint8  sec_;	

	//@ Data-Member: msec_
	// 00-999
	Uint16  msec_;	
};

// Inlined methods
#include "TimeStamp.in"

#endif // TimeStamp_HH 
