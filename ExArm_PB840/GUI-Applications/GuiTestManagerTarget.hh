#ifndef GuiTestManagerTarget_HH
#define GuiTestManagerTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: GuiTestManagerTarget -  A target for using the ScrollableLog class.  Provides
// callbacks to ScrollableLog for retrieving log text retrieval and for
// communicating row selection/deselection events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiTestManagerTarget.hhv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "GuiAppClassIds.hh"

//@ Usage-Classes
class TestResult;
//@ End-Usage

class GuiTestManagerTarget
{
public:
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
protected:
	GuiTestManagerTarget(void);
	virtual ~GuiTestManagerTarget(void);

private:
    // these methods are purposely declared, but not implemented...
    GuiTestManagerTarget(const GuiTestManagerTarget&);				// not implemented...
    void   operator=(const GuiTestManagerTarget&);			// not implemented...
};


#endif // GuiTestManagerTarget_HH
