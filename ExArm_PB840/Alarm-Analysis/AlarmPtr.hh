#ifndef AlarmPtr_HH
#define AlarmPtr_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================
//
//====================================================================
// Filename: AlarmPtr - Type defined pointer to the Alarm class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmPtr.hhv   25.0.4.0   19 Nov 2013 13:51:16   pvcs  $  $
//
//@ Modification-Log
//
//
//====================================================================

//@ Usage-Classes
#include "Sigma.hh"
#include "AlarmName.hh"

class Alarm;

//@ End-Usage

//@ Type: AlarmPtr
// Typedef used in defining the contents of container class AlarmList.
typedef Alarm * AlarmPtr;



#endif // AlarmPtr_HH
