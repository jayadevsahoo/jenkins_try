
#ifndef SListR_Drawable_MAX_CHANGE_LIST_NUM_HH
#define SListR_Drawable_MAX_CHANGE_LIST_NUM_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SListR_Drawable_MAX_CHANGE_LIST_NUM - Fixed Singly-Linked List of 'Drawable' References.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedSListR.H_v   25.0.4.0   19 Nov 2013 14:03:48   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================
 
#include "GuiSize.hh"
#include "GuiSize.hh"

class  Drawable;  // forward declaration...

//@ Usage-Classes
#include "SListR_Drawable.hh"
#include "SListC_MemPtr_MAX_CHANGE_LIST_NUM.hh"
//@ End-Usage


class SListR_Drawable_MAX_CHANGE_LIST_NUM : public SListR_Drawable
{
  public:
    SListR_Drawable_MAX_CHANGE_LIST_NUM(const SListR_Drawable_MAX_CHANGE_LIST_NUM& sList);
    SListR_Drawable_MAX_CHANGE_LIST_NUM(void);
    virtual ~SListR_Drawable_MAX_CHANGE_LIST_NUM(void);

    inline void  operator=(const SListR_Drawable_MAX_CHANGE_LIST_NUM& list);

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    //@ Data-Member:  ptrList_
    // A list of 'void*'s to store the pointers of this list's objects.
    SListC_MemPtr_MAX_CHANGE_LIST_NUM  ptrList_;
};


// Inlined methods...
#include "SListR_Drawable_MAX_CHANGE_LIST_NUM.in"


#endif  // SListR_Drawable_MAX_CHANGE_LIST_NUM_HH
