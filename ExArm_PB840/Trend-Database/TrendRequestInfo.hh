#ifndef TrendRequestInfo_HH
#define TrendRequestInfo_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendRequestInfo - An encapsulation of the request information
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendRequestInfo.hhv   25.0.4.0   19 Nov 2013 14:34:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 001  By:  mjf    Date:  13-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//
//=====================================================================

#include "TimeStamp.hh"             // OS-Foundation
#include "TrendTimeScaleValue.hh"   // Setting-Validation
#include "TrendSelectValue.hh"    	// Setting-Validation

#include "TrendDataMgr.hh"



class TrendRequestInfo
{
public:    

	TrendRequestInfo( void );
	~TrendRequestInfo( void );
	TrendRequestInfo( const TrendRequestInfo & source );
	TrendRequestInfo & operator=( const TrendRequestInfo & source );

	void clear( void );

	inline void setTimescale( TrendTimeScaleValue::TrendTimeScaleValueId timescale );
	inline void setStartFrameNumber( Int32 startFrame );
	inline void setColumnId( Uint16 column, TrendSelectValue::TrendSelectValueId trendId );

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:

	// Give special "friend class" access to the Data Manager and Repositories, so
	// that they can load the trend data into the set.
	friend class TrendDataMgr;
	friend class TrendDbPatientRepository;
	friend class TrendDbSettingRepository;

	//@ Data-Member: timescale_
	// The requested timescale for the trend data
	TrendTimeScaleValue::TrendTimeScaleValueId timescale_;

	//@ Data-Member: seqNumber_
	// The starting frame sequence number for the trend data request
	Int32 seqNumber_;

	//@ Data-Member: position_
	// Position in the database for this request
	// This is filled in by the Trend Data Manager
	Uint16 position_;

	//@ Data-Member: requestSize_
	// The size of the data request "rows"
	// This is filled in by the Data Manager at a max of 360
	Uint16 requestSize_;

	//@ Data-Member: requestIds_
	// The requested Trend Select Ids for each column
	TrendSelectValue::TrendSelectValueId requestIds_[ TrendDataMgr::MAX_COLUMNS ];
};


// Inlined methods
#include "TrendRequestInfo.in"

#endif // TrendRequestInfo_HH
