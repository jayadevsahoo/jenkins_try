//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIUserFloat.h
//----------------------------------------------------------------------------

#ifndef USERFLOAT_H
#define USERFLOAT_H


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "PatientDataUIValue.h"


//-------------------------------------
// C L A S S  D E C L
//-------------------------------------
class UserFloat:public Value
{
    public:
    UserFloat(char *pName,float f=0.0f);
    
    void  updatePacket(void *address);
    bool  updateValue(float fIn);
    float getFloatValue();
    bool  isFloat();
    bool  setFloat(float fValue);

    protected:
    float  m_fDesiredValue;
    float  m_fValue       ;
};

#endif
