#ifndef ServiceInitializationSubScreen_HH
#define ServiceInitializationSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceInitializationSubScreen - Activated by checking
// GuiApp::IsServiceInitialStateOK() status.  If at startup, the system
// failed any one of the following criteria then this subscreen will be
// activated depending the failing factors.  The list below are the criteria
// we checked: IsFlowSensorInfoRequired, IsBdAndGuiFlashTheSame,
// AreSerialNumberMatches, and IsBdVentInopTestInProgress.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceInitializationSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  syw    Date: 27-Jul-1998    DR Number: DCS 5082
//       Project:  Sigma (840)
//       Description:
//            Increased MAX_CONDITION_ID.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//====================================================================

#include "AdjustPanelTarget.hh"
#include "SubScreen.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "GuiTestManager.hh"
#include "ServiceStatusArea.hh"
#include "SmPromptId.hh"
#include "SubScreenTitleArea.hh"
#include "GuiAppClassIds.hh"
class TestResult;
//@ End-Usage

class ServiceInitializationSubScreen : public SubScreen,
							public AdjustPanelTarget,
							public GuiTestManagerTarget 
{
public:
	ServiceInitializationSubScreen(SubScreenArea *pSubScreenArea);
	~ServiceInitializationSubScreen(void);

	// Redefine SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelRestoreFocusHappened(void);
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);

	// GuiTestMangerTarget virtual methods
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId=-1);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);



	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	enum CalConditionId
	{
		MAX_CONDITION_ID = 6
	};

	//@ Type: serviceInitalTestId
	// Lists the possible screen layout scenarios.
	enum serviceInitalTestId
	{
		INITIAL_NO_TEST_ID=-1,
		INITIAL_TEST_START_ID,
		INITIAL_TEST_GUI_VENT_INOP_ID = INITIAL_TEST_START_ID,
		INITIAL_TEST_BD_VENT_INOP_ID,
		INITIAL_TEST_INITIALIZE_FLOW_SENSORS_ID,
		INITIAL_TEST_CAL_INFO_DUPLICATION_ID,
		INITIAL_TEST_MAX
	};

	enum PromptId
	{
		TEST_POWER_DOWN_PROMPT,
		TEST_GUI_SIDE_TEST_PROMPT,
		TEST_VENT_INOP_A_TEST_PROMPT,
		TEST_VENT_INOP_B_TEST_PROMPT,
		TEST_TEN_SEC_A_TEST_PROMPT,
		TEST_TEN_SEC_B_TEST_PROMPT,
		TEST_SOUND_PROMPT,
		TEST_VENTILATOR_INOPERATIVE_PROMPT,
		TEST_SAFETY_VALVE_OPEN_PROMPT,
		NUM_TEST_PROMPT_MAX
	};

	// these methods are purposely declared, but not implemented...
	ServiceInitializationSubScreen(void);						// not implemented...
	ServiceInitializationSubScreen(const ServiceInitializationSubScreen&);	// not implemented...
	void operator=(const ServiceInitializationSubScreen&);		// not implemented...

	void setTestTable_(void);
	void setTestPromptTable_(void);
	void setErrorTable_(void);
	void startTest_(Int currentTestId);
	void setTestResultStatus_(void);
	void nextTest_(void);

	//@ Data-Member: serviceInitializationStatus
	// A container for displaying the current Service Initialization status.
	ServiceStatusArea serviceInitializationStatus;

	//@ Data-Member: testPromptId_
	// An array which stores the test prompt ids.
	SmPromptId::PromptId testPromptId_[NUM_TEST_PROMPT_MAX];
	
	//@ Data-Member: testPromptName_
	// An array which stores the prompt test item names text.
	StringId testPromptName_[NUM_TEST_PROMPT_MAX];

	//@ Data-Member: errDisplayArea_
	// An array of TextField objects used to display test conditions in the
	// lower screen
	TextField errDisplayArea_[MAX_CONDITION_ID];

    //@ Data-Member: conditionText_
	// Text for error condition prompts
    StringId conditionText_[MAX_CONDITION_ID];

	//@ Data-Member: errorHappened_
	// Flag that indicates status of test
	int	errorHappened_;
	
	//@ Data-Member: errorIndex_
	// Keep tracks of index into TextFields in the error display area
	int	errorIndex_;
	
	//@ Data-Member: promptId_
	// A field which stores the ServiceInitialization prompt ID.
	Int promptId_;

	//@ Data-Member: keyAllowedId_
	// A field which stores the ServiceInitialization keyAllowed ID.
	SmPromptId::KeysAllowedId keyAllowedId_;

	//@ Data-Member: userKeyPressedId_
	// A field which stores the ServiceInitialization userKeyPressed ID.
	SmPromptId::ActionId userKeyPressedId_;

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member: testManager_
	// Object instance of GuiTestManager
	GuiTestManager testManager_;

	//@ Data-Member: currentTestId_
	// Variable to remember currently running test Id.
	Int currentTestId_;

	//@ Data-Member: maxServiceModeTest_
	// The maximum number of test in the ServiceInitialization test item ids.
	Int maxServiceModeTest_;
	
	//@ Data-Member: serviceInitalTestId_
	// An array which stores the ServiceInitialization test item ids.
	SmTestId::ServiceModeTestId serviceInitalTestId_[INITIAL_TEST_MAX];

	//@ Data-Member: serviceInitalTestName_
	// An array which stores the ServiceInitialization test item names text.
	StringId serviceInitalTestName_[INITIAL_TEST_MAX];

};

#endif // ServiceInitializationSubScreen_HH 
