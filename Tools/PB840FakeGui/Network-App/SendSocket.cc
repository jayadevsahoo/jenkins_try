
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SendSocket  - The send only Network socket connection
//     between the GUI and BD boards. This class provides interfaces
//     to send network messages to the other Sigma board.  This
//       class implments the main part of the Network Application
//       protocol between the GUI and BD boards.
//---------------------------------------------------------------------
//@ Interface-Description
//       The class provides interfaces to transmit network messages to
//     other Sigma board using the send only dedicated TCP/IP socket 
//     connection.  The class also ensures the success of each network
//     message transmission without a user task having to delay for a
//     complete network message transmission activity.
//
//     If a network message cannot be transmitted due to the repeated
//     socket library call failures, the class raises a lost message
//     flag, which eventually causes a loss of communication state.
//     The class monitors whether a successfully transmitted message
//     is received by the other side by keeping track of an acknowlege
//     message for each transmitted message. If the other side doesn't
//     respond with an ACK message within a second period, the class
//     raises the message lost flag. 
//
//     The class also provides an interface to send a "This board alive"
//     message periodically. The class maintains the last time that a 
//     network message is sent and it sends the "This board alive"
//     message only if no network message is sent during last 300
//     milli-seconds. 
//---------------------------------------------------------------------
//@ Rationale
//     The SendSocket class uniquely encapsulates the functionalities
//     to send only socket connection to  the other Sigma board.
//---------------------------------------------------------------------
//@ Implementation-Description
//      A SendSocket object is a regular TCP/IP socket connection
//      which is capable of two way communication, but it is used just
//      to send network data to the other Sigma board. The TCP/IP 
//      socket guarantees the data integrity through the CRC checking
//      and sequential order of data delivery. 
//    
//      A SendSocket socket runs in a non-blocked IO mode to avoid
//      transmitting task to be blocked until a network message is
//      completely transmitted at the transport layer ( TCP/IP code).
//---------------------------------------------------------------------
//@ Fault-Handling
//  Some of the repeated socket library errors are recorded to
//  the non-volatile diagnostic memory with the socket library errno
//  code. All the failed requests return an error status to calling
//  methods. Each method follows the contract programming practice to
//  generate a soft fault in the case of software logic error. 
//  The class detects a loss of communication state by timing out
//  the unacknowledged network message or a lost network message.
//---------------------------------------------------------------------
//@ Restrictions
//   This class is designed specifically used by the Network Application
//   task rather than being a generic Socket class. Only one object
//   of this class should be instantiated due to the static memory
//   allocation requirments within the Sigma system. 
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/SendSocket.ccv   25.0.4.0   19 Nov 2013 14:14:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: gdc     Date: 30-Dec-2010  SCR Number: 6719
//  Project:  XENA2
//  Description:
//      Removed unnecessary and uninstatiated MutEx.
//
//  Revision: 005  By: gdc     Date: 30-Dec-2010  SCR Number: 6717
//  Project:  XENA2
//  Description:
//      Corrected creation and initialization of all socket types.
//
//  Revision: 004  By: gdc     Date: 30-Dec-2010  SCR Number: 6716
//  Project:  XENA2
//  Description:
//      Use thread-safe socket library.
//
//  Revision: 003  By: gdc     Date: 30-Dec-2010  SCR Number: 6712
//  Project:  XENA2
//  Description:
//      Use socket sendmsg() instead of send() to eliminate copy 
//      operation.
//
//  Revision: 002  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  18-09-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================
#include "StdAfx.h"
#include "SendSocket.hh"
#include "NetworkApp.hh"

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SendSocket()  [Default Constructor]
//
//@ Interface-Description
//    No parameters. Instantiates the SendSocket object. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

SendSocket::SendSocket(void) :
lastXmitTime_()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SendSocket()  [Destructor]
//
//@ Interface-Description
//   Default Constructor. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Close the socket if it is opened()
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

SendSocket::~SendSocket(void)
{
    if ( opened() )
    {
        down(); 
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendMsg( XmitDataMsgId msgId, char *buf, Uint nbytes)
//
//@ Interface-Description
//    This method transmits a Sigma network message. The 'nbytes'
//    is the size of the data not including the message header.
//    Any network message sent through this interface requires
//    an acknowledgement message from the receving end.
//      The 'buf' points a memory area that contains the network message
//    data. It is allowed to send a network message with a zero
//    length data. If the transmission of a network message fails due
//      to a communication error, the method raises the lost message flag.
//    The method returns a number of bytes that it successfully transmits.
//    If the method fails in sending a specified network message, 
//    the method returns a negative return value.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    This method transmits the network message by calling the sendMsg
//    method of the parent class. A unique  message sequence number
//    is allocated for the network message within the 
//    NetworkSocket::sendMsg method. The message sequence number 
//    and the message id combination uniquely identifies a message.
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//=====================================================================
Int
SendSocket::sendMsg( XmitDataMsgId msgId, char *buf, Uint nbytes)
{
    lastXmitTime_.now();
    return NetworkSocket::sendMsg(msgId, buf, nbytes);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  down()
//
//@ Interface-Description
//      close the socket.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Call the parent class's close method and clear up the lost
//      transmission messge  flag.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SendSocket::down(void)
{
    NetworkSocket::down();
    lostMsgFlag_ = FALSE; // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  sendAliveMsg(XmitDataMsgId msgId)
//
//@ Interface-Description
//    Send the " I am Alive" message if no message is sent to the other
//    board within the last wait alive message tick cycle. 
//---------------------------------------------------------------------
//@ Implementation-Description
//      Call the parent class's close method and clear up the lost
//      transmission messge  flag.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
SendSocket::sendAliveMsg(XmitDataMsgId aliveMsg)
{
    if ( lastXmitTime_.getPassedTime() >= KEEP_ALIVE_INTERVAL )
    {
        sendMsg(aliveMsg, (char *) NULL, 0);
    } 
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifyConnection(XmitDataMsgId msgId)
//
//@ Interface-Description
//    Verify if this socket connection is functioning by sending
//    this board alive message, then receiving the other board alive
//    message using this socket connection.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The messages are received and sent through the NetworkSocket
//    class's sendMsg and readData method to avoid having to handle
//    the ACK messages for these two messages.
//---------------------------------------------------------------------
//@ PreCondition
//      ( opened() )
//   This socket should have been opened.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//---------------------------------------------------------------------

SigmaStatus
SendSocket::verifyConnection(XmitDataMsgId thisActive,
                             XmitDataMsgId otherActive)
{
    CLASS_PRE_CONDITION(opened());

    if ( NetworkSocket::sendMsg(thisActive, (char *) NULL, 0) != 0 )
    {
        NetworkApp::ReportCommError("SendSocket::verifyConnection,sendMsg.", 0);
        return(FAILURE);
    }

    fd_set readMask;
    FD_ZERO(&readMask);
    FD_SET(socketId_, &readMask);

    if ( NetworkApp::WaitForInput(socketId_+1, &readMask, 500) <= 0)
    {
        TaskMonitor::Report();
        NetworkApp::ReportCommError("SendSocket::verifyConnection,WaitForInput.", 0);
        return(FAILURE); 
    }

    TaskMonitor::Report();
    if ( NetworkSocket::recvMsg(otherActive, (char *) NULL, 0) != 0 )
    {
        TaskMonitor::Report();
        NetworkApp::ReportCommError("SendSocket::verifyConnection,recvMsg.", 0);
        return(FAILURE); 
    }

    return(SUCCESS); 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  anyLostMsg()
//
//@ Interface-Description
//      Determines if this socket has lost a network message. A network
//      message is considered lost when a network message transmission
//      fails due to the repeated send socket library call failures or
//      a successfully tranmitted network message hasn't been acknowledged
//      by the receiver within a second.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//     The ackMsgTimeout method is called to timeout an un-acknowledged
//     network message only if the lost message flag is not raised, yet.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Boolean
SendSocket::anyLostMsg() const
{
    if ( lostMsgFlag_ )
    {
        NetworkApp::ReportCommError(
                                   "SendSocket::anyLostMsg lost a xmit message",
                                   NETWORKAPP_MSG_TRANSMISSION_FAILED);
    } 
    return lostMsgFlag_;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SendSocket::SoftFault(const SoftFaultID  softFaultID,
                      const Uint32       lineNumber,
                      const char*        pFileName,
                      const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, NETWORK_APPLICATION,
                            SEND_SOCKET_CLASS, lineNumber, pFileName, pPredicate);
}
