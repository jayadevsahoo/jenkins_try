
#ifndef Btps_HH
#define Btps_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Btps - BTPS compensation
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Btps.hhv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  syw    Date:  15-May-1997    DR Number: DCS 1620
//       Project:  Sigma (R8027)
//       Description:
//       	Moved CalculateBtps to .cc since code added is no longer an inline
//			method.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.  Added missing Sigma.hh
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

#include "LinearSensor.hh"
#include "MiscSensorRefs.hh"

//@ Usage-Classes
//@ End-Usage

extern const Real32 STD_ATM_PRESSURE;
extern const Real32 VAPOR_PRESS_BTPS;
extern const Real32 VAPOR_PRESS_GAS;
extern const Real32 TEMPERATURE_SLPM;
extern const Real32 TEMPERATURE_BTPS;
extern const Real32 BODY_TEMPERATURE;
extern const Real32 STD_TEMPERATURE;

class Btps {
  public:
    static inline Real32 GetExpBtpsCf( void) ;
    static inline Real32 GetInspBtpsCf( void) ;
    static void CalculateBtps( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL) ;

  protected:

  private:
    Btps( void) ;						// not implemented...
    ~Btps( void) ;						// not implemented...
    Btps( const Btps&) ;				// not implemented...
    void   operator=( const Btps&) ;	// not implemented...

    //@ Data-Member:  InspBtpsCf_
    // compensation from BTPS to dry for inspiration
    static Real32 InspBtpsCf_ ;

    //@ Data-Member:  ExpBtpsCf_
    // compensation from dry to BTPS for expiration
    static Real32 ExpBtpsCf_ ;

} ;


// Inlined methods...
#include "Btps.in"


#endif // Btps_HH 





