#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbTableBuffer
//---------------------------------------------------------------------
//@ Interface-Description
//  The table buffer is used to hold the data that will be written to
//  the compact flash or data that has been read from the compact flash.
//  The buffer is made up of some number of blocks or some size.  The 
//  buffer knows that size of the sample records but has no knowledge of 
//  the insides (details) of the sample records.  The client is responsible
//  for providing the memory that the buffer will use, 
// 
//  The Buffer is constructed by passing in a pointer to the memory to use and
//  the number of blocks and the size of each block.  The sample record size is
//  used to iterate over the sample records stored in the buffer.
// 
//  The sample records can iterated in either a forward or backward direction.
//  The firstRecord() method is used to position the record at the start of
//  the buffer and the nextRecord() can be used to iterate in the forward 
//  direction, nextRecord() will return FAILURE once the end of the buffer is
//  reached.  The lastRecord() positions the record at the last record in the
//  buffer (setLastPosition() is used to set this position) and then nextRecord()
//  can be used to iterate in the backward direction over the sample records.
//  nextRecord() will return FAILURE once it reaches the start of the buffer.
//  
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the handling of buffers that are used to queue data to/from
//  the compact flash.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Memory management is outside of this class to give more control of memory 
//  usage to the clients which allows sharing of the memory between different 
//  buffers objects.
//
//  Records are fixed into blocks, a sample record will not span a block.
//  Therefore 1 or more sample record will be fixed into a block and there
//  maybe a few extra bytes at the end of the block which is not used.  The
//  record sizes should be configured to reduce this waste as much as possible.
//  
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbTableBuffer.ccv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  mjf    Date:  18-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
//
//=====================================================================

//@ Usage-Classes
//@ End-Usage

#include "Trend_Database.hh"
#include "TrendDbTableBuffer.hh"
#include "TrendDbRecord.hh"


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbTableBuffer [constructor]
//
//@ Interface-Description
//  Initializes the buffer passing in a memory pointer to use the number
//  of blocks, the size of each block and the size of a sample record
//  The client must provide enough memory to hold the buffer size 
//  (blockCount * blockSize)
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the buffer variables, defaults the record position to 
//  empty (-1)
//---------------------------------------------------------------------
//@ PreCondition
//	pMemory must exist 
//  both recordSize and blockSize are non zero
//  and recordSize <= blockSize
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbTableBuffer::TrendDbTableBuffer( MemPtr pMemory, Uint8 blockCount,
										Int16 blockSize, Int16 recordSize )
{
	AUX_CLASS_PRE_CONDITION( pMemory, (Uint32) pMemory );
	AUX_CLASS_PRE_CONDITION( blockSize, blockSize );
	AUX_CLASS_PRE_CONDITION( recordSize, recordSize );
	AUX_CLASS_PRE_CONDITION( recordSize <= blockSize,
							 (blockSize << 8) | recordSize );
	// Initialize
	pMemoryBuffer_ = pMemory;
	blockCount_ = blockCount;
	blockSize_ = blockSize;
	recordSize_ = recordSize;
	recordPerBlock_ = blockSize / recordSize;
	recordPosition_ = -1;
	lastPosition_ = getRecordCapacity() - 1;

	TREND_MGR_DBG4( 32, "TableBuffer Addr ", hex, pMemoryBuffer_, dec );
	TREND_MGR_DBG4( 32, "  Blocks ", blockCount_,
					", size ", blockSize_ );
	TREND_MGR_DBG6( 32, "  Record size ", recordSize_,
					", perBlock ", recordPerBlock_,
					", last position ", lastPosition_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbTableBuffer [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbTableBuffer::~TrendDbTableBuffer()	
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getRecordOffset_ [private]
//
//@ Interface-Description
// Method to return a memory pointer to the given record position inside
// the buffer
//---------------------------------------------------------------------
//@ Implementation-Description
//  Finds the block which holds the record and then finds the the offset into
//  the block for the given sample record.  Returns the address of the record.
//---------------------------------------------------------------------
//@ PreCondition
//	position is non negative and position < capacity
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
MemPtr TrendDbTableBuffer::getRecordOffset_( Int16 position )
{
	AUX_CLASS_PRE_CONDITION( (position >= 0 && position <= lastPosition_), 
							 ((lastPosition_ << 16) | position) );

	Uint8 blocks = position / recordPerBlock_;
	Uint8 entry = position % recordPerBlock_;
	Uint32 addr = (Uint32) pMemoryBuffer_ + (blocks * blockSize_) + (entry * recordSize_);
	return( (MemPtr) addr ); 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFirstRecord
//
//@ Interface-Description
// Sets the record position to the first record in the
// database, and sets the record to access the first position
//---------------------------------------------------------------------
//@ Implementation-Description
//  Updates the records memory pointer to point into the buffer
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTableBuffer::setFirstRecord( TrendDbRecord & record )
{
	recordPosition_ = 0;
	MemPtr pRecord = getRecordOffset_( recordPosition_ );
	record.setMemoryPointer( pRecord );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setLastRecord
//
//@ Interface-Description
// Sets the record position to the last record in the buffer
// and sets the record to access that position in the buffer
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the high water mark "lastPosition" to know where the data ends
//  in the buffer and sets the last record to this position
//  Updates the records memory pointer to point into the buffer
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTableBuffer::setLastRecord( TrendDbRecord & record )
{
	recordPosition_ = lastPosition_;
	MemPtr pRecord = getRecordOffset_( recordPosition_ );
	record.setMemoryPointer( pRecord );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: gotoNextEntry
//
//@ Interface-Description
//  Moves the record position
//   Will retuen SUCCESS if the move was OK otherwise,
//   TRUE = forward until end and then returns FAILURE
//   FALSE = backward until begin and then returns FAILURE
//---------------------------------------------------------------------
//@ Implementation-Description
//  Updates the record memory pointer
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus TrendDbTableBuffer::gotoNextRecord( TrendDbRecord & record, Boolean goForward )
{
	SigmaStatus moveOk  = FAILURE;
	MemPtr pRecord = 0;

	// Update the record position forward or backward
	if (goForward)
	{
		recordPosition_++;
	}
	else
	{
		recordPosition_--;
	}

	// Update the record memory pointer if the position is in range
	// else return FALSE
	if (recordPosition_ >= 0 && recordPosition_ <= lastPosition_)
	{
		pRecord = getRecordOffset_( recordPosition_ );
		record.setMemoryPointer( pRecord );
		moveOk = SUCCESS;
	}

	// return SUCCESS if record position updated
	return( moveOk );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setLastPosition
//
//@ Interface-Description
//  Set the high water mark in the database, where is the last record in
//  the buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Used by the lastRecord() method
//---------------------------------------------------------------------
//@ PreCondition
//  Must be less then or equal to recordCapacity()
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTableBuffer::setLastPosition( Int16 lastPosition )
{
	AUX_CLASS_PRE_CONDITION( (lastPosition >= 0 && lastPosition < getRecordCapacity()), 
							 ((lastPosition_ << 16) | getRecordCapacity()) );

	lastPosition_ = lastPosition;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setRecordPosition
//
//@ Interface-Description
//  Set the current record position
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTableBuffer::setRecordPosition( Int16 position )
{
	if (position >= 0 && position < getRecordCapacity())
	{
		recordPosition_ = position;
	}
	else
	{
		// Set position to empty
		recordPosition_ = -1;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getBlockMemory
//
//@ Interface-Description
// Method used to get access to the raw memory on a block boundary, returns 
// the starting address of the given block in the buffer
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	blockNum must be in range
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
MemPtr TrendDbTableBuffer::getBlockMemory( Uint8 blockNum )
{
	AUX_CLASS_PRE_CONDITION( (blockNum < blockCount_),
							 ((blockNum << 16) | blockCount_ ) );

	Uint32 memAddr = (Uint32) pMemoryBuffer_ + (blockNum * blockSize_);     
	return( (MemPtr) memAddr );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbTableBuffer::SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName,
								   const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_TABLE_BUFFER,
							lineNumber, pFileName, pPredicate);
}


