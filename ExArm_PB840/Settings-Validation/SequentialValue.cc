#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Filename:  SequentialValue - Type Definition of 'SequentialValue'.
//---------------------------------------------------------------------
//@ Interface-Description
//  This type defines the structure of sequential values.  These values
//  provide a storage type for the values of all sequential settings.  These
//  values are defined as 32-bit integers.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a generic sequential setting storage type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This type is a typedef of a 'Int32'.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SequentialValue.ccv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SequentialValue.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...
