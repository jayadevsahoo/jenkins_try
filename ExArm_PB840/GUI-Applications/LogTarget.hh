#ifndef LogTarget_HH
#define LogTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LogTarget -  A target for using the ScrollableLog class.  Provides
// callbacks to ScrollableLog for retrieving log text retrieval and for
// communicating row selection/deselection events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LogTarget.hhv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "GuiAppClassIds.hh"
//@ End-Usage

class LogTarget
{
public:
	virtual void getLogEntryColumn(Uint16 entryNumber, Uint16 columnNumber,
									Boolean &rIsCheapText, StringId &rString1,
									StringId &rString2, StringId &rString3,
									StringId &rString1Message);

	virtual void currentLogEntryChangeHappened(
						Uint16 changedEntry, Boolean isSelected);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
protected:
	LogTarget(void);
	virtual ~LogTarget(void);

private:
    // these methods are purposely declared, but not implemented...
    LogTarget(const LogTarget&);				// not implemented...
    void   operator=(const LogTarget&);			// not implemented...
};


#endif // LogTarget_HH
