#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MutEx - Mutual exclusion mechanism to protect shared data.
//---------------------------------------------------------------------
//@ Interface-Description
// The MutEx class can be used to protect the integrity of data that
// is shared between multiple tasks, or any code that must have
// multiple tasks access it in a Mutually Exclusive manner.  To use
// a MutEx, information about the MutEx must be entered in the 
// IpcTableDef.cc file including a unique I.D. and the type, Ipc::MUTEX. 
// This will cause the mutually exclusion mechanism to be created
// at initialization time. Multiple tasks (objects) 
// create their own handle by passing the IpcIds to the MutEx 
// constructor.  After the MutEx has been constructed, each task
// calls the methods request() and release() to use the MutEx.
// A common use would be for a class that has methods that update
// static class data. These methods can be called from multiple
// tasks, so the update code must not be interrupted by another
// task calling the same function.  To solve this problem, a MutEx
// is added as a private class member, and the update methods simply
// call request() and release() which surround the critical code.
// The MutEx can be a static (shared) or non-static class member.
// It is recommended and simplifies the code if the MutEx is
// a non-static member, so that each instance has its own MutEx instance.
//---------------------------------------------------------------------
//@ Rationale
// The MutEx class was created to provide a mutual exclusion binary
// for code that is not inherently task safe.
// The users of this class need to determine exactly why and when
// the use of a MutEx is required. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The MutEx class uses the Windows MutEx services to 
//  provide mutual exclusion.
//---------------------------------------------------------------------
//@ Fault-Handling
// The only error that can be returned from a MutEx method is 
// Ipc::TIMED_OUT, which will be returned if request() is called with
// a non-zero timeout value, and the time expired before the MutEx is
// released. Assertions are also used to catch fault conditions, along with 
// pre and post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/MutEx.ccv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
#include "Sigma.hh"
#include "OsFoundation.hh"
#include "MutEx.hh"

//@ Usage-Classes
#include "IpcTable.hh"
//@ End-Usage

//  Static Data definition

//  lookup array to store handles of all created mutexs when they are initialized
//  entry 0 is NULL
static HANDLE    MutExLookup[MutEx::MAX_MUTEX_ID+1];


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MutEx()
//
//@ Interface-Description
// Uses the I.D. from the IpcTable in IpcTableDef.cc, which also must be
// in the list of I.D.s in IpcIds.hh.  This creates a handle to a 
// MutEx that is initially free.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Copies the the MutEx id from the static MutEx::MutExLookup 
//  array.
//---------------------------------------------------------------------
//@ PreCondition
//  (0 < id && id <= MAX_MUTEX_ID)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
MutEx::MutEx(const Int32 id) : ID_(id)
{ 
    CLASS_PRE_CONDITION(0 < id && id <= MAX_MUTEX_ID);
    handle_ = MutExLookup[id];		// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MutEx()  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
MutEx::~MutEx(void)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: request()
//
//@ Interface-Description
// Takes one argument which indicates how many milliseconds that the
// caller is willing to wait for the resource.  The default is
// NO_TIMEOUT(=0), which will cause the call to block until the resource
// is acquired.  If a wait time in milliseconds is given,
// then it is converted to VRTX ticks (about 10 milliseconds per tick),
// and rounded up to the nearest tick (e.g., 1 millisecond = 1 tick).
// If the time expires before the resource is available, then 
// Ipc::TIMED_OUT is returned, otherwise Ipc::OK is returned.
//
// Note: The timeout is not synchronized with the VRTX32 clock. Thus,
// a time-out value of one VRTX32 clock tick reults in the task's time-out
// period ending on the next occurrence of any VRTX32 clock tick. The
// actual elaspsed time the task times out could be less than one VRTX32
// clock tick in this example.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls WaitForSingleObject to gain access to the semaphore.
//  The arguments are the seamphore id, timeout value, and the address
//  of an error return location.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32 MutEx::request(const Uint32 timeout)
{
//If OS is Windows:
#if defined(_WIN32_WCE) || defined(WIN32)
    DWORD Result = WaitForSingleObject(handle_, timeout);
	switch(Result)
	{
	case WAIT_OBJECT_0:
		error_ = Ipc::OK;
		break;
	case WAIT_TIMEOUT:
		error_ = Ipc::TIMED_OUT;
		break;
	case WAIT_FAILED:
		error_ = WAIT_FAILED;
		break;
	case WAIT_ABANDONED:
	default:
		error_ = GetLastError();
		break;
	}
	AUX_CLASS_ASSERTION( (error_ == Ipc::OK) || (error_ == Ipc::TIMED_OUT), error_ | ID_<<24 | GetCurrentThreadId()<<16);

    return error_;						   // $[TI1]
#else
	return Ipc::TIMED_OUT;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: release()
//
//@ Interface-Description
// Takes no arguments, and simply releases the resource.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls Windows ReleaseMutex. With the MutEx id and address of error
//  return location as arguments.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void MutEx::release(void)
{
//If OS is Windows:
#if defined(_WIN32_WCE) || defined(WIN32)
	if(ReleaseMutex(handle_) == 0)
	{
		error_ = GetLastError();
        //TODO E600: Below error occures if attempt to release mutex NOT owned by caller. Yet to find solution for the same.
        if(error_ == ERROR_NOT_OWNER)
		{
           error_ = Ipc::OK;
		}
	}
	else
	{
		error_ = Ipc::OK;
	}
	AUX_CLASS_ASSERTION(error_ == Ipc::OK, error_ | ID_<<24);		// $[TI1]
#endif
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
//  This function creates a MutEx for every entry listed in the
//  IpcTableDef.cc file labeled as MUTEX.
//---------------------------------------------------------------------
//@ Implementation-Description
//  For each entry in the IpcTable, this function calls CreateMutex 
//  to create a MutEx with the specified name and return the MutEx id 
//  which is stored in the MutExLookup table.
//---------------------------------------------------------------------
//@ PreCondition
//  (IpcTable::IsInitialized() == TRUE);
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void MutEx::Initialize(void)
{
	CLASS_PRE_CONDITION( IpcTable::IsInitialized() );
//If OS is Windows:
#if defined(_WIN32_WCE) || defined(WIN32)
 
	for (Int32 i = 0; i < IpcTable::GetNumEntries(); i++)
	{
		const IpcTableEntry& ipcEntry = IpcTable::GetIpcTableEntry(i);
		if (ipcEntry.type == Ipc::MUTEX)
		{					// $[TI1]
			CLASS_ASSERTION(0 < ipcEntry.id && ipcEntry.id <= MutEx::MAX_MUTEX_ID);
			Uint32 error = Ipc::OK;
			Uint32 id = ipcEntry.id;
			WCHAR name[IpcTable::MAX_Q_NAME_STRING_LENGTH];
			mbstowcs(name, ipcEntry.name, IpcTable::MAX_Q_NAME_STRING_LENGTH);
			MutExLookup[id] = CreateMutex(NULL, FALSE, name);
			if(MutExLookup[id] == NULL)
			{
				error = GetLastError();
			}
			else
			{
				SetLastError(0);
			}
			//TODO E600 VM upgrade to __try / except handling
			AUX_CLASS_ASSERTION( (MutExLookup[id] != NULL) && (error == Ipc::OK), error | id<<24)
		}					// $[TI2]
	}
#endif
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
MutEx::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::MUTEX,
      lineNumber, pFileName, pPredicate);
}
