//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file EventLogger.h
//----------------------------------------------------------------------------


#ifndef EVENTLOGGER_H
#define EVENTLOGGER_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "IEventLogger.h"
#include "Message.h"
#include "MessageHandlerCallBack.h"
#include <list>
#include <boost/shared_ptr.hpp>
#include <map>
#include "Mutex.h"
#include "Thread.h"
#include "Semaphore.hh"
#include "NetworkMsgHeader.hh"


//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class EventLogger:public IEventLogger
{
    public:

    /// @brief EventLogger Constructor
    EventLogger();

    /// @brief ~EventLogger Destructor
    ~EventLogger();

    /// @brief enqueueEvent
    /// 
    /// @param pMsg A pointer to a message.
    /// 
    /// @return true if success.
    virtual bool enqueueEvent(const boost::shared_ptr<Message> pMsg);


    /// @brief addMessageHandler
    /// 
    /// @param id Id of message to look for.
    /// @param function A pointer to a function to handle messages with given id.
    /// @param pUser Pointer to user data.
    /// 
    /// @return true if successfull.
    virtual bool addMessageHandler(unsigned int id, 
            bool (*function)(boost::shared_ptr<Message> msg, void *pUser), void *pUser);
    

	virtual bool enable();
	virtual bool disable();



    private:

    void ThreadEntranceProc();
    static void *ThreadEntranceProc   (void *pVoid);

    boost::shared_ptr<Message> dequeueEvent();


    bool          m_bRunning;
    std::list<boost::shared_ptr<Message> > m_MessageList;

    Mutex     * m_MessageListMutex;
    Semaphore * m_MessageListSemaphore;
    Thread    * m_pThread;
	bool      m_bActive;

    std::map<unsigned int,boost::shared_ptr<MessageHandlerCallBack> > m_CallBack;
};
#endif

