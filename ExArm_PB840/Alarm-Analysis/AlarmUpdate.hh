#ifndef AlarmUpdate_HH
#define AlarmUpdate_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmUpdate - Alarm-Analysis class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmUpdate.hhv   25.0.4.0   19 Nov 2013 13:51:18   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 01/11/96         DR Number:   649
//      Project:   Sigma   (R8027)
//      Description:
//         Changed conditional compilation label from SIGMA_DEVELOPMENT to
//         SIGMA_SIMULATOR.
//  
//   Revision 003   By:   hct      Date: 02/10/97         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.  Don't need AlarmComm anymore.
//
//   Revision 012	By:   sah	   Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//		   Removed all referenced to newly-obsoleted class ('AlarmMessages').
//  
//   Revision 004   By:   sah      Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//         Removed all references to newly-obsoleted class ('AlarmMessages').
//
//   Revision 005   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 006   By:   yyy      Date: 08/18/99         DR Number:   5513
//      Project:   ATC
//      Description:
//         Modified to handle alarm lock and break through.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "Alarm.hh"
#include "MessageNameArray.hh"
#include "TimeStamp.hh"

class AlarmAnnunciator;
//@ End-Usage


class AlarmUpdate
{
  public:
    AlarmUpdate();      
    AlarmUpdate (const AlarmUpdate& rUpdate);
    AlarmUpdate (const Alarm& rAlarm);
    ~AlarmUpdate(void);

    void      operator= (const AlarmUpdate& rUpdate);
    Boolean   operator==(const AlarmUpdate& rUpdate) const;
    Boolean   operator!=(const AlarmUpdate& rUpdate) const;
    Boolean   operator< (const AlarmUpdate& rUpdate) const;
    Boolean   operator> (const AlarmUpdate& rUpdate) const;

#ifdef SIGMA_SIMULATOR
    MessageName  getUpdateTypeMessage   (void) const;
#endif

    MessageName  getUrgencyMessage      (void) const;


    inline  MessageName    getBaseMessage       (void)                    const;
    inline  const          MessageNameArray&    getAnalysisMessage(void)  const;
    inline  const          MessageNameArray&    getRemedyMessage  (void)  const;
    inline  Alarm::Urgency getCurrentUrgency    (void)                    const;
    inline  Alarm::Urgency getPreviousUrgency   (void)                    const;
    inline  const          TimeStamp&           getWhenOccurred   (void)  const;
    inline  void           setPreviousUrgency   (Alarm::Urgency currentUrgency);

    inline  Alarm::UpdateType                 getUpdateType        (void) const;
    inline  AlarmName                         getName              (void) const;
    inline  Uint32                            getAnalysisIndex     (void) const;
    inline  Uint32                            getRemedyIndex       (void) const;
    inline  Boolean                           getIsActive          (void) const;
    inline  Int16                             getCurrentPriority   (void) const;
    inline  Boolean                           getIsDependent       (void) const;
    inline  Boolean                           getBreakThroughSilence(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL);
  
  protected:

  private:

    //@ Data-Member:    name_
    // Enumerator uniquely identifying an alarm.
    AlarmName           name_;

    //@ Data-Member:    isActive_
    // State of the alarm.
    Boolean             isActive_;

    //@ Data-Member:    isDependent_
    // Attribute isDependent_ tracks whether the alarm is a dependent
    // augmentation or an independent alarm.
    Boolean             isDependent_;

    //@ Data-Member:    whenOccurred_
    // When the alarm changed state.
    TimeStamp           whenOccurred_;

    //@ Data-Member:    currentUrgency_
    // Current Urgency of the alarm.
    Alarm::Urgency             currentUrgency_;

    //@ Data-Member:    previousUrgency_
    // The urgency of the Alarm on the previous  cycle of alarm processing.
    Alarm::Urgency             previousUrgency_;

    //@ Data-Member:    currentPriority_
    // Current priority of the alarm.
    Int16               currentPriority_;

    //@ Data-Member:    baseMessage_
    // This is the enumerator which identifies the text associated with the
    // Alarm Name/Base Message column of the Alarm Matrices.
    MessageName         baseMessage_;

    //@ Data-Member:    currentAnalysisMessage_
    // Array containing the message enumerators for the analysis messages.
    MessageNameArray    currentAnalysisMessage_;

    //@ Data-Member:    currentRemedyMessage_
    // Array containing the message enumerators for the remedy messages.
    MessageNameArray    currentRemedyMessage_;

    //@ Data-Member:    updateType_
    // Instance variable used in determining the reason for the reevaluation
    // of an Alarm.
    Alarm::UpdateType   updateType_;

    //@ Data-Member:    analysisIndex_
    // Attribute analysisIndex_ tracks the number of analysis messages in the
    // currentAnalysisMessage_ array.
    Uint32              analysisIndex_;

    //@ Data-Member:    remedyIndex_
    // Attribute remedyIndex_ tracks the number of remedy messages in the
    // currentRemedyMessage_ array.
    Uint32              remedyIndex_;

    //@ Data-Member:    breakThroughSilence_
    // Attribute breakThroughSilence_ allows an alarm to break through
    // Alarm Silence.
    Boolean             breakThroughSilence_;    

};


// Inlined methods
#include "AlarmUpdate.in"


#endif // AlarmUpdate_HH 
