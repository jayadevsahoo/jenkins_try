//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file pool_allocator.h
///
/// This file contains the STL-compatible fixed block allocator. Fixed block
/// allocators are used to eliminate dynamic storage allocation heap
/// fragmentation issues. It is also much faster than the standard global
///	heap.
///
///	This class is abstract and therefore you must inherit from it and implement
/// the pure virtual function get_allocator(). The get_allocator returns
/// the instance of Allocator that is to be used to allocate memory.
///
/// This class does not actually implement the fixed block allocator algorithm
/// but instead is an STL-compatible wrapper around the Allocator class which
/// does provide the fixed block memory services.
///
/// Fixed block allocators are used to eliminate dynamic storage allocation
/// heap fragmentation issues.
///
/// The user of this class is responsible to ensure the STL container never
///	needs a size greater than the fixed size provided by the allocator,
/// otherwise a	SoftFault is triggered.
//------------------------------------------------------------------------------
#ifndef POOL_ALLOCATOR_H
#define POOL_ALLOCATOR_H

//------------------------------------------------------------------------------
// I N C L U D E S
//------------------------------------------------------------------------------
#include "allocator.hh"
#include "FaultHandlerMacros.hh"
#include "UtilityClassId.hh"

//-----------------------------------------------------------------------------
/// pool_allocator is an STL allocator that uses a fixed-block memory
/// allocator.
//-----------------------------------------------------------------------------
template <typename T> class pool_allocator;
template <> class pool_allocator<void>
{
public:
    typedef void* pointer;
    typedef const void* const_pointer;
    // reference to void members are impossible.
    typedef void value_type;

	//-------------------------------------------------------------------------
    /// Convert an pool_allocator<void> to an pool_allocator<U>.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    template <class U>
        struct rebind { typedef pool_allocator<U> other; };
};

/*namespace stl_alloc
{
    inline void destruct(char *){}
    inline void destruct(wchar_t*){}
    template <typename T>
        inline void destruct(T *t){t->~T();}
} // namespace*/


template <typename T>
class pool_allocator
{
public:
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef T* pointer;
    typedef const T* const_pointer;
    typedef T& reference;
    typedef const T& const_reference;
    typedef T value_type;

	//-------------------------------------------------------------------------
    /// Convert an pool_allocator<void> to an pool_allocator<U>.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    template <class U>
        struct rebind { typedef pool_allocator<U> other; };

	//-------------------------------------------------------------------------
    /// Constructor
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    pool_allocator(){}

	//-------------------------------------------------------------------------
    /// Destructor
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    virtual ~pool_allocator(){}

	//-------------------------------------------------------------------------
    /// Return reference address.
	/// @return		Pointer to T memory.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    pointer address(reference x) const {return &x;}

	//-------------------------------------------------------------------------
    /// Return reference address.
	/// @return		Const pointer to T memory.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    const_pointer address(const_reference x) const {return &x;}

	//-------------------------------------------------------------------------
    /// Allocate a fixed block of memory.
	/// @param[in]	n - the size of the memory to allocate.
	/// @param[in]	hint
	/// @return		Pointer to the memory.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    pointer allocate(size_type n, pool_allocator<void>::const_pointer hint = 0)
    {
		return static_cast<pointer>(get_allocator(n*sizeof(T))->Allocate(n*sizeof(T)));
    }

    //for Dinkumware:
	// This member function is used by containers when compiled with a compiler that cannot
	// compile rebind.
    //char *_Charalloc(size_type n){return static_cast<char*>(mem_.allocate(n));}
    // end Dinkumware

	//-------------------------------------------------------------------------
    /// Copy constructor
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    template <class U> pool_allocator(const pool_allocator<U>&){}

	//-------------------------------------------------------------------------
    /// Deallocate a fixed memory block.
	/// @param[in]	p - pointer to the memory
	/// @param[in]	n - size of memory
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    void deallocate(pointer p, size_type n)
    {
		get_allocator(n*sizeof(T))->Deallocate(p);
    }

	//-------------------------------------------------------------------------
    /// Deallocate a fixed memory block.
	/// @param[in]	p - pointer to the memory
	/// @param[in]	n - size of memory
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    void deallocate(void *p, size_type n)
    {
		get_allocator(n*sizeof(T))->Deallocate(p);
    }

	//-------------------------------------------------------------------------
    /// Maximum size of memory.
	/// @return		Max memory size.
	/// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    size_type max_size() const throw() {return size_t(-1) / sizeof(value_type);}

	//-------------------------------------------------------------------------
    /// Constructs a new instance.
	/// @param[in]	p - pointer to the memory where the instance is constructed
	///		using placement new.
	/// @param[in]	val - instance of object to copy construct.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    void construct(pointer p, const T& val)
	{
		new(static_cast<void*>(p)) T(val);
    }

	//-------------------------------------------------------------------------
    /// Constructs a new instance.
	/// @param[in]	p - pointer to the memory where the instance is constructed
	///		using placement new.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    void construct(pointer p)
    {
        new(static_cast<void*>(p)) T();
    }

	//-------------------------------------------------------------------------
    /// Destroys an instance. Objects created with placement new must
	///	explicitly call the destructor.
	/// @param[in]	p - pointer to object instance.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
	void destroy(pointer p)
	{
		p->~T();
	}
    //void destroy(pointer p) { stl_alloc::destruct(p); }

	//-------------------------------------------------------------------------
    /// Get an instance of the memory allocator.
	/// @return		An instance of the allocator used to create variable sized
	///	byte blocks depending on the size requested.
	/// @param[in]	n - size of memory
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
	virtual Allocator* get_allocator(size_t n) { CLASS_ASSERTION_FAILURE(); return NULL; }

private:
	//--------------------------------------------------------------------------
	///  This method is called when a software fault is detected by the
	///  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
	///  and 'lineNumber' are essential pieces of information.  The
	/// 'pFileName' and 'pPredicate' strings may be defaulted in the macro
	/// to reduce code space.
	///
	/// @param[in] SoftFaultID The ID of the softFault.
	/// @param[in] lineNumber The line number of the file.
	/// @param[in] pFileName The filename string.
	/// @param[in] pPredicate The boolean expression causing the fault
	/// @param[out] none.
	/// @return     none.
	///
	/// @pre  none.
	/// @post A software fault is reported.
	//--------------------------------------------------------------------------
	static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const Char*       pFileName  = NULL,
						   const Char*       pPredicate = NULL);
};

//------------------------------------------------------------------------------
// SoftFault
//------------------------------------------------------------------------------
template <typename T>
void pool_allocator<T>::SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const Char*        pFileName,
						  const Char*        pPredicate)
{
	FaultHandler::SoftFault(softFaultID, TOOLS,
			POOL_ALLOCATOR, lineNumber, pFileName, pPredicate);
}

template <typename T, typename U>
inline bool operator==(const pool_allocator<T>&, const pool_allocator<U>){return true;}

template <typename T, typename U>
inline bool operator!=(const pool_allocator<T>&, const pool_allocator<U>){return false;}

// For VC6/STLPort 4-5-3 see /stl/_alloc.h, line 464
// "If custom allocators are being used without member template classes support :
// user (on purpose) is forced to define rebind/get operations !!!"
#ifdef _WIN32
#define POOL_ALLOC_CDECL __cdecl
#else
#define POOL_ALLOC_CDECL
#endif

namespace std{
template <class _Tp1, class _Tp2>
inline pool_allocator<_Tp2>& POOL_ALLOC_CDECL
__stl_alloc_rebind(pool_allocator<_Tp1>& __a, const _Tp2*)
{
    return (pool_allocator<_Tp2>&)(__a);
}


template <class _Tp1, class _Tp2>
inline pool_allocator<_Tp2> POOL_ALLOC_CDECL
__stl_alloc_create(const pool_allocator<_Tp1>&, const _Tp2*)
{
    return pool_allocator<_Tp2>();
}

} // namespace std
// end STLPort

#endif // POOL_ALLOCATOR_H

