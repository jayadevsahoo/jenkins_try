#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: EventSubScreen - Activated by selecting the event tab button
//                          in the upper screen. This subscreen allows 
//                          the user to enter manual events. 
//---------------------------------------------------------------------
//@ Interface-Description
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// timer event is passed into timerEventHappened() to reset the user's
// selection after three minutes is timed out.  
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the EventSubScreen
// in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/EventSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: mnr    Date: 07-Jan-2010    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Comments updated as per code review feedback.
//
//  Revision: 003   By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Events not dependent on Software option anymore.
//
//  Revision: 002   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      New Manual Event buttons added and enabled two page mode.
//
//  Revision: 001  By: rhj    Date: 06-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version.
//=====================================================================

#include "EventSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "UpperScreen.hh"
#include "UpperSubScreenArea.hh"
#include "TimeStamp.hh"
#include "TextUtil.hh"
#include "SettingContextHandle.hh"
#include "TrendDataMgr.hh"
#include "TrendEvents.hh"
#include "ContextObserver.hh"
#include "AcceptedContextHandle.hh"
#include "Sound.hh"
#include "AdjustedContext.hh"
#include "Setting.hh"
#include "SettingsMgr.hh"
#include "PatientCctTypeValue.hh"

#ifdef EVENTSUBSCREEN_DEBUG
    #include "Ostream.hh"
#endif 
//@ End-Usage

//@ Code...
static const Int32 EVENT_BUTTON_WIDTH_ = 105; 
static const Int32 EVENT_BUTTON_HEIGHT_ = 50;
static const Int32 EVENT_BUTTON_X_GAP_ = 20;
static const Int32 EVENT_BUTTON_Y_GAP_ = 20;
static const Int32 EVENT_BUTTON_X1_ = 15; 
static const Int32 EVENT_BUTTON_Y1_ = 55;
static const Int32 EVENT_BUTTON_BORDER_ = 3;

static const Int32 EVENT_DIALOG_BUTTON_HEIGHT_ = 40;
static const Int32 EVENT_DIALOG_BUTTON_WIDTH_ = 90;
static const Int32 EVENT_DIALOG_BUTTON_X_GAP_ = 10;
static const Int32 EVENT_DIALOG_BUTTON_Y_GAP_ = 10;

static const Int32 EVENT_DIALOG_BOX_X_GAP_ = 15;
static const Int32 EVENT_DIALOG_BOX_Y_GAP_ = 15;

static const Int32 EVENT_DIALOG_BUTTON_X1_ = EVENT_DIALOG_BUTTON_X_GAP_  + EVENT_DIALOG_BOX_X_GAP_; 
static const Int32 EVENT_DIALOG_BUTTON_Y1_ = (EVENT_DIALOG_BOX_Y_GAP_) +  (2* EVENT_DIALOG_BUTTON_Y_GAP_);

static const Int32 EVENT_DIALOG_BUTTON_X2_ = EVENT_DIALOG_BUTTON_X1_ + EVENT_DIALOG_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_; 
static const Int32 EVENT_DIALOG_BUTTON_Y2_ = EVENT_DIALOG_BUTTON_Y1_;

static const Int32 EVENT_DIALOG_TITLE_X_ = EVENT_DIALOG_BUTTON_X1_; 
static const Int32 EVENT_DIALOG_TITLE_Y_ = (EVENT_DIALOG_BOX_Y_GAP_ );
static const Int32 MAX_EVENT_BUTTON_PRESSED_DOWN = 3;

// This is the number of buttons that can fit nicely on the screen
static const Int32 NUM_EVENT_BUTTONS_PER_PAGE = 10;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EventSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen (the Upper subscreen
// area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all buttons and the subscreen title.  Adds all buttons
// and the subscreen title object to the parent container for display.
// Registers for button callbacks.
// $[TR01152] - Each Event Button shall contain an Event Id and a text
// $[TR01155] - The Event Selection subscreen shall contain...
//---------------------------------------------------------------------
//@ PreCondition
// The given 'pSubScreenArea' pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

EventSubScreen::EventSubScreen(SubScreenArea* pSubScreenArea) :     
SubScreen(pSubScreenArea),
eventSuctionButton_( EVENT_BUTTON_X1_ + 0 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
					 EVENT_BUTTON_Y1_ + 0 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
					 EVENT_BUTTON_WIDTH_, 
					 EVENT_BUTTON_HEIGHT_, 
					 Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
					 MiscStrs::EVENT_SUCTION_TITLE_BUTTON, 
					 MiscStrs::EMPTY_STRING), 
eventRxButton_(EVENT_BUTTON_X1_ + 1 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
			   EVENT_BUTTON_Y1_ + 0 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
			   EVENT_BUTTON_WIDTH_, 
			   EVENT_BUTTON_HEIGHT_, 
			   Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
			   MiscStrs::EVENT_RX_TITLE_BUTTON ,
			   MiscStrs::EMPTY_STRING), 
eventBloodGasButton_(EVENT_BUTTON_X1_ + 2 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
					 EVENT_BUTTON_Y1_ + 0 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
					 EVENT_BUTTON_WIDTH_, 
					 EVENT_BUTTON_HEIGHT_, 
					 Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
					 MiscStrs::EVENT_BLOOD_GAS_TITLE_BUTTON ,
					 MiscStrs::EMPTY_STRING), 
eventRespiratoryManeuverButton_(EVENT_BUTTON_X1_ + 3 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
								EVENT_BUTTON_Y1_ + 0 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
								EVENT_BUTTON_WIDTH_, 
								EVENT_BUTTON_HEIGHT_, 
								Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
								MiscStrs::EVENT_RESPIRATORY_MANEUVER_TITLE_BUTTON, 
								MiscStrs::EMPTY_STRING), 
eventCircuitChangeButton_(EVENT_BUTTON_X1_ + 4 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
						  EVENT_BUTTON_Y1_ + 0 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
						  EVENT_BUTTON_WIDTH_, 
						  EVENT_BUTTON_HEIGHT_, 
						  Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
						  MiscStrs::EVENT_CIRCUIT_CHANGE_TITLE_BUTTON ,
						  MiscStrs::EMPTY_STRING), 
eventStartWeaningButton_(EVENT_BUTTON_X1_ + 0 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
						 EVENT_BUTTON_Y1_ + 1 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
						 EVENT_BUTTON_WIDTH_, 
						 EVENT_BUTTON_HEIGHT_, 
						 Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
						 MiscStrs::EVENT_START_WEANING_TITLE_BUTTON,
						 MiscStrs::EMPTY_STRING), 
eventStopWeaningButton_(EVENT_BUTTON_X1_ + 1 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
						EVENT_BUTTON_Y1_ + 1 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
						EVENT_BUTTON_WIDTH_, 
						EVENT_BUTTON_HEIGHT_, 
						Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
						MiscStrs::EVENT_STOP_WEANING_TITLE_BUTTON, 
						MiscStrs::EMPTY_STRING), 
eventBronchoscopyButton_(EVENT_BUTTON_X1_ + 2 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
						 EVENT_BUTTON_Y1_ + 1 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
						 EVENT_BUTTON_WIDTH_, 
						 EVENT_BUTTON_HEIGHT_, 
						 Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
						 MiscStrs::EVENT_BRONCHOSCOPY_TITLE_BUTTON ,
						 MiscStrs::EMPTY_STRING), 
eventXrayButton_(EVENT_BUTTON_X1_ + 3 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
				 EVENT_BUTTON_Y1_ + 1 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
				 EVENT_BUTTON_WIDTH_, 
				 EVENT_BUTTON_HEIGHT_, 
				 Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
				 MiscStrs::EVENT_XRAY_TITLE_BUTTON,
				 MiscStrs::EMPTY_STRING), 
eventRecruitmentManeuverButton_(EVENT_BUTTON_X1_ + 4 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
								EVENT_BUTTON_Y1_ + 1 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
								EVENT_BUTTON_WIDTH_, 
								EVENT_BUTTON_HEIGHT_, 
								Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
								MiscStrs::EVENT_RECRUITMENT_MANEUVER_TITLE_BUTTON,
								MiscStrs::EMPTY_STRING), 
eventPrevScreenButton_(EVENT_BUTTON_X1_ + 4 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
							   EVENT_BUTTON_Y1_ + 2 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
							   EVENT_BUTTON_WIDTH_, 
							   EVENT_BUTTON_HEIGHT_, 
							   Button::DARK, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
							   MiscStrs::EVENT_PREV_SCREEN_BUTTON,
							   MiscStrs::EMPTY_STRING), 
eventNextScreenButton_(EVENT_BUTTON_X1_ + 4 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
				  EVENT_BUTTON_Y1_ + 2 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
				  EVENT_BUTTON_WIDTH_, 
				  EVENT_BUTTON_HEIGHT_, 
				  Button::DARK, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
				  MiscStrs::EVENT_NEXT_SCREEN_BUTTON,
				  MiscStrs::EMPTY_STRING),
eventRespositionPatientButton_(EVENT_BUTTON_X1_ + 0 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
							   EVENT_BUTTON_Y1_ + 0 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
							   EVENT_BUTTON_WIDTH_, 
							   EVENT_BUTTON_HEIGHT_, 
							   Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
							   MiscStrs::EVENT_RESPOSITION_PATIENT_TITLE_BUTTON,
							   MiscStrs::EMPTY_STRING), 
eventOtherButton_(EVENT_BUTTON_X1_ + 1 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
				  EVENT_BUTTON_Y1_ + 0 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
				  EVENT_BUTTON_WIDTH_, 
				  EVENT_BUTTON_HEIGHT_, 
				  Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
				  MiscStrs::EVENT_OTHER_TITLE_BUTTON,
				  MiscStrs::EMPTY_STRING), 
eventSurfactantAdminButton_(EVENT_BUTTON_X1_ + 2 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
							   EVENT_BUTTON_Y1_ + 0 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
							   EVENT_BUTTON_WIDTH_, 
							   EVENT_BUTTON_HEIGHT_, 
							   Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
							   MiscStrs::EVENT_SURFACTANT_ADMIN_TITLE_BUTTON,
							   MiscStrs::EMPTY_STRING), 
eventPronePositionButton_(EVENT_BUTTON_X1_ + 3 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
				  EVENT_BUTTON_Y1_ + 0 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
				  EVENT_BUTTON_WIDTH_, 
				  EVENT_BUTTON_HEIGHT_, 
				  Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
				  MiscStrs::EVENT_PRONE_POSITION_TITLE_BUTTON,
				  MiscStrs::EMPTY_STRING),
eventSupinePositionButton_(EVENT_BUTTON_X1_ + 4 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
							   EVENT_BUTTON_Y1_ + 0 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
							   EVENT_BUTTON_WIDTH_, 
							   EVENT_BUTTON_HEIGHT_, 
							   Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
							   MiscStrs::EVENT_SUPINE_POSITION_TITLE_BUTTON,
							   MiscStrs::EMPTY_STRING), 
eventLeftSidePositionButton_(EVENT_BUTTON_X1_ + 0 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
				  EVENT_BUTTON_Y1_ + 1 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
				  EVENT_BUTTON_WIDTH_, 
				  EVENT_BUTTON_HEIGHT_, 
				  Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
				  MiscStrs::EVENT_LEFT_SIDE_POSITION_TITLE_BUTTON,
				  MiscStrs::EMPTY_STRING),
eventRightSidePositionButton_(EVENT_BUTTON_X1_ + 1 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
							   EVENT_BUTTON_Y1_ + 1 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
							   EVENT_BUTTON_WIDTH_, 
							   EVENT_BUTTON_HEIGHT_, 
							   Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
							   MiscStrs::EVENT_RIGHT_SIDE_POSITION_TITLE_BUTTON,
							   MiscStrs::EMPTY_STRING), 
eventManualStimulationButton_(EVENT_BUTTON_X1_ + 2 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_),
				  EVENT_BUTTON_Y1_ + 1 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_),
				  EVENT_BUTTON_WIDTH_, 
				  EVENT_BUTTON_HEIGHT_, 
				  Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
				  MiscStrs::EVENT_MANUAL_STIMULATION_TITLE_BUTTON,
				  MiscStrs::EMPTY_STRING),
numEventButtonsPressed_(0),
eventAcceptButton_(  EVENT_DIALOG_BUTTON_X2_,
					 EVENT_DIALOG_BUTTON_Y2_,
					 EVENT_DIALOG_BUTTON_WIDTH_, 
					 EVENT_DIALOG_BUTTON_HEIGHT_,
					 Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
					 MiscStrs::EVENT_ACCEPT_BUTTON_TITLE,
					 MiscStrs::EMPTY_STRING, 
					 ::GRAVITY_RIGHT, 
					 Button::PB_TOGGLE 
				  ),                      
eventClearButton_(EVENT_DIALOG_BUTTON_X1_,
				  EVENT_DIALOG_BUTTON_Y1_,
				  EVENT_DIALOG_BUTTON_WIDTH_, 
				  EVENT_DIALOG_BUTTON_HEIGHT_,
				  Button::LIGHT, EVENT_BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER, 
				  MiscStrs::EVENT_CLEAR_BUTTON_TITLE,
				  MiscStrs::EMPTY_STRING, 
				  ::GRAVITY_RIGHT,  
				  Button::PB_TOGGLE ),
eventDialogTitle_(MiscStrs::EVENT_DIALOG_TITLE),
titleArea_( MiscStrs::EVENT_SUBSCREEN_TITLE, SubScreenTitleArea::SSTA_CENTER)
{
	CALL_TRACE("EventSubScreen::EventSubScreen(pSubScreenArea)");

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Add the title area and button
	addDrawable(&titleArea_);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

    // Add the event buttons.
	Uint8 idx = 0u;
	eventButtons_[idx++] = &eventSuctionButton_;
	eventButtons_[idx++] = &eventRxButton_;
	eventButtons_[idx++] = &eventBloodGasButton_;
	eventButtons_[idx++] = &eventRespiratoryManeuverButton_;
	eventButtons_[idx++] = &eventCircuitChangeButton_;
	eventButtons_[idx++] = &eventStartWeaningButton_;
	eventButtons_[idx++] = &eventStopWeaningButton_;
	eventButtons_[idx++] = &eventBronchoscopyButton_;
	eventButtons_[idx++] = &eventXrayButton_;
	eventButtons_[idx++] = &eventRecruitmentManeuverButton_;
	eventButtons_[idx++] = &eventRespositionPatientButton_;
	eventButtons_[idx++] = &eventOtherButton_;
	eventButtons_[idx++] = &eventSurfactantAdminButton_;
	eventButtons_[idx++] = &eventPronePositionButton_;
	eventButtons_[idx++] = &eventSupinePositionButton_;
	eventButtons_[idx++] = &eventLeftSidePositionButton_;
	eventButtons_[idx++] = &eventRightSidePositionButton_;
	eventButtons_[idx++] = &eventManualStimulationButton_;

	eventButtons_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= NUM_EVENT_BUTTONS_), idx);

	for (idx = 0u; eventButtons_[idx] != NULL; idx++)
	{
		// Set event button callbacks
		eventButtons_[idx]->setButtonCallback( this );
		if( idx < NUM_EVENT_BUTTONS_PER_PAGE ) 
		{
			// add NUM_EVENT_BUTTONS_PER_PAGE event buttons to the subscreen
			// to display first page
			addDrawable(eventButtons_[idx] );
		}
	}

	// [TR01155] flatten the event buttons that are not available for current cct type
	if( PATIENT_CCT_TYPE_VALUE == PatientCctTypeValue::NEONATAL_CIRCUIT )
	{
		eventRespiratoryManeuverButton_.setToFlat(); 
		eventBronchoscopyButton_.setToFlat();
		eventRecruitmentManeuverButton_.setToFlat();
		eventRespositionPatientButton_.setToFlat();
	}
	else
	{
		eventSurfactantAdminButton_.setToFlat();
		eventPronePositionButton_.setToFlat();
		eventSupinePositionButton_.setToFlat();
		eventLeftSidePositionButton_.setToFlat();
		eventRightSidePositionButton_.setToFlat();
		eventManualStimulationButton_.setToFlat();
	}

    // Set the PrevScreen and NextScreen callbacks
	eventPrevScreenButton_.setButtonCallback( this );
	eventNextScreenButton_.setButtonCallback( this );
	
	addDrawable( &eventNextScreenButton_ );
    
	// Set the Accept and Clear callbacks
	eventAcceptButton_.setButtonCallback( this );
	eventClearButton_.setButtonCallback( this );

	// Set the Event Dialog title's attributes.
	eventDialogTitle_.setX( EVENT_DIALOG_TITLE_X_ );
	eventDialogTitle_.setY( EVENT_DIALOG_TITLE_Y_ );
	eventDialogTitle_.setColor(Colors::WHITE);


	// Set the event Dialog container's attributes.
	eventDialogCtr_.setX( EVENT_BUTTON_X1_ + 1.5 * (EVENT_BUTTON_WIDTH_ + EVENT_BUTTON_X_GAP_) - EVENT_DIALOG_BUTTON_X_GAP_);
	eventDialogCtr_.setY( EVENT_BUTTON_Y1_ + 2 * (EVENT_BUTTON_HEIGHT_ + EVENT_BUTTON_Y_GAP_) - EVENT_DIALOG_BOX_Y_GAP_ );
	eventDialogCtr_.setWidth( (2* EVENT_DIALOG_BUTTON_X_GAP_) + (2 * EVENT_DIALOG_BOX_X_GAP_) + (2 * EVENT_DIALOG_BUTTON_WIDTH_) + EVENT_BUTTON_X_GAP_);
	eventDialogCtr_.setHeight( (3* EVENT_DIALOG_BOX_Y_GAP_) + EVENT_DIALOG_BUTTON_HEIGHT_ + (2* EVENT_DIALOG_BUTTON_Y_GAP_) );

	// Set the event dialog box's attributes.
	eventDialogBox_.setX( EVENT_DIALOG_BUTTON_X_GAP_  );
	eventDialogBox_.setY( (EVENT_DIALOG_BUTTON_Y_GAP_ / 2)  );
	eventDialogBox_.setWidth( eventDialogCtr_.getWidth() - (2* EVENT_DIALOG_BUTTON_X_GAP_) );
	eventDialogBox_.setHeight( eventDialogCtr_.getHeight() - (2 * EVENT_DIALOG_BUTTON_Y_GAP_));
	eventDialogBox_.setColor(Colors::WHITE);

	// Add drawable objects to the event dialog container.
	eventDialogCtr_.addDrawable(&eventDialogTitle_ );
	eventDialogCtr_.addDrawable(&eventDialogBox_ );
	eventDialogCtr_.addDrawable(&eventClearButton_ );
	eventDialogCtr_.addDrawable(&eventAcceptButton_ );

	addDrawable( &eventDialogCtr_ );


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~EventSubScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

EventSubScreen::~EventSubScreen(void)
{
	CALL_TRACE("EventSubScreen::~EventSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate() 
//
//@ Interface-Description
//  Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Hide the Event Dialog Container and reset the number of buttons 
//  pressed. 
// $[TR01151] -- Selecting the EVENT tab on the upper screen shall 
//                cause the Event Selection Subscreen to display... 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
EventSubScreen::activate(void)
{
	CALL_TRACE("EventSubScreen::activate(void)");

	numEventButtonsPressed_ = 0;

	eventDialogCtr_.setShow(FALSE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate() [virtual]
//
//@ Interface-Description
//  Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set all of the event buttons to up and clear the prompts. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
EventSubScreen::deactivate(void)
{
	CALL_TRACE("EventSubScreen::deactivate(void)");

	numEventButtonsPressed_ = 0;

	for (Uint8 idx = 0u; eventButtons_[idx] != NULL; idx++)
	{
		// Set event button to up only if they were DOWN before
		// leave the FLAT ones alone
		if( eventButtons_[idx]->getButtonState() == Button::DOWN )
		{
			eventButtons_[idx]->setToUp();
		}
	}

	// Clear Primary and Advisory prompts.
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
								   NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
								   NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
								   PromptArea::PA_LOW, NULL_STRING_ID);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// This method handles button down events.  The `pButton' parameter 
// is the button that was pressed down.  The `isByOperatorAction' flag 
// is TRUE if the given button was pressed by operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// If the accept button is pressed, all of the selected event buttons 
// shall be saved. $[TR01157] 
//
// If the clear button is pressed, all of the event buttons shall be
// deselected. $[TR01162] 
//
// $[TR01204] -  Whenever an Event Button is selected, an ACCEPT and 
//               a CLEAR button shall appear...
// $[TR01158] - If an Event Button is selected, touching it a second 
//              time shall cause the button to be deselected.
//---------------------------------------------------------------------
//@ PreCondition
// The 'isByOperatorAction' parameter must be TRUE and the 'pButton'
// parameter must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
EventSubScreen::buttonDownHappened(Button *pButton,
								   Boolean isByOperatorAction)
{
	CALL_TRACE("UpperOtherScreensSubScreen::buttonDownHappened(pButton, isByOperatorAction)");

#ifdef EVENTSUBSCREEN_DEBUG
	cout << "buttonDown" << endl;
#endif 
	CLASS_ASSERTION(isByOperatorAction);
	CLASS_PRE_CONDITION(pButton != NULL);

	AdjustPanel::TakePersistentFocus(NULL);		// Release focus

	if (pButton == &eventAcceptButton_)
	{

		// Clear the prompts, make an Accept sound, store the
		// events, and hide the event dialog container.
		Sound::Start(Sound::ACCEPT);

		// Clear Primary and Advisory prompts.
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
									   NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
									   NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW, NULL_STRING_ID);

		// Reset the number of buttons pressed.
		numEventButtonsPressed_ = 0;


		for (Uint8 idx = 0u; eventButtons_[idx] != NULL; idx++)
		{
			// Store only the selected event ids.
			if (eventButtons_[idx]->getButtonState() == Button::DOWN)
			{
				TrendDataMgr::PostEvent( (TrendEvents::EventId) idx );
			}
			// Set event button to up only if they were DOWN before
			// leave the FLAT ones alone
			if( eventButtons_[idx]->getButtonState() == Button::DOWN )
			{
				eventButtons_[idx]->setToUp();
			}
		}
		// Hide the event dialog containter.
		eventDialogCtr_.setShow(FALSE );
		eventAcceptButton_.setToUp();

	}
	else if (pButton == &eventClearButton_)
	{

		// Clear Primary and Advisory prompts.
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
									   NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
									   NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW, NULL_STRING_ID);

		// Reset the number of buttons pressed.
		numEventButtonsPressed_ = 0;

		for (Uint8 idx = 0u; eventButtons_[idx] != NULL; idx++)
		{
			// Set event button to up only if they were DOWN before
			// leave the FLAT ones alone
			if( eventButtons_[idx]->getButtonState() == Button::DOWN )
			{
				eventButtons_[idx]->setToUp();
			}
		}

		// Hide the event dialog containter.
		eventDialogCtr_.setShow(FALSE );
		eventClearButton_.setToUp();

	}
	else if (isEventButton_(pButton))
	{
		// Start a timer when an event button down occurs.
		// If the timer is up and there has been no user activity,
		// we shall cause all buttons to be deselected. $[TR01207]
		GuiTimerRegistrar::StartTimer(GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT,
									  this);

		// Show the event dialog container.
		eventDialogCtr_.setShow(TRUE );

		// Increase the number of Event button pressed.
		numEventButtonsPressed_++;

		// Only allow three Event buttons to be pressed down
		// at a time.
		if (numEventButtonsPressed_ > MAX_EVENT_BUTTON_PRESSED_DOWN)
		{
			numEventButtonsPressed_ = MAX_EVENT_BUTTON_PRESSED_DOWN;
			pButton->setToUp();
			Sound::Start(Sound::INVALID_ENTRY);

		}
		else
		{

			// Set Primary "Press ACCEPT if OK." prompt.
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
										   PromptArea::PA_LOW, 
										   PromptStrs::TREND_EVENT_COMPLETE_ADVISORY_S);
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, 
										   PromptArea::PA_LOW,
										   NULL_STRING_ID);
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
										   PromptArea::PA_LOW, NULL_STRING_ID);

		}

	}
	else if (pButton == &eventNextScreenButton_)
	{
		Uint8 idx;
		for (idx = 0u; idx < NUM_EVENT_BUTTONS_PER_PAGE; idx++)
		{
            // remove event buttons 
			removeDrawable(eventButtons_[idx] );
		}

		// create the next page starting with correct index
		for (idx = NUM_EVENT_BUTTONS_PER_PAGE; eventButtons_[idx] != NULL; idx++)
		{
			// Add event buttons to the subscreen
			addDrawable(eventButtons_[idx] );
		}
		
		eventPrevScreenButton_.setToUp();
		removeDrawable( &eventNextScreenButton_ );
		addDrawable( &eventPrevScreenButton_ );
	}
	else if (pButton == &eventPrevScreenButton_)
	{
		Uint8 idx;
		for (idx = NUM_EVENT_BUTTONS_PER_PAGE; eventButtons_[idx] != NULL; idx++)
		{
            // remove event buttons 
			removeDrawable(eventButtons_[idx] );
		}

		for (idx = 0u; idx < NUM_EVENT_BUTTONS_PER_PAGE; idx++)
		{
			// Add event buttons to the subscreen
			addDrawable(eventButtons_[idx] );
		}

		eventNextScreenButton_.setToUp();
		removeDrawable( &eventPrevScreenButton_ );
		addDrawable( &eventNextScreenButton_ );
	}
	else
	{
		// Should never get to this state.
		AUX_CLASS_ASSERTION_FAILURE((Uint32) pButton );
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// This method handles button up events. The `pButton' parameter 
// is the button that was set to up.  The `isByOperatorAction' flag 
// is TRUE if the given button was pressed by operator.
//---------------------------------------------------------------------
//@ PreCondition
// The 'isByOperatorAction' parameter must be TRUE and the 'pButton'
// parameter must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
EventSubScreen::buttonUpHappened(Button *pButton,
								 Boolean isByOperatorAction)
{
	CALL_TRACE("UpperOtherScreensSubScreen::buttonUpHappened(pButton, isByOperatorAction)");

#ifdef EVENTSUBSCREEN_DEBUG
	cout << "uphappened" << endl;
#endif 

	if (isByOperatorAction && isEventButton_(pButton))
	{

		// Decrease the number of Event button pressed.
		numEventButtonsPressed_ --;

		// If all of the event buttons are up,
		// Hide the event dialog container, and
		// clear the prompts.
		if (numEventButtonsPressed_ == 0)
		{

			// Hide the event dialog container.
			eventDialogCtr_.setShow(FALSE );

			// Clear Primary and Advisory prompts.
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
										   NULL_STRING_ID);
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
										   NULL_STRING_ID);
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
										   PromptArea::PA_LOW, NULL_STRING_ID);

		}

	}
	else
	{
	    // do nothing
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the Subscreen Setting Change timer runs out. 
// Passed the id of the timer.
//---------------------------------------------------------------------
//@ Implementation-Description
// Once the Subscreen Setting Change timeout occurs, and this subscreen
// is visible, reactivate this SubScreen.
// $[TR01207] - If an Event Button is selected, a 3 minute time-out 
//               without any user activity shall cause the button to 
//               be deselected. 
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the Subscreen Setting Change Timer Id.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
EventSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("EventSubScreen::timerEventHappened("
			   "GuiTimerId::GuiTimerIdType timerId)");

	AUX_CLASS_PRE_CONDITION(timerId == GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT,
	                        timerId);

	if ( isVisible() )
	{
		// reactivate this screen
		deactivate();
		activate();
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isEventButton_
//
//@ Interface-Description
//  If the given button pointer parameter is part of the list 
//  of event buttons, return true. Otherwise return false.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Return true if the given parameter is an event button, otherwise
//  return false.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean 
EventSubScreen::isEventButton_(Button * pButton)
{

	// Loop through the eventButton array to find a match
	// with the given parameter.
	for (Uint8 idx = 0u; eventButtons_[idx] != NULL; idx++)
	{
		if (eventButtons_[idx] == pButton)
		{
			return TRUE;
		}

	}

	return FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void
EventSubScreen::SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName,
						  const char*        pPredicate)  
{
	CALL_TRACE("EventSubScreen::SoftFault(softFaultID, "
			   "lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, EVENTSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}

