
//#############################################################################
//
//  NvDataVersions.h - Header file containg structures for non-volatile storage
//                     versions.
//
//#############################################################################

#ifndef NV_DATA_VERSIONS_H

#define NV_DATA_VERSIONS_H

static const char* NV_DATA_CALIBRATIONS_VERSION         =   "01.10";
static const char* NV_DATA_CONFIGURATIONS_VERSION       =   "01.04";
static const char* NV_DATA_SETTINGS_VERSION             =   "01.19";
static const char* NV_DATA_DUAL_VERSION                 =   "01.00";


#endif

