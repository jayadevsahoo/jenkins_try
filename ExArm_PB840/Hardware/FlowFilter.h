
//*****************************************************************************
//
//  PressureFilter.h - Header file for the PressureFilter_t class. This class
//      is used by the servo thread for filtering pressure data from Paw, Pint, etc.
//
//*****************************************************************************

#ifndef FLOW_FILTER_H

#define FLOW_FILTER_H

#include "BD_IO_Devices.hh"

class FlowFilter_t
{
    public:
        FlowFilter_t();

        ~FlowFilter_t();

        Flow_t FilterValue(Flow_t UnfilteredValue, uint16_t TimeConst);

        Flow_t GetFullyFilteredValue()
        {
            return FullyFilteredValue;
        }


    protected:
        Flow_t           FullyFilteredValue;
};


#endif

