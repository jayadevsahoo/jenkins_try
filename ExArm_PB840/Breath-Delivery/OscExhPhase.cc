#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OscExhPhase - Implements OSC exhalation phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class BreathPhase.  Virtual
//      methods are implemented to initialize the OscExhPhase and to perform OSC
//      Exhalation activities each BD cycle.
//---------------------------------------------------------------------
//@ Rationale
//      This class implements the algorithms for exhalation phase during OSC. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		The Psols, exhalation valve, and the damping gain are applied
//		zero command.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OscExhPhase.ccv   25.0.4.0   19 Nov 2013 13:59:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 022  By: syw     Date:  26-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//		Elimiante deleted requirement.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003 By: syw   Date: 16-Aug-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added method updateFlowControllerFlags.  Use FlowController::updatePsol( 0)
//			to close psols instead of Psol::updatePsol( 0).  Call
//			FlowController::newBreath().
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "OscExhPhase.hh"

#include "ValveRefs.hh"
#include "ControllersRefs.hh"

//@ Usage-Classes

#include "ExhalationValve.hh"
#include "Psol.hh"
#include "BD_IO_Devices.hh"
#include "FlowController.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OscExhPhase()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called with the phase type argument.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

OscExhPhase::OscExhPhase(void)
 : BreathPhase(BreathPhaseType::EXHALATION)	  	// $[TI1]
{
	CALL_TRACE("OscExhPhase::OscExhPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OscExhPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

OscExhPhase::~OscExhPhase(void)
{
	CALL_TRACE("OscExhPhase::~OscExhPhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called every BD cycle during an OSC exhalation phase. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		The flow controller flags are updated.  The exhalation valve and damping
//		gain are set to zero in newBreath() so no action is	required by these
//		two objects.  The psols are commanded to zero.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscExhPhase::newCycle(void)
{
	CALL_TRACE("OscExhPhase::newCycle(void)") ;
	
  	// $[TI1]
	updateFlowControllerFlags_() ;
	
    RAirFlowController.updatePsol( 0.0F) ;
   	RO2FlowController.updatePsol( 0.0F) ;

    elapsedTimeMs_ += CYCLE_TIME_MS ;	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called at the beginning of OscExhPhase.  
//---------------------------------------------------------------------
//@ Implementation-Description
//		The exhalation valve and damping gain are commanded to
//		zero.  The flow controllers are initialized via newBreath().
//		$[04206]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscExhPhase::newBreath( void)
{
	CALL_TRACE("newBreath( void)") ;

  	// $[TI1]
    RAirFlowController.newBreath() ;
   	RO2FlowController.newBreath() ;

	RExhalationValve.deenergize() ;

    elapsedTimeMs_ = 0 ;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
OscExhPhase::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, OSCEXHPHASE,
                          	 lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFlowControllerFlags_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to set the controller shutdown flag of the flow controller to
//		TRUE.  This method overloads the base class method.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set flags to true.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
OscExhPhase::updateFlowControllerFlags_( void)
{
	CALL_TRACE("OscExhPhase::updateFlowControllerFlags_( void)") ;

  	// $[TI1]
	RO2FlowController.setControllerShutdown( TRUE) ;
	RAirFlowController.setControllerShutdown( TRUE) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

