//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================
#ifndef __Barometer_H__
#define __Barometer_H__

#include "Sigma.hh"

/// @class Barometer
/// @detail Abstraction for Atmospheric Pressure Sensor. The actual implementation
/// (i.e. derived classes) would vary depending on the actual sensor being used. This
/// base class provides a common interface, and provides a standard correction for
/// value read from sensor.
class Barometer
{
public:
	/// @detail Get *corrected* pressure value from the sensor. To get
	/// uncorrected value, use GetPressureRaw() API. The calibration used
	/// to compute the correction required is generic, and can be used
	/// regardless of the sensor used. So this API is implemented at the
	/// base class. The correction applied is determined by the offset set
	/// through setOffset() API invocation.
	/// @return The *corrected* pressure reading in cmH2O
	Real32 getPressure() { return pressure_; }
	/// @detail This API is here for backward compatibility with 840-code, where
	/// the pressure sensor was a LinearSensor. It returns the same value as
	/// the GetPressure() API.
	Real32 getValue() { return getPressure(); }

	/// @detail Get *uncorrected* pressure value from the sensor. The actual
	/// implementation of this API depends on the hardware. Note that this API
	/// reads from the hardware, and hence can take longer than getPressure()
	/// API.
	/// @return The *uncorrected* pressure reading in cmH2O.
    virtual Real32 getPressureRaw() = 0;

	/// @detail Get pressure ADC count (Padc) from the sensor. This API
	/// is implemented for backward compatibility with 840's pressure sensor. This
	/// value by itself doesnt mean much, without temperature ADC count (Tadc). This
	/// API reads from hardware (i.e. it does not return a cached value).
	/// @return The pressure ADC count (Padc)
    virtual Uint16 getCount() = 0;

	/// @detail Returns the single instance of the atmospheric pressure sensor. 
	/// The actual type depends on the hardware, but the callers do not need to 
	/// know it.
	/// @return The single, global instance of Barometer. 
    static Barometer* getInstance();

	/// @detail The API sets the calibration value to be utilized for correcting
	/// the raw value read from the hardware. The calibration is done outside the
	/// Barometer class. The Barometer class just uses the 'offset' to add to the
	/// value read from the actual sensor. The 'offset' is expected to be in cmH2O.
	/// @param offset - specifies the offset to be used for correction.
	void setOffset( const Real32 offset);

	/// @detail Returns the current value of offset being used for correction.
	/// @return The offset being used for correction.
	Real32 getOffset( void) const ;

	//@detail Updates the cached pressure value, reading from hardware
	void updateValue();

protected:
	Barometer();
    virtual ~Barometer();

private:
    static Barometer* instance_;
	//Last pressure reading. This is cached every time a reading is made from
	//hardware. The getPressure() and getValue() APIs return this cached value, instead
	//of reading from the hardware, because the reading can disrupt timing.
	Real32 pressure_;
	//Offset used to correct the raw value. This offset is to be determined external
	//to this class (via a calibration).
	Real32 offset_;
};
#endif


