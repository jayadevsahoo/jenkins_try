#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ModeTrigger -  This class is an abstract base class for all
//              mode triggers the Breath Delivery system may need.
//---------------------------------------------------------------------
//@ Interface-Description
//    A Trigger may be enabled or disabled.  When enabled, and on the 
//    list registered with the ModeTriggerMediator, it notifies the
//    active BreathPhaseScheduler when the condition that it has been set
//    up to detect has occured.  Depending upon the trigger, it may also
//    notify  other objects when its condition is true.
//---------------------------------------------------------------------
//@ Rationale
//    There are many types of conditions in the breath delivery system 
//    that may trigger changes to the active mode of breathing. They 
//    may or may not cause changes to the delivery of breaths.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Trigger has a boolean state data member that is used to determine 
//     if the trigger is active. There are methods for setting the state 
//     (enabling and disabling the Trigger), and methods for requesting to
//     enable or disable the trigger. The method for checking the 
//     condition that the trigger is set up to detect is a pure virtual 
//     function, and must be defined for each derived trigger class.
//---------------------------------------------------------------------
//@ Fault-Handling
//     n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    This is an abstract base class for many types of  mode triggers.  
//    It can not be instanciated.  A ModeTrigger must belong to a list
//    in a BreathPhaseScheduler, and may belong on more than one list.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ModeTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ModeTrigger.hh"

//@ Usage-Classes
#include "BreathPhaseScheduler.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ModeTrigger 
//
//@ Interface-Description
//	This constructor takes trigger id as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Sets the two private data member to an initial state of FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ModeTrigger::ModeTrigger(const Trigger::TriggerId id)
: Trigger(id)
{
	CALL_TRACE("ModeTrigger(TriggerId id)");

	// $[TI1]
	isEnableRequested_ = FALSE;
	isDisableRequested_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ModeTrigger 
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ModeTrigger::~ModeTrigger()
{
  CALL_TRACE("~ModeTrigger()");
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
ModeTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, MODETRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerAction_
//
//@ Interface-Description
//  Called by determineState to do whatever actions are required,
//  depending on the trigger condition. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Check the trigger condition and if it is true then pass itself to 
//  the breath phase scheduler via relinquishControl. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ModeTrigger::triggerAction_(const Boolean trigCond)
{
  CALL_TRACE("triggerAction_(const Boolean trigCond)");

  if (trigCond)
  {
    // $[TI1]
    (BreathPhaseScheduler::GetCurrentScheduler()).relinquishControl(*this);
  }
  // $[TI2]
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

