#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SstFilterTest - SST expiratory filter test
//---------------------------------------------------------------------
//@ Interface-Description
//	This class implements the SST expiratory filter test, which checks
//	the expiratory filter for occlusions.
//
//	NOTE: this test is now called the "Expiratory Filter Test"
//---------------------------------------------------------------------
//@ Rationale
//	This test is required to check for expiratory filter occlusions.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() performs the expiratory filter test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SstFilterTest.ccv   25.0.4.0   19 Nov 2013 14:23:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011  By: syw    Date: 16-Feb-2000     DR Number: 5627
//  Project:  NeoMode
//  Description:
//		Changed EXH_FILTER_HIGH_FAILURE_LIMITS_ for neo to 3.0
//
//  Revision: 010  By: syw    Date: 02-Feb-2000     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Incorporation of NeoMode Project requirements.
//
//  Revision: 009  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 008  By:  quf    Date: 15-Jan-1998    DR Number: DCS 2720
//       Project:  Sigma (840)
//       Description:
//			Fixed test to use the secondary Pexp adc channel, added 5 sec
//			delay after flow stability and before sampling pressures to
//			for pressures to stabilize, added use of filtered pressures
//			to decrease residual noise.
//
//  Revision: 007  By:  quf    Date: 23-Oct-1997    DR Number: DCS 2105
//       Project:  Sigma (840)
//       Description:
//			Updated filter low delta P Failure and Alert and high delta P
//			Alert criteria.
//
//  Revision: 006  By:  quf    Date: 10-Oct-1997    DR Number: DCS 2262
//       Project:  Sigma (840)
//       Description:
//			Added comment to class-level Interface-Description indicating
//			alternate names for this test.
//
//  Revision: 005  By:  quf    Date: 05-Aug-1997    DR Number: DCS 2130
//       Project:  Sigma (840)
//       Description:
//            Moved total circuit resistance measurement from here to
//            the Circuit Resistance Test (InspResistanceTest.cc).
//
//  Revision: 004  By:  quf    Date: 14-Jul-1997    DR Number: DCS 2216
//       Project:  Sigma (840)
//       Description:
//            Added exp filter low delta-P Failure and Alert.
//
//  Revision: 003  By:  quf    Date: 24-Jun-1997    DR Number: DCS 2220
//       Project:  Sigma (840)
//       Description:
//            Changed circuit disconnect zero flow threshold to 1 lpm.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "SstFilterTest.hh"

//@ Usage-Classes

#include "Task.hh"

#include "SmConstants.hh"
#include "SmRefs.hh"
#include "SmFlowController.hh"
#include "SmExhValveController.hh"
#include "SmUtilities.hh"
#include "SmManager.hh"
#include "SmGasSupply.hh"

#include "MainSensorRefs.hh"
#include "ValveRefs.hh"
#include "PressureSensor.hh"
#include "Psol.hh"
#include "ExhFlowSensor.h"
#include "PatientCctTypeValue.hh"
#include "PhasedInContextHandle.hh"

#if defined(SIGMA_DEVELOPMENT)
# include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage


//@ Code...

// flow rate for filter occlusion test
static const Real32  EXH_FILTER_TEST_FLOWS_[PatientCctTypeValue::TOTAL_CIRCUIT_TYPES] =
	{
		60.0f,  // L/min -- ADULT circuit...
		60.0f,  // L/min -- PEDIATRIC circuit...
		30.0f   // L/min -- NEONATAL circuit...
	};

// expiratory compartment occlusion failure limit
static const Real32  EXH_COMPARTMENT_FAILURE_LIMITS_[PatientCctTypeValue::TOTAL_CIRCUIT_TYPES] =
	{
		6.0f,  // cmH2O -- ADULT circuit...
		6.0f,  // cmH2O -- PEDIATRIC circuit...
		3.0f   // cmH2O -- NEONATAL circuit...
	};

// expiratory compartment occlusion alert limit
static const Real32  EXH_COMPARTMENT_ALERT_LIMITS_[PatientCctTypeValue::TOTAL_CIRCUIT_TYPES] =
	{
		4.0f,  // cmH2O -- ADULT circuit...
		4.0f,  // cmH2O -- PEDIATRIC circuit...
		2.0f   // cmH2O -- NEONATAL circuit...
	};

// expiratory filter occlusion high failure limit
static const Real32  EXH_FILTER_HIGH_FAILURE_LIMITS_[PatientCctTypeValue::TOTAL_CIRCUIT_TYPES] =
	{		
		3.0f,  // cmH2O -- ADULT circuit...
		3.0f,  // cmH2O -- PEDIATRIC circuit...
		3.0f   // cmH2O -- NEONATAL circuit...
		
	};

// expiratory filter occlusion high alert limit
static const Real32  EXH_FILTER_HIGH_ALERT_LIMITS_[PatientCctTypeValue::TOTAL_CIRCUIT_TYPES] =
	{
		2.4f,  // cmH2O -- ADULT circuit...
		2.4f,  // cmH2O -- PEDIATRIC circuit...
		9.9f   // cmH2O -- NEONATAL circuit (no alert for NEONATAL; failure only)...
		
	};

// expiratory filter occlusion low failure limit
static const Real32 EXH_FILTER_LOW_FAILURE_LIMIT_ = 0.1f;  // cm H2O

// expiratory filter occlusion low alert limit
static const Real32 EXH_FILTER_LOW_ALERT_LIMITS_[PatientCctTypeValue::TOTAL_CIRCUIT_TYPES] =
	{
		0.4f,  // cmH2O -- ADULT circuit...
		0.4f,  // cmH2O -- PEDIATRIC circuit...
		0.0f   // cmH2O -- NEONATAL circuit (no alert for NEONATAL; failure only)...
	};

// 0 lpm exh flow measurement margin
static const Real32 ZERO_EXH_FLOW_MARGIN_ = 1.0f;  // cm H2O


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: SstFilterTest  [Default Constructor]
//
//@ Interface-Description
//  	Constructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=======================================================================
SstFilterTest::SstFilterTest( void)
{
	CALL_TRACE("SstFilterTest::SstFilterTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~SstFilterTest  [Destructor]
//
//@ Interface-Description
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
SstFilterTest::~SstFilterTest( void)
{
	CALL_TRACE("SstFilterTest::~SstFilterTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runTest
//
//@ Interface-Description
//	This method implements the SST expiratory filter test.  No arguments,
//	no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	$[10013]
//	Prompt operator to disconnect circuit from the expiratory filter and
//	and verify it was disconnected.
//	Measure pressure drop across circuit using the test flow.
//	Prompt operator to connect circuit to the expiratory filter and verify
//	it was reconnected.
//	Measure pressure drop across circuit using the test flow.
//	From these measurements, calculate the pressure drop across the
//	expiratory filter then compare this value to the failure and alert
//	occlusion criteria.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================
void
SstFilterTest::runTest( void)
{
	CALL_TRACE("SstFilterTest::runTest( void)") ;

	Boolean noFailure = TRUE;
	Boolean operatorExit = FALSE;

	Real32 initialInspPressure = 0;
	Real32 initialExhPressure;
	Real32 finalInspPressure;
	Real32 finalExhPressure;
	Real32 exhFlow;

	if (!RSmUtilities.messagePrompt(
		SmPromptId::DISCNT_TUBE_BFR_EXH_FLTR_PROMPT))
	{
	// $[TI1]
		// EXIT button wa pressed
		operatorExit = TRUE;
	}
	// $[TI2]
	// implied else: ACCEPT key was pressed

	if (!operatorExit)
	{
	// $[TI3]
		// Test phase 1: measure insp pressure with circuit
		// disconnected from exh filter

		RSmUtilities.autozeroPressureXducers();

		if ( !measureExhFilterResistance_( DISCONNECTED,
				initialInspPressure, initialExhPressure, exhFlow) )
		{

		// $[TI3.1]
			// Unable to establish flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_1);

			noFailure = FALSE;
		}
		else if (exhFlow > ZERO_EXH_FLOW_MARGIN_)
		{
		// $[TI3.2]
			// exh flow != 0 lpm + margin,
			// probably operator error
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_6);

			noFailure = FALSE;
		}
		// $[TI3.3]
		// implied else: disconnected phase passed

#ifdef SIGMA_DEBUG
	printf("initial insp pressure = %f\n", initialInspPressure);
	printf("initial exh pressure = %f\n", initialExhPressure);
#endif
	}
	// $[TI4]
	// implied else: operator hit exit button

	if (noFailure && !operatorExit)
	{
	// $[TI5]
		if (!RSmUtilities.messagePrompt(
			SmPromptId::CONCT_TUBE_BFR_EXH_FLTR_PROMPT))
		{
		// $[TI5.1]
			// EXIT button wa pressed
			operatorExit = TRUE;
		}
		// $[TI5.2]
		// implied else: ACCEPT key was pressed
	}
	// $[TI6]
	// implied else

	if (noFailure && !operatorExit)
	{
	// $[TI7]
		// Test phase 2: measure insp and exh pressure with circuit
		// reconnected to exh filter
		const DiscreteValue  CIRCUIT_TYPE =
		  PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

		if (!measureExhFilterResistance_( CONNECTED, finalInspPressure,
										  finalExhPressure, exhFlow) )
		{
		// $[TI7.1]
			// Unable to establish flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_1);

			noFailure = FALSE;
		}
		else if ( exhFlow < EXH_FILTER_TEST_FLOWS_[CIRCUIT_TYPE] * 0.8 )
		{
		// $[TI7.2]
			// exh flow != insp flow - margin,
			// either operator error or
			// circuit severely occluded
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_7);

			noFailure = FALSE;
		}
		else if (finalExhPressure > EXH_COMPARTMENT_FAILURE_LIMITS_[CIRCUIT_TYPE])
		{
		// $[TI7.3]
			// Exhalation compartment occlusion FAILURE
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_3);

			noFailure = FALSE;
		}
		else
		{
		// $[TI7.4]
			if (finalExhPressure > EXH_COMPARTMENT_ALERT_LIMITS_[CIRCUIT_TYPE])
			{
			// $[TI7.4.1]
				// Exhalation compartment occlusion ALERT
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT,
					SmStatusId::CONDITION_8);
			}
			// $[TI7.4.2]
			// implied else

			Real32 exhFilterDeltaP = finalInspPressure -
							finalExhPressure - initialInspPressure;


			//  Send the exh filter delta to service data
			RSmManager.sendTestPointToServiceData( exhFilterDeltaP);

			if (exhFilterDeltaP > EXH_FILTER_HIGH_FAILURE_LIMITS_[CIRCUIT_TYPE])
			{
			// $[TI7.4.3]
				// Exhalation filter occlusion FAILURE
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_4);

				noFailure = FALSE;
			}
			else if (exhFilterDeltaP > EXH_FILTER_HIGH_ALERT_LIMITS_[CIRCUIT_TYPE])
			{
			// $[TI7.4.4]
				// Exhalation filter occlusion ALERT
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT,
					SmStatusId::CONDITION_5);
			}
			else if (exhFilterDeltaP < EXH_FILTER_LOW_FAILURE_LIMIT_)
			{
			// $[TI7.4.5]
				// Exhalation filter low delta-P FAILURE
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_10);

				noFailure = FALSE;
			}
			else if (exhFilterDeltaP < EXH_FILTER_LOW_ALERT_LIMITS_[CIRCUIT_TYPE])
			{
			// $[TI7.4.6]
				// Exhalation filter low delta-P ALERT
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT,
					SmStatusId::CONDITION_11);
			}
			// $[TI7.4.7]
			// implied else: connected phase passed

#ifdef SIGMA_DEBUG
	printf("final insp pressure = %f\n", finalInspPressure);
	printf("final exh pressure = %f\n", finalExhPressure);
#endif
		}
		// end else
	}
	// $[TI8]
	// implied else

// Note: TI's 9, 9.1, 9.2, 9.3, and 10 were deleted.

	if (operatorExit)
	{
	// $[TI11]
		RSmManager.sendOperatorExitToServiceData();
	}
	// $[TI12]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SstFilterTest::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("SstFilterTest::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, SSTFILTERTEST,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: measureExhFilterResistance_
//
//@ Interface-Description
//	This method is used to measure the pressure drop along the circuit
//	and to verify that the appropriate prompt (to connect or disconnect
//	circuit from filter) was followed.  4 arguments: test phase (disconnected
//	or connected), inspiratory pressure (passed as reference), expiratory
//	pressure (passed as reference), and expiratory flow (passed as reference).
//	The last three parameters are actually used as additional return values.
//
//	Returns result of the flow stability test: TRUE = flow stability was
//	reached, FALSE = flow stability not reached.
//	Else
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Establish the test flow using either gas, and if flow stability can't
//	be reached within the timeout, return FALSE.
//	Else, read and store the insp and exp pressures and exp flow and return
//	TRUE.
//-----------------------------------------------------------------------
//@ PreCondition
//	testPhase == CONNECTED or DISCONNECTED
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================
Boolean
SstFilterTest::measureExhFilterResistance_ ( TestPhase testPhase,
											 Real32& inspPressure,
											 Real32& exhPressure,
											 Real32& exhFlow )
{
	CALL_TRACE("SstFilterTest::measureExhFilterResistance_ (\
		TestPhase testPhase, Real32& inspPressure, Real32& exhPressure, \
		Real32& exhFlow)");

	CLASS_PRE_CONDITION( testPhase == CONNECTED ||
						 testPhase == DISCONNECTED );

	const DiscreteValue  CIRCUIT_TYPE =
		  PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	Boolean flowEstablished;

    RSmFlowController.establishFlow(
    			EXH_FILTER_TEST_FLOWS_[CIRCUIT_TYPE], DELIVERY, EITHER);

	if (RSmFlowController.waitForStableFlow())
	{
	// $[TI1]
		flowEstablished = TRUE;

		// delay to reach pressure equilbrium
		Task::Delay(5);

		// read alpha-filtered pressures to reduce noise
		inspPressure = RInspPressureSensor.getFilteredValue();
		// use the secondary Pexh channel to eliminate Pinsp-Pexh coupling
		exhPressure = RSecondExhPressureSensor.getFilteredValue();

		if (RSmFlowController.gasUsed() == AIR)
		{
		// $[TI1.1]
			exhFlow = RExhFlowSensor.getValue();
		}
		else
		{
		// $[TI1.2]
// E600 BDIO            exhFlow = RExhO2FlowSensor.getValue();
			exhFlow = RExhFlowSensor.getValue();
		}
		// end else

		if (testPhase == DISCONNECTED)
		{
		// $[TI1.3]
			// Send the inspiratory pressure to service data
			RSmManager.sendTestPointToServiceData
				(SmDataId::SST_FILTER_INSPIRATORY_PRESSURE1, inspPressure);

#ifdef SIGMA_DEBUG
	printf("initial insp test pressure = %f\n", inspPressure);
	printf("initial exh test pressure = %f\n", exhPressure);
#endif  // SIGMA_DEBUG
		}
		else
		{
		// $[TI1.4]
			// testPhase must be CONNECTED

			// Send the inspiratory and exhalation pressures to service data
			RSmManager.sendTestPointToServiceData
				(SmDataId::SST_FILTER_INSPIRATORY_PRESSURE2, inspPressure);
			RSmManager.sendTestPointToServiceData
				(SmDataId::SST_FILTER_EXHALATION_PRESSURE, exhPressure);

#ifdef SIGMA_DEBUG
	printf("final insp test pressure = %f\n", inspPressure);
	printf("final exh test pressure  = %f\n", exhPressure);
#endif  // SIGMA_DEBUG

		}
		// end else

#ifdef SIGMA_DEBUG
	printf("%s phase: exh flow = %f\n",
		(testPhase == DISCONNECTED) ? "DISCONNECTED" : "CONNECTED", exhFlow);
#endif  // SIGMA_DEBUG

	}
	else
	{
	// $[TI2]
		flowEstablished = FALSE;
	}
	// end else

	RSmFlowController.stopFlowController();

	return( flowEstablished);
}
