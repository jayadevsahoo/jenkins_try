#include "StdAfx.h"
#include "Parser.h"
#include <stdlib.h>
#include <stdio.h>

const CHAR* Parser::MESSAGE_ROOT = "m";
const CHAR* Parser::HEADER = "h";
const CHAR* Parser::ACK_NAK_RESP = "ack_nak_resp";
const CHAR* Parser::QUERY_XY_RESP = "query_xy_resp";
const CHAR* Parser::QUERY_WH_RESP = "query_wh_resp";
const CHAR* Parser::GET_PROPERTIES_RESP = "get_properties_resp";
const CHAR* Parser::GET_OBJECT_VALUE_RESP = "get_object_value_resp";
const CHAR* Parser::GET_FOCUS_RESP = "get_focus_resp";
const CHAR* Parser::UI_OUTPUT_RESP = "ui_output_resp";
const CHAR* Parser::SCREEN_CAPTURE_BUFFER_INFO_RESP = "screen_capture_buffer_info_resp";
const CHAR* Parser::VENT_INFO_RESP = "vent_info_resp";
const CHAR* Parser::ALTIA_POLL_EVENT_RESP = "altia_poll_event_resp";
const CHAR* Parser::QUERY_SCREENSHOT_PATH_RESP = "query_sc_ftp_addr_resp";


Parser::Parser(void) :
	transactionId(0)
{
}

Parser::~Parser(void)
{
}

//------------------------------------------------------------------------------
// ParseCmd
//------------------------------------------------------------------------------
INT Parser::ParseCmd(const CHAR* buffer,
                          INT maxLen,
                          INT startIndex,
                          QGuiIOActionRespEvent& action)
{
	action.responseType = QGuiIOActionRespEvent::INVALID_RESPONSE_TYPE;

    // set all defaults
	m_parseErr = false;
    INT nextIndex = -1;

    if (buffer == NULL)
        return nextIndex;

    const CHAR* pStart = &buffer[startIndex];
    const CHAR* pEnd = &buffer[startIndex];

    // search for '<m>'
    pStart = strstr(pStart, "<m>");
    if (pStart == NULL)
    	return nextIndex;

    // search for '</m>' from where we found '<m>'
    pEnd = strstr(pStart, "</m>");
    if (pEnd == NULL)
    	return nextIndex;

    // sanity check
    INT msg_len = (pEnd - pStart + 4);
    nextIndex = pEnd - &buffer[startIndex] + startIndex + 4;
    if (msg_len > MAX_MSG_LENGTH)
    {
        // valid token, but too long for us to process
        return nextIndex;
    }

    // copy message into local buffer for parsing
	memset(m_msg, 0, MAX_MSG_LENGTH);
    memcpy(m_msg, pStart, msg_len);

    // give message to XML parser
    m_doc.Clear();
	m_doc.Parse(m_msg);

	// get the XML fragment root element
	TiXmlElement* ele = m_doc.RootElement();
	if (!ele)
		return nextIndex;

	// element m must start each XML message
	if (strcmp(ele->Value(), MESSAGE_ROOT) != 0)
		return nextIndex;

	// parse the message header
	ele = ParseHeader(ele, action.header);
	if (!ele)
		return nextIndex;

	// get the next element after the header
	ele = ele->NextSiblingElement();
	if (!ele)
		return nextIndex;

	// based upon the next element value, parse the element children
	if (strcmp(ele->Value(), ACK_NAK_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::ACK_NAK_RESP;
		ele = ParseAckNakResp(ele, action.msg.ackNakResp);
	}
	else if (strcmp(ele->Value(), QUERY_XY_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::QUERY_XY_RESP;
		ele = ParseQueryXYResp(ele, action.msg.queryXYResp);
	}
	else if (strcmp(ele->Value(), QUERY_WH_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::QUERY_WH_RESP;
		ele = ParseQueryWHResp(ele, action.msg.queryWHResp);
	}
	else if (strcmp(ele->Value(), GET_PROPERTIES_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::GET_PROPERTIES_RESP;
		ele = ParseGetPropertiesResp(ele, action.msg.getPropertiesResp);
	}
	else if (strcmp(ele->Value(), GET_OBJECT_VALUE_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::GET_OBJECT_VALUE_RESP;
		ele = ParseGetObjectValueResp(ele, action.msg.getObjectValueResp);
	}
	else if (strcmp(ele->Value(), GET_FOCUS_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::GET_FOCUS_RESP;
		ele = ParseGetFocusResp(ele, action.msg.getFocusResp);
	}
	else if (strcmp(ele->Value(), UI_OUTPUT_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::UI_OUTPUT_RESP;
		ele = ParseUIOutputResp(ele, action.msg.uiOutputResp);
	}
	else if (strcmp(ele->Value(), SCREEN_CAPTURE_BUFFER_INFO_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::SCREEN_CAPTURE_BUFFER_INFO_RESP;
		ele = ParseScreenCaptureBufferInfoResp(ele, action.msg.screenCaptureBufferInfoResp);
	}
	else if (strcmp(ele->Value(), VENT_INFO_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::VENT_INFO_RESP;
		ele = ParseVentInfoResp(ele, action.msg.ventInfoResp);
	}
	else if (strcmp(ele->Value(), ALTIA_POLL_EVENT_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::ALTIA_POLL_EVENT_RESP;
		ele = ParseAltiaPollEventResp(ele, action.msg.altiaPollEventResp);
	}
	else if (strcmp(ele->Value(), QUERY_SCREENSHOT_PATH_RESP) == 0)
	{
		action.responseType = QGuiIOActionRespEvent::QUERY_SCREENSHOT_PATH_RESP;
		ele = ParseQueryScreenshotPathResp(ele, action.msg.queryScreenshotPathResp);
	}
	/*else if (strcmp(ele->Value(), BYE) == 0)
	{
	}*/
	else
	{
		return -1;
	}

	// return invalid action if any parse routine detected an error
	if (m_parseErr)
		return -1;

    return nextIndex;
}

//------------------------------------------------------------------------------
// ParseHeader
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseHeader(TiXmlElement* ele, GuiIOMsgHeader& s)
{
	// find the h (header) element
	ele = ele->FirstChildElement(HEADER);
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}

	// extract header attributes
	if (ele->QueryIntAttribute( "t", &s.transactionId ) != TIXML_SUCCESS)
		m_parseErr = true;
	if (ele->QueryIntAttribute( "s", &s.seconds ) != TIXML_SUCCESS)
		m_parseErr = true;
	if (ele->QueryIntAttribute( "m", &s.microseconds ) != TIXML_SUCCESS)
		m_parseErr = true;

	return ele;
}

//------------------------------------------------------------------------------
// ParseAckNakResp
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseAckNakResp(TiXmlElement* ele, GuiIOAckNakResp& s)
{
	ele = ele->FirstChildElement("error");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.error) == 0)
		m_parseErr = true;

	return ele;
}

//------------------------------------------------------------------------------
// ParseQueryXYResp
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseQueryXYResp(TiXmlElement* ele, GuiIOQueryXYResp& s)
{
	ele = ele->FirstChildElement("object_id");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.objectId, ele->GetText());

	ele = ele->NextSiblingElement("x");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.x) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("y");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.y) == 0)
		m_parseErr = true;

	return ele;
}

//------------------------------------------------------------------------------
// ParseQueryWHResp
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseQueryWHResp(TiXmlElement* ele, GuiIOQueryWHResp& s)
{
	ele = ele->FirstChildElement("object_id");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.objectId, ele->GetText());

	ele = ele->NextSiblingElement("w");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.w) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("h");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.h) == 0)
		m_parseErr = true;

	return ele;
}

//------------------------------------------------------------------------------
// ParseGetPropertiesResp
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseGetPropertiesResp(TiXmlElement* ele, GuiIOGetPropertiesResp& s)
{
	ele = ele->FirstChildElement("object_id");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.objectId, ele->GetText());

	ele = ele->NextSiblingElement("enabled");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.enabled) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("viewable");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.viewable) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("activated");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.activated) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("state");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.state) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("adornment");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.adornment) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("blink");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.blink) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("can_focus");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.canFocus) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("row");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.row) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("col");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.col) == 0)
		m_parseErr = true;

	return ele;
}

//------------------------------------------------------------------------------
// ParseGetObjectValueResp
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseGetObjectValueResp(TiXmlElement* ele, GuiIOGetObjectValueResp& s)
{
	ele = ele->FirstChildElement("object_id");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.objectId, ele->GetText());

	ele = ele->NextSiblingElement("value_str");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.value, ele->GetText());

	ele = ele->NextSiblingElement("row");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.row) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("col");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.col) == 0)
		m_parseErr = true;
	return ele;
}

//------------------------------------------------------------------------------
// ParseGetFocusResp
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseGetFocusResp(TiXmlElement* ele, GuiIOGetFocusResp& s)
{
	ele = ele->FirstChildElement("object_id");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.objectId, ele->GetText());
	return ele;
}

//------------------------------------------------------------------------------
// ParseUIOutputResp
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseUIOutputResp(TiXmlElement* ele, GuiIOUIOutputResp& s)
{
	ele = ele->FirstChildElement("object_id");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.objectId, ele->GetText());

	ele = ele->NextSiblingElement("value");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.value, ele->GetText());

	ele = ele->NextSiblingElement("output_type");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.outputType) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("state");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.state) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("state_2");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.state2) == 0)
		m_parseErr = true;
	return ele;
}

//------------------------------------------------------------------------------
// ParseScreenCaptureBufferInfoResp
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseScreenCaptureBufferInfoResp(TiXmlElement* ele, GuiIOScreenCaptureBufferInfoResp& s)
{
	ele = ele->FirstChildElement("buffer_size");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.bufferSize) == 0)
		m_parseErr = true;
	return ele;
}

//------------------------------------------------------------------------------
// ParseVentInfoResp
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseVentInfoResp(TiXmlElement* ele, GuiIOVentInfoResp& s)
{
	ele = ele->FirstChildElement("gui_software_ver");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.guiSoftwareVer, ele->GetText());

	ele = ele->NextSiblingElement("bd_software_ver");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.bdSoftwareVer, ele->GetText());

	ele = ele->NextSiblingElement("serial_num");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.serialNum, ele->GetText());

	ele = ele->NextSiblingElement("language");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.language) == 0)
		m_parseErr = true;

	return ele;
}

//------------------------------------------------------------------------------
// ParseAltiaPollEventResp
//------------------------------------------------------------------------------
TiXmlElement* Parser::ParseAltiaPollEventResp(TiXmlElement* ele, GuiIOAltiaPollEventResp& s)
{
	ele = ele->FirstChildElement("event_name");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	strcpy_s(s.eventName, ele->GetText());

	ele = ele->NextSiblingElement("event_value");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.eventValue) == 0)
		m_parseErr = true;
	return ele;
}

TiXmlElement* Parser::ParseQueryScreenshotPathResp(TiXmlElement* ele, GuiIOQueryScreenshotPathResp& s)
{
	ele = ele->FirstChildElement("index");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}
	if (sscanf_s(ele->GetText(), "%d", &s.index) == 0)
		m_parseErr = true;

	ele = ele->NextSiblingElement("ftp_addr");
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}

	if ( ele->GetText() )
	{
		strcpy_s(s.address, ele->GetText());
	}
	else
	{
		memset( s.address, 0, sizeof( s.address ) );
	}

	return ele;
}

INT Parser::BuildSimulateTouch(int x, int y)
{
	INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<push_xy>"
				"<p1>%d</p1><p2>%d</p2>"		// x, y
			"</push_xy>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		x, y);

	return transactionId;
}

INT Parser::BuildClearScreenshots()
{
	INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<clear_screens>"
			"</clear_screens>"
		"</m>",
		transactionId,
		seconds,
		microseconds);

	return transactionId;
}

INT Parser::BuildQueryScreenshotPath(int index)
{
	INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<query_sc_ftp_addr>"
				"<p1>%d</p1>"		// index
			"</query_sc_ftp_addr>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		index);

	return transactionId;
}

INT Parser::BuildQueryXY(const CHAR* objectId)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<query_xy>"
				"<object_id>%s</object_id>"
			"</query_xy>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		objectId);

	return transactionId;
}

INT Parser::BuildQueryWH(const CHAR* objectId)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<query_wh>"
				"<object_id>%s</object_id>"
			"</query_wh>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		objectId);

	return transactionId; 
}

INT Parser::BuildGetProperties(const CHAR* objectId, INT row, INT col)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<get_properties>"
				"<object_id>%s</object_id>"
				"<row>%d</row>"
				"<col>%d</col>"
			"</get_properties>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		objectId,
		row,
		col);

	return transactionId;
}

INT Parser::BuildGetObjectValue(const CHAR* objectId, INT row, INT col)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<get_object_value>"
				"<object_id>%s</object_id>"
				"<row>%d</row>"
				"<col>%d</col>"
			"</get_object_value>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		objectId,
		row,
		col);

	return transactionId;
}

INT Parser::BuildGetFocus()
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<get_focus>"
			"</get_focus>"
		"</m>",
		transactionId,
		seconds,
		microseconds);

	return transactionId;
}

INT Parser::BuildMousePos(int x, int y)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<mouse_pos>"
				"<p1>%d</p1><p2>%d</p2>"
			"</mouse_pos>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		x, 
		y);

	return transactionId;
}

INT Parser::BuildLButtonDown(int x, int y)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<l_button_down>"
				"<p1>%d</p1><p2>%d</p2>"
			"</l_button_down>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		x, 
		y);

	return transactionId;
}

INT Parser::BuildLButtonUp(int x, int y)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<l_button_up>"
				"<p1>%d</p1><p2>%d</p2>"
			"</l_button_up>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		x,
		y);

	return transactionId;
}

INT Parser::BuildButtonClick(int x, int y)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<button_click>"
				"<p1>%d</p1><p2>%d</p2>"
			"</button_click>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		x,
		y);

	return transactionId;
}

INT Parser::BuildUIOutputOnOff(bool onOff)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<ui_output_on_off>"
				"<on_off>%d</on_off>"
			"</ui_output_on_off>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		onOff);

	return transactionId;
}

INT Parser::BuildCursor(bool onOff)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<cursor>"
				"<on_off>%d</on_off>"
			"</cursor>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		onOff);

	return transactionId;
}

INT Parser::BuildScreenCapture(const CHAR* fileName)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<screen_capture>"
				"<file_name>%s</file_name>"
			"</screen_capture>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		fileName);

	return transactionId;
}

INT Parser::BuildScreenCaptureBufferInfo()
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<screen_capture_buffer_info>"
			"</screen_capture_buffer_info>"
		"</m>",
		transactionId,
		seconds,
		microseconds);

	return transactionId;
}

INT Parser::BuildSimulateBDAlarm(const CHAR* alarmId, const CHAR* condAugOptions, const CHAR* depenAugOptions)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<simulate_bd_alarm>"
				"<alarm_id>%s</alarm_id>"
				"<cond_aug>%s</cond_aug>"
				"<depen_aug>%s</depen_aug>"
			"</simulate_bd_alarm>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		alarmId,
		condAugOptions,
		depenAugOptions);

	return transactionId;
}

INT Parser::BuildVentInfo()
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<vent_info>"
			"</vent_info>"
		"</m>",
		transactionId,
		seconds,
		microseconds);

	return transactionId;
}

INT Parser::BuildBezelKeyPress(int keyId, bool onOff)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<bezel_key_press>"
				"<on_off>%d</on_off>"
				"<key_id>%d</key_id>"
			"</bezel_key_press>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		onOff,
		keyId);

	return transactionId;
}

INT Parser::BuildKnob(int delta)
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<knob>"
				"<p1>%d</p1>"
			"</knob>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		delta);

	return transactionId;
}

INT Parser::BuildAltiaPollEvent(const CHAR* eventName)
{
	INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<altia_poll_event>"
				"<event_name>%s</event_name>"
			"</altia_poll_event>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		eventName);

	return transactionId;
}

INT Parser::BuildAltiaSendEvent(const CHAR* eventName, int eventValue)
{
	INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<altia_send_event>"
				"<event_name>%s</event_name>"
				"<event_value>%d</event_value>"
			"</altia_send_event>"
		"</m>",
		transactionId,
		seconds,
		microseconds,
		eventName,
		eventValue);

	return transactionId;
}

INT Parser::BuildBye()
{
    INT seconds = 0;
    INT microseconds = 0;
	INT transactionId = rand();
	_snprintf_s(m_msg, MAX_MSG_LENGTH,
		"<m>"
			"<h t='%d' s='%d' m='%d' />"
			"<bye>"
			"</bye>"
		"</m>",
		transactionId,
		seconds,
		microseconds);

	return transactionId;
}