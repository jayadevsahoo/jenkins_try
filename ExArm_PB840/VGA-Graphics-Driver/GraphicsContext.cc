#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: GraphicsContext -- Graphics Context for VGA Library Functions
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides management of the clipping zones during execution.
//  Methods are provided for setting the clipping region, as well as, querying
//  the current clipping axes. It contains graphics parameters for the 
//  VGA drawing routines.
//---------------------------------------------------------------------
//@ Rationale
//  Contains a drawing request graphics parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class provides a set of private, special-purpose clipping methods,
//  including 'clipLine()' and 'calcClippingCode_()' which are used for
//  line clipping.  (The Sutherland-Cohen algorithm is implemented for
//  clipping a line.)
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/GraphicsContext.ccv   25.0.4.0   19 Nov 2013 14:37:28   pvcs  $
//
//@ Modification-Log
//
// 
//  Revision: 002   By: rpr    Date:  29-Jul-2010    DCS Number: 6658
//  Project: PROX
//  Description:
//	Removed assertions when trying deciding to clip a line.  Returns not a valid
//  line instead of asserting.
//
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//=====================================================================  

#include "GraphicsContext.hh"
#include "DisplayContext.hh"

//@ Usage-Classes
//@ End-Usage

//=====================================================================  
//
//  Public Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GraphicsContext [constructor]
//
//@ Interface-Description
//  Contructor for the GraphicsContext class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
GraphicsContext::GraphicsContext()
  : clipX0_(0)
  , clipY0_(0)
  , clipX1_(0)
  , clipY1_(0)
  , lineWidth_(1)
  , foregroundColor_(0)
{
  	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GraphicsContext [destructor]
//
//@ Interface-Description
//  Destructor for the GraphicsContext class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
GraphicsContext::~GraphicsContext()
{
  	// no code
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setClip
//
//@ Interface-Description
//  Sets the clipping boundries in this graphics context for the 
//  specified display context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

void
GraphicsContext::setClip(const DisplayContext & rDc, Int x0, Int y0, Int x1, Int y1)
{
  Int   tmp;

  if (x0 > x1)    // ensure that the coordinates are ordered ascending...
  {   // $[TI1] -- swap 'x0' and 'x1'...
    tmp = x0;
    x0 = x1;
    x1 = tmp;
  }   // $[TI2] -- no swapping needed...

  if (y0 > y1)
  {   // $[TI3] -- swap 'y0' and 'y1'...
    tmp = y0;
    y0 = y1;
    y1 = tmp;
  }   // $[TI4] -- no swapping needed...

  Uint MAX_X = rDc.getWidth() - 1;
  Uint MAX_Y = rDc.getHeight() - 1;

  // adjust the boundaries if there is a problem (ie: clip)
  if (x1 > (Uint)MAX_X) 
  {   // $[TI5]
    x1 = MAX_X;
  }   // $[TI6]

  if (y1 > (Uint)MAX_Y) 
  {   // $[TI7]
    y1 = MAX_Y;
  }   // $[TI8]

  if (x0 < 0)
  {   // $[TI9]
    x0 = 0;
  }   // $[TI10]

  if (y0 < 0)
  {   // $[TI11]
    y0 = 0;
  }   // $[TI12]

  clipX0_ = x0;
  clipY0_ = y0;
  clipX1_ = x1;
  clipY1_ = y1;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  clipLine
//
//@ Interface-Description
//  Clips a line and resets the endpoint for the new line.  Returns 'TRUE'
//  if a valid line is found, otherwise 'FALSE' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Implements a Sutherland-Cohen algorithm, which efficiently compares
//  the endpoints of the line segment with the boundaries of the rectagular
//  "clipping" region before computing the intersection points.  Little
//  computation is performed for lines that don't need to be clipped.  
//
//  The actual Sutherland-Cohen algorithm calls for the processing of the
//  line's endpoints to be with floating-point math.  For reasons of
//  execution optimization, integer math is used, instead.  However, using
//  integer math does reduce symmetrical equivalence of a line.  For example,
//  if a line's endpoints are swapped, it may not occupy the exact same
//  set of pixels as its original endpoints resulted in.  The differences
//  are trivial, and should never be a problem in our product.
//---------------------------------------------------------------------
//@ PreCondition
//  (*pY0 != *pY1  &&  *pX0 != *pX1)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================  

Boolean
GraphicsContext::clipLine(Int* pX0, Int* pY0, Int* pX1, Int* pY1) const
{
  Int  outCode0, outCode1;

  // Initialize the outCodes, relative to the clipping rectangle...
  outCode0 = calcClippingCode_(*pX0, *pY0);
  outCode1 = calcClippingCode_(*pX1, *pY1);

  // keep iterating until either both points are completely inside the clipping
  // region, or both points are completely outside the clipping region...
  while ((outCode0 | outCode1) != 0  &&		// NOT completely inside...
	 (outCode0 & outCode1) == 0)		// NOT completely outside...
  {   // $[TI1]
    // can't calculate the slope in one step, because of limitations with
    // integer math (e.g., ((200 - 150) / (200 - 50)) == 0)...
    const Int  DELTA_Y = (*pY1 - *pY0);
    const Int  DELTA_X = (*pX1 - *pX0);

    const Int  OUT_CODE = (outCode0 != 0) ? outCode0    // $[TI1.1]
					  : outCode1;   // $[TI1.2]
    Int  x, y;

    //-----------------------------------------------------------------
    // NOTE:  in the formulas below, it is imperative that the full
    //        product is done before the division is done, because of
    //        of the use of integer math (i.e., "3 * (2 / 3) != (3 * 2) / 3"
    //        using integer math, though it is with floating-point math).
    //-----------------------------------------------------------------

    if ((OUT_CODE & BELOW_MASK) != 0)
    {   // $[TI1.3] -- clip below...
		// protect from divide by zero valid line not found
		if (DELTA_Y == 0)
		{
			return (FALSE);
		}
      x = *pX0 + (((clipY1_ - *pY0) * DELTA_X) / DELTA_Y);
      y = clipY1_;
    }
    else if ((OUT_CODE & ABOVE_MASK) != 0)
    {   // $[TI1.4] -- clip above...
		// protect from divide by zero valid line not found
		if (DELTA_Y == 0)
		{
			return (FALSE);
		}
      x = *pX0 + (((clipY0_ - *pY0) * DELTA_X) / DELTA_Y);
      y = clipY0_;
    }
    else if ((OUT_CODE & RIGHT_MASK) != 0)
    {   // $[TI1.5] -- clip right...
		// protect from divide by zero valid line not found
		if (DELTA_X == 0)
		{
			return (FALSE);
		}
      x = clipX1_;
      y = *pY0 + (((clipX1_ - *pX0) * DELTA_Y) / DELTA_X);
    }
    else if ((OUT_CODE & LEFT_MASK) != 0)
    {   // $[TI1.6] -- clip left...
      x = clipX0_;
	  // protect from divide by zero valid line not found
	  if (DELTA_X == 0)
	  {
		  return (FALSE);
	  }
      y = *pY0 + (((clipX0_ - *pX0) * DELTA_Y) / DELTA_X);
    }
    else
    {
		// since 'OUT_CODE' is NEVER zero, this path can NEVER be taken...
		// valid line not found
		return (FALSE);
    }

    if (OUT_CODE == outCode0)
    {   // $[TI1.7] -- 'x' and 'y' are the new values for '*pX0' and '*pY0'...
      *pX0 = x;
      *pY0 = y;

      // update outCode0...
      outCode0 = calcClippingCode_(*pX0, *pY0);
    }
    else
    {   // $[TI1.8] -- 'x' and 'y' are the new values for '*pX1' and '*pY1'...
      *pX1 = x;
      *pY1 = y;

      // update outCode1...
      outCode1 = calcClippingCode_(*pX1, *pY1);
    }
  }   // $[TI2] -- starts either completely inside or completely outside...

  // are both points completely inside the clipping region?...
  return((outCode0 | outCode1) == 0);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
GraphicsContext::SoftFault(const SoftFaultID softFaultID,
		   const Uint32 lineNumber,
		   const char *pFileName,
		   const char *pPredicate)
{
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
			VgaGraphicsDriver::VGA_GRAPHICS_CONTEXT_ID, 
			lineNumber, pFileName, pPredicate);
}


//=====================================================================  
//
//  Private Methods...
//
//=====================================================================  

