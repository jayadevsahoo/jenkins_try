#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  MathConstants - Mathematical Constants.
//---------------------------------------------------------------------
//@ Interface-Description
//  This defines global math constants that are "scoped" by a 'class'.
//  (The scoping of the math constants eliminates global name-clashing.)
//---------------------------------------------------------------------
//@ Rationale
//  Provides a central location to define "scoped" mathematical
//  constants.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/MathConstants.ccv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "MathConstants.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

const Real32  MathConstants::PI = 3.14159f;
const Real32  MathConstants::E  = 2.71828f;
