#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SerialNumSetupSubScreen - Activated automatically at start up
// time in Service Mode if BD or/and GUI serial numbers don't match those
// on data key.
//---------------------------------------------------------------------
//@ Interface-Description
// Only one instance of this class is created by LowerSubScreenArea.  The
// interface is pretty generic, as a SubScreen it defines the activate()
// and deactivate() methods and then receives button events
// through via the usual means (i.e., the buttonDownHappened(),
// buttonUpHappened(), adjustPanelRestoreFocusHappened() and
// adjustPanelAcceptPressHappened()).
//---------------------------------------------------------------------
//@ Rationale
// This is a class which implements all the user confirmation steps
// required to set or bypass copying serial numbers to match the data key. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The main operation of this class is to wait for button down and up events,
// received via buttonUpHappened() ,buttonDownHappened() and 
// adjustPanelAcceptPressHappened().  It also waits for test result data and
// test commands received from Service-Data and and dispatched to the virtual
// methods processTestPrompt(), processTestKeyAllowed(), 
// processTestResultStatus(), processTestResultCondition(), and 
// processTestData().  
// The user's responses to the prompts are what drive the process forward.
// Users can command to overwrite serial numbers to match the datakey only if
// either the BD or the GUI serial number is the default signature, or if they 
// match the numbers on the data key.  If the BD or GUI serial numbers do not
// match the serial numbers on BD or GUI boards, but the NovRam's serial numbers
// match the BD or GUI's datakey then users can command to overwrite serial numbers
// also.
// To copy serial numbers, two Service-Mode order-independent tests are started:
// to copy BD serial number then GUI serial number.  Regardless of the result
// of the "tests", service can be resumed immediately either automatically in
// case of "test" success or after the ACCEPT key press in case of failure ; the
// users are returned to the Service Lower Screen where normal service can be
// performed.  The alternative to copying serial numbers to match the data key 
// is bypassing the setting process, in which case normal service is also
// resumed.  To allow users a great deal of flexibility, service even resumes
// under catastrophies like BD or/and GUI serial numbers don't match either the
// initial signatures or the data key.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only be
// displayed in the UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//  @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SerialNumSetupSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc   Date:  26-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 006   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//
//  Revision: 005  By:  syw	   Date:  19-Nov-1998    DCS Number: 5218
//  Project:  BiLevel
//  Description:
//		Handle DATAKEY_INVALID case in activate().
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003  By:  gdc    Date: 23-Sep-1997    DR Number: 2513
//    Project:  Sigma (R8027)
//    Description:
//      Cleaned up Service-Mode interfaces in support of remote test.
//
//  Revision: 002  By:  yyy    Date:  28-JUL-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//		Added SN_SETTING_SERIAL_NUMBER_MSG_A prompt to replace a hard
//		coded string.
//
//  Revision: 01  By:  yyy    Date:  08-Apr-1997    DR Number: 1884
//       Project:  Sigma (840)
//       Description:
//             Reset the BD and GUI serial number after a successful serial number
//             programming.
//=====================================================================

#include "SerialNumSetupSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "PromptStrs.hh"
#include "SmTestId.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceLowerScreenSelectArea.hh"

#if defined FAKE_SM
#	include "FakeServiceModeManager.hh"
#   define SmManager FakeServiceModeManager
#	include "GuiEventRegistrar.hh"
#else		
#	include "SmManager.hh"
#endif	// FAKE_SM

#include "AdjustPanel.hh"
#include "LowerScreen.hh"
#include "PromptArea.hh"
#include "SubScreenArea.hh"
#include "ServiceModeAgent.hh"
#include "Colors.hh"
 
#include "StringConverter.hh" 
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Uint16 BUTTON_X_ = 30;
static const Uint16 BUTTON_Y_ = 130;
static const Uint16 BUTTON_X_GAP_ = 20;
static const Uint16 BUTTON_X2_ = 324;
static const Uint16 BUTTON_Y2_ = 225;
static const Uint16 BUTTON_WIDTH_ = 285;
static const Uint16 TROUBLE_S_BUTTON_WIDTH_1_ = 265;
static const Uint16 BUTTON_HEIGHT_ = 50;
static const Uint8  BUTTON_BORDER_ = 3;
static const Int32 STATUS_LABEL_X_ = 379;
static const Int32 STATUS_LABEL_Y_ = 0;
static const Int32 STATUS_WIDTH_ = 255;
static const Int32 STATUS_HEIGHT_ = 31;
static const Int32 ERR_DISPLAY_AREA_X1_ = 239;
static const Int32 ERR_DISPLAY_ROW_WIDTH_ = 25;
static const Int32 ERR_DISPLAY_AREA_Y1_ = 300;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SerialNumSetupSubScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the SerialNumSetupSubScreen.  The given `pSubScreenArea' is 
// the SubScreenArea in which it will be displayed (in this case the upper 
// subscreen area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Creates, positions, and initializes all the graphical components of this
// subscreen.  The error text table is initialized. Finally, invoke the
// test manager's methods to setup the test result data array and the test
// command array.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SerialNumSetupSubScreen::SerialNumSetupSubScreen(SubScreenArea *pSubScreenArea) :
		SubScreen(pSubScreenArea),
		setSNStatus_(STATUS_LABEL_X_, STATUS_LABEL_Y_,
						STATUS_WIDTH_, STATUS_HEIGHT_,
						MiscStrs::EXH_V_CAL_STATUS_LABEL,
						NULL_STRING_ID),
		pCurrentButton_(NULL),
		errorHappened_(FALSE),
		errorIndex_(0),
		titleArea_(MiscStrs::SER_NUMB_SETUP_SUBSCREEN_TITLE,
					SubScreenTitleArea::SSTA_CENTER),
		testManager_(this),
		serNumberSettingButton_(
					BUTTON_X_, 		BUTTON_Y_,
					TROUBLE_S_BUTTON_WIDTH_1_, 	BUTTON_HEIGHT_,
					Button::LIGHT, 	BUTTON_BORDER_,
					Button::SQUARE, Button::NO_BORDER,
					MiscStrs::SER_NUM_SETTING_BUTTON_TITLE),
		serNumberBypassingButton_(
					BUTTON_X_ + BUTTON_WIDTH_ + BUTTON_X_GAP_, BUTTON_Y_,
					BUTTON_WIDTH_, 		BUTTON_HEIGHT_,
					Button::LIGHT, 		BUTTON_BORDER_,
					Button::SQUARE, 	Button::NO_BORDER,
					MiscStrs::SER_NUM_BYPASSING_BUTTON_TITLE),
		troubleShootingFlag_(FALSE)

{
	CALL_TRACE("SerialNumSetupSubScreen::SerialNumSetupSubScreen(pSubScreenArea)");
	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Setup the error display area. 
	setupErrorDisplay_();

	snTroubleShootingField_.setColor(Colors::WHITE);
	snTroubleShootingField_.setX(BUTTON_X_);
	snTroubleShootingField_.setY(BUTTON_Y_);
	snTroubleShootingField_.setText(MiscStrs::EMPTY_STRING);

	serNumberDisplayField1_.setColor(Colors::WHITE);
	serNumberDisplayField1_.setX(BUTTON_X_);
	serNumberDisplayField1_.setY(BUTTON_Y2_);

	serNumberDisplayField2_.setColor(Colors::WHITE);
	serNumberDisplayField2_.setX(BUTTON_X2_);
	serNumberDisplayField2_.setY(BUTTON_Y2_);

	// Register for callbacks for all buttons in which we are interested.
	serNumberSettingButton_.setButtonCallback(this);
	serNumberBypassingButton_.setButtonCallback(this);

	addDrawable(&titleArea_);
	addDrawable(&serNumberSettingButton_);
	addDrawable(&serNumberBypassingButton_);
	addDrawable(&snTroubleShootingField_);
	addDrawable(&serNumberDisplayField1_);
	addDrawable(&serNumberDisplayField2_);

	// Hide the following drawables
	setSNStatus_.setShow(FALSE);

	// Add the following drawables
	addDrawable(&setSNStatus_);
	
	// Initialize the error table.
	setErrorTable_();

	testManager_.setupTestResultDataArray();
	testManager_.setupTestCommandArray();
										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SerialNumSetupSubScreen  [Destructor]
//
//@ Interface-Description
//  Empty destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//  N/A 
//---------------------------------------------------------------------
//@ PreCondition
//  N/A	
//---------------------------------------------------------------------
//@ PostCondition
//  N/A	
//@ End-Method
//=====================================================================

SerialNumSetupSubScreen::~SerialNumSetupSubScreen(void)
{
	CALL_TRACE("SerialNumSetupSubScreen::~SerialNumSetupSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just before this subscreen is displayed
//  on the screen.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First hide the lower screen select area.  If datakey is not installed
//  then display the diagnostic message.  If the datakey does not match
//  the serial number then we either display two buttons for operator to
//  override or skip the serial number programming process or we display
//  one button to let user acknowledge that serial numbers is mismatched.
//  Finally, Register all test results for test events, by initializing
//  ServiceDataRegistrar.
// $[07007] When an EST test, an Exhalation valve calibration or ...
// $[07063] If the Serial Number Setup subscreen is displayed ...
// $[07066] If the Serial Number Setup subscreen is displayed because ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::activate(void)
{
	CALL_TRACE("SerialNumSetupSubScreen::activate(void)");

	// Hide the lower screen select area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

	switch (GuiApp::GetSerialNumberInfoState())
	{
	 case GuiApp::DATAKEY_NOT_INSTALLED:	
										// $[TI1]
		// Data key is not installed, display the diagnostic.
		displayDiagnostic_(MiscStrs::SER_NUM_TROUBLE_SHOOTING_BUTTON_TITLE1);
		serNumberDisplayField1_.setShow(FALSE);
		serNumberDisplayField2_.setShow(FALSE);
		break;
		
	 case GuiApp::DATAKEY_INVALID:	
										// $[TI5]
		// Data key not valid, display the diagnostic.
		displayDiagnostic_(MiscStrs::SER_NUM_TROUBLE_SHOOTING_BUTTON_TITLE3);
		serNumberDisplayField1_.setShow(FALSE);
		serNumberDisplayField2_.setShow(FALSE);
		break;
		
	case GuiApp::SERIAL_NUMBER_NOT_MATCHED:
										// $[TI2]
		// Prepare serial numbers to be displayed.
		setSNDisplayArea();

		if (areSerialNumbersProgrammable_())
		{	// $[TI3]
			// Serial numbers are programmable, we display buttons to allow
			// options for copying or bypassing. 
			serNumberSettingButton_.setShow(TRUE);
			serNumberBypassingButton_.setShow(TRUE);
			snTroubleShootingField_.setShow(FALSE);

			// Update Prompt area.
			testManager_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								PromptStrs::TOUCH_A_BUTTON_P);
		}
		else
		{	// $[TI4]
			// Serial numbers mismatched due to some other reason, display 
			// informational button.
			displayDiagnostic_(MiscStrs::SER_NUM_TROUBLE_SHOOTING_BUTTON_TITLE2);
		}
		break;
		
	case GuiApp::SERIAL_NUMBER_OK:
	case GuiApp::MAX_SERIAL_NUMBER_STATES:
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

	//
	// Register all general possible test results for test events, but first
	// we have to initialize ServiceDataRegistrar.
	//
	testManager_.registerTestResultData();
	testManager_.registerTestCommands();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by UpperSubScreenArea just after this subscreen has been
// removed from the screen.  No parameters are needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply dismiss the AdjustPanel target, clear the prompts, clear the error
// display area and set buttons to up.
// $[07008] When the test run finished or is stopped by the technician ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::deactivate(void)
{
	CALL_TRACE("SerialNumSetupSubScreen::deactivate(void)");


#if defined FORNOW
printf("SerialNumSetupSubScreen::deactivate() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompt
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);

	// Show the lower screen select area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);

	// Clear the error display area and set buttons to up.
	for (int i=0; i<2*MAX_CONDITION_ID; i++)
	{	
		// Hide the following drawables
		errDisplayArea_[i].setShow(FALSE);
	}
	
	serNumberSettingButton_.setToUp();
	serNumberBypassingButton_.setToUp();

										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.
//  It is normally called when the operator release the off-screen
//  keyboard key and the AdjustPanel needs to restore the default
//  AdjustPanel target.  It is the duty of this inherited method to restore
//  the previous test prompts in order to continue the SerialNum Setup process.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Invoke the test manager's method to restore the prompts displayed before
//  the focus is taken.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("SerialNumSetupSubScreen::adjustPanelRestoreFocusHappened(void)");


#if defined FORNOW
printf("SerialNumSetupSubScreen::adjustPanelRestoreFocusHappened() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	testManager_.restoreTestPrompt();
										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setErrorTable_
//
//@ Interface-Description
// 	Sets the error text in the error table.  Any condition that arises
//	has a unique id which is used as an index into this table of condition
//	text.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assign condition text (non-cheap text) to error table.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::setErrorTable_(void)
{
	Int testIndex = 0;
	
	CALL_TRACE("SerialNumSetupSubScreen::setErrorTable_(void)");


#if defined FORNOW
printf("SerialNumSetupSubScreen::setErrorTable_() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	testIndex = 0;
	conditionText_[testIndex++] = MiscStrs::SN_CANT_PROGRAM_FLASH;
	conditionText_[testIndex++] = MiscStrs::NO_DATA_KEY;
										// $[TI1]

}

//============================ m e t h o d   d e s c r i p t i o n ====
//@ Method: processTestPrompt_
//
//@ Interface-Description
//  Empty method
//---------------------------------------------------------------------
//@ Implementation-Description
//  N/A
//---------------------------------------------------------------------
//@ PreCondition
//  A safe class assertion is invoked.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("SerialNumSetupSubScreen::processTestPrompt(Int command)");
	SAFE_CLASS_ASSERTION(FALSE);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//  Empty method
//---------------------------------------------------------------------
//@ Implementation-Description
//  N/A
//---------------------------------------------------------------------
//@ PreCondition
//  A safe class assertion is invoked.
//---------------------------------------------------------------------
//@ PostCondition
//  N/A	
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::processTestKeyAllowed(Int command)
{
	CALL_TRACE("SerialNumSetupSubScreen::processTestKeyAllowed(Int command)");
	SAFE_CLASS_ASSERTION(FALSE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//  Process the test result status given by Service Mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dispatch the given command and carry out appropriate actions. 
//  If test ends, we invoke the next necessary test.  If test alert or
//  failure is received, we set the errorHappened_ flag.  Otherwise, we
//  invoke a safe class assertion. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("SerialNumSetupSubScreen::processTestResultStatus_"
								"(Int resultStatus Int testResultId)");



	switch (resultStatus)
	{
	case SmStatusId::TEST_END:			// $[TI1]
		nextTest_();
		break;

	case SmStatusId::TEST_START:		// $[TI2]
		break;

	case SmStatusId::TEST_ALERT:		// $[TI3]

	case SmStatusId::TEST_FAILURE:		// $[TI4]
		errorHappened_ = TRUE;
		break;

	default:
		SAFE_CLASS_ASSERTION(FALSE);
		break;
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//  Process test result condition passed by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Display the text for the corresponding result condition on the screen
//  and update the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//  Result condition has to be in the range (0, MAX_CONDITION_ID]
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("SerialNumSetupSubScreen::processTestResultCondition(Int resultCondition)");


#if defined FORNOW
printf("SerialNumSetupSubScreen::processTestResultCondition() at line  %d, resultCondition = %d\n", __LINE__, resultCondition);
#endif	// FORNOW

	SAFE_CLASS_ASSERTION((resultCondition > 0) && (resultCondition <= MAX_CONDITION_ID));

	errDisplayArea_[errorIndex_].setText(conditionText_[resultCondition-1]);
	errDisplayArea_[errorIndex_].setShow(TRUE);
	errorIndex_++;

	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_HIGH, NULL_STRING_ID);
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_SEE_OP_MANUAL_A);

	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);
											// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  Process data given by Service-Mode
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this subscreen is designed to not display test data,
//  this is an empty method.
//---------------------------------------------------------------------
//@ PreCondition
//  A safe class assertion is invoked. 
//---------------------------------------------------------------------
//@ PostCondition
//  N/A	
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("SerialNumSetupSubScreen::processTestData(Int dataIndex, TestResult *pResult)");

#if defined FORNOW
printf("SerialNumSetupSubScreen::processTestData() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Empty method
	// This subscreen doesn't display test data
	SAFE_CLASS_ASSERTION(FALSE);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened(void) 
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.  It is
//  normally called when the operator presses down the Accept key to signal the
//  accepting of continuing the test process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First AdjustPanel's non-persistent focus is cleared.
//  If the users want to program the serial number, we 
//     1. Flatten the button temporarily.
//     2. Update the prompt area.
//     3. Start the copying process.
//  Else user want to skip the serial number setup, we
//     1. return to normal service mode.
//  Else if users acknowledge the warning message, we
//     1. return to normal service mode.
//  Lastly, we reset the pCurrentButton_.
// $[07068] If the operator chooses to synchronize the serial number ...
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("SerialNumSetupSubScreen::adjustPanelAcceptPressHappened(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);


	if (pCurrentButton_ == &serNumberSettingButton_) 
	{												// $[TI1]
#if defined FORNOW
printf("Copying SN\n");
#endif
		serNumberSettingButton_.setToFlat();

		// Set all prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_HIGH, NULL_STRING_ID); 
	
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, NULL_STRING_ID);
	
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_HIGH, PromptStrs::SN_SETTING_SERIAL_NUMBER_MSG_A);

		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_LOW, NULL_STRING_ID); 

		// Starting first of a self-driven two-step process:  
		// BD and GUI Serial Number copy
		startTest_(SmTestId::BD_INIT_SERIAL_NUMBER_ID);
	}
	else if (pCurrentButton_ == &serNumberBypassingButton_)
	{												// $[TI2]
#if defined FORNOW
printf("acceptHappened, bypassing copy\n"); 
#endif
		// Go back the initial Service Mode lower screen
		returnToNormalService_();
	}
	else if ((pCurrentButton_ == NULL) && (troubleShootingFlag_ == TRUE))
		// Serial Number problem diagnostic was displayed in previous state.
	{												// $[TI3]
		// Either data key not installed or bad key(s)
#if defined FORNOW
printf("*** acceptHappened, data key uninstalled or bad key***\n");
#endif
		returnToNormalService_();
	}
													// $[TI4]
	pCurrentButton_ = NULL;

}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTest_
//
//@ Interface-Description
//  Start the copying (considered via Service-Mode's tests).
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply inform the Service Mode the type of test followed by start
//  test command.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)
{
	CALL_TRACE("SerialNumSetupSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)");

#if defined FORNOW
printf("startTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// Let the service mode  manager knows the type of Service mode.
	SmManager::DoFunction(SmTestId::SET_TEST_TYPE_MISC_ID);

	currentTestId_ = currentTestId;

	// Start the test.
	SmManager::DoTest(currentTestId);
												// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: endTest_
//
//@ Interface-Description
//  Proper handling when tests end.
//---------------------------------------------------------------------
//@ Implementation-Description
//  On the second test completion, if error has happened, we hide the current
//  button, then update the prompt and status area.  If no error happened,
//  we update the prompt area then return to normal service mode.
//  Then we signal end of test to the test manager.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::endTest_()
{
	CALL_TRACE("SerialNumSetupSubScreen::endTest_()");

	// If second test ends
	if (currentTestId_ == SmTestId::GUI_INIT_SERIAL_NUMBER_ID)
	{											// $[TI1]
		// Update Status area
		if (errorHappened_)
		{										// $[TI2]
			// Hide the SN setting button
			serNumberSettingButton_.setShow(FALSE);

			// Update prompt and status area.
			testManager_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								PromptStrs::TOUCH_A_BUTTON_P);
			testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
				 				PromptArea::PA_HIGH, 
								NULL_STRING_ID);
								
			// Hide the following drawables
			setSNStatus_.setShow(TRUE);
			setSNStatus_.setStatus(MiscStrs::COPY_SN_FAILED_MSG);
		}
		else
		{										// $[TI3]
			// Clear prompt area
			testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
									PromptArea::PA_HIGH, 
									NULL_STRING_ID);

			GuiApp::SetGuiSN(GuiApp::GetGuiDataKeySN());
			GuiApp::SetBdSN(GuiApp::GetBdDataKeySN());
			
			// Return to initial screen in Service Mode
			returnToNormalService_();
		}
	}											// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nextTest_
//
//@ Interface-Description
//  Handles the next state after a test is done.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If first test is done, start the second test.  If second test is done
//  we invoke a function to wrap up the process.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::nextTest_(void)
{
	CALL_TRACE("SerialNumSetupSubScreen::nextTest_(void)");


	if (currentTestId_ == SmTestId::BD_INIT_SERIAL_NUMBER_ID)
	{												// $[TI1]

		startTest_(SmTestId::GUI_INIT_SERIAL_NUMBER_ID);
	}
	else if (currentTestId_ == SmTestId::GUI_INIT_SERIAL_NUMBER_ID)
	{												// $[TI2]
		endTest_();
	}												// $[TI3]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when the any button is pressed up.  Passed a pointer to the
// button as well as 'byOperatorAction', a flag which indicates whether the
// operator pressed the button up or whether the setToUp() method was called.
// We restore prompts appropriately.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We update the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::buttonUpHappened(Button *pButton, Boolean)
{
	CALL_TRACE("SerialNumSetupSubScreen::buttonUpHappened(Button *pButton, "
																"Boolean)");

	// Don't do anything if this subscreen isn't displayed
	if (!isVisible())
	{											// $[TI1]
		return;
	}											// $[TI2]

	if ((pButton == &serNumberSettingButton_) || 
		(pButton == &serNumberBypassingButton_)
	   )
	{											// $[TI3]
		// Clear prompt area 
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								PromptStrs::TOUCH_A_BUTTON_P);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_HIGH,
								NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_LOW,
								NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
           						PromptArea::PA_HIGH, 
								NULL_STRING_ID);
	}											// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when any button SerialNumSetupSubScreen is pressed down.  We
// must display the screen corresponding to the pressed button.
// Passed a pointer to the button as well as 'byOperatorAction', a flag
// which indicates whether the operator pressed the button down or whether the
// setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// Check the button pressed and then activate the corresponding action.
// We just make sure the previously selected button is pressed down.
// If the serNumberSettingButton_ or the serNumberBypassingButton_ is being
// pressed down, we
// 1. Grab the Adjust Panel's non-persistent focus.
// 2. Update the prompt area.
// 3. Remember which button pressed.
//---------------------------------------------------------------------
//@ PreCondition
// Callback must have been generated by an operator action.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::buttonDownHappened(Button *pButton,
												Boolean byOperatorAction)
{
	CALL_TRACE("SerialNumSetupSubScreen::buttonDownHappened("
							"Button *pButton, Boolean byOperatorAction)");
	CLASS_PRE_CONDITION(byOperatorAction);

	if (!isVisible())
	{											// $[TI1]
		return;
	}											// $[TI2]

	// Another button is selected.  Deselect the previously selected button
	if (pCurrentButton_ != pButton && pCurrentButton_ != NULL)
	{											// $[TI3]	
#ifdef FORNOW
printf("SerialNumSetupSubScreen, line %d\n", __LINE__);
#endif
		pCurrentButton_->setToUp();
		
		// Lose Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
						 PromptArea::PA_HIGH, NULL_STRING_ID);

	}											// $[TI4]

	// Prompt for users's acceptance.
	if ((pButton == &serNumberSettingButton_) || 
		(pButton == &serNumberBypassingButton_)
	   )
	{											// $[TI5]
#ifdef FORNOW
printf("SerialNumSetupSubScreen,  line %d\n", __LINE__);
#endif
		// Grab Adjust Panel focus (white background is displayed)
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();
	
		//
		// Prompt the users to press ACCEPT key
		//
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								PromptStrs::SM_KEY_ACCEPT_P);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_HIGH,
								NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_LOW,
								NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
           						PromptArea::PA_HIGH, 
								NULL_STRING_ID);
	}											// $[TI6]

#ifdef FORNOW
printf("SerialNumSetupSubScreen, line %d\n", __LINE__);
#endif
	pCurrentButton_ = pButton;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: returnToNormalService_
//
//@ Interface-Description
//	Called when the operation must go back to normal operation and
//	the users have total access to all commands in service mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Deactivate current subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void SerialNumSetupSubScreen::returnToNormalService_()
{
	CALL_TRACE("SerialNumSetupSubScreen::returnToNormalService_(void)");
#ifdef FORNOW
	printf("returnToNormalService_ called\n");
#endif

	// Deactivate this subscreen.
	getSubScreenArea()->deactivateSubScreen();
						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayDiagnostic_
//
//@ Interface-Description
//  Called when a trouble shooting message must be displayed
//---------------------------------------------------------------------
//@ Implementation-Description
//  Display/Hide the appropriate buttons for the subscreen.  
//  Set the primary prompt to ask user to accept, set the persistent focus
//  target and the trouble shooting flag.  Grab Adjust Panel focus and
//  Disable the knob sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void SerialNumSetupSubScreen::displayDiagnostic_(StringId displayText)
{
	CALL_TRACE("SerialNumSetupSubScreen::displayDiagnostic_(StringId displayText)");

	serNumberSettingButton_.setShow(FALSE);
	serNumberBypassingButton_.setShow(FALSE);
	snTroubleShootingField_.setShow(TRUE);
	snTroubleShootingField_.setText(displayText);
	snTroubleShootingField_.positionInContainer(GRAVITY_CENTER);

	testManager_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								PromptStrs::SM_KEY_ACCEPT_P);

	// Grab Adjust Panel focus
	AdjustPanel::TakeNonPersistentFocus(this, TRUE);

	// Disable the knob sound.
	AdjustPanel::DisableKnobRotateSound();
	
	troubleShootingFlag_ = TRUE;
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: areSerialNumbersProgrammable_
//
//@ Interface-Description
//  Called to determine if GUI and BD serial numbers in flash are
//  programmable.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the serial numbers are either the initial signature or do not match
//  the numbers on the data key and the NovRam serial number matches the
//  number on the datakey, return TRUE. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean SerialNumSetupSubScreen::areSerialNumbersProgrammable_()
{
	CALL_TRACE("SerialNumSetupSubScreen::areSerialNumbersProgrammable_(void)");

	SerialNumber defaultSerialNumber;

	Boolean novRamMatchesBdDatakey = (GuiApp::GetBdNovRamSN() == GuiApp::GetBdDataKeySN()) ;
	Boolean novRamMatchesGuiDatakey = (GuiApp::GetGuiNovRamSN() == GuiApp::GetGuiDataKeySN()) ;

	return ( 
			// For NOW, allow the users to copy serial numbers regardless	

#ifdef SIGMA_DEVELOPMENT
				GuiApp::GetGuiSN() == defaultSerialNumber ||
				GuiApp::GetBdSN() == defaultSerialNumber ||
				((GuiApp::GetGuiDataKeySN() != GuiApp::GetGuiSN()) && novRamMatchesGuiDatakey)	||
				((GuiApp::GetBdDataKeySN() != GuiApp::GetBdSN()) && novRamMatchesBdDatakey)

#else
				GuiApp::GetGuiSN() == defaultSerialNumber 	||
				GuiApp::GetBdSN() == defaultSerialNumber	||
				((GuiApp::GetGuiDataKeySN() != GuiApp::GetGuiSN()) && novRamMatchesGuiDatakey)	||
				((GuiApp::GetBdDataKeySN() != GuiApp::GetBdSN()) && novRamMatchesBdDatakey)

#endif // SIGMA_DEVELOPMENT
		   );
		   // $[TI1], $[TI2], $[TI3], $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSNDisplayArea
//
//@ Interface-Description
//  Set up the display area for Serial numbers.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Format the messages and display them.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void SerialNumSetupSubScreen::setSNDisplayArea(void)
{
	CALL_TRACE("SerialNumSetupSubScreen::setSNDisplayArea(void)");

	wchar_t tmpBuffer[256];

	// Display serial numbers
	wchar_t tmpGuiSN[128];
	wchar_t tmpBdSN[128];
	StringConverter::ToWString(tmpGuiSN, 128, GuiApp::GetGuiSN().getString());
	StringConverter::ToWString(tmpBdSN, 128, GuiApp::GetBdSN().getString());
	swprintf(tmpBuffer, MiscStrs::SERIAL_NUMBER_COLUMN1_DISPLAY
		, tmpGuiSN
		, tmpBdSN);
	serNumberDisplayField1_.setText(tmpBuffer);
	serNumberDisplayField1_.setShow(TRUE);


	wchar_t tmpGuiDataKeySN[128];
	wchar_t tmpBdDataKeySN[128];
	StringConverter::ToWString(tmpGuiDataKeySN, 128, GuiApp::GetGuiDataKeySN().getString());
	StringConverter::ToWString(tmpBdDataKeySN, 128, GuiApp::GetBdDataKeySN().getString());
	swprintf_s(tmpBuffer, 256, MiscStrs::SERIAL_NUMBER_COLUMN2_DISPLAY 
		, tmpGuiDataKeySN
		, tmpBdDataKeySN);
	serNumberDisplayField2_.setText(tmpBuffer);
	serNumberDisplayField2_.setShow(TRUE);
						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupErrorDisplay_
//
//@ Interface-Description
//	This method sets up the error display area.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  It sets the color, location, show flag for this area then add this graphical 
//  element to the drawable list.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void SerialNumSetupSubScreen::setupErrorDisplay_()
{
	CALL_TRACE("SerialNumSetupSubScreen::setupErrorDisplay_(void)");

	for (int i=0; i<2*MAX_CONDITION_ID; i++)
	{	
		errDisplayArea_[i].setColor(Colors::WHITE);
		errDisplayArea_[i].setY(ERR_DISPLAY_AREA_Y1_+(i*ERR_DISPLAY_ROW_WIDTH_));
		errDisplayArea_[i].setX(ERR_DISPLAY_AREA_X1_);

		// Hide the following drawables
		errDisplayArea_[i].setShow(FALSE);

		// Add the following drawables
		addDrawable(&errDisplayArea_[i]);
	}
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SerialNumSetupSubScreen::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("SerialNumSetupSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							SERIALNUMSETUPSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}

