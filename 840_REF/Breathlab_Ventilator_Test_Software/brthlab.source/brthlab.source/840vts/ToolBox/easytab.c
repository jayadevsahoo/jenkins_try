#include <cvidef.h>
#if _NI_mswin32_
#include <windows.h>
#endif /* _NI_mswin32 */
#include <utility.h>
#include <userint.h>
#include "toolbox.h"
#include "easytab.h"

#define TABBUTTON_ACTIVE_EXTRA_TOP      2   /* tab is made larger when active */
#define TABBUTTON_ACTIVE_EXTRA_LEFT     2   
#define TABBUTTON_ACTIVE_EXTRA_RIGHT    2   

#define TABBUTTON_LEFT_MARGIN           6
#define TABBUTTON_RIGHT_MARGIN          6
#define TABBUTTON_TOP_MARGIN            3
#define TABBUTTON_BOTTOM_MARGIN         2

#define CLIENT_LEFT_FRAME_WIDTH         1
#define CLIENT_RIGHT_FRAME_WIDTH        2
#define CLIENT_TOP_FRAME_WIDTH          1
#define CLIENT_BOTTOM_FRAME_WIDTH       2
#define CLIENT_VERTICAL_FRAME_WIDTH     (CLIENT_TOP_FRAME_WIDTH + CLIENT_BOTTOM_FRAME_WIDTH)
#define CLIENT_HORIZONTAL_FRAME_WIDTH   (CLIENT_LEFT_FRAME_WIDTH + CLIENT_RIGHT_FRAME_WIDTH)

#define CLIENT_MIN_WIDTH                1
#define CLIENT_MIN_HEIGHT               1

#define EASY_TAB_SIGNATURE              CAT4CHARS('T', 'G', 'R', 'P')
#define SHEET_SIGNATURE                 CAT4CHARS('S', 'H', 'E', 'T')
#define TAB_BUTTON_SIGNATURE            CAT4CHARS('T', 'B', 'U', 'T')

#define THREAD_SLEEP_TIME               10L

#define ATTR_EASY_TAB_OVERRIDE_CTRL_ACTIVATION_POLICY   13000


typedef struct  /* for virtual tab-order lists */
    {
    int panel;          /* panel of the control */
    int ctrl;           /* a control in the tab list */
    int isTabStop;      /* TRUE if this control can be tabbed to */
    } TabOrderEntry;
    
typedef struct TabButtonStruct  /* object which represent the visual appearance of a tab button */
    {
    int                 signature;              /* debugging aid */   
    struct SheetStruct  *sheet;                 /* the sheet that owns this tab button */
    char                *label;                 /* CString that holds the given text for the buttons label */
    char                *displayLabel;          /* CString that holds the display text for the buttons label (same as label except double underlines have been removed) */
    Point               labelSize;              /* the width of the text in pixels given the owning groups current font */
    Point               minSize;                /* the minimum height and width the tab button can have given its label text and font */
    Rect                activeBounds;           /* in canvas coordinates */
    Rect                inactiveBounds;         /* in canvas coordinates */
    char                acceleratorKey;         /* 0 if button does not have an underline accelerator */
    int                 underlineOffset;        /* number of pixels from the start of the displayed label to the start of the accelerator underline */
    int                 underlineWidth;         /* how wide the accelerator underline is in pixels */
    int                 bitmap;                 /* bitmap image of text */
    int                 dimmed;                 /* true if dimmed */
    }   *TabButton;

typedef struct  SheetStruct     /* object which represents a tab sheet in a tab group */
    {
    int                     signature;          /* debugging aid */
    int                     panel;              /* panel that holds the controls, may be same as dialog panel */
    int                     ownsPanel;          /* TRUE if the sheet owns a child panel that contains the sheets controls */
    TabButton               tabButton;          /* the tab button for this sheet */
    int                     originalRow;        /* which row is the tab on (1-based, starting from the top) */
    struct TabGroupStruct   *group;             /* owner of this sheet */
    ListType                ctrlList;           /* controls on this sheet if ownsPanel == FALSE */
    int                     activatePanelWhenShown;  /* if TRUE, the panel of this sheet is activated the next time the group is redrawn, used to postpone panel activation since it can cause premature drawing */
    int                     originalWidth;      /* width of the panel when it was added to the group */
    int                     originalHeight;     /* height of the panel when it was added to the group */
    int                     hidden;             /* true if button for sheet is to be hidden */
    } *Sheet;

typedef struct TabGroupStruct   /* object which represents a group of tab sheets */
    {
    int                 signature;              /*  debugging aid */
    int                 dialogPanel;            /*  panel on which this tab dialog lives */
    struct SheetStruct  *activeSheet;           /*  which sheet is the active sheet */
    ListType            sheetList;              /*  list of sheets in this tab dialog */
    ListType            rowLists;               /*  list of lists of sheets on each row (currently displayed rows, not the rows the sheets were created on) */
    int                 canvas;                 /*  canvas to draw tab buttons on */
    Point               originalCanvasSize;     /*  size of the canvas when it was converted to a tab ctrl */
    Rect                frameRect;              /*  client area frame bounding box in panel coordinates (surrounds the client area) */
    Rect                clientRect;             /*  dialog area in panel coordinates, tab buttons appear above this */
    Rect                exteriorRect;           /*  canvas area in panel coordinates, dialog area, frame, and tab buttons appear within this */
    int                 sizeButtonsToFillRows;  /*  if TRUE, buttons stretch to fit all the available space */
    int                 tabButtonPositionsCalculated;   /* if FALSE, the tab button sizes need to be calculated */
    int                 canvasSetup;            /*  if FALSE, the canvas needs to be sized, etc.. */
    Rect                tabButtonBoundsUnion;   /*  in canvas coordinates */
    Rect                tabButtonEraseArea;     /*  same as tabButtonBoundsUnion, with a little extra to account for the widening of buttons when they become active */
    int                 displayInvalidated;     /*  if TRUE, the group is waiting for a pending deferred callback which was posted in to redraw the group */
    int                 textColor;              /*  color of the text in the Tab buttons (RGB) */
    int                 sizeHasBeenSet;         /*  TRUE if the size of the tab group has ever been explicitly set. */
    int                 buttonPosition;         /*  indicates where the tab buttons are in relation to the client area */
    int                 buttonGap;              /*  gap between tab buttons (-1 means use a much gap as possible if sizeButtonsToFillRows is TRUE) */
    char                metaFont[256];          /*  the font used to draw the text on the tab buttons */
    int                 hidden;                 /*  TRUE if the entire tab control is hidden */
    int                 bgColor;                /*  color for background of tabGroup (defaults to the panel background color of the panel holding the tabGroup ctrl), 
                                                    VAL_TRANSPARENT is the only other "reasonable" value, but it makes drawing slower */
    int                 valid;                  /* Tells if the lock is good. */
    int                 lockHandle;
    int                 overrideActivation;     /* if TRUE, easytab never activates the first control when switching tabs */
    } *TabGroup;
    
    /*  Add thread local variables here */
typedef struct
    {
    int    bitmapPanel;
    int    bitmapCanvas;        
    int    ctrlWasClicked;
    } threadLocalVars;

static TabButton    TabButton_Create(Sheet sheet, char *label);
static void         TabButton_Dispose(TabButton tabButton);
static void         TabButton_GetMinBounds(TabButton tabButton, int active, Rect *r);
static void         TabButton_SetBounds(TabButton tabButton, Rect r);
static Rect         TabButton_GetBounds(TabButton tabButton);
static int          TabButton_SetLabel(TabButton tabButton, char *label);
static void         TabButton_Draw(TabButton tabButton, int drawActive, int drawFocus);
static void         TabButton_DrawText(TabButton tabButton, int panel, int canvas, Point textPosition);
static void         TabButton_DrawLabel(TabButton tabButton, Rect bounds);
static int          TabButton_HasFocus(TabButton tabButton);
static int          TabButton_CreateTextBitmap(TabButton tabButton);
static int          TabButton_GetTextCanvas(int *panel, int *canvas, Point size);


static int          Sheet_Create(TabGroup group, int sheetPanel, int row, char *label, ListType ctrlList, Sheet *sheet);
static void         Sheet_Dispose(Sheet sheet);
static void         Sheet_ActivateFirstCtrl(Sheet sheet);
static int CVICALLBACK Sheet_PanelCallback (int panel, int event, void *callbackData, int eventData1, int eventData2);

static int          EasyTab_CreateFromCanvas(int panel, int canvas);
static void         EasyTab_Dispose(TabGroup group);
static Sheet        EasyTab_IndexToSheet(TabGroup group, int index);
static int          EasyTab_SheetToIndex(TabGroup group, Sheet sheet);
static Sheet        EasyTab_PanelToSheet(TabGroup group, int panel);
static Sheet        EasyTab_NameToSheet(TabGroup group, char *name, int *index);
static int          EasyTab_NumSheets(TabGroup sheetGroup);
static int          EasyTab_CalculateTabSizes(TabGroup group);
static void         EasyTab_FreeRowLists(TabGroup group);
static int          EasyTab_CalculateNumRows(TabGroup group);
static int          EasyTab_IdToGroup(int panel, int groupCanvas, TabGroup *group);
static int CVICALLBACK EasyTab_CanvasCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
static int CVICALLBACK EasyTab_PanelCallback (int panel, int event, void *callbackData, int eventData1, int eventData2);
static Sheet        EasyTab_GetActive(TabGroup sheetGroup);
static void         EasyTab_ActivateHorizontalNeighbor(TabGroup group, int previous, int wrap);
static void         EasyTab_ActivateVerticalNeighbor(TabGroup group);
static void         EasyTab_SetActive(TabGroup group, Sheet activeSheet);
static int          EasyTab_Draw(TabGroup group);
static void         EasyTab_DrawTabButtons(TabGroup group);
static int          EasyTab_HasFocus(TabGroup group);
static void         EasyTab_SetFocus(TabGroup group);
static Rect         EasyTab_CtrlsBoundsUnion(TabGroup group);   
static Rect         EasyTab_PanelBoundsUnion(TabGroup group, int sizeOnly, int useOriginalPanelSizes);
static int          EasyTab_SetClientRect(TabGroup group, Rect clientRect);
static Rect         EasyTab_GetClientRect(TabGroup group);
static Rect         EasyTab_GetExteriorRect(TabGroup group);
static int          EasyTab_SetExteriorRect(TabGroup group, Rect exteriorRect);
static void         CVICALLBACK EasyTab_UpdateDisplay(void *callbackData);
static void         EasyTab_InvalDisplay(TabGroup group);
static void         EasyTab_InvalRowLists(TabGroup group);
static void         EasyTab_InvalTabPositions(TabGroup group);
static void         EasyTab_InvalAll(TabGroup group);
static void         EasyTab_InvalCanvasSetup(TabGroup group);
static ListType     EasyTab_GetRowLists(TabGroup group);
static void         EasyTab_SetupCanvas(TabGroup group);
static int          EasyTab_CalcMinButtonArea(TabGroup group, int *minHeight, int *minWidth);
static TabButton    EasyTab_TabButtonAtPoint(TabGroup group, Point p);
static void         EasyTab_HandleLeftClick(TabGroup group, Point p);
static void         EasyTab_DrawFrame(TabGroup group);
static int          EasyTab_CalculateMinClientWidth(TabGroup group, int *minWidth);
static Rect         EasyTab_GetAutoSizeRect(TabGroup group, int makeRoomForButtons);
static int          EasyTab_HandleNavigationKeys(TabGroup group, int key, int *callbackReturnVal);
static int          EasyTab_HandleAcceleratorKey(int key, int *callbackReturnVal);
static Sheet        EasyTab_AccelKeyToSheet(TabGroup group, int key);
static int          EasyTab_AddSheetFromParmInfo(int panel, int easyTabCtrl, char *sheetName, void *module, int row, int numCtrls, va_list parmInfo);
static void         EasyTab_SetTextColorToDefault(TabGroup group);
static int          EasyTab_CheckButtonPosition(int buttonPosition);
static int          EasyTab_ResetLabels(TabGroup group);
static int          EasyTab_GetSheetDisplayPosition(TabGroup group, Sheet sheet, int *row, int *column);
static void         EasyTab_MoveActiveRowToEnd(TabGroup group);
static void         EasyTab_InvalDisplayForPanel(int panel);
static Sheet        EasyTab_GetVisibleInactiveSheet(TabGroup group);
static int          EasyTab_SetInactive(TabGroup group, Sheet sheet);
static int          EasyTab_NumVisibleTabsOnRow(TabGroup group, int rowIndex);
static int          EasyTab_CtrlCanBeTabStop(int panel, int ctrl);


static ListType     GetVirtualTabOrder(int panel, int tabGroupsOnly);
static int          AddToVirtualTabOrder(int panel, ListType list, int tabGroupsOnly);
static int          HandleTabKey(int key, int *callbackReturnVal);
static int          HandleOtherKeys(int key, int *callbackReturnVal);
static int          GetVirtualTabTopLevelPanel(int panel);
static int          NextVirtualTabStop(int panel, int ctrl, int tabbingForward, int wrap, TabOrderEntry *entry);
static void         InstallActivationHooks(int panel);

static void         DrawRectFrameWithFill(int panel, int canvas, Rect r);
static int          CreateTransparentOneColorBitmap(int *pixels, int color, int height, int width, int *bitmap);
static int          RotatePixels90Degrees(int **pixels, int *height, int *width, int rotateLeft);
static int          IsSheetPanel(int panel) ;

static void CVIFUNC InitializeMultithreadingIfNecessary(void);
static void CVIANSI UninitializeMultithreadingProcedure(void);

static int          gThreadLocalHandle     = 0;

/*************************************************************/

static void CVIFUNC InitializeMultithreadingIfNecessary(void)
{
    static int sMultithreadingInitDone = 0;       /* Multithreading has already been initialized */
    static int sMultithreadingInitNow  = 0;       /* Thread local is currently being initialized. */
    int prev = 0;
    
    /* Check if already been initialized, if not, do so now. */
    if (!sMultithreadingInitDone)
        {
        prev = InterlockedExchange(&sMultithreadingInitNow, 1);
        if (prev)                                   /* If it is initialized or is in the process of initializing. */
            {
            while (!sMultithreadingInitDone)        /* Wait until the thread initializing the multithreading finishes */
                Sleep(THREAD_SLEEP_TIME);
            }
        else
            {
            CmtNewThreadLocalVar (sizeof(threadLocalVars),
                                  (void *)NULL, NULL, NULL,
                                  &gThreadLocalHandle);
            atexit(UninitializeMultithreadingProcedure);
            sMultithreadingInitDone = 1;
            }
        }
}
                                                                             
/*************************************************************/

static void CVIANSI UninitializeMultithreadingProcedure(void)
{
    /* This function used to call CmtDiscardThreadLocalVar.
       That is a problem:  This may be in a DLL which is being dynamically
       unloaded by a program.  We also added the check for
       CVIRTEHasBeenDetached, which is a new function in CVI 5.0.
    */
    if (!CVIRTEHasBeenDetached())
        {
        if (gThreadLocalHandle)
            CmtDiscardThreadLocalVar (gThreadLocalHandle);
        }
}

/*************************************************************/

static void FreeListOfLists(ListType listOfLists)
{
    int         index;
    ListType    list;
    int         numLists;
    
    if (listOfLists)
        {
        numLists = ListNumItems(listOfLists);
        
        for (index = 1; index <= numLists; index++)
            {
            ListGetItem(listOfLists, &list, index);
            ListDispose(list);
            }
        
        ListDispose(listOfLists);
        }
}

/*************************************************************/

static void EasyTab_InvalRowLists(TabGroup group)
{
    EasyTab_FreeRowLists(group);
}

/*************************************************************/

static void EasyTab_FreeRowLists(TabGroup group)
{
    FreeListOfLists(group->rowLists);
    group->rowLists = NULL;
}

/*************************************************************/

static int EasyTab_CalculateNumRows(TabGroup group)
{
    int     index;
    int     numSheets;
    int     numRows = 0;
    Sheet   sheet;
    
    numSheets = EasyTab_NumSheets(group);   
    for (index = 1; index <= numSheets; index++)
        {
        ListGetItem(group->sheetList, &sheet, index);
        numRows = Max(numRows, sheet->originalRow);
        }
        
    return numRows;
}

/*************************************************************/

    /*  Build a list of lists of the sheets in each tab button row.  The row member
        of a sheet determines its row, and its order in the groups sheet lists 
        determines its order respect to other sheets in the same row.
        
        Note: The active row (if any) is alway moved to the end of the list so it
        appears at the bottom.
    */
static int EasyTab_CalculateRowLists(TabGroup group)
{
    int         error = 0;
    ListType    rowLists = NULL;
    ListType    sheetList = NULL;
    ListType    currSheetList;
    Sheet       sheet;
    int         numRows;
    int         numSheets;
    int         index;
    
    if (group->rowLists)    /* already done */
        return 0;
    
    numRows = EasyTab_CalculateNumRows(group);
    
    nullChk( rowLists = ListCreate(sizeof(ListType)));
    
    for (index = 1; index <= numRows; index++)
        {
        nullChk( sheetList = ListCreate(sizeof(Sheet)));
        nullChk( ListInsertItem(rowLists, &sheetList, END_OF_LIST));        
        sheetList = NULL;
        }
    
    numSheets = EasyTab_NumSheets(group);
    
    for (index = 1; index <= numSheets; index++)    
        {
        ListGetItem(group->sheetList, &sheet, index);
        if (!sheet->hidden)
            {
            ListGetItem(rowLists, &currSheetList, sheet->originalRow);
            nullChk( ListInsertItem(currSheetList, &sheet, END_OF_LIST));
            }
        }

    EasyTab_FreeRowLists(group);        
    group->rowLists = rowLists;
    EasyTab_MoveActiveRowToEnd(group);
        
Error:
    ListDispose(sheetList);     /* will be NULL unless an insertion error occurred */
    
    if  (error < 0)
        FreeListOfLists(rowLists);
        
    return error;
}
/*************************************************************/

    /*  Returns NULL if out of memory, the rowsLists returned belong to the group and should not
        be disposed by the caller
    */
static ListType EasyTab_GetRowLists(TabGroup group)
{
    if (EasyTab_CalculateRowLists(group) >= 0)
        return group->rowLists;
    else
        return NULL;
}

/*************************************************************/

static int EasyTab_NumVisibleTabsOnRow(TabGroup group, int rowIndex)
{
    ListType    rowLists = EasyTab_GetRowLists(group);
    ListType    currRowList;
    int         numRows;
    int         index;
    Sheet       firstSheet;

    numRows = ListNumItems(rowLists);

    for (index = 1; index <= numRows; index++)
        {
        ListGetItem(rowLists, &currRowList, index);
        ListGetItem(currRowList, &firstSheet, 1);
        if (firstSheet->originalRow == rowIndex)
            {
            int     sheetIndex;
            int     numSheets = ListNumItems(currRowList);
            Sheet   currSheet;
            int     numVisible = 0;
            
            for (sheetIndex = 1; sheetIndex <= numSheets; sheetIndex++)
                {
                ListGetItem(currRowList, &currSheet, sheetIndex);
                if (!currSheet->hidden)
                    numVisible++;
                }
                
            return numVisible;
            }
        }
        
    return 0;
}

/*************************************************************/

static int EasyTab_GetSheetDisplayPosition(TabGroup group, Sheet sheet, int *row, int *column)
{               
    int         numRows;
    ListType    rowLists, currRowList;
    int         columnIndex, rowIndex;
    int         found = FALSE;
    
    rowLists = EasyTab_GetRowLists(group);
    
    if (rowLists)
        {   
        numRows = ListNumItems(rowLists);
    
        for (rowIndex = 1; rowIndex <= numRows; rowIndex++)
            {
            ListGetItem(rowLists, &currRowList, rowIndex);
            columnIndex = ListFindItem (currRowList, &sheet, FRONT_OF_LIST, NULL);
        
            if (columnIndex > 0)
                {
                found = TRUE;
                break;
                }
            }
        }
        
    if (!found)
        {
        columnIndex = 0;
        rowIndex = 0;
        }
        
    if (column)
        *column = columnIndex;
    if (row)
        *row = rowIndex;
    
    return found;
}

/*************************************************************/

static void EasyTab_MoveActiveRowToEnd(TabGroup group)
{
    Sheet       activeSheet;
    int         row;
    ListType    oldActiveSheetRowList;
    ListType    newActiveSheetRowList;
    ListType    rowLists;
    int         numRows;

        /* move list of sheets on the active row to the end of the list of sheet lists so they will draw on the bottom */
    rowLists = EasyTab_GetRowLists(group);
    
    if (rowLists)
        {
        numRows = ListNumItems(rowLists);
        
        activeSheet = EasyTab_GetActive(group);     
        if (activeSheet)
            {
            if (!EasyTab_GetSheetDisplayPosition(group, activeSheet, &row, NULL))
                Assert(FALSE);
        
            if (row < numRows)
                {
                ListRemoveItem(rowLists, &newActiveSheetRowList, row);
                ListRemoveItem(rowLists, &oldActiveSheetRowList, END_OF_LIST);
                ListInsertItem(rowLists, &newActiveSheetRowList, END_OF_LIST);
                ListInsertItem(rowLists, &oldActiveSheetRowList, FRONT_OF_LIST);
                }
            }
        }
}

/*************************************************************/

    /*  minHeight and minWidth are given in relationship to the orientation of of the buttons.
        Therefore, if the buttons were on the side, the minWidth would actually be the vertical
        space needed by the buttons.
    */
static int EasyTab_CalcMinButtonArea(TabGroup group, int *minHeight, int *minWidth)
{
    int         error = 0;
    int         numRows;
    int         rowIndex;
    ListType    rowLists;
    ListType    sheetList;
    int         numSheets;
    int         sheetIndex;
    Sheet       sheet;
    int         largestRowMinWidth = 0;
    int         minRowWidth;
    Rect        minBounds;
    int         height = 0;

    nullChk( rowLists = EasyTab_GetRowLists(group));
    numRows = ListNumItems(rowLists);
    
    for (rowIndex = numRows; rowIndex >= 1 ; rowIndex--)
        {
        ListGetItem(rowLists, &sheetList, rowIndex);
        numSheets = ListNumItems(sheetList);

        minRowWidth = 0;
        for (sheetIndex  = 1; sheetIndex <= numSheets; sheetIndex++)
            {
            ListGetItem(sheetList, &sheet, sheetIndex);

            if(sheetIndex == 1)
                {
                TabButton_GetMinBounds(sheet->tabButton, rowIndex == 1, &minBounds);
                height += minBounds.height;
                }
                
            TabButton_GetMinBounds(sheet->tabButton, sheetIndex == 1, &minBounds);
            minRowWidth += minBounds.width;     
            }
            
        largestRowMinWidth = Max(largestRowMinWidth, minRowWidth);
        }

    if (minWidth)
        *minWidth = largestRowMinWidth;
    if (minHeight)
        *minHeight = height;
    
Error:
    return error;
}

/*************************************************************/

    /*  TabButton positions are calculated relative to the frameRect of the group 
        (whose top,left is always 0,0 in canvas coordinates).
    */
static int EasyTab_CalculateTabSizes(TabGroup group)
{
    int         error = 0;
    Sheet       sheet;
    Sheet       activeSheet;
    TabButton   tabButton;
    ListType    sheetList;
    ListType    rowLists;
    int         numRows;
    int         rowIndex;
    int         numSheets;
    int         numSheetsLeft;
    int         sheetIndex;
    Rect        minBounds;
    Rect        buttonBounds;
    Rect        unionRect;
    Rect        eraseRect;
    int         gapSpace;   /* number of pixels in row that need to be covered when the buttons are stretch to cover the row */
    int         availableGapSpace;
    int         stretchSpace;
    Rect        relBounds;
    int         relVOffset, relHOffset;
    int         minRowWidth, maxRowWidth;
    int         thisGapSpace;
    int         thisStretchSpace;
    int         activeSheetIsLeftMost;
    int         activeSheetIsRightMost;

    if (!group->tabButtonPositionsCalculated)   
        {
        if (!group->sizeHasBeenSet)
            EasyTab_AutoSize(group->dialogPanel, group->canvas);
        
        nullChk( rowLists = EasyTab_GetRowLists(group));
        numRows = ListNumItems(rowLists);
    
        relVOffset = 0;

        switch (group->buttonPosition)
            {
            case VAL_EASY_TAB_BTNS_ON_TOP:
            case VAL_EASY_TAB_BTNS_ON_BOTTOM:
                maxRowWidth = group->frameRect.width;
                break;
            case VAL_EASY_TAB_BTNS_ON_RIGHT:
            case VAL_EASY_TAB_BTNS_ON_LEFT:
                maxRowWidth = group->frameRect.height;
                break;
            }
            
        maxRowWidth = maxRowWidth - (TABBUTTON_ACTIVE_EXTRA_LEFT + TABBUTTON_ACTIVE_EXTRA_RIGHT);  
    
        RectSet(&unionRect, 0, 0, 0, 0);
    
        activeSheet = EasyTab_GetActive(group);
        
        for (rowIndex = numRows; rowIndex >= 1 ; rowIndex--)
            {
            ListGetItem(rowLists, &sheetList, rowIndex);
            numSheets = ListNumItems(sheetList);

            relHOffset = TABBUTTON_ACTIVE_EXTRA_LEFT;   /* non-active tab button are inset from the edge */

                /* Calculate minRowWidth and find the active sheet position */
            minRowWidth = 0;
            activeSheetIsLeftMost = FALSE;
            activeSheetIsRightMost = FALSE;
            for (sheetIndex  = 1; sheetIndex <= numSheets; sheetIndex++)
                {
                ListGetItem(sheetList, &sheet, sheetIndex);
                TabButton_GetMinBounds(sheet->tabButton, FALSE, &minBounds);
                minRowWidth += minBounds.width;             
                
                if (sheet == activeSheet)
                    {
                    if (sheetIndex == 1)
                        activeSheetIsLeftMost = TRUE;
                    if (sheetIndex == numSheets)
                        activeSheetIsRightMost = TRUE;
                    }
                }

            availableGapSpace = maxRowWidth - minRowWidth;
            
            if (group->buttonGap >= 0)
                gapSpace = Min((numSheets - 1) * group->buttonGap, availableGapSpace);
            else
                gapSpace = availableGapSpace;
                
            if (group->sizeButtonsToFillRows)
                stretchSpace = availableGapSpace - gapSpace;
            else
                stretchSpace = 0;
                
            for (sheetIndex  = 1; sheetIndex <= numSheets; sheetIndex++)
                {
                numSheetsLeft = numSheets - sheetIndex;
                ListGetItem(sheetList, &sheet, sheetIndex);
                tabButton = sheet->tabButton;
            
                TabButton_GetMinBounds(tabButton, FALSE, &minBounds);
                
                relBounds.top = relVOffset - minBounds.height;
                relBounds.left = relHOffset;
                    
                relBounds.height = 0 - relBounds.top;
                
                
                thisStretchSpace = stretchSpace / (numSheetsLeft + 1);
                stretchSpace -= thisStretchSpace;
                relBounds.width  = minBounds.width + thisStretchSpace;
                
                if (numSheetsLeft > 0)
                    thisGapSpace = (gapSpace / numSheetsLeft);
                else
                    thisGapSpace = 0;
                gapSpace -= thisGapSpace;
                
                relHOffset += thisGapSpace + relBounds.width;
            
                switch (group->buttonPosition)
                    {
                    case VAL_EASY_TAB_BTNS_ON_TOP:
                        buttonBounds = relBounds;
                        break;
                    case VAL_EASY_TAB_BTNS_ON_BOTTOM:
                        buttonBounds = relBounds;
                        buttonBounds.top = group->frameRect.height; 
                        break;
                    case VAL_EASY_TAB_BTNS_ON_RIGHT:
                        buttonBounds.left = group->frameRect.width;
                        buttonBounds.width = relBounds.height;
                        buttonBounds.top = relBounds.left;
                        buttonBounds.height = relBounds.width;
                        break;
                    case VAL_EASY_TAB_BTNS_ON_LEFT:
                        buttonBounds.left = relBounds.top;
                        buttonBounds.width = relBounds.height;
                        buttonBounds.top = group->frameRect.height - relBounds.left - relBounds.width;
                        buttonBounds.height = relBounds.width;
                        break;
                    }

                TabButton_SetBounds(tabButton, buttonBounds);
            
                if (RectEmpty(unionRect))
                    unionRect = buttonBounds;
                else
                    RectUnion(buttonBounds, unionRect, &unionRect);
                }
            
                relVOffset -= minBounds.height; /* use minBounds, because buttonBounds may have been vertical extended if group->sizeButtonsToFillRows is FALSE */
            }

        group->tabButtonPositionsCalculated = TRUE;
        
        if (!RectEmpty(unionRect))
            {
            if (numRows = 1)    /* need extra for when tab is active */
                switch (group->buttonPosition)
                    {
                    case VAL_EASY_TAB_BTNS_ON_TOP:
                        unionRect.height += TABBUTTON_ACTIVE_EXTRA_TOP;
                        unionRect.top -= TABBUTTON_ACTIVE_EXTRA_TOP;
                        break;
                    case VAL_EASY_TAB_BTNS_ON_BOTTOM:
                        unionRect.height += TABBUTTON_ACTIVE_EXTRA_TOP;
                        break;
                    case VAL_EASY_TAB_BTNS_ON_RIGHT:
                        unionRect.width += TABBUTTON_ACTIVE_EXTRA_TOP;
                        break;
                    case VAL_EASY_TAB_BTNS_ON_LEFT:
                        unionRect.width += TABBUTTON_ACTIVE_EXTRA_TOP;
                        unionRect.left -= TABBUTTON_ACTIVE_EXTRA_TOP;
                        break;
                    }
                
            eraseRect = unionRect;
            switch (group->buttonPosition)
                {
                case VAL_EASY_TAB_BTNS_ON_TOP:
                case VAL_EASY_TAB_BTNS_ON_BOTTOM:
                    if (!activeSheetIsRightMost)
                        eraseRect.width  += TABBUTTON_ACTIVE_EXTRA_RIGHT;
                    if (!activeSheetIsLeftMost)
                        {
                        eraseRect.width  += TABBUTTON_ACTIVE_EXTRA_LEFT;
                        eraseRect.left  -= TABBUTTON_ACTIVE_EXTRA_LEFT;
                        }
                    break;
                case VAL_EASY_TAB_BTNS_ON_RIGHT:
                    if (!activeSheetIsRightMost)
                        eraseRect.height += TABBUTTON_ACTIVE_EXTRA_RIGHT;
                    if (!activeSheetIsLeftMost)
                        {
                        eraseRect.height += TABBUTTON_ACTIVE_EXTRA_LEFT;
                        eraseRect.top -= TABBUTTON_ACTIVE_EXTRA_LEFT;
                        }
                    break;
                case VAL_EASY_TAB_BTNS_ON_LEFT:
                    if (!activeSheetIsRightMost)
                        {
                        eraseRect.height += TABBUTTON_ACTIVE_EXTRA_RIGHT;
                        eraseRect.top -= TABBUTTON_ACTIVE_EXTRA_RIGHT;
                        }
                    if (!activeSheetIsLeftMost)
                        eraseRect.height += TABBUTTON_ACTIVE_EXTRA_LEFT;
                    break;
                }
            }
        group->tabButtonBoundsUnion = unionRect;
        group->tabButtonEraseArea = eraseRect;
        }
        
Error:
    return error;
}

/*************************************************************/

static TabButton TabButton_Create(Sheet sheet, char *label)
{
    int         error = 0;
    TabButton   tabButton = 0;
    
    nullChk( tabButton = (TabButton)calloc(sizeof(*tabButton), 1));
    tabButton->signature = TAB_BUTTON_SIGNATURE;
    tabButton->sheet = sheet;
    tabButton->dimmed = FALSE;
    errChk( TabButton_SetLabel(tabButton, label));

Error:
    if (error < 0)
        {
        TabButton_Dispose(tabButton);
        tabButton = NULL;
        }
    
    return tabButton;
}

/*************************************************************/

static void TabButton_Dispose(TabButton tabButton)
{
    if (tabButton)
        {
        free(tabButton->label);
        free(tabButton->displayLabel);
        if (tabButton->bitmap != 0)
            DiscardBitmap(tabButton->bitmap);
        free(tabButton);
        }
}

/*************************************************************/

static int TabButton_SetLabel(TabButton tabButton, char *label)
{
    int     error = 0;
    char    *newLabel = 0;
    char    *newDisplayLabel = 0;
    int     underlinePos;
    char    savedChar;
    char    accelKeyStr[2];
    
    nullChk( newLabel = StrDup(label));
    nullChk( newDisplayLabel = StrDup(label));

    free(tabButton->label);
    free(tabButton->displayLabel);
    if (tabButton->bitmap != 0)
        {
        DiscardBitmap(tabButton->bitmap);
        tabButton->bitmap = 0;
        }
    
    tabButton->label = newLabel;
    tabButton->displayLabel = newDisplayLabel;
    
    RemoveAcceleratorEscapeCode(newDisplayLabel);
    underlinePos = AcceleratorEscapeCodeLocation(newLabel);
    
    if (underlinePos >= 0)
        {
        tabButton->acceleratorKey = newDisplayLabel[underlinePos];
        savedChar = newDisplayLabel[underlinePos];
        newDisplayLabel[underlinePos] = 0;  /* NULL terminate string before underline character for call to GetTextDisplaySize */
        GetTextDisplaySize (newDisplayLabel, tabButton->sheet->group->metaFont, 0, &tabButton->underlineOffset);
        newDisplayLabel[underlinePos] = savedChar;  /* fix label */
        
        accelKeyStr[0] = tabButton->acceleratorKey;
        accelKeyStr[1] = 0; /* NULL terminator */
        GetTextDisplaySize (accelKeyStr, tabButton->sheet->group->metaFont, 0, &tabButton->underlineWidth);
        }
    else
        tabButton->acceleratorKey = 0;
    
    GetTextDisplaySize (newDisplayLabel, tabButton->sheet->group->metaFont, 
                            &tabButton->labelSize.y, &tabButton->labelSize.x);
                            
    tabButton->minSize = tabButton->labelSize;
    tabButton->minSize.x += TABBUTTON_LEFT_MARGIN + TABBUTTON_RIGHT_MARGIN;
    tabButton->minSize.y += TABBUTTON_TOP_MARGIN + TABBUTTON_BOTTOM_MARGIN;
    
    EasyTab_InvalAll(tabButton->sheet->group);

Error:
    return error;
}

/*************************************************************/

    /*  all pixels values that are not VAL_TRANSPARENT will converted to the specified color. */
static int CreateTransparentOneColorBitmap(int *pixels, int color, int height, int width, int *bitmap)
{
    int             error = 0;
    unsigned char   *bits = 0;
    int             rowBytes;
    int             h, w;
    int             bitNum;
    unsigned char   *rowPtr;
    unsigned char   byte;
    int             colorTable[2];
    int             numBytes;

    *bitmap = 0;
    rowBytes = (width + 7) / 8;             /* how many bytes are needed (8 monochrome pixels per byte) */
    if (rowBytes & 0x01)
        rowBytes++;                         /* round up to nearest 2 byte boundary */
    numBytes = rowBytes * height;
    nullChk(bits = malloc(numBytes));
    
    colorTable[0] = VAL_GREEN;  /* all zero bits should be eliminated by the mask anyway */
    colorTable[1] = color;
    
    for (h = 0; h < height; h++)
        {
        rowPtr = bits + h * rowBytes;
        bitNum = 7;
        byte = 0;

        for (w = 0; w < width; w++)
            {
            if (pixels[h*width + w] != VAL_TRANSPARENT)
                byte |= 1 << bitNum;
            
            bitNum--;
            if (bitNum < 0)
                {
                *rowPtr = byte;
                rowPtr++;
                bitNum = 7;
                byte = 0;
                }
            }
        if (bitNum != 7)
            *rowPtr = byte;     /* finish last partial byte */
        }
    
    errChk( NewBitmap (rowBytes, 1, width, height, colorTable, bits, bits, bitmap));
    
Error:
    free(bits);
    return error;
}

/*************************************************************/

static int TabButton_GetTextCanvas(int *panel, int *canvas, Point size)
{
    int         error = 0;
    int         textPanel = *panel;
    int         textCanvas = *canvas;

    if (textPanel <= 0)
        errChk( textPanel = NewPanel(0, "", 40,40, 100, 100));
        
    if (textCanvas <= 0)
        {
        errChk( textCanvas = NewCtrl (textPanel, CTRL_CANVAS, "", 50, 0));
        SetCtrlAttribute(textPanel, textCanvas, ATTR_PICT_BGCOLOR, VAL_TRANSPARENT);
        }
        
    SetCtrlAttribute(textPanel, textCanvas, ATTR_HEIGHT, size.y);
    SetCtrlAttribute(textPanel, textCanvas, ATTR_WIDTH, size.x);
    *panel = textPanel;
    *canvas = textCanvas;
    
Error:
    return error;
}

/*************************************************************/

    /*  The pixels array is reallocated and the values of height and width are change to 
        match the rotatation.
    */
static int RotatePixels90Degrees(int **pixels, int *height, int *width, int rotateLeft)
{
    int error = 0;
    int *rotatedPixels = NULL;
    int *currPixels = *pixels;
    int oldHeight;
    int oldWidth;
    int newHeight;
    int newWidth;
    int h, w;
    
    oldHeight = *height;
    oldWidth = *width;
    newHeight = *width;
    newWidth = *height;
    
    nullChk( rotatedPixels = (int *)malloc(newHeight * newWidth * sizeof(int)));
    
    if (rotateLeft)
        {
        for (h = 0; h < oldHeight; h++)
            for (w = 0; w < oldWidth; w++)
                rotatedPixels[(oldWidth - w - 1) * newWidth + h] = currPixels[h * oldWidth + w];
        }
    else    /* rotate right */
        {
        for (h = 0; h < oldHeight; h++)
            for (w = 0; w < oldWidth; w++)
                rotatedPixels[w * newWidth + (oldHeight - h - 1)] = currPixels[h * oldWidth + w];
        }

Error:
    if (error < 0)
        {
        free(rotatedPixels);
        }
    else
        {
        free(*pixels);
        *pixels = rotatedPixels;
        *height = newHeight;
        *width = newWidth;
        }
        
    return error;
}

/*************************************************************/

static int TabButton_CreateTextBitmap(TabButton tabButton)
{
    int             error = 0;
    threadLocalVars *pTlv = 0;
    TabGroup        group;
    int             *pixels = 0;
    int             height, width;
    
    group = tabButton->sheet->group;
    height = tabButton->labelSize.y;
    width = tabButton->labelSize.x;
    
    InitializeMultithreadingIfNecessary();
    CmtGetThreadLocalVar (gThreadLocalHandle, &pTlv);
        
    errChk( TabButton_GetTextCanvas(&pTlv->bitmapPanel, &pTlv->bitmapCanvas, tabButton->labelSize));
    CanvasClear(pTlv->bitmapPanel, pTlv->bitmapCanvas, VAL_ENTIRE_OBJECT);
    
    /*DisplayPanel(panel);*/
    
    TabButton_DrawText(tabButton, pTlv->bitmapPanel, pTlv->bitmapCanvas, MakePoint(0,0));
    nullChk( pixels = (int *)calloc(height * width * sizeof(int), 1));
    CanvasGetPixels (pTlv->bitmapPanel, pTlv->bitmapCanvas, MakeRect(0,0, height, width), pixels);

    switch (group->buttonPosition)
        {
        case VAL_EASY_TAB_BTNS_ON_TOP:
        case VAL_EASY_TAB_BTNS_ON_BOTTOM:
            break;
        case VAL_EASY_TAB_BTNS_ON_RIGHT:
            errChk( RotatePixels90Degrees(&pixels, &height, &width, FALSE));
            break;
        case VAL_EASY_TAB_BTNS_ON_LEFT:
            errChk( RotatePixels90Degrees(&pixels, &height, &width, TRUE));
            break;
        }
        
    errChk( CreateTransparentOneColorBitmap(pixels, group->textColor, height, width, &tabButton->bitmap));
    
Error:
    free(pixels);
    return error;
}

/*************************************************************/

static void TabButton_DrawText(TabButton tabButton, int panel, int canvas, Point textPosition)
{
    TabGroup    group;
    
    group = tabButton->sheet->group;

    SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, group->textColor);
    SetCtrlAttribute(panel, canvas, ATTR_PEN_FILL_COLOR, VAL_TRANSPARENT);
    CanvasDrawTextAtPoint(panel, canvas, tabButton->displayLabel, group->metaFont, textPosition, VAL_UPPER_LEFT);
    
    if (tabButton->acceleratorKey != 0)
        {
        Point   start = MakePoint(textPosition.x + tabButton->underlineOffset, textPosition.y + tabButton->labelSize.y - 1);
        Point   end =   MakePoint(textPosition.x + tabButton->underlineOffset + tabButton->underlineWidth - 1, start.y);

        if (end.x <= start.x)
            end.x = start.x + 2;
        CanvasDrawLine(panel, canvas, start, end);
        }
}

/*************************************************************/

static void TabButton_DrawLabel(TabButton tabButton, Rect bounds)
{
    Point       textPosition;
    TabGroup    group;
    int         panel;
    int         canvas;
    
    group = tabButton->sheet->group;
    panel = group->dialogPanel;
    canvas = group->canvas;
    
    switch (group->buttonPosition)
        {
        case VAL_EASY_TAB_BTNS_ON_TOP:
            textPosition.x = bounds.left + (bounds.width - tabButton->labelSize.x) / 2;
            textPosition.y = bounds.top + TABBUTTON_TOP_MARGIN;
            break;
        case VAL_EASY_TAB_BTNS_ON_RIGHT:
            textPosition.x = bounds.left + bounds.width - TABBUTTON_TOP_MARGIN - tabButton->labelSize.y - 1;
            textPosition.y = bounds.top + (bounds.height - tabButton->labelSize.x) / 2;
            break;
        case VAL_EASY_TAB_BTNS_ON_LEFT:
            textPosition.x = bounds.left + TABBUTTON_TOP_MARGIN;
            textPosition.y = bounds.top  + (bounds.height - tabButton->labelSize.x) / 2;
            break;
        case VAL_EASY_TAB_BTNS_ON_BOTTOM:
            textPosition.y = bounds.top + bounds.height - TABBUTTON_TOP_MARGIN - tabButton->labelSize.y - 2;
            textPosition.x = bounds.left + (bounds.width - tabButton->labelSize.x) / 2;
            break;
        }
        
    switch (group->buttonPosition)
        {
        case VAL_EASY_TAB_BTNS_ON_RIGHT:
        case VAL_EASY_TAB_BTNS_ON_LEFT:
            if (tabButton->bitmap == 0)
                TabButton_CreateTextBitmap(tabButton);
            break;
        }

    if (tabButton->bitmap != 0)
        CanvasDrawBitmap (panel, canvas, tabButton->bitmap, VAL_ENTIRE_OBJECT, 
                        MakeRect(textPosition.y, textPosition.x, VAL_KEEP_SAME_SIZE, VAL_KEEP_SAME_SIZE));
    else
        TabButton_DrawText(tabButton, panel, canvas, textPosition);
        
    if (tabButton->dimmed)
        {
        int bgColor, parent;

        GetPanelAttribute (tabButton->sheet->panel, ATTR_PANEL_PARENT, &parent);
        if (parent)
            GetPanelAttribute (tabButton->sheet->panel, ATTR_TITLE_BACKCOLOR, &bgColor);
        else
            GetPanelAttribute (tabButton->sheet->panel, ATTR_BACKCOLOR, &bgColor);
        SetCtrlAttribute (panel, canvas, ATTR_PEN_FILL_COLOR, bgColor);
        CanvasDimRect (panel, canvas, bounds);
        }
}

/*************************************************************/

static void TabButton_Draw(TabButton tabButton, int drawActive, int drawFocus)
{
    TabGroup    group;
    int         canvas;
    int         panel, parent;
    Rect        bounds;
    Rect        fillBounds;
    int         buttonRight;
    int         buttonBottom;
    int         bgColor;
    int         highlightColor;
    int         shadowColor;
    int         darkShadowColor;
    static const unsigned char    checkerPattern[8]   = {0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55};
    static const unsigned char    solidPattern[8]     = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    
    group = tabButton->sheet->group;
    panel = group->dialogPanel;
    canvas = group->canvas;
                                                               
    GetPanelAttribute (tabButton->sheet->panel, ATTR_PANEL_PARENT, &parent);
    if (parent)
        GetPanelAttribute (tabButton->sheet->panel, ATTR_TITLE_BACKCOLOR, &bgColor);
    else
        GetPanelAttribute (tabButton->sheet->panel, ATTR_BACKCOLOR, &bgColor);
    Get3dBorderColors (bgColor & 0xFFFFFF, &highlightColor, &bgColor, &shadowColor, &darkShadowColor);
    
    if (drawActive) 
        bounds = tabButton->activeBounds;
    else
        bounds = tabButton->inactiveBounds;
    buttonRight = bounds.left + bounds.width - 1;
    buttonBottom = bounds.top + bounds.height - 1;
    
    switch (tabButton->sheet->group->buttonPosition)
        {
        case VAL_EASY_TAB_BTNS_ON_TOP:
                /*  shrink fill so that we don't draw outside of the rounded corners at the top of the tab button */
            SetCtrlAttribute(panel, canvas, ATTR_PEN_FILL_COLOR, bgColor);
            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, bgColor);
            RectSet(&fillBounds, bounds.top+1, bounds.left+1, bounds.height-1, bounds.width-2);
            CanvasDrawRect(panel, canvas, fillBounds, VAL_DRAW_FRAME_AND_INTERIOR);

            TabButton_DrawLabel(tabButton, bounds);
    
            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, highlightColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(bounds.left, buttonBottom));
            CanvasDrawLineTo(panel, canvas, MakePoint(bounds.left, bounds.top + 2));
            CanvasDrawLineTo(panel, canvas, MakePoint(bounds.left + 2, bounds.top));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight - 2, bounds.top));

            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, darkShadowColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(buttonRight - 1, bounds.top + 1));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight, bounds.top + 2));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight, buttonBottom));

            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, shadowColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(buttonRight - 1, bounds.top + 2));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight - 1, buttonBottom));
            break;
        case VAL_EASY_TAB_BTNS_ON_BOTTOM:
                /*  shrink fill so that we don't draw outside of the rounded corners at the top of the tab button */
            SetCtrlAttribute(panel, canvas, ATTR_PEN_FILL_COLOR, bgColor);
            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, bgColor);
            RectSet(&fillBounds, bounds.top, bounds.left + 1, bounds.height - 2, bounds.width - 2);
            CanvasDrawRect(panel, canvas, fillBounds, VAL_DRAW_FRAME_AND_INTERIOR);

            TabButton_DrawLabel(tabButton, bounds);
    
            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, highlightColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(bounds.left, bounds.top));
            CanvasDrawLineTo(panel, canvas, MakePoint(bounds.left, buttonBottom - 2));
            CanvasDrawLineTo(panel, canvas, MakePoint(bounds.left + 1, buttonBottom - 1));

            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, darkShadowColor);
            CanvasDrawPoint(panel, canvas, MakePoint(bounds.left + 1, buttonBottom - 1));
            CanvasSetPenPosition(panel, canvas, MakePoint(bounds.left + 2, buttonBottom));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight - 2, buttonBottom));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight, buttonBottom - 2));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight, bounds.top));

            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, shadowColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(buttonRight - 1, bounds.top));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight - 1, buttonBottom - 2));
            CanvasSetPenPosition(panel, canvas, MakePoint(bounds.left + 2, buttonBottom - 1));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight - 2, buttonBottom - 1));
            break;
        case VAL_EASY_TAB_BTNS_ON_RIGHT:
                /*  shrink fill so that we don't draw outside of the rounded corners at the top of the tab button */
            SetCtrlAttribute(panel, canvas, ATTR_PEN_FILL_COLOR, bgColor);
            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, bgColor);
            RectSet(&fillBounds, bounds.top+1, bounds.left, bounds.height-2, bounds.width-1);
            CanvasDrawRect(panel, canvas, fillBounds, VAL_DRAW_FRAME_AND_INTERIOR);

            TabButton_DrawLabel(tabButton, bounds);
    
            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, highlightColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(bounds.left, bounds.top));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight - 2, bounds.top));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight, bounds.top + 2));

            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, darkShadowColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(bounds.left, buttonBottom));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight - 2, buttonBottom));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight, buttonBottom - 2));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight, bounds.top + 2));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight - 1, bounds.top + 1));

            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, shadowColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(bounds.left - 1, buttonBottom - 1));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight - 2, buttonBottom - 1));
            CanvasSetPenPosition(panel, canvas, MakePoint(buttonRight - 1, buttonBottom - 2));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight - 1, bounds.top + 2));
            break;
        case VAL_EASY_TAB_BTNS_ON_LEFT:
                /*  shrink fill so that we don't draw outside of the rounded corners at the top of the tab button */
            SetCtrlAttribute(panel, canvas, ATTR_PEN_FILL_COLOR, bgColor);
            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, bgColor);
            RectSet(&fillBounds, bounds.top+1, bounds.left+1, bounds.height-2, bounds.width-1);
            CanvasDrawRect(panel, canvas, fillBounds, VAL_DRAW_FRAME_AND_INTERIOR);

            TabButton_DrawLabel(tabButton, bounds);
    
            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, highlightColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(buttonRight, bounds.top));
            CanvasDrawLineTo(panel, canvas, MakePoint(bounds.left + 2, bounds.top));
            CanvasDrawLineTo(panel, canvas, MakePoint(bounds.left, bounds.top + 2));
            CanvasDrawLineTo(panel, canvas, MakePoint(bounds.left, buttonBottom - 2));

            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, darkShadowColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(buttonRight, buttonBottom));
            CanvasDrawLineTo(panel, canvas, MakePoint(bounds.left + 2, buttonBottom));
            CanvasDrawLineTo(panel, canvas, MakePoint(bounds.left + 1, buttonBottom - 1));

            SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, shadowColor);
            CanvasSetPenPosition(panel, canvas, MakePoint(bounds.left + 2, buttonBottom - 1));
            CanvasDrawLineTo(panel, canvas, MakePoint(buttonRight, buttonBottom - 1));
            break;
        }
    
    if (drawFocus)
        {
        Rect    focusRect;
            
            /* Draw the dotted rectangle which indicates keyboard focus */
        switch (group->buttonPosition)
            {
            case VAL_EASY_TAB_BTNS_ON_TOP:
                RectSet(&focusRect, bounds.top + 3, bounds.left + 3, bounds.height - 4, bounds.width - 6);
                break;
            case VAL_EASY_TAB_BTNS_ON_RIGHT:
                RectSet(&focusRect, bounds.top + 3, bounds.left + 1, bounds.height - 6, bounds.width - 5);
                break;
            case VAL_EASY_TAB_BTNS_ON_LEFT:
                RectSet(&focusRect, bounds.top + 3, bounds.left + 3, bounds.height - 7, bounds.width - 5);
                break;
            case VAL_EASY_TAB_BTNS_ON_BOTTOM:
                RectSet(&focusRect, bounds.top + 1, bounds.left + 3, bounds.height - 4, bounds.width - 6);
                break;
            }
            
        SetCtrlAttribute(panel, canvas, ATTR_PEN_FILL_COLOR, bgColor);
        SetCtrlAttribute(group->dialogPanel, group->canvas, ATTR_PEN_PATTERN, checkerPattern);
        DrawRectFrameWithFill (group->dialogPanel, group->canvas, focusRect);
        SetCtrlAttribute(group->dialogPanel, group->canvas, ATTR_PEN_PATTERN, solidPattern);
        }
}

/*************************************************************/

    /* Frame a rect, but use interior drawing so pen patterns will apply (this is MS Windows fault!) */
static void DrawRectFrameWithFill(int panel, int canvas, Rect r)
{
    Rect fillRect;
    
    RectSet(&fillRect, r.top, r.left, 1, r.width - 1);
    CanvasDrawRect (panel, canvas, fillRect, VAL_DRAW_INTERIOR);
    RectSet(&fillRect, r.top, r.left, r.height - 1, 1);
    CanvasDrawRect (panel, canvas, fillRect, VAL_DRAW_INTERIOR);
    RectSet(&fillRect, r.top + r.height - 1, r.left, 1, r.width);
    CanvasDrawRect (panel, canvas, fillRect, VAL_DRAW_INTERIOR);
    RectSet(&fillRect, r.top, r.left + r.width - 1, r.height, 1);
    CanvasDrawRect (panel, canvas, fillRect, VAL_DRAW_INTERIOR);
}

/*************************************************************/

    /*  returns a Rect with a top and left of zero and a height and width set to the minimum
        height and width a tab button can be set to before its label starts to be obscured.
    */
static void TabButton_GetMinBounds(TabButton tabButton, int active, Rect *r)
{
    if (active)
        RectSet(r, 0, 0, tabButton->minSize.y + TABBUTTON_ACTIVE_EXTRA_TOP ,
                tabButton->minSize.x + TABBUTTON_ACTIVE_EXTRA_LEFT + TABBUTTON_ACTIVE_EXTRA_RIGHT);
    else
        RectSet(r, 0, 0, tabButton->minSize.y, tabButton->minSize.x);
}

/*************************************************************/

    /*  Sets the "inactive bounds", the button automatically grows when activated.
        The caller should call TabButton_GetMinBounds to get the smallest rect
        the button can be set to before its label starts to become obscured.
    */
static void TabButton_SetBounds(TabButton tabButton, Rect r)
{
    int dt = 0, dl = 0, dh = 0, dw = 0;

    tabButton->inactiveBounds = r;
    
    switch (tabButton->sheet->group->buttonPosition)
        {
        case VAL_EASY_TAB_BTNS_ON_TOP:
            dt = 0 - TABBUTTON_ACTIVE_EXTRA_TOP;
            dl = 0 - TABBUTTON_ACTIVE_EXTRA_LEFT;
            dh = TABBUTTON_ACTIVE_EXTRA_TOP;
            dw = TABBUTTON_ACTIVE_EXTRA_LEFT + TABBUTTON_ACTIVE_EXTRA_RIGHT;
            break;
        case VAL_EASY_TAB_BTNS_ON_BOTTOM:
            dt = 0;
            dl = 0 - TABBUTTON_ACTIVE_EXTRA_LEFT;
            dh = TABBUTTON_ACTIVE_EXTRA_TOP;
            dw = TABBUTTON_ACTIVE_EXTRA_LEFT + TABBUTTON_ACTIVE_EXTRA_RIGHT;
            break;
        case VAL_EASY_TAB_BTNS_ON_RIGHT:
            dt = 0 - TABBUTTON_ACTIVE_EXTRA_LEFT;
            dl = 0;
            dh = TABBUTTON_ACTIVE_EXTRA_LEFT + TABBUTTON_ACTIVE_EXTRA_RIGHT;
            dw =  TABBUTTON_ACTIVE_EXTRA_TOP;
            break;
        case VAL_EASY_TAB_BTNS_ON_LEFT:
            dt = 0 - TABBUTTON_ACTIVE_EXTRA_LEFT;
            dl = 0 - TABBUTTON_ACTIVE_EXTRA_TOP;
            dh = TABBUTTON_ACTIVE_EXTRA_LEFT + TABBUTTON_ACTIVE_EXTRA_RIGHT;
            dw =  TABBUTTON_ACTIVE_EXTRA_TOP;
            break;
        }
    
    RectSet(&tabButton->activeBounds, r.top + dt, r.left + dl, r.height + dh, r.width + dw);
}

/*************************************************************/

    /*  Gets the "inactive bounds", the button automatically grows when activated.
    */
static Rect TabButton_GetBounds(TabButton tabButton)
{
    return tabButton->inactiveBounds;
}

/*************************************************************/

static int Sheet_Create(TabGroup group, int sheetPanel, int row, char *label, ListType ctrlList, Sheet *sheetToReturn)
{
    int     error = 0;
    Sheet   sheet = 0;
    int     parentPanel;

    nullChk( sheet = (Sheet)calloc(sizeof(*sheet), 1));

    sheet->signature = SHEET_SIGNATURE;
    sheet->panel = sheetPanel;
    sheet->originalRow = row;
    sheet->group = group;
    sheet->hidden = FALSE;
    nullChk( sheet->tabButton = TabButton_Create(sheet, label));

    sheet->ownsPanel = group->dialogPanel != sheetPanel;
    
    if (sheet->ownsPanel)
        {
        GetPanelAttribute(sheet->panel, ATTR_PANEL_PARENT, &parentPanel);
        
        if (parentPanel != group->dialogPanel)
            errChk( ToolErr_PanelNotAChildOfCorrectPanel);

        GetPanelAttribute(sheet->panel, ATTR_WIDTH, &sheet->originalWidth);
        GetPanelAttribute(sheet->panel, ATTR_HEIGHT, &sheet->originalHeight);
            
        InstallActivationHooks(sheet->panel);
        errChk( SetPanelAttribute (sheetPanel, ATTR_PARENT_SHARES_SHORTCUT_KEYS, TRUE));
        errChk( SetPanelAttribute(sheetPanel, ATTR_FRAME_STYLE, VAL_HIDDEN_FRAME));
        errChk( SetPanelAttribute (sheetPanel, ATTR_TITLEBAR_VISIBLE, 0));
        errChk( ChainPanelCallback(sheetPanel, Sheet_PanelCallback, (void *)sheet, "Sheet Panel")); 
        }
    else
        SetAttributeForList (sheet->panel, ctrlList, ATTR_VISIBLE, FALSE);

    EasyTab_InvalRowLists(group);       /* make sure this sheet gets added to the rowLists */
    sheet->ctrlList = ctrlList;
        
Error:
    if (error < 0)
        {
        Sheet_Dispose(sheet);
        *sheetToReturn = 0;
        }
    else
        {
        *sheetToReturn = sheet;
        }
    return error;
}

/*************************************************************/

static int IsSheetPanel(int panel)
{
    void *callbackData;
    
    return GetChainedPanelCallbackData (panel, "Sheet Panel", &callbackData) >= 0;
}

/*************************************************************/

static void Sheet_Dispose(Sheet sheet)
{
    if (sheet)
        {
        if (sheet->panel > 0)
            UnchainPanelCallback(sheet->panel, "Sheet Panel");
        if (sheet->ownsPanel)
            DiscardPanelNoBOLE(sheet->panel, FALSE);
        TabButton_Dispose(sheet->tabButton);
        ListDispose(sheet->ctrlList);
        free(sheet);
        }
}

/******************************************************************/

    /* invalidate the display of every easy tab control on a panel (does not recurse into child panels) */
static void EasyTab_InvalDisplayForPanel(int panel)
{
    int         ctrl;
    TabGroup    group ; 
    
    GetPanelAttribute(panel, ATTR_PANEL_FIRST_CTRL, &ctrl);
    
    while (ctrl > 0)
        {
        if (EasyTab_IdToGroup(panel, ctrl, &group) >= 0)
            EasyTab_InvalDisplay(group);

        GetCtrlAttribute(panel, ctrl, ATTR_NEXT_CTRL, &ctrl);
        }
}

/******************************************************************/

    /* This is shared by all the tab controls on a panel */
static int CVICALLBACK EasyTab_PanelCallback (int panel, int event, void *callbackData, int eventData1, int eventData2)
{
    int         returnVal = FALSE, ctrl, scaleContents;
    TabGroup    group; 
    Rect        canvasRect;
    
    switch (event)
        {
        case EVENT_PANEL_SIZE:
            GetPanelAttribute (panel, ATTR_SCALE_CONTENTS_ON_RESIZE, &scaleContents);
            if (scaleContents)
                {
                GetPanelAttribute (panel, ATTR_PANEL_FIRST_CTRL, &ctrl);
                while (ctrl > 0) {
                    if (EasyTab_IdToGroup (panel, ctrl, &group) >= 0)
                        {
                        GetCtrlAttribute (panel, ctrl, ATTR_TOP, &canvasRect.top);
                        GetCtrlAttribute (panel, ctrl, ATTR_LEFT, &canvasRect.left);
                        GetCtrlAttribute (panel, ctrl, ATTR_HEIGHT, &canvasRect.height);
                        GetCtrlAttribute (panel, ctrl, ATTR_WIDTH, &canvasRect.width);
                        EasyTab_SetExteriorRect (group, canvasRect);
                        }
                    GetCtrlAttribute (panel, ctrl, ATTR_NEXT_CTRL, &ctrl);
                    }

                }
            break;
        case EVENT_GOT_FOCUS:
        case EVENT_LOST_FOCUS:
            EasyTab_InvalDisplayForPanel(panel);        /* dotted focus rect probably needs to be updated */
            break;
        case EVENT_KEYPRESS:
            if (HandleTabKey(eventData1, &returnVal))
                break;
            else
            if (EasyTab_HandleAcceleratorKey(eventData1, &returnVal))
                break;
//            else
//            if (HandleOtherKeys(eventData1, &returnVal))
//                break;
            break;
        }

    return returnVal;
}

/******************************************************************/

static int CVICALLBACK Sheet_PanelCallback (int panel, int event, void *callbackData, int eventData1, int eventData2)
{
    int     returnVal = FALSE;
    Sheet   sheet = (Sheet)callbackData;    
    
    UNUSED(panel);
    
    switch (event)
        {
        case EVENT_KEYPRESS:
            if (HandleTabKey(eventData1, &returnVal))
                break;
            break;
        case EVENT_LOST_FOCUS:
        case EVENT_GOT_FOCUS:
            returnVal = TRUE;
            break;
        }

    return returnVal;
}

/******************************************************************/

static int CVICALLBACK EasyTab_CanvasCallback (int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
    TabGroup    group = (TabGroup)callbackData;
    int         returnVal = FALSE;
    int         locked = FALSE;

    if (!group->valid)
        return 0;

    CmtGetLock(group->lockHandle);
    locked = TRUE;

    UNUSED(panel);
    UNUSED(control);

    switch (event)
        {
        case EVENT_DISCARD:
            CmtReleaseLock(group->lockHandle);      /* Must release lock before EasyTab_Dispose(). */
            locked = FALSE;
            EasyTab_Dispose(group);
            break;
        case EVENT_LEFT_CLICK:
        case EVENT_LEFT_DOUBLE_CLICK:
            EasyTab_HandleLeftClick(group, MakePoint(eventData2, eventData1));
            returnVal = 1;   /* don't let canvas become active just because of click */
            /*break;*/
        case EVENT_GOT_FOCUS:
        case EVENT_LOST_FOCUS:
            EasyTab_InvalDisplay(group);    /*  need to redraw dotted focus rect */
            break;
        case EVENT_KEYPRESS:
            if (HandleTabKey(eventData1, &returnVal))
                break;
            if (EasyTab_HandleNavigationKeys(group, eventData1, &returnVal))
                break;
            break;
        }

    if (locked)
        CmtReleaseLock(group->lockHandle);
        
    return returnVal;
}

/*************************************************************/

    /*  returns TRUE if the key was handled, FALSE otherwise */
static int EasyTab_HandleNavigationKeys(TabGroup group, int key, int *callbackReturnVal)
{
    int     keyHandled = TRUE;
    Sheet   sheet;
    
    UNUSED(callbackReturnVal);

    if ((key & VAL_VKEY_MASK) == VAL_HOME_VKEY)
        {
        sheet = EasyTab_IndexToSheet(group, 1);
        if (sheet)
            EasyTab_SetActive(group, sheet);
        }
    else
    if ((key & VAL_VKEY_MASK) == VAL_END_VKEY)
        {
        sheet = EasyTab_IndexToSheet(group, EasyTab_NumSheets(group));
        if (sheet)
            EasyTab_SetActive(group, sheet);
        }
    else            
    if ((key & VAL_VKEY_MASK) == VAL_LEFT_ARROW_VKEY)
        EasyTab_ActivateHorizontalNeighbor(group, TRUE, FALSE);
    else
    if ((key & VAL_VKEY_MASK) == VAL_RIGHT_ARROW_VKEY)
        EasyTab_ActivateHorizontalNeighbor(group, FALSE, FALSE);    
    else
    if ((key & VAL_VKEY_MASK) == VAL_UP_ARROW_VKEY)
        EasyTab_ActivateVerticalNeighbor(group);
    else
    if ((key & VAL_VKEY_MASK) == VAL_DOWN_ARROW_VKEY
        && (group->buttonPosition == VAL_EASY_TAB_BTNS_ON_BOTTOM))
        EasyTab_ActivateVerticalNeighbor(group);
    else
        keyHandled = FALSE;
        
    if (keyHandled)
        ProcessDrawEvents();    /* make sure we see everything when keys are auto-repeating */
        
    return keyHandled;
}

/*************************************************************/

static void EasyTab_HandleLeftClick(TabGroup group, Point p)
{
    TabButton   clickTarget;
    Sheet       activeSheet;
    int         hadFocus;
    
    p.x -= group->clientRect.left; /* map point from panel coordinates to canvas coordinates */
    p.y -= group->clientRect.top;
    
    clickTarget = EasyTab_TabButtonAtPoint(group, p);
    
    if (clickTarget && !clickTarget->dimmed)
        {
        hadFocus = EasyTab_HasFocus(group);
        activeSheet = EasyTab_GetActive(group);
        
        EasyTab_SetActive(group, clickTarget->sheet);
        if (activeSheet == clickTarget->sheet)
            EasyTab_SetFocus(group);
        else
        if (!hadFocus)
            Sheet_ActivateFirstCtrl(clickTarget->sheet);
        }
}

/*************************************************************/
    
    /*  The point should be in canvas coordinates */
static TabButton EasyTab_TabButtonAtPoint(TabGroup group, Point p)
{
    int         numRows;
    int         numSheets;
    int         rowIndex;
    int         sheetIndex;
    ListType    sheetList;
    ListType    rowLists;
    Sheet       sheet;
    Sheet       activeSheet;
    
    activeSheet = EasyTab_GetActive(group); 
    numRows = EasyTab_CalculateNumRows(group);
    
        /* check active button, its the top most button */
    if (activeSheet && RectContainsPoint(activeSheet->tabButton->activeBounds, p))
        return activeSheet->tabButton;

        /* check from the topmost row to the bottommost row in the ZPLANE order */
    rowLists = EasyTab_GetRowLists(group);
    if (rowLists)
        for (rowIndex = numRows; rowIndex >= 1; rowIndex--)
            {
            ListGetItem(rowLists, &sheetList, rowIndex);
        
            numSheets = ListNumItems(sheetList);
            for (sheetIndex = 1; sheetIndex <= numSheets; sheetIndex++)
                {
                ListGetItem(sheetList, &sheet, sheetIndex);
                if (sheet != activeSheet && RectContainsPoint(sheet->tabButton->inactiveBounds, p))
                    return sheet->tabButton;
                }
            }

    return NULL;
}

/*************************************************************/

static void EasyTab_Dispose(TabGroup group)
{
    Sheet   sheet;
    
    if (group)
        {
        if (group->valid)
            {
            group->valid = FALSE;
            CmtDiscardLock(group->lockHandle);
            }
        
        if (group->sheetList)
            {
            while (ListNumItems(group->sheetList) > 0)
                {
                ListRemoveItem(group->sheetList, &sheet, END_OF_LIST);
                Sheet_Dispose(sheet);
                }
            ListDispose(group->sheetList);
            }
    
        if (group->dialogPanel > 0)
            {
            if (group->canvas > 0)
                UnchainCtrlCallback (group->dialogPanel, group->canvas, "Sheet Group");
            UnchainPanelCallback(group->dialogPanel, "Sheet Group Panel");
            }

        DiscardCtrlNoBOLE(group->dialogPanel, group->canvas, TRUE);
        EasyTab_FreeRowLists(group);
        DeRegisterExistence(group);
        free(group);
        }
}

/*******************************************************************/

int CVIFUNC EasyTab_ConvertFromCanvas(int panel, int canvas)
{
    int error = 0;
    int style;

    errChk( GetCtrlAttribute(panel, canvas, ATTR_CTRL_STYLE, &style));
    if (style != CTRL_CANVAS)
        errChk( UIEInvalidControlType);

    errChk( EasyTab_CreateFromCanvas(panel, canvas));

Error:
    if (error < 0)
        return error;
    else
        return canvas;
}

/*******************************************************************/

int CVIFUNC EasyTab_Create(int panel, int top, int left, CtrlCallbackPtr callback, void *callbackData)
{
    int error = 0;
    int canvas = 0;
    
    errChk( canvas = NewCtrl (panel, CTRL_CANVAS, "", top, left));
    SetCtrlAttribute(panel, canvas, ATTR_WIDTH, 20);
    SetCtrlAttribute(panel, canvas, ATTR_HEIGHT, 20);
    errChk( InstallCtrlCallback(panel, canvas, callback, callbackData));
    errChk( EasyTab_CreateFromCanvas(panel, canvas));
    
Error:
    if (error < 0)
        {
        DiscardCtrlNoBOLE(panel, canvas, TRUE);
        return error;
        }
    else
        return canvas;
}

/*******************************************************************/

    /*  Note: this function doesn't return the group, but the caller can call 
        EasyTab_IdToGroup on the canvas to retrieve it if needed.
    */
static int EasyTab_CreateFromCanvas(int panel, int canvas)
{
    int         error = 0;
    TabGroup    group = 0;
    Rect        canvasRect;
    int         locked = FALSE;

    nullChk( group = (TabGroup)calloc(1, sizeof(*group)));
    errChk( CmtNewLock(NULL, 0, &group->lockHandle) );
    errChk( CmtGetLock(group->lockHandle) );
    locked = TRUE;
    group->valid = TRUE;

    SetCtrlAttribute (panel, canvas, ATTR_VISIBLE, FALSE);
    SetCtrlAttribute (panel, canvas, ATTR_DRAW_POLICY, VAL_MARK_FOR_UPDATE);

    group->signature = EASY_TAB_SIGNATURE;
    group->dialogPanel = panel;
    group->canvas = canvas;
    group->buttonGap = 0;
    group->buttonPosition = VAL_EASY_TAB_BTNS_ON_TOP;
    group->hidden = FALSE;
    group->overrideActivation = FALSE;
    GetPanelAttribute(panel, ATTR_BACKCOLOR, &group->bgColor);

    InstallActivationHooks(panel);
    errChk( ChainCtrlCallback (panel, canvas, EasyTab_CanvasCallback, (void *)group, "Sheet Group"));
    error = ChainPanelCallback(panel, EasyTab_PanelCallback, NULL, "Sheet Group Panel");    
    if (error == UIEInvalidControlType)  /* ok if already installed */
        error = 0;
    errChk( error);

    group->activeSheet = 0;
    strcpy(group->metaFont, VAL_DIALOG_META_FONT);

    nullChk( group->sheetList = ListCreate(sizeof(Sheet)));
    EasyTab_InvalRowLists(group);           /* a noop really, they are already NULL */
    nullChk( RegisterExistence(group));
    
    GetCtrlAttribute(panel, canvas, ATTR_TOP, &canvasRect.top);
    GetCtrlAttribute(panel, canvas, ATTR_LEFT, &canvasRect.left);
    GetCtrlAttribute(panel, canvas, ATTR_HEIGHT, &canvasRect.height);
    GetCtrlAttribute(panel, canvas, ATTR_WIDTH, &canvasRect.width);
    
    SetCtrlAttribute(panel, canvas, ATTR_LABEL_VISIBLE, FALSE);

    group->originalCanvasSize.x = canvasRect.width;
    group->originalCanvasSize.y = canvasRect.height;
    EasyTab_SetClientRect(group, canvasRect);
    group->sizeHasBeenSet = FALSE;  /* Force an autosize on the first draw */
    EasyTab_SetTextColorToDefault(group);
    EasyTab_InvalDisplay(group);

Error:
    if (locked)
        CmtReleaseLock(group->lockHandle);
    if (error < 0)
        EasyTab_Dispose(group);

    return error;
}

/*******************************************************************/

static void EasyTab_SetTextColorToDefault(TabGroup group)
{
    int textColor = VAL_BLACK;  /* default if not conforming to system */
    int conformToSystem;
    int textMsg = 0;
        
    GetPanelAttribute(group->dialogPanel, ATTR_CONFORM_TO_SYSTEM, &conformToSystem);
    
    if (conformToSystem)
        {
            /* kludgy way to get the system dialog text color */
        textMsg = NewCtrl (group->dialogPanel, CTRL_SQUARE_COMMAND_BUTTON, "",
                           -1000, -100);
        if (textMsg > 0)
            {
            GetCtrlAttribute (group->dialogPanel, textMsg, ATTR_LABEL_COLOR, &textColor);   
            DiscardCtrl(group->dialogPanel, textMsg);
            }       
        }

    group->textColor = textColor;
    EasyTab_InvalDisplay(group);
}

/*******************************************************************/

static int EasyTab_IdToGroup(int panel, int groupCanvas, TabGroup *group)
{
    int error = 0;

    if (GetChainedCallbackData (panel, groupCanvas, "Sheet Group", (void **)group) != 0)
        errChk( UIEInvalidControlType);

Error:
    return error;
}

/*******************************************************************/

    /*  If numCtrls == VAL_TAB_SHEET_FROM_LIST, then the first parameter after numCtrls is assumed to be
        a ListType containing the control IDs of the control to include in the sheet.
        
        If numCtrls == VAL_TAB_SHEET_FROM_PANEL_HANDLE, then the first parameter after numCtrls is assumed to be
        the handle of a panel that is to be made a sheet.
        
        If numCtrls == VAL_TAB_SHEET_FROM_FILE, then the first parameter after numCtrls is assumed to be
        the file name of a uir file and the parameter after that is assumed to be the resource id of a panel
        to load and make a sheet.
        
        If numCtrls is > 0, then there are assumed to <numCtrls> number of control Id parameters
        following the numCtrls parameter.
        
        If numCtrls == 0, then the it is assumed that all the arguments following numCtrls are controlsIDs 
        until a NULL argument is found.
        
        The row parameter must be at least one and no more that one greater than the largest
        row of any sheet already in the group.
    */
    
int CVIFUNC EasyTab_AddSheet(int panel, int easyTabCtrl, char *sheetName, void *module, int row, int numCtrls, ...)
{
    int     error = 0;
    va_list parmInfo;
    
    va_start(parmInfo, numCtrls);
    errChk(EasyTab_AddSheetFromParmInfo(panel, easyTabCtrl, sheetName, module, row, numCtrls, parmInfo));

Error:
    va_end(parmInfo);
    return error;
}
    
/***********/

static int EasyTab_AddSheetFromParmInfo(int panel, int easyTabCtrl, char *sheetName, void *module, int row, int numCtrls, va_list parmInfo)
{
    int         error = 0;
    TabGroup    group = 0;
    ListType    ctrlList = 0;
    Sheet       sheet = 0;
    int         firstSheet;
    int         sheetPanel = panel;
    char        panelTitle[256];
    int         locked = FALSE;

    panelTitle[0] = 0;
    
    errChk( EasyTab_IdToGroup(panel, easyTabCtrl, &group));
    if (!group->valid)
        errChk(UIEHandleInvalid);

    errChk( CmtGetLock(group->lockHandle) );
    locked = TRUE;
    
    if (row < 0 || row > (EasyTab_CalculateNumRows(group) + 1))
        errChk( UIEValueIsInvalidOrOutOfRange);

    if (numCtrls == VAL_TAB_SHEET_FROM_LIST)
        nullChk( ctrlList = ListCopy(va_arg(parmInfo, ListType)));
    else
    if (numCtrls == VAL_TAB_SHEET_FROM_PANEL_HANDLE)
        {
        nullChk( ctrlList = ListCreate(sizeof(int)));
        sheetPanel = va_arg(parmInfo, int);
        errChk( HidePanel(sheetPanel));
        GetPanelAttribute(sheetPanel, ATTR_TITLE, panelTitle);
        }
    else
    if (numCtrls == VAL_TAB_SHEET_FROM_FILE)
        {
        char    *fileName;
        int     resId;
        nullChk( ctrlList = ListCreate(sizeof(int)));
        fileName = va_arg(parmInfo, char *);
        resId = va_arg(parmInfo, int);
        errChk( sheetPanel = LoadPanelEx (panel, fileName, resId, module));
        GetPanelAttribute(sheetPanel, ATTR_TITLE, panelTitle);
        }
    else
        errChk( GetIntListFromParmInfo(parmInfo, numCtrls, &ctrlList));


    if (!sheetName)
        sheetName = panelTitle;
        
    errChk( Sheet_Create(group, sheetPanel, row, sheetName, ctrlList, &sheet));
    ctrlList = 0;   /* now owned by the sheet */

    sheet->group = group;

    nullChk( ListInsertItem(group->sheetList, &sheet, END_OF_LIST));
    sheet = 0;  /* now owned by sheetGroup */

    firstSheet = EasyTab_NumSheets(group) == 1;
    if (firstSheet)
        EasyTab_SetActive(group, EasyTab_IndexToSheet(group, 1));

    EasyTab_InvalAll(group);
    
    if (group->sizeHasBeenSet)  /* reset client rect in case it needs to grow to accomodate the new sheet */
        EasyTab_SetClientRect(group, group->clientRect);

Error:
    if (locked)
        CmtReleaseLock(group->lockHandle);
    if (error < 0)
        {
        Sheet_Dispose(sheet);
        ListDispose(ctrlList);
        }
        
    return error;
}

/*******************************************************************/

static void CVICALLBACK EasyTab_UpdateDisplay(void *callbackData)
{
    TabGroup    group = (TabGroup)callbackData;
    
    if (ObjectExists(group) && group->displayInvalidated)
        EasyTab_Draw(group);
}

/*******************************************************************/

static void EasyTab_InvalAll(TabGroup group)
{
    EasyTab_InvalTabPositions(group);
    EasyTab_InvalCanvasSetup(group);
    EasyTab_InvalDisplay(group);
}

/*******************************************************************/

static void EasyTab_InvalDisplay(TabGroup group)
{
    if (!group->displayInvalidated)
        {
        group->displayInvalidated = TRUE;
        PostDeferredCallToThread(EasyTab_UpdateDisplay, group, CurrThreadId ());
        }
}

/*******************************************************************/

    /*  moves the canvas to the correct location with the correct size to serve as the background
        decoration for the controls on the dialog and as the drawing surface for the tab buttons.
        
        The boundary decoration is drawn into the canvas.
    */
static void EasyTab_SetupCanvas(TabGroup group)
{
    int     canvas;
    int     panel;
    int     top, left, width, height;
    double  xCoordAtOrigin, yCoordAtOrigin;
    
    if (!group->canvasSetup)
        {
        group->canvasSetup = TRUE;

        canvas = group->canvas;
        panel = group->dialogPanel;

            /* make sure we don't see any of these changes as they happen, the control will be made visible in the next EasyTab_Draw() call */
        SetCtrlAttribute(panel, canvas, ATTR_VISIBLE, FALSE);
        
        top = group->frameRect.top;
        left = group->frameRect.left; 
        height = group->frameRect.height;
        width = group->frameRect.width;
        xCoordAtOrigin = 0;
        yCoordAtOrigin = 0;
        
        switch (group->buttonPosition)
            {
            case VAL_EASY_TAB_BTNS_ON_TOP:
                top -= group->tabButtonBoundsUnion.height;
                height += group->tabButtonBoundsUnion.height;
                yCoordAtOrigin = group->tabButtonBoundsUnion.top;
                break;
            case VAL_EASY_TAB_BTNS_ON_BOTTOM:
                height += group->tabButtonBoundsUnion.height;
                break;
            case VAL_EASY_TAB_BTNS_ON_RIGHT:
                width += group->tabButtonBoundsUnion.width;
                break;
            case VAL_EASY_TAB_BTNS_ON_LEFT:
                left -= group->tabButtonBoundsUnion.width; 
                width += group->tabButtonBoundsUnion.width;
                xCoordAtOrigin = group->tabButtonBoundsUnion.left;
                break;
            }
            
            /*  Set canvas the size to encompass the client area and the space for the tab buttons */
        SetCtrlAttribute(panel, canvas, ATTR_TOP, top);
        SetCtrlAttribute(panel, canvas, ATTR_LEFT, left);
        SetCtrlAttribute(panel, canvas, ATTR_HEIGHT, height); 
        SetCtrlAttribute(panel, canvas, ATTR_WIDTH, width);
        
        group->exteriorRect.top = top;
        group->exteriorRect.left = left;
        group->exteriorRect.height = height;
        group->exteriorRect.width = width;

        SetCtrlAttribute(panel, canvas, ATTR_PICT_BGCOLOR, group->bgColor);

            /* clear the canvas control */
        SetCtrlAttribute(panel, canvas, ATTR_YCOORD_AT_ORIGIN, 0.0);
        SetCtrlAttribute(panel, canvas, ATTR_XCOORD_AT_ORIGIN, 0.0);
        CanvasClear(panel, canvas, VAL_ENTIRE_OBJECT);

            /* set the origin of the canvas to be the top left of where the frame should be drawn */
        SetCtrlAttribute(panel, canvas, ATTR_YCOORD_AT_ORIGIN, yCoordAtOrigin);
        SetCtrlAttribute(panel, canvas, ATTR_XCOORD_AT_ORIGIN, xCoordAtOrigin);
        }
}

/*******************************************************************/

    /*  Draw the 3d frame around the client area */
static void EasyTab_DrawFrame(TabGroup group)
{
    int     frameBottom, frameRight;  
    int     canvas;
    int     panel;
    Sheet   activeSheet;
    int     shadowColor;
    int     highlightColor;
    int     darkShadowColor;
    int     bgColor;

    activeSheet = EasyTab_GetActive(group);
    
    GetPanelAttribute(activeSheet ? activeSheet->panel : group->dialogPanel, ATTR_BACKCOLOR, &bgColor);
        
    Get3dBorderColors (bgColor, &highlightColor, 0, &shadowColor, &darkShadowColor);
    canvas = group->canvas;
    panel = group->dialogPanel;
    
    frameBottom = group->frameRect.height - 1;          
    frameRight = group->frameRect.width - 1;

    SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, highlightColor);
    CanvasDrawLine(panel, canvas, MakePoint(0,0), MakePoint(0, frameBottom - 1));
    CanvasDrawLine(panel, canvas, MakePoint(0,0), MakePoint(frameRight - 1, 0));
    SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, shadowColor);
    CanvasDrawLine(panel, canvas, MakePoint(1, frameBottom - 1), MakePoint(frameRight - 1, frameBottom - 1));
    CanvasDrawLine(panel, canvas, MakePoint(frameRight - 1, 1), MakePoint(frameRight - 1, frameBottom - 1));
    SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, darkShadowColor);
    CanvasDrawLine(panel, canvas, MakePoint(0, frameBottom), MakePoint(frameRight, frameBottom));
    CanvasDrawLine(panel, canvas, MakePoint(frameRight, 0), MakePoint(frameRight, frameBottom));
    
            /*  cut out the section of the client area frame next to the active tab so
                that the tab appear to be a piece of the client area
            */
    if (activeSheet)
        {
        Rect    bounds = activeSheet->tabButton->activeBounds;
        int     buttonRight = bounds.left + bounds.width - 1;
        int     buttonBottom = bounds.top + bounds.height - 1;

        switch (group->buttonPosition)
            {
            case VAL_EASY_TAB_BTNS_ON_TOP:
                SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, bgColor);
                CanvasDrawLine(panel, canvas, MakePoint(bounds.left + 1, buttonBottom + 1), MakePoint(buttonRight - 2, buttonBottom + 1));
                SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, shadowColor);
                CanvasDrawPoint(panel, canvas, MakePoint(buttonRight - 1, buttonBottom + 1));
                SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, darkShadowColor);
                CanvasDrawPoint(panel, canvas, MakePoint(buttonRight, buttonBottom + 1));
                break;
            case VAL_EASY_TAB_BTNS_ON_BOTTOM:
                SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, bgColor);
                CanvasDrawLine(panel, canvas, MakePoint(bounds.left + 1, bounds.top - 1), MakePoint(buttonRight - 2, bounds.top - 1));
                CanvasDrawLine(panel, canvas, MakePoint(bounds.left + 1, bounds.top - 2), MakePoint(buttonRight - 2, bounds.top - 2));
                SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, shadowColor);
                CanvasDrawLine(panel, canvas, MakePoint(buttonRight - 1, bounds.top - 1), MakePoint(buttonRight - 1, bounds.top - 2));
                SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, darkShadowColor);
                CanvasDrawLine(panel, canvas, MakePoint(buttonRight, bounds.top - 1), MakePoint(buttonRight, bounds.top - 2));
                SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, highlightColor);
                CanvasDrawLine(panel, canvas, MakePoint(bounds.left, bounds.top - 1), MakePoint(bounds.left, bounds.top - 2));
                break;
            case VAL_EASY_TAB_BTNS_ON_RIGHT:
                SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, bgColor);
                CanvasDrawLine(panel, canvas, MakePoint(bounds.left - 1, bounds.top + 1), MakePoint(bounds.left - 1, buttonBottom - 1));
                CanvasDrawLine(panel, canvas, MakePoint(bounds.left - 2, bounds.top + 1), MakePoint(bounds.left - 2, buttonBottom - 1));
                if (bounds.top == 0)    /* if this is the first button (located at the top right corner of the canvas */
                    {
                    SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, highlightColor);
                    CanvasDrawPoint(panel, canvas, MakePoint(bounds.left - 1, bounds.top)); /* draw one extra pixel to match up with frame edge */
                    }
                break;
            case VAL_EASY_TAB_BTNS_ON_LEFT:
                SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, bgColor);
                CanvasDrawLine(panel, canvas, MakePoint(buttonRight + 1, bounds.top + 1), MakePoint(buttonRight + 1, buttonBottom - 2));
                if (buttonBottom == group->frameRect.height - 1)    /* if this is the first button (located at the bottom left corner of the canvas */
                    {
                    SetCtrlAttribute(panel, canvas, ATTR_PEN_COLOR, shadowColor);
                    CanvasDrawPoint(panel, canvas, MakePoint(buttonRight + 1, buttonBottom - 1));   /* draw one extra pixel to match up with frame edge */
                    }
                break;
            }
        }
}

/*******************************************************************/

static void EasyTab_InvalCanvasSetup(TabGroup group)
{   
    group->canvasSetup = FALSE;
}

/*******************************************************************/

    /*  The strange ordering of drawing, panel activation and panel hiding/showing in this function is 
        needed because some operations (like activating a panel) can cause draw events to be processed 
        immediately, which if done in the wrong place, can cause unpleasant flickering on the screen.
        
        This would be a problem if I wasn't trying to "use panels as drawing primitives" which is a 
        conceptual violation.
    */
static int EasyTab_Draw(TabGroup group)
{
    int     error = 0;
    int     index;
    int     numItems;
    Sheet   sheet;
    Sheet   activeSheet = 0;
    int     numCtrls;
    #define ATTR_VISIBLE_NO_UPDATE  20464   /* drawing seems a tad smoother using this hidden attribute (could be my imagination) */
        
    errChk( EasyTab_CalculateTabSizes(group));
    EasyTab_SetupCanvas(group);

    group->displayInvalidated = FALSE;      /* anything that can invalidated the display should be done before this to avoid unneeded double drawing */

    CanvasStartBatchDraw(group->dialogPanel, group->canvas);
    
    EasyTab_DrawTabButtons(group);
    EasyTab_DrawFrame(group);  

    activeSheet = EasyTab_GetActive(group);

    if (!group->hidden)
        {
        if (activeSheet && activeSheet->ownsPanel)
            SetPanelAttribute(activeSheet->panel, ATTR_VISIBLE_NO_UPDATE, TRUE);
        
        SetCtrlAttribute (group->dialogPanel, group->canvas, ATTR_VISIBLE, TRUE);
        }
    
Error:
    if (error < 0)
        Breakpoint();
        
    CanvasEndBatchDraw(group->dialogPanel, group->canvas);

    if (activeSheet && activeSheet->ownsPanel)
        {
        if (activeSheet->activatePanelWhenShown)
            {
            int  visible;
            
            activeSheet->activatePanelWhenShown = FALSE;
            
            if (!group->overrideActivation)
                {
                GetPanelAttribute(activeSheet->panel, ATTR_VISIBLE, &visible);  /* true only if parents are visible too */
                if (visible)    /* must check, otherwise it would make it visible when it isn't supposed to be */
                    SetActivePanel(activeSheet->panel); 
                }
            }
        }

    numItems = EasyTab_NumSheets(group);
    for (index = 1; index <= numItems; index++)
        {
        sheet = EasyTab_IndexToSheet(group, index);

        if (sheet->ownsPanel)
            {
            if (sheet != activeSheet)
                SetPanelAttribute(sheet->panel, ATTR_VISIBLE, FALSE);
            }
        else
            {
            SetAttributeForList (sheet->panel, sheet->ctrlList, ATTR_VISIBLE, activeSheet == sheet);
            if (sheet == activeSheet)       /* move sheet group background to the back so it is behind all of the visible controls */
                {
                GetPanelAttribute(group->dialogPanel, ATTR_NUM_CTRLS, &numCtrls);
                SetCtrlAttribute(group->dialogPanel, group->canvas, ATTR_ZPLANE_POSITION, numCtrls-1);
                }
            }
        }

    return error;
}

/*******************************************************************/

static void EasyTab_DrawTabButtons(TabGroup group)
{
    int         numRows;
    int         numSheets;
    int         rowIndex;
    int         sheetIndex;
    ListType    sheetList;
    ListType    rowLists;
    Sheet       sheet;
    Sheet       activeSheet;
    int         canvasWidth;
    int         hasFocus;
    
    hasFocus = EasyTab_HasFocus(group);
    activeSheet = EasyTab_GetActive(group); 
    numRows = EasyTab_CalculateNumRows(group);
    
        /* erase tab button area */
    GetCtrlAttribute(group->dialogPanel, group->canvas, ATTR_WIDTH, &canvasWidth);
    CanvasClear(group->dialogPanel, group->canvas, group->tabButtonEraseArea);
    
    rowLists = EasyTab_GetRowLists(group);

    if (rowLists)
        for (rowIndex = 1; rowIndex <= numRows; rowIndex++)
            {
            ListGetItem(rowLists, &sheetList, rowIndex);
        
            numSheets = ListNumItems(sheetList);
            for (sheetIndex = 1; sheetIndex <= numSheets; sheetIndex++)
                {
                ListGetItem(sheetList, &sheet, sheetIndex);
                if (sheet != activeSheet)
                    TabButton_Draw(sheet->tabButton, FALSE, FALSE);
                }
            }

        /* Draw active button last since it must be on top */
    if (activeSheet)
        TabButton_Draw(activeSheet->tabButton, TRUE, hasFocus);
}

/*******************************************************************/

static Sheet EasyTab_GetActive(TabGroup sheetGroup)
{
    return sheetGroup->activeSheet;
}

/*******************************************************************/

    /*  if previous is TRUE, the previous neighboring tab sheet is activated,
        otherwise, the next sheet is activated
    */
static void EasyTab_ActivateHorizontalNeighbor(TabGroup group, int previous, int wrap)
{
    Sheet   activeSheet;
    Sheet   newActiveSheet;
    int     activeIndex;
    int     newActiveIndex;
    int     numSheets;
    int     error = 0;
    int     hitEnd = FALSE;
    
    activeSheet = EasyTab_GetActive(group);
    numSheets = EasyTab_NumSheets(group);
    
    if (activeSheet)
        {
        newActiveIndex = activeIndex = EasyTab_SheetToIndex(group, activeSheet);
        
        do
            {
            if (previous)
                newActiveIndex = newActiveIndex - 1;
            else
                newActiveIndex = newActiveIndex + 1;
            
            if (wrap)
                {
                if (newActiveIndex > numSheets)
                    newActiveIndex = 1;
                else
                if (newActiveIndex <= 0)
                    newActiveIndex = numSheets;
                }
            else
            if (newActiveIndex != (int)Pin(newActiveIndex, 1, numSheets))
                {
                newActiveIndex = (int)Pin(newActiveIndex, 1, numSheets);
                hitEnd = TRUE;
                }
                
            newActiveSheet = EasyTab_IndexToSheet(group, newActiveIndex);
            }
         while (!hitEnd && newActiveSheet != activeSheet && (newActiveSheet->hidden || newActiveSheet->tabButton->dimmed));  /* skip hidden and dimmed sheets */
        
        if (newActiveSheet != activeSheet && !newActiveSheet->hidden && !newActiveSheet->tabButton->dimmed)
            EasyTab_SetActive(group, newActiveSheet);
        }
}

/*******************************************************************/

static void EasyTab_ActivateVerticalNeighbor(TabGroup group)
{
    Sheet       activeSheet;
    Sheet       newActiveSheet;
    ListType    sheetList;
    ListType    rowLists;
    int         column;
    int         numColumns;
    int         numRows;
    
    activeSheet = EasyTab_GetActive(group);
    rowLists = EasyTab_GetRowLists(group);  
    if (activeSheet && rowLists)
        {
        numRows = ListNumItems(rowLists);
        
        if (numRows > 1)
            {
            EasyTab_GetSheetDisplayPosition(group, activeSheet, NULL, &column);
            ListGetItem(rowLists, &sheetList, numRows - 1); /* get new active row */
            
            numColumns = ListNumItems(sheetList);   /* numColumns in new active row */
            
            column = (int)Pin(column, 1, numColumns);   /* goto same column if possible, else goto last column */
            
            do  /* find undimmed sheet if necessary */
                {
                ListGetItem(sheetList, &newActiveSheet, column);                    
                column--;
                }
            while (newActiveSheet->tabButton->dimmed && column > 0);
            
            if  (!newActiveSheet->tabButton->dimmed)
                EasyTab_SetActive(group, newActiveSheet);
            }
        }
}

/*******************************************************************/

static Sheet EasyTab_PanelToSheet(TabGroup group, int panel)
{
    int         index, numSheets;
    Sheet       sheet;
        
    numSheets = EasyTab_NumSheets(group);
    for (index = 1; index <= numSheets; index++)
        {
        sheet = EasyTab_IndexToSheet(group, index);
        if (sheet->ownsPanel && sheet->panel == panel)
            return sheet;
        }

    return NULL;
}

/*******************************************************************/

    /*  out of range indices are indicated by a NULL return value */
static Sheet EasyTab_IndexToSheet(TabGroup group, int index)
{
    Sheet   sheet = NULL;

    if (index > 0 && index <= EasyTab_NumSheets(group))
        ListGetItem(group->sheetList, &sheet, index);

    return sheet;
}

/*******************************************************************/

static int EasyTab_NumSheets(TabGroup group)
{
    return ListNumItems(group->sheetList);
}

/*******************************************************************/

static int EasyTab_SheetToIndex(TabGroup group, Sheet sheet)
{
    int     index ;
    int     numSheets;
    Sheet   currSheet;
    int     sheetIndex = 0;
    
    numSheets = EasyTab_NumSheets(group);

    for (index = 1; !sheetIndex && index <= numSheets; index++)
        {
        currSheet = EasyTab_IndexToSheet(group, index);
        if (currSheet == sheet)
            sheetIndex = index;
        }

    return sheetIndex;
}

/*******************************************************************/

    /*  check if the tabbuttons on a sheet group have the keyboard focus */
static int EasyTab_HasFocus(TabGroup group)
{
    int     activePanel = GetActivePanel();
    int     activeCtrl = GetActiveCtrl(group->dialogPanel);
    Sheet   activeSheet;
    
    activeSheet = EasyTab_GetActive(group);
    
    return (activePanel == group->dialogPanel) && (activeCtrl == group->canvas) && activeSheet && 
            (!activeSheet->activatePanelWhenShown || group->overrideActivation);
}

/*******************************************************************/

static void EasyTab_SetFocus(TabGroup group)
{
    int hadFocus;
    int activePanel = GetActivePanel();
    int activeCtrl = GetActiveCtrl(group->dialogPanel);
    
    hadFocus = EasyTab_HasFocus(group);
    
    if (!hadFocus)
        {
        int BOLE;
        SetActivePanel(group->dialogPanel);
        BOLE = SetBreakOnLibraryErrors (0); 
        SetActiveCtrl(group->dialogPanel, group->canvas);
        SetBreakOnLibraryErrors (BOLE);     

            /*  if the canvas isn't already active, its display will be invalidated when it gets the EVENT_GOT_FOCUS,
                otherwise, explicitly invalidate it now (this check prevents unneeded double drawing.
            */
        if (activePanel == group->dialogPanel && activeCtrl == group->canvas)
            EasyTab_InvalDisplay(group);
        }   
}

/*******************************************************************/

static void EasyTab_SetActive(TabGroup group, Sheet activeSheet)
{
    Sheet   oldActiveSheet;

    oldActiveSheet = EasyTab_GetActive(group);
    group->activeSheet = activeSheet;

    if (oldActiveSheet != group->activeSheet)
        {
        EasyTab_MoveActiveRowToEnd(group);
        EasyTab_InvalTabPositions(group);
        EasyTab_InvalDisplay(group);
        if (oldActiveSheet)
            CallPanelCallback (oldActiveSheet->panel, EVENT_TAB_CHANGED, activeSheet ? activeSheet->panel : 0, oldActiveSheet->panel, 0);
        if (activeSheet)
            CallPanelCallback (activeSheet->panel, EVENT_TAB_CHANGED, activeSheet->panel, oldActiveSheet ? oldActiveSheet->panel : 0, 0);
        CallCtrlCallback (group->dialogPanel, group->canvas, EVENT_TAB_CHANGED, activeSheet ? activeSheet->panel : 0, oldActiveSheet ? oldActiveSheet->panel : 0, 0);
        }
}

/*******************************************************************/

static Sheet EasyTab_GetVisibleInactiveSheet(TabGroup group)
{
    int     index;
    int     numSheets;
    Sheet   sheet;
    
    numSheets = EasyTab_NumSheets(group);   
    for (index = 1; index <= numSheets; index++)
        {
        ListGetItem(group->sheetList, &sheet, index);
        if ((sheet != group->activeSheet) && !sheet->hidden)
            return sheet;
        }
        
    return 0;
 }

/*******************************************************************/

/* returns true if sheet was deactivated */
static int EasyTab_SetInactive(TabGroup group, Sheet sheet)
{
    Sheet   sheetToActivate;

    if (sheet != group->activeSheet)
        return TRUE;
        
    sheetToActivate = EasyTab_GetVisibleInactiveSheet(group);
    
    if (!sheetToActivate)
        return FALSE;
        
    EasyTab_SetActive(group, sheetToActivate);
    
    return sheet != group->activeSheet;
}

/*******************************************************************/

static void Sheet_ActivateFirstCtrl(Sheet sheet)
{
    int dialogVisible;
    
    GetPanelAttribute(sheet->panel, ATTR_VISIBLE, &dialogVisible);
    
    if (sheet->ownsPanel)
        {
        if (ActivateFirstCtrl(sheet->panel) && !sheet->group->overrideActivation)
            {
            sheet->activatePanelWhenShown = TRUE;
            EasyTab_InvalDisplay(sheet->group);
            }
        else
            EasyTab_SetFocus(sheet->group);
        }
    else
        {
        int ctrlIndex, numCtrls, ctrl;
        
        numCtrls = ListNumItems(sheet->ctrlList);

        for (ctrlIndex = 1; ctrlIndex <= numCtrls; ctrlIndex++)
            {
            ListGetItem(sheet->ctrlList, &ctrl, ctrlIndex);
            if (EasyTab_CtrlCanBeTabStop(sheet->panel, ctrl))
                {
                SetActiveCtrl(sheet->panel, ctrl);
                SetActivePanel(sheet->panel);
                break;
                }
            }
        }
}

/*******************************************************************/

    /*  returns the bounds rect of all controls in the sheetGroup which reside
        on the main dialog panel (not on their own panel).
    */
static Rect EasyTab_CtrlsBoundsUnion(TabGroup group)
{
    Rect    ctrlRect;
    Rect    unionRect;
    int     numSheets;
    int     sheetIndex;
    int     numCtrls;
    int     ctrlIndex;
    Sheet   sheet;
    int     ctrl;
    int     top, left, width, height;
    
    RectSet(&unionRect, 0, 0, 0, 0);
    
    numSheets = EasyTab_NumSheets(group);
    
    for (sheetIndex = 1; sheetIndex <= numSheets; sheetIndex++)
        {
        sheet = EasyTab_IndexToSheet(group, sheetIndex);
        
        if (!sheet->ownsPanel)
            {       
            numCtrls = ListNumItems(sheet->ctrlList);
        
            for (ctrlIndex = 1; ctrlIndex <= numCtrls; ctrlIndex++)
                {
                ListGetItem(sheet->ctrlList, &ctrl, ctrlIndex);
                GetCtrlBoundingRect(sheet->panel, ctrl, &top, &left, &height,&width);
                RectSet(&ctrlRect, top, left, height, width);
                
                if (RectEmpty(unionRect))
                    unionRect = ctrlRect;
                else
                    RectUnion(ctrlRect, unionRect, &unionRect);
                }
            }
        }
    
    return unionRect;
}

/*******************************************************************/

    /*  If sizeOnly is TRUE then the union rect is computed using only the
        height and width of the panels and its top, left will be 0,0.
        
        Only child panels owned by sheets are considered.
    */
static Rect EasyTab_PanelBoundsUnion(TabGroup group, int sizeOnly, int useOriginalPanelSizes)
{
    Rect    panelBounds;
    Rect    unionRect;
    int     numSheets;
    int     sheetIndex;
    Sheet   sheet;
    int     top, left, width, height;
    
    RectSet(&unionRect, 0, 0, 0, 0);
    
    numSheets = EasyTab_NumSheets(group);
    
    for (sheetIndex = 1; sheetIndex <= numSheets; sheetIndex++)
        {
        sheet = EasyTab_IndexToSheet(group, sheetIndex);
        if (sheet->ownsPanel)
            {
            GetPanelAttribute(sheet->panel, ATTR_TOP, &top);
            GetPanelAttribute(sheet->panel, ATTR_LEFT, &left);
            
            if (useOriginalPanelSizes)
                {
                height = sheet->originalHeight;
                width = sheet->originalWidth;
                }
            else
                {
                GetPanelAttribute(sheet->panel, ATTR_HEIGHT, &height);
                GetPanelAttribute(sheet->panel, ATTR_WIDTH, &width);
                }
        
            if (sizeOnly)
                RectSet(&panelBounds, 0, 0, height, width);
            else
                RectSet(&panelBounds, top, left, height, width);
            
            if (sheetIndex == 1)
                unionRect = panelBounds;
            else
                RectUnion(panelBounds, unionRect, &unionRect);
            }
        }
    
    return unionRect;
}

/*******************************************************************/

    /*  If the specified width is less than the minimum width needed by the 
        tabbuttons, the minimum width will be used instead. (not yet enforced, should we?).
    */
static int EasyTab_SetClientRect(TabGroup group, Rect clientRect)
{
    int     error = 0;
    int     index;
    int     numSheets;
    Sheet   sheet;

    clientRect.width = Max(clientRect.width, CLIENT_MIN_WIDTH);
    clientRect.height = Max(clientRect.height, CLIENT_MIN_HEIGHT);
    
    /*  errChk( EasyTab_CalculateMinClientWidth(group, &minWidth)); we haven't decided to enforce this */
    
    group->clientRect = clientRect;
    
    numSheets = EasyTab_NumSheets(group);
    for (index = 1; index <= numSheets; index++)
        {
        sheet = EasyTab_IndexToSheet(group, index);
        if (sheet->ownsPanel)
            {
            SetPanelAttribute(sheet->panel, ATTR_TOP, clientRect.top);
            SetPanelAttribute(sheet->panel, ATTR_LEFT, clientRect.left);
            SetPanelAttribute(sheet->panel, ATTR_HEIGHT, clientRect.height);
            SetPanelAttribute(sheet->panel, ATTR_WIDTH, clientRect.width);
            }
        }
        
    group->frameRect = clientRect;
    group->frameRect.top -= CLIENT_TOP_FRAME_WIDTH; 
    group->frameRect.height += CLIENT_VERTICAL_FRAME_WIDTH; 
    group->frameRect.left -= CLIENT_LEFT_FRAME_WIDTH;   
    group->frameRect.width += CLIENT_HORIZONTAL_FRAME_WIDTH;    
    
    EasyTab_InvalAll(group);
    group->sizeHasBeenSet = TRUE;
    
    return error;
}

/*******************************************************************/

static Rect EasyTab_GetClientRect(TabGroup group)
{
    EasyTab_CalculateTabSizes(group);
    EasyTab_SetupCanvas(group);
    return group->clientRect;
}

/*******************************************************************/

static Rect EasyTab_GetExteriorRect(TabGroup group)
{
    EasyTab_CalculateTabSizes(group);
    EasyTab_SetupCanvas(group);
    return group->exteriorRect;
}

/*******************************************************************/

static int EasyTab_SetExteriorRect(TabGroup group, Rect exteriorRect)
{
    int     error = 0;
    Rect    oldExteriorRect, oldClientRect, newInteriorRect;
    int     panel;
    int     canvas;
    int     leftGap, rightGap, topGap, bottomGap;
    
    errChk( EasyTab_CalculateTabSizes(group));
    EasyTab_SetupCanvas(group);

    panel = group->dialogPanel;
    canvas = group->canvas;
    
    oldClientRect = EasyTab_GetClientRect(group);
    oldExteriorRect = EasyTab_GetExteriorRect(group);
    
    leftGap = oldClientRect.left - oldExteriorRect.left;
    rightGap = (oldExteriorRect.left + oldExteriorRect.width) - (oldClientRect.left + oldClientRect.width);
    topGap = oldClientRect.top - oldExteriorRect.top;
    bottomGap = (oldExteriorRect.top + oldExteriorRect.height) - (oldClientRect.top + oldClientRect.height);

    newInteriorRect.top = exteriorRect.top + topGap;
    newInteriorRect.left = exteriorRect.left + leftGap;
    newInteriorRect.width = exteriorRect.width - leftGap - rightGap;
    newInteriorRect.height = exteriorRect.height - topGap - bottomGap;
    
    errChk( EasyTab_SetClientRect(group, newInteriorRect));
    
Error:  
    return error;
}

/*******************************************************************/

static Rect EasyTab_GetAutoSizeRect(TabGroup group, int makeRoomForButtons)
{
    #define CTRL_BOUNDS_MARGIN  10
    Rect    clientRect;
    Rect    panelUnionRect;
    Rect    ctrlUnionRect;
    int     minBtnHeight, minBtnWidth;

    EasyTab_CalcMinButtonArea(group, &minBtnHeight, &minBtnWidth);
    
    panelUnionRect = EasyTab_PanelBoundsUnion(group, TRUE, TRUE);
    panelUnionRect.top = group->clientRect.top;
    panelUnionRect.left = group->clientRect.left;
    
    ctrlUnionRect = EasyTab_CtrlsBoundsUnion(group);


    if (!RectEmpty(ctrlUnionRect))
        RectGrow(&ctrlUnionRect, CTRL_BOUNDS_MARGIN, CTRL_BOUNDS_MARGIN);
    else
        {
        ctrlUnionRect.top = group->clientRect.top;
        ctrlUnionRect.left = group->clientRect.left;
        }

    if (RectEmpty(panelUnionRect))
        clientRect = ctrlUnionRect;
    else
    if (RectEmpty(ctrlUnionRect))
        clientRect = panelUnionRect;
    else
        RectUnion(ctrlUnionRect, panelUnionRect, &clientRect);

    if (makeRoomForButtons)
        switch (group->buttonPosition)
            {
            case VAL_EASY_TAB_BTNS_ON_TOP:
            case VAL_EASY_TAB_BTNS_ON_BOTTOM:
                clientRect.width = Max(clientRect.width, minBtnWidth - CLIENT_HORIZONTAL_FRAME_WIDTH);
                break;
            case VAL_EASY_TAB_BTNS_ON_RIGHT:
            case VAL_EASY_TAB_BTNS_ON_LEFT:
                clientRect.height = Max(clientRect.height, minBtnHeight - CLIENT_VERTICAL_FRAME_WIDTH);
                break;
            }
        
    clientRect.width = Max(clientRect.width, group->originalCanvasSize.x);
    clientRect.height = Max(clientRect.height, group->originalCanvasSize.y);
    
    return clientRect;
}

/*******************************************************************/

int CVIFUNC EasyTab_AutoSize(int panel, int easyTabCtrl)
{
    int         error = 0;
    TabGroup    group;
    Rect        clientRect;
    int         locked = FALSE;

    errChk( EasyTab_IdToGroup(panel, easyTabCtrl, &group));
    if (!group->valid)
    errChk(UIEHandleInvalid);

    errChk( CmtGetLock(group->lockHandle) );
    locked = TRUE;

    clientRect = EasyTab_GetAutoSizeRect(group, TRUE);
    if (!RectEmpty(clientRect))
        errChk( EasyTab_SetClientRect(group, clientRect));

Error:
    if (locked)
        CmtReleaseLock(group->lockHandle);
    if (error < 0)
        return error;
    else
        return 0;
}

/**************************************************************************/

static void EasyTab_InvalTabPositions(TabGroup group)
{
    group->tabButtonPositionsCalculated = FALSE;
}

/**************************************************************************/

static int CVICALLBACK ActivationHookCtrlCallback (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    threadLocalVars *pTsv = 0;

    UNUSED(panel);
    UNUSED(control);
    UNUSED(eventData1);
    UNUSED(eventData2);

    switch (event)
        {
        case EVENT_LEFT_CLICK:
            CmtGetThreadLocalVar (gThreadLocalHandle, &pTsv);
            /* let ActivationHookPanelCallback() know that the click was on a control */
            pTsv->ctrlWasClicked = TRUE;  
            break;
        }
        
    return 0;
}

/**************************************************************************/

    /*  This function is deferred because it is called in response to a mouse click on a control
        and we need to make sure the full activation effects of the mouse click are complete before
        this function executes.  The purpose of this function is to prevent an empty tab sheet (or
        one with no operable controls) from becoming active, since (based on the current code for 
        the virtual tab order) there would be no way to leave the sheet with the keyboard.
        The other purpose is to prevent a panel with no active control from becoming active;
        this might occur if the user click on a non-activatable control.  We want to return
        to the previously active panel.
    */
static void CVICALLBACK RevertActivationIfPanelHasNoActiveCtrl(void *callbackData)
{
    int savedBole = SetBOLE(FALSE);
    int btnDown = FALSE;

    GetGlobalMouseState (0, 0, 0, &btnDown, 0, 0);
    
    if (btnDown)
        PostDeferredCallToThread(RevertActivationIfPanelHasNoActiveCtrl, callbackData, CurrThreadId ());
    else
        {
        int currActivePanel = GetActivePanel();
        int prevActivePanel = (int)callbackData;
        int prevActivePanelIsVisible = 0;
        
        if ((currActivePanel > 0) 
                            && (currActivePanel != prevActivePanel)
                            && (GetActiveCtrl(currActivePanel) <= 0))
            {
            GetPanelAttribute(prevActivePanel, ATTR_VISIBLE, &prevActivePanelIsVisible);
            if (prevActivePanelIsVisible)
                SetActivePanel(prevActivePanel);
            }
        }

    SetBOLE(savedBole);
}

/**************************************************************************/

static int CVICALLBACK ActivationHookPanelCallback (int panel, int event,
        void *callbackData, int eventData1, int eventData2)
{
    threadLocalVars *pTsv = 0;
    
    UNUSED(eventData1);
    UNUSED(eventData2);

    switch (event)
        {
        case EVENT_LEFT_CLICK:
            CmtGetThreadLocalVar (gThreadLocalHandle, &pTsv);
            /*  Clicking on an easy tab panel (or parent) shouldn't activate it unless
                you click on a control because the panels are supposed to behave
                as a single panel.
            */
            if (pTsv->ctrlWasClicked)    
                {
                int prevActivePanel = GetActivePanel();
                
                SetActivePanel(panel);  
                
                if ((prevActivePanel > 0) && (prevActivePanel != panel))
                    PostDeferredCallToThread(RevertActivationIfPanelHasNoActiveCtrl, (void *)prevActivePanel, CurrThreadId ());
                    /* gCtrlWasClicked might be set even if you click on a decoration or  */
                    /* indicator. So we need to post a call which will reactivate the old */
                    /* panel if the new panel does not have an active control.            */
                }
            
            pTsv->ctrlWasClicked = FALSE;
            break;
        }
        
    return FALSE;
}

/**************************************************************************/

    /*  Since tab sheets can be made of child panels, the difference between the rules panels
        follow to become active and the rules needed to make tab sheets work properly are handled by 
        disabling normal panel activation and installing callback which perform the appropriate activation
        for tab sheets.
    */
static void InstallActivationHooks(int panel)
{
    int ctrl;
    int child;
    
    SetPanelAttribute (panel, ATTR_ACTIVATE_WHEN_CLICKED_ON, FALSE);    /* don't automatically activate when clicked on */

    InitializeMultithreadingIfNecessary();                              /* activate thread local variables. */
    
        /*  hook into every control callback on the panel */
    GetPanelAttribute(panel, ATTR_PANEL_FIRST_CTRL, &ctrl);
    while (ctrl)
        {
        ChainCtrlCallback(panel, ctrl, ActivationHookCtrlCallback, NULL, "Ctrl Activation Hook");
        GetCtrlAttribute(panel, ctrl, ATTR_NEXT_CTRL, &ctrl);
        }
        
        /* hook into the panels callback */
    ChainPanelCallback(panel, ActivationHookPanelCallback, NULL, "Panel Activation Hook");

        /* recurse on each child panel */
    GetPanelAttribute(panel, ATTR_FIRST_CHILD, &child);
    
    while (child)
        {
        InstallActivationHooks(child);
        GetPanelAttribute(child, ATTR_NEXT_PANEL, &child);
        }
}

/**************************************************************************/

static int EasyTab_CtrlCanBeTabStop(int panel, int ctrl)
{
    int dimmed;
    
    GetPanelAttribute(panel, ATTR_DIMMED, &dimmed);
    
    return !dimmed && CtrlCanBeTabStop(panel, ctrl);
}

/**************************************************************************/

    /*  Given a panel where a tab keypress was detected, return the topmost parent panel
        
        Note: If a child panel with a tab group on it is installed as a popup panel, its parent
        panel will be included in the virtual tab list.  Therefore, don't do this (there isn't a 
        good reason to make a child panel a popup anyway). Eventually this could be extended to stop 
        if the parent of a popup isn't a popup itself.
        
        Bug Fix: 6-26-96.  Now it stops when it before it gets to a panel that is not a easytab sheet panel  (CAR X7CGJZL)
    */
    
static int GetVirtualTabTopLevelPanel(int panel)
{                    
    int parentPanel;
    
    while (panel)
        {
        if (!IsSheetPanel(panel))
            break;
        GetPanelAttribute(panel,ATTR_PANEL_PARENT, &parentPanel);   
        if (parentPanel)
            panel = parentPanel;
        else
            break;
        }
        
    return panel;       
}

/**************************************************************************/

static ListType GetVirtualTabOrder(int panel, int tabGroupsOnly)
{
    int         error = 0;
    ListType    list = NULL;
    
    panel = GetVirtualTabTopLevelPanel(panel);
    
    nullChk( list = ListCreate(sizeof(TabOrderEntry)));
    errChk( AddToVirtualTabOrder(panel, list, tabGroupsOnly));
    
Error:
    if (error < 0)
        {
        ListDispose(list);
        list = NULL;
        }
        
    return list;
}

/**************************************************************************/

    /*  Add all the controls on this panel to the tab stop list.  If a TabGroup control is 
        found, recurse into the child panel for its active sheet.
    */
static int AddToVirtualTabOrder(int panel, ListType tabList, int tabGroupsOnly)
{
    int             error = 0;
    int             ctrl;
    TabOrderEntry   entry;
    TabGroup        group;
    Sheet           activeSheet;
    int             result;
    ListType        ctrlList = 0;
    int             numCtrls;
    int             index;
    
    errChk( GetCtrlList(panel, &ctrlList));
    SortCtrlListByTabOrder(panel, ctrlList);
    
    numCtrls = ListNumItems(ctrlList);
    for (index = 1; index <= numCtrls; index++)
        {
        ListGetItem(ctrlList, &ctrl, index);
        
        entry.isTabStop = EasyTab_CtrlCanBeTabStop(panel, ctrl);
        entry.panel = panel;
        entry.ctrl = ctrl;
        result = EasyTab_IdToGroup(panel, ctrl, &group);

        if (result >= 0 || !tabGroupsOnly)
            nullChk( ListInsertItem(tabList, &entry, END_OF_LIST));
    
        if (result >= 0 && entry.isTabStop)
            {
            activeSheet = EasyTab_GetActive(group);
            if (activeSheet->ownsPanel)
                errChk( AddToVirtualTabOrder(activeSheet->panel, tabList, tabGroupsOnly));
            }
        }
        
Error:
    ListDispose(ctrlList);
    return error;
}

/**************************************************************************/

    /*
        Given a control and a direction, this function computes the next tab stop, accounting for
        TabGroup controls and the child panels they own.
        
        Note: If not enough memory for tabstop list, the navigation key silenty (and otherwise harmlessly) fails to works (this will be the least of your problems) 
    */
static int NextVirtualTabStop(int panel, int ctrl, int tabbingForward, int wrap, TabOrderEntry *entry)
{
    int             error = 0;
    ListType        tabList = 0;
    int             numEntries;
    int             startIndex;
    int             currIndex;
    int             found = FALSE;
    TabOrderEntry   currEntry;

    nullChk( tabList = GetVirtualTabOrder(panel, FALSE));
    
    currEntry.panel = panel;
    currEntry.ctrl = ctrl;
    currEntry.isTabStop = EasyTab_CtrlCanBeTabStop(panel, ctrl);    

    numEntries  = ListNumItems(tabList);
    currIndex = startIndex = ListFindItem (tabList, &currEntry, FRONT_OF_LIST, 0);
    
    if (currIndex > 0 && numEntries > 0)
    while (!found)
        {
        currIndex += tabbingForward ? 1 : -1;
        if (wrap)
            {
            if (currIndex == 0)
                currIndex = numEntries;
            else
            if (currIndex > numEntries)
                currIndex = 1;
            }
            
        if (currIndex == startIndex)
            break;      /* wrapped without finding anything */
            
        ListGetItem(tabList, &currEntry, currIndex);
        if (currEntry.isTabStop)
            {
            *entry = currEntry;
            found = TRUE;
            }
        }

Error:
    ListDispose(tabList);
    return found;
}

/**************************************************************************/

    /*  
        Returns TRUE if the key was handled, FALSE otherwise
    */
static int HandleTabKey(int key, int *callbackReturnVal)
{
    int             tabbingForward;
    int             activeCtrl;
    int             activePanel;
    int             keyHandled = FALSE;
    TabOrderEntry   entry;
    
    if ((key & VAL_VKEY_MASK) == VAL_TAB_VKEY && !(key & VAL_MENUKEY_MODIFIER))
        {
        activePanel = GetActivePanel();
        activeCtrl = GetActiveCtrl(activePanel);
        
        keyHandled = TRUE;
        *callbackReturnVal = TRUE;
        
        tabbingForward = !(key & VAL_SHIFT_MODIFIER);
        
        if (activeCtrl > 0 && NextVirtualTabStop(activePanel, activeCtrl, tabbingForward, TRUE, &entry))
            {
            SetActiveCtrl(entry.panel, entry.ctrl);
            SetActivePanel(entry.panel);
            }
        }
        
    return keyHandled;
}

/**************************************************************************/

    /*  Returns the sheet in the group with the matching accelerator key, or NULL if no sheet 
        matches (only the ASCII portion of key is examined).
    */
static Sheet EasyTab_AccelKeyToSheet(TabGroup group, int key)
{
    int     numSheets;
    int     index;
    Sheet   sheet;
    int     accelKey;
    
    numSheets = EasyTab_NumSheets(group);
    for (index = 1; index <= numSheets; index++)
        {
        sheet = EasyTab_IndexToSheet(group, index);
        accelKey = sheet->tabButton->acceleratorKey;
        
        if (accelKey && toupper(accelKey & VAL_ASCII_KEY_MASK) == toupper(key & VAL_ASCII_KEY_MASK))
            return sheet;
        }
        
    return NULL;
}

/**************************************************************************/

    /*  
        Returns TRUE if the key was handled, FALSE otherwise
    */
static int  EasyTab_HandleAcceleratorKey(int key, int *callbackReturnVal)
{
    int             error = 0;
    ListType        groupList = 0;
    int             numItems;
    TabGroup        group;
    int             index;
    TabOrderEntry   entry;
    Sheet           sheet;
    int             asciiVal = key & VAL_ASCII_KEY_MASK; 
    int             vKey = key & VAL_VKEY_MASK;
    int             keyHandled = FALSE;
    int             activePanel = GetActivePanel();
    
    if (activePanel > 0)
        {
        groupList = GetVirtualTabOrder(activePanel, TRUE);
        
        if (groupList)
            {
            numItems = ListNumItems(groupList);
            
            if ((key & VAL_UNDERLINE_MODIFIER) && asciiVal) 
                {
                for (index = 1;  index <= numItems; index++)
                    {
                    ListGetItem(groupList, &entry, index);
                    if (EasyTab_IdToGroup(entry.panel, entry.ctrl, &group) >= 0)
                        {
                        sheet = EasyTab_AccelKeyToSheet(group, key);
            
                        if (sheet)
                            {
                            EasyTab_SetActive(group, sheet);
                            EasyTab_SetFocus(group);
                            *callbackReturnVal = TRUE;      
                            keyHandled = TRUE;
                            }   /* if */
                        }   /* if */
                    } /* for */
                }  /* if */
            else
            if (vKey == VAL_TAB_VKEY && (key & VAL_MENUKEY_MODIFIER))
                {
                if (numItems > 0)
                    {
                    /*  Since Win95 tab sheets activate the tab control for CTRL-TAB and CTRL-SHIFT-TAB,
                        regardless of which control has the focus, they must assume that only one tab sheet can be
                        on a dialog.  We don't impose that limitation, however, we do alway map CTRL-TAB and CTRL-SHIFT-TAB
                        to the first tab control on the dialog because there is no other criteria by which to choose which
                        tab control to activate.
                    */
                    ListGetItem(groupList, &entry, 1);       /* get first tab control */
                    if (EasyTab_IdToGroup(entry.panel, entry.ctrl, &group) >= 0)
                        {
                        EasyTab_ActivateHorizontalNeighbor(group, key & VAL_SHIFT_MODIFIER, TRUE);
                        EasyTab_SetFocus(group);
                        *callbackReturnVal = TRUE;      
                        keyHandled = TRUE;
                        }
                    }
                }
            }               
        }

    
    ListDispose(groupList);
    return keyHandled;
}

/**************************************************************************/

    /*  
        Returns TRUE if the key was handled, FALSE otherwise
    */
static int HandleOtherKeys(int key, int *callbackReturnVal)
{
    int              activeCtrl;
    int              activePanel;
    int panel;
    CtrlCallbackPtr  ctrlEventFunction;
    PanelCallbackPtr panelEventFunction;
    int              keyHandled = FALSE;

    activePanel = GetActivePanel();
//    EasyTab_GetAttribute(activePanel, int easyTabCtrl, ATTR_EASY_TAB_ACTIVE_PANEL, &panel)
    GetPanelAttribute(activePanel, ATTR_CALLBACK_FUNCTION_POINTER, &panelEventFunction);

    activeCtrl = GetActiveCtrl(activePanel);
    GetCtrlAttribute(activePanel, activeCtrl, ATTR_CALLBACK_FUNCTION_POINTER, &ctrlEventFunction);

    if (ctrlEventFunction){
        if (keyHandled = ctrlEventFunction(activePanel, activeCtrl, EVENT_KEYPRESS, NULL, key, 0) ){
            *callbackReturnVal = TRUE;
            return keyHandled;
        }
    }
    if (panelEventFunction){
        if (keyHandled = panelEventFunction(activePanel, EVENT_KEYPRESS, NULL, key, 0) ){
            *callbackReturnVal = TRUE;
            return keyHandled;
        }
    }
    *callbackReturnVal = FALSE;
    
    return keyHandled;
}

/**************************************************************************/

    /*  Reset all labels so they adjust to any font/orientation/etc... change */
static int EasyTab_ResetLabels(TabGroup group)
{
    int     index;
    int     numSheets;
    int     error = 0;
    Sheet   sheet;
    
    numSheets = EasyTab_NumSheets(group);
    
    for (index = 1; index <= numSheets; index++)
        {
        sheet = EasyTab_IndexToSheet(group, index);
        errChk( TabButton_SetLabel(sheet->tabButton, sheet->tabButton->label));
        }

Error:
    return error;
}

/**************************************************************************/
    
    /*  Loads panels to a Tabgroup where they are made into tab sheets.
        This function expects a list of panelResId, panelHandlePtr pairs, terminated by a zero.
        the panelHandlePtr's may be NULL.
    */
int CVIFUNC_C EasyTab_LoadPanels(int panel, int easyTabCtrl, int row, char *fileName, void *module, ...)
{
    int     error = 0;
    va_list parmInfo;
    
    va_start(parmInfo, module);
    errChk( EasyTab_LoadPanelsFromParmInfo(panel, easyTabCtrl, row, fileName, module, parmInfo));

Error:
    va_end(parmInfo);
    return error;
}

int CVIFUNC EasyTab_LoadPanelsFromParmInfo(int panel, int easyTabCtrl, int row, char *fileName, void *module, va_list parmInfo)
{
    int         error = 0;
    int         resId;
    int         *panelHandlePtr;
    TabGroup    group;
    int         locked = FALSE;

    errChk( EasyTab_IdToGroup(panel, easyTabCtrl, &group));
    if (!group->valid)
        errChk(UIEHandleInvalid);

    errChk( CmtGetLock(group->lockHandle) );
    locked = TRUE;
    
    if (!fileName)
        errChk( UIENullPointerPassed);

    resId = va_arg(parmInfo, int);
    while (resId > 0) 
        {
        errChk( EasyTab_AddSheet(panel, easyTabCtrl, NULL, module, row, VAL_TAB_SHEET_FROM_FILE, fileName, resId));
        
        panelHandlePtr = va_arg(parmInfo, int *);
        if (panelHandlePtr)
            *panelHandlePtr = EasyTab_IndexToSheet(group, EasyTab_NumSheets(group))->panel; /* the loaded panel should be the last in the sheet list */

        resId = va_arg(parmInfo, int);
        }

Error:
    if (locked)
        CmtReleaseLock(group->lockHandle);
    if (error < 0)
        return error;
    else
        return 0;
}

/**************************************************************************/
    
    /*  Adds panels to a Tabgroup where they are made into tab sheets.
        This function expects a series of panel handle arguments terminated by a zero.
    */
int CVIFUNC_C EasyTab_AddPanels(int panel, int easyTabCtrl, int row, ...)
{
    int     error = 0;
    va_list parmInfo;

    va_start(parmInfo, row);
    errChk( EasyTab_AddPanelsFromParmInfo(panel, easyTabCtrl, row, parmInfo));

Error:
    va_end(parmInfo);
    return error;
}

int CVIFUNC EasyTab_AddPanelsFromParmInfo(int panel, int easyTabCtrl, int row, va_list parmInfo)
{
    int     error = 0;
    int     sheetPanel;

    sheetPanel = va_arg(parmInfo, int);
    while (sheetPanel > 0) 
        {
        errChk( EasyTab_AddSheet(panel, easyTabCtrl, NULL, (void *)0, row, VAL_TAB_SHEET_FROM_PANEL_HANDLE, sheetPanel));
        sheetPanel = va_arg(parmInfo, int);
        }

Error:
    if (error < 0)
        return error;
    else
        return 0;
}

/**************************************************************************/

int CVIFUNC_C EasyTab_SetAttribute(int panel, int easyTabCtrl, int attribute, ...)
{
    int             error = 0;
    va_list         parmInfo;
    
    va_start(parmInfo, attribute);
    errChk( EasyTab_SetAttributeFromParmInfo(panel, easyTabCtrl, attribute, parmInfo));

Error:
    va_end(parmInfo);
    if (error < 0)
        return error;
    else
        return 0;
}

/**************************************************************************/

int CVIFUNC EasyTab_SetAttributeFromParmInfo(int panel, int easyTabCtrl, int attribute, va_list parmInfo)
{
    int         error = 0;
    TabGroup    group;
    Sheet       sheet;
    int         intVal;
    char        *stringVal;
    Sheet       activeSheet;
    int         locked = FALSE;
    
    errChk( EasyTab_IdToGroup(panel, easyTabCtrl, &group));
    if (!group->valid)
    errChk(UIEHandleInvalid);

    errChk( CmtGetLock(group->lockHandle) );
    locked = TRUE;
    
    switch (attribute)
        {
        case ATTR_EASY_TAB_ACTIVE_PANEL:
            sheet = EasyTab_PanelToSheet(group, va_arg(parmInfo, int));
            if (sheet)
                {
                if (sheet->hidden)
                    {
                    sheet->hidden = FALSE;
                    EasyTab_InvalRowLists(group);
                    }

                EasyTab_SetActive(group, sheet);
                }
            else
                errChk( UIEValueIsInvalidOrOutOfRange);
            break;
        case ATTR_EASY_TAB_ORIENTATION:
            intVal = va_arg(parmInfo, int);
            errChk( EasyTab_CheckButtonPosition(intVal));
            group->buttonPosition = intVal;
            EasyTab_InvalAll(group);
            EasyTab_ResetLabels(group);
            break;
        case ATTR_EASY_TAB_STRETCH_TO_FIT:
            intVal = va_arg(parmInfo, int) ? TRUE : FALSE;
            if (intVal != group->sizeButtonsToFillRows)
                {
                group->sizeButtonsToFillRows = intVal;
                EasyTab_InvalAll(group);
                }
            break;
        case ATTR_EASY_TAB_BTN_GAP_SIZE:
            group->buttonGap = va_arg(parmInfo, int);
            EasyTab_InvalAll(group);
            break;      
        case ATTR_EASY_TAB_META_FONT:
            stringVal = va_arg(parmInfo, char *);
            if (!IsMetaFont(stringVal))
                errChk(UIEFontNotInFontTable);
                
            StringCopyMax (group->metaFont, stringVal, 256);
            EasyTab_ResetLabels(group);
            break;
        case ATTR_EASY_TAB_META_FONT_LENGTH:
            errChk( UIEAttributeNotSettable);
            break;
        case ATTR_EASY_TAB_BG_COLOR:
            intVal = va_arg(parmInfo, int);
            if (intVal != VAL_TRANSPARENT)
                intVal &= 0xFFFFFF;     /* coerce to valid color just in case */
                
            group->bgColor = intVal;
            EasyTab_InvalAll(group);
            break;
        case ATTR_EASY_TAB_LABEL_COLOR:
            intVal = va_arg(parmInfo, int);
            if (intVal != VAL_TRANSPARENT)
                intVal &= 0xFFFFFF;     /* coerce to valid color just in case */
                
            group->textColor = intVal;
            EasyTab_InvalAll(group);
            break;          
        case ATTR_EASY_TAB_VISIBLE:
            group->hidden = !va_arg(parmInfo, int);
            activeSheet = EasyTab_GetActive(group);   
            
            if (group->hidden)
                {
                SetCtrlAttribute(group->dialogPanel, group->canvas, ATTR_VISIBLE, FALSE);
                if (activeSheet)
                    SetPanelAttribute(activeSheet->panel, ATTR_VISIBLE, FALSE);
                }
            else            
                {
                SetCtrlAttribute(group->dialogPanel, group->canvas, ATTR_VISIBLE, TRUE);
                if (activeSheet)
                    SetPanelAttribute(activeSheet->panel, ATTR_VISIBLE, TRUE);
                }
            break;
        case ATTR_EASY_TAB_OVERRIDE_CTRL_ACTIVATION_POLICY:
            group->overrideActivation = va_arg(parmInfo, int);
            break;
        default:
            errChk( UIEInvalidAttribute);    
            break;
        }
    
Error:
    if (locked)
        CmtReleaseLock(group->lockHandle);
    return error;
}

/**************************************************************************/

static int  EasyTab_CheckButtonPosition(int buttonPosition)
{
    switch (buttonPosition)
        {
        case VAL_EASY_TAB_BTNS_ON_TOP:
        case VAL_EASY_TAB_BTNS_ON_BOTTOM:       
        case VAL_EASY_TAB_BTNS_ON_RIGHT:        
        case VAL_EASY_TAB_BTNS_ON_LEFT:  
            return 0;
        default:
            return UIEBadAttributeValue;
        }
}

/**************************************************************************/

int CVIFUNC EasyTab_GetAttribute(int panel, int easyTabCtrl, int attribute, void *value)
{
    int         error = 0;
    TabGroup    group;
    Sheet       activeSheet;
    int         locked = FALSE;

    errChk( EasyTab_IdToGroup(panel, easyTabCtrl, &group));
    if (!group->valid)
        errChk(UIEHandleInvalid);

    errChk( CmtGetLock(group->lockHandle) );
    locked = TRUE;

    if (!value)
        errChk( UIENullPointerPassed);
        
    switch (attribute)
        {
        case ATTR_EASY_TAB_ACTIVE_PANEL:
            activeSheet = EasyTab_GetActive(group);
            if (activeSheet)
                *(int *)value = activeSheet->panel;
            else
                *(int *)value = 0;
            break;
        case ATTR_EASY_TAB_ORIENTATION:
            *(int *)value = group->buttonPosition;
            break;
        case ATTR_EASY_TAB_STRETCH_TO_FIT:
            *(int *)value = group->sizeButtonsToFillRows ? TRUE : FALSE;
            break;
        case ATTR_EASY_TAB_BTN_GAP_SIZE:
            *(int *)value = group->buttonGap;
            break;      
        case ATTR_EASY_TAB_META_FONT:   
            StringCopyMax ((char *)value, group->metaFont, 256);
            break;
        case ATTR_EASY_TAB_META_FONT_LENGTH:
            *(int *)value = strlen(group->metaFont);
            break;
        case ATTR_EASY_TAB_BG_COLOR:
            *(int *)value = group->bgColor;
            break;
        case ATTR_EASY_TAB_LABEL_COLOR:
            *(int *)value = group->textColor;
            break;
        case ATTR_EASY_TAB_VISIBLE:
            *(int *)value = !group->hidden;
            break;
        case ATTR_EASY_TAB_OVERRIDE_CTRL_ACTIVATION_POLICY:
            *(int *)value = group->overrideActivation;
        default:
            errChk( UIEInvalidAttribute);    
            break;
        }
        
Error:
    if (locked)
        CmtReleaseLock(group->lockHandle);
    if (error < 0)
        return error;
    else
        return 0;
}

/**************************************************************************/

int CVIFUNC EasyTab_SetBounds(int panel, int easyTabCtrl, int whichBounds, Rect bounds)
{
    int         error = 0;
    TabGroup    group;
    int         locked = FALSE;

    errChk( EasyTab_IdToGroup(panel, easyTabCtrl, &group));
    if (!group->valid)
        errChk(UIEHandleInvalid);

    errChk( CmtGetLock(group->lockHandle) );
    locked = TRUE;
    
    if (RectEmpty(bounds))
        errChk( UIEValueIsInvalidOrOutOfRange);
    
    switch (whichBounds)
        {
        case VAL_EASY_TAB_INTERIOR_BOUNDS:
            errChk( EasyTab_SetClientRect(group, bounds));
            break;
        case VAL_EASY_TAB_EXTERIOR_BOUNDS:
            errChk( EasyTab_SetExteriorRect(group, bounds));
            break;
        default:
            errChk( UIEValueIsInvalidOrOutOfRange);
            break;
        }

Error:
    if (locked)
        CmtReleaseLock(group->lockHandle);
    if (error < 0)
        return error;
    else
        return 0;
}

/**************************************************************************/

int CVIFUNC EasyTab_GetBounds(int panel, int easyTabCtrl, int whichBounds, Rect *bounds)
{
    int         error = 0;
    TabGroup    group;
    int         locked = FALSE;

    errChk( EasyTab_IdToGroup(panel, easyTabCtrl, &group));
    if (!group->valid)
        errChk(UIEHandleInvalid);

    errChk( CmtGetLock(group->lockHandle) );
    locked = TRUE;

    if (!bounds)
        errChk( UIENullPointerPassed);
            
    switch (whichBounds)
        {
        case VAL_EASY_TAB_INTERIOR_BOUNDS:
            *bounds = EasyTab_GetClientRect(group);
            break;
        case VAL_EASY_TAB_EXTERIOR_BOUNDS:
            *bounds = EasyTab_GetExteriorRect(group);
            
            break;
        default:
            errChk( UIEValueIsInvalidOrOutOfRange);
            break;
        }

Error:
    if (locked)
        CmtReleaseLock(group->lockHandle);
    if (error < 0)
        return error;
    else
        return 0;
}

/**************************************************************************/
    
    /*  allocates a list of the panelHandles for the tab sheets owned by the control */
int CVIFUNC EasyTab_GetPanelList(int panel, int easyTabCtrl, ListType *list)        /* seldom needed, no function panel */   
{
    int         error = 0;
    ListType    newList = NULL;
    int         numSheets;
    int         index;
    TabGroup    group;
    Sheet       sheet;

    *list = NULL;

    errChk( EasyTab_IdToGroup(panel, easyTabCtrl, &group)); 
    
    nullChk( newList = ListCreate(sizeof(int)));
    numSheets = EasyTab_NumSheets(group);
    for (index = 1; index <= numSheets; index++)
        {
        sheet = EasyTab_IndexToSheet(group, index);
        if (sheet->ownsPanel)
            nullChk( ListInsertItem(newList, &sheet->panel, END_OF_LIST));
       }
       
Error:
    if (error < 0)
        {
        ListDispose(newList);
        return error;
        }
    else
        {
        *list = newList;
        return 0;
        }
}

/**************************************************************************/

int CVIFUNC_C EasyTab_SetTabAttribute(int panel, int easyTabCtrl, int tabPanelHandle, int attribute, ...)
{
    int             error = 0;
    va_list         parmInfo;
    
    va_start(parmInfo, attribute);
    errChk( EasyTab_SetTabAttributeFromParmInfo(panel, easyTabCtrl, tabPanelHandle, attribute, parmInfo));

Error:
    va_end(parmInfo);
    if (error < 0)
        return error;
    else
        return 0;
}

/**************************************************************************/

int CVIFUNC EasyTab_SetTabAttributeFromParmInfo(int panel, int easyTabCtrl, int tabPanelHandle, int attribute, va_list parmInfo)
{
    int         error = 0;
    TabGroup    group;
    Sheet       sheet;
    int         intVal;
    int         parent;
    int         locked = FALSE;
    
    errChk( EasyTab_IdToGroup(panel, easyTabCtrl, &group));
    if (!group->valid)
        errChk(UIEHandleInvalid);

    errChk( CmtGetLock(group->lockHandle) );
    locked = TRUE;
    
    sheet = EasyTab_PanelToSheet(group, tabPanelHandle);
    if (!sheet)
        errChk( UIEValueIsInvalidOrOutOfRange);

    switch (attribute)
        {
        case ATTR_EASY_TAB_LABEL_TEXT:
            TabButton_SetLabel(sheet->tabButton, va_arg(parmInfo, char *));
            break;
        case ATTR_EASY_TAB_LABEL_TEXT_LENGTH:
            errChk( UIEAttributeNotSettable);
            break;
        case ATTR_EASY_TAB_VISIBLE:
            intVal = va_arg(parmInfo, int) ? TRUE : FALSE;
            
                /* make sure an active visible tab remains on the row */
            if (!intVal)
                {
                if (EasyTab_NumVisibleTabsOnRow(group, sheet->originalRow) <= 1 ||
                    !EasyTab_SetInactive(group, sheet))
                break;  /* can't hide an tab that can't be made inactive */
                }
                
            sheet->hidden = !intVal;
            EasyTab_InvalRowLists(group);
            EasyTab_InvalAll(group);
            break;
        case ATTR_EASY_TAB_LABEL_BG_COLOR:
            intVal = va_arg(parmInfo, int);
            if (intVal != VAL_TRANSPARENT)
                intVal &= 0xFFFFFF;     /* coerce to valid color just in case */
            GetPanelAttribute (sheet->panel, ATTR_PANEL_PARENT, &parent);
            if (parent)
                SetPanelAttribute (sheet->panel, ATTR_TITLE_BACKCOLOR, intVal);
            else
                SetPanelAttribute (sheet->panel, ATTR_BACKCOLOR, intVal);
            EasyTab_InvalAll(group);
            break;
        case ATTR_EASY_TAB_DIMMED:
            sheet->tabButton->dimmed = va_arg(parmInfo, int);
            SetInputMode (sheet->panel, -1, !sheet->tabButton->dimmed);
            EasyTab_InvalAll(group);
            break;
        default:
            errChk( UIEInvalidAttribute);    
            break;
        }
    
Error:
    if (locked)
        CmtReleaseLock(group->lockHandle);
    return error;
}


/**************************************************************************/

int CVIFUNC EasyTab_GetTabAttribute(int panel, int easyTabCtrl, int tabPanelHandle, int attribute, void *value)
{
    int         error = 0;
    TabGroup    group;
    Sheet       sheet;
    int         parent;
    int         locked = FALSE;
    
    errChk( EasyTab_IdToGroup(panel, easyTabCtrl, &group));
    if (!group->valid)
        errChk(UIEHandleInvalid);

    errChk( CmtGetLock(group->lockHandle) );
    locked = TRUE;

    sheet = EasyTab_PanelToSheet(group, tabPanelHandle);
    if (!sheet)
        errChk( UIEValueIsInvalidOrOutOfRange);

    if (!value)
        errChk( UIENullPointerPassed);
        
    switch (attribute)
        {
        case ATTR_EASY_TAB_LABEL_TEXT:
            strcpy ((char *)value, sheet->tabButton->label);
            break;
        case ATTR_EASY_TAB_LABEL_TEXT_LENGTH:
            *(int *)value = strlen(sheet->tabButton->label);
            break;
        case ATTR_EASY_TAB_VISIBLE:
            *(int *)value = !sheet->hidden;
            break;
        case ATTR_EASY_TAB_LABEL_BG_COLOR:
            GetPanelAttribute (sheet->panel, ATTR_PANEL_PARENT, &parent);
            if (parent)
                GetPanelAttribute (sheet->panel, ATTR_TITLE_BACKCOLOR, (int *)value);
            else
                GetPanelAttribute (sheet->panel, ATTR_BACKCOLOR, (int *)value);
            break;
        case ATTR_EASY_TAB_DIMMED:
            *(int *)value = sheet->tabButton->dimmed;
            break;
        default:
            errChk( UIEInvalidAttribute);    
            break;
        }
        
Error:
    if (locked)
        CmtReleaseLock(group->lockHandle);
    if (error < 0)
        return error;
    else
        return 0;
}
