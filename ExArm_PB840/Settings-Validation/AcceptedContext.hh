
#ifndef AcceptedContext_HH
#define AcceptedContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AcceptedContext - Context for ALL Accepted Settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/AcceptedContext.hhv   25.0.4.0   19 Nov 2013 14:27:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: rhj    Date: 01-Dec-2006    DR Number: 6291
//  Project:  RESPM
//  Description:
//       Found a better way to implement a fix for SCR 6291.
//
//  Revision: 008   By: rhj    Date: 27-Nov-2006    DR Number: 6291
//  Project:  RESPM
//  Description:
//	    Added a functionality to prevent waveform plot settings  
//      getting stored into NOVRAM when "foisting" is in progress. 
//
//  Revision: 007   By: sah    Date: 19-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  to handle special operations due to changing circuit type,
//         the 'acceptSstBatch()' method was changed from an inline
//         to a non-inline method
//
//  Revision: 006   By: sah   Date:  28-Jun-1999    DR Number: 5450
//  Project:  ATC
//  Description:
//	Added new private member function, 'notifyAllBatchObservers_()'
//      to solve two problems:  the one for this DCS where when Same-Patient
//      O2% was 21% and New-Patient is 21%, no callback to synchronize
//      alarm observers occurs; and, to ensure that, when an observer of
//      any accepted setting or context gets notified of an acceptance,
//      all accepted values are available for query.  Also, now using a
//      bit array to keep track of changed batch values, to notify those
//      observers after all changes have been made.
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  now derived off of 'ContextSubject' base class
//	*  added new 'isSettingChanged()' virtual method
//	*  added new 'areAnyBatchSettingsChanged()' virtual method
//
//  Revision: 004   By: dosman Date: 22-Aug-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//      removed the reference on getNonBatchBoundedValue
//      because it caused a bug, found during unit test,
//      where the value this method was returning was not an l-value
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 002   By: sah    Date: 29-Sep-1997    DR Number: 2410
//  Project: Sigma (R8027)
//  Description:
//	Added new interface method for the acceptance of the new
//	Atmospheric Pressure Setting.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Integration baseline.
//
//====================================================================

#include "ApneaCorrectionRule.hh"
#include "ApneaCorrectionStatus.hh"
#include "SigmaState.hh"

//@ Usage-Classes
#include "BatchSettingValues.hh"
#include "NonBatchSettingValues.hh"
#include "ContextSubject.hh"
#include "BitArray_NUM_BATCH_SETTING_IDS.hh"
//@ End-Usage


class AcceptedContext : public ContextSubject
{
  public:
    AcceptedContext(void);
    ~AcceptedContext(void);

    Boolean  arePatientSettingsAvailable (void) const;
    Boolean  areBatchValuesInitialized   (void) const;
    Boolean  areNonBatchValuesInitialized(void) const;

    inline Boolean  isBdInSafetyPcvState(void) const;
    inline void     setBdSafetyPcvFlag  (const Boolean newFlagValue);

    void                   acceptNewPatientBatch (void);
    void                   acceptSamePatientBatch(void);
    ApneaCorrectionStatus  acceptVentSetupBatch  (void);
    inline void            acceptApneaSetupBatch (void);

    inline ApneaCorrectionStatus  acceptGenericBatch     (void);
    void                          acceptSstBatch         (void);
    void                          acceptTimeDateBatch    (void) const;
    void                          acceptDciSetupBatch    (void);
    void                          acceptServiceSetupBatch(void);
    inline void                   acceptAtmPressureCal   (void);

    inline const BatchSettingValues&  getAcceptedBatchValues(void) const;

    BoundedValue     getBoundedValue (
    				const SettingId::SettingIdType boundedId
				     )  const;
    DiscreteValue    getDiscreteValue(
    				const SettingId::SettingIdType discreteId
				     )  const;
    SequentialValue  getSequentialValue(
    				const SettingId::SettingIdType sequentialId
				       )  const;

    inline SettingValue  getBatchSettingValue(
				     const SettingId::SettingIdType batchId
						    ) const;
    inline BoundedValue  getBatchBoundedValue(
				     const SettingId::SettingIdType batchId
						    ) const;
    inline DiscreteValue        getBatchDiscreteValue(
				     const SettingId::SettingIdType batchId
						     ) const;
    inline SequentialValue      getBatchSequentialValue(
				     const SettingId::SettingIdType batchId
						       ) const;

    inline void  setBatchSettingValue(
			       const SettingId::SettingIdType batchId,
			       const SettingValue&            newSettingValue
				     );
    inline void  setBatchBoundedValue(
			       const SettingId::SettingIdType batchId,
			       const BoundedValue&            newBoundedValue
				     );
    inline void  setBatchDiscreteValue(
			      const SettingId::SettingIdType batchId,
			      const DiscreteValue            newDiscreteValue
			       );
    inline void  setBatchSequentialValue(
			    const SettingId::SettingIdType batchId,
			    const SequentialValue          newSequentialValue
				 );

    inline BoundedValue  getNonBatchBoundedValue(
				   const SettingId::SettingIdType nonBatchId
						       ) const;
    inline DiscreteValue        getNonBatchDiscreteValue(
				   const SettingId::SettingIdType nonBatchId
							) const;
    inline SequentialValue      getNonBatchSequentialValue(
				   const SettingId::SettingIdType nonBatchId
							  ) const;

    inline void  setNonBatchSettingValue(
			       const SettingId::SettingIdType nonBatchId,
			       const SettingValue&            newSettingValue
				        );
    inline void  setNonBatchBoundedValue(
			      const SettingId::SettingIdType nonBatchId,
			      const BoundedValue&            newBoundedValue
					);
    inline void  setNonBatchDiscreteValue(
			      const SettingId::SettingIdType nonBatchId,
			      const DiscreteValue            newDiscreteValue
					 );
    inline void  setNonBatchSequentialValue(
			    const SettingId::SettingIdType nonBatchId,
			    const SequentialValue          newSequentialValue
					   );


    //-----------------------------------------------------------------
    // virtual methods inherited from ContextSubject class...
    //-----------------------------------------------------------------

    virtual Boolean  isSettingChanged(
			      const SettingId::SettingIdType settingId
				     ) const;

    virtual Boolean  areAnyBatchSettingsChanged(void) const;

    virtual SettingValue  getSettingValue(
			      const SettingId::SettingIdType settingId
					 ) const;


#if defined(SIGMA_DEVELOPMENT)
    Boolean  areAllBatchValuesValid   (void) const;
    Boolean  areAllNonBatchValuesValid(void) const;
#endif  // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    AcceptedContext(const AcceptedContext&);	// not implemented...
    void  operator=(const AcceptedContext&);	// not implemented...

    ApneaCorrectionStatus  acceptAdjustedBatch_(
			    const ApneaCorrectionRule correctionRule,
			    const Boolean             isVentStartup = FALSE
					       );

    void  notifyAllBatchObservers_(void);

    static void  RegisterSettingChange_(
				const SettingId::SettingIdType settingId
				       );

    //@ Data-Member:  bdSafetyPcvFlag_
    // This flag indicates whether BD is in a Safety-PCV state.
    Boolean  bdSafetyPcvFlag_;

    // Data-Member:  changedBatchValues_
    // Bit flags to keep track of which batch settings changed during a
    // batch acceptance.
    FixedBitArray(NUM_BATCH_SETTING_IDS)  changedBatchValues_;

    //@ Data-Member:  acceptedBatchValues_
    // The accepted batch setting values.
    BatchSettingValues  acceptedBatchValues_;

    //@ Data-Member:  acceptedNonBatchValues_
    // The accepted non-batch setting values.
    NonBatchSettingValues  acceptedNonBatchValues_;

};


// Inlined methods...
#include "AcceptedContext.in"


#endif // AcceptedContext_HH 
