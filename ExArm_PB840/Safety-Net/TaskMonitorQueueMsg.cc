#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: TaskMonitorQueueMsg - Queue message for the TaskMonitor.
//---------------------------------------------------------------------
//@ Interface-Description
//  Message object used for communication with other tasks.  Instances
//  of this class take on one, of two, forms:  a "ping" message, which
//  contains the message ID 'PING_MSG_ID' (an alias of 'TASK_MONITOR_MSG');
//  or as a "response" message, containing the ID 'RESPONSE_MSG_ID'.
//---------------------------------------------------------------------
//@ Rationale
//  To provide a standard message for the TaskMonitor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains a 32-bit message divided into ID and data fields.
//
//  ---------------------------------------------
//  |               |                            |
//  | Msg Id 8 bits |     Msg Data 24 bits       |
//  |               |                            |
//  ---------------------------------------------
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with pre- and
//  post-conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/TaskMonitorQueueMsg.ccv   25.0.4.0   19 Nov 2013 14:20:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001   By: sah   Date: 26-Mar-1997   DR Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//	Initial version to coding standards (Integration baseline).
//
//=====================================================================

#include "TaskMonitorQueueMsg.hh"
#include "Task.hh"


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   F U N C T I O N   D E S C R I P T I O N ====
//@ Method:  TaskMonitorQueueMsg(msgId)  [Constructor]
//
//@ Interface-Description
//  Construct an instance using 'msgId' to identify this instance as a
//  "ping" message or "response" message.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (msgId == TaskMonitorQueueMsg::PING_MSG_ID  ||
//   msgId == TaskMonitorQueueMsg::RESPONSE_MSG_ID)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskMonitorQueueMsg::TaskMonitorQueueMsg(
			const TaskMonitorQueueMsg::TaskMonitorMsgId msgId
					)
{
  CALL_TRACE("TaskMonitorQueueMsg(msgId)");

  // initialize full class to '0'...
  msg_.rawMsg = 0u;

  switch (msgId)
  {
  case TaskMonitorQueueMsg::RESPONSE_MSG_ID :		// $[TI1]
	  // store this task's ID in the message...
	  //this is the Task number, not necessarely the actual thread ID
	  msg_.taskMsg.msgData = Task::GetTaskBaseInfo().GetIndex();
	  
	  // fall through...
  case TaskMonitorQueueMsg::PING_MSG_ID :		// $[TI2]
	  // store 'msgId' as the task-monitor message type...
	  msg_.taskMsg.msgId = msgId;
	  break;

  default :
	  // invalid 'msgId'...
	  AUX_CLASS_ASSERTION_FAILURE(msgId);
	  break;
  };
}


//============== M E T H O D   F U N C T I O N   D E S C R I P T I O N ====
//@ Method: ~TaskMonitorQueueMsg()  [Destructor] 
//
//@ Interface-Description
//  De-initialize this instance.
//---------------------------------------------------------------------
//@ Implementation-Description 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================

TaskMonitorQueueMsg::~TaskMonitorQueueMsg(void)
{
  CALL_TRACE("~TaskMonitorQueueMsg()"); 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskMonitorQueueMsg::SoftFault(const SoftFaultID  softFaultID,
			       const Uint32       lineNumber,
			       const char*        pFileName,
			       const char*        pPredicate)
{
  FaultHandler::SoftFault(softFaultID, SAFETY_NET, TASKMONITORQUEUEMSG, 
			  lineNumber, pFileName, pPredicate);
}
