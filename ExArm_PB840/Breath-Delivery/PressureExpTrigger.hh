#ifndef PressureExpTrigger_HH
#define PressureExpTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PressureExpTrigger - Triggers exhalation based on maximum allowed
//    pressure defined in the breath phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureExpTrigger.hhv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 003  By:  sp    Date:  26-Apr-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Add new method getMaxInspiratoryPressure for TUV.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "BreathTrigger.hh"

//@ End-Usage

class PressureExpTrigger : public BreathTrigger{
  public:
    PressureExpTrigger(const Trigger::TriggerId id);
    virtual ~PressureExpTrigger(void);

    inline Real32 getMaxInspiratoryPressure(void) const;
    void updateTargetPressure(const Real32 targetPressure); 
    virtual void enable(void); 

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
 
  protected:
    virtual Boolean triggerCondition_();

    //@ Data-Member:  maxInspiratoryPressure_
    // It is the maximum inspiratory pressure
    Real32  maxInspiratoryPressure_;

    //@ Data-Member:  intervalNumber_
    // interval number counter
    Int32  intervalNumber_;

  private:
    PressureExpTrigger(void);
    PressureExpTrigger(const PressureExpTrigger&);		// not implemented...
    void   operator=(const PressureExpTrigger&);	// not implemented...

};


// Inlined methods...
#include "PressureExpTrigger.in"


#endif // PressureExpTrigger_HH 
