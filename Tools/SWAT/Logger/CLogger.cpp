//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file CLogger.cpp
//----------------------------------------------------------------------------


#include "stdafx.h"
#include "Clogger.h"

ILogger *CLogger::m_ILogger=0;

CLogger::CLogger(){
}

void CLogger::set(ILogger *pILogger){
    m_ILogger=pILogger;
}


//Global instance
CLogger  g_GlobalLogger;

