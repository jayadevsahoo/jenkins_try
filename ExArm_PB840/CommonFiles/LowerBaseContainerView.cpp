#include "stdafx.h"
// LowerBaseContainerView.cpp : implementation file
//
#ifdef SIGMA_GUIPC_CPU
#include "ExArm.h"
#else
#include "main_e600GUI.h"
#endif

#include "LowerBaseContainerView.h"
#include "GuiFoundation.hh"


// CLowerBaseContainerView
////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(CLowerBaseContainerView, BaseContainerView)

CLowerBaseContainerView::CLowerBaseContainerView()
{

}

CLowerBaseContainerView::~CLowerBaseContainerView()
{
}




// CLowerBaseContainerView drawing

void CLowerBaseContainerView::OnDraw(CDC* pDC)
{
	// TODO: add draw code here

}


// CLowerBaseContainerView diagnostics

#ifdef _DEBUG
void CLowerBaseContainerView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CLowerBaseContainerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// CLowerBaseContainerView message handlers


void CLowerBaseContainerView::CreateLowerBaseViewCDC(){


#ifdef SIGMA_GUIPC_CPU
	if(((CExArmApp *)AfxGetApp())->ptrLowerCDC_ == NULL) {
		((CExArmApp *)AfxGetApp())->ptrLowerCDC_ = new CClientDC(this);
	}
#else
	if(((Cmain_e600GUIApp *)AfxGetApp())->ptrLowerCDC_ == NULL) {
		((Cmain_e600GUIApp *)AfxGetApp())->ptrLowerCDC_ = new CClientDC(this);
	}
#endif
}

LONG CLowerBaseContainerView::GetOriginY()
{
	return GuiFoundation::SCREEN_HEIGHT_PIXELS;
}
