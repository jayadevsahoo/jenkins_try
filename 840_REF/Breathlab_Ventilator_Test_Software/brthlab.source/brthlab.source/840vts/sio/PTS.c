/*****************************************************************************/
/*   Module Name:   PTS.c                                            
/*   Purpose:      PTS Serial Communications Module                                
/*                                                                          
/*                                                                           
/*   Requirements:                                                 
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETRIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.

Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <rs232.h>
#include <toolbox.h>

#include "commreg.h"
#include "PTS.h"
#include "ptsreset.h"

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#define TEST_STR        "X\rv\r"
#define TEST_STR_LEN    4
#define TEST_STR_RESPONSE_STR       "Revision"
#define TEST_STR_RESPONSE_STR_LEN   8 

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

// structure to hold all com variables
static COM_STRUCT com;
static int PTSconnected = 0;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     IsPTSConnected                                                     
/*                                                                           
/* Purpose:  Returns value of last search for the PTS.
/*                                          
/*                                                                           
/* Example Call:  int IsPTSConnected(void);                                    
/*                                                                           
/* Input:    None        
/*                                                                           
/* Output:   int - 0(not connected) or 1(connected)                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int IsPTSConnected(void)
{
	return PTSconnected;
}

/*****************************************************************************/
/* Name:     EstablishPTS                                                      
/*                                                                           
/* Purpose:  Establish initial communications with the PTS.
/*                                          
/*                                                                           
/* Example Call:   int EstablishPTS(void);                                    
/*                                                                           
/* Input:    None          
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                       
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int EstablishPTS(int autoDetect)
{
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors

	// try to establish communications with the PTS
	if(ComEstablish(&com, autoDetect) == SUCCESS){
		PTSconnected  = 1;
		com.connected = 1;
		com.timeout   = 3;      // increase timeout once connected
		nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if testing for a non-existent port
		SetComTime(com.port, com.timeout);
		SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
		UpdatePTSInfo();

		return SUCCESS;   // success
	}
	else{
		PTSconnected = 0;
		return FAILURE;   // failure
    }
}

/*****************************************************************************/
/* Name:     SendPTS                                                     
/*                                                                           
/* Purpose:  Send data to the PTS.
/*                                          
/*                                                                           
/* Example Call:   int SendPTS(char* msg, int len);                                    
/*                                                                           
/* Input:    msg - a pointer to the message being sent
/*  len - the length of the string pointed to by msg         
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int SendPTS(char* msg, int len)
{
	int tries;
	char ret;

	if(!com.connected){
		return FAILURE;   // failure, not connected
	}

	// keep trying to send message 
	for(tries = 0; tries < MAX_COM_TRIES; tries++){
		// send the message
		if(ComSend(msg, len, &com) == SUCCESS){
			return SUCCESS;
		}

		// flush Q, lets try sending again
		FlushInQ(com.port);
		FlushOutQ(com.port);
	}

	return FAILURE;   // failure
}

/*****************************************************************************/
/* Name:     SendPTSByte                                                      
/*                                                                           
/* Purpose: Send a data byte to the PTS.
/*                                          
/*                                                                           
/* Example Call:   int SendPTSByte(int byte);                                    
/*                                                                           
/* Input:    byte - the byte of data being sent to PTS.        
/*                                                                           
/* Output:   int - the byte from the selected port, unless and error occurs. If an
/*      error occurs, the result is a negative CVI error code.                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int SendPTSByte(int byte)
{
	return ComWrtByte(com.port, byte);
}

/*****************************************************************************/
/* Name:     RecvPTSByte                                                      
/*                                                                           
/* Purpose:  Receive a data byte from the PTS.
/*                                          
/*                                                                           
/* Example Call:  int RecvPTSByte(void);                                    
/*                                                                           
/* Input:    None         
/*                                                                           
/* Output:   int - the byte from the selected port, unless and error occurs. If an
/*      error occurs, the result is a negative CVI error code.                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int RecvPTSByte(void)
{
	int charRcvd;

	charRcvd = ComRdByte(com.port); 

	return charRcvd;
}

/*****************************************************************************/
/* Name:     RecvPTS                                                      
/*                                                                           
/* Purpose:  Receive data from the PTS.
/*                                          
/*                                                                           
/* Example Call:   int RecvPTS(char* msg, int len, int* actual);                                    
/*                                                                           
/* Input:    msg - a pointer to the message being sent
/*  len - the length of the string pointed to by msg
/*  actual - the number of bytes actually read          
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int RecvPTS(char* msg, int len, int* actual)
{
	if(!com.connected){
		return FAILURE;   // failure, not connected
	}

	return ComRecv(msg, len, actual, &com);
}

/*****************************************************************************/
/* Name:     RecvPTS_CS                                                     
/*                                                                           
/* Purpose:  Receive data from the PTS and validate the checksum (CS).
/*  Not all PTS responses have a checksum attached, so use this one when the
/*  response expected will have the 1 byte checksum.
/*                                                                                                                  
/* Example Call:   int RecvPTS_CS(char* msg, int len, int* actual);                                    
/*                                                                           
/* Input:     msg - a pointer to the message being sent
/*  len - the length of the string pointed to by msg
/*  actual - the number of bytes actually read     
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int RecvPTS_CS(char* msg, int len, int* actual)
{
	int cnt;
	char checksum;

	if(!com.connected){
		return FAILURE;   // failure, not connected
	}

	// get characters from serial port
	if(ComRecv(msg, len, actual, &com) == SUCCESS){
		// add up checksum
		for(cnt = 0; cnt<*actual-1; cnt++){
			checksum += msg[cnt];
		}

		// if calculate checksum equals the sent checksum then success
		if(checksum == msg[*actual-1]){
			return SUCCESS;   // success
		}
	}

	return FAILURE; // failure
}

/*****************************************************************************/
/* Name:     TerminatePTS                                                      
/*                                                                           
/* Purpose:  Close the connection to the PTS.
/*                                                                                                                   
/* Example Call:  int TerminatePTS(void);                                    
/*                                                                           
/* Input:    None       
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                         
/*                                                                           
/* Globals Changed:  <None>                                                                                                                  
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int TerminatePTS(void)
{
	if(com.connected){
		com.connected = 0;
		return ComTerminate(&com);
	}
	else{
		return SUCCESS;   // success, not connected so don't try
	}
}

/*****************************************************************************/
/* Name:     InitComStruct                                                     
/*                                                                           
/* Purpose:  Set the com struct to its initial value.
/*                                          
/*                                                                           
/* Example Call:    void InitPTSComStruct(void);                                    
/*                                                                           
/* Input:    None          
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
 void InitPTSComStruct(void)
{
	strcpy(com.name, "PTS");
	com.uniqueID = UNIQUE_ID_PTS;

	com.timeout = 0.50;      // in seconds

	com.delayBetweenComTries = 0.1;

	com.inQueueSize  = 8192;     // large size needed for PTS "send continuous mode"
	com.outQueueSize = MAX_COM_STR;
	com.connected    = 0;

	// port parameters min and max values
	com.portMax     = MAX_SUPPORTED_COM_PORT;
	com.portMin     = 1;
	com.baudMax     = 38400;
	com.baudMin     = 38400;  
	com.parityMax   = 0;
	com.parityMin   = 0;
	com.databitsMax = 8;
	com.databitsMin = 8;
	com.stopbitsMax = 1;
	com.stopbitsMin = 1;

	// test string for com
	com.testStrLen = TEST_STR_LEN;             
	com.testStrResponseLen = TEST_STR_RESPONSE_STR_LEN;
	memcpy(com.testStr, TEST_STR, com.testStrLen);

	com.pResponseValidFunc = TestStrResponseValidFunc;
	com.pComDetectStatusFunc = NULL;

	com.registryKey = COM_PTSKEY;
	com.autoDetectDisabled = 0;

	if(ReadComConfig(&com) != SUCCESS){
		com.port     = 0;
		com.baud     = com.baudMin;
		com.parity   = com.parityMin;
		com.databits = com.databitsMin;
		com.stopbits = com.stopbitsMin;

		WriteComConfig(&com);
	}

	return;
}

/*****************************************************************************/
/* Name:     TestStrResponseValidFunc                                                    
/*                                                                           
/* Purpose:  Checks if the response to sending the test string is what 
/*  was expected. To autodetect the COM port settings, we send the same message
/*  to the device, each time changing the port settings. Once we get a correct
/*  response, then we have found the correct baud/parity/databits/...
/*                                                                                                                   
/* Example Call:  static int TestStrResponseValidFunc(char* str, int len);                                    
/*                                                                           
/* Input:    char* - pointer to response message string
/*  len - the length of the message string                                               
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static int TestStrResponseValidFunc(char* str, int len)
{
	int cnt;
	char* responseStr = TEST_STR_RESPONSE_STR;

	// don't both trying to match, the str is not long enough
	if(len < TEST_STR_RESPONSE_STR_LEN){
		return FAILURE;
	}

	// if response matches expected response, then success
	for(cnt = 0; cnt<TEST_STR_RESPONSE_STR_LEN; cnt++){
		if(str[cnt] != responseStr[cnt]){
			return FAILURE;
		}
	}

	return SUCCESS;   
}

/*****************************************************************************/
/* Name:     FlushInQPTS                                                     
/*                                                                           
/* Purpose:  Flush all characters from the in queue.
/*                                          
/*                                                                           
/* Example Call:   int FlushInQPTS(void);                                    
/*                                                                           
/* Input:    None         
/*                                                                           
/* Output:   int - the byte from the selected port, unless and error occurs. If an
/*      error occurs, the result is a negative CVI error code.                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int FlushInQPTS(void)
{
	return ComFlushInQ(&com);
}

/*****************************************************************************/
/* Name:     ComFlushOutQ                                                     
/*                                                                           
/* Purpose:  Flush all characters from the out queue.
/*                                          
/*                                                                           
/* Example Call:   int FlushOutQPTS(void);                                    
/*                                                                           
/* Input:    None       
/*                                                                           
/* Output:   int - the byte from the selected port, unless and error occurs. If an
/*      error occurs, the result is a negative CVI error code.                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int FlushOutQPTS(void)
{
	return ComFlushOutQ(&com);
}

/*****************************************************************************/
/* Name:     CharsInPTSQ                                                     
/*                                                                           
/* Purpose:  Are chars in the serial port in Q waiting to be 
/*  processed?
/*                                          
/*                                                                           
/* Example Call:   int CharsInPTSQ(void);                                    
/*                                                                           
/* Input:    None        
/*                                                                           
/* Output:   int - # of characters in input queue, 0 if none.,                                                       
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int CharsInPTSQ(void)
{
	int numCharsInQ;

	numCharsInQ = GetInQLen(com.port);

	return numCharsInQ;
}

/*****************************************************************************/
/* Name:     ComBreakPTS                                                    
/*                                                                           
/* Purpose:  Send com break signal to PTS.
/*                                          
/*                                                                           
/* Example Call:   int ComBreakPTS(int breakTime);                                    
/*                                                                           
/* Input:    None          
/*                                                                           
/* Output:   int - LabWindows CVI ComBreak() function return value.                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int ComBreakPTS(int breakTime)
{
	return ComBreak(com.port, breakTime);
}

/*****************************************************************************/
/* Name:     RegisterForPTSDetectCB                                                      
/*                                                                           
/* Purpose:  Register for a callback during the com detection 
/*  sequence. When the PTS-2000 is being detected, it tries numerous different
/*  com ports and speed settings. During this process, the UI needs to 
/*  know what's happening so the user can be informed. Also, if the user
/*  decides to cancel the com code needs to abort the detection process.
/*
/*  The caller can register a function pointer that conforms to the 
/*  ComDetectStatusFuncPtr typedef. The com routines will call this 
/*  function upon changing to each different com port. It will also
/*  send indication on whether the detection was successful. The implementer
/*  of this routine returns 0 if the detection process is to continue, otherwise
/*  a 1 will cause the com detection algorithm to abort early. 
/*                                                                           
/* Example Call:   void RegisterForPTSDetectCB(ComDetectStatusFuncPtr func);                                    
/*                                                                           
/* Input:    func - pointer to the callback function.        
/*                                                                           
/* Output:   None                                                        
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void RegisterForPTSDetectCB(ComDetectStatusFuncPtr func)
{
	com.pComDetectStatusFunc = func;
}
