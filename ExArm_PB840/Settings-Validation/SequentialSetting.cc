#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  SequentialSetting - Sequential Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the setting's framework for the Sequential Settings.
//  All sequential settings ultimately are derived from this setting.  This
//  class overrides some of the virtual methods of 'Setting' including
//  those to:  update to a sequential setting's default and new-patient
//  values, respond to an activation by the operator, calculate a new
//  sequential value based on a knob delta, and to reset a sequential
//  setting's value to its value upon activation.  This class also defines
//  new virtual methods, with default behavior, that can be overridden by
//  derived classes, if need be.  These new virtual methods include methods
//  to:  get this sequential setting's default and new-patient value, get
//  this sequential setting's "accepted" and "adjusted" value, accept a new
//  value from a primary setting, calculate (update) this settings dependent
//  settings, and store a new value for this setting into the Adjusted
//  Context.  These methods are overridden, if needed, by the derived
//  sequential settings to provide the specific behavior needed.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to establish the sequential setting's
//  framework for the rest of the settings to build upon.
//
//  This class originated after its counterparts (i.e., BoundedSetting
//  and DiscreteSetting) were fairly well matured.  At that time, it was
//  realized that another category of settings was needed:  one whose value
//  didn't wrap around -- like the bounded settings, but does dampen
//  its knob sensitivity as the discrete setting do.  In other words,
//  a hybrid of the two other setting types.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class initializes its hard bound instances with the IDs and
//  values of each of the corresponding bounds (i.e., upper and lower).
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SequentialSetting.ccv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah   Date:  05-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added new pre-condition to 'calcNewValue()', to ensure that only
//	CHANGEABLE settings are directly changed.
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.12.1.0) into Rev "BiLevel" (1.12.2.0)
//	Added ability of sequential settings to disable their bounds,
//	thereby allowing their values to cycle around.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SequentialSetting.hh"
#include "SettingConstants.hh"
#include "SettingBoundTypes.hh"

//@ Usage-Classes
#include "SequentialRange.hh"
//@ End-Usage

//@ Code...

//===================================================================
//
//  Public Method...
//
//===================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~SequentialSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SequentialSetting::~SequentialSetting(void)
{
  CALL_TRACE("~SequentialSetting()");
}


//============== M E T H O D   D E S C R I P T I O N ================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  Calculate, and initialize to, the new sequential value.  If a bound
//  violation occurs, the violation information will be stored in the
//  returned bound status.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The value of 'knobDelta' is clipped if its absolute value is greater
//  than a value of 3.
//
//  $[02001] -- The validation of a proposed setting value...
//---------------------------------------------------------------------
//@ PreCondition
//  (getApplicability() == Applicability::CHANGEABLE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus&
SequentialSetting::calcNewValue(const Int16 knobDelta)
{
  CALL_TRACE("calcNewValue(knobDelta)");

#if !defined(SIGMA_UNIT_TEST)
  // checked when NOT unit testing...
  {
    // batch settings can only be modified in the ADJUSTED context, while
    // non-batch settings can only be modified in the ACCEPTED context...
    const Notification::ChangeQualifier  QUALIFIER_ID =
      (SettingId::IsBatchId(getId())) ? Notification::ADJUSTED
				      : Notification::ACCEPTED;
    AUX_CLASS_PRE_CONDITION(
		    (getApplicability(QUALIFIER_ID) == Applicability::CHANGEABLE),
			    getId());
  }
#endif // !defined(SIGMA_UNIT_TEST)

  // maximum delta accepted at one time; this is used to "clip" the rate
  // of changes to sequential settings, to a more acceptable rate...
  static const Int16  MAX_DELTA_ = 3;

  // store whether this knob delta will cause an increase to this setting's
  // value...
  const Boolean  IS_INCREASING = (knobDelta > 0);

  BoundStatus&  rBoundStatus = getBoundStatus_();

  // reset the setting's bound status...
  rBoundStatus.reset();

  SequentialValue  proposedValue;

  // get the current value...
  proposedValue = getAdjustedValue();

  if (knobDelta < 0)
  {   // $[TI1]
    const Int16  APPLIED_DELTA = (-(knobDelta) > MAX_DELTA_)
    				  ? MAX_DELTA_		// $[TI1.1]
				  : -(knobDelta);	// $[TI1.2]

    // the decremented value is returned in 'proposedValue', and any bound
    // violation is returned in 'rBoundStatus'...
    rSequentialRange_.decrement(APPLIED_DELTA, proposedValue, rBoundStatus);

    if (IS_IN_WRAPPING_MODE_  &&  rBoundStatus.isInViolation())
    {   // $[TI1.3] -- in "wrapping" mode, therefore reset bound and "wrap"
      rBoundStatus.reset();
      proposedValue = rSequentialRange_.getMaxConstraintValue();
    }   // $[TI1.4]
  }
  else
  {   // $[TI2]
    const Int16  APPLIED_DELTA = (knobDelta > MAX_DELTA_)
    				  ? MAX_DELTA_		// $[TI2.1]
				  : knobDelta;		// $[TI2.2]

    // the incremented value is returned in 'proposedValue', and any bound
    // violation is returned in 'rBoundStatus'...
    rSequentialRange_.increment(APPLIED_DELTA, proposedValue, rBoundStatus);

    if (IS_IN_WRAPPING_MODE_  &&  rBoundStatus.isInViolation())
    {   // $[TI2.3] -- in "wrapping" mode, therefore reset bound and "wrap"
      rBoundStatus.reset();
      proposedValue = rSequentialRange_.getMinConstraintValue();
    }   // $[TI2.4]
  }

  SAFE_CLASS_POST_CONDITION((rSequentialRange_.isLegalValue(proposedValue)),
			     SequentialRange);

  SequentialValue  currAdjustedValue;

  // get the current adjusted value of this setting...
  currAdjustedValue = getAdjustedValue();

  if (proposedValue != currAdjustedValue)
  {   // $[TI3]
    // the new value is valid against this setting's bounds and different
    // from what is currently stored in the Adjusted Context, therefore
    // store it in the Adjusted Context to allow access to this new value
    // during the dependent setting updates...
    setAdjustedValue(proposedValue);

    // update the dependent settings...
    calcDependentValues_(IS_INCREASING);
  }   // $[TI4] -- the value has not changed...

  // at this point this setting shall be valid with its bounds, and the
  // values of its dependent settings...
  SAFE_AUX_CLASS_ASSERTION((isAdjustedValid()), getId());

  return(rBoundStatus);
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
SequentialSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  const SequentialValue  ACCEPTED_VALUE = getAcceptedValue();

  return(rSequentialRange_.isLegalValue(ACCEPTED_VALUE));
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
SequentialSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  // update this setting's dynamic contraints, if any...
  updateConstraints_();

  const SequentialValue  ADJUSTED_VALUE = getAdjustedValue();

  return(rSequentialRange_.isLegalValue(ADJUSTED_VALUE));
}

#endif // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SequentialSetting::SoftFault(const SoftFaultID  softFaultID,
			     const Uint32       lineNumber,
			     const char*        pFileName,
			     const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
  			  SEQUENTIAL_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//======================================================================
//
//  Protected Methods...
//
//======================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method:  SequentialSetting(settingId, arrDependentSettingIds, rSequentialRange, useWrappingMode)
//
//@ Interface-Description
//  Construct the sequential setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This constructor runs to completion BEFORE the constructor for the
//  range reference via 'rSequentialRange' is run, therefore no operations
//  can be done in this constructor with 'rSequentialRange'.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsSequentialId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SequentialSetting::SequentialSetting(
		     const SettingId::SettingIdType  settingId,
		     const SettingId::SettingIdType* arrDependentSettingIds,
		     SequentialRange& rSequentialRange,
		     const Boolean    useWrappingMode
		     		    )
		     : Setting(settingId, arrDependentSettingIds),
			      	IS_IN_WRAPPING_MODE_(useWrappingMode),
		       		rSequentialRange_(rSequentialRange)
{
  CALL_TRACE("SequentialSetting(settingId, arrDependentSettingIds, rSequentialRange, useWrappingMode)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsSequentialId(settingId)),
  			  settingId);
}   // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

#include "Settings_Validation.hh"

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  print_(ostr)  [const]
//
// Interface-Description
//  Print the contents of this sequential setting into 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//  Build message with setting contents inserted in it.
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsSequentialId(getId()))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Ostream&
SequentialSetting::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsSequentialId(getId())));

  ostr << "  Name:            " << SettingId::GetSettingName(getId())
       << endl;

  if (Settings_Validation::IsDebugOn(SEQUENTIAL_SETTING))
  {
    // print out this sequential setting's range...
    ostr << rSequentialRange_;
  }

  const SequentialValue  ACCEPTED_VALUE = getAcceptedValue();

  ostr << "  Accepted Value:  " << ACCEPTED_VALUE << endl;

  return(ostr);
}

#endif  // defined(SIGMA_DEVELOPMENT)
