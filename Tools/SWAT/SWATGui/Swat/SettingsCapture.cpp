#pragma once

#include "stdafx.h"
//#include <stdio.h>
#include "ISettingsCapture.h"
//#include "Windows.h"


#include "Swat.h"




 
CGUISettingsCapture::CGUISettingsCapture()
	{

		m_pWnd = AfxGetMainWnd()->m_hWnd;
        IConfiguration_F   *f1;  
        ISettingsCapture_F *f2;
        bool bRetval;
    

        // Get a handle to the DLL module.
        hinstLib = LoadLibrary(TEXT("PCAPP.dll"));

        if(hinstLib == NULL)
        {
			AfxMessageBox("Unable to load PCApp.dll");
            throw -1;
        }
     
        f1 =  (IConfiguration_F   *)GetProcAddress(hinstLib, "getConfigurationInstance");
        f2 =  (ISettingsCapture_F *)GetProcAddress(hinstLib, "getSettingsCaptureInstance");

        if(f1==NULL || f2==NULL)
        {
			AfxMessageBox("Invalid PCApp.dll");
            FreeLibrary(hinstLib);
            throw -1;
        }

        m_pIConfiguration   =  f1();


        m_pIConfiguration     = getConfigurationInstance();

        if(m_pIConfiguration == NULL){
        //    FreeLibrary(hinstLib); 
			AfxMessageBox("Error in creating instance of getConfigurationInstance");
            throw -1;
        }

        bRetval = m_pIConfiguration->loadXMLDocument ("setting_id.xml");
        if(bRetval == false)
        {
            m_pIConfiguration->destroy();
         //   FreeLibrary(hinstLib); 
			AfxMessageBox("Unable to load setting_id.xml");
            throw -1;
        }

		CSwatApp* pApp = (CSwatApp*)AfxGetApp();
		
		static char chIPAdd[80];
		strcpy(chIPAdd, (LPCTSTR)pApp->m_strBD_IPAddr);

		strIp2ulongIp(chIPAdd);

		m_pISettingsCapture = getSettingsCaptureInstance(pApp->m_strBD_IPAddr.GetBuffer(),CGUISettingsCapture::printData,this);

        if(m_pISettingsCapture == NULL)
        {
            m_pIConfiguration->destroy();
         //   FreeLibrary(hinstLib); 
			CString strError;
			strError = "Unable to connect for capturing settings. IP address correct? WinPCAP is not installed? Check network.\n" + pApp->m_strBD_IPAddr;
			AfxMessageBox(strError);
            throw -1;
        }

}


unsigned long int CGUISettingsCapture::strIp2ulongIp(char *strIp)
{
    unsigned int  i[4];

    union{
        unsigned char j[4];
        unsigned long k;
    } m;

    if(strIp)
    {
        sscanf (strIp,"%i.%i.%i.%i",&i[0],&i[1],&i[2],&i[3]);
        m.j[0]=(unsigned char)i[0];
        m.j[1]=(unsigned char)i[1];
        m.j[2]=(unsigned char)i[2];
        m.j[3]=(unsigned char)i[3];
    }
    else
    {
        m.k = (unsigned long)0;
    }

    return m.k;
}


int CGUISettingsCapture::printData(char *pData, int iLength)
	{
			m_pIConfiguration->readContext ((char *)pData, iLength) ;
			//const char * ref=m_pIConfiguration->getCsv ();
			CString str(m_pIConfiguration->getCsv());

			//printf("%s",ref);


			CString* p = new CString(str);
			
			if(!m_pWnd) return 0; //Window is not present, do not try to send to it

			SendMessage((HWND)m_pWnd, NEW_SETTINGS_RCVD, 0, reinterpret_cast<LPARAM>(p));
	

			return 0;
	}


