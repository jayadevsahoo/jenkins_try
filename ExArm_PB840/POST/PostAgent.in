#ifndef PostAgent_IN
#define PostAgent_IN
 
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  PostAgent - POST Data Network Container Class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/PostAgent.inv   25.0.4.0   19 Nov 2013 14:17:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004    By: sah   Date: 14-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003    By: Gary Cederquist  Date: 26-FEB-1998  DR Number: 5029
//    Project: Sigma (R8027)
//    Description:
//       Corrected subsystem and class ID passed to SoftFault.
//
//  Revision: 002    By: Gary Cederquist  Date: 21-APR-1997  DR Number: 1982
//    Project: Sigma (R8027)
//    Description:
//       Changed to accomodate new part number format (length).
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================
 

//=====================================================================
//      Public Methods...
//=====================================================================
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isPostFailed
//
//@ Interface-Description
//  Returns TRUE if POST contains an uncorrected failure which would
//  force service mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the value of private postFailed_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline Boolean
PostAgent::isPostFailed(void) const
{
    CALL_TRACE("isPostFailed(void)");
    // $[TI1]

	// TODO E600 MS This is a hack for now until POST is ported.
	// Remove later and uncooment return  postFailed_
	return FALSE;
    //return  postFailed_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getBackgroundFailed
//
//@ Interface-Description
//  Returns the level of Background failures for the processor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the value of private backgroundFailed_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline Criticality
PostAgent::getBackgroundFailed(void) const
{
    CALL_TRACE("getBackgroundFailed(void)");
    // $[TI1]
    return  backgroundFailed_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isServiceModeRequested
//
//@ Interface-Description
//  For a BD PostAgent, returns TRUE if the operator has pressed the
//  service button to request service mode.  For a GUI PostAgent, it
//  always returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the value of private ServiceModeRequested.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline Boolean
PostAgent::isServiceModeRequested(void) const
{
    CALL_TRACE("isServiceModeRequested(void)");
    // $[TI1]
    return  serviceModeRequested_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getPowerDownTime
//
//@ Interface-Description
//  For a BD PostAgent, returns a TimeStamp for the last time power
//  was lost.  For the GUI PostAgent, it returns a null TimeStamp since
//  the GUI lacks a powerfail interrupt.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns const reference to private powerDownTime_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline const TimeStamp&
PostAgent::getPowerDownTime(void) const
{
    CALL_TRACE("getPowerDownTime(void)");
    // $[TI1]
    return  powerDownTime_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getPowerUpTime
//
//@ Interface-Description
//  Returns a TimeStamp for when the processor began Phase 2 POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns const reference to private powerUpTime_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline const TimeStamp&
PostAgent::getPowerUpTime(void) const
{
    CALL_TRACE("getPowerUpTime(void)");
    // $[TI1]
    return  powerUpTime_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getDowntimeStatus
//
//@ Interface-Description
//  For the BD PostAgent, returns either DOWN_SHORT if the processor
//  has been powered off 5 minutes of less or DOWN_LONG for longer
//  periods.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the value of private downtimeStatus_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline DowntimeStatus
PostAgent::getDowntimeStatus(void) const
{
    CALL_TRACE("getDowntimeStatus(void)");
    // $[TI1]
    return  DowntimeStatus(downtimeStatus_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getStartupState
//
//@ Interface-Description
//  Returns the startup state for the processor.  This state reflects 
//  the reason why the processor reentered POST.  It can be either 
//  POWERFAIL, ACSWITCH, UNKNOWN or WATCHDOG.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the value of private startupState_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline ShutdownState
PostAgent::getStartupState(void) const
{
    CALL_TRACE("getStartupState(void)");
    // $[TI1]
    return  ShutdownState(startupState_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getKernelPartNumber_
//
//@ Interface-Description
//  Returns the string containing the part number of the Kernel POST
//  in PROM.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns const pointer to private kernelPartNumber_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline const char *
PostAgent::getKernelPartNumber(void) const
{
    CALL_TRACE("getKernelPartNumber(void)");
    // $[TI1]
    return  kernelPartNumber_;
}

#endif // PostAgent_IN
