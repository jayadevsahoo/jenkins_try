// TabTwo.cpp : implementation file
//

#include "stdafx.h"
#include "E600Util.h"
#include "TabTwo.h"
#include "UserMessages.h"
#include "BDSignals.h"

// CTabTwo dialog

IMPLEMENT_DYNAMIC(CTabTwo, CDialog)

CTabTwo::CTabTwo(CWnd* pParent /*=NULL*/)
	: CDialog(CTabTwo::IDD, pParent)
    , Signal1(0)
    , Signal2(0)
    , Signal3(0)
{
	bFreezed1 = FALSE;
}

CTabTwo::~CTabTwo()
{
}

BOOL CTabTwo::OnInitDialog()
{
	CDialog::OnInitDialog();

	CRect rect;	//(l,t,r,b)	
	GetClientRect(&rect);
	int nClientWidth = rect.Width();
	int nClientHeight = rect.Height();

	m_GlobalInvalidateRgn.CreateRectRgn(0, 0, 0, 0);

	GetDlgItem(IDC_STATIC_PIC_CTRL1)->GetWindowRect(&Wave1Rect);
	ScreenToClient(&Wave1Rect);
	WaveRect[0] = Wave1Rect;
	Plot* pPlot1 = new Plot(m_hWnd, Wave1Rect.Width(), Wave1Rect.Height(), 10, 100, 50, 0);
	m_Plots.push_back(pPlot1);

	GetDlgItem(IDC_STATIC_PIC_CTRL2)->GetWindowRect(&Wave2Rect);
	ScreenToClient(&Wave2Rect);
	WaveRect[1] = Wave2Rect;
	Plot* pPlot2 = new Plot(m_hWnd, Wave2Rect.Width(), Wave2Rect.Height(), 10, 100, 50, 0);
	m_Plots.push_back(pPlot2);

	GetDlgItem(IDC_STATIC_PIC_CTRL3)->GetWindowRect(&Wave3Rect);
	ScreenToClient(&Wave3Rect);
	WaveRect[2] = Wave3Rect;
	Plot* pPlot3 = new Plot(m_hWnd, Wave3Rect.Width(), Wave3Rect.Height(), 10, 50, 0, -50);
	m_Plots.push_back(pPlot3);
	
	CString StrMax, StrMin;
	StrMax.Format(L"100");
	StrMin.Format(L"0");
	GetDlgItem(IDC_EDIT_MAX1)->SetWindowTextW(StrMax);
	GetDlgItem(IDC_EDIT_MIN1)->SetWindowTextW(StrMin);

	StrMax.Format(L"100");
	StrMin.Format(L"0");
	GetDlgItem(IDC_EDIT_MAX2)->SetWindowTextW(StrMax);
	GetDlgItem(IDC_EDIT_MIN2)->SetWindowTextW(StrMin);

	StrMax.Format(L"50");
	StrMin.Format(L"-50");
	GetDlgItem(IDC_EDIT_MAX3)->SetWindowTextW(StrMax);
	GetDlgItem(IDC_EDIT_MIN3)->SetWindowTextW(StrMin);

#if 0	
	if(!SetTimer(1, 20, NULL))
	{
		MessageBox(L"Error: SetTimer 1 failed");
	}
	if(!SetTimer(2, 33, NULL))
	{
		MessageBox(L"Error: SetTimer 2 failed");
	}
#endif

	CButton *pCheckButton = (CButton *)GetDlgItem(IDC_CHECK_PB880_VENT);
	pCheckButton->SetCheck(BST_CHECKED);
	SetDataSpeed(PB880_DATA_SPEED);

	m_Client.pDlg=this;
	return TRUE;
}

void CTabTwo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTabTwo, CDialog)
	ON_WM_PAINT()
	ON_MESSAGE(WM_BD_DATA, OnBDData)
	ON_MESSAGE(WM_HEADER_STRING, OnHeaderString)
	ON_BN_CLICKED(IDC_BUTTON_SET1, &CTabTwo::OnBnClickedButtonSet1)
	ON_BN_CLICKED(IDC_BUTTON_SET2, &CTabTwo::OnBnClickedButtonSet2)
	ON_BN_CLICKED(IDC_BUTTON_SET3, &CTabTwo::OnBnClickedButtonSet3)
	ON_CBN_SELCHANGE(IDC_COMBO_WAVE_SELECT1, &CTabTwo::OnCbnSelchangeComboWaveSelect1)
	ON_CBN_SELCHANGE(IDC_COMBO_WAVE_SELECT2, &CTabTwo::OnCbnSelchangeComboWaveSelect2)
	ON_CBN_SELCHANGE(IDC_COMBO_WAVE_SELECT3, &CTabTwo::OnCbnSelchangeComboWaveSelect3)
	ON_BN_CLICKED(IDC_BUTTON_FREEZE1, &CTabTwo::OnBnClickedButtonFreeze1)
	ON_BN_CLICKED(IDC_CHECK_PB880_VENT, &CTabTwo::OnBnClickedButtonPB880)
END_MESSAGE_MAP()


// CTabTwo message handlers
void CTabTwo::OnPaint()
{
	//////////////////////////////////////// draw all graphs //////////////////////////////////////////////////////

	CPaintDC paintDC(this);
	
	for (int i = 0; i < PLOT_COUNT; ++ i)
	{
		m_Plots[i]->draw(&paintDC, WaveRect[i].left, WaveRect[i].top);
	}
}


void CTabTwo::UpdateWaveform(int Index, float Data)
{
	CRect rcWave;
	m_Plots[Index]->getWaveRect(rcWave);
	
	// move wave area down for the 2nd and 3rd waveforms
	rcWave.OffsetRect(WaveRect[Index].left, WaveRect[Index].top);
	
	m_Plots[Index]->appendData( Data );

	// refresh whole wave
	cacheInvalidateRgn_(rcWave);
}


LRESULT CTabTwo::OnBDData(WPARAM x, LPARAM y)
{
	if(bFreezed1)
		return 0;
	
	int nBytes = (int)x;
	float *pData = (float *)y;
	int nData = nBytes / sizeof(float);
	float SignalData[NUM_SIGNALS][NUM_SLOTS];

	memcpy(SignalData, pData, nBytes);
	for(int i=0; i<NUM_SIGNALS; i++)
	{
		MonitorData[i] = SignalData[i][NUM_SLOTS-1];
	}

	UpdateWaveform(0, MonitorData[Signal1]);
	UpdateWaveform(1, MonitorData[Signal2]);
	UpdateWaveform(2, MonitorData[Signal3]);
	
	InvalidateRgn(&m_GlobalInvalidateRgn, FALSE);
	m_GlobalInvalidateRgn.SetRectRgn(0, 0, 0, 0);

	return 0;
}

void CTabTwo::cacheInvalidateRgn_( const CRect& rect )
{
	CRgn rgnCopy;
	rgnCopy.CreateRectRgn(0, 0, 0, 0);
	rgnCopy.CopyRgn(&m_GlobalInvalidateRgn);	// copy from region m_GlobalInvalidateRgn to rgnCopy

	CRgn rgnNew;
	rgnNew.CreateRectRgnIndirect(&rect);		// create a new region from input rect

	m_GlobalInvalidateRgn.CombineRgn(&rgnCopy,  &rgnNew, RGN_OR);	// combine the existing region and the new region

	rgnNew.DeleteObject();
	rgnCopy.DeleteObject();
}

LRESULT CTabTwo::OnHeaderString(WPARAM x, LPARAM y)
{
	CString HeaderStr = (wchar_t *)y;	
	vector<CString>WaveSignals;
	ParseCSVHeader(HeaderStr, WaveSignals);

	CComboBox *pComboBox1 = (CComboBox *)(GetDlgItem(IDC_COMBO_WAVE_SELECT1));
	CComboBox *pComboBox2 = (CComboBox *)(GetDlgItem(IDC_COMBO_WAVE_SELECT2));
	CComboBox *pComboBox3 = (CComboBox *)(GetDlgItem(IDC_COMBO_WAVE_SELECT3));

	if(pComboBox1->GetCount() > 0)
	{
		pComboBox1->ResetContent();
		pComboBox2->ResetContent();
		pComboBox3->ResetContent();
	}

	for(int i=0; i<(int)WaveSignals.size(); i++)
	{
		pComboBox1->AddString(WaveSignals[i]);
		pComboBox2->AddString(WaveSignals[i]);
		pComboBox3->AddString(WaveSignals[i]);
	}

	if(WaveSignals.size() > 0)
	{
		pComboBox1->SelectString(-1, L"Pexp");
		Signal1 = pComboBox1->GetCurSel();
		pComboBox2->SelectString(-1, L"Qair");
		Signal2 = pComboBox2->GetCurSel();
		pComboBox3->SelectString(-1, L"Qexh");
		Signal3 = pComboBox3->GetCurSel();
	}
	
	return 0;
}

void CTabTwo::SetPlotScale(UINT MaxId, UINT MinId, int Index)
{
	CString strMax;
	CString strMin;

	GetDlgItem(MaxId)->GetWindowTextW(strMax);
	GetDlgItem(MinId)->GetWindowTextW(strMin);

	int iMax = _tstoi(strMax);
	int iMin = _tstoi(strMin);

	if ( iMax > iMin )
	{
		int iMid = (iMax - iMin) / 2 + iMin;

		m_Plots[Index]->setY( iMin, iMid, iMax );
	}

	InvalidateRect(&WaveRect[Index]);
}

void CTabTwo::SetScale()
{
	SetPlotScale(IDC_EDIT_MAX1, IDC_EDIT_MIN1, 0);
	SetPlotScale(IDC_EDIT_MAX2, IDC_EDIT_MIN2, 1);
	SetPlotScale(IDC_EDIT_MAX3, IDC_EDIT_MIN3, 2);
}

void CTabTwo::OnBnClickedButtonSet1()
{
	SetScale();
}

void CTabTwo::OnBnClickedButtonSet2()
{
	SetScale();
}

void CTabTwo::OnBnClickedButtonSet3()
{
	SetScale();
}

void CTabTwo::OnCbnSelchangeComboWaveSelect1()
{
	CComboBox *pComboBox = (CComboBox *)GetDlgItem(IDC_COMBO_WAVE_SELECT1);
	Signal1 = pComboBox->GetCurSel();
}

void CTabTwo::OnCbnSelchangeComboWaveSelect2()
{
	CComboBox *pComboBox = (CComboBox *)GetDlgItem(IDC_COMBO_WAVE_SELECT2);
	Signal2 = pComboBox->GetCurSel();
}

void CTabTwo::OnCbnSelchangeComboWaveSelect3()
{
	CComboBox *pComboBox = (CComboBox *)GetDlgItem(IDC_COMBO_WAVE_SELECT3);
	Signal3 = pComboBox->GetCurSel();
}

void CTabTwo::OnBnClickedButtonFreeze1()
{
	CButton *pFreezeButton = (CButton *)GetDlgItem(IDC_BUTTON_FREEZE1);
	if(bFreezed1)
	{
		//unfreeze
		SetScale();
		bFreezed1 = FALSE;
		pFreezeButton->SetWindowTextW(L"Freeze");
	}
	else
	{
		bFreezed1 = TRUE;
		pFreezeButton->SetWindowTextW(L"UnFreeze");
	}
}


void CTabTwo::SetDataSpeed(int speed)
{
	for (int i = 0; i < PLOT_COUNT; ++ i)
	{
		m_Plots[i]->setDataSpeed(speed);
	}
}

void CTabTwo::OnBnClickedButtonPB880()
{

	CButton *pCheckButton = (CButton *)GetDlgItem(IDC_CHECK_PB880_VENT);
	if(pCheckButton->GetCheck())
		SetDataSpeed(PB880_DATA_SPEED);
	else
		SetDataSpeed(E600_DATA_SPEED);

}
