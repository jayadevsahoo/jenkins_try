#ifndef FsmState_HH
#define FsmState_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  FsmState - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/FsmState.hhv   25.0.4.0   19 Nov 2013 14:33:52   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Added IsTransitioning() method to determine if the state
//         machine is in transition.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
//@ Usage-Classes
#include "Sys_Init.hh"
#include "SigmaState.hh"
//@ End-Usage

class TaskControlMessage;
class BdReadyMessage;
class BdFailedMessage;
class GuiReadyMessage;
class ChangeStateMessage;
class CommUpMessage;
class CommDownMessage;
class StartupTimer;
class SyncClockMessage;
class SyncClockTimer;
class TransitionTimer;
class TaskReadyMessage;

class FsmState
{
  public:
    FsmState();
    virtual ~FsmState();
    virtual const char* nameOf(void) const = 0;
    virtual SigmaState getSigmaState(void) const = 0;

    virtual void activate(void) const;
    virtual void receiveBdReady(const BdReadyMessage& message) const;
    virtual void receiveGuiReady(const GuiReadyMessage& message) const;
    virtual void receiveCommUp(const CommUpMessage& message) const;
    virtual void receiveCommDown(const CommDownMessage& message) const;

    virtual void receiveChangeState(const ChangeStateMessage& message) const;
    virtual void receiveSyncClock(const SyncClockMessage& message) const;
    virtual void receiveStartupTimer(const StartupTimer& message) const;
    virtual void receiveSyncClockTimer(const SyncClockTimer& message) const;
    virtual void receiveTransitionTimer(const TransitionTimer& message) const;
    virtual void receiveTaskReady(const TaskReadyMessage& message) const;
    virtual void receiveBdFailed(const BdFailedMessage& message) const;
    virtual void invalid(const TaskControlMessage& message) const;
    void startTransition(void) const;
 
    static void             Initialize(void);
    static void             Start(void);

    static const FsmState&  GetCurrentState(void);
    static const FsmState&  GetTargetState(void);
    static SigmaState       GetGuiState(void);
    static SigmaState       GetBdState(void);
    static Boolean          IsTransitioning(void);

    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);

    friend  class GuiFsm;
    friend  class BdFsm;

  protected:

    static void             SetGuiState(const GuiReadyMessage& message);
    static void             SetBdState(const BdReadyMessage& message);

  private:
    FsmState(const FsmState&);                // declared only
    void operator=(const FsmState&);          // declared only
 
    static void             Activate_(void);

};
 

#endif // FsmState_HH
