#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: RecvSocket  - Provides interfaces to the main socket 
//      connection between the GUI and BD boards that is mainly used
//      to receive data from the other Sigma board.
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/RecvSocket.ccv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: gdc     Date: 30-Dec-2010  SCR Number: 6714
//  Project:  XENA2
//  Description:
//      Changed so an out of mbufs condition brings down communications
//      allowing stack to free all mbufs and restart communications.
//
//  Revision: 004  By: gdc     Date: 30-Dec-2010  SCR Number: 6716
//  Project:  XENA2
//  Description:
//      Changed to use thread-safe socket library.
//
//  Revision: 003  By: gdc     Date: 30-Dec-2010  SCR Number: 6717
//  Project:  XENA2
//  Description:
//      Corrected creation and initialization of all socket types.
//
//  Revision: 002  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  15-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "RecvSocket.hh"
#include "NetworkMsgHeader.hh"
#include "NetworkApp.hh"
#include "CommFaultId.hh"
#include "TaskMonitor.hh"



//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RecvSocket()  [Default Constructor]
//
//@ Interface-Description
//    Initilizes all the instance variables, sets the Active
//    Message Delay counter to zero. Set the socket state to be closed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

RecvSocket::RecvSocket(void)
{
    setMsgDelayCounter(0); 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~RecvSocket()  [Destructor]
//
//@ Interface-Description
//   Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Close the socket if it is opened()
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

RecvSocket::~RecvSocket(void)
{
    if ( opened() )
    {
        down(); 
    }
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reInit(Int fd) 
//
//@ Interface-Description
//	  Sets the socket state to be open. Initializes the socket id with
//    a passed in socket id. Reinitialize the data packet sequence number
//    to zero.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	(fd > 0)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
RecvSocket::reInit(Int fd)
{
    NetworkSocket::reInit(fd);
    setMsgDelayCounter(0);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  readMainData(void)
//
//@ Interface-Description
// 	The method reads the main data that comes from the other Sigma
//  board through this socket connection. The method expects 
//  one of the three types of network messages, 
//			 . a "I am alive" message
//			 . a user network message that needs to be passed to
// 			   callback routines.
//   If it fails to receive any one of the above messages, it increments
//   the active message delay counter.
//   When it receives a network message successfully, the active message
//   delay counter is set back to zero. The method delivers a received
//   user network message to the callback routines that are registered
//   for the received network message type.
//   The method returns  -1 when it encounters a communication error.
//   Otherwise, the method returns 0.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//   The method tries to read a network message header first to
//   determine what type of network message and the actual size of
//   the network message following. It calls the readData method of 
//   parent class, NetworkSocket, to actually read data from the
//   the network connection. The I am alive message doesn't
//   have any data associated with it, just a message header is sent
//   for the message type. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
RecvSocket::readMainData()
{
    NetworkMsgHeader header;
    Uint size;

    size = readData((char *) &header, sizeof(header));
    if ( size  != sizeof(header) )
    {
        setLostMsgFlag_(TRUE);
        NetworkApp::ReportCommError("RecvSocket::readMainData()",
                                    NETWORKAPP_INVALID_COMM_DATA); 
        return -1;
    }
	//else, proper header was received..
	header.convNtoH();	//fix the header endianness
	if ( header.pktSize >  MAX_NETWORK_BUF_SIZE )
    {
        setLostMsgFlag_(TRUE);
        NetworkApp::ReportCommError("RecvSocket::readMainData()",
                                    NETWORKAPP_DATA_TOO_LARGE); 
        return -1;
    }
	//If the header indicates there is data in msg (i.e. pktSize > 0), receive data, 
	//returne failure if num bytes recieved not equal number indicated in pktSize
    if (header.pktSize &&
        (readData((char *)recvBuffer_, header.pktSize) != header.pktSize ))
    {
        setLostMsgFlag_(TRUE);
        NetworkApp::ReportCommError("RecvSocket::readMainData()",
                                    NETWORKAPP_INVALID_COMM_DATA); 
        TaskMonitor::Report();
        return -1;
    }
    if (!NetworkApp::IsValidId(((XmitDataMsgId) header.msgId)))
    {
        setLostMsgFlag_(TRUE);
        NetworkApp::ReportCommError("RecvSocket::readMainData()",
                                    NETWORKAPP_INVALID_COMM_MSGID); 
        return -1;
    }
    else
    {
        setMsgDelayCounter(0);
        if ( header.msgId != NetworkApp::OtherBoardAliveMsg )
        {
            NetworkApp::CallbackMsg *pBuf;

// For GUI integration test only, ignore patient data packets
// coming from BD which are coming over written from SWAT. GUI PD will go to normal
// if it doesn't receive packets from SWAT but receives 3 consecutive packets from BD.
//
#if defined( SIGMA_GUI_CPU )
#if defined( INTEGRATION_TEST_ENABLE )
		if( (NetworkApp::m_iIsCallbackTbl[header.msgId - LOWEST_NETWORK_MSG_ID] == 0)  )
		{
#endif
#endif
			//call the registered receive callback functions
            pBuf=NetworkApp::PCallbackTbl_[header.msgId - LOWEST_NETWORK_MSG_ID];
            CLASS_ASSERTION(pBuf != NULL);
            for (;pBuf;pBuf=pBuf->pNext)
            {
                CLASS_ASSERTION(pBuf->callback != NULL);
				//Note: the individual calback functions each handles data endiannes
				//individually because they know the data type they receive.
                (*pBuf->callback)((XmitDataMsgId)header.msgId,
                                  (void *)recvBuffer_, header.pktSize);
            }

//Making sure we are not above the 3 packets limit.
#if defined( SIGMA_GUI_CPU )
#if defined( INTEGRATION_TEST_ENABLE )
		}
		else {
			if(NetworkApp::m_iIsCallbackTbl[header.msgId - LOWEST_NETWORK_MSG_ID] >3 ){
				NetworkApp::m_iIsCallbackTbl[header.msgId - LOWEST_NETWORK_MSG_ID] = 0;
			}
			else
			{
				NetworkApp::m_iIsCallbackTbl[header.msgId - LOWEST_NETWORK_MSG_ID] --;
			}
		}
#endif
#endif

        }
    }
    TaskMonitor::Report();
    return 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isMaxActiveDelay()
//		Determines if the total number of delays of receiving the
//      "I am Alive" message from the other exceeds the maximum
//      delay time defined.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Boolean
RecvSocket::isMaxActiveMsgDelay(void)
{
	return activeMsgDelay_ >= MAX_ALIVE_MSG_MISS; 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  down()
//
//@ Interface-Description
//      close the socket.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Call the parent class's close method and clear up the active
//      messge delay flag.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
RecvSocket::down(void)
{
    NetworkSocket::down();
    activeMsgDelay_ = 0; 
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifyConnection(XmitDataMsgId thisActive,
//	  XmitDataMsgId otherActive)
//
//@ Interface-Description
//    Verify if this socket connection is functioning by receiving
//    the other Sigma board alive message, then sending this Sigma
//    board alive message using this socket connection.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The messages are received and sent through the NetworkSocket
//    class's sendMsg and readData method to avoid having to handle
//    the ACK messages for these two messages.
//---------------------------------------------------------------------
//@ PreCondition
//      ( opened() )
//   This socket should have been opened.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//---------------------------------------------------------------------
SigmaStatus
RecvSocket::verifyConnection(XmitDataMsgId thisActive,
                             XmitDataMsgId otherActive)
{
    CLASS_PRE_CONDITION(opened());

    fd_set readMask;
    FD_ZERO(&readMask);
    FD_SET(socketId_, &readMask);
    if ( WaitForInput(socketId_+1, &readMask, 1000) <= 0 )
    {
        NetworkApp::ReportCommError("RecvSocket::verifyConnection WaitForInput.", 0);
        return FAILURE;
    }

	Uint nDataBytesInMsg = 0;	//expected received msg data bytes = 0 (header-only msg, no data)
	
	//Expecting a network message with header only, no data. Thus, buf = NULL
	//recvMsg returns the number of received "data" bytes disregarding the header itself..
    if ( NetworkSocket::recvMsg(otherActive, (char *) NULL, nDataBytesInMsg) != nDataBytesInMsg )
    {
        NetworkApp::ReportCommError("RecvSocket::verifyConnection recvMsg.", 0);
        return FAILURE; 
    }

	nDataBytesInMsg = 0;	//sending a msg with 0 data (header only)
	//sending a network message with header only, no data. Thus, buf = NULL
	//sendMsg returns the number of sent "data" bytes disregarding the header itself..
    if ( NetworkSocket::sendMsg(thisActive, (char *) NULL, nDataBytesInMsg) != nDataBytesInMsg )
    {
        NetworkApp::ReportCommError("RecvSocket::verifyConnection sendMsg.", 0);
        return FAILURE;  
    }

    return SUCCESS;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  incrementDelayCounter()
//      Increment total number of Delay counter that the other board is
//      missing to send a keep-alive message.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      If the activeMsgDelay_ counter reaches the maximum delay count,
//      set the communication down flag immediately to
//      avoid sending more network messages right away. $[06089] $[06091]
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
RecvSocket::incrementDelayCounter(void)
{
	if ( ++activeMsgDelay_ >= MAX_ALIVE_MSG_MISS )
    {
		//TODO E600 VM: another cause of circular dependency. NetworkSocket
		//  should not be calling NetworkApp; particularly, should not be setting
		//  something for NetworkApp on its behalf. Instead, use a callback
		//  or set a certain status here and let NetworkApp::IsCommunicationUp()
		//  poll it and combine it in its logic to determine if network is down.
        NetworkApp::SetCommDown_();
        NetworkApp::ReportCommError("RecvSocket::incrementDelayCounter()",
                                    NETWORKAPP_MAX_ACTIVE_MSG_DELAY); 
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setMsgDelayCounter(Int counter)
//      Set the  keep alive message Delay counter  to a given number.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      If the activeMsgDelay_ counter is set to a maximum delay count,
//      set the communication down flag immediately to
//      avoid sending more network messages right away. $[06089] $[06091]
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
RecvSocket::setMsgDelayCounter( Int counter )
{
    activeMsgDelay_ = counter;
    if ( counter >= MAX_ALIVE_MSG_MISS  )
    {
        NetworkApp::SetCommDown_();
        NetworkApp::ReportCommError("RecvSocket::setMsgDelayCounter()",
                                    NETWORKAPP_MAX_ACTIVE_MSG_DELAY); 
    }
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
RecvSocket::SoftFault(const SoftFaultID  softFaultID,
                      const Uint32       lineNumber,
                      const char*        pFileName,
                      const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, NETWORK_APPLICATION, RECV_SOCKET_CLASS,
                            lineNumber, pFileName, pPredicate);
}
