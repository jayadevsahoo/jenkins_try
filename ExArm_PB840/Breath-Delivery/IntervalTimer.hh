#ifndef IntervalTimer_HH
#define IntervalTimer_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: IntervalTimer - A general purpose server that is responsible to
// notify its client when a pre-set time interval has elapsed
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/IntervalTimer.hhv   25.0.4.0   19 Nov 2013 13:59:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By:  sp    Date:  29-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection Rework.
//
//  Revision: 004  By:  sp    Date:  27-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp    Date:  15-Feb-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed method changeElapsedTime.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
class TimerTarget;

//@ End-Usage

class IntervalTimer
{
  public:
    enum TimerStatus {OFF=0, ON};
    IntervalTimer(void);
    ~IntervalTimer(void);

    void updateTimer(void);
  
    inline void start(void);
    inline void stop(void);
    inline void restart(void);
    inline Int32 getCurrentTime(void) const;
    inline void setCurrentTime(const Int32 intervalMs);
    inline void setTargetTime(const Int32 intervalMs);
    inline void extendInterval(const Int32 intervalMs);
    inline Int32 getTargetTime(void) const;
    inline void setupTarget(TimerTarget* timerTarget);
    
    static void SoftFault(const SoftFaultID softFaultID,
                          const Uint32      lineNumber,
                          const char*       pFileName  = NULL,
                          const char*       pPredicate = NULL);

  protected:

  private:
    IntervalTimer(const IntervalTimer&);          // Declared but not implemented
    void operator=(const IntervalTimer&);   // Declared but not implemented

    //@ Data-Member: status_
    // status of the timer
    TimerStatus status_;
    
    //@ Data-Member: currentTime_
    // current timer count
    Int32 currentTime_;
    
    //@ Data-Member: pTargetPtr_
    // the client of this server
    TimerTarget* pTargetPtr_;  

    //@ Data-Member: targetTime_
    // target time interval
    Int32 targetTime_;  

};

// Inlined methods
#include "IntervalTimer.in"

#endif // IntervalTimer_HH 

