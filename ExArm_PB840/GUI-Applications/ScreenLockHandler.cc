#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ScreenLock - Handles control of Screen Lock
// (initiated by the Screen Lock off-screen key).
//---------------------------------------------------------------------
//@ Interface-Description
// This is a class dedicated to the control of the Screen Lock.
// Screen Lock is set by pressing down the Screen Lock key once.  To unlock
// simply press down the Screen Lock key once more.
// The keyPanelPressHappened() method performs the necessary functions to
// enable/disable the screen touch, knob and off-screen keys. 
// The unlockScreen() method turns off the LED, enables the screen touch, knob
// and off-screen keys then finally updates user prompts.	
//---------------------------------------------------------------------
//@ Rationale
// This class is to handle the Screen Lock key function.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply register for Screen Lock Key event callbacks.
// When key events happen, this object posts prompts to users, turn on/off the
// LED and disable/enable the GUI I/O devices (the Touch screen, Knob and keys).
//---------------------------------------------------------------------
//@ Fault-Handling
// None
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScreenLockHandler.ccv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: mnr    Date:  24-Feb-2010    SCR Number: 6555
//  Project: NEO
//  Description:
// 	Screen Lock panel related updates.
//
//  Revision: 004   By: quf    Date:  15-Oct-2001    DCS Number: 5493
//  Project: GUIComms
//  Description:
// 	Implemented single screen compatibility.
//
//  Revision: 003   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd		Date:  22-JUNE-95	DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//             Integration baseline.
//=====================================================================

#include "ScreenLockHandler.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "KeyPanel.hh"
#include "PromptArea.hh"
#include "Touch.hh"
#include "MiscStrs.hh"
#include "MessageArea.hh"
#include "GuiTimerId.hh"
#include "GuiApp.hh"
#include "LowerScreen.hh"
#include "LowerSubScreenArea.hh"
#include "ServiceLowerScreen.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ScreenLockHandler()  [Default Constructor]
//
//@ Interface-Description
// Creates the Screen Lock Handler object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Register for Screen Lock Key event callbacks. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScreenLockHandler::ScreenLockHandler(void) :
	isScreenLocked_(FALSE)
{
	CALL_TRACE("ScreenLock::ScreenLock(void)");

	// Register for Screen Lock key presses
	KeyPanel::SetCallback(KeyPanel::SCREEN_LOCK_KEY, this);
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ScreenLockHandler  [Destructor]
//
//@ Interface-Description
// Destroys the Screen Lock Handler object.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Empty 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScreenLockHandler::~ScreenLockHandler(void)
{
	CALL_TRACE("ScreenLockHandler::~ScreenLockHandler(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: unlock_		[Private]
//                      
//@ Interface-Description       
// Called when the locked screen needs to be unlocked.
//---------------------------------------------------------------------
//@ Implementation-Description                  
// We need to enable the screen by turning off the LED, enabling the touch, 
// enable all keys, update prompts in the prompt area then set the
// isScreenLocked_ flag to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//  none                
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void ScreenLockHandler::unlock_(void)
{
	// Turn key LED off
	KeyPanel::SetLight(KeyPanel::SCREEN_LOCK_KEY, FALSE);

	// Enable Touch screen
	Touch::Enable();

	// Enable all off-screen keys
	KeyPanel::EnableAllKeys();

	// Update user prompts
	clearPrompts_();

	// Trigger AdjustPanel to restore focus
	// We need these two calls to make sure the
	// prompt being restored properly.
	AdjustPanel::TakePersistentFocus(this);
    AdjustPanel::TakePersistentFocus(NULL);


	isScreenLocked_ = FALSE;
}													// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: unlockScreen
//
//@ Interface-Description
// Called when the locked screen needs to be unlocked.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply call private method unlock_ to do the job.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void ScreenLockHandler::unlockScreen(void)
{
	if (isScreenLocked_)
	{												// $[TI1]
		unlock_();
	}												// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelPressHappened
//
//@ Interface-Description
// Called when the Screen Lock key is pressed.	
//---------------------------------------------------------------------
//@ Implementation-Description
// If ScreenLock is activated, turn on the LED, disable the GUI I/O devices
// (touch, knob, Clear key), post prompt messages, disable all other 
// off-screen keys.  If screenlock is deactivated, call the function 
// unlockScreen() to turn off the LED, enable touch and keys, update
// prompts in the prompt area.  In both cases, set/unset the isScreenLocked_
// flag.
// $[01219] If the screen lock function is ON, the ventilator shall not ...
// $[01232] When the user presses the screen lock key and the screen ...
// $[01234] When the screen lock function is ON, the prompt area shall ...
// $[01235] When the user presses the screen lock key and the screen ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenLockHandler::keyPanelPressHappened(KeyPanel::KeyId)
{
	CALL_TRACE("ScreenLockHandler::keyPanelPressHappened(KeyPanel::KeyId)");

	// Display the offscreen key help message 
	GuiApp::PMessageArea->setMessage(
							MiscStrs::SCREEN_LOCK_HELP_MESSAGE,
									MessageArea::MA_HIGH);

	if (!isScreenLocked_)		// ScreenLock is activated
	{												// $[TI1]
		// Turn LED on 
		KeyPanel::SetLight(KeyPanel::SCREEN_LOCK_KEY, TRUE);

		// Make sure that if the user is currently touching the screen that
		// the touch is completed by faking a release event.
		Touch::ReleaseEvent(0,0);

		// Disable Touch screen
		Touch::Disable();

        // Disable Knob, Accept and Clear keys by grabbing and releasing
		// Adjust Panel focus, thus making sure no object has Adjust Panel
		// focus.
		AdjustPanel::TakePersistentFocus(this);
        AdjustPanel::TakePersistentFocus(NULL);

		if (GuiApp::GetGuiState() == STATE_ONLINE &&
			!GuiApp::IsSettingsLockedOut())
		{
			// discard main setting changes if there were any
			LowerScreen::RLowerScreen.getMainSettingsArea()->batchSettingUpdate(
				Notification::ACCEPTED, NULL,  (SettingId::SettingIdType) 0);

			// discard subscreen changes if there were any
			(LowerScreen::RLowerScreen.getLowerSubScreenArea())->refreshActiveSubScreen();
		}
		else if (GuiApp::GetGuiState() == STATE_SERVICE)
		{
			// discard Service mode subscreen changes if there were any
			(ServiceLowerScreen::RServiceLowerScreen.getLowerSubScreenArea())->refreshActiveSubScreen();
		}

		// Update user prompts 
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
						PromptArea::PA_HIGH, PromptStrs::SCREENLOCK_CONTROL_LOCKED_P);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_HIGH, PromptStrs::SCREENLOCK_UNLOCK_S);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_HIGH, PromptStrs::EMPTY_A);

		// Disable all off-screen keys except for ScreenLock key
		// $[01351] For off-screen keys, the Contact sound is generated ...
		KeyPanel::DisableAllKeys();
		KeyPanel::EnableKey(KeyPanel::SCREEN_LOCK_KEY);
		isScreenLocked_ = TRUE;

	}
	else
	{												// $[TI2]	
		unlockScreen();
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelReleaseHappened
//
//@ Interface-Description
// Called when the Screen Lock is released.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply turn off the offscreen help message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenLockHandler::keyPanelReleaseHappened(KeyPanel::KeyId)
{
	CALL_TRACE("ScreenLockHandler::keyPanelReleaseHappened(KeyPanel::KeyId)");

	//								 $[TI1]
	// Clear the off screen key message
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearPrompts_           [Private]
//
//@ Interface-Description
// Clears any prompts displayed by this handler.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just clear all 3 high priority prompts in Prompt Area.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
ScreenLockHandler::clearPrompts_(void)
{
    CALL_TRACE("ScreenLock::clearPrompts_(void)");

	// Clear the pause-related prompts
    GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
                                    PromptArea::PA_HIGH, NULL_STRING_ID);
    GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
                                    PromptArea::PA_HIGH, NULL_STRING_ID);
    GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
                                    PromptArea::PA_HIGH, NULL_STRING_ID);
													// $[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ScreenLockHandler::SoftFault(const SoftFaultID  softFaultID,
								    const Uint32       lineNumber,
								    const char*        pFileName,
								    const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, 
							SCREENLOCKHANDLER,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
