#ifndef BdQueuesMsg_HH
#define BdQueuesMsg_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Union: BdQueuesMsg - Breath delivery inter task message format
//---------------------------------------------------------------------
// Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/BdQueuesMsg.hhv   10.7   08/17/07 09:33:52   pvcs  
//
// Modification-Log
//
//  Revision: 010   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 009   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Add new RM maneuver data messages.
//
//  Revision: 008   By: yakovb   Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Added messages for VC+/VS Startup messages.
//
//  Revision: 007   By: syw   Date:  24-Jul-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		* added SEND_PAV_DATA.
//
//  Revision: 006   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added BD_GRAPHICS.
//
//  Revision: 005  By:  yyy    Date:  27-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling for Inspiratory Pause.
//			   Added SEND_BL_LOW_TO_HIGH_DATA and SEND_BL_HIGH_TO_LOW_DATA
//
//  Revision: 004  By:  iv    Date:  03-Apr-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review for BD Scheulers.
//
//  Revision: 003  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 002  By:  iv    Date:  03-Feb-1997    DR Number: DCS 1297
//       Project:  Sigma (R8027)
//       Description:
//             Added a BdMsgEventType BD_NOVRAM_UPDATE_EVENT.
//
//  Revision: 001  By:  iv    Date:  28-Jun-96    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "TaskControlQueueMsg.hh"
#include "InterTaskMessage.hh"
#include "EventData.hh"
#include "BdAlarmId.hh"

union BdQueuesMsg
{
    // Type: BdMsgEventType
    // This enum identifies the event type. The constant FIRST_APPLICATION_MSG
    // is used accross subsystems to offset application messages so they are
    // different form task control messages and task monitor messages.
    enum BdMsgEventType
    {
        TASK_CONTROL_EVENT = TaskControlQueueMsg::TASK_CONTROL_MSG,

        BD_ALARM_INIT_EVENT = FIRST_APPLICATION_MSG,
        BD_EVENT_STATUS_EVENT,
        BD_ALARM_EVENT,
        BD_MAIN_CYCLE_EVENT,
        BD_SECONDARY_CYCLE_EVENT,
		BD_FIO2_CYCLE_EVENT,
		BD_NOVRAM_UPDATE_EVENT,
        BD_GRAPHICS
    };

    enum BreathDataWaveformMsg
    {
        LOW_BREATH_DATA_MSG_ID = 0x241095,

        SEND_END_INSP_DATA = LOW_BREATH_DATA_MSG_ID,
        SEND_END_EXH_DATA,
        SEND_BEGIN_EXP_PAUSE_DATA,
        SEND_BEGIN_INSP_PAUSE_DATA,
        SEND_END_INSP_PAUSE_DATA,
        SEND_END_EXP_PAUSE_DATA,
        SEND_PEEP_RECV_DATA,
        SEND_WAVEFORM_MSG,
        SEND_BL_MAND_EXH_DATA,
		SEND_BL_MAND_DATA,
		SEND_VTPCV_STATE,
        SEND_PAV_BREATH_DATA,
        SEND_PAV_PLATEAU_DATA,
        SEND_PAV_STARTUP_DATA,
        SEND_END_P100_PAUSE_DATA,
        SEND_END_NIF_PAUSE_DATA,
        SEND_VITAL_CAPACITY_DATA,

        HIGH_BREATH_DATA_MSG_ID = SEND_VITAL_CAPACITY_DATA
    };
    
    // Generic event structure
    struct
    {
        BdMsgEventType eventType:   8;
        Int32          eventData:  24;
    } event;

    // Ventilator and User Event Status Structure 
    struct
    {
        BdMsgEventType eventType:      8;
        EventData::EventId id:         8;
        EventData::EventStatus status: 8;
        EventData::EventPrompt prompt: 8;
    } eventStatusEvent;

    // BD Alarm Event Structure 
    struct
    {
        BdMsgEventType eventType:    8;
        BdAlarmId::BdAlarmIdType id: 24;
    } bdAlarmEvent;

    // Use the following member to simply pass the 32 bits of
    // BdQueuesMsg data to message queues.
    Uint32 qWord;
};

#endif // BdQueuesMsg_HH





