
#ifndef BoundedInterval_HH
#define BoundedInterval_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Struct:  BoundedInterval - Interval used within a Bounded Range.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BoundedInterval.hhv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  21-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Project-related changes:
//	*  created to support dynamic definition of bounded ranges, so
//         that the new, circuit-based alarm limits can also be changed
//         to the 'OFF' value
//
//====================================================================

#include "Sigma.hh"
#include "Precision.hh"

//@ Usage-Classes
//@ End-Usage


struct BoundedInterval
{
  //@ Data-Member:  value
  // This defines the upper value for this interval; lower value is found
  // in next interval, if any.
  Real32  value;

  //@ Data-Member:  resolution
  // This defines the resolution for this interval.
  Real32  resolution;

  //@ Data-Member:  precision
  // This defines the precision for this interval.
  Precision  precision;

  //@ Data-Member:  pNext
  // This is a pointer to the next interval, if any.
  const BoundedInterval*  pNext;
};


#endif  // BoundedInterval_HH
