#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmOffKeyPanel - Alarm offkey panel class which allows
// the viewing of alarm silence progress status.
//---------------------------------------------------------------------
//@ Interface-Description
// This is a class dedicated to the displaying of the Alarm Silence offscreen
// key status.
//
// The activatePanel() method display the alarm silence specific status
// information.  The deactivatePanel() method does the house cleaning
// tasks.
//---------------------------------------------------------------------
//@ Rationale
// Something needs to handle the alarm silence and 100 % O2 keys.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply display the alarm silence specific information and clearn up
// the display after alarm silence is completed/cancelled.
// particular events.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmOffKeyPanel.ccv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: hhd	Date: 09-May-2000   DCS Number:  5657, 5629
//  Project:  NeoMode
//  Description:
//  	As part of these DCSs, don't hide the progress bar when settings are locked out.
// 
//  Revision: 002  By: sah	Date: 10-Apr-2000   DCS Number:  5683, 5673
//  Project:  NeoMode
//  Description:
//      As part of this DCS, removed unneeded parameters from 'activatePanel()',
//      to go along with the removal of the corresponding unneeded strings.
//		Mapped requirement BLO1023 to code.
//
//  Revision: 001  By:  yyy    Date:  12-Jul-99    DR Number: 5327
//       Project:  NeoMode (R8027)
//       Description:
//             Integration baseline.
//		(hhd) 29-Nov-99	Modified to fix bugs.
//=====================================================================

#include "GuiApp.hh"
#include "AlarmOffKeyPanel.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "OffKeysSubScreen.hh"
#include "KeyHandlers.hh"
#include "AlarmSilenceResetHandler.hh"
#include "HundredPercentO2Handler.hh"
//@ End-Usage

//=====================================================================
//
//		Static Data...
//
//=====================================================================

// Initialize static constants.
static const Int32 PANEL_COMMAND_Y_ = 40;
static const Int32 BAR_HEIGHT_ = 15;
static const Int32 BAR_WIDTH_ = 122;
static const Int32 TIMING_STATUS_MSG_Y_ = 20;
static Boolean GuiIsReset = FALSE;

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmOffKeyPanel()  [Constructor]
//
//@ Interface-Description
// Constructs a AlarmOffKeyPanel.  Passed the following parameters:
// >Von
//	xOrg			x coordinaate for the panel location.
//  yOrg			y coordinaate for the panel location.
//  width			width for the panel.
//  height			height for the panel.
//  numButtons		number of buttons supported by the panel.
//  panelTitle		title for the panel to be displayed.
//	pFocusSubScreen	the subscreen which gains the Adjust Panel focus after
//					this button is pressed up by the operator.
//	eventId			event Id associated for this panel.
//	pEventCallBack  the callback method to handle cancel command.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize all data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmOffKeyPanel::AlarmOffKeyPanel(Uint32 xOrg, Uint32 yOrg, Uint32 width, Uint32 height,
						 Uint16 numButtons, StringId  panelTitle, SubScreen *pFocusSubScreen,
						 EventData::EventId eventId,
						 UserEventCallbackPtr pEventCallBack) :
		OffKeyPanel(xOrg, yOrg, width, height, numButtons, panelTitle, pFocusSubScreen,
					eventId, pEventCallBack),
		timeProgressBox_(0, PANEL_COMMAND_Y_ + 1, BAR_WIDTH_, BAR_HEIGHT_ - 2),
		timeBox_(0, PANEL_COMMAND_Y_, BAR_WIDTH_, BAR_HEIGHT_),
		timeStatusMsg_(MiscStrs::HUNDRED_PERCENT_O2_TIME_UNKNOWN)

{
	CALL_TRACE("AlarmOffKeyPanel::AlarmOffKeyPanel(void)");

	addDrawable(&timeBox_);
	timeBox_.setColor(Colors::LIGHT_BLUE);
	timeBox_.setFillColor(Colors::LIGHT_BLUE);
	timeBox_.positionInContainer(GRAVITY_CENTER);
	timeBox_.setShow(FALSE);

	addDrawable(&timeProgressBox_);
	timeProgressBox_.setColor(Colors::EXTRA_DARK_BLUE);
	timeProgressBox_.setFillColor(Colors::EXTRA_DARK_BLUE);
	timeProgressBox_.setX(timeBox_.getX() + 1);
	timeProgressBox_.setShow(FALSE);

	addDrawable(&timeStatusMsg_);
	timeStatusMsg_.setY(TIMING_STATUS_MSG_Y_);
	timeStatusMsg_.setShow(FALSE);
	timeStatusMsg_.setColor(Colors::WHITE);
				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmOffKeyPanel()  [Destructor]
//
//@ Interface-Description
// AlarmOffKeyPanel destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmOffKeyPanel::~AlarmOffKeyPanel(void)
{
	CALL_TRACE("AlarmOffKeyPanel::~AlarmOffKeyPanel(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activatePanel
//
//@ Interface-Description
// Called whenever the panel needs display update.  Passed the following parameters:
// >Von
//	msg					status message to be displayed.
//  displayCancelButton	flag indicates whether to display the cancel button.
//  elapseTime			time left to display the timing bar.
//  redraw				flags whether to display components of this panel.
// >Voff

//---------------------------------------------------------------------
//@ Implementation-Description
// Hides the approrpiate drawings only when the SettingsLockedOut.  
// Otherwise display the timing bar and message for this panel.
// $[BL01023] A cancel button shall be displayed for a pending maneuver...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmOffKeyPanel::activatePanel(StringId msg, Boolean displayCancelButton, Int32 elapseTime, 
								Boolean redraw)
{
	CALL_TRACE("AlarmOffKeyPanel::activatePanel(StringId msg,"
							   "Boolean displayCancelButton, Int32 elapseTime,"
							   "Boolean redraw)");
		
	GuiIsReset = (elapseTime == HundredPercentO2Handler::ELAPSE_TIME_UNKNOWN &&
							eventId_ == EventData::PERCENT_O2);

	if (GuiIsReset)
	{		// $[TI1.2.1]
		timeBox_.setShow(FALSE);
		timeProgressBox_.setShow(FALSE);
		timeStatusMsg_.setShow(redraw);
	}
	else
	{		// $[TI1.2.2]
		if (elapseTime == 0)
		{												// $[TI1.2.2.1]
			elapseTime = 1;
		}												// $[TI1.2.2.2]

		timeProgressBox_.setWidth(elapseTime);

		timeBox_.setShow(redraw);
		timeProgressBox_.setShow(redraw);
		timeStatusMsg_.setShow(FALSE);

	}

	OffKeyPanel::activatePanel(msg, FALSE, displayCancelButton, redraw);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivatePanel
//
//@ Interface-Description
// Called when alarm silence condition is no longer existance.
//---------------------------------------------------------------------
//@ Implementation-Description
// Hides all the drawings only for this panel.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmOffKeyPanel::deactivatePanel(void)
{
	CALL_TRACE("AlarmOffKeyPanel::deactivatePanel(void)");

	timeBox_.setShow(FALSE);
	timeProgressBox_.setShow(FALSE);
	timeStatusMsg_.setShow(FALSE);
	OffKeyPanel::deactivatePanel();
				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: showPanel
//
//@ Interface-Description
// Called by OffKeys subScreen to display the AlarmOffKeyPanel info.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the Show flags of panel cancel button, message and title to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//
//=====================================================================

void
AlarmOffKeyPanel::showPanel(Boolean redraw)
{
	// Time progress status text is shown if gui is reset.
	timeStatusMsg_.setShow(redraw && GuiIsReset);
	timeBox_.setShow(redraw && !GuiIsReset);
	timeProgressBox_.setShow(redraw && !GuiIsReset);

	OffKeyPanel::showPanel(redraw, TRUE);
							// $[TI1]
}

		


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmOffKeyPanel::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMOFFKEYPANEL,
  				lineNumber, pFileName, pPredicate);
}

