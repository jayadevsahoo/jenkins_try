#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CommSetupSubScreen - Activated by selecting CommSetup
// in the Service Lower Other Screen after user press/release the
// Lower Other screen tab button.
//---------------------------------------------------------------------
//@ Interface-Description
// CommSetupSubScreen contains Communication Setup settings: Device,
// Baud Rate, Data Bit, Parity Mode.
//
// The subscreen contains a setting button for each of the above settings.  
// The Accept key must be pressed in order to accept and apply the
// settings changes.  Note that if com ports 2 and 3 are present the
// device setting defaults to DCI and cannot be changed.
//
// The activateHappened()/deactivateHappened() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// acceptHappened() method is called by the BatchSettingsSubScreen when
// the settings are being accepted.  Timer events (specifically the
// subscreen setting change timeout event) are passed into
// timerEventHappened() and Adjust Panel events are communicated via
// the various Adjust Panel "happened" methods.  Settings events result
// in a call to valueUpdateHappened().
//
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the Communication Setup subscreen in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The implementation is quite simple.  Three setting buttons are displayed.
// This subscreen registers as a target of each
// of the three settings.  If any of the settings are modified the subscreen is
// informed via valueUpdateHappened().
//
// If the Accept key is pressed then the BatchSettingsSubScreen will
// call acceptHappened() to accept the setting changes and deactivate
// the subscreen.
//
// The subscreen is also registered as a target of the subscreen inactivity
// timer.  If the timer runs out this is detected in timerEventHappened() and
// the subscreen is reset.
//
// $[01057]
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/CommSetupSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 017   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 016    By: mnr   Date: 03-Mar-2010  SCR Number: 6556
//  Project: NEO
//  Description:
//  Added advisory prompt for Philips IntelliBridge COM setting.
// 
//  Revision: 015    By: mnr   Date: 23-Feb-2010  SCR Number: 6556
//  Project: NEO
//  Description:
//  Added support for Philips to serial port
// 
//  Revision: 014    By: erm   Date: 5-Nov-2008  DCS Number: 6441
//  Project: 840S
//  Description:
//  Added support for RT waveform to serial port
// 
//  Revision: 013   By: rhj    Date: 23-Jan-2008   SCR Number: 6441
//  Project:  XMIT
//  Description:
//	Added support for transmitting data through the serial port.
// 
//  Revision: 012   By: erm    Date: 02-Jun-2003   DR Number: 6058
//  Project:  AVER
//  Description:
//	Added support for spacelab monitors
//
//  Revision: 011   By: hlg    Date: 01-Oct-2001    DR Number: 5966
//  Project:  GUIComms
//  Description:
//	Added mapping to SRS print requirements.
//
//  Revision: 010   By: quf    Date: 17-Sep-2001    DR Number: 5943
//  Project:  GUIComms
//  Description:
//	Removed the comm device "None" setting.
//
//  Revision: 009   By: quf    Date:  13-Sep-2001    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Print implementation changes:
//	- Removed "print configure" button, re-centered other buttons.
//	- Made only com1 selectable for printer, with com2 and com3 select
//	  buttons flattened and forced to DCI.
//
//  Revision: 008   By: gdc    Date:  20-Sep-2000    DR Number: 5769
//  Project:  GuiComms
//  Description:
//		Added 38400 baud rate.
//
//  Revision: 007   By: sah   Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//
//  Revision: 006   By: hct    Date:  03-DEC-1999    DR Number: 5493
//  Project:  GUIComms
//  Description:
//		Incorporated initial specifications for GUIComms Project.
//
//  Revision: 005   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//      Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By: yyy      Date: 08-Sep-1997  DR Number: 2392
//    Project:  Sigma (R8027)
//    Description:
//      Replaced the deactivate() call with activate() when setting are
//		accepted.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "CommSetupSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "AdjustPanel.hh"
#include "BaudRateValue.hh"
#include "DciDataBitsValue.hh"
#include "DciParityModeValue.hh"
#include "SettingContextHandle.hh"
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "OsUtil.hh"
#include "PromptArea.hh"
#include "Sound.hh"
#include "SubScreenArea.hh"
#include "ComPortConfigValue.hh"
#include "GuiApp.hh"
#include "CpuDevice.hh"
#include "NmiSource.hh"
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int16 BUTTON_HEIGHT_ = 46;
static const Int16 BUTTON_WIDTH_ = 120;
static const Int16 BUTTON_BORDER_ = 3;
static const Int16 BUTTON_GAP_X_ = 10;
static const Int16 BUTTON_GAP_Y_ = 0;

// The GUI Cost Reduction board has three serial ports and therefore three
// columns of configuration buttons and one column for "Print Device"
static const Int16 BUTTON_X_3_COLUMNS_ = 
        ( ::SUB_SCREEN_AREA_WIDTH - 3*BUTTON_WIDTH_ - 2*BUTTON_GAP_X_) / 2;
// The original GUI board has one serial port and therefore one
// column of configuration buttons.
static const Int16 BUTTON_X_1_COLUMN_ = 
        ( ::SUB_SCREEN_AREA_WIDTH - BUTTON_WIDTH_) / 2;
static const Int16 BUTTON_Y_ = 
        ( (::LOWER_SUB_SCREEN_AREA_HEIGHT+32) - 4*BUTTON_HEIGHT_ - 3*BUTTON_GAP_Y_) / 2;

static Uint  ComConfigInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
				  (sizeof(StringId) * (ComPortConfigValue::TOTAL_CONFIG_VALUE - 1)))];
static Uint  DciBaudRateInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
				  (sizeof(StringId) * (BaudRateValue::TOTAL_BAUD_RATES - 1)))];
static Uint  DciDataBitsInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
	     (sizeof(StringId) * (DciDataBitsValue::TOTAL_DATA_BITS_VALUES - 1)))];
static Uint  DciParityInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		   (sizeof(StringId) * (DciParityModeValue::TOTAL_PARITY_MODES - 1)))];

static DiscreteSettingButton::ValueInfo*  PCom1ConfigInfo_ =
				(DiscreteSettingButton::ValueInfo*)::ComConfigInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PDciBaudRateInfo_ =
				(DiscreteSettingButton::ValueInfo*)::DciBaudRateInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PDciDataBitsInfo_ =
				(DiscreteSettingButton::ValueInfo*)::DciDataBitsInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PDciParityInfo_ =
				(DiscreteSettingButton::ValueInfo*)::DciParityInfoMemory_;

static DiscreteSettingButton::ValueInfo*  PCom2ConfigInfo_ =
				(DiscreteSettingButton::ValueInfo*)::ComConfigInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PCom2BaudRateInfo_ =
				(DiscreteSettingButton::ValueInfo*)::DciBaudRateInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PCom2DataBitsInfo_ =
				(DiscreteSettingButton::ValueInfo*)::DciDataBitsInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PCom2ParityInfo_ =
				(DiscreteSettingButton::ValueInfo*)::DciParityInfoMemory_;

static DiscreteSettingButton::ValueInfo*  PCom3ConfigInfo_ =
				(DiscreteSettingButton::ValueInfo*)::ComConfigInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PCom3BaudRateInfo_ =
				(DiscreteSettingButton::ValueInfo*)::DciBaudRateInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PCom3DataBitsInfo_ =
				(DiscreteSettingButton::ValueInfo*)::DciDataBitsInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PCom3ParityInfo_ =
				(DiscreteSettingButton::ValueInfo*)::DciParityInfoMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CommSetupSubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the Communication Setup subscreen.  Passed a pointer to the subscreen
// area which creates it (LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the subscreen container.
// Creates the various buttons displayed in the subscreen and adds them
// to the container.  Registers for callbacks from each button and each
// setting.  If this is a GUI Cost Reduction board use BUTTON_X_3_COLUMNS to
// position the four columns of configuration buttons on the subscreen.
// If this is an original GUI board use BUTTON_X_1_COLUMN to position the
// single column on the subscreen.
//
// $[GC01001] The Communication Setup subscreen shall allow the user ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CommSetupSubScreen::CommSetupSubScreen(SubScreenArea *pSubScreenArea) :
	BatchSettingsSubScreen(pSubScreenArea),
    com1ConfigureButton_(IsGuiCommsConfig() ?
                        BUTTON_X_3_COLUMNS_ : BUTTON_X_1_COLUMN_,
            		BUTTON_Y_ + 0*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
            BUTTON_WIDTH_,
           	BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
            Button::NO_BORDER, MiscStrs::COM1_CONFIG_BUTTON_TITLE,
            SettingId::COM1_CONFIG, this,
            MiscStrs::COM_CONFIG_HELP_MESSAGE, 16 ), 
    dciBaudRateButton_(IsGuiCommsConfig() ?
                        BUTTON_X_3_COLUMNS_ : BUTTON_X_1_COLUMN_,
            		BUTTON_Y_ + 1*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
			BUTTON_WIDTH_, BUTTON_HEIGHT_, 
			Button::LIGHT, BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, 
			MiscStrs::DCI_BAUD_RATE_BUTTON_TITLE,
			SettingId::DCI_BAUD_RATE, this,
			MiscStrs::DCI_BAUD_RATE_HELP_MESSAGE, 18), 
    dciDataBitButton_(IsGuiCommsConfig() ?
                        BUTTON_X_3_COLUMNS_ : BUTTON_X_1_COLUMN_,
	                BUTTON_Y_ + 2*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
			BUTTON_WIDTH_, BUTTON_HEIGHT_,
			Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
			Button::NO_BORDER, MiscStrs::DCI_DATA_BITS_BUTTON_TITLE,
			SettingId::DCI_DATA_BITS, this,
			MiscStrs::DCI_DATA_BITS_HELP_MESSAGE, 18),
    dciParityModeButton_(IsGuiCommsConfig() ?
                        BUTTON_X_3_COLUMNS_ : BUTTON_X_1_COLUMN_,
	                BUTTON_Y_ + 3*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
			BUTTON_WIDTH_,
			BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
			Button::NO_BORDER, MiscStrs::DCI_PARITY_MODE_BUTTON_TITLE,
			SettingId::DCI_PARITY_MODE, this,
			MiscStrs::DCI_PARITY_MODE_HELP_MESSAGE, 18),
    com2ConfigureButton_(BUTTON_X_3_COLUMNS_ + 1*(BUTTON_WIDTH_+BUTTON_GAP_X_),
            BUTTON_Y_ + 0*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
            BUTTON_WIDTH_,
            BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
            Button::NO_BORDER, MiscStrs::COM2_CONFIG_BUTTON_TITLE,
            SettingId::COM2_CONFIG, this,
            MiscStrs::COM_CONFIG_HELP_MESSAGE, 16),
    com2BaudRateButton_(BUTTON_X_3_COLUMNS_ + 1*(BUTTON_WIDTH_+BUTTON_GAP_X_),
            BUTTON_Y_ + 1*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
            BUTTON_WIDTH_, BUTTON_HEIGHT_, 
            Button::LIGHT, BUTTON_BORDER_,
            Button::ROUND, Button::NO_BORDER, 
           	MiscStrs::DCI_BAUD_RATE_BUTTON_TITLE,
            SettingId::COM2_BAUD_RATE, this,
           	MiscStrs::DCI_BAUD_RATE_HELP_MESSAGE, 18),
    com2DataBitButton_(BUTTON_X_3_COLUMNS_ + 1*(BUTTON_WIDTH_+BUTTON_GAP_X_),
            BUTTON_Y_ + 2*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
            BUTTON_WIDTH_, BUTTON_HEIGHT_,
            Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
            Button::NO_BORDER, MiscStrs::DCI_DATA_BITS_BUTTON_TITLE,
            SettingId::COM2_DATA_BITS, this,
            MiscStrs::DCI_DATA_BITS_HELP_MESSAGE, 18 ),
    com2ParityModeButton_(BUTTON_X_3_COLUMNS_ + 1*(BUTTON_WIDTH_+BUTTON_GAP_X_),
            BUTTON_Y_ + 3*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
            BUTTON_WIDTH_,
            BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
            Button::NO_BORDER, MiscStrs::DCI_PARITY_MODE_BUTTON_TITLE,
            SettingId::COM2_PARITY_MODE, this,
            MiscStrs::DCI_PARITY_MODE_HELP_MESSAGE, 18),
    com3ConfigureButton_(BUTTON_X_3_COLUMNS_ + 2*(BUTTON_WIDTH_+BUTTON_GAP_X_),
            BUTTON_Y_ + 0*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
            BUTTON_WIDTH_,
            BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
            Button::NO_BORDER, MiscStrs::COM3_CONFIG_BUTTON_TITLE,
            SettingId::COM3_CONFIG, this,
            MiscStrs::COM_CONFIG_HELP_MESSAGE, 16),
    com3BaudRateButton_(BUTTON_X_3_COLUMNS_ + 2*(BUTTON_WIDTH_+BUTTON_GAP_X_),
            BUTTON_Y_ + 1*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
            BUTTON_WIDTH_, BUTTON_HEIGHT_, 
            Button::LIGHT, BUTTON_BORDER_,
            Button::ROUND, Button::NO_BORDER, 
            MiscStrs::DCI_BAUD_RATE_BUTTON_TITLE,
            SettingId::COM3_BAUD_RATE, this,
            MiscStrs::DCI_BAUD_RATE_HELP_MESSAGE, 18),
    com3DataBitButton_(BUTTON_X_3_COLUMNS_ + 2*(BUTTON_WIDTH_+BUTTON_GAP_X_),
            BUTTON_Y_ + 2*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
            BUTTON_WIDTH_, BUTTON_HEIGHT_,
            Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
            Button::NO_BORDER, MiscStrs::DCI_DATA_BITS_BUTTON_TITLE,
            SettingId::COM3_DATA_BITS, this,
            MiscStrs::DCI_DATA_BITS_HELP_MESSAGE, 18),
    com3ParityModeButton_(BUTTON_X_3_COLUMNS_ + 2*(BUTTON_WIDTH_+BUTTON_GAP_X_),
            BUTTON_Y_ + 3*(BUTTON_HEIGHT_+BUTTON_GAP_Y_),
            BUTTON_WIDTH_,
            BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
            Button::NO_BORDER, MiscStrs::DCI_PARITY_MODE_BUTTON_TITLE,
            SettingId::COM3_PARITY_MODE, this,
            MiscStrs::DCI_PARITY_MODE_HELP_MESSAGE, 18),
	titleArea_(NULL_STRING_ID, SubScreenTitleArea::SSTA_CENTER)
{
	CALL_TRACE("CommSetupSubScreen::CommSetupSubScreen(SubScreenArea "
													"*pSubScreenArea)");

	// Size and position the subscreen
	setX(0);
	setY(0);

	// Add the title area
	addDrawable(&titleArea_);

	// set up values for Com1 Configure ....
	::PCom1ConfigInfo_->numValues = ComPortConfigValue::TOTAL_CONFIG_VALUE;
	::PCom1ConfigInfo_->arrValues[ComPortConfigValue::DCI_VALUE] =
												MiscStrs::DCI_VALUE_TITLE;
	::PCom1ConfigInfo_->arrValues[ComPortConfigValue::PRINTER_VALUE] =
												MiscStrs::PRINTER_VALUE_TITLE;
	
	::PCom1ConfigInfo_->arrValues[ComPortConfigValue::SPACELAB_VALUE] =
												MiscStrs::SPACELAB_VALUE_TITLE;
	::PCom1ConfigInfo_->arrValues[ComPortConfigValue::SENSOR_VALUE] =
												MiscStrs::SENSOR_VALUE_TITLE;

	::PCom1ConfigInfo_->arrValues[ComPortConfigValue::PHILIPS_VALUE] =
												MiscStrs::PHILIPS_VALUE_TITLE;

	::PCom1ConfigInfo_->arrValues[ComPortConfigValue::VSET_VALUE] =
												MiscStrs::VSET_VALUE_TITLE;

	com1ConfigureButton_.setValueInfo(::PCom1ConfigInfo_);

	// set up values for DCI baud rate....
	::PDciBaudRateInfo_->numValues = BaudRateValue::TOTAL_BAUD_RATES;
	::PDciBaudRateInfo_->arrValues[BaudRateValue::BAUD_1200_VALUE] =
												MiscStrs::BAUD_1200_VALUE_LABEL;
	::PDciBaudRateInfo_->arrValues[BaudRateValue::BAUD_2400_VALUE] =
												MiscStrs::BAUD_2400_VALUE_LABEL;
	::PDciBaudRateInfo_->arrValues[BaudRateValue::BAUD_4800_VALUE] =
												MiscStrs::BAUD_4800_VALUE_LABEL;
	::PDciBaudRateInfo_->arrValues[BaudRateValue::BAUD_9600_VALUE] =
												MiscStrs::BAUD_9600_VALUE_LABEL;
	::PDciBaudRateInfo_->arrValues[BaudRateValue::BAUD_19200_VALUE] =
												MiscStrs::BAUD_19200_VALUE_LABEL;
	::PDciBaudRateInfo_->arrValues[BaudRateValue::BAUD_38400_VALUE] =			
												MiscStrs::BAUD_38400_VALUE_LABEL;
	dciBaudRateButton_.setValueInfo(::PDciBaudRateInfo_);


	// set up values for DCI data bits....
	::PDciDataBitsInfo_->numValues = DciDataBitsValue::TOTAL_DATA_BITS_VALUES;
	::PDciDataBitsInfo_->arrValues[DciDataBitsValue::BITS_7_VALUE] =
												MiscStrs::DCI_BITS_7_VALUE_LABEL;
	::PDciDataBitsInfo_->arrValues[DciDataBitsValue::BITS_8_VALUE] =
												MiscStrs::DCI_BITS_8_VALUE_LABEL;
	dciDataBitButton_.setValueInfo(::PDciDataBitsInfo_);


	// set up values for DCI parity....
	::PDciParityInfo_->numValues = DciParityModeValue::TOTAL_PARITY_MODES;
	::PDciParityInfo_->arrValues[DciParityModeValue::EVEN_PARITY_VALUE] =
												MiscStrs::DCI_EVEN_PARITY_VALUE;
	::PDciParityInfo_->arrValues[DciParityModeValue::ODD_PARITY_VALUE] =
												MiscStrs::DCI_ODD_PARITY_VALUE;
	::PDciParityInfo_->arrValues[DciParityModeValue::NO_PARITY_VALUE] =
												MiscStrs::DCI_NO_PARITY_VALUE;
	dciParityModeButton_.setValueInfo(::PDciParityInfo_);

	// set up values for Com2 Configure ....
	::PCom2ConfigInfo_->numValues = ComPortConfigValue::TOTAL_CONFIG_VALUE;
	::PCom2ConfigInfo_->arrValues[ComPortConfigValue::DCI_VALUE] =
												MiscStrs::DCI_VALUE_TITLE;
	::PCom2ConfigInfo_->arrValues[ComPortConfigValue::PRINTER_VALUE] =
												MiscStrs::PRINTER_VALUE_TITLE;
    ::PCom2ConfigInfo_->arrValues[ComPortConfigValue::SENSOR_VALUE] =
												MiscStrs::SENSOR_VALUE_TITLE;
	::PCom2ConfigInfo_->arrValues[ComPortConfigValue::PHILIPS_VALUE] =
												MiscStrs::PHILIPS_VALUE_TITLE;

	::PCom2ConfigInfo_->arrValues[ComPortConfigValue::VSET_VALUE] =
												MiscStrs::VSET_VALUE_TITLE;

	com2ConfigureButton_.setValueInfo(::PCom2ConfigInfo_);

	// set up values for Com2 DCI baud rate....
	::PCom2BaudRateInfo_->numValues = BaudRateValue::TOTAL_BAUD_RATES;
	::PCom2BaudRateInfo_->arrValues[BaudRateValue::BAUD_1200_VALUE] =
												MiscStrs::BAUD_1200_VALUE_LABEL;
	::PCom2BaudRateInfo_->arrValues[BaudRateValue::BAUD_2400_VALUE] =
												MiscStrs::BAUD_2400_VALUE_LABEL;
	::PCom2BaudRateInfo_->arrValues[BaudRateValue::BAUD_4800_VALUE] =
												MiscStrs::BAUD_4800_VALUE_LABEL;
	::PCom2BaudRateInfo_->arrValues[BaudRateValue::BAUD_9600_VALUE] =
												MiscStrs::BAUD_9600_VALUE_LABEL;
	::PCom2BaudRateInfo_->arrValues[BaudRateValue::BAUD_19200_VALUE] =
												MiscStrs::BAUD_19200_VALUE_LABEL;
	::PCom2BaudRateInfo_->arrValues[BaudRateValue::BAUD_38400_VALUE] =			
												MiscStrs::BAUD_38400_VALUE_LABEL;
	com2BaudRateButton_.setValueInfo(::PCom2BaudRateInfo_);


	// set up values for Com2 data bits....
	::PCom2DataBitsInfo_->numValues = DciDataBitsValue::TOTAL_DATA_BITS_VALUES;
	::PCom2DataBitsInfo_->arrValues[DciDataBitsValue::BITS_7_VALUE] =
												MiscStrs::DCI_BITS_7_VALUE_LABEL;
	::PCom2DataBitsInfo_->arrValues[DciDataBitsValue::BITS_8_VALUE] =
												MiscStrs::DCI_BITS_8_VALUE_LABEL;
	com2DataBitButton_.setValueInfo(::PCom2DataBitsInfo_);


	// set up values for Com2 parity....
	::PCom2ParityInfo_->numValues = DciParityModeValue::TOTAL_PARITY_MODES;
	::PCom2ParityInfo_->arrValues[DciParityModeValue::EVEN_PARITY_VALUE] =
												MiscStrs::DCI_EVEN_PARITY_VALUE;
	::PCom2ParityInfo_->arrValues[DciParityModeValue::ODD_PARITY_VALUE] =
												MiscStrs::DCI_ODD_PARITY_VALUE;
	::PCom2ParityInfo_->arrValues[DciParityModeValue::NO_PARITY_VALUE] =
												MiscStrs::DCI_NO_PARITY_VALUE;
	com2ParityModeButton_.setValueInfo(::PCom2ParityInfo_);

	// set up values for Com3 Configure ....
	::PCom3ConfigInfo_->numValues = ComPortConfigValue::TOTAL_CONFIG_VALUE;
	::PCom3ConfigInfo_->arrValues[ComPortConfigValue::DCI_VALUE] =
												MiscStrs::DCI_VALUE_TITLE;
	::PCom3ConfigInfo_->arrValues[ComPortConfigValue::PRINTER_VALUE] =
												MiscStrs::PRINTER_VALUE_TITLE;
    ::PCom3ConfigInfo_->arrValues[ComPortConfigValue::SENSOR_VALUE] =
												MiscStrs::SENSOR_VALUE_TITLE;
	::PCom3ConfigInfo_->arrValues[ComPortConfigValue::PHILIPS_VALUE] =
												MiscStrs::PHILIPS_VALUE_TITLE;

	::PCom3ConfigInfo_->arrValues[ComPortConfigValue::VSET_VALUE] =
												MiscStrs::VSET_VALUE_TITLE;

	com3ConfigureButton_.setValueInfo(::PCom3ConfigInfo_);

	// set up values for Com3 DCI baud rate....
	::PCom3BaudRateInfo_->numValues = BaudRateValue::TOTAL_BAUD_RATES;
	::PCom3BaudRateInfo_->arrValues[BaudRateValue::BAUD_1200_VALUE] =
												MiscStrs::BAUD_1200_VALUE_LABEL;
	::PCom3BaudRateInfo_->arrValues[BaudRateValue::BAUD_2400_VALUE] =
												MiscStrs::BAUD_2400_VALUE_LABEL;
	::PCom3BaudRateInfo_->arrValues[BaudRateValue::BAUD_4800_VALUE] =
												MiscStrs::BAUD_4800_VALUE_LABEL;
	::PCom3BaudRateInfo_->arrValues[BaudRateValue::BAUD_9600_VALUE] =
												MiscStrs::BAUD_9600_VALUE_LABEL;
	::PCom3BaudRateInfo_->arrValues[BaudRateValue::BAUD_19200_VALUE] =
												MiscStrs::BAUD_19200_VALUE_LABEL;
	::PCom3BaudRateInfo_->arrValues[BaudRateValue::BAUD_38400_VALUE] =			
												MiscStrs::BAUD_38400_VALUE_LABEL;
	com3BaudRateButton_.setValueInfo(::PCom3BaudRateInfo_);


	// set up values for Com3 data bits....
	::PCom3DataBitsInfo_->numValues = DciDataBitsValue::TOTAL_DATA_BITS_VALUES;
	::PCom3DataBitsInfo_->arrValues[DciDataBitsValue::BITS_7_VALUE] =
												MiscStrs::DCI_BITS_7_VALUE_LABEL;
	::PCom3DataBitsInfo_->arrValues[DciDataBitsValue::BITS_8_VALUE] =
												MiscStrs::DCI_BITS_8_VALUE_LABEL;
	com3DataBitButton_.setValueInfo(::PCom3DataBitsInfo_);


	// set up values for Com3 parity....
	::PCom3ParityInfo_->numValues = DciParityModeValue::TOTAL_PARITY_MODES;
	::PCom3ParityInfo_->arrValues[DciParityModeValue::EVEN_PARITY_VALUE] =
												MiscStrs::DCI_EVEN_PARITY_VALUE;
	::PCom3ParityInfo_->arrValues[DciParityModeValue::ODD_PARITY_VALUE] =
												MiscStrs::DCI_ODD_PARITY_VALUE;
	::PCom3ParityInfo_->arrValues[DciParityModeValue::NO_PARITY_VALUE] =
												MiscStrs::DCI_NO_PARITY_VALUE;
	com3ParityModeButton_.setValueInfo(::PCom3ParityInfo_);

       Uint  idx;

	idx = 0u;
	arrSettingButtonPtrs_[idx++] = &com1ConfigureButton_;
	arrSettingButtonPtrs_[idx++] = &dciDataBitButton_;
	arrSettingButtonPtrs_[idx++] = &dciBaudRateButton_;
	arrSettingButtonPtrs_[idx++] = &dciParityModeButton_;

	// If this is a GUI Comms Configuration board with three
	// serial ports, add the data configuration buttons for
	// the extra two ports.
	if ( IsGuiCommsConfig() )
	{
	// $[TI1]
	  arrSettingButtonPtrs_[idx++] = &com2ConfigureButton_;
	  arrSettingButtonPtrs_[idx++] = &com2DataBitButton_;
	  arrSettingButtonPtrs_[idx++] = &com2BaudRateButton_;
	  arrSettingButtonPtrs_[idx++] = &com2ParityModeButton_;
	  arrSettingButtonPtrs_[idx++] = &com3ConfigureButton_;
	  arrSettingButtonPtrs_[idx++] = &com3DataBitButton_;
	  arrSettingButtonPtrs_[idx++] = &com3BaudRateButton_;
	  arrSettingButtonPtrs_[idx++] = &com3ParityModeButton_;
	}
	// $[TI2]

	arrSettingButtonPtrs_[idx]   = NULL;

	AUX_CLASS_ASSERTION((idx <= MAX_SETTING_BUTTONS_), idx);

	// Add the buttons to the subscreen
	for (idx = 0u; arrSettingButtonPtrs_[idx] != NULL; idx++ )
	{
		addDrawable(arrSettingButtonPtrs_[idx]);
	        arrSettingButtonPtrs_[idx]->setButtonCallback(this);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CommSetupSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys CommSetupSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CommSetupSubScreen::~CommSetupSubScreen(void)
{
	CALL_TRACE("CommSetupSubScreen::~CommSetupSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called by the BatchSettingsSubScreen when the operator presses the 
// Accept key. We accept the settings and redisplay this screen in 
// current form.
//---------------------------------------------------------------------
//@ Implementation-Description
// Make the "Accept" sound, clear the Adjust Panel focus, accept the
// adjusted settings, and reactivate this subscreen.
//
// $[01056] When batch settings are being setup ventilator settings
//			will not be affected
// $[01118] User must press Accept key to accept batch settings.
// $[01257] The Accept key shall be the ultimate confirmation step for all ...
// $[01258] The GUI shall accept all adjusted settings.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CommSetupSubScreen::acceptHappened(void)
{
	CALL_TRACE("CommSetupSubScreen::acceptHappened(void)");

	if ( areSettingsCurrent_() )
	{															// $[TI1.1]
		Sound::Start(Sound::INVALID_ENTRY);
	}
	else
	{															// $[TI1.2]
		// Clear the Adjust Panel focus, make the Accept sound, store the
		// current settings, accept the adjusted settings and reactivate
		// this subscreen.
		// $[01360] Sigma is capable of creating the following user interface sound.
		Sound::Start(Sound::ACCEPT);

		// Clear Advisory prompt if ACCEPT happens
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, NULL_STRING_ID);

		// Make sure nothing on this screen has focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);
	
		// Accept Dci Setup settings
		SettingContextHandle::AcceptDciSetupBatch();
	
		activate();			// Reactive this subscreen
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when a timer that we started runs out, in this case the subscreen
// setting change timer.  We are passed an enum id identifying the timer.
// We must reset the subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// We reset the subscreen by deactivating and reactivating it.  We also
// clear the Adjust Panel focus.
//
// We only react to timers if this subscreen is visible.  There is a small
// chance that this timer event might arrive just as we are being deactivated,
// in which case we ignore it.
//
// $[01319] If the communication setup subscreen is displayed and the user ...
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the subscreen setting change timer.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CommSetupSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("CommSetupSubScreen::timerEventHappened("
										"GuiTimerId::GuiTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId ==
								GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT);

	if (isVisible())
	{													// $[TI1.1]
		// Reactivate this sub-screen
		deactivate();
		activate();
	}													// $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Starts the subscreen setting change timer.  Tells the Settings-Validation
// subsystem to begin adjusting settings.  Calls the activate() method
// of each setting button in order to sync their displays with the current
// setting values.  Displays some prompts to guide the operator.
//
// $[GC01001] The Communication Setup subscreen shall allow the user ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CommSetupSubScreen::activateHappened_(void)
{
	CALL_TRACE("activateHappened_()");

	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Begin adjusting settings
	SettingContextHandle::AdjustDciSetupBatch();

	// Display the "current" title
	titleArea_.setTitle(MiscStrs::CURRENT_COMM_SETUP_SUBSCREEN_TITLE);

	// Activate all setting buttons...
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::activate);

	// Set Primary prompt to "Adjust settings."
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_P);

	// Set Secondary prompt to tell user to press OTHER SCREENS button to cancel
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears any prompts which may have been displayed and clears the Adjust Panel
// focus in case we had it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CommSetupSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("deactivateHappened_(void)");

	// deactivate all setting buttons...
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::deactivate);
}			// $[TI1]

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when any button in CommSetupSubScreen is pressed down. If it 
// is a COM setting button then display the advisory prompt.
//---------------------------------------------------------------------
//@ Implementation-Description
// Check the button pressed only if it is by operator action.
//---------------------------------------------------------------------
//@ PreCondition
// Callback must have been generated by an operator action.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CommSetupSubScreen::buttonDownHappened(Button *pButton, Boolean byOperatorAction)
{
	if( !byOperatorAction )
		return;

    if ( pButton == &com1ConfigureButton_ || pButton == &com2ConfigureButton_ ||
		 pButton == &com3ConfigureButton_  )
	{													
		// Set Advisory prompt to ADJUST_COM_SETTINGS_A string
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, PromptStrs::ADJUST_COM_SETTINGS_A);
	}
	else
	{
		// Clear Advisory prompt if any other button is pressed 
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, NULL_STRING_ID);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when any button in CommSetupSubScreen is released up. If it 
// is a COM setting button then clear the advisory prompt.
//---------------------------------------------------------------------
//@ Implementation-Description
// Check the button pressed only if it is by operator action.
//---------------------------------------------------------------------
//@ PreCondition
// Callback must have been generated by an operator action.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CommSetupSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
	if( !byOperatorAction )
		return;

    if ( pButton == &com1ConfigureButton_ || pButton == &com2ConfigureButton_ ||
		 pButton == &com3ConfigureButton_  )
	{													
		// Clear Advisory prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, NULL_STRING_ID);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdateHappened_
//
//@ Interface-Description
// Called when a setting has changed.
// Passed the following parameters:
// >Von
//  transitionId The id of the type of transition happenning in this subscreen.
//	qualifierId	The context in which the change happened, either
//				ACCEPTED or ADJUSTED.
//	pSubject	The pointer to the Context subject.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// We first check if we're visible.  We ignore all setting changes when this
// subscreen is not active.  If settings have changed from being current
// to proposed then change the screen title and activate the Accept key
// else if settings have reverted to current from being proposed then
// change back the screen title and deactivate the Accept key.
//
// $[01058] Accept key only active on first setting change...
// $[01118] As soon as any settings have changed activate Accept key...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CommSetupSubScreen::valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									    )
{
	CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");

	// If the setting has been changed and the screen was in the
	// current state then go to the proposed state.
	if (transitionId == BatchSettingsSubScreen::CURRENT_TO_PROPOSED)
	{													// $[TI2.1]
		// Display proposed title
		titleArea_.setTitle(MiscStrs::PROPOSED_COMM_SETUP_SUBSCREEN_TITLE);

		// Change the Secondary prompt to explain the use of the Accept
		// button
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_PROCEED_CANCEL_S);
	}
	// If the setting has been adjusted back to its original value and now
	// all settings are unchanged then reset the screen
	else if (transitionId == BatchSettingsSubScreen::PROPOSED_TO_CURRENT)
	{													// $[TI2.2]
		// Display current title
		titleArea_.setTitle(MiscStrs::CURRENT_COMM_SETUP_SUBSCREEN_TITLE);

		// Reset the Secondary prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);
	}													// $[TI2.3]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
CommSetupSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
			COMMSETUPSUBSCREEN, lineNumber, pFileName, pPredicate);
}

