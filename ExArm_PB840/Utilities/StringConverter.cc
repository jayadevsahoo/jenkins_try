#include "stdafx.h"
//----------------------------------------------------------------------------
//            Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------
/// @file StringConverter.cc

#include "UtilityClassId.hh"
#include "StringConverter.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ToWString
//
//@ Interface-Description
// The API will take narrow-charaters (i.e.char) string as an input and will provide
// the wide-character string as output. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
// Example Usage:
//	char* str = "Hello";
//  wchar_t pdestBuff[256];
//  StringConverter::ToWString( pdestBuff,256, str );
//---------------------------------------------------------------------
//@ PreCondition
//  pSourceChrStr != NULL
// wcslen(pDestWcharStr) >= strlen(pSourceChrStr)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void StringConverter::ToWString( wchar_t* pDestWcharStr, Uint32 iMaxDestSize, 
								const char* pSourceChrStr )
{
	CLASS_PRE_CONDITION( pSourceChrStr != NULL );
	CLASS_PRE_CONDITION( wcslen(pDestWcharStr) >= strlen(pSourceChrStr) );

	size_t convertedChars = 0;

	//conversion
	mbstowcs_s( &convertedChars, pDestWcharStr, iMaxDestSize, 
								pSourceChrStr, iMaxDestSize); 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ToString
//
//@ Interface-Description
// The API will take wide-character (i.e.wchar_t) string as an input and will provide
// the narrow-character string as output.
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
// Example Usage:
//	wchar_t* str = L"Hello";
//  char pdestBuff[256];
//  StringConverter::ToString( pdestBuff, 256, str );
//---------------------------------------------------------------------
//@ PreCondition
//  pSourceWcharStr != NULL
//  strlen(pDestCharStr) >= wcslen(pSourceWcharStr)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void StringConverter::ToString( char* pDestCharStr, Uint32 iMaxDestSize,
							   const wchar_t* pSourceWcharStr )
{
	CLASS_PRE_CONDITION( pSourceWcharStr != NULL );
	CLASS_PRE_CONDITION( strlen(pDestCharStr) >= wcslen(pSourceWcharStr) );


	size_t i = 0;

    // Conversion
    wcstombs_s(&i, pDestCharStr, iMaxDestSize, pSourceWcharStr, iMaxDestSize);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// TThis method is called when a software fault is detected by the
// fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
// and 'lineNumber' are essential pieces of information.  The
// 'pFileName' and 'pPredicate' strings may be defaulted in the macro
// to reduce code space..
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void  StringConverter::SoftFault(const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const Char*       pFileName,
							   const Char*       pPredicate )
{
	FaultHandler::SoftFault(softFaultID, TOOLS,
		STRING_CONVERTER, lineNumber, pFileName, pPredicate);
}
