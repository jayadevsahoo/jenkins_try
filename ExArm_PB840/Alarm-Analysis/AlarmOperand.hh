#ifndef AlarmOperand_HH
#define AlarmOperand_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AlarmOperand - Alarm-Analysis class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmOperand.hhv   25.0.4.0   19 Nov 2013 13:51:16   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 01/18/96         DR Number:   659
//      Project:   Sigma   (R8027)
//      Description:
//         Use OsTimeStamp for interval timing instead of TimeStamp.
//  
//   Revision 003   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

//@ Usage-Classes
#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "AlarmOperandList.hh"
#include "OperandName.hh"
#include "OsTimeStamp.hh"
#include "TimeStamp.hh"
//@ End-Usage

class AlarmOperand 
{
  public:
    AlarmOperand (const  OperandName n = OPERAND_NAME_NULL);
    AlarmOperand (const AlarmOperand& rOp);
    virtual ~AlarmOperand();

    static void             Initialize (void);
    static void             InitGroups (void);
    static void             AddOperand (AlarmOperand* pOperand);

    virtual  void setReset  (void) = 0;
    virtual  void setUpdate (void) = 0;
    Boolean  evaluate       (void);


    inline  OperandName             getName     (void) const;
    inline Boolean                  getUpdate   (void) const;
    inline Boolean                  getReset    (void) const;
    inline const                    TimeStamp&  getWhenOccurred(void) const;
    inline const                    OsTimeStamp& getIntervalTime(void) const;
    inline Boolean                  getIsActive (void) const;
    inline Boolean                  operator==  (const AlarmOperand& rOp) const;
    inline Boolean                  operator!=  (const AlarmOperand& rOp) const;
    inline const                    AlarmOperandList& getParents   (void) const;   
    inline void                     addParent   (AlarmOperand& parent);

    inline static AlarmOperand*     GetOperand ( OperandName n);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

   protected:
    virtual Boolean evaluateUpdate_(void) = 0;  // Evaluate for operating 
                                                //parameter change
    virtual Boolean evaluateReset_ (void) = 0;  // Evaluate for manual/patient
                                                //data reset 

    virtual void setCauseUpdate_        (void) = 0;
    virtual void setAugmentUpdate_      (void) = 0;
    virtual void updateViolationHistory_(void) = 0;
    virtual void resetViolationHistory_ (void) = 0;
    virtual void initMaxViolations_     (void) = 0;
    virtual void initTopLevel_          (void) = 0;

    void setParentUpdate_(void);   // Set the update attribute of parents
    void setParentReset_ (void);   // Set the reset attribute of parents

    //@ Data-Member:    isActive_
    // The state of the operand is determined by evaluating the states
    // of all of its descendents during the analysis of the Operating Parameter
    // hierarchy.
    Boolean   isActive_;

    //@ Data-Member:    reset_
    // When an Alarm causing Loss of Patient Data autoresets, all alarms whose 
    // detection algorithms are based on patient data must be reset.  When the 
    // user presses the Alarm Reset key, all active alarms must be reset.  Both 
    // are accomplished by evaluating the OperatingParameter Hierarchy in reset 
    // mode.  For Patient Data Reset, the list of Alarm objects is examined 
    // and for each Alarm which has attribute patientDataAlarm_ set to TRUE the 
    // OperandName enumerator naming the OperatingCondition which causes the 
    // Alarm is placed on the AlarmManager::OperandNameList_.  For Alarm Reset,
    // the list of Alarm objects is examined and for each active Alarm, the 
    // OperandName enumerator naming the OperatingCondition which actually
    // causes the Alarm is placed on the AlarmManager::OperandNameList_.
    // Operating Parameter Hierarchy processing sets attribute reset_ for all 
    // named OperatingConditions and all of their ancestors.  WHen the hierarchy
    // is being evaluated, the isActive_ attribute of OperatingCondition objects
    // with attribute reset_ set to TRUE are negated which leads to the Alarm 
    // they cause to become inactive.
    Boolean   reset_;

    //@ Data-Member:    update_
    // When an alarm event is received on the OperatingParameterEvent Queue
    // and translated into an OperandName enumerator, the named
    // AlarmOperand and all of its ancestors are marked as needing to be
    // updated by setting attribute update_ to TRUE.
    Boolean   update_;

    //@ Data-Member:    whenOccurred_
    // Attribute whenOccurred_ is set to the current time whenever the
    // AlarmOperand changes state.
    TimeStamp whenOccurred_;

    //@ Data-Member:    intervalTime_
    // Attribute intervalTime_ is set to the current interval time whenever the
    // AlarmOperand changes state.
    OsTimeStamp         intervalTime_;


  private:
    void operator=(const AlarmOperand& op); // Declared only

    //@ Data-Member
    // The enumerated name of this AlarmOperand.
    const             OperandName name_;

    //@ Data-Member
    // Attribute parents_ is a list of references to the direct parents
    // of this AlarmOperand.
    AlarmOperandList parents_;

    //@ Data-Member
    // Operands_ is a static array of pointers to all of the AlarmOperand
    // objects.
    static AlarmOperand* Operands_[MAX_NUM_OPERANDS];
       
};

// Inlined methods
#include "AlarmOperand.in"

#endif // AlarmOperand_HH 
