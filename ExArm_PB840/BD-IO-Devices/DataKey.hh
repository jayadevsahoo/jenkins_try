#ifndef DataKey_HH
#define DataKey_HH
/**
//=======================================================================
// @copyright This is a proprietary work to which Puritan-Bennett Corporation
// claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or,
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or, otherwise with the prior written permission of
// Puritan-Bennett Corporation.
//
//        Copyright (c) 1995, Puritan-Bennett Corporation
//=======================================================================

//=======================================================================
// @class DataKey DataKey.hh "DataKey.hh"
// @brief Provides the abstraction for data key in the ventilator. The 
// data key stores the SW option keys, operational hours, serial numbers, etc.
//-----------------------------------------------------------------------
// @date 1/29/2014 Initial implementation of DataKey class based on Win CE
// file system, keeping most of the interfaces from PB840. 

The implementation borrows from the PB980 implementation where a USB thumb drive 
with a FAT file system to store data. This implementation, however, just requires 
a file folder which may be a USB thumb drive's mount point, or just a regular folder 
anywhere in the file system.
//=======================================================================*/

#include "Sigma.hh"
#include "OsTimeStamp.hh"
#include "SerialNumber.hh"

//symbolic constants

// Class declaration

class DataKey
{
	friend class DataKeyPower ;
	
    public:

        enum DataKeyType
        {
            DEMO = 0x30000000,
            PRODUCTION = 0x31000000
        };

        enum SerialNumberId 
        {
            GUI,
            BD
        };

        DataKey();
        ~DataKey( void );

        static void  SoftFault( const SoftFaultID softFaultID,
							    const Uint32      lineNumber,
							    const char*       pFileName = NULL,
							    const char*       pPredicate = NULL );

        inline Boolean isInstalled( void ) const;
        inline DataKeyType getDataKeyType( void ) const;

        SigmaStatus setDataKeyType( const DataKey::DataKeyType dataKeyType );

		SigmaStatus setSerialNumber( SerialNumberId serialNumberId,
									 const SerialNumber & rSerialNumber );

       SerialNumber getSerialNumber( SerialNumberId serialNumberId ) const; 	

        void syncVentOperationalTimes( void );
        SigmaStatus updateVentHeadOperationalTime( void );
        Uint32 readVentHeadOperationalHours( void );

        SigmaStatus writeVentHeadOperationalHours( const Uint32 operationalHours );


		SigmaStatus setOptionKey ( const Char *ptr )const; //sets optionkey in file
		const Char* getOptionKey ( void );				   //gets optionkey fromfile
				
    protected:

    private:

        DataKey( const DataKey& );          // not implemented
        void operator= ( const DataKey& );  // not implemented

		SigmaStatus syncDataKeyHours_(void);

		//@ Data-Member: bdSerialNumber_
		//  stores the BD serial number
		SerialNumber bdSerialNumber_;

        //@ Data-Member: dataKeyType_
        //  indicates the data key type
        DataKeyType dataKeyType_;

        //@ Data-Member: guiSerialNumber_
        //  stores the GUI serial number
        SerialNumber guiSerialNumber_;

		//@ Data-Member: intervalDriftMs_ 
		//  tracks the number of milliseconds "left over" at the previous
		//  update to operational minutes when interval time is truncated to
		//  nearest minute
		Int32  intervalDriftMs_;

        //@ Data-Member: isFirstUpdate_ 
		//  TRUE during first call to updateVentHeadOperationalTime_
        Boolean isFirstUpdate_;

		//@ Data-Member: minutesSincePowerup_ 
		//  the number of minutes since power up 
		Int32  minutesSincePowerUp_;

        //@ Data-Member: minutesToDataKeyUpdate_ 
        //  countdown timer to update operational time on datakey 
		//  negative if update is overdue for any reason
        Int32 minutesToDataKeyUpdate_;

		//@ Data-Member: rSoftwareOptions_
		//  Reference to the SoftwareOptions singleton object
		class SoftwareOptions& rSoftwareOptions_;

        //@ Data-Member: updateTimer_ 
        //  tracks the time since the last update to operational time
        OsTimeStamp  updateTimer_;

        //@ Data-Member: ventMinutesAtPowerup_ 
        //  the number of minutes stored in NOVRAM at powerup
        Uint32  ventMinutesAtPowerup_;

		
};  // DataKey

// Inlined methods
#include "DataKey.in"

#endif // DataKey_HH

