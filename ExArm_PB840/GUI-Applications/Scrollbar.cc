#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995-2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Scrollbar -  A generic vertical scrollbar, containing an
// elevator button used display the current position in the log and
// to activate or deactivate scrolling through the log using the knob.
//---------------------------------------------------------------------
//@ Interface-Description
// A Scrollbar object is a vertical bar with an elevator button that enables
// or disables scrolling of the scrollbar target object. When pressed, the
// scrollbar grabs panel focus and processes knob events as up or down
// scrolling actions.  In addition to enabling or disabling scrolling, 
// the elevator button changes size to indicate how much information is 
// currently being viewed and changes position to indicate the relative 
// position in the log that is being viewed.
//
// Scrollbar is designed to be used as a generic scrollbar that can be
// attached to any application object that needs vertical scrolling ability.
// Conceptually, the information that is scrolled should be organized in
// equally spaced rows.  The information can be thought of as having
// a total number of rows that we call "numEntries".  At any one time
// only a certain number of rows can be viewed, "numDisplayRows".  Each
// row has an index, with the first row of information having an index of
// zero.  The index of the row that is displayed at the top of the application
// object is called "firstDisplayedEntry".  These three numbers are the
// basis for controlling and interfacing to Scrollbar.  Typically,
// "numEntries" and "numDisplayRows" do not change.  Scrolling is achieved
// by changing the "firstDisplayEntry" value.
//
// On construction, Scrollbar is passed "numDisplayRows".  This number cannot
// change.  It is also passed a pointer to a ScrollbarTarget object.
// The object which must react to Scrollbar scroll events must inherit
// from ScrollbarTarget.  It then receives scroll events via a
// scrollHappened() method.
//
// Before Scrollbar can be displayed the setEntryInfo() method should be
// called.  This gives Scrollbar the other two important values,
// "firstDisplayedEntry" and "numEntries".  With this information Scrollbar
// can determine the size and position of the elevator.  When the user
// scrolls "firstDisplayedEntry" is changed and the change communicated
// to the target object via scrollHappened().  The target object then
// takes charge of updating the display accordingly.
//
// Button callback events are received via buttonDownHappened().
//---------------------------------------------------------------------
//@ Rationale
// A useful class that can be used by any object needing scrolling ability.
//---------------------------------------------------------------------
//@ Implementation-Description
// The main operation of Scrollbar is controlled via the button and
// panel/knob focus callback functions.  The button methods detect user 
// interaction with the elevator button used to activate/deactivate 
// scrolling through the log. The knob events are then processed to scroll 
// the log up or down. Knob events are processed as scrolling events as 
// long as the elevator button is pressed down.
//
// If "numEntries" is greater than "numDisplayRows", i.e. the user can
// only view a subset of the total data, then "firstDisplayedEntry" is
// not allowed to advance beyond a point that would cause the last
// entry to be displayed on a row other than the last row of the view.
// In other words, scrolling up the view will halt when the last entry
// becomes visible on the last row of the view.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// None of the construction parameters can subsequently be changed after
// construction e.g. you cannot change "numDisplayRows".
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/Scrollbar.ccv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc    Date:  13-Mar-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//  Changed to virtual base class to allow for derived TrendingScrollbar
//  and SettingScrollbar classes. Added getTargetType and isScrolling
//  methods. Added setWidth to support narrower SettingScrollbar.
//  Modified elevator positioning algorithm to work properly with middle
//  highlighted tables (Trending) and for variable width scrollbars.
//  Added getTargetType and isScrolling methods.
//
//
//  Revision: 006   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 004  By:  gdc    Date:  28-Apr-1998    DR Number: XXXX
//  Project:  Sigma (R8027)
//  Description:
//		Changed to scroll with the knob instead of up/down buttons.
//
//  Revision: 003  By:  sah    Date:  15-Jan-1998    DR Number: 5004
//  Project:  Sigma (R8027)
//  Description:
//      Added 'MiscStrs::SCROLLBAR_UP_ARROW' and
//      'MiscStrs::SCROLLBAR_DN_ARROW', replacing the "raw" Cheap Text
//      strings previously used.
//
//  Revision: 002  By: yyy      Date: 04-Nov-1997  DR Number: 2366
//  	Project:  Sigma (R8027)
//  	Description:
//      		In timerEventHappened() when the button just changed the
//				state from being touched to being released (buttonDownHappened),
//				even though the timer is cancelled by buttonDownHappened(),
//				there may be a timerEvent already sitting in the GuiApp's queue
//				waiting to be executed.  We need to ignore the timerEventHappend
//				call when the above	situation occured.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Scrollbar.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "GuiApp.hh"
#include "MiscStrs.hh"
#include "PromptArea.hh"
#include "PromptStrs.hh"
#include "ScrollbarTarget.hh"
#include "Sound.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 DEFAULT_WIDTH_ = 20;
static const Int32 BUTTON_BORDER_ = 2;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Scrollbar()  [Constructor]
//
//@ Interface-Description
// Constructs a Scrollbar object.  Passed the following parameters:
// >Von
//	height			Height of scrollbar in pixels (width is fixed).
//	numDisplayRows	The number of displayed rows in the scrolling area.
//	pTarget			Pointer to a ScrollbarTarget object which will receive
//					the scrolling events.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize data members, color and add all graphicals to container,
// register for button callbacks and size the scrollbar container.
//---------------------------------------------------------------------
//@ PreCondition
// pTarget must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Scrollbar::Scrollbar(Uint16 height, 
					 Uint16 numDisplayRows,
					 ScrollbarTarget *pTarget) 
:   pTarget_(               pTarget),
	maxElevatorHeight_(     height),
	elevatorButton_(        0, 0, DEFAULT_WIDTH_, maxElevatorHeight_, Button::LIGHT,
							BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER, NULL_STRING_ID),
	NUM_DISPLAY_ROWS_(      numDisplayRows),
	firstDisplayedEntry_(   0),
	numEntries_(            0),
	isScrollable_(          TRUE),
	buttonDeactivated_(     TRUE)
{
	CALL_TRACE("Scrollbar::Scrollbar(void)");

	setFillColor(Colors::LIGHT_BLUE);
	elevatorButton_.setToFlat();
	addDrawable(&elevatorButton_);

	// Register for button callbacks
	elevatorButton_.setButtonCallback(this);

	setWidth(DEFAULT_WIDTH_);
	setHeight(height);							 // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Scrollbar()  [Destructor]
//
//@ Interface-Description
// Scrollbar destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Scrollbar::~Scrollbar(void)
{
	CALL_TRACE("Scrollbar::~Scrollbar(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setWidth
//
//@ Interface-Description
// Sets the width of the scrollbar and the contained elevator button.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the new values and update the display accordingly.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void Scrollbar::setWidth(Int width)
{
	CALL_TRACE("setWidth(Int width)");

	elevatorButton_.setWidth(width);

	Container::setWidth(width);

	// Update state and adjust size/position of elevator button.
	adjustElevator_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setEntryInfo
//
//@ Interface-Description
// Sets or changes the "entry info", i.e. "firstDisplayedEntry" and
// "numEntries".
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the new values and update the display accordingly.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void Scrollbar::setEntryInfo(Uint16 firstDisplayedEntry, Uint16 numEntries)
{
	CALL_TRACE("setEntryInfo(Uint16 firstDisplayedEntry, Uint16 numEntries)");

	// Store new values
	firstDisplayedEntry_ = firstDisplayedEntry;
	numEntries_ = numEntries;

	// Update state and adjust size/position of elevator button.
	adjustElevator_();							 // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setScrollable
//
//@ Interface-Description
// Passed a flag which determines if Scrollbar is scrollable or not.
// If FALSE both the up and down buttons go to the flat state disallowing
// user interaction.  If TRUE the up and down button revert to their
// expected states.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the new value and update the button states.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void Scrollbar::setScrollable(Boolean isScrollable)
{
	CALL_TRACE("setScrollable(Uint16 isScrollable)");

	// $[TI1]
	// store the new state
	isScrollable_ = isScrollable;

	adjustElevator_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called to activate the scrollbar. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void Scrollbar::activate(void)
{
	CALL_TRACE("activate(void)");
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called to deactivate the scrollbar. Releases focus.
//---------------------------------------------------------------------
//@ Implementation-Description
// Releases panel focus via AjustPanel::TakePersistentFocus().
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	Scrollbar::deactivate(void)
{
	CALL_TRACE("activate(void)");

	// $[TI1]
	AdjustPanel::TakePersistentFocus(NULL);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isScrolling
//
//@ Interface-Description
// Returns TRUE if the scroll button is pressed to enable scrolling.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns TRUE if the button state is down.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean Scrollbar::isScrolling(void) const
{
	CALL_TRACE("isScrolling(void)");

	return(elevatorButton_.getButtonState() == Button::DOWN);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelKnobDeltaHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  If this
// object has the Adjust Panel focus (as we do when the scrollbar is selected)
// then it will be called when the operator turns the knob.  It is
// passed an integer corresponding to the amount the knob was turned.  We
// scroll the log by the delta amount.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls scroll with a negative delta so a clockwise rotation will
// scroll to the bottom of the log.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void Scrollbar::adjustPanelKnobDeltaHappened(Int32 delta)
{
	CALL_TRACE("adjustPanelKnobDeltaHappened(Int32 delta)");

	if (delta > 0)
	{											 // $[TI1.1]
		scroll(-1);
	}
	else
	{											 // $[TI1.2]
		scroll(1);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelLoseFocusHappened
//
//@ Interface-Description
// Called when this scrollbar is about to lose the focus of the Adjust
// Panel (e.g. when the operator presses down a button which
// grabs the Adjust Panel focus for itself).  We must pop ourselves up.
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets elevator button up. Clears the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void Scrollbar::adjustPanelLoseFocusHappened(void)
{
	CALL_TRACE("adjustPanelLoseFocusHappened(void)");

	if (!buttonDeactivated_)
	{											 // $[TI1.1]
		elevatorButton_.setToUp();
	}											 // $[TI1.2]

	// Clear prompts
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
								   PromptArea::PA_HIGH, NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
								   PromptArea::PA_HIGH, NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
								   PromptArea::PA_HIGH, NULL_STRING_ID);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// Called when the Clear key is pressed (and we're the current
// Adjust Panel focus).  This is an invalid choice so make the Invalid
// Entry sound.
//---------------------------------------------------------------------
//@ Implementation-Description
// Tell the sound system to make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void Scrollbar::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("adjustPanelClearPressHappened(void)");

	// Wrong choice, dude
	Sound::Start(Sound::INVALID_ENTRY);			 // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// Called when the Accept key is pressed down (and we're the current
// Adjust Panel focus).  We pass the event on to the focus subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call the acceptHappened() method of 'pFocusSubScreen_' and
// let it do its stuff.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void Scrollbar::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("adjustPanelAcceptPressHappened(void)");

	// Wrong choice, dude
	Sound::Start(Sound::INVALID_ENTRY);			 // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTargetType [virtual]
//
//@ Interface-Description
// Returns the AdjustPanelTarget::TargetType for this object. Virtual
// function of AdjustPanelTarget. Returns TARGET_TYPE_SETTING to
// indicate this focus is sensitive to user feedback and should not be
// interrupted by any extended operation (e.g. table refresh).
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns TARGET_TYPE_SETTING for this scrollbar object.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AdjustPanelTarget::TargetType Scrollbar::getTargetType(void) const
{
	CALL_TRACE("getTargetType(void)");

	return(AdjustPanelTarget::TARGET_TYPE_SETTING);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened [public, virtual]
//
//@ Interface-Description
// Callback function which handles "button down" events on the elevator
// button. 
//---------------------------------------------------------------------
//@ Implementation-Description
// This callback function is called when the user touches the elevator 
// button with the button in the up state (i.e., the button "down" event). 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void Scrollbar::buttonDownHappened(Button*, Boolean)
{
	CALL_TRACE("buttonDownHappened(Button*, Boolean)");

	// This is a key press.  Grab the Adjust Panel focus
	AdjustPanel::TakePersistentFocus(this);

	// Display a Primary and Secondary prompt, and clear Advisory prompt
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
								   PromptArea::PA_HIGH, PromptStrs::USE_KNOB_TO_SCROLL_P);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
								   PromptArea::PA_HIGH, PromptStrs::EMPTY_A);

	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened [public, virtual]
//
//@ Interface-Description
// Callback function which handles "button up" events on the elevator
// button. 
//---------------------------------------------------------------------
//@ Implementation-Description
// This callback function is called when the user touches the elevator
// button with the button in the down state (i.e., the button "up" event). 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void Scrollbar::buttonUpHappened(Button*, Boolean byOperatorAction)
{
	CALL_TRACE("buttonUpHappened(Button*, Boolean)");

	// If the operator pressed us up then release Adjust Panel focus
	if (byOperatorAction)
	{											 // $[TI1.1]
		AdjustPanel::TakePersistentFocus(NULL);
	}											 // $[TI1.2]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: scroll			[public]
//
//@ Interface-Description
// Given the number of rows to scroll (a negative number for scrolling up,
// positive for scrolling down), apply that to "firstDisplayedEntry".
//---------------------------------------------------------------------
//@ Implementation-Description
// First check to see if the scroll is invalid (i.e. trying to scroll beyond
// the top or bottom) and ignore the request if so.  For a valid scroll
// the scroll amount must be limited by the fact that firstDisplayedEntry_
// cannot go beyond the first or last pages of data.  Adjust the elevator
// according to the applied change to firstDisplayedEntry_ and inform
// the ScrollbarTarget of the change via its scrollHappened() method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void Scrollbar::scroll(Int32 numRowsToScroll)
{
	CALL_TRACE("Scrollbar::scroll(Int32 numRowsToScroll)");

	// Check to see if the scroll is invalid, i.e. an attempt is made
	// to scroll up when already at the top or an attempt to scroll down
	// when already at the bottom
	if (((0 == firstDisplayedEntry_) && (numRowsToScroll <= 0)) ||
		((firstDisplayedEntry_ == numEntries_ - NUM_DISPLAY_ROWS_) &&
		 (numRowsToScroll >= 0)))
	{											 // $[TI1]
		// Ignore scroll request
		return;
	}											 // $[TI2]

	firstDisplayedEntry_ += numRowsToScroll;

	if (firstDisplayedEntry_ < 0)
	{											 // $[TI3]
		firstDisplayedEntry_ = 0;
	}
	else if ((firstDisplayedEntry_+NUM_DISPLAY_ROWS_) > numEntries_)
	{											 // $[TI4]
		firstDisplayedEntry_ = numEntries_ - NUM_DISPLAY_ROWS_;
	}
	// $[TI5]

	adjustElevator_();

	if (pTarget_)
	{
		pTarget_->scrollHappened(firstDisplayedEntry_);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustElevator_			[Private]
//
//@ Interface-Description
// Sets the height and position of the elevator.  The height is related
// to the fraction "numDisplayRows" / "numEntries".  The position is
// related to the position of "firstDisplayedEntry" within "numEntries".
//---------------------------------------------------------------------
//@ Implementation-Description
// If the number of display rows is less than or equal to the number
// of entries then the elevator should be sized to fill the
// scrollbar. Otherwise size according to the interface description.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void Scrollbar::adjustElevator_()
{
	// Check are we displaying all entries
	if (numEntries_ <= NUM_DISPLAY_ROWS_)
	{
		// We are so max out the elevator.
		elevatorButton_.setY(0);
		elevatorButton_.setHeight(maxElevatorHeight_);
	}
	else
	{
		// Calculate the correct height of the elevator
		Uint16 height = maxElevatorHeight_ * NUM_DISPLAY_ROWS_ / numEntries_;

		// Do not let height go below the width of the scrollbar
		if (height < getWidth())
		{
			height = getWidth();
		}
		elevatorButton_.setHeight(height);

		const Int32 MAX_Y = maxElevatorHeight_ - height;

		// position based on relative position of firstDisplayedEntry_ within numEntries_
		elevatorButton_.setY( MAX_Y * firstDisplayedEntry_ / (numEntries_ - NUM_DISPLAY_ROWS_));
	}

	// take care of elevator button appearance due to changes in "scrollability"
	if (!isScrollable_ || numEntries_ <= NUM_DISPLAY_ROWS_)
	{
		elevatorButton_.setToFlat();
		buttonDeactivated_ = TRUE;
	}
	else if (buttonDeactivated_ && numEntries_ > NUM_DISPLAY_ROWS_)
	{
		elevatorButton_.setToUp();
		buttonDeactivated_ = FALSE;
	}

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void Scrollbar::SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName,
						  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SCROLLBAR,
							lineNumber, pFileName, pPredicate);
}
