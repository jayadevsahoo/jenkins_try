
#ifndef Heap_VHMCurrBreathCountDur_NUM_VHM_HH
#define Heap_VHMCurrBreathCountDur_NUM_VHM_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Heap_VHMCurrBreathCountDur_NUM_VHM - Fixed Heap of Memory Blocks.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedHeap.H_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "AlarmConstants.hh"
#include "AlarmConstants.hh"

//@ Usage-Classes
#include "VHMCurrBreathCountDur.hh"
#include "Heap.hh"
//@ End-Usage


class Heap_VHMCurrBreathCountDur_NUM_VHM : public Heap
{
  public:
    Heap_VHMCurrBreathCountDur_NUM_VHM(void);
    virtual ~Heap_VHMCurrBreathCountDur_NUM_VHM(void);

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
    virtual const Node*  getFirstNode_(void) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    Heap_VHMCurrBreathCountDur_NUM_VHM(const Heap_VHMCurrBreathCountDur_NUM_VHM&);	// not implemented...
    void  operator=(const Heap_VHMCurrBreathCountDur_NUM_VHM&);	// not implemented...

    //@ Constant:  BLOCK_NODES_
    // The number of 'Nodes' that are in a single memory block.
    enum { BLOCK_NODES_ = ((sizeof(VHMCurrBreathCountDur) + sizeof(Node)-1) / sizeof(Node)) };

    //@ Data-Member:  pMemBlock_
    // A block of nodes provides the memory block.
    Node  pMemBlock_[BLOCK_NODES_ * NUM_VHM];
};


#endif  // Heap_VHMCurrBreathCountDur_NUM_VHM_HH
