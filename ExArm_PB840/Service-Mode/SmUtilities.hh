#ifndef SmUtilities_HH
#define SmUtilities_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  SmUtilities - Service Mode Utilities
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmUtilities.hhv   25.0.4.0   19 Nov 2013 14:23:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: quf    Date: 13-Oct-1999     DR Number: 5540
//  Project:  ATC
//  Description:
//	Added utility functions atmPressureBounded() and btpsToStpd().
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  quf    Date: 07-Oct-1997    DR Number: DCS 2406
//       Project:  Sigma (840)
//       Description:
//			Implemented new PSOL exercise procedure, renamed popPsol()
//			as popPsols().
//
//  Revision: 003  By:  syw    Date: 29-Jul-1997    DR Number: DCS 2279
//       Project:  Sigma (840)
//       Description:
//			Added getMaximumFlow() method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "SmPromptId.hh"

#include "BinaryIndicator.hh"

#include "SigmaState.hh"

//@ End-Usage

extern const Real32 POP_PSOL_CURRENT;
extern const Int32  EXH_VALVE_STEP_SIZE;
extern const Real32 SERVICE_FLUSHING_VOLUME;
extern const Uint32 SERVICE_MAX_FLUSHING_TIME;
extern const Real32 SST_FLUSHING_VOLUME;
extern const Uint32 SST_MAX_FLUSHING_TIME;
extern const Uint32 DRAIN_GAS_SUPPLY_HOSE_TIMEOUT;

class SmUtilities
{
  public:
	//@ Type: TestResult
	// This enum lists all results of SmUtility tests
	enum TestResult
	{
		// common test result
		OPERATOR_EXIT,

		// gas supply check results
		GAS_CONNECTED,
		GAS_NOT_CONNECTED,

		// autozero check results
		AUTOZERO_PASSED,
		INSP_AUTOZERO_FAILED,
		EXH_AUTOZERO_FAILED,
		BOTH_AUTOZERO_FAILED,

		// BPS check results
		NOT_INSTALLED,
		CHARGED,
		CHARGING,
		ON_BATTERY_POWER,
		UNKNOWN,

		// AC check results
		AC_CONNECTED,
		AC_NOT_CONNECTED
	};	

    SmUtilities( void);
    ~SmUtilities( void);

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL,
						   const char		*pPredicate = NULL);

	Boolean messagePrompt( const SmPromptId::PromptId promptId);
	TestResult verifyWallAirConnected( void);
	TestResult verifyAirConnected( void);
	TestResult verifyO2Connected( void);
	TestResult verifyAnyGasConnected( void);
	void drainAir( void);
	void drainWallAir( void);
	void drainO2( void);
	TestResult autozeroPressureXducers( void);
	void openSafetyValve( void);
	void closeSafetyValve( void);
	void flushTubing( GasType gasType, SigmaState state = STATE_SERVICE);
	void commandExhValve( Int16 desiredCommand);
	void openExhValve( void);
	void popPsols( void);
	GasType selectGasType( void);
	TestResult getBpsStatus(void);
	TestResult checkAcPower(void);
	Uint32 pressureTimeout( SigmaState state,
							Real32 pressure, Real32 flow);
	Real32 flowError( Real32 flow, MeasurementSide side = DELIVERY);
	Real32 getMaximumFlow( const Uint32 waitTimeSecs, const GasType gasType) ;
	Real32 atmPressureBounded(void);
	Real32 btpsToStpd(Real32 btpsValue, Real32 atmPressure);

  protected:

  private:
    SmUtilities( const SmUtilities&) ;		// not implemented...
    void operator=( const SmUtilities&) ;	// not implemented...

	//@ Data-Member: bpsCharged_
	// interface to the BPS charged bit
// E600 BDIO   	BinaryIndicator  bpsCharged_;

	//@ Data-Member: bpsCharging_
	// interface to the BPS charging bit
// E600 BDIO	BinaryIndicator bpsCharging_;

};


#endif // SmUtilities_HH
