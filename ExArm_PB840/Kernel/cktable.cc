#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename: cktable.cc - POST EEPROM Checksum Table
//
//---------------------------------------------------------------------
//@ Interface-Description
//  >Von
//
//  This contains the definition structure of the checksum table of
//  of the FLASH EEPROM that is tested in POST.  It is located at
//  a fixed location in FLASH memory.
//
//  The checksum table is a structure of checksum records, where a
//  checksum record itself is a structure that has length, address, and
//  valid checksum as its members.  Refer to the header file of this module
//  for definition.
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/cktable.ccv   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "MemoryMap.hh"
#include "cktable.hh"

#ifdef E600_840_TEMP_REMOVED
const EepromStruct *PEePromChecksums   = (EepromStruct *)FLASH_CHECKSUM_BASE;
#endif

