#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ApneaSetupSubScreen - The subscreen in the Lower screen
// selected by pressing the Apnea Setup button.  Displays and allows the user
// to adjust the current Apnea vent modes and settings.
//---------------------------------------------------------------------
//@ Interface-Description
// Only one instance of ApneaSetupSubScreen should be created (member of
// LowerSubScreenArea).  The class reacts to events such as activation,
// deactivation and button presses as described below.
//
// The activateHappened()/deactivateHappened() methods are called automatically on
// display/removal of this subscreen by the LowerSubScreenArea.
// buttonDownHappened() and buttonUpHappened() are called when any button in
// the subscreen is pressed.  If the Accept key is pressed then acceptHappened()
// is called.  Changes to settings are indicated via valueUpdateHappened().
// Timer events (specifically the subscreen timeout event) are passed into
// timerEventHappened() and Adjust Panel events are communicated via the various
// adjustPanel methods.
//
// This subscreen is divided into two parts, screen 1 and screen 2.  Screen 1
// is displayed when first entering this screen.  It displays the Apnea breath
// settings appropriate to the current Apnea control setting (Apnea Mandatory
// Type) along with a button allowing passage to screen 2.  Screen 2 allows
// modification of the the Apnea control setting.  This can only be done once
// per Apnea Setup session.  On completion of screen 2 screen 1 is redisplayed.
// Settings-Validation is informed of each switch from screen 1 to screen
// 2 so that it can rerun any transition rules etc.  Note: transition to
// screen 2 is not allowed if any settings have been modified on screen 1.
//---------------------------------------------------------------------
//@ Rationale
// This class gathers all the functionality of the Apnea Setup subscreen into
// one place.
//---------------------------------------------------------------------
//@ Implementation-Description
// The class creates many buttons (mostly setting buttons) and most of its work
// is in determining which ones to display and where onscreen should they go.
// The most complicated setup is on screen 1.  The layout of both screens is
// handled by the layoutScreen_() method.  It determines where to place all the
// various setting buttons applicable to the current Apnea Mandatory Type.
//
// To confirm settings changes and exit the screen, the Accept key must be pressed.
//
// Some of the prompts displayed by this subscreen change depending on whether
// the subscreen is activated during normal operation or by way of Same/New
// Patient Setup.  The method LowerScreen::RLowerScreen.apneaPatientSetupStatus()
// tells us if we need to display these prompts.
//
// $[01057]
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// which verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created, by LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ApneaSetupSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 012   By: gdc    Date: 11-Aug-2008     SCR Number: 6439 
//  Project:  840S
//  Description:
//      Removed workaround for touch "drill-down" problem.
//
//  Revision: 011  By:  gdc	   Date:  03-Jun-2007    SCR Number:  6237
//  Project:  Trend
//  Description:
//      Added positioning gravity for flow pattern as part of automated 
//      text positioning change.
//
//  Revision: 010   By: gdc    Date:  04-Jan-2001    DCS Number: 5493
//  Project:  GuiComms
//  Description:
//      Modified for new Touch Driver interface.
//
//  Revision: 009   By: sah    Date: 05-May-2000     DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  swapped placement of Ti and Pi setting buttons, and re-organized
//         initialization for improved readability
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//
//  Revision: 008   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Modified initialization of the "Pi" button to include the use of
//      the "above PEEP" phrase, as an auxillary title.
//
//  Revision: 007   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
//		As RLowerScreen, RUpperScreen, RServiceLowerScreen and RServiceUpperScreen
//	have become members of their corresponding classes, class scopes for these
//	reference variables must change from GuiApp to their respective classes, i.e,
//	LowerScreen, UpperScreen, ServiceLowerScreen and ServiceUpperScreen.
//
//  Revision: 006  By:  hhd	   Date:  26-Jul-1999    DCS Number:  5480
//  Project:  ATC
//  Description:
//		Show the ChangeMandTypeButton depending on if this setting value has been changed,
//  (remove the show flag's hardcode value).
//
//  Revision: 005  By:  hhd	   Date:  29-Jun-1999    DCS Number:  5454
//  Project:  ATC
//  Description:
//		Turn the changeMandTypeButton_'s show flag back on when laying out Screen1 again
//		after Continue button is pressed.
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  10-Sep-97    DR Number: 2169
//    Project:  Sigma (R8027)
//    Description:
//      Removed obsolete SRS references.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "Colors.hh"
#include "ApneaSetupSubScreen.hh"

#include "Sigma.hh"
#include "SettingContextHandle.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "FlowPatternValue.hh"
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "MandTypeValue.hh"
#include "PromptArea.hh"
#include "Sound.hh"
#include "LowerScreen.hh"
#include "SubScreenArea.hh"
#include "ContextSubject.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 MAND_TYPE_BUTTON_WIDTH_ = 140;
static const Int32 MAND_TYPE_BUTTON_HEIGHT_ = 46;
static const Int32 MAND_TYPE_BUTTON_Y_ = 65;
static const Int32 MAND_TYPE_BUTTON_X_ =
		( ::LOWER_SUB_SCREEN_AREA_WIDTH - MAND_TYPE_BUTTON_WIDTH_ ) / 2;
static const Int32 MAND_TYPE_BUTTON_BORDER_ = 3;

static const Int32 SETTING_BUTTON_BORDER_ = 3;
static const Int32 CONTINUE_BUTTON_X_ = 540;
static const Int32 CONTINUE_BUTTON_Y_ = 200;
static const Int32 CONTINUE_BUTTON_WIDTH_ = 84;
static const Int32 CONTINUE_BUTTON_HEIGHT_ = 34;
static const Int32 CONTINUE_BUTTON_BORDER_ = 3;
static const Int32 SCRN1_BUTTON_WIDTH_ = 106;
static const Int32 SCRN1_BUTTON_HEIGHT_ = 46;
static const Int32 SCRN1_BUTTON_BORDER_ = 3;
static const Int32 SCRN1_BUTTON_WIDTH_SHAVE_ = 2;
static const Int32 STATUS_BAR_X_ = 1;
static const Int32 STATUS_BAR_Y_ = 1;
static const Int32 STATUS_BAR_WIDTH_ = ::LOWER_SUB_SCREEN_AREA_WIDTH - 2;
static const Int32 STATUS_BAR_HEIGHT_ = 19;
static const Int32 MAIN_SETTINGS_AREA_X_ = 0;
static const Int32 MAIN_SETTINGS_AREA_Y_ = STATUS_BAR_Y_ + STATUS_BAR_HEIGHT_;
static const Int32 MAIN_SETTINGS_AREA_WIDTH_ = LOWER_SUB_SCREEN_AREA_WIDTH;
static const Int32 MAIN_SETTINGS_AREA_HEIGHT_ = 2 * SCRN1_BUTTON_HEIGHT_ + 2;
static const Int32 ROW0_BUTTON_Y_ = 2;
static const Int32 ROW1_BUTTON_Y_ = ROW0_BUTTON_Y_ + SCRN1_BUTTON_HEIGHT_;
static const Int32 ROW2_BUTTON_Y_ = MAIN_SETTINGS_AREA_Y_
					+ MAIN_SETTINGS_AREA_HEIGHT_ + 2;
static const Int32 ROW3_BUTTON_Y_ = ROW2_BUTTON_Y_ + SCRN1_BUTTON_HEIGHT_;
static const Int32 NUMERIC_VALUE_CENTER_X_ = 40;
static const Int32 NUMERIC_VALUE_CENTER_Y_ = 26;
static const Int32 LEFT_WING_X1_ = 113;
static const Int32 LEFT_WING_X2_ = 188;
static const Int32 RIGHT_WING_X1_ = 235;
static const Int32 RIGHT_WING_X2_ = 310;
static const Int32 WING_Y_ = 10;
static const Int32 WING_PEN_ = 2;
static const Int32 WINGTIP_WIDTH_ = 6;
static const Int32 WINGTIP_HEIGHT_ = 6;
static const Int32 TIMING_DIAGRAM_X_ = 20;
static const Int32 TIMING_DIAGRAM_Y_ = 124;
static const Int32 APNEA_SETUP_TEXT_X_ = 6;
static const Int32 APNEA_SETUP_TEXT_Y_ = 97;

static Uint  MandTypeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
				  (sizeof(StringId) * (MandTypeValue::TOTAL_MAND_TYPES - 1)))];
static Uint  FlowPatternInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		    (sizeof(StringId) * (FlowPatternValue::TOTAL_FLOW_PATTERNS - 1)))];

static DiscreteSettingButton::ValueInfo*  PMandTypeInfo_ =
					(DiscreteSettingButton::ValueInfo*)::MandTypeInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PFlowPatternInfo_ =
				 (DiscreteSettingButton::ValueInfo*)::FlowPatternInfoMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ApneaSetupSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructs the Apnea Setup subscreen.  Only one instance of this
// class should be created (by LowerSubScreenArea).  It is passed a
// pointer to LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// Many buttons are initialized here.  The subscreen is sized and
// colored.  We register as a target for all settings in this subscreen.
// We register for callbacks from numerous buttons.  The valid
// discrete values for all DiscreteSettingButton's are setup.
//
// $[01034] All of these buttons must be of the select type ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ApneaSetupSubScreen::ApneaSetupSubScreen(SubScreenArea *pSubScreenArea) :
	BatchSettingsSubScreen(pSubScreenArea),
	onScreen1_(TRUE),

	changeMandTypeButton_(5 * SCRN1_BUTTON_WIDTH_, ROW3_BUTTON_Y_,
			SCRN1_BUTTON_WIDTH_ - SCRN1_BUTTON_WIDTH_SHAVE_, 
			SCRN1_BUTTON_HEIGHT_,
			Button::LIGHT, SETTING_BUTTON_BORDER_, Button::SQUARE,
			Button::NO_BORDER, MiscStrs::CHANGE_MAND_TYPE_BUTTON_TITLE),
	continueButton_(CONTINUE_BUTTON_X_, CONTINUE_BUTTON_Y_,
			CONTINUE_BUTTON_WIDTH_, CONTINUE_BUTTON_HEIGHT_,
			Button::LIGHT, CONTINUE_BUTTON_BORDER_, Button::SQUARE,
			Button::NO_BORDER, MiscStrs::CONTINUE_BUTTON_TITLE),

	//=================================================================
	// Main Control Settings (1st screen)...
	//=================================================================
	mandatoryTypeButton_(MAND_TYPE_BUTTON_X_, MAND_TYPE_BUTTON_Y_,
			MAND_TYPE_BUTTON_WIDTH_, MAND_TYPE_BUTTON_HEIGHT_,
			Button::LIGHT, MAND_TYPE_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER,
			MiscStrs::MAND_TYPE_BUTTON_TITLE_SMALL,
			SettingId::APNEA_MAND_TYPE, this,
			MiscStrs::APNEA_MAND_TYPE_HELP_MESSAGE, 18),

	//=================================================================
	// Breath Settings (2nd Screen)...
	//=================================================================

	//-----------------------------------------------------------------
	// Column #0, Row #0...
	//-----------------------------------------------------------------
	respiratoryRateButton_(0, ROW0_BUTTON_Y_,
			SCRN1_BUTTON_WIDTH_, SCRN1_BUTTON_HEIGHT_,
			Button::LIGHT, SCRN1_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::RESPIRATORY_RATE_BUTTON_TITLE_SMALL,
			MiscStrs::ONE_PER_MIN_UNITS_SMALL,
			SettingId::APNEA_RESP_RATE, this,
			MiscStrs::APNEA_RESPIRATORY_RATE_HELP_MESSAGE),

	//-----------------------------------------------------------------
	// Column #0, Row #1...
	//-----------------------------------------------------------------


	//-----------------------------------------------------------------
	// Column #1, Row #0...
	//-----------------------------------------------------------------
	inspiratoryPressureButton_(SCRN1_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
			SCRN1_BUTTON_WIDTH_, SCRN1_BUTTON_HEIGHT_,
			Button::LIGHT, SCRN1_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::INSPIRATORY_PRESSURE_BUTTON_TITLE_SMALL,
			GuiApp::GetPressureUnitsSmall(),
			SettingId::APNEA_INSP_PRESS, this,
			MiscStrs::APNEA_INSPIRATORY_PRESSURE_HELP_MESSAGE,
			FALSE, MiscStrs::ABOVE_PEEP_AUX_TEXT_SMALL, ::GRAVITY_RIGHT),
	tidalVolumeButton_(SCRN1_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
			SCRN1_BUTTON_WIDTH_, SCRN1_BUTTON_HEIGHT_,
			Button::LIGHT, SCRN1_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::TIDAL_VOLUME_BUTTON_TITLE_SMALL,
			MiscStrs::ML_UNITS_SMALL,
			SettingId::APNEA_TIDAL_VOLUME, this,
			MiscStrs::APNEA_TIDAL_VOLUME_HELP_MESSAGE),


	//-----------------------------------------------------------------
	// Column #1, Row #1...
	//-----------------------------------------------------------------


	//-----------------------------------------------------------------
	// Column #2, Row #0...
	//-----------------------------------------------------------------
	inspiratoryTimeButton_(2 * SCRN1_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
			SCRN1_BUTTON_WIDTH_, SCRN1_BUTTON_HEIGHT_, Button::LIGHT,
			SCRN1_BUTTON_BORDER_, Button::ROUND,
			Button::NO_BORDER, TextFont::EIGHTEEN,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::INSPIRATORY_TIME_BUTTON_TITLE_SMALL,
			MiscStrs::SEC_UNITS_SMALL,
			SettingId::APNEA_INSP_TIME, this,
			MiscStrs::APNEA_INSPIRATORY_TIME_HELP_MESSAGE),
	peakFlowButton_(2 * SCRN1_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
			SCRN1_BUTTON_WIDTH_, SCRN1_BUTTON_HEIGHT_,
			Button::LIGHT, SCRN1_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PEAK_FLOW_BUTTON_TITLE_SMALL,
			MiscStrs::L_PER_MIN_UNITS_SMALL,
			SettingId::APNEA_PEAK_INSP_FLOW, this,
			MiscStrs::APNEA_PEAK_FLOW_HELP_MESSAGE),


	//-----------------------------------------------------------------
	// Column #2, Row #1...
	//-----------------------------------------------------------------
	flowPatternButton_(2 * SCRN1_BUTTON_WIDTH_, ROW1_BUTTON_Y_,
			SCRN1_BUTTON_WIDTH_, SCRN1_BUTTON_HEIGHT_,
			Button::LIGHT, SCRN1_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, NULL_STRING_ID,
			SettingId::APNEA_FLOW_PATTERN, this,
			MiscStrs::APNEA_FLOW_PATTERN_HELP_MESSAGE, 12),


	//-----------------------------------------------------------------
	// Column #3, Row #0...
	//-----------------------------------------------------------------


	//-----------------------------------------------------------------
	// Column #3, Row #1...
	//-----------------------------------------------------------------


	//-----------------------------------------------------------------
	// Column #4, Row #0...
	//-----------------------------------------------------------------


	//-----------------------------------------------------------------
	// Column #4, Row #1...
	//-----------------------------------------------------------------


	//-----------------------------------------------------------------
	// Column #5, Row #0...
	//-----------------------------------------------------------------
	oxygenPercentageButton_(5 * SCRN1_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
			SCRN1_BUTTON_WIDTH_ - SCRN1_BUTTON_WIDTH_SHAVE_, SCRN1_BUTTON_HEIGHT_,
			Button::LIGHT, SCRN1_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::OXYGEN_PERCENTAGE_BUTTON_TITLE_SMALL,
			MiscStrs::PERCENT_UNITS_SMALL,
			SettingId::APNEA_O2_PERCENT, this,
			MiscStrs::APNEA_OXYGEN_PERCENTAGE_HELP_MESSAGE),


	//-----------------------------------------------------------------
	// Column #5, Row #1...
	//-----------------------------------------------------------------


	//-----------------------------------------------------------------
	// Column #5, Row #2...
	//-----------------------------------------------------------------
	intervalButton_(5 * SCRN1_BUTTON_WIDTH_, ROW2_BUTTON_Y_,
			SCRN1_BUTTON_WIDTH_ - SCRN1_BUTTON_WIDTH_SHAVE_, SCRN1_BUTTON_HEIGHT_,
			Button::LIGHT, SCRN1_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::APNEA_INTERVAL_BUTTON_TITLE,
			MiscStrs::SEC_UNITS_SMALL,
			SettingId::APNEA_INTERVAL, this,
			MiscStrs::APNEA_INTERVAL_HELP_MESSAGE),

	titleArea_(NULL_STRING_ID, SubScreenTitleArea::SSTA_CENTER),
	leftWingtip_(LEFT_WING_X1_ - WINGTIP_WIDTH_,
					WING_Y_ + WINGTIP_HEIGHT_, LEFT_WING_X1_,
					WING_Y_, WING_PEN_),
	leftWing_(LEFT_WING_X1_, WING_Y_, LEFT_WING_X2_,
					WING_Y_, WING_PEN_),
	rightWing_(RIGHT_WING_X1_, WING_Y_,
					RIGHT_WING_X2_, WING_Y_, WING_PEN_),
	rightWingtip_(RIGHT_WING_X2_, WING_Y_, 
					RIGHT_WING_X2_ + WINGTIP_WIDTH_,
					WING_Y_ + WINGTIP_HEIGHT_, WING_PEN_),
	breathTimingDiagram_(TRUE),
	apneaSetupText_(MiscStrs::APNEA_SETUP_TEXT)
{
	CALL_TRACE("ApneaSetupSubScreen::ApneaSetupSubScreen(SubScreenArea "
													"*pSubScreenArea)");

	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);
	setFillColor(Colors::MEDIUM_BLUE);

	// Register for callbacks for any buttons we need.
	changeMandTypeButton_.setButtonCallback(this);
	continueButton_.setButtonCallback(this);
	inspiratoryTimeButton_.setButtonCallback(this);
	respiratoryRateButton_.setButtonCallback(this);

	setTitleChangeState_(MiscStrs::MODE_APNEA_TITLE, FALSE, modeTitle_);

	// set up values for apnea mandatory type....
	::PMandTypeInfo_->numValues = MandTypeValue::TOTAL_MAND_TYPES;
	::PMandTypeInfo_->arrValues[MandTypeValue::PCV_MAND_TYPE] =
											MiscStrs::MAND_TYPE_PCV_VALUE;
	::PMandTypeInfo_->arrValues[MandTypeValue::VCV_MAND_TYPE] =
											MiscStrs::MAND_TYPE_VCV_VALUE;
	mandatoryTypeButton_.setValueInfo(::PMandTypeInfo_);

	// set up values for apnea flow pattern....
	::PFlowPatternInfo_->numValues = FlowPatternValue::TOTAL_FLOW_PATTERNS;
	::PFlowPatternInfo_->arrValues[FlowPatternValue::SQUARE_FLOW_PATTERN] =
									MiscStrs::FLOW_PATTERN_SQUARE_VALUE_SMALL;
	::PFlowPatternInfo_->arrValues[FlowPatternValue::RAMP_FLOW_PATTERN] =
									MiscStrs::FLOW_PATTERN_RAMP_VALUE_SMALL;
	flowPatternButton_.setValueInfo(::PFlowPatternInfo_);
	flowPatternButton_.setValueTextPosition(::GRAVITY_TOP);


	// Position, size and color graphics
	statusBar_.setX(STATUS_BAR_X_);
	statusBar_.setY(STATUS_BAR_Y_);
	statusBar_.setWidth(STATUS_BAR_WIDTH_);
	statusBar_.setHeight(STATUS_BAR_HEIGHT_);
	statusBar_.setFillColor(Colors::LIGHT_BLUE);
	statusBar_.setContentsColor(Colors::BLACK);
	statusBar_.addDrawable(&modeTitle_);
	statusBar_.addDrawable(&mandatoryTypeTitle_);
	statusBar_.addDrawable(&leftWingtip_);
	statusBar_.addDrawable(&leftWing_);
	statusBar_.addDrawable(&rightWing_);
	statusBar_.addDrawable(&rightWingtip_);
	addDrawable(&statusBar_);

	mandatoryTypeTitle_.setX(SCRN1_BUTTON_WIDTH_);

	mainSettingsAreaContainer_.setX(MAIN_SETTINGS_AREA_X_);
	mainSettingsAreaContainer_.setY(MAIN_SETTINGS_AREA_Y_);
	mainSettingsAreaContainer_.setWidth(MAIN_SETTINGS_AREA_WIDTH_);
	mainSettingsAreaContainer_.setHeight(MAIN_SETTINGS_AREA_HEIGHT_);
	mainSettingsAreaContainer_.setFillColor(Colors::EXTRA_DARK_BLUE);
	addDrawable(&mainSettingsAreaContainer_);

	apneaSetupText_.setColor(Colors::YELLOW);
	addDrawable(&apneaSetupText_);

	breathTimingDiagram_.setX(TIMING_DIAGRAM_X_);
	breathTimingDiagram_.setY(TIMING_DIAGRAM_Y_);
	addDrawable(&breathTimingDiagram_);

	addDrawable(&titleArea_);
	addDrawable(&continueButton_);
	addDrawable(&changeMandTypeButton_);


	intervalButton_.setTimingResolutionEnabled(TRUE);

	//-----------------------------------------------------------------
	// initialize array of Screen #1's setting button pointers...
	//-----------------------------------------------------------------

	// add Screen #1 buttons to array...
	Uint idx = 0u;
	arrScreen1BtnPtrs_[idx++] = &intervalButton_;   // this must be 1st...
	arrScreen1BtnPtrs_[idx++] = &flowPatternButton_;
	arrScreen1BtnPtrs_[idx++] = &inspiratoryPressureButton_;
	arrScreen1BtnPtrs_[idx++] = &inspiratoryTimeButton_;
	arrScreen1BtnPtrs_[idx++] = &oxygenPercentageButton_;
	arrScreen1BtnPtrs_[idx++] = &peakFlowButton_;
	arrScreen1BtnPtrs_[idx++] = &respiratoryRateButton_;
	arrScreen1BtnPtrs_[idx++] = &tidalVolumeButton_;
	arrScreen1BtnPtrs_[idx] = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_SCREEN1_SETTING_BUTTONS_), idx);

	//-----------------------------------------------------------------
	// add Screen #1's setting buttons to container...
	//-----------------------------------------------------------------

	idx = 0u;
	addDrawable(arrScreen1BtnPtrs_[idx++]);  // add Ta to subscreen...

	for (; arrScreen1BtnPtrs_[idx] != NULL; idx++)
	{
		// the rest of Screen #1's buttons attach to an internal container...
		mainSettingsAreaContainer_.addDrawable(arrScreen1BtnPtrs_[idx]);
	}

	//-----------------------------------------------------------------
	// initialize array of Screen #2's setting button pointers...
	//-----------------------------------------------------------------

	// add Screen #2 buttons to array...
	idx = 0u;
	arrScreen2BtnPtrs_[idx++] = &mandatoryTypeButton_;
	arrScreen2BtnPtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_SCREEN2_SETTING_BUTTONS_), idx);

	//-----------------------------------------------------------------
	// add Screen #2's setting buttons to container...
	//-----------------------------------------------------------------

	for (idx = 0u; arrScreen2BtnPtrs_[idx] != NULL; idx++)
	{
		// all of Screen #2's buttons attach to this subscreen directly...
		addDrawable(arrScreen2BtnPtrs_[idx]);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ApneaSetupSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys the Apnea Setup subscreen.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ApneaSetupSubScreen::~ApneaSetupSubScreen(void)
{
	CALL_TRACE("ApneaSetupSubScreen::~ApneaSetupSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called when the Accept key is pressed.  Allows us to accept the Apnea
// settings and exit this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// The operator has accepted the settings so we inform the Settings-Validation
// subsystem of such.  The Settings-Validation subsystem takes care of storing
// the new settings for disaster recovery purposes.
//
// We then reactivate this subscreen, leaving it visible with the newly
// accepted settings visible as current settings.
//
// $[01056] When batch settings are being setup ventilator settings
//			will not be affected
// $[01257] The Accept key shall be the ultimate confirmation step for all ...
// $[01258] The GUI shall accept all adjusted settings.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaSetupSubScreen::acceptHappened(void)
{
	CALL_TRACE("ApneaSetupSubScreen::acceptHappened(void)");

	if ( areSettingsCurrent_() || !onScreen1_ )
	{														// $[TI1.1]
        Sound::Start(Sound::INVALID_ENTRY);
	}
	else
	{														// $[TI1.2]
		// Make the Accept sound, clear the Adjust Panel focus, store the
		// current settings, accept the adjusted settings and deactivate
		// this subscreen.
		Sound::Start(Sound::ACCEPT);
		AdjustPanel::TakeNonPersistentFocus(NULL);
		SettingContextHandle::AcceptApneaSetupBatch();

		deactivate();
		activate();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when any button in Apnea Setup is pressed down.  We must restart the
// subscreen setting change timer and act upon the Continue, Change Mandatory
// Type buttons being pressed.  Passed a pointer to the button as
// well as `byOperatorAction', a flag which indicates whether the operator
// pressed the button down or whether the setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the continue button or change mandatory type button was pressed down
// then we must switch screens (continue button switches from 2 to 1, change
// mandatory type from 1 to 2).  The screen layout for either case is handled
// by layoutScreen_().
//
// If the Respiratory Rate or Inspiratory Time buttons were pressed down then
// we need to inform the Breath Timing diagram of the fact 'cos it needs to
// highlight the corresponding setting value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaSetupSubScreen::buttonDownHappened(Button *pButton,
													Boolean byOperatorAction)
{
	CALL_TRACE("ApneaSetupSubScreen::buttonDownHappened(Button *pButton, "
												"Boolean byOperatorAction)");

	// Check for the Change Mandatory Type or Continue buttons being pressed.
	// If either has been then clear the Adjust Panel focus and switch
	// screens.
	if ((pButton == &changeMandTypeButton_) || (pButton == &continueButton_))
	{													// $[TI1]
		CLASS_ASSERTION(byOperatorAction);

		// Go to Screen 1 if Continue button pressed, else Screen 2
		if (pButton == &continueButton_)
		{												// $[TI2]
			onScreen1_ = TRUE;
			SettingContextHandle::
					AdjustApneaSetupBatch(BREATH_SETTINGS_ADJUST_PHASE, TRUE);
		}
		else
		{												// $[TI3]
			onScreen1_ = FALSE;
			SettingContextHandle::
					AdjustApneaSetupBatch(MAIN_CONTROL_ADJUST_PHASE);
		}

		// Reset panel focus to the default (this) subscreen
		AdjustPanel::TakeNonPersistentFocus(NULL);

		// Bounce the button
		pButton->setToUp();

		// Do screen layout
		layoutScreen_();
	}
	// Check for the Respiratory Rate button being pressed
	else if (pButton == &respiratoryRateButton_)
	{													// $[TI5]
		// Inform the breath timing diagram of same
		breathTimingDiagram_.setActiveSetting(SettingId::APNEA_RESP_RATE);
	}
	// Check for the Inspiratory Time button being pressed
	else if (pButton == &inspiratoryTimeButton_)
	{													// $[TI6]
		// Inform the breath timing diagram of same
		breathTimingDiagram_.setActiveSetting(SettingId::APNEA_INSP_TIME);
	}
	else
	{
		// Unknown button!
		CLASS_ASSERTION(FALSE);							// $[TI7]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when any button in Apnea Setup is pressed up.  Passed a pointer to
// the button as well as `byOperatorAction', a flag which indicates whether the
// operator pressed the button up or whether the setToUp() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// If the Respiratory Rate or Inspiratory Time buttons was pressed up then we
// need to inform the breath timing diagram that there is no active setting so
// that it can unhighlight the setting in question.
//
// There is a possibility that this event occurs after this subscreen is
// removed from the screen (when this subscreen is deactivated and the Adjust
// Panel focus is lost).  In this case we do nothing, simply return.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaSetupSubScreen::buttonUpHappened(Button *pButton, Boolean)
{
	CALL_TRACE("ApneaSetupSubScreen::buttonUpHappened(Button *pButton, Boolean)");

	// Don't do anything if this subscreen isn't displayed
	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	// Check for one of the timing buttons being pressed up
	if ((pButton == &respiratoryRateButton_)
			|| (pButton == &inspiratoryTimeButton_))
	{													// $[TI4]
		// Inform the breath timing diagram that there is no active setting
		breathTimingDiagram_.setActiveSetting(SettingId::NULL_SETTING_ID);
	}													// $[TI5]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_
//
//@ Interface-Description
// No parameters needed.  Lays out all the drawables for either screen
// of Apnea Setup.
//---------------------------------------------------------------------
//@ Implementation-Description
// Uses the onScreen1_ flag to determine which screen must be setup.
// Screen 2 contains just the Apnea Mandatory Type setting button and a
// Continue button to allow the user to return to screen 1.
//
// Screen 1 displays the Apnea breath settings applicable to the current
// Apnea Mandatory Type.  Here's a list of them and when they are displayed.
//
// >Von
//	Apnea flow pattern			when Apnea mandatory type is VCV
//	Apnea inspiratory pressure	when Apnea mandatory type is PCV
//	Apnea inspiratory time		when Apnea mandatory type is PCV
//	Apnea interval				always
//	Apnea oxygen percentage		always
//	Apnea peak inspiratory flow	when Apnea mandatory type is VCV
//	Apnea respiratory rate		always
//	Apnea tidal volume			when Apnea mandatory type is VCV
// >Voff
//
// $[01002] Inactive settings and data must be removed from view ...
// $[01051] If a setting is inapplicable then remove its setting button ...
// $[01055] Multiple setting changes are allowed before accepting ...
// $[01095] Apnea control setting consists of apnea mandatory type.
// $[01096] Apnea breath settings consist of ...
// $[01097] Can't adjust control setting and breath settings at the same time.
// $[01098] Apnea breath setting button only displayed when applicable.
// $[01099] Apnea control setting displayed read-only when ...
// $[01102] Button to go to control setting must be displayed until ...
// $[01103] Continue button displayed with apnea control setting.
// $[01106] Breath timing graph displayed with apnea breath settings.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaSetupSubScreen::layoutScreen_(void)
{
	CALL_TRACE("ApneaSetupSubScreen::layoutScreen_(void)");

	// these are only to show when NOT on Screen #1...
	continueButton_.setShow(!onScreen1_);
	titleArea_.setShow(!onScreen1_);

	// these are only to show when on Screen #1...
	mainSettingsAreaContainer_.setShow(onScreen1_);
	statusBar_.setShow(onScreen1_);
	breathTimingDiagram_.setShow(onScreen1_);
	apneaSetupText_.setShow(onScreen1_);

	if (!onScreen1_)
	{													// $[TI1]
		// Layout Screen 2
		continueButton_.activate();
		changeMandTypeButton_.setShow(FALSE);

		// activate all Screen #2's setting buttons...
		operateOnButtons_(arrScreen2BtnPtrs_, &SettingButton::activate);
		operateOnButtons_(arrScreen1BtnPtrs_, &SettingButton::deactivate);

		// Set the title
		titleArea_.setTitle(MiscStrs::CURRENT_APNEA_VENT_TYPE_TITLE);
		setApneaSetupTabTitleText_(MiscStrs::APNEA_CURRENT_SETUP_TAB_BUTTON_LABEL);

		// Change Secondary prompt to explain the Continue button
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_LOW, PromptStrs::APNEA_SETUP_CONTINUE_CANCEL_S);

		// Finished Screen 2 layout
	}
	else
	{													// $[TI2]
		// Layout Screen 1
		changeMandTypeButton_.activate();
		mainSettingsAreaContainer_.activate();
		statusBar_.activate();
		breathTimingDiagram_.activate();
		apneaSetupText_.activate();

		// activate all Screen #1's setting buttons...
		operateOnButtons_(arrScreen1BtnPtrs_, &SettingButton::activate);
		operateOnButtons_(arrScreen2BtnPtrs_, &SettingButton::deactivate);

		apneaSetupText_.setX(APNEA_SETUP_TEXT_X_);
		apneaSetupText_.setY(APNEA_SETUP_TEXT_Y_);

		const ContextSubject *const  P_ADJUSTED_SUBJECT =
								getSubjectPtr_(ContextId::ADJUSTED_CONTEXT_ID);

		const DiscreteValue  MANDATORY_TYPE =
			P_ADJUSTED_SUBJECT->getSettingValue(SettingId::APNEA_MAND_TYPE);

		const Boolean MANDATORY_TYPE_CHANGED =
			P_ADJUSTED_SUBJECT->isSettingChanged(SettingId::APNEA_MAND_TYPE);

		StringId  titleFormatStr;
	
		switch (MANDATORY_TYPE)
		{
		case MandTypeValue::PCV_MAND_TYPE:					// $[TI5]
			titleFormatStr = MiscStrs::MAND_TYPE_PCV_TITLE;
			break;

		case MandTypeValue::VCV_MAND_TYPE:					// $[TI6]
			titleFormatStr = MiscStrs::MAND_TYPE_VCV_TITLE;
			break;

		default:											// $[TI7]
			// unexpected mandatory type value...
			titleFormatStr = L"{p=12,x=93,y=16,%c:***}";  
			break;
		}

		// set the state of Mandatory Type's title field...
		setTitleChangeState_(titleFormatStr, MANDATORY_TYPE_CHANGED,
							 mandatoryTypeTitle_);

		// Show changeMandTypeButton depending on whether this setting value
		// has been changed.
		changeMandTypeButton_.setShow(!MANDATORY_TYPE_CHANGED);
		
		// Display the appropriate title, current or proposed
		if (areSettingsCurrent_())
		{													// $[TI3]
			setApneaSetupTabTitleText_(MiscStrs::APNEA_CURRENT_SETUP_TAB_BUTTON_LABEL);
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::APNEA_SETUP_CANCEL_S);
		}
		else
		{													// $[TI4]
			setApneaSetupTabTitleText_(MiscStrs::APNEA_PROPOSED_SETUP_TAB_BUTTON_LABEL);
       		// Set Primary "Press ACCEPT if OK." prompt.
       		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_HIGH, PromptStrs::PRESS_ACCEPT_IF_OK_P);
	
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_LOW,
								PromptStrs::APNEA_SETUP_PROCEED_CANCEL_S);
		}
	
	
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_AS_NEEDED_P);
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the subscreen setting change timer that we started runs out.
// Passed the id of the timer.  We reset the subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// We check that this subscreen is visible and we reset the subscreen back
// to screen 1 by activating this subscreen.
//
// $[01105] If no user interface action is performed for 3 minutes then ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaSetupSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	if ( isVisible() )
	{														// $[TI1.1]
    	switch (timerId)
    	{
    	case GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT:	// $[TI2.1]
			// Clear the Adjust Panel focus
			AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);
	
			// Go back to Screen 1 by reactivating this screen
			activate();
			break;
	
    	default:
        	AUX_CLASS_ASSERTION_FAILURE(timerId);
        	break;
		}
	}														// $[TI1.2]

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened()
//
//@ Interface-Description
//  This virtual function of BatchSettingsSubScreen is overriden here
//  to always clear the primary prompt when panel focus is lost.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
ApneaSetupSubScreen::adjustPanelRestoreFocusHappened(void)
{
    CALL_TRACE("adjustPanelRestoreFocusHappened()");
 
	if ( !areSettingsCurrent_() )
	{														// $[TI1.1]
		if ( !onScreen1_ )
		{													// $[TI2.1]
        	// Set Primary "Press CONTINUE if OK." prompt.
        	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
                   	PromptArea::PA_HIGH, PromptStrs::PRESS_CONTINUE_IF_OK_P);
		}
		else
		{													// $[TI2.2]
        	// Set Primary "Press ACCEPT if OK." prompt.
        	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
                   	PromptArea::PA_HIGH, PromptStrs::PRESS_ACCEPT_IF_OK_P);
		}
	}
	else
	{														// $[TI1.2]
		// Clear the high priority prompts
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
// Called by LowerSubScreenArea just before this subscreen is displayed.
// Needs no arguments.  Allows setting up of the subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// First of all we start the subscreen setting change timer.  We then begin
// adjustment of the accepted settings.  The Change Mandatory Type button is
// displayed (if we're not in Settings Lockout mode) and then screen 1 is laid
// out (via layoutScreen_()).  Some prompts are also displayed to assist the
// user, with particular attention being paid to advising the operator of
// auto-corrections to Apnea Oxygen Percentage and/or Apnea Inpiratory
// Pressure.   If we are in settings lockedout mode then we flat all
// buttons and display the setting locked out prompt.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaSetupSubScreen::activateHappened_(void)
{
	CALL_TRACE("activateHappened_()");

	// Adjust accepted settings,
	correctionStatus_ = SettingContextHandle::
							AdjustApneaSetupBatch(BREATH_SETTINGS_ADJUST_PHASE);

	onScreen1_ = TRUE;				// We start on Screen 1

	changeMandTypeButton_.setShow(TRUE);

	// All settings are current if none were corrected when we adjusted the
	// Apnea batch settings.
	setCurrentSettingsFlag_((NONE_CORRECTED == correctionStatus_));
														// $[TI1], $[TI2]

	// Layout the screen
	layoutScreen_();

	// Check if we need to display special advisory prompts.
	if (LowerScreen::RLowerScreen.apneaPatientSetupStatus() !=
				LowerScreen::APNEA_NO_SETUP)
	{													// $[TI6]
		switch (LowerScreen::RLowerScreen.apneaPatientSetupStatus())
		{
		case LowerScreen::APNEA_NEW_PATIENT:			// $[TI7]
			// We were activated at the end of New Patient Setup mode.
			// Set Advisory prompt to "New Patient Apnea settings have been
			// auto-set! ..."
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, PromptStrs::NP_APNEA_SETTINGS_AUTO_SET_A);
			break;

		case LowerScreen::APNEA_SAME_PATIENT:			// $[TI8]
			// We were activated at the end of Same Patient Setup mode.
			// Set Advisory prompt to "Please review all values before
			// proceeding."
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, PromptStrs::SP_APNEA_SETTINGS_REVIEW_A);
			break;

		default:										//$[TI9]
			// Unknow Apnea Patient Setup status
			AUX_CLASS_ASSERTION_FAILURE(
							LowerScreen::RLowerScreen.apneaPatientSetupStatus()
									   );
			break;
		}
	}
	else
	{													// $[TI10]
		ApneaCorrectionStatus tmpCorrectionStatus = correctionStatus_;

		// OK, we may have been auto-launched after Vent Setup caused an
		// apnea setting to be corrected or an apnea setting may have been
		// corrected when we adjusted the apnea settings batch above.
		// Determine the applicable correction status (only one should be
		// set) and then decide which advisory prompt if any to display.

		if (NONE_CORRECTED == tmpCorrectionStatus)
		{												// $[TI11]
			// correctionStatus_ was empty so set tmpCorrectionStatus
			// to the apnea review status.
			tmpCorrectionStatus = LowerScreen::RLowerScreen.apneaReviewStatus();
		}
		else
		{												// $[TI12]
			// Only one status allowed to be set.
			CLASS_ASSERTION(NONE_CORRECTED ==
									LowerScreen::RLowerScreen.apneaReviewStatus());
		}

		// Now check which advisory prompt, if any, we should display
		switch (tmpCorrectionStatus)
		{
		case NONE_CORRECTED:							// $[TI13]
			// We are not in apnea review mode so no special advisory
			// prompt needed.
			break;

		case APNEA_PI_CORRECTED:						// $[TI14]
			// Advise operator that Inspiratory Pressure was auto-corrected.
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_LOW, PromptStrs::APNEA_PI_CORRECTED_A);
			break;

		case APNEA_O2_CORRECTED:						// $[TI15]
			// Advise operator that Oxygen Percentage was auto-corrected.
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_LOW, PromptStrs::APNEA_O2_CORRECTED_A);
			break;

		case BOTH_CORRECTED:							// $[TI16]
			// Advise operator that Inspiratory Pressure and Oxygen Percentage
			// was auto-corrected.
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_LOW, PromptStrs::APNEA_PI_O2_CORRECTED_A);
			break;

		default:										// $[TI17]
			// Illegal apnea review status
			CLASS_ASSERTION(FALSE);
			break;
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
// Called by LowerSubScreenArea just after this subscreen is removed from the
// display allowing us to do any necessary cleanup.  Needs no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// We clear all prompts that we may have displayed.  The Adjust Panel focus is
// released.  If Same/New Patient Setup mode is active then we inform the Lower
// Screen that we have been deactivated (this signals the end of Same/New
// Patient Setup mode).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaSetupSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("deactivateHappened_()");

	// deactivate all setting buttons...
	operateOnButtons_(arrScreen1BtnPtrs_, &SettingButton::deactivate);
	operateOnButtons_(arrScreen2BtnPtrs_, &SettingButton::deactivate);

	// If we were in Apnea Patient Setup mode then tell the Lower Screen that
	// we have been deactivated
	if (LowerScreen::RLowerScreen.apneaPatientSetupStatus() !=
												LowerScreen::APNEA_NO_SETUP)
	{													// $[TI1]
		LowerScreen::RLowerScreen.apneaSetupDeactivated();
	}
	else if (LowerScreen::RLowerScreen.apneaReviewStatus() != NONE_CORRECTED)
	{													// $[TI2]
		//
		// We are in Apnea Setup review mode, so inform the Lower Screen
		// that we have been deactivated, thus concluding the review.
		//
		LowerScreen::RLowerScreen.apneaSetupReviewed();
	}													// $[TI3]

	setApneaSetupTabTitleText_(MiscStrs::APNEA_SETUP_TAB_BUTTON_LABEL);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdateHappened_
//
//@ Interface-Description
// Called when an Apnea setting has changed.  Among other things we set
// the screen title to be "proposed" on the first such event.  Passed 
// the following parameters:
// >Von
//  transitionId The id of the type of transition happenning in this subscreen.
//	qualifierId	The context in which the change happened, either
//				ACCEPTED or ADJUSTED.
//	pSubject	The pointer to the Context subject.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// If this subscreen is not displayed we ignore the setting change.  Otherwise,
// we do the following:
//
// If it is the first change on either screen then we record the fact that the
// settings have been adjusted.  We also change the screen
// title to "Proposed" instead of "Current".  The Change Mandatory Type button
// must be hidden (if on screen 1), the operator is not allowed to change the
// mandatory type once a setting has been changed.
//
// $[01102] Button to go to control setting must be removed when ...
// $[01104] Can't go back to control setting after it's adjusted.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaSetupSubScreen::valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									     )
{
	CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");

	// If we're invisible, do nothing
	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	// An adjustment of Apnea Mandatory Type on screen 2 causes the screen
	// title to be changed.
	if (!onScreen1_)
	{													// $[TI3]
		if (transitionId == BatchSettingsSubScreen::CURRENT_TO_PROPOSED)
		{												// $[TI4]
			// Mandatory Type was changed, change the screen title.
			titleArea_.setTitle(MiscStrs::PROPOSED_APNEA_VENT_TYPE_TITLE);
			setApneaSetupTabTitleText_(MiscStrs::APNEA_PROPOSED_SETUP_TAB_BUTTON_LABEL);
		}
		else if (transitionId == BatchSettingsSubScreen::PROPOSED_TO_CURRENT)
		{												// $[TI5]
			// Mandatory type was adjusted back to its original value, revert
			// to previous display.
			titleArea_.setTitle(MiscStrs::CURRENT_APNEA_VENT_TYPE_TITLE);
			setApneaSetupTabTitleText_(MiscStrs::APNEA_CURRENT_SETUP_TAB_BUTTON_LABEL);
		}											   // $[TI11]
	}
	else if (transitionId == BatchSettingsSubScreen::CURRENT_TO_PROPOSED)
	{													// $[TI6]
		// Display proposed apnea settings screen title
		setApneaSetupTabTitleText_(MiscStrs::APNEA_PROPOSED_SETUP_TAB_BUTTON_LABEL);

		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_LOW,
							PromptStrs::APNEA_SETUP_PROCEED_CANCEL_S);

		// Clear Advisory prompt if there is one.
		if ((LowerScreen::RLowerScreen.apneaPatientSetupStatus() !=
												LowerScreen::APNEA_NO_SETUP) ||
				(LowerScreen::RLowerScreen.apneaReviewStatus() != NONE_CORRECTED) ||
				(correctionStatus_ != NONE_CORRECTED))
		{												// $[TI7]
			// Clear Advisory prompt ("All values above auto-set!").
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_LOW, NULL_STRING_ID);
		}												// $[TI8]
	}
	else if (transitionId == BatchSettingsSubScreen::PROPOSED_TO_CURRENT)
	{													// $[TI9]
		// Display current apnea settings screen title
		setApneaSetupTabTitleText_(MiscStrs::APNEA_CURRENT_SETUP_TAB_BUTTON_LABEL);

		// Reset secondary prompt to "To cancel, touch APNEA"
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_LOW, PromptStrs::APNEA_SETUP_CANCEL_S);
	}													// $[TI10]

	if (transitionId != BatchSettingsSubScreen::NO_TRANSITION)
	{	// $[TI12]
		// a transition has occurred, only show this button if no settings
		// are currently changed...
		changeMandTypeButton_.setShow(onScreen1_  &&  areSettingsCurrent_());
	}	// $[TI13]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setApneaSetupTabTitleText_
//
//@ Interface-Description
//  Sets the title on the apnea setup subscreen select tab button.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the LowerScreenSelectArea method to set the button's title.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
ApneaSetupSubScreen::setApneaSetupTabTitleText_( StringId stringId )
{
    static LowerScreenSelectArea * PScreenSelectArea_
         = LowerScreen::RLowerScreen.getLowerScreenSelectArea();
 
	// $[TI1]
    PScreenSelectArea_->setApneaSetupTabTitleText( stringId );
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ApneaSetupSubScreen::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, APNEASETUPSUBSCREEN,
									lineNumber, pFileName, pPredicate);
}
