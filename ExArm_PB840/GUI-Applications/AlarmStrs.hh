
#ifndef AlarmStrs_HH
#define AlarmStrs_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AlarmStrs - Messages class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmStrs.hhv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:  sah    Date: 15-Jan-1998        DR Number:  5004
//   Project:   Sigma   (R8027)
//   Description:
//       Moved from Alarm-Analysis subsystem, and renamed to 'AlarmStrs',
//       from 'AlarmMessages'.  Also, moved 'TextInfoStruct' inside of the
//       class, and renamed it to 'AlarmInfo'.
//
//   Revision 002   By:   hhd      Date: 08/26/97         DR Number:   2310
//      Project:   Sigma   (R8O27)
//      Description:
//  	   Changed TextInfo_ structure and query method.  
//		   Added InitAlarmTextInfo().
//
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8O27)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================

#include "GuiAppClassIds.hh"
#include "Sigma.hh"

//@ Usage-Classes

#include "MessageName.hh"

//@ End-Usage


class AlarmStrs
{
  public:
	  // maximum number of languages to size AlarmInfo pText
	  enum { LANGUAGE_COUNT = 10 };
	//@ Type:  AlarmInfo
	// Structure used to hold Alarm message info.
	struct AlarmInfo
	{
	   Uint   messageName;	// ID of Alarm message.
	    
	   wchar_t * pText[LANGUAGE_COUNT];		// Textual description of Alarm message. 
	    
	};

    static void  Initialize(void);

	 
    static inline const wchar_t*  GetMessage(const MessageName name);
	 
    static void SoftFault(const SoftFaultID softFaultID,
					      const Uint32      lineNumber,
					      const char*       pFileName  = NULL, 
					      const char*       pPredicate = NULL);

  private:
    AlarmStrs(void);                    // Declared only
    ~AlarmStrs(void);                   // Declared only
    AlarmStrs(const AlarmStrs&);    // Declared only
    void   operator=(const AlarmStrs&); // Declared only

    //@ Data-Member:     TextInfo_[]
    // TextInfo_ is a static array of structures which hold message ids 
    // and their corresponding alarm messages.
    static AlarmStrs::AlarmInfo  TextInfo_[];
};


// Inlined methods
#include "AlarmStrs.in"


#endif // AlarmStrs_HH 
