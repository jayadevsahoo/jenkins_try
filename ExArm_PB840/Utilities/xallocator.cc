#include "stdafx.h"
//------------------------------------------------------------------------------
//                   Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//	@file xallocator.cc
//	
//	This class is a C compatible version of a fixed block allocator. 
//
//	You do not need to know the size of the blocks, the maximum size, nor the
//	quantity of blocks being requested in advance. The algorithm handles everything 
//	dynamically. xallocator is thread-safe.
//
// 	The xallocator defines three C APIs used just like their CRT equivalent:
//
//	- xalloc
//	- xfree
//	- xrealloc
//
//  The fixed block sizes created are a power of two - 4, 8, 16, 32, 64, ...
// 	xallocator relies upon the Allocator class for fixed block allocations.
//	Each Allocator class instance handle one size fixed block. At runtime, 
//	if an allocator is not available for a particular fixed size block one is
//	created dynamically. The number of blocks handled by each Allocator instance
//	is also dynamic. As blocks are requested new ones are created on the heap as
//	as necessary. Freed blocks are not returned to the heap but instead saved in 
//	a list for reuse later by another block of the same size. Therefore, blocks 
//	are created on the heap but never returned with delete or free. 
//
//  The algorithm needs to know the size of the block when xfree is called. xfree()
//	does not have a size argument but instead is just a raw void* pointer. 
//	Therefore, upon creation of the block in xalloc() an extra 4 bytes is added
//	to the size requested by the client. In these 4 bytes is the size of the 
//	block created. When xfree is called with a void* the original size of the 
//	block can be extracted and the correct fixed block allocator Deallocate() 
//	function called.
//------------------------------------------------------------------------------
#include <tchar.h>
#include "xallocator.hh"
#include "allocator.hh"
#include "pmap.hh"
#include "UtilityClassId.hh"
#include "FaultHandlerMacros.hh"
#include <Windows.h>

// map a fixed block allocator size to an instance of Allocator
static pmap<size_t, Allocator*>* _allocatorMap = NULL;
static Boolean mutex_initialized = FALSE;
static HANDLE mutex;

namespace OutOfMemory
{
	//--------------------------------------------------------------------------
	///  This method is called when a software fault is detected by the
	///  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
	///  and 'lineNumber' are essential pieces of information.  The
	/// 'pFileName' and 'pPredicate' strings may be defaulted in the macro
	/// to reduce code space.
	///
	/// @param[in] SoftFaultID The ID of the softFault.
	/// @param[in] lineNumber The line number of the file.
	/// @param[in] pFileName The filename string.
	/// @param[in] pPredicate The boolean expression causing the fault
	/// @param[out] none.
	/// @return     none.
	///
	/// @pre  none.
	/// @post A software fault is reported.
	//--------------------------------------------------------------------------
	static void SoftFault(const SoftFaultID  softFaultID,
			const Uint32       lineNumber,
			const Char*        pFileName = NULL,
			const Char*        pPredicate = NULL)
	{
		FaultHandler::SoftFault(softFaultID, TOOLS,
				XALLOCATOR, lineNumber, pFileName, pPredicate);
	}
}

using namespace OutOfMemory;

//------------------------------------------------------------------------------
/// Takes an unsigned T type as the template argument and returns the next 
///	higher power of two value. For instance, pass in 28 and the next higher
/// power of two would be 32. 
/// @param[in]	k - numeric value to compute the next higher power of two.
/// @return		The next higher power of two based on the input k. 
/// @pre        none
/// @post       none
//------------------------------------------------------------------------------
template <class T>
T nexthigher(T k) 
{
	if (k == 0)
		return 1;
	k--;
	for (unsigned int i=1; i<sizeof(T)*CHAR_BIT; i<<=1)
		k = k | k >> i;
	return k+1;
}

//------------------------------------------------------------------------------
/// Called exactly one time to create the xallocator mutex.
/// @pre        None
/// @post       None
//------------------------------------------------------------------------------
static void mutex_init()
{
    mutex = CreateMutex(NULL, FALSE, _T("xallocator"));
    CLASS_ASSERTION(mutex != NULL);

    mutex_initialized = TRUE;
}

//------------------------------------------------------------------------------
/// Call to lock the mutex.
/// @pre        None
/// @post       None
//------------------------------------------------------------------------------
static void mutex_lock()
{
    // if mutex is not initialized we're still single threaded
    if (mutex_initialized == FALSE)
    	return;

    DWORD status = WaitForSingleObject(mutex, INFINITE);
    CLASS_ASSERTION(status == WAIT_OBJECT_0);
}

//------------------------------------------------------------------------------
/// Call to unlock the mutex.
/// @pre        None
/// @post       None
//------------------------------------------------------------------------------
static void mutex_unlock()
{
    // if mutex is not initialized we're still single threaded
    if (mutex_initialized == FALSE)
    	return;

    BOOL status = ReleaseMutex(mutex);
    CLASS_ASSERTION(status);
}

//------------------------------------------------------------------------------
/// Stored the size of the memory block within the block itself and returns
///	a pointer to the client's area within the block.
/// @param[in]	block - a pointer to the raw memory block. 
///	@param[in]	size - the client requested size of the memory block.
/// @return		A pointer to the client's address within the raw memory block. 
/// @pre        none
/// @post       none
//------------------------------------------------------------------------------
static inline void *_setBlockSize(void* block, size_t size)
{
	// cast the raw block memory to a size_t pointer
	size_t* pSizeInMemory = static_cast<size_t*>(block);

	// write the size into the memory block
	*pSizeInMemory = size;

	// advance the pointer past the size_t block size and return a pointer to
	// the client's memory region
	return ++pSizeInMemory;
}

//------------------------------------------------------------------------------
/// Gets the size of the memory block stored within the block.
/// @param[in]	block - a pointer to the client's memory block. 
/// @return		The original client requested size of the memory block (*not*
/// 	the actual block size returned by the fixed block allocator).
/// @pre        none
/// @post       none
//------------------------------------------------------------------------------
static inline size_t _getBlockSize(void* block)
{
	// cast the client memory to a size_t pointer
	size_t* pSizeInMemory = static_cast<size_t*>(block);

	// back up one size_t position to get the stored block size
	pSizeInMemory--;

	// return the block size stored within the memory block
	return *pSizeInMemory;
}

//------------------------------------------------------------------------------
/// Returns the raw memory block pointer given a client memory pointer. 
/// @param[in]	block - a pointer to the client memory block. 
/// @return		A pointer to the original raw memory block address. 
/// @pre        none
/// @post       none
//------------------------------------------------------------------------------
static inline void *_getBlockPtr(void* block)
{
	// cast the client memory to a size_t pointer
	size_t* pSizeInMemory = static_cast<size_t*>(block);

	// back up one size_t position and return the original raw memory block pointer
	return --pSizeInMemory;
}

//------------------------------------------------------------------------------
/// This function must be called exactly one time *before* the operating system
/// threading starts. When the system is still single threaded at startup,
/// the xallocator API does not need mutex protection.
/// @pre        None
/// @post       None
//------------------------------------------------------------------------------
extern "C" void xallocator_init()
{
	Allocator_Init();

	mutex_init();

	CLASS_ASSERTION(_allocatorMap == NULL);
	_allocatorMap = new pmap<size_t, Allocator*>;
}

//------------------------------------------------------------------------------
/// Get an Allocator instance based upon the client's requested block size.
/// If a Allocator instance is not currently available to handle the size,
///	then a new Allocator instance is create.
///	@param[in]	size - the client's requested block size.
///	@return 	An Allocator instance that handles blocks of the requested
///		size.
/// @pre        None
/// @post       None
//------------------------------------------------------------------------------
extern "C" Allocator* xallocator_get_allocator(size_t size)
{
	CLASS_ASSERTION(_allocatorMap != NULL);

	// Based on the size, find the next higher power of two value.
	// Add sizeof(size_t) to the requested block size to hold the size
	// within the block memory region.
	size_t powerOfTwoSize = nexthigher<size_t>(size + sizeof(size_t));

	Allocator* allocator = NULL;

	// find an existing allocator that handles this power of two size
	pmap<size_t, Allocator*>::iterator it;

	mutex_lock();
	it = _allocatorMap->find(powerOfTwoSize);

	// if there is not an allocator already created to handle this block size
	if (it == _allocatorMap->end())  
	{
		// create a new allocator to handle blocks of the size required
		allocator = new Allocator(powerOfTwoSize, 0, "xallocator");

		// add the allocator to the map
		std::pair< pmap<size_t, Allocator*>::iterator, bool > ret;

		ret = _allocatorMap->insert(std::pair<size_t, Allocator*>(powerOfTwoSize, allocator));
		mutex_unlock();
		CLASS_ASSERTION(ret.second == true);
	}
	else
	{
		mutex_unlock();

		// the correct allocator block size already exists, so use it
		allocator = (*it).second;
	}

	return allocator;
}

//------------------------------------------------------------------------------
/// Allocates a memory block of the requested size. The blocks are created from
///	the fixed block allocators.
///	@param[in]	size - the client requested size of the block.
/// @return		A pointer to the client's memory block.
/// @pre        none
/// @post       none
//------------------------------------------------------------------------------
extern "C" void *xalloc(size_t size)
{
	// get an allocator instance to create a memory block
	Allocator* allocator = xallocator_get_allocator(size);

	// allocate a raw memory block 
	void* blockMemoryPtr = allocator->Allocate(size);

	// set the block size within the raw memory block region
	void* clientsMemoryPtr = _setBlockSize(blockMemoryPtr, size);
	return clientsMemoryPtr;
}

//------------------------------------------------------------------------------
/// Frees a memory block previously allocated with xalloc. The blocks are returned
///	to the fixed block allocator that originally created it.
///	@param[in]	ptr - a pointer to a block created with xalloc.
/// @pre        none
/// @post       none
//-------------------------	-----------------------------------------------------
extern "C" void xfree(void* ptr)
{
	if (ptr == 0)
		return;

	// extract the original client requested block size from the caller's block pointer
	size_t size = _getBlockSize(ptr);

	// convert the a client pointer into the original raw block pointer
	void* blockPtr = _getBlockPtr(ptr);

	// deallocate the block 
	Allocator* allocator = xallocator_get_allocator(size);
	allocator->Deallocate(blockPtr);
}

//------------------------------------------------------------------------------
/// Reallocates a memory block previously allocated with xalloc.
///	@param[in]	ptr - a pointer to a block created with xalloc.
///	@param[in]	size - the client requested block size to create.
/// @return		A pointer to the newly created block.
/// @pre        none
/// @post       none
//------------------------------------------------------------------------------
extern "C" void *xrealloc(void *oldMem, size_t size)
{
	if (oldMem == 0)
		return xalloc(size);

	if (size == 0) 
	{
		xfree(oldMem);
		return 0;
	}
	else 
	{
		// create a new memory block
		void* newMem = xalloc(size);
		if (newMem != 0) 
		{
			// get the original client requested size of the old memory block
			size_t oldSize = _getBlockSize(oldMem);

			// copy the bytes from the old memory block into the new (as much as will fit)
			memcpy(newMem, oldMem, (oldSize < size) ? oldSize : size);

			// free the old memory block
			xfree(oldMem);

			// return the client pointer to the new memory block
			return newMem;
		}
		return 0;
	}
}
