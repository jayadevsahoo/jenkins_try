#ifndef DiscreteSetting_HH
#define DiscreteSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  DiscreteSetting - Discrete Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DiscreteSetting.hhv   25.0.4.0   19 Nov 2013 14:27:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 008   By: gdc   Date:  31-May-2007    SCR Number: 6237 
//  Project:  Trend
//  Description:
//     Moved TOTAL_NUM_VALUES_ from private to protected.
//     This allows the derived classes gain access to this member variable.
//     (i.e Trend Select Setting)
//
//  Revision: 007   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  eliminated private 'isEnabledValue_()', now defined higher
//
//  Revision: 006   By: dosman    Date:  26-Jan-1999    DR Number: 5187
//  Project:  BILEVEL
//  Description:
//	Added arrays to store the bounds hit by dependent settings:
//	(both the bound ids and the bound values of the dependents).
//
//  Revision: 005   By: dosman    Date:  30-Apr-1998    DR Number: 4
//  Project:  BILEVEL
//  Description:
//	Removed totalNumAccessibleValues_.
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: dosman    Date:  05-Feb-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial version for bilevel.
//	added totalNumAccessibleValues_
//	as a way to be able to control the total number
//	of discrete values to be displayed so that
//	we can leave out the last setting value if we want;
//	this value is initialized to the same value as
//	the constant TOTAL_NUM_VALUES_, but it is non constant
//	and can be set to a smaller number than TOTAL_NUM_VALUES_
//	or even reset to TOTAL_NUM_VALUES_ as desired.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5092
//  Project:  COLOR
//  Description:
// 	--- Merged Rev "Color" (1.17.1.0) into Rev "BiLevel" (1.17.2.0)
//	Added ability of discrete settings to activate, via constructor
//	parameters, bounds for this setting.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "Setting.hh"
//@ End-Usage

struct DiscreteParameterItem
{
	const char* name;
	DiscreteValue  value;
};


class DiscreteSetting : public Setting 
{
  public:
    virtual ~DiscreteSetting(void);

    virtual const BoundStatus&  calcNewValue(const Int16 knobDelta);

    virtual void  calcTransition(const DiscreteValue newValue,
				 const DiscreteValue currValue) = 0;

		virtual SigmaStatus ventset(const char* requestedValue);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    //@ Constant:  MAX_DEPENDENT_VALUES_
    //
		enum
		{
			MAX_DEPENDENT_VALUES_ = 5
		};

    DiscreteSetting(const SettingId::SettingIdType  discreteId,
		    const SettingId::SettingIdType* arrDependentSettingIds,
                    const Uint                      totalNumValues,
		    const Boolean                   useNonWrappingMode,
		    const SettingBoundId               upperLimitBoundId,
		    const SettingBoundId               lowerLimitBoundId);

    virtual Boolean  calcDependentValues_(const Boolean isValueIncreasing = TRUE);

    virtual Boolean  resolveDependentChange_(
			const SettingId::SettingIdType dependentId,
			const Boolean                  isValueIncreasing = TRUE
					    );

    virtual void  processBoundViolation_(void);

		void setLookupTable(const DiscreteParameterItem* pLookupTable);

    //@ Data-Member:  arrDependentBoundIds_
    // array of setting bound Ids, describing for each dependent of this setting,
    // what, if any, bound the dependent hit when trying to change in response to this
    // primary setting changing.
    SettingBoundId arrDependentBoundIds_[MAX_DEPENDENT_VALUES_];
	 
    //@ Data-Member:  arrDependentValues_
    // array to hold dependent setting values:
    // This array is used to hold values of dependent settings that hit a bound
    // during the time the dependents are accepting the change of this primary setting.
    // The user of this array should not assume that the values in this array
    // always contain the dependent settings' current values. 
    // This array is a convenient location to store the dependent setting values,
    // and to retrieve them, while accepting the primary setting's change.
    SettingValue arrDependentValues_[MAX_DEPENDENT_VALUES_];

    //@ Constant:  TOTAL_NUM_VALUES_
    // The total number of values for the discrete setting 
    const Uint  TOTAL_NUM_VALUES_;

#if defined(SIGMA_DEVELOPMENT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_DEVELOPMENT)

  private:
    DiscreteSetting(const DiscreteSetting&);	// not implemented...
    DiscreteSetting(void);                      // not implemented...
    void  operator=(const DiscreteSetting&);	// not implemented...

		SigmaStatus lookupValue_(const char* pName, DiscreteValue& value);

		const char* lookupName_(DiscreteValue value);

    //@ Constant:  IS_IN_NON_WRAPPING_MODE_
    // Boolean constant that indicates whether this instance's values
    // are to be cycled through, or bounded.
    const Boolean  IS_IN_NON_WRAPPING_MODE_;

    //@ Constant:  UPPER_LIMIT_BOUND_ID_
    // Constant that stores this instance's upper limit bound id, if any.
    const SettingBoundId  UPPER_LIMIT_BOUND_ID_;

    //@ Constant:  LOWER_LIMIT_BOUND_ID_
    // Constant that stores this instance's lower limit bound id, if any.
    const SettingBoundId  LOWER_LIMIT_BOUND_ID_;

		//@ Data-Member:  pLookupTable_
		// Pointer to the parameter lookup table for the ventset command interface
		const DiscreteParameterItem*   pLookupTable_;
};


#endif // DiscreteSetting_HH 
