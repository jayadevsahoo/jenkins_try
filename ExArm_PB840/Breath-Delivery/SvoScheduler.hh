
#ifndef SvoScheduler_HH
#define SvoScheduler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SvoScheduler - Safety Valve Open scheduler
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SvoScheduler.hhv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 003 By: syw   Date:   06-Mar-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "BreathPhaseScheduler.hh"

//@ Usage-Classes

class BreathTrigger;
class ModeTrigger;

//@ End-Usage


class SvoScheduler: public BreathPhaseScheduler
{
  public:
    SvoScheduler(void);
    virtual ~SvoScheduler(void);

    static void SoftFault(const SoftFaultID softFaultID,
  const Uint32      lineNumber,
  const char*       pFileName  = NULL, 
  const char*       pPredicate = NULL);
  
    virtual void determineBreathPhase(const BreathTrigger& breathTrigger);
    virtual void relinquishControl(const ModeTrigger& modeTrigger);
    virtual void takeControl(const BreathPhaseScheduler& scheduler);

  protected:
	virtual EventData::EventStatus reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus);
	virtual void enableTriggers_();

  private:
    SvoScheduler(const SvoScheduler&);		// not implemented...
    void   operator=(const SvoScheduler&);	// not implemented...

};


#endif // SvoScheduler_HH 
