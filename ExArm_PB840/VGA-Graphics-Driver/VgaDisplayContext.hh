#ifndef VgaDisplayContext_HH
#define VgaDisplayContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VgaDisplayContext -- Clipping for VGA library.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaDisplayContext.hhv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: rpr     Date: 11-Mar-2011     SCR Number: 6752
//  Project: XENA2
//      ExpandFont and ExpandIcon passes the original witdth of the font/bitmap
//		so clipping can be accounted for properly.
// 
//  Revision: 006   By: mnr    Date: 24-Sep-2010    SCR Number: 6672
//  Project: XENA2
//  Description:
//      New method expandIcon() added.
//
//  Revision: 005   By: gdc    Date:  29-Jan-2007    SCR Number: 6237
//  Project: Trend
//  Added off-screen image storage/recovery for cursor movement in 
//  emulated overlay plane used to implement the trend cursor line.
//
//  Revision: 004   By: erm    Date:  15-Oct-2001    DCS Number: 5959
//  Project: GUIComms
//  Description:
//  Code CleanUp
//
//  Revision: 003   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GUIComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 002    By: hct   Date: 28-MAR-2000     DCS Number: 5493
//  Project: GuiComms
//  Description:
//  Add GuiComms functionality.
//
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//====================================================================

#include "VgaGraphicsDriver.hh"
#include "DisplayContext.hh"
#include "VgaDisplayId.hh"

//@ Usage-Classes
#include "RgbQuad.hh"
//@ End-Usage


class VgaDisplayContext : public DisplayContext
{
  public:
    enum ColorMapFlag
	{
	  CM_READ_ONLY = 1,
	  CM_ALLOCATED = 2,
	};

	VgaDisplayContext( VgaDisplayId displayId, Int frame = 0);

	~VgaDisplayContext();

	virtual void lock(void);
	virtual void unlock(void);
	virtual void accessDisplay(void) const;
	
	virtual void fillSolid(
					Int xDest, Int yDest, Int xWidth, Int yHeight, Int color);

	virtual void expandFont(Int xDest, Int yDest, Int xWidth, Int yHeight, 
					const Byte * pFontData, Uint fontOffset, Int color, Int orignalWidth);

	virtual void expandIcon(Int xDest, Int yDest, Int xWidth, Int yHeight, 
					const Byte * pFontData, Uint fontOffset, Int color, Int orignalWidth);

	virtual void moveBlock(Int xSrc, Int ySrc, Int xDest, 
					Int yDest, Int xWidth, Int yHeight);

	virtual void *allocateImage(Uint size);

	virtual void freeImage(void *pBlock);

	virtual void storeImage(void *pDest, Int xSrc, Int ySrc, Int xWidth, Int yHeight);

	virtual void recoverImage(Int xDest, Int yDest, const void *pSrc, Int xWidth, Int yHeight);

	virtual void allocateColor(Byte & index, const RgbQuad & rgb );

	virtual void storeColor(Byte index, const RgbQuad & rgb, 
					Boolean readOnly=TRUE);

	virtual void getColor(Byte index, RgbQuad & rgb);

	virtual void showFrame(void);

    virtual void  drawPoint(
					GraphicsContext & rGc, Int x, Int y);

    virtual void  drawLine(
					GraphicsContext & rGc,
					Int x0, Int y0, Int x1, Int y1);

    virtual void  fillRectangle(
					GraphicsContext & rGc,
					Int x0, Int y0, Int x1, Int y1 );

    virtual void  drawTriangle(
					GraphicsContext & rGc,
					Int x0, Int y0,
					Int x1, Int y1,
					Int x2, Int y2 );

    virtual void  fillTriangle(
					GraphicsContext & rGc,
					Int x0, Int y0,
					Int x1, Int y1,
					Int x2, Int y2 );

    virtual void  drawBitmap(
					GraphicsContext & rGc,
					DibInfo & rDibInfo,
			        Int xPos, Int yPos );
 
    virtual void  drawText(
					GraphicsContext & rGc,
					Int xPos, Int yPos, Uint fontId, const wchar_t* pString);

    virtual void  drawJapaneseText(
					GraphicsContext & rGc,
					Int xPos, Int yPos, Uint fontId, const char* pString);

	virtual void setTextColor(RgbQuad& color);
	virtual CDC* getCDC(void) const;
	virtual void setCDC(CDC*);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
	VgaDisplayContext(); //declared only
    VgaDisplayContext(const VgaDisplayContext&); // declared only
	void operator=(const VgaDisplayContext&);    // declared only

    Boolean isSameVideo_( const RgbQuad & rgb, const RgbQuad & key ) const;

	// overlay of off-screen memory
	struct OffScreenMemory
	{
		Byte offScreenImage[1000];	// for off-screen image storage
		Byte scratchPad[36*36];		// for 36 * 36 icons
		Byte fontData[1];			// start of off-screen font storage
	};

	enum
	{
	  SCRATCH_PAD_SIZE_ = 288   // for 36 * 36 icons
	};

    //@ Data-Member:  isOffScreenImageAllocated_
    // TRUE if the off-screen image is allocated, FALSE when free
    Boolean isOffScreenImageAllocated_;

    //@ Data-Member:  pOffScreenMemory_
    // pointer to VGA memory located above the display frame memory area(s)
    OffScreenMemory *pOffScreenMemory_;

	//@ Data-Member:  pCDC_
	//  Pointer to Windows DC.
	CDC* pCDC_;

};

#endif // VgaDisplayContext_HH 
