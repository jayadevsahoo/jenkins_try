#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NovRamDiscrete - NOVRAM Discrete Value.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a means of storing Discrete Setting values in
//  NOVRAM.  What this class provides that the Settings' discrete value
//  does not provide, is a set of discrete values that are represented by
//  bit patterns -- as opposed to '0' thru N-1.
//
//  This use of bit patterns provides protection against "lost" bytes
//  due to power-down and power-up sequences.  If a byte is "lost",
//  it will be detected via the bit patterns.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a safe mechanism for the storing of Discrete
//  Setting values in NOVRAM.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamDiscrete.ccv   25.0.4.0   19 Nov 2013 14:18:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Added calling of 'NovRamManager::ReportAuxillaryInfo()' to
//	initialization and verification methods.  Also, added new 'isValid_()'
//	method.
//
//  Revision: 002  By:  sah    Date:  24-Oct-1997    DCS Number: 2584
//  Project:  Sigma (R8027)
//  Description:
//	Modified 'verifySelf()' and 'getValue()' to remove any multiple-thread
//	contention.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "NovRamDiscrete.hh"

//@ Usage-Classes
#include "NovRamManager.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

// this allows up to 6 legal discrete values (i.e., 0 through 5)...
const Uint  NovRamDiscrete::ARR_DISCRETE_PATTERN_VALUES_[] =
{
  0xa55aa55a, 0x5aa55aa5, 0x55aaaa55, 0xaa5555aa, 0x5555aaaa, 0xaaaa5555
};

const Uint  NovRamDiscrete::MAX_DISCRETE_VALUE_ =
  (sizeof(NovRamDiscrete::ARR_DISCRETE_PATTERN_VALUES_) / sizeof(Uint)) - 1;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  NovRamDiscrete()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default instance of this class.  The default value of
//  this discrete class is '0'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//  (getValue()   == 0)
//@ End-Method 
//===================================================================== 

NovRamDiscrete::NovRamDiscrete(void)
	  : discreteValue_(NovRamDiscrete::ARR_DISCRETE_PATTERN_VALUES_[0])
{
  CALL_TRACE("NovRamDiscrete()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~NovRamDiscrete()  [Destructor]
//
//@ Interface-Description
//  Destroy this instance.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

NovRamDiscrete::~NovRamDiscrete(void)
{
  CALL_TRACE("~NovRamDiscrete()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize()  [const]
//
//@ Interface-Description
//  A reset sequence occurred, therefore verify the integrity of this
//  instance.  If this value is determined to be valid,
//  'NovRamStatus::ITEM_VERIFIED' is returned.  If this log is not valid
//  this item is fully initialized (from scratch) and
//  'NovRamStatus::ITEM_INITIALIZED' is returned.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method 
//===================================================================== 

NovRamStatus::InitializationId
NovRamDiscrete::initialize(void)
{
  CALL_TRACE("initialize()");

  NovRamStatus::InitializationId  initStatusId;

  if (!isValid_())
  {   // $[TI1]
    // initiate a full initialization of this instance...
    new (this) NovRamDiscrete();

    initStatusId = NovRamStatus::ITEM_INITIALIZED;

    // report failure location...
    NovRamManager::ReportAuxillaryInfo(this, sizeof(NovRamDiscrete),
				       initStatusId);
  }
  else
  {   // $[TI2] -- a full initialization is NOT needed...
    // item is valid, as is...
    initStatusId = NovRamStatus::ITEM_VERIFIED;
  }

  // this should ALWAYS be true...
  SAFE_CLASS_POST_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID),
  			    NovRamDiscrete);

  return(initStatusId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifySelf()  [const]
//
//@ Interface-Description
//  Verify that this instance's value is a legal value.  If this instance
//  is determined to be valid, 'NovRamStatus::ITEM_VALID' is returned.
//  Otherwise, 'NovRamStatus::ITEM_INVALID' is returned.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

NovRamStatus::VerificationId
NovRamDiscrete::verifySelf(void) const
{
  CALL_TRACE("verifySelf()");

  NovRamStatus::VerificationId  verifStatusId;

  if (!isValid_())
  {   // $[TI1]
    verifStatusId = NovRamStatus::ITEM_INVALID;

    NovRamManager::ReportAuxillaryInfo(this, sizeof(NovRamDiscrete));
  }
  else
  {   // $[TI2]
    verifStatusId = NovRamStatus::ITEM_VALID;
  }

  return(verifStatusId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getValue()  [const]
//
//@ Interface-Description
//  Return the stored discrete value.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Convert the stored discrete pattern to a discrete value.
//--------------------------------------------------------------------- 
//@ PreCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//---------------------------------------------------------------------
//@ PostCondition 
//  (getValue() < maxDiscreteValue_)
//@ End-Method 
//===================================================================== 

DiscreteValue
NovRamDiscrete::getValue(void) const
{
  CALL_TRACE("getValue()");

  // must be stored locally to avoid multiple-thread contention...
  const Uint  CURR_VALUE = discreteValue_;

  Uint idx;
  for (idx = 0u; idx < NovRamDiscrete::MAX_DISCRETE_VALUE_; idx++)
  {   // $[TI1] -- ALWAYS executes this path...
    if (CURR_VALUE == NovRamDiscrete::ARR_DISCRETE_PATTERN_VALUES_[idx])
    {   // $[TI1.1] -- a match is found...
      break;  // break out of this loop...
    }   // $[TI1.2] -- a match is NOT found...
  }

  // make sure we found a match...
  CLASS_ASSERTION((idx < NovRamDiscrete::MAX_DISCRETE_VALUE_));

  return((DiscreteValue)idx);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateValue(newValue)  [const]
//
//@ Interface-Description
//  Update this instance's value, using 'newValue'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Convert 'newValue' to a boolean pattern value.
//--------------------------------------------------------------------- 
//@ PreCondition
//  (newValue < NovRamDiscrete::MAX_DISCRETE_VALUE_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method 
//===================================================================== 

void
NovRamDiscrete::updateValue(const DiscreteValue newValue)
{
  CALL_TRACE("updateValue(newValue)");
  CLASS_PRE_CONDITION((newValue < NovRamDiscrete::MAX_DISCRETE_VALUE_));

  discreteValue_ = NovRamDiscrete::ARR_DISCRETE_PATTERN_VALUES_[newValue];
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamDiscrete::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, NOV_RAM_DISCRETE,
                          lineNumber, pFileName, pPredicate);
}


//===================================================================== 
//
//  Private Methods...
//
//===================================================================== 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isValid_()  [const]
//
//@ Interface-Description
//  Return a boolean indicating whether this instance's internal values
//  are in a valid state, or not.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

inline Boolean
NovRamDiscrete::isValid_(void) const
{
  CALL_TRACE("isValid_()");

  // must be stored locally to avoid multiple-thread contention...
  const Uint  CURR_VALUE = discreteValue_;

  Boolean  isFound = FALSE;

  for (Uint idx = 0u; !isFound  &&  idx < NovRamDiscrete::MAX_DISCRETE_VALUE_;
       idx++)
  {   // $[TI1] -- ALWAYS executes this path...
    isFound = (CURR_VALUE == NovRamDiscrete::ARR_DISCRETE_PATTERN_VALUES_[idx]);
  }

  return(isFound);
}   // $[TI2] (TRUE)  $[TI3] (FALSE)...
