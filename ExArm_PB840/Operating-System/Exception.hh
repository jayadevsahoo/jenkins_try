#ifndef  Exception_HH
#define  Exception_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 
//====================================================================
// Class:  Exception - Sigma Exception Handler Structures
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/Exception.hhv   25.0.4.0   19 Nov 2013 14:16:18   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//====================================================================
 
#include "Sigma.hh"
#include "Post.hh"

//@ Usage-Classes
//@ End-Usage

struct StackFrameFormat2and3
{
   Byte    * effectiveAddress;
};

struct StackFrameFormat7
{
   Byte    * effectiveAddress;
   Uint16    specialStatusRegister;
   Uint16    writeBack3Status;
   Uint16    writeBack2Status;
   Uint16    writeBack1Status;
   Byte    * faultAddress;
   Byte    * writeBack3Address;
   Uint32    writeBack3Data;
   Byte    * writeBack2Address;
   Uint32    writeBack2Data;
   Byte    * writeBack1Address;
   Uint32    writeBack1Data;
   Uint32    pushDataLw1;
   Uint32    pushDataLw2;
   Uint32    pushDataLw3;
};


struct ExceptionStackFrame
{
   Uint32   tc;
   Uint32   itt0;
   Uint32   itt1;
   Uint32   dtt0;
   Uint32   dtt1;
   Uint32   mmusr;
   Uint32   urp;
   Uint32   srp;

   Uint32   sfc;
   Uint32   dfc;
   Uint32   vbr;
   Uint32   cacr;
   Uint32   msp;
   Uint32   isp;
   Uint32   usp;

   Uint32   d[8];
   Uint32   a[8];

   Uint16   sr;
   Uint16   pcHigh;
   Uint16   pcLow;
   Uint16   formatAndVectorOffset;

   union
   {
      StackFrameFormat2and3  format2Or3;
      StackFrameFormat7      format7;

   } frame;

};


struct Level7StackFrame
{
   Uint16   nmiSource;

};


//  public inheritance used to access protected member functions
//  from Post subsystem

class Exception : public Post
{
  public:
    static void InstallHandlers(void);
    static void Decode(const ExceptionStackFrame * pFrame);
    static void DecodeAutovector7(const Byte nmiSource);
    static void PowerFail(void);

    static const char * GetVectorName(const Uint vectorNumber);

    static void GetNmiMessage(  const Uint   nmiSource
                              , const Uint   errorCode
                              , const Uint   cpuFlag
                              , char * buffer1
                              , const Uint   bufferLength1
                              , char * buffer2
                              , const Uint   bufferLength2);

  private:
    Exception();       // not implemented
    ~Exception();      // not implemented

};

#endif   // Exception_HH
