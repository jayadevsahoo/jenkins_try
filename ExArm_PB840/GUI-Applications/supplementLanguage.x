#!/usr/bin/csh -f

# This script is called from the GUI-Applications Makefile. It is used to
# supplement each language file (.LA) with untranslated english, producing
# a file of the format .IN. The .IN file will contain both the translated
# and untranslated strings.

# If it is an english version being built, there is no supplementing 
# necessary. All we need to do to create the .IN files is to copy them from 
# the .LA files.
if ( ($1 == ENGLISH) || ($1 == ENGLISH_US) || ($1 == ENGLISH_NON_US) ) then
  cp $2 ${2:r}.IN
  exit
endif

# *AlarmStrs.IN and *HelpStrs.LA do not need to be supplemented with english. 
if ( (`ls $2 | grep 'Alarm'` == $2) || (`ls $2 | grep 'Help'` == $2) ) then
  cp $2 ${2:r}.IN
  exit 
else if (`ls $2 | grep 'Misc'` == $2) then
  set ENGLISH_FILE="EnglishMiscStrs.LA"
else if (`ls $2 | grep 'Prompt'` == $2) then
  set ENGLISH_FILE="EnglishPromptStrs.LA"
else
  echo "$2 is an invalid file, it must be of type *Strs.LA"
  exit 1 
endif

set PPID=$$

# Build a list of sorted english symbols.
nawk -f extractLineWithSymbolName.awk ${ENGLISH_FILE} > englishWithSymbolNameLines$PPID.tmp 
nawk -f extractSymbolName.awk englishWithSymbolNameLines$PPID.tmp > englishWithSymbolName$PPID.tmp
sort englishWithSymbolName$PPID.tmp > englishWithSymbolNameSorted$PPID.tmp

# Build a list of sorted language symbols.
nawk -f extractLineWithSymbolName.awk $2 > languageWithSymbolNameLines$PPID.tmp
nawk -f extractSymbolName.awk languageWithSymbolNameLines$PPID.tmp > languageWithSymbolName$PPID.tmp
sort languageWithSymbolName$PPID.tmp > languageWithSymbolNameSorted$PPID.tmp

# comm -3 extracts the lines (symbols) that are in common between the two files. 
comm -3 englishWithSymbolNameSorted$PPID.tmp languageWithSymbolNameSorted$PPID.tmp > untranslatedSymbols$PPID.tmp

# Append a blank space to end of each symbol. This helps to uniquely identify
# each symbol allowing us to differenciate between (e.g.) EST_CANCEL and
# EST_CANCEL_TEST.
nawk -f appendBlankSpace.awk untranslatedSymbols$PPID.tmp > untranslatedSymbolsBS$PPID.tmp

# Remove all comments from the english file. This is then used to build an awk array
# of records from which the untranslated symbol and cheaptext is extracted.
nawk -f stripComments.awk ${ENGLISH_FILE} > englishWithCommentsStripped$PPID.tmp
nawk -f buildArrayOfRecords.awk untranslatedSymbolsBS$PPID.tmp \
	englishWithCommentsStripped$PPID.tmp > untranslatedSymbolsAndCheapText$PPID.tmp

# Split the language file, insert the untranslated symbol and cheaptext between
# both parts and write the output to a new .IN file.
set OUTPUT_FILE=${2:r}.IN
nawk '{ if ($0 !~/endif/) print $0 }' < $2 >> $OUTPUT_FILE
cat untranslatedSymbolsAndCheapText$PPID.tmp >> $OUTPUT_FILE
nawk '{ if ($0 ~/endif/) print $0 }' < $2 >> $OUTPUT_FILE

rm *$PPID.tmp
