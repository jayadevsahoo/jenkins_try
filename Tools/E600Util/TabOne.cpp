// TabOne.cpp : implementation file
//

#include "stdafx.h"
#include "E600Util.h"
#include "TabOne.h"
#include "UserMessages.h"

typedef struct PC2BDCommands
{
	CString CommandString;
	UINT16 CommandValue;
} PC2BD_COMMANDS;

const CString StopBroadcastStr(L"StopBroadcast");
const CString StartBroadcastStr(L"StartBroadcast");
const CString UpdateSignalsStr(L"UpdateSignals");
const CString SignalsRequestStr(L"SignalsRequest");
const CString SetSampleTimeStr(L"SetSampleTime");
const CString PingStr(L"Ping");
const CString DoCalStr(L"DoCalibration");

static const PC2BD_COMMANDS CommandList[NUMBER_PC_COMMANDS] = { 
{StopBroadcastStr, PC_DISABLE_BROADCAST_MSG_ID},
{StartBroadcastStr, PC_ENABLE_BROADCAST_MSG_ID},
{UpdateSignalsStr, PC_SET_SIGNALS_MSG_ID}, 
{SignalsRequestStr, PC_REQUEST_SIGNALS_MSG_ID},
{SetSampleTimeStr, PC_SET_SAMPLE_TIME_MSG_ID},
{PingStr, PC_PING_MSG_ID},
{DoCalStr, PC_CALIBRATION_MSG_ID}
};


#define USE_SOCK_DGRAM				1
//#define TEST_E600_UI				1

#define SELECTED_SIGS_FILE_NAME		L"SelectedSignals.txt"
#define SELECTABLE_SIGS_FILE_NAME	L"Signals.h"
#define SIG_GROUPS_FILE_NAME		L"SignalsGroups.txt"
#define DEFAULT_GROUP_NAME			L"Default"

#define	GROUP_ID_CHAR				':'
#define ENDLINE						L"\n"

// CTabOne dialog

IMPLEMENT_DYNAMIC(CTabOne, CDialog)

//**********************************************************************************************//
CTabOne::CTabOne(CWnd* pParent /*=NULL*/)
	: CDialog(CTabOne::IDD, pParent)
{
	HeaderStr = ENDLINE;
	SignalsFileName = SELECTED_SIGS_FILE_NAME;
	GroupsFileName = SIG_GROUPS_FILE_NAME;
	SelectableSignalsFileName = SELECTABLE_SIGS_FILE_NAME;
	bSelectedSignalsChanged = FALSE;
	bGroupChanged = FALSE;

	NumSelectableSignals = 0;
	pParentWnd = pParent;

	// initialize selected signals container with default
	for(int i=0; i<NUM_SIGNALS; i++)
	{
		EngineeringSignals.push_back(DEFAULT_ENGINEERING_SIGNALS[i]);
	}
}

//**********************************************************************************************//
CTabOne::~CTabOne()
{
	//m_Client.Close();
	if(bSelectedSignalsChanged)
	{
		// Signals changed, so create a new file
		if(SelectedSigsFile.Open((LPCTSTR)SELECTED_SIGS_FILE_NAME, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			// seek to beginning and end
			//SelectedSigsFile.SeekToEnd();
			//SelectedSigsFile.SeekToBegin();

			int Size = SelectedSignals.size();
			for(int i=0; i<Size; i++)
			{
				CString TempStr = SelectedSignals[i];
				if(i != (Size - 1))
					TempStr += L",";
				SelectedSigsFile.WriteString((LPCTSTR)TempStr);
			}
			SelectedSigsFile.Close();
		}
		else
		{
			MessageBox(L"Failed to create Signals file!");	
		}
	}

	if(bGroupChanged)
	{	
		if(GroupsFile.Open(GroupsFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			list<GROUP_DATA_TYPE>::iterator ListItr = GroupList.begin();
			list<GROUP_DATA_TYPE>::iterator ListItrEnd = GroupList.end();
		
			do
			{
				CString CurGroupName = ListItr->GroupName;
				CurGroupName.Insert(0, GROUP_ID_CHAR);
				CurGroupName += ENDLINE;
				GroupsFile.WriteString(CurGroupName);
				
				CString SignalName;
				for(int i=0; i<(int)(ListItr->Signals.size()); i++)
				{
					SignalName = ListItr->Signals[i];
					SignalName += ENDLINE;
					GroupsFile.WriteString(SignalName);
				}
				ListItr++;
			}while(ListItr != ListItrEnd);

			GroupsFile.Close();
		}
	}
}

//**********************************************************************************************//
//**********************************************************************************************//
void CTabOne::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_EDIT_PARAM, ParamValue);
}


BEGIN_MESSAGE_MAP(CTabOne, CDialog)
	ON_WM_PAINT()
	ON_MESSAGE_VOID(WM_KICKIDLE, OnKickIdle)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CTabOne::OnBnClickedButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_SEND, &CTabOne::OnBnClickedButtonSend)
	ON_BN_CLICKED(IDC_BUTTON_CAPTURE, &CTabOne::OnBnClickedButtonCapture)
	ON_BN_CLICKED(IDC_BUTTON_MONITOR, &CTabOne::OnBnClickedButtonMonitor)
	ON_MESSAGE(WM_ETHERNET_DATA, &CTabOne::ReceiveUdpData)
	ON_MESSAGE(WM_CHECK_APP_BUTTONS, &CTabOne::CheckAppButtons)
	ON_MESSAGE(WM_APP_SHUTDOWN, &CTabOne::OnShutdown)
	ON_MESSAGE(WM_CAL_CMD, &CTabOne::SendCalCommand)
	ON_MESSAGE(WM_SETTING_CMD, &CTabOne::SendSettingCommand)
	ON_MESSAGE(WM_GAIN_CMD, &CTabOne::SendGainCommand)

	ON_UPDATE_COMMAND_UI(IDC_BUTTON_CONNECT, &CTabOne::OnUpdateButtonConnect)
	ON_UPDATE_COMMAND_UI(IDC_BUTTON_CAPTURE, &CTabOne::OnUpdateButtonCapture)
	ON_BN_CLICKED(IDC_CHECK_TCP, &CTabOne::OnBnClickedCheckTcp)
	ON_BN_CLICKED(IDC_BUTTON_ADD_SIG, &CTabOne::OnBnClickedButtonAddSig)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_SIG, &CTabOne::OnBnClickedButtonRemoveSig)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_UP, &CTabOne::OnBnClickedButtonMoveUp)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_DOWN, &CTabOne::OnBnClickedButtonMoveDown)
	ON_BN_CLICKED(IDC_BUTTON_CREATE, &CTabOne::OnBnClickedButtonCreateGroup)
	ON_BN_CLICKED(IDC_BUTTON_POPULATE, &CTabOne::OnBnClickedButtonPopulate)
	ON_BN_CLICKED(IDC_BUTTON_MODIFY, &CTabOne::OnBnClickedButtonModify)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, &CTabOne::OnBnClickedButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, &CTabOne::OnBnClickedButtonBrowse)
	ON_BN_CLICKED(IDC_BUTTON_DOWNLOAD_FILE, &CTabOne::OnBnClickedButtonDownloadFile)
	ON_BN_CLICKED(IDC_BUTTON_UPDATE_SIGNALS, &CTabOne::OnBnClickedButtonUpdateSignals)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_SIGNALS, &CTabOne::OnBnClickedButtonClearSignals)
	ON_BN_CLICKED(IDC_BUTTON_DLOAD_BROWSE, &CTabOne::OnBnClickedButtonDloadBrowse)
	ON_BN_CLICKED(IDC_CHECK_IN_ENGINEERING, &CTabOne::OnBnClickedCheckInEngineering)
END_MESSAGE_MAP()

//**********************************************************************************************//
//**********************************************************************************************//
void CTabOne::OnPaint()
{
	CDialog::OnPaint();
}

//**********************************************************************************************//
void CTabOne::UpdateGlobalHeaderString()
{
	SetGlobalHeaderString(SelectedSignals);
	pTabTwo->PostMessageW(WM_HEADER_STRING, 0, (LPARAM)(LPCTSTR)GlobalHeaderString);
	pTabThree->PostMessageW(WM_HEADER_STRING, 0, (LPARAM)(LPCTSTR)GlobalHeaderString);
}

//**********************************************************************************************//
bool CTabOne::FindGroupFromFileAndFillList(CString GroupName)
{
	bool GroupFound = false;
	bool Looping = true;
	
	CString NameStr(L":");
	NameStr += GroupName;

	if(GroupsFile.Open(GroupsFileName, CFile::modeRead | CFile::typeText))
	{
		while(Looping)
		{
			CString TempStr;		
			if(GroupsFile.ReadString(TempStr))
			{
				int Match = NameStr.Compare(TempStr);
				if(Match == 0)	// found it
				{
					GroupFound = true;
					SelectedSignals.clear();
					
					while(1)	// loop to read all signals
					{
						BOOL NotEOF = GroupsFile.ReadString(TempStr);
						TempStr.Trim();
						TCHAR Char = TempStr.GetAt(0);
						if(NotEOF && ( Char != GROUP_ID_CHAR))
						{
							SelectedSignals.push_back(TempStr);
						}
						else
						{
							Looping = false;
							break;
						}
					}
				}					
			}
			else	// EOF
			{
				break;
			}
		}
		GroupsFile.Close();
	}
	else
	{
		MessageBox(L"Failed to open Groups file for finding group!");
	}
	
	// Refill the selected signals list with signals from the new group
	if(GroupFound)
	{
		CListBox * pSelectedSigs = (CListBox *)(GetDlgItem(IDC_LIST_SELECTED_SIGNALS));
		pSelectedSigs->ResetContent();	// clear all entries from the list box
		
		unsigned int i;
		for(i=0; i<SelectedSignals.size(); i++)
		{
			pSelectedSigs->AddString(SelectedSignals[i]);	
		}
		return true;
	}
	else
	{
		return false;
	}
}


bool CTabOne::FindGroupAndFillList(CString GroupName)
{
	bool Success = false;
	list<GROUP_DATA_TYPE>::iterator ListItr = GroupList.begin();
	list<GROUP_DATA_TYPE>::iterator ListItrEnd = GroupList.end();

	for(; ListItr != ListItrEnd; ListItr++)
	{
		if(GroupName == ListItr->GroupName)
		{
			SelectedSignals.clear();
			CListBox * pSelectedSigs = (CListBox *)(GetDlgItem(IDC_LIST_SELECTED_SIGNALS));
			pSelectedSigs->ResetContent();	// clear all entries from the list box

			for(int i=0; i<(int)ListItr->Signals.size(); i++)
			{
				SelectedSignals.push_back(ListItr->Signals[i]);
				pSelectedSigs->AddString(ListItr->Signals[i]);
			}
			Success = true;
			break;
		}
	}

	if(!Success)
	{
		MessageBox(L"Can not find group", L"Error", MB_ICONERROR);
	}
	return Success;
}

//**********************************************************************************************//
CString CTabOne::ReadGroupSignals(vector<CString> &Signals)
{
	CString Str;
		
	while(true)
	{
		BOOL Result = GroupsFile.ReadString(Str);
		if(Result)
		{
			Str.TrimLeft();
			LPCTSTR pStr = Str;
		
			if(Str.IsEmpty()) 
				break;
			//else if(Str.GetAt(0) == GROUP_ID_CHAR)	// same as next line
			//else if(Str[0] == GROUP_ID_CHAR)			// same as next line
			//else if (pStr[0] == GROUP_ID_CHAR)		// same as next line
			else if (*pStr == GROUP_ID_CHAR)
				break;	// how to take care when reading the next group?
			else
				Signals.push_back(Str);
		}
		else
		{
			break;
		}
	}
	return Str;
}

//**********************************************************************************************//
bool CTabOne::ReadGroupNamesFromFile(bool bCloseFile)
{
	CString GroupStr;
	int GroupCount = 0;
	CString RetStr;
	bool GroupFound = false;

	if(GroupsFile.Open(GroupsFileName, CFile::modeRead | CFile::typeText))
	{
		while(true)
		{
			// Read the first group,
			if(GroupsFile.ReadString(GroupStr))
			{
				GroupStr.TrimLeft();
				if(GroupStr[0] == GROUP_ID_CHAR)
				{
					int nChar = GroupStr.Remove(GROUP_ID_CHAR);
					ASSERT(nChar == 1);
					GroupFound = true;
					break;
				}
			}
			else // if hit EOF, exit loop
			{
				break;
			}
		}

		if(GroupFound)
		{
			// builing a list of GROUP_DATA_TYPE which includes both names and signals
			while(true)
			{
				// a separate list of group names
				GroupNames.push_back(GroupStr);

				GROUP_DATA_TYPE SigGroup;
				SigGroup.GroupName = GroupStr;
				RetStr = ReadGroupSignals(SigGroup.Signals);
				if(SigGroup.Signals.size() > 0)
				{
					GroupList.push_back(SigGroup);
				}
				
				if(RetStr[0] == GROUP_ID_CHAR)
				{
					RetStr.Remove(GROUP_ID_CHAR);
					GroupStr = RetStr;
				}
				else
				{
					break;
				}
			}
		}

		if(bCloseFile)
		{
			GroupsFile.Close();
		}
		return true;
	}
	else
	{
		MessageBox(L"Failed to open Groups file for reading!");
		return false;
	}
}

//**********************************************************************************************//
bool CTabOne::ReadSelectedSignals()
{
	CString SignalStr;

	if(SelectedSigsFile.Open(SignalsFileName, CFile::modeRead | CFile::typeText))
	{	
		SelectedSigsFile.ReadString(SignalStr);
		SelectedSignals.clear();
		ParseCSVHeader(SignalStr, SelectedSignals);
			
		SelectedSigsFile.Close();
		return true;
	}
	else
	{
		MessageBox(L"Failed to open Signals file %s", (LPCTSTR)SELECTED_SIGS_FILE_NAME);
		return false;
	}
}

//**********************************************************************************************//
void CTabOne::CreateNewGroupFile(CString GroupName, const TCHAR *GroupSignals[])
{
	CString SignalStr;
	CString GroupStr;

	// if successfully creating group file, write all default signals to this file
	if(GroupsFile.Open(GroupsFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		GroupStr = L":";
		GroupStr += GroupName;
		GroupStr += ENDLINE;
		GroupsFile.WriteString(GroupStr);

		for(int i=0; i<NUM_SIGNALS; i++)
		{
			SignalStr = GroupSignals[i];
			SignalStr += ENDLINE;
			GroupsFile.WriteString(SignalStr);
		}
		GroupsFile.Close();
	}
	else
	{
		MessageBox(L"Failed to create Groups file!");	
	}
}

//**********************************************************************************************//
bool CTabOne::IsSignalAvailable(const TCHAR *Signals, const vector<CString> SelectedSignals)
{
	CString Str = Signals;
	for(int i=0; i<(int)SelectedSignals.size(); i++)
	{
		if(Str == SelectedSignals[i])
		{
			return false;
		}
	}
	return true;
}

//**********************************************************************************************//
UINT16 CTabOne::GetSignalNumber(const CString& SigName)
{
	UINT16 SigNum = 0;
	UINT16 Size = (UINT16)SelectableSignals.size();

	for(UINT16 i=0; i<Size; i++)
	{
		if(SigName == SelectableSignals[i])
		{
			SigNum = i + 1;
			break;
		}
	}
	return SigNum;
}

//**********************************************************************************************//
void CTabOne::PopulateSignalLists()
{
	// populate BD Signals in the list boxes
	CListBox * pSelectedSigs = (CListBox *)(GetDlgItem(IDC_LIST_SELECTED_SIGNALS));

	// check if signals file exists
	CFileStatus Status;
	
	if(!CFile::GetStatus(SignalsFileName, Status))	// if not exist
	{
		// Selected signals file does not exist, create
		if(SelectedSigsFile.Open((LPCTSTR)SELECTED_SIGS_FILE_NAME, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			if(!CFile::GetStatus(GroupsFileName, Status))
			{
				CreateNewGroupFile(DEFAULT_GROUP_NAME, DEFAULT_SIGNALS);

				// initialize selected signals container with default
				for(int i=0; i<NUM_SIGNALS; i++)
				{
					pSelectedSigs->AddString(DEFAULT_SIGNALS[i]);
					SelectedSignals.push_back(DEFAULT_SIGNALS[i]);
				}
				SelectedGroup = DEFAULT_GROUP_NAME;
			}
			
			// create one single header string for all selected signals
			SetGlobalHeaderString(SelectedSignals);
			bSelectedSignalsChanged = true;

			// Group file should exist now, read all names in the file and close file when done
			ReadGroupNamesFromFile(true);
			SelectedSigsFile.Close();
		}
		else
		{
			MessageBox(L"Failed to create Signals file!");	
		}
	}
	else
	{
		// Selected signals file exists, read the signals
		if(ReadSelectedSignals())
		{
			// populate selected signals listbox
			for(int i=0; i<(int)SelectedSignals.size(); i++)
			{
				pSelectedSigs->AddString(SelectedSignals[i]);
			}

			// create one single header string for all selected signals
			SetGlobalHeaderString(SelectedSignals);

			// read group names from file and close file
			ReadGroupNamesFromFile(true);
		}
	}

	// should be at least 1 group - default
	if(!GroupNames.empty())
	{
		// fill group combobox
		CComboBox *pComboBox = (CComboBox *)(GetDlgItem(IDC_COMBO_GROUP));
		int NumGroups = GroupNames.size();
		for(int i=0; i<NumGroups; i++)
		{
			pComboBox->AddString(GroupNames[i]);
		}
		//pComboBox->SetWindowTextW((LPCTSTR)SelectedGroup);
		pComboBox->SetWindowTextW(GroupNames[0]);
	}
	
	// populate all available signals
	CListBox * pListBoxAllSigs = (CListBox *)(GetDlgItem(IDC_LIST_ALL_SIGNALS));
	for(int i=0; i<NumSelectableSignals; i++)
	{
		if(IsSignalAvailable(SelectableSignals[i], SelectedSignals))
		{
			pListBoxAllSigs->AddString(SelectableSignals[i]);
			AvailSignals.push_back(SelectableSignals[i]);
		}
	}
}

//**********************************************************************************************//
void CTabOne::ReadSelectableSignalsFromFile()
{
	CFileStatus Status;
	CString SignalStr;
	CString TempStr;
	
	if(!CFile::GetStatus(SelectableSignalsFileName, Status))
	{
		MessageBox(L"SelectableSignals file does not exist!");	
	}
	else
	{
		if(SelectableSigsFile.Open(SelectableSignalsFileName, CFile::modeRead | CFile::typeText))
		{
			while(true)
			{
				// if EOF, exit loop
				if(!SelectableSigsFile.ReadString(TempStr))
				{
					break;
				}

				int i = TempStr.Find('/');
				if( i > 0)
				{
					// parse the signal name
					SignalStr = TempStr.Right(TempStr.GetLength() - (i + 2));
					
					// if getting a blank line or the last signal "Total"
					if(SignalStr.IsEmpty() || (SignalStr.Compare(L"Total") == 0))
					{
						SelectableSigsFile.Close();
						if(NumSelectableSignals > 0)
						{
							// fill both selectable and selected lists
							PopulateSignalLists();
						}
						break;
					}
					else	// save signal name
					{
						if(SignalStr.Compare(L"None") == 0)
						{
							//first signal found, ignore this one
						}
						else
						{
							SelectableSignals.push_back(SignalStr);
							NumSelectableSignals++;
						}
					}		
				}
			}
		}
		else
		{
			MessageBox(L"Failed to open SelectableSignals file!");
		}
	}
}

//**********************************************************************************************//
void CTabOne::PopulateAvailSignalsList()
{
	CListBox *pListBoxAvailSigs = (CListBox *)(GetDlgItem(IDC_LIST_ALL_SIGNALS));
	pListBoxAvailSigs->ResetContent();
	AvailSignals.clear();

	int SigCount = (int)SelectableSignals.size();
	for(int i=0; i<SigCount; i++)
	{
		if(IsSignalAvailable(SelectableSignals[i], SelectedSignals))
		{
			pListBoxAvailSigs->AddString(SelectableSignals[i]);
			AvailSignals.push_back(SelectableSignals[i]);
		}
	}
}

//**********************************************************************************************//
int CTabOne::LoadButtonBitmap(DWORD ButtonID, LPCWSTR Path, DWORD BitmapID)
{
	CBitmapButton *pSigButton = (CBitmapButton *)GetDlgItem(ButtonID);
	/* HBITMAP hBitmap = (HBITMAP) ::LoadImage(NULL, Path, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if(hBitmap)
	{
		pSigButton->SetBitmap(hBitmap);
		return 1;
	}*/
	
	//CBitmap Bitmap;
	//if (Bitmap.LoadBitmap(BitmapID))
	HBITMAP hBitmap = ::LoadBitmap( (HMODULE)GetModuleHandle(NULL), MAKEINTRESOURCE(BitmapID) );
	{
		pSigButton->SetBitmap(hBitmap);
		return 1;
	}

	return 0;
}

//**********************************************************************************************//
BOOL CTabOne::OnInitDialog()
{
	CDialog::OnInitDialog();

	bCapture = FALSE;
	bMonitor = FALSE;
	bConnect = FALSE;
	ParamValue = 2;

	// Show default sample time
	CWnd *pEditParam = GetDlgItem(IDC_EDIT_PARAM);
	pEditParam->SetWindowTextW(L"2");

	CButton *pDownloadFileButton = (CButton *)GetDlgItem(IDC_BUTTON_DOWNLOAD_FILE);
	pDownloadFileButton->EnableWindow(FALSE);

	// capture file name
	CWnd *pEditFileName = GetDlgItem(IDC_EDIT_FILE_NAME);
	pEditFileName->SetWindowTextW(L"c:\\test1.csv");

	// populate test commands in combo box
	CComboBox *pCommandBox = (CComboBox *)GetDlgItem(IDC_COMBO_DATA_CMD);
	for(int i=0; i<NUMBER_PC_COMMANDS; i++)
	{
		pCommandBox->AddString(CommandList[i].CommandString);
	}
	pCommandBox->SelectString(-1, CommandList[0].CommandString);

	// initialize radio buttons
	CButton *pRadioButton = (CButton *)GetDlgItem(IDC_RADIO_SEND_BD);
	pRadioButton->SetCheck(BST_CHECKED);

	
	CIPAddressCtrl *pIPAddr = (CIPAddressCtrl *)GetDlgItem(IDC_IPADDRESS);

#if TEST_E600_UI
	pIPAddr->SetAddress(0xC0A80002);
#else
	pIPAddr->SetAddress(0xC0A80003);
#endif
	
	BYTE nField0, nField1, nField2, nField3; 
	pIPAddr->GetAddress(nField0, nField1, nField2, nField3);
	IPAddrStr.Format(L"%d.%d.%d.%d", nField0, nField1, nField2, nField3);

	CEdit *pEditPort = (CEdit *)GetDlgItem(IDC_EDIT_PORT);
	CString PortNumber;
	PortNumber.Format(L"%d", BD_ETHERNET_PORT_UDP); 
	pEditPort->SetWindowTextW(PortNumber);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ReadSelectableSignalsFromFile();
		
	// Fill signal order number from 1 - 18
	CListBox * pSigNumList = (CListBox *)(GetDlgItem(IDC_LIST_SIG_NUM));
	for(int i=0; i<NUM_SIGNALS; i++)
	{
		CString SigNum;
		SigNum.Format(L"%d", i+1);
		pSigNumList->AddString(SigNum);
	}

	// initialize selected signal numbers array
	SetSelectedSignalNumbers();
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// loading bitmap for arrow buttons
	LoadButtonBitmap(IDC_BUTTON_ADD_SIG, L".\\res\\RArrow.bmp", IDB_BITMAP_RARROW);
	LoadButtonBitmap(IDC_BUTTON_REMOVE_SIG, L".\\res\\LArrow.bmp", IDB_BITMAP_LARROW);
	LoadButtonBitmap(IDC_BUTTON_MOVE_UP, L".\\res\\UArrow.bmp", IDB_BITMAP_UARROW);
	LoadButtonBitmap(IDC_BUTTON_MOVE_DOWN, L".\\res\\DArrow.bmp", IDB_BITMAP_DARROW);
	LoadButtonBitmap(IDC_BUTTON_POPULATE, L".\\res\\RArrow.bmp", IDB_BITMAP_RARROW);
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	CButton *pCheckButton = (CButton *)GetDlgItem(IDC_CHECK_TCP);
	CButton *pConnectButton = (CButton *)GetDlgItem(IDC_BUTTON_CONNECT);	

#if USE_SOCK_DGRAM

	pCheckButton->SetCheck(BST_UNCHECKED);
	pConnectButton->EnableWindow(FALSE);

	// (1) automatically calls Bind() in this way but if wanting to set sock option use (2)
	//if(m_Client.Create(TOOL_ETHERNET_PORT_UDP, SOCK_DGRAM) == FALSE)
	// (2) second method: create, set option, then bind manually
	if(!m_Client.Socket(SOCK_DGRAM))
	{
		MessageBox(L"TabTwo: Failed to Create Socket");
		return FALSE;
	}
	BOOL OptValue = TRUE; // 
	m_Client.SetSockOpt(SO_BROADCAST, &OptValue, sizeof(OptValue), SOL_SOCKET);
	m_Client.Bind(TOOL_ETHERNET_PORT_UDP);

#else

	pCheckButton->SetCheck(BST_CHECKED);
	pConnectButton->EnableWindow(TRUE);

	if(m_Client.Create(TOOL_ETHERNET_PORT_TCP, SOCK_STREAM) == FALSE)
	{
		MessageBox(L"Failed to Create Socket");
		return FALSE;
	}
#endif

	m_Client.pDlg=this;
	return TRUE;
}


// CTabOne message handlers
//**********************************************************************************************//
void CTabOne::OnBnClickedButtonConnect()
{
	if( AfxSocketInit() == FALSE)
	{ 
		AfxMessageBox(L"Failed to Initialize Sockets"); 
	}
	else
	{
#if USE_SOCK_DGRAM		
		AfxMessageBox(L"Do Nothing For UDP!");

#else // For UDP, no need to connect
		
#if 0
		if(m_Client.Connect((LPCTSTR)IPAddrStr, DEVICE_ETHERNET_PORT_TCP) == FALSE)
		{
			MessageBox(L"Failed to Connect");
			return;
		}
#else		
		CButton *pConnectButton = (CButton *)GetDlgItem(IDC_BUTTON_CONNECT);

		if(bConnect == FALSE)
		{
			if(m_Client.Connect((LPCTSTR)IPAddrStr, DEVICE_ETHERNET_PORT_TCP) == FALSE)
			{
				MessageBox(L"Failed to Connect");
				return;
			}
			bConnect = TRUE;
			pConnectButton->SetWindowText((LPTSTR)L"DISCONNECT");
		}
		else
		{
			m_Client.Close();
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText((LPTSTR)L"CONNECT");
			bConnect = FALSE;
		}
#endif

#endif
	}
}


#if 1
//**********************************************************************************************//
LRESULT CTabOne::ReceiveUdpData(WPARAM x, LPARAM y)
{
	UINT16 *pMsgID = (UINT16 *)y;									// point to message id
	UINT16 nBytes = *(UINT16 *)(y + sizeof(UINT16));					// point to string size for header and sync msg
	//int nBytes = *(int *)x - (4 * sizeof(UINT));				// size of data field
	char *pcPacketData = (char *)(y + (2 * sizeof(UINT)));		// point to the beginning of data field
	wchar_t *pwPacketData = (wchar_t *)(y + (2 * sizeof(UINT)));
	UINT *plPacketData = (UINT *)(y + (2 * sizeof(UINT)));
	CString MsgStr = pwPacketData;
	CWnd *pEditResponse = GetDlgItem(IDC_EDIT_RESPONSE);
	bool Success = false;
		
	switch(*pMsgID)
	{
	case BD_BROADCAST_MSG_ID:
		ProcessDataPacket(nBytes, pcPacketData);
		break;
	case BD_SIGNALS_RESPONSE_MSG_ID:
#if 0		
		// Copy header string - Method 1
		HeaderStr = pwPacketData;	// copy header string
		
		// Copy header string - Method 2
		//HeaderStr.Format(L"%s", pwPacketData);

		//LPCTSTR pHeader;		// same as const wchar_t *pHeader;
		//pHeader = HeaderStr.GetBuffer();
		//HeaderStr.ReleaseBuffer();			// must call ReleaseBuffer() after GetBuffer()		
		//AfxMessageBox (pHeader);
		
		pEditResponse = GetDlgItem(IDC_EDIT_RESPONSE);
		pEditResponse->SetWindowTextW(pwPacketData);
#endif
		Success = true;
		for(int i = 0; i<MAX_SELECTED_SIGNALS; i++)
		{
			BDSignalNumbers[i] = pwPacketData[i];
			if(BDSignalNumbers[i] != SelectedSignalNumbers[i])
			{
				AfxMessageBox (L"Selected Signals out of Sync");
				Success = false;
			}
		}
		if(Success)
		{
			pEditResponse->SetWindowTextW(L"OK");
		}
		else
		{
			pEditResponse->SetWindowTextW(L"FAILED");
		}
		break;
	case BD_RESPONSE_MSG_ID:
		pEditResponse->SetWindowTextW(pwPacketData);
		pStatusBar->SetPaneText(0, L"ACK Message Received");
		break;
	case BD_PING_MSG_ID:
		pEditResponse->SetWindowTextW(MsgStr);
		pStatusBar->SetPaneText(0, L"PING Reply Message Received");
		break;
	case BD_CAL_STATUS_MSG_ID:
		GetParent()->SendMessageW(WM_CAL_MSG_UPDATE_WINDOW, *plPacketData, 0);
		break;
	case BD_ALL_GAINS_REPLY_MSG_ID:
		GetParent()->SendMessageW(WM_ALL_GAINS_RX, x, y);
		pStatusBar->SetPaneText(0, L"Gains Reply Message Received");
		break;
	case BD_READ_DEBUG_REPLY_MSG_ID:

		pStatusBar->SetPaneText(0, L"Debug Reply Message Received");
		break;
	case PC_REQUEST_SETTINGS_MSG_ID:
		GetParent()->SendMessageW(WM_SETTINGS_REPLY, x, y);
		pStatusBar->SetPaneText(0, L"Settings Reply Message Received");
		break;
	default:
		pwPacketData = (wchar_t *)y;
		pEditResponse->SetWindowTextW(pwPacketData);
		break;
	}

	return 0;
}

//**********************************************************************************************//
void CTabOne::ProcessDataPacket(int DataSize, char *pPacketData)
{
	int nBytes = DataSize;
	float *pData = (float *)pPacketData;
	int nData = nBytes / sizeof(float);
	float SignalData[NUM_SIGNALS][NUM_SLOTS];

	memcpy(SignalData, pData, nBytes);
	
	if(bMonitor)
	{		
		CWnd *pEdit1 = GetDlgItem(IDC_EDIT_RX_DATA);
#if 1
		int StrLen = 0;
		CString str(L"");
		CString Str(L"");

		for(int i = 0; i < NUM_SLOTS; i++)
		{
			for(int j = 0; j < NUM_SIGNALS; j++)
			{
				if(j == (NUM_SIGNALS - 1))
				{
					str.Format(L"%3.2f\r\n", SignalData[j][i]);
				}
				else
				{
					str.Format(L"%3.2f ", SignalData[j][i]);
				}			
				Str += str;
			}
		}
		pEdit1->SetWindowText((LPCTSTR)Str);
		pTabTwo->PostMessage(WM_BD_DATA, DataSize, (LPARAM)pPacketData);
		pTabThree->PostMessage(WM_BD_DATA, DataSize, (LPARAM)pPacketData);
#else
		wchar_t Str[20];
		wchar_t TempBuff[3000];
		int j = 0;
		for(int i = 0; i < nData; i++)
		{
			if(++j % 10)
				swprintf_s(Str, 20, L"%3.2f ", *pData++);
			else
				swprintf_s(Str, 20, L"%3.2f\r\n", *pData++);
			if(i==0)
				wcscpy_s(TempBuff, 3000, Str);
			else
				wcscat_s(TempBuff, 3000, Str);
		}
		wcscat_s(TempBuff, 3000, ENDLINE);
		pEdit1->SetWindowText((LPCTSTR)TempBuff);
#endif
	}

	if(bCapture)
	{
#if 1
		int StrLen = 0;
		CString str(L"");
		CString Str(L"");

		for(int i = 0; i < NUM_SLOTS; i++)
		{
			for(int j = 0; j < NUM_SIGNALS; j++)
			{
				str = "";
				if(j == (NUM_SIGNALS - 1))
				{
					str.Format(L"%3.2f\r\n", SignalData[j][i]);
				}
				else
				{
					str.Format(L"%3.2f,", SignalData[j][i]);
				}
				
				Str += str;
			}
		}

		StrLen += Str.GetLength();	// number of characters
		CaptureFile.Write((LPCTSTR)Str, StrLen * 2);	// but need number of bytes so double
#else
		CString Str;
		static CString OutStr;

		for(int i = 0; i < nData; i++)
		{
			if(i == nData - 1)
				Str.Format(_T("%03.2f"), *pData++);
			else
				Str.Format(_T("%03.2f,"), *pData++);

			OutStr += Str;
			if(((i+1) % 18) == 0)
			{
				OutStr += ENDLINE;
				TCHAR	szBuff[1024];
				ULONG	LenToWrite = 2 * _stprintf_s(szBuff, 1024, _T("%s"), OutStr);
				CaptureFile.Write(szBuff, LenToWrite);	
				OutStr = "";
			}
		}
#endif
	}

	GetParent()->SendMessageW(WM_ETH_MSG_UPDATE_WINDOW, 0, 0);
}

#else

void CTabOne::ReceiveUdpData(char *Buff, size_t nBytes)
{
	wchar_t *pStr;
	size_t convertedChars = 0;
	wchar_t TempBuff[1000];
	CString Str;
	
	pStr = new wchar_t[nBytes + 1];
	mbstowcs_s(&convertedChars, pStr, nBytes, Buff, _TRUNCATE);

	GetDlgItem(IDC_EDIT2_RX_DATA)->GetWindowText((LPTSTR)TempBuff, 1000);
	wcsncat_s(TempBuff, 1000, pStr, 1000);
	//GetDlgItem(IDC_EDIT2_RX_DATA)->SetWindowText((LPCTSTR)pStr);
	GetDlgItem(IDC_EDIT2_RX_DATA)->SetWindowText((LPCTSTR)TempBuff);
}

#endif

//**********************************************************************************************//
LRESULT CTabOne::CheckAppButtons(WPARAM x, LPARAM y)
{
	return 0;
}

//**********************************************************************************************//
LRESULT CTabOne::SendCalCommand(WPARAM x, LPARAM y)
{
	SendCmdMessage((wchar_t *)y, x);
	return 0;
}

//**********************************************************************************************//
LRESULT CTabOne::SendSettingCommand(WPARAM x, LPARAM y)
{
	SendCmdMessage((wchar_t *)y, x);
	return 0;
}

//**********************************************************************************************//
LRESULT CTabOne::SendGainCommand(WPARAM x, LPARAM y)
{
	SendCmdMessage((wchar_t *)y, x);
	return 0;
}

//**********************************************************************************************//
int CTabOne::SendCmdMessage(wchar_t *pBuff, int Size)
{
	// determine port and IP address
	int Port;
	CString IpAddr;
	CButton *pRadioButton = (CButton *)GetDlgItem(IDC_RADIO_SEND_UI);
	if(pRadioButton->GetCheck())
	{
		Port = UI_ETHERNET_PORT_UDP;
		IpAddr.Format(L"192.168.0.2");
	}
	else
	{
		Port = BD_ETHERNET_PORT_UDP;
		IpAddr.Format(L"192.168.0.3");
	}

	int SizeSent = m_Client.SendToEx((LPCTSTR)pBuff, Size, Port, (LPCTSTR)IpAddr);

	if(SizeSent != Size)
	{
		 UINT uErr = GetLastError();
		 if (uErr != WSAEWOULDBLOCK) 
		 {
			TCHAR szError[256];
			wsprintf(szError, L"Server Socket failed to send: %d", uErr);
			AfxMessageBox(szError);
		 }
	}

	return SizeSent;
}

//**********************************************************************************************//
int CTabOne::SendPacketToBD(char *pBuff, int Size)
{
	// configure port and IP address
	CString IpAddr;
	int Port = BD_ETHERNET_PORT_UDP;
	IpAddr.Format(L"192.168.0.3");

	int SizeSent = m_Client.SendToEx((LPCTSTR)pBuff, Size, Port, (LPCTSTR)IpAddr);

	if(SizeSent != Size)
	{
		 UINT uErr = GetLastError();
		 if (uErr != WSAEWOULDBLOCK) 
		 {
			TCHAR szError[256];
			wsprintf(szError, L"Server Socket failed to send: %d", uErr);
			AfxMessageBox(szError);
		 }
	}

	return SizeSent;
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonSend()
{
	int SizeToSend, SizeSent;
	CString Str;
	//const CString BD_Address(L"192.168.0.3");
	
	CComboBox *pCommandBox = (CComboBox *)GetDlgItem(IDC_COMBO_DATA_CMD);
	pCommandBox->GetWindowText(Str);
	int CurSelIndex = pCommandBox->GetCurSel();

	// clear the response edit control
	CWnd *pEditResponse;
	pEditResponse = GetDlgItem(IDC_EDIT_RESPONSE);
	pEditResponse->SetWindowTextW(L"");

#if USE_SOCK_DGRAM	
	wchar_t wBuff[100];
/*
	if(Str == PingStr)
	{
		wBuff[0] = PC_PING_MSG_ID;
		SizeToSend = sizeof(UINT16);
		SizeSent = m_Client.SendToEx((LPCTSTR)wBuff, SizeToSend, Port, (LPCTSTR)IpAddr);
	}
	else if(Str == SignalsRequestStr)
	{
		wBuff[0] = PC_SIGNALS_REQUEST_MSG_ID;
		SizeToSend = sizeof(UINT16);
		SizeSent = m_Client.SendToEx((LPCTSTR)wBuff, SizeToSend, Port, (LPCTSTR)IpAddr);
	}
	else	// send out the string as-is
	{
		SizeToSend = (Str.GetLength() + 1) * 2;
		SizeSent = m_Client.SendToEx((LPCTSTR)Str, SizeToSend, Port, (LPCTSTR)IpAddr);
	}
	*/
	
	wBuff[0] = CommandList[CurSelIndex].CommandValue;

	if(Str == UpdateSignalsStr)
	{
		//OnBnClickedButtonUpdateSignals();
		if(SetSelectedSignalNumbers())
		{
			for(int i=0; i<MAX_SELECTED_SIGNALS; i++)
			{
				wBuff[i+1] = SelectedSignalNumbers[i];
			}
			SizeToSend = (MAX_SELECTED_SIGNALS * sizeof(UINT16)) + sizeof(UINT16);
			UpdateGlobalHeaderString();
		}
		else
		{
			MessageBox(L"SetSelectedSignalNumbers() failed", L"Error", MB_ICONERROR);
			return;
		}
	}
	else if(Str == SetSampleTimeStr)
	{
		UpdateData(TRUE);
		wBuff[0] = CommandList[CurSelIndex].CommandValue;
		wBuff[1] = ParamValue;
		SizeToSend = sizeof(UINT);
	}
	else
	{
		wBuff[0] = CommandList[CurSelIndex].CommandValue;
		SizeToSend = sizeof(UINT16);
	}
	//SizeSent = m_Client.SendToEx((LPCTSTR)wBuff, SizeToSend, Port, (LPCTSTR)IpAddr);
	SizeSent = SendCmdMessage(wBuff, SizeToSend);
#else
	// send out the string as-is
	SizeToSend = (Str.GetLength() + 1) * 2;
	SizeSent = m_Client.SendToEx((LPCTSTR)Str, SizeToSend, DEVICE_ETHERNET_PORT_TCP, (LPCTSTR)IPAddrStr);
#endif
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonMonitor()
{
	CButton *pMonitorButton = (CButton *)GetDlgItem(IDC_BUTTON_MONITOR);
	if(bMonitor)
	{
		pMonitorButton->SetWindowText((LPTSTR)L"START MONITOR");
		bMonitor = FALSE;
		//m_Client.bRxUDP = false;
		//pMonitorButton->SetState(FALSE);
	}
	else
	{
		pMonitorButton->SetWindowText((LPTSTR)L"STOP MONITOR");
		bMonitor = TRUE;
		CWnd *pEditResponse = GetDlgItem(IDC_EDIT_RESPONSE);
		pEditResponse->SetWindowTextW(L"");

		//m_Client.bRxUDP = true;
		//pMonitorButton->SetState(TRUE);
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonCapture()
{
	// TODO: Add your control notification handler code here
	wchar_t FileName[100];

	CButton *pCaptureButton = (CButton *)GetDlgItem(IDC_BUTTON_CAPTURE);
	CButton *pUpdateButton = (CButton *)GetDlgItem(IDC_BUTTON_UPDATE_SIGNALS);

	if(bCapture)
	{
		pCaptureButton->SetWindowText((LPTSTR)L"START CAPTURE");
		pCaptureButton->SetCheck(BST_UNCHECKED);
		bCapture = FALSE;
		CaptureFile.Close();
		pUpdateButton->EnableWindow(TRUE);
		//pCaptureButton->SetState(FALSE);
	}
	else
	{	
		GetDlgItem(IDC_EDIT_FILE_NAME)->GetWindowText((LPTSTR)FileName, 100);
		if(FileName[0] == 0)
		{
			MessageBox(L"Invalid Capture File Name");
			return;
		}
	
		if(CaptureFile.Open((LPCTSTR)FileName, CFile::modeCreate | CFile::modeWrite))
		{
			CButton *pCheckButton = (CButton *)GetDlgItem(IDC_CHECK_IN_ENGINEERING);
			CString HeaderString;
				
			if(pCheckButton->GetCheck() == BST_CHECKED)
			{
				HeaderString = L"Qo2,Qair,Qexh,Pinsp,Pexp,Pdrv,Pair,Po2,Pawy,ADC_Qo2,Pbar,DAC_Air,DAC_O2,DAC_Exh,ADC_Tair,ADC_Qexh,ADC_Qair,Sync";
			}
			else
			{
				HeaderString = GlobalHeaderString;
			}
			HeaderString += ENDLINE;
			CaptureFile.Write((LPCTSTR)HeaderString, HeaderString.GetLength() * 2);
			
			pCaptureButton->SetWindowText((LPTSTR)L"STOP CAPTURE");
			pCaptureButton->SetCheck(BST_CHECKED);
			bCapture = TRUE;
			pUpdateButton->EnableWindow(FALSE);
		}
		else
		{
			MessageBox(L"Failed open capture file");
		}
		//pCaptureButton->SetState(TRUE);
	}
}

//**********************************************************************************************//
void CTabOne::OnKickIdle()
{
	UpdateDialogControls(this, FALSE);
}

//**********************************************************************************************//
void CTabOne::OnUpdateButtonConnect(CCmdUI* pCmdUI)
{
	return;
}

//**********************************************************************************************//
void CTabOne::OnUpdateButtonCapture(CCmdUI* pCmdUI)
{
}

//**********************************************************************************************//
LRESULT CTabOne::OnShutdown(WPARAM x, LPARAM y)
{
	m_Client.Close();
	return 0;
}

//**********************************************************************************************//
void CTabOne::OnBnClickedCheckTcp()
{
	CButton *pCheckButton = (CButton *)GetDlgItem(IDC_CHECK_TCP);
	CButton *pConnectButton = (CButton *)GetDlgItem(IDC_BUTTON_CONNECT);
	
	if(pCheckButton->GetCheck() == BST_CHECKED)
	{
		if(!pConnectButton->IsWindowEnabled())
		{
			pConnectButton->EnableWindow(TRUE);
		}
	}
	else
	{
		if(pConnectButton->IsWindowEnabled())
		{
			pConnectButton->EnableWindow(FALSE);
		}
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonAddSig()
{
	CListBox *pListBoxAllSigs = (CListBox *)(GetDlgItem(IDC_LIST_ALL_SIGNALS));
	CListBox *pListBoxSelSigs = (CListBox *)(GetDlgItem(IDC_LIST_SELECTED_SIGNALS));

	//???if(pListBoxSelSigs->GetCount() < (int)AvailSignals.size())
	if(pListBoxSelSigs->GetCount() < MAX_SELECTED_SIGNALS)
	{
		int SelItem = pListBoxAllSigs->GetCurSel();
		if(SelItem >= 0)
		{
			bSelectedSignalsChanged = true;
			pListBoxAllSigs->DeleteString(SelItem);
		
			int NumSelItems = pListBoxSelSigs->GetCount();
			pListBoxSelSigs->InsertString(NumSelItems, AvailSignals[SelItem]);
			SelectedSignals.push_back(AvailSignals[SelItem]);		
			AvailSignals.erase(AvailSignals.begin() + SelItem);

			//UpdateGlobalHeaderString();
		}
	}
	else
	{
		MessageBox(L"Error: Already have maximum number of selected signals", L"Error", MB_ICONERROR);
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonRemoveSig()
{
	CListBox *pListBoxAvailSigs = (CListBox *)(GetDlgItem(IDC_LIST_ALL_SIGNALS));
	CListBox *pListBoxSelSigs = (CListBox *)(GetDlgItem(IDC_LIST_SELECTED_SIGNALS));

	int SelItem = pListBoxSelSigs->GetCurSel();
	if((SelItem >= 0) && (SelItem < pListBoxSelSigs->GetCount()))
	{
		bSelectedSignalsChanged = true;
		pListBoxSelSigs->DeleteString(SelItem);
		
		int NumSelItems = pListBoxAvailSigs->GetCount();
		pListBoxAvailSigs->InsertString(NumSelItems, SelectedSignals[SelItem]);
		AvailSignals.push_back(SelectedSignals[SelItem]);	
		SelectedSignals.erase(SelectedSignals.begin() + SelItem);

		//UpdateGlobalHeaderString();		
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonMoveUp()
{
	CListBox *pListBoxSelSigs = (CListBox *)(GetDlgItem(IDC_LIST_SELECTED_SIGNALS));

	int SelItem = pListBoxSelSigs->GetCurSel();
	if((SelItem > 0) && (SelItem < (pListBoxSelSigs->GetCount())))
	{
		bSelectedSignalsChanged = true;
		SwapSignalStrings(SelItem-1, SelectedSignals);
		pListBoxSelSigs->DeleteString(SelItem-1);
		pListBoxSelSigs->InsertString(SelItem, SelectedSignals[SelItem]);

		//UpdateGlobalHeaderString();
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonMoveDown()
{
	CListBox *pListBoxSelSigs = (CListBox *)(GetDlgItem(IDC_LIST_SELECTED_SIGNALS));

	int SelItem = pListBoxSelSigs->GetCurSel();
	if((SelItem >= 0) && (SelItem < (pListBoxSelSigs->GetCount() - 1)))
	{
		bSelectedSignalsChanged = true;
		SwapSignalStrings(SelItem, SelectedSignals);
		pListBoxSelSigs->DeleteString(SelItem);
		pListBoxSelSigs->InsertString(SelItem+1, SelectedSignals[SelItem+1]);
		pListBoxSelSigs->SetCurSel(SelItem+1);

		//UpdateGlobalHeaderString();
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonCreateGroup()
{
#if 1
	TCHAR GroupName[MAX_GROUP_NAME_LENGTH];
	CEdit *pEdit = (CEdit *)GetDlgItem(IDC_EDIT_GROUP_NAME);
	pEdit->GetWindowText(GroupName, MAX_GROUP_NAME_LENGTH);
	if(GroupName[0] == 0)
#else
	TCHAR GroupNameStr[MAX_GROUP_NAME_LENGTH];
	CString GroupName;

	CEdit *pEdit = (CEdit *)GetDlgItem(IDC_EDIT_GROUP_NAME);
	pEdit->GetWindowText(GroupNameStr, MAX_GROUP_NAME_LENGTH);
	GroupName = GroupNameStr;
	if(GroupName.IsEmpty())
#endif
	{
		MessageBox(L"Invalid group name to create", L"Error", MB_ICONERROR);
		return;
	}

	list<GROUP_DATA_TYPE>::iterator ListItr = GroupList.begin();
	list<GROUP_DATA_TYPE>::iterator ListItrEnd = GroupList.end();

	for(; ListItr != ListItrEnd; ListItr++)
	{
		if(GroupName == ListItr->GroupName)
		{
			MessageBox(L"Duplicate group name", L"Error", MB_ICONERROR);
			return;
		}
	}

	CString GroupStr(L":");
	GroupStr += GroupName;
	GroupStr += ENDLINE;

	if(GroupsFile.Open(GroupsFileName, CFile::modeWrite | CFile::typeText))
	{
		// must have at least 1 selected signal to add group
		if(SelectedSignals.size() > 0)
		{
			bSelectedSignalsChanged = true;
			GroupsFile.SeekToEnd();
			GroupsFile.WriteString(GroupStr);

			int Count;
			CListBox * pSelectedSigs = (CListBox *)(GetDlgItem(IDC_LIST_SELECTED_SIGNALS));
			Count = pSelectedSigs->GetCount();
			
			Count = SelectedSignals.size();
			for(int i=0; i<Count; i++)
			{
				GroupsFile.WriteString(SelectedSignals[i]);
				GroupsFile.WriteString(ENDLINE);
			}

			GroupsFile.Close();

			CComboBox *pGroupBox = (CComboBox *)GetDlgItem(IDC_COMBO_GROUP);
			pGroupBox->AddString(GroupName);
			pGroupBox->SetWindowTextW(GroupName);

			GROUP_DATA_TYPE NewGroup;
			NewGroup.GroupName = GroupName;
			NewGroup.Signals = SelectedSignals;
			GroupList.push_back(NewGroup);
			
			//UpdateGlobalHeaderString();
		}
		else
		{
			MessageBox(L"Selected Signal List is Empty", L"Error", MB_ICONERROR);
		}
	}
	else
	{
		MessageBox(L"Failed to open Group File to add group", L"Error", MB_ICONERROR);
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonPopulate()
{
	CString GroupName;
	CComboBox *pGroupBox = (CComboBox *)GetDlgItem(IDC_COMBO_GROUP);

	pGroupBox->GetWindowText(GroupName);
	if(GroupName == L"")
	{
		MessageBox(L"Invalid group name", L"Error", MB_ICONERROR);
	}
	else
	{
		bSelectedSignalsChanged = true;
		FindGroupAndFillList(GroupName);
		PopulateAvailSignalsList();
		//UpdateGlobalHeaderString();
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonModify()
{
	CString GroupNameToModify;
	CComboBox *pGroupBox = (CComboBox *)GetDlgItem(IDC_COMBO_GROUP);

	pGroupBox->GetWindowText(GroupNameToModify);
	CString TempStr = GroupNameToModify;
	TempStr.TrimRight();

	CString Msg;
	
	if(TempStr.Compare(DEFAULT_GROUP_NAME) == 0)
	{	
		MessageBox(L"Can not modify Default group!", L"Error", MB_ICONSTOP);
	}
	else
	{
		Msg.Format(L"Modify Group %s", GroupNameToModify);
		if(AfxMessageBox(Msg, MB_YESNO) == IDYES)
		{
			list<GROUP_DATA_TYPE>::iterator ListItr = GroupList.begin();
			list<GROUP_DATA_TYPE>::iterator ListItrEnd = GroupList.end();
				
			while(true)
			{
				CString CurGroupName = ListItr->GroupName;
		
				if(CurGroupName.Compare(GroupNameToModify) == 0)
				{
					ListItr->Signals.clear();
					for(int i = 0; i<(int)SelectedSignals.size(); i++)
					{
						ListItr->Signals.push_back(SelectedSignals[i]);
					}
					bSelectedSignalsChanged = true;
					bGroupChanged = true;
					
					//UpdateGlobalHeaderString();
					break;
				}
				else if(++ListItr == GroupList.end())
				{
					Msg.Format(L"Can not find Group %s to modify", GroupNameToModify);
					MessageBox(Msg, L"Error", MB_ICONSTOP);
					break;
				}
			}
		}
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonDelete()
{
	CString GroupNameToDelete;
	CComboBox *pGroupNameBox = (CComboBox *)GetDlgItem(IDC_COMBO_GROUP);

	pGroupNameBox->GetWindowText(GroupNameToDelete);
	CString TempStr = GroupNameToDelete;
	TempStr.TrimRight();

	if(TempStr.Compare(DEFAULT_GROUP_NAME) == 0)
	{	
		CString Msg(L"Can not delete Default group!");
		MessageBox(Msg, L"Error", MB_ICONSTOP);
	}
	else
	{
		CString Msg(L"Delete Group ");
		Msg += GroupNameToDelete;
		if(AfxMessageBox(Msg, MB_YESNO) == IDYES)
		{
			list<GROUP_DATA_TYPE>::iterator ListItr = GroupList.begin();
			list<GROUP_DATA_TYPE>::iterator ListItrEnd = GroupList.end();
			
			while(true)
			{
				CString CurGroupName = ListItr->GroupName;
	
				if(CurGroupName.Compare(GroupNameToDelete) == 0)
				{
					GroupList.erase(ListItr);
					pGroupNameBox->DeleteString(pGroupNameBox->GetCurSel());
					pGroupNameBox->SelectString(-1, DEFAULT_GROUP_NAME);				
					
					bGroupChanged = true;

					//UpdateGlobalHeaderString();
					break;
				}
				else if(++ListItr == GroupList.end())
				{
					break;
				}
			}
		}
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonBrowse()
{
	CFileDialog FileDlg(FALSE, L".txt", L"", 0, 
		L"Log Files (*.log; *.txt; *.csv) | *.log; *.txt; *.csv | All Files (*.*) |*.*||", NULL);
	if(FileDlg.DoModal() == IDOK)
	{
		CString path = FileDlg.GetPathName();
		CEdit* pEdit = reinterpret_cast<CEdit *>(GetDlgItem(IDC_EDIT_FILE_NAME));
		pEdit->SetWindowTextW(path);
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonDloadBrowse()
{
	CFileDialog FileDlg(TRUE, L".bin", L"", OFN_READONLY, 
		L"Binary Files (*.bin; *.nb0) | *.bin; *.nb0 | All Files (*.*) |*.*||", NULL);
	if(FileDlg.DoModal() == IDOK)
	{
		CString path = FileDlg.GetPathName();
		CEdit* pEdit = reinterpret_cast<CEdit *>(GetDlgItem(IDC_EDIT_DLOAD_FNAME));
		pEdit->SetWindowTextW(path);
	}
}

//**********************************************************************************************//
void CTabOne::OnBnClickedButtonDownloadFile()
{
	// TODO: Add your control notification handler code here
}


bool CTabOne::SetSelectedSignalNumbers()
{
	bool Success = true;
	UINT16 i;
	UINT16 NumSignals;
	
	NumSignals = SelectedSignals.size();

	for(i=0; i<NumSignals; i++)
	{
		CString signalName = SelectedSignals[i];
		signalName.TrimRight('\r');
		UINT16 SigNum = GetSignalNumber(signalName);
		
		if(SigNum > 0)
		{
			SelectedSignalNumbers[i] = SigNum;
		}
		else
		{
			CString Msg(L"Selected Signal Number = 0");
			MessageBox(Msg, L"Error", MB_ICONSTOP);
			Success = false;
			break;
		}
	}

	// Fill in 0 if number of selected signal < MAX_SELECTED_SIGNALS
	for(; i<MAX_SELECTED_SIGNALS; i++)
	{
		SelectedSignalNumbers[i] = 0;
	}
	return Success;
}

void CTabOne::OnBnClickedButtonUpdateSignals()
{
	wchar_t wBuff[100];
	int SizeToSend, SizeSent;

	// clear the response edit control
	CWnd *pEditResponse;
	pEditResponse = GetDlgItem(IDC_EDIT_RESPONSE);
	pEditResponse->SetWindowTextW(L"");

	int Port = BD_ETHERNET_PORT_UDP;
	CString IpAddr(L"192.168.0.3");

	wBuff[0] = PC_SET_SIGNALS_MSG_ID;
	if(SetSelectedSignalNumbers())
	{
		// Send Signal Numbers to target
		for(int i=0; i<MAX_SELECTED_SIGNALS; i++)
		{
			wBuff[i+1] = SelectedSignalNumbers[i];
		}
		SizeToSend = (MAX_SELECTED_SIGNALS * sizeof(UINT16)) + sizeof(UINT16);
		SizeSent = m_Client.SendToEx((LPCTSTR)wBuff, SizeToSend, Port, (LPCTSTR)IpAddr);
		
		UpdateGlobalHeaderString();
	}
}

void CTabOne::OnBnClickedButtonClearSignals()
{
	CListBox * pSelectedSigsList = (CListBox *)(GetDlgItem(IDC_LIST_SELECTED_SIGNALS));
	pSelectedSigsList->ResetContent();	
	SelectedSignals.clear();

	CListBox *pListBoxAvailSigs = (CListBox *)(GetDlgItem(IDC_LIST_ALL_SIGNALS));
	pListBoxAvailSigs->ResetContent();
	AvailSignals.clear();

	int SigCount = (int)SelectableSignals.size();
	for(int i=0; i<SigCount; i++)
	{
		pListBoxAvailSigs->AddString(SelectableSignals[i]);
		AvailSignals.push_back(SelectableSignals[i]);
	}
	bSelectedSignalsChanged = true;
}


void CTabOne::OnBnClickedCheckInEngineering()
{
	CButton *pCheckButton = (CButton *)GetDlgItem(IDC_CHECK_IN_ENGINEERING);
		
	if(pCheckButton->GetCheck() == BST_CHECKED)
	{
		SetGlobalHeaderString(EngineeringSignals);
		pTabTwo->PostMessageW(WM_HEADER_STRING, 0, (LPARAM)(LPCTSTR)GlobalHeaderString);
		pTabThree->PostMessageW(WM_HEADER_STRING, 0, (LPARAM)(LPCTSTR)GlobalHeaderString);
	}
	else
	{
		UpdateGlobalHeaderString();
	}	
}
