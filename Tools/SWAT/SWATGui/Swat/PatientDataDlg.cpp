// PatientDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Swat.h"
#include "PatientDataDlg.h"
#include "IPatientDataSet.h"
#include "WaveFormDlg.h"


// CPatientDataDlg dialog

IMPLEMENT_DYNAMIC(CPatientDataDlg, CDialog)

CPatientDataDlg::CPatientDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPatientDataDlg::IDD, pParent)
{
	m_iItemCount = 0;
}

CPatientDataDlg::~CPatientDataDlg()
{
}

void CPatientDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CONTROL, m_List);
}


BEGIN_MESSAGE_MAP(CPatientDataDlg, CDialog)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_CONTROL, &CPatientDataDlg::OnLvnItemchangedListControl)
	ON_NOTIFY(LVN_BEGINLABELEDIT, IDC_LIST_CONTROL, &CPatientDataDlg::OnLvnBeginlabeleditListControl)
	ON_MESSAGE(WM_VALIDATE, &CPatientDataDlg::OnEndLabelEditVariableCriteria)
	ON_MESSAGE(WM_SET_ITEMS, &CPatientDataDlg::PopulateComboList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CONTROL, &CPatientDataDlg::OnRclickList)
	ON_COMMAND(32786, &CPatientDataDlg::OnLstInsert)
	ON_WM_QUERYDRAGICON()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDCANCEL, &CPatientDataDlg::OnBnClickedCancel)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_WAVEFORM, &CPatientDataDlg::OnBnClickedButtonWaveform)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR, &CPatientDataDlg::OnBnClickedButtonClear)
	ON_BN_CLICKED(IDC_BUTTON_COPY_ID, &CPatientDataDlg::OnBnClickedButtonCopyID)
END_MESSAGE_MAP()
// CPatientDataDlg message handlers

void CPatientDataDlg::OnLvnItemchangedListControl(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}

BOOL CPatientDataDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here

	
	// Make the necessary initializations
	m_List.InsertColumn(0, "Parameter", LVCFMT_LEFT, 150);
	m_List.InsertColumn(1, "Value from BD", LVCFMT_LEFT, 150);
	m_List.InsertColumn(2, "Overwrite to GUI", LVCFMT_LEFT, 150);

	m_List.SetReadOnlyColumns(0);
	m_List.SetReadOnlyColumns(1);
	
	
	m_List.SetValidEditCtrlCharacters(CString("0123456789.-"));
	m_List.EnableVScroll(); 

	m_List.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);


	PopulateListBox();

	SetTimer(1234,500, NULL); //refresh every 500 ms.

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}



void CPatientDataDlg::InsertColumns()
{
	LV_COLUMN Column;
	int nCol;

	Column.mask = LVCF_TEXT|LVCF_WIDTH ;
	CString szText;
	
	szText = "Parameter";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	
	Column.cx = 135;
	nCol = m_List.InsertColumn( 0, &Column );


	Column.mask = LVCF_TEXT|LVCF_WIDTH ;
	
	szText = "Value from BD";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	
	Column.cx = 100;
	nCol = m_List.InsertColumn( 1, &Column );

	szText = "Override to GUI";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	
	Column.cx = 100;
	nCol = m_List.InsertColumn( 2, &Column );

}


void CPatientDataDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

void CPatientDataDlg::OnLvnBeginlabeleditListControl(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);


	// TODO: Add your control notification handler code here
	*pResult = 0;
}

HCURSOR CPatientDataDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

LRESULT CPatientDataDlg::OnEndLabelEditVariableCriteria(WPARAM wParam, LPARAM lParam) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)lParam;
	// TODO: Add your control notification handler code here
	
	if (wParam == IDC_LIST_CONTROL)
	{
		// Update the item text with the new text
		CString strUpdatedTxt = pDispInfo->item.pszText;  
		
		// Validate the format of the new string for the edit columns
		// If invalid then
		// Set pResult to 1 

		switch(pDispInfo->item.iSubItem)
		//switch(pDispInfo->item.iItem)
		{
		case 0:
		case 1:
			break;
		case 2:
			{

				OverWrite(); //if anything changed in column 2, send it for overwrite

				/*
				if (!strUpdatedTxt.IsEmpty()) 
				{

					
					// Get the left most non numeral characters
					// Get the remaining numerals
					// If the left most part does not contain the mandatory conditions 
					// Or the rest contains the mandatory conditions 
					// Display an error and set focus back onto the control
					int iFirstNumeralOccurrance = strUpdatedTxt.FindOneOf("0123456789.");
					CString strNonNumerals = strUpdatedTxt.Left(iFirstNumeralOccurrance);
					CString strNumerals = strUpdatedTxt.Mid(iFirstNumeralOccurrance);
					
					strNonNumerals.TrimLeft(); 
					strNonNumerals.TrimRight();
					strNumerals.TrimLeft();
					strNumerals.TrimRight();  

					int iDecimalIndex = strNumerals.Find(".");
					
					if ((-1 != iDecimalIndex) && (iDecimalIndex != (strNumerals.GetLength() - 1)))  
					{
						iDecimalIndex = strNumerals.Find(".", iDecimalIndex + 1);
					}

					// Check that the condition is either
					// Not empty or "=" or "!=" or "<" or ">" or "<=" or ">="
					// The Numerals do not contain 
					// Space or more than 1 "." or any of the conditions
					if (
						(!strNonNumerals.IsEmpty()) || 
						(-1 != strNumerals.Find(" ")) ||
						(-1 != iDecimalIndex)
						)
					{
						AfxMessageBox("Invalid text entered. The text will be reset"); 
						m_List.SetItemText(pDispInfo->item.iItem, pDispInfo->item.iSubItem, "");
					}
					return 0;
				}
				*/
			}
			break;
		}
	}

	return 1;
}


LRESULT CPatientDataDlg::PopulateComboList(WPARAM wParam, LPARAM lParam)
{
	LOG_MESSAGE(LM_INFO,"Enter CPatientDataDlg::PopulateComboList() ");

	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();     

	// Get the Combobox window pointer
	CComboBox* pInPlaceCombo = static_cast<CComboBox*> (GetFocus());

	// Get the inplace combbox top left
	CRect obWindowRect;

	pInPlaceCombo->GetWindowRect(&obWindowRect);

	CPoint obInPlaceComboTopLeft(obWindowRect.TopLeft()); 
	
	// Get the active list
	// Get the control window rect
	// If the inplace combobox top left is in the rect then
	// The control is the active control
	m_List.GetWindowRect(&obWindowRect);
	
	int iRowIndex = wParam;
	
	CStringList* pComboList = reinterpret_cast<CStringList*>(lParam);
	pComboList->RemoveAll(); 

	if (obWindowRect.PtInRect(obInPlaceComboTopLeft)) 
	{
		pEl=pPd->getPatientDataElement(iRowIndex);
		if(pEl->isEnum())
		{
			char *t=NULL;
            for(int cnt=0;t=pEl->getEnum(cnt);cnt++)
			{
				pComboList->AddTail(t);
			}
			pComboList->AddTail("");//add one blank element to deselect
		}
	}
	LOG_MESSAGE(LM_INFO,"Exit CPatientDataDlg::PopulateComboList() ");

	return true;

}


void CPatientDataDlg::OnRclickList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	/* Create the pop up menu
	CMenu obMenu;
	obMenu.LoadMenu(IDR_LIST_POPUP); 

	CMenu* pPopupMenu = obMenu.GetSubMenu(0);
	ASSERT(pPopupMenu); 
	
	// Get the cursor position
	CPoint obCursorPoint = (0, 0);
	
	GetCursorPos(&obCursorPoint);

	if (0 >= m_List.GetSelectedCount())
	{
		pPopupMenu->EnableMenuItem(ID_LST_DELETE, MF_BYCOMMAND | MF_GRAYED | MF_DISABLED); 
	}
	else
	{
		pPopupMenu->EnableMenuItem(ID_LST_DELETE, MF_BYCOMMAND | MF_ENABLED);
	}

	// Track the popup menu
	pPopupMenu->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON, obCursorPoint.x, 
									obCursorPoint.y, this);
	*/
	*pResult = 0;
}
void CPatientDataDlg::OnLstInsert()
{
	//NOT USED CURRENTLY
/*
	// TODO: Add your command handler code here
	// Insert a new row below the selected row or at the end
	CString strResource;
	strResource.Format("%d", ++m_iItemCount);

	int iCount = m_List.GetItemCount();

	int iRow = -1;

	POSITION SelectedItemPos = m_List.GetFirstSelectedItemPosition();
	int iIndex = -1;
	int iLastSelectedIndex = -1;

	while (SelectedItemPos)
	{
		iIndex = m_List.GetNextSelectedItem(SelectedItemPos);

		if ((LVIS_SELECTED == m_List.GetItemState(iIndex, LVIS_SELECTED)) && 
			(LVIS_FOCUSED == m_List.GetItemState(iIndex, LVIS_FOCUSED)))
		{
			iRow = iIndex;
		}
		m_List.SetItemState(iIndex, 0, LVIS_SELECTED|LVIS_FOCUSED);
	}


	if (-1 == iRow)
	{
		m_List.InsertItem(LVIF_TEXT|LVIF_STATE, iCount, 
			strResource, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0, 0);
	}
	else
	{
		m_List.InsertItem(LVIF_TEXT|LVIF_STATE, iRow, 
			strResource, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0, 0);
	}
	*/

}



void CPatientDataDlg::OverWrite()
{
	LOG_MESSAGE(LM_INFO,"Enter CPatientDataDlg::OverWrite() ");

	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();     
	CString strNewValue;
	int nTotalRows = m_List.GetItemCount();

	
	//for each item in PD, check if it has been overwritten in the listbox
	for(int row_cnt=0;pEl=pPd->getPatientDataElement(row_cnt);row_cnt++)
	{
		strNewValue = m_List.GetItemText(row_cnt, 2);
		//if there is overwritten value, send that to GUI
		if(!strNewValue.IsEmpty())
		{
			if(pEl->isFloat())
		    {
				if(pEl->isValid())
				{
					float f = static_cast<float>( atof(strNewValue) );
					pEl->setFloat(f);
				}
		    }
		    else if(pEl->isEnum())
		    {
				if(pEl->isValid())
				{
					char *t=NULL;
					for(int cnt=0;t=pEl->getEnum(cnt);cnt++)
					{
						if(strNewValue.CompareNoCase(t)==0)
						{
							pEl->setEnum(cnt);
						}
					}
				}
			}
		    else if(pEl->isInt())
		    {
				if(pEl->isValid())
				{
					int i = atoi(strNewValue);
					pEl->setInt(i);
				}
		    }
		}
		else
		{
			pEl->unsetValue(); //if the string is blank, unset the value
		}
	}
	LOG_MESSAGE(LM_INFO,"Exit CPatientDataDlg::OverWrite() ");

}

void CPatientDataDlg::RefreshBDValues()
{
	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();     

   for(int row_cnt=0;pEl=pPd->getPatientDataElement(row_cnt);row_cnt++)
   {
	   char *pName=pEl->getName();
	   bool bEnumDef=false;
	   char szValue[300];
	   szValue[0]='\0';
	   if(pEl->isFloat())
	   {
			if(pEl->isValid())
			{
				float f=pEl->getFloatValue();
				sprintf(szValue, "%f",f);
			}
	   }
	   else if(pEl->isEnum())
	   {
			if(pEl->isValid())
			{
				char *e=pEl->getEnumValue();
				if(e)
				{
					sprintf(szValue, "%s",e);
				}
			}
			bEnumDef = true;
	   }
	   else if(pEl->isInt())
	   {
			if(pEl->isValid())
			{
				int i=pEl->getIntValue();
				sprintf(szValue, "%i",i);
			}
	   }

	   //set the new BD value in the listbox only if different than the current display
	   CString strCurrentVal = m_List.GetItemText(row_cnt, 1); 
	   if(strCurrentVal.CompareNoCase(szValue)!= 0)
	   {
			m_List.SetItemText(row_cnt, 1, szValue); 
	   }
	}
   
}
void CPatientDataDlg::PopulateListBox()

{
	LOG_MESSAGE(LM_INFO,"Enter CPatientDataDlg::PopulateListBox() ");

   IPatientDataEl  *pEl;
   IPatientDataSet *pPd=getPatientDataSetInstance();     

   for(int row_cnt=0;pEl=pPd->getPatientDataElement(row_cnt);row_cnt++)
   {
       char *pName=pEl->getName();
       bool bEnumDef=false;
       char szValue[300];
       szValue[0]='\0';

       if(pEl->isFloat())
	   {
            if(pEl->isValid())
			{
                float f=pEl->getFloatValue();
                sprintf(szValue, "%f",f);
            }

       }
       else if(pEl->isEnum())
	   {
		   m_List.SetComboRows(row_cnt);//This is an ENUM variable, set it show combo box rather than int/float

            if(pEl->isValid())
			{
                char *e=pEl->getEnumValue();
                if(e)
				{
                    sprintf(szValue, "%s",e);
                }
            }
            bEnumDef = true;
       }
       else if(pEl->isInt())
	   {
            if(pEl->isValid())
			{
                int i=pEl->getIntValue();
                sprintf(szValue, "%i",i);
            }
       }

	   m_List.InsertItem(row_cnt, pName);      //Parameter name
	   m_List.SetItemText(row_cnt, 1, szValue); //BD value

   }
   LOG_MESSAGE(LM_INFO,"Exit CPatientDataDlg::PopulateListBox() ");
}
void CPatientDataDlg::OnBnClickedCancel()
{
	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();     

	for(int row_cnt=0;pEl=pPd->getPatientDataElement(row_cnt);row_cnt++)
	{
		//if there is overwritten value, unset that
		if(!(m_List.GetItemText(row_cnt, 2)).IsEmpty())
		{
			pEl->unsetValue(); //unset the value sent to GUI
		}
		
	}
	//clear all settings before calling cancel
	OnCancel();
}

void CPatientDataDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default

	RefreshBDValues();

	CDialog::OnTimer(nIDEvent);
}

void CPatientDataDlg::OnBnClickedButtonWaveform()
{
	CWaveFormDlg dlg;
	dlg.DoModal();
}

BOOL CPatientDataDlg::PreTranslateMessage(MSG* pMsg)
{
	// Avoid closing the dialog box with enter key

	HWND hWnd;
	GetDlgItem(IDOK, &hWnd);
	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN) && (pMsg->hwnd != hWnd))
	{
		TRACE("VK_RETURN\n");
		TRACE("OK button\n");
		return FALSE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CPatientDataDlg::OnBnClickedButtonClear()
{
	LOG_MESSAGE(LM_INFO,"Entering CPatientDataDlg::OnBnClickedButtonClear() ");
	//unset all values sent to GUI and clear the list control
	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();     

	for(int row_cnt=0;pEl=pPd->getPatientDataElement(row_cnt);row_cnt++)
	{
		m_List.SetItemText(row_cnt, 2, "");
		pEl->unsetValue();
				
	}
	LOG_MESSAGE(LM_INFO,"Exit CPatientDataDlg::OnBnClickedButtonClear() ");
}

void CPatientDataDlg::OnBnClickedButtonCopyID()
{
	
	int n = m_List.GetSelectionMark();
	if(n == -1) 
	{
		AfxMessageBox("select the item you want to copy and retry");
	}
	CString strLine = m_List.GetItemText(n, 0);
	

	//copy strLine to the clipboard
	HGLOBAL hMem;
	hMem = GlobalAlloc(GMEM_FIXED, strLine.GetLength()+1);
	if(hMem == NULL) { 
		AfxMessageBox("GlobalAlloc failed.");
		return;
	}
	strcpy((char *) GlobalLock(hMem), strLine.GetBuffer());
	strLine.ReleaseBuffer();
	GlobalUnlock(hMem);
	::OpenClipboard(m_hWnd);
	EmptyClipboard();
	SetClipboardData(CF_TEXT, hMem);
	CloseClipboard();
}
