//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file STDLogger.h
//----------------------------------------------------------------------------

#ifndef STDLOGGER_H
#define STDLOGGER_H

#include "ILogger.h"
#include <stdio.h>

class STDLogger:public ILogger
{
    public:
    STDLogger(FILE *pfp=stderr);

    STDLogger::STDLogger(char *pName);

    ~STDLogger();

    
    virtual void logMessage(unsigned int line, int logtype, char *filename,
            char *str, ...);


    private:

    bool m_bCreated;
    FILE *m_fp;


};


#endif

