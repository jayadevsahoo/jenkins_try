#ifndef LowerOtherScreensSubScreen_HH
#define LowerOtherScreensSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LowerOtherScreensSubScreen - The subscreen selected by the
// Other Screens button on the Lower screen.  Allows access to the
// Communications Setup, Date/Time Settings and More Settings subscreens.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LowerOtherScreensSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: rhj   Date: 02-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Modified to display PROX setup subscreen when the prox 
//      board is installed and neonatal is the current vent's circuit type.
// 
//  Revision: 004   By: rhj    Date: 17-Feb-2010   SCR Number: 6436
//  Project: PROX
//  Description:
//      Added a Prox setup button.
// 
//  Revision: 003   By: rhj    Date:  19-Sept-2006  SCR Number: 6236 
//  Project: RESPM
//  Description:
//      Add the ability to disable or enable RESPM.
// 
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"

//@ Usage-Classes
#include "Box.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
#include "BdEventRegistrar.hh"
#include "BdEventTarget.hh"
//@ End-Usage

class LowerOtherScreensSubScreen : public SubScreen,
	                               public BdEventTarget
{
public:
	LowerOtherScreensSubScreen(SubScreenArea *pSubScreenArea);
	~LowerOtherScreensSubScreen(void);

	// Overload SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);

	// BdEventTarget virtual method
	virtual void bdEventHappened(EventData::EventId eventId,
								 EventData::EventStatus eventStatus,
								 EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);



	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	LowerOtherScreensSubScreen(void);								// not implemented...
	LowerOtherScreensSubScreen(const LowerOtherScreensSubScreen&);	// not implemented...
	void operator=(const LowerOtherScreensSubScreen&);				// not implemented...

	//@ Data-Member: commSetupButton_;
	// A text button which activates the Communcation Setup subscreen.
	TextButton commSetupButton_;

	//@ Data-Member: dateTimeSettingsButton_;
	// A text button which activates the Date/Time settings subscreen.
	TextButton dateTimeSettingsButton_;

	//@ Data-Member: moreSettingsButton_;
	// A text button which activates the More Settings subscreen.
	TextButton moreSettingsButton_;

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
    //@ Data-Member: respMechTitle_
    // The text title for the respiratory mechanics area
    TextField respMechTitle_;

    //@ Data-Member: respMechTitle_
    // The text not available message for the respiratory mechanics
    TextField respMechNotAvailMsg_;

    //@ Data-Member: respMechBox_
    // The box around the respiratory mechanics area
    Box respMechBox_;

    //@ Data-Member: nifManeuverButton_;
    // A text button which activates the NIF maneuver panel
    TextButton nifManeuverButton_;

    //@ Data-Member: p100ManeuverButton_;
    // A text button which activates the P0.1 maneuver panel
    TextButton p100ManeuverButton_;

    //@ Data-Member: svcManeuverButton_;
    // A text button which activates the SVC maneuver panel
    TextButton svcManeuverButton_;

    //@ Data-Member: proxSetupButton_;
    // A text button which activates the Prox setup Subscreen
    TextButton proxSetupButton_;
};

#endif // LowerOtherScreensSubScreen_HH 
