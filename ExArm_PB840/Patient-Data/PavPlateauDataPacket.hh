#ifndef PavPlateauDataPacket_HH
#define PavPlateauDataPacket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct : PavPlateauDataPacket - structure definition of patient
//   data packet containing PAV data that are only updated every
//   plateau breath.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PavPlateauDataPacket.hhv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: sah    Date:  18-Jan-2001    DR Number: 5838
//	Project:  PAV
//	Description:
//		Split up original 'ComputedPavDataPacket' putting those PAV
//      data items that are only updated every plateau breath in
//      this structure.
//
//====================================================================

#include "Sigma.hh"
#include "PavState.hh"

// @Type: PavPlateauDataPacket
struct   PavPlateauDataPacket
{
	Real32  lungElastance;
	Real32  lungCompliance;
	Real32  patientResistance;
	Real32  totalResistance;
	PavState::Id  pavState;

	void convHtoN()
	{
		SocketWrap sock;
		lungElastance = sock.HTONF(lungElastance);
		lungCompliance = sock.HTONF(lungCompliance);
		patientResistance = sock.HTONF(patientResistance);
		totalResistance = sock.HTONF(totalResistance);
		pavState = (PavState::Id)sock.HTONL((Uint32)pavState);
	}

	void convNtoH()
	{
		SocketWrap sock;
		lungElastance = sock.NTOHF(lungElastance);
		lungCompliance = sock.NTOHF(lungCompliance);
		patientResistance = sock.NTOHF(patientResistance);
		totalResistance = sock.NTOHF(totalResistance);
		pavState = (PavState::Id)sock.NTOHL((Uint32)pavState);
	}
};

#endif // PavPlateauDataPacket_HH 

