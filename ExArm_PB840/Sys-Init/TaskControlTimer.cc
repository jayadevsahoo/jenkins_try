#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TaskControlTimer - Base class for all Task-Control task
//                            timers.
//---------------------------------------------------------------------
//@ Interface-Description
//  TaskControlTimer is a virtual base class that handles timers for
//  Task Control.  Task Control contructs a TaskControlTimer for each 
//  timed event.  The TaskControlTimer contains the MsgTimer which
//  queues this object to the task control queue when the timer
//  expires.  The set() and cancel() methods provide access to the
//  embedded MsgTimer to start or stop the timer.
//---------------------------------------------------------------------
//@ Rationale
//  The TaskControlTimer base class is Task Control's timer/timeout
//  facility.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The base class constructor initializes its MsgTimer and 
//  TaskControlQueueMsg.  The set() function
//  starts the encapsulated MsgTimer using MsgTimer::set.  The cancel()
//  function messages MsgTimer::cancel to stop the timer.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskControlTimer.ccv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Modified to queue derived TaskControlTimers to a specified
//         queue so so TaskControl can process them using different 
//         contexts.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TaskControlTimer.hh"
#include <string.h>
#include "IpcIds.hh"

//@ Usage-Classes
#include "AppContext.hh"
#include "BufferPool.hh"
#include "MsgTimer.hh"
#include "TaskControlQueueMsg.hh"
#include "MemoryHandle.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isExpired
//
//@ Interface-Description
//  Determines if the period for the timer has transpired. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
Boolean
TaskControlTimer::isExpired(void) const
{
    CALL_TRACE("isExpired(void)");
    
    if (!active_)       // $[TI1]
    {
        return FALSE;
    }
    // $[TI2]

    if (!timeSet_.isValid())  // $[TI3]
    {
        return FALSE;
    }
    // $[TI4]

    return timeSet_.diffTime(OsTimeStamp::CurrentTime()) >= duration_; 
    // $[TI5] $[TI6]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: set
//
//@ Interface-Description
//  Start the message timer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Messages MsgQueue::set to start the private timer_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControlTimer::set(void)
{
    CALL_TRACE("set(void)");

    active_ = TRUE;
    timeSet_ = OsTimeStamp::CurrentTime();
    timer_.set();
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: cancel
//
//@ Interface-Description
//  Cancel the message timer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Messages MsgTimer::cancel to cancel the private timer_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControlTimer::cancel(void)
{
    CALL_TRACE("cancel(void)");
    active_ = FALSE;
    timer_.cancel();
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskControlTimer [constructor]
//
//@ Interface-Description
//  The TaskControlTimer base class constructor is a protected member.
//  It creates a TaskControlQueueMsg for this TaskControlTimer and
//  creates a MsgTimer with this message and the specified timeout
//  value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is protected allowing access only by TaskControlTimer
//  and its derived classes.  Initializes a TaskControlQueueMsg with
//  the address of this TaskControlTimer.  Constructs the private 
//  MsgTimer timer_ with the TaskControlQueueMsg and specified timeout 
//  value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskControlTimer::TaskControlTimer(  Sys_Init::InitClassId classId
                                   , const Int32 timeout
                                   , const Int32 queueNumber )
    :  classId_(classId)
	 , msg_(TaskControlQueueMsg::TASK_CONTROL_TIMER, MemoryHandle::CreateHandle(this))
     , timer_(queueNumber, msg_.getQueueData(), timeout)
     , duration_(timeout)
     , timeSet_()
     , active_(FALSE)
{
    CALL_TRACE("TaskControlTimer(void)");
    timeSet_.invalidate();
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TaskControlTimer [destructor]
//
//@ Interface-Description
//  TaskControlTimer destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskControlTimer::~TaskControlTimer()
{
    CALL_TRACE("~TaskControlTimer()");
}



#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
// Method: isA
//
// Interface-Description
//  Returns the class identifier.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
Sys_Init::InitClassId
TaskControlTimer::isA(void) const
{
    CALL_TRACE("isA(void)");
    return classId_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method: getMsg
//
// Interface-Description
//  Returns the internal TaskControlQueueMsg.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
TaskControlQueueMsg
TaskControlTimer::getMsg(void) const
{
    CALL_TRACE("getMsg(void)");
    return msg_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method: getMsgTimer
//
// Interface-Description
//  Returns a reference to the internal MsgTimer.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const MsgTimer &
TaskControlTimer::getMsgTimer(void) const
{
    CALL_TRACE("getMsgTimer(void)");
    return timer_;
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
TaskControlTimer::SoftFault(const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_TASKCONTROLTIMER, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

#endif  // SIGMA_UNIT_TEST
//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

