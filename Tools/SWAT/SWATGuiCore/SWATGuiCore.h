// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SWATGUICORE_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SWATGUICORE_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef SWATGUICORE_EXPORTS
#define SWATGUICORE_API __declspec(dllexport)
#else
#define SWATGUICORE_API __declspec(dllimport)
#endif

struct lua_State;

extern "C" {
	int SWATGUICORE_API luaopen_swat_guicore(lua_State* L);
}