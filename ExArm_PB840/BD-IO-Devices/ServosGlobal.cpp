#include "stdafx.h"
#include "ServosGlobal.h"

InspValveCalibrationTable_t *pAirValve = NULL;
InspValveCalibrationTable_t *pO2Valve = NULL;
ExhFlowSensorCalibrationTable_t *pExhFlowSensorCal = NULL;
ExhValveCalibrationTable_t *pExhValve = NULL;
TransducerCalibrationTable_t *pP1Transducer = NULL;
TransducerCalibrationTable_t *pP2Transducer = NULL;
TransducerCalibrationTable_t *pPdrvTransducer = NULL;
TransducerCalibrationTable_t *pAirInletPressureTransducer = NULL;
TransducerCalibrationTable_t *pO2InletPressureTransducer = NULL;

CalibrationTable_t *CalibrationObj[NUM_CAL_IDS];
CommandValues_t CommandValues;
