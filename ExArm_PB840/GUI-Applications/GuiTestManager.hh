#ifndef GuiTestManager_HH
#define GuiTestManager_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

//====================================================================
// Class: GuiTestManager - Generic service test class which handles the
// housekeeping procedure needed by the test.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiTestManager.hhv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


//@ Usage-Classes
#include "TestResult.hh"
#include "TestResultTarget.hh"
#include "SmTestId.hh"
#include "SmPromptId.hh"
#include "SmDataId.hh"
#include "ServicePromptMgr.hh"
#include "GuiAppClassIds.hh"

class GuiTestManagerTarget;
//@ End-Usage


class GuiTestManager : public TestResultTarget,
						public ServicePromptMgr
{
public:

	GuiTestManager(GuiTestManagerTarget *pTarget);
	virtual ~GuiTestManager(void);

	void setupTestResultDataArray(void);
	void setupTestCommandArray(void);

	void registerTestResultData(void);
	void registerTestCommands(void);

	// TestResultTarget virtual methods
	virtual void updateChangedData(TestResult *pResult);

	inline void setPrecision(SmDataId::ItemizedTestDataId dataId, Int precision);
	
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	GuiTestManager(const GuiTestManager&);	// not implemented...
	void operator=(const GuiTestManager&);		// not implemented...

	void processGuiTestPrompt_(Int command);
	void processGuiTestKeyAllowed_(Int keyAllowed);
	void processGuiTestResultStatus_(Int resultStatus);
	void processGuiTestResultCondition_(Int resultCondition);
	void processGuiTestData_(Int dataIndex, TestResult *pResult);
	
	
	//@ Data-Member: pTarget_
	// A pointer to the GuiTestManagerTarget object which processes the
	// prompt, status, and data received from the Service Mode test.
	GuiTestManagerTarget* pTarget_;

	//@ Data-Member: pCommand_
	// The ServiceDataTarget object which handles the command type of test
	// result.
	TestResult *pCommand_;

	//@ Data-Member: pKeyAllowed_
	// The ServiceDataTarget object which handles the keyAllowed type of test
	// result.
	TestResult *pKeyAllowed_;

	//@ Data-Member: pStatus_
	// The ServiceDataTarget object which handles the status type of test
	// result.
	TestResult *pStatus_;

	//@ Data-Member: pCondition_
	// The ServiceDataTarget object which handles the condition type of test
	// result.
	TestResult *pCondition_;

	//@ Data-Member: testResultDataMemory_
	// The memory reserved for an array of ServiceDataTarget objects which
	// handles the data types of test result.
	Int testResultDataMemory_[((sizeof (TestResult) + sizeof (Int) - 1) /
							sizeof (Int)) *
							TestDataId::MAX_DATA_ENTRIES];

	//@ Data-Member: testResultCommandsMemory_
	// The memory reserved for an array of ServiceDataTarget objects which
	// handles the command types of test result.
	Int testResultCommandsMemory_[((sizeof (TestResult) + sizeof (Int) - 1) /
							sizeof (Int)) *
							TestDataId::MAX_PROMPT_ENTRIES];

	//@ Data-Member: testResultStatusMemory_
	// The memory reserved for an array of ServiceDataTarget objects which
	// handles the command types of test result.
	Int testResultStatusMemory_[((sizeof (TestResult) + sizeof (Int) - 1) /
							sizeof (Int)) *
							TestDataId::MAX_STATUS_ENTRIES];

	//@ Data-Member: serviceModeTestTypeId_
	//  Identifier to hold the service mode test type.
	SmTestId::ServiceModeTestTypeId serviceModeTestTypeId_;

	//@ Data-Member: *pTestResultDataArray_
	// A TestResult pointer which will be associated with testResultDataMemory_
	TestResult *pTestResultDataArray_;

	//@ Data-Member: *pTestResultCommandArray_
	// A TestResult pointer which will be associated with testResultCommandsMemory_
	TestResult *pTestResultCommandArray_;

	//@ Data-Member: *pTestResultStatusArray_
	// A TestResult pointer which will be associated with testResultStatusMemory_
	TestResult *pTestResultStatusArray_;

};

// Inlined methods
#include "GuiTestManager.in"

#endif // GuiTestManager_HH 
