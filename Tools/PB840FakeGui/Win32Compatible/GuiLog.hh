#pragma once

// allow redirect these log messages from different threads to UI
class GuiLog
{
public:
	GuiLog(void);
	~GuiLog(void);

	// for 'PRINTF' macro to invoke
	static void printf(const char* format, ...);

	// for PULL, not used currently
	static const char* retrieve();

private:

	static void append_(const char* p);

	static deque<const char *> loglist_;
};
