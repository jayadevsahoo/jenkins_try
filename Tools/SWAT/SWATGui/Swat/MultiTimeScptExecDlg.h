#pragma once


// CMultiTimeScptExecDlg dialog

class CMultiTimeScptExecDlg : public CDialog
{
	DECLARE_DYNAMIC(CMultiTimeScptExecDlg)

public:
	CMultiTimeScptExecDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMultiTimeScptExecDlg();

// Dialog Data
	enum { IDD = IDD_MULTI_TIMES_SCRT_EXE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	int m_nNumberTimes;
	afx_msg void OnBnClickedOk();
};
