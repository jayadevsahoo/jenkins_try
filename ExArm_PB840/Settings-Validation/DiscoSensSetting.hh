
#ifndef DiscoSensSetting_HH
#define DiscoSensSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  DiscoSensSetting - Disconnection Sensitivity Setting 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DiscoSensSetting.hhv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By:   rhj    Date: 13-July-2010  SCR Number: 6581 
//  Project:  XENA2
//  Description:
//       Added findSoftMinMaxValues_() method
// 
//  Revision: 005   By: rhj   Date: 19-Jan-2009    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 004   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"

//@ End-Usage


class DiscoSensSetting : public BatchBoundedSetting
{
  public:
    DiscoSensSetting(void); 
    virtual ~DiscoSensSetting(void);

    virtual Applicability::Id  getApplicability(
				      const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

	virtual void  acceptTransition(const SettingId::SettingIdType settingId,
								   const DiscreteValue            newValue,
								   const DiscreteValue            currValue);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

	virtual const BoundStatus*  acceptPrimaryChange(
				    const SettingId::SettingIdType primaryId
						   );
  protected:
	virtual void  updateConstraints_(void);
    virtual Real32  getAbsoluteMaxValue_(void) const;
    virtual void  findSoftMinMaxValues_(Real32& rSoftMinValue,
					Real32& rSoftMaxValue) const;

  private:
    DiscoSensSetting(const DiscoSensSetting&);	// not implemented...
    void  operator=(const DiscoSensSetting&);	// not implemented...
												 
												
};


#endif // DiscoSensSetting_HH 
