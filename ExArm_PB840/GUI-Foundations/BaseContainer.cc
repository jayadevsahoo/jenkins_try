#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BaseContainer - A specialized Container which is at the top
// of the container tree.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is used as a master container for all containers and graphics
// (all Drawables) located on a given screen.  It serves as the root of the
// containment tree for a given screen.  The upper and lower screens each use a
// BaseContainer (the GuiFoundation::GetLowerBaseContainer() and the
// GuiFoundation::GetUpperBaseContainer()) to contain all objects displayed within
// them.  These are the only two instances of this class; these are created by
// the GuiFoundation::Intialize() method, using the "placement new" technique
// to create them within pre-allocated memory buffers.  No other instances
// are allowed.  To prevent any instances other than these two, the
// constructor for BaseContainer is defined as a private method.  Friendship
// is granted to GuiFoundation::Intialize() to allow it to construct the
// two BaseContainers.  These are the only non-static objects provided by
// the GUI-Foundations subsystem.
//
// BaseContainer has the responsibility for managing screen repainting; to do
// this, it tracks which Drawables have been changed.  All graphics rendering
// is triggered by calls from outside this subsystem.  The application code
// triggers a repaint by calling the repaint() method.  BaseContainer is also
// involved in touch processing; it serves as a starting point for the search
// to determine which Drawable has been touched.  To accomplish these
// functions, it collaborates with parent classes Container and Drawable,
// and with the Touch class.
//---------------------------------------------------------------------
//@ Rationale
// This class is needed to encapsulate high-level screen management
// functions such as repainting the screen and keeping track of which
// Drawables have been changed.  It adds the minimum amount of
// functionality to achieve this aim, leaving as much as possible to the
// individual screen objects and its parent class, Container.
//---------------------------------------------------------------------
//@ Implementation-Description
// The BaseContainer class elaborates on its parent class mainly by
// adding the repaint() method, along with a list of references to
// Drawables which it uses to track which Drawables have been changed.
// This is used by the application code when a repaint is needed.  A
// private data element is also added to specify which screen the
// BaseContainer is managing (upper or lower).
//
// The repaint() method uses the list of changed Drawables to produce a
// list of changed screen areas, which includes both the new and old
// screen areas for each changed Drawable.  Overlapping areas in this
// list are merged, thus shortening the list.  Finally, the draw()
// method of the BaseContainer (inherited from Container) is called once
// for each area on the list, using that area as a clipping area.  This
// method redraws all contained Drawables which overlap with the
// clipping area, and calls draw() for each contained Container that
// overlaps the clipping area.  This recursion results in a complete
// traversal down the containment tree.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/BaseContainer.ccv   25.0.4.0   19 Nov 2013 14:11:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 013   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 012   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 010   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 009  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 008  By: sah       Date: 06-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Added the requesting and releasing of the VGA interface access
//      around 'setScreen_()' and all of the 'draw()' calls, to prevent
//      multi-thread problems with drawing (as noted in this DCS).
//
//  Revision: 007  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 006  By: yyy       Date: 17-Jan-1997    DCR Number: 1526
//    Project:  Sigma (R8027)
//    Description:
//      Added isRepaintNeeded()
//
//  Revision: 005  By: mpm       Date: 29-Mar-1996    DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Removed calls to RequestDisplayAccess() and ReleaseDisplayAccess().  This
//      is now done at a lower level, e.g. with the Drawable::draw() methods.
//
//  Revision: 004  By: yyy       Date: 12-July-1995   DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Removed isTaskingOn() call before accessing the VGA display due to
//      the recent changes in the tasking initialization.
//
//  Revision: 003  By: yyy       Date: 06-June-1995   DCR Number: 473 
//    Project:  Sigma (R8027)
//    Description:
//      Call IsTaskingOn() before sending command to request or release DisplayAccess
//      in repaint().
//
//  Revision: 002  By: yyy       Date: 02-June-1995   DCR Number: 473 
//    Project:  Sigma (R8027)
//    Description:
//      Added calls to RequestDisplayAccess() and ReleaseDisplayAccess() in repaint().
//      This should synchronize with the Blink task in accessing the VGA display
//      memory.
//  
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "BaseContainer.hh"

#include "Task.hh"
#include "DisplayContext.hh"
#include "VgaDisplayContext.hh"
#include "GraphicsContext.hh"

//@ Usage-Classes
#include "TemplateMacros.hh"
#include "SListC_Area_MAX_AREA_LIST_NUM.hh"

#if defined( SIGMA_GUIPC_CPU )
#include "SIterR_Drawable.hh"
#endif // SIGMA_GUIPC_CPU

//@ End-Usage

//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFillColor    [virtual]
//
//@ Interface-Description
// This is a redefined version of the setFillColor() method inherited from
// parent class Container. It sets the fill ("interior" or "background") color
// for this BaseContainer to the passed "fillColor".  Note that it is not
// legal to set the fill color to Colors::NO_COLOR.
//---------------------------------------------------------------------
//@ Implementation-Description
// Because fillColor_ is private, it must be set via the parent class
// setFillColor() method.  This simply sets the internal fillColor_ member to
// the given "fillColor", and sets the "isOpaque" attribute to TRUE.
//
// The reason this must be redefined is to make it illegal to set the fill
// color to NO_COLOR.  If the fill color were NO_COLOR, there would be no
// guarantee that the old image of a changed drawable would be erased, since
// it would be possible for all of its Containers to have no fill color.
//---------------------------------------------------------------------
//@ PreCondition
// The passed fill color must be a legal color, and must NOT be NO_COLOR.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
BaseContainer::setFillColor(Colors::ColorS fillColor)
{
	CALL_TRACE("BaseContainer::setFillColor(Colors::ColorS)");

	CLASS_PRE_CONDITION(fillColor != Colors::NO_COLOR);

	//											$[TI1]
	Container::setFillColor(fillColor);	    // note that this calls "changing()"
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setX [virtual]
//
//@ Interface-Description
// Redefined version of the method inherited from the Drawable class.
// If called, an error will be generated.
//
// The x position of a BaseContainer is always 0.
//---------------------------------------------------------------------
//@ Implementation-Description
// Causes an error via the CLASS_ASSERTION macro.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
BaseContainer::setX(Int)
{
	CALL_TRACE("BaseContainer::setX(Int)");

	CLASS_ASSERTION_FAILURE();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setY [virtual]
//
//@ Interface-Description
// Redefined version of the method inherited from the Drawable class.
// If called, an error will be generated.
//
// The y position of a BaseContainer is always 0.
//---------------------------------------------------------------------
//@ Implementation-Description
// Causes an error via the CLASS_ASSERTION macro.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
BaseContainer::setY(Int)
{
	CALL_TRACE("BaseContainer::setY(Int)");

	CLASS_ASSERTION_FAILURE();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: appendTo [virtual]
//
//@ Interface-Description
// Redefined version of the method inherited from the Drawable class.
// If called, an error will be generated.
//
// A BaseContainer cannot be appended to a Container.
//---------------------------------------------------------------------
//@ Implementation-Description
// Causes an error via the CLASS_ASSERTION macro.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
BaseContainer::appendTo(Container *)
{
	CALL_TRACE("BaseContainer::appendTo(Container *)");

	CLASS_ASSERTION_FAILURE();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: removeFromParent [virtual]
//
//@ Interface-Description
// Redefined version of the method inherited from the Drawable class.
// If called, an error will be generated.
//
// A BaseContainer cannot be appended to a Container, so therefore it
// also cannot be removed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Causes an error via the CLASS_ASSERTION macro.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
BaseContainer::removeFromParent(void)
{
	CALL_TRACE("BaseContainer::removeFromParent(void)");

	CLASS_ASSERTION_FAILURE();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setShow [virtual]
//
//@ Interface-Description
// Redefined version of the method inherited from the Drawable class.
// If called, an error will be generated.
//
// A BaseContainer is always in the shown state.
//---------------------------------------------------------------------
//@ Implementation-Description
// Causes an error via the CLASS_ASSERTION macro.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
BaseContainer::setShow(Boolean)
{
	CALL_TRACE("BaseContainer::setShow(Boolean)");

	CLASS_ASSERTION_FAILURE();
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateBase [virtual]
//
//@ Interface-Description
// This is a redefined version of the evaluateBase() method inherited
// from parent class Container.  Since this is a BaseContainer, its
// BaseContainer pointer was set to point to itself during construction.
// Therefore this method does nothing except to make sure that the
// BaseContainer pointer is still pointing to itself.  
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply asserts that the pBaseContainer_ is == this.
//
// Note: the Container base class is responsible for the updating of the
// BaseContainer pointers of the contained Drawables.  This is why the
// BaseContainer's evaluateBase() method does not need to call for its
// children.  There is no series of recursive calls moving UP the containment
// tree.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
BaseContainer::evaluateBase(void)
{
	CALL_TRACE("BaseContainer::evaluateBase(void)");

	CLASS_ASSERTION(pBaseContainer_ == this);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateVisible [virtual]
//
//@ Interface-Description
// This method sets the visibility status of this BaseContainer.  It redefines
// the method inherited from Container; this is needed because BaseContainers
// are always visible.  The evaluateVisible() inherited from Container would
// set its "visible" attribute to FALSE because it requires a parent container
// to be defined.  This version simply sets the visibility status to TRUE.
//---------------------------------------------------------------------
//@ Implementation-Description
// Since a BaseContainer is always visible, visible_ is set to TRUE 
// to override the default behavior of Container.   
//
// Note: the Container base class is responsible for the updating of the
// the contained children drawable's visibility.  This is why the
// BaseContainer's evaluateBase() method does not need to call for its
// children.  There is no series of recursive calls moving UP the containment
// tree.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BaseContainer::evaluateVisible(void)
{
	CALL_TRACE("BaseContainer::evaluateVisible(void)");

	//												$[TI1]
	setVisible_(TRUE);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: repaint
//
//@ Interface-Description
// Updates the display from the internal model.  With the exception of
// the PlotContainer class, this is the only call the application makes
// to update the screen.  When screen Drawables are updated, a record
// is kept by this class; this information is used to identify the
// screen areas that must be updated.
//---------------------------------------------------------------------
//@ Implementation-Description
// There are two distinct processing phases.  The first phase creates an
// optimized list of areas from the existing list of changedDrawables_.
// The optimization consists of collapsing each set of overlapping areas into a
// single large area.  The second phase, then draws the changed areas on
// the screen.
//
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
BaseContainer::repaint(void)
{
	CALL_TRACE("BaseContainer::repaint(void)");

	FixedSListC(Area, MAX_AREA_LIST_NUM)	areaList;			// list of copies of Areas
	Area 							realArea;			// for rel-to-abs transformation
	SigmaStatus 					status = FAILURE;	// loop control

	//
	// First, iterate through the list of changed Drawables, collecting
	// their Areas in a list, using mergeAreaToList_().  As areas are
	// added, smaller enclosed areas are eliminated to optimize the drawing
	// process.  This avoids drawing the same area twice.
	//
	for (status = changedDrawables_.goFirst();
		 status == SUCCESS;
		 status = changedDrawables_.goNext())
	{	//												$[TI1]
		Drawable& drawable = changedDrawables_.currentItem();

		//
		// Compute the "real area" (absolute screen coordinates) of the
		// changed Drawable, and add its area to the list of merged areas.

		// Note: if an object has been moved from one BaseContainer to another
		// - and this method has been called on the old BaseContainer - the
		// isVisible() call below will return true, when really the object is
		// not visible in the old base, only in the new.  Result: area relative
		// to *new* base gets added to area list.  The only effect of this
		// problem is to cause some extra parts of the screen to be redrawn -
		// and it should never occur, because an application will not typically
		// move Drawables between BaseContainers.  Therefore, to avoid unneeded
		// performance degradation, no check is made to ensure that this
		// drawable's pBaseContainer == this.
		// 
		if (drawable.isVisible())
		{	//											$[TI1.1]
			realArea.setX(drawable.getRealX());
			realArea.setY(drawable.getRealY());
			realArea.setWidth(drawable.getWidth());
			realArea.setHeight(drawable.getHeight());
			mergeAreaToList_(areaList, realArea);
		}
		//												$[TI1.2]
		//
		// An object may not be visible but still be on the list if show was
		// turned off, or if it was removed from the parent.  We still want the
		// old area even if the object is not visible now, because changing()
		// made sure it was visible when it was put on the list.  The
		// lastDrawnArea is already in real coordinates so no need to do the
		// rel-to-abs coordinate transformation.
		//
		// The object may have been added to the changed list because it is
		// just becoming visible - it wasn't previously displayed.  In that
		// case, its lastDrawnArea is not applicable, and should not be merged
		// into the area list.  This is avoided by checking the
		// pBaseWhereLastDrawn_ private member.  If it matches the "this"
		// pointer of this BaseContainer, then the old image of this object is
		// in this BaseContainer, so the lastDrawnArea is added to the area
		// list.

		Area& lastDrawnArea = drawable.getLastDrawnArea();

		if (this == drawable.getBaseWhereLastDrawn())
		{	//											$[TI1.3]
			mergeAreaToList_(areaList, lastDrawnArea);
		}
		//												$[TI1.4]
	}	// end of for loop
	//													$[TI2]

	//
	// Setup the display for this container.
	//
	GraphicsContext graphicsContext;
	
	Boolean displayLocked = FALSE;

	//
	// Now, draw each of these areas in turn.  Note that some parts of the
	// screen may still be drawn more than once.
	//
    
	for (status = areaList.goFirst();
		 status == SUCCESS;
		 status = areaList.goNext())
	{	//												$[TI3]
		Area& clipArea = areaList.currentItem();
		
		if ( !displayLocked )
		{ //											$[TI3.1]
		  displayContext_.lock();
		  displayLocked = TRUE;
		} //											$[TI3.2]

		draw(clipArea);
		
		if ( displayContext_.isLockPending() )
		{ // 											$[TI3.3]
		  displayContext_.unlock();
		  displayLocked = FALSE;
		} //											$[TI3.4]
	}

	if ( displayLocked )
	{	//												$[TI4.1]
	  displayContext_.unlock();
	  displayLocked = FALSE;
	}   //												$[TI4.2]

	//												
	// Iterate through the list of changed Drawables, clearing the changed flag
	// of each one.  This must be done after the draw loop above, because some
	// classes (such as Button) perform processing during their draw which can
	// call changing(). For example, Button calls setColor() on each of the
	// Drawables in its labelContainer_.
	//
	for (status = changedDrawables_.goFirst();
		 status == SUCCESS;
		 status = changedDrawables_.goNext())
	{	//												$[TI5]
		Drawable& drawable = changedDrawables_.currentItem();
		drawable.clearChanged();
	}
	//													$[TI6]
	//
	// Blow away changed Drawables list.
	//
	changedDrawables_.clearList();

}

#if defined( SIGMA_GUIPC_CPU )
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: invalidate
//
//@ Interface-Description
// Invalidate all drawings in this base container.
//---------------------------------------------------------------------
//@ Implementation-Description
// Usually, we swtiches between windows when we are debugging on WIN32,
// after we swtich back to simulator, the UPPER and LOWER container 
// will become blank. The method provides a way to force the 
// invalidation on base containers.
//
// The method go through all the child of this container and add them
// to changed list to allow forced redrawing of children.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BaseContainer::invalidate(void)
{
	// Go through all child and append to change list
	SIterR_Drawable children(*getChildren_());

	for (SigmaStatus status = children.goFirst();
		 SUCCESS == status;
		 status = children.goNext())
	{
		// TODO E600 the conversion here may not safe
		addToChangedList( (Drawable*)&children.currentItem() );
	}
}
#endif // SIGMA_GUIPC_CPU

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isRepaintNeeded
//
//@ Interface-Description
// Query thd changedDrawables_ list to see if we need to repaint.
//---------------------------------------------------------------------
//@ Implementation-Description
// If we can find the first changed drawable, then we know repaint is
// required.  And vice versa.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

Boolean
BaseContainer::isRepaintNeeded(void)
{
	CALL_TRACE("BaseContainer::isRepaintNeeded(void)");

	return((SUCCESS == changedDrawables_.goFirst()) ? TRUE : FALSE);
	//											$[TI1]$[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addToChangedList
//
//@ Interface-Description
// This method adds the Drawable pointed to by "pDrawable" to the list of
// changed drawables.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the append() method of the private list of Drawables called
// "changedDrawables_".
//---------------------------------------------------------------------
//@ PreCondition
// The given "pDrawable" must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BaseContainer::addToChangedList(Drawable *pDrawable)
{
	CALL_TRACE("BaseContainer::addToChangedList(const Drawable *)");
	CLASS_PRE_CONDITION(pDrawable != NULL);

	//											$[TI1]
	changedDrawables_.append(*pDrawable);
}



//=====================================================================
//
// Private Methods:
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BaseContainer()
//
//@ Interface-Description
// Construct and initialize a BaseContainer object.  The "screen" parameter
// indicates which screen (upper or lower) will contain this base container.
//
// The constructor is made private to prevent the application from
// instantiating any BaseContainers.  This is done for the application
// by the GuiFoundation::Initialize() method; to make this possible,
// this class grants friendship to GuiFoundation::Initialize().
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all private data. The color_ data member inherited
// from Drawable is changed from default BLACK to NO_COLOR.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
BaseContainer::BaseContainer(DisplayContext & rDisplayContext) :
	displayContext_(rDisplayContext),
	isDirectDrawEnabled_(FALSE)
{
	CALL_TRACE("BaseContainer::BaseContainer(VgaDisplayId screen)");

	//												$[TI1]
	//
	// Initialize protected data members.
	//
	setVisible_(TRUE);

	color_ = Colors::NO_COLOR;

	// Set the pBaseContainer_ pointer to point to myself. Note that the
	//  pParentContainer_ pointer defaults to NULL; the reason that the
	//  pBaseContainer_ is not also set to NULL is that when a Drawable's
	//  pBaseContainer_ is NULL, it means that it is not in a BaseContainer.
	pBaseContainer_ = this;

	// The following is needed to avoid inheriting the fillColor_ == NO_COLOR
	// setting from Container.  If a BaseContainer's fillColor_ was ==
	// NO_COLOR, the following problem could occur: when draw() is called from
	// within the repaint() method, and all contained containers at a given
	// location also have fillColor_ == NO_COLOR, then the old image of a changed
	// Drawable would not be erased properly.  fillColor_ is private so it must
	// be set via the provided method.
	setFillColor(Colors::WHITE);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BaseContainer()
//
//@ Interface-Description
// Destroy object. Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
BaseContainer::~BaseContainer(void)
{
	CALL_TRACE("BaseContainer::~BaseContainer()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: mergeAreaToList_
//
//@ Interface-Description
// This method adds the given "newArea" to a list of areas ("areaList"),
// collapsing overlapping areas into larger areas where possible.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method  performs optimizations when possible by checking to see
// if the area to be added is contained in the merged area.  If it is
// contained, the new area is not added.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BaseContainer::mergeAreaToList_(FixedSListC(Area, MAX_AREA_LIST_NUM)& areaList,
								Area newArea)
{
	CALL_TRACE("BaseContainer::mergeAreaToList_()");

	//
	// Walk through the list, searching for areas which intersect the
	// "newArea".  When an intersecting area is found, remove it and swell
	// the "newArea" to contain the union of itself and the intersecting
	// area.
	//
	SigmaStatus status = areaList.goFirst();
	while (SUCCESS == status)
	{	//												$[TI1]
		Area& currentArea = areaList.currentItem();

		//
		// Note that Area::overlapsArea() returns TRUE if either area
		// overlaps with (intersects) or contains the other area.
		//
		if (currentArea.overlapsArea(newArea))
		{	//											$[TI1.1]
			//
			// Modify newArea to be the containing rectangle of the
			// currentArea and the newArea.
			//
			newArea.makeMerge(currentArea, newArea);

			//
			// Dispose of current area, then check to see whether there are any
			// more list entries to process.  There is a big assumption here
			// that the SListC class sets the current element to the element
			// *after* the item that was removed!  When the current element is
			// the last element in the list, then the new current element is
			// the element *previous* to the item that was removed, resulting
			// in a (harmless) extra loop iteration.
			//
			// Find out if this item is at the end of the areaList.
			// If this item is at the end of list, then set the looping flag
			// to FALSE and terminate the while loop.
			// If this item is not at the end of the list, then set the looping
			// flag to TRUE and continue to process the rest of list.
			//
			if (areaList.isAtLast())
			{	//										$[TI1.1.1]
				status = FAILURE;
			}
			else
			{	//										$[TI1.1.2]
				status = SUCCESS;
			}
			
			areaList.removeCurrent();
		}
		else
		{	//											$[TI1.2]
			status = areaList.goNext();
		}
	}
	//													$[TI2]
	//
	// Note that the following call creates a *copy* of the newArea on the
	// list.
	//
	areaList.append(newArea);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDirectDraw
//
//@ Interface-Description
//  Enables or disables the Direct Draw mode as specified.
//  Locks the DisplayContext when enabling the Direct Draw mode. Unlocks
//  the DisplayContext when disabling Direct Draw. Prior to obtaining 
//  the lock, sets "display lock pending" as an advisory to a lower
//  priority display task (waveforms) that a higher priority display
//  task (breath bar) is waiting for access to the display. Upon detecting
//  the pending display lock, the lower priority task will unlock the
//  display and allow the higher priority task its chance update the
//  display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  see Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BaseContainer::setDirectDraw(Boolean enableDirectDraw)
{
  if ( enableDirectDraw )
  { //											$[TI1]
	displayContext_.setLockPending(TRUE);
	displayContext_.lock();
	displayContext_.setLockPending(FALSE);
  }
  else
  { //											$[TI2]
	displayContext_.unlock();
  }
  isDirectDrawEnabled_ = enableDirectDraw;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isDirectDrawEnabled
//
//@ Interface-Description
//  Returns TRUE if the Direct Draw mode is enabled, otherwise FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns value of private isDirectDrawEnabled_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
BaseContainer::isDirectDrawEnabled(void) const
{
  return isDirectDrawEnabled_;
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds its sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BaseContainer::SoftFault(const SoftFaultID  softFaultID,
						 const Uint32       lineNumber,
						 const char*        pFileName,
						 const char*        pPredicate)
{
	CALL_TRACE("BaseContainer::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, BASECONTAINER, 
							lineNumber, pFileName, pPredicate);
}

