#ifndef TrendDbSampleNonAvg_HH
#define TrendDbSampleNonAvg_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbSampleNonAvg
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSampleNonAvg.hhv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  ksg    Date:  17-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
// 
//=====================================================================


#include "TrendDbSampleData.hh"



class TrendDbSampleNonAvg : public TrendDbSampleData
{
public:    
	TrendDbSampleNonAvg( PatientDataId::PatientItemId id,
						 TrendSelectValue::TrendSelectValueId trendId );
	TrendDbSampleNonAvg( SettingId::SettingIdType id,
						 TrendSelectValue::TrendSelectValueId trendId );
	virtual ~ TrendDbSampleNonAvg();


	virtual void compressData( TrendTimeScaleValue::TrendTimeScaleValueId timescale );


	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
protected:

	virtual void clear_( void );

private:
	TrendDbSampleNonAvg( const TrendDbSampleNonAvg& );	// not implemented
	void operator=( const TrendDbSampleNonAvg& );	// not implemented

};

#endif // TrendDbSampleNonAvg_HH
