
#ifndef Foundation_HH
#define Foundation_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Foundation - Foundation Subsystem's class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/Foundation.hhv   25.0.4.0   19 Nov 2013 14:03:48   pvcs  $
//
//@ Modification-Log
//  
//   By: sah   Date: 26-Feb-1997   DCS Number: 1799
//   Project:  Sigma (R8027)
//   Description:
//	Removed #ifdef's around DEBUG code.
//  
//   By: sah   Date: 26-Feb-1997   DCS Number: 1800
//   Project:  Sigma (R8027)
//   Description:
//	Fixed code that is out of spec with Coding Standard.
//  
//   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "FoundationClassId.hh"

//@ Usage-Classes
#if defined(SIGMA_DEBUG)
#include "BitUtilities.hh"
#endif  // defined(SIGMA_DEBUG)
//@ End-Usage


class Foundation {
  public:
    static void  Initialize(void);

#if defined(SIGMA_DEBUG)
    static inline Boolean  IsDebugOn(const FoundationClassId foundationId);

    static inline void  TurnDebugOn (const FoundationClassId foundationId);
    static inline void  TurnDebugOff(const FoundationClassId foundationId);

    static void  TurnAllDebugOn (void);
    static void  TurnAllDebugOff(void);
#endif  // defined(SIGMA_DEBUG)

  private:
    Foundation(const Foundation&);		// not implemented...
    Foundation(void);				// not implemented...
    ~Foundation(void);				// not implemented...
    void  operator=(const Foundation&);		// not implemented...

    //@ Constant:  NUM_LWORDS_
    // The number of long words needed by 'arrDebugFlags_[]' to contain all
    // of the debug flags.
    enum
    {
      NUM_LWORDS_ = ((NUM_FOUNDATION_CLASSES+sizeof(Uint)-1) / sizeof(Uint))
    };

    //@ Data-Member:  arrDebugFlags_
    // A reference to a static array of debug flags (one bit each) to be
    // used by this subsystem's code.
    static Uint  arrDebugFlags_[Foundation::NUM_LWORDS_];
};


// Inlined Methods...
#include "Foundation.in"


#endif // Foundation_HH 
