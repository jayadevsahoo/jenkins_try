#ifndef SafetyNetRangeTest_HH
#define SafetyNetRangeTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: SafetyNetRangeTest - Base class for OOR (out-of-range) BD
//                              Safety Net Tests
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SafetyNetRangeTest.hhv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added an access method for error code
//
//  Revision: 001  By:  by    Date:  05-Sep-1995    DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "SafetyNetSensorData.hh"

//@ Usage-Classes
#include "Background.hh"
//@ End-Usage

class SafetyNetRangeTest
{
    public:

        SafetyNetRangeTest( void );
        virtual ~SafetyNetRangeTest( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL, 
			       const char*       pPredicate = NULL );

        virtual BkEventName checkCriteria( void ) = 0;
        inline Uint16 getErrorCode( void ) const;

    protected:

        //@ Data-Member: *pSafetyNetSensorData_;
        // A pointer to the SafetyNetSensorData for the sensor
        SafetyNetSensorData *pSafetyNetSensorData_;

    private:

        // these methods are purposely declared, but not implemented...
        SafetyNetRangeTest( const SafetyNetRangeTest& );  // not implemented...
        void operator=( const SafetyNetRangeTest& );      // not implemented...
};

// Inlined methods...
#include "SafetyNetRangeTest.in"

#endif // SafetyNetRangeTest_HH 


