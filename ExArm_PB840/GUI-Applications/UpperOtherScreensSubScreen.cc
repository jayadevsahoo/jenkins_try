#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: UpperOtherScreensSubScreen - Allows access to the Diagnostic Code,
// Operational Time, Ventilator Configuration, SST Results and Test Summary
// subscreens.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container class.
// This class contains a SubScreenTitleArea object for the subscreen title and
// one TextButton for each other subscreen that can be activated from this
// subscreen. Specifically, there is one of these `instant action' screen
// select buttons for each of the following subscreens:
// >Von
//	1.	Diagnostic Code subscreen.
//	2.	Operational Time subscreen.
//	3.	SST Results subscreen.
//	4.	Ventilator Configuration subscreen.
//	5.	Ventilator Test Summary subscreen.
// >Voff
// This class collaborates with the UpperSubScreenArea to activate the
// subscreens associated with the screen select buttons contained in this
// subscreen.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// The buttonDownHappened() method is called when any button in
// the subscreen is pressed down allowing us to activate its associated
// subscreen.
//---------------------------------------------------------------------
//@ Rationale
// Created to provide a navigation interface to a number of infrequently used
// subscreens on the upper screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Each screen select button is labeled with the title of the subscreen
// associated with that button.  Each button is registered with this subscreen
// for button press events.  When a screen select button is pressed, the
// buttonDownHappened() handler instructs the UpperSubScreenArea to activate
// the subscreen associated with the button.  Note that activating the new
// subscreen displaces this subscreen from the UpperSubScreenArea.
//
// There is tricky problem with the Other Screens screen select button
// (contained by the UpperScreenSelectArea).  That is, when a new "other"
// subscreen is activated from this Other Screens subscreen, the default
// deactivation behavior for the Other Screens subscreen pops up the Other
// Screens screen select button.  However, this is not the desired result -- we
// want the Other Screens screen select button to remain selected when a new
// "other" subscreen is activated.  Thus, the Other Screens screen select
// button must be "manually" pushed down (selected) when a new "other" subscreen
// is activated.  See the buttonDownHappened() method for details.
//
// The various screen select buttons and the subscreen title are all
// added to the Container when it is constructed.
//---------------------------------------------------------------------
//@ Fault-Handling
// None, except for sanity checks in the button press handler to make
// sure that it was given one of the expected buttons.
//---------------------------------------------------------------------
//@ Restrictions
// None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/UpperOtherScreensSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  btray	   Date:  21-Jul-1999    DCS Number: 5424 
//  Project: Neonatal 
//  Description:
//		Initial Neonatal version. Removed development option button
//		from normal mode.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date: 16-Oct-1997    DR Number: DCS 2561
//  Project:  Sigma (840)
//  Description:
//		Disabled the development screen option when in final official release.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "UpperOtherScreensSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "UpperScreen.hh"
#include "UpperSubScreenArea.hh"
#include "DiagLogMenuSubScreen.hh"
#include "OperationalTimeSubScreen.hh"
#include "VentConfigSubScreen.hh"
#include "SstResultsSubScreen.hh"
#include "VentTestSummarySubScreen.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BUTTON_WIDTH_ = 160;
static const Int32 BUTTON_HEIGHT_ = 46;
static const Int32 BUTTON_X_GAP_ = 10;
static const Int32 BUTTON_Y_GAP_ = 10;
static const Int32 BUTTON_X1_ = 
	( ::UPPER_SUB_SCREEN_AREA_WIDTH - 3 * BUTTON_WIDTH_ - 2 * BUTTON_X_GAP_ )/2;
static const Int32 BUTTON_Y1_ = 73;
static const Int32 BUTTON_BORDER_ = 3;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UpperOtherScreensSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen (the Upper subscreen
// area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all buttons and the subscreen title.  Adds all buttons
// and the subscreen title object to the parent container for display.
// Registers for button callbacks.
//
// $[01190] The Upper Other Screens subscreen should display ...
//---------------------------------------------------------------------
//@ PreCondition
// The given 'pSubScreenArea' pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UpperOtherScreensSubScreen::UpperOtherScreensSubScreen(
											SubScreenArea* pSubScreenArea) :
	SubScreen(pSubScreenArea),
	diagnosticCodeButton_(
				BUTTON_X1_,
				BUTTON_Y1_,
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::UPOTHER_SUBSCREEN_DIAG_CODE), operationalTimeButton_(
				BUTTON_X1_ + 1 * (BUTTON_WIDTH_ + BUTTON_X_GAP_),
				BUTTON_Y1_,
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::UPOTHER_SUBSCREEN_OPER_TIME),
	sstResultsButton_(
				BUTTON_X1_ + 2 * (BUTTON_WIDTH_ + BUTTON_X_GAP_),
				BUTTON_Y1_,
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::UPOTHER_SUBSCREEN_TEST_RESULT),
	ventConfigButton_(
				BUTTON_X1_,
				BUTTON_Y1_ + 1 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_),
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::UPOTHER_SUBSCREEN_SW_REV),
	testSummaryButton_(
				BUTTON_X1_ + 1 * (BUTTON_WIDTH_ + BUTTON_X_GAP_),
				BUTTON_Y1_ + 1 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_),
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
			    MiscStrs::UPOTHER_SUBSCREEN_TEST_SUMMARY),
	titleArea_(MiscStrs::UPOTHER_SUBSCREEN_TITLE,
				SubScreenTitleArea::SSTA_CENTER)
{
	CALL_TRACE("UpperOtherScreensSubScreen::UpperOtherScreensSubScreen(pSubScreenArea)");

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	//
	// Set the button callbacks.
	//
	diagnosticCodeButton_.setButtonCallback(this);
	operationalTimeButton_.setButtonCallback(this);
	ventConfigButton_.setButtonCallback(this);
	sstResultsButton_.setButtonCallback(this);
	testSummaryButton_.setButtonCallback(this);

	//
	// Add the buttons.
	//
	addDrawable(&diagnosticCodeButton_);
	addDrawable(&operationalTimeButton_);
	addDrawable(&ventConfigButton_);
	addDrawable(&sstResultsButton_);
	addDrawable(&testSummaryButton_);

	//
	// Add the following drawables

	//
	// Add screen title object.
	//
	addDrawable(&titleArea_);							// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~UpperOtherScreensSubScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UpperOtherScreensSubScreen::~UpperOtherScreensSubScreen(void)
{
	CALL_TRACE("UpperOtherScreensSubScreen::~UpperOtherScreensSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate() [virtual]
//
//@ Interface-Description
// Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Empty implementation of a pure virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperOtherScreensSubScreen::activate(void)
{
	CALL_TRACE("UpperOtherScreensSubScreen::activate(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate() [virtual]
//
//@ Interface-Description
// Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Empty implementation of a pure virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperOtherScreensSubScreen::deactivate(void)
{
	CALL_TRACE("UpperOtherScreensSubScreen::deactivate(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Handles button down events for all of the "instant action" screen
// select buttons on this subscreen.  The 'pButton' parameter is the
// button that was pressed down.  The 'byOperatorAction' flag is TRUE
// if the given button was pressed by the operator.
//---------------------------------------------------------------------
//@ Implementation-Description
// Decodes which button was pressed and instructs the UpperSubScreenArea
// to activate the subscreen associated with that button (implicitly
// deactivates this Other Screens subscreen).  Also, "presses" the Other
// Screen screen select button (contained by the UpperScreenSelectArea)
// back down to show that the new subscreen is an "other" subscreen and
// to provide a way for the user to dismiss the new "other" subscreen.
// Also, resets the button that was pressed to the "up" state so that it
// will be in the correct state the next time this subscreen is
// displayed.
//
// $[01063] The Other Screens screen select button shall remain selected ...
//---------------------------------------------------------------------
//@ PreCondition
// The 'isByOperatorAction' parameter must be TRUE and the 'pButton'
// parameter must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperOtherScreensSubScreen::buttonDownHappened(Button *pButton,
											   Boolean isByOperatorAction)
{
	CALL_TRACE("UpperOtherScreensSubScreen::buttonDownHappened(pButton, isByOperatorAction)");

	SAFE_CLASS_PRE_CONDITION(pButton != NULL);
	SAFE_CLASS_PRE_CONDITION(TRUE == isByOperatorAction);

	//
	// Fetch the subscreen associated with the pressed button.
	//
	SubScreen*	pSubscreen = NULL;		// subscreen to be activated
	if (pButton == &diagnosticCodeButton_)
	{													// $[TI1]
		pSubscreen = UpperSubScreenArea::GetDiagLogMenuSubScreen();
	}
	else if (pButton == &operationalTimeButton_)
	{													// $[TI2]
		pSubscreen = UpperSubScreenArea::GetOperationalTimeSubScreen();
	}
	else if (pButton == &ventConfigButton_)
	{													// $[TI3]
		pSubscreen = UpperSubScreenArea::GetVentConfigSubScreen();
	}
	else if (pButton == &sstResultsButton_)
	{													// $[TI4]
		pSubscreen = UpperSubScreenArea::GetSstResultsSubScreen();
	}
	else if (pButton == &testSummaryButton_)
	{													// $[TI5]
		pSubscreen = UpperSubScreenArea::GetVentTestSummarySubScreen();
	}
	CLASS_ASSERTION(pSubscreen != NULL);

	//
	// Fetch the Upper Other Screens tab button.
	//
	TabButton*	pTabButton = UpperScreen::RUpperScreen.getUpperScreenSelectArea()->
								   getUpperOtherScreensTabButton();
	CLASS_ASSERTION(pTabButton != NULL);

	//
	// Activate the subscreen and associate the Upper Other Screens tab button
	// with it. Activating the subscreen automatically forces the associated
	// tab button down. $[01063]
	//
	getSubScreenArea()->activateSubScreen(pSubscreen, pTabButton);

	//
	// Reset whichever button was pressed.
	//
	pButton->setToUp();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
UpperOtherScreensSubScreen::SoftFault(const SoftFaultID  softFaultID,
									  const Uint32       lineNumber,
									  const char*        pFileName,
									  const char*        pPredicate)  
{
	CALL_TRACE("UpperOtherScreensSubScreen::SoftFault(softFaultID, "
			   "lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, UPPEROTHERSCREENSSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
