#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbSettingRecord
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the database record for the trended settings
//  values. This class inherits from the TrendDbRecord base class and 
//  overloads the following pure virtual methods:
//  size()
//  get/setSeqNumber()
//	get/setTimeOfDay()
//	get/setTrendValue()
//	setChecksum()
// 
//---------------------------------------------------------------------
//@ Rationale
//  To define a data record specific to the trended setting values
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines methods that provide read/write access to the 
//  individual setting records.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used (defined in the base class)
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSettingRecord.ccv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  ksg    Date:  18-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
//
//=====================================================================

#include "Trend_Database.hh"
#include "TrendDbSettingRecord.hh"

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSettingRecord [constructor]
//
//@ Interface-Description
//  No action
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSettingRecord::TrendDbSettingRecord( void )
{
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSettingRecord [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSettingRecord::~TrendDbSettingRecord()   
{
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: size
//
//@ Interface-Description
//  Returns the size in bytes of the configured record
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Int16 TrendDbSettingRecord::size( void ) const
{
	return( sizeof( Schema ) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSeqNumber
//
//@ Interface-Description
//  Returns the seq number for the record
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the memoryPointer is defined
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDbSettingRecord::getSeqNumber( void ) const
{
	Schema * pSchema = (Schema *) getMemoryPointer();
	AUX_CLASS_PRE_CONDITION( pSchema, (Uint32) pSchema );
	return( pSchema->seqNumber );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSeqNumber
//
//@ Interface-Description
//  Sets the seq number for the record
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the memoryPointer is defined
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRecord::setSeqNumber( Uint32 newSeqNumber )
{
	Schema * pSchema = (Schema *) getMemoryPointer();
	AUX_CLASS_PRE_CONDITION( pSchema, (Uint32) pSchema );
	pSchema->seqNumber = newSeqNumber;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTimeStamp
//
//@ Interface-Description
//  Returns the timestamp for the record
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the memoryPointer is defined
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
const TimeOfDay & TrendDbSettingRecord::getTimeOfDay( void ) const
{
	Schema * pSchema = (Schema *) getMemoryPointer();
	AUX_CLASS_PRE_CONDITION( pSchema, (Uint32) pSchema );
	return( pSchema->timeOfDay );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTimeStamp
//
//@ Interface-Description
//  Sets the timestamp for the record
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the memoryPointer is defined
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRecord::setTimeOfDay( const TimeOfDay & newTimeStamp )
{
	Schema * pSchema = (Schema *) getMemoryPointer();
	AUX_CLASS_PRE_CONDITION( pSchema, (Uint32) pSchema );
	pSchema->timeOfDay = newTimeStamp;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTrendValue
//
//@ Interface-Description
//  Returns the trend value for the given slot
//---------------------------------------------------------------------
//@ Implementation-Description
//  slotNumber start with 0
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the memoryPointer is defined
//  Ensures the slot is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
const TrendValue & TrendDbSettingRecord::getTrendValue( Int8 slotNumber ) const
{
	static TrendValue rtnValue;
	Schema * pSchema = (Schema *) getMemoryPointer();
	AUX_CLASS_PRE_CONDITION( pSchema, (Uint32) pSchema );
	AUX_CLASS_PRE_CONDITION( slotNumber < TrendDataMgr::TREND_SETTING_MAX_SLOTS, slotNumber );

	// Copy the data and flag values
	rtnValue.data = pSchema->dataValueArray[ slotNumber ];
	rtnValue.isValid = pSchema->isValidArray[ slotNumber ];

	return( rtnValue );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTrendValue
//
//@ Interface-Description
//  Sets the trend value for the given slot
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the memoryPointer is defined
//  Ensures the slotNumber is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRecord::setTrendValue( Int8 slotNumber, const TrendValue & value )
{
	Schema * pSchema = (Schema *) getMemoryPointer();
	AUX_CLASS_PRE_CONDITION( pSchema, (Uint32) pSchema );
	AUX_CLASS_PRE_CONDITION( slotNumber < TrendDataMgr::TREND_SETTING_MAX_SLOTS, slotNumber );

	// Set the data and flag parts
	pSchema->dataValueArray[ slotNumber ] = value.data;
	pSchema->isValidArray[ slotNumber ] = value.isValid;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setChecksum
//
//@ Interface-Description
//  Computes and stores the checksum
//---------------------------------------------------------------------
//@ Implementation-Description
//  Computes an XOR checksum
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the memoryPointer is defined
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRecord::setChecksum( void )
{
	Schema * pSchema = (Schema *) getMemoryPointer();
	AUX_CLASS_PRE_CONDITION( pSchema, (Uint32) pSchema );

	// Compute and store checksum
	pSchema->checkSum = 0;
	pSchema->checkSum = computeChecksum_();
}
