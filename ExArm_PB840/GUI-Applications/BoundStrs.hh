#ifndef BoundStrs_HH
#define BoundStrs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BoundStrs - Matches advisory message string ids to the hard
// bound ids of the Settings-Validation subsystem.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BoundStrs.hhv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SettingBoundId.hh"
#include "GuiAppClassIds.hh"

class BoundStrs
{
public:
	static void Initialize(void);
	static StringId GetSettingBoundString(SettingBoundId hardBoundId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	BoundStrs(void);						// not implemented...
	~BoundStrs(void);						// not implemented...
	BoundStrs(const BoundStrs&);			// not implemented...
	void operator=(const BoundStrs&);		// not implemented...

	//@ Data-Member: SettingBoundStrs_
	// An array, which stores an advisory message for every hard bound id.
	// The hard bound id value is used as an index into this array to find
	// the appropriate advisory message (which will be displayed in the
	// Prompt Area).
	static StringId SettingBoundStrs_[NUM_SETTING_BOUND_IDS];
};


#endif // BoundStrs_HH 
