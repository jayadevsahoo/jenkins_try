#ifndef OscScheduler_HH
#define OscScheduler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: OscScheduler - active scheduler during Occlusion Status Cycling
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OscScheduler.hhv   25.0.4.0   19 Nov 2013 13:59:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 005  By:  iv    Date:  04-Dec-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Cleanup for code inspection.
//
//  Revision: 004 By:  iv   Date:   04-June-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Removed previousScheduler_.
//
//  Revision: 003 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#include "BreathPhaseScheduler.hh"
#include "Trigger.hh"

//@ Usage-Classes
// forward declarations:
class BreathTrigger;
class ModeTrigger;
class BreathPhase;
//@ End-Usage

class OscScheduler : public BreathPhaseScheduler
{
  public:
    OscScheduler(void);
    virtual ~OscScheduler(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

   virtual void determineBreathPhase(const BreathTrigger& breathTrigger);
   virtual void relinquishControl(const ModeTrigger& modeTrigger);
   virtual void takeControl(const BreathPhaseScheduler& scheduler);
  
  protected:
   virtual EventData::EventStatus reportEventStatus_(const EventData::EventId id,
  														const Boolean eventStatus);
   virtual void enableTriggers_(void);
   void determineNextScheduler_(const Trigger::TriggerId triggerId,
   													const Boolean isVentSetupComplete);

  private:
    OscScheduler(const OscScheduler&);		// not implemented...
    void operator=(const OscScheduler&);	// not implemented...
    void restartOcclusion_(BreathPhase* pBreathPhase, const BreathTrigger& breathTrigger);
    void startInspiration_(const Boolean apneaBreath, const BreathTrigger& breathTrigger,
    															BreathPhase* pBreathPhase);

    //@ Data-Member:  occlusionDetected_
    // Indicates if occlusion was detected during an OSC breath.
    Boolean occlusionDetected_;	

    //@ Data-Member:  numBreathsDelivered_
    // Indicates number of consecutive successful breaths delivered.
    Int16 numBreathsDelivered_;

    //@ Data-Member:  firstOcclusion_
    //  Set true the first time OSC is started.
    Boolean firstOcclusion_;

};


#endif // OscScheduler_HH 
