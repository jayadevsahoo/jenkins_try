#ifndef PROX_MANAGER_HH
#define PROX_MANAGER_HH
#if defined(SIGMA_BD_CPU)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ProxManager - Manage data to/from Prox 
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
// This class is responsible for managaing the interface to the  
// Prox system low level layers.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a. 
//---------------------------------------------------------------------
//@ Restrictions
// 
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ProxManager.hhv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $  
//
//@ Modification-Log
//
//  Revision: 011   By: erm    Date: 07-Feb-2011   SCR Number: 6436 
//  Project:  PROX
//  Description:
//     Added previousFaultPhase_
// 
//  Revision: 010   By: gdc    Date: 07-Feb-2011   SCR Number: 6733 
//  Project:  PROX
//  Description:
//     Added VCO2 study code for development reference.
// 
//  Revision: 009   By: rhj    Date: 19-Jan-2011   SCR Number: 6619 
//  Project:  PROX
//  Description:
//     Added getstartupFailure() method.
// 
//  Revision: 008   By: rhj    Date: 22-Dec-2010   SCR Number: 6631
//  Project:  PROX
//  Description:
//     Added isProxStartup_ member variable. 
//  
//  Revision: 007   By: rhj   Date: 05-Oct-2010    SCR Number: 6627
//  Project:  PROX
//  Description:
//     Changed the pressure alarms for prox by comparing the 
//     maximum pressures of both 840 and prox during a whole breath.
//     Also the check shall only be executed during the start 
//     of inhalation.
// 
//  Revision: 006   By: rhj   Date: 10-Sept-2010   SCR Number: 6627
//  Project:  PROX
//  Description:
//     Added  getMaxInspProxPressure(), getMaxInspWyePressure(),
//            maxInspWyePressure,_maxInspProxPressure,
//           _maxInspProxPressTemp and_ maxInspWyePressureTemp.
// 
//  Revision: 005   By: erm   Date: 10-May-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//     added test to check for missng I/E cable 
// 
// 
//  Revision: 004   By: mnr    Date: 22-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//  	SERIAL_ERROR added.
//
//  Revision: 003   By: mnr    Date: 13-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//  	REVISION_ERROR added.
//
//  Revision: 002   By: mnr    Date: 24-Mar-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//  	Serial Number and Firmware rev related updates.
//
//  Revision: 001   By: erm    Date: 1-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//  Initial revision
//=====================================================================
#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "BreathPhaseType.hh"
#include "ProxiInterface.hh"
#include "EventData.hh"
#include "BigSerialNumber.hh"

class ProxManager
{

public:

    enum
    {
        MERCURY_NAME_SIZE = 128
    };


    enum SensorFault 
    {
        SENSOR_ERROR  = 0x01,
        PURGE_ERROR   =  0x02,
        BOARD_ERROR   =  0x04,
        VOLUME_ERROR   =   0x08,
        PRESSURE_ERROR = 0x10,
        ACCUMULATOR_ERROR = 0x20,
        VERSION_ERROR = 0x40,
        DATA_ERROR = 0x80
    };
    enum VersionErrors
    {
        SERIAL_ERROR   = 0x1,
        REVISION_ERROR = 0x2

    };
    enum DataErrors
    {
        NO_PROX_DATA = 0x0,
        IE_CABLE_MISSING = 0x1,
        FLOW_DATA_OUT_OF_RANGE = 0x2,
        PRESSURE_DATA_OUT_OF_RANGE = 0x3,
        START_UP_TOO_LONG = 0x4

    };

    enum
    {
        MAX_DELAYED = 20
    } ;           // max one hundred milli-second

    ProxManager();
    ~ProxManager();

    void setDelay(Uint32 delay);
    void newCycle(void);
    Real32 getInspiredVolume(void);
    Real32 getExpiredVolume(void);
    Real32 getFlow(void);
    Real32 getPressure(void);
    Real32 getVolume(void);
    BreathPhaseType::PhaseType getBreathPhase(void);
    Uint32 getCurrentDelay(void);
    Boolean getstartupFailure(void);




    Boolean isAvailable(void);
    Uint8 proxManeuverStatus(void);


    BigSerialNumber getFirmwareRev(void);
    Uint32 getBoardSerialNumber(void);
    void setDistalPressure(Real32 pressure);


    static EventData::EventStatus RequestManualPurge(const EventData::EventId id,
                                                     const Boolean eventStatus);
    static void SoftFault(const SoftFaultID softFaultID,
                          const Uint32      lineNumber,
                          const char*       pFileName  = NULL, 
                          const char*       pPredicate = NULL) ;

    Boolean IsProxSubPressEnabled(void);
#if 0
//keep for valadation group
    void DoTimedManualPurge(void);
    Real32 getMaxProxPressureBreath();
    Real32 getMaxWyePressureBreath();
#endif

private:
    void calculateVolume_(void);
    Boolean updatePurgeParams_(void);
    void checkActiveExhalationCompete_(void);
    void  updateDelayedQues_(void);
    void  setProxGasConditions_(void);
    Boolean checkAvailabity_(void);
    Boolean isProxAtFault_(void);
    void updateDelayedValues_(BreathPhaseType::PhaseType phase);
    BreathPhaseType::PhaseType getDelayedValue_(void);
    void clearDelayedValues_(void);



    //@ Data-Member: rawFlow_
    // Flow from proxSensor
    Real32 rawFlow_;

    //@ Data-Member: rawPressure_
    // Pressure From proxSensor
    Real32 rawPressure_;

    //@ Data-Member: rawVolume_;
    // volume From proxSensor
    Real32 rawVolume_;

    //@ Data-Member: expiredLungVolume_
    // proximal expired lung volume
    Real32 expiredVolume_;

    //@ Data-Member: inspiredLungVolume_
    // proximal inspiredlung volume
    Real32 inspiredVolume_;

    //@ Data-Member: netVolume_
    // proximal net  Volume_
    Real32 netVolume_;

    //@ Data-Member: currentBreathPhase_
    // proximal currentBreathPhase_
    BreathPhaseType::PhaseType currentBreathPhase_;

    //@ Data-Member:gasConditions_
    //hold params for setting gas conditions
    ProxiInterface::GasConditions gasConditions_;

    //@ Data-Member:avilable_
    //indicator to determine if Prox is usable
    Boolean isAvailable_;

    //@ Data-Member:qualifiedExhVolume_
    //value without Purge/Autozero activities
    Real32 qualifiedExhVolume_;

    //@ Data-Member:qualifiedInspVolume_
    //value without Purge/Autozero activities
    Real32 qualifiedInspVolume_;

    //@ Data-Member:isValidInspVolume_ 
    //value was there a Purge/Autozero during insp
    Boolean isValidInspVolume_;

    //@ Data-Member:isValidInspVolume_ 
    //value was there a Purge/Autozero during exh
    Boolean isValidExhVolume_;

    //@ Data-Member:prevNetFlow1_ 
    //temp storage for Flow
    Real32 prevNetFlow1_;

    //@ Data-Member:peakPrevNetFlow1_
    //temp storage for Flow
    Real32 prevNetFlow2_;

    //@ Data-Member:peakPrevNetFlow1_
    //temp storage for Flow
    Real32 peakPrevNetFlow1_;

    //@ Data-Member:spiroTerminateCount_
    //use for stabilty in spiro
    Uint8 spiroTerminateCount_;

    //@ Data-Member:exhTime_
    //count countrol cycle during exh
    Uint32 exhTime_;

    //@ Data-Member:isExhFlowFinished_
    //flag to show end of intergation on exh
    Boolean isExhFlowFinished_;

    //@ Data-Member:myPurge_
    //holder for purge parameters
    ProxiInterface::PurgeParams myPurge_;

    //@ Data-Member:isActiveFault_
    //currently is prox at fault
    Boolean isActiveFault_;

    //@ Data-Member:revStrFromBoard_
    //hold PROX firmware rev string from the board
    BigSerialNumber revStrFromBoard_;

    //@ Data-Member:distalPressure_
    //distal pressure from prev 840 insp.
    Real32 distalPressure_;

    //@ Data-Member:inspiredVolumeFactor_
    //factor to used for inspired
    Real32 inspiredVolumeFactor_;

    //@ Data-Member:expiredVolumeFactor_
    //factor to used for expired
    Real32 expiredVolumeFactor_;

    //@ Data-Member:resetFaultCounter_
    //used to reset fault counter
    Boolean resetFaultCounter_;

    //@ Data-Member: enableProxSubPress_
    // used to determine to use prox substitute
    // pressure instead of the actual prox 
    // pressure.
    Boolean enableProxSubPress_;

    //@ Data-Member: waveformDataStallCounter_
    // used to determine the time between
    Uint32 waveformDataStallCounter_;

    //@ Data-Member: isWaveformReady_
    // used to determine when to Prox reading is ready
    Boolean isWaveformReady_;

    //@ Data-Member: maxWyePressureBreath_
    // stores the max estimated wye pressure 
    Real32 maxWyePressureBreath_;

    //@ Data-Member: maxProxPressureBreath_
    // stores the max Prox pressure 
    Real32 maxProxPressureBreath_;

    //@ Data-Member: maxProxPressBreathTemp_
    // Temporarily stores the max Prox pressure of the
    // entire breath
    Real32 maxProxPressBreathTemp_;

    //@ Data-Member: maxWyePressureBreathTemp_
    // Temporarily stores the max  
    // estimated wye pressure of the entire
    // breath
    Real32 maxWyePressureBreathTemp_;

    //@ Data-Member: startupFailure_
    // used as a flag to determine if
    // there a extended start-up time
    Boolean startupFailure_;

    //@ Data-Member: isProxStartupComplete_
    // uses as a flag to determine if
    // prox startup is complete.
    Boolean isProxStartupComplete_;    

    //@ Data-Member: startTimeCheck_
    // start Prox Time out check
    Boolean startTimeCheck_;

    //@ Data-Member:  index_
    // this represent the current insertion point into the buffer
    Int index_;

    //@ Data-Member:  retPoint_
    // the next location in the buffer to be return to callers
    Int retPoint_;

    //@ Data-Member:  delayTarget_
    // the number of deleayed elements 
    Uint32 delayTarget_;

    //@ Data-Member:  buffer_
    // array of all elements 
    BreathPhaseType::PhaseType buffer_[MAX_DELAYED];

    //@ Data-Member:  startUpMessageSendOut_
    // flag for sending event notification on start up
    Boolean startUpMessageSendOut_;


    //@ Data-Member:  startUpMessageSendOut_
    // flag for sending event notification on prox installed
    Boolean sendOneProxInstalledMessage_;


    //@ Data-Member:  prevPhase_
    // used to keep previous breath phase on a cycle bais
    BreathPhaseType::PhaseType prevPhase_;

    //@ Data-Member:  numBreaths_
    // used to keep track of number of breaths
    Uint8 numBreaths_;

    //@ Data-Member:  sendAllParameters_
    // used at stratup to send all gas params
    Boolean sendAllParameters_;

    //@ Data-Member:  checkStartUpTime_
    // used at start counting startup time
    Boolean checkStartUpTime_;

    //@ Data-Member:  revChecked_
    // used to indicate if rev was checked
    Boolean revChecked_;

    //@ Data-Member:  serialNumChecked_
    // used to indicate if SN was checked
    Boolean serialNumChecked_;

    //@ Data-Member:  isVersionFailure_
    // used to indicate if version failure
    Boolean isVersionFailure_ ;

    //@ Data-Member:  whichVersionFailure_
    // used to indicate if version failure happended
    Uint8 whichVersionFailure_ ;

    //@ Data-Member:  checkTimer_
    // used to time how long in start up
    OsTimeStamp checkTimer_;

    //@ Data-Member:  NegVolumeCounter_
    // used to count negative volume hits
    Uint negVolumeCounter_;

    //@ Data-Member:  pressureFaultCount_
    // used to count pressure faults hits
    Uint32 pressureFaultCount_;

    //@ Data-Member:  overAccumPressureFaultHit_
    // used to count accum pressure faults hits
    Uint8 overAccumPressureFaultHit_;

    //@ Data-Member:  reservoirFaultHit_
    // used to count reservoir pressure faults hits
    Uint8 reservoirFaultHit_;

    //@ Data-Member:  currentOverPressureCount_
    // used to count over pressure faults hits
    Uint32 currentOverPressureCount_;

    //@ Data-Member:  inphaseCounter_
    // used to count how long within a given phase
    Uint32 inphaseCounter_;

    //@ Data-Member:  previousPhase_
    // used to hold previous I/E prom prox board
    Boolean previousPhase_;

    //@ Data-Member:  doAccumCheck_
    // flag to indicate do accum check
    Boolean doAccumCheck_ ;


    //@ Data-Member:  resultPress_
    // hold the difference of pressure between 840 and Prox
    Real32 resultPress_;

    //@ Data-Member:  isSensorErrorOccured_
    //flag for sensor error
    Boolean isSensorErrorOccured_; 

    //@ Data-Member:  isPurgeErrorOccured_
    //flag for purge error
    Boolean isPurgeErrorOccured_; 

    //@ Data-Member:  isBoardErrorOccured_
    //flag for board error
    Boolean isBoardErrorOccured_; 

    //@ Data-Member:  isVolumeErrorOccured_
    //flag for volume error
    Boolean isVolumeErrorOccured_; 

    //@ Data-Member:  isPressureErrorOccured_
    //flag for pressure error
    Boolean isPressureErrorOccured_;

    //@ Data-Member:  isAccumulatorErrorOccured__
    //flag for Accum error
    Boolean isAccumulatorErrorOccured_; 

    //@ Data-Member:  isVersionErrorOccured_
    //flag for version error
    Boolean isVersionErrorOccured_; 

    //@ Data-Member:  isStalledDataErrorOccured_
    //flag for board install error
    Boolean isStalledDataErrorOccured_; 

    //@ Data-Member:  isFirstTime_
    //flag getting firmware rev
    Boolean isFirstTime_;

    //@ Data-Member:  prevInspiredVolume_
    //hold previous inspired volume
    Real32  prevInspiredVolume_;

    //@ Data-Member:  endInspiredFlag_
    //signals end of inspiration
    Boolean endInspiredFlag_;

    //@ Data-Member: endExhFlag_
    //signals end of exhalation
    Boolean endExhFlag_;

    //@ Data-Member: endExhFlag_
    //hold the current volume factor
    Real32 netVolumeFactor_;

    //@ Data-Member: updatedOnce_
    //Flag to indicate gas condition was sent
    Boolean updatedOnce_;


    //@ Data-Member: gasStarted_ 
    //Flag to indicate gas has started
    Boolean gasStarted_ ;

    //@ Data-Member: prevHumidType_ 
    //hold previous humifier type
    DiscreteValue prevHumidType_;

    //@ Data-Member: prevFio2_ 
    //hold previous o2 reading
    Uint8 prevFio2_;


    //@ Data-Member:  previousFaultPhase_
    // used to keep previous breath phase on a cycle bais
    // during prox at fault method.
    BreathPhaseType::PhaseType previousFaultPhase_;


};
#endif
#endif
