#ifndef  DownloadError_HH
#define	DownloadError_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: DownloadError - Enum definition of all the error codes
//   that can happen  during a download operation.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/DownloadError.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $ 
//@ Modification-Log
//
//  Revision: 001  By:  <jhv>    Date:  05-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================


enum DownloadError
{
	//NO_ERROR = 0,

	////Download LED error Numbers 0x30 - 0x4f reserved
	//// Disregard +100, it is to indicate LED error types. 

	//DOWNLOAD_MIN_ERR = 0x30 + 100,
	//DOWNLOAD_SERVER_ERROR = DOWNLOAD_MIN_ERR,
	//FLASH_MEMORY_WRITE_FAILED  = 0x31 + 100 ,
	//COMM_RECV_ERROR = 0x32 + 100 , 
	//COMM_SEND_ERROR = 0x33 + 100 , 
	//OUT_OF_SYNC_COMM_DATA = 0x34 + 100 ,
	//FLASH_CHECKSUM_FAILURE = 0x35 + 100 ,
	//FLASH_ERASE_FAILURE = 0x36 + 100 ,
	//SERVICE_DATA_CHECKSUM_FAILURE = 0x37 + 100 ,
	//FLASH_BURNING_DONE = 0x38 + 100 ,
	//STWARE_LOG_DIAGNOSTIC = 0x39 + 100 ,
	//STWARE_LOG_ERROR = 0x3a + 100 ,
	//SOCKET_LIB_ERROR = 0x3b + 100 ,
	//OUT_OF_DOWNLOAD_BUFFER = 0x3c + 100 ,
	//SERVER_NOT_SENDING_DATA = 0x3d + 100 ,
	//FLASH_BURNING_TIMEOUT = 0x3e + 100 ,
	//DOWNLOAD_DATA_CORRUPTION = 0x3f + 100 ,
	//ILLEGAL_TFTP_OP = 0x40 + 100 ,
	//INCOMPATIBLE_DATA_BLOCK_SIZE = 0x41 + 100 ,
	//FINDING_SERVER_TIMEOUT = 0x42 + 100 ,
	//CONNECT_TO_SERVER_TIMEOUT = 0x43 + 100 ,
	//SERVER_NOT_ACTIVE = 0x44 + 100,
	//DOWNLOAD_SOFTFAULT = 0x45 + 100 ,
	//DOWNLOAD_MAX_ERR = DOWNLOAD_SOFTFAULT
};
#endif // DownloadError_HH 
