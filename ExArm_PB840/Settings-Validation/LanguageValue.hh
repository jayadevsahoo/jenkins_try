
#ifndef LanguageValue_HH
#define LanguageValue_HH

//====================================================================
//
// This is a proprietary work to which Covidien corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Covidien Corporation of California.
//
//            Copyright (c) 2012, Covidien Corporation
//====================================================================

//====================================================================
// Class:  LanguageValue - Values of the Language Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /data/840/Baseline/Settings-Validation/vcssrc/LanguageValue.hhv   27.0   27 Aug 2013 10:42:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date:  06-Aug-2012    SCR Number: 6778
//  Project: ZHO1
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct LanguageValue
{
    //@ Type:  LanguageValueId 
    // All of the possible values of the Language Setting.
    enum LanguageValueId
    {
        // $[ZH02001] -- values of the range of Language Setting...
        ENGLISH,
        CHINESE,
        FRENCH,
        GERMAN,
        ITALIAN,
        JAPANESE,
        POLISH,
        PORTUGUESE,
        RUSSIAN,
        SPANISH,
        TOTAL_LANGUAGE_VALUES
    };
};


#endif // LanguageValue_HH 
