#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmStateManager -- Manager of the alarm states
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is used to process the changing states of alarms, and
//  provide that processed information to the reset of GUI-Apps.
//
//  This class applies the following rules to the annunciation (i.e.,
//  an on-screen, textual message, an LED indicator, and an audible alarm)
//  of alarms:
//
//		*	if the overall alarm urgency is de-escalating, but the higher
//			urgency was not active long enough for a full "burst" of its
//			corresponding alarm sound, the alarm will be held in the
//			higher state until that minimum time requirement has been met
//		*	if the overall alarm urgency is de-escalating, and the higher
//			urgency alarm's minimum time requirement has been met, the
//			higher urgency alarm is reset, immediately
//		*	if the overall alarm urgency is escalating, the higher
//			urgency will immediately take precedence over the previous
//			urgency
//		*	if the overall alarm urgency is unchanged, no changes to the
//			state of the alarm sounds, LEDs, or textual displays occurs
//			(see exception below)
//		*	if a low-urgency alarm is activated while already being in
//			a low-urgency alarm state, the newly-activated low-urgency
//			alarm will issue a new alarm burst (this is because the low
//			urgency sounds are a single burst, which is not repeated)
//		*	if the alarm-reset key or alarm-silence key is pressed, they
//			are responded to immediately (irregardless of annunciation
//			time)
//
//---------------------------------------------------------------------
//@ Rationale
//  A central class for interdicting alarm events and applying "special"
//  rules for resetting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class uses an array of 'AlarmUrgencyInfo' structs for managing
//  the minimum annunciation times and timers, for each alarm urgency.
//
//  This class also contains two static, sorted lists for managing the
//  current ('RCurrAlarmsList_') and previous ('RPrevAlarmsList_') list
//  of active alarms.  These lists are used to determine changes in
//  urgency levels, and to "hold" any alarms active, when necessary.
//
//  Since alarms are always supposed to be eliminated immediately upon
//  activation of an alarm reset or (for alarm sounds, only) alarm
//  silence, a set of static, boolean flags are used to manage these
//  transitions.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Checks for out-of-range enum values with assertions.
//---------------------------------------------------------------------
//@ Restrictions
//  None.  In particular, there is no restriction in changing from one
//  display mode to another at any time.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmStateManager.ccv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//  Removed DELTA project single screen support.
//
//  Revision: 009  By:  jja	   Date:  31-May-2000    DCS Number: 5669
//  Project:  NeoMode
//  Description:
//      Fixed date field in Mod-Log #007 entry.
//
//  Revision: 008  By:  sah	   Date:  21-Mar-2000    DCS Number: 5669
//  Project:  NeoMode
//  Description:
//      Fixed comments in Mod-Log #007 entry, to remove potential confusion.
//
//  Revision: 007  By:  hhd	   Date:  17-Nov-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//      Project-related changes:
//      *  removed display of "Alarm Silence Active" message
//      *  modified to reduce header file dependencies
//  
//  Revision: 006  By:  sah	   Date:  19-Oct-1999    DCS Number: 5551
//  Project:  ATC
//  Description:
//      Added comparison of analysis message arrays when determining if
//      alarm update events are equivalent.
//  
//  Revision: 005  By:  sah	   Date:  08-Sep-1999    DCS Number: 5525
//  Project:  ATC
//  Description:
//		Undid change done for DCS #5434, because it breaks other scripts.
//      No new attempt is needed due to S/W Test's work-around of original
//      problem.
//
//   Revision 004   By:   yyy      Date: 08/18/99         DR Number:   5513
//      Project:   ATC
//      Description:
//         Modified to handle alarm lock and break through.
//  
//  Revision: 003  By:  sah	   Date:  13-Aug-1999    DCS Number: 5434
//  Project:  ATC
//  Description:
//		Modified BTREE-only code to ALWAYS update BTREE's alarm urgency
//      status when different from actual alarm urgency status.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  sah    Date:  28-Jul-97    DCS Number: 2320
//  Project:  Sigma (R8027)
//  Description:
//       Integration baseline.
//
//=====================================================================


#include "AlarmStateManager.hh"

//@ Usage-Classes
#include "Led.hh"
#include "Sound.hh"
#include "KeyPanel.hh"
#include "SortIterC_AlarmUpdate.hh"
#include "GuiApp.hh"
#include "UpperScreen.hh"
#include "OsUtil.hh"
#include "AlarmAnnunciator.hh"
#include "BtreeVars.hh"
#include "LowerSubScreenArea.hh"
#include "AlarmSetupSubScreen.hh"
#include "GuiTimerId.hh"
#include "KeyHandlers.hh"
#include "AlarmSilenceResetHandler.hh"
#include "MessageArea.hh"
//@ End-Usage


//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

static Uint  PrevAlarmsListMemory_[
			(sizeof(AlarmUpdateSortedList) + sizeof(Uint) - 1) / sizeof(Uint)
								  ];

static Uint  CurrAlarmsListMemory_[
			(sizeof(AlarmUpdateSortedList) + sizeof(Uint) - 1) / sizeof(Uint)
								  ];


//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

AlarmStateManager::AlarmUrgencyInfo
				  AlarmStateManager::ArrUrgencyInfo_[Alarm::MAX_NUM_URGENCIES];

AlarmUpdateSortedList&  AlarmStateManager::RPrevAlarmsList_ =
							*((AlarmUpdateSortedList*)::PrevAlarmsListMemory_);
AlarmUpdateSortedList&  AlarmStateManager::RCurrAlarmsList_ =
							*((AlarmUpdateSortedList*)::CurrAlarmsListMemory_);

Boolean  AlarmStateManager::PrevAlarmsSilencedFlag_   = FALSE;
Boolean  AlarmStateManager::AlarmsResetActivatedFlag_ = FALSE;
Boolean  AlarmStateManager::DoPeriodicProcessing_     = FALSE;

const Uint  AlarmStateManager::MIN_LOW_URGENCY_TIME_  = 1000;  // milliseconds...
const Uint  AlarmStateManager::MIN_MED_URGENCY_TIME_  = 1500;  // milliseconds...
const Uint  AlarmStateManager::MIN_HIGH_URGENCY_TIME_ = 4000;  // milliseconds...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
//  This method is responsible for initializing the static data members
//  of this class, and preparing this class for the processing of alarm
//  state changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmStateManager::Initialize(void)
{
	CALL_TRACE("AlarmStateManager::Initialize()");

	//=================================================================
	// initialize array of urgency infos...
	//=================================================================

	for (Uint idx = 0u; idx < Alarm::MAX_NUM_URGENCIES; idx++)
	{
	  new (&ArrUrgencyInfo_[idx].urgencyTimer) OsTimeStamp();
	}

	ArrUrgencyInfo_[Alarm::NORMAL_URGENCY].minUrgencyTime = 0u;  // N/A...
	ArrUrgencyInfo_[Alarm::LOW_URGENCY].minUrgencyTime    = MIN_LOW_URGENCY_TIME_;
	ArrUrgencyInfo_[Alarm::MEDIUM_URGENCY].minUrgencyTime = MIN_MED_URGENCY_TIME_;
	ArrUrgencyInfo_[Alarm::HIGH_URGENCY].minUrgencyTime   = MIN_HIGH_URGENCY_TIME_;

	//=================================================================
	// initialize the two sorted list...
	//=================================================================

	new (&AlarmStateManager::RPrevAlarmsList_) AlarmUpdateSortedList();
	new (&AlarmStateManager::RCurrAlarmsList_) AlarmUpdateSortedList();

	//=================================================================
	// initialize periodic-processing flag...
	//=================================================================

	AlarmStateManager::DoPeriodicProcessing_ = FALSE;

	//=================================================================
	// initialize B-Tree variable...
	//=================================================================

	BtreeVars::CurrAlarmUrgency = Alarm::NORMAL_URGENCY;
}														// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessEventBasedAlarmUpdates()
//
//@ Interface-Description
//  This method is responsible for processing the changes in alarm states,
//  based on alarm events.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmStateManager::ProcessEventBasedAlarmUpdates(void)
{
	CALL_TRACE("AlarmStateManager::ProcessEventBasedAlarmUpdates()");

	const Boolean  ARE_ALARMS_SILENCED =
									AlarmAnnunciator::GetAlarmsAreSilenced();

	if (GuiApp::GetGuiState() == STATE_SST ||
		GuiApp::GetGuiState() == STATE_SERVICE)
	{	// Must not be in STATE_SST or STATE_SERVICE	// $[TI1]
		return;
	}													// $[TI2]

	if (GuiApp::IsSstFailure()  ||  GuiApp::IsServiceRequired())
	{													// $[TI3]
		// static boolean to indicate whether this state has been processed,
		// yet...
		static Boolean  HasBeenProcessed_ = FALSE;

		if (!HasBeenProcessed_)
		{												// $[TI3.1]
			// until we get an SST-Required Alarm (if ever), we shall
			// hard-code the UI reaction...
			Led::SetState(Led::NORMAL, Led::OFF_STATE);
			Led::SetState(Led::LOW_ALARM_URGENCY, Led::OFF_STATE);
			Led::SetState(Led::MEDIUM_ALARM_URGENCY, Led::OFF_STATE);
			Led::SetState(Led::HIGH_ALARM_URGENCY, Led::FAST_BLINK_STATE);
			Sound::Start(Sound::HIGH_URG_ALARM);
			::SetRemoteAlarm();		// Make sure remote alarm is on

			// store the current urgency...
			BtreeVars::CurrAlarmUrgency = Alarm::HIGH_URGENCY;

			HasBeenProcessed_ = TRUE;
		}												// $[TI3.2]

		return;
	}													// $[TI4]

	Led::LedState highUrgencyState;

	if (AlarmAnnunciator::GetHighUrgencyAutoReset())
	{													// $[TI5]
		highUrgencyState = Led::ON_STATE;
	}
	else
	{													// $[TI6]
		highUrgencyState = Led::OFF_STATE;
	}

	// update own copy of current alarm events...
	RCurrAlarmsList_ = AlarmAnnunciator::GetAlarmUpdates();

	const Alarm::Urgency  PREV_HIGHEST_URGENCY =
					  AlarmStateManager::CalcHighestUrgency_(RPrevAlarmsList_);
	const Alarm::Urgency  ACTUAL_CURR_HIGHEST_URGENCY =
					  AlarmStateManager::CalcHighestUrgency_(RCurrAlarmsList_);

	Alarm::Urgency  currHighestUrgency = ACTUAL_CURR_HIGHEST_URGENCY;

	if (currHighestUrgency < PREV_HIGHEST_URGENCY  &&
	    !AlarmsResetActivatedFlag_)
	{   												// $[TI7]
		// de-escalation NOT due to an alarms-reset action...
		const AlarmUrgencyInfo&  R_PREV_URGENCY =
										  ArrUrgencyInfo_[PREV_HIGHEST_URGENCY];

		if (R_PREV_URGENCY.urgencyTimer.getPassedTime() <
												R_PREV_URGENCY.minUrgencyTime)
		{												// $[TI7.1]
			// leave the (pre-maturely) reset alarm in the current list of
			// alarms...
			RPrevAlarmsList_.goFirst();
			RCurrAlarmsList_.addItem(RPrevAlarmsList_.currentItem());

			// set the current highest urgency level to what the previous
			// one was...
			currHighestUrgency = PREV_HIGHEST_URGENCY;

			// since we're waiting for a timer to expire, we need to
			// periodically check for this expiration...
			DoPeriodicProcessing_ = TRUE;
		}
		else
		{												// $[TI7.2]
			// no need for periodic processing, since an immediate de-escalation
			// is allowed...
			DoPeriodicProcessing_ = FALSE;
		}
	}													// $[TI8]

	// only process the current state if a new urgency is active, or the low
	// urgency is re-activated, or the alarms-silence state is newly activated,
	// or an alarms reset is in progress...
	const Boolean  DO_PROCESS_CURR_STATE =
					  (currHighestUrgency != PREV_HIGHEST_URGENCY        ||
					   currHighestUrgency == Alarm::LOW_URGENCY          ||
					   (PrevAlarmsSilencedFlag_ != ARE_ALARMS_SILENCED)  ||
					   AlarmsResetActivatedFlag_);	// $[TI9] (T)  $[TI10] (F)...

	if (DO_PROCESS_CURR_STATE)
	{														// $[TI11]
		if (currHighestUrgency != PREV_HIGHEST_URGENCY)
		{													// $[11.1]
			// start timer from change to new state...
			ArrUrgencyInfo_[currHighestUrgency].urgencyTimer.now();
		}													// $[11.2]

		switch (currHighestUrgency)
		{
		case Alarm::NORMAL_URGENCY:							// $[TI11.3]
			Led::SetState(Led::NORMAL, Led::ON_STATE);
			Led::SetState(Led::LOW_ALARM_URGENCY, Led::OFF_STATE);
			Led::SetState(Led::MEDIUM_ALARM_URGENCY, Led::OFF_STATE);
			Led::SetState(Led::HIGH_ALARM_URGENCY, highUrgencyState);
			Sound::CancelAlarm();
			::ClearRemoteAlarm();		// Make sure remote alarm is off
			break;

		case Alarm::LOW_URGENCY:							// $[TI11.4]
			Led::SetState(Led::NORMAL, Led::OFF_STATE);
			Led::SetState(Led::LOW_ALARM_URGENCY, Led::ON_STATE);
			Led::SetState(Led::MEDIUM_ALARM_URGENCY, Led::OFF_STATE);
			Led::SetState(Led::HIGH_ALARM_URGENCY, highUrgencyState);
			if (!ARE_ALARMS_SILENCED)
			{												// $[TI11.4.1]
				if (PREV_HIGHEST_URGENCY != Alarm::LOW_URGENCY  ||
					PrevAlarmsSilencedFlag_  					||
					AlarmsResetActivatedFlag_  					||
					AlarmStateManager::IsNewLowAlarmActive_())
				{											// $[TI11.4.1.1]
					// either the previous urgency was not equal to the new
					// urgency, OR the urgencies are equal and there's a new
					// low-urgency alarm, OR we were previously silenced,
					// but not any more, OR an alarm reset just occurred
					// leaving a low-urgency alarm active...
					Sound::Start(Sound::LOW_URG_ALARM);
				}											// $[TI11.4.1.2]
			}
			else
			{												// $[TI11.4.2]
				Sound::CancelAlarm();
			}
			::ClearRemoteAlarm();		// Make sure remote alarm is off
			break;

		case Alarm::MEDIUM_URGENCY:							// $[TI11.5]
			Led::SetState(Led::NORMAL, Led::OFF_STATE);
			Led::SetState(Led::LOW_ALARM_URGENCY, Led::OFF_STATE);
			Led::SetState(Led::MEDIUM_ALARM_URGENCY, Led::SLOW_BLINK_STATE);
			Led::SetState(Led::HIGH_ALARM_URGENCY, highUrgencyState);
			if (!ARE_ALARMS_SILENCED)
			{												// $[TI11.5.1]
				Sound::Start(Sound::MEDIUM_URG_ALARM);
				::SetRemoteAlarm();		// Make sure remote alarm is on
			}
			else
			{												// $[TI11.5.2]
				Sound::CancelAlarm();
				::ClearRemoteAlarm();		// Make sure remote alarm is off
			}
			break;

		case Alarm::HIGH_URGENCY:							// $[TI11.6]
			Led::SetState(Led::NORMAL, Led::OFF_STATE);
			Led::SetState(Led::LOW_ALARM_URGENCY, Led::OFF_STATE);
			Led::SetState(Led::MEDIUM_ALARM_URGENCY, Led::OFF_STATE);
			Led::SetState(Led::HIGH_ALARM_URGENCY, Led::FAST_BLINK_STATE);
			if (!ARE_ALARMS_SILENCED)
			{												// $[TI11.6.1]
				Sound::Start(Sound::HIGH_URG_ALARM);
				::SetRemoteAlarm();		// Make sure remote alarm is on
				// Make sure alarm silence display is dismissed.
				GuiApp::RKeyHandlers.getAlarmSilenceResetHandler().ClearDisplay();
			}
			else
			{												// $[TI11.6.2]
				Sound::CancelAlarm();
				::ClearRemoteAlarm();		// Make sure remote alarm is off
			}
			break;

		default:
			AUX_CLASS_ASSERTION_FAILURE(currHighestUrgency);
			break;
		};
	}														// $[TI12]

	//
	// Set the Alarm Silence Key LED.
	//
	KeyPanel::SetLight(KeyPanel::ALARM_SILENCE_KEY, ARE_ALARMS_SILENCED);

	Boolean  doScreenUpdate;

	if (currHighestUrgency == PREV_HIGHEST_URGENCY)
	{														// $[TI13]
		// the current and previous high urgencies are the same, therefore
		// check to make sure the current and previous individual alarms
		// are the same, if not the screen needs to be updated...

		SortedIterC(AlarmUpdate)  currAlarmsIter(RCurrAlarmsList_);
		SortedIterC(AlarmUpdate)  prevAlarmsIter(RPrevAlarmsList_);

		SigmaStatus  currStatus, prevStatus;
		Boolean      isEquiv = TRUE;

		for (currStatus = currAlarmsIter.goFirst(),
			 prevStatus = prevAlarmsIter.goFirst();
			 isEquiv  &&  currStatus == prevStatus  &&  currStatus == SUCCESS;
			 currStatus = currAlarmsIter.goNext(),
			 prevStatus = prevAlarmsIter.goNext())
		{
			const AlarmUpdate&  R_CURR_UPDATE = currAlarmsIter.currentItem();
			const AlarmUpdate&  R_PREV_UPDATE = prevAlarmsIter.currentItem();

			// make sure that the two items have the same urgency and priority,
			// AND make sure that one hasn't been augmented by an augmentation...
			// $[TI13.1] (TRUE)  $[TI13.2] (FALSE)
			isEquiv = (R_CURR_UPDATE == R_PREV_UPDATE  &&
			  R_CURR_UPDATE.getWhenOccurred() == R_PREV_UPDATE.getWhenOccurred()  &&
			  R_CURR_UPDATE.getAnalysisMessage() == R_PREV_UPDATE.getAnalysisMessage());
		}

		// if the individual alarm updates are not equivalent, OR there is
		// a different number of alarm updates between the two lists, the
		// screen needs to be updated...
		doScreenUpdate = (!isEquiv  ||  currStatus != prevStatus);
	}
	else
	{														// $[TI14]
		// the current and previous high urgencies are different, therefore
		// we definitely need to update the screen...
		doScreenUpdate = TRUE;
	}

	if (doScreenUpdate)
	{														// $[TI15]
		// update the alarm and status area...
		UpperScreen::RUpperScreen.getAlarmAndStatusArea()->updateAlarmMsgDisplay();
		LowerSubScreenArea::GetAlarmSetupSubScreen()->alarmEventHappened();
	}														// $[TI16]

	// reset static data members for next processing cycle...
	RPrevAlarmsList_          = RCurrAlarmsList_;
	PrevAlarmsSilencedFlag_   = ARE_ALARMS_SILENCED;
	AlarmsResetActivatedFlag_ = FALSE;

	//=================================================================
	//  BEGIN B-TREE ONLY BLOCK:  this code is only relavent to B-Tree...
	//=================================================================

	if (!DoPeriodicProcessing_)
	{
		switch (ACTUAL_CURR_HIGHEST_URGENCY)
		{
		case Alarm::NORMAL_URGENCY:
			BtreeVars::CurrAlarmUrgency = ACTUAL_CURR_HIGHEST_URGENCY;
			break;

		case Alarm::LOW_URGENCY:
		case Alarm::MEDIUM_URGENCY:
		case Alarm::HIGH_URGENCY:
			if (!ARE_ALARMS_SILENCED)
			{
				BtreeVars::CurrAlarmUrgency = ACTUAL_CURR_HIGHEST_URGENCY;
			}
			break;

		default:
			AUX_CLASS_ASSERTION_FAILURE(ACTUAL_CURR_HIGHEST_URGENCY);
			break;
		};
	}

	//=================================================================
	//  END B-TREE ONLY BLOCK.
	//=================================================================
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsNewLowAlarmActive_()
//
//@ Interface-Description
//  Returns a boolean as to whether a newly-activated, low-urgency alarm
//  is active.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (!RCurrAlarmsList_.isEmpty())
//  (CalcHighestUrgency_(RCurrAlarmsList_) == Alarm::LOW_URGENCY)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
AlarmStateManager::IsNewLowAlarmActive_(void)
{
	CALL_TRACE("AlarmStateManager::IsNewLowAlarmActive_()");
	SAFE_CLASS_PRE_CONDITION((!RCurrAlarmsList_.isEmpty()));
	SAFE_CLASS_PRE_CONDITION(
			(CalcHighestUrgency_(RCurrAlarmsList_) == Alarm::LOW_URGENCY)
							);
	SAFE_CLASS_PRE_CONDITION(
			(CalcHighestUrgency_(RPrevAlarmsList_) == Alarm::LOW_URGENCY)
							);

	Boolean  isNewLowAlarmActive;

	SortedIterC(AlarmUpdate)  currAlarmsIter(RCurrAlarmsList_);
	SortedIterC(AlarmUpdate)  prevAlarmsIter(RPrevAlarmsList_);

	SigmaStatus  status;

	isNewLowAlarmActive = FALSE;

	// NOTE:  since 'RCurrAlarmsList_' is a sorted list, based on priority,
	//        AND the highest priority is 'LOW_URGENCY', it can be safely
	//        assumed that ALL entries in the list are of 'LOW_URGENCY'.
	for (status = currAlarmsIter.goFirst();
		 status == SUCCESS  &&  !isNewLowAlarmActive;
		 status = currAlarmsIter.goNext())
	{
		// search for 'currAlarmsIter.currentItem()' among the previous
		// low-urgency alarms; if NOT found, then there's a new alarm...
		// $[TI1] (TRUE)  $[TI2] (FALSE)...
		isNewLowAlarmActive =
			(prevAlarmsIter.search(currAlarmsIter.currentItem()) == FAILURE);
	}

	return(isNewLowAlarmActive);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CalcHighestUrgency_()
//
//@ Interface-Description
//  Calculate and return the highest urgency level in the list of alarms
//  given by 'sortedList'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use the fact that the passed-in lists are sorted by urgency (highest
//  first).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Alarm::Urgency
AlarmStateManager::CalcHighestUrgency_(const AlarmUpdateSortedList& sortedList)
{
	CALL_TRACE("AlarmStateManager::CalcHighestUrgency_(sortedList)");

	SortedIterC(AlarmUpdate)  alarmUpdateIter(sortedList);
	Alarm::Urgency  highestUrgency;

	if (alarmUpdateIter.goFirst() == SUCCESS)
	{   // $[TI1] -- list is not empty...
		highestUrgency = alarmUpdateIter.currentItem().getCurrentUrgency();
	}
	else
	{   // $[TI2] -- list is empty...
		highestUrgency = Alarm::NORMAL_URGENCY;
	}

	return(highestUrgency);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmStateManager::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("AlarmStateManager::SoftFault()");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMSTATEMANAGER,
							lineNumber, pFileName, pPredicate);
}

