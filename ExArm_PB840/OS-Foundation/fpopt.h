/***************************************************************************
*
*		Copyright (c) 1990 READY SYSTEMS CORPORATION.
*
*	All rights reserved. READY SYSTEMS' source code is an unpublished 
*	work and the use of a copyright notice does not imply otherwise. 
*	This source code contains confidential, trade secret material of 
*	READY SYSTEMS. Any attempt or participation in deciphering, decoding,
*	reverse engineering or in any way altering the source code is 
*	strictly prohibited, unless the prior written consent of 
*	READY SYSTEMS is obtained.
*
*
*	Module Name:		fpopt.h
*
*	Identification:		@(#) 24.1 fpopt.h
*
*	Date:			6/16/94  10:17:53
*
****************************************************************************
*/
/* header file for fpopt: set/get floating point options (68881) */

#ifdef __cplusplus
extern "C" {
#endif

//TODO E600 removed
//extern int fpopt(_ANSIPROT2(int, ...));

/* commands */

#define FPOPT_BEGIN		0
#define FPOPT_END		1
#define FPOPT_SET_ROUND		2
#define FPOPT_GET_ROUND		3
#define FPOPT_SET_PREC		4
#define FPOPT_GET_PREC		5
#define FPOPT_SET_INF		6
#define FPOPT_GET_INF		7
#define FPOPT_SET_MASK		8
#define FPOPT_CLEAR_MASK	9
#define FPOPT_RESTORE_MASK	10
#define FPOPT_GET_MASK		11
#define FPOPT_CLEAR_ACCR	12
#define FPOPT_GET_ACCR		13

/* rounding modes */

#define FPOPT_ROUND_NEAREST	0
#define FPOPT_ROUND_ZERO	1
#define FPOPT_ROUND_DOWN	2
#define FPOPT_ROUND_UP		3

/* precision modes */

#define FPOPT_PREC_EXTENDED	0
#define FPOPT_PREC_SINGLE	1
#define FPOPT_PREC_DOUBLE	2

/* infinity modes */

#define FPOPT_INF_PROJECTIVE	1
#define FPOPT_INF_AFFINE	0

/* accrued exception conditions */

#define FPOPT_ACCR_INEXACT	8
#define FPOPT_ACCR_DIV0		16
#define FPOPT_ACCR_UNDER	32
#define FPOPT_ACCR_OVER		64
#define FPOPT_ACCR_INVALID	128

/* exception masks */

#define FPOPT_MASK_INEX1	1
#define FPOPT_MASK_INEX2	2
#define FPOPT_MASK_INEXACT	3
#define FPOPT_MASK_DIV0		4
#define FPOPT_MASK_UNDER	8
#define FPOPT_MASK_OVER		16
#define FPOPT_MASK_OPERR	32
#define FPOPT_MASK_SNAN		64
#define FPOPT_MASK_BSUN		128
#define FPOPT_MASK_INVALID	224

#ifdef __cplusplus
}
#endif
