#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DiagCodeLogSubScreen - The screen selected by pressing a select 
// button in the Diagnostic Log Menu on the Upper Other Screens Subscreen.
// Displays system diagnostic codes and POST error codes.
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DiagCodeLogSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:42   pvcs  $
//
//@ Modification-Log  
//
//  Revision: 023  By: mnr   Date: 24-Mar-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      SST support for PROX added.
//
//  Revision: 022 By: rhj    Date: 20-July-2010   SCR Number: 6612
//  Project:  MAT
//	Description:
//	    Prevent the word failure to be displayed during an assertion.
//
//  Revision: 021 By: rhj    Date: 15-Apr-2009   SCR Number: 6495
//  Project:  840S2
//	Description:
//	    Added a single test log. 
//
//  Revision: 020 By: rhj    Date: 16-Jan-2008   SCR Number: 6423
//  Project:  Baseline
//	Description:
//		Modified getLogEntryColumn() to return the original date 
//      format of the diagnostic logs when the laptop request 
//      occurs.
// 
//  Revision: 019 By: gdc    Date: 27-May-2007   SCR Number: 6330
//  Project:  Trend
//	Description:
//		Removed SUN prototype code.
//
//  Revision: 018 By: rhj    Date: 23-May-2007   SCR Number: 6237
//  Project:  Trend
//	Description:
//		Added Date International feature. Properly identified cheap
//      text strings in getLogEntryColumn.
//
//  Revision: 017 By: srp    Date: 28-May-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 016   By: hlg    Date:  07-Sep-2001    DCS Number: 5954
//  Project: GUIComms
//  Description:
//	Replaced generic serial port DCI error codes with unique DCI
//	error codes for each port.
//
//  Revision: 015   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 014  By:  hhd	   Date:  12-Mar-1999    DCS Number: 5340 
//  Project:  ATC
//  Description:
//		Modified to clear the logEntries_ before re/loading them with new data.
//
//  Revision: 013  By:  sah    Date:  21-May-1999    DCS Number: 5399
//  Project:  CostReduce
//  Description:
//      Changed the description of 'APP_NOVRAM_RESTORE_FAILURE' system
//      diagnostics to include the CPU name.
//
//  Revision: 012  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 011  By:  sah   Date:  11-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Changed error code information to be displayed as hexadecimal numbers.
//	Also, modified display of all hex numbers to include a "0x" moniker.
//
//  Revision: 010  By:  hhd	   Date:  15-Jan-1998    DCS Number: 5003
//  Project:  Sigma (R8027)
//  Description:
//      Fixed number of columns for System Information Log; Made necessary
//		call to GUI-Applications to initialize the newly added column.
//
//  Revision: 009  By:  gdc    Date:  10-Oct-1997    DCS Number: 2172
//  Project:  Sigma (R8027)
//  Description:
//      Added annotation for requirement 08054.
//
//  Revision: 008  By:  sah    Date:  30-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Replaced the system event ID for a diagnostic log clearance,
//      with an event ID for a task-control transition timeout.
//
//  Revision: 007  By:  yyy    Date:  10-Sep-97    DR Number: 2169
//    Project:  Sigma (R8027)
//    Description:
//      Removed obsolete SRS references.
//
//  Revision: 006  By:  yyy    Date:  10-Sep-97    DR Number: 1923
//    Project:  Sigma (R8027)
//    Description:
//      Changed the name of communications Diagnostic log to the system
//		information log.
//
//  Revision: 005  By:  yyy    Date:  15-JUL-97    DR Number: 2295
//    Project:  Sigma (R8027)
//    Description:
//      Removed 
//		SST/EST diagnostic display logs.
//
//  Revision: 004  By:  yyy    Date:  09-JUL-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      Removed SST: or EST: from the test EVENT and NOTE columns in the
//		SST/EST diagnostic display logs.
//
//  Revision: 003  By:  hhd    Date:  03-JUNE-97    DR Number: 2122
//       Project:  Sigma (R8027)
//       Description:
//    			Changed display format of POST data so that the event vector is 
//					displayed as DECIMAL number and the ErrCode as HEX number.
//
//  Revision: 002  By:  hhd    Date:  13-MAY-97    DR Number: 2085
//       Project:  Sigma (R8027)
//       Description:
//             Added display of program counter, NMI content, exception vector
//					and auxiliary code for POST information.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================
#include "DiagnosticCode.hh"
#include "DiagCodeLogSubScreen.hh"
#include "CommFaultId.hh"
#include "SmTestId.hh"
#include "DiagnosticFault.hh"
#include "LaptopMessageHandler.hh"
#include "LaptopEventRegistrar.hh"

//@ Usage-Classes
#include "Bitmap.hh"
#include "Image.hh"
#include "Exception.hh"
#include "MiscStrs.hh"
#include "NovRamEventRegistrar.hh"
#include "NovRamManager.hh"
#include "ScrollableLog.hh"
#include "ServiceUpperScreen.hh"
#include "UpperSubScreenArea.hh"
#include "TextUtil.hh"
#include "UpperScreen.hh"
#include "UpperSubScreenArea.hh"
#include "DiagLogMenuSubScreen.hh"
#include "ResultTableEntry.hh"
#include "TaskMonitor.hh"
#include "SmStatusId.hh"
#include "StringConverter.hh"
//@ End-Usage

//@ Code...
static const Int32 GO_BACK_BUTTON_X_ = 550;
static const Int32 GO_BACK_BUTTON_Y_ = 3;
static const Int32 GO_BACK_BUTTON_WIDTH_ = 80;
static const Int32 GO_BACK_BUTTON_HEIGHT_ = 25;
static const Int32 GO_BACK_BUTTON_BORDER_ = 2;
static const Int32 LOG_ROW_HEIGHT_ = 36;
static const Int32 LOG_X_ = -1;
static const Int32 LOG_Y_ = 38;
static const Int32 MAX_DIAG_CODE_STR_= 7;
static const Int32 NUM_COLUMNS4_ = 4;
static const Int32 NUM_COLUMNS5_ = 5;
static const Int32 NUM_ROWS_ = 6;
static const Int32 TEST_DIAG_CODE_COL_ = 2;
static const Int32 TEST_EVENT_COL_ = 1;
static const Int32 TEST_NOTE_TYPE_COL_ = 3;
static const Int32 TEST_NOTE_COL_ = 4;
static const Int32 TIME_STAMP_COL_ = 0;
static const Int32 SMALL_DATA_LENGTH_=3;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DiagCodeLogSubScreen()  [Constructor]
//
//@ Interface-Description
//	Constructs a DiagCodeLogSubScreen.  It is passed a pointer to the 
//	SubScreenArea.	
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Initializes data members; sets position, size and color of the upper
//	subscreen area; sets up the goBackButton_ and the pLog_.  Initialize
//  the test result strings.  Adds the titleArea, the goBackButton_
//	and the scrollable log to display.  Finally, register this subscreen
//  with the LaptopEventRegistrar.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DiagCodeLogSubScreen::DiagCodeLogSubScreen(SubScreenArea* pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleArea_(MiscStrs::DIAG_CODE_LOG_SUBSCREEN_TITLE,
			SubScreenTitleArea::SSTA_CENTER),
	pLog_(NULL),
	logType_(DiagCodeLogSubScreen::DL_SYSTEM),
	goBackButton_(GO_BACK_BUTTON_X_, GO_BACK_BUTTON_Y_,
					GO_BACK_BUTTON_WIDTH_, GO_BACK_BUTTON_HEIGHT_,
					Button::LIGHT, GO_BACK_BUTTON_BORDER_,
					Button::SQUARE, Button::NO_BORDER,
					MiscStrs::GO_BACK_BUTTON_TITLE),
	                leftArrowBitmap_( Image::RLeftArrowIcon )
{
	CALL_TRACE("DiagCodeLogSubScreen::DiagCodeLogSubScreen(pSubScreenArea)");

/****************************************************/

#ifdef DIAG_TEST
	DiagnosticCode diagCode;

/*****************************************************************************/

	// EST_TEST_GAS_SUPPLY_ID

	NovRamManager::StartingEstTest(SmTestId::EST_TEST_GAS_SUPPLY_ID);

	diagCode.setExtendedSelfTestCode(MINOR_TEST_FAILURE_ID, SmTestId::EST_TEST_GAS_SUPPLY_ID, 1);
	NovRamManager::LogEstSstDiagnostic(diagCode);

	diagCode.setExtendedSelfTestCode(MINOR_TEST_FAILURE_ID, SmTestId::EST_TEST_GAS_SUPPLY_ID, 2);

	NovRamManager::LogEstSstDiagnostic(diagCode);

	NovRamManager::CompletedEstTest(SmTestId::EST_TEST_GAS_SUPPLY_ID);
	NovRamManager::CompletedEst();


	// SST_TEST_EXPIRATORY_FILTER_ID
	NovRamManager::StartingSstTest(SmTestId::SST_TEST_EXPIRATORY_FILTER_ID);
	diagCode.setShortSelfTestCode(MINOR_TEST_FAILURE_ID, SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, 1);
	NovRamManager::LogEstSstDiagnostic(diagCode);

	diagCode.setShortSelfTestCode(MINOR_TEST_FAILURE_ID, SmTestId::SST_TEST_EXPIRATORY_FILTER_ID, 2);
	NovRamManager::LogEstSstDiagnostic(diagCode);

	NovRamManager::CompletedSstTest(SmTestId::SST_TEST_EXPIRATORY_FILTER_ID);


	// SST_TEST_CIRCUIT_LEAK_ID
	NovRamManager::StartingSstTest(SmTestId::SST_TEST_CIRCUIT_LEAK_ID);
	diagCode.setShortSelfTestCode(MINOR_TEST_FAILURE_ID, SmTestId::SST_TEST_CIRCUIT_LEAK_ID, 1);

	NovRamManager::LogEstSstDiagnostic(diagCode);

	NovRamManager::CompletedSstTest(SmTestId::SST_TEST_CIRCUIT_LEAK_ID);


	// SST_TEST_FLOW_SENSORS_ID
	NovRamManager::StartingSstTest(SmTestId::SST_TEST_FLOW_SENSORS_ID);
	diagCode.setShortSelfTestCode(MINOR_TEST_FAILURE_ID, SmTestId::SST_TEST_FLOW_SENSORS_ID, 1);
	NovRamManager::LogEstSstDiagnostic(diagCode);

	diagCode.setShortSelfTestCode(MINOR_TEST_FAILURE_ID, SmTestId::SST_TEST_FLOW_SENSORS_ID, 2);
	NovRamManager::LogEstSstDiagnostic(diagCode);

	diagCode.setShortSelfTestCode(MINOR_TEST_FAILURE_ID, SmTestId::SST_TEST_FLOW_SENSORS_ID, 3);
	NovRamManager::LogEstSstDiagnostic(diagCode);

	diagCode.setShortSelfTestCode(MINOR_TEST_FAILURE_ID, SmTestId::SST_TEST_FLOW_SENSORS_ID, 4);
	NovRamManager::LogEstSstDiagnostic(diagCode);

	diagCode.setShortSelfTestCode(MINOR_TEST_FAILURE_ID, SmTestId::SST_TEST_FLOW_SENSORS_ID, 5);
	NovRamManager::LogEstSstDiagnostic(diagCode);

	NovRamManager::CompletedSstTest(SmTestId::SST_TEST_FLOW_SENSORS_ID);

	// SST_TEST_COMPLIANCE_CALIBRATION
	NovRamManager::StartingSstTest(SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID);

	diagCode.setShortSelfTestCode(MAJOR_TEST_FAILURE_ID, SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID, 1);

	NovRamManager::LogEstSstDiagnostic(diagCode);

	NovRamManager::CompletedSstTest(SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID);

	
	// SST_TEST_PROX
	NovRamManager::StartingSstTest(SmTestId::SST_TEST_PROX_FLOW_ID);
	diagCode.setShortSelfTestCode(MAJOR_TEST_FAILURE_ID, SmTestId::SST_TEST_PROX_FLOW_ID, 1);
	NovRamManager::LogEstSstDiagnostic(diagCode);
	NovRamManager::CompletedSstTest(SmTestId::SST_TEST_PROX_FLOW_ID);

/*****************************************************************************/

	NovRamManager::CompletedSst();

/*****************************************************************************/

	// SYSTEM EVENT

	diagCode.setSystemEventCode(DiagnosticCode::BD_CLOCK_SYNC_PENDING);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setSystemEventCode(DiagnosticCode::BD_CLOCK_SYNC_DONE);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setSystemEventCode(DiagnosticCode::TASK_CONTROL_TRANSITION_TIMEOUT);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setSystemEventCode(DiagnosticCode::DIAG_LOG_ENTRY_CORRECTED);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setSystemEventCode(DiagnosticCode::WATCHDOG_TIMER_FAILED);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setSystemEventCode(DiagnosticCode::SETTINGS_STATE_MISMATCH);
	NovRamManager::LogSystemDiagnostic(diagCode);

	// COMMUNICATION

	diagCode.setCommunicationCode(NETWORKAPP_EPERM);
	NovRamManager::LogSystemInformation(diagCode);

	diagCode.setCommunicationCode(NETWORKAPP_INVALID_COMM_MSGID);
	NovRamManager::LogSystemInformation(diagCode);

	diagCode.setCommunicationCode(NETWORKAPP_NO_ACK_MESSAGE);
	NovRamManager::LogSystemInformation(diagCode);

	diagCode.setCommunicationCode(NETWORKAPP_MAX_ACTIVE_MSG_DELAY);
	NovRamManager::LogSystemInformation(diagCode);

	diagCode.setCommunicationCode(NETWORKAPP_MSG_TRANSMISSION_FAILED);
	NovRamManager::LogSystemInformation(diagCode);

	diagCode.setCommunicationCode(DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT1);
	NovRamManager::LogSystemInformation(diagCode);

	diagCode.setCommunicationCode(DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT2);
	NovRamManager::LogSystemInformation(diagCode);

	diagCode.setCommunicationCode(DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT3);
	NovRamManager::LogSystemInformation(diagCode);

	// Trap faults
	diagCode.setExceptionCode(2, 100, 200, 300);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setNmiCode(16, 7);
	NovRamManager::LogSystemDiagnostic(diagCode);

	// Background faults
	diagCode.setBackgroundTestCode(MINOR_TEST_FAILURE_ID, 1, 0);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setBackgroundTestCode(MINOR_TEST_FAILURE_ID, 32, 0);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setBackgroundTestCode(MINOR_TEST_FAILURE_ID, 33, 0);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setBackgroundTestCode(MINOR_TEST_FAILURE_ID, 40, 0);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setBackgroundTestCode(MINOR_TEST_FAILURE_ID, 41, 0);
	NovRamManager::LogSystemDiagnostic(diagCode);

	diagCode.setBackgroundTestCode(MINOR_TEST_FAILURE_ID, 43, 0);
	NovRamManager::LogSystemDiagnostic(diagCode);

	// Startup
	diagCode.setStartupSelfTestCode(MAJOR_TEST_FAILURE_ID, 1, 2, 3, 4, 5);
	NovRamManager::LogSystemDiagnostic(diagCode);

#endif

/****************************************************/


	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	// Set up go back button bitmap
	leftArrowBitmap_.setX(3);
	leftArrowBitmap_.setY(2);
	goBackButton_.addLabel( &leftArrowBitmap_ );

	// Set goBackButton_'s callback function
	goBackButton_.setButtonCallback(this);

	// Display the screen title and goBackButton_ and titleArea_
	addDrawable(&titleArea_);
	addDrawable(&goBackButton_);				

	TestResultString_[NOT_APPLICABLE_TEST_RESULT_ID] = MiscStrs::EMPTY_STRING;
	TestResultString_[UNDEFINED_TEST_RESULT_ID] = MiscStrs::DLOG_TEST_UNDEFINED;
	TestResultString_[INCOMPLETE_TEST_RESULT_ID] = MiscStrs::VTS_INCOMPLETE_TEST_LABEL;
	TestResultString_[NOT_INSTALLED_TEST_ID] = MiscStrs::DLOG_TEST_NOT_INSTALLED;
	TestResultString_[PASSED_TEST_ID] = MiscStrs::DLOG_TEST_PASSED;
	TestResultString_[OVERRIDDEN_TEST_ID] = MiscStrs::DLOG_TEST_OVERRIDDEN;
	TestResultString_[MINOR_TEST_FAILURE_ID] = MiscStrs::DLOG_TEST_MINOR_FAILURE;
	TestResultString_[MAJOR_TEST_FAILURE_ID] = MiscStrs::DLOG_TEST_FAILED;


	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{
		//
		// Register for laptop event callbacks.
		//
		LaptopEventRegistrar::RegisterTarget(LaptopEventId::UPLOAD_SYSTEM_LOG, this);
		LaptopEventRegistrar::RegisterTarget(LaptopEventId::UPLOAD_SYS_INFO_LOG, this);
		LaptopEventRegistrar::RegisterTarget(LaptopEventId::UPLOAD_EST_SST_LOG, this);
	}

	// Register all logs with NovRam for receiving event updates
	NovRamEventRegistrar::RegisterTarget(
		NovRamUpdateManager::SYS_INFO_LOG_UPDATE,
		this);
	NovRamEventRegistrar::RegisterTarget(
		NovRamUpdateManager::SYSTEM_LOG_UPDATE,
		this);
	NovRamEventRegistrar::RegisterTarget(
		NovRamUpdateManager::EST_LOG_UPDATE, 
		this); 
	NovRamEventRegistrar::RegisterTarget(
		NovRamUpdateManager::SST_LOG_UPDATE, 
		this); 
												// $[TI1]
	enableDateLocalization_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DiagCodeLogSubScreen  [Destructor]
//
//@ Interface-Description
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//	Empty 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DiagCodeLogSubScreen::~DiagCodeLogSubScreen(void)
{
	CALL_TRACE("DiagCodeLogSubScreen::~DiagCodeLogSubScreen(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()		[virtual]
//
//@ Interface-Description
// 	Activate the DiagCodeLogSubScreen. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the screen title.  Retrieve log info from NovRam.  Set the
//	column then the number of entries in the scrollable log.  Update
//	log display.  Activate the scrollable log.  Finally, register this
//  subscreen with NovRamEventRegistrar for signals of event updates.
// $[01193] The diagnostic code subscreen shall allowe user to view ...
// $[01197] If there are more entryis in a log than can be displayed ...
// $[01199] Each time a different log is activated the view shall ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DiagCodeLogSubScreen::activate(void)
{
	CALL_TRACE("DiagCodeLogSubScreen::activate(void)");


	StringId screenTitle = MiscStrs::DLOG_SYS_TITLE; 

	// Keep track of the screen title.  If the log is clear,
	// get the log data from NovRam.

	clearLogEntries();
	switch(logType_)
	{
	case DiagCodeLogSubScreen::DL_SYSTEM:				// $[TI1]
		screenTitle = MiscStrs::DLOG_SYS_TITLE;

		// Get system's log from NovRam
		NovRamManager::GetSystemLogEntries(logEntries_);
		pLog_ = ScrollableLog::New(this, NUM_ROWS_, NUM_COLUMNS5_, LOG_ROW_HEIGHT_);     
		pLog_->setColumnInfo(TEST_EVENT_COL_, 
							MiscStrs::DLOG_TEST_EVENT_LABEL,
							265, LEFT, 0, TextFont::NORMAL, TextFont::TEN,
							TextFont::TEN);
		pLog_->setColumnInfo(TEST_NOTE_TYPE_COL_, 
							MiscStrs::DLOG_TEST_TYPE_LABEL, 54, 
							CENTER, 0, TextFont::NORMAL, TextFont::TWELVE);
		pLog_->setColumnInfo(TEST_NOTE_COL_, 
							MiscStrs::DLOG_TEST_NOTES_LABEL,
							179, LEFT, 0, TextFont::NORMAL, 
							TextFont::TEN, TextFont::TEN);
		break;

	case DiagCodeLogSubScreen::DL_SYS_INFO:					// $[TI2]
		screenTitle = MiscStrs::DLOG_SYSTEM_INFO_TITLE;

		// Get system's log from NovRam
		NovRamManager::GetSystemInfoEntries(logEntries_);

		pLog_ = ScrollableLog::New(this, NUM_ROWS_, NUM_COLUMNS5_, LOG_ROW_HEIGHT_);     
		pLog_->setColumnInfo(TEST_EVENT_COL_, 
							MiscStrs::DLOG_TEST_EVENT_LABEL,
							265, LEFT, 0, TextFont::NORMAL, TextFont::TEN,
							TextFont::TEN);
		pLog_->setColumnInfo(TEST_NOTE_TYPE_COL_, 
							MiscStrs::DLOG_TEST_TYPE_LABEL, 54, 
							CENTER, 0, TextFont::NORMAL, TextFont::TWELVE);
		pLog_->setColumnInfo(TEST_NOTE_COL_, 
							MiscStrs::DLOG_TEST_NOTES_LABEL,
							179, LEFT, 0, TextFont::NORMAL, 
							TextFont::TEN, TextFont::TEN);
		break;

	case DiagCodeLogSubScreen::DL_EST_SST:						// $[TI3]
		screenTitle = MiscStrs::DLOG_EST_SST_TITLE; 

		NovRamManager::GetEstSstLogEntries(logEntries_);
		pLog_ = ScrollableLog::New(this, NUM_ROWS_, NUM_COLUMNS4_, LOG_ROW_HEIGHT_);
		pLog_->setColumnInfo(TEST_EVENT_COL_, 
							MiscStrs::DLOG_TEST_EVENT_LABEL,
							273, LEFT, 0, TextFont::NORMAL,  TextFont::TEN);
		pLog_->setColumnInfo(TEST_NOTE_TYPE_COL_, 
							MiscStrs::DLOG_TEST_NOTES_LABEL,
							225, LEFT, 0,
							TextFont::NORMAL, TextFont::TEN);
		break;

	default:												
			SAFE_CLASS_ASSERTION_FAILURE();
		break;
	}

	// Setup log
	pLog_->setX(LOG_X_);										
	pLog_->setY(LOG_Y_);										
	pLog_->setColumnInfo(TIME_STAMP_COL_, 
							MiscStrs::DLOG_TEST_TIME_DATE_LABEL,
							56, CENTER, 0, TextFont::NORMAL, TextFont::TEN, TextFont::EIGHT);
	pLog_->setColumnInfo(TEST_DIAG_CODE_COL_, 
							MiscStrs::DLOG_TEST_CODE_LABEL, 
							60, CENTER, 0, TextFont::NORMAL,
							TextFont::TWELVE);

	titleArea_.setTitle(screenTitle);

	// Find number of entries
	Int32 entry = 0;
	for (entry = 0; entry < logEntries_.getNumElems(); entry++)
	{														// $[TI5]
		if (logEntries_[entry].isClear())					// $[TI6]	
		{
			break;
		}	// $[TI7]
	} // $[TI8]

	pLog_->setEntryInfo(0, entry);
	addDrawable(pLog_);

	// Redraw the diagnostic log display as necessary
	updateDiagnosticLogDisplay_(getNovRamUpdateId(logType_));

	pLog_->activate();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()	[virtual]
//
//@ Interface-Description
// 	Deactivate DiagCodeLogSubScreen. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	Remove log from Drawable list.	
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DiagCodeLogSubScreen::deactivate(void)
{
	CALL_TRACE("DiagCodeLogSubScreen::deactivate(void)");

	pLog_->deactivate();
	removeDrawable(pLog_);
															// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
//	Called by ScrollableLog when it needs to retrieve the text content of a
//	particular column of a log entry.  Passed an entry number, a column number,
//	a CheapText flag.  Return four strings via parameter list.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	If log entry is empty then a NULL string is passed back.  Otherwise, fill up
//	temporary buffer for the content of each log column.  This temporary
//	buffer is what will be passed back through the parameter list.
// $[01194] Each displayed diagnostic code log entry shall contains ...
// $[01195] The time stamp portion of the diagnostic code log entry shall ...
// $[01321] If the date or time is changed, an entry shall be placed ...
//---------------------------------------------------------------------
//@ PreCondition
//	Entry has to be less than log's total number of element.
//	Column has to be within bound set. 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
DiagCodeLogSubScreen::getLogEntryColumn(Uint16 entry, Uint16 column,
				Boolean &rIsCheapText, StringId &rString1, StringId &rString2, 
				StringId &, StringId &)
{
	CALL_TRACE("DiagCodeLogSubScreen::getLogEntryColumn(Uint16, Uint16, Boolean &," 
							"StringId &, StringId &, StringId &, StringId &)");

	SAFE_CLASS_ASSERTION(entry < logEntries_.getNumElems());
	SAFE_CLASS_ASSERTION(
			column < (logType_ == DiagCodeLogSubScreen::DL_SYSTEM ? 
								NUM_COLUMNS5_ : NUM_COLUMNS4_));

	static wchar_t tmpBuffer[256];
	static wchar_t tmpBuffer2[256];

	tmpBuffer[0] = tmpBuffer2[0] = '\0';
	rIsCheapText = FALSE;
	rString1 = &tmpBuffer[0];
	rString2 = NULL_STRING_ID;


	// Maybe the array entry is not set at all.  Just return nothing.
	if (logEntries_[entry].isClear())					// $[TI1]
	{
		rString1 = NULL_STRING_ID;
	}
	else
	{
		if (!logEntries_[entry].isCorrected())			// $[TI2]	
		{
			const DiagnosticCode &R_DIAG_CODE =
								logEntries_[entry].getDiagnosticCode();
			DiagnosticCode::DiagCodeTypeId	
								diagCodeTypeId = R_DIAG_CODE.getDiagCodeTypeId();
			DiagnosticCode::StartupSelfTestBits startupSelfTestBits; 
			DiagnosticCode::ExceptionBits exceptionBits; 
			DiagnosticCode::NmiBits nmiBits; 
			DiagnosticCode::BackgroundTestBits backgroundTestBits;
			DiagnosticCode::SoftwareTestBits softwareTestBits;
			DiagnosticCode::ShortSelfTestBits shortSelfTestBits;
			DiagnosticCode::ExtendedSelfTestBits extendedSelfTestBits;
			DiagnosticCode::SystemEventBits systemEventBits;

			switch (column)
			{
			case TIME_STAMP_COL_:
			{											// $[TI5]
				// Timestamp, let's format it
				TimeStamp timeStamp = logEntries_[entry].getWhenOccurred();
	
				// Formatting Time/Date string
				if (GuiApp::IsSerialCommunicationUp() == TRUE)
				{										// $[TI6]
#ifdef FORNOW
printf("TIME_STAMP_COL_, line %d\n", __LINE__);
#endif
                    if (enableDateLocalization_)
					{
						TextUtil::FormatTime(tmpBuffer, L"%H:%M:%S %P", timeStamp);
					}
					else
					{
						TextUtil::FormatTime(tmpBuffer, L"%H:%M:%S %d %n %y", timeStamp);
					}

				}
				else
				{										// $[TI7]
#ifdef FORNOW
printf("TIME_STAMP_COL_, line %d\n", __LINE__);
#endif
					TextUtil::FormatTime(tmpBuffer, L"{p=10,y=10:%H:%M:%S}", timeStamp);

					if (enableDateLocalization_)
					{
						TextUtil::FormatTime(tmpBuffer2, L"{p=8,y=10:%P}", timeStamp);
					}
					else
					{
						TextUtil::FormatTime(tmpBuffer2, L"{p=8,x=2,y=32:%d %n %y}", timeStamp);
					}

					rString2 = &tmpBuffer2[0];
					// rIsCheapText set false so LogCell centers the text
					rIsCheapText = FALSE;
				}
				break;
			}
			case TEST_EVENT_COL_:						// $[TI8]
			{
				// 
				// Check what type of diagnostic code we have then prepare the
				// tmpBuffer to contain the appropriate data for the current column.
				//
				// Test/Event
				switch(diagCodeTypeId)
				{
				case DiagnosticCode::COMMUNICATION_DIAG:	// $[TI9]
				{
					DiagnosticCode::CommunicationBits communicationBits = 
									R_DIAG_CODE.getCommunicationCode();
	
							swprintf(tmpBuffer, L"%s", DiagnosticFault::GetCommTestEventStr(
											communicationBits.commFaultId)); 
	
							break;
				}
				case DiagnosticCode::SOFTWARE_TEST_DIAG:
				{
													// $[TI10]
					swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_ASSERTION_FAILURE);
					break;
				}
				case DiagnosticCode::NMI_DIAG:
				{										// $[TI11]
					nmiBits = R_DIAG_CODE.getNmiCode();

					// TODO E600 port
					//Exception::GetNmiMessage(nmiBits.nmiSourceId, nmiBits.errorCode,
					//		nmiBits.cpuFlagId, tmpBuffer, 256, tmpBuffer2, 256);
					rString2 = &tmpBuffer2[0];
					break;
				}
				case DiagnosticCode::EXCEPTION_DIAG:
				{										// $[TI12]
					exceptionBits =	R_DIAG_CODE.getExceptionCode();
					// TODO E600 port
					//swprintf(tmpBuffer, "%s",
					//	Exception::GetVectorName(exceptionBits.vectorNumber));
					break;
				}
				case DiagnosticCode::STARTUP_SELF_TEST_DIAG:
				{										// $[TI13]
					startupSelfTestBits = R_DIAG_CODE.getStartupSelfTestCode();
					swprintf(tmpBuffer, L"%s", 
						DiagnosticFault::GetStartupTestEventStr
								(startupSelfTestBits.startupFaultId));
					break;
				}
				case DiagnosticCode::BACKGROUND_TEST_DIAG:
				{										// $[TI14]
					backgroundTestBits = R_DIAG_CODE.getBackgroundTestCode();
					swprintf(tmpBuffer, L"%s", DiagnosticFault::GetBackgroundFaultStr
									(backgroundTestBits.testNumber));      
					break;
				}
				case DiagnosticCode::SHORT_SELF_TEST_DIAG:
				{										// $[TI15]
					shortSelfTestBits = R_DIAG_CODE.getShortSelfTestCode();
					// If SST test is complete, announce it
					if ( shortSelfTestBits.testNumber == ::MAX_SST_RESULTS)
					{									// $[TI16]
						swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_SST_COMPLETED);
					}
					// Else display the test name
					else if ( shortSelfTestBits.failureLevel == 
											MAJOR_TEST_FAILURE_ID ||
								shortSelfTestBits.failureLevel ==
											MINOR_TEST_FAILURE_ID || 
								shortSelfTestBits.failureLevel ==
											NOT_INSTALLED_TEST_ID ) 
					{									// $[TI17]
						swprintf(tmpBuffer, L"%s",
							DiagnosticFault::GetSstTestNameStr(
											shortSelfTestBits.testNumber));
					}
					else
					{									 // $[TI18]
						rString1 = NULL_STRING_ID;
					}
					break;
				}
				case DiagnosticCode::EXTENDED_SELF_TEST_DIAG:
				{										// $[TI19]
					extendedSelfTestBits = R_DIAG_CODE.getExtendedSelfTestCode();

					// Single test log.
					if (extendedSelfTestBits.isSingleTest == TRUE )
					{					
						swprintf(tmpBuffer, L"%s",
								MiscStrs::DLOG_EST_SINGLE_TEST_INITIATED);
					}
					// If EST test is complete, announce it
					else if ( extendedSelfTestBits.testNumber == ::MAX_EST_RESULTS )
					{									 // $[TI20]
						swprintf(tmpBuffer, L"%s",
										MiscStrs::DLOG_EST_COMPLETED);
					}
					// Else display test name
					else if ( extendedSelfTestBits.failureLevel == 
											MAJOR_TEST_FAILURE_ID || 
						extendedSelfTestBits.failureLevel ==
											MINOR_TEST_FAILURE_ID || 
						extendedSelfTestBits.failureLevel ==
											NOT_INSTALLED_TEST_ID ) 
					{									 // $[TI21]	
						swprintf(tmpBuffer, L"%s", 
							DiagnosticFault::GetEstTestNameStr
													(extendedSelfTestBits.testNumber));
					}	
					else
					{									 // $[TI22]
						rString1 = NULL_STRING_ID;
					}
					break;
				}
				case DiagnosticCode::SYSTEM_EVENT_DIAG:	
				{										// $[TI23]
					systemEventBits = R_DIAG_CODE.getSystemEventCode();

					const Uint16  SYSTEM_EVENT_ID = systemEventBits.systemEventId;

					if (SYSTEM_EVENT_ID == DiagnosticCode::APP_NOVRAM_RESTORE_FAILURE)
					{	// $[TI23.1] -- insert CPU into description...
						const Uint16  CPU_ID = systemEventBits.cpuFlagId;

						swprintf(tmpBuffer, L"%s %s",
							((CPU_ID == DiagnosticCode::BD_CPU_DIAG) ? L"BD" : L"GUI"),
							DiagnosticFault::GetSystemEventStr(SYSTEM_EVENT_ID));
					}
					else
					{	// $[TI23.2] -- just display the description...
						swprintf(tmpBuffer, L"%s",
						   DiagnosticFault::GetSystemEventStr(SYSTEM_EVENT_ID));
					}
					break;
				}
				default:								// $[TI24]
					SAFE_CLASS_ASSERTION_FAILURE();
					break;
				}
				break;
			}
			case TEST_DIAG_CODE_COL_:
			{											// $[TI30]
				// No diagnostic code is displayed if this entry contains the overall
				// result of an EST or SST test.
				if (((diagCodeTypeId == DiagnosticCode::SHORT_SELF_TEST_DIAG) 
					&&
					(R_DIAG_CODE.getShortSelfTestCode().testNumber == ::MAX_SST_RESULTS)) 
					||
					((diagCodeTypeId == DiagnosticCode::EXTENDED_SELF_TEST_DIAG) 
					&&
					(R_DIAG_CODE.getExtendedSelfTestCode().testNumber == ::MAX_EST_RESULTS))
				   )
				{					// $[TI33] 
						break;
				}					// $[TI34]
				wchar_t diagCodeString[MAX_DIAG_CODE_STR_];	
				R_DIAG_CODE.getDiagCodeString(diagCodeString);
	
				swprintf(tmpBuffer, L"%s", diagCodeString);
				break;
			}	
			case TEST_NOTE_TYPE_COL_:							
			{						// $[TI35]	
				// This column contains "Notes" in EST/SST log; "Type" for others. 
				switch(diagCodeTypeId)
				{					// $[TI36]
				case DiagnosticCode::COMMUNICATION_DIAG:
				{
					// How to determine severity level, display "Alert" FORNOW
	
					if (GuiApp::IsSerialCommunicationUp() == TRUE)
					{									// $[TI37]
						swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_TYPE_ALERT);
					}
					else
					{				   // $[TI38]
						swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_TYPE_ALERT_CHP_TXT);
					}
					break;
				}
				case DiagnosticCode::SOFTWARE_TEST_DIAG:
					break;
				case DiagnosticCode::NMI_DIAG:
				// Falling through to case EXCEPTION_DIAG
	
				case DiagnosticCode::EXCEPTION_DIAG:	// $[TI40]
				{
					swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_TYPE_FAILURE);
					break;
				}
				case DiagnosticCode::STARTUP_SELF_TEST_DIAG:
				{										// $[TI41]
					startupSelfTestBits = R_DIAG_CODE.getStartupSelfTestCode();
					if (startupSelfTestBits.failureLevel ==
								       MAJOR_TEST_FAILURE_ID)
					{									// $[TI42]
						swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_TYPE_FAILURE);
					}
					else if (startupSelfTestBits.failureLevel ==
								       MINOR_TEST_FAILURE_ID)
					{									// $[TI43]
						if (GuiApp::IsSerialCommunicationUp() == TRUE)
						{  							// $[TI43.1]

							swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_TYPE_ALERT);
						}
						else							// $[TI44]
						{
							swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_TYPE_ALERT_CHP_TXT);
						}
					}
					break;
				}
				case DiagnosticCode::BACKGROUND_TEST_DIAG:
				{										// $[TI45]
					backgroundTestBits = R_DIAG_CODE.getBackgroundTestCode();
					if (backgroundTestBits.failureLevel ==
														MAJOR_TEST_FAILURE_ID)
					{									// $[TI46]
						swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_TYPE_FAILURE);
					}
					else if (backgroundTestBits.failureLevel ==
														MINOR_TEST_FAILURE_ID)
					{									// $[TI47]
						if (GuiApp::IsSerialCommunicationUp() == TRUE)
						{			       // $[TI48]
							swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_TYPE_ALERT);
						}
						else
						{			       // $[TI49]
							swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_TYPE_ALERT_CHP_TXT);
						}
					}
					break;
				}
				case DiagnosticCode::SHORT_SELF_TEST_DIAG:
				{			       		// $[TI50]
					shortSelfTestBits = R_DIAG_CODE.getShortSelfTestCode();
	
					// If this entry stores overall test result then the test has 
					// been complete.  Display the overall result.
					if ( shortSelfTestBits.testNumber == ::MAX_SST_RESULTS )
					{				       // $[TI51]
						swprintf(tmpBuffer, L"{%s {N:%s}}", 
							MiscStrs::DLOG_OUTCOME,
							TestResultString_[shortSelfTestBits.failureLevel]);
					}
					else if ( shortSelfTestBits.failureLevel == 
											MAJOR_TEST_FAILURE_ID || 
						shortSelfTestBits.failureLevel == 
											MINOR_TEST_FAILURE_ID )
					{									// $[TI52], $[TI53]
						swprintf(tmpBuffer, L"%s", 
								DiagnosticFault::GetSstTestEventStr(
											shortSelfTestBits.testNumber, 
											shortSelfTestBits.stepNumber)); 
					}
					else if ( shortSelfTestBits.failureLevel ==
										NOT_INSTALLED_TEST_ID )	
					{									// $[TI53.1]
						swprintf(tmpBuffer, L"%s:%s", MiscStrs::DLOG_SST,
									MiscStrs::DLOG_TEST_NOT_INSTALLED);
					}
					else
					{				       // $[TI54]
						rString1 = NULL_STRING_ID;
					}
					break;
				}
				case DiagnosticCode::EXTENDED_SELF_TEST_DIAG:
				{
									       // $[TI55]
					extendedSelfTestBits = R_DIAG_CODE.getExtendedSelfTestCode();

					// Single Test log
					if (extendedSelfTestBits.isSingleTest == TRUE)
					{								
						swprintf(tmpBuffer, L"{%s {N:%s}}", MiscStrs::DLOG_OUTCOME,
								MiscStrs::DLOG_TEST_ALL_TESTS_REQUIRED);
					}
					// If this entry stores overall result 
					// then the test has been complete.  Display overall result.
					else if ( extendedSelfTestBits.testNumber == ::MAX_EST_RESULTS )
					{				       // $[TI56]
						swprintf(tmpBuffer, L"{%s {N:%s}}", MiscStrs::DLOG_OUTCOME,
							TestResultString_[extendedSelfTestBits.failureLevel]);
					}
					else if ( extendedSelfTestBits.failureLevel == 
										MAJOR_TEST_FAILURE_ID ||
						extendedSelfTestBits.failureLevel == 
										MINOR_TEST_FAILURE_ID ) 
					{				       // $[TI57]
						swprintf(tmpBuffer, L"%s",
								DiagnosticFault::GetEstTestEventStr
										(extendedSelfTestBits.testNumber,
										extendedSelfTestBits.stepNumber)); 
					}
					else if ( extendedSelfTestBits.failureLevel ==
										NOT_INSTALLED_TEST_ID )	
					{					 	// $[TI57.1]

						swprintf(tmpBuffer, L"%s:%s", MiscStrs::DLOG_EST,
									MiscStrs::DLOG_TEST_NOT_INSTALLED);
					}
					else	                                    
					{				       // $[TI58]
						rString1 = NULL_STRING_ID;
					}
					break;
				}
				case DiagnosticCode::SYSTEM_EVENT_DIAG:
				{				       	// $[TI59]
					Uint16 systemEventId = R_DIAG_CODE.getSystemEventCode().systemEventId;
					if (systemEventId == DiagnosticCode::WATCHDOG_TIMER_FAILED)
					{				       // $[TI60]
						swprintf(tmpBuffer, L"%s", MiscStrs::DLOG_TYPE_FAILURE);
					}
															// $[TI61]	
					break;				
				}
				default:
					CLASS_ASSERTION_FAILURE();
					break;
				}	// End switch
				break;
			}	
			case TEST_NOTE_COL_:							// $[TI62]
			// This column contains "Notes" for all logs except for EST/SST
			{
				switch(diagCodeTypeId)
				{
				case DiagnosticCode::COMMUNICATION_DIAG:	// $[TI63]
					// There will not be a 3rd string sent.  It is incorporated into
					// FaultId code. 
					break;
	
			    case DiagnosticCode::SOFTWARE_TEST_DIAG:	// $[TI64]
					softwareTestBits = R_DIAG_CODE.getSoftwareTestCode();
					swprintf(tmpBuffer, L"%s %d %s %d",
										MiscStrs::DLOG_TASK, 
										softwareTestBits.activeTaskId, 
										MiscStrs::DLOG_LINE, 
										softwareTestBits.lineNumber);
					swprintf(tmpBuffer2, L"%s  0x%X", MiscStrs::DLOG_ERRCODE, softwareTestBits.auxErrorCode);

					rString2 = &tmpBuffer2[0];
					break;
	
				case DiagnosticCode::NMI_DIAG:				 // $[TI65]
					nmiBits = R_DIAG_CODE.getNmiCode();
					swprintf(tmpBuffer, L"%s 0x%2.2X", MiscStrs::DLOG_SRCID, nmiBits.nmiSourceId);
					swprintf(tmpBuffer2, L"%s 0x%4.4X", MiscStrs::DLOG_ERRCODE, nmiBits.errorCode);
					rString2 = &tmpBuffer2[0];
					break;
	
				case DiagnosticCode::EXCEPTION_DIAG:		// $[TI66]
					exceptionBits =	R_DIAG_CODE.getExceptionCode();
					swprintf(tmpBuffer, L"%s 0x%08.8X %s %3d",
										MiscStrs::DLOG_PC,	
										exceptionBits.instructionAddr,
										MiscStrs::DLOG_TASK_ID,
										exceptionBits.activeTaskId);
					swprintf(tmpBuffer2, L"%s 0x%08.8X", 
										MiscStrs::DLOG_FA,
										exceptionBits.faultAddr);
					rString2 = &tmpBuffer2[0];
					break;
	
			    case DiagnosticCode::STARTUP_SELF_TEST_DIAG:// $[TI67]
				 	startupSelfTestBits = R_DIAG_CODE.getStartupSelfTestCode();
					swprintf(tmpBuffer, L"%s 0x%08.8X %s %d",
										MiscStrs::DLOG_PC,	
										startupSelfTestBits.programCounter,
										MiscStrs::DLOG_EXCEPTION_VECTOR,
										startupSelfTestBits.vectorNum);
					swprintf(tmpBuffer2, L"%s 0x%02.2X %s 0x%02.2X", 
										MiscStrs::DLOG_NMI_SHORT,
										startupSelfTestBits.nmiSourceReg,
										MiscStrs::DLOG_ERRCODE,
										startupSelfTestBits.auxErrorCode
										);
					rString2 = &tmpBuffer2[0];
					break;
	
		    	case DiagnosticCode::BACKGROUND_TEST_DIAG:	// $[TI68]
					backgroundTestBits = R_DIAG_CODE.getBackgroundTestCode();
					swprintf(tmpBuffer, L"%s %d %s", 
						MiscStrs::DLOG_TASK, 
						backgroundTestBits.activeTaskId,
						(backgroundTestBits.cpuFlagId == 1) //'1' for BD; '2' for GUI
											? MiscStrs::DLOG_BD : MiscStrs::DLOG_GUI);
	
					swprintf(tmpBuffer2, L"%s 0x%X", 
										MiscStrs::DLOG_ERRCODE,
										backgroundTestBits.errorCode);
					rString2 = &tmpBuffer2[0];
					break;
	
		    	case DiagnosticCode::SHORT_SELF_TEST_DIAG:	// $[TI69]
			    case DiagnosticCode::EXTENDED_SELF_TEST_DIAG:
															// $[TI70]
					break;
			    case DiagnosticCode::SYSTEM_EVENT_DIAG:
									// $[TI71]
					systemEventBits = R_DIAG_CODE.getSystemEventCode();
					swprintf(tmpBuffer, L"%s  0x%X", MiscStrs::ALARM_LOG_EVENT, systemEventBits.auxEventCode);
					break;
	
			    default:									// $[TI72]
					SAFE_AUX_CLASS_ASSERTION_FAILURE(diagCodeTypeId);
					break;
		    	}   // End switch
				break;
			}
			default:										// $[TI73]
				SAFE_AUX_CLASS_ASSERTION_FAILURE(column);
				break;
			} // End switch
	
		} // End if
		else  	// Entry is corrected
		{
			switch (column)
			{
			case 
                TIME_STAMP_COL_:
			{											// $[TI74]
				// Timestamp, let's format it
				TimeStamp timeStamp = logEntries_[entry].getWhenOccurred();

				// Formatting Time/Date string
				if (GuiApp::IsSerialCommunicationUp() == TRUE)
				{										// $[TI74.1]
#ifdef FORNOW
printf("TIME_STAMP_COL_, line %d\n", __LINE__);
#endif
                    if (enableDateLocalization_)
					{
						TextUtil::FormatTime(tmpBuffer, L"%H:%M:%S %P", timeStamp);
					}
					else
					{
						TextUtil::FormatTime(tmpBuffer, L"%H:%M:%S %d %n %y", timeStamp);
					}

				}
				else
				{										// $[TI74.2]
#ifdef FORNOW
printf("TIME_STAMP_COL_, line %d\n", __LINE__);
#endif
					TextUtil::FormatTime(tmpBuffer, L"{p=10,x=3,y=16:%H:%M:%S}", timeStamp);

					if (enableDateLocalization_)
					{
						TextUtil::FormatTime(tmpBuffer2, L"{p=8,y=10:%P}", timeStamp);
					}
					else
					{
						TextUtil::FormatTime(tmpBuffer2, L"{p=8,x=2,y=32:%d %n %y}", timeStamp);
					}

					rString2 = &tmpBuffer2[0];
					rIsCheapText = FALSE;
				}
				break;
			}
			case TEST_EVENT_COL_:						// $[TI75]
			{
				// Check what type of diagnostic code we have then prepare the
				// tmpBuffer to contain the appropriate data for the current column.
				// Test/Event
				wcscpy(tmpBuffer, MiscStrs::DLOG_UNKNOWN_DIAGCODE_EVENT);
				rString2 = NULL_STRING_ID;	
				break;
			}
			case TEST_DIAG_CODE_COL_:					// $[TI76]	
			case TEST_NOTE_TYPE_COL_:					// $[TI77]	
				rString1 = NULL_STRING_ID;
				break;
			case TEST_NOTE_COL_:						// $[TI78]
				wcscpy(tmpBuffer, MiscStrs::DLOG_DATA_CORRUPT);
				rString2 = NULL_STRING_ID;	
				break;
				
			default:
				SAFE_CLASS_ASSERTION_FAILURE();
				break;
			} // End switch
		}	// End else	
	}	// End else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Handles button down events for all of the "instant action" screen
// select buttons on this subscreen.  At the moment, it is used for 
// responding to button down events on the goBackButton_.
//  The `pButton' parameter is the button that was pressed down.
// The `isByOperatorAction' flag is TRUE if the given button was pressed
// by operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Reset the goBackButton to the "up" state so that it will be in the 
// 	correct state the next time this subscreen is displayed.  Deactivate the
//	diagnostic log then retrieving the DiagLogMenuSubScreen subscreen.
//	The screen select tab button is also retrieved. 
//	The DiagLogMenuSubScreen is activated and associated to this tab button.
//	The tab button is immediately set to down to resolve the problem of
//	activating a new screen automatically bouncing back up the tab button.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiagCodeLogSubScreen::buttonDownHappened(Button* pButton, Boolean)
{
CALL_TRACE("DiagCodeLogSubScreen::buttonDownHappened(Button* , Boolean )");

	SAFE_CLASS_ASSERTION(pButton == &goBackButton_);

	DiagLogMenuSubScreen	*pSubscreen;
	TabButton 				*pTabButton;

	// Bounce the "go back" button.
	goBackButton_.setToUp();

	// Deactivate/dismiss the diagnostic log.
	getSubScreenArea()->deactivateSubScreen();

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{	// Service Mode									// $[TI1]

		// Fetch the previous screen, the diagnostic log menu subscreen.
		pSubscreen = UpperSubScreenArea::GetDiagLogMenuSubScreen();
		//
		// Fetch the Upper Diag Result's tab button.
		//
		pTabButton = 
			ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->
									getDiagResultTabButton();
	}
	else
	{  													// $[TI2]
		// Fetch the previous screen, the diagnostic log menu subscreen.
		pSubscreen = UpperSubScreenArea::GetDiagLogMenuSubScreen();
		//
		// Fetch the Upper Other Screens tab button.
		//
		pTabButton = UpperScreen::RUpperScreen.getUpperScreenSelectArea()->
										getUpperOtherScreensTabButton();
	} 

	SAFE_CLASS_ASSERTION(pSubscreen && pTabButton);

	// Activate the subscreen and force down the associated tab 
	// button. $[01063]
	getSubScreenArea()->activateSubScreen(pSubscreen, pTabButton);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDiagnosticLogDisplay_
//
//@ Interface-Description
// Redraw the diagnostic log display as necessary.  This processing only
// takes place if the subscreen is displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Mostly this function is called when a new entry has been added to the
// diagnostic log.  If we are currently displaying the top of the log then
// we should display the new entry.  If however we are displaying another
// part of the log we try to keep the same log entries displayed.  We
// use the firstDisplayedEntryId to help us find where to set our
// position in the newest diagnostic log so as to keep the display unchanged. 
//
// $[01198] If the view is positioned at the "top" of the log ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DiagCodeLogSubScreen::updateDiagnosticLogDisplay_(NovRamUpdateManager::UpdateTypeId updateTypeId)
{
	CALL_TRACE("DiagCodeLogSubScreen::updateDiagnosticLogDisplay_"
				"(NovRamUpdateManager::UpdateTypeId updateTypeId)");

	Uint16 firstDisplayedEntryId = 0;		// Index of first displayed entry

	CodeLogEntry firstDisplayedEntry;		// First displayed entry 

	// Store the diagnostics that's currently the first displayed entry (the 
	// row that's displayed in the first row on the screen).
	firstDisplayedEntryId = pLog_->getFirstDisplayedEntry();

	if (!logEntries_[firstDisplayedEntryId].isClear())
	{ // $[TI2]
		firstDisplayedEntry = logEntries_[firstDisplayedEntryId];
	} // $[TI3]

	// Clear logEntries_ then copy the latest diagnostic log from NovRam
	clearLogEntries();	
	switch(logType_)
	{ 
		case DiagCodeLogSubScreen::DL_SYSTEM:				// $[TI4]

			// Get system's log from NovRam
			NovRamManager::GetSystemLogEntries(logEntries_);
			break;

		case DiagCodeLogSubScreen::DL_SYS_INFO:					// $[TI5]

			// Get system info log from NovRam
			NovRamManager::GetSystemInfoEntries(logEntries_);
			break;

		case DiagCodeLogSubScreen::DL_EST_SST:				// $[TI6]

			NovRamManager::GetEstSstLogEntries(logEntries_);
			break;

		default:
			SAFE_CLASS_ASSERTION_FAILURE();
			break;
	}

	// Find number of entries
	Uint16 numLogEntries = 0;
	for (numLogEntries = 0; numLogEntries < logEntries_.getNumElems(); numLogEntries++)
	{													// $[TI7]
		if (logEntries_[numLogEntries].isClear())
		{	// $[TI8]
			break;
		}	// $[TI9]		
	}// $[TI10]


	SAFE_CLASS_ASSERTION( (numLogEntries==0 && firstDisplayedEntryId==0) ||
					 (firstDisplayedEntryId >= 0 &&
								firstDisplayedEntryId < numLogEntries));

	// Find where in the new log, the previous "first displayed entry" is
	if (numLogEntries && firstDisplayedEntryId)
	{												// $[TI11]
		for (; firstDisplayedEntryId < numLogEntries; firstDisplayedEntryId++)
		{ // $[TI12]
			if (firstDisplayedEntry.getSequenceNumber() == 
				logEntries_[firstDisplayedEntryId].getSequenceNumber())
			{										// $[TI13]
				break;
			}										// $[TI14]
		} // $[TI15]

			
		// If the saved entry has moved down in the log so far that we
		// are unable to make it the first displayed entry in the log
		// then we must pick a new first displayed entry, one that
		// will cause the very last page in the log to be displayed.
		if (firstDisplayedEntryId > numLogEntries - NUM_ROWS_)
		{											// $[TI16]
			firstDisplayedEntryId = numLogEntries - NUM_ROWS_;
		}											// $[TI17]

		SAFE_CLASS_ASSERTION(firstDisplayedEntryId >= 0 &&
									firstDisplayedEntryId < numLogEntries);
	}												// $[TI18]

	// Inform the scrollable log of the change in log entries.
	pLog_->setEntryInfo(firstDisplayedEntryId, numLogEntries);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: novRamUpdateEventHappened
//
//@ Interface-Description
// Called by NovRam manager, this function updates the display with
// the latest, real-time data in the log.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call updateDiagnosticLogDisplay_ to process it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
DiagCodeLogSubScreen::novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId updateTypeId)
{

	if (isVisible())
	{ 		// $[TI1]
		// If this subscreen is the currently active subscreen then we should
		// update our display.
		if ((updateTypeId == NovRamUpdateManager::SYS_INFO_LOG_UPDATE &&
			 logType_ == DiagCodeLogSubScreen::DL_SYS_INFO)					 ||
			(updateTypeId == NovRamUpdateManager::SYSTEM_LOG_UPDATE &&
			 logType_ == DiagCodeLogSubScreen::DL_SYSTEM)				 ||
			((updateTypeId == NovRamUpdateManager::EST_LOG_UPDATE ||
			  updateTypeId == NovRamUpdateManager::SST_LOG_UPDATE) &&
			 logType_ == DiagCodeLogSubScreen::DL_EST_SST))
		{ 		// $[TI1.1]
			updateDiagnosticLogDisplay_(updateTypeId);
		}		// $[TI1.2]
	}
	// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fillMessageBuffer_ [private] 
//
//@ Interface-Description
//	This method fetches and store data in message then send it
//---------------------------------------------------------------------
//@ Implementation-Description
//	Make sure that the request is for one of the 3 valid log types.
//	The message buffer is formatted according to the request protocol.
// 	The data in the Diagnostic Log then fills the buffer.
//	
//  $[08054] The entries from each diagnostic log shall be returned to
//  the PC upon request through the serial interface.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
DiagCodeLogSubScreen::fillMessageBuffer_(LaptopEventId::LTEventId eventId)
{
	CALL_TRACE("DiagCodeLogSubScreen::fillMessageBuffer_("
					"LaptopEventId::LTEventId eventId)");

	CLASS_ASSERTION( (eventId == LaptopEventId::UPLOAD_SYSTEM_LOG) ||
					 (eventId == LaptopEventId::UPLOAD_SYS_INFO_LOG) ||
					 (eventId == LaptopEventId::UPLOAD_EST_SST_LOG) );

	Int32 length = SMALL_DATA_LENGTH_;	// Shortest length of message
	Uint16 numColumns;
	wchar_t tmpBuf[256];
	StringId	string1, string2, string3, string4;	
	string3 = string4 = NULL_STRING_ID;


	// Send the message for START_UPLOAD_COMMAND to the SerialOutput queue. 
	LaptopMessageHandler::ClearBuffer();
	LaptopMessageHandler::SetMessageNo();
	LaptopMessageHandler::SetLogCommand(LaptopEventId::START_UPLOAD_COMMAND);

	// Ask GuiApp to post the message to the Serial I/O queue.
	LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
					LaptopMessageHandler::GetBuffer(),
					LaptopMessageHandler::GetBufferIndex());

	// Send data message 
	// Clear logEntries_ then copy the latest diagnostic log from NovRam
	clearLogEntries();	
	switch (eventId)
	{
		// Get the logType_ and the diagnostic log from NovRam
		case LaptopEventId::UPLOAD_SYSTEM_LOG:		// $[TI1]
			logType_ = DiagCodeLogSubScreen::DL_SYSTEM;
			NovRamManager::GetSystemLogEntries(logEntries_);
			numColumns = NUM_COLUMNS5_;
			break;

		case LaptopEventId::UPLOAD_SYS_INFO_LOG:// $[TI2]
			logType_ = DiagCodeLogSubScreen::DL_SYS_INFO;
			NovRamManager::GetSystemInfoEntries(logEntries_);
			numColumns = NUM_COLUMNS4_;
			break;

		case LaptopEventId::UPLOAD_EST_SST_LOG:		// $[TI3]
			logType_ = DiagCodeLogSubScreen::DL_EST_SST;
			NovRamManager::GetEstSstLogEntries(logEntries_);
			numColumns = NUM_COLUMNS4_;
			break;

		default:									// $[TI4]
			SAFE_CLASS_ASSERTION_FAILURE();
			break;
	} // End switch

	// Find log size
	Int32 logSize;
	for (logSize = 0; logSize < logEntries_.getNumElems(); logSize++)
	{														// $[TI5]
		if (logEntries_[logSize].isClear())					// $[TI6]	
		{
			break;
		}													// $[TI7]
	}														// $[TI8]


#ifdef FORNOW
	cout << "fillMessageBuffer, logSize is " << logSize << endl;
#endif

	Boolean boolVal = FALSE;
	enableDateLocalization_ = FALSE;
	if (logSize > 0)
	{ // $[TI10]
		// While not end log, fetch for data
		for (Uint16 i=0; i<logSize; i++)
		{ // $[TI11]

			// Strobing TaskMonitor to keep alive
			TaskMonitor::Report();

			// Get data from each row
			length = 0;
			tmpBuf[0] = L'\0';
			for (Uint16 j=0; j<numColumns; j++)
			{ // $[TI12]
				// Get data to be displayed
				getLogEntryColumn(i, j, boolVal, string1, string2, string3, string4);
				// Pack data into tmpBuf
				if (string1 != NULL_STRING_ID)
				{ // $[TI13]
					wcscat(tmpBuf, (wchar_t *) string1);
					wcscat(tmpBuf, L"	");
					length += (wcslen(string1) + 1);
#ifdef FORNOW
printf("line %d tmpBuf: %s\n", __LINE__, tmpBuf);
#endif
				}	// $[TI14]
				if (string2 != NULL_STRING_ID)
				{ // $[TI15]
					wcscat(tmpBuf, (wchar_t *)string2);
					wcscat(tmpBuf, L"	");
					length += (wcslen(string2) + 1);
#ifdef FORNOW
printf("line %d tmpBuf: %s\n", __LINE__, tmpBuf);
#endif
				}
				 // $[TI16]
			} // j<numColumns					// $[TI17]
#ifdef FORNOW
printf("line %d tmpBuf: %s\n", __LINE__, tmpBuf);
#endif

			// Send the message for DATA_UPLOAD_COMMAND to the SerialOutput queue. 
#ifdef FORNOW
printf("   tmpBuf = %s\n", tmpBuf);
#endif
			LaptopMessageHandler::ClearBuffer();
			LaptopMessageHandler::SetMessageNo();

			// TODO E600 Unicode Text 
			// This is an external communication interface class and not sure that other side is ready to .
			// accept wide char hence converting to char
			char cTempBuff[256]; 
			StringConverter::ToString( cTempBuff, 256, tmpBuf );
			LaptopMessageHandler::SetLogCommand(LaptopEventId::DATA_UPLOAD_COMMAND, cTempBuff);

#ifdef SIGMA_UNIT_TEST
printf("		Log Data = %s\n", &(LaptopMessageHandler::GetBuffer()[LaptopEventId::OUTGOING_LOG_DATA_IDX])); 				
#endif
			// Ask GuiApp to post the message to the Serial I/O queue.
			LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
							LaptopMessageHandler::GetBuffer(),
							LaptopMessageHandler::GetBufferIndex());
		} // i<logSize							// $[TI18]
	} // logSize > 0							// $[TI19]

	enableDateLocalization_ = TRUE;

	// Send the message for END_UPLOAD_COMMAND to the SerialOutput queue. 
	LaptopMessageHandler::ClearBuffer();
	LaptopMessageHandler::SetMessageNo();
	LaptopMessageHandler::SetLogCommand(LaptopEventId::END_UPLOAD_COMMAND);

	// Ask GuiApp to post the message to the Serial I/O queue.
	LaptopMessageHandler::SerialOutputEventHappened(
					SerialInterface::SERIAL_OUTPUT_AVAILABLE,
					LaptopMessageHandler::GetBuffer(),
					LaptopMessageHandler::GetBufferIndex());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: laptopRequestHappened
//
//@ Interface-Description
// This is a virtual method inherited from being a LaptopEventTarget.  It is
// called when the GuiApp task queue detected an request/response from the
// Serial I/O interface.   Each registered target shall process this
// request/response and an appropriated action shall be conducted.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply call fillMessageBuffer_ with the eventId to setup a string which
// contains the log data before sending it across the Serial Line.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DiagCodeLogSubScreen::laptopRequestHappened(LaptopEventId::LTEventId eventId,
											Uint8 *pMsgData)
{
	CALL_TRACE("DiagCodeLogSubScreen::laptopRequestHappened("
					"LaptopEventId::LTEventId eventId, Uint8 *pMsgData)");

	CLASS_ASSERTION(GuiApp::IsSerialCommunicationUp());

	// Set serial system info flag
#ifdef FORNOW
printf("In DiagCodeLogSubScreen::laptopRequestHappened(), line %d\n", __LINE__);	
#endif

	// Strobing TaskMonitor to keep alive
	TaskMonitor::Report();

	// Fetch, pack, then put DATA 
	fillMessageBuffer_(eventId);
									// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNovRamUpdateId
//
//@ Interface-Description
//  Called when users want to convert Diagnostic Log's log type to 
//  NovRam's UpdateTypeId.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply returns the corresponding NovRam's UpdateTypeId.
//	There are three types of DiagnosticLog types:  System DiagnosticLog, 
//  Communication DiagnosticLog and EST/SST Diagnostic Log.  Each maps
//	into a NovRam's UpdateTypeId, except for EST/SST which arbitrarily 
//	maps into EST_LOG_UPDATE (SST_LOG_UPDATE could as well serves the purpose).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NovRamUpdateManager::UpdateTypeId
DiagCodeLogSubScreen::getNovRamUpdateId(int logId)

{
	CALL_TRACE("DiagCodeLogSubScreen::getNovRamUpdateId(int logId)");

	NovRamUpdateManager::UpdateTypeId novRamUpdateId;

	switch (logId)
	{
		case DiagCodeLogSubScreen::DL_SYSTEM:
												//	$[TI1]
			novRamUpdateId = NovRamUpdateManager::SYSTEM_LOG_UPDATE;
			break;

		case DiagCodeLogSubScreen::DL_SYS_INFO:
												//	$[TI2]
			novRamUpdateId = NovRamUpdateManager::SYS_INFO_LOG_UPDATE;
			break;

		case DiagCodeLogSubScreen::DL_EST_SST:
												//	$[TI3]
			novRamUpdateId = NovRamUpdateManager::EST_LOG_UPDATE;
			break;

		default:
			SAFE_CLASS_ASSERTION(FALSE);
			break;
	}
	return novRamUpdateId;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiagCodeLogSubScreen::SoftFault(const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, DIAGCODELOGSUBSCREEN,
                            lineNumber, pFileName, pPredicate);
}
