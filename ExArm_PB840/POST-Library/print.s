        IFDEF SIGMA_DEBUG      ;{
; *************************************************************************
;                        PB Test Monitor Utilitys Module
;
;
;  This file contains utility modules for PBMon - the Puritan-Bennett
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
; *************************************************************************
               SECTION  code,,C

               XDEF     _SendString__FiPc
               XDEF     _GetChar__Fi
               XDEF     _SendChar__Fic
               XDEF     _CharIsReady__Fi
               XDEF     _SendIsReady__Fi
               XDEF     _TerminalIsPresent__Fi
               XDEF     _InitializeDiagnosticPort__Fi
               XDEF     _PrintInitInterrupt

               XREF     _add_carriage_return


               INCLUDE  kernel.inc


GUI_CONTROL	EQU	$FFBE8000


;****************************************************************************
;
;  9600Baud -  $16 in timer
;
;
;  These are macros for accessing the Zilog Z85230. 
;  The Z85230 requires a two step read/write process.
;     1. write to a register pointer to set the register to be read/written
;     2. next read/write will access the specified register
;
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;!!!!!! WARNING: After step 2. above - the register pointer is           !!!!
;!!!!!!          automatically reset to point to the control registers.  !!!!
;!!!!!!          If an interrupt occurs between step 1. and 2. above and !!!!
;!!!!!!          the interrupt routine accesses the Z85230 - the         !!!!
;!!!!!!          results will be undefined.                              !!!!
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;
;  Note: the src and dst values in the macro are the register numbers
;
;  Examples:
;
;     Z85WRITEA $22,11 ;write $22 to Z85230 Register #11      (Clock Control)
;     Z85READB    8,d0 ;read from Z85230 Register #8 into d0  (Receive Data)
;
;

;--------------------- Macros  ------------------------------
Z85WRITE          MACRO  value,dst
                  move.b  #dst,(a1)
                  move.b value,(a1)
                  ENDM

Z85READ           MACRO   src,dst
                  move.b  #src,(a1)
                  move.b  (a1),dst
                  ENDM

DELAY             MACRO count
                  LOCAL loop
                  move.w   #count,d0
loop:             nop
                  dbra  d0,loop
                  ENDM


;----------------------------------------------------------------------------
; void SendString(int port, char *string);
TxEMPTY           EQU         2
_SendString__FiPc:
                  link        A6,#0
                  move.l      #Z85230_COMMAND_A,a1
                  move.l      (12,a6),a0        ;get the string pointer
                  move.l      (8,A6),d0         ;(port == 0) => channel A
                  beq         send
                  subq.l      #1,d0             ;(port == 1) => channel B
                  bne         send_exit         ; else ignore
                  move.l      #Z85230_COMMAND_B,a1

send:             move.b      (a0)+,d0          ; at the end of the string?
                  beq         send_exit         ; else ignore

tx_wait:          clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_wait
                  move.b      d0,(2,a1)         ; send the character

                  cmp.b       #$a,d0             ; newline?
                  bne         send              ; no, continue
                  tst.b      _add_carriage_return  ;add cr to new lines?
                  beq         send              ; no
                  move.b      #$d,d0             ; yes, send it
                  bra         tx_wait           ; 
                  bra         send

send_exit:        unlk  A6
                  rts



;----------------------------------------------------------------------------
; void SendChar(int port, char ch);
_SendChar__Fic:
                  link        A6,#0
                  move.l      #Z85230_COMMAND_A,a1
                  move.l      (12,a6),d1        ;get the character
                  move.l      (8,A6),d0         ;(port == 0) => channel A
                  beq         tx_char_wait
                  subq.l      #1,d0             ;(port == 1) => channel B
                  bne         char_send_exit    ; else ignore
                  move.l      #Z85230_COMMAND_B,a1

tx_char_wait:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_char_wait
                  move.b      d1,(2,a1)         ; send the character
                  cmp.b       #$a,d1             ; newline?
                  bne         char_send_exit    ; no, return
                  tst.b      _add_carriage_return  ;add cr to new lines?
                  beq         char_send_exit    ; no
                  move.b      #$d,d1             ; yes, send it
                  bra         tx_char_wait      ; 
char_send_exit:   unlk  A6
                  rts

;----------------------------------------------------------------------------
; int SendIsReady(int port);
_SendIsReady__Fi:
                  link        A6,#0
                  move.l      #Z85230_COMMAND_A,a1
                  move.l      (8,A6),d0         ;(port == 0) => channel A
                  beq         send_ready_test
                  subq.l      #1,d0             ;(port == 1) => channel B
                  bne         send_not_ready    ; else ignore
                  move.l      #Z85230_COMMAND_B,a1

send_ready_test:  moveq.l     #1,d0
                  clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  bne         send_ready
send_not_ready:   clr.l       d0
send_ready:       unlk  A6
                  rts



;----------------------------------------------------------------------------
; int TerminalIsPresent(int port);
CTS           EQU         5
DCD           EQU         3
_TerminalIsPresent__Fi:
                  link        A6,#0
                  move.l      #Z85230_COMMAND_A,a1
                  move.l      (8,A6),d0         ;(port == 0) => channel A
                  beq         terminal_test
                  subq.l      #1,d0             ;(port == 1) => channel B
                  bne         not_present       ; else ignore
                  move.l      #Z85230_COMMAND_B,a1

terminal_test:    moveq.l     #1,d0
                  clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #CTS,(a1)         ; CTS = 1  => Terminal connected
                  bne         present
not_present:      clr.l       d0
present:          unlk  A6
                  rts



;----------------------------------------------------------------------------
; 
;  char  GetChar(int port)
;
RxREADY           EQU         0
_GetChar__Fi:
                  link        A6,#0
                  move.l      #Z85230_COMMAND_A,a1
                  moveq.l     #0,d0             ; clear the return value
                  move.l      (8,A6),d1         ; (port == 0) => channel A
                  beq         rx_wait
                  subq.l      #1,d1             ; (port == 1) => channel B
                  bne         get_exit          ; else ignore
                  move.l      #Z85230_COMMAND_B,a1

rx_wait:          clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #RxREADY,(a1)     ; character ready?
                  beq         rx_wait
                  move.b      (2,a1),d0         ; get the character

get_exit:         unlk  A6
                  rts


;----------------------------------------------------------------------------
; 
;  char  CharIsReady(int port)
;
_CharIsReady__Fi:
                  link        A6,#0
                  move.l      #Z85230_COMMAND_A,a1
                  moveq.l     #0,d0             ; clear the return value
                  move.l      (8,A6),d1         ; (port == 0) => channel A
                  beq         test
                  subq.l      #1,d1             ; (port == 1) => channel B
                  bne         test_exit          ; else ignore
                  move.l      #Z85230_COMMAND_B,a1

test:             clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #RxREADY,(a1)     ; character ready?
                  beq         test_exit
                  addq.l      #1,d0
test_exit:        unlk  A6
                  rts



;----------------------------------------------------------------------------
; 
;  void   InitializeDiagnosticPort(int port)
;
;  Note: For now initialize to fixed setup
;
;
_InitializeDiagnosticPort__Fi:
                  link        A6,#0
                  tst.l       (8,A6)         ;(port == 0) => channel A
                  beq         init_port_a
                  subq.l      #1,(8,A6)      ;(port == 1) => channel B
                  bne         init_exit      ; else ignore

                  move.l      #Z85230_COMMAND_B,a1
                  Z85WRITE    #%01000000,9   ; reset channel b

                  DELAY   16                 ; delay for reset recover
                  Z85WRITE    #%01010101,11  ; set all clocks to baud rate gen

                  Z85WRITE          #$00,13  ; Select Baud

;                 Z85WRITE          #$16,12  ;  9,600 baud rate = $0016
                  Z85WRITE          #$0a,12  ; 19,200 baud rate = $000a
;                 Z85WRITE          #$04,12  ; 38,400 baud rate = $0004
;                 Z85WRITE          #$00,12  ; 115k   baud rate = $0000


                  Z85WRITE    #%10000001,14  ; baud clock external and enabled
                  Z85WRITE    #%01000100,4   ; /16, 1 stop bit, odd parity disabled
                  Z85WRITE    #%11101010,5   ; Tx = 8 bits , enabled
                  Z85WRITE    #%11100001,3   ; Rx = 8 bits , enabled
                  bra init_exit


init_port_a:      
                  move.l      #Z85230_COMMAND_A,a1
                  Z85WRITE    #%10000000,9   ; reset channel a
init_port:      
                  DELAY   16                 ; delay for reset recover
                  Z85WRITE    #%01010101,11  ; set all clocks to baud rate gen

                  Z85WRITE          #$00,13  ; Select Baud

;                 Z85WRITE          #$16,12  ;  9,600 baud rate = $0016
                  Z85WRITE          #$0a,12  ; 19,200 baud rate = $000a
;                 Z85WRITE          #$04,12  ; 38,400 baud rate = $0004
;                 Z85WRITE          #$00,12  ; 115k   baud rate = $0000


                  Z85WRITE    #%10000001,14  ; baud clock external and enabled
                  Z85WRITE    #%01000100,4   ; /16, 1 stop bit, odd parity disabled
                  Z85WRITE    #%11101010,5   ; Tx = 8 bits , enabled
                  Z85WRITE    #%11100001,3   ; Rx = 8 bits , enabled
init_exit:        unlk  A6
                  rts

;-----------------------------------------------------------------------------                  
hextable:
                DC.B	$30	;0
                DC.B	$31	;1
                DC.B	$32	;2
                DC.B	$33	;3
                DC.B	$34	;4
                DC.B	$35	;5
                DC.B	$36	;6
                DC.B	$37	;7
                DC.B	$38	;8
                DC.B	$39	;9
                DC.B	$41	;A
                DC.B	$42	;B
                DC.B	$43	;C
                DC.B	$44	;D
                DC.B	$45	;E
                DC.B	$46	;F



;----------------------------------------------------------------------------
; 
;  Note: Initialize both serial ports to a fixed setup
;
;----------------------------------------------------------------------------
_PrintInitInterrupt:
init_p_b:      

                  move.l      #Z85230_COMMAND_B,a1
                  Z85WRITE    #%01000000,9   ; reset channel b

                  DELAY   16                 ; delay for reset recover
                  Z85WRITE    #%01010101,11  ; set all clocks to baud rate gen

                  Z85WRITE          #$00,13  ; Select Baud

;                 Z85WRITE          #$16,12  ;  9,600 baud rate = $0016
                  Z85WRITE          #$0a,12  ; 19,200 baud rate = $000a
;                 Z85WRITE          #$04,12  ; 38,400 baud rate = $0004
;                 Z85WRITE          #$00,12  ; 115k   baud rate = $0000

                  Z85WRITE    #%10000001,14  ; baud clock external and enabled
                  Z85WRITE    #%01000100,4   ; /16, 1 stop bit, odd par disabled
                  Z85WRITE    #%11101010,5   ; Tx = 8 bits , enabled
                  Z85WRITE    #%11100001,3   ; Rx = 8 bits , enabled

init_p_a:      
                  move.l      #Z85230_COMMAND_A,a1
                  Z85WRITE    #%10000000,9   ; reset channel a

                  DELAY   16                 ; delay for reset recover
                  Z85WRITE    #%01010101,11  ; set all clocks to baud rate gen

                  Z85WRITE          #$00,13  ; Select Baud

;                 Z85WRITE          #$16,12  ;  9,600 baud rate = $0016
                  Z85WRITE          #$0a,12  ; 19,200 baud rate = $000a
;                 Z85WRITE          #$04,12  ; 38,400 baud rate = $0004
;                 Z85WRITE          #$00,12  ; 115k   baud rate = $0000

                  Z85WRITE    #%10000001,14  ; baud clock external and enabled
                  Z85WRITE    #%01000100,4   ; /16, 1 stop bit, odd par disabled
                  Z85WRITE    #%11101010,5   ; Tx = 8 bits , enabled
                  Z85WRITE    #%11100001,3   ; Rx = 8 bits , enabled

;----------------------------------------------------------------------------
no_term:
                  move.l      #Z85230_COMMAND_A,a1
                  clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #CTS,(a1)         ; CTS = 1  => Terminal connected
                  bne         term_found

                  move.l      #Z85230_COMMAND_B,a1
                  clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #CTS,(a1)         ; CTS = 1  => Terminal connected
                  rte                           ; NO TERMINAL - EXIT

term_found:
                  move.l      #$0000,d6   ; default to no low-level tests
                  clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #DCD,(a1)         ; DCD = 1  => no low-level tests
                  bne         no_lo
                  move.l      #$0001,d6   ; do low-level tests
no_lo:
;----------------------------------------------------------------------------

                move.l      #hextable,a2

;----------------------------------Space

                  move.b      #$20,d1		; space

tx_ch_wait6b:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait6b
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 3 STATUS

                  move.b      (sp),d1		;get the data
                  ror         #4,d1		;get upper data nibble
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait0a:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait0a
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 2 Status

                  move.b      (sp),d1		;get the data
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait0:      clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait0
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 1 Status

                  move.b      (1,sp),d1		;get the data
                  ror         #4,d1		;get upper data nibble
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait1a:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait1a
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 0 Status

                  move.b      (1,sp),d1		;get the data  
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait1:      clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait1
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Space

                  move.b      #$20,d1		; space

tx_ch_wait6a:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait6a
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 7 PC

                  move.b      (2,sp),d1		;get the data
                  ror         #4,d1		;get upper data nibble
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait2a:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait2a
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 6 PC

                  move.b      (2,sp),d1		;get the data  
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait2:      clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait2
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 5 PC

                  move.b      (3,sp),d1		;get the data
                  ror         #4,d1		;get upper data nibble
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait3a:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait3a
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 4 PC

                  move.b      (3,sp),d1		;get the data  
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait3:      clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait3
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 3 PC

                  move.b      (4,sp),d1		;get the data  
                  ror         #4,d1		;get upper data nibble
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait4a:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait4a
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 2 PC

                  move.b      (4,sp),d1		;get the data  
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait4:      clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait4
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 1 PC

                  move.b      (5,sp),d1		;get the data  
                  ror         #4,d1		;get upper data nibble
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait5a:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait5a
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 0 PC

                  move.b      (5,sp),d1		;get the data  
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait5:      clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait5
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Space

                  move.b      #$20,d1		; space

tx_ch_wait6c:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait6c
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 3 Format

                  move.b      (6,sp),d1		;get the data
                  ror         #4,d1		;get upper data nibble
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait0af:    clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait0af
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Space

                  move.b      #$20,d1		; space

tx_ch_wait6cf:    clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait6cf
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 2 Vector

                  move.b      (6,sp),d1		;get the data
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait0v:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait0v
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 1 Vector

                  move.b      (7,sp),d1		;get the data
                  ror         #4,d1		;get upper data nibble
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait1av:    clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait1av
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Nibble 0 Vector

                  move.b      (7,sp),d1		;get the data  
                  and.l       #$f,d1		;mask the data nibble
                  move.b      (0,a2,d1),d1	;get character from table

tx_ch_wait1v:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait1v
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Linefeed

                  move.b      #$a,d1		; linefeed

tx_ch_wait6:      clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait6
                  move.b      d1,(2,a1)         ; send the character

;----------------------------------Carriage Return

                  move.b      #$d,d1		; cr
tx_ch_wait7:      clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait7
                  move.b      d1,(2,a1)         ; send the character


;----------------------------------NULL pad for time delay

                  move.l      #3840,d2		; Delay one second
s_delay:
                  move.b      #$0,d1		; NULL
tx_ch_wait7a:     clr.b       (a1)              ; reset reg ptr and get reg 0
                  btst.b      #TxEMPTY,(a1)     ; ready to send?
                  beq         tx_ch_wait7a
                  move.b      d1,(2,a1)         ; send the character

                  subq.l      #1,d2             ; decrement loop count
                  bne         s_delay		; and loop `till time's up

;----------------------------------
                  rte              


       ENDC        ;} IFDEF SIGMA_DEBUG

                END
