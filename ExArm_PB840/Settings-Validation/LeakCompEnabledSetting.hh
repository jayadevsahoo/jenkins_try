
#ifndef LeakCompEnabledSetting_HH
#define LeakCompEnabledSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2008, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  LeakCompEnabledSetting - Leak Compensation Enabled Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/LeakCompEnabledSetting.hhv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: rhj    Date:  05-Aug-2008    SCR Number: 6435
//  Project: LEAK
//  Description:
//		Initial version.
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
//@ End-Usage


class LeakCompEnabledSetting : public BatchDiscreteSetting
{
public:
	LeakCompEnabledSetting(void);
	virtual ~LeakCompEnabledSetting(void);

	virtual Applicability::Id  getApplicability(
											   const Notification::ChangeQualifier qualifierId
											   ) const;

	virtual SettingValue  getNewPatientValue(void) const;
	virtual void  acceptTransition(const SettingId::SettingIdType settingId,
								   const DiscreteValue            newValue,
								   const DiscreteValue            currValue);

	static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32 lineNumber,
						   const char*  pFileName  = NULL, 
						   const char*  pPredicate = NULL);
protected:
	virtual Boolean calcDependentValues_(const Boolean isValueIncreasing);

private:
	LeakCompEnabledSetting(const LeakCompEnabledSetting&);	// not implemented...
	void  operator=(const LeakCompEnabledSetting&);		// not implemented...

};


#endif // LeakCompEnabledSetting_HH 
