#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  DaySetting - Day Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, sequential setting that contains the day of the
//  month.  This class inherits from 'BatchSequentialSetting' and
//  provides the specific information needed for representation of the day
//  of the month.  This information includes the interval and range of this
//  setting's values, and this batch setting's new-patient value (see
//  'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains a sequential range class ('SequentialRange')
//  to manage the resolution and range of this setting.  This class has NO
//  dependent settings, but it does have dynamic bounds (see
//  'updateConstraints_()').
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DaySetting.ccv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Corrected incomplete comment.
//
//  Revision: 006   By: sah  Date:  18-Jun-1999    DR Number:  5440
//  Project:  ATC
//  Description:
//      Eliminated 'isChanged()' method, and added 'resetForcedChangeFlag()',
//      to do nothing.
//
//  Revision: 005   By: sah  Date:  15-Jun-1999    DR Number:  5429
//  Project:  ATC
//  Description:
//	Now overridding 'isChanged()' to ALWAYS return 'TRUE', thereby
//      forcing highlight mode.
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.21.1.0) into Rev "BiLevel" (1.21.2.0)
//	Changed this sequential setting to allow its values to be cycled
//	through, rather than bounded.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "DaySetting.hh"
#include "MonthValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  DaySetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  No bound messages will be generated for this setting.
//
//  $[02102] -- The setting's range ...
//  $[02104] -- The setting's resolution ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DaySetting::DaySetting(void)
	 : BatchSequentialSetting(SettingId::DAY,
				  Setting::NULL_DEPENDENT_ARRAY_,
				  sequentialRange_, TRUE),
		 sequentialRange_(31,		// maxValue...
				  NULL_SETTING_BOUND_ID,	// maxConstraintId...
				  1,		// resolution...
				  NULL_SETTING_BOUND_ID,	// minConstraintId...
				  1)		// minValue...
{
  CALL_TRACE("DaySetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~DaySetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DaySetting::~DaySetting(void)
{
  CALL_TRACE("~DaySetting(void)");
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  resetForcedChangeFlag()  [virtual]
//
//@ Interface-Description
//  This method RESETS (to 'FALSE') this setting's internal flag that
//  indicates whether a forced change is being initiated (e.g., New-Patient
//  Setup or Transition Rules).
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is overridden to ensure that this setting's change-state is NOT reset.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DaySetting::resetForcedChangeFlag(void)
{
  CALL_TRACE("resetForcedChangeFlag()");

  // do nothing...
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Day is ALWAYS changeable.
//
//  $[01320] -- date/time settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
DaySetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this batch setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
DaySetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  const SequentialValue  NEW_PATIENT_VALUE = 1;

  return(NEW_PATIENT_VALUE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  acceptPrimaryChange(primaryId)  [virtual]
//
//@ Interface-Description
//  This setting's primary setting -- indicated by 'primaryId' -- has changed
//  therefore update this setting's value, correspondingly.  The day value
//  is "clipped" if it is not a legal day for the current month and year.
//  No bound violation is ever reported to the primary setting class, this
//  setting takes care of keeping its value valid with respect to month and
//  year.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this setting does not generate a bound message upon a bound
//  violation, 'NULL' is ALWAYS returned.
//
//  $[02001] -- responses to dependent setting bound violations...
//  $[02002] -- new dependent settings based on proposed primary settings...
//  $[02102] -- upper bound...
//---------------------------------------------------------------------
//@ PreCondition
//  (primaryId == SettingId::MONTH  ||  primaryId == SettingId::YEAR)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
DaySetting::acceptPrimaryChange(const SettingId::SettingIdType primaryId)
{
  CALL_TRACE("acceptPrimaryChange(primaryId)");
  SAFE_CLASS_PRE_CONDITION((primaryId == SettingId::MONTH  ||
			    primaryId == SettingId::YEAR));

  const SequentialValue  MAX_DAY_VALUE  = getMaxDayValue_();
  const SequentialValue  ADJUSTED_VALUE = getAdjustedValue();

  if (ADJUSTED_VALUE > MAX_DAY_VALUE)
  {   // $[TI1]
    // the current "adjusted" value is out-of-bounds, therefore "clip" its
    // value to the maximum allowable value...
    setAdjustedValue(MAX_DAY_VALUE);
  }   // $[TI2] -- the current "adjusted" value is NOT out-of-bounds...

  return(NULL);
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The "accepted" value of this setting is NEVER stored in the Accepted
//  Context, therefore always return 'TRUE'.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
DaySetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  return(TRUE);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DaySetting::SoftFault(const SoftFaultID  softFaultID,
		      const Uint32       lineNumber,
		      const char*        pFileName,
		      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
                          DAY_SETTING, lineNumber, pFileName,
                          pPredicate);
} 


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DaySetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  const SequentialValue  MAX_ALLOWABLE_DAY = getMaxDayValue_();

  // update this setting's maximum constraint...
  sequentialRange_.updateMaxConstraint(NULL_SETTING_BOUND_ID, MAX_ALLOWABLE_DAY);
}   // $[TI1]


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getMaxDayValue_()  [const]
//
//@ Interface-Description
//  Return the maximum day value based on the current month and year.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02102] -- upper bound...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SequentialValue
DaySetting::getMaxDayValue_(void) const
{
  CALL_TRACE("getMaxDayValue_()");

  SequentialValue  maxDayValue = 0;

  const Setting*  pMonth = SettingsMgr::GetSettingPtr(SettingId::MONTH);

  switch (DiscreteValue(pMonth->getAdjustedValue()))
  {
  case MonthValue::FEB :	// $[TI1]
    {
      const Setting*  pYear = SettingsMgr::GetSettingPtr(SettingId::YEAR);

      if ((SequentialValue(pYear->getAdjustedValue()) % 4) == 0)
      {   // $[TI1.1] -- leap year...
        maxDayValue = 29;
      }
      else
      {   // $[TI1.2] -- NOT leap year...
        maxDayValue = 28;
      }
    }
    break;

  case MonthValue::APR :
  case MonthValue::JUN :
  case MonthValue::SEP :
  case MonthValue::NOV :	// $[TI2]
    // 30-day months...
    maxDayValue = 30;
    break;

  case MonthValue::JAN :
  case MonthValue::MAR :
  case MonthValue::MAY :
  case MonthValue::JUL :
  case MonthValue::AUG :
  case MonthValue::OCT :
  case MonthValue::DEC :	// $[TI3]
    // 31-day months...
    maxDayValue = 31;
    break;

  default :
    // illegal month value...
    AUX_CLASS_ASSERTION_FAILURE(DiscreteValue(pMonth->getAdjustedValue()));
    break;
  };

  return(maxDayValue);
}
