#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OcclusionTrigger - Monitors the system for the specific 
//  conditions that cause severe occlusion or exhaust port occlusion to
//  be detected.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers a change in mode based on the detection of 
//    either severe occlusion or exhaust port occlusion.  The trigger 
//    can be enabled or disabled, depending upon the current mode of
//    breathing and the applicability of the trigger.  When either 
//    condition calculated by this trigger is true, the trigger is 
//    considered to have "fired".  Any Scheduler that needs to use this
//    trigger must have a pointer to it on the list within the scheduler.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements the algorithm for detecting a severe 
//    occlusion or an exhaust port occlusion, and triggers the transition 
//    to occlusion status cycling.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of whether
//    this trigger is enabled or not.  If the trigger is not enabled, it will
//    always return state of false.  The trigger contains constant values for
//    determining the pressure drop thresholds that are dependent upon the
//    circuit type set by the operator.  When either of the occlusion
//    conditions have been detected, the trigger fires by notifying the
//    current active scheduler of its state.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OcclusionTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013  By: sah     Date:  22-Sep-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  added neonatal-specific circuit factors
//
//  Revision: 012  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 011  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 010  By:  sp    Date: 1-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 009  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1780
//       Project:  Sigma (R8027)
//       Description:
//			Changed rExhDryFlowSensor.getValue() to rExhFlowSensor.getDryValue().
//
//  Revision: 007  By:  sp    Date:  31-Oct-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 006  By:  sp    Date:  19-Sept-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Remove unit test methods. Add unit test items.
//
//  Revision: 005  By:  sp    Date:  3-Sept-1996    DR Number: DCS 1293
//       Project:  Sigma (R8027)
//       Description:
//             update per controller spec rev 5.0.
//
//  Revision: 004  By:  sp    Date:  29-July-1996    DR Number:DCS 1166
//       Project:  Sigma (R8027)
//       Description:
//             Compare CYCLE_TIME_MS to 5 (int).
//
//  Revision: 003  By:  sp    Date:  6-Jun-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             modify const 2 to "2.0F".
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "OcclusionTrigger.hh"

//@ Usage-Classes

#include "MathUtilities.hh"

#include "MainSensorRefs.hh"

#include "Sensor.hh"
#include "FlowSensor.hh"
//E600 #include "ExhFlowSensor.hh"
#include "ExhFlowSensor.h"

#include "BreathRecord.hh"

#include "PhasedInContextHandle.hh"

#include "PatientCctTypeValue.hh"

#include "PressureXducerAutozero.hh"

#include "Peep.hh"

#include "BreathMiscRefs.hh"

#include "BreathPhase.hh"

#include "BD_IO_Devices.hh"
#include "ControllersRefs.hh"
#include "PeepController.hh"

//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

static const Real32  ADULT_CIRCUIT_FACTOR1 = 0.005F;
static const Real32  ADULT_CIRCUIT_FACTOR2 = 0.1491F;
static const Real32  ADULT_CIRCUIT_FACTOR3 = 0.0142F;

static const Real32  PED_CIRCUIT_FACTOR1 = 0.0082F;
static const Real32  PED_CIRCUIT_FACTOR2 = 0.1431F;
static const Real32  PED_CIRCUIT_FACTOR3 = 0.0136F;

static const Real32  NEO_CIRCUIT_FACTOR1 = 0.1339F;
static const Real32  NEO_CIRCUIT_FACTOR2 = 0.1424F;
static const Real32  NEO_CIRCUIT_FACTOR3 = -0.1165F;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OcclusionTrigger 
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The triggerId is set by the base class constructor.
//---------------------------------------------------------------------
//@ PreCondition
//   CYCLE_TIME_MS == 5
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
OcclusionTrigger::OcclusionTrigger(void) : ModeTrigger(Trigger::OCCLUSION)
{
  CALL_TRACE("OcclusionTrigger(void)");
  CLASS_PRE_CONDITION(CYCLE_TIME_MS == 5);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OcclusionTrigger 
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
OcclusionTrigger::~OcclusionTrigger(void)
{
  CALL_TRACE("~OcclusionTrigger(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable
//
//@ Interface-Description
//    This method takes no parameters and has no return value. In addition 
//    to enabling the trigger, this method is also responsible for initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The state data member isEnabled_ is set to TRUE, enabling this trigger.  
//    Circuit factors and counts are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
OcclusionTrigger::enable(void)
{
  CALL_TRACE("enable()");

  isEnabled_ = TRUE;

  const DiscreteValue  CIRCUIT_TYPE =
      PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

  switch (CIRCUIT_TYPE)
  {
	  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
	      // $[TI2]
    	  factor1_ = PED_CIRCUIT_FACTOR1;
    	  factor2_ = PED_CIRCUIT_FACTOR2;
	      factor3_ = PED_CIRCUIT_FACTOR3;
    	  break;
    	  
	  case PatientCctTypeValue::ADULT_CIRCUIT :
    	  // $[TI1]
	      factor1_ = ADULT_CIRCUIT_FACTOR1;
    	  factor2_ = ADULT_CIRCUIT_FACTOR2;
	      factor3_ = ADULT_CIRCUIT_FACTOR3;
    	  break;
    	  
	  case PatientCctTypeValue::NEONATAL_CIRCUIT :
    	  // $[TI3]
	      factor1_ = NEO_CIRCUIT_FACTOR1;
    	  factor2_ = NEO_CIRCUIT_FACTOR2;
	      factor3_ = NEO_CIRCUIT_FACTOR3;
    	  break;
    	  
	  default :
    	  AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
	      break;
  }

  count1_ = 0;
  count2_ = 0;
  count3_ = 0;
  exhaustPortCount_ = 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
OcclusionTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, OCCLUSIONTRIGGER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If a severe or exhaust port occlusion
//    has occured, the method returns true. Otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method invokes methods that check for severeOcclusion() and
//    exhaustPortOccluded().
//    If either of these methods return TRUE, then an occlusion has been
//    detected and this method returns true.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
OcclusionTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

  Boolean retVal = FALSE;

  if (severeOcclusion_() || exhaustPortOccluded_())
  {
    // $[TI1]
    retVal = TRUE;
  }
  // $[TI2]

  // FALSE: $[TI3]
  // TRUE: $[TI4]
  return (retVal);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: severeOcclusion_
//
//@ Interface-Description
//    This method takes no parameters.  It returns a Boolean indicating 
//    if severe occlusion criteria have been met for this cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The threshold for pressure is computed by taking the max of insp and exh
//    flow measured, and using the following formula: factor1_ * sqr(Qmax) +
//    factor2_ * Qmax + factor3_.
//    The following conditions must be checked :
//       (measured pressure > threshold) AND 
//       ((measured Pressure > 20 cmH2O for 50ms)  OR 
//        (measured Pressure > 10 cmH2O for 100ms) OR
//        (measured Pressure > 5 cmH2O for 200ms))
//    If the condition is true, then this method returns TRUE.  
//    Otherwise, this method returns FALSE.
// $[04126] $[04127] $[04128] $[04061] $[04064]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
static const Real32 THRESHOLD1 = 20.0F; //cmH2O
static const Real32 THRESHOLD2 = 10.0F; //cmH2O
static const Real32 MAX_DP = 100.0F; //cmH2O
static const Real32 MIN_DP = 5.0F; //cmH2O

static const Int32 COUNT1_MAX = 10; //50ms
static const Int32 COUNT2_MAX = 20; //100 ms
static const Int32 COUNT3_MAX = 40; //200 ms
Boolean
OcclusionTrigger::severeOcclusion_(void)
{
  CALL_TRACE("severeOcclusion_(void)");

  Boolean retVal = FALSE;
  Real32 maxFlow = MAX_VALUE(	((Sensor&)RO2FlowSensor).getValue() + 
				((Sensor&)RAirFlowSensor).getValue(),
				 RExhFlowSensor.getDryValue()
				);
  Real32 dPthreshold = maxFlow * (factor1_ * maxFlow + factor2_) + factor3_;

  //dPthreshold must be limited between MIN_DP and MAX_DP cmH2O
  if (dPthreshold > MAX_DP)
  {
    // $[TI1]
    dPthreshold = MAX_DP;
  }
  else if (dPthreshold < MIN_DP)
  {
    // $[TI2]
    dPthreshold = MIN_DP;
  }
  // $[TI3]


  Real32 inspPressure = ((Sensor&)RInspPressureSensor).getValue();
  Real32 dPmeasured = ( ( inspPressure - ((Sensor&)RExhPressureSensor).getValue() ) -
			( .7F + ABS_VALUE(inspPressure) * .062F ) );

  if (RPressureXducerAutozero.getIsAutozeroInProgress())
  {
    // $[TI4]
    count1_ = 0;
    count2_ = 0;
    count3_ = 0;
  }
  else
  {
    if (dPmeasured > dPthreshold) 
    {
      if (dPmeasured > THRESHOLD1)
      {
        //$[TI5] 
        count1_++;
      }
      else
      {
        //$[TI6] 
        count1_ = 0;
      }
      if  (dPmeasured > THRESHOLD2)
      {
        //$[TI7] 
        count2_++;
      }
      else
      {
        //$[TI8] 
        count2_ = 0;
      }

      //$[TI9] 
      // the counter for dPmeasured > MIN_DP
      count3_++;
    }
    else 
    {
      // $[TI11]
      count1_ = 0;
      count2_ = 0;
      count3_ = 0;
    }
  }

  if ((count1_ >= COUNT1_MAX) || (count2_ >= COUNT2_MAX) || (count3_ >= COUNT3_MAX))
  {
    //$[TI12]
    retVal = TRUE;
  }
  //$[TI13]

  //FALSE: $[TI14]
  //TRUE: $[TI15]
  return (retVal);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: exhaustPortOccluded_
//
//@ Interface-Description
//    This method takes no parameters.  If an exhaust port occlusion has
//    been detected, it returns true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    During the exhalation phase of a breath, the pressure drop in the exhalation
//    compartment shall be calculated as: 
//       dP = ADULT_CIRCUIT_FACTOR1 * sqr(exhFlow) + 
//            ADULT_CIRCUIT_FACTOR2 * exhFlow + ADULT_CIRCUIT_FACTOR3.
//    The threshold for exhalation pressure measured by the exhalation 
//    pressure sensor is:
//       threshold = dP + trajectoryValue (PEEP)
//    If the pressure measured by the expiratory pressure 
//    transducer > threshold for 100 ms, then this method returns true.
// $[04132] $[04130] $[04133] $[04061] $[04064]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
const Int32 MAX_COUNT = 20; // 100 ms
const Real32 MAX_PRESSURE = 100.0F; //100 cmH2O
const Int32 EXH_PORT_DISABLE = 200; //200 ms
Boolean
OcclusionTrigger::exhaustPortOccluded_(void)
{
  CALL_TRACE("exhaustPortOccluded_(void)");

  // $[04131]
  Real32 exhFlow = RExhFlowSensor.getDryValue();
  Real32 dP = exhFlow * (ADULT_CIRCUIT_FACTOR1 * exhFlow + ADULT_CIRCUIT_FACTOR2) +
              ADULT_CIRCUIT_FACTOR3;
  Real32 peep = RPeep.getActualPeep();
  Real32 exhPressure = ((Sensor&)RExhPressureSensor).getValue();

  Real32 pressureThreshold;
  if (dP < 1.0F)
  { 
    // $[TI9]
    pressureThreshold = 7.35F + peep + exhPressure * .03F;
  }
  else
  { 
    // $[TI10]
    pressureThreshold = dP + peep + exhPressure *.03F + 6.35F;
  }

  pressureThreshold = MIN_VALUE(MAX_PRESSURE, pressureThreshold);

  BreathPhaseType::PhaseType phase = BreathRecord::GetPhaseType();
  Boolean retVal = FALSE;
  if ( RPressureXducerAutozero.getIsAutozeroInProgress() || 
       (phase != BreathPhaseType::EXHALATION) )
  { 
    // $[TI1]
    exhaustPortCount_ = 0;
  }
  else 
  {
    // $[TI2]
    if ( exhPressure >  pressureThreshold)
    {
      // $[TI3]
      exhaustPortCount_++;
    }
    else
    {
      // $[TI4]
      exhaustPortCount_ = 0;
    }

    if ( (exhaustPortCount_ >= MAX_COUNT) && 
         ((BreathPhase::GetCurrentBreathPhase())->getElapsedTimeMs() >= EXH_PORT_DISABLE) && !RPeepController.isPeepChanged())
    {	
      // $[TI5]
      retVal = TRUE;
    }
    // $[TI6]
  }

  //FALSE: $[TI7]
  //TRUE: $[TI8]
  return (retVal);
}


