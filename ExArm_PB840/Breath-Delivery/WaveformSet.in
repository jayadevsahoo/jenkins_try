
#ifndef WaveformSet_IN
#define WaveformSet_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: WaveformSet - Set of waveform records
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/WaveformSet.inv   25.0.4.0   19 Nov 2013 14:00:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  26-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearAll
//
//@ Interface-Description
//	This method takes no parameters and has no return value.
//	This method is used to initialize WaveformSet object,
//	before it can be used.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set data members currentRecord_ and numRecords_ to 0.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
inline void
WaveformSet::clearAll(void)
{
  CALL_TRACE("clearAll(void)");

  // $[TI1]
  currentRecord_ = 0; 
  numRecords_ = 0; 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetCurrentIndex
//
//@ Interface-Description
//	This method takes no parameters and has no return value.
//	This method resets the current index before using method 
//	getNextWaveformRecord.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set data member currentRecord_ to 0.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
inline void
WaveformSet::resetCurrentIndex(void)
{
  CALL_TRACE("resetCurrentIndex(void)");

  // $[TI1]
  currentRecord_ = 0; 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isFull
//
//@ Interface-Description
//	This method takes no parameters and has no return value.
//	This method returns true if the waveform set is full else
//	return false.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If numRecords_ >= NUM_WAVEFORM_SET returns true else returns false. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
inline Boolean
WaveformSet::isFull(void)
{
  CALL_TRACE("isFull(void)");

  // TRUE: $[TI1]
  // FALSE: $[TI2]
  return (numRecords_ >= NUM_WAVEFORM_SET);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNextWaveformRecord
//
//@ Interface-Description
//	This method takes no parameters and returns a pointer to WaveformRecord.
//	This method traverses an array of WaveformRecord and returns a pointer to
//	an element of the array .  A NULL pointer is returned when the end of the
//	array has been reached.
//---------------------------------------------------------------------
//@ Implementation-Description
//	if currentRecord_ is less than NUM_WAVEFORM_SET and currentRecord_ <
//	numRecords_ then a pointer to WaveformRecord indexed by currentRecord_ is
//	returned, else returns NULL. Increments currentRecord_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
inline const WaveformRecord*
WaveformSet::getNextWaveformRecord(void)
{
    CALL_TRACE("getNextWaveformRecord(void)");

    WaveformRecord* rtnValue = NULL;

    if( (currentRecord_ < NUM_WAVEFORM_SET) && (currentRecord_ < numRecords_) )
    {
        // $[TI1]
	rtnValue = &waveformRecord_[currentRecord_++];
    }
    // $[TI2]
	
    // NOT NULL: $[TI3] 
    // NULL: $[TI4] 
    return (rtnValue);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: add
//
//@ Interface-Description
//	This method takes a constant reference to WaveformRecord 
//	and returns a Boolean. This method returns TRUE if a WaveformRecord
//	can be successfully added, else returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//	if numRecords_ is less than NUM_WAVEFORM_SET then the
//	argument waverec is added to the data member waveformRecord_,
//	and returns true. Otherwise returns false.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
inline Boolean
WaveformSet::add(const WaveformRecord& waverec)
{
    CALL_TRACE("add(const WaveformRecord& waverec)");

    Boolean rtnValue = FALSE;

    if (numRecords_ < NUM_WAVEFORM_SET)
    {
	// $[TI1]
	waveformRecord_[numRecords_++] = waverec;
	rtnValue = TRUE;
    }
    // $[TI2]
	
    // TRUE: $[TI3] 
    // FALSE: $[TI4] 
    return (rtnValue);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#endif // WaveformSet_IN 
