#include "stdafx.h"
#if	 defined(SIGMA_CLIENT)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Filename:  SystemSupport - System Support Utilities.
//---------------------------------------------------------------------
//@ Interface-Description
//     Sigma target system dependent support routines to write its
//     LED, to find out target board CPU type..
//---------------------------------------------------------------------
//@ Rationale
//    System dependent stuff in one module.
//---------------------------------------------------------------------
//@ Implementation-Description
//   ResetTheSystem goes into a infinite wait loop. If the download
//   system really needs to reset the system, this routine should
//   be modified in the future.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Download/vcssrc/SystemSupport.ccv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//
//@ Modification-Log
//  
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//   Revision: 002   By: Gary Cederquist  Date: 29-JUL-1997  DR Number: 2197
//   Project:  Sigma (R8027)
//   Description:
//	 Changed to use DIAG_LEG_REG definition provided in MemoryMap.hh.
//
//   Revision: 001   By: jhv   Date: 9-Feb-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================
#include "IoRegisterCons.hh"
#include "Download.hh"
#include "MemoryMap.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  External Functions...
//
//=====================================================================

#define WatchdogStrobe() { *(Uint8 *)0xFFBE9000 = 0;}    // Any write will strobe

#ifdef SIGMA_UNIT_TEST
#define DISABLE_INTERRUPT_STRING " nop"
extern xsFlag_t use_local_SystemSupport__BoardType_reverseboard;
#define BD_BOARD_KIND  (use_local_SystemSupport__BoardType_reverseboard ? (!(*IO_REGISTER_1 & IO_1_BD_BOARD)) : (*IO_REGISTER_1 & IO_1_BD_BOARD))
#else
#define DISABLE_INTERRUPT_STRING " move.w #$2700,sr"
#define BD_BOARD_KIND  (*IO_REGISTER_1 & IO_1_BD_BOARD)
#endif

#define DisableInterrupt() { asm(DISABLE_INTERRUPT_STRING);}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: BoardType (void)
//
//@ Interface-Description
//  >Von
//  Determines if the CPU type is Breath delivery or GUI.
//
//  OUTPUTS: BD_BOARD or GUI_BOARD  (as enumerators)
//
//  A specific bit of a hardware port is set to indicate the type
//  of CPU installed.  If it is grounded a GUI CPU is installed,
//  if it is high (VCC) a BD CPU is installed.
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
TargetTypes
BoardType (void)
{
	if ( BD_BOARD_KIND ) // $[TI1]
	{
		return (BD_BOARD);
   	}
	else // $[TI2]
	{
		return (GUI_BOARD);
	}
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: ResetTheSystem (Uint8 errCode)
//
//@ Interface-Description
//  >Von
//	Resets the system, hardware and software.
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Go on an infinite loop until the system is reset externally since
//    all the software mechanism to reset the system doesn't work.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
extern "C"
void
ResetTheSystem (Uint8 )
{
Boolean forever = TRUE;

   // $[TI1]
#if	defined(SIGMA_DEBUG)
   printf("Reset the system\n");
#endif //defined(SIGMA_DEBUG)

   while(forever)
   {
		Download::DelayTask(10);
#ifdef SIGMA_UNIT_TEST
		break;
#endif // SIGMA_UNIT_TEST
   } // no [TI] needed
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: LightDiagnosticLED (Uint8 errCode)
//
//@ Interface-Description
//  >Von
//	Displays a given error on the SIGMA diagnostic LED only if
//  the error is unrecoverable. An unrecoverable error code
//  gets displayed on the LED until the system gets reset.
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Disable all interrupts to avoid the system timer handler overriding
//    the LED.
//    Any error code less than 100 can be a socket library errno
//    and these error can be recovered.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

extern "C"
void
LightDiagnosticLED (Uint8 errCode)
{	
#if	defined(SIGMA_DEBUG)
	printf("LightDiagnosticLED error Code %d \n", errCode);
#endif //defined(SIGMA_DEBUG)
#if	defined(SIGMA_TARGET)
Boolean forever = TRUE;

	if ( errCode < 100 ) // $[TI1]
	{
		return;
	}
	else // $[TI2]
	{
		errCode -= 100;
	}
	// Disable all the interrupts except Watchdog.
	DisableInterrupt();
	*DIAG_LED_REG = ~errCode;

	while(forever) 
	{
		// strobe the watchdog since there is nobodyelse doing it, not
		// even a timer interrupt handler.
		WatchdogStrobe();
#ifdef SIGMA_UNIT_TEST
		break;
#endif // SIGMA_UNIT_TEST
	} // no [TI] needed
#endif //defined(SIGMA_TARGET)
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: EnableFlashWrite(void)
//
//@ Interface-Description
//  >Von
//  Put the entire flash memory in a writable mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//    It sets the flash memory writable bit in the IO control register.
//    Wait 10 millisecond since it takes up to 10 millisecond for the 
//    setting to become effective.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
void
EnableFlashWrite(void)
{
	if ( BoardType() == BD_BOARD ) // $[TI1]
	{
		*IO_REGISTER_1 = (Uint8) ( BD_IO_1_DEFAULT | IO_1_WR_FLASH_WRITE );
	}
	else // $[TI2]
	{
		*IO_REGISTER_1 = (Uint8) ( GUI_IO_1_DEFAULT | IO_1_WR_FLASH_WRITE );
	}
	Download::DelayTask((Uint)10);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: DisableFlashWrite(void)
//
//@ Interface-Description
//  >Von
//  Puts the flash memory in a read only mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//   It clears the flash memory writable bit in the IO control register.
//	 The IO_1_DEFAULT doesn't reset remote, audio alarm and lan.
//   Wait 10 millisecond since it takes up to 10 millisecond for the 
//   setting to become effective.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
void
DisableFlashWrite(void)
{
	if ( BoardType() == BD_BOARD ) // $[TI1]
    	{
		*IO_REGISTER_1 = (Uint8) ( BD_IO_1_DEFAULT ) ;
	}
	else // $[TI2]
    	{
		*IO_REGISTER_1 = (Uint8 ) ( GUI_IO_1_DEFAULT );
   	}
	Download::DelayTask((Uint) 10); 
}

#endif //defined(SIGMA_CLIENT)
