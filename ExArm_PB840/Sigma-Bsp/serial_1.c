/*
** serial.c
**
** Contents:
**
**    This file contains functions that implement the logio functions for
**    DEV_SERIAL.
**
**    The functions here use the serial device descriptors to call device 
**    specific functions to access the serial chip.
**
**    1. logio_serial_init..... initialize the serial device
**    2. logio_serial_read..... stream-based input from serial device
**    3. logio_serial_write.... stream-based output to serial device
**    4. logio_serial_control.. change characteristics of serial device
**    5. logio_serial_getmsg... packet-based input from serial device
**    6. logio_serial_putmsg... packet-based output to serial device
**    7. logio_serial_poll..... poll on pending control write operations
**
** History:
**
**    Date        Name              Description
**    ---------------------------------------------------------------------- 
**    06-29-92    Mike Milne        First Version, mo332qsm.c
**    09-30-92    Roger Cole        Reformatted file
**    09-30-92    Roger Cole        Added temporary logio_serial_packet_rx
**    09-30-92    Roger Cole        Added fops calls in functions
**    10-20-92    Roger Cole        Added logio_serial_packet_tx function
**    10-21-92    Roger Cole        Added logio_serial_prime_pump function
**    10-21-92    Roger Cole        Added code to solve problems in serial_read
**                                  when OLD is not defined
**    10-21-92    Roger Cole        Modified code in serial_poll to NOT issue
**                                  characters
**    10-21-92    Roger Cole        Cleaned up inefficient enable/disable code
**    10-21-92    Roger Cole        Added POLLED_AND_INTERRUPT
**                                  ifdefs and new code
**                                  for non-combined polled/interrupt interface
**    10-21-92    Roger Cole        serial.6
**    10-21-92    Roger Cole        Added buffer allocation to
**                                  init for every device
**    10-21-92    Roger Cole        Added code to read,write,poll,getmsg
**                                  and putmsg to not input/output
**                                  polled characters (used ifdefs)
**    10-21-92    Roger Cole        serial.7
**    10-22-92    Roger Cole        Added i/o poll functions to serial_control
**    10-22-92    Roger Cole        Cleaned up file a bit
**    10-22-92    Roger Cole        Got a good compile without
**                                  POLLED_AND_INTERRUPT defined
**                                  ready for test with application
**    10-22-92    Roger Cole        serial.8
**    10-22-92    Roger Cole        undefined POLLED_AND_INTERRUPT
**    10-22-92    Roger Cole        PROBLEM: tty data not being 
**                                  transmitted out (only 1st two characters)
**    10-26-92    Roger Cole        split POLLED_AND_INTERRUPT to TTY_ and _PKT
**    10-26-92    Roger Cole        undefined PKT_POLLED_AND_INTERRUPT
**    10-26-92    Roger Cole        serial.9
**    10-26-92    Roger Cole        serial.9 works -- packet channel is up,
**                                  using control functions
**    10-28-92    Roger Cole        finished the buffer allocation schemes
**    10-28-92    Roger Cole        undefined OLD_BUFFER_ALLOC
**    10-28-92    Roger Cole        serial.10
**    10-28-92    Roger Cole        attempting to isolate bugs
**                                  in new buffer allocation
**    10-28-92    Roger Cole        Added printf debug statements
**    10-28-92    Roger Cole        serial.11
**    10-28-92    Roger Cole        added code in serial_packet_rx
**                                  to correct logic problems
**    10-28-92    Roger Cole        increased number of max buffers to five
**    10-28-92    Roger Cole        the zi8530.c file allocates one
**                                  buffer in the init function
**    10-28-92    Roger Cole        tested to work properly...
**    10-28-92    Roger Cole        serial.12
**    10-28-92    Roger Cole        set the max buffer count to 2
**    10-28-92    Roger Cole        ....and it works
**    10-28-92    Roger Cole        removed some printf statements
**    10-28-92    Roger Cole        serial.13
**    12-15-92    Roger Cole        reduced interrupt disable time
**                                  in logio_serial_read
**    12-15-92    Roger Cole        serial.26
**    01-04-93    Mike/Donn         Using polled and interrupt 
**                                  for packet serial devices
**    01-27-93    Roger Cole        removed init, max global
**                                  declarations (write to ROM problem)
**    01-27-93    Roger Cole        removed unnecessary mo147 and 
**                                  mvme147 .h files
**    03-08-93    Roger Cole        moved pkt_get_serial_buf
**                                  after a buffer has been received 
**    01-26-95    Cindy Gin         aligned the tabs in this file;
**                                  was hard to read 
**
*/

#include <ansiprot.h>		/* ANSI prototypes         */
#include <compiler.h>		/* compiler-specific typedefs      */
#include "logio.h"		/* logio defines and typedefs      */
#include <vtrsint.h>		/* ethernet headers for get/putmsg   */
#include <serialpkt.h>		/* encaps/decaps support for serial pkt   */
#include <serial_1.h>           /* serial device types                  */

#include "vbsp.h"		/* VBSP return codes and typedefs   */
#include "logio_charbuf.h"	/* circular character buffer typedefs   */


/* FOR PACKET DRIVER */
#define TARGET_ONLY
/* define internal constants that should not be externally visible */
#define SYNC_SIZE		1
#define LENGTH_SIZE		2
#define MAX_PACKET_LENGTH	1600
#define IP_SIZE			4
#define PORT_SIZE		2
#define CRC_SIZE		2
#define SERIAL_HEADER_SIZE	(IP_SIZE + PORT_SIZE + LENGTH_SIZE + SYNC_SIZE)
#define SERIAL_ENCAPS_SIZE	(SERIAL_HEADER_SIZE + CRC_SIZE)

#define CRC_OK			0

#ifdef TARGET_ONLY

#define ALLIGN  4       /* must be power of 2 */
#define ROUNDUP(x)      (((unsigned int)(x) + ALLIGN - 1) & ~(ALLIGN - 1))
#define ROUNDDOWN(x)    ((unsigned int)(x) & ~(ALLIGN - 1))
#define ETHHDRSIZE      14      /* sizeof(struct ether_header) exactly */
#define UDPIPETHSZ      ROUNDUP(ETHHDRSIZE + sizeof(struct ip)+ \
				sizeof(struct udphdr))
#define VTHEADER_OFFSET	(UDPIPETHSZ - 2)	/* exclude fill */
#endif

#define TTY_POLLED_AND_INTERRUPT
/* #define PKT_POLLED_AND_INTERRUPT */
/* #define OLD_BUFFER_ALLOC */
/* #define DEBUG */
/* #define DEBUG1 */
/* #define DEBUG2 */
/* #include <stdio.h> */
#ifdef DEBUG
#include <stdio.h>
#endif
#ifdef DEBUG
char *d0 = (char *)0x3000;
char *d1 = (char *)0x3100;
char *d2 = (char *)0x3200;
char *d3 = (char *)0x3300;
char *d4 = (char *)0x3400;
char *d5 = (char *)0x3500;
char *d6 = (char *)0x3600;
char *d7 = (char *)0x3650;
char *d8 = (char *)0x3250;
char *getmsgbuf = (char *)0x4000;
char *putmsgbuf = (char *)0x5000;
int gmflg = 0;
int pmflg = 0;
#endif
#ifdef DEBUG1
char *putmsgbuf = (char *)0x5000;
char *getmsgbuf = (char *)0x4000;
char *putbuf = (char *)0x4500;
int pmflg1 = 0;
char *mybuf = "\0\0\0\0";
#endif
#ifdef DEBUG2
char *getmsgbuf = (char *)0x4000;
char *mybuf = "\0\0\0\0";
#endif


/* TEMP FIX only !! sould be in a header file somewhere */
#define NULL   0   /* TEMP Fix !! */
#define FALSE  0
#define TRUE   1

int logio_serial_packet_rx_isr(_ANSIPROT1(serial_dev_desc_t *));

/*
 ** definition of device id
 */

logio_status_t logio_serial_init(_ANSIPROT1(logio_device_id_t));

logio_status_t logio_serial_read(_ANSIPROT3(logio_device_id_t, 
					    char *, 
					    int *));

logio_status_t logio_serial_write(_ANSIPROT3(logio_device_id_t, 
					     char *,   
					     int *));

logio_status_t logio_serial_control(_ANSIPROT3(logio_device_id_t, 
					       logio_control_t,
					       void *));

logio_status_t logio_serial_getmsg(_ANSIPROT2(logio_device_id_t, 
					      logio_buff_t **));

logio_status_t logio_serial_putmsg(_ANSIPROT2(logio_device_id_t, 
					      logio_buff_t *));

logio_status_t logio_serial_poll(_ANSIPROT3(logio_device_id_t, 
					    logio_control_t, 
					    void *));

#if defined(PPC860)
vbsp_return_t
logio_serial_dev_pgetc(serial_dev_desc_t *, unsigned char *pCh);
vbsp_return_t
logio_serial_dev_pputc(serial_dev_desc_t *, unsigned char ch);
#else
#define logio_serial_dev_pgetc(dev, pCh) (*dev->fops.pgetc)(dev, pCh)
#define logio_serial_dev_pputc(dev, ch) (*dev->fops.pputc)(dev, ch)
#endif

/* the following two structures define the logio ID for serial */

const logio_fops_t logio_serial_fops = {
    logio_serial_init,
    logio_serial_read,
    logio_serial_write,
    logio_serial_control,
    logio_serial_getmsg,
    logio_serial_putmsg,
    logio_serial_poll
};

#ifdef __STDC__
logio_status_t oldpkt_get_serial_buf(serial_pkt_tst_info_t *bp)
#else
logio_status_t oldpkt_get_serial_buf(bp)
serial_pkt_tst_info_t *bp;
#endif
{
    logio_buff_t *tmp;
    
    if((tmp = logio_buff_get(1600, LOGIO_BUFF_TYPE_VT)) ==
       logio_buff_NULL){
	return(LOGIO_STATUS_RESOURCES);
    }
    /* should be etherhdrsize - decaps_preamble for xtdemon */
    tmp->data = &tmp->buff[UDPIPETHSZ - SERIAL_HEADER_SIZE];
    tmp->dlen = 0;
    bp->curr = tmp;
    bp->mode = get_preamble_field;
    /* sync + len size */
    bp->byte_cnt = 3;
    bp->next_ch = (unsigned char *)tmp->data;
    
    return(LOGIO_STATUS_OK);
}

#ifdef __STDC__
logio_status_t pkt_get_serial_buf(serial_dev_desc_t *dev)
#else
logio_status_t pkt_get_serial_buf(dev)
serial_dev_desc_t *dev;
#endif
{
    /* if the variable is prefixed with 'p', it is a pointer */
    /* the remaining portion of the pointer name is an acronym
     * for the full structure name */
    cpu_interrupt_t prev_sr;
    logio_buff_t *plio;
    serial_pkt_tst_info_t *pptit = &dev->buf.pkt.rx;
    serial_pkt_buff_t     *ppbt = &dev->buf.pkt;
    /* if the count is below */
    if ( ppbt->rx_logio_buff_count < ppbt->rx_max_logio_buff_count )
    {
	/* if the first buffer has not been allocated....*/
	if ( !pptit->curr )
	{
	    /* attempt to allocate the first buffer */
	    /* if we can not allocate a buffer return resources problem */
	    if((pptit->curr = logio_buff_get(1600, LOGIO_BUFF_TYPE_VT)) ==
	       logio_buff_NULL)
            {
		return(LOGIO_STATUS_RESOURCES);
	    }
	    /* should be etherhdrsize - decaps_preamble for xtdemon */
	    pptit->curr->data = &pptit->curr->buff[UDPIPETHSZ -
						   SERIAL_HEADER_SIZE];
	    pptit->curr->dlen = 0;
	    ppbt->rx_logio_buff_count++;
	}
	/* get to the end of the chain */
	for ( plio=pptit->curr; plio->next_pckt; plio=plio->next_pckt);
	
	/* while we do not have the maximum buffers */
	for ( ;ppbt->rx_logio_buff_count < ppbt->rx_max_logio_buff_count;)
	{
	    /* if we can not allocate a buffer return resources problem */
	    if((plio->next_pckt = logio_buff_get(1600, LOGIO_BUFF_TYPE_VT)) ==
	       logio_buff_NULL)
            {
		return(LOGIO_STATUS_RESOURCES);
	    }
	    plio = plio->next_pckt; /* point to the buffer just allocated */
	    /* should be etherhdrsize - decaps_preamble for xtdemon */
	    plio->data = &plio->buff[UDPIPETHSZ - SERIAL_HEADER_SIZE];
	    plio->dlen = 0;
	    prev_sr = logio_interrupt_disable();
	    ppbt->rx_logio_buff_count++;
	    logio_interrupt_enable(prev_sr);
	}
    }
    return( LOGIO_STATUS_OK );
}


/*****************************************************************************
 **
 ** logio_serial_init                                                   
 **                                                                         
 **     This function initializes the serial device. On the M68332EVK, the  
 **     serial device is the SCI sub-module of the QSM-module inside the    
 **     M68332. If the device is in interrupt mode, then this function sets 
 **     up the logio interrupt handing functions, and then performs the     
 **     device-specific initialization.                                     
 **                                                                         
 ** Inputs:                                                                 
 **                                                                         
 **     id:   ID of the target device                                      
 **                                                                         
 ** Outputs:                                                                
 **                                                                         
 **     NONE                                                                
 **                                                                         
 ** Return codes:                                                           
 **                                                                         
 **    LOGIO_STATUS_OK                                                      
 **    LOGIO_STATUS_RESOURCES                                               
 **                                                                         
 ** Side-effects:                                                           
 ** Global variables:                                                       
 **                                                                         
 ******************************************************************************/
#ifdef __STDC__
logio_status_t logio_serial_init(logio_device_id_t id)
#else
logio_status_t logio_serial_init(id)
logio_device_id_t id;
#endif
{
    logio_status_t ret = LOGIO_STATUS_OK;     
    /* return codes from logio functions */
   
    /* dev is access to device descriptor */
    serial_dev_desc_t *devid = (serial_dev_desc_t *)id->data;
    
    /* 
     * Set up the vectors and test routines for the serial device 
     * 
     * The serial device uses one vector for all events.  The events that 
     * cause the exception are:
     * 
     *    1. transmit register empty
     *    2. character received 
     *    3. error caused by data overrun, noisy line,
     *       framing error, parity error
     * 
     * Thus, the logio interrupt structures for serial should look like:
     * (if on the same vector....will be separate if vectors are not the same)
     * 
     *     +-------------------------------------------+
     *  +--+-next                                      |
     *  |  +-------------------------------------------+
     *  |  | vector number = SCI_VECTOR                |
     *  |  +-------------------------------------------+
     *  |  | table_size = 3                            |
     *  |  +-------------------------------------------+
     *  |  | num_events = 3                            |
     *  |  +-------------------------------------------+
     *  |  | table     o                               |
     *  |  +-----------+-------------------------------+
     *  |              |
     *  |              V
     *  |     +--------+----------+---------+-------+-------+-----------------+
     *  |     | serial | TTYTXRDY | <flags> |<null> |<null> | ser1_tx_tty_tst |
     *  |     +--------+----------+---------+-------+-------+----------------+
     *  |     | serial | TTYRXCHR | <flags> | <null>|<null> | ser1_rx_tty_tst |
     *  |     +--------+----------+---------+-------+-------+-----------------+
     *  |     | serial | TTYERROR | <flags> | <null>|<null> | ser1_er_tst     |
     *  |     +--------+----------+---------+-------+-------+-----------------+
     *  V
     */
    
    /* install <event,ID> pair for received character interrupt */
    if((ret = logio_set_vector_and_test(id, 
					devid->config.flavor ==
					serial_tty_device ?
					LOGIO_INT_EVENT_TTYRXCHR :
					LOGIO_INT_EVENT_PCKTRXPCKT, 
					devid->config.rx_int_vector, 
					devid->fops.rx_tst))!= LOGIO_STATUS_OK)
	return(ret);

    /* install <event,ID> pair for txrdy interrupt */
    if((ret = logio_set_vector_and_test(
					id, 
					devid->config.flavor
					== serial_tty_device ?
					LOGIO_INT_EVENT_TTYTXRDY :
					LOGIO_INT_EVENT_PCKTTXRDY, 
					devid->config.tx_int_vector, 
					devid->fops.tx_tst))!=LOGIO_STATUS_OK)
	return(ret);

    /* install <event,ID> pair for error condition interrupt */
    /* check if err_tst function is available */
    if(devid->fops.err_tst){	
	if((ret = logio_set_vector_and_test(
					    id, 
					    devid->config.flavor ==
					    serial_tty_device ?
					    LOGIO_INT_EVENT_TTYERROR :
					    LOGIO_INT_EVENT_PCKTERROR,
					    devid->config.err_int_vector,
					    devid->fops.err_tst)) !=
	   LOGIO_STATUS_OK)
	    return(ret);
    }
    
    /* 
     * Perform the device-specific initialization. 
     * 
     * On the M68332EVK, the serial device is part of the 68332 processor
     * and at board init, the descriptor is initialized and interrupts are
     * disabled.
     * At this point the device should be completely initialized with
     * interrupts still disabled. (A control function call will enable ints)
     */
    
   

    /* take care of device specific initialization */
    (*devid->fops.init)(devid);
    
#ifndef OLD_BUFFER_ALLOC
    
    /* this is NOT COMPLETE! (but will work for now) */
    /* dynamic allocation?  */
    
    /* take care of interface specific initialization */
    if ( devid->config.flavor == serial_packet_device )
    {
	devid->buf.pkt.rx_max_logio_buff_count = 2;
	devid->buf.pkt.rx_logio_buff_count     = 0;
	/* this will attempt to get up to rx_max_logio_buff_count buffers */
	ret = pkt_get_serial_buf(devid);     
	resync_pkt(&devid->buf.pkt.rx, &devid->buf.pkt.preamble);
    }
#endif
    
    
    return(ret);
}


/*****************************************************************************
**
** logio_serial_read                                                 
**                                                                      
**      This function reads characters from the serial device.          
**                                                                      
** Inputs:                                                              
**                                                                      
**      id:      The ID of the target device                          
**      buflenp:   On input, this contains a pointer to an integer that
**          contains the number of bytes in the input buffer 
**                                                                    
** Outputs:                                                           
**                                                                    
**      buf:      The data that were read                             
**      buflenp:   The number of bytes read                            
**                                                                      
** Return codes:                                                        
**                                                                      
**      LOGIO_STATUS_OK                                                 
**      LOGIO_STATUS_NOTSUPPORTED                                       
**                                                                      
** Side-effects:                                                        
** Global variables:                                                    
**                                                                      
******************************************************************************/
logio_status_t logio_serial_read(id, buf, buflenp)
logio_device_id_t   id;
char         *buf;
int         *buflenp;
{
    int status;
    int len=0;			/* number of characters received */
    unsigned int x;
    cpu_interrupt_t  prev_sr;
    unsigned char ch;
    /* dev is access to device descriptor */
    serial_dev_desc_t *dev = (serial_dev_desc_t *)id->data;
    logio_charbuf_t *inbuf = &dev->buf.tty.inbuf;
    
    /*
     * A logical device read is only supported on a TTYRXCHR device 
     * and not a PCKTRXPCKT device.
     */
    if(dev->config.flavor == serial_packet_device)
	return (LOGIO_STATUS_NOTSUPPORTED);
    
    /* if no chars available, call test */
    if(logio_charbuf_isempty(inbuf))
    {
#ifdef TTY_POLLED_AND_INTERRUPT
	/*
	 ** NOTE! this is an intermediate fix
	 ** to support both interrupt and polled modes before
	 ** the two have been separated.
	 */
	prev_sr = logio_interrupt_disable();
	status = logio_serial_dev_pgetc(dev, &ch);   
	logio_interrupt_enable(prev_sr);
	if ( status == VBSP_NO_CHAR_PRESENT )
	    return(LOGIO_STATUS_WOULDBLOCK);
	else
	{
	    /* store character to input buffer */
	    if (logio_charbuf_notfull(inbuf))
	    {
		logio_charbuf_write(inbuf, ch);
		inbuf->cbuf_cnt++;
		inbuf->cbuf_end = logio_charbuf_inc(inbuf, inbuf->cbuf_end);
	    }
	}
#else
	/* this for the non-combined interrupt/polled interface */
	return(LOGIO_STATUS_WOULDBLOCK);
#endif
    }
    
    /* read all chars available upto buflenp */
    
    len = inbuf->cbuf_cnt;	/* number of characters available now */
    if ( len > *buflenp )     
	/* get up to the number of characters in *buflenp */
	
	len = *buflenp;         /* but no more */
    
    for ( x=0; x<len; x++)	/* get the characters */
    {
	*buf++ = logio_charbuf_read(inbuf); /* copy character */
	inbuf->cbuf_start = logio_charbuf_inc(inbuf,inbuf->cbuf_start);
	/* and go to next character */
    }
    
    /* update the current count of characters in the buffer */
    prev_sr = logio_interrupt_disable();
    inbuf->cbuf_cnt -= len;
    logio_interrupt_enable(prev_sr);
    
#if 0
    /* this code had a long disable time.... */
    /* changed by rogerc */
    prev_sr = logio_interrupt_disable();
    while ((inbuf->cbuf_cnt > 0) && (len < *buflenp))
    {
	len++;
	*buf++ = logio_charbuf_read(inbuf); 
	inbuf->cbuf_start = logio_charbuf_inc(inbuf,inbuf->cbuf_start);
	inbuf->cbuf_cnt--;
    }
    logio_interrupt_enable(prev_sr);
#endif
    
    /* return the number of characters actually read */
    *buflenp = len;
    
    return(LOGIO_STATUS_OK);
}


/*****************************************************************************
**                                                               
** logio_serial_write                                         
**                                                               
**      This function writes characters to the serial device.    
**                                                               
** Inputs:                                                       
**                                                               
**      id:      The ID of the target device                       
**      buf:      The data to be written                         
**      buflenp:   On input, this contains a pointer to an integer that
**          contains the number of bytes in the input buffer     
**                                                               
** Outputs:                                                      
**                                                               
**      buflenp:   The number of bytes written                    
**                                                               
** Return codes:                                                 
**                                                               
**      LOGIO_STATUS_OK                                          
**      LOGIO_STATUS_NOTSUPPORTED                                
**                                                               
** Side-effects:                                                 
** Global variables:                                             
**                                                               
******************************************************************************/
#ifdef __STDC__
logio_status_t logio_serial_write(logio_device_id_t id, char *buf, int *buflenp)
#else
logio_status_t logio_serial_write(id, buf, buflenp)
logio_device_id_t   id;
char         *buf;
int         *buflenp;
#endif
{
    /* dev is access to device descriptor */
    serial_dev_desc_t *dev = (serial_dev_desc_t *)id->data;
    
    int len=0;			/* number of characters written */
    cpu_interrupt_t prev_sr;	/* the status register value */
    logio_charbuf_t *outbuf;
    
#ifdef DEBUG
    static int d1cnt = 0;
    sprintf(d1,"ser_write:buflen=%d d1cnt=%d",*buflenp,d1cnt);
    d1cnt++;
#endif
    
    /*
     * A logical device write is only supported on a TTYTXRDY device 
     * and not a PCKTTXRDY device.
     */
    if(dev->config.flavor == serial_packet_device)
	return (LOGIO_STATUS_NOTSUPPORTED);
    
    outbuf = &dev->buf.tty.outbuf;
    
    /* 
     * while (the output buffer is not full)
     *    write the character to the circular buffer 
     *    update the character buffer's end pointer
     *
     * after data placed in buffer
     *    increment the buffer's character count
     *
     * only the character count is in common with the isr
     */
    
    while (logio_charbuf_notfull(outbuf) && (len < *buflenp))
    {
	logio_charbuf_write(outbuf, *buf++);
	outbuf->cbuf_end = logio_charbuf_inc(outbuf, outbuf->cbuf_end);
	len++;
    }
    
    prev_sr = logio_interrupt_disable();
    outbuf->cbuf_cnt+=len;
    logio_interrupt_enable(prev_sr);
    
    /* normally returns 1 */
    /* START TRANSMITTER ONLY IF THERE IS CURRENTLY
       NOT A CHARACTER IN THE TRANSMITTER */
    if ( (*dev->fops.start_transmitter)(dev) == VBSP_SERIAL_PRIME_PUMP_NEEDED )
	logio_serial_prime_pump(dev);
    
    /* Return the actual number of characters written */
    *buflenp = len;
    
    return(LOGIO_STATUS_INPROGRESS);
}


/*****************************************************************************
**                                                                    
** logio_serial_control                                            
**                                                                    
** logio_device_control() is used for device specific commands. The last 
** parameter "arg" is a pointer to data to be used by the function or to 
** be filled in by the function.  Other functions may ignore arg or may  
** treat it directly as a data item; they may, for example, be passed an 
** int value.                                                            
**                                                                       
** For each device, the following controls are required for the BSP      
** developer to provide.                                                 
**                                                                       
** Control LOGIO_CONTROL_GETDEVNAME can be used for getting the name of a
** device. In this case "arg" is a buffer up to LOGIO_DEVNAMELEN,        
** currently 16 bytes.                                                   
**                                                                       
** Control LOGIO_CONTROL_GETDEVTYPE can be used to get the type of the   
** underlying physical device. In this case "arg" points to a variable of
** type logio_device_type_t. The only device type currently defined is   
** LOGIO_DEV_TYPE_GENERIC.                                               
**                                                                       
** Controls LOGIO_CONTROL_TIMERSET and LOGIO_CONTROL_TIMER_RESET can be used
** for devices LOGIO_VT_TIMER and LOGIO_TIMER_N. In this case "arg"         
** is a structure of type logio_time_t.                                     
**                                                                          
**         typedef struct {
**                 unsigned int    time;
**                 unsigned int    interval;
**         } logio_time_t;                                                  
**                                                                          
** If "time" is non-zero, it indicates the time to the next timer           
** expiration.  If "interval" is non-zero, it specifies a value to be
** used in reloading "time" when the timer expires.  Setting "time" to
** zero disables a timer; however, "time" and "interval" must still be      
** initialized.  Setting "interval" to zero causes a timer to be disabled   
** after its next expiration (assuming "time" is non-zero). Both "time"     
** and "interval" are in microseconds.                                      
**                                                                          
** Controls LOGIO_CONTROL_SETHWADDR and LOGIO_CONTROL_GETHWADDR can be used 
** for devices that support the notion of a physical address. In this case  
** "arg" is a structure.                                                    
**                                                                          
**      typedef struct{                                                     
**              int     bufflen;                                            
**              char    buff[MAX_SIZE];                                     
**      } logio_buff_t;                                                     
**                                                                          
** where bufflen is both input and output parameter.  For packet based      
** interfaces "arg" points to a structure of type logio_hw_addr_t (an
** array of LOGIO_HW_ADDR_LEN characters).                                  
**                                                                          
** Control LOGIO_CONTROL_RESET can be used to reset the device when         
** applicable.  The result is device dependent.                             
**                                                                          
** Control LOGIO_CONTROL_INT_INFO_GET is used to get information about a    
** specific interrupt.  Currently the only information is "flags".  In      
** this case "arg" points to a structure of type logio_int_entry_t          
** defined in logio.h by                                                    
**                                                                          
**    typedef struct {                                   
**       logio_int_event_t       event;                  
**       int         flags;                              
**    } logio_int_entry_t;                               
**                                                                          
** "event" is the only input parameter. "flags" values currently            
** supported are LOGIO_INT_ENABLED and LOGIO_INT_DISABLED.
** Control LOGIO_CONTROL_INTINFOSET is used to set the values specified in
** the structure.  It returns the old values.
**                                                                          
** Other device specific control command can be added to the list of        
** available commands. Ready Systems reserves the use of constants in the   
** range 0-0xffff.                                                          
**                                                                          
** This function is nonblocking. Certain operation may result the status    
** LOGIO_STATUS_INPROGRESS. In this case, the function logio_device_poll() can
** be used to find out the status of this operation.                       
**                                                                         
** Inputs:                                                                 
**                                                                         
**      id:      The ID of the target device                          
**      control:   The control command                               
**      arg:       A pointer to data used by the function or to be   
**             filled in by the function                            
**                                                                  
** Outputs:                                                         
**                                                                  
**      arg:       A pointer to data used by the function or to be   
**             filled in by the function                            
**                                                                         
** Return codes:                                                           
**                                                                         
**    LOGIO_STATUS_OK                                                      
**    LOGIO_STATUS_NOTSUPPORTED                                            
**    LOGIO_STATUS_INPROGRESS                                              
**                                                                         
** Side-effects:                                                           
** Global variables:                                                       
**                                                                         
******************************************************************************/
#ifdef __STDC__
logio_status_t logio_serial_control(logio_device_id_t id,
				    logio_control_t control, void *arg)
#else
logio_status_t logio_serial_control(id, control, arg)
logio_device_id_t id;
logio_control_t control;
void *arg;
#endif
{
    serial_dev_desc_t *dev = (serial_dev_desc_t *)id->data;
    logio_status_t retcode = LOGIO_STATUS_OK;
    logio_charbuf_t *outbuf;	/* for polled serial transmit */
    logio_charbuf_t *inbuf;
    cpu_interrupt_t  prev_sr;   
    /* previous status register for disable */
    
    vbsp_return_t stat;		/* return code for pgetc */
    unsigned char ch;		/* for pgetc */
#ifdef OPT332
    serial_pkt_tst_info_t *rx_info;
#endif
    switch(control)
    {
    case LOGIO_CONTROL_IN_POLL:
	
        /*
	 ** this option is called when interrupts are disabled and the
	 ** application wants to input data for either a packet or a
	 ** serial device.  Each time this option is called, the pgetc
	 ** function for the device will be called.  If the device is
	 ** a packet device, then the character will be processed for
	 ** the packet.  If the device is a tty device, then the character
	 ** will be placed in the character buffer for the device.
	 **
	 ** Interrupts are disabled for a large amount of the processing
	 ** here to ensure that there is no possible conflict with
	 ** the device interrupt service routine.  The application normally
	 ** would call this option ONLY with interrupts disabled, but the
	 ** disables are in place to provide a safe transfer mechanism.
	 **
	 ** Return Codes:
	 **
	 **    LOGIO_STATUS_INPROGRESS  - pkt:  not a complete packet
	 **                               tty:  not a character received
	 **
	 **    LOGIO_STATUS_OK - pkt:  packet rx complete
	 **                      tty:  character rx complete
	 */
#ifdef OPT332
        if ( dev->config.flavor == serial_packet_device )
	{
	    rx_info = &dev->buf.pkt.rx;
	    /* try to get more buffers if we are low */
	    if (dev->buf.pkt.rx_logio_buff_count <
		dev->buf.pkt.rx_max_logio_buff_count )
		pkt_get_serial_buf(dev);
	    
	    do
	    {
		prev_sr = logio_interrupt_disable();
		/* store character directly */
		stat = logio_serial_dev_pgetc(dev, rx_info->next_ch);
		if ( stat != VBSP_SUCCESS ) /* if no character recieved okay */
		{
		    if ( stat == VBSP_RX_OVERRUN )
			/* only resync if catastrophic error */
		    {
			resync_pkt(&dev->buf.pkt.rx, &dev->buf.pkt.preamble);
		    }
		}                   
		/* character rx okay */
		else 
		{
		    /* adjust for the character that was read */
		    rx_info->next_ch++;
		    if(!(--rx_info->byte_cnt))
			/* only call handler when byte_cnt decrements to 0 */
			logio_serial_packet_rx_isr(dev);
		    /* if we did not recieve a packet */
		}
		logio_interrupt_enable(prev_sr); /* make certain we reenable interrupts */
	    }
	    while (stat == VBSP_SUCCESS);
	    if(dev->buf.pkt.nxt_pkt)	
		/* if a message is currently available, return OK */
		retcode = LOGIO_STATUS_OK;
	    else
		retcode = LOGIO_STATUS_INPROGRESS;
	}
#else
	if ( dev->config.flavor == serial_packet_device )
	{
	    retcode = LOGIO_STATUS_OK; /* char/pkt received */
	    /* try to get more buffers if we are low */
	    if (dev->buf.pkt.rx_logio_buff_count <
		dev->buf.pkt.rx_max_logio_buff_count )
		pkt_get_serial_buf(dev);
	    
	    do
	    {
		prev_sr = logio_interrupt_disable();
		stat = logio_serial_dev_pgetc(dev, &ch);
		if ( stat != VBSP_SUCCESS )
		    /* if no character recieved okay */
		{
		    if ( stat == VBSP_RX_OVERRUN )
			/* only resync if catastrophic error */
		    {
			resync_pkt(&dev->buf.pkt.rx, &dev->buf.pkt.preamble);
		    }
		    retcode = LOGIO_STATUS_INPROGRESS;  
		    /* no character */
		}   
		/* character rx okay */
		else if(!logio_serial_packet_rx(dev,ch))
		    /* if we did not recieve a packet */
		{		/* common packet code is in this function */
		    retcode = LOGIO_STATUS_INPROGRESS; /* no char/pkt */
		}
		logio_interrupt_enable(prev_sr);
		/* make certain we reenable interrupts */
	    }
	    while (stat == VBSP_SUCCESS);
	    if(dev->buf.pkt.nxt_pkt)		
		/* if a message is currently available, return OK */
		retcode = LOGIO_STATUS_OK;
	}
#endif
	else			/* this is a tty device */
	{
	    inbuf = &dev->buf.tty.inbuf;
	    prev_sr = logio_interrupt_disable(); /* protect device with disables */
	    stat = logio_serial_dev_pgetc(dev, &ch); /* get a character */
	    logio_interrupt_enable(prev_sr);          
	    if ( stat == VBSP_NO_CHAR_PRESENT )
	    {
		retcode = LOGIO_STATUS_INPROGRESS; /* OK == no rx character */
	    }
	    else		/* there is a character! */
	    {
		/* store character to input buffer */
		if (logio_charbuf_notfull(inbuf))
		{
		    logio_charbuf_write(inbuf, ch);
		    inbuf->cbuf_cnt++;
		    inbuf->cbuf_end =
			logio_charbuf_inc(inbuf, inbuf->cbuf_end);
		}
		retcode = LOGIO_STATUS_OK; /* yes, there is a character */
	    }
	} /* end if tty device */
	break;
	
    case LOGIO_CONTROL_OUT_POLL:
        /*
	 ** This option is used by the application program to poll characters
	 ** out of the serial port.
	 **
	 ** This option should normally be called only when interrupts are
	 ** disabled.  This option ensures that interrupts are disabled
	 ** before any manipulation of the data structures or acces to
	 ** the device occurs.  This is to maintain the integrity of the
	 ** data structures in the case where the interrupts are enabled.
	 **
	 ** Return Codes:
	 **    LOGIO_OK          -  There are no more characters to transmit
	 **    LOGIO_INPROGRESS  -  There are more characters in the buffer
	 **                         to transmit.
	 **  
	 */
	
	if(dev->config.flavor == serial_packet_device)
	{ 
	    prev_sr = logio_interrupt_disable();
	    if (logio_serial_packet_tx(dev))
		/* if packet completely transmitted */
		retcode = LOGIO_STATUS_OK;
	    else
		retcode = LOGIO_STATUS_INPROGRESS;
	    logio_interrupt_enable(prev_sr);
	} 
	else			/* this is a tty device */
	{
	    outbuf  = &dev->buf.tty.outbuf;
	    retcode = LOGIO_STATUS_INPROGRESS;
	    prev_sr = logio_interrupt_disable();
	    if (outbuf->cbuf_cnt > 0)              
		/* still characters in the buffer */
	    {			/* try to output */
		stat = logio_serial_dev_pputc(dev, logio_charbuf_read(outbuf));
		if (stat == VBSP_SUCCESS)
		{
		    outbuf->cbuf_start =
			logio_charbuf_inc(outbuf,outbuf->cbuf_start);
		    outbuf->cbuf_cnt--;
		}
	    }
	    else		/* there are no more characters to transmit */
	    {
		retcode = LOGIO_STATUS_OK;
	    }
	    vbsp_interrupt_enable(prev_sr);
	}
	break;
	
    case LOGIO_CONTROL_SET_PARAMETERS:
	retcode = (*dev->fops.set_params)(dev, (serial_params_t*)arg);
	break;
	
    case LOGIO_CONTROL_INTINFOSET:
	retcode = (*dev->fops.int_info_set)(dev, (logio_int_entry_t *)arg);
	break;
	
    case LOGIO_CONTROL_INTINFOGET:
	retcode = (*dev->fops.int_info_get)(dev, (logio_int_entry_t *)arg);
	break;
	
    case LOGIO_CONTROL_GETDEVNAME:
	retcode = logio_device_get_name(id, (char *)arg);
	break;
	
    case LOGIO_CONTROL_GETDEVTYPE:
	*(logio_device_type_t *)arg = LOGIO_DEV_TYPE_GENERIC;
	retcode = LOGIO_STATUS_OK;
	break;
	
    case LOGIO_CONTROL_RESET:
	retcode = LOGIO_STATUS_OK;
	break;
	
    default:
	return(LOGIO_STATUS_NOTSUPPORTED);
    }
    
#ifdef CONTROL_DEBUG
    chk_pt10 = retcode;
#endif
    
    return(retcode);
}



/*****************************************************************************
**                                                                  
** logio_serial_poll                                             
**                                                                  
** Description:                                                     
**                                                                  
** This function is used to poll the status of an earlier control   
** operation.  When the returning status is LOGIO_STATUS_OK, arg will
** contain the expected results.                                     
**                                                                   
** Inputs:                                                           
**                                                                   
**      id:      The ID of the target device                    
**      control:   The control code                                   
**                                                                   
** Outputs:                                                          
**                                                                   
**      arg:      Pointer to data that contain the result. The data  
**          are valid when logio_device_poll returns          
**         LOGIO_STATUS_OK                                      
**                                                                   
** Return codes:                                                     
**                                                                   
**    LOGIO_STATUS_OK                                                
**    LOGIO_STATUS_INPROGRESS                                        
**      LOGIO_STATUS_NOTSUPPORTED                                    
**                                                                   
** Side-effects:                                                     
** Global variables:                                                 
**                                                                   
******************************************************************************/
#ifdef __STDC__
logio_status_t logio_serial_poll(logio_device_id_t id,
				 logio_control_t control, void *arg)
#else
logio_status_t logio_serial_poll(id, control, arg)
logio_device_id_t         id;
logio_control_t      control;
void          *arg;
#endif
{
    serial_dev_desc_t *dev = (serial_dev_desc_t *)id->data;
    cpu_interrupt_t prev_sr;
    logio_charbuf_t *outbuf;
    int stat;
    logio_status_t result = LOGIO_STATUS_INPROGRESS;
    
#ifdef DEBUG
    static int d5cnt = 0;
    sprintf(d5,"poll:%d", d5cnt);
    d5cnt++;
#endif
    
    if(control != LOGIO_CONTROL_WRITE)
	return(LOGIO_STATUS_NOTSUPPORTED);
    
    if(dev->config.flavor == serial_packet_device)
    {
	if(!dev->buf.pkt.tx.curr)
	    return(LOGIO_STATUS_OK);
	else
	    /* allow attempt to send a character */
	    logio_device_control(id,LOGIO_CONTROL_OUT_POLL,0);
    }
    /* else its a tty device so is charbuf empty */
    else if(logio_charbuf_isempty(&dev->buf.tty.outbuf))
	return(LOGIO_STATUS_OK);
    
    
    if(dev->config.flavor == serial_packet_device)
    { 
#ifdef PKT_POLLED_AND_INTERRUPT
	prev_sr = logio_interrupt_disable();
	if (logio_serial_packet_tx(dev))
	    result = LOGIO_STATUS_OK;
	logio_interrupt_enable(prev_sr);
#else
	if ( dev->buf.pkt.tx.curr == logio_buff_NULL )
	    result = LOGIO_STATUS_OK;
	else
	    result = LOGIO_STATUS_INPROGRESS;
#endif
    } 
    else			/* this is a tty device */
    {
#ifdef TTY_POLLED_AND_INTERRUPT
	outbuf = &dev->buf.tty.outbuf;
	prev_sr = logio_interrupt_disable();
	if (outbuf->cbuf_cnt > 0)
	{
	    stat = logio_serial_dev_pputc(dev, logio_charbuf_read(outbuf));
	    if (stat == VBSP_SUCCESS)
	    {
		outbuf->cbuf_start = 
		    logio_charbuf_inc(outbuf,outbuf->cbuf_start);
		outbuf->cbuf_cnt--;
		if (outbuf->cbuf_cnt == 0)
		    result = LOGIO_STATUS_OK;
		else
		    result = LOGIO_STATUS_INPROGRESS;
	    }
	    else
	    {
		result = LOGIO_STATUS_INPROGRESS;
	    }
	}
	else
	{
	    result = LOGIO_STATUS_OK;
	}
	vbsp_interrupt_enable(prev_sr);
#else
	if (outbuf->cbuf_cnt == 0)
	    result = LOGIO_STATUS_OK;
	else
	    result = LOGIO_STATUS_INPROGRESS;
#endif
    }
    return(result);
}


/*****************************************************************************
**                                                                     
** logio_serial_getmsg                                              
**                                                                     
**      This operation is not supported for the serial device, so this 
**      call will always return LOGIO_STATUS_NOTSUPPORTED.             
**                                                                     
** Inputs:                                                             
**                                                                     
**      id:      The ID of the target device                         
**                                                                     
** Outputs:                                                            
**                                                                     
**   **bufpp:          Received message                               
**                                                                     
** Return codes:                                                       
**                                                                     
**    LOGIO_STATUS_NOTSUPPORTED                                        
**                                                                     
** Side-effects:                                                       
** Global variables:                                                   
**                                                                     
******************************************************************************/
#ifdef __STDC__
logio_status_t logio_serial_getmsg(logio_device_id_t id, logio_buff_t**bufpp)
#else
logio_status_t logio_serial_getmsg(id, bufpp)
logio_device_id_t id;
logio_buff_t**bufpp;
#endif
{
    void pkt_add_ether_hdr(_ANSIPROT2(logio_buff_t *b,
				      struct sockaddr_in *port_p));
    
    struct sockaddr_in src;
    /* a flag thats true if sync & len have been read */
    cpu_interrupt_t prev_sr;
    /* dev is access to device descriptor */
    serial_dev_desc_t *dev = (serial_dev_desc_t *)id->data;
#ifdef PKT_POLLED_AND_INTERRUPT
    unsigned char ch;
    vbsp_return_t stat;
#endif
    
#ifdef DEBUG
    static int d2cnt = 0;
    static int d8cnt = 0;
    sprintf(d2,"getmsg: %d",d2cnt);
    d2cnt++;
#endif
  
    /*
     * A logical device getmsg is only supported on a PCKTRXPCKT device 
     * and not a TTYRXCHR device.
     */
    if(dev->config.flavor == serial_tty_device)
    {
	*bufpp = logio_buff_NULL;
	return (LOGIO_STATUS_OK);
    }
  
  
    /* if there is not currently a next packet available */
    if(!dev->buf.pkt.nxt_pkt)
    {
	/* try to get more buffers if we are low */
	if (dev->buf.pkt.rx_logio_buff_count <
	    dev->buf.pkt.rx_max_logio_buff_count )
	    pkt_get_serial_buf(dev);
	
	/* THERE IS NOT CURRENTLY A PACKET AVAILABLE!!! */
	
#ifdef PKT_POLLED_AND_INTERRUPT
	prev_sr = logio_interrupt_disable();
	stat = logio_serial_dev_pgetc(dev, &ch);
	if ( stat != VBSP_SUCCESS )
      	{
	    if ( stat == VBSP_RX_OVERRUN )
            {
		/* INSTRUCTION: if the device contains an error counter,
		   increment thecounter here.  An example follows:
		   ((zi8530_specific_config_t *)
		   devid->config.specific)->err_count += 1;
		   */
		/* only resync if catastrophic error */
		resync_pkt(&dev->buf.pkt.rx, &dev->buf.pkt.preamble);
            }
	    logio_interrupt_enable(prev_sr);
	    *bufpp = logio_buff_NULL;
	    return(LOGIO_STATUS_OK);
	}
	else if(!logio_serial_packet_rx(dev,ch)) /* common packet code is in this function */
	{
	    logio_interrupt_enable(prev_sr);
	    *bufpp = logio_buff_NULL;
	    return(LOGIO_STATUS_OK);
	}
	else			/* added at serial.6 */
	{
	    logio_interrupt_enable(prev_sr);
	}
#else
	/* THIS CODE WILL PERFORM THE SAME CODE AS ABOVE....*/
	logio_device_control(id,LOGIO_CONTROL_IN_POLL,0);
	/* this is non-combined interrupt/polled mode */
	*bufpp = logio_buff_NULL;
	return(LOGIO_STATUS_OK);
#endif
    }

#ifndef OLD_BUFFER_ALLOC
    /* refresh the receive buffers */
    /* this code is placed here since we assume that we will always have */
    /* buffers available for a bridge device. */
    /* previously, this code was called on each character to refresh any */
    /* depleted buffers */
    pkt_get_serial_buf(dev);
#endif
    
    *bufpp = dev->buf.pkt.nxt_pkt;
    
    /* protect data structure shared with the isr */
    
    prev_sr = logio_interrupt_disable();
    dev->buf.pkt.nxt_pkt = (*bufpp)->next_pckt;
    logio_interrupt_enable(prev_sr);
    
    (*bufpp)->next_pckt = 0;
  
#ifdef DEBUG
    memcpy(getmsgbuf,(char *)(*bufpp)->data,(*bufpp)->dlen);
#endif
    
  
#ifdef DEBUG2
    memcpy(getmsgbuf,(char *)(*bufpp)->data,20);
#endif
    
    /* decapsulate the message */
    if(pkt_serial_decaps(bufpp, &src.sin_addr.s_addr, &src.sin_port,
			 &(*bufpp)->dlen) != PKT_SERIAL_DECAPS_SUCCESS)
    {
	((*bufpp)->free)(*bufpp);
	*bufpp = logio_buff_NULL;
	return(LOGIO_STATUS_EPARAM);
    }
  
    /* not used until xtdemon, should be inside decaps for targets */
    pkt_add_ether_hdr(*bufpp, &src);
    
#ifdef DEBUG
    sprintf(d8,"getmsg: got msg =%d",d8cnt);
    d8cnt++;
#endif
  
    return(LOGIO_STATUS_OK);
}


/*****************************************************************************
**                                                                     
** logio_serial_putmsg                                              
**                                                                     
**      This operation is not supported for the serial device, so this 
**      call will always return LOGIO_STATUS_NOTSUPPORTED.             
**                                                                     
** Inputs:                                                             
**                                                                     
**      id:      The ID of the target device                         
**    *bufp:      buffer to send                                       
**                                                                     
** Outputs:                                                            
**                                                                     
**      NONE                                                           
**                                                                     
** Return codes:                                                       
**                                                                     
**    LOGIO_STATUS_NOTSUPPORTED                                        
**                                                                     
** Side-effects:                                                       
** Global variables:                                                   
**                                                                     
******************************************************************************/
#ifdef __STDC__
logio_status_t logio_serial_putmsg(logio_device_id_t id, logio_buff_t *bufp)
#else
logio_status_t logio_serial_putmsg(id, bufp)
logio_device_id_t         id;
logio_buff_t      *bufp;
#endif
{
    /* dev is access to device descriptor */
    WORD dummy;
    
    serial_dev_desc_t *dev = (serial_dev_desc_t *)id->data;
    cpu_interrupt_t  prev_sr;
    
#ifdef DEBUG
    static int d4cnt = 0;
#endif
    
    /*
     * A logical device putmsg is only supported on a PCKTTXRDY device 
     * and not a TTYTXRDY device.
     */
    if(dev->config.flavor == serial_tty_device)
	return (LOGIO_STATUS_NOTSUPPORTED);
  
    /* only one message allowed to xmit at a time in driver */
    prev_sr = logio_interrupt_disable();
    if(dev->buf.pkt.tx.curr)
    {
	logio_interrupt_enable(prev_sr);
	return(LOGIO_STATUS_WOULDBLOCK);
    }
  
    /*
     * Encapsulate message for serial communication.
     */
  
#ifdef DEBUG1
    if (!pmflg1)
    {
	sprintf(putbuf,"pkt=0x%x len=%d\n",bufp,bufp->dlen);
	memcpy(getmsgbuf,(char *)(bufp)->data,0x100);
	printf("pkt=0x%x len=%d\n",bufp,bufp->dlen);
    }
#endif
    
    /* dummy is a variable necessary since the pkt_serial_encaps()
    ** will modify the variable passed.  bufp->dlen cannot be
    ** passed since pkt_serial_encaps may deallocate the buffer
    ** in the course of the routine.
     */
    dummy =  bufp->dlen;
    /* port and ipaddr are found in ethernet headers by encaps */
    /*   if(pkt_serial_encaps(&bufp, 0, 0,&bufp->dlen)) */
    if(pkt_serial_encaps(&bufp, 0, 0,&dummy))
    {
	logio_interrupt_enable(prev_sr);
	return(LOGIO_STATUS_RESOURCES);
    }
  
    /*
       #ifdef DEBUG
       sprintf(d4,"putmsg: %d",d4cnt);
       d4cnt++;
       if (!pmflg)
       {
       pmflg = 1;
       if (memcmp((char *)&bufp->data[3], mybuf,4) == 0)
       memcpy(putmsgbuf,(char *)(bufp)->data,(bufp)->dlen);
       }
       #endif
       */

#ifdef DEBUG2
    if (memcmp((char *)&bufp->data[3], mybuf,4) == 0)
    {
	while (1);
    }
#endif
    
#if 0
    /* protect against race condition */
    prev_sr = logio_interrupt_disable();
#endif
    dev->buf.pkt.tx.curr = bufp;
    dev->buf.pkt.tx.byte_cnt = bufp->dlen;
    dev->buf.pkt.tx.next_ch = (BYTE *)bufp->data;
    logio_interrupt_enable(prev_sr);
    
    if ( (*dev->fops.start_transmitter)(dev) == VBSP_SERIAL_PRIME_PUMP_NEEDED )
	logio_serial_prime_pump(dev);
    
    return(LOGIO_STATUS_INPROGRESS);
}


/*****************************************************************************
**                                                                         
** resync_pkt
**                                                                         
** Inputs:                                                                 
**                                                                         
** Outputs:                                                                
** Return codes:                                                           
**                                                                         
**                                                                         
** Method:                                                                 
** Side-effects:                                                           
** Global variables:                                                       
**                                                                         
******************************************************************************/
#ifdef __STDC__
void resync_pkt(serial_pkt_tst_info_t *tst_info, pkt_preamble_t *preamble)
#else
void resync_pkt(tst_info, preamble)
serial_pkt_tst_info_t *tst_info;
pkt_preamble_t *preamble;
#endif
{
#ifdef OPT332
    /* a more elaborate algorithm could be used to resync, but for now */
    /* just throw away last message and try to sync */
    tst_info->mode = get_preamble_field;
    tst_info->byte_cnt = preamble->sync_cnt;
    tst_info->next_ch = (unsigned char *)tst_info->curr->data;
    tst_info->curr->dlen = 0;
#else
    /* a more elaborate algorithm could be used to resync, but for now */
    /* just throw away last message and try to sync */
    tst_info->mode = get_preamble_field;
    tst_info->byte_cnt = preamble->sync_cnt + preamble->len_cnt;
    tst_info->next_ch = (unsigned char *)tst_info->curr->data;
    tst_info->curr->dlen = 0;
#endif
    return;
}


#ifdef __STDC__
vbsp_return_t 
logio_serial_packet_tx(serial_dev_desc_t *devid)
#else
vbsp_return_t 
logio_serial_packet_tx(devid)
serial_dev_desc_t *devid;
#endif
{
    register serial_pkt_tst_info_t *outp = &devid->buf.pkt.tx;
    cpu_interrupt_t save_status;
    
    if (!outp->byte_cnt)
    {
	return( 1 );
    }
    else			/* there are characters to transmit */
    {
	save_status = cpu_interrupt_disable();
	if ( VBSP_SUCCESS == (*devid->fops.pputc)(devid,*outp->next_ch))
	{
	    outp->next_ch++;
	    if (!(--outp->byte_cnt))
	    {
		/* free and clean up when done */
		(*outp->curr->free)(outp->curr);
		outp->curr = logio_buff_NULL;
	    }
	}
	cpu_interrupt_restore(save_status);
    }
    
    return (FALSE);
}

/*****************************************************************************
**                                                                         
** logio_serial_packet_rx
**
**    This routine will be called from the packet interrupt service routine
**    and from the getmsg routine.
**                                                                         
** Inputs:                                                                 
**                                                                         
**    serial device descriptor
**
** Outputs:                                                                
** Return codes:                                                           
**                                                                         
**    0: serial did not recieve a packet
**    1: serial received a packet                                          
**                                                                         
** Method:                                                                 
** Side-effects:                                                           
** Global variables:                                                       
**                                                                         
******************************************************************************/
#ifdef __STDC__
int logio_serial_packet_rx(serial_dev_desc_t *id, unsigned char ch)
#else
int logio_serial_packet_rx(id, ch)
serial_dev_desc_t *id;
unsigned char ch;
#endif
{
    /* NOTE! remove the buffer allocation from this routine! */
    
    unsigned char *lenp;
    
    unsigned int len, i;
    /* NOTE! make the argument REGISTER instead of another variable! */
    register serial_dev_desc_t *devid = id;      
    register serial_pkt_tst_info_t *rx_info;
    logio_buff_t *lbufp, *newmsg;
    
    cpu_interrupt_t save_status;
#ifdef DEBUG
    static int d6cnt = 0;
#endif
    
    rx_info = &devid->buf.pkt.rx;
    
#ifdef DEBUG
    sprintf(d6,"pkt_rx: pgetcnt=%d",d6cnt);
    d6cnt++;
#endif
    
    /* we successfully read a character */
    *rx_info->next_ch = ch;
    
    save_status = cpu_interrupt_disable();
    
    if(!(--rx_info->byte_cnt))
    {
#ifdef DEBUG
	sprintf(d7,"pkt_rx: bytecnt=%d",rx_info->byte_cnt);
#endif

	if(rx_info->mode == get_data_field)
	{
            newmsg = rx_info->curr;
#ifdef OLD_BUFFER_ALLOC
            /* allocate a new buffer */
            if(pkt_get_serial_pkt_buf(rx_info) != LOGIO_STATUS_OK)
	    {
		/* a new buffer was not allocated, so
		 * reclaim the old one and don't 
		 * return TRUE
		 */
		rx_info->curr = newmsg;
		resync_pkt(rx_info, 
			   &devid->buf.pkt.preamble);
		cpu_interrupt_restore(save_status);
		return( FALSE );
	    }
            /* insert current pkt into nxt_pkt */
            /* if nxt_pkt is empty insert directly */
            if(!(lbufp = devid->buf.pkt.nxt_pkt))
		devid->buf.pkt.nxt_pkt = newmsg;
            else
	    {
		while(lbufp->next_pckt)
		    lbufp = lbufp->next_pckt;
		lbufp->next_pckt = newmsg;
	    }
            cpu_interrupt_restore(save_status);
            return( TRUE );
#else
	    /* NO BUFFER ALLOC IN ISR */
            if ( !newmsg->next_pckt )
	    {
		/* if there is not another buffer, recycle the current buffer
		** until a second buffer becomes available */
		rx_info->curr = newmsg;
		resync_pkt(rx_info, 
			   &devid->buf.pkt.preamble);
		/* return true here so that the getmsg routine will be called.
		** getmsg will cause the buffer allocation routine to be called
		** to link in another buffer....
		 */
		cpu_interrupt_restore(save_status);
		return( TRUE );
	    }
            else		/* there is a next buffer in the chain */
	    {
		/* insert current pkt into nxt_pkt */
		/* if nxt_pkt is empty insert directly */
		rx_info->curr = newmsg->next_pckt;
		/* point to the next packet for input */
		
		resync_pkt(rx_info,&devid->buf.pkt.preamble);
		devid->buf.pkt.rx_logio_buff_count--;
		/* decrement available buffers */
		newmsg->next_pckt = 0; /* and terminate the chain */
		lbufp = devid->buf.pkt.nxt_pkt;
		if(!lbufp)
		{
		    devid->buf.pkt.nxt_pkt = newmsg;
		}
		else
		{
		    while(lbufp->next_pckt)
			lbufp = lbufp->next_pckt;
		    lbufp->next_pckt = newmsg;
		    /* put the new packet on the data chain */
		}
		cpu_interrupt_restore(save_status);
		return( TRUE );
	    }
#endif
	}
	else			/* process len field and switch modes */
	{   
	    lenp = (unsigned char *)
		&rx_info->curr->data[devid->buf.pkt.preamble.sync_cnt];
	    len = *lenp++;
	    if(devid->buf.pkt.preamble.len_cnt > 1)
		len = (len << 8) | *lenp;
	    /* check len for valid */
	    if(len > devid->buf.pkt.preamble.max_pkt_len ||
	       len <= 0){
		resync_pkt(rx_info,
			   &devid->buf.pkt.preamble);
		cpu_interrupt_restore(save_status);
		return( FALSE );
	    }
	    /* increase by len and the current char */
	    rx_info->curr->dlen += 
		(rx_info->byte_cnt = len) + 1;
	    rx_info->mode = get_data_field;
	    rx_info->next_ch++;
	    cpu_interrupt_restore(save_status);
	    return(FALSE);
	}
    }
    else if(rx_info->mode == get_preamble_field)
    {
	/* check preamble sync chars */
	if(((i = rx_info->curr->dlen) <
	    devid->buf.pkt.preamble.sync_cnt) &&
	   (*rx_info->next_ch != 
	    devid->buf.pkt.preamble.sync_chars[i])){
	    resync_pkt(rx_info, &devid->buf.pkt.preamble);
	    cpu_interrupt_restore(save_status);
	    return(FALSE);
	}
	rx_info->curr->dlen++;
    }
    rx_info->next_ch++;
    cpu_interrupt_restore(save_status);
    return ( FALSE );
}

#ifdef OPT332

/*****************************************************************************
**                                                                         
** logio_serial_packet_rx_isr
**
**    This routine will be called from the packet interrupt service routine
**                                                                         
** Inputs:                                                                 
**                                                                         
**    serial device descriptor
**
** Outputs:                                                                
** Return codes:                                                           
**                                                                         
**    0: serial did not recieve a packet
**    1: serial received a packet                                          
**                                                                         
** Method:                                                                 
** Side-effects:                                                           
** Global variables:                                                       
**                                                                         
******************************************************************************/
#ifdef __STDC__
int logio_serial_packet_rx_isr(serial_dev_desc_t *id)
#else
int logio_serial_packet_rx_isr(id)
serial_dev_desc_t *id;
#endif
{
    /* NOTE! remove the buffer allocation from this routine! */
    
    unsigned char *lenp;
    
    unsigned int len, i;
    /* NOTE! make the argument REGISTER instead of another variable! */
    register serial_dev_desc_t *devid = id;      
    register serial_pkt_tst_info_t *rx_info;
    logio_buff_t *lbufp, *newmsg;
    
#ifdef DEBUG
    static int d6cnt = 0;
#endif
    
    rx_info = &devid->buf.pkt.rx;
    
#ifdef DEBUG
    sprintf(d6,"pkt_rx: pgetcnt=%d",d6cnt);
    d6cnt++;
#endif
    
    /* ASSUMPTIONS!!!!!!!!! */
    /* we have successfully read a character, and its been stored in */
    /* the next_ch pointer, and the byte_cnt has been decremented */
    /* the device driver for rx_packet should do the following */
    /* 1. store the character from the device data_register directly
     *    into the next_ch pointer location.
     * 2. increment the next_ch pointer.
     * 3. decrement the byte_cnt which is the number of characters
     *    that are to be read.
     * 4. if the byte_cnt has decremented to zero, then call this function
     *       and return the value this function returns
     *    else
     *       return false.
     */
    if(!(rx_info->byte_cnt))	/* make sure its not called in error */
    {
#ifdef DEBUG
	sprintf(d7,"pkt_rx: bytecnt=%d",rx_info->byte_cnt);
#endif
	
	if(rx_info->mode == get_data_field)
	{
	    newmsg = rx_info->curr;
	    /* NO BUFFER ALLOC IN ISR */
	    if ( !newmsg->next_pckt )
	    {
		/* if there is not another buffer, recycle the current buffer
		** until a second buffer becomes available */
		rx_info->curr = newmsg;
		resync_pkt(rx_info, 
			   &devid->buf.pkt.preamble);
		/* return true here so that the getmsg routine will be called.
		** getmsg will cause the buffer allocation
		** routine to be called
		** to link in another buffer....
		 */
		return( TRUE );
	    }
	    else		/* there is a next buffer in the chain */
	    {
		/* insert current pkt into nxt_pkt */
		/* if nxt_pkt is empty insert directly */
		rx_info->curr = newmsg->next_pckt;
		/* point to the next packet for input */
		
		resync_pkt(rx_info,&devid->buf.pkt.preamble);
		
		devid->buf.pkt.rx_logio_buff_count--;
		/* decrement available buffers */
	      
		newmsg->next_pckt = 0;
		/* and terminate the chain */
		
		if(!(lbufp = devid->buf.pkt.nxt_pkt))
		{
		    devid->buf.pkt.nxt_pkt = newmsg;
		}
		else
		{
		    while(lbufp->next_pckt)
			lbufp = lbufp->next_pckt;
		    lbufp->next_pckt = newmsg;
		    /* put the new packet on the data chain */
		}
		return( TRUE );
	    }
	}
	/* we are processing preamble, so bufp->dlen == number chars read */
	/* this count is used to determine if we are testing for sync, */
	/* or processing length */
	else if ((i = rx_info->curr->dlen) < devid->buf.pkt.preamble.sync_cnt)
	{			/* looking for sync char(s) */
	    /* check preamble sync chars */
	    if((unsigned char)rx_info->curr->data[i] != 
	       devid->buf.pkt.preamble.sync_chars[i])
	    {
		resync_pkt(rx_info, &devid->buf.pkt.preamble);
		return(FALSE);
	    }
	    /* increment dlen and check if changing state */
	    if(++rx_info->curr->dlen == devid->buf.pkt.preamble.sync_cnt)
		rx_info->byte_cnt = devid->buf.pkt.preamble.len_cnt;
	    else
		rx_info->byte_cnt = 1; /* just check on every char */
	}
	else			/* process len field and switch modes */
	{   
	    lenp = (unsigned char *)&rx_info->curr->data[i];
	    len = *lenp++;
	    if(devid->buf.pkt.preamble.len_cnt > 1)
		len = (len << 8) | *lenp;
	    /* check len for valid */
	    if(len > devid->buf.pkt.preamble.max_pkt_len ||
	       len <= 0){
		resync_pkt(rx_info,
			   &devid->buf.pkt.preamble);
		return( FALSE );
	    }
	    /* increase by len and the byte size of len cnt */
	    rx_info->curr->dlen += 
		(rx_info->byte_cnt = len) + devid->buf.pkt.preamble.len_cnt;
	    rx_info->mode = get_data_field;
	    return(FALSE);
	}
    }
    return ( FALSE );
}
#endif

/*************************************************************************
** logio_serial_prime_pump
**
** This function enables the interrupts of the transmitter to put out
** the first character
**
** Inputs:
**     devid
** Outputs:
**     VBSP_SUCCESS
**
 */

#ifdef __STDC__
vbsp_return_t 
logio_serial_prime_pump(serial_dev_desc_t *dev)
#else
vbsp_return_t logio_serial_prime_pump(dev)
serial_dev_desc_t *dev;
#endif
{
    cpu_interrupt_t s;		/* for interrupt enable/disable */
    unsigned char ch;		/* for reading the tty character */
    
    register serial_pkt_tst_info_t *p_sptit;
    register logio_charbuf_t *outbuf;
    
    if(dev->config.flavor == serial_packet_device)
    {
	p_sptit = &dev->buf.pkt.tx;
	s = vbsp_interrupt_disable();
	if (p_sptit->byte_cnt > 0)
	{
#ifdef DEBUG
	    sprintf(b3,"pkt_strt_xmitter:");
#endif 
	    if (logio_serial_dev_pputc(dev, *(p_sptit->next_ch)) == VBSP_SUCCESS)
	    {
		p_sptit->next_ch++;
		if (!(--p_sptit->byte_cnt))
		{
		    /*
		     ** THIS SECTION OF CODE SHOULD NEVER
		     ** EXECTUTE SINCE PACKETS ARE
		     ** LONGER THAN A SINGLE BYTE.
		     ** This is just for insurance! 
		     ** (everyone likes insurance at 9:00 at night......)
		     */
		    /* free and clean up when done */
		    (*p_sptit->curr->free)(p_sptit->curr);
		    p_sptit->curr = logio_buff_NULL;
		}
            }
	}
	vbsp_interrupt_enable(s);
    }
    else			/* this is a tty device */
    {
	outbuf = &dev->buf.tty.outbuf;
	s = vbsp_interrupt_disable();
	if (outbuf->cbuf_cnt > 0)
	{
	    ch = logio_charbuf_read(outbuf);
	    
#ifdef DEBUG
	    sprintf(b2,"tty_strt_xmitter:ch=%x",ch);
#endif 
	    
	    if (logio_serial_dev_pputc(dev, ch) == VBSP_SUCCESS)
	    {
		outbuf->cbuf_start = 
		    logio_charbuf_inc(outbuf, outbuf->cbuf_start);
		outbuf->cbuf_cnt--;
	    }
	}
	vbsp_interrupt_enable(s);
    } /* end else this is a tty device */
    
    return (VBSP_SUCCESS);
}

#if defined(PPC860)
vbsp_return_t
logio_serial_dev_pgetc(serial_dev_desc_t *dev, unsigned char *pchar)
{
    unsigned int dcache_status;
    vbsp_return_t status;

    dcache_status = cpu_cm_data_cache_disable();
    status = (*dev->fops.pgetc)(dev, pchar);   
    cpu_cm_data_cache_restore(dcache_status);

    return(status);
}

vbsp_return_t
logio_serial_dev_pputc(serial_dev_desc_t *dev, unsigned char ch)
{
    unsigned int dcache_status;
    vbsp_return_t status;

    dcache_status = cpu_cm_data_cache_disable();
    status = (*dev->fops.pputc)(dev, ch);   
    cpu_cm_data_cache_restore(dcache_status);

    return(status);
}

#endif /* defined(PPC860) */
