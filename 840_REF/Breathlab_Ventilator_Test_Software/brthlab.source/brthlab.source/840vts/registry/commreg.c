/*****************************************************************************/
/*   Module Name:    COMMREG.c                                            
/*   Purpose:      API for managing comm information in the registry                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <windows.h>
#include <ansi_c.h>
#include <stdio.h>

#include "commreg.h"

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     GetCommParams                                                  
/*                                                                           
/* Purpose:  Retrieves device comm settings from the registry
/*                                          
/*                                                                           
/* Example Call:   int GetCommParams(RegComStruct* Params, char* DevKey);                                    
/*                                                                           
/* Input:    params - struct to store settings to
/*			 DevKey - the registry key of the device
/*                                          
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0=No Error													 ***/
/* 	     1=Error	                                                        
/*                                                                           
/*****************************************************************************/
int GetCommParams(RegComStruct* Params, char* DevKey)
{
	DWORD retlen;
	HKEY CPVTkey;
	HKEY COMMkey;

	if(Params == NULL || strcmp(DevKey, COM_840KEY) && strcmp(DevKey, COM_PTSKEY)){
		return 1;
	}
	Params->port     = 0;
	Params->baud     = 0;
	Params->databits = 0;
	Params->stopbits = 0;
	Params->parity   = 0;
	Params->autoDetectDisabled = 0;
	retlen = 4;
	RegOpenKeyEx(COM_HKEY, COM_SUBKEY, RESERVED_ZERO, KEY_QUERY_VALUE, &CPVTkey);
	RegOpenKeyEx(CPVTkey,  DevKey,     RESERVED_ZERO, KEY_QUERY_VALUE, &COMMkey);
	RegQueryValueEx(COMMkey, COM_PORTKEY,       NULL, NULL, (BYTE *)&Params->port,     &retlen);
	RegQueryValueEx(COMMkey, COM_BAUDKEY,       NULL, NULL, (BYTE *)&Params->baud,     &retlen);
	RegQueryValueEx(COMMkey, COM_DBKEY,         NULL, NULL, (BYTE *)&Params->databits, &retlen);
	RegQueryValueEx(COMMkey, COM_SBKEY,         NULL, NULL, (BYTE *)&Params->stopbits, &retlen);
	RegQueryValueEx(COMMkey, COM_PARITYKEY,     NULL, NULL, (BYTE *)&Params->parity,   &retlen);
	RegQueryValueEx(COMMkey, COM_AUTODETECTKEY, NULL, NULL, (BYTE *)&Params->autoDetectDisabled, &retlen);
	RegCloseKey(COMMkey);
	RegCloseKey(CPVTkey);

	return 0;
}

/*****************************************************************************/
/* Name:     SetCommParams                                                      
/*                                                                           
/* Purpose:  sets a devices' comm settings in the registry
/*                                          
/*                                                                           
/* Example Call:   int SetCommParams(RegComStruct* Params, char* DevKey);                                    
/*                                                                           
/* Input:   Params - structure which has device comm settings to save
/*			DevKey - registry key of the device to save
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0=No Error													 ***/
/*	     1=Error	 	                                                      
/*                                                                           
/*****************************************************************************/
int SetCommParams(RegComStruct* Params, char* DevKey)
{
	DWORD retlen;
	HKEY CPVTkey;
	HKEY COMMkey;
	unsigned int disposition;

	if(Params == NULL || strcmp(DevKey, COM_840KEY) && strcmp(DevKey, COM_PTSKEY)){
		return 1;
	}
	retlen = 4;
	RegOpenKeyEx(  COM_HKEY, COM_SUBKEY, RESERVED_ZERO, KEY_QUERY_VALUE, &CPVTkey);
	RegCreateKeyEx(CPVTkey,  DevKey,     RESERVED_ZERO, "KEY", REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, 
					NULL, &COMMkey, &disposition);
	RegSetValueEx(COMMkey, COM_PORTKEY,       RESERVED_ZERO, REG_DWORD, (BYTE *)&Params->port,     4);
	RegSetValueEx(COMMkey, COM_BAUDKEY,       RESERVED_ZERO, REG_DWORD, (BYTE *)&Params->baud,     4);
	RegSetValueEx(COMMkey, COM_DBKEY,         RESERVED_ZERO, REG_DWORD, (BYTE *)&Params->databits, 4);
	RegSetValueEx(COMMkey, COM_SBKEY,         RESERVED_ZERO, REG_DWORD, (BYTE *)&Params->stopbits, 4);
	RegSetValueEx(COMMkey, COM_PARITYKEY,     RESERVED_ZERO, REG_DWORD, (BYTE *)&Params->parity,   4);
	RegSetValueEx(COMMkey, COM_AUTODETECTKEY, RESERVED_ZERO, REG_DWORD, (BYTE *)&Params->autoDetectDisabled, 4);
	RegCloseKey(COMMkey);
	RegCloseKey(CPVTkey);
	return 0;
}

/*****************************************************************************/
/* Name:     Get840CommParams                                                 
/*                                                                           
/* Purpose:  Get the 840 comm params from the registry
/*                                          
/*                                                                           
/* Example Call:  int Get840CommParams(RegComStruct* Params);                                    
/*                                                                           
/* Input:    structure to store settings in       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0=No Error													 ***/
/*	     1=Error	 	                                                         
/*                                                                           
/*****************************************************************************/
int Get840CommParams(RegComStruct* Params)
{
	return GetCommParams(Params, COM_840KEY);
}

/*****************************************************************************/
/* Name:     GetPTSCommParams                                                    
/*                                                                           
/* Purpose:  Get the PTS comm params from the registry.
/*                                          
/*                                                                           
/* Example Call:   int GetPTSCommParams(RegComStruct* Params);                                    
/*                                                                           
/* Input:    Params - structure to store params in		         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0=No Error													 ***/
/*		1=Error                                                         
/*                                                                           
/*****************************************************************************/
int GetPTSCommParams(RegComStruct* Params)
{
	return GetCommParams(Params, COM_PTSKEY);
}


/*****************************************************************************/
/* Name:     Set840CommParams                                                     
/*                                                                           
/* Purpose:  store 840 comm params to the registry
/*                                          
/*                                                                           
/* Example Call:   int Set840CommParams(RegComStruct* Params);                                    
/*                                                                           
/* Input:    structure which contains params to save		         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0=No Error													 ***/
/*		1=Error	 	                                                         
/*                                                                           
/*****************************************************************************/
int Set840CommParams(RegComStruct* Params)
{
	return SetCommParams(Params, COM_840KEY);
}

/*****************************************************************************/
/* Name:     SetPTSCommParams                                                     
/*                                                                           
/* Purpose:   store 840 comm params to the registry  
/*                                          
/*                                                                           
/* Example Call:   int SetPTSCommParams(RegComStruct* Params);                                    
/*                                                                           
/* Input:   structure which contains params to save        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0=No Error													 ***/
/*		1=Error	                                                        
/*                                                                           
/*****************************************************************************/
int SetPTSCommParams(RegComStruct* Params)
{
	return SetCommParams(Params, COM_PTSKEY);
}
