
#ifndef PatientDataMgr_HH
#define PatientDataMgr_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PatientDataMgr - Manager Class of all patient data.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PatientDataMgr.hhv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc      Date:  16-May-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project related changes.
//
//  Revision: 007   By: yakovb   Date:  19-Mar-2002    DR Number: 5863
//  Project:  VCP
//  Description:
//      Handle VC+ mandatory messages.
//
//  Revision: 006   By: yakovb   Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Handle VC+/VS Startup messages.
//
//  Revision: 005   By: syw   Date:  03-Aug-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      * added new 'NewComputedPAVData()' method
//      * added new 'RecvComputedPavData()' method
//
//  Revision: 004  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  hct    Date:  03-FEB-98    DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//        Added static compliance and static resistance.
//
//  Revision: 002  By:  yyy    Date:  27-Jan-97    DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//        Initial version for Bilevel.
//
//  Revision: 001  By:  jhv    Date:  01-APR-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "PatientDataId.hh"
#include "ComputedInspPauseDataPacket.hh"
#include "AveragedBDPacket.hh"
#include  "BreathDataPacket.hh"
#include  "WaveformData.hh"
#include  "DiscreteValue.hh"
#include  "BoundedValue.hh"
#include  "BreathItemDataInfo.hh"
#include  "WaveformDataInfo.hh"
#include  "PEEPDataInfo.hh"
#include "NetworkMsgId.hh"
#include "PatientDataClassIds.hh"
#include "NetworkApp.hh"
#include "WaveformSet.hh"

#if   defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    
#  include "SettingObserver.hh"
#endif  // defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    

struct RealTimeDataPacket;
struct AveragedBDPacket;
struct BreathDataPacket;
struct WaveformData;
struct BiLevelMandDataPacket;
struct BiLevelMandExhDataPacket;
struct ComputedInspPauseDataPacket;
struct PavBreathDataPacket;
struct PavPlateauDataPacket;
struct PavStartupDataPacket;
struct BoundedBreathDatum;
class BreathItemDataInfo;
class WaveformDataInfo;
class InspPauseDataInfo;

struct VtpcvStateDataPacket;

class PatientDataMgr
#if   defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    
					: public SettingObserver
#endif   // defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    
{
  public:

    typedef void (*BDItemCallbackPtr)(const PatientDataId::PatientItemId id);
    typedef void (*CallbackPtr)(void);

    enum  callback_limits
    {
            MAX_CALLBACKS =10 
    };

#if   defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
							  const SettingSubject*               pSubject);
#endif   // defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    

    static void Initialize(void);

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
    static inline void NewRTBreathData(RealTimeDataPacket *bdPtr, Uint size);
    static inline void NewAveragedBreathData(AveragedBDPacket *bdPtr, Uint size);
    static inline void NewEndBreathData(BreathDataPacket *bdPtr, Uint size);
    static inline void NewPeepRecovery(void);
    static inline void NewWaveformData(WaveformData *WaveData, Uint size);
    static inline void NewPeEndData(Real32 value);
    static inline void NewPiEndData(Real32 value);
    static inline void NewNifPressureData(Real32 value);
    static inline void NewP100PressureData(Real32 value);
    static inline void NewVitalCapacityData(Real32 value);
    static inline void NewBiLevelMandData(BiLevelMandDataPacket *bdPtr, Uint size);
    static inline void NewBiLevelMandExhData(BiLevelMandExhDataPacket *bdPtr, Uint size);
    static inline void NewComputedInspPauseData(ComputedInspPauseDataPacket *pPacket);
        
    static void  NewPavBreathData(PavBreathDataPacket *pPacket);
    static void  NewPavPlateauData(PavPlateauDataPacket *pPacket);
    static void  NewPavStartupData(PavStartupDataPacket *pPacket);

    static inline void NewEndExpPauseData(PauseTypes::PpiState pressureState);
    static inline void NewFiO2Data(Real32 value);
#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)


#if   defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    
    static inline Boolean  IsBoundedId (const PatientDataId::PatientItemId itemId);
    static inline Boolean  IsDiscreteId(const PatientDataId::PatientItemId itemId);
    
    static inline Boolean  IsWaveformActive();
    static Boolean  IsBreathPaused();
    static Boolean  IsBreathItemId(const PatientDataId::PatientItemId itemId);
    
    static inline BreathItemDataInfo *GetDataAgent( const PatientDataId::PatientItemId itemId);
    
    static inline void ActivateWaveformData();
    static inline void DeactivateWaveformData();
    
    static inline SigmaStatus TakeWaveformData(WaveformData *pData);
    
    static void RecvWaveformData( XmitDataMsgId, void* wfPtr, Uint size);
    static void RecvEndBreathData( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvRTBreathData( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvPeepRecovery( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvAveragedBreathData( XmitDataMsgId, void  *bdPtr, Uint  size);
    static void RecvPeEndData( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvPiEndData( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvBiLevelMandData( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvBiLevelMandExhData( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvComputedInspPauseData(XmitDataMsgId, void *bdPtr, 
        Uint  size);
    
    static void RecvPavBreathData(XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvPavPlateauData(XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvPavStartupData(XmitDataMsgId, void *bdPtr, Uint  size);
    
    static void RecvEndExpPauseData( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvNifPressureData( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvP100PressureData( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvVitalCapacityData( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvFiO2Data( XmitDataMsgId, void *bdPtr, Uint  size);
    static void RecvVtpcvStateData(XmitDataMsgId, void *pData, Uint size);
    
    static inline void RegisterBDItemCallback(BDItemCallbackPtr pCallback);
    static inline void RegisterEndOfBreath( CallbackPtr pCallback);
    static inline void RegisterAverageData( CallbackPtr pCallback);
    static inline void RegisterRealtimeData( CallbackPtr pCallback);
    
    static inline const BoundedValue& GetBoundedValue
                        (const PatientDataId::PatientItemId id);
    static const BoundedBreathDatum& GetBreathDatum
	                    ( const PatientDataId::PatientItemId id);
    static inline DiscreteValue GetDiscreteValue
                        (const PatientDataId::PatientItemId id);
    
    static inline void MakeItemCalls(const PatientDataId::PatientItemId id);
#endif  // defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    

#if   defined(SIGMA_BD_CPU) || defined(SIGMA_COMMON)    
    static void  NewVtpcvStateData(VtpcvStateDataPacket *pPacket);
#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

    static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL,
						   const char*       pPredicate = NULL);

#if   defined(SIGMA_UNIT_TEST) 
    static void GenerateTimerEvent( const PatientDataId::PatientItemId id);
    static void StartPauseKey( Real32 initValue );
#endif // defined(SIGMA_UNIT_TEST) 
    
#if   defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    
    public:	
        PatientDataMgr(void);
        ~PatientDataMgr(void);
#else
    private:                  
        PatientDataMgr(void);                   // not implemented
        ~PatientDataMgr(void);                   // not implemented
#endif   // defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    

    private:      
      
        PatientDataMgr(const PatientDataMgr &); // not implemented
        void operator=(const PatientDataMgr&);  // not implemented


#if   defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    
    //@Data-Member: PBreathDataInfo_[NUM_TOTAL_PATIENT_DATA];
    // PBreathDataInfo_ is an array that contains Breath Data Processing
    // objects.
    static BreathItemDataInfo *PBreathDataInfo_[PatientDataId::NUM_TOTAL_PATIENT_DATA];

    //@Data-Member: PWaveformDataInfo_;
    // PWaveformDataInfo_ is a pointer to the  Waveform Data Processing
    // object which manages waveform buffer pool. The object receives 
    // waveform data from the Breath Delivery subsystemand provides 
	// the waveform data to other subsystems.
    static WaveformDataInfo  *PWaveformDataInfo_;

    //@Data-Member: PPEEPDataInfo_;
    // PPeepDataInfo_ points to a PEEPDataInfo object which manages
    // intrinsic and total PEEP values during a breath pause phase.
    static PEEPDataInfo  *PPEEPDataInfo_;

    //@Data-Member: PInspPauseDataInfo_;
    // PInspPauseDataInfo_ points to a InspPauseDataInfo object which manages
    // circuit pressure values during inspiratory pause phase.
    static InspPauseDataInfo  *PInspPauseDataInfo_;

    //@Data-Member: TotalBDItemCalls_ [static]
    // Total number of callback routines that are registered to be called
    // whenever a new patient data item is available.
    static Uint TotalBDItemCalls_;

    //@Data-Member: BDItemCallbacks  [static]
    // BDItemCallbacks_ contains an array of callback routines
    // that are called whenever a new breath data item is available. 
    static BDItemCallbackPtr BDItemCallbacks_[MAX_CALLBACKS];

    //@Data-Member: TotalEndBreathCalls_ [static]
    // Total number of callback routines that are registered to be called
    // when a new end of breath data packet arrives.
    static Uint TotalEndBreathCalls_;

    //@Data-Member: EndBreathCallbacks_  [static]
    // The following static array contains addresses of callback routines
    // that are to be called when an end of breath data packet arrives to the
    // Patient Data Manager Sub-system.
    static CallbackPtr EndBreathCallbacks_[MAX_CALLBACKS];


    //@Data-Member: TotalAverageCalls_ [static]
    // Total number of callback routines that are registered to be called
    // when a new averaged breath data packet arrives.
    static Uint TotalAverageCalls_;

    //@Data-Member: AverageBreathCallbacks_  [static]
    // AverageBreathCallbacks_ is an array which contains addresses of callback
    // routines that are to be called when an averaged breath data packet
    // arrives.
    static CallbackPtr AverageBreathCallbacks_[MAX_CALLBACKS];

    //@Data-Member: TotalRealtimeCalls_ [static]
    // Total number of callback routines that are registered to be called
    // when a new realtime breath data packet arrives.
    static Uint TotalRealtimeCalls_;

    //@Data-Member: ReatimeCallbacks_  [static]
    // ReatimeDataCallbacks_ is an array which contains addresses of callback
	// routines that are to be called when a realtime breath data packet
	// arrives.
    static CallbackPtr RealtimeCallbacks_[MAX_CALLBACKS];

#endif //defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    
};


#include "PatientDataMgr.in"


#endif // PatientDataMgr_HH
 
