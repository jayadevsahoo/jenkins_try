#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ExpTimeSetting - Expiratory Time Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the time (in milliseconds)
//  an expiration lasts.  This class inherits from 'BatchBoundedSetting' and
//  provides the specific information needed for representation of
//  expiratory time.  This information includes the interval and range of
//  this setting's values, this setting's response to a change of its
//  primary setting (see 'acceptPrimaryChange()'), this batch setting's
//  new-patient value (see 'getNewPatientValue()'), and the contraints and
//  dependent settings of this setting.
//
//  This setting also dynamically monitors the constant parm setting,
//  and updates its applicability accordingly.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines an 'updateConstraints_()' method for updating
//  the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ExpTimeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 014   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 013  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  changed array of dependent ids to a non-constant array, which
//         can include peak flow, for 'VC+'
//      *  added new transition rule requirement mappings
//      *  added new 'calcDependentValues_()' methods for determining list
//         of dependents
//
//  Revision: 012   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 011   By: sah   Date:  12-Mar-1999    DR Number: 5310
//  Project:  ATC
//  Description:
//     Added missing '$' to requirement mapping.
//
//  Revision: 010   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed unnecessary pre-condition
//	*  added 'acceptTransition()' method due to new Transition Rule
//	   implementation
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//	*  now an observer of constant parm setting, therefore 'valueUpdate()',
//	   'doRetainAttachment()' and 'settingObserverInit()' overridden from
//	   observer base class
//
//  Revision: 009   By: dosman Date:  11-Nov-1998    DR Number: 5252
//  Project:  BILEVEL
//  Description:
//	Added SRS requirement #
//
//  Revision: 008   By: dosman Date:  16-Sep-1998    DR Number: BiLevel 66 & 144
//  Project:  BILEVEL
//  Description:
//      The intermediate value of inspiratory time computed in
//      calcBasedOnSetting_ was changed to be warped in the same
//      direction as the direction the "setting being calculated".
//      Also, special processing was added in BoundedSetting::calcNewValue()
//
//  Revision: 006   By: dosman Date:  07-May-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	added TIs
//
//  Revision: 005   By: dosman Date:  29-Apr-1998    DR Number: 34
//  Project:  BILEVEL
//  Description:
//	Added explicit change of TE versus TL prompt based on mode BiLevel
//	Added symbolic constants.
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	Intermediate calculations of NON-CONSTRAINT values of this setting
//	no longer need to be "warped" to a resolution value, therefore replace
//	'warpValue()' with the new 'getValuePrecision()' method, and
//	remove the 'warpDirection' parameter from the calculation
//	method(s).  Also, update the two DEVELOPMENT-only methods for
//	testing the validity of the adjusted/accepted values to use
//	'IsEquivalent()'.  NOTE:  the warping directions for the insp.
//	time min/max bounds was changed to 'WARP_NEAREST', because the
//	calculation is just a summation of floating points which may
//	have a very small delta from the actual resolution value.
//
//  Revision: 002   By: sah    Date:  10-Aug-1997    DCS Number: 2359
//  Project: Sigma (R8027)
//  Description:
//	Added warning message for future maintenance.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ExpTimeSetting.hh"
#include "SettingConstants.hh"
#include "ModeValue.hh"
#include "MandTypeValue.hh"
#include "ConstantParmValue.hh"
#include "FlowPatternValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "RespRateSetting.hh"
#include "ConstantParmSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02119] -- this setting's dependent settings...
static SettingId::SettingIdType  ArrDependentSettingIds_[] =
{
	SettingId::IE_RATIO,
	SettingId::INSP_TIME,
	SettingId::PEAK_INSP_FLOW,

	SettingId::NULL_SETTING_ID
};


// $[02115] The setting's range ...
// $[02117] The setting's resolution ..
static const BoundedInterval  LAST_NODE_ =
{
	200.0f,		// absolute minimum...
	0.0f,		// unused...
	TENS,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	100000.0f,		// absolute maximum...
	10.0f,		// resolution...
	TENS,		// precision...
	&::LAST_NODE_
};



//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ExpTimeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, the method
//  initializes the value interval range.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ExpTimeSetting::ExpTimeSetting(void)
: BatchBoundedSetting(SettingId::EXP_TIME,
					  ::ArrDependentSettingIds_,
					  &::INTERVAL_LIST_,
					  NULL_SETTING_BOUND_ID, // maxConstraintId...
					  EXP_TIME_MIN_ID)	 // minConstraintId...
{
	CALL_TRACE("ExpTimeSetting()");
	isATimingSetting_ = TRUE;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ExpTimeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ExpTimeSetting::~ExpTimeSetting(void)
{
	CALL_TRACE("~ExpTimeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01049] -- Te not shown in SPONT mode
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	ExpTimeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pMode     = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);
	const Setting*  pConstantParm =
		SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM);

	DiscreteValue  modeValue;
	DiscreteValue  mandTypeValue;
	DiscreteValue  constantParmValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			modeValue         = pMode->getAcceptedValue();
			mandTypeValue     = pMandType->getAcceptedValue();
			constantParmValue = pConstantParm->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			modeValue         = pMode->getAdjustedValue();
			mandTypeValue     = pMandType->getAdjustedValue();
			constantParmValue = pConstantParm->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	Applicability::Id  applicability;

	switch ( modeValue )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :	  // $[TI3]
			switch ( mandTypeValue )
			{
				case MandTypeValue::VCP_MAND_TYPE :
				case MandTypeValue::PCV_MAND_TYPE :		// $[TI3.1]
					applicability =
						(constantParmValue == ConstantParmValue::EXP_TIME_CONSTANT)
						? Applicability::CHANGEABLE	   // $[TI3.1.1]
						: Applicability::VIEWABLE;	   // $[TI3.1.2]
					break;
				case MandTypeValue::VCV_MAND_TYPE :		// $[TI3.2]
					applicability = Applicability::VIEWABLE;
					break;
				default :
					// unexpected mandatory type...
					AUX_CLASS_ASSERTION_FAILURE(mandTypeValue);
					break;
			}
			break;
		case ModeValue::SPONT_MODE_VALUE :
		case ModeValue::CPAP_MODE_VALUE :
		case ModeValue::BILEVEL_MODE_VALUE :	  // $[TI4]
			applicability = Applicability::INAPPLICABLE;
			break;
		default :
			// unexpected mode...
			AUX_CLASS_ASSERTION_FAILURE(modeValue);
			break;
	}

	return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02116] -- this setting's new patient value
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	ExpTimeSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	BoundedValue  newPatient;

	// the expiratory time's new patient value is based on the new-patient
	// values of inspiratory time and respiratory rate...
	newPatient = calcBasedOnSetting_(SettingId::INSP_TIME,
									 BoundedRange::WARP_NEAREST,
									 BASED_ON_NEW_PATIENT_VALUES);

	return(newPatient);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned, and this setting's value is "clipped"
//  to that bound.  Otherwise, a value of 'NULL' is returned to indicate no
//  dependent bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When respiratory rate is changing while holding I:E ratio constant,
//  AND when I:E ratio is the primary setting, this implementation is based
//  on inspiratory time being update BEFORE this method gets called; this
//  method depends on the new inspiratory time being in the Adjusted Context,
//  during this type of update.
//
//  $[02002] -- new dependent settings based on proposed primary settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
	ExpTimeSetting::acceptPrimaryChange(const SettingId::SettingIdType primaryId)
{
	CALL_TRACE("acceptPrimaryChange(primaryId)");

	// update dynamic constraints...
	updateConstraints_();

	BoundStatus&  rBoundStatus = getBoundStatus_();

	// initialize to holding this setting's id...
	rBoundStatus.setViolationId(getId());

	BoundedValue  newExpTime;

	newExpTime = calcBasedOnSetting_(primaryId, BoundedRange::WARP_NEAREST,
									 BASED_ON_ADJUSTED_VALUES);

	// test 'newExpTime' against this setting's bounded range; the bound
	// violation state, if any, is returned in 'rBoundStatus', while
	// 'newExpTime' is "clipped" if a bound is violated...
	getBoundedRange_().testValue(newExpTime, rBoundStatus);

	// store the value...
	setAdjustedValue(newExpTime);

	const BoundStatus*  pBoundStatus;

	// if there is a bound violation return a pointer to this setting's
	// bound status, otherwise return 'NULL'...
	pBoundStatus = (rBoundStatus.isInViolation()) ? &rBoundStatus // $[TI1]
				   : NULL;	   // $[TI2]

	return(pBoundStatus);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02026]\b\   -- from-SPONT-to-AC/SIMV transition rules
//  $[BL02000]\g\ -- from-BILEVEL-to-AC/SIMV transition rules
//  $[02022]\i\   -- from-PC-to-VC transition rules
//  $[VC02006]\d\ -- from-VC-to-VC+ transition rules
//  $[VC02006]\k\ -- from-PC-to-VC+ transition rules
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ExpTimeSetting::acceptTransition(const SettingId::SettingIdType,
									 const DiscreteValue,
									 const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	if ( getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE )
	{  // $[TI1] -- this setting is applicable, process transition...
		BoundedValue  newExpTime;

		newExpTime = calcBasedOnSetting_(SettingId::INSP_TIME,
										 BoundedRange::WARP_NEAREST,
										 BASED_ON_ADJUSTED_VALUES);

		// store the transition value into the adjusted context...
		setAdjustedValue(newExpTime);

		setForcedChangeFlag();
	}  // $[TI2] -- this setting is inapplicable, leave unchanged...
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to subject's value changes, by, possibly, updating this setting's
//  applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::CONSTANT_PARM)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ExpTimeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
								const SettingSubject*               pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");
	SAFE_AUX_CLASS_PRE_CONDITION((pSubject->getId() == SettingId::CONSTANT_PARM),
								 pSubject->getId());

	// update this instance's applicability...
	updateApplicability(qualifierId);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	ExpTimeSetting::doRetainAttachment(const SettingSubject*) const
{
	CALL_TRACE("doRetainAttachment(pSubject)");

	return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Constant Parm setting,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ExpTimeSetting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	// monitor changes in constant parm's value...
	attachToSubject_(SettingId::CONSTANT_PARM, Notification::VALUE_CHANGED);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?   Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	ExpTimeSetting::isAcceptedValid(void) const
{
	CALL_TRACE("isAcceptedValid()");

	Boolean  isValid = BoundedSetting::isAcceptedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ACCEPTED) !=
			 Applicability::INAPPLICABLE )
		{
			const Real32  EXP_TIME_VALUE = BoundedValue(getAcceptedValue()).value;

			const DiscreteValue  CONSTANT_PARM_VALUE =
				ConstantParmSetting::GetValue(BASED_ON_ACCEPTED_VALUES);

			SettingId::SettingIdType  basedOnSettingId;

			switch ( CONSTANT_PARM_VALUE )
			{
				case ConstantParmValue::EXP_TIME_CONSTANT :
				case ConstantParmValue::INSP_TIME_CONSTANT :
					basedOnSettingId = SettingId::INSP_TIME;
					break;
				case ConstantParmValue::IE_RATIO_CONSTANT :
					basedOnSettingId = SettingId::IE_RATIO;
					break;
				default : 
					AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
					break;
			};

			const BoundedValue  CALC_EXP_TIME  =
				calcBasedOnSetting_(basedOnSettingId,
									BoundedRange::WARP_NEAREST,
									BASED_ON_ACCEPTED_VALUES);

			isValid = ::IsEquivalent(EXP_TIME_VALUE,
									 CALC_EXP_TIME.value,
									 CALC_EXP_TIME.precision);

			if ( !isValid )
			{
				// if not "equivalent" it may be because of round-off error, due
				// to the fact that this is being held at a resolution (via the
				// knob)...
				isValid =
					(EXP_TIME_VALUE >= (CALC_EXP_TIME.value - INTERVAL_LIST_.resolution)  &&
					 EXP_TIME_VALUE <= (CALC_EXP_TIME.value + INTERVAL_LIST_.resolution));
			}
		}
	}

	return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies? Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	ExpTimeSetting::isAdjustedValid(void)
{
	CALL_TRACE("IsAdjustedValid()");

	Boolean  isValid = BoundedSetting::isAdjustedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ADJUSTED) !=
			 Applicability::INAPPLICABLE )
		{
			const Real32  EXP_TIME_VALUE = BoundedValue(getAdjustedValue()).value;

			const DiscreteValue  CONSTANT_PARM_VALUE =
				ConstantParmSetting::GetValue(BASED_ON_ADJUSTED_VALUES);

			SettingId::SettingIdType  basedOnSettingId;

			switch ( CONSTANT_PARM_VALUE )
			{
				case ConstantParmValue::INSP_TIME_CONSTANT :
				case ConstantParmValue::EXP_TIME_CONSTANT :
					basedOnSettingId = SettingId::INSP_TIME;
					break;
				case ConstantParmValue::IE_RATIO_CONSTANT :
					basedOnSettingId = SettingId::IE_RATIO;
					break;
				default : 
					AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
					break;
			};

			const BoundedValue  CALC_EXP_TIME  =
				calcBasedOnSetting_(basedOnSettingId,
									BoundedRange::WARP_NEAREST,
									BASED_ON_ADJUSTED_VALUES);

			isValid = ::IsEquivalent(EXP_TIME_VALUE,
									 CALC_EXP_TIME.value,
									 CALC_EXP_TIME.precision);

			if ( !isValid )
			{
				// if not "equivalent" it may be because of round-off error, due
				// to the fact that this is being held at a resolution (via the
				// knob)...
				isValid =
					(EXP_TIME_VALUE >= (CALC_EXP_TIME.value - INTERVAL_LIST_.resolution)  &&
					 EXP_TIME_VALUE <= (CALC_EXP_TIME.value + INTERVAL_LIST_.resolution));
			}

			if ( !isValid )
			{
				const BoundedSetting*  pInspTime =
					SettingsMgr::GetBoundedSettingPtr(SettingId::INSP_TIME);
				const BoundedSetting*  pIeRatio =
					SettingsMgr::GetBoundedSettingPtr(SettingId::IE_RATIO);
				const BoundedSetting*  pRespRate =
					SettingsMgr::GetBoundedSettingPtr(SettingId::RESP_RATE);

				cout << "INVALID Expiratory Time:\n";
				cout << "CALC = " << CALC_EXP_TIME << endl;
				cout << *this << *pInspTime << *pIeRatio << *pRespRate << endl;
			}
		}
	}

	return(isValid);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ExpTimeSetting::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							EXP_TIME_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02119] -- this setting's dependent settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	ExpTimeSetting::calcDependentValues_(const Boolean isValueIncreasing)
{
	CALL_TRACE("calcDependentValues_(isValueIncreasing)");

	const Setting*  pMandType =
		SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

	Uint  idx = 0u;

	switch ( MAND_TYPE_VALUE )
	{
		case MandTypeValue::PCV_MAND_TYPE :
		case MandTypeValue::VCV_MAND_TYPE :	  // $[TI1]
			// all timing dependents are active here...
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			ArrDependentSettingIds_[idx++] = SettingId::IE_RATIO;
			break;
		case MandTypeValue::VCP_MAND_TYPE :	  // $[TI2]
			// all dependents are active here...
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			ArrDependentSettingIds_[idx++] = SettingId::IE_RATIO;
			ArrDependentSettingIds_[idx++] = SettingId::PEAK_INSP_FLOW;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
			break;
	}

	// terminate with the null id...
	ArrDependentSettingIds_[idx] = SettingId::NULL_SETTING_ID;

	// forward on to base class...
	return(Setting::calcDependentValues_(isValueIncreasing));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//
//  WARNING:  any new bounds that are added to this method may also affect
//            the 'calcDependentValues()' method of RespRateSetting.  Please
//            make sure that the latter method is checked.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ExpTimeSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	//===================================================================
	// determine expiratory time's maximum limit...
	//===================================================================

	const Real32  BREATH_PERIOD_BASED_MAX =
		(RespRateSetting::GetBreathPeriod(BASED_ON_ADJUSTED_VALUES) - 20.0f);

	BoundedRange::ConstraintInfo  maxConstraintInfo;

	// NOTE:   though this constraint will never be hit in a quiescent mode,
	//         it is possible with a LARGE knob delta to jump to an exp
	//         time value larger than this constraint, thereby messing
	//         up I:E ratio bound calculations; this prevents the screwed
	//         up I:E calculation scenario, and allows for I:E ratio to
	//         detect the bound violation, itself...
	maxConstraintInfo.id    = NULL_SETTING_BOUND_ID;
	maxConstraintInfo.value = BREATH_PERIOD_BASED_MAX;

	// this setting has no soft bounds...
	maxConstraintInfo.isSoft = FALSE;

	// update expiratory time's maximum constraint info...
	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnSettingId == SettingId::INSP_TIME  &&
//   (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//    basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//			||
//  ((basedOnSettingId == SettingId::RESP_RATE  ||
//    basedOnSettingId == SettingId::IE_RATIO  ||
//    basedOnSettingId == SettingId::TIDAL_VOLUME  ||
//    basedOnSettingId == SettingId::PEAK_INSP_FLOW  ||
//    basedOnSettingId == SettingId::PLATEAU_TIME  ||
//    basedOnSettingId == SettingId::FLOW_PATTERN)  &&
//    basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
	ExpTimeSetting::calcBasedOnSetting_(
									   const SettingId::SettingIdType basedOnSettingId,
									   const BoundedRange::WarpDir    warpDirection,
									   const BasedOnCategory          basedOnCategory
									   ) const
{
	CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

#if defined(SIGMA_DEVELOPMENT)
	if ( Settings_Validation::IsDebugOn(::EXP_TIME_SETTING) )
	{
		cout << "\nETS::cBOS bOSI==" << basedOnSettingId
			<< " wD==" << warpDirection
			<< " bOC==" << basedOnCategory
			<< endl;
	}
#endif  // defined(SIGMA_DEVELOPMENT)

	SettingId::SettingIdType  finalBasedOnSettingId;

	if ( basedOnSettingId == SettingId::RESP_RATE )
	{	// $[TI1] -- convert 'RESP_RATE' to "based-on" id...
		const DiscreteValue  CONSTANT_PARM_VALUE =
			ConstantParmSetting::GetValue(basedOnCategory);

		switch ( CONSTANT_PARM_VALUE )
		{
			case ConstantParmValue::INSP_TIME_CONSTANT :	// $[TI1.1]
				// respiratory rate is being changed while holding inspiratory time
				// constant, therefore calculate the new expiratory time from the
				// current inspiratory time...
				finalBasedOnSettingId = SettingId::INSP_TIME;
				break;
			case ConstantParmValue::IE_RATIO_CONSTANT :	   // $[TI1.2]
				// respiratory rate is being changed while holding I:E ratio
				// constant, therefore calculate the new expiratory time from the
				// current I:E ratio...
				finalBasedOnSettingId = SettingId::IE_RATIO;
				break;
			case ConstantParmValue::EXP_TIME_CONSTANT :
			case ConstantParmValue::TOTAL_CONSTANT_PARMS :
			default :
				// invalid value for 'CONSTANT_PARM_VALUE'...
				AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
				break;
		};
	}
	else
	{	// $[TI2]
		// use passed in parameter...
		finalBasedOnSettingId = basedOnSettingId;
	}

	// get the breath period from the respiratory rate...
	const Real32  BREATH_PERIOD_VALUE =
		RespRateSetting::GetBreathPeriod(basedOnCategory);

	BoundedValue  expTime;

	switch ( finalBasedOnSettingId )
	{
		//-------------------------------------------------------------------
		// $[02014](2) -- new exp. time while changing insp. time...
		// $[02018](2) -- new exp. time while keeping insp. time constant...
		// $[02163]    -- formula for calculating this setting's new-patient value...
		//-------------------------------------------------------------------
		case SettingId::INSP_TIME :
			{	// $[TI3] -- base expiratory time's calculation on inspiratory time...
				const BoundedSetting*  pInspTime =
					SettingsMgr::GetBoundedSettingPtr(SettingId::INSP_TIME);

				Real32  inspTimeValue;

				switch ( basedOnCategory )
				{
					case BASED_ON_NEW_PATIENT_VALUES :	  // $[TI3.1]
						// get the new-patient values of inspiratory time...
						inspTimeValue = BoundedValue(pInspTime->getNewPatientValue()).value;
						break;
					case BASED_ON_ADJUSTED_VALUES :		  // $[TI3.2]
						// get the "adjusted" values of inspiratory time...
						inspTimeValue = BoundedValue(pInspTime->getAdjustedValue()).value;
						break;
					case BASED_ON_ACCEPTED_VALUES :
						// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
						// get the "accepted" values of inspiratory time...
						inspTimeValue = BoundedValue(pInspTime->getAcceptedValue()).value;
						break;
#endif // defined(SIGMA_DEVELOPMENT)
					default :
						// unexpected 'basedOnCategory' value..
						AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
						break;
				};

				// calculate expiratory time...
				expTime.value = BREATH_PERIOD_VALUE - inspTimeValue;

#if defined(SIGMA_DEVELOPMENT)
				if ( Settings_Validation::IsDebugOn(::EXP_TIME_SETTING) )
				{
					cout << "ETS::cBOS BPV==" << BREATH_PERIOD_VALUE
						<< " iTV==" << inspTimeValue
						<< " eTv==" << expTime.value
						<< endl;
				}
#endif  // defined(SIGMA_DEVELOPMENT)

			}
			break;

			//-------------------------------------------------------------------
			// $[02013](2) -- new exp. time while changing I:E ratio...
			// $[02017](2) -- new exp. time while keeping I:E ratio constant...
			//-------------------------------------------------------------------
		case SettingId::IE_RATIO :
			{	// $[TI4] -- base expiratory time's calculation on I:E ratio...
				const Setting*  pIeRatio =
					SettingsMgr::GetSettingPtr(SettingId::IE_RATIO);

				Real32  ieRatioValue;

				switch ( basedOnCategory )
				{
					case BASED_ON_ADJUSTED_VALUES :
						// get the "adjusted" I:E ratio value...
						ieRatioValue = BoundedValue(pIeRatio->getAdjustedValue()).value;
						break;
					case BASED_ON_ACCEPTED_VALUES :
						// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
						// get the "accepted" I:E ratio value...
						ieRatioValue = BoundedValue(pIeRatio->getAcceptedValue()).value;
						break;
#endif // defined(SIGMA_DEVELOPMENT)
					case BASED_ON_NEW_PATIENT_VALUES :
					default :
						// unexpected 'basedOnCategory' value..
						AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
						break;
				};

				// calculate expiratory time from I:E ratio...
				Real32  eRatioValue;
				Real32  iRatioValue;

				if ( ieRatioValue < 0.0f )
				{	// $[TI4.1]
					// the I:E ratio value is negative, therefore we have 1:xx with
					// "xx" the expiratory portion of the ratio...
					iRatioValue = 1.0f;
					eRatioValue = -(ieRatioValue);
				}
				else
				{	// $[TI4.2]
					// the I:E ratio value is positive, therefore we have xx:1 with
					// "xx" the inspiratory portion of the ratio...
					iRatioValue = ieRatioValue;
					eRatioValue = 1.0f;
				}

				// calculate the expiratory time from the I:E ratio...
				expTime.value = (eRatioValue * BREATH_PERIOD_VALUE) /
								(iRatioValue + eRatioValue);
			}
			break;

			//-------------------------------------------------------------------
			// $[02019](2) -- new exp. time while changing VCV values...
			//-------------------------------------------------------------------
		case SettingId::TIDAL_VOLUME :
		case SettingId::PEAK_INSP_FLOW :
		case SettingId::PLATEAU_TIME :
		case SettingId::FLOW_PATTERN :
			{	// $[TI5] -- base exp time's calculation on the VCV settings...
				// expiratory time is to base its value on the "adjusted" VCV values,
				// only...
				AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
									basedOnCategory);

				// get pointers to each of the VCV parameters...
				const Setting*  pFlowPattern =
					SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
				const Setting*  pPeakInspFlow =
					SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW);
				const Setting*  pPlateauTime =
					SettingsMgr::GetSettingPtr(SettingId::PLATEAU_TIME);
				const Setting*  pTidalVolume =
					SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

				const DiscreteValue  FLOW_PATTERN_VALUE =
					pFlowPattern->getAdjustedValue();

				const Real32  PEAK_INSP_FLOW_VALUE =
					BoundedValue(pPeakInspFlow->getAdjustedValue()).value;
				const Real32  PLATEAU_TIME_VALUE =
					BoundedValue(pPlateauTime->getAdjustedValue()).value;
				const Real32  TIDAL_VOLUME_VALUE =
					BoundedValue(pTidalVolume->getAdjustedValue()).value;

				BoundedValue  inspTime;

				switch ( FLOW_PATTERN_VALUE )
				{
					case FlowPatternValue::SQUARE_FLOW_PATTERN : 
						{	// $[TI5.3] 
							static const Real32  SQUARE_FACTOR_ = (60000.0f * 0.001f);

							// $[02019]b -- formula for calculating inspiratory time from the
							//	          VCV parameters...
							inspTime.value = ((SQUARE_FACTOR_ * TIDAL_VOLUME_VALUE) /
											  PEAK_INSP_FLOW_VALUE);

#if defined(SIGMA_DEVELOPMENT)
							if ( Settings_Validation::IsDebugOn(::EXP_TIME_SETTING) )
							{
								cout << "ETS::cBOS "
									<< " SF==" << SQUARE_FACTOR_
									<< " TVV=" << TIDAL_VOLUME_VALUE
									<< " PIFV==" << PEAK_INSP_FLOW_VALUE
									<< " iTv==" << inspTime.value
									<< endl;
							}
#endif  // defined(SIGMA_DEVELOPMENT)

						}
						break;
					case FlowPatternValue::RAMP_FLOW_PATTERN :  
						{	// $[TI5.4] 
							static const Real32  RAMP_FACTOR_ = (60000.0f * 0.001f * 2.0f);

							const Setting*  pMinInspFlow =
								SettingsMgr::GetSettingPtr(SettingId::MIN_INSP_FLOW);

							const Real32  MIN_INSP_FLOW_VALUE =
								BoundedValue(pMinInspFlow->getAdjustedValue()).value;

							// $[02019]b -- formula for calculating inspiratory time from the
							//	          VCV parameters...
							inspTime.value = ((RAMP_FACTOR_ * TIDAL_VOLUME_VALUE) /
											  (PEAK_INSP_FLOW_VALUE + MIN_INSP_FLOW_VALUE));
						}
						break;
					default :
						AUX_CLASS_ASSERTION_FAILURE(FLOW_PATTERN_VALUE);
						break;
				};

				// $[02019]b -- add the plateau time...
				inspTime.value += PLATEAU_TIME_VALUE;

				BoundedSetting*  pInspTime =
					SettingsMgr::GetBoundedSettingPtr(SettingId::INSP_TIME);

				// because inspiratory time has a different resolution in VCV
				// than expiratory time, the "raw" insp time value must be rounded
				// to a resolution boundary, before calculating expiratory time...
				pInspTime->warpToRange(inspTime, warpDirection, FALSE);

				// calculate expiratory time...
				expTime.value = BREATH_PERIOD_VALUE - inspTime.value;

#if defined(SIGMA_DEVELOPMENT)
				if ( Settings_Validation::IsDebugOn(::EXP_TIME_SETTING) )
				{
					cout << "ETS::cBOS "
						<< " iTv==" << inspTime.value
						<< " BPV==" << BREATH_PERIOD_VALUE 
						<< " eTv==" << expTime.value
						<< endl;
				}
#endif  // defined(SIGMA_DEVELOPMENT)

			}
			break;

		default :
			// unexpected dependent setting id...
			AUX_CLASS_ASSERTION_FAILURE(finalBasedOnSettingId);
			break;
	};

	// place expiratory on a "click" boundary...
	getBoundedRange().warpValue(expTime, warpDirection, FALSE);

#if defined(SIGMA_DEVELOPMENT)
	if ( Settings_Validation::IsDebugOn(::EXP_TIME_SETTING) )
	{
		cout << "ETS::cBOS aft warp eTv==" << expTime.value << endl;
	}
#endif  // defined(SIGMA_DEVELOPMENT)

	return(expTime);
}
