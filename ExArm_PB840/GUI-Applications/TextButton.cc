#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TextButton - A button which displays a piece of text in its
// interior (in the button's Label container).
//---------------------------------------------------------------------
//@ Interface-Description
// This is a button derived directly from the GUI Foundation's Button
// class which provides functionality for displaying buttons with
// a string of text within.  The text, specified in the constructor,
// is positioned at 0, 0 in the button's "label container".
//---------------------------------------------------------------------
//@ Rationale
// Virtually all buttons used in Sigma display some sort of text so
// it made sense to create this class.
//---------------------------------------------------------------------
//@ Implementation-Description
// We simply create a TextField object and add it to the button's
// label container.  We then call the setTitleText() method of this class
// to display the text.
//---------------------------------------------------------------------
//@ Fault-Handling
// none
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TextButton.ccv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By:   rhj    Date: 07-July-2008      SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 008   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 007   By: rhj    Date:  13-Oct-2006   DCS Number: 6236
//  Project:  RESPM
//  Description:
//      Added a functionality to change the title text with an Align 
//      right position.
//
//  Revision: 006   By: gdc    Date:  2-Oct-2006   DCS Number: 6236
//  Project:  RESPM
//  Description:
//      Added push Button Type.
//
//  Revision: 005   By: rhj    Date:  20-April-2006   DCS Number: 6181
//  Project:  PAV4
//  Description:
//      Added a functionality to change the auxillary title text.
//
//  Revision: 004   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Modified constructor to take arguments for auxillary title text
//      and position, and initialize corresponding data members.  Also,
//      added new 'setAuxTitleShowFlag_()' method, and code to support
//      the displaying/erasing of the auxillary title text.
//
//  Revision: 003  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "TextButton.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TextButton  [Constructor]
//
//@ Interface-Description
// Creates a TextButton.  Passed the normal Button parameters along
// with a StringId for specifying the text of the button.  The
// parameter list is as follows:
// >Von
//	xOrg			The X co-ordinate of the button.
//	yOrg			The Y co-ordinate of the button.
//	width			Width of the button.
//	height			Height of the button.
//	buttonType		Type of the button, either LIGHT, LIGHT_NON_LIT or DARK.
//	bevelSize		Width of the button's internal border.
//	cornerType		Tells whether its corners are rounded or not, can be
//					ROUND or SQUARE.
//	borderType		The border type (whether it has an external border or not).
//	title			The string displayed as the button's title (text).
//>Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Pass most parameters on to the parent class, Button, initialize
// title text and add the text field, titleText_, to the label
// container.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TextButton::TextButton(Uint16 xOrg, 
					   Uint16 yOrg, 
					   Uint16 width, 
					   Uint16 height,
					   Button::ButtonType buttonType, 
					   Uint8 bevelSize,
					   Button::CornerType cornerType, 
					   Button::BorderType borderType,
					   StringId title, 
					   StringId auxTitleText, 
					   Gravity auxTitlePos,
					   Button::PushButtonType pushButtonType) :
    Button(xOrg, 
		   yOrg, 
		   width, 
		   height, 
		   buttonType, 
		   bevelSize, 
		   cornerType,
    	   borderType, 
		   Button::UP,
		   pushButtonType),
    titleText_(title),
    auxTitleText_(auxTitleText),
    AUX_TITLE_POS_(auxTitlePos)
{

    titlePos_ = GRAVITY_CENTER;

	// Add title text to button's label container and position it
	addLabel(&titleText_);		
	titleText_.positionInContainer(titlePos_);

	if (auxTitleText != ::NULL_STRING_ID)
	{	// $[TI1]
		// Add aux title text to button's label container and position it,
		// but don't show it, yet...
		addLabel(&auxTitleText_);		
		auxTitleText_.positionInContainer(AUX_TITLE_POS_);  // sets 'x' pos...
		auxTitleText_.setShow(FALSE);
	}	// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TextButton  [Destructor]
//
//@ Interface-Description
// Destroys a TextButton.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TextButton::~TextButton(void)
{
	CALL_TRACE("TextButton::~TextButton(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTitleText
//
//@ Interface-Description
// Changes the title string of a text button.  Passed the StringId of
// the new title.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls setText method of the private titleText_ to change its text
// and center it within its parent container.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextButton::setTitleText(StringId title, Gravity gravity)
{
	CALL_TRACE("TextButton::setTitleText(StringId title)");

	titleText_.setText(title);		
    titlePos_ = gravity;
	titleText_.positionInContainer(titlePos_);		

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAuxTitleText(StringId title)
//
//@ Interface-Description
//  Changes the aux title string of a text button.  Passed the StringId of
//  the new aux title.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Checks if the string is not empty before it calls the setText method 
//  to change the auxTitleText_ value.  
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TextButton::setAuxTitleText(StringId title)
{
    CALL_TRACE("TextButton::setAuxTitleText(StringId title)");

    if (title != ::NULL_STRING_ID)
    {   
        auxTitleText_.setText(title);
    }   

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTitleColor
//
//@ Interface-Description
// Changes the default color of the title text.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls setColor method of the private titleText_ to change the text
// color.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextButton::setTitleColor(Colors::ColorS color)
{
	CALL_TRACE("TextButton::setTitleColor(Colors::ColorS color)");

	titleText_.setColor(color);		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTitleHighlighted
//
//@ Interface-Description
// Changes the highlight attribute of the title text.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the setHighlighted method of the private titleText_ to highlight
// or unhighlight the text.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextButton::setTitleHighlighted(Boolean val)
{
	CALL_TRACE("setTitleHighlighted(Boolean)");

	titleText_.setHighlighted(val);		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTitleHighlightColor
//
//@ Interface-Description
// Changes the highlight color of the title text.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the setHighlightColor method of the private titleText_ to set
// its color when highlighted.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextButton::setTitleHighlightColor(Colors::ColorS color)
{
	CALL_TRACE("setTitleHighlightColor(Colors::ColorS color)");

	titleText_.setHighlightColor(color);		// $[TI1]
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
TextButton::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TEXTBUTTON,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAuxTitleShowFlag_
//
//@ Interface-Description
// Set the show flag of the auxillary title text, according to the passed-in
// boolean flag.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextButton::setAuxTitleShowFlag_(const Boolean auxShowFlag)
{
	CALL_TRACE("setAuxTitleShowFlag_()");

	if (auxTitleText_ != ::NULL_STRING_ID)
	{  // $[TI1]
		// show auxillary title based on 'auxShowFlag'...
		auxTitleText_.setShow(auxShowFlag);

		// start off the title text positioned directly in the center of the
		// button...
		titleText_.positionInContainer(titlePos_);

		if (auxShowFlag)
		{  // $[TI1.1]
			// need to show auxillary title text, therefore re-position the
			// title text to be centered in the remaining space...
			const Int  HALF_AUX_WIDTH = (auxTitleText_.getWidth() / 2);

			// initialize to current (centered) x position value...
			Int  newTitleX = titleText_.getX();

			if (AUX_TITLE_POS_ == ::GRAVITY_RIGHT)
			{  // $[TI1.1.1]
				// position title left of button center, to accomodate display
				// of auxillary title text on the RIGHT edge of the button...
				newTitleX -= HALF_AUX_WIDTH;

				// make sure that there is at least a one-pixel border spacing
				// from the left of the containter...
				newTitleX = MAX_VALUE(1, newTitleX);
			}
			else
			{  // $[TI1.1.2]
				// NOTE:  if 'GRAVITY_CENTER' is used for the auxillary title
				//        position -- which wouldn't make much sense -- it will
				//        be treated as 'GRAVITY_LEFT'...

				// position title right of button center, to accomodate display
				// of auxillary title text on the LEFT edge of the button...
				newTitleX += HALF_AUX_WIDTH;
			}
			
			titleText_.setX(newTitleX);
		}  // $[TI1.2]
	}  // $[TI2]
}

