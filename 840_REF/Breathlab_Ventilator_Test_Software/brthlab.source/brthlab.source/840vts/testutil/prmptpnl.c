/*****************************************************************************/
/*   Module Name:   prmptpnl.c                                               
/*   Purpose:       functions for managing the state of the test prompt panel.                                
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Removed the Metabolic Port Test.       Rhomere Jimenez   10 Oct,2007
/*
/*   Make button background color           Dave Richardson   20 Nov, 2002
/*   transparent
/*
/*   $Revision::   1.2      $                                                
/*                                                                          
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/testutil/prmptpnl.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:42   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   20 Nov 2002 16:14:28   Richardson
 * Updated per SCR # 24

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include "globals.h"
#include "common.h"   // VAL_OUR_ORANGE
#include "cpvtmain.h" // gPanelHandle
#include "ldb.h"
#include "mainpnls.h"
#include "prmptpnl.h"

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

static int AlarmPanel = -1;
static int CurrState  = Initial;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     PromptDisplay                                                  
/*                                                                           
/* Purpose:  Displays the prompt panel
/*                                          
/*                                                                           
/* Example Call:   int PromptDisplay(int panelhandle);                                    
/*                                                                           
/* Input:    panelhandle - handle of the prompt panel to display        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   result of DisplayPanel                                                         
/*                                                                           
/*****************************************************************************/
int PromptDisplay(int panelhandle)
{
	return DisplayPanel(panelhandle);
}

/*****************************************************************************/
/* Name:     PromptDiscard                                                
/*                                                                           
/* Purpose:  Discards the prompt panel
/*                                          
/*                                                                           
/* Example Call:   int PromptDiscard(int panelhandle);                                    
/*                                                                           
/* Input:    panelhandle - handle of the prompt panel to discard          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   result of DiscardPanel                                                         
/*                                                                           
/*****************************************************************************/
int PromptDiscard(int panelhandle)
{
	return DiscardPanel(panelhandle);
}

/*****************************************************************************/
/* Name:     TestPrompt                                                    
/*                                                                           
/* Purpose:  Sets up the prompt panel for a specific test based on the state arg
/*                                                                      
/* Example Call:   int TestPrompt(int State);                                    
/*                                                                           
/* Input:    int State - The state to set the panel to.      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   the handle of the prompt panel ready to display                                                         
/*                                                                           
/*****************************************************************************/
int TestPrompt(int State)
{
	int t,l,h,w; // Bounding Rectangle TESTPANEL: TESTPANEL_PROMPT_AREA
	int TBh;     // panel title height
	int cw;

	// AlarmPanel is in requested state  &  is not initializing
	if(CurrState == State && CurrState != Initial){
		return AlarmPanel;
	}

	// AlarmPanel NULL  &  is not initializing  &  is not being discarded
	if(AlarmPanel <= 0 && State != Initial && State != Discard){
		TestPrompt(Initial);
	}

	// AlarmPanel exists  &  is not clear  &  is not requested state
	if(AlarmPanel >= 0 && State != Clear && CurrState != State){
		TestPrompt(Clear);
	}

	switch(State){
		case (Initial):
			//Change the Alarm Panel into a generic prompt panel
			GetCtrlBoundingRect(gPanelHandle[TESTPANEL], TESTPANEL_PROMPT_AREA, &t, &l, &h, &w);
			AlarmPanel = LoadPanel(gPanelHandle[TESTPANEL], LDBf(Ldb_MainpanelFile), ALARMPANEL);
			SetPanelAttribute(AlarmPanel, ATTR_FRAME_STYLE,      VAL_HIDDEN_FRAME);
			SetPanelAttribute(AlarmPanel, ATTR_TITLE_BACKCOLOR,  VAL_OUR_ORANGE);
			SetPanelAttribute(AlarmPanel, ATTR_TITLE_FONT,       VAL_DIALOG_FONT);
			SetPanelAttribute(AlarmPanel, ATTR_TITLE_POINT_SIZE, 16);
			SetPanelAttribute(AlarmPanel, ATTR_TITLE_BOLD,       TRUE);
			GetPanelAttribute(AlarmPanel, ATTR_TITLEBAR_THICKNESS, &TBh);
			w = w -4;
			SetPanelSize(AlarmPanel, h-TBh-4, w);
			SetPanelPos( AlarmPanel, t+TBh+2, l+2);
			SetPanelAttribute(AlarmPanel, ATTR_ZPLANE_POSITION, 0);

			GetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX1,  ATTR_WIDTH, &cw);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX1,  ATTR_LEFT, (w/2)-(cw/2));

			GetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX2,  ATTR_WIDTH, &cw);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX2,  ATTR_LEFT, (w/2)-(cw/2));

			GetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX3,  ATTR_WIDTH, &cw);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX3,  ATTR_LEFT, (w/2)-(cw/2));

			GetCtrlAttribute(AlarmPanel, ALARMPANEL_DONE,       ATTR_WIDTH, &cw);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_DONE,       ATTR_LEFT, (w/2)-(cw/2));
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_DONE,       ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			GetCtrlAttribute(AlarmPanel, ALARMPANEL_YES,        ATTR_WIDTH, &cw);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_YES,        ATTR_LEFT, (w/4)-(cw/2));
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_YES,        ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			GetCtrlAttribute(AlarmPanel, ALARMPANEL_NO,         ATTR_WIDTH, &cw);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_NO,         ATTR_LEFT, ((w/4)*3)-(cw/2));
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_NO,         ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			GetCtrlAttribute(AlarmPanel, ALARMPANEL_DECORATION, ATTR_WIDTH, &cw);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_DECORATION, ATTR_LEFT, (w/2)-(cw/2));
			// NOTE: falls thru to case (Clear):, below
		case (Clear):
			HidePanel(AlarmPanel);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX1, ATTR_VISIBLE, FALSE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX2, ATTR_VISIBLE, FALSE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX3, ATTR_VISIBLE, FALSE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_DONE,      ATTR_VISIBLE, FALSE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_YES,       ATTR_VISIBLE, FALSE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_NO,        ATTR_VISIBLE, FALSE);
			SetPanelAttribute(AlarmPanel, ATTR_TITLE, "");
			ResetTextBox(AlarmPanel, ALARMPANEL_TEXT_BOX1, "");
			ResetTextBox(AlarmPanel, ALARMPANEL_TEXT_BOX2, "");
			ResetTextBox(AlarmPanel, ALARMPANEL_TEXT_BOX3, "");
			CurrState = Clear;
			break;

		case (Discard):
			if(AlarmPanel > 0){
				DiscardPanel(AlarmPanel);
				AlarmPanel = -1;
			}
			return 0;

		case (BDUAlarm):
			InsertTextBoxLine(AlarmPanel, ALARMPANEL_TEXT_BOX1, -1, LDBf(Ldb_BDU_Message));

			SetPanelAttribute(AlarmPanel, ATTR_TITLE, LDBf(Ldb_BDU_Title));

			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX1, ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_YES,       ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_NO,        ATTR_VISIBLE, TRUE);
			break;

		case (GUIAlarm):
			InsertTextBoxLine(AlarmPanel, ALARMPANEL_TEXT_BOX1, -1, LDBf(Ldb_GUI_Message));

			SetPanelAttribute(AlarmPanel, ATTR_TITLE, LDBf(Ldb_GUI_Title));

			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX1, ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_YES,       ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_NO,        ATTR_VISIBLE, TRUE);
			break;

		case (CPT1):
			SetPanelAttribute(AlarmPanel, ATTR_TITLE, LDBf(Ldb_Circ_Pres));

			InsertTextBoxLine(AlarmPanel, ALARMPANEL_TEXT_BOX2, -1, LDBf(Ldb_CP_Air_Msg));

			SetCtrlAttribute(AlarmPanel, ALARMPANEL_DONE,      ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX2, ATTR_VISIBLE, TRUE);
			break;

		case(CPT2):
			SetPanelAttribute(AlarmPanel, ATTR_TITLE, LDBf(Ldb_Circ_Pres));

			InsertTextBoxLine(AlarmPanel, ALARMPANEL_TEXT_BOX2, -1, LDBf(Ldb_CP_CIR_Msg));

			SetCtrlAttribute(AlarmPanel, ALARMPANEL_DONE,      ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX2, ATTR_VISIBLE, TRUE);
			break;

		case (SSTPanel):
			InsertTextBoxLine(AlarmPanel, ALARMPANEL_TEXT_BOX1, -1, LDBf(Ldb_SST_Msg_1));
#if 0
			InsertTextBoxLine(AlarmPanel, ALARMPANEL_TEXT_BOX2, -1, LDBf(Ldb_SST_Msg_2));
			InsertTextBoxLine(AlarmPanel, ALARMPANEL_TEXT_BOX3, -1, LDBf(Ldb_SST_Msg_3));
#endif
			SetPanelAttribute(AlarmPanel, ATTR_TITLE, LDBf(Ldb_SST_Title));

			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX1, ATTR_VISIBLE, TRUE);
#if 0
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX2, ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_TEXT_BOX3, ATTR_VISIBLE, TRUE);
#endif
			SetCtrlAttribute(AlarmPanel, ALARMPANEL_DONE,      ATTR_VISIBLE, TRUE);
			break;
	} // end switch(State)

	if(State != Clear){
		DisplayPanel(AlarmPanel);
		CurrState = State;
	}

	return AlarmPanel;
}
