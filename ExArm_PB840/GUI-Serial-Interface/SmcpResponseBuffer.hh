#ifndef	SmcpResponseBuffer_HH
# define SmcpResponseBuffer_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  SmcpResponseBuffer - SMCP Message Response Buffer
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SmcpResponseBuffer.hhv   25.0.4.0   19 Nov 2013 14:12:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003    By: rhj       Date: 05-Sept-2006  DR Number: 6169
//  Project: RESPM
//  Description:
//    Increase the buffer size to support the Alarm log downloads.
//
//  Revision: 002    By: quf       Date: 16-Aug-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//  Introduced encapsulated version of class IDs.
//
//  Revision: 001    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "SerialBuffer.hh"
#include "CodeLogEntry.hh"  // for MAX_CODE_LOG_ENTRIES

//@ Usage-Classes
//@ End-Usage

class MsgQueue;

class SmcpResponseBuffer : public SerialBuffer
{
  public:

	enum SmcpSize
	{
		 SMCP_RESPONSE_SIZE = 208  
		,SMCP_RESPONSE_COUNT = ::MAX_CODE_LOG_ENTRIES + 2
	};

	SmcpResponseBuffer(void);
	virtual ~SmcpResponseBuffer(void);

    static void  SoftFault(const SoftFaultID softFaultID,
                           const Uint32      lineNumber,
                           const char*       pFileName  = NULL,
                           const char*       pPredicate = NULL);

  protected:

  private:

	//@ Data-Member: packetMemory_
	//  Contains the SMCP message response packets.
	Uint packetMemory_[  SmcpResponseBuffer::SMCP_RESPONSE_SIZE 
	                   * SmcpResponseBuffer::SMCP_RESPONSE_COUNT / sizeof(Uint) ];

	//@ Data-Member: packetMap_
	//  Array containing pointers to each packet in packetMemory_.
	Uint * packetMap_[ SmcpResponseBuffer::SMCP_RESPONSE_COUNT ];

};

#endif	// ! SmcpResponseBuffer_HH
