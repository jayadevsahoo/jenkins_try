#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 2010, Covidien Ltd
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: BigSerialNumber - a big string that contains serial number information.
//---------------------------------------------------------------------
//@ Interface-Description
//		The serial number class contains a string of length 
//		PROX_MAX_REV_STR_SIZE. The default serial number is "XXXXXXXXXXXXX". 
//		Methods are provided to copy, compare, and get the string of 
// 		the serial number.
//---------------------------------------------------------------------
//@ Rationale
//		Abstraction of serial number.
//---------------------------------------------------------------------
//@ Implementation-Description
//		A default serial number of "XXXXXXXXXXXXX" can be constructed or 
// 		a serial number with any string. The serial number is stored as 
//      a char[] of length PROX_MAX_REV_STR_SIZE + 1 (NULL terminated).
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//  	None
//---------------------------------------------------------------------
//@ Invariants
//  	None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BigSerialNumber.ccv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: mnr    Date: 15-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Method header comments updated acc. to code review.
//
//  Revision: 002  By: mnr    Date: 30-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Init value string reduced to match max size length.
//
//  Revision: 001  By: mnr    Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Initial version checked in.
//
//=====================================================================

#include "BigSerialNumber.hh"
#include <string.h>

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BigSerialNumber
//
//@ Interface-Description
//  	Default constructor.  This method has no arguments and returns nothing.
//		This method will initialize the serial number to "XXXXXXXXXXXXX".
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize data_ to "XXXXXXXXXXXXX".
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

BigSerialNumber::BigSerialNumber()
{
    // $[TI1]
    strncpy( data_, "XXXXXXXXXXXXX           ", sizeof( data_)) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BigSerialNumber
//
//@ Interface-Description
//  	Constructor.  This method takes a pointer to a char[] as an argument
//		and returns nothing.  This method initializes the serial number
//		to the string passed in.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize data_ to the string passed in.  Null terminate string.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

BigSerialNumber::BigSerialNumber(const char * pString)
{
    // $[TI2]
    strncpy( data_, pString, sizeof( data_) - 1) ;
    data_[sizeof( data_) - 1] = '\0' ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BigSerialNumber
//
//@ Interface-Description
//  	Copy constructor.  This method has a BigSerialNumber as an argument
//		and returns nothing.  
//---------------------------------------------------------------------
//@ Implementation-Description
//		The BigSerialNumber passed in is copied to the	newly instantiated one.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

BigSerialNumber::BigSerialNumber(const BigSerialNumber& rBigSerialNumber)
{ 
    // $[TI3]
    strncpy( data_, rBigSerialNumber.data_, sizeof( data_)) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BigSerialNumber
//
//@ Interface-Description
//  	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		None
//---------------------------------------------------------------------
//@ PreCondition
//  	None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

BigSerialNumber::~BigSerialNumber(void)
{
	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//  	= operator.  This method has a BigSerialNumber as an argument and
//		returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The data members are set equal to the object passed in.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

BigSerialNumber &
BigSerialNumber::operator=(const BigSerialNumber& rBigSerialNumber)
{
	if (this != &rBigSerialNumber)
    {
        // $[TI1.1]
        strncpy( data_, rBigSerialNumber.data_, sizeof( data_));
    }	// implied else $[TI1.2] 

    return (*this);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator==
//
//@ Interface-Description
//  	== operator.  This method has a BigSerialNumber as an argument and
//		returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The BigSerialNumber passed in is compared against the current
//		BigSerialNumber.  If they are the same return true, else false.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

Boolean
BigSerialNumber::operator==(const BigSerialNumber& rBigSerialNumber) const
{
    // $[TI1.1] FALSE
    // $[TI1.2]	TRUE
  	return (strncmp( data_, rBigSerialNumber.data_, sizeof( data_)) ? FALSE : TRUE) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator!=
//
//@ Interface-Description
//  	!= operator.  This method has a BigSerialNumber as an argument and
//		returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The BigSerialNumber passed in is compared against the current
//		BigSerialNumber.  If they are not the same return true, else false.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

Boolean
BigSerialNumber::operator!=(const BigSerialNumber& rBigSerialNumber) const
{
    // $[TI1.1] FALSE
    // $[TI1.2]	TRUE
    return( !operator==(rBigSerialNumber)) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getString
//
//@ Interface-Description
//  	This method has no arguments and returns a const char * which
//		the serial number is stored.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns the data member data_.
//---------------------------------------------------------------------
//@ PreCondition
//  	None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================
const char *
BigSerialNumber::getString(void) const
{
    // $[TI1]
    return data_;
}

#ifdef SIGMA_DEVELOPMENT

ostream&
operator<< (ostream& theStream, const BigSerialNumber& rNumber)
{
    theStream << rNumber.data_;
    
    return (theStream) ;
}

#endif // SIGMA_DEVELOPMENT

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator[]
//
//@ Interface-Description
//  	[] operator.  This method has index as an argument and returns the
//		character that the index points to.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The index is checked for bounds and the data_ that the index points
//		to is returned.
//---------------------------------------------------------------------
//@ PreCondition
//		index < PROX_MAX_REV_STR_SIZE
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================
 
char
BigSerialNumber::operator[](const Uint index) const
{
    CLASS_ASSERTION( index < ProxiInterface::PROX_MAX_REV_STR_SIZE) ;

    // $[TI1]
	return( data_[index]) ;
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BigSerialNumber::SoftFault(const SoftFaultID  softFaultID,
                      const Uint32       lineNumber,
                      const char*        pFileName,
                      const char*        pPredicate)
{
  FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES, BIGSERIALNUMBER, 
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

