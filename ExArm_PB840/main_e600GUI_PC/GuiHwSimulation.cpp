#include "stdafx.h"
#ifdef SIGMA_GUIPC_CPU
//------------------------------------------------------------------------------
//                   Copyright (c) 2014 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: GuiHwSimulation.cpp  - Implementation File which handles GUI IO HW Simulations
//---------------------------------------------------------------------
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------



#include "ExArm.h"
#include "GuiFoundation.hh"
#include "GuiHwSimulation.h"
#include "IpcIds.hh"
#include "MsgQueue.hh"
#include "Sound.hh"



extern CExArmApp theApp;

//For Painting LED's
CDC* ptrGuiHwSimulationViewCDC = NULL;
// The pens and brushes needed to do the Red Led drawing
CPen m_RedLedPenBright,m_RedLedPenDark;
CBrush m_RedLedBrushBright,m_RedLedBrushDark;
// The pens and brushes needed to do the Yellow Led drawing
CPen m_YellowLedPenBright,m_YellowLedPenDark;
CBrush m_YellowLedBrushBright,m_YellowLedBrushDark;

const static Uint8 numberOfSimulatedLeds = 8;
static CRect redLeds[numberOfSimulatedLeds];
static CRect yellowLeds[numberOfSimulatedLeds];

extern Boolean KeyInputAllowed;
extern Boolean KnobAcknowledged_;
extern Int32 KnobDelta_;

Int32 GUIHWSIMULATION_SCREEN_HEIGHT_PIXELS = 942;
Int32 GUIHWSIMULATION_SCREEN_WIDTH_PIXELS = 165;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// GuiHwSimulationFrm - Start
////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(GuiHwSimulationFrm, CFrameWnd)

BEGIN_MESSAGE_MAP(GuiHwSimulationFrm, CFrameWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()

// GuiHwSimulationFrm construction/destruction

GuiHwSimulationFrm::GuiHwSimulationFrm()
{
	// TODO: add member initialization code here
}

GuiHwSimulationFrm::~GuiHwSimulationFrm()
{
}

int GuiHwSimulationFrm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

BOOL GuiHwSimulationFrm::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	//  Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	cs.cx = GUIHWSIMULATION_SCREEN_WIDTH_PIXELS;
	cs.cy = GUIHWSIMULATION_SCREEN_HEIGHT_PIXELS;
	cs.x = GuiFoundation::SCREEN_WIDTH_PIXELS + 22;
	cs.y =0;
	cs.style = WS_OVERLAPPED ;
	
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);

	return TRUE;
}

// GuiHwSimulationFrm diagnostics

#ifdef _DEBUG
void GuiHwSimulationFrm::AssertValid() const
{
	CFrameWnd::AssertValid();
}

#ifndef _WIN32_WCE
void GuiHwSimulationFrm::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif

#endif //_DEBUG

// GuiHwSimulationFrm message handlers

BOOL GuiHwSimulationFrm::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// define the 2 views sizes
	CSize pane1(GUIHWSIMULATION_SCREEN_WIDTH_PIXELS , GUIHWSIMULATION_SCREEN_HEIGHT_PIXELS);
 
	// create the 2 splitters
	m_bSplitterCreated = m_ObjSplitterUper_Lower.CreateStatic(this,2, 1,WS_CHILD|WS_VISIBLE); 

	// associate the classes to the views
	m_ObjSplitterUper_Lower.CreateView(0, 0, RUNTIME_CLASS(GuiHwSimulationView), pane1,pContext);
	m_ObjSplitterUper_Lower.CreateView(1, 0, RUNTIME_CLASS(GuiHwSimulationView),pane1, pContext);

	//return m_bSplitterCreated;
	return CFrameWnd::OnCreateClient(lpcs, pContext);
}
 

////////////////////////////////////////////////////////////
// GuiHwSimulationFrm - End
////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// GuiHwSimulationView - Start
////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(GuiHwSimulationView, CView)

BEGIN_MESSAGE_MAP(GuiHwSimulationView, CView)
END_MESSAGE_MAP()


GuiHwSimulationView::GuiHwSimulationView()
{

}

GuiHwSimulationView::~GuiHwSimulationView()
{
	delete ptrGuiHwSimulationViewCDC;
}

void GuiHwSimulationView::OnInitialUpdate()
{
	//Using KeysTask global variable for queue flow control 
	KeyInputAllowed = TRUE;

	//All of the simulated hardware buttons are given its text and a DlgCtrlID(GuiHwSimulatedKey).
	//Each of these buttons's DlgCtrlID maps to software buttons array located in KeyPanel::KeyId	KeyMapPass[].
	m_Button[0].Create(L"Accept", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY1);
	m_Button[1].Create(L"Clear", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this,CMyButton::GuiHwSimulatedKey::GUI_KEY2);
	m_Button[2].Create(L"Home", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY3);
	m_Button[3].Create(L"Manual Breath", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY4);
	m_Button[4].Create(L"Insp Pause", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY5);
	m_Button[5].Create(L"Exp Pause", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY6);
	m_Button[6].Create(L"ALarm Reset", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY7);
	m_Button[7].Create(L"ALarm SiLence", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY8);
	m_Button[8].Create(L"100% O2", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY9);
	m_Button[9].Create(L"Info", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY10);
	m_Button[10].Create(L"ALarm VoLume", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY11);
	m_Button[11].Create(L"Screen Brightness", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY12);
	m_Button[12].Create(L"Screen Contrast", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY13);
	m_Button[13].Create(L"Screen Lock", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY14);
	m_Button[14].Create(L"Spare Key", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY15);

	//Knob Buttons
	m_Button[15].Create(L"Knob Up", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY16);
	m_Button[16].Create(L"Knob Down", WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(0, 0, 0, 0), this, CMyButton::GuiHwSimulatedKey::GUI_KEY17);


	Int32 y = 50;
	for(Int32 i = 0; i< CMyButton::GuiHwSimulatedKey::NUMBER_OF_GUI_SIMULATED_KEYS; i++)
	{
		//Place buttons on respective positions on the view layout
		m_Button[i].MoveWindow(0, y, 155, 50);
		y = y + 50;
	}

	//LED Simulation
	if(ptrGuiHwSimulationViewCDC == NULL)
	{
		ptrGuiHwSimulationViewCDC = new CClientDC(this);

		Uint8 x1 = 1;
		Uint8 y1 = 0;
		Uint8 x2 = 20;
		Uint8 y2 = 22;

		for(Uint8 i=0; i < numberOfSimulatedLeds ; i++)
		{
			redLeds[i].SetRect(x1,y1,x2,y2);
			x1 += 20;
			x2 += 20;
		}

		x1 = 1;
		y1 = 29;
		x2 = 20;
		y2 = 48;

		for(Uint8 i=0; i < numberOfSimulatedLeds ; i++)
		{
			yellowLeds[i].SetRect(x1,y1,x2,y2);
			x1 += 20;
			x2 += 20;
		}


		// I'm creating a light shade of red here for displaying the bright
		// LED. You can change the values to any colour that you want
		m_RedLedPenBright.DeleteObject();
		m_RedLedBrushBright.DeleteObject();
		m_RedLedPenBright.CreatePen(0,1,RGB(250,0,0));
		m_RedLedBrushBright.CreateSolidBrush(RGB(250,0,0));

		// Here i'm creating a dark shade of red. You can play with the values to
		// see the effect on the LED control

		m_RedLedPenDark.DeleteObject();
		m_RedLedBrushDark.DeleteObject();
		m_RedLedPenDark.CreatePen(0,1,RGB(102,0,0));
		m_RedLedBrushDark.CreateSolidBrush(RGB(102,0,0));

		// I'm creating a light shade of yellow here for displaying the bright
		// LED. You can change the values to any colour that you want
		m_YellowLedPenBright.DeleteObject();
		m_YellowLedBrushBright.DeleteObject();
		m_YellowLedPenBright.CreatePen(0,1,RGB(255,255 ,0));
		m_YellowLedBrushBright.CreateSolidBrush(RGB(255,255,0));

		// Here i'm creating a dark shade of yellow. You can play with the values to
		// see the effect on the LED control
		m_YellowLedPenDark.DeleteObject();
		m_YellowLedBrushDark.DeleteObject();
		m_YellowLedPenDark.CreatePen(0,1,RGB(150,150,0));
		m_YellowLedBrushDark.CreateSolidBrush(RGB(150,150,0));


		ptrGuiHwSimulationViewCDC->SelectObject(&m_RedLedPenDark);
		ptrGuiHwSimulationViewCDC->SelectObject(&m_RedLedBrushDark);

		for(Uint8 i=0; i < numberOfSimulatedLeds ; i++)
		{
			// Draw the actual colour of the LED
			ptrGuiHwSimulationViewCDC->Ellipse(redLeds[i]);
		}

		ptrGuiHwSimulationViewCDC->SelectObject(&m_YellowLedPenDark);
		ptrGuiHwSimulationViewCDC->SelectObject(&m_YellowLedBrushDark);

		for(Uint8 i=0; i < numberOfSimulatedLeds ; i++)
		{
			// Draw the actual colour of the LED
			ptrGuiHwSimulationViewCDC->Ellipse(yellowLeds[i]);
		}
	}

	CView::OnInitialUpdate();
}

// CLowerBaseContainerView drawing

void GuiHwSimulationView::OnDraw(CDC* pDC)
{
		
		//create and select a solid blue brush
		CBrush brushBlue(RGB(0, 0, 255));
		CBrush* pOldBrush = pDC->SelectObject(&brushBlue);

		// create and select a thick, black pen
		CPen penBlack;
		penBlack.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
		CPen* pOldPen = pDC->SelectObject(&penBlack);

		// get our LED client rectangle area
		CRect rectForRedLed(0,0,300,25);
		CRect rectForyellowLed(0,28,300,49);

		// draw a thick black rectangle filled with blue
		pDC->Rectangle(rectForRedLed);
		pDC->Rectangle(rectForyellowLed);

		// put back the objects
		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);

	CView::OnDraw(pDC);
}

void GuiHwSimulationView::SetAlarmLedColor(VgaBlink::AlarmLedColors LedColor)
{
    switch (LedColor)
    {

        case VgaBlink::ALARM_LED_COLOR_RED:
           SetRedLed(ALARM_LED_ON);
            break;

        case VgaBlink::ALARM_LED_COLOR_YELLOW:
           SetYellowLed(ALARM_LED_ON);
            break;

		case VgaBlink::ALARM_LED_COLOR_RED_OFF:
           SetRedLed(ALARM_LED_OFF);
            break;

		case VgaBlink::ALARM_LED_COLOR_YELLOW_OFF:
           SetYellowLed(ALARM_LED_OFF);
            break;

		case VgaBlink::ALARM_LED_COLOR_OFF:
			SetYellowLed(ALARM_LED_OFF);
			SetRedLed(ALARM_LED_OFF);
			break;
    }
}

void GuiHwSimulationView::SetYellowLed(LedStatus_t State)
{
	switch (State)
	{
		case LedStatus_t::ALARM_LED_OFF:
			// If we have to switch off the LED to display the dark colour select
			// the bright pen and brush that we have created above
			ptrGuiHwSimulationViewCDC->SelectObject(&m_YellowLedPenDark);
			ptrGuiHwSimulationViewCDC->SelectObject(&m_YellowLedBrushDark);
		break;

		case LedStatus_t::ALARM_LED_ON:
			// If we have to switch on the LED to display the bright colour select
			// the bright pen and brush that we have created above
			ptrGuiHwSimulationViewCDC->SelectObject(&m_YellowLedPenBright);
			ptrGuiHwSimulationViewCDC->SelectObject(&m_YellowLedBrushBright);
		break;
		default:
		break;
	}

	for(Uint8 i=0; i < numberOfSimulatedLeds ; i++)
	{
		// Draw the actual colour of the LED
		 ptrGuiHwSimulationViewCDC->Ellipse(yellowLeds[i]);
	}
}
void GuiHwSimulationView::SetRedLed(LedStatus_t State)
{
	switch (State)
	{
		case LedStatus_t::ALARM_LED_OFF:
			// If we have to switch off the LED to display the dark colour select
			// the bright pen and brush that we have created above
			ptrGuiHwSimulationViewCDC->SelectObject(&m_RedLedPenDark);
			ptrGuiHwSimulationViewCDC->SelectObject(&m_RedLedBrushDark);
		break;

		case LedStatus_t::ALARM_LED_ON:
			// If we have to switch on the LED to display the bright colour select
			// the bright pen and brush that we have created above
			ptrGuiHwSimulationViewCDC->SelectObject(&m_RedLedPenBright);
			ptrGuiHwSimulationViewCDC->SelectObject(&m_RedLedBrushBright);
		break;
		default:
		break;
	}

	for(Uint8 i=0; i < numberOfSimulatedLeds ; i++)
	{
		// Draw the actual colour of the LED
		ptrGuiHwSimulationViewCDC->Ellipse(redLeds[i]);
	}
}

////////////////////////////////////////////////////////////
// GuiHwSimulationView - End
////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// CMyButton - Start
////////////////////////////////////////////////////////////

CMyButton::CMyButton()
{
}

CMyButton::~CMyButton()
{
}

BEGIN_MESSAGE_MAP(CMyButton, CButton)
	//{{AFX_MSG_MAP(CMyButton)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyClass message handlers

void CMyButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	CDC dc;
	dc.Attach(lpDrawItemStruct->hDC);		//Get device context object
	CRect rt;
	rt = lpDrawItemStruct->rcItem;		//Get button rect

	dc.FillSolidRect(rt, RGB(0, 0, 255));		//Fill button with blue color

	UINT state = lpDrawItemStruct->itemState;	//Get state of the button
	if ( (state & ODS_SELECTED) )		// If it is pressed
	{
		dc.DrawEdge(rt,EDGE_SUNKEN,BF_RECT);    // Draw a sunken face
	}
	else
	{
		dc.DrawEdge(rt,EDGE_RAISED,BF_RECT);	// Draw a raised face
	}
	dc.SetTextColor(RGB(255,255,120));		// Set the color of the caption to be yellow
	CString strTemp;
	GetWindowText(strTemp);		// Get the caption which have been set
	dc.DrawText(strTemp,rt,DT_CENTER|DT_VCENTER|DT_SINGLELINE);		// Draw out the caption
	if ( (state & ODS_FOCUS ) )       // If the button is focused
	{
		// Draw a focus rect which indicates the user 
		// that the button is focused
		Int32 iChange = 3;
		rt.top += iChange;
		rt.left += iChange;
		rt.right -= iChange;
		rt.bottom -= iChange;
		dc.DrawFocusRect(rt);
	}
	dc.Detach();
}

void CMyButton::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(GetDlgCtrlID() == GUI_KEY16 || GetDlgCtrlID() == GUI_KEY17)
	{
		//Knob Button
		if(KnobAcknowledged_)
		{
			if(GetDlgCtrlID() == GUI_KEY16)
			{
				KnobDelta_ = 1;
			}
			else //if(GetDlgCtrlID() == GUI_KEY17)
			{
				KnobDelta_ = -1;
			}
			PostKnob();
			KnobAcknowledged_ = FALSE;
			//Knob Sound
			Sound::Start(Sound::KNOB);
		}
	}
	else if(KeyInputAllowed)
	{
		// make a key press sound
		Sound::KeyPressSound(GuiKeys(GetDlgCtrlID()));
		PostKey((UserAnnunciationMsg::KeyAction)UserAnnunciationMsg::KEY_PRESS, GuiKeys(GetDlgCtrlID()));
		KeyInputAllowed = false;
	}
	CButton::OnLButtonDown(nFlags, point);
}

void CMyButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(GetDlgCtrlID() != GUI_KEY16 && GetDlgCtrlID() != GUI_KEY17)
	{
		// optionally, make a key release sound
		Sound::KeyReleaseSound(GuiKeys(GetDlgCtrlID()));
		PostKey((UserAnnunciationMsg::KeyAction)UserAnnunciationMsg::KEY_RELEASE, GuiKeys(GetDlgCtrlID()));
	}
	CButton::OnLButtonUp(nFlags, point);
}

void CMyButton::PostKnob(void)
{
    //$[TI1]
    UserAnnunciationMsg knobMsg;			// appl message
    MsgQueue	userInputQueue(USER_ANNUNCIATION_Q);		// appl queue

    knobMsg.qWord = 0;
    knobMsg.keyParts.eventType = UserAnnunciationMsg::INPUT_FROM_KNOB;

    userInputQueue.putMsg((Uint32)knobMsg.qWord);
                //$[TI2]
}

void CMyButton::PostKey(UserAnnunciationMsg::KeyAction action, GuiKeys key)
{
	UserAnnunciationMsg keyMsg;			// appl message
	MsgQueue	userInputQueue(USER_ANNUNCIATION_Q);		// appl queue

	keyMsg.qWord = 0;
	keyMsg.keyParts.eventType = UserAnnunciationMsg::INPUT_FROM_KEY;
	keyMsg.keyParts.action = action;
	keyMsg.keyParts.keyCode = key;

	userInputQueue.putMsg((Uint32)keyMsg.qWord);
							//$[TI1]
}

////////////////////////////////////////////////////////////
// CMyButton - End
////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif
