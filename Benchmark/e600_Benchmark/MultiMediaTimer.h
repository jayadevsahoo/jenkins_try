#pragma once
#include <MMSystem.h>
//
// This class is a wrapper for the multi-media timer, the default resolution is 10ms
//
class MultiMediaTimer
{
private:
	UINT				Period_;
	vector<MMRESULT> 	Timers_;

public:
	MultiMediaTimer(UINT nPeriod = 10);
	~MultiMediaTimer(void);

	MMRESULT setEvent( UINT uDelay, LPTIMECALLBACK fptc, DWORD_PTR dwUser );
	MMRESULT killEvent( UINT nTimerId );
};
