#ifndef UpperOtherScreensSubScreen_HH
#define UpperOtherScreensSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: UpperOtherScreensSubScreen - Allows access to the Diagnostic Code,
// Operational Time, Ventilator Configuration, SST Results and Test Summary
// subscreens.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/UpperOtherScreensSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  btray	   Date:  21-Jul-1999    DCS Number: 5424 
//  Project: Neonatal 
//  Description:
//		Initial Neonatal version. Removed development option button
//		from normal mode.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"

//@ Usage-Classes
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
class SubScreenArea;
//@ End-Usage

class UpperOtherScreensSubScreen : public SubScreen
{
public:
	UpperOtherScreensSubScreen(SubScreenArea* pSubScreenArea);
	~UpperOtherScreensSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	virtual void buttonDownHappened(Button* pButton,
									Boolean isByOperatorAction);

	static void SoftFault(const SoftFaultID softFaultID,
							     const Uint32      lineNumber,
							     const char*       pFileName  = NULL, 
							     const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	UpperOtherScreensSubScreen(void);								// not implemented
	UpperOtherScreensSubScreen(const UpperOtherScreensSubScreen&);	// not implemented...
	void operator=(const UpperOtherScreensSubScreen&);				// not implemented...

	//@ Data-Member: diagnosticCodeButton_
	// Activates the DiagnosticCodeLogSubScreen.
	TextButton diagnosticCodeButton_;

	//@ Data-Member: operationalTimeButton_
	// Activates the OperationalTimeSubScreen.
	TextButton operationalTimeButton_;

	//@ Data-Member: ventConfigButton_
	// Activates the VentConfigSubScreen.
	TextButton ventConfigButton_;

	//@ Data-Member: sstResultsButton_
	// Activates the SstResultsLogSubScreen.
	TextButton sstResultsButton_;

	//@ Data-Member: testSummaryButton_
	// Activates the VentTestSummarySubScreen.
	TextButton testSummaryButton_;

	//@ Data-Member: titleArea_
	// Title banner for this subscreen.
	SubScreenTitleArea titleArea_;

};

#endif // UpperOtherScreensSubScreen_HH 
