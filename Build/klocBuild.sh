#!/bin/bash

URL_ROOT="http://taboud-ap09v:8080"
KW_ADMIN=kwadmin.exe
PROJ_PARSER=kwvcprojparser.exe
KW_BUILD=kwbuildproject.exe

function buildKloc()
{
    projectFile=$1
    database=$2
    url=$URL_ROOT/$database

    #Temporary files and folders
    kwTablesFolder=kwtables
    kwBuildSpec=target.klocspec

    #Clean up intermediate files
    rm -rf $kwTablesFolder
    rm -f $kwBuildSpec
    
    #Create klocwork build specification. We are going to do
    #static analysis on the release build all the time. It is necessary
    #to create the build specification everytime, because Klocwork extracts all
    #project settings for its build and analysis
    $PROJ_PARSER $projectFile -c "Release|OMAP_CE7_SDK (ARMv4I)"  --output $kwBuildSpec

    #Build the project through Klocwork and load the results into the 
    #server database.
    $KW_BUILD --url $url --tables-directory $kwTablesFolder $kwBuildSpec
    $KW_ADMIN --url $URL_ROOT load $database $kwTablesFolder

    #Clean up the folders
    rm -rf $kwTablesFolder
    rm -f $kwBuildSpec
}


#Start the action!
export PATH=$PATH:"/cygdrive/c/Klocwork/Server\ 9.6/bin/"

#Look for the visual studio project files and identify which project (BD or GUI) we are
#building. The KW build settings are different for each project, and the results go 
#into different databases on the server
if [ -f main_e600gascontrol.vcproj ]; then
    buildKloc main_e600gascontrol.vcproj PB880-BD
elif [ -f main_e600GUI.vcproj ]; then
    buildKloc main_e600GUI.vcproj PB880-GUI
else
    echo "Script does not support a klocwork build on this folder."
    echo "Only supported folders are ExArm_PB840/main_e600gascontrol and ExArm_PB840/main_e600GUI"
fi


