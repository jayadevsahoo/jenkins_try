#ifndef BdGuiCommSync_HH
#define BdGuiCommSync_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BdGuiCommSync - implements the synchronizing of alarm and
//  ventilator status when Communications is restored between BD and GUI
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdGuiCommSync.hhv   25.0.4.0   19 Nov 2013 13:59:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  26-Feb-1997    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  kam    Date:  23-Oct-1995    DR Number:DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial version
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

// Usage-Classes
class GuiReadyMessage;
class CommDownMessage;
// End-Usage


class BdGuiCommSync
{
  public:
    // Type: CommState
    // This enum identifies the state of communication of alarm and vent
    // status events to Gui
    enum CommState
    {
        COMMS_DOWN,         // Communication between BD and GUI not available
        COMMS_UP            // Communication active and alarms are being sent
    };
  
    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

    static void GuiReadyCallback (const GuiReadyMessage& rMessage);
    static void CommDownCallback (const CommDownMessage& rMessage);


  protected:


  private:
    BdGuiCommSync(void);                      // not implemented...
    ~BdGuiCommSync(void);                     // not implemented...
    BdGuiCommSync (const BdGuiCommSync&);     // not implemented...
    void operator= (const BdGuiCommSync&);    // not implemented...
};


#endif  //BdGuiCommSync_HH
