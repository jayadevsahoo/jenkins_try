#include "stdafx.h"
#if		defined(SIGMA_CLIENT)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FlashMem - Defines interface to entire Flash Memory
//      of a Sigma board for download purpose.  
//---------------------------------------------------------------------
//@ Interface-Description
//		Provides a Flash Memory burner class which accepts the
//		download data block to its data list and writes out data
//	    to Flash Memory in a sequentially ascending order.
//      The class initializes and builds the flash memory checksum
//		table for the POST. Flash Memory is divided into POST checksum
//		blocks.  As the data is burnt into the flash memory,
//      the task builds a checksum for each flash memory block.
//		These checksum tables are checked against a downloaded checksum
//		table of the downloaded image. The class provides an interface to
//	    reconstruct a POST checksum table based on the entire flash memory
//		contents after a new boot image is burnt into the flash memory.
//		The Service-Mode Calibration data space within the flash memory
//		and POST checksum tables are not included in the POST checksum
//	    table.
//	    After a successful flash memory burning, the task goes into
// 	    in a infinite wait loop until the system gets rebooted.
//---------------------------------------------------------------------
//@ Rationale
//     Encapsulates all the intelligence and data that are required to
//     burn a download image to the Sigma flash memory successfully
//     within a class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions  and pre-conditions are used to
//  ensure correctness of software conditons.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/FlashMem.ccv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 007  By: Rhomere Jimenez  Date: 22-June-2006  DR Number: 6192
//   Project:   Xena
//   Description:
//      Added the capability to be backwards compatible with the pre-cost and
//      Cost reduced boards.
//
//  Revision: 006  By: rhj    Date:  11-April-2006    DR Number: 6192
//  Project:  XENA
//  Description:
//      Added support for the XENA Spansion Flash memory.
//      These changes only supports the new XENA GUI board not
//      the older GUI or BD boards.
//
//  Revision: 005  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//--Break in Revision numbers--
//
//  Revision: 001  By:  <jhv>    Date:  08-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//  Revision: 002  By:  hct      Date:  25-APR-1997  DR Number: 1996
//       Project:  Sigma (R8027)
//       Description:
//             Added requirement number 00538.
//
//  Revision: 003  By:  hct      Date:  28-APR-1997  DR Number: 1996
//       Project:  Sigma (R8027)
//       Description:
//             Added requirement number 08044.
//
//  Revision: 004  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh and MemoryMap.hh
//
//=====================================================================

#include "FlashMem.hh"
#include "Download.hh"
#include "TftpSocket.hh"
#include "Queue.hh"
#include "OsUtil.hh"
extern  void EnableFlashWrite(void);
extern  void DisableFlashWrite(void);

#define MAX_NUM_CAL_DATA_BLOCK  0x4000 // 16K

Uint32 CalibrationDataBlock_[MAX_NUM_CAL_DATA_BLOCK ]; // Calibration Data block

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlashMem(void)  [Default Constructor]
//
//@ Interface-Description
//		Constructs a FlashMem object. It initializes the data block
//	    list to be empty. Initializes the check sum table for a
//		download boot image.  Computes the starting address and ending
//		address of the Flash Memory to be downloaded onto. It also
//      computes block length of individual checksum block.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 A fixed number of checksum table entries are allocated regardless
//   of the flash memory size. Divide the entire flash memory size
//	 by the number of check sum table entries defined, you will come
//	 up with individual flash block length that each checksum entry
//	 will compute its checksum for. The beginning part of the first
//   flash memory block is allocated to the service data and the POST
//	 check table area. These areas are excluded from the checksum
//   monitoring.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
FlashMem::FlashMem(void)
{
	writeAddr_ = startAddr_ =  (Uint32 *)FLASH_CHECKSUM_BASE;
	if ( ::BoardType() == BD_BOARD ) // $[TI1]
	{
		endAddr_  = (Uint32 *)FLASH_BASE + BD_FLASH_MEM_SIZE/sizeof(Uint32) -1;
	}
	else // $[TI2]
	{
		endAddr_ = (Uint32 *)FLASH_BASE + GUI_FLASH_MEM_SIZE/sizeof(Uint32) -1;
	}
	pCurrentBlock_ = NULL;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FlashMem(void)  [Destructor]
//
//@ Interface-Description
//   Default Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Do nothing.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

FlashMem::~FlashMem(void)
{
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: writeBlockToFlash_(NetworkBuf *pBuf) 
//
//@ Interface-Description
//	  Burn the flash memory with a given block of data in a sequentially
//    ascending order. The method returns a success status value when
//    all the data in a given block is successfully written out to the
//	  flash memory.  The method returns a finish status when it writes
//	  out to the last flash memory address. An error status is returned
//    when write to flash memory fails.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		CLASS_PRE_CONDITION( pBuf)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
DownloadError
FlashMem::writeBlockToFlash_(NetworkBuf *pBuf)
{
int i, nWord;
DownloadError rCode;
char *pBlockEndAddr;

	CLASS_PRE_CONDITION(pBuf);
	CLASS_ASSERTION(pCurrentBlock_);

	nWord = pBuf->data_len/sizeof(Int);
	pBlockEndAddr =
			(char *)pCurrentBlock_->pStartAddress + pCurrentBlock_->length;
	for(i = 0; i< nWord; i++, writeAddr_++) // $[TI1]
	{

		if ( writeAddr_ > endImageAddr_ ) // $[TI1.1]
		{
			return(FLASH_BURNING_DONE);
		} // else $[TI1.2]

		if ( (char *) writeAddr_ >=  pBlockEndAddr ) // $[TI1.3]
		{
			pCurrentBlock_++;
			pBlockEndAddr = (char *)pCurrentBlock_->pStartAddress +
					pCurrentBlock_->length;
		} // else $[TI1.4]

        //printf("writeAddr_ -> 0x%x; pBuf->data[%d] -> %d  \n", (Uint32 *) writeAddr_, i,(Uint32) pBuf->data[i]);

        
        if ((rCode =  hardWare_.burnFlashWord( (Uint32 *) writeAddr_,
					(Uint32) pBuf->data[i])) != NO_ERROR ) // $[TI1.5]
		{
			return(rCode); // FLASH_MEMORY_WRITE_FAILED
		} // else $[TI1.6]

		if ( writeAddr_ >= pCurrentBlock_->pStartAddress ) // $[TI1.7]
		{
			pCurrentBlock_->additiveChecksum += (Int32 )pBuf->data[i];
		} // else $[TI1.8]
	} // $[TI2]

	if ( writeAddr_ > endImageAddr_ ) // $[TI3]
	{
		return(FLASH_BURNING_DONE);
	}
	else // $[TI4]
	{
		return(NO_ERROR);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: matchCheckSumTbl( EepromStruct *hostCheckSum ) 
//
//@ Interface-Description
//	  Compares a passed in checksum table against the object's
//	  checksum table. If they are equal, the method returns a TRUE
//	  value, otherwise the method returns a FALSE value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Only the number of checksum entries that are specified in
//    the object's checksum table is checked.
//---------------------------------------------------------------------
//@ PreCondition
//	( hostCheckSum && hostCheckSum->checksumBlock[0].length )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
FlashMem::matchCheckSumTbl(EepromStruct *hostCheckSum)
{

#if	defined(SIGMA_PRODUCTION)
	CLASS_PRE_CONDITION(hostCheckSum && hostCheckSum->checksumBlock[0].length );
#endif //defined(SIGMA_PRODUCTION)

    for (int i=0; i < checkSumTable_.numChecksums ; i++) // $[TI1]
   	{

      //  if ( i < NUMBER_OF_CHECKSUM_BLOCKS)
      //  {
       		if ( checkSumTable_.checksumBlock[i].additiveChecksum !=
       			hostCheckSum->checksumBlock[i].additiveChecksum ) // $[TI1.1]
    		{
    			return(FALSE);
    		} // else $[TI1.2]
       // }
 /*       else
        {
            Uint8 index = ( i - NUMBER_OF_CHECKSUM_BLOCKS);
       		if ( checkSumTable_.checksumBlockXena[index].additiveChecksum !=
       			hostCheckSum->checksumBlockXena[index].additiveChecksum ) // $[TI1.1]
    		{
    			return(FALSE);
    		} // else $[TI1.2]

        }
        */

	} // else $[TI2]
	return(TRUE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reInit( EepromStruct *pHostTbl ) 
//
//@ Interface-Description
//		Reinitializes flash memory checksum table and other
//	    instance variables to be able to restart a download operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Reset the checksum value for each Flash memory block to
//      their initial value, zero. 
//		The flash memory address to write and the current checksum
//      table entry is set to the beginning of the flash memory.
//---------------------------------------------------------------------
//@ PreCondition
//    (pHostTbl)
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
FlashMem::reInit( EepromStruct *pHostTbl ) 
{
int i;

	CLASS_PRE_CONDITION(pHostTbl);

	printf("Host checksum table\n");
	printf(" image Start Addr %X endAddr %X\n", startAddr_,
		pHostTbl->endImageAddr);

    hostCheckSumTbl_ = *pHostTbl;
	checkSumTable_ = *pHostTbl;

	for(i=0; i< pHostTbl-> numChecksums; i++) // $[TI1]
	{
      //  if(i < NUMBER_OF_CHECKSUM_BLOCKS)
      //  {
            checkSumTable_.checksumBlock[i].additiveChecksum = 0;
            printf("%d block startaddr %X length %X checksum %X\n",i,
            pHostTbl->checksumBlock[i].pStartAddress,
            pHostTbl->checksumBlock[i].length,
            pHostTbl->checksumBlock[i].additiveChecksum);

    /*    }
        else
        {
            Uint8 index = ( i - NUMBER_OF_CHECKSUM_BLOCKS);
            checkSumTable_.checksumBlockXena[index].additiveChecksum = 0;
    #if defined(SIGMA_DEBUG)
            printf("%d block startaddr %X length %X checksum %X\n",i,
            pHostTbl->checksumBlockXena[index].pStartAddress,
            pHostTbl->checksumBlockXena[index].length,
            pHostTbl->checksumBlockXena[index].additiveChecksum);
    #endif //defined(SIGMA_DEBUG)


        }
*/
			
	} // $[TI2]
	 
	writeAddr_ = startAddr_;
	pCurrentBlock_ = &checkSumTable_.checksumBlock[0];
	endImageAddr_ = pHostTbl->endImageAddr;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: completeNetDataChkSumTbl(void) 
//
//@ Interface-Description
//	  Complete the checksum table of the downloaded data.
//	  In order to speed up the download time, the ending part of
//    of the flash memory that are just filled with a flash memory
//    FILL_PATTERN are not actually downloaded since the flash
//    memory are set to the fill patterns when it was erased.
//    But the host checksum tables are pre-built based on these
//    fill characters getting filled with the FILL pattern.
//    Therefore, we need to complete the checksum table entries that
//    cover the portion of the memory that are actually written with
//    downloaded data.
//    
//---------------------------------------------------------------------
//@ Implementation-Description
//    We need to finish  up only the block that actually contains 
//    some downloaded data and ending fill characters. The checksum
//	  values for the previous blocks are computed while each download
//    data is written to the flash memory. We do not have to compute
//    ending blocks that only contains those fill characters.
//    
//---------------------------------------------------------------------
//@ PreCondition
//		none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlashMem::completeNetDataChkSumTbl(void)
{
register Uint32 *pAddr, *pEnd;
ChecksumStruct *pBlock;
	
    for (int i=0; i < checkSumTable_.numChecksums; i++) // $[TI1]
   	{

      //  if(i < NUMBER_OF_CHECKSUM_BLOCKS)
      //  {
        
       		pBlock = &checkSumTable_.checksumBlock[i];
     		pAddr = pBlock->pStartAddress;	
    		pEnd = pAddr + pBlock->length/sizeof(Int32);
    		if ( (pBlock->pStartAddress < checkSumTable_.endImageAddr)
    			&& ( pEnd > checkSumTable_.endImageAddr)) // $[TI1.1]
    		{
    			for(pAddr = checkSumTable_.endImageAddr+1; pAddr < pEnd; pAddr++) // $[TI1.1.1]
    			{
    				pBlock->additiveChecksum  +=  (Uint32) FLASH_FILL_WORD;
    			} // $[TI1.1.2]
    			checkSumTable_.numChecksums = i;
    			printf("complete block i %d startAddr %X length %X checksum %X\n",i,
    				pBlock->pStartAddress,pBlock->length,pBlock->additiveChecksum );
    			return;
    		} // else $[TI1.2]
    		printf("CompleteNetDataChkSumTbl block i %d startAddr %X length %X checksum %X\n",i,
    			pBlock->pStartAddress,pBlock->length,
    				pBlock->additiveChecksum);
      /*  }
        else
        {

            Uint8 index = ( i - NUMBER_OF_CHECKSUM_BLOCKS);
       		pBlock = &checkSumTable_.checksumBlockXena[index];
     		pAddr = pBlock->pStartAddress;	
    		pEnd = pAddr + pBlock->length/sizeof(Int32);
    		if ( (pBlock->pStartAddress < checkSumTable_.endImageAddr)
    			&& ( pEnd > checkSumTable_.endImageAddr)) // $[TI1.1]
    		{
    			for(pAddr = checkSumTable_.endImageAddr+1; pAddr < pEnd; pAddr++) // $[TI1.1.1]
    			{
    				pBlock->additiveChecksum  +=  (Uint32) FLASH_FILL_WORD;
    			} // $[TI1.1.2]
    			checkSumTable_.numChecksums = i;
    #if	defined(SIGMA_DEBUG)
    			printf("complete block i %d startAddr %X length %X checksum %X\n",i,
    				pBlock->pStartAddress,pBlock->length,pBlock->additiveChecksum );
    #endif //defined(SIGMA_DEBUG)
    			return;
    		} // else $[TI1.2]
    #if	defined(SIGMA_DEBUG)
    		printf("CompleteNetDataChkSumTbl block i %d startAddr %X length %X checksum %X\n",i,
    			pBlock->pStartAddress,pBlock->length,
    				pBlock->additiveChecksum);
    #endif //defined(SIGMA_DEBUG)

        }
*/


	} // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buildChkSumFromFlashMem(void) 
//
//@ Interface-Description
//	  Rebuild a flash memory check sum table based on the flash
//	  memory contents.
//---------------------------------------------------------------------
//@ Implementation-Description
//	   The service data block and and the POST checksum table area
//	   in the flash memory are excluded from checksum calculation.
//---------------------------------------------------------------------
//@ PreCondition
//		none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlashMem::buildChkSumFromFlashMem(void)
{
register Uint32 *pAddr, *pEnd;
register Uint32 addNum;
ChecksumStruct *pBlock;
	
    for (int i=0; i < hostCheckSumTbl_.numChecksums; i++) // $[TI1]
   	{
     //   if (i < NUMBER_OF_CHECKSUM_BLOCKS)
     //   {
        
    		pBlock = &checkSumTable_.checksumBlock[i];
     		pAddr = pBlock->pStartAddress;	
    		pEnd = pAddr + pBlock->length/sizeof(Int32);
    		CLASS_ASSERTION(!(pBlock->length%64));
    
    		addNum = 0;
    		for( pAddr = pBlock->pStartAddress; pAddr < pEnd; pAddr++) // $[TI1.1]
    		{
    			addNum += *pAddr;
    		} // $[TI1.2]
    		pBlock->additiveChecksum = addNum;
    		printf("block i %d startAddr %X length %d checksum %X\n",i,
    			pBlock->pStartAddress, pBlock->length
    			, pBlock->additiveChecksum );
     /*   }
        else
        {
            Uint8 index = ( i - NUMBER_OF_CHECKSUM_BLOCKS);
    		pBlock = &checkSumTable_.checksumBlockXena[index];
     		pAddr = pBlock->pStartAddress;	
    		pEnd = pAddr + pBlock->length/sizeof(Int32);
    		CLASS_ASSERTION(!(pBlock->length%64));
    
    		addNum = 0;
    		for( pAddr = pBlock->pStartAddress; pAddr < pEnd; pAddr++) // $[TI1.1]
    		{
    			addNum += *pAddr;
    		} // $[TI1.2]
    		pBlock->additiveChecksum = addNum;
    #if	defined(SIGMA_DEBUG)
    		printf("block i %d startAddr %X length %d checksum %X\n",i,
    			pBlock->pStartAddress, pBlock->length
    			, pBlock->additiveChecksum );
    #endif //defined(SIGMA_DEBUG)

        }
      */
	} // $[TI2]

	checkSumTable_.numChecksums = hostCheckSumTbl_.numChecksums;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: task(void) 
//
//@ Interface-Description
//	 The main task routine for the flash memory burner. It gets the
//   down loaded boot file data from the data block list and writes the
//	 data one word at a time to the flash memory. The boot image is
//   written to a flash memory in an sequential ascending address order.
//   The task  builds a 32 bit checksum for each flash memory block. 
//   The task expects that the TFTP client task down loads a full
//   image of the flash memory. When the task writes out to the last
//   flash memory successfully. It sends out a message to the
//   the main download task on the Download::RFlashMemQueue message queue
//   to indicate that flash memory burning is done.
//   If it encounters errors, it reports the error and generates an
//   appropriate failure error messages to the common queue.
//---------------------------------------------------------------------
//@ Implementation-Description
//		After a data block is written to flash memory,  the buffer
//		needs to be freed.
//		Each time a block of data is successfully written
//	    to flash memory, the writeBlockToFlash_ methods returns the 
//		'SUCCESSFUL' status, except when it detects a completion 
//		of the flash memory burning. 
//		The task continually tries to get a data block from the 
//	    data block list until the writeBlockToFlash_ method returns
//		a flash memory burning completion status. 
//		Flash memory cannot be written unless its contents are erased
//		first. It takes a long time to erase the entire flash memory.
//		The eraseFlashMem method internally delays the flash burner
//		task while it is waiting for flash memory to be erased.
//		When the flash burning process doesn't finish successfully,
//	    the task erases the entire flash memory so the download task
//	    can re-start a download process.
//	$[08044]
//		
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlashMem::task(void)
{
NetworkBuf *pBuf;
Uint32 errCode;
Int retry;
Boolean firstTime;
Boolean forever = TRUE; 

	for (retry = 0; retry < DOWNLOAD_COMM_RETRY; retry++)
	{

		firstTime = TRUE;
		errCode = NO_ERROR;
		while(  errCode == NO_ERROR  ) // $[TI1]
		{

			while(!(pBuf = TftpSocket::RBufQueue.getDataBlock())) // $[TI1.1]
			{
				Download::DelayTask(DATA_BLOCK_WAIT_INTERVAL);
#if defined(SIGMA_UNIT_TEST)
				return;
#endif //defined(SIGMA_UNIT_TEST)
			} // $[TI1.2]

			if ( firstTime ) // $[TI1.3]
			{
                printf("IsXenaConfig() = %d\n",  IsXenaConfig());
				::EnableFlashWrite();
				::sc_delay(1);       // set up time for flash write enable
				for (int i=0; i<10000; i++)
				{
					;  // set up time
				}

				firstTime = FALSE;
                     printf("Erasing Flash...\n");
                // Erase the entire flash chip before writing data
                if ( errCode = hardWare_.entireFlashErase()) // $[TI1.3.1]
                {
                    TftpSocket::RBufQueue.freeBuf(pBuf);
                    continue;
                } 
                     printf("Done.\n");
			} // else $[TI1.4]

            printf("Now writing to Flash...\n");
			errCode = writeBlockToFlash_(pBuf);
            printf("errCode = %d, NO_ERROR = %d \n", errCode, NO_ERROR);
			TftpSocket::RBufQueue.freeBuf(pBuf);
		} // $[TI2]
		::DisableFlashWrite();
		if ( errCode == FLASH_BURNING_DONE ) // $[TI3]
		{
                  printf("Complete! Now executing completNetDataChkSumTbl...\n");
			completeNetDataChkSumTbl();
                  printf("Done. Now executing matchCheckSumTbl...\n");
    		if ( matchCheckSumTbl(&hostCheckSumTbl_) != TRUE ) // $[TI3.1]
			{
				Download::RFlashMemQueue.putMsg(DOWNLOAD_DATA_CORRUPTION);
				Download::DelayTask(ONE_SECOND);
				continue;
			} // else $[TI3.2]
                 printf("Finished! executing buildChkSumFromFlashMem!\n");
			buildChkSumFromFlashMem();
                 printf("Done. Now executing matchCheckSumTbl\n");
    		if ( matchCheckSumTbl(&hostCheckSumTbl_) != TRUE ) // $[TI3.3]
			{
				Download::RFlashMemQueue.putMsg(FLASH_CHECKSUM_FAILURE );
				Download::DelayTask(ONE_SECOND);
				continue;
			} // else $[TI3.4]
                 printf("Complete! initSerialNumber()...\n");
			if ( (errCode= initSerialNumber()) != NO_ERROR ) // $[TI3.5]
			{
				Download::RFlashMemQueue.putMsg(errCode);
				Download::DelayTask(ONE_SECOND);
				continue;
			} // else $[TI3.6]
                 printf("Download Complete!!!\n");
	printf("FlashMem::task FLASH_BURNING_DONE\n");
			Download::RFlashMemQueue.putMsg(FLASH_BURNING_DONE);
			break;
		} // else $[TI4]
		Download::RFlashMemQueue.putMsg(errCode);
	} // no [TI] needed

	while(forever)
	{
		Download::DelayTask(DATA_BLOCK_WAIT_INTERVAL);
#ifdef SIGMA_UNIT_TEST
		break;
#endif
	} // no [TI] needed
}
#endif		//defined(SIGMA_CLIENT)

#if		defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task(void) 
//
//@ Interface-Description
//		VRTX OS entrance into the Flash memory burner task.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Kick off an instance method, "task" method on the download 
//		class's Flash Memory Manager object.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlashMem::Task(void)
{
	// $[TI1]
	Download::RFlashMemBurner.task();
}
#endif		//defined(SIGMA_CLIENT)


#if		defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initSerialNumber(void) 
//
//@ Interface-Description
//		Initialize the serial number in the service data (calibration)
//      flash memory block to its initial pattern.
//---------------------------------------------------------------------
//@ Implementation-Description
//	   The initial value of the serial number is all 10 'X' characters.
//	   Since the serial number resides within a same flash memory block
//     as the service calibration data, we need to read in the entire
//     flash memory block into the RAM memory first in order to erase
//     the flash memory and write out to the serial number area.
//	   A addictive checksum number is computed as the flash memory
//     block is read into the RAM memory. The three words are excluded
//    from the checksumming since these three words are initialized
//    with a fresh Serial number initial pattern.
//    $[00538]
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
DownloadError
FlashMem::initSerialNumber(void)
{
Uint32 *pTo, *pFrom;
Uint32 *endAddr;
Uint i, size;
DownloadError errCode;
#if	defined(SIGMA_DEVELOPMENT)
Uint32 checkSum1 = 0, checkSum2 = 0;
#endif //defined(SIGMA_DEVELOPMENT)

	pFrom = (Uint32 *) FLASH_SERIAL_NO_ADDR ;
	pTo = CalibrationDataBlock_ ;

	for(size=0; size< SERIAL_NO_LENGTH; size++)
	{
		pTo[size] = SERIAL_NO_INIT_PATTERN ;
	} // no [TI] needed

#if	defined(SIGMA_DEVELOPMENT)
    for( ;size < FlashProgrammer::GetSubBlockSize(0); size++)
	{
		checkSum1 += pFrom[size];
		pTo[size] = pFrom[size];
	} // no [TI] needed cause SIGMA_DEVELOPMENT
#endif //defined(SIGMA_DEVELOPMENT)

    endAddr=(Uint32 *)(FLASH_SERIAL_NO_ADDR+MAX_NUM_CAL_DATA_BLOCK-1); 

    ::EnableFlashWrite();
	::sc_delay(1);      // set up time for flash write enable

    // if XENA
    // Must Backup Checksum table before erasing the serial number data.
    hardWare_.BackupCheckSumTable();

	if ((errCode = hardWare_.eraseFlashMem((Uint32 *)FLASH_SERIAL_NO_ADDR,endAddr))
			!= NO_ERROR ) // $[TI1]
	{
		return(errCode);
	} // else $[TI2]

    // if XENA
    // Must restore the checksum table since it has been erased.
    if(((errCode = hardWare_.RestoreCheckSumTable()) != NO_ERROR))
    {
        return(errCode);
    }

	pTo =(Uint32 *) FLASH_SERIAL_NO_ADDR ;
	pFrom = CalibrationDataBlock_ ;
	for( i= 0; i < size; i++ )
	{
		if ((errCode = hardWare_.burnFlashWord( pTo++, pFrom[i]))
					!= NO_ERROR ) // $[TI3]
		{
			::DisableFlashWrite();
			return(errCode);
		} // else $[TI4]
	} // no [TI] needed cause size = SERIAL_NO_LENGTH
	::DisableFlashWrite();

#if	defined(SIGMA_DEVELOPMENT)
	pFrom = (Uint32 *) FLASH_SERIAL_NO_ADDR;
    for( i= SERIAL_NO_LENGTH; i < FlashProgrammer::GetSubBlockSize(0); i++)
	{
		checkSum2 += pFrom[i];
	} // no [TI] needed cause SIGMA_DEVELOPMENT
	if ( checkSum1 != checkSum2 )
	{
		return(SERVICE_DATA_CHECKSUM_FAILURE);
	} // no [TI] needed cause SIGMA_DEVELOPMENT
#endif //defined(SIGMA_DEVELOPMENT)

	return(NO_ERROR);
}


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setNumChecksums(void)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlashMem::setNumChecksums(Int32 num)
{

    checkSumTable_.numChecksums = num;
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHostNumChecksums(void)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlashMem::setHostNumChecksums(Int32 num)
{

    hostCheckSumTbl_.numChecksums = num;
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setpStartAddress(void)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlashMem::setpStartAddress(void)
{

    checkSumTable_.checksumBlock[0].pStartAddress = (Uint32 *)FLASH_SERIAL_NO_ADDR;
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setEndImageAddr(void)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlashMem::setEndImageAddr(void)
{

    checkSumTable_.endImageAddr = (Uint32 *)(FLASH_SERIAL_NO_ADDR+4);
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setLength(Uint32 num)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlashMem::setLength(Uint32 num)
{

    checkSumTable_.checksumBlock[0].length = num;
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAdditiveChecksum(Uint32 num)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlashMem::setAdditiveChecksum(Uint32 num)
{

    checkSumTable_.checksumBlock[0].additiveChecksum = num;
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: callWriteBlockToFlash(NetworkBuf *pBuf)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
DownloadError
FlashMem::callWriteBlockToFlash(NetworkBuf *pBuf)
{

	return writeBlockToFlash_(pBuf);
}
#endif // SIGMA_UNIT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
FlashMem::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
           const char*        pFileName,
           const char*        pPredicate)
{
    DownloadFault::SoftFault(softFaultID, DOWNLOAD,
    FLASH_MEM_CLASS, lineNumber, pFileName, pPredicate);
}

#endif		//defined(SIGMA_CLIENT)
