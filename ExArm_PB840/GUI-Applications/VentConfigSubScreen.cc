#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VentConfigSubScreen - The screen selected by pressing the
// Ventilator Configuration button on the Upper Other Screens Subscreen.
// Allows user to view the current ventilator hardware/software configuration.
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VentConfigSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 023  By:  mnr    Date:  14-Jun-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      SRS tracing number updated.
//
//  Revision: 022  By:  mnr    Date:  25-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Yet another major layout change to accomodate PROX config info.
//
//  Revision: 021  By:  mnr    Date:  28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Major layout changes to accomodate new options and create more
//		room for future options.
//
//  Revision: 020  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 019  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 018   By: rhj    Date: 20-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//  Added Leak Comp option.
// 
//  Revision: 017  By: gdc		Date: 26-May-2007   SCR Number:  6330
//  Project:  Trend
//  Description:
//  Removed SUN prototype code.
//
//  Revision: 016  By: rhj		Date: 02-Oct-2006   SCR Number:  6237
//  Project:  RESPM
//  Description:
//  Added PAV and Trending options.
//
//  Revision: 015  By: gdc		Date: 16-May-2006   SCR Number:  6236
//  Project:  RESPM
//  Description:
//  Added RM option.
//
//  Revision: 014  By: rhj		Date: 05-May-2006   SCR Number:  6153
//  Project:  RESPM
//  Description:
//  Revert back to rev 9.1 to enable the PAV+ option for the English US Version.
//
//  Revision: 013  By: rhj		Date: 17-Jan-2005   SCR Number:  
//  Project:  NIV
//  Description:
//  Added a check to disable the PAV+ option for the ENGLISH US Version
//
//  Revision: 012  By: jja		Date: 23-May-2002   DCS Number:  6010 
//  Project:  VCP
//  Description:
//      Fixed option display following GuiComm/VC+ merge.
//
//  Revision: 011  By: gdc		Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 010  By: sah    Date: 17-Jul-2000   DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added display of VTPC option state
//      *  added "default" initialization of option info pointers to 'NULL',
//         and corrected checking of the NULL pointers to be based on the
//         fields rather than the always-non-NULL option info pointer
//
//  Revision: 009   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added display of PAV option state
//
//  Revision: 008  By: sah		Date: 08-Mar-2000   DCS Number:  5683
//  Project:  NeoMode
//  Description:
//      Re-implemented to calculate value offsets at run-time.  This allows
//      for foreign languages to be automatically formatted.
//
//  Revision: 007  By: sah    Date: 27-Jul-1999   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Improvements made for display of data in Ventilator Configuration Subscreen. 
//
//  Revision: 006  By:  healey	   Date:  08-Feb-1999    DCS Number: 5322
//  Project:  ATC
//  Description:
//		Modified the configuration code label.
//
//  Revision: 005  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 004  By: syw      Date: 27-Oct-1998  DR Number: 5215
//    Project:  BiLevel
//    Description:
//		Changed GetConfigurationCode to GetVentilatorOptions.
//		Changed configurationCodeLabel_ to ventilatorOptionsLabel_.
//	  	Changed CONFIGURATION_CODE_LABEL to VENTILATOR_OPTIONS_LABEL
//
//  Revision: 003  By:  syw	   Date:  20-Aug-1998    DCS Number: 
//  Project:  Color
//  Description:
//		BiLevel Initial version.  Added configurationCodeLabel_.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  07-NOV-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "VentConfigSubScreen.hh"

#include <string.h>			// FORNOW
//@ Usage-Classes
#include "Post.hh"
#include "MiscStrs.hh"
#include "UpperSubScreenArea.hh"
#include "SoftwareRevNum.hh"
#include "Image.hh"
#include "SoftwareOptions.hh"
#include "GuiApp.hh"
#include "BigSerialNumber.hh"
#include "StringConverter.hh"  
//@ End-Usage

//@ Code...
static const Int32 VERTICAL_DIVIDING_LINE_X1_POINT_ = 335;
static const Uint8 VERTICAL_DIVIDING_LINE_Y1_POINT_ = 0;
static const Int32 VERTICAL_DIVIDING_LINE_X2_POINT_ = 335;
static const Int32 VERTICAL_DIVIDING_LINE_Y2_POINT_ = 276;
static const Uint8 HORIZ_DIVIDING_LINE_1_X1_POINT_ = 0;
static const Int32 HORIZ_DIVIDING_LINE_1_Y1_POINT_ = 125;
static const Int32 HORIZ_DIVIDING_LINE_1_X2_POINT_ = 335;
static const Int32 HORIZ_DIVIDING_LINE_1_Y2_POINT_ = 125;
static const Uint8 HORIZ_DIVIDING_LINE_2_X1_POINT_ = 0;
static const Int32 HORIZ_DIVIDING_LINE_2_Y1_POINT_ = 215;
static const Int32 HORIZ_DIVIDING_LINE_2_X2_POINT_ = 640;
static const Int32 HORIZ_DIVIDING_LINE_2_Y2_POINT_ = 215;

static const Int32 GUI_SYMBOL_LABEL_X_POSITION_ = 155;
static const Int32 GUI_SYMBOL_LABEL_Y_POSITION_ = 37;
static const Int32 BD_SYMBOL_LABEL_X_POSITION_ = 155; //455;
static const Int32 BD_SYMBOL_LABEL_Y_POSITION_ = 130; //GUI_SYMBOL_LABEL_Y_POSITION_;
static const Int32 SOFTWARE_OPTIONS_TITLE_X_ = 340;
static const Int32 SOFTWARE_OPTIONS_TITLE_Y_ = 37;
static const Int32 SOFTWARE_OPTIONS_OFFSET_X_ = 110;
static const Int32 SOFTWARE_OPTIONS_OFFSET_Y_ = 19;
static const Int32 SOFTWARE_OPTIONS_BOX_WIDTH_ = 15;
static const Int32 SOFTWARE_OPTIONS_BOX_HEIGHT_ = 15;
static const Int32 MAX_OPTIONS_PER_COLUMN_ = 8; //total max 22, 2 columns of 11 each

static Int  SwPostMaxValueOffset_;	// offset for Software and POST values...
static Int  SerialNoMaxValueOffset_;	// offset for Serial Number value...
static Int  ProxMaxValueOffset_;   // offset for PROX Serial Number and Revision values...

static wchar_t  ConfigString_[250];  


struct OptionInfo_
{
	Boolean		isShown;
	TextField*  pLabel;
	Box*        pBox;
	Bitmap*     pBitmap;
};

static OptionInfo_  ArrOptionInfo_[SoftwareOptions::NUM_OPTIONS];


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VentConfigSubScreen()  [Constructor]
//
//@ Interface-Description
//	Constructs a VentConfigSubScreen.  It is passed a pointer to the 
//	SubScreenArea.	
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Initializes data members; sets position, size and color of the upper
//	subscreen area; 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VentConfigSubScreen::VentConfigSubScreen(SubScreenArea* pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleArea_(MiscStrs::VENT_CONFIG_SUBSCREEN_TITLE, SubScreenTitleArea::SSTA_CENTER),
	verticalDividingLine_(VERTICAL_DIVIDING_LINE_X1_POINT_, VERTICAL_DIVIDING_LINE_Y1_POINT_,
			      VERTICAL_DIVIDING_LINE_X2_POINT_, VERTICAL_DIVIDING_LINE_Y2_POINT_), 
	horizontalDividingLine1_(HORIZ_DIVIDING_LINE_1_X1_POINT_, HORIZ_DIVIDING_LINE_1_Y1_POINT_, 
			        HORIZ_DIVIDING_LINE_1_X2_POINT_, HORIZ_DIVIDING_LINE_1_Y2_POINT_),
	horizontalDividingLine2_(HORIZ_DIVIDING_LINE_2_X1_POINT_, HORIZ_DIVIDING_LINE_2_Y1_POINT_, 
					HORIZ_DIVIDING_LINE_2_X2_POINT_, HORIZ_DIVIDING_LINE_2_Y2_POINT_),
	softwareOptionsLabel_(MiscStrs::SOFTWARE_OPTIONS_LABEL),
	softwareOptionBiLevelLabel_(MiscStrs::SOFTWARE_OPTION_BILEVEL_LABEL),
	softwareOptionTCLabel_(MiscStrs::SOFTWARE_OPTION_TC_LABEL),
	softwareOptionNeoModeLabel_(MiscStrs::SOFTWARE_OPTION_NEOMODE_LABEL),
	softwareOptionVtpcLabel_(MiscStrs::SOFTWARE_OPTION_VTPC_LABEL),
	softwareOptionPavLabel_(MiscStrs::SOFTWARE_OPTION_PAV_LABEL),
	softwareOptionRespMechLabel_(MiscStrs::SOFTWARE_OPTION_RESP_MECH_LABEL),
	softwareOptionTrendingLabel_(MiscStrs::SOFTWARE_OPTION_TRENDING_LABEL),
	softwareOptionLeakCompLabel_(MiscStrs::SOFTWARE_OPTION_LEAK_COMP_LABEL),
	softwareOptionNeoModeUpdateLabel_(MiscStrs::SOFTWARE_OPTION_NEOMODE_UPDATE_LABEL),
	softwareOptionNeoModeAdvancedLabel_(MiscStrs::SOFTWARE_OPTION_NEOMODE_ADVANCED_LABEL),
	softwareOptionNeoModeLockoutLabel_(MiscStrs::SOFTWARE_OPTION_NEOMODE_LOCKOUT_LABEL),

	softwareOptionBiLevelBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),
	softwareOptionTCBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),
	softwareOptionNeoModeBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),
	softwareOptionVtpcBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),
	softwareOptionPavBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),
	softwareOptionRespMechBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),
	softwareOptionTrendingBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),
	softwareOptionLeakCompBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),
	softwareOptionNeoModeUpdateBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),
	softwareOptionNeoModeAdvancedBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),
	softwareOptionNeoModeLockoutBox_(0, 0, SOFTWARE_OPTIONS_BOX_WIDTH_, SOFTWARE_OPTIONS_BOX_HEIGHT_),

	optionBiLevelEnabledBitmap_(Image::RCheckMarkIcon),
	optionTCEnabledBitmap_(Image::RCheckMarkIcon),
	optionNeoModeEnabledBitmap_(Image::RCheckMarkIcon),
	optionVtpcEnabledBitmap_(Image::RCheckMarkIcon),
	optionPavEnabledBitmap_(Image::RCheckMarkIcon),
	optionRespMechEnabledBitmap_(Image::RCheckMarkIcon),
	optionTrendingEnabledBitmap_(Image::RCheckMarkIcon),
	optionLeakCompEnabledBitmap_(Image::RCheckMarkIcon),
	optionNeoModeUpdateEnabledBitmap_(Image::RCheckMarkIcon),
	optionNeoModeAdvancedEnabledBitmap_(Image::RCheckMarkIcon),
	optionNeoModeLockoutEnabledBitmap_(Image::RCheckMarkIcon),
	//TODO E600 TT: the following 2 RTrendIcon should be replaced 
	//with appropriate bitmaps for GUI and BD as on 840
	guiSymbolBitmap_(Image::RTrendIcon),
	bdSymbolBitmap_(Image::RTrendIcon)
{
	CALL_TRACE("VentConfigSubScreen::VentConfigSubScreen(pSubScreenArea)");

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	// Add the screen dividing lines
	verticalDividingLine_.setColor(Colors::WHITE);
	addDrawable(&verticalDividingLine_);
	horizontalDividingLine1_.setColor(Colors::WHITE);
	addDrawable(&horizontalDividingLine1_);
	horizontalDividingLine2_.setColor(Colors::WHITE);
	addDrawable(&horizontalDividingLine2_);

	//remove the original symbol labels which were shown as weird rectangles
	//instead showing bitmap for GUI and BD
	guiSymbolBitmap_.setX(GUI_SYMBOL_LABEL_X_POSITION_);
	guiSymbolBitmap_.setY(GUI_SYMBOL_LABEL_Y_POSITION_);
	guiSymbolBitmap_.setShow(true);
	addDrawable(&guiSymbolBitmap_);

	bdSymbolBitmap_.setX(BD_SYMBOL_LABEL_X_POSITION_);
	bdSymbolBitmap_.setY(BD_SYMBOL_LABEL_Y_POSITION_);
	bdSymbolBitmap_.setShow(true);
	addDrawable(&bdSymbolBitmap_);

	// Set color, place and add field labels of sofware options label.
	softwareOptionsLabel_.setColor(Colors::WHITE);
	softwareOptionsLabel_.setX(SOFTWARE_OPTIONS_TITLE_X_);
	softwareOptionsLabel_.setY(SOFTWARE_OPTIONS_TITLE_Y_);
	addDrawable(&softwareOptionsLabel_);

	//---------------------------------------------------------------------
	// Determine maximum width of Software and POST labels...
	// NOTE:  any text field object can be used here, so 'guiVersionLabel_'
	//        was picked.
	//---------------------------------------------------------------------

	// determine width of Software version label...
	swprintf(ConfigString_, L"{p=10:%s}", MiscStrs::SOFTWARE_VERSION_LABEL);  
	guiVersionLabel_.setText(ConfigString_);
	SwPostMaxValueOffset_ = guiVersionLabel_.getWidth();

	// determine width of POST version label...
	swprintf(ConfigString_, L"{p=10:%s}", MiscStrs::POST_VERSION_LABEL);  
	guiVersionLabel_.setText(ConfigString_);
	SwPostMaxValueOffset_ = MAX_VALUE(SwPostMaxValueOffset_, guiVersionLabel_.getWidth());

	//---------------------------------------------------------------------
	// Determine width of Serial Number label...
	// NOTE:  any text field object can be used here, so 'guiVersionLabel_'
	//        was picked.
	//---------------------------------------------------------------------

	// determine width of Serial Number label...
	swprintf(ConfigString_, L"{p=10:%s}", MiscStrs::SERIAL_NUM_LABEL);  
	guiVersionLabel_.setText(ConfigString_);
	SerialNoMaxValueOffset_ = guiVersionLabel_.getWidth();

	// determine width of PROX Revision label...
	swprintf(ConfigString_, L"{p=10:%s}", MiscStrs::PROX_REV_LABEL);  
	guiVersionLabel_.setText(ConfigString_);
	ProxMaxValueOffset_ = guiVersionLabel_.getWidth();

	guiVersionLabel_.setColor(Colors::WHITE);
	addDrawable(&guiVersionLabel_);

	guiPostPartlNumberLabel_.setColor(Colors::WHITE);
	addDrawable(&guiPostPartlNumberLabel_);

	bdVersionLabel_.setColor(Colors::WHITE);
	addDrawable(&bdVersionLabel_);

	bdPostPartlNumberLabel_.setColor(Colors::WHITE);
	addDrawable(&bdPostPartlNumberLabel_);

	guiSerialNumberLabel_.setColor(Colors::WHITE);
	addDrawable(&guiSerialNumberLabel_);

	bdSerialNumberLabel_.setColor(Colors::WHITE);
	addDrawable(&bdSerialNumberLabel_);

	proxConfigTitleLabel_.setColor(Colors::WHITE);
	addDrawable(&proxConfigTitleLabel_);

	//[PX00408] 840 Application shall show the PROX system firmware
	//			revision and serial number on the configuration sub-screen
    proxFirmwareRevLabel_.setColor(Colors::WHITE);
	addDrawable(&proxFirmwareRevLabel_);

	proxSerialNumberLabel_.setColor(Colors::WHITE);
	addDrawable(&proxSerialNumberLabel_);

	// Display the screen title
	addDrawable(&titleArea_);

	for (Uint optionIdx = 0u; optionIdx < SoftwareOptions::NUM_OPTIONS; optionIdx++)
	{	// $[TI1] -- this path is ALWAYS taken...
		OptionInfo_ *const  POptionInfo = (::ArrOptionInfo_ + optionIdx);

		switch (optionIdx)
		{
		case SoftwareOptions::BILEVEL :				// $[TI1.1]
			POptionInfo->isShown = TRUE;
			POptionInfo->pLabel  = &softwareOptionBiLevelLabel_;
			POptionInfo->pBox    = &softwareOptionBiLevelBox_;
			POptionInfo->pBitmap = &optionBiLevelEnabledBitmap_;
			break;

		case SoftwareOptions::NEO_MODE :			// $[TI1.2]
			POptionInfo->isShown = TRUE;
			POptionInfo->pLabel  = &softwareOptionNeoModeLabel_;
			POptionInfo->pBox    = &softwareOptionNeoModeBox_;
			POptionInfo->pBitmap = &optionNeoModeEnabledBitmap_;
			break;

		case SoftwareOptions::ATC :					// $[TI1.3]
			POptionInfo->isShown = TRUE;
			POptionInfo->pLabel  = &softwareOptionTCLabel_;
			POptionInfo->pBox    = &softwareOptionTCBox_;
			POptionInfo->pBitmap = &optionTCEnabledBitmap_;
			break;

		case SoftwareOptions::RESERVED1 :		// $[TI1.4]
			POptionInfo->isShown = FALSE;
			break;

		case SoftwareOptions::VTPC :				// $[TI1.6]
			POptionInfo->pLabel  = &softwareOptionVtpcLabel_;
			POptionInfo->pBox    = &softwareOptionVtpcBox_;
			POptionInfo->pBitmap = &optionVtpcEnabledBitmap_;
			break;

		case SoftwareOptions::PAV :					// $[TI1.7]
			POptionInfo->pLabel  = &softwareOptionPavLabel_;
			POptionInfo->pBox    = &softwareOptionPavBox_;
			POptionInfo->pBitmap = &optionPavEnabledBitmap_;
			break;

		case SoftwareOptions::RESP_MECH :
			POptionInfo->pLabel  = &softwareOptionRespMechLabel_;
			POptionInfo->pBox    = &softwareOptionRespMechBox_;
			POptionInfo->pBitmap = &optionRespMechEnabledBitmap_;
			break;

		case SoftwareOptions::TRENDING :
			POptionInfo->pLabel  = &softwareOptionTrendingLabel_;
			POptionInfo->pBox    = &softwareOptionTrendingBox_;
			POptionInfo->pBitmap = &optionTrendingEnabledBitmap_;
			break;
		case SoftwareOptions::LEAK_COMP :
			POptionInfo->pLabel  = &softwareOptionLeakCompLabel_;
			POptionInfo->pBox    = &softwareOptionLeakCompBox_;
			POptionInfo->pBitmap = &optionLeakCompEnabledBitmap_;
			break;

		case SoftwareOptions::NEO_MODE_UPDATE :
			POptionInfo->isShown = TRUE;
			POptionInfo->pLabel  = &softwareOptionNeoModeUpdateLabel_;
			POptionInfo->pBox    = &softwareOptionNeoModeUpdateBox_;
			POptionInfo->pBitmap = &optionNeoModeUpdateEnabledBitmap_;
			break;

		case SoftwareOptions::NEO_MODE_ADVANCED :
			POptionInfo->isShown = TRUE;
			POptionInfo->pLabel  = &softwareOptionNeoModeAdvancedLabel_;
			POptionInfo->pBox    = &softwareOptionNeoModeAdvancedBox_;
			POptionInfo->pBitmap = &optionNeoModeAdvancedEnabledBitmap_;
			break;

		case SoftwareOptions::NEO_MODE_LOCKOUT :
			POptionInfo->isShown = TRUE;
			POptionInfo->pLabel  = &softwareOptionNeoModeLockoutLabel_;
			POptionInfo->pBox    = &softwareOptionNeoModeLockoutBox_;
			POptionInfo->pBitmap = &optionNeoModeLockoutEnabledBitmap_;
			break;

		default :
			// unavailable option...
			POptionInfo->pLabel  = NULL;
			POptionInfo->pBox    = NULL;
			POptionInfo->pBitmap = NULL;
			break;
		}

		if (POptionInfo->pLabel != NULL)
		{	// $[TI1.4]
 			// RESERVED1 option is not shown.
 			// Shift remaining options up one line to avoid
 			// the appearance that a customer is lacking an available option.
 			Uint displayIndex = optionIdx;
			if( displayIndex > SoftwareOptions::RESERVED1)
			{
				displayIndex -= 1;
			} 

			Int32 X_VALUE = SOFTWARE_OPTIONS_TITLE_X_;

			// options 0-7 in first column, 8+ in second column
			if (displayIndex >= MAX_OPTIONS_PER_COLUMN_)
			{
				X_VALUE = SOFTWARE_OPTIONS_TITLE_X_ + SOFTWARE_OPTIONS_OFFSET_X_ + 
						  SOFTWARE_OPTIONS_BOX_WIDTH_ + 20;
				displayIndex -= MAX_OPTIONS_PER_COLUMN_;
			}

			const Int32  Y_VALUE = (SOFTWARE_OPTIONS_TITLE_Y_ +
								((displayIndex + 1) * SOFTWARE_OPTIONS_OFFSET_Y_));

			// Set color, place and add field labels of this sofware option
			// label.
			POptionInfo->pLabel->setColor(Colors::WHITE);
			POptionInfo->pLabel->setX(X_VALUE);
			POptionInfo->pLabel->setY(Y_VALUE);
			addDrawable(POptionInfo->pLabel);

			// initialize each box...
			POptionInfo->pBox->setColor(Colors::WHITE);
			POptionInfo->pBox->setFillColor(Colors::LIGHT_BLUE);
			POptionInfo->pBox->setX(X_VALUE + SOFTWARE_OPTIONS_OFFSET_X_);
			POptionInfo->pBox->setY(Y_VALUE);
			addDrawable(POptionInfo->pBox);

			// Position the bitmap.
			POptionInfo->pBitmap->setX(X_VALUE + SOFTWARE_OPTIONS_OFFSET_X_ + 1);
			POptionInfo->pBitmap->setY(Y_VALUE + 1);
			addDrawable(POptionInfo->pBitmap);
		}	// $[TI1.5]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VentConfigSubScreen  [Destructor]
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VentConfigSubScreen::~VentConfigSubScreen(void)
{
	CALL_TRACE("VentConfigSubScreen::~VentConfigSubScreen(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()		[virtual]
//
//@ Interface-Description
// 	Activate the VentConfigSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the screen title.  Retrieve Gui and Bd SoftwareRevNum, Gui and Bd
//  POST_PART_VERSION, SaasVersion, GuiSN, tBdSN CompressorSerialNum, and
//	Configuration Code.
// $[01203] The configuration subscreen shall display the part number of ...
// $[01339] The configuration subscreen shall display the serial number ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentConfigSubScreen::activate(void)
{
	CALL_TRACE("VentConfigSubScreen::activate(void)");

// GUI START

	//-----------------------------------------------------------------
	// build the GUI Software version string...
	//-----------------------------------------------------------------
	wchar_t tmpSoftRevNum[256];
	StringConverter::ToWString(tmpSoftRevNum, 256, SoftwareRevNum::GetInstance().GetAsShortString());
	swprintf(ConfigString_, MiscStrs::CONFIG_TEXT_FIELD,
								  4,	// x
								  80,	// y
								  MiscStrs::SOFTWARE_VERSION_LABEL,
								  ::SwPostMaxValueOffset_,	// x=<offset>
								  tmpSoftRevNum);
	guiVersionLabel_.setText(ConfigString_);

	//-----------------------------------------------------------------
	// build the GUI POST version string...
	//-----------------------------------------------------------------
	wchar_t tmpBuffer[256];
	StringConverter::ToWString( tmpBuffer, 256, Post::GetKernelPartNumber() );
	swprintf(ConfigString_, MiscStrs::CONFIG_TEXT_FIELD,
								  4,	// x
								  100,	// y
								  MiscStrs::POST_VERSION_LABEL,
								  ::SwPostMaxValueOffset_,	// x=<offset>
								  tmpBuffer);
	guiPostPartlNumberLabel_.setText(ConfigString_);

	//-----------------------------------------------------------------
	// build the GUI Serial Number string...
	//-----------------------------------------------------------------
	StringConverter::ToWString( tmpBuffer, 256, GuiApp::GetGuiSN().getString() );
	swprintf(ConfigString_, MiscStrs::CONFIG_TEXT_FIELD,
								  4,	// x
								  120,	// y
								  MiscStrs::SERIAL_NUM_LABEL,
								  ::SerialNoMaxValueOffset_,	// x=<offset>
								  tmpBuffer );
	guiSerialNumberLabel_.setText(ConfigString_);

// GUI END

// BD START

	//-----------------------------------------------------------------
	// build the BD Software version string...
	//-----------------------------------------------------------------
	StringConverter::ToWString( tmpBuffer, 256, GuiApp::GetBdSoftwareRevNum());
	swprintf(ConfigString_, MiscStrs::CONFIG_TEXT_FIELD,
								  4,	// x
								  170,	// y
								  MiscStrs::SOFTWARE_VERSION_LABEL,
								  (::SwPostMaxValueOffset_),// x=<offset>
								  tmpBuffer);
	bdVersionLabel_.setText(ConfigString_);

	//-----------------------------------------------------------------
	// build the BD POST version string...
	//-----------------------------------------------------------------
	StringConverter::ToWString( tmpBuffer, 256, GuiApp::GetBdKernelPartNumber());
	swprintf(ConfigString_, MiscStrs::CONFIG_TEXT_FIELD,
								  4,	// x
								  190,	// y
								  MiscStrs::POST_VERSION_LABEL,
								  (::SwPostMaxValueOffset_),// x=<offset>
								  tmpBuffer);
	bdPostPartlNumberLabel_.setText(ConfigString_);

	//-----------------------------------------------------------------
	// build the BD Serial Number string...
	//-----------------------------------------------------------------
	wchar_t tmpBdSN[256];
	StringConverter::ToWString( tmpBdSN, 256, GuiApp::GetBdSN().getString() );
	swprintf(ConfigString_, MiscStrs::CONFIG_TEXT_FIELD,
								  4,	// x
								  210,	// y
								  MiscStrs::SERIAL_NUM_LABEL,
								  (::SerialNoMaxValueOffset_),// x=<offset>
								 tmpBdSN );
	bdSerialNumberLabel_.setText(ConfigString_);

// BD END
	
	if (GuiApp::IsProxInstalled())
	{
		//-----------------------------------------------------------------
		// build the PROX Firmware Config title string...    x    y
		//-----------------------------------------------------------------
		swprintf(ConfigString_, MiscStrs::PROX_CONFIG_LABEL, 340, 230); 
		proxConfigTitleLabel_.setText(ConfigString_);
	
		//-----------------------------------------------------------------
		// build the PROX Firmware rev string...
		//-----------------------------------------------------------------
		wchar_t tmpProxFirmwareRev[256];
		StringConverter::ToWString( tmpProxFirmwareRev, 256, GuiApp::GetProxFirmwareRev().getString());
		swprintf(ConfigString_, MiscStrs::CONFIG_TEXT_FIELD,
									  340,	// x
									  250,	// y
									  MiscStrs::PROX_REV_LABEL,
									  (340 + ::ProxMaxValueOffset_),	// x=<offset>
									  tmpProxFirmwareRev);
		proxFirmwareRevLabel_.setText(ConfigString_);
	
		//-----------------------------------------------------------------
		// build the PROX Firmware rev string...
		//-----------------------------------------------------------------
		swprintf(ConfigString_, MiscStrs::CONFIG_NUM_FIELD,
									  340,	// x
									  270,	// y
									  MiscStrs::SERIAL_NUM_LABEL,
									  (340 + ::SerialNoMaxValueOffset_),	// x=<offset>
									  GuiApp::GetProxSerialNum());
		proxSerialNumberLabel_.setText(ConfigString_);
	}

	//-----------------------------------------------------------------
	// build the Software Options table...
	//-----------------------------------------------------------------
	for (Uint optionIdx = 0u; optionIdx < SoftwareOptions::NUM_OPTIONS; optionIdx++)
	{	// $[TI1] -- this path is ALWAYS taken...
		OptionInfo_ *const  POptionInfo = (::ArrOptionInfo_ + optionIdx);

		if (POptionInfo->pBitmap != NULL)
		{	// $[TI1.1]
			const SoftwareOptions::OptionId  OPTION_ID =
										(SoftwareOptions::OptionId)optionIdx;

			POptionInfo->pBitmap->setShow(
					SoftwareOptions::IsOptionEnabled(OPTION_ID)
										 );
		}	// $[TI1.2]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()	[virtual]
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentConfigSubScreen::deactivate(void)
{
	CALL_TRACE("VentConfigSubScreen::deactivate(void)");

	// do nothing...
}	// $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
VentConfigSubScreen::SoftFault(const SoftFaultID  softFaultID,
								     const Uint32       lineNumber,
								     const char*        pFileName,
								     const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, VENTCONFIGSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
