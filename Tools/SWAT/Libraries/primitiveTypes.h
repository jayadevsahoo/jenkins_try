//---------------------------------------------------------------------------
//            Copyright (c) 2012 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//---------------------------------------------------------------------------

#ifndef PRIMITIVETYPES_H_
#define PRIMITIVETYPES_H_

//=====================================================================
//
//  Preprocessor Constants...
//
//=====================================================================

#ifdef NULL
  // 'NULL' is defined explicitly by us to make sure that it is not defined
  // as '(void*)0' -- which is the ANSI C definition.
#  undef NULL
#endif

#ifdef TRUE
#  undef TRUE
#endif

#ifdef FALSE
#  undef FALSE
#endif

//@ Constant:  NULL
// It is guaranteed that a pointer containing this address represents a
// pointer that is distinguishable from a pointer to any object or function.
#define NULL 0

//@ Constant:  FALSE
// This constant is guaranteed to represent the result of an invalid
// condition.
enum { FALSE = 0 };  // anonymous enum...

//@ Constant:  TRUE
// This constant is guaranteed to represent the result of a valid
// condition.
enum { TRUE = !FALSE };  // anonymous enum...


//=====================================================================
//
//  Sigma Standard Types...
//
//=====================================================================

//@ Type:  Byte
// An unsigned, 8-bit data type.
typedef unsigned char	Byte;

//@ Type:  Char
// A reserved type of characters.
typedef char	Char;

//@ Type:  Boolean
// A logical value (true or false) data type.
typedef unsigned char	Boolean;

//@ Type:  Uint8
// An unsigned, 8-bit integer data type.
typedef unsigned char	Uint8;

//@ Type:  Uint16
// An unsigned, 16-bit integer data type.
typedef unsigned short	Uint16;

//@ Type:  Uint32
// An unsigned, 32-bit integer data type.
typedef unsigned int	Uint32;

//@ Type:  Uint
// An unsigned, integer data type that is representative of a native long
// word.
typedef unsigned int	Uint;

//@ Type:  Int8
// A signed, 8-bit integer data type.
typedef signed char	Int8;

//@ Type:  Int16
// A signed, 16-bit integer data type.
typedef short	Int16;

//@ Type:  Int32
// A signed, 32-bit integer data type.
typedef int	Int32;

//@ Type:  Int
// A signed, integer data type that is representative of a native long word.
typedef int	Int;

//@ Type:  Real32
// A 32-bit, floating-point data type.
typedef float	Real32;

//@ Type:  MemPtr
// A generic pointer to memory.
typedef void*	MemPtr;

//@ Type:  Int64
// A signed, 64-bit integer data type.
typedef long long Int64;

//@ Type:  Int64
// An unsigned, 64-bit integer data type.
typedef unsigned long long Uint64;

#endif /* PRIMITIVETYPES_H_ */
