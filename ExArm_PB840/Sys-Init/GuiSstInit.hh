#ifndef GuiSstInit_HH
#define GuiSstInit_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  GuiSstInit - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiSstInit.hhv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 17-JUN-1997 DR Number:  1902
//      Project:   Sigma   (R8027)
//      Description:
//         Removed receiveBdReady() as "dead" code since GuiSstInit
//         is a transitory state only.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
//@ Usage-Classes
#include "Sys_Init.hh"
#include "FsmState.hh"
//@ End-Usage

class GuiSstInit : public FsmState
{
  public:
    GuiSstInit();
    ~GuiSstInit();

    const char* nameOf(void) const;
    SigmaState getSigmaState(void) const;
    void activate(void) const;
 
    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);

  private:
    GuiSstInit(const GuiSstInit&);              // not implemented
    void operator=(const GuiSstInit&);        // not implemented
};
 

#endif // GuiSstInit_HH
