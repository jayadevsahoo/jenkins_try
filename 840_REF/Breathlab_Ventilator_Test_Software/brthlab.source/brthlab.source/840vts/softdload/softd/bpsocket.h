// BPSocket.h : header file
//

#if !defined(AFX_BPSOCKET_H__607BB36A_026F_11D1_9F84_00805FCAC1CC__INCLUDED_)
#define AFX_BPSOCKET_H__607BB36A_026F_11D1_9F84_00805FCAC1CC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "BPServer.h"

// forward declaration

class BootPServer;

// end forward declaration

/////////////////////////////////////////////////////////////////////////////
// BootPSocket command target

class BootPSocket : public CAsyncSocket
{
// Attributes
private:
	BootPServer *bpServer;

// Operations
public:
	BootPSocket();
	BootPSocket(BootPServer *server);
	virtual ~BootPSocket();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BootPSocket)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(BootPSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BPSOCKET_H__607BB36A_026F_11D1_9F84_00805FCAC1CC__INCLUDED_)

