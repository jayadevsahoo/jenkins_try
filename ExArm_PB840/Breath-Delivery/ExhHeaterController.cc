#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhHeaterController - Implements an exhalation heater controller.
//---------------------------------------------------------------------
//@ Interface-Description
//		A methods is implemented to update the exhalation heater such that
//		the temperature is maintained near 60 C every CYCLE_TIME_MS.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms to maintain the exhalation heater
//		temperature.
//---------------------------------------------------------------------
//@ Implementation-Description
//		This controller is invoked every BD cycle to control the exhalation
//		heater to 60 C.  The controller is a simple off/on type of controller.
//		The controller is called every CYCLE_TIME_MS but is updated every
//		EXH_HEATER_CONTROL_CYCLE_MS.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		Only one instance can be instanciated.
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExhHeaterController.ccv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: syw    Date:  19-Dec-1996    DR Number: DCS 1603
//  	Project:  Sigma (R8027)
//		Description:
//			Changed setBitOn() and setBitOff to setBit().
//
//  Revision: 001  By:  syw    Date:  06-Nov-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

#include "ExhHeaterController.hh"

#include "MainSensorRefs.hh"

//@ Usage-Classes

#include "TemperatureSensor.hh"
#include "BinaryCommand.hh"
#include "BD_IO_Devices.hh"

//@ End-Usage


//@ Code...

// $[04116]
const Int32 EXH_HEATER_CONTROL_CYCLE_MS = 1000 ;    // millisecond
const Real32 MIN_TEMP = 59.0 ;      				// degrees C
const Real32 MAX_TEMP = 61.0 ;      				// degrees C

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhHeaterController()
//
//@ Interface-Description
//      Constructor.  A TemperatureSensor& and a ExhHeater& are passed as
//		arguments for initialization and this method returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 	The data members are initialized with the arguments passed in.
//		The clock timer is also initialized to EXH_HEATER_CONTROL_CYCLE_MS
//		in order for the exhalation heater to be updated the first time
//		newCycle() is called.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none
//---------------------------------------------------------------------
//@ PostCondition
//	 	none
//@ End-Method
//=====================================================================
ExhHeaterController::ExhHeaterController( TemperatureSensor& rTempSensor,
										  BinaryCommand& rExhHeater)
 : rTempSensor_( rTempSensor),				   	// $[TI1.1]
   rExhHeater_( rExhHeater)						// $[TI1.2]
{
	CALL_TRACE("ExhHeaterController::ExhHeaterController( \
				TemperatureSensor& rTempSensor, BinaryCommand& rExhHeater)") ;

   	// $[TI2]
	clockTimer_ = EXH_HEATER_CONTROL_CYCLE_MS ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExhHeaterController()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
// 		none
//---------------------------------------------------------------------
//@ PostCondition
// 		none
//@ End-Method
//=====================================================================
ExhHeaterController::~ExhHeaterController( void)
{
	CALL_TRACE("ExhHeaterController::~ExhHeaterController( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This
//      method is called during the BdTask::BdSecondaryTask to control the
//      exhalation heater temperature.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The exhalation heater is controlled every EXH_HEATER_CONTROL_CYCLE_MS.
//		If the temperature > MAX_TEMP, then the heater is turned off.
//		If the temperature < MIN_TEMP, then the heater is turned on.
//		The clock is updated by SECONDARY_CYCLE_TIME_MS.
// 		$[04116]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
ExhHeaterController::newCycle( void)
{
	CALL_TRACE("ExhHeaterController::newCycle( void)") ;

	// $[04116]

	if (clockTimer_ >= EXH_HEATER_CONTROL_CYCLE_MS)
	{
	   	// $[TI1.1]
		clockTimer_ = 0 ;

		Real32 temperature = rTempSensor_.getValue() ;

		if (temperature > MAX_TEMP)
		{
		   	// $[TI2.1]
            rExhHeater_.setBit( BitAccessGpio::OFF) ;
		}
		else if (temperature < MIN_TEMP)
		{
		   	// $[TI2.2]
            rExhHeater_.setBit( BitAccessGpio::ON) ;
		}  	// implied else $[TI2.3]
	} 	// implied else $[TI1.2]

	clockTimer_ += SECONDARY_CYCLE_TIME_MS;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ExhHeaterController::SoftFault( const SoftFaultID  softFaultID,
                   		   const Uint32       lineNumber,
	                       const char*        pFileName,
	                       const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, EXHHEATERCONTROLLER,
							 lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================







