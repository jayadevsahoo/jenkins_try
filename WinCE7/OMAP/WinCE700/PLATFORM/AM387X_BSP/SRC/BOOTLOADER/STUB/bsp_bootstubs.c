// All rights reserved ADENEO EMBEDDED 2010
/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//  File:  bsp_bootstubs.c
// stub routines which exist in full images but not in bootloader images

#include "bsp.h"
#include "bsp_cfg.h"
#include "am387x_prcm.h"
#include "am387x_clocks.h"
#include "am387x_base_regs.h"

BOOL INTERRUPTS_STATUS() { return FALSE; }
BOOL CloseHandle(HANDLE hObject){
    UNREFERENCED_PARAMETER(hObject);
    return TRUE;
}

void WINAPI EnterCriticalSection(LPCRITICAL_SECTION lpcs) {UNREFERENCED_PARAMETER(lpcs);}
void WINAPI LeaveCriticalSection(LPCRITICAL_SECTION lpcs) {UNREFERENCED_PARAMETER(lpcs);}
void WINAPI InitializeCriticalSection( LPCRITICAL_SECTION lpcs){UNREFERENCED_PARAMETER(lpcs);}
void DeleteCriticalSection(LPCRITICAL_SECTION lpCriticalSection){UNREFERENCED_PARAMETER(lpCriticalSection);}

HANDLE WINAPI SC_CreateMutex(LPSECURITY_ATTRIBUTES lpsa,BOOL bInitialOwner,LPCTSTR lpName){
    UNREFERENCED_PARAMETER(lpsa);
    UNREFERENCED_PARAMETER(bInitialOwner);
    UNREFERENCED_PARAMETER(lpName);
    return NULL;
}

DWORD WINAPI SC_WaitForMultiple(DWORD cObjects, CONST HANDLE *lphObjects, 
								BOOL fWaitAll, DWORD dwTimeout) {
    UNREFERENCED_PARAMETER(cObjects);
    UNREFERENCED_PARAMETER(lphObjects);
    UNREFERENCED_PARAMETER(fWaitAll);
    UNREFERENCED_PARAMETER(dwTimeout);
    return 0;
}

BOOL WINAPI SC_ReleaseMutex( HANDLE hMutex) {
    UNREFERENCED_PARAMETER(hMutex);
    return TRUE;
}

BOOL EnableDeviceClocks( UINT devId, BOOL bEnable)
{
    AM387X_PRCM_REGS* pPrcmRegs = OALPAtoUA(AM387X_PRCM_REGS_PA);

    switch (devId){
        case AM_DEVICE_UART0:
            if (bEnable){                
                SETREG32(&pPrcmRegs->CM_ALWON_UART_0_CLKCTRL, 0x2);
                while (pPrcmRegs->CM_ALWON_UART_0_CLKCTRL & 0x30000);
            } else {
                CLRREG32(&pPrcmRegs->CM_ALWON_UART_0_CLKCTRL, 0x2);
            }
            break;
        case AM_DEVICE_UART1:
            if (bEnable){                
                SETREG32(&pPrcmRegs->CM_ALWON_UART_1_CLKCTRL, 0x2);
                while (pPrcmRegs->CM_ALWON_UART_1_CLKCTRL & 0x30000);
            } else {
                CLRREG32(&pPrcmRegs->CM_ALWON_UART_1_CLKCTRL, 0x2);
            }
            break;
        case AM_DEVICE_UART2:
            if (bEnable){                
                SETREG32(&pPrcmRegs->CM_ALWON_UART_2_CLKCTRL, 0x2);
                while (pPrcmRegs->CM_ALWON_UART_2_CLKCTRL & 0x30000);
            } else {
                CLRREG32(&pPrcmRegs->CM_ALWON_UART_2_CLKCTRL, 0x2);
            }
            break;

        case AM_DEVICE_EMACSW:
            // +++FIXME
            break;

#if 0
		case AM_DEVICE_TIMER1:
            if (bEnable){                
                SETREG32(&pPrcmRegs->CM_ALWON_TIMER_1_CLKCTRL, 0x2);
                while (pPrcmRegs->CM_ALWON_TIMER_1_CLKCTRL & 0x30000);
            } else {
                CLRREG32(&pPrcmRegs->CM_ALWON_TIMER_1_CLKCTRL, 0x2);
            }
            break;

		case AM_DEVICE_EMAC0:
            if (bEnable){
                pPrcmRegs->CM_ETHERNET_CLKSTCTRL = 0x2;
                pPrcmRegs->CM_ALWON_ETHERNET_0_CLKCTRL = 0x2;
                while (pPrcmRegs->CM_ALWON_ETHERNET_0_CLKCTRL & 0x70000);
            } else {
                CLRREG32(&pPrcmRegs->CM_ALWON_ETHERNET_0_CLKCTRL, 0x2);
            }
            break;

		case AM_DEVICE_EMAC1:
            if (bEnable){
                pPrcmRegs->CM_ETHERNET_CLKSTCTRL = 0x2;
                pPrcmRegs->CM_ALWON_ETHERNET_1_CLKCTRL = 0x2;
                while (pPrcmRegs->CM_ALWON_ETHERNET_1_CLKCTRL & 0x70000);
            } else {
                CLRREG32(&pPrcmRegs->CM_ALWON_ETHERNET_1_CLKCTRL, 0x2);
            }
            break;
#endif
	
		}
    return TRUE;
}

BOOL PrcmDeviceGetContextState( UINT devId, BOOL bSet){
    UNREFERENCED_PARAMETER(devId);
    UNREFERENCED_PARAMETER(bSet);
    return TRUE;
}

BOOL EnableDeviceClocksNoRefCount( UINT devId, BOOL bEnable)
{
	return EnableDeviceClocks( devId, bEnable );
}

BOOL BusClockRelease(HANDLE hBus, UINT id)
{
    UNREFERENCED_PARAMETER(hBus);
    return EnableDeviceClocks(id,FALSE);
}

BOOL BusClockRequest(HANDLE hBus, UINT id){
    UNREFERENCED_PARAMETER(hBus);
    return EnableDeviceClocks(id,TRUE);
}

VOID MmUnmapIoSpace(PVOID BaseAddress, ULONG NumberOfBytes){
    UNREFERENCED_PARAMETER(BaseAddress);
    UNREFERENCED_PARAMETER(NumberOfBytes);
}

PVOID MmMapIoSpace( PHYSICAL_ADDRESS PhysicalAddress,ULONG NumberOfBytes, 
					BOOLEAN CacheEnable )
{
    UNREFERENCED_PARAMETER(NumberOfBytes);
    return OALPAtoVA(PhysicalAddress.LowPart,CacheEnable);
}

HANDLE CreateBusAccessHandle (LPCTSTR lpActiveRegPath){    
    UNREFERENCED_PARAMETER(lpActiveRegPath);
    return (HANDLE) 0xAA;
}

void HalContextUpdateDirtyRegister(UINT32 ffRegister){UNREFERENCED_PARAMETER(ffRegister);}
// 383
