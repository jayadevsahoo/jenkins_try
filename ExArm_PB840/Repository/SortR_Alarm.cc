#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SortR_Alarm - Abstract Sorted List of 'Alarm' References.
//---------------------------------------------------------------------
//@ Interface-Description
//  This provides an abstraction for a sorted list of references to
//  data items each of type 'Alarm'.  The data items of the list are
//  references to the data items that are added to the list.  The items
//  are inserted based on the sorted order of the new item against the
//  list's items.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a sorted collection of sortable objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Items are sorted as they're inserted into the list.  A
//  doubly-linked list of references is used internally to keep the
//  items.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions namely have to do with the data type that is
//  chosen for this list.  The data type must provide the following
//  mechanisms, to be allowed to be used in this list:
//>Von
//      Alarm::operator==(const Alarm& aType) const;// equivalence operator...
//      Alarm::operator!=(const Alarm& aType) const;// inequivalence operator...
//      Alarm::operator>(const Alarm& aType) const; // greater-than operator...
//      Alarm::operator<(const Alarm& aType) const; // less-than operator...
//>Voff
//  For primitive (built-in) types these are already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//      Since these lists contain pointers to the inserted items, those
//      items that are inserted into these lists must have a lifetime that
//      is longer than the lifetime of these lists' reference to them.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSortR.C_v   25.0.4.0   19 Nov 2013 14:03:44   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SortR_Alarm.hh"
#include "Alarm.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SortR_Alarm()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this sorted list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The 'rElemList_' alias can NOT be used from within this
//  destructor, because the element list will be destructed by the
//  derived class's destructor BEFORE this destructor is run.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SortR_Alarm::~SortR_Alarm(void)
{
  CALL_TRACE("~SortR_Alarm()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isSorted()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by all
//  derived clases, and defined to return a boolean indicator as to
//  whether this instance is currently sorted.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  search(item, foundWhen)
//
//@ Interface-Description
//  Search the current list for a node that is a match of 'item'.
//  When 'foundWhen' is 'EQUIV_OBJECTS', then a match occurs when
//  an equivalent item is found in this list.  When 'foundWhen' is
//  'EQUIV_REFERENCES', then a match occurs when a reference to
//  'item' is found in this list.  If a match is found, then
//  'SUCCESS' is returned, and the current node is left pointing to
//  the matching node.  If NO match is found, then 'FAILURE' is
//  returned, and the list is unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Near the end of this method a pointer to a constant node,
//  'pFoundNode', has its const-ness cast away.  This is due to the
//  fact that 'findNode_()' is a constant method (to allow its use
//  in constant public methods) that returns a pointer.  To ensure
//  consistent const-ness of 'findNode_()', the pointer that is
//  returned is a pointer to a CONSTANT node.  The const-ness of
//  the returned node is not necessary in the design of 'findNode_()',
//  but rather a consistency of design of that method.  Therefore,
//  when this method casts the const-ness of 'pFoundNode' away, it
//  is an acceptable case of working properly with a proper design.
//---------------------------------------------------------------------
//@ PreConditions
//  ((foundWhen == EQUIV_OBJECTS  &&  isSorted())  ||
//    foundWhen == EQUIV_REFERENCES)
//  (foundWhen == EQUIV_OBJECT  ||  foundWhen == EQUIV_REFERENCE)
//---------------------------------------------------------------------
//@ PostConditions
//  (status == SUCCESS) ==> (currentItem() == item)
//  (status == FAILURE) ==> (pCurrent_ == OLD{pCurrent_})
//@ End-Method
//=====================================================================

SigmaStatus
SortR_Alarm::search(Alarm&                 item,
			    const EquivResolution foundWhen)
{
  CALL_TRACE("search(item, foundWhen)");

  SigmaStatus  status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this list is empty, therefore search for a node that contains an
    // item that matches 'item'...

    // get a pointer to a node that is either equivalent to 'item', or
    // is an immediate neighbor of where an equivalent node would be...
    const DNodeC_MemPtr*  pFoundNode = findNode_(item, foundWhen);

    switch (foundWhen)
    {
    case EQUIV_OBJECTS:		// $[TI1.1]
      AUX_CLASS_PRE_CONDITION((isSorted()),
			      ((getMaxItems() << 16) | sizeof(Alarm)));

      // test the equivalence of 'item' and the item that 'pFoundNode'
      // points to...
      if (getItem_(pFoundNode) == item)
      {   // $[TI1.1.1]
	// a matching object is found, therefore set the found node as
	// the current node of this list...
	setCurrent_((DNodeC_MemPtr*)pFoundNode); // "const cast-away:"
	status = SUCCESS;
      }   // $[TI1.1.2] -- no matching OBJECT was found...

      break;

    case EQUIV_REFERENCES:	// $[TI1.2]
      // test the equivalence of the address of 'item' and the address
      // that 'pFoundNode' contains...
      if ((const Alarm*)(pFoundNode->getItem()) == &item)
      {   // $[TI1.2.1]
	// a matching reference is found, therefore set the found node as
	// the current node of this list...
	setCurrent_((DNodeC_MemPtr*)pFoundNode); // "const cast-away:"
	status = SUCCESS;
      }   // $[TI1.2.2] -- no matching REFERENCE was found...

      break;

    default:
      AUX_CLASS_ASSERTION_FAILURE(((foundWhen     << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(Alarm)));	    // bottom 12 bits...
      break;
    }
  }   // $[TI2] -- this list is currently empty...

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  addItem(newItem)
//
//@ Interface-Description
//  Add 'newItem' into its proper location within this sorted list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method must first determine where it is the new node that
//  is to contain 'newItem' needs to be located, then it needs to
//  insert a new node at that location.  The 'insert()' method of
//  this list's sorted list is used for the insertion of the new
//  node.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//  (isSorted())
//---------------------------------------------------------------------
//@ PostConditions
//  (currentItem() == newItem)
//@ End-Method
//=====================================================================

void
SortR_Alarm::addItem(Alarm& newItem)
{
  CALL_TRACE("addItem(newItem)");

  InsertionPlace  insertPlace;

  if (!rElemList_.isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore determine the "sorted" location
    // for 'newItem' then add a new node there with an item equivalent
    // to 'newItem'...
    AUX_CLASS_PRE_CONDITION((!rElemList_.isFull()),
			    ((getMaxItems() << 16) | sizeof(Alarm)));
    SAFE_CLASS_PRE_CONDITION((isSorted()));  // checked by 'findNode_()'...

    // find a node whose item is equivalent to 'newItem', or a neighbor
    // node that is located where 'newItem' should be added...
    const DNodeC_MemPtr*  pFoundNode = findNode_(newItem,
							EQUIV_OBJECTS);

    if (newItem == getItem_(pFoundNode))
    {   // $[TI1.1]
      // 'findNode_()' found a node with an item that is equivalent to
      // 'newItem', therefore add 'newItem' after the last node that is
      // equivalent to 'newItem'...
      for (; pFoundNode->getNextPtr() != getFirst_()  &&
			     newItem == getItem_(pFoundNode->getNextPtr());
	   pFoundNode = pFoundNode->getNextPtr())
      {   // $[TI1.1.1] -- at least one iteration...
	// do nothing...
      }   // $[TI1.1.2] -- no iterations...

      // insert the new node after the last node that is equivalent to
      // 'newItem'...
      insertPlace = AFTER_CURRENT;
    }
    else
    {   // $[TI1.2]
      // 'pFoundNode' does not contain an item that is equivalent to
      // 'newItem', but it does contain a node that is to be the neighbor
      // of this new item's node, therefore if 'newItem' is less than
      // the item of 'pFoundNode', then insert it before the current
      // node, otherwise insert it after the current node...
      insertPlace = (newItem < getItem_(pFoundNode))
		     ? BEFORE_CURRENT		// $[TI1.2.1]
		     : AFTER_CURRENT;		// $[TI1.2.2]
    }

    // set the current node of this list to the node returned by
    // 'findNode_()'...
    setCurrent_((DNodeC_MemPtr*)pFoundNode);  // "const cast-away..."
  }
  else
  {   // $[TI2] -- 'rElemList' is currently empty..
    insertPlace = AFTER_CURRENT;  // either place is fine...
  }

  // insert a new node based on the current node...
  rElemList_.insert(newItem, insertPlace);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeItem(oldItem, foundWhen)
//
//@ Interface-Description
//  Remove the first node in this list that is a match of 'oldItem',
//  and return 'SUCCESS'.  If there is NO match, then 'FAILURE' is
//  returned, and the list is unchanged.  When 'foundWhen' is
//  'EQUIV_OBJECTS', then a match occurs when an item equivalent to
//  'item' is found in this list.  When 'foundWhen' is
//  'EQUIV_REFERENCES', then a match occurs when a reference to 'item'
//  is found in this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method relies on the fact that search will set the current
//      node to the item found, when an equivalence is found.
//---------------------------------------------------------------------
//@ PreConditions
//  ((foundWhen == EQUIV_OBJECTS  &&  isSorted())  ||
//    foundWhen == EQUIV_REFERENCES)
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SigmaStatus
SortR_Alarm::removeItem(Alarm&                 oldItem,
				const EquivResolution foundWhen)
{
  CALL_TRACE("removeItem(oldItem, foundWhen)");
  SAFE_CLASS_PRE_CONDITION(((foundWhen == EQUIV_OBJECTS && isSorted())  ||
			     foundWhen == EQUIV_REFERENCES));

  SigmaStatus  status;

  // if this list is not empty and the search for 'oldItem' is successful,
  // then remove the node that the search found 'oldItem' in, otherwise
  // return 'FAILURE'...
  status = (!isEmpty()  &&  search(oldItem, foundWhen) == SUCCESS)
	    ? rElemList_.removeCurrent()	// $[TI1]
	    : FAILURE;				// $[TI2]

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//	                [static]
//
//@ Interface-Description
//	Report the software fault that occured within the source code
//	of this class.  The fault, indicated by 'softFaultID', occured
//	at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

void
SortR_Alarm::SoftFault(const SoftFaultID softFaultID,
                                  const Uint32      lineNumber,
                                  const char*       pFileName,
                                  const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, ABSTRACT_SORTR,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  findNode_(item, foundWhen)  [const]
//
//@ Interface-Description
//  Find the node that contains an element equivalent to 'item',
//  and return a pointer to it.  If no match is found then return
//  the node where the search terminated; if the list is empty, then
//  return 'NULL'.  If there are multiple matches to 'item', then
//  a pointer is returned to the very first node in this list that
//  is a match, AND that is not before the current.
//---------------------------------------------------------------------
//@ Implementation-Description
//      If this list is not empty, then the search will terminate right
//      where a node containing 'item' should be located.
//---------------------------------------------------------------------
//@ PreConditions
//  (foundWhen == EQUIV_OBJECTS  ||  isSorted())
//---------------------------------------------------------------------
//@ PostConditions
//  ((!isEmpty()  &&  findNode_() != NULL)  ||
//   (isEmpty()   &&  findNode_() == NULL))
//@ End-Method
//=====================================================================

const DNodeC_MemPtr *
SortR_Alarm::findNode_(Alarm&                 item,
			       const EquivResolution foundWhen) const
{
  CALL_TRACE("findNode_(item, foundWhen)");

  const DNodeC_MemPtr*  pFound = getCurrent_();

  if (!isEmpty())
  {   // $[TI1]
    switch (foundWhen)
    {
    case EQUIV_OBJECTS:		// $[TI1.1]
      AUX_CLASS_PRE_CONDITION((isSorted()),
			      ((getMaxItems() << 16) | sizeof(Alarm)));

      // if 'pFoundNode' is not at the front of this list and it's item is
      // greater than 'item', then search backwards from 'pFoundNode'.
      // Otherwise, if 'pFoundNode' is not at the end of this list and it's
      // item is less than 'item', then search forwards from 'pFoundNode'...
      if (pFound != getFirst_()  &&  item < getItem_(pFound))
      {   // $[TI1.1.1] -- search "backwards"...
	do
	{ // while not pointing at the front of the list and 'item'
	  // is less than the node pointed to by 'pFound', search backward
	  // from 'pFound'...
	  pFound = pFound->getPrevPtr();
	} while (pFound != getFirst_()  &&  item < getItem_(pFound));

	if (item == getItem_(pFound))
	{   // $[TI1.1.1.1]
	  // a matching object was found, now make sure that the object
	  // is the first object in this list that is equivalent to
	  // 'item'...
	  for (; pFound != getFirst_()  &&
				   getItem_(pFound->getPrevPtr()) == item;
	       pFound = pFound->getPrevPtr())
	  {   // $[TI1.1.1.1.1] -- at least one iteration...
	    // do nothing...
	  }   // $[TI1.1.1.1.2] -- no iterations...
	}   // $[TI1.1.1.2] -- a matching object was NOT found...
      }
      else if (pFound->getNextPtr() != getFirst_()  &&
	       item > getItem_(pFound))
      {   // $[TI1.1.2] -- search "forwards"...
	do
	{ // while not at the end of the list and 'item' is greater than
	  // the node pointed to by 'pFound', search forward from
	  // 'pFound'...
	  pFound = pFound->getNextPtr();
	} while (pFound->getNextPtr() != getFirst_()  &&
		 item > getItem_(pFound));
      }   // $[TI1.1.3] -- 'pFound' is at the equivalent location...

      break;

    case EQUIV_REFERENCES:	// $[TI1.2]
      {
	const Alarm* const  P_CURR_ITEM = (const Alarm*)(pFound->getItem());

	if (P_CURR_ITEM != &item)
	{   // $[TI1.2.1]
	  // the current node does NOT reference 'item', therefore search for
	  // the first reference that is a reference to 'item'...
	  const DNodeC_MemPtr*  pTempFound;

	  pTempFound = rElemList_.findNode_(item, getFirst_(), FORWARD,
					    EQUIV_REFERENCES);

	  if (pTempFound != NULL)
	  {   // $[TI1.2.1.1]
	    // a matching reference is found...
	    pFound = pTempFound;
	  }   // $[TI1.2.1.2] -- NO matching reference is found...
	}   // $[TI1.2.2] -- the current node references 'item'...
      }  // end of scope...

      break;

    default:
      AUX_CLASS_ASSERTION_FAILURE(((foundWhen     << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(Alarm)));	    // bottom 12 bits...
      break;
    }
  }   // $[TI2] -- this list is currently empty...

  return(pFound);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  print_(ostr)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by all
//  derived clases, and defined to dump the contents of this instance
//  to 'ostr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented
