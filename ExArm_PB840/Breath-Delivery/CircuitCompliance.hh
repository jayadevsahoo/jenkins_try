#ifndef CircuitCompliance_HH
#define CircuitCompliance_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: CircuitCompliance - Manages patient circuit compliance volume.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/CircuitCompliance.hhv   25.0.4.0   19 Nov 2013 13:59:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015   By: syw   Date:  29-Aug-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		added flag to limit 200 msec min time.
//
//  Revision: 010  By: syw     Date:  12-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Added NeoMode-specific bacteria filter compliance.
//
//  Revision: 009  By: dosman     Date:  26-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release.
//      Added an enum to help choose between use of TOTAL_COMPLIANCE
//	versus TUBE_COMPLIANCE for getComplianceVolume()
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007 By: syw    Date: 04-Nov-1997   DR Number: DCS 2602
//  	Project:  Sigma (R8027)
//		Description:
//			Added areValuesDefault() method.
//
//  Revision: 006 By: syw    Date: 11-Aug-1997   DR Number: DCS 2356
//  	Project:  Sigma (R8027)
//		Description:
//			Added adiabatic factors.
//
//  Revision: 005 By: syw    Date: 12-Jun-1997   DR Number: DCS 1908
//  	Project:  Sigma (R8027)
//		Description:
//			Created log table lookup method to reduce run time overhead of
//			computing log().
//
//  Revision: 004 By: syw    Date: 27-Mar-1997   DR Number: DCS 1780, 1827
//  	Project:  Sigma (R8027)
//		Description:
//			New compliance definition.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Moved copy and =operator from private to public to
//			accommodate NovRam design.  Added setComplianceTable()
//			method.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes
//@ End-Usage

// minimum adiabatic factor (pressure ratio)
const Real32 MIN_ADIABATIC_FACTOR = 0.71;

// maximum adiabatic factor (pressure ratio)
const Real32 MAX_ADIABATIC_FACTOR = 1.0;

//@ Constant: PRESSURE_TO_OBTAIN_COMPLIANCE
// pressure used to obtain compliance (ml/cmH20)
extern const Real32 PRESSURE_TO_OBTAIN_COMPLIANCE;


class CircuitCompliance 
{
  public:
    CircuitCompliance( void) ;
    ~CircuitCompliance( void) ;
    CircuitCompliance( const CircuitCompliance& rCircCompl) ;
    void operator=( const CircuitCompliance& rCircCompl) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

	enum ComplianceType {TOTAL_COMPLIANCE, TUBE_COMPLIANCE};

    Real32 getHumidifierCompliance(Boolean isWaterInHumidifier = TRUE);
    Real32 getBacteriaFilterCompliance(void);
    Real32 getComplianceVolume( Real32 pressure, Uint32 timeMs, 
		ComplianceType complianceType = TOTAL_COMPLIANCE,
		Boolean limitTime = TRUE) ;

    inline void setLowCompliance( const Real32 lowCompliance) ;
    inline void setHighCompliance( const Real32 highCompliance) ;
    inline void setLowFlowTime( const Uint32 lowFlowTimeMs) ;
    inline void setHighFlowTime( const Uint32 highFlowTimeMs) ;
    inline void setLowFlowAdiabaticFactor( const Real32 factor) ;
    inline void setHighFlowAdiabaticFactor( const Real32 factor) ;
    inline Uint32 getLowFlowTime( void) const ;

	Real32 getAdiabaticFactor( Uint32 timeMs) ;
	Boolean areValuesDefault( void) ;
		
  protected:

  private:

  	Real32 logValue_( const Uint32 value) ;
  	
	//@ Data-Member: lowCompliance_
	// low compliance value obtained during SST
	Real32 lowCompliance_ ;

	//@ Data-Member: highCompliance_
	// high compliance value obtained during SST
	Real32 highCompliance_ ;

	//@ Data-Member: lowFlowTimeMs_
	// amount of time to calibrate with low flow
	Uint32 lowFlowTimeMs_ ;

	//@ Data-Member: highFlowTimeMs_
	// amount of time to calibrate with high flow
	Uint32 highFlowTimeMs_ ;

	//@ Data-Member: lowFlowAdiabaticFactor_
	// Adiabatic Factor obtained with low flow
	Real32 lowFlowAdiabaticFactor_ ;

	//@ Data-Member: highFlowAdiabaticFactor_
	// Adiabatic Factor obtained with high flow
	Real32 highFlowAdiabaticFactor_ ;

} ;

// Inlined methods
#include "CircuitCompliance.in"

#endif // CircuitCompliance_HH 
