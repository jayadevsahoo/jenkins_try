/*****************************************************************************/
/*   Module Name:    perf.c                                         
/*   Purpose:      840 Ventilator Final Test                                     
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in        
/*          <72361-93>
/*   Modification History:
/*
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 Added Globals.h for the removal of 
/* 	 Test Executive	functions				Hamsavally(CGS)   28 Dec,2000
/*                                                                           
/*   Modified several routines for PTS 2000 serial communications so as to process 
/*   not just the CarriageReturn character at the end of a message, but also the LineFeed 
/*   which was being left behind in the incoming buffer.  Routines affected include 
/*   MeasurePressureSingle, SetupFlow, SetupPressure, MeasureFlowSingle, 
/*   MeasureFlowSingleO2, MeasurePressureSingleOneSec, 
/*   and MeasurePressureSingleOneSecAfter.
/*                                          Dave Richardson   20 Nov, 2002
/*
/*   $Revision::   1.2      $                                                
/*
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/testutil/perf.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:40   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   20 Nov 2002 15:01:12   Richardson
 * Updated Per SCRs # 17

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.

Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

// TBD - must handle all com through the new routines, each return error code
// indicating com failed
//  if(840Send(blahblah) != SUCCESS)
//      return FAILURE
//

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <rs232.h>
#include <userint.h>
#include <utility.h>

#include "perf.h"
#include "pts.h"
#include "globals.h"



/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     MeasureBarometricPressureLocal                                                   
/*                                                                           
/* Purpose:  To send pts 2000 command for Barometric pressure 
/*                                          
/*                                                                           
/* Example Call:  int MeasureBarometricPressureLocal(double *MeasuredVal);                                    
/*                                                                           
/* Input:   Double *         
/*                                                                           
/* Output:   <int>                                                           
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ERROR = NONE ZERO                                                       
/*                                                                           
/*****************************************************************************/
int MeasureBarometricPressureLocal(double *MeasuredVal)
{
	unsigned int reading;
	int b1, b2, b3;

	FlushInQPTS();

	if(SendPTS("B\r", 2) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte();
	b2 = RecvPTSByte();
	b3 = RecvPTSByte();
	if(b1 < 0 || b2 < 0 || b3 < 0){
		return PTS_ERROR;
	}

	reading = (unsigned int)b1;
	reading = ( (reading<<8) + ((unsigned int)(b2 & 0x00ff)) );
	*MeasuredVal = (double)reading / 100.0;

	// verify checksum
	if( ((unsigned char)b1 + ((unsigned char)b2 & 0x00ff)) == (unsigned char)b3){
		return PTS_OK;
	}
	else{
		return PTS_ERROR;
	}
}


/*****************************************************************************/
/* Name:     MeasurePressureSingle                                                       
/*                                                                           
/* Purpose:  To send pts 2000 command for low Barometric pressure reading 
/*          This function requires a external trigger.            
/*                                                                           
/* Example Call:   int MeasurePressureSingle(double *Reading, int TimeOut);                                    
/*                                                                           
/* Input:    <Double *>Reading , <int> time to expries         
/*                                                                           
/* Output:   <int>                                                           
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ERROR = NONE ZERO                                                        
/*                                                                           
/*****************************************************************************/
int MeasurePressureSingle(double *Reading, int TimeOut)
{
	int reading;
	int b1, b2, b3;
	int x;
	double timeoutval;

	if(SendPTS("SS5\r", 4) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte(); // ack or nak
	b2 = RecvPTSByte(); // CR
	b3 = RecvPTSByte(); // LF
	if(b1 < 0 || b2 < 0 || b3 < 0){
		return PTS_ERROR;
	}

	if(b1 == NAK){
		return PTS_ERROR;
	}

	if(SendPTS("STP+\r",5) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte(); // ack or nak
	b2 = RecvPTSByte(); //CR
	b3 = RecvPTSByte(); //LF
	if(b1 < 0 || b2 < 0 || b3 < 0){
		return PTS_ERROR;
	}

	if(b1 == NAK){
		return PTS_ERROR;
	}

	if(SendPTS("pB00001\r", 8) != SUCCESS){
		return PTS_ERROR;
	}

	FlushInQPTS();
	//********************************************************************
	//command issued and trigger armed now wait for trigger and data

	timeoutval = Timer() + TimeOut; // get current timer reading and add the requested time out
	while(!CharsInPTSQ()){
		ProcessSystemEvents();
		if(Timer() >= timeoutval){
			return PTS_ERROR;
		}
	}

	b1 = RecvPTSByte(); // get num of reading s/b 1
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte(); // get the reading s/b 1
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	reading = (unsigned int)b1;
	reading = ((reading<<8)+((unsigned int) b2&0x00ff));

	if(reading & 0x8000){
		*Reading = -1 * (double)(~reading&0x7fff) / 100.0;
	}
	else{
		*Reading = (double)(reading&0x7fff) / 100.0;
	}

	b1 = RecvPTSByte(); // CRC
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	return PTS_OK;
}



/*****************************************************************************/
/* Name:     SetupFlow                                                       
/*                                                                           
/* Purpose:  to prepare pte for a external trigger, To burst on a gas, for  
/*            predetermined time and rate                                    
/*                                                                           
/* Example Call:   int SetupFlow(int sampleRate, int burst, int gas);                                    
/*                                                                           
/* Input:  <int>sampleRate , <int> burst number ,<int> type of gas            
/*                                                                           
/* Output:   <int>                                                       
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:  ERROR = NONE ZERO                                                        
/*                                                                           
/*****************************************************************************/
int SetupFlow(int sampleRate, int burst, int gas)
{
	int ack, crc;
	int b1, b2, b3;
	char buff[20],reply[10];
	int outCount, i;

	buff[0] = '\0';

	sprintf(buff, "SS%d\r", sampleRate); // set sample rate
	outCount = strlen(buff);

	if(SendPTS(buff, outCount) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte();
	b2 = RecvPTSByte();
	b3 = RecvPTSByte();
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	FlushInQPTS();

	if(SendPTS("STF+\r",5) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte();
	b2 = RecvPTSByte();
	b3 = RecvPTSByte();
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	FlushInQPTS();

	sprintf(buff, "FB%d%d\r", gas, burst); // set high flow burst points
	outCount = strlen(buff);

	if(SendPTS(buff, outCount) != SUCCESS){
		return PTS_ERROR;
	}

	FlushInQPTS();

	return PTS_OK;
}


/*****************************************************************************/
/* Name:     RecieveReading                                                      
/*                                                                           
/* Purpose:  To retrieve points recorded from an armed viper 
/*                                          
/*                                                                           
/* Example Call:   int RecieveReading(double *reading, int *pointsReceived,int timeReq);                                    
/*                                                                           
/* Input:    <double *>reading , <int *>number of point recorded               
/*            <int> time to wait for pts                                
/*                                                                           
/* Output:   <int>                                                           
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ERROR = NONE ZERO                                                    
/*                                                                           
/*****************************************************************************/
int RecieveReading(double *reading, int *pointsReceived, int timeReq)
{
	int numberOfPoints;
	int b1, b2;
	double timeOut;
	BOOL timeFlag = FALSE;
	int readingTemp = 0;
	double temp[1000];
	int x;

	(*reading) = 0.0;

	timeFlag = FALSE;
	timeOut = Timer() + timeReq;
	while(!CharsInPTSQ() && timeFlag != TRUE){
		if(Timer() > timeOut){
			timeFlag = TRUE;
		}
	}

	if(timeFlag){ // if time out
		ComBreakPTS(255);
		FlushInQPTS();
		FlushOutQPTS();
		Delay(1.0);
		return( PTS_ERROR );
	}

	b1 = RecvPTSByte();
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	numberOfPoints = (unsigned int)b1;
	numberOfPoints = ( (numberOfPoints<<8) + ((unsigned int)(b2 & 0x00ff)) );

	// F L O W R E A D I N G
	for(x = 0; x < numberOfPoints; x++){
		//F L O W R E A D I N G

		b1 = RecvPTSByte();
		b2 = RecvPTSByte();
		if(b1 < 0 || b2 < 0){
			return PTS_ERROR;
		}

		readingTemp = (unsigned int)b1;
		readingTemp = ( (readingTemp<<8) + ((unsigned int)(b2 & 0x00ff)) );

		temp[x] =  (double)readingTemp / 100.0;
		(*reading) += (double)readingTemp / 100.0;
	}

	b1 = RecvPTSByte(); // CRC
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	(*reading) /= (double)numberOfPoints;
	(*pointsReceived) = numberOfPoints;

	return PTS_OK;
}


/*****************************************************************************/
/* Name:     SetupPressure                                                       
/*                                                                           
/* Purpose:  to prepare pts for a external trigger, To burst on a pressure 
/*            predetermined time and rate         
/*                                                                           
/* Example Call:  int SetupPressure(int sampleRate, int burst );                                    
/*                                                                           
/* Input:   <int>sampleRate , <int> burst number ,              
/*                                                                           
/* Output:   <int>                                                        
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ERROR = NONE ZERO                                                           
/*                                                                           
/*****************************************************************************/
int SetupPressure(int sampleRate, int burst )
{
	int ack, crc;
	int b1, b2, b3;
	char buff[20];
	char reply[10];
	int outCount, i;

	buff[0] = '\0';

	sprintf(buff, "SS%d\r", sampleRate); // set sample rate
	outCount = strlen(buff);

	if(SendPTS(buff, outCount) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte();
	b2 = RecvPTSByte();
	b3 = RecvPTSByte();
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	FlushInQPTS();

	if(SendPTS("STP+\r", 5) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte();
	b2 = RecvPTSByte();
	b3 = RecvPTSByte();
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	FlushInQPTS();

	sprintf(buff, "pB%d\r", burst); // set high flow burst points
	outCount = strlen(buff);

	if(SendPTS(buff, outCount) != SUCCESS){
		return PTS_ERROR;
	}

	FlushInQPTS();

	return PTS_OK;
}


/*****************************************************************************/
/* Name:     RecievePressureReading                                                       
/*                                                                           
/* Purpose:  To retrieve points recorded from an armed viper  
/*                                          
/*                                                                           
/* Example Call:   int RecievePressureReading(double *reading, int *pointsReceived,int timeReq);                                    
/*                                                                           
/* Input:    <double *>reading , <int *>number of point recorded              
/*            <int> time to wait for pts                     
/*                                                                           
/* Output:   <int>                                                           
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:  ERROR = NONE ZERO                                                             
/*                                                                           
/*****************************************************************************/
int RecievePressureReading(double *reading, int *pointsReceived, int timeReq)
{
	int numberOfPoints;
	int b1, b2;
	double timeOut;
	BOOL timeFlag = FALSE;
	int  readingTemp = 0;
	double readingPrime = 0.0;
	int  x;

	(*reading) = 0.0;

	timeFlag = FALSE;
	timeOut = Timer() + (double)timeReq;
	while(!CharsInPTSQ() && timeFlag != TRUE){
		if(Timer() > timeOut){
			timeFlag = TRUE;
		}
	}

	if(timeFlag){ // if time out
		ComBreakPTS(255);
		FlushInQPTS();
		FlushOutQPTS();
		Delay(1.0); 
		return PTS_ERROR;
	}

	b1 = RecvPTSByte();
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	numberOfPoints = (unsigned int)b1;
	numberOfPoints = ( (numberOfPoints<<8) + ((unsigned int)(b2 & 0x00ff)) );

	// F L O W R E A D I N G
	for(x = 0; x < numberOfPoints; x++){
		//F L O W R E A D I N G

		b1 = RecvPTSByte();
		b2 = RecvPTSByte();
		if(b1 < 0 || b2 < 0){
			return PTS_ERROR;
		}

		readingTemp = (unsigned int)b1;
		readingTemp = ((readingTemp<<8) + ((unsigned int)b2&0x00ff));

		if(readingTemp & 0x8000){
			readingPrime  = -1 * (double)(~readingTemp & 0x7fff) / 100.0;
		}
		else{
			readingPrime = (double)(readingTemp & 0x7fff) / 100.0;
		}

		(*reading) += readingPrime;
	}

	b1 = RecvPTSByte(); // CRC
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	(*reading) /= (double)numberOfPoints;
	(*pointsReceived) = numberOfPoints;

	return PTS_OK;
}


/*****************************************************************************/
/* Name:     MeasureFlowSingle                                                       
/*                                                                           
/* Purpose: To retrieve reading pts waits for external trigger  
/*                                          
/*                                                                           
/* Example Call:   int MeasureFlowSingle(double *Reading, int TimeOut));                                    
/*                                                                           
/* Input:    <double *>reading , <int >time to wait         
/*                                                                           
/* Output:   <int>                                                        
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:    ERROR = NONE ZERO                                                       
/*                                                                           
/*****************************************************************************/
int MeasureFlowSingle(double *Reading, int timeReq)
{
	int reading;
	int b1, b2, b3;
	double timeOut;
	int x;

	if(SendPTS("SS1\r", 5) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte(); // ack or nak
	b2 = RecvPTSByte(); // CR
	b3 = RecvPTSByte(); // LF
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	FlushInQPTS();
	if(b1 == NAK){
		return PTS_ERROR;
	}

	if(SendPTS("STF+\r", 5) != SUCCESS){  //t r i g __|-- +pos edge
		return PTS_ERROR;
	}

	b1 = RecvPTSByte();//ack or nak
	b2 = RecvPTSByte(); //CR
	b3 = RecvPTSByte(); //LF
	if(b1 < 0 || b2 < 0 | b3 < 0)
		return PTS_ERROR;

	FlushInQPTS();
	if(b1 == NAK){
		return PTS_ERROR;
	}

	FlushInQPTS();
	FlushOutQPTS();

	if(SendPTS("FB000020\r",9) != SUCCESS){
		return PTS_ERROR;
	}

	FlushInQPTS();
	//********************************************************************
	//command issued and trigger armed now wait for trigger and data

	timeOut = Timer() + timeReq; //get current timer reading and add the requested time out
	while(!CharsInPTSQ()){
		ProcessSystemEvents();
		if(Timer() >= timeOut){
			return PTS_ERROR;
		}
	}

	b1 = RecvPTSByte();        //get num of reading s/b 10
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	*Reading = 0.0;
	// F L O W R E A D I N G
	for(x = 0; x < 20; x++){
		// F L O W R E A D I N G

		b1 = RecvPTSByte();
		b2 = RecvPTSByte();
		if(b1 < 0 || b2 < 0){
			return PTS_ERROR;
		}

		reading = (unsigned int)b1;
		reading = ( (reading<<8) + ((unsigned int)(b2 & 0x00ff)) );

		*Reading += (double)reading / 100.0;
	}

	b1 = RecvPTSByte(); //CRC
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	*Reading /= 20.0;

	return PTS_OK;
}


/*****************************************************************************/
/* Name:     MeasureFlowSingleO2                                                        
/*                                                                           
/* Purpose:  To retrieve reading pts waits for external trigger
/*                                          
/*                                                                           
/* Example Call:   int MeasureFlowSingleO2(double *Reading, int TimeOut);                                    
/*                                                                           
/* Input:    <double *>reading , <int >time to wait          
/*                                                                           
/* Output:   <int>                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:  ERROR = NONE ZERO                                                          
/*                                                                           
/*****************************************************************************/
int MeasureFlowSingleO2(double *Reading, int timeReq)
{
	int b1, b2, b3;
	double timeOut;
	int reading;
	int x;

	if(SendPTS("SS1\r", 5) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte();//ack or nak
	b2 = RecvPTSByte(); //CR
	b3 = RecvPTSByte(); //LF
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	FlushInQPTS();

	if(b1 == NAK){
		return PTS_ERROR;
	}

	if(SendPTS("STF+\r", 5) != SUCCESS){  //t r i g __|-- +pos edge
		return PTS_ERROR;
	}

	b1 = RecvPTSByte();//ack or nak
	b2 = RecvPTSByte(); //CR
	b3 = RecvPTSByte(); //LF
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	FlushInQPTS();

	if(b1 == NAK){
		return PTS_ERROR;
	}

	if(SendPTS("FB100020\r", 9) != SUCCESS){
		return PTS_ERROR;
	}

	FlushInQPTS();
	//********************************************************************
	//command issued and trigger armed now wait for trigger and data

	timeOut = Timer() + timeReq; //get current timer reading and add the requested time out
	while(!CharsInPTSQ()){
		ProcessSystemEvents();
		if(Timer() >= timeOut){
			return PTS_ERROR;
		}
	}

	b1 = RecvPTSByte();//get num of reading s/b 10
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	*Reading = 0.0;
	// F L O W R E A D I N G
	for(x = 0; x < 20; x++){
		// F L O W R E A D I N G

		b1 = RecvPTSByte();
		b2 = RecvPTSByte();
		if(b1 < 0 || b2 < 0){
			return PTS_ERROR;
		}

		reading = (unsigned int)b1;
		reading = ( (reading<<8) + ((unsigned int)(b2 & 0x00ff)) );

		*Reading += (double)reading / 100.0;
	}

	b1 = RecvPTSByte(); // CRC
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	*Reading /= 20.0;

	return PTS_OK;
}


/*****************************************************************************/
/* Name:      MeasurePressureSingleOneSec                                                   
/*                                                                           
/* Purpose: To retrieve reading pts waits for external trigger          
/*            after trigger a reading is calcuated approx one second after   
/*            trigger                                         
/*                                                                           
/* Example Call:  int MeasurePressureSingleOneSec(double *Reading, int TimeOut);                                    
/*                                                                           
/* Input:    <double *>reading , <int >time to wait         
/*                                                                           
/* Output:   <int>                                                             
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ERROR = NONE ZERO                                                           
/*                                                                           
/*****************************************************************************/
int MeasurePressureSingleOneSec(double *Reading, int timeReq)
{
	int numberOfPoints;
	int b1, b2, b3;
	int reading;
	double timeOut;
	double avg;
	float samples[500];
	int x;

	if(SendPTS("SS10\r", 5) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte(); // ack or nak
	b2 = RecvPTSByte(); // CR
	b3 = RecvPTSByte(); // LF
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	if(b1 == NAK){
		return PTS_ERROR;
	}

	if(SendPTS("STP+\r", 5) != SUCCESS){ //t r i g __|-- +pos edge
		return PTS_ERROR;
	}

	b1 = RecvPTSByte(); // ack or nak
	b2 = RecvPTSByte(); //CR
	b3 = RecvPTSByte(); //LF
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	if(b1 == NAK){
		return PTS_ERROR;
	}

	if(SendPTS("pB00400\r", 8) != SUCCESS){
		return PTS_ERROR;
	}

	FlushInQPTS();
	//********************************************************************
	//command issued and trigger armed now wait for trigger and data

	timeOut = Timer() + timeReq; //get current timer reading and add the requested time out
	while(!CharsInPTSQ()){
		ProcessSystemEvents();
		if(Timer() >= timeOut){
			return PTS_ERROR;
		}
	}

	b1 = RecvPTSByte();//get num of reading
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	numberOfPoints = (unsigned int)b1;
	numberOfPoints = ( (numberOfPoints<<8) + ((unsigned int)(b2 & 0x00ff)) );

	avg = 0.0;
	for(x = 0; x < numberOfPoints; x++){
		b1 = RecvPTSByte(); // get num of reading
		b2 = RecvPTSByte();
		if(b1 < 0 || b2 < 0){
			return PTS_ERROR;
		}

		reading = (unsigned int)b1;
		reading = ( (reading<<8) + ((unsigned int)(b2 & 0x00ff)) );

		if(reading & 0x8000){
			samples[x] = -1 * (double)(~reading & 0x7fff) / 100.0;
		}
		else{
			samples[x] = (double)(reading & 0x7fff) / 100.0;
		}
	}

	*Reading = (double)samples[200]; // this will get us the reading 2 sec after start of breath

	b1 = RecvPTSByte(); // CRC
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	return PTS_OK;
}


/*****************************************************************************/
/* Name:    MeasurePressureSingleOneSec                                                    
/*                                                                           
/* Purpose:  To retrieve reading pts waits for external trigger          
/*            after trigger a reading is calcuated approx one second after   
/*            trigger                                       
/*                                                                           
/* Example Call:   int MeasurePressureSingleOneSecAfter(double *Reading, int TimeOut);                                    
/*                                                                           
/* Input:    <double *>reading , <int >time to wait         
/*                                                                           
/* Output:   <int>                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ERROR = NONE ZERO                                                        
/*                                                                           
/*****************************************************************************/
int MeasurePressureSingleOneSecAfter(double *Reading, int timeReq)
{
	int numberOfPoints;
	int b1, b2, b3;
	int reading;
	int x;
	double timeOut;
	double avg;
	float samples[500];

	if(SendPTS("SS10\r", 5) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte(); // ack or nak
	b2 = RecvPTSByte(); // CR
	b3 = RecvPTSByte(); // LF
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	if(b1 == NAK){
		return PTS_ERROR;
	}

	if(SendPTS("STP-\r", 5) != SUCCESS){  //t r i g --|__ -pos edge
		return PTS_ERROR;
	}

	b1 = RecvPTSByte(); // ack or nak
	b2 = RecvPTSByte(); // CR
	b3 = RecvPTSByte(); // LF
	if(b1 < 0 || b2 < 0 | b3 < 0){
		return PTS_ERROR;
	}

	if(b1 == NAK){
		return PTS_ERROR;
	}

	if(SendPTS("pB00200\r", 8) != SUCCESS){
		return PTS_ERROR;
	}

	FlushInQPTS();
	//********************************************************************
	//command issued and trigger armed now wait for trigger and data

	timeOut = Timer() + timeReq; //get current timer reading and add the requested time out
	while(!CharsInPTSQ()){
		ProcessSystemEvents();
		if(Timer() >= timeOut){
			return PTS_ERROR;
		}
	}

	b1 = RecvPTSByte(); // get num of reading
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	numberOfPoints = (unsigned int)b1;
	numberOfPoints = ( (numberOfPoints<<8) + ((unsigned int)(b2 & 0x00ff)) );

	avg = 0.0;
	for(x = 0; x < numberOfPoints; x++){
		b1 = RecvPTSByte();//get num of reading
		b2 = RecvPTSByte();
		if(b1 < 0 || b2 < 0){
			return PTS_ERROR;
		}

		reading = (unsigned int)b1;
		reading = ( (reading<<8) + ((unsigned int)(b2 & 0x00ff)) );

		if(reading & 0x8000){
			samples[x] = -1 * (double)(~reading & 0x7fff) / 100.0;
		}
		else{
			samples[x] = (double)(reading & 0x7fff) / 100.0;
		}
	}

	*Reading = (double)samples[150]; // this will get us the reading 1 sec after start of breath

	b1 = RecvPTSByte(); // CRC
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return PTS_ERROR;
	}

	return PTS_OK;
}


/*****************************************************************************/
/* Name:     MeasureOxygenLocal                                                     
/*                                                                           
/* Purpose:  To retrieve reading pts waits for external trigger   
/*                                                                           
/* Example Call:   int MeasureOxygenLocal(double *MeasuredVal);                                    
/*                                                                           
/* Input:   <double *>reading             
/*                                                                           
/* Output:    <int>                                                        
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:  ERROR = NONE ZERO                                                         
/*                                                                           
/*****************************************************************************/
int MeasureOxygenLocal(double *MeasuredVal)
{
	unsigned int reading;
	int b1, b2, b3;

	FlushInQPTS();

	if(SendPTS("O\r", 2) != SUCCESS){
		return PTS_ERROR;
	}

	b1 = RecvPTSByte();
	b2 = RecvPTSByte();
	b3 = RecvPTSByte();
	if(b1 < 0 || b2 < 0 || b3 < 0){
		return PTS_ERROR;
	}

	reading = (unsigned int)b1;
	reading = ( (reading<<8) + ((unsigned int)(b2 & 0x00ff)) );
	*MeasuredVal = (double)reading / 10.0;

	// verify checksum
	if( ((unsigned char)b1 + ((unsigned char)b2 & 0xff)) == (unsigned char)b3){
		return PTS_OK;
	}
	else{
		return PTS_ERROR;
	}
}

