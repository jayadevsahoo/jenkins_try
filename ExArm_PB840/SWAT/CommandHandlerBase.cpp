#include "stdafx.h"
////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
Name:		CommandHandlerBase.cpp

Purpose:
	Header file for CommandHandlerBase class.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 20-AUG-2013
PROJECT: e600 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/

#include "CommandHandlerBase.h"
#include "SWATAgent.h"

void CommandHandlerBase::BuildResponse()
{
	sprintf(resp_, "<ack_nak_resp><error>%d</error></ack_nak_resp>", this->err_);
}


CommandHandlerBase::CommandHandlerBase() 
    : err_( SWAT_SUCCESS )
    , PSWATService_( NULL ) 
{
	memset(resp_, 0, sizeof(resp_));
	PSWATService_ = SWATAgent::GetService();
	ASSERT( PSWATService_ );
}

CommandHandlerBase::~CommandHandlerBase()
{
}

void CommandHandlerBase::ProcessCommandBody(TiXmlElement* ele)
{
	ParseCommand( ele );

	// if command was parsed successfully, then we exectue the command
	// the EexecuteCommand method of child handler will be called because
	// EexecuteCommand in this class is a pure-virtual function
	if ( !HasError() )
	{
		EexecuteCommand();
	}

	
	if ( !HasError() )
	{
		// if no error, give child a chance to build response if child has
		// no implementation of BuildResponse, BuildResponse method of this 
		// class will be called to generate a ACK_NAK response.
		BuildResponse();
	}
	else
	{
		// EexecuteCommand may generate some error
		CommandHandlerBase::BuildResponse();
	}
}

