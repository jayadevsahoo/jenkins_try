// GetUserInputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Swat.h"
#include "GetUserInputDlg.h"


// CGetUserInputDlg dialog

IMPLEMENT_DYNAMIC(CGetUserInputDlg, CDialog)

CGetUserInputDlg::CGetUserInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGetUserInputDlg::IDD, pParent)
	, m_strUserInput(_T(""))
{

}

CGetUserInputDlg::~CGetUserInputDlg()
{
}

void CGetUserInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_USR_INPUT, m_strUserInput);
}


BEGIN_MESSAGE_MAP(CGetUserInputDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CGetUserInputDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CGetUserInputDlg message handlers



BOOL CGetUserInputDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_STATIC_USER_TITLE)->SetWindowTextA(m_strTitle);

	return TRUE;
}
void CGetUserInputDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here



	OnOK();
}
