#ifndef TaskControlAgent_HH
#define TaskControlAgent_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  TaskControlAgent - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskControlAgent.hhv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Modified to send TaskReadyMessages to a different queue
//         so TaskControl cann process them in a different context 
//         than other message so one transition completes before 
//         starting another.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 

//@ Usage-Classes
#include "Sys_Init.hh"
#include "SigmaState.hh"
#include "IpcIds.hh"
//@ End-Usage

class TaskControlMessage;
class ChangeStateMessage;

class TaskControlAgent
{
  public:

    static void   Initialize(void);
    static void   AddEvent(  TaskControlMessage* message
                           , Int32 queueNumber = TASK_CONTROL_Q );
    static void   ChangeState(SigmaState state);
    static void   ReportNodeReady(void);
    static void   ReportTaskReady(const ChangeStateMessage& rMessage);
    static void   SyncClocks(Boolean userInitiated = FALSE);
    static void   Update(void);
    static void   FailBd(void);

    static inline SigmaState  GetMyState(void);
    static inline SigmaState  GetTargetState(void);
    static inline SigmaState  GetPeerState(void);

    static inline SigmaState  GetGuiState(void);
    static inline SigmaState  GetBdState(void);

    static void     SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL,
			      const char*       pPredicate = NULL);

  private:
    TaskControlAgent();                             // declared only
    ~TaskControlAgent();                            // declared only

    TaskControlAgent(const TaskControlAgent&);      // declared only
    void operator=(const TaskControlAgent&);        // declared only

    //@ Data-Member:    GuiState_
    //  State of the GUI processor.
    static SigmaState  GuiState_;

    //@ Data-Member:    BdState_
    //  State of the BD processor.
    static SigmaState  BdState_;

    //@ Data-Member:    TargetState_
    //  Target state for the processor.
    static SigmaState  TargetState_;

};

#include "TaskControlAgent.in"

#endif // TaskControlAgent_HH
