#ifndef TrendDbSampleEvents_HH
#define TrendDbSampleEvents_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbSampleEvents
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSampleEvents.hhv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
// 
//=====================================================================



#include "TrendDbSampleData.hh"



class TrendDbSampleEvents : public TrendDbSampleData
{
public:    
	TrendDbSampleEvents( TrendDataMgr::TrendId trendId );
	virtual ~ TrendDbSampleEvents();
	virtual void compressData( TrendTimeScaleValue::TrendTimeScaleValueId timescale );
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	virtual void clear_( void );

private:
	TrendDbSampleEvents( const TrendDbSampleEvents& );	// not implemented
	void operator=( const TrendDbSampleEvents& );	// not implemented

	TrendValue  returnAndClear_( TrendTimeScaleValue::TrendTimeScaleValueId timescale );
	void        addToAcumm_( const TrendValue & value, 
							 TrendTimeScaleValue::TrendTimeScaleValueId timescale );

	//@ Data-Member: acummArray_
	// Array used to collect the event bits for a timescale
	Uint32  acummArray_[ TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES - 1 ];
};

#endif // TrendDbSampleEvents_HH
