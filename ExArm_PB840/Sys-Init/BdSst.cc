#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: BdSst - BD State: Online.
//---------------------------------------------------------------------
//@ Interface-Description
//  The BdSst class is a FsmState used by the BdFsm finite state
//  machine.  The class represents the wait state that BD Task Control
//  enters upon completing a BD transition to the SST state.  It
//  contains methods for processing events while in this state. 
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the BD SST state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  BdSst is derived from the FsmState class.  It overrides the
//  virtual method receiveGuiReady to provide state specific behavior
//  when it receives a GuiReadyMessage.  It overrides the virtual
//  method receiveCommUp to provide state specific behavior when it
//  receives a CommUpMessage.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/BdSst.ccv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "BdSst.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "TaskControlAgent.hh"
#include "GuiReadyMessage.hh"
#include "TaskAgentManager.hh"
#include "ChangeStateMessage.hh"
#include "BdInop.hh"
#include "BdOnline.hh"
#include "BdFsm.hh"
//TODO #include "Reset.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdSst [default constructor]
//
//@ Interface-Description
//  BdSst class default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdSst::BdSst()
{
    CALL_TRACE("BdSst()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdSst
//
//@ Interface-Description
//  BdSst class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
BdSst::~BdSst()
{
    CALL_TRACE("~BdSst()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
BdSst::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "BdSst";
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with 
//  this BdSst state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the SST state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
SigmaState
BdSst::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_SST;
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveCommUp
//
//@ Interface-Description
//  The receiveCommUp method processes a CommUpMessage received by BD
//  task control in the BdSst state.  It sends a BdReady-SST message
//  to GUI task control.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method invokes TaskControlAgent::ReportNodeReady to report
//  to the GUI that the BD is ready in the SST state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdSst::receiveCommUp(const CommUpMessage& ) const
{
    CALL_TRACE("receiveCommUp(const CommUpMessage&)");
    TaskControlAgent::ReportNodeReady();
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveGuiReady
//
//@ Interface-Description
//  The receiveGuiReady method processes a GuiReadyMessage received 
//  from GUI task control by BD task control in the BdSst state.  It
//  updates the local state of the GUI from the message.  It then
//  forwards the GuiReadyMessage to each application task if the
//  GUI is in the INIT or SST states.  Otherwise, the GUI and BD are
//  out of sync and this method asserts to restart the state machine
//  and resync the processors.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method invokes FsmState::SetGuiState to set the local state of
//  the GUI to the state contained in the GuiReadyMessage.  
//  It invokes TaskAgentManager::Send to forward the GuiReadyMessage 
//  to each managed task.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdSst::receiveGuiReady(const GuiReadyMessage& message) const
{
    CALL_TRACE("receiveGuiReady(const GuiReadyMessage&)");
 
    FsmState::SetGuiState(message);

    switch (message.getState())
    {
        case STATE_INIT:     // $[TI1]
        case STATE_ONLINE:
        case STATE_SST:
        case STATE_START:
            TaskAgentManager::Send(message);
            break;
        case STATE_SERVICE:  // $[TI2]
        case STATE_INOP:
        case STATE_TIMEOUT:
        default:
//TODO E600 port Reset   Reset::Initiate(INTENTIONAL, Reset::BDSST);
            break;
    }

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveChangeState
//
//@ Interface-Description
//  The receiveChangeState method processes a ChangeStateMessage 
//  received from the Breath Delivery subsystem by BD task control 
//  in the BdOnline state.  It changes the task control state from 
//  ONLINE to either INOP or SST based on the ChangeStateMessage.
//  Upon changing states, this method informs the application tasks
//  of the change and sends a BdReadyMessage to GUI task control.
//  The Breath Delivery subsystem queues the ChangeStateMessage
//  using the TaskControlAgent.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method changes the BD task control state to the state 
//  specified in the ChangeStateMessage for INOP and SST.  Other 
//  state change requests cause an assertion.  Upon changing state, 
//  this method invokes TaskAgentManager::ChangeState to inform managed
//  tasks of the transition.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdSst::receiveChangeState(const ChangeStateMessage& message) const
{
    CALL_TRACE("receiveChangeState(const ChangeStateMessage&)");
 
    switch (message.getState())
    {
        case STATE_INOP: // $[TI1]
            //BdFsm::GetBdInopState().startTransition();
            break;
        case STATE_ONLINE:  // $[TI2]

			// TODO E600 MS implement this 
			//BdTasks::SetIsSstRequired(ServiceMode::IsSstRequired());
			BdFsm::GetBdOnlineState().startTransition();
            break;
        default:  // $[TI3]
			break;
	}
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BdSst::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_BDSST, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

