// CaptureScreen840pc.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <errno.h>

#define DS_BITMAP_FILEMARKER  ((WORD) ('M' << 8) | 'B')    // is always "BM" = 0x4D42

void usage()
{
	MessageBox(NULL, 
		_T("CaptureScreen.exe [/x x] [/y y] [/w w] [/h h] [/f szFileName] [/?]\n")
		_T("Capture full or part of the screen into a bmp file.\n")
		_T("/x x,	x-coordinate of upper-left corner, should >=0 and <nFullScreenWidth, default is 0\n")
		_T("/y y,	y-coordinate of upper-left corner, should >=0 and <nFullScreenHeight, default is 0\n")
		_T("/w w,	width of rectangle, default is (nFullScreenWidth minus x) \n")
		_T("/h h,	height of rectangle, default is (nFullScreenHeight minus y) \n")
		_T("/f file,	bmp file name, default is CapScreen plus timestamp (CapScreenYYYYMMDD_HHmmSS.bmp)\n")
		_T("/?		Print this usage\n"),
		_T("usage"),
		MB_OK);	
}

void showError(TCHAR *szErr)
{
	TCHAR szMsg[MAX_PATH] = {0};
	_stprintf_s(szMsg, MAX_PATH, _T("%s, GetLastError: %d\n"), szErr, GetLastError());
	MessageBox(NULL, szMsg, _T("error"), MB_OK | MB_ICONERROR);
}

void ValidateParams(const int nFullScreenWidth, const int nFullScreenHeight,
				int &x, int &y, int &w, int &h, 
				const TCHAR *pBmpFileNameIn,
				TCHAR *szBmpFileNameOut)
{
	if(x < 0 || x >= nFullScreenWidth)
		x = 0;

	if(w < 1 || (x + w > nFullScreenWidth))
		w = nFullScreenWidth - x;

	if(y < 0 || y >= nFullScreenHeight)
		y = 0;

	if(h < 1 || (y + h > nFullScreenHeight))
		h = nFullScreenHeight - y;

	if(pBmpFileNameIn)
		_tcscpy_s(szBmpFileNameOut, MAX_PATH, pBmpFileNameIn);
	else
	{
		SYSTEMTIME  SystemTime;
		GetLocalTime(&SystemTime);

		_stprintf_s(szBmpFileNameOut, MAX_PATH, _T("CapScreen%04d%02d%02d_%02d%02d%02d.bmp"),
			SystemTime.wYear,
			SystemTime.wMonth,
			SystemTime.wDay,
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond);
	}
}

int SaveScreenToFile(int x = 0,
					 int y = 0,
					 int w = 0,
					 int h = 0,
					 const TCHAR *pBmpFileName = NULL
					 )
{
	int err = 0;
	int nFullScreenWidth = 0;
	int	nFullScreenHeight = 0;
	HDC hScrDC = NULL;
	HDC hMemDC = NULL;
	HGDIOBJ oldbmp = NULL;
	HBITMAP hBitmap = NULL;
	BYTE *lpBitmapBits = NULL; 
	try
	{
		hScrDC = ::GetDC(NULL);
		if(!hScrDC)
			throw _T("GetDC failed");

		//get screen info
		int nBitsPix = GetDeviceCaps(hScrDC, BITSPIXEL), nBytesPix = nBitsPix / 8;
		nFullScreenWidth = GetDeviceCaps(hScrDC, HORZRES);
		nFullScreenHeight = GetDeviceCaps(hScrDC, VERTRES);

		TCHAR szBmpFileName[MAX_PATH] = {0};//bmp file name, default is CapScreen plus timestamp (CapScreenYYYYMMDD_HHmmSS.bmp)
		ValidateParams(nFullScreenWidth, nFullScreenHeight,
			x, y, w, h,
			pBmpFileName, szBmpFileName);
		
		//init bitmap info header
		BITMAPINFO bi; 
		ZeroMemory(&bi, sizeof(BITMAPINFO));
		bi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		bi.bmiHeader.biWidth = w;
		bi.bmiHeader.biHeight = h;
		bi.bmiHeader.biPlanes = 1;
		bi.bmiHeader.biBitCount = nBitsPix;

		//copy the screen into a bitmap in memory
		hMemDC = ::CreateCompatibleDC(hScrDC); 
		if(!hMemDC)
			throw _T("CreateCompatibleDC failed");

		hBitmap = ::CreateDIBSection(hMemDC, &bi, DIB_RGB_COLORS, (LPVOID*)&lpBitmapBits, NULL, 0);
		if(!hBitmap)
			throw _T("CreateDIBSection failed");

		oldbmp = ::SelectObject(hMemDC, hBitmap); 
		if(!oldbmp)
			throw _T("SelectObject failed");

		if(0 == ::BitBlt(hMemDC, 0, 0, w, h, hScrDC, x, y, SRCCOPY))
			throw _T("BitBlt failed");

		//init bmp file header
		BITMAPFILEHEADER bh;
		ZeroMemory(&bh, sizeof(BITMAPFILEHEADER));
		bh.bfType = DS_BITMAP_FILEMARKER;
		bh.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
		bh.bfSize = bh.bfOffBits + (w*h)* nBytesPix;

		//write bmp file
		HANDLE file = CreateFile(szBmpFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if(INVALID_HANDLE_VALUE == file)
			throw _T("CreateFile failed");

		DWORD dwBytesWriten;
		WriteFile(file, &bh, sizeof(BITMAPFILEHEADER), &dwBytesWriten, NULL);
		WriteFile(file, &(bi.bmiHeader), sizeof(BITMAPINFOHEADER), &dwBytesWriten, NULL);
		WriteFile(file, lpBitmapBits, nBytesPix * w * h, &dwBytesWriten, NULL);
		CloseHandle(file);
	}
	catch(TCHAR *szErr)
	{
		showError(szErr);
	}

	if(oldbmp)
		::SelectObject(hMemDC, oldbmp);
	if(hBitmap)
		::DeleteObject(hBitmap);
	if(hMemDC)
		::DeleteObject(hMemDC);
	if(hScrDC)
		::ReleaseDC(NULL, hScrDC);

	return err;
}

int captureScreenArea(int x,
					  int y,
					  int w,
					  int h,
					  const TCHAR *pBmpFileName
					  )
{
	return SaveScreenToFile(x, y, w, h, pBmpFileName);
}

int captureFullScreen(const TCHAR *pBmpFileName)
{
	return captureScreenArea(0, 0, 0, 0, pBmpFileName);
}

int WINAPI wWinMain(HINSTANCE hInstance, 
  HINSTANCE hPrevInstance, 
  LPTSTR lpCmdLine, 
  int nShowCmd 
  )
{
	int err = 0;

//#define TEST_API

#ifdef TEST_API
	captureFullScreen(_T("bb.bmp"));
	captureScreenArea(200, 200, 200, 200, _T("cc.bmp"));
#else
	int x = 0;//x-coordinate of upper-left corner, default is 0
	int y = 0;//y-coordinate of upper-left corner, default is 0
	int w = 0;//width of rectangle
	int h = 0;//height of rectangle, default is (nFullScreenHeight � y)
	const TCHAR *pBmpFileName = NULL;
	int i = 1;

	try
	{
		TCHAR *szNextArg = NULL;

		while(i < __argc)
		{
			szNextArg = __wargv[i++];
			if(0 == _tcscmp(szNextArg, _T("/x")))
			{
				if(i == __argc)
					throw _T("no x");
				x = _ttoi(__wargv[i++]);
			}
			else if(0 == _tcscmp(szNextArg, _T("/y")))
			{
				if(i == __argc)
					throw _T("no y");
				y = _ttoi(__wargv[i++]);
			}
			else if(0 == _tcscmp(szNextArg, _T("/w")))
			{
				if(i == __argc)
					throw _T("no w");
				w = _ttoi(__wargv[i++]);
			}
			else if(0 == _tcscmp(szNextArg, _T("/h")))
			{
				if(i == __argc)
					throw _T("no h");
				h = _ttoi(__wargv[i++]);
			}
			else if(0 == _tcscmp(szNextArg, _T("/f")))
			{
				if(i == __argc)
					throw _T("no file name");
				pBmpFileName = __wargv[i++];
			}
			//else if(0 == _tcscmp(szNextArg, _T("/?"))) //fallthrough
			else 
			{				
				throw _T("");
			}
		}

		err = SaveScreenToFile(x, y, w, h, pBmpFileName);
	}	
	catch(TCHAR *szErr)
	{
		if(szErr[0])
			showError(szErr);
		usage();
	}
#endif

	return err;
}

