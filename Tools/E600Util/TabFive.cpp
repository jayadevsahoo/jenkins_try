// TabFive.cpp : implementation file
//

#include "stdafx.h"
#include "E600Util.h"
#include "TabFive.h"
#include "GlobalObjects.h"
#include "UserMessages.h"

// TabFive dialog

IMPLEMENT_DYNAMIC(CTabFive, CDialog)

CTabFive::CTabFive(CWnd* pParent /*=NULL*/)
	: CDialog(CTabFive::IDD, pParent)
{
	GainFilePath = L"";
}

CTabFive::~CTabFive()
{
}

void CTabFive::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_EDIT_PC_KI, CEditGains[PC_KI]);
	DDX_Control(pDX, IDC_EDIT_PC_KP, CEditGains[PC_KP]);
	DDX_Control(pDX, IDC_EDIT_FC_AFACTOR, CEditGains[FC_AFACTOR]);
	DDX_Control(pDX, IDC_EDIT_FC_BFACTOR, CEditGains[FC_BFACTOR]);
	DDX_Control(pDX, IDC_EDIT_FC_CFACTOR, CEditGains[FC_CFACTOR]);
	DDX_Control(pDX, IDC_EDIT_FC_DFACTOR, CEditGains[FC_DFACTOR]);
	DDX_Control(pDX, IDC_EDIT_EV_KI, CEditGains[EV_KI]);
	DDX_Control(pDX, IDC_EDIT_EV_KP, CEditGains[EV_KP]);
	DDX_Control(pDX, IDC_EDIT_PEEP_KI, CEditGains[PEEP_KI]);
	DDX_Control(pDX, IDC_EDIT_PID_KI, CEditGains[PID_KI]);
	DDX_Control(pDX, IDC_EDIT_PID_KP, CEditGains[PID_KP]);
	DDX_Control(pDX, IDC_EDIT_PID_KD, CEditGains[PID_KD]);
	DDX_Control(pDX, IDC_EDIT_PBP_KI, CEditGains[PBP_KI]);
	DDX_Control(pDX, IDC_EDIT_PBP_KP, CEditGains[PBP_KP]);

	DDX_Control(pDX, IDC_EDIT_DB_VAR1, CEditWriteDebugs[0]);
	DDX_Control(pDX, IDC_EDIT_DB_VAR2, CEditWriteDebugs[1]);
	DDX_Control(pDX, IDC_EDIT_DB_VAR3, CEditWriteDebugs[2]);
	DDX_Control(pDX, IDC_EDIT_DB_VAR4, CEditWriteDebugs[3]);
	DDX_Control(pDX, IDC_EDIT_DB_VAR5, CEditWriteDebugs[4]);

	DDX_Control(pDX, IDC_EDIT_DB_VAR6, CEditReadDebugs[0]);
	DDX_Control(pDX, IDC_EDIT_DB_VAR7, CEditReadDebugs[1]);
	DDX_Control(pDX, IDC_EDIT_DB_VAR8, CEditReadDebugs[2]);
	DDX_Control(pDX, IDC_EDIT_DB_VAR9, CEditReadDebugs[3]);
	DDX_Control(pDX, IDC_EDIT_DB_VAR10, CEditReadDebugs[4]);	
}


BEGIN_MESSAGE_MAP(CTabFive, CDialog)
	
	ON_BN_CLICKED(IDC_BUTTON_PC_UPDATE, &CTabFive::OnBnClickedButtonPcUpdate)
	ON_BN_CLICKED(IDC_BUTTON_FC_UPDATE, &CTabFive::OnBnClickedButtonFcUpdate)
	ON_BN_CLICKED(IDC_BUTTON_EV_UPDATE, &CTabFive::OnBnClickedButtonEvUpdate)
	ON_BN_CLICKED(IDC_BUTTON_PEEP_UPDATE, &CTabFive::OnBnClickedButtonPeepUpdate)
	ON_BN_CLICKED(IDC_BUTTON_PID_UPDATE, &CTabFive::OnBnClickedButtonPidUpdate)
	ON_BN_CLICKED(IDC_BUTTON_ALL_GAINS_UPDATE, &CTabFive::OnBnClickedButtonAllGainsUpdate)
	ON_BN_CLICKED(IDC_BUTTON_ALL_GAINS_READ, &CTabFive::OnBnClickedButtonAllGainsRead)

	ON_MESSAGE(WM_ALL_GAINS_RX, &CTabFive::OnAllGainsReceived)

	ON_BN_CLICKED(IDC_BUTTON_PBP_UPDATE, &CTabFive::OnBnClickedButtonPbpUpdate)
	ON_BN_CLICKED(IDC_BUTTON_LOAD_GAIN_FILE, &CTabFive::OnBnClickedButtonLoadGainFile)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_GAIN_FILE, &CTabFive::OnBnClickedButtonSaveGainFile)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_NEW_GAIN_FILE, &CTabFive::OnBnClickedButtonSaveNewGainFile)
	ON_BN_CLICKED(IDC_BUTTON_ALL_GAINS_UPDATE2, &CTabFive::OnBnClickedButtonEnableGainSetting)
	ON_BN_CLICKED(IDC_BUTTON_SET_DB, &CTabFive::OnBnClickedButtonSetDb)
END_MESSAGE_MAP()


//**********************************************************************************************//
// TabFive message handlers
BOOL CTabFive::OnInitDialog()
{
	CDialog::OnInitDialog();

	return FALSE;
}

//**********************************************************************************************//
LRESULT CTabFive::OnAllGainsReceived(WPARAM x, LPARAM y)
{
	ALL_GainsPacket_t *pPacket = (ALL_GainsPacket_t *)y;
	ALL_GainsStruct_t AllGains;
	CString Str;

	if(x == sizeof(ALL_GainsStruct_t))
	{
		AllGains = pPacket->data;

		Str.Format(L"%f", AllGains.PcGains.ki); 
		CEditGains[PC_KI].SetWindowTextW(Str);
		Str.Format(L"%f", AllGains.PcGains.kp); 
		CEditGains[PC_KP].SetWindowTextW(Str);

		Str.Format(L"%f", AllGains.FcFactors.aFactor); 
		CEditGains[FC_AFACTOR].SetWindowTextW(Str);
		Str.Format(L"%f", AllGains.FcFactors.bFactor); 
		CEditGains[FC_BFACTOR].SetWindowTextW(Str);
		Str.Format(L"%f", AllGains.FcFactors.cFactor); 
		CEditGains[FC_CFACTOR].SetWindowTextW(Str);
		Str.Format(L"%f", AllGains.FcFactors.dFactor); 
		CEditGains[FC_DFACTOR].SetWindowTextW(Str);

		Str.Format(L"%f", AllGains.EvGains.ki); 
		CEditGains[EV_KI].SetWindowTextW(Str);
		Str.Format(L"%f", AllGains.EvGains.kp); 
		CEditGains[EV_KP].SetWindowTextW(Str);

		Str.Format(L"%f", AllGains.PeepGains.ki); 
		CEditGains[PEEP_KI].SetWindowTextW(Str);

		Str.Format(L"%f", AllGains.PidGains.ki); 
		CEditGains[PID_KI].SetWindowTextW(Str);
		Str.Format(L"%f", AllGains.PidGains.kp); 
		CEditGains[PID_KP].SetWindowTextW(Str);
		Str.Format(L"%f", AllGains.PidGains.kd); 
		CEditGains[PID_KD].SetWindowTextW(Str);
	}
	else
	{
		TRACE(L"Size of All-gain packet is invalid, %d", x);
	}
	return 0;
}


//**********************************************************************************************//
void CTabFive::SendPacketToBD(char *pBuff, int Size)
{
	//pTabOne->SendMessage(WM_GAIN_CMD, Size, (LPARAM)pBuff);
	pTabOne->SendPacketToBD(pBuff, Size);

	//BdTxPacket.SendBdPacket(pBuff, Size);
}

//**********************************************************************************************//
void CTabFive::OnBnClickedButtonPcUpdate()
{
	PC_GainsStruct_t PcGains;
	PC_GainsPacket_t Packet;

	CString Str;
	CEditGains[PC_KI].GetWindowText(Str);
	PcGains.ki = (float)_wtof(Str);
	CEditGains[PC_KP].GetWindowText(Str);
	PcGains.kp = (float)_wtof(Str);

	Packet.messageId = PC_SET_PC_GAIN_MSG_ID;
	Packet.dataSize = sizeof(PC_GainsStruct_t);
	Packet.data = PcGains;

	SendPacketToBD((char *)&Packet, sizeof(PC_GainsPacket_t));
}


//**********************************************************************************************//
void CTabFive::OnBnClickedButtonFcUpdate()
{
	FC_FactorsStruct_t FcFactors;
	FC_FactorsPacket_t Packet;

	CString Str;
	CEditGains[FC_AFACTOR].GetWindowText(Str);
	FcFactors.aFactor = (float)_wtof(Str);
	CEditGains[FC_BFACTOR].GetWindowText(Str);
	FcFactors.bFactor = (float)_wtof(Str);
	CEditGains[FC_CFACTOR].GetWindowText(Str);
	FcFactors.cFactor = (float)_wtof(Str);
	CEditGains[FC_DFACTOR].GetWindowText(Str);
	FcFactors.dFactor = (float)_wtof(Str);

	Packet.messageId = PC_SET_FC_GAIN_MSG_ID;
	Packet.dataSize = sizeof(FC_FactorsStruct_t);
	Packet.data = FcFactors;

	SendPacketToBD((char *)&Packet, sizeof(FC_FactorsPacket_t));
}

void CTabFive::OnBnClickedButtonEvUpdate()
{
	EVC_GainsStruct_t EvcGains;
	EVC_GainsPacket_t Packet;

	CString Str;
	CEditGains[EV_KI].GetWindowText(Str);
	EvcGains.ki = (float)_wtof(Str);
	CEditGains[EV_KP].GetWindowText(Str);
	EvcGains.kp = (float)_wtof(Str);

	Packet.messageId = PC_SET_EV_GAIN_MSG_ID;
	Packet.dataSize = sizeof(EVC_GainsStruct_t);
	Packet.data = EvcGains;

	SendPacketToBD((char *)&Packet, sizeof(EVC_GainsPacket_t));
}

void CTabFive::OnBnClickedButtonPeepUpdate()
{
	PEEP_GainsStruct_t PeepGains;
	PEEP_GainsPacket_t Packet;

	CString Str;
	CEditGains[PEEP_KI].GetWindowText(Str);
	PeepGains.ki = (float)_wtof(Str);

	Packet.messageId = PC_SET_PEEP_GAIN_MSG_ID;
	Packet.dataSize = sizeof(PEEP_GainsStruct_t);
	Packet.data = PeepGains;

	SendPacketToBD((char *)&Packet, sizeof(PEEP_GainsPacket_t));
}

void CTabFive::OnBnClickedButtonPidUpdate()
{
	PID_GainsStruct_t PidGains;
	PID_GainsPacket_t Packet;

	CString Str;
	CEditGains[PID_KI].GetWindowText(Str);
	PidGains.ki = (float)_wtof(Str);
	CEditGains[PID_KP].GetWindowText(Str);
	PidGains.kp = (float)_wtof(Str);
	CEditGains[PID_KD].GetWindowText(Str);
	PidGains.kd = (float)_wtof(Str);

	Packet.messageId = PC_SET_PID_GAIN_MSG_ID;
	Packet.dataSize = sizeof(PID_GainsStruct_t);
	Packet.data = PidGains;

	SendPacketToBD((char *)&Packet, sizeof(PID_GainsPacket_t));
}


void CTabFive::OnBnClickedButtonPbpUpdate()
{
	PBP_GainsStruct_t PbpGains;
	PBP_GainsPacket_t Packet;

	CString Str;
	CEditGains[PBP_KI].GetWindowText(Str);
	PbpGains.ki = (float)_wtof(Str);
	CEditGains[PBP_KP].GetWindowText(Str);
	PbpGains.kp = (float)_wtof(Str);

	Packet.messageId = PC_SET_PBP_GAIN_MSG_ID;
	Packet.dataSize = sizeof(PBP_GainsStruct_t);
	Packet.data = PbpGains;

	SendPacketToBD((char *)&Packet, sizeof(PBP_GainsPacket_t));
}


void CTabFive::OnBnClickedButtonAllGainsUpdate()
{
	OnBnClickedButtonPcUpdate();
	OnBnClickedButtonFcUpdate();
	OnBnClickedButtonEvUpdate();
	OnBnClickedButtonPeepUpdate();
	OnBnClickedButtonPidUpdate();
	OnBnClickedButtonPbpUpdate();
}

void CTabFive::OnBnClickedButtonAllGainsRead()
{
	GainRequestPacket_t Packet;

	Packet.messageId = PC_GET_ALL_GAINS_MSG_ID;
	Packet.dataSize = 0;

	SendPacketToBD((char *)&Packet, sizeof(GainRequestPacket_t));
}


void CTabFive::OnBnClickedButtonLoadGainFile()
{
	CFileDialog FileDlg(TRUE, L".txt", L"", 0, 
		L"Text Files (*.txt)|*.txt| All Files (*.*)|*.*||", NULL);
	if(FileDlg.DoModal() == IDOK)
	{
		GainFilePath = FileDlg.GetPathName();
		CEdit* pEdit = reinterpret_cast<CEdit *>(GetDlgItem(IDC_EDIT_TUNE_FNAME1));
		pEdit->SetWindowTextW(GainFilePath);

		if(GainFile.Open(GainFilePath, CFile::modeRead | CFile::typeText))
		{
			CString Str;			
			GainFile.ReadString(Str);
			Str.Trim();
			if(Str.Compare(L":PressureController") == 0)
			{
				GainFile.ReadString(Str);
				Str.Trim();
				CEditGains[PC_KI].SetWindowText(Str);
				GainFile.ReadString(Str);
				Str.Trim();
				CEditGains[PC_KP].SetWindowText(Str);
			}

			GainFile.Close();
		}
		else
		{
			MessageBox(L"Failed open gain file", L"Error", MB_ICONERROR);
		}
	}
}


void CTabFive::SaveGainFile()
{
	if(GainFile.Open(GainFilePath, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		GainFile.WriteString(L":PressureController\n");
		CString Str;
		CEditGains[PC_KI].GetWindowText(Str);
		Str += L"\n";
		GainFile.WriteString((LPCTSTR)Str);
		CEditGains[PC_KP].GetWindowText(Str);
		Str += L"\n";
		GainFile.WriteString((LPCTSTR)Str);

		GainFile.Close();
	}
	else
	{
		MessageBox(L"Failed open gain file", L"Error", MB_ICONERROR);
	}
}

void CTabFive::OnBnClickedButtonSaveGainFile()
{
	int Match = GainFilePath .Compare(L"");
	if(Match == 0)
	{
		OnBnClickedButtonSaveNewGainFile();
	}
	else
	{
		SaveGainFile();
	}
}

void CTabFive::OnBnClickedButtonSaveNewGainFile()
{
	CFileDialog FileDlg(FALSE, L".txt", L"", 0, 
		L"Text Files (*.txt)|*.txt| All Files (*.*)|*.*||", NULL);
	if(FileDlg.DoModal() == IDOK)
	{
		GainFilePath = FileDlg.GetPathName();
		CEdit* pEdit = reinterpret_cast<CEdit *>(GetDlgItem(IDC_EDIT_TUNE_FNAME1));
		pEdit->SetWindowTextW(GainFilePath);

		SaveGainFile();
	}
}

void CTabFive::OnBnClickedButtonEnableGainSetting()
{
	GainEnablePacket_t Packet;
	Packet.messageId = PC_ENABLE_GAIN_MSG_ID;
	Packet.dataSize = 0;

	SendPacketToBD((char *)&Packet, sizeof(GainEnablePacket_t));
}

void CTabFive::OnBnClickedButtonSetDb()
{
	WriteDebugPacket_t Packet;
	Packet.messageId = PC_WRITE_DEBUG_MSG_ID;

	for(int i=0; i<NUM_WRITE_DEBUG_VARS; i++)
	{
		CString Str;
		CEditWriteDebugs[i].GetWindowText(Str);
		Packet.debugArr[i].floatValue = (float)_wtof(Str);
	}
	Packet.dataSize = sizeof(Packet.debugArr);

	SendPacketToBD((char *)&Packet, sizeof(WriteDebugPacket_t));
}
