#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LargeContainer - A Container that includes a larger
//  Drawables list than the Drawables list in the Container base class.
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/LargeContainer.ccv   25.0.4.0   19 Nov 2013 14:11:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date:  23-Jan-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Initial version.
//
//=====================================================================

#include "LargeContainer.hh"

//@ End-Usage

//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LargeContainer [constructor]
//
//@ Interface-Description
//  Default constructor which initializes a LargeContainer object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes a reference to this class's large Drawables list to 
//  the Container base class constructor where the list is used and 
//  managed.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LargeContainer::LargeContainer(void) :
	Container(children_)
{
	CALL_TRACE("LargeContainer(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LargeContainer [destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LargeContainer::~LargeContainer(void)
{
	CALL_TRACE("~LargeContainer(void)");
}

