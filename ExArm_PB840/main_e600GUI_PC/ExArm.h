// ExArm.h : main header file for the EXARM application
//

#if !defined(AFX_EXARM_H__F8276317_156B_4513_A3DF_1F35D6CE4CF2__INCLUDED_)
#define AFX_EXARM_H__F8276317_156B_4513_A3DF_1F35D6CE4CF2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

//#include "resource.h"		// main symbols
//#include "TopLevelWindow.h"

/////////////////////////////////////////////////////////////////////////////
// CExArmApp:
// See ExArm.cpp for the implementation of this class
//

class CExArmApp : public CWinApp
{
public:
	CExArmApp();
	CDC* ptrLowerCDC_; 
	CDC* ptrUpperCDC_; 
	
	void InitializeLowerViewCDC();   
	void InitializeUpperViewCDC();	 
    void GUISystemInitalization(void);
private:
    BOOL CreateFrmView();  
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExArmApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CExArmApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

    protected:
		//TopLevelWindow_t *TopLevelWindow;
        void ConditionallyUpdateStartup();
public:
    virtual int Run();
	virtual BOOL OnIdle(LONG lCount);
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXARM_H__F8276317_156B_4513_A3DF_1F35D6CE4CF2__INCLUDED_)
