#include "stdafx.h"
#ifdef SIGMA_GUI_CPU
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmMsg - A container for displaying the main alarm messages.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains textual fields and a line object.  This
// class provides a public updateDisplay() method, which contains all of
// the required display processing code.
//
// This class relies on alarm data provided by the AlarmUpdate interface
// object of the Alarms subsystem.  The details of rendering each piece
// of text in an alarm "slot" are handled by the AlarmRender class.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates all of the graphics, logic, and processing
// required to display a main alarm message (i.e., an "alarm
// slot").  Having common code avoids the duplication of code that
// would occur if the AlarmAndStatusArea and the MoreAlarmsSubScreen
// performed their own low-level alarm display processing.  Having a
// common, high-level alarm message object also dramatically simplifies
// the display of multiple alarms in both the AlarmAndStatusArea and the
// MoreAlarmsSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// An alarm message consists of two major parts -- a "base message"
// (aka, root cause message) and an "analyis/remedy message".  The
// base message is displayed with a large font as a single-line or
// dual-line phrase/symbol in the left hand side of the alarm slot.  The
// analysis/remedy message consists of one or more "phrases" which are
// displayed in the right hand side of the alarm slot.  Each phrase
// consists of one or more words/symbols.  The analysis/remedy message
// is displayed with a smaller font using up to three lines as
// necessary.
//
// Because of the varied display requirements, this class contains many
// TextField objects.  Each single/dual-line base message configuration
// and each single/double/triple-line analysis/remedy message
// configuration have dedicated text objects to support each
// configuration.  This class also contains one Line object, which acts
// as a graphical separator between adjacent alarm messages.  At
// construction time, all container objects are created, positioned, and
// added to the container as hidden objects.  When an alarm occurs, the
// updateDisplay() method configures the display based on the
// requirements of the particular alarm message.
//
// The base message portion of the display is rendered on one line
// (rather than two lines) whenever possible.  If the base message
// consists of a single alarm symbol, then that symbol is made touchable
// so that a translation message can be displayed in the lower screen
// MessageArea.
//
// Whenever possible, the analysis/remedy message portion of the display
// is rendered such that up to three "alarm phrases" are each
// displayed on their own line.  If there are more than 3 phrases or if
// a phrase is too long to fit on a line by itself, then the phrases are
// packed onto as few lines as possible using word wrapping.
//---------------------------------------------------------------------
//@ Fault-Handling
// Usual use of asserts to verify the validity of arguments and correctness
// of the code.
//---------------------------------------------------------------------
//@ Restrictions
// The base message portion and the analysis/remedy message portion of
// the display must fit within their allotted display areas.  If the
// rendering code detects that a particular message does not fit within
// the required space, assertions will fire.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmMsg.ccv   25.0.4.0   19 Nov 2013 14:07:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision:  004    By:  hhd	 Date:  04-10-1997      DR Number: 2521 
//  Project:    Sigma (R8027)
//  Description:
//		Fixed problem with English text being displayed in smaller font unnecessarily, 
//     	causing vent to reset.
//
//  Revision:  003  By: hhd   Date:  30-July-997      DR Number: DCS 2265
//    Project:    Sigma (R8027)
//    Description:
//		   Changed column widths for foreign language version
//			of the Alarm messages.
// 
//  Revision: 002  By:  sah    Date:  28-Jul-1997    DCS Number: 2320
//  Project:  Sigma (R8027)
//  Description:
//      Now using 'AlarmStateManger' for retrieving the highest current
//		alarm urgency.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "Colors.hh"
#include "AlarmMsg.hh"
#include "OsUtil.hh"

// Sigma includes.
#include "AlarmAnnunciator.hh"
#include "AlarmUpdate.hh"

// GUI-App includes.
#include "AlarmRender.hh"

//@ Usage-Classes
#include "AlarmStateManager.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_WIDTH_ = 640;
static const Int32 CONTAINER_HEIGHT_ = 44;
static const Int32 SEPARATOR_X1_ = 0;
static const Int32 SEPARATOR_X2_ = 639;
static const Int32 SEPARATOR_Y_ = 43;
static const Int32 BASE_MSG_MARGIN_ = 6;
static const Int32 ANALYSIS_MSG_X_ = 243;

static const Int32 BASE_MSG_MAX_WIDTH_ = 235; 
static const Int32 ANALYSIS_MSG_MAX_WIDTH_ = 392; 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmMsg()  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects.  Adds all graphical objects to
// the parent container for display.  All text objects are initially
// hidden.
//
// $[01136] Alarm messages should be distinguishable from status data
// $[01137] Alarm analysis messages shall be displayed
// $[01139] Each alarm analysis message shall contain the following ...
// $[01189] Alarm analysis messages in More Alarms should be the same as ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmMsg::AlarmMsg(void) :
	baseMsg1_(NULL),
	baseMsg2_(NULL),
	analysisMsg1_(NULL),
	analysisMsg2_(NULL),
	analysisMsg3_(NULL),
	alarmSeparator_(SEPARATOR_X1_, SEPARATOR_Y_, SEPARATOR_X2_, SEPARATOR_Y_)
{
	CALL_TRACE("AlarmMsg::AlarmMsg(void)");

    // Size and position the area
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Set color
	setColor(Colors::BLACK);
	setFillColor(Colors::BLACK);

	// Setup the alarm separator line, then add it to the collection.
	alarmSeparator_.setColor(Colors::WHITE);
	addDrawable(&alarmSeparator_);

	// Setup base message drawables
	baseMsg1_.setColor(Colors::WHITE);
	baseMsg2_.setColor(Colors::WHITE);

	addDrawable(&baseMsg1_);
	addDrawable(&baseMsg2_);

	// Setup analysis message drawables
	analysisMsg1_.setX(ANALYSIS_MSG_X_);
	analysisMsg1_.setColor(Colors::WHITE);

	analysisMsg2_.setX(ANALYSIS_MSG_X_);
	analysisMsg2_.setColor(Colors::WHITE);

	analysisMsg3_.setX(ANALYSIS_MSG_X_);
	analysisMsg3_.setColor(Colors::WHITE);
	addDrawable(&analysisMsg1_);
	addDrawable(&analysisMsg2_);
	addDrawable(&analysisMsg3_);						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmMsg  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmMsg::~AlarmMsg(void)
{
	CALL_TRACE("AlarmMsg::~AlarmMsg(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay
//
//@ Interface-Description
// This routine fetches alarm message data from the Alarms subsystem
// through the provided 'alarmUpdate' parameter, renders the text, and
// configures the display based on how many lines were required to
// render the text.  AlarmUpdate objects are interface objects which
// provide the message id's which make up the alarm message.
//---------------------------------------------------------------------
//@ Implementation-Description
// Fetches the message id's from the AlarmUpdate object then passes the
// message id's and the text objects to the AlarmRender methods to
// render the message id's as displayable text.  The low-level rendering
// details are handled by the AlarmRender methods.  Once the text has
// been rendered, the text fields which contain the rendered text are
// made visible.
//
// The base message portion and the analysis/remedy message portion are
// handled separately.  For the base message, the urgency associated
// with the alarm determines the flash rate of the base message.
//
// $[01141] Root cause portion of medium/high alarms flash at LED rate ...
// $[01142] Alarm LED shall flash synchronously with root cause message(s).
//---------------------------------------------------------------------
//@ PreCondition
// The rendered alarm message text must fit within the allowed areas.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
AlarmMsg::updateDisplay(const AlarmUpdate& alarmUpdate)
{
	CALL_TRACE("AlarmMsg::updateDisplay(alarmUpdate)");

#if BTREE_TEST
	static const Boolean IS_COLOR = TRUE;
#else
	static const Boolean IS_COLOR = IsUpperDisplayColor();
#endif

	const Int32	MAX_MSG_IDS = 20;
	Int32 	messageIds[MAX_MSG_IDS];	// null-terminated array of MessageName id's
	 
	wchar_t	tmpBuffer1[TextField::MAX_STRING_LENGTH + 1];
	wchar_t	tmpBuffer2[TextField::MAX_STRING_LENGTH + 1];
	wchar_t	tmpBuffer3[TextField::MAX_STRING_LENGTH + 1];

	// Put the base message id in the id array.
	messageIds[0] = alarmUpdate.getBaseMessage();
	messageIds[1] = MESSAGE_NAME_NULL;

	// Assume that the single-line base message is not touchable.
	baseMsg1_.setMessage(NULL_STRING_ID);

	// Terminate buffers in case the alarm is not rendered - AlarmRender
	// does not write to the output buffers if the string will not fit
	tmpBuffer1[0] = L'\0';
	tmpBuffer2[0] = L'\0';

	// Try rendering the base message on a single line without word wrap
	// in 24 point font. 
	// If it doesn't fit, try rendering as a single line in 18 point font. 
	// If it still doesn't fit, render on two lines in 18 point font.

	if ( AlarmRender::WithoutWordWrap(tmpBuffer1, NULL, NULL, 
						messageIds, TextFont::TWENTY_FOUR, BASE_MSG_MAX_WIDTH_,
						CONTAINER_HEIGHT_, LEFT, BASE_MSG_MARGIN_) 
		|| AlarmRender::WithoutWordWrap(tmpBuffer1, NULL, NULL, 
						messageIds, TextFont::EIGHTEEN, BASE_MSG_MAX_WIDTH_,
						CONTAINER_HEIGHT_, LEFT, BASE_MSG_MARGIN_) )
	{													// $[TI1]
		// If the base message is a touchable symbol, then a corresponding
		// Message Area message will be attached to the symbol.
		baseMsg1_.setMessage(AlarmRender::GetSymbolHelpMessage(messageIds));
	}
	else
	{													// $[TI2]
	   	CLASS_ASSERTION(
			AlarmRender::WithWordWrap(tmpBuffer1, tmpBuffer2, tmpBuffer3,
				    	messageIds, TextFont::EIGHTEEN, BASE_MSG_MAX_WIDTH_,
						CONTAINER_HEIGHT_, LEFT, BASE_MSG_MARGIN_,TRUE) > 0);
	}


	baseMsg1_.setText(tmpBuffer1);
	baseMsg2_.setText(tmpBuffer2);

	// Setup the flashing states for base message.
	switch (alarmUpdate.getCurrentUrgency())
	{
	case Alarm::NORMAL_URGENCY:							// $[TI3]
		SAFE_CLASS_ASSERTION(FALSE);	// huh? this ain't no alarm!
		break;

	case Alarm::LOW_URGENCY:							// $[TI4]
		// Set to solid color to turn off flashing.
		if ( IS_COLOR )
		{												// $[TI13.1]
			setFillColor(Colors::YELLOW);
			baseMsg1_.setColor(Colors::BLACK);
			baseMsg2_.setColor(Colors::BLACK);
    		analysisMsg1_.setColor(Colors::BLACK);
    		analysisMsg2_.setColor(Colors::BLACK);
    		analysisMsg3_.setColor(Colors::BLACK);
		}
		else
		{												// $[TI13.2]
			setFillColor(Colors::BLACK);
			baseMsg1_.setColor(Colors::WHITE);
			baseMsg2_.setColor(Colors::WHITE);
    		analysisMsg1_.setColor(Colors::WHITE);
    		analysisMsg2_.setColor(Colors::WHITE);
    		analysisMsg3_.setColor(Colors::WHITE);
		}
		break;

	case Alarm::MEDIUM_URGENCY:							// $[TI5]
		// Flash base message at slow rate.
		if ( IS_COLOR )
		{												// $[TI14.1]
			setFillColor(Colors::YELLOW);
			baseMsg1_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
			baseMsg2_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
    		analysisMsg1_.setColor(Colors::BLACK);
    		analysisMsg2_.setColor(Colors::BLACK);
    		analysisMsg3_.setColor(Colors::BLACK);
		}
		else
		{												// $[TI14.2]
			setFillColor(Colors::BLACK);
			baseMsg1_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
			baseMsg2_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
    		analysisMsg1_.setColor(Colors::WHITE);
    		analysisMsg2_.setColor(Colors::WHITE);
    		analysisMsg3_.setColor(Colors::WHITE);
		}
		break;

	case Alarm::HIGH_URGENCY:							// $[TI6]
		SAFE_CLASS_ASSERTION(Alarm::HIGH_URGENCY ==
										AlarmStateManager::GetHighestUrgency());
		// Flash double-line base message at fast rate.
		if ( IS_COLOR )
		{												// $[TI15.1]
			setFillColor(Colors::RED);
			baseMsg1_.setColor(Colors::WHITE_ON_BLACK_BLINK_FAST);
			baseMsg2_.setColor(Colors::WHITE_ON_BLACK_BLINK_FAST);
    		analysisMsg1_.setColor(Colors::WHITE);
    		analysisMsg2_.setColor(Colors::WHITE);
    		analysisMsg3_.setColor(Colors::WHITE);
		}
		else
		{												// $[TI15.2]
			setFillColor(Colors::BLACK);
			baseMsg1_.setColor(Colors::WHITE_ON_BLACK_BLINK_FAST);
			baseMsg2_.setColor(Colors::WHITE_ON_BLACK_BLINK_FAST);
    		analysisMsg1_.setColor(Colors::WHITE);
    		analysisMsg2_.setColor(Colors::WHITE);
    		analysisMsg3_.setColor(Colors::WHITE);
		}
		break;

	default:
		CLASS_ASSERTION(FALSE);			// bogus urgency level
		break;
	}

	// Now, display the analysis/remedy messages.
	const MessageNameArray& analysisMsgs = alarmUpdate.getAnalysisMessage();
	const MessageNameArray& remedyMsgs = alarmUpdate.getRemedyMessage();

	// Copy the message id's into the null-terminated messageIds[] array.
	Int32 numMsgIds = 0;
	Int32 i = 0;
	for (i = 0; i < alarmUpdate.getAnalysisIndex(); i++)
	{													// $[TI7]
		CLASS_ASSERTION(numMsgIds < MAX_MSG_IDS);
		CLASS_ASSERTION(analysisMsgs[i] != MESSAGE_NAME_NULL);
		messageIds[numMsgIds++] = analysisMsgs[i];
	}													// $[TI8]

	for (i = 0; i < alarmUpdate.getRemedyIndex(); i++)
	{													// $[TI9]
		CLASS_ASSERTION(numMsgIds < MAX_MSG_IDS);
		CLASS_ASSERTION(remedyMsgs[i] != MESSAGE_NAME_NULL);
		messageIds[numMsgIds++] = remedyMsgs[i];
	}													// $[TI10]

	CLASS_ASSERTION(numMsgIds < MAX_MSG_IDS);
	messageIds[numMsgIds] = MESSAGE_NAME_NULL;

	tmpBuffer1[0] = L'\0';
	tmpBuffer2[0] = L'\0';
	tmpBuffer3[0] = L'\0';

	// Check for no analysis/remedy messages
	if (numMsgIds > 0)
	{													// $[TI11]
		CLASS_ASSERTION(
			AlarmRender::WithWordWrap(tmpBuffer1, tmpBuffer2, tmpBuffer3,
						messageIds, TextFont::TEN,
						ANALYSIS_MSG_MAX_WIDTH_, CONTAINER_HEIGHT_ - 1,
						LEFT, 0) > 0);
	}													// $[TI12]

	analysisMsg1_.setText(tmpBuffer1);
	analysisMsg2_.setText(tmpBuffer2);
	analysisMsg3_.setText(tmpBuffer3);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmMsg::SoftFault(const SoftFaultID  softFaultID,
					const Uint32       lineNumber,
					const char*        pFileName,
					const char*        pPredicate)  
{
	CALL_TRACE("AlarmMsg::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMMSG,
							lineNumber, pFileName, pPredicate);
}
#endif // SIGMA_GUI_CPU
