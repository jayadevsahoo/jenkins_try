
#ifndef VsvPhase_HH
#define VsvPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VsvPhase - Implements volume support ventilation phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VsvPhase.hhv   25.0.4.0   19 Nov 2013 14:00:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  syw    Date:  08-Nov-2000    DR Number: 5794
//       Project:  VTPC
//       Description:
//			Added variable alpha.
//
//  Revision: 001  By:  syw    Date:  03-Sep-2000    DR Number: 5755
//       Project:  VTPC
//       Description:
//             Initial version
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "PsvPhase.hh"

//@ End-Usage

// $[VC24006]
static const Real32 VSV_INIT_ALPHA = 0.5 ;

class VsvPhase : public PsvPhase
{
  public:
    VsvPhase( void) ;
    virtual ~VsvPhase( void) ;

	inline void reinitAlpha( void) ;
	
    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) ;

  private:
    VsvPhase( const VsvPhase&) ;		// not implemented...
    void operator=( const VsvPhase&) ;	// not implemented...

    //@ Data-Member: alpha_
    // alpha value for filtering compliance values
    Real32 alpha_ ;
} ;

// Inlined methods
#include "VsvPhase.in"

#endif // VsvPhase_HH 
