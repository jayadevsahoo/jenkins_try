#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: EstResultTable - EST Result Table.
//---------------------------------------------------------------------
//@ Interface-Description
//  This persistent table contains EST's result information.  When an EST
//  test is run, information (e.g., time of run and ending result) is stored.
//
//  This table is derived from 'TestResultTable', which manages the internal
//  array of table entries.  There are no virtual methods -- THEY ARE NOT
//  ALLOWED IN NOVRAM!
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  This table, working with 'TestResultTable', manages an internal fixed
//  array of table entries ('ResultTableEntry').  The fixed array is defined
//  in this class, with a pointer to the array stored in this class's base
//  class.  This allows common functionality between the the other result
//  tables to be shared in the common base class.
//
//  The base class is also responsible for protecting against multiple
//  threads, therefore this class should NOT be using any semaphores
//  to protect itself -- the base class already has protection.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/EstResultTable.ccv   25.0.4.0   19 Nov 2013 14:18:50   pvcs  $
//
//@ Modification-Table
//
//  Revision: 005  By:  rhj    Date:  21-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//      Added functionality to indicate a Single test EST step was 
//      performed.
//
//  Revision: 004  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Added calling of 'NovRamManager::ReportAuxillaryInfo()' to
//	initialization and verification methods.  Also, changed
//	'SoftFault()' to a non-inlined method.
//
//  Revision: 003  By:  sah    Date:  18-Jul-1997    DCS Number: 2246
//  Project:  Sigma (R8027)
//  Description:
//	Removed 'overrideFailures()', which is now implemented in base
//	class.
//
//  Revision: 002  By:  sah    Date:  15-Jul-1997    DCS Number: 2175
//  Project:  Sigma (R8027)
//  Description:
//	Added one static method ('IntermediateResultLogged()') for handling
//	the forwarding of this request to the one instance of this class.  Also,
//	simplified the removal of the overall result diagnostic via a new
//	static method of EstDiagLog.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "EstResultTable.hh"

//@ Usage-Classes...
#include "Array_ResultTableEntry.hh"
#include "NonVolatileMemory.hh"
#include "NovRamManager.hh"
#include "SmStatusId.hh"

//@ End-Usage

//@ Code...

#if defined(SIGMA_GUI_CPU)
Boolean EstResultTable::SingleTestLogEnabled_ = TRUE;
#endif // defined(SIGMA_GUI_CPU)


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  EstResultTable()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default result table.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (isEmpty())
//@ End-Method
//=====================================================================

EstResultTable::EstResultTable(void)
	   : TestResultTable(arrEntries_,
		             ::MAX_EST_RESULTS,
			     NovRamUpdateManager::EST_RESULT_TABLE_UPDATE)
{
  CALL_TRACE("EstResultTable()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~EstResultTable()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this result table.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

EstResultTable::~EstResultTable(void)
{
  CALL_TRACE("~EstResultTable()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize()  [const]
//
//@ Interface-Description
//  A reset sequence occurred, therefore verify the integrity of this
//  instance.  If this table is determined to be valid, 
//  'NovRamStatus::ITEM_VERIFIED' is returned.  If the internal
//  infrastructure is valid, but some of the result entries are invalid,
//  'NovRamStatus::ITEM_RECOVERED' is returned.  If this table's
//  infrastructure is invalid, the table is fully initialized (from scratch)
//  and 'NovRamStatus::ITEM_INITIALIZED' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use the base class's initialization method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
EstResultTable::initialize(void)
{
  CALL_TRACE("initialize()");

  NovRamStatus::InitializationId  initStatusId;

  // forward to base class's method...
  initStatusId = initTable_(::MAX_EST_RESULTS);

  switch (initStatusId)
  {
  case NovRamStatus::ITEM_INITIALIZED :		// $[TI1]
    // a full initialization of this result table is needed...
    new (this) EstResultTable();
    break;
  case NovRamStatus::ITEM_RECOVERED :
  case NovRamStatus::ITEM_VERIFIED :		// $[TI2]
    // do nothing...
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(initStatusId);
    break;
  };

  // at this point, the entire table should be valid...
  SAFE_CLASS_POST_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID),
  			    EstResultTable);

  if (initStatusId != NovRamStatus::ITEM_VERIFIED)
  {   // $[TI3]
    NovRamManager::ReportAuxillaryInfo(this, sizeof(EstResultTable),
				       initStatusId);
  }   // $[TI4]

  return(initStatusId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifySelf()  [const]
//
//@ Interface-Description
//  Verify the integrity of this EST Result Table.  This involves
//  verifying the integrity of each of this table's entries.  If this table
//  is determined to be valid, 'NovRamStatus::ITEM_VALID' is returned.  If
//  this table is not valid, 'NovRamStatus::ITEM_INVALID' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID  ||
//   verifySelf() == NovRamStatus::ITEM_INVALID)
//@ End-Method
//=====================================================================

NovRamStatus::VerificationId
EstResultTable::verifySelf(void) const
{
  CALL_TRACE("verifySelf()");

  const NovRamStatus::VerificationId  VERIF_STATUS =
					    verifyTable_(::MAX_EST_RESULTS);

  if (VERIF_STATUS != NovRamStatus::ITEM_VALID)
  {  // $[TI1]
    NovRamManager::ReportAuxillaryInfo(this, sizeof(EstResultTable));
  }  // $[TI2]

  return(VERIF_STATUS);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  repeatingTest(testNumber)
//
//@ Interface-Description
//  Repeat the EST test given by 'testNumber'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
EstResultTable::repeatingTest(const Uint testNumber)
{
  CALL_TRACE("repeatingTest(testNumber)");
  CLASS_PRE_CONDITION((testNumber < ::MAX_EST_RESULTS));

  ResultEntryData  entryData;
  
  arrEntries_[testNumber].getResultData(entryData);

  // "back-out" all of the diagnostics that may have been produced by this
  // test from the run that just completed...
  EstDiagLog::BackoutDiagsOfLastRun(entryData.getTestStartSeqNumber());

  startingTest(testNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCurrEntries(rArrEntries)  [const]
//
//@ Interface-Description
//  Initialize the array 'rArrEntries' to contain all of the current
//  result table entries.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

void
EstResultTable::getCurrEntries(
			    AbstractArray(ResultTableEntry)& rArrEntries
			      ) const
{
  CALL_TRACE("getCurrEntries()");
  CLASS_PRE_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID));
  SAFE_CLASS_PRE_CONDITION((rArrEntries.getNumElems() >=
  						::MAX_EST_RESULTS));

  // store all of the current entries from the base class into
  // 'rArrEntries'...
  copyCurrEntries_(rArrEntries);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IntermediateResultLogged(diagCode)  [static]
//
//@ Interface-Description
//  This method is used to forward the fact that an intermediate EST
//  test result has been logged, to the EST result table.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (diagCode.getDiagCodeTypeId() == DiagnosticCode::EXTENDED_SELF_TEST_DIAG)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
EstResultTable::IntermediateResultLogged(const DiagnosticCode& diagCode)
{
  CALL_TRACE("IntermediateResultLogged(diagCode)");
  SAFE_CLASS_PRE_CONDITION((diagCode.getDiagCodeTypeId() ==
				  DiagnosticCode::EXTENDED_SELF_TEST_DIAG));

  DiagnosticCode::ExtendedSelfTestBits  estCode =
					diagCode.getExtendedSelfTestCode();

  NonVolatileMemory::P_INSTANCE_->estResultTable_.intermediateResultLogged(
					   estCode.testNumber,
					   (TestResultId)estCode.failureLevel
									  );
}   // $[TI1]


#if defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  EnableSingleTestLog(void)   [static]
//
//@ Interface-Description
//  Sets the SingleTestLogEnabled_ to TRUE to enable single test 
//  logging.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void EstResultTable::EnableSingleTestLog(void)
{
	SingleTestLogEnabled_ = TRUE;
}
#endif // defined(SIGMA_GUI_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setAllTestsUndefined(void)   
//
//@ Interface-Description
//  Calls the setAllTestsUndefined_ in the base class to clear the logs.
//  Then add a single test entry in the Est Sst Diagonstic logs.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  [LC07006] A timestamped indication that a SINGLE TEST EST step was 
//            performed shall be added to the EST/SST Diagnostic Code Log, 
//            but only one entry shall be logged.
//  \a\ after power-up
//  \b\ after FULL EST has been completed (whether passed, failed, 
//            overridden, alert).
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void EstResultTable::setAllTestsUndefined(void)
{

	// Clear the Est logs.
	setAllTestsUndefined_();

	#if defined(SIGMA_GUI_CPU)
	
		if (SingleTestLogEnabled_)
		{	
			DiagnosticCode   estDiag;
		
			// $[TI2] -- log an EST overall result diagnostic...
			estDiag.setExtendedSelfTestCode(::UNDEFINED_TEST_RESULT_ID,
											::MAX_EST_RESULTS,
											SmStatusId::NUM_CONDITION_ITEM,
											TRUE);
			NovRamManager::LogEstSstDiagnostic(estDiag);
			SingleTestLogEnabled_  = FALSE;
			
		}
	
	#endif // defined(SIGMA_GUI_CPU)
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//				[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
EstResultTable::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, EST_RESULT_TABLE,
                          lineNumber, pFileName, pPredicate);
}

