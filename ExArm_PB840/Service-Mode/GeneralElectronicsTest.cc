#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: GeneralElectronicsTest - Read and display the analog data channels
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is used to read and display the analog data channels.  It
//	is not a test per se since it does not check the data for goodness.
//
//	NOTE: this "test" is now called the Analog Data Display.
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the methods required to read and display the
//	analog data channels.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method runTest() is responsible for reading and displaying the
//	analog data channels.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/GeneralElectronicsTest.ccv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  quf    Date: 10-Oct-1997    DR Number: DCS 2262
//       Project:  Sigma (840)
//       Description:
//			Added comment to class-level Interface-Description indicating
//			current test name.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  02-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "GeneralElectronicsTest.hh"

//@ Usage-Classes

#include "MainSensorRefs.hh"
#include "MiscSensorRefs.hh"
#include "VentObjectRefs.hh"
#include "SmRefs.hh"
#include "SmManager.hh"
#include "SmConstants.hh"
#include "AdcChannels.hh"
#include "Barometer.hh"

#ifdef SIGMA_UNIT_TEST
#include "SmUnitTest.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GeneralElectronicsTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.  No arguments.  Initializes the array of sensor pointers.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the sensor pointer array.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	number of sensor pointers initialized == NUM_CHANNELS
//@ End-Method
//=====================================================================
GeneralElectronicsTest::GeneralElectronicsTest(void)
{
// $[TI1]
  CALL_TRACE("GeneralElectronicsTest::GeneralElectronicsTest(void)");

  	// Assign sensor pointers to the array pSensors_[]
  	// NOTE: the temperature sensors have to come before the flow sensor
  	//      because the flow sensors require the temperature value.

	Int16	ii = 0 ;

   	pSensors_[ii++] = (Sensor*)&RInspPressureSensor ;
   	pSensors_[ii++] = (Sensor*)&RExhPressureSensor ;

   	pSensors_[ii++] = (Sensor*)&RO2TemperatureSensor ;
   	pSensors_[ii++] = (Sensor*)&RAirTemperatureSensor ;
// TODO E600 BDIO   	pSensors_[ii++] = (Sensor*)&RExhGasTemperatureSensor ;
// TODO E600 BDIO   	pSensors_[ii++] = (Sensor*)&RExhVoiceCoilTemperatureSensor ;
// TODO E600 BDIO 	pSensors_[ii++] = (Sensor*)&RExhHeaterTemperatureSensor ;

   	pSensors_[ii++] = (Sensor*)&RO2FlowSensor ;
   	pSensors_[ii++] = (Sensor*)&RAirFlowSensor ;
   	pSensors_[ii++] = (Sensor*)&RExhFlowSensor ;

 	pSensors_[ii++] = (Sensor*)&RFio2Monitor ;

   	pSensors_[ii++] = (Sensor*)&RO2PsolCurrent ;
   	pSensors_[ii++] = (Sensor*)&RAirPsolCurrent ;
   	pSensors_[ii++] = (Sensor*)&RExhMotorCurrent ;
// TODO E600 BDIO    pSensors_[ii++] = (Sensor*)&RSafetyValveCurrent ;

   	pSensors_[ii++] = (Sensor*)&RPowerFailCapVoltage ;
   	pSensors_[ii++] = (Sensor*)&RDc10vSentry ;
  	pSensors_[ii++] = (Sensor*)&RDc15vSentry ;
  	pSensors_[ii++] = (Sensor*)&RDcNeg15vSentry ;
  	pSensors_[ii++] = (Sensor*)&RDc5vGui ;
  	pSensors_[ii++] = (Sensor*)&RDc12vGui ;
  	pSensors_[ii++] = (Sensor*)&RDc5vVentHead ;
  	pSensors_[ii++] = (Sensor*)&RDc12vSentry ;

   	pSensors_[ii++] = (Sensor*)&RSystemBatteryVoltage ;
   	pSensors_[ii++] = (Sensor*)&RSystemBatteryCurrent ;
   	pSensors_[ii++] = (Sensor*)&RSystemBatteryModel ;
  	pSensors_[ii++] = (Sensor*)&RAlarmCableVoltage ;

  	pSensors_[ii++] = (Sensor*)&RSafetyValveSwitchedSide ;

  	pSensors_[ii++] = (Sensor*)&RDacWrap ;
  	pSensors_[ii++] = (Sensor*)&RAiPcbaRevision ;
  	pSensors_[ii++] = (Sensor*)&RLowVoltageReference ;


	// make sure the number of array elements initialized is correct
	CLASS_ASSERTION( ii == NUM_CHANNELS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GeneralElectronicsTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
GeneralElectronicsTest::~GeneralElectronicsTest(void)
{
	CALL_TRACE("GeneralElectronicsTest::~GeneralElectronicsTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest(void)
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.  It is used
//  to read and display the analog data channels.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09150]
//  Read each analog data channel and display its engineering value on
//	the GUI, except the O2 sensor channel which is displayed in ADC counts.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
GeneralElectronicsTest::runTest(void)
{
	CALL_TRACE("GeneralElectronicsTest::runTest(void)");

	// Read all channels
	for (Int32 index = 0; index < NUM_CHANNELS; index++)
	{
	// $[TI1]
		Real32 channelData = pSensors_[index]->getValue();

// TODO E600 BDIO
#if 0
		if ( pSensors_[index]->getAdcId() ==
					AdcChannels::O2_SENSOR )
		{
		// $[TI1.2]
			// Display DAC counts for the fio2 sensor
			channelData = pSensors_[index]->getCount();
		}
#endif
		RSmManager.sendTestPointToServiceData(channelData);
	}
	// end for
#if defined(SIGMA_BD_CPU)
	//Run test on the atmospheric pressure sensor
	Real32 channelData = RAtmosphericPressureSensor.getValue();
	// Convert atm pressure sensor data from
	// cm H20 absolute to PSIA
	channelData /= PSI_TO_CMH20_FACTOR;
	RSmManager.sendTestPointToServiceData(channelData);
#endif

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the fault 
//  macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and 'lineNumber' 
//  are essential pieces of information.  The 'pFileName' and 'pPredicate' 
//  strings may be defaulted in the macro to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system 
//  and class name ID and sends the message to the non-member function 
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
GeneralElectronicsTest::SoftFault(const SoftFaultID  softFaultID,
				  const Uint32       lineNumber,
				  const char*        pFileName,
				  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, GENERALELECTRONICSTEST,
                          lineNumber, pFileName, pPredicate);

}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
