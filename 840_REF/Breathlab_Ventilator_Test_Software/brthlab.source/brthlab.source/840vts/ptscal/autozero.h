/*****************************************************************************/
/*   Module Name:  autozero.h                                        
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _AUTOZERO_INCLUDED
#define _AUTOZERO_INCLUDED

//----------------------------------------------------------------------------------------------------
//  Include required headers
//----------------------------------------------------------------------------------------------------

#include "globals.h"


HPNL AutoZero(void);
int CVICALLBACK CmdAutoZero(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: AUTOZERO
// ------------------------------------------------------------------------------------------------

// Control: AUTOZERO_COMMAND_CANCEL
int CVICALLBACK ExitAutoZeroCMD(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

// Control: AUTOZERO_COMMANDBUTTON_HP
int CVICALLBACK AutoZeroSelect(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

// Control: AUTOZERO_COMMANDBUTTON_LP
int CVICALLBACK AutoZeroSelect(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

// Control: AUTOZERO_COMMANDBUTTON_O2
int CVICALLBACK AutoZeroSelect(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: AUTOZEROHP
// ------------------------------------------------------------------------------------------------

// Control: AUTOZEROHP_COMMANDBUTTON_OK
int CVICALLBACK AutoZeroCMDHP(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

// Control: AUTOZEROHP_COMMAND_CANCEL
int CVICALLBACK ExitAutoZeroCMDHP(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: AUTOZEROLP
// ------------------------------------------------------------------------------------------------

// Control: AUTOZEROLP_COMMANDBUTTON_OK
int CVICALLBACK AutoZeroCMDLP(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

// Control: AUTOZEROLP_COMMAND_CANCEL
int CVICALLBACK ExitAutoZeroCMDLP(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: AUTOZEROO2
// ------------------------------------------------------------------------------------------------

// Control: AUTOZEROO2_COMMAND_CANCEL
int CVICALLBACK ExitAutoZeroCMDO2(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

// Control: AUTOZEROO2_TIMER
int CVICALLBACK OxygenTimer(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

// Control: AUTOZEROO2_CMD_CAL_O2_21
int CVICALLBACK cmd_Cal_O2_21(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

// Control: AUTOZEROO2_CMD_CAL_O2_100
int CVICALLBACK cmd_Cal_O2_100(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

#endif // _AUTOZERO_INCLUDED
