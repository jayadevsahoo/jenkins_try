#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ConditionAugmentation - Info for condition augmentation matrix row.
//---------------------------------------------------------------------
//@ Interface-Description
// ConditionAugmentation is derived from pure virtual base class
// AlarmAugmentation.  It defines the pure virtual methods operator== and 
// operator!= and contains a reference to the OperatingGroup object which 
// defines the conditions in the Condition Augmentation column of the Technical
// and Non-Technical alarm matrices.  It also provides a method for accessing 
// the OperatingGroup.  The virtual base class provides a method for accessing 
// the Alarm.
//---------------------------------------------------------------------
//@ Rationale
// ConditionAugmentation collects the annunciated alarm information for a
// condition augmentation matrix row and defines a relationship between an Alarm
// object and an OperatingGroup object (defining the condition augmentation
// criteria in the alarm matrix row) which augments it.
//---------------------------------------------------------------------
//@ Implementation-Description
// In addition to the functionality provided by pure virtual base class
// AlarmAugmentation, ConditionAugmentation provides a method to return the
// reference to an OperatingGroup which must evaluate to true for the
// augmentation information to be included in the alarm annunciation.
// It also defines operator== and operator!= which are used to determine 
// whether two ConditionAugmentation objects are equal or not.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/ConditionAugmentation.ccv   25.0.4.0   19 Nov 2013 13:51:20   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "ConditionAugmentation.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...
//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ConditionAugmentation
//
//@ Interface-Description
//    Constructor.
//
// >Von
//    rInfo          Reference to AlarmAnalysisInformation object which contains
//                   analysis and remedy message enumerators, urgency/priority 
//                   information and pointers to the System Response and Reset 
//                   System Response associated with the augmentation in the 
//                   Alarm Matrices.
//    rAlarm         Reference to the Alarm in the OperatingGroup /
//                   ConditionAugmentation / Alarm relationship.
//    rAugmentGroup  References the OperatingGroup in the OperatingGroup / 
//                   ConditionAugmentation / Alarm relationship
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

ConditionAugmentation::ConditionAugmentation(
                      const            AlarmAnalysisInformation& rInfo,
                      Alarm&           rAlarm,
                      OperatingGroup&  rAugmentGroup) :

                      AlarmAugmentation(CONDITION_AUGMENTATION, rInfo, rAlarm),
                      rAugmentGroup_(rAugmentGroup)
{
  CALL_TRACE("ConditionAugmentation(rInfo, rAlarm, rAugmentGroup)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ConditionAugmentation
//
//@ Interface-Description
//    Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

ConditionAugmentation::~ConditionAugmentation(void)
{
  CALL_TRACE("~ConditionAugmentation(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator==
//
//@ Interface-Description
//    Equality operator.
//
// >Von
//    rAug  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    Two ConditionAugmentation objects are equal if their rAugmentGroup_
//    attributes and base classes are equal.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
ConditionAugmentation::operator==(const AlarmAugmentation& rAug) const
{
  CALL_TRACE("operator==(rAug)");

  const ConditionAugmentation& rConditionAug = 
      (const ConditionAugmentation&)rAug;

  return((&rAugmentGroup_ == &rConditionAug.rAugmentGroup_) && 
          this->AlarmAugmentation::operator==(rAug));  // $[TI1]  $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator!=
//
//@ Interface-Description
//    Inequality operator.
//
// >Von
//    rAug  Two ConditionAugmentation objects are not equal if their
//          rAugmentGroup_ attributes or base classes are not equal.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
ConditionAugmentation::operator!=(const AlarmAugmentation& rAug) const
{
  CALL_TRACE("operator!=(rAug)");

  return (!(operator==(rAug)));  // $[TI1]  $[TI2]
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//    This method is called when a software fault is detected by the
//    fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//    'lineNumber' are essential pieces of information.  The 'pFileName'
//    and 'pPredicate' strings may be defaulted in the macro to reduce
//    code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method receives the call for the SoftFault, adds it sub-system
//    and class name ID and sends the message to the non-member function
//    SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition    
//    none
//@ End-Method
//=====================================================================

void
ConditionAugmentation::SoftFault(const SoftFaultID  softFaultID,
                                 const Uint32       lineNumber,
                                 const char*        pFileName,
                                 const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::CONDITIONAUGMENTATION, lineNumber,
                          pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

