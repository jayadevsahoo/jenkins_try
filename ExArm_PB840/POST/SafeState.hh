#ifndef SafeState_HH
#define SafeState_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
// Filename: SafeState.hh - Power On Self Test, Safe-State System
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/SafeState.hhv   25.0.4.0   19 Nov 2013 14:17:06   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#ifdef SIGMA_BD_CPU

#include "Post.hh"

//@ Begin-Free-Declarations

extern	Criticality TestSafeStateSystem  (void);
extern	Criticality TestVentInop         (void);

//@ End-Free-Declarations

#endif //SIGMA_BD_CPU

#endif	// #ifndef SafeState_HH

