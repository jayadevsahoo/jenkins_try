#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  VtIbwRatioSetting - Tidal Volume-to-IBW Ratio.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the ratio of volume
//  per weight, as related by the tidal volume (Vt) setting and IBW.
//  This class inherits from 'BatchBoundedSetting' and provides the
//  specific information needed for representation of the Vt/IBW ratio.
//  This information includes the interval and range of this setting's values,
//  this setting's response to a change of its tracked setting (see
//  'valueUpdate()'), and this batch setting's new-patient value (see
//  'getNewPatientValue()').
//
//  This class cannot be directly changed by the operator, yet it is not
//  fixed.  This setting is strictly changed via changes to the volume
//  support setting, via the Setting Observer/Subject Framework.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/VtIbwRatioSetting.ccv   25.0.4.0   19 Nov 2013 14:27:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 001   By: sah   Date:  16-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added this setting, as a tracking setting, for informational
//         display
//
//=====================================================================

#include "VtIbwRatioSetting.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[VC02009] The setting's range ...
// $[VC02010] The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	1.0f,		// absolute minimum...
	0.0f,		// unused...
	HUNDREDTHS,		// unused...
	NULL
};
static const BoundedInterval  NODE1_ =
{
	10.0f,		// change-point...
	0.01f,		// resolution...
	HUNDREDTHS,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	50.0f,		// absolute maximum...
	0.1f,		// resolution...
	TENTHS,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: VtIbwRatioSetting()  [Default Constructor]
//
//@ Interface-Description
//  Construct an instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VtIbwRatioSetting::VtIbwRatioSetting(void)
: BatchBoundedSetting(SettingId::VT_IBW_RATIO,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  NULL_SETTING_BOUND_ID,	// maxConstraintId...
					  NULL_SETTING_BOUND_ID)	// minConstraintId...
{
	CALL_TRACE("VtIbwRatioSetting()");
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~VtIbwRatioSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VtIbwRatioSetting::~VtIbwRatioSetting(void)
{
	CALL_TRACE("~VtIbwRatioSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is only applicable (viewable) if target support volume
//  (Vt-supp) is applicable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
VtIbwRatioSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pTidalVolume =
	SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

	const Applicability::Id  TIDAL_VOL_APPLIC_ID =
	pTidalVolume->getApplicability(qualifierId);

	return((TIDAL_VOL_APPLIC_ID != Applicability::INAPPLICABLE)
		   ? Applicability::VIEWABLE	 // $[TI1]
		   : Applicability::INAPPLICABLE);	 // $[TI2]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
VtIbwRatioSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	// calculate the ratio's new-patient value using tidal volume's new-patient
	// value and IBW's current value...
	const BoundedValue  NEW_PATIENT = calcBasedOnSetting_(
														 SettingId::TIDAL_VOLUME,
														 BoundedRange::WARP_NEAREST,
														 BASED_ON_NEW_PATIENT_VALUES
														 );

	return(NEW_PATIENT);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to value changes of tidal volume and IBW by re-calculating 
//  current value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[VC02009] -- equal to (Vt / IBW)
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::TIDAL_VOLUME)
//  || (pSubject->getId() == SettingId::IBW)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VtIbwRatioSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
							   const SettingSubject*               pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");
	AUX_CLASS_PRE_CONDITION((pSubject->getId() == SettingId::TIDAL_VOLUME
							 || pSubject->getId() == SettingId::IBW),
							pSubject->getId());

	if ( qualifierId == Notification::ADJUSTED )  // $[TI1] -- tidal volume or IBW's adjusted value changed...
	{
		BoundedValue  newVtIbwRatio;

		newVtIbwRatio = calcBasedOnSetting_(SettingId::TIDAL_VOLUME,
											BoundedRange::WARP_NEAREST,
											BASED_ON_ADJUSTED_VALUES);

		// store the value...
		setAdjustedValue(newVtIbwRatio);
	}  // $[TI2] -- don't care about accepted value changes...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
VtIbwRatioSetting::doRetainAttachment(const SettingSubject*) const
{
	CALL_TRACE("doRetainAttachment(pSubject)");

	return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting monitors the values of tidal volume and IBW, therefore
//  this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VtIbwRatioSetting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	attachToSubject_(SettingId::TIDAL_VOLUME, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::IBW, Notification::VALUE_CHANGED);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
VtIbwRatioSetting::isAcceptedValid(void) const
{
	CALL_TRACE("isAcceptedValid()");

	Boolean  isValid = BoundedSetting::isAcceptedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ACCEPTED) !=
			 Applicability::INAPPLICABLE )
		{
			const Real32  VT_IBW_RATIO_VALUE = BoundedValue(getAcceptedValue()).value;
			const Real32  CALC_VT_IBW_RATIO_VALUE = calcBasedOnSetting_(SettingId::TIDAL_VOLUME,
																		BoundedRange::WARP_NEAREST,
																		BASED_ON_ACCEPTED_VALUES).value;

			isValid = (VT_IBW_RATIO_VALUE == CALC_VT_IBW_RATIO_VALUE);

			if ( !isValid )
			{
				const Setting*  pTidalVolume = SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
				const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

				cout << "\nINVALID Minute Volume:\n";
				cout << "CALC = " << CALC_VT_IBW_RATIO_VALUE << endl;
				cout << *this << *pTidalVolume << *pIbw << endl;
			}
		}
	}

	return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
VtIbwRatioSetting::isAdjustedValid(void)
{
	CALL_TRACE("isAdjustedValid()");

	Boolean  isValid = BoundedSetting::isAdjustedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ADJUSTED) !=
			 Applicability::INAPPLICABLE )
		{
			const Real32  VT_IBW_RATIO_VALUE = BoundedValue(getAdjustedValue()).value;
			const Real32  CALC_VT_IBW_RATIO_VALUE = calcBasedOnSetting_(SettingId::TIDAL_VOLUME,
																		BoundedRange::WARP_NEAREST,
																		BASED_ON_ADJUSTED_VALUES).value;

			isValid = (VT_IBW_RATIO_VALUE == CALC_VT_IBW_RATIO_VALUE);

			if ( !isValid )
			{
				const Setting*  pTidalVolume = SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
				const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

				cout << "\nINVALID Minute Volume:\n";
				cout << "CALC = " << CALC_VT_IBW_RATIO_VALUE << endl;
				cout << *this << *pTidalVolume << *pIbw << endl;
			}
		}
	}

	return(isValid);
}

#endif // defined (SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VtIbwRatioSetting::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							VT_IBW_RATIO_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnSettingId == SettingId::TIDAL_VOLUME  &&
//   (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//    basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
VtIbwRatioSetting::calcBasedOnSetting_(
									  const SettingId::SettingIdType basedOnSettingId,
									  const BoundedRange::WarpDir    warpDirection,
									  const BasedOnCategory          basedOnCategory
									  ) const
{
	CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

	AUX_CLASS_PRE_CONDITION((basedOnSettingId == SettingId::TIDAL_VOLUME), basedOnSettingId);

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);
	const Setting*  pTidalVolume = SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

	Real32  ibwValue;
	Real32  tidalVolumeValue;

	switch ( basedOnCategory )
	{
		case BASED_ON_NEW_PATIENT_VALUES :		  // $[TI1]
			// get the "adjusted" value of IBW...
			ibwValue = BoundedValue(pIbw->getAdjustedValue()).value;

			// get the "new-patient" value of tidal volume...
			tidalVolumeValue = BoundedValue(pTidalVolume->getNewPatientValue()).value;
			break;

		case BASED_ON_ADJUSTED_VALUES :		  // $[TI2]
			// get the "adjusted" values of each of the settings...
			ibwValue         = BoundedValue(pIbw->getAdjustedValue()).value;
			tidalVolumeValue = BoundedValue(pTidalVolume->getAdjustedValue()).value;
			break;

		case BASED_ON_ACCEPTED_VALUES :
			// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
			// get the "accepted" values of each of the settings...
			ibwValue         = BoundedValue(pIbw->getAcceptedValue()).value;
			tidalVolumeValue = BoundedValue(pTidalVolume->getAcceptedValue()).value;
			break;
#endif // defined(SIGMA_DEVELOPMENT)

		default :
			// unexpected 'basedOnCategory' value..
			AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
			break;
	};

	BoundedValue  vtIbwRatio;

	vtIbwRatio.value = (tidalVolumeValue / ibwValue);

	// place value on a "click" boundary...
	getBoundedRange().warpValue(vtIbwRatio, warpDirection, FALSE);

	return(vtIbwRatio);
}

