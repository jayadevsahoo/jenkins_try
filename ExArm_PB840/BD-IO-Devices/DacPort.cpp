#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DacPort - Provide interface to control psols.
//---------------------------------------------------------------------
#include "DacPort.h"
#include "BDIORefs.hh"

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DacPort()
//
//@ Interface-Description
//      Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
DacPort::DacPort()
{
	dacId_ = DAC_AIR_VALVE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DacPort()
//
//@ Interface-Description
//      Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Set DAC port id with input value
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
DacPort::DacPort(AnalogOutputs_t id)
{
	dacId_ = id;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDac()
//
//@ Interface-Description
//      This method takes an value to output to the DAC port.
//---------------------------------------------------------------------
//@ Implementation-Description
//      
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void DacPort::setDac(DacCounts value)
{
	if(value > MAX_DAC_COUNT_VALUE)
	{
		value = MAX_DAC_COUNT_VALUE;
	}

	AioDioDriver.Write(dacId_, (DacCounts)value);
}
