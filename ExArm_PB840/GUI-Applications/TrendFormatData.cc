#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: TrendFormatData - Handles display formatting of trended data
//---------------------------------------------------------------------
//@ Interface-Description
//  This class handles the string formatting for the trend data displayed
//  on the GUI.
//---------------------------------------------------------------------
//@ Rationale
//  To centralize the data formatting for the displayed trend parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All trended parameters are assigned an integer representing the 
//  number of digits to display following the decimal point. 
//  TH:TL and I:E ratio parameters have special formatting requirements
//  that are handled in this class. This class also handles data
//  conversions from ms to seconds for the trended parameters that
//  require it.
// 
//  This class maps the eventIds assigned in TrendEvents to the event
//  numbers that get displayed on the GUI.  
//  
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used.
//---------------------------------------------------------------------
//@ Restrictions
//  Access to this class is provided through static methods FormatData()
//  and FormatEvent().
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/GUI-Applications/vcssrc/TrendFormatData.ccv   10.7   08/17/07 10:11:02   pvcs  
//
//  @ Modification-Log
//
//  Revision: 002   By: rpr    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation
// 
//  Revision: 001  By:  ksg    Date:  19-June-2007    SCR Number: 6237
//  Project:  Trend
//  Description: Initial version.
//====================================================================
#include "TrendFormatData.hh"
#include "GuiAppClassIds.hh"

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendFormatData()  [Constructor]
//
//@ Interface-Description
//  Constructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TrendFormatData::TrendFormatData(void)
{
	Uint8 i = 0;

	// initialize array
	for (i = 0;i < TrendSelectValue::TOTAL_TREND_SELECT_VALUES; i++)
	{
		fractionalDigitArray_[i] = 0;
	}

	fractionalDigitArray_[TrendSelectValue::TREND_EMPTY] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_DYNAMIC_COMPLIANCE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_PAV_COMPLIANCE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_STATIC_LUNG_COMPLIANCE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_PAV_ELASTANCE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_END_EXPIRATORY_FLOW] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_PEAK_EXPIRATORY_FLOW_RATE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_PEAK_SPONTANEOUS_FLOW_RATE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_TOTAL_RESPIRATORY_RATE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_I_E_RATIO] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_NEGATIVE_INSPIRATORY_FORCE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_OXYGEN_PERCENTAGE] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_P100] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_END_EXPIRATORY_PRESSURE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_INTRINSIC_PEEP] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_PAV_INTRINSIC_PEEP] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_TOTAL_PEEP] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_MEAN_CIRCUIT_PRESSURE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_PEAK_CIRCUIT_PRESSURE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_PLATEAU_PRESSURE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_SPONTANEOUS_RAPID_SHALLOW_BREATHING_INDEX] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_DYNAMIC_RESISTANCE]  = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_PAV_RESISTANCE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_STATIC_AIRWAY_RESISTANCE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_PAV_TOTAL_AIRWAY_RESISTANCE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_SPONTANEOUS_INSPIRATORY_TIME] = 2;
	fractionalDigitArray_[TrendSelectValue::TREND_SPONTANEOUS_INSPIRATORY_TIME_RATIO] = 2;
	fractionalDigitArray_[TrendSelectValue::TREND_VITAL_CAPACITY]  = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_EXHALED_MINUTE_VOLUME] = 2;
	fractionalDigitArray_[TrendSelectValue::TREND_EXHALED_SPONTANEOUS_MINUTE_VOLUME] = 2;
	fractionalDigitArray_[TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_EXHALED_SPONTANEOUS_TIDAL_VOLUME] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_EXHALED_MANDATORY_TIDAL_VOLUME] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_INSPIRED_TIDAL_VOLUME] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_TOTAL_WORK_OF_BREATHING] = 1;

	fractionalDigitArray_[TrendSelectValue::TREND_SET_EXPIRATORY_SENSITIVITY] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_RESPIRATORY_RATE] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_PEAK_INSPIRATORY_FLOW] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_I_E_RATIO] = 1;  
	fractionalDigitArray_[TrendSelectValue::TREND_SET_OXYGEN_PERCENTAGE] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_PERCENT_SUPPORT] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_PEEP] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_PEEP_HIGH] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_PEEP_LOW] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_INSPIRATORY_PRESSURE] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_PRESSURE_SUPPORT_LEVEL] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_FLOW_ACCELERATION_OVER_RISE_TIME] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_FLOW_SENSITIVITY] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_PRESSURE_SENSITIVITY] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_PEEP_HIGH_TIME] = 2;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_TH_TL_RATIO] = 1; 
	fractionalDigitArray_[TrendSelectValue::TREND_SET_INSPIRATORY_TIME] = 2;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_PEEP_LOW_TIME] = 2;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_TIDAL_VOLUME] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_SET_VOLUME_SUPPORT_LEVEL_VS] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_PERCENT_LEAK] = 0;
	fractionalDigitArray_[TrendSelectValue::TREND_LEAK] = 1;
	fractionalDigitArray_[TrendSelectValue::TREND_ILEAK] = 0;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendMenuItems()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendFormatData::~TrendFormatData(void)
{
	CALL_TRACE("TrendFormatData::~TrendFormatData(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTrendFormatData
//
//@ Interface-Description
//  Return a static instance of class object
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TrendFormatData& TrendFormatData::GetTrendFormatData(void)
{
	static TrendFormatData TrendFormatDataObject_;

	return TrendFormatDataObject_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FormatData
//
//@ Interface-Description
//  Returns a formatted string in the character buffer provided for 
//  the specified TrendSelectValueId and Real32 data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  invokes private method formatData_()
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures that trendId is less than TrendSelectValue::TOTAL_TREND_SELECT_VALUES
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void TrendFormatData::FormatData(wchar_t* rtnBuffer, TrendSelectValue::TrendSelectValueId trendId, Real32 data)
{
	AUX_CLASS_PRE_CONDITION(trendId < TrendSelectValue::TOTAL_TREND_SELECT_VALUES, trendId);
	GetTrendFormatData().formatData_(rtnBuffer, trendId, data);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FormatEvent
//
//@ Interface-Description
//  Returns an integer value to display for the specified
//  TrendEvents::EventId.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures that eventId is less than the maximum TOTAL_EVENT_IDS
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int32 TrendFormatData::FormatEvent(TrendEvents::EventId eventId)
{
	AUX_CLASS_PRE_CONDITION(eventId < TrendEvents::TOTAL_EVENT_IDS, eventId);
	return GetTrendFormatData().formatEvent_(eventId);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: formatData_ [private]
//
//@ Interface-Description
//  Returns a formatted string in the character buffer provided for 
//  the specified TrendSelectValueId and Real32 data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Formats data with number of fractional digits specified in
//  constructor. Handles special display cases I:E and TH:TL ratios and
//  values that require conversion from ms to seconds.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void TrendFormatData::formatData_(wchar_t* rtnBuffer, TrendSelectValue::TrendSelectValueId trendId, Real32 data)
{
	Uint8 fracVal= fractionalDigitArray_[trendId];
	static wchar_t tmpBuffer[40];

	// format I:E and TH:TL ratio displays
	if ((trendId == TrendSelectValue::TREND_I_E_RATIO) || 
		(trendId == TrendSelectValue::TREND_SET_I_E_RATIO) ||
		(trendId == TrendSelectValue::TREND_SET_TH_TL_RATIO))
	{
		if (data < 0.0)
		{
			// Formulate the text for the 1:num display
			swprintf( tmpBuffer, L"1:%%.%df", fracVal);
			swprintf( rtnBuffer, tmpBuffer, (data * -1) );
		}
		else if (data > 0.0)
		{
			// Formulate the text for the num:1 display
			swprintf( tmpBuffer, L"%%.%df:1", fracVal);
			swprintf( rtnBuffer, tmpBuffer, data );
		}
		else
		{
			// Formulate the text for the 0:0 display
			swprintf( rtnBuffer, L"0:0");
		}
	}
	// These values are in milliseconds. Convert to seconds.
	else if ((trendId == TrendSelectValue::TREND_SET_PEEP_LOW_TIME)||
			 (trendId == TrendSelectValue::TREND_SET_PEEP_HIGH_TIME)||
			 (trendId == TrendSelectValue::TREND_SET_INSPIRATORY_TIME))
	{
		// Bounded "Real" value, float output
		swprintf( tmpBuffer, L"%%.%df", fracVal);
		swprintf( rtnBuffer, tmpBuffer, data/1000 );
	}
	// Format everything else according to num of decimals specified in the fractionalDigitArray_
	else
	{
		// Bounded "Real" value, float output
		swprintf( tmpBuffer, L"%%.%df", fracVal);
		swprintf( rtnBuffer, tmpBuffer, data );
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: formatEvent_ [private]
//
//@ Interface-Description
// Returns an integer value to display for the specified TrendEvents::EventId.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method translates the EventId enumerator value used in the 
// TrendEvents class to the formatted event number displayed by the GUI.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int32 TrendFormatData::formatEvent_(TrendEvents::EventId eventId) const
{
    AUX_CLASS_ASSERTION(0 <= eventId && eventId < TrendEvents::TOTAL_EVENT_IDS, eventId);

	// define the ranges for the user and auto events
	static const Int32 FIRST_USER_EVENT_DISPLAY_ = 1;
	static const Int32 FIRST_AUTO_EVENT_DISPLAY_ = 51;

	if (eventId <= TrendEvents::MAX_USER_EVENT_ID)
	{
		// user events range [1..49]
        return (eventId + (FIRST_USER_EVENT_DISPLAY_-TrendEvents::BEGIN_USER_EVENT_ID));
	}
	else
	{
		// auto-events range [51..99]
		return (eventId + (FIRST_AUTO_EVENT_DISPLAY_-TrendEvents::BEGIN_AUTO_EVENT_ID));
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendFormatData::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDFORMATDATA,
							lineNumber, pFileName, pPredicate);
}
