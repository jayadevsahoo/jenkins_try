/*****************************************************************************/
/*   Module Name:    globals.h                                        
/*   Purpose:      Definitions used across several files                                
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _GLOBALS
#define _GLOBALS

// Uncomment the line below to compile the program for running in 'Simulator' mode
// In Simulator mode the program provides its own data and does not use a PTS 2000 device
//#define SIMULATOR

// Uncomment the line below to enable debugging outputs while the program is running in real-time
//#define DEBUG

// Uncomment the line below to use macros (in-line code) instead of function calls
//#define _USEMACROS

// extern char *szEmptyString;

typedef int HPNL;  // handle to a panel
typedef int HCTRL; // handle to a control
typedef int HMBAR; // handle to a menuBar
typedef int HMNU;  // handle to a menu
typedef int HFILE; // handle to a file
typedef int HBMAP; // handle to a bitmap

typedef int CVI_COLOR;

typedef int BOOL;

#ifndef NULL
#define NULL 0
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#define GAS_TYPE_AIR 0
#define GAS_TYPE_O2  1

#define OFF 0
#define ON  1

#define POSITIVE  1
#define NEGATIVE -1

#define SUCCESS 0

#define CVI_NOERROR           0
#define CVI_ERROR            -1
#define CVI_INVALID_PORTNUM  -2
#define CVI_IO_TIMEOUT      -99
#define CVI_CHECKSUM_ERROR -265

// printer setup dialog
#define NO_PRINTER_PROMPT  0
#define PROMPT_FOR_PRINTER 1

// colors
// shades of gray from lightest to darkest
#define OFFWHITE         0XE0E0E0
#define DIALOG_COLOR     0XB0B0B0
#define CMD_BUTTON_COLOR 0X606060
#define PANEL_GRAY       0X505050
#define GRIDCOLOR        0X404040
#define PLOTBGCOLOR      0X202020

#define DARK_GREEN       0X336633
#define LIGHT_GREEN      0X99CC99

#define DARK_RED         0X800000
#define MEDIUM_RED       0X663333
#define LIGHT_RED        0XFFCCCC

// panel stuff
#define PANELGAP          2
#define BEVEL_THICKNESS   2
#define CHART_BORDER_SIZE 8

#define INVALID_PANEL_LOCATION -44

// pixel thickness of 3D edges around command buttons
#define CMD_BUTTON_TOP_OVERHEAD   2
#define CMD_BUTTON_BTM_OVERHEAD   3
#define CMD_BUTTON_VERT_OVERHEAD  5

#define CMD_BUTTON_LFT_OVERHEAD   2
#define CMD_BUTTON_RT_OVERHEAD    3
#define CMD_BUTTON_HORIZ_OVERHEAD 5

// pixel thickness of 3D edges around ring menus
#define RING_MENU_TOP_OVERHEAD    2
#define RING_MENU_BTM_OVERHEAD    2
#define RING_MENU_VERT_OVERHEAD   4
#define RING_MENU_MIN_HEIGHT      7

#define RING_MENU_LFT_OVERHEAD    2
#define RING_MENU_ARROW_WIDTH    16
#define RING_MENU_RT_OVERHEAD     2
#define RING_MENU_HORIZ_OVERHEAD 20
#define RING_MENU_MIN_WIDTH      15

extern char *itoa(int n, char s[]);


							
#endif // #ifndef _GLOBALS
