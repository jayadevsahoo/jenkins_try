
#ifndef ShadowTraceEnableValue_HH
#define ShadowTraceEnableValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  ShadowTraceEnableValue - Values of the Shadow Trace Enable Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ShadowTraceEnableValue.hhv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: emccoy  Date:  14-Apr-2002    DR Number:  5848
//  Project:  VCP
//  Description:
//	Added for supporting the enabling/disabling of shadow traces.
//
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct ShadowTraceEnableValue
{
  //@ Type:  Id
  // All of the possible values of the Shadow Trace Enable Setting.
  enum ShadowTraceEnableValueId
  {
    // $[VC01001] -- enable/disable of Pcari's shadow trace
    // $[PA01000] -- enable/disable of Plung's shadow trace
    DISABLE_SHADOW,
    ENABLE_SHADOW,
    
    TOTAL_VALUES  
  };
};


#endif // ShadowTraceEnableValue_HH 
