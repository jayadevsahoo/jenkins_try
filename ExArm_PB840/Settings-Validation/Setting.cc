#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N ================
//@ Class:  Setting - Setting Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is the base class of all of the settings.  The
//  'BoundedSetting', 'DiscreteSetting' and 'SequentialSetting' -- and
//  ultimately all -- classes inherit from this class.  The class sets-up
//  the setting's framework for the settings to build upon.  This framework
//  includes virtual methods determining if a setting's "adjusted" value
//  has been changed (batch settings, only), updating a setting to either
//  its default or new-patient value, responding to an activation by the
//  operator, calculating a new value based on a knob delta, resetting a
//  setting's value to its value upon activation, accepting a change to one
//  of its Primary Settings, and accepting a Main Control Setting's
//  transition.  These methods are overridden by the derived settings to
//  provide the specific behavior needed.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to establish the setting's framework
//  for the rest of the settings to build upon.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has an instance of 'BoundStatus' which contains the
//  information about a setting's constraint status, and an instance to
//  store a pointer to a setting's dependent bound status, if any.
//
//  This class also defines a "protected" framework of two virtual methods:
//  one to update the values of dependent settings, the other to update the
//  dynamic constraints, if any, of a setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Setting.ccv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 016   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 015   By: gdc    Date: 21-Jan-2009    SCR Number: 6458
//  Project:  840S
//  Description:
//      Modified to use isNewlyApplicable() method to determine if
//		setting's applicability has changed from non-applicable to
//		an applicable state. Resolves problem of setting getting
//		into a changed state and not being able to get out of it when
//		even when the applicability reverts to non-applicable.
//
//  Revision: 014   By: gdc    Date: 08-Jan-2009    SCR Number: 6177
//  Project:  840S
//  Description:
//      Implemented isNewlyApplicable() to correctly identify when a
//      setting has become newly applicable in the adjusted context.
//
//  Revision: 013   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 012 By: rhj    Date: 01-Dec-2006   DR Number: 6291
//  Project:  RESPM
//	Description:
//      Added functionality to prevent settings getting stored into NOVRAM. 
//
//  Revision: 011 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 010   By: sah    Date:  07-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added 'isEnabledValue()' to provide support for the new
//         drop-down menus
//      *  added call to 'resetState()' in 'updateToNewPatientValue()'
//         to ensure spontaneous and mandatory types' internal value
//         fields are cleared
//      *  added 'testTransition()' to provide support for the new
//         conditional transition rules of mandatory type
//
//  Revision: 009   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//      *  added new virtual method, 'resetConstraints()' for resetting
//         all dynamic constraints to their absolute limit counterpart,
//         to ensure correct calculations during new-patient initialization
//      *  re-designed much of the settings framework, moving many methods
//         up to this level, further simplifying, and genericizing, the
//         design, especially with respect to the soft-limit framework
//      *  added DEVELOPMENT-only code for debugging purposes only
//
//  Revision: 008   By: sah  Date:  18-Jun-1999    DR Number:  5440
//  Project:  ATC
//  Description:
//      As part of this DCS, 'resetForcedChangeFlag()' changed from an
//      inlined method to a virtual method, to allow derived settings to
//      override and change its behavior.  However, within
//      'Setting::resetState()', we need to make ensure the change-flag
//      is, indeed, reset.
//
//  Revision: 007   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  defined new, pure virtual 'getApplicability()' method
//	*  defined new, virtual 'settingObserverInit()' method
//	*  moved 'get{Max,Min}Limit()' methods from derived class up
//	   to here, to simplify implementation throughout subsystem
//	*  now derived off of new 'SettingSubject' class, thereby
//	   making all settings "subjects" for observers to monitor
//
//  Revision: 006   By: dosman Date:  28-Oct-1998    DR Number: 5234
//  Project:  BILEVEL
//  Description:
//     Whenever a setting is activated, 
//     we ensure that all the affected soft bounds are updated.
//
//  Revision: 005   By: dosman Date:  16-Sep-1998    DR Number: BiLevel 144
//  Project:  BILEVEL
//  Description:
//	Changed calcDependentValues_ to return a boolean flag stating
//	whether it was able to settle on a satisfactory value for 
//	the primary setting that would satisfy all of the dependent settings' bounds
//
//  Revision: 004   By: dosman Date:  08-Sep-1998    DR Number: BILEVEL 130
//  Project:  BILEVEL
//  Description:
//      Added updateAllDependentsSoftBoundStates_ because primary  
//      settings need to be able to tell dependent settings to override
//      their soft bounds during the "resolve dependent" process, and then
//      update their soft bounds after the process is complete. 
//      Added overrideAllSoftBoundStates_ so that a setting has one method 
//      that overrides all its soft bounds. This functionality is implemented
//	using virtual functions so it is separate from 
//	updateAllDependentsSoftBoundStates_ 
//      Changed name to updateAllSoftBoundStates_ to clarify that all of 
//      this setting's soft bounds are updated through this method. 
// 
//  Revision: 003   By: dosman Date:  16-Jun-1998    DR Number: BILEVEL 116
//  Project:  BILEVEL
//  Description:
//	change updateSoftBoundState_() to handle case where setting not changing
//	and create deactivateSoftBound_() to work in parallel fashion to
//	activateSoftBound_().
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//      Initial version
//
//===================================================================

#include "Setting.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "AdjustedContext.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

static Uint  PStoredSettingMem_[
							   (sizeof(Setting::SettingIdAndValue) + sizeof(Uint) - 1) / sizeof(Uint)
							   ];


//====================================================================
//
//  Static Member Definitions...
//
//====================================================================

const SettingId::SettingIdType  Setting::NULL_DEPENDENT_ARRAY_[] =
{
	SettingId::NULL_SETTING_ID
};

Setting::SettingIdAndValue&  Setting::RStoredSetting_ =
*((Setting::SettingIdAndValue*)::PStoredSettingMem_);


//====================================================================
//
//  Public Methods...
//
//====================================================================

//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method: ~Setting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Setting::~Setting(void)
{
	CALL_TRACE("~Setting()");
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getApplicability(qualifierId)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return a boolean indicating whether this setting is currently applicable,
//  based on 'qualifierId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  resetState()  [virtual]
//
//@ Interface-Description
//  Reset the state of this setting, preparing it for changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Reset the override state of each of this setting's soft bounds, if
//  any.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::resetState(void)
{
	CALL_TRACE("resetState()");

	// ensure the reset of the forced-change flag, by disallowing virtual
	// calls that may not reset this flag...
	this->SettingSubject::resetForcedChangeFlag();

	updateAllSoftBoundStates_(NON_CHANGING);
}	// $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all setting
//  subjects that are also observers.  This method is to handle attaching
//  to this observer's subjects.  This method provides the default operation
//  of doing nothing, because if it's not overridden then this must not
//  be an observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	// do nothing, this subject must not be an observer...
}  // $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAcceptedValue()  [virtual, const]
//
//@ Interface-Description
//  Return this setting's current value from the Accepted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
Setting::getAcceptedValue(void) const
{
	CALL_TRACE("getAcceptedValue()");

	return(ContextMgr::GetAcceptedContext().getSettingValue(getId()));
}	// $[TI1]

//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAdjustedValue()  [virtual, const]
//
//@ Interface-Description
//  Return this batch setting's current value from the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchId(getId()))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
Setting::getAdjustedValue(void) const
{
	CALL_TRACE("getAdjustedValue()");
	AUX_CLASS_PRE_CONDITION((SettingId::IsBatchId(getId())), getId());

	return(ContextMgr::GetAdjustedContext().getSettingValue(getId()));
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setAdjustedValue(newAdjustedValue)  [virtual]
//
//@ Interface-Description
//  Store 'newAdjustedValue' as this setting's new value in the Adjusted
//  Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchId(getId()))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::setAdjustedValue(const SettingValue& newAdjustedValue)
{
	CALL_TRACE("setAdjustedValue(newAdjustedValue)");
	AUX_CLASS_PRE_CONDITION((SettingId::IsBatchId(getId())), getId());

	ContextMgr::GetAdjustedContext().setSettingValue(getId(), newAdjustedValue);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setAcceptedValue(newAcceptedValue)  [virtual]
//
//@ Interface-Description
//  Store 'newAcceptedValue' as this setting's new value in the Accepted
//  Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::setAcceptedValue(const SettingValue& newAcceptedValue)
{
	CALL_TRACE("setAcceptedValue(newAcceptedValue)");

	if ( SettingId::IsBatchId(getId()) )
	{	// $[TI1]
		ContextMgr::GetAcceptedContext().setBatchSettingValue(getId(),
															  newAcceptedValue);
	}
	else
	{	// $[TI2]
		ContextMgr::GetAcceptedContext().setNonBatchSettingValue(getId(),
																 newAcceptedValue);
	}
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getMaxLimit()  [virtual]
//
//@ Interface-Description
//  This virtual method is to be overridden by all alarm settings,
//  and only called for them.  Therefore, this version of this method is
//  NEVER to be called.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
Setting::getMaxLimit(void)
{
	CALL_TRACE("getMaxLimit()");

	// a failure of this assertion indicates that this method was incorrectly
	// called, and this setting is not considered an alarm setting...
	AUX_CLASS_PRE_CONDITION((SettingId::IsAlarmLimitId(getId())), getId());

	// a failure of this assertion indicates that, though this setting is
	// an alarm setting, it has not defined its own version of this virtual
	// function...
	AUX_CLASS_ASSERTION_FAILURE(getId());

	// this shall NEVER run...
	return(::MAX_REAL32_VALUE);
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getMinLimit()  [virtual]
//
//@ Interface-Description
//  This virtual method is to be overridden by the alarm settings,
//  and only called for them.  Therefore, this version of this method is
//  NEVER to be called.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
Setting::getMinLimit(void)
{
	CALL_TRACE("getMinLimit()");

	// a failure of this assertion indicates that this method was incorrectly
	// called, and this setting is not considered an alarm setting...
	AUX_CLASS_PRE_CONDITION((SettingId::IsAlarmLimitId(getId())), getId());

	// a failure of this assertion indicates that, though this setting is
	// an alarm setting, it has not defined its own version of this virtual
	// function...
	AUX_CLASS_ASSERTION_FAILURE(getId());

	// this shall NEVER run...
	return(::MAX_REAL32_VALUE);
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  isEnabledValue(value)  [virtual]
//
//@ Interface-Description
//  This virtual method is to be overridden by all discrete settings
//  whose values have varying states of applicability.  This version of
//  this method always returns 'TRUE', irregardless of 'value'.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[BL04079]
//---------------------------------------------------------------------
//@ PreCondition
//  (isDiscreteSetting())
//---------------------------------------------------------------------
//@ PostCondition
//  (TRUE)
//@ End-Method
//=====================================================================

Boolean
Setting::isEnabledValue(const DiscreteValue) const
{
	CALL_TRACE("isEnabledValue(value)");

	// a failure of this assertion indicates that this method was incorrectly
	// called for a non-discrete setting...
	AUX_CLASS_PRE_CONDITION((isDiscreteSetting()), getId());

	return(TRUE);
}  // $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  isChanged()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return a boolean indicating whether this setting has changed, or not.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented

//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  isNewlyApplicable()  [virtual, const]
//
//@ Interface-Description
//  This method returns TRUE if this setting is applicable in the 
//  adjusted context, but is not applicable in the accepted context,
//  otherwise it returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean Setting::isNewlyApplicable(void) const
{
	const Applicability::Id  ADJUSTED_APPLICABILITY = getApplicability(Notification::ADJUSTED);
	const Applicability::Id  ACCEPTED_APPLICABILITY = getApplicability(Notification::ACCEPTED);

	return ( ( ADJUSTED_APPLICABILITY != ACCEPTED_APPLICABILITY ) &&
			 ( ACCEPTED_APPLICABILITY == Applicability::INAPPLICABLE ) );
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  resetConstraints()  [virtual]
//
//@ Interface-Description
//  Reset this setting's minimum and maximum constraints, if any, to their
//  absolute min/max values, to ensure proper new-patient initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Because during New-Patient Setup, their could be "old" patient setting
//  values, and constraints, we need to reset these constraints to ensure
//  proper initialization.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::resetConstraints(void)
{
	CALL_TRACE("resetConstraints()");

	// this setting has no constriants, therefore do nothing...
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateToDefaultValue()  [virtual]
//
//@ Interface-Description
//  Set this setting's "accepted" value to it's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getAcceptedValue() == getDefaultValue())
//@ End-Method
//=====================================================================

void
Setting::updateToDefaultValue(void)
{
	CALL_TRACE("updateToDefaultValue()");

	// set this setting's adjusted value to its new-patient value...
	setAcceptedValue(getDefaultValue());
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateToNewPatientValue()  [virtual]
//
//@ Interface-Description
//  Set this batch setting's "adjusted" value to it's new-patient value.
//  Unlike other methods that store a value into the Adjusted Context, this
//  method will "set" this setting's forced-change flag after it stores it's
//  new-patient value.  This "setting" is done because initializing this
//  setting to it's new-patient value is considered a change that needs to
//  be communicated to the operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchId(getId()))
//---------------------------------------------------------------------
//@ PostCondition
//  (getAdjustedValue() == getNewPatientValue())
//  (isChanged())
//@ End-Method
//=====================================================================

void
Setting::updateToNewPatientValue(void)
{
	CALL_TRACE("updateToNewPatientValue()");
	AUX_CLASS_PRE_CONDITION((SettingId::IsBatchId(getId())), getId());

	// ensure state of setting is reset prior to continuation of New-Patient
	// Setup...
	resetState();

	// set this setting's adjusted value to its new-patient value...
	setAdjustedValue(getNewPatientValue());

	// set this setting's forced-change flag, because this initialization to a 
	// new-patient value is considered a change that is to be communicated
	// to the operator...
	setForcedChangeFlag();

	// now that we're initialized, update this setting's soft-bound states...
	updateAllSoftBoundStates_(NON_CHANGING);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateToNewIbwValue()  [virtual]
//
//@ Interface-Description
//  Update this setting for a changed IBW value. Called when the user
//  changes the current patient's IBW setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting's bounds are updated based on the changed IBW using 
//  the updateAllSoftBoundsStates_ method. Setting values are not changed
//  based on a changed IBW, but the settings' bounds are.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchId(getId()))
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

void
Setting::updateToNewIbwValue(void)
{
	CALL_TRACE("updateToNewIbwValue()");
	AUX_CLASS_PRE_CONDITION((SettingId::IsBatchId(getId())), getId());

	// need to reset the constraints to min/max so the constraints 
	// themselves can be adjusted based on the new IBW
	resetConstraints();

	// adjust this setting's constraints based on the new IBW 
	// in the adjusted context
	updateConstraints_();

	// update this setting's soft-bound states...
	updateAllSoftBoundStates_(NON_CHANGING);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  activationHappened()  [virtual]
//
//@ Interface-Description
//  The purpose of this method is to save the setting value when the
//  setting first gets selected.  This is in case the user "CLEARS" the
//  setting, after making some adjustments without accepting the changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::activationHappened(void)
{
	CALL_TRACE("activationHappened()");

	// store the ID and current adjusted value of this setting...
	Setting::StoreActivatedSetting_(getId(), getAdjustedValue());

	// update any dynamic bounds...
	updateConstraints_();
}	  // $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  calcNewValue()  [pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to respond to knob deltas from the operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  resetToStoredValue()
//
//@ Interface-Description
//  Reset this setting's value to the value stored upon activation.
//  This allows the operator easy "clearing" of the setting that is currently
//  being adjusted.  After resetting to the stored value, this method updates
//  this setting's dependent setting (if any) to be based on the reset
//  value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (getId() == Setting::GetStoredSettingId_())
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::resetToStoredValue(void)
{
	CALL_TRACE("resetToStoredValue()");
	AUX_CLASS_PRE_CONDITION((getId() == Setting::GetStoredSettingId_()),
							Uint32((getId() << 16) | Setting::GetStoredSettingId_()));

	// override all soft bounds during the resetting (non-quiescent) process
	updateAllDependentsSoftBoundStates_(TRUE);

	const SettingValue  STORED_VALUE = Setting::GetStoredSettingValue_();

	// get the current value...
	SettingValue  currentValue = getAdjustedValue();

	// update this setting's "adjusted" value with the stored value...
	setAdjustedValue(STORED_VALUE);

#if defined(SIGMA_DEVELOPMENT)
	if ( Settings_Validation::IsDebugOn(::IE_RATIO_SETTING) )
	{
		cout << "S::rTSV STORED==" << (BoundedValue(STORED_VALUE).value) << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	// update the soft bound state of this setting to reflect the stored value
	// to which it has been adjusted
	updateAllSoftBoundStates_(NON_CHANGING);

	// reset this setting's bound status...
	getBoundStatus_().reset();

	Boolean  isValueIncreasing;

	if ( SettingId::IsBoundedId(getId()) )
	{	// $[TI1]
		isValueIncreasing = ((BoundedValue)currentValue <
							 (BoundedValue)STORED_VALUE);
	}
	else if ( SettingId::IsDiscreteId(getId()) )
	{	// $[TI2]
		isValueIncreasing = ((DiscreteValue)currentValue <
							 (DiscreteValue)STORED_VALUE);
	}
	else if ( SettingId::IsSequentialId(getId()) )
	{	// $[TI3]
		isValueIncreasing = ((SequentialValue)currentValue <
							 (SequentialValue)STORED_VALUE);
	}
	else
	{
		// unexpected setting category...
		AUX_CLASS_ASSERTION_FAILURE(getId());
	}

#if defined(SIGMA_DEVELOPMENT)
	// store return value of 'calcDependentValues_()'...
	Boolean  dependentAbleToSettle =
#endif // defined(SIGMA_DEVELOPMENT)

	// unlike other transition methods, apnea inspiratory time updates its
	// dependent settings, this allows for simpler implementation by the
	// Main Control Settings...
	calcDependentValues_(isValueIncreasing);

#if defined(SIGMA_DEVELOPMENT)
	SAFE_AUX_CLASS_ASSERTION((dependentAbleToSettle),
							 ((isValueIncreasing << 16) | getId()));
#endif // defined(SIGMA_DEVELOPMENT)

	// update all the soft bounds for all of the dependent settings 
	// to reflect their current state
	updateAllDependentsSoftBoundStates_(FALSE);

	// this value was previously accepted, therefore it shall NEVER cause
	// a dependent setting to violate its bounds...
	SAFE_CLASS_ASSERTION((!getBoundStatus().isInViolation()));

	SAFE_CLASS_ASSERTION((isAdjustedValid()));
} 


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  testTransition(settingId, toValue, fromValue)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all settings that
//  determine conditional transitions.  This version of this virtual method
//  is NEVER to be called -- it asserts that fact.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Setting::TransitionState
Setting::testTransition(const SettingId::SettingIdType settingId,
						const DiscreteValue,
						const DiscreteValue)
{
	CALL_TRACE("resetToStoredValue(settingId, toValue, fromValue)");
	AUX_CLASS_ASSERTION_FAILURE(((getId() << 16) | settingId));

	// never gets this far...
	return(Setting::STAND_ALONE);
} 


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  acceptTransition(settingId, newValue, currValue)  [pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by all derived
//  classes to respond to Main Control Setting transitions.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  acceptPrimaryChange(primaryId)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all settings that
//  are dependent on another (primary) setting's value.  This version of
//  this virtual method is NEVER to be called -- it asserts that fact.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
Setting::acceptPrimaryChange(const SettingId::SettingIdType)
{
	CALL_TRACE("acceptPrimaryChange(primaryId)");
	AUX_CLASS_ASSERTION_FAILURE(getId());

	// never gets this far...
	return(NULL);
}


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  overrideSoftBound(boundId)
//
//@ Interface-Description
//  This method is called when the user has indicated that the soft-bound
//  indicated by 'boundId' is to be overridden.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::overrideSoftBound(const SettingBoundId boundId)
{
	CALL_TRACE("overrideSoftBound(boundId)");

	deactivateSoftBound_(boundId);
	updateConstraints_();
}  // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName,
				   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, SETTING,
							lineNumber, pFileName, pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  Setting(settingId, arrDependentSettingIds)  [Constructor]
//
//@ Interface-Description
//  Create a default setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Constructs a setting.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsId(settingId))
//  ARR_DEPENDENT_SETTING_IDS_ != NULL 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Setting::Setting(const SettingId::SettingIdType  settingId,
				 const SettingId::SettingIdType* arrDependentSettingIds)
: SettingSubject(settingId),
boundStatus_(settingId),
ARR_DEPENDENT_SETTING_IDS_(arrDependentSettingIds),
isNovRamUpdateDisabled_(FALSE )
{
	CALL_TRACE("Setting(settingId, arrDependentSettingIds)");
	AUX_CLASS_PRE_CONDITION((SettingId::IsId(settingId)),
							settingId);
	AUX_CLASS_PRE_CONDITION((ARR_DEPENDENT_SETTING_IDS_ != NULL),
							settingId);

	lowerSoftBoundInfo_.boundId  = NULL_SETTING_BOUND_ID;
	lowerSoftBoundInfo_.isActive = FALSE;

	upperSoftBoundInfo_.boundId  = NULL_SETTING_BOUND_ID;
	upperSoftBoundInfo_.isActive = FALSE;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
Setting::calcDependentValues_(const Boolean isValueIncreasing)
{
	CALL_TRACE("calcDependentValues_(isValueIncreasing)");

#if defined(SIGMA_DEVELOPMENT)
	if ( Settings_Validation::IsDebugOn(BOUNDED_SETTING) )
	{
		cout << "CALC-DEP-00:"
		<< "  id = " << SettingId::GetSettingName(getId())
		<< ", isIncr = " << isValueIncreasing
		<< endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	Boolean ableToSettleOnASatisfactoryValue = TRUE;

	if ( *ARR_DEPENDENT_SETTING_IDS_ != SettingId::NULL_SETTING_ID )
	{	// $[TI1] -- this setting has at least one dependent setting...
		// this flag is used to indicate whether a violation occurred in the
		// subsequent (not the first) dependent settings, this would require
		// that a re-resolution of all other dependent settings -- including
		// the first one...
		Boolean  didViolationOccur;

		Uint  loopCount = 0u;

		do
		{
#if defined(SIGMA_DEVELOPMENT)
			if ( Settings_Validation::IsDebugOn(BOUNDED_SETTING) )
			{
				cout << "CALC-DEP-01:"
				<< "  loopCount = " << loopCount
				<< endl;
			}
#endif // defined(SIGMA_DEVELOPMENT)

			// should never need more than three loops...
			if ( loopCount >= 3 )
			{	// $[TI1.3]
				ableToSettleOnASatisfactoryValue = FALSE;
				break;
			}	// $[TI1.4]

			// must be reset with each loop...
			didViolationOccur = FALSE;

			// store in local pointer for more optimal traversal...
			const SettingId::SettingIdType*  pDependentId =
			ARR_DEPENDENT_SETTING_IDS_;

			// resolve the first dependent setting's needed changes, based on this
			// primary setting's change...
			resolveDependentChange_(*pDependentId, isValueIncreasing);
			pDependentId++;

			// loop through each of the remaining dependent setting ids, if any, and
			// resolve their change with this primary setting's change...
			for ( ; *pDependentId != SettingId::NULL_SETTING_ID; pDependentId++ )
			{	// $[TI1.1] -- this setting has more than one dependent setting...
				// "or" in whether a new violation has occurred, or not; retain whether
				// a violation has occurred with ANY of the dependent settings...
				didViolationOccur =
				(resolveDependentChange_(*pDependentId, isValueIncreasing)  ||
				 didViolationOccur);
			}	// $[TI1.2] -- this setting has only one dependent setting...

			loopCount++;

			// if a violation occurred, or a previous violation occurred causing
			// a second loop, then loop through again to ensure all dependent
			// settings are synchronized with each other...
		} while ( didViolationOccur  ||  loopCount == 2 );
	}	// $[TI2] -- this setting has no dependent settings...

	return(ableToSettleOnASatisfactoryValue);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_() [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all settings
//  that have dynamic bounds.  This version of this virtual method
//  does nothing.  Whereas, the overridden versions are to update this
//  setting's dynamic bounds.
//
//  If a setting has no dynamic bounds, this method gets called, which
//  does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	// do nothing...
}	// $[TI1]


//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method:  resolveDependentChange_(dependentId, isValueIncreasing)
//
//@ Interface-Description
//  A virtual method that is to be overridden by all settings that have
//  dependent settings.  This method is called by 'calcDependentValues_()'
//  to handle the change to a dependent setting, and any adjustments needed
//  to this primary setting, given a dependent setting bound violation.
//
//  This class's version of this method is NEVER to be called -- it asserts
//  this restriction.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
Setting::resolveDependentChange_(const SettingId::SettingIdType dependentId,
								 const Boolean)
{
	CALL_TRACE("resolveDependentChange_(dependentId, isValueIncreasing)");

	AUX_CLASS_ASSERTION_FAILURE((Uint32((getId() << 16) | dependentId)));

	//-------------------------------------------------------------------
	// the remaining block of code is NEVER run...
	//-------------------------------------------------------------------

	return(FALSE);
}


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  registerSoftBound_(boundId, boundType)
//
//@ Interface-Description
//  Register the bound identified by 'boundId' as the lower or upper bound,
//  as identified by 'boundType'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assertion below is "SAFE", because failure of that test (at this
//  point) is not disastrous.  The 'overrideBound()' and 'isSoftBoundActive_()'
//  methods use non-SAFE assertions, therefore these are (psuedo) redundant.
//---------------------------------------------------------------------
//@ PreCondition
//  (boundType == Setting::LOWER  ||  boundType == Setting::UPPER)
//---------------------------------------------------------------------
//@ PostCondition
//  (findSoftBoundInfo_(boundId) != NULL)
//@ End-Method
//=====================================================================

void
Setting::registerSoftBound_(const SettingBoundId          boundId,
							const Setting::SoftBoundType_ boundType)
{
	CALL_TRACE("registerSoftBound_(boundId, boundType)");

	switch ( boundType )
	{
		case Setting::LOWER :	  // $[TI1]
			lowerSoftBoundInfo_.boundId  = boundId;
			lowerSoftBoundInfo_.isActive = TRUE;
			break;
		case Setting::UPPER :	  // $[TI2]
			upperSoftBoundInfo_.boundId  = boundId;
			upperSoftBoundInfo_.isActive = TRUE;
			break;
		default :
			// unexpected bound type...
			AUX_CLASS_ASSERTION_FAILURE(boundType);
			break;
	}

	SAFE_AUX_CLASS_ASSERTION((findSoftBoundInfo_(boundId) != NULL),
							 Uint32((boundId << 16) | getId()));
}


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  isSoftBoundActive_(boundId)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (findSoftBoundInfo_(boundId) != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
Setting::isSoftBoundActive_(const SettingBoundId boundId) const
{
	CALL_TRACE("isSoftBoundActive_(boundId)");

	Boolean  isActive;

	if ( boundId != NULL_SETTING_BOUND_ID )
	{  // $[TI1]
		const Setting::SoftBoundInfo*  pSoftBoundInfo = findSoftBoundInfo_(boundId);

		AUX_CLASS_PRE_CONDITION((pSoftBoundInfo != NULL),
								Uint32((boundId << 16) | getId()));

		isActive = pSoftBoundInfo->isActive;
	}
	else
	{  // $[TI2]
		isActive = FALSE;
	}

	return(isActive);
}


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  activateSoftBound_(boundId)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (findSoftBoundInfo_(boundId) != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//  (isSoftBoundActive_(boundId))
//@ End-Method
//=====================================================================

void
Setting::activateSoftBound_(const SettingBoundId boundId)
{
	CALL_TRACE("activateSoftBound_(boundId)");

	// casting away const-ness...
	Setting::SoftBoundInfo*  pSoftBoundInfo =
	(Setting::SoftBoundInfo*)findSoftBoundInfo_(boundId);

	AUX_CLASS_PRE_CONDITION((pSoftBoundInfo != NULL),
							Uint32((boundId << 16) | getId()));

	pSoftBoundInfo->isActive = TRUE;
}	// $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  deactivateSoftBound_(boundId)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (findSoftBoundInfo_(boundId) != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//  (!(isSoftBoundActive_(boundId)))
//@ End-Method
//=====================================================================

void
Setting::deactivateSoftBound_(const SettingBoundId boundId)
{
	CALL_TRACE("deactivateSoftBound_(boundId)");

	// casting away const-ness...
	Setting::SoftBoundInfo*  pSoftBoundInfo =
	(Setting::SoftBoundInfo*)findSoftBoundInfo_(boundId);

	AUX_CLASS_PRE_CONDITION((pSoftBoundInfo != NULL),
							Uint32((boundId << 16) | getId()));

	pSoftBoundInfo->isActive = FALSE;
}	// $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  updateAllDependentsSoftBoundStates_(isResetting) 
//
//@ Interface-Description
//  This method is called when it is desired either
//  to update the soft bound states for all of the dependent settings
//  of this primary setting, or to override them all.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::updateAllDependentsSoftBoundStates_(const Boolean isResetting)
{
	CALL_TRACE("updateAllDependentsSoftBoundStates_(isResetting)");

	if ( *ARR_DEPENDENT_SETTING_IDS_ != SettingId::NULL_SETTING_ID )
	{	// $[TI1] -- this setting has at least one dependent setting...
		// store in local pointer for more optimal traversal...
		const SettingId::SettingIdType*  pDependentId =
		ARR_DEPENDENT_SETTING_IDS_;

		Setting*  pSetting; 

		// loop through each of the dependent setting ids, and
		// overried or update their soft bound state, depending on isResetting...
		for ( ; *pDependentId != SettingId::NULL_SETTING_ID; pDependentId++ )
		{  // $[TI1.1] -- this path is ALWAYS executed
			pSetting = SettingsMgr::GetSettingPtr(*pDependentId);

			if ( isResetting )
			{  // $[TI1.1.1] -- each dependent setting is being reset
				// override soft bounds in order to allow setting value to reset
				pSetting->overrideAllSoftBoundStates_();
			}
			else
			{  // $[TI1.1.2] -- update dependent's soft bounds based on current values
				pSetting->updateAllSoftBoundStates_(NON_CHANGING);
			}
		}
	}  // $[TI2] -- implied else, this setting has no dependent settings...
}


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  overrideAllSoftBoundStates_(void)
//
//@ Interface-Description
//  This method is responsible for overriding/disabling all of the
//  soft bounds (if any) of this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (!(isSoftBoundActive_(lowerSoftBoundInfo_.boundId)))
//  (!(isSoftBoundActive_(upperSoftBoundInfo_.boundId)))
//@ End-Method
//=====================================================================

void
Setting::overrideAllSoftBoundStates_(void)
{
	CALL_TRACE("overrideAllSoftBoundStates_()");

	lowerSoftBoundInfo_.isActive = FALSE;
	upperSoftBoundInfo_.isActive = FALSE;

	updateConstraints_();
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  updateAllSoftBoundStates_(changeState)  [virtual]
//
//@ Interface-Description
//  This method is responsible for checking the newly-proposed value
//  of this setting against its soft bounds (using 'changeState', as well),
//  to see if the newly-proposed value has passed back into a soft bound's
//  non-violation region.  When this happens, the soft bound needs to be
//  re-activated.
//  In addition, if we are in a non-changing state, and the value of
//  of the setting is in the violation range, then it is assumed that
//  the soft bound should be deactivated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Because this method has to compare this setting's current ADJUSTED
//  value, it must be a batch setting, and because we're comparing values,
//  it can't be a discrete setting.  As of this writing, no sequential
//  settings had soft bounds, therefore this implementation was kept
//  simple by only handling bounded setting values.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchBoundedId(getId()))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::updateAllSoftBoundStates_(const ChangeState changeState)
{
	CALL_TRACE("updateAllSoftBoundStates_(changeState)");

	Real32  softMinValue, softMaxValue; 

	// get the "soft" minimum and maximum values...
	findSoftMinMaxValues_(softMinValue, softMaxValue);

	Boolean  doUpdateConstraints = FALSE;

	//-------------------------------------------------------------------
	// update state of this setting's soft maximum constraint...
	//-------------------------------------------------------------------

	if ( upperSoftBoundInfo_.boundId != NULL_SETTING_BOUND_ID )
	{  // $[TI1] -- this setting has an upper soft bound...
		// currently, only batch, bounded settings can have soft bounds...
		AUX_CLASS_PRE_CONDITION((SettingId::IsBatchBoundedId(getId())), getId());

		const Boolean  IS_SOFT_BOUND_ACTIVE =
		isSoftBoundActive_(upperSoftBoundInfo_.boundId);

		const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

		if ( ABSOLUTE_MAX > softMaxValue )
		{  // $[TI1.1] -- soft bound more restrictive than absolute hard bound...
			const Real32  ADJUSTED_VALUE = BoundedValue(getAdjustedValue()).value;

			if ( ADJUSTED_VALUE <= softMaxValue  &&  !IS_SOFT_BOUND_ACTIVE )
			{  // $[TI1.1.1] -- within nonviolation range and currently inactive...
				activateSoftBound_(upperSoftBoundInfo_.boundId);
			}
			else
			{  // $[TI1.1.2] -- in violation range, and/or already active...
				if ( ADJUSTED_VALUE > softMaxValue  &&  IS_SOFT_BOUND_ACTIVE )
				{  // $[TI1.1.2.1] -- within violation region and currently active...
					if ( changeState == Setting::NON_CHANGING )
					{  // $[TI1.1.2.1.1] -- this setting's value is not changing, so soft
						//                   bound should be deactivated, because already
						//                   in violation range from earlier...
						deactivateSoftBound_(upperSoftBoundInfo_.boundId);
					}  // $[TI1.1.2.1.2] -- implied else, setting is changing
				}  // $[TI1.1.2.2] -- within nonviolation range or not currently active
			}
		}
		else
		{  // $[TI1.2] -- absolute hard bound more restrictive than soft bound...
			// deactivate soft bound...
			deactivateSoftBound_(upperSoftBoundInfo_.boundId);
		}

		doUpdateConstraints = TRUE;
	}  // $[TI2] -- this setting has no upper soft bound...

	//-------------------------------------------------------------------
	// update state of this setting's soft minimum constraint...
	//-------------------------------------------------------------------

	if ( lowerSoftBoundInfo_.boundId != NULL_SETTING_BOUND_ID )
	{  // $[TI3] -- this setting has a lower soft bound...
		// currently, only batch, bounded settings can have soft bounds...
		AUX_CLASS_PRE_CONDITION((SettingId::IsBatchBoundedId(getId())), getId());

		const Boolean  IS_SOFT_BOUND_ACTIVE =
		isSoftBoundActive_(lowerSoftBoundInfo_.boundId);

		const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

		if ( ABSOLUTE_MIN < softMinValue )
		{  // $[TI3.1] -- soft bound is more restrictive than absolute hard bound...
			const Real32  ADJUSTED_VALUE = BoundedValue(getAdjustedValue()).value;

			if ( ADJUSTED_VALUE >= softMinValue  &&  !IS_SOFT_BOUND_ACTIVE )
			{  // $[TI3.1.1] -- within nonviolation range and currently inactive...
				activateSoftBound_(lowerSoftBoundInfo_.boundId);
			}
			else
			{  // $[TI3.1.2] -- in violation range, and/or already active...
				if ( ADJUSTED_VALUE < softMinValue  &&  IS_SOFT_BOUND_ACTIVE )
				{  // $[TI3.1.2.1] -- within violation region and currently active...
					if ( changeState == Setting::NON_CHANGING )
					{  // $[TI3.1.2.1.1] -- this setting's value is not changing, so soft
						//                   bound should be deactivated, because already
						//                   in violation range from earlier...
						deactivateSoftBound_(lowerSoftBoundInfo_.boundId);
					}  // $[TI3.1.2.1.2] -- implied else, setting is changing
				}  // $[TI3.1.2.2] -- within nonviolation range or not currently active
			}
		}
		else
		{  // $[TI3.2] -- absolute hard bound is more restrictive than soft bound...
			// deactivate soft bound...
			deactivateSoftBound_(lowerSoftBoundInfo_.boundId);
		}

		doUpdateConstraints = TRUE;
	}  // $[TI4] -- this setting has no lower soft bound...

	if ( doUpdateConstraints )
	{  // $[TI5]
		updateConstraints_();
	}  // $[TI6]
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  This returns the absolute maximum value that is currently available
//  from this setting.  This method MUST be overridden by a derived class,
//  or not called for this instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
Setting::getAbsoluteMaxValue_(void) const
{
	CALL_TRACE("getAbsoluteMaxValue_()");
	AUX_CLASS_ASSERTION_FAILURE(getId());

	return(0.0f);
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  This returns the absolute minimum value that is currently available
//  from this setting.  This method MUST be overridden by a derived class,
//  or not called for this instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
Setting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");
	AUX_CLASS_ASSERTION_FAILURE(getId());

	return(0.0f);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//
//  This implements the default response by doing nothing.  For those
//  settings that have soft bounds, this method must be overridden to
//  provide these values.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (lowerSoftBoundInfo_.boundId == NULL_SETTING_BOUND_ID  &&
//   upperSoftBoundInfo_.boundId == NULL_SETTING_BOUND_ID)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Setting::findSoftMinMaxValues_(Real32&, Real32&) const
{
	CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");
	AUX_CLASS_PRE_CONDITION(
						   (lowerSoftBoundInfo_.boundId == NULL_SETTING_BOUND_ID  &&
							upperSoftBoundInfo_.boundId == NULL_SETTING_BOUND_ID),
						   getId()
						   );

	// do nothing...
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  ventset() [virtual]
//
//@ Interface-Description
//  This is a virtual function to implement the VentSet command 
//  interface to the settings subsystem. Derived classes implement
//  this method to change the setting value to the specified parameter
//  string. This base class method returns SigmaStatus of FAILURE
//  since this functionality is not implemented in the base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
SigmaStatus
Setting::ventset(const char*)
{
	return FAILURE;
}

//====================================================================
//
//  Private Methods...
//
//====================================================================

//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  findSoftBoundInfo_(boundId)
//
//@ Interface-Description
//  Return a pointer to the soft-bound information that matches 'boundId'.
//  'NULL' is returned if not found.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const Setting::SoftBoundInfo*
Setting::findSoftBoundInfo_(const SettingBoundId boundId) const
{
	CALL_TRACE("findSoftBoundInfo_(boundId)");

	const Setting::SoftBoundInfo*  pSoftBoundInfo;

	if ( lowerSoftBoundInfo_.boundId == boundId )
	{  // $[TI1] -- 'boundId' matches this setting's lower soft bound...
		pSoftBoundInfo = &lowerSoftBoundInfo_;
	}
	else if ( upperSoftBoundInfo_.boundId == boundId )
	{  // $[TI2] -- 'boundId' matches this setting's upper soft bound...
		pSoftBoundInfo = &upperSoftBoundInfo_;
	}
	else
	{  // $[TI3] -- no match found...
		pSoftBoundInfo = NULL;
	}

	return(pSoftBoundInfo);
}
