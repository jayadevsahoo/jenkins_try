#ifndef PressureController_HH
#define PressureController_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PressureController - Implements a pressure controller using a
//		PSOL for flow delivery.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureController.hhv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added getDesiredPressure().
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005 By: syw    Date: 03-Jul-1996   DR Number: 1073
//  	Project:  Sigma (R8027)
//		Description:
//			Added initIntegratorValue_ and set methods  to accommodate
//			Controls Spec changes.
//
//  Revision: 004 By: syw    Date: 06-Feb-1996   DR Number: 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added getFlowCommadLimit() method.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "MathUtilities.hh"

#include "Resistance.hh"


//@ Usage-Classes

class PressureSensor ;
class FlowController ;

//@ End-Usage

class PressureController 
{
  public:
   	PressureController( const PressureSensor& inspPressure,
	    	   			const PressureSensor& expPressure,
	       				FlowController& o2FlCtrl,
	    				FlowController& airFlCtrl) ;
	~PressureController( void) ;

   	static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32 lineNumber,
						   const char* pFileName  = NULL,
						   const char* pPredicate = NULL) ;

	inline void	setKp( const Real32 kp) ;
	inline Real32 getWf( void) const ;
	inline Real32 getErrorGain( void) const ;
	inline Real32 getFlowCommandLimit( void) const ;
	inline void setInitIntegratorValue( const Real32 lastDesiredFlow) ;	
	inline Real32 getDesiredPressure( void) const ;

	void setWf( const Real32 wf) ;
    void newBreath( const Real32 kp, const Real32 ki, const Real32 alpha) ;
    void updatePsol( const Real32 desiredPressure, const Real32 targetPressure, Int32 elapsedTime) ;
	void determineErrorGain( const Real32 referenceTarget,
							 const Real32 targetPressure) ;

	//Returns the Kp gain
    Real32 getKp() { return kp_; }

    //Returns the Ki gain
    Real32 getKi() { return ki_; }

    //Sets the Ki gain.
	void setKi( Real32 ki );

  protected:

  private:
   	PressureController( void) ;							// Declared but not implemented
   	PressureController( const PressureController&) ;	// Declared but not implemented
   	void operator=( const PressureController&) ;   		// Declared but not implemented

	//@ Data-Member: kp_
    // proportional gain in lpm/cmH2O
	Real32 kp_ ;

    //@ Data-Member: ki_
	// integral gain in lpm/cmH2O-msec
    Real32 ki_ ;

	//@ Data-Member: integrator_
    // integral of pressure error
	Real32 integrator_ ;

    //@ Data-Member: wf_
	// the weighting factor range 0 <= wf <= 1.0.  Used to determine feedback signal.
    Real32 wf_ ;

	//@ Data-Member: flowCommandLimit_ 
    // maximum flow command in lpm
	Real32 flowCommandLimit_ ; 

	//@ Data-Member: rInspPressureSensor_
	//  the inspiratory pressure sensing element in the control circuit
	const PressureSensor& rInspPressureSensor_ ;

	//@ Data-Member: rExhPressureSensor_
	//  the expiratory pressure sensing element in the control circuit
	const PressureSensor& rExhPressureSensor_ ;

	//@ Data-Member: rAirFlowController_
	// reference to air flow controller
	FlowController& rAirFlowController_ ;

    //@ Data-Member: rO2FlowController_
	// reference to O2  flow controller
	FlowController& rO2FlowController_ ;

	//@ Data-Member: desiredPressure_
	// input to pressureController
	Real32 desiredPressure_ ;

	//@ Data-Member: errorGain_
	// gain to amplify error
	Real32 errorGain_ ;

	//@ Data-Member: crossingFlag_
	// set true when expPressure < referenceTarget
	Boolean crossingFlag_ ;

	//@ Data-Member: error_
	// controller error
	Real32 error_ ;

	//@ Data-Member: initIntegratorValue_ ;
	// initial integrator value
	Real32 initIntegratorValue_ ;

	//@ Data-Member: trajectoryAlpha_
	// alpha constant for trajectory alpha filter
	Real32 trajectoryAlpha_ ;

	//@ Data-Member: kpInitial_
    // proportional gain in lpm/cmH2O
	Real32 kpInitial_ ;

	//@ Data-Member: kpFinal_
    // proportional gain in lpm/cmH2O
	Real32 kpFinal_ ;

	//@ Data-Member: kiInitial_
    // proportional gain in lpm/cmH2O
	Real32 kiInitial_ ;

	//@ Data-Member: kiFinal_
    // proportional gain in lpm/cmH2O
	Real32 kiFinal_ ;

	Boolean targetReached_;
	Real32 prevDesiredFlow_;

	//@ Data-Member: exhResistance_ ;
	// expiratory side resistance
	Resistance exhResistance_ ;

	Real32 desPressFilter_;
	Real32 desPressAlpha_;
	Real32 prevKi_;

#if CONTROLS_TUNING	
	Boolean overrideKp_;
	Boolean overrideKi_;
#endif
} ;

// Inlined methods
#include "PressureController.in"

#endif // PressureController_HH 


