#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: DownloadFault - Software fault handler class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is the entry point for all the download software faults.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a central "hub" for all diagnostic faults
//  to enter and generate a specific fault information promptly 
//  instead of program behaving mysteriously when these fault
//  conditions are not immediately detected.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Generate debugging information which includes line number,
//   file name and other fault conditions. On a target machine, display
//   a software fault error on the system LED and wait to be reset
//   by an operator.
//---------------------------------------------------------------------
//@ Fault-Handling
//  This class does no fault-handling of itself.  This class strictly
//  responds to faults that occur.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//@(#) $Header:   /840/Baseline/Download/vcssrc/DownloadFault.ccv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $ 
//  
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//   Revision: 001   By: jhv   Date: 24-Jan-1996   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//=====================================================================

#include "DownloadFault.hh"
#include "DownloadError.hh"

#include <stdlib.h>
#if defined(SIGMA_SUN)
#endif //defined(SIGMA_SUN)

#include <stdio.h>

#if	defined(SIGMA_TARGET)
extern "C" void LightDiagnosticLED(Uint8 eCode);
#endif //defined(SIGMA_TARGET)

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultId,  moduleId, lineNumber,
//  	               pFileName, pBoolTest)  [static]
//
//@ Interface-Description
//  Log the occurrence of this software fault, then issue a system
//  reset.
//---------------------------------------------------------------------
//@ Implementation-Description
//    none.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DownloadFault::SoftFault(const FaultType softFaultId,
			  const SubSystemID subSystemId,
			const Uint32      moduleId,
			const Uint32      lineNumber,
			const char*       pFileName,
			const char*       pBoolTest)
{
#if	defined(SIGMA_DEBUG) || !defined(SIGMA_TARGET) 
	printf("\n\n");
    switch (softFaultId)
    {
    case PRE_CONDITION :
      printf("PRE-CONDITION\n");
      break;
    case ASSERTION :
      printf("ASSERTION\n");
      break;
    default :
      DownloadFault::SoftFault(ASSERTION, DOWNLOAD, DOWNLOAD_FAULT_CLASS,
			      __LINE__, __FILE__, "(FALSE)");
      break;
    }

    printf(" FAILURE {\n");

    if (pBoolTest != NULL)
    {
      printf("Failed Test: %s\n", pBoolTest);
    }

    printf("   SubSystemId:  %d\n", subSystemId);
    printf("   ModuleId:     %d\n", moduleId);

    if (pFileName != NULL)
    {
      printf("   File:         %s\n", pFileName );
    }
    printf("   Line:         %d\n",lineNumber);

    printf( "};\n");
#endif //defined(SIGMA_DEBUG) || !defined(SIGMA_TARGET)

#if	defined(SIGMA_TARGET)
	::LightDiagnosticLED (DOWNLOAD_SOFTFAULT);
#endif //defined(SIGMA_TARGET)

	abort();
}
