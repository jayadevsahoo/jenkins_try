#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbSampleData
//---------------------------------------------------------------------
//@ Interface-Description
//  Virtual base class for trend sample data.  Clients use the derived
//  class to handle the different types of trended values: averaged,
//  last sampled "non averaged", RM, or events. 
//  
//  Client use the setValue() to set the trended value and getValue
//  to retrieve the trended value for a given timescale.  The
//  compressData() method is called once at the end of the time period
//  (1Hr - 10sec, 2Hr - 20sec, etc) to compute the sample value for
//  the timescale.
// 
//---------------------------------------------------------------------
//@ Rationale
//  Generalizes the handling of trend values, regardless of the type
//---------------------------------------------------------------------
//@ Implementation-Description
//  This base class relies on the derived class to implement the
//  compressData() and clear_() methods for the type of data they define.  
// 
//  For each trend frame, the setValue() method is called on the 1 hr
//  timescale. At the end of each timescale period, the compressData()
//  method is called to set the next value for that timescale. 
//  
//  Since getValue() is called by the client to retrieve the data for a
//  given timescale at a given timestamp, the above compressData() method
//  must be called for that timestamp/timescale first. 
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSampleData.ccv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  mjf    Date:  28-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
//
//=====================================================================

#include <string.h>

#include "Trend_Database.hh"
#include "TrendDbSampleData.hh"

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleData [constructor]
//
//@ Interface-Description
//  Construct with the source data Id (PatientDataId) and TrendSelectId
//  of the trended value
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the local class variables.
//  Clears the buffered values
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleData::TrendDbSampleData( PatientDataId::PatientItemId id,TrendSelectValue::TrendSelectValueId trendId )
{
	id_.patient = id;
	trendId_ = trendId;
	clear();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendSampleData [constructor]
//
//@ Interface-Description
//  Construct with the source data Id (SettingIdType) and TrendSelectId
//  for the trended value
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the private class variables.
//  Clears the buffered values.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleData::TrendDbSampleData( SettingId::SettingIdType id,TrendSelectValue::TrendSelectValueId trendId )
{
	id_.setting = id;
	trendId_ = trendId;
	clear();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleData [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleData::~TrendDbSampleData() 
{
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: compressData [pure virtual]
//
//@ Interface-Description
//  This method must be defined be the derived classes to compress/average
//  the data into the higher timescales. It is called once per timescale
//  period for each of the timescales (ie 2Hr - 20 sec, 4Hr - 40 sec, etc)
//---------------------------------------------------------------------
//@ Implementation-Description
//  not implemented
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear_ [pure virtual]
//
//@ Interface-Description
//  This method must be implemented by the derived class to clear the 
//  buffered data specific to that class.
//	This method is called from the public clear() method which is
//  defined in this base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  not implemented
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
//  Clear the TrendDbSampleData object
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clears the valueArray and then calls the virtual clear_() method,
//  implemented in the derived classes, so that derived data is also
//  cleared.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleData::clear( void )
{
	TREND_MGR_DBG4( 8, "Clear SampleData Id ", id_.rawValue,
					", Trend Id ", trendId_ );

	memset( valueArray_, 0x00, sizeof( valueArray_ ) );
	clear_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValue
//
//@ Interface-Description
//  Sets the value for the provided timescale
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method, so that derived classes (such as
//  TrendDbSampleRM) can overload if needed.
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the provided timescale is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleData::setValue( const TrendValue & newValue, TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION((timescale < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES), timescale );
	valueArray_[ timescale ] = newValue;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbSampleData::SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_SAMPLE_DATA,
							lineNumber, pFileName, pPredicate);
}


