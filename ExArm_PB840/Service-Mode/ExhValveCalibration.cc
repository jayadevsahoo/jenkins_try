#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  ExhValveCalibration - Calibrate the exhalation valve
//---------------------------------------------------------------------
//@ Interface-Description
//	This class implements the exhalation valve calibration,
//	generating a table of exhalation valve DAC commands vs
//	system pressure.  The flow and pressure sensors are cross-checked
//	prior to performing the calibration, and the calibration does
//	not proceed if any of the cross-checks fails.
//	During the calibration, a fixed expiratory-controlled flow
//	is established and the EV DAC is slowly incremented, and
//	an array of EV DAC commands vs expiratory pressure is created.
//	Various tests are performed throughout the EV DAC ramping to ensure
//	the calibration is proceeding correctly.
//
//	At the end of a successful calibration, the EV calibration table
//	is built from the measured data, and this table is stored in FLASH.
//
//	In addition, during the EV DAC ramping the EV DAC count and EV loopback
//	sensor DAC count are measured at the highest and lowest test points,
//	and at the end of a successful calibration the slope and intercept
//	of the resulting line (EV loopback count vs EV command count) is
//	stored in FLASH.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the exhalation valve calibration.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() performs the exhalation valve calibration.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ExhValveCalibration.ccv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 008  By:  quf    Date: 09-Oct-1997    DR Number: DCS 2452
//       Project:  Sigma (840)
//       Description:
//			Cleaned up measureTestPoints_() and calibrateExhValve_(),
//			added several new private methods for use in measureTestPoints_().
//
//  Revision: 007  By:  quf    Date: 09-Oct-1997    DR Number: DCS 1865
//       Project:  Sigma (840)
//       Description:
//			Removed const EV_LOOPBACK_OFFSET from here and moved it
//			to SmConstants.hh.
//
//  Revision: 006  By:  quf    Date: 26-Sep-1997    DR Number: DCS 2525
//       Project:  Sigma (840)
//       Description:
//            Changed novram status flag set method.
//
//  Revision: 005  By:  quf    Date: 19-Aug-1997    DR Number: DCS 2338
//       Project:  Sigma (840)
//       Description:
//            Added condition 19 = flash programming error
//
//  Revision: 004  By:  quf    Date: 19-Aug-1997    DR Number: DCS 2193
//       Project:  Sigma (840)
//       Description:
//            Added pressure transducer error and EV hysteresis to the
//            loopback current bounds check to reduce false failures.
//
//  Revision: 003  By:  quf    Date: 02-Jul-1997    DR Number: DCS 2107
//       Project:  Sigma (840)
//       Description:
//            Cal-in-progress novram flag is now an enum instead of a
//            Boolean, and the setting of this flag in this class
//            is fixed accordingly.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//            Removed "extern...PFlashMemory" -- no longer required.
//			  Added comments to measureTestPoints_() for easier comparison
//            to 840 Self-Tests Spec.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "ExhValveCalibration.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmFlowController.hh"
#include "SmPressureController.hh"
#include "SmExhValveController.hh"
#include "SmUtilities.hh"
#include "SmManager.hh"
#include "SmGasSupply.hh"
#include "SmConstants.hh"
#include "ServiceModeNovramData.hh"

#include "Task.hh"

#include "ValveRefs.hh"
#include "MainSensorRefs.hh"
#include "VentObjectRefs.hh"
#include "TemperatureSensor.hh"
#include "MiscSensorRefs.hh"
#include "LinearSensor.hh"
#include "ExhFlowSensor.h"
#include "Compressor.hh"

#include "ExhalationValve.hh"
#include "ExhValveTable.hh"
#include "CalInfoRefs.hh"
#include "CalInfoFlashBlock.hh"
#include "TestData.hh"

//@ End-Usage

#if defined(SIGMA_DEVELOPMENT)
# include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"
# include "ExhValveCalibration_UT.hh"
# include "FakeTask.hh"
#endif  // SIGMA_UNIT_TEST


//@ Code...

//@ Constant: MIN_CALIB_PRESSURE
// lowest calibration pressure
const Uint32 MIN_CALIB_PRESSURE = 1;  // cm H2O

//@ Constant: MAX_CALIB_PRESSURE
// highest calibration pressure
const Uint32 MAX_CALIB_PRESSURE = 108;  // cm H2O

//@ Constant: STEP_INTERVAL
// delay time between EV DAC increments
const Int32  STEP_INTERVAL = 20;  // ms

//@ Constant: DWELL_TIME
// dwell time after target pressure is reached
const Int32  DWELL_TIME = 200 ;  // ms

//@ Constant: EV_LOOPBACK_ACCURACY
// accuracy of EV loopback current
const Real32 EV_LOOPBACK_ACCURACY = 0.04;

//@ Constant: MIN_EV_SLOPE
// slope of lower EV current vs pressure bound line
const Real32 MIN_EV_SLOPE = 11.1;  // mA per cm H2O

//@ Constant: MAX_EV_SLOPE
// slope of upper EV current vs pressure bound line
const Real32 MAX_EV_SLOPE = 13.0;  // mA per cm H2O

//@ Constant: MIN_EV_OFFSET
// offset of lower EV current vs pressure bound line
const Real32 MIN_EV_OFFSET =  2.0;  // mA

//@ Constant: MAX_EV_OFFSET
// offset of upper EV current vs pressure bound line
const Real32 MAX_EV_OFFSET = 44.0;  // mA

//@ Constant: GAIN_TEST_MIN
// minimum value of (delta EV DAC counts) / (delta pressure)
// as measured between two successive integer pressure points
const Real32 GAIN_TEST_MIN = 19.0;  // counts per cm H2O

//@ Constant: NORMAL_PRESSURE_TIMEOUT
// timeout between successive integer preesure points
const Int32 NORMAL_PRESSURE_TIMEOUT = 2500;  // ms

//@ Constant: INITIAL_PRESSURE_TIMEOUT
// timeout to reach the initial 1 cmH2O pressure point
const Int32 INITIAL_PRESSURE_TIMEOUT = 71500;  // ms

const Real32 ExhValveCalibration::EXH_CAL_STEP = 1.0f ;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhValveCalibration()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.  No arguments.  Sets up array of pressure levels at
//	which to make the EV DAC command vs expiratory pressure readings at.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the test pressure array.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExhValveCalibration::ExhValveCalibration(void)
{
	CALL_TRACE("ExhValveCalibration::ExhValveCalibration(void)") ;
	Uint32 idx;

	// set up test pressures: 5, 25, 50, 75, 100 cmH2O
	testPressure_[0] = 5.0;
	for (idx = 1; idx < NUM_TEST_PRESSURES; idx++)
	{
	// $[TI2]
		testPressure_[idx] = idx * 25.0F;
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExhValveCalibration()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExhValveCalibration::~ExhValveCalibration(void)
{
  CALL_TRACE("~ExhValveCalibration()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest(void)
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.  It is used
//  to perform system setup steps, to do sensor checks, and to
//	calibrate the exhalation valve after system is setup properly and
//	sensors are judged to be OK.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set "EV calib status flag" to IN_PROGRESS to indicate calibration
//	in progress, in case of test interruption (e.g. power fail).
//	If flow sensor data stored in FLASH is not valid, generate
//	a FAILURE and exit.
//	If AC power is not detected, prompt user to connect AC,
//	and if AC is not connected generate FAILURE and exit.
//	If no air source is connected prompt user to connect air,
//	and if air is not connected generate FAILURE and exit.
//	Prompt user to remove insp filter and connect test circuit.
//	Perform sensor checks, generate	ALERTs and/or FAILUREs as
//	appropriate, exit test if FAILURE occurred or if ALERT was not
//	overridden.
//	If sensor checks passed or ALERT was overridden, execute
//	calibrateExhValve_().
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ExhValveCalibration::runTest(void)
{
	CALL_TRACE("ExhValveCalibration::runTest(void)");

    // Set novram EV cal status flag to IN_PROGRESS at start of cal
	ServiceModeNovramData::SetExhValveCalStatus(ServiceMode::IN_PROGRESS);

	// first check if flow sensor info is valid, allowing
	// newCycle()'s to perform in "background" task
	if (!ServiceMode::IsFlowSensorInfoValid())
	{

		DEBUGMSG( "Bad flow sensor data" );
	// $[TI1]
		// Flow sensor info is invalid
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_1);
	}
	else
	{
	// $[TI2]
		// flow sensor data is valid

		Boolean acDetected = TRUE;
		Boolean operatorExit = FALSE;

#ifdef SIGMA_UNIT_TEST
		SmUtilities::TestResult acResult =
								ExhValveCalibration_UT::AcResult;
#else
		SmUtilities::TestResult acResult =
								RSmUtilities.checkAcPower();
#endif  // SIGMA_UNIT_TEST

		switch( acResult)
		{
			case SmUtilities::AC_CONNECTED:
			// $[TI2.1]
				// AC power is connected
				break;				

			case SmUtilities::AC_NOT_CONNECTED:
			// $[TI2.2]
				// Running on BPS power
				RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_FAILURE,
						SmStatusId::CONDITION_18);

				acDetected = FALSE;
				break;

			case SmUtilities::OPERATOR_EXIT:
			// $[TI2.3]
				// EXIT button was pressed
				operatorExit = TRUE;
				break;

			default:
			// $[TI2.4]
				CLASS_ASSERTION_FAILURE();
				break;
		}
		// end switch

		if (!operatorExit && acDetected)
		{
		// $[TI2.5]
			// AC power is connected

#ifdef SIGMA_UNIT_TEST
			SmUtilities::TestResult airResult =
							ExhValveCalibration_UT::AirResult;
#else
			SmUtilities::TestResult airResult =
							RSmUtilities.verifyAirConnected();
#endif  // SIGMA_UNIT_TEST

			switch (airResult)
			{
				case SmUtilities::GAS_CONNECTED:
				// $[TI2.5.1]
					// prompt user to remove insp filter and
					// connect test circuit
					if ( RSmUtilities.messagePrompt (
							SmPromptId::REMOVE_INSP_FILTER_PROMPT) )
					{
						calibrateExhValve_();
					}
					else
					{
					// $[TI2.5.1.2]
						// EXIT button was pressed
						operatorExit = TRUE;
					}
					// end else

					break;

				case SmUtilities::GAS_NOT_CONNECTED:
				// $[TI2.5.2]
					// No air connected
					RSmManager.sendStatusToServiceData(
							SmStatusId::TEST_FAILURE,
							SmStatusId::CONDITION_2);

					break;

				case SmUtilities::OPERATOR_EXIT:
				// $[TI2.5.3]
					// EXIT button was pressed
					operatorExit = TRUE;
					break;

				default:
				// $[TI2.5.4]
					CLASS_ASSERTION_FAILURE();
					break;
			}
			// end switch
		}
		// $[TI2.6]
		// implied else: operator hit EXIT button or AC not connected

		if (operatorExit)
		{
		// $[TI2.7]
			RSmManager.sendOperatorExitToServiceData();
		}

		RSmFlowController.stopFlowController();
		RSmUtilities.openExhValve();
		// $[TI2.8]
		// implied else
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
ExhValveCalibration::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, EXHVALVECALIBRATION,
                          lineNumber, pFileName, pPredicate);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calibrateExhValve_(void)
//
//@ Interface-Description
//  This method takes no arguments and has no return value.
//  It is used to calibrate the exhalation valve and to build then 
//	store the calibration table in FLASH.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09113]
//	Measure the calibration test points, check for FAILUREs as
//	measurements are made, exit once any FAILURE is detected.
//	Calculate the slope and offset of EV command count vs EV loopback
//	current count, generate a FAILURE if the EV loopback count
//	didn't increase between the two tested points.
//	Build the EV calibration table from the test points.
//	Burn the calibration table and slope and offset into FLASH.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ExhValveCalibration::calibrateExhValve_(void)
{
	CALL_TRACE("ExhValveCalibration::calibrateExhValve_(void)");

	const Real32 INITIAL_FLOW = 6.0F;

	FlowSensorTestResult flowSensorResult = checkFlowSensors_();

	if ( flowSensorResult != FLOW_SENSORS_OK )
	{
		if ( flowSensorResult == FLOW_NOT_ESTABLISHED )
		{
			// unable to establish flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_12);
		}
		else
		{
	
			// Insp/exh flow sensor readings are different
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_7);
		}
	}
	else
	{
		if ( testValvePressurization_() )
		{
			Boolean passed = doLowToHighCalibration_();

			if ( TRUE == passed )
			{
				//printTable_();
				ExhValveTable table( exhValveTable_ );
				table.printLowToHighValues();

				passed = doHighToLowCalibration_();
				
			}

			if ( TRUE == passed )
			{
				//printTable_( FALSE );

				ExhValveTable table( exhValveTable_ );
				table.printHighToLowValues();

				RETAILMSG( TRUE, (L"Updating the exhalation valve table\r\n") );

				// TODO E600 MS. Generate an error if writing to flash fails.
				// Don't want to write bogus cal data into the flash
				RExhalationValve.UpdateCalData( exhValveTable_ );

				// Give enough time for the data to get written
				Task::Delay( 2 );

				// exh valve cal passed, so set ev cal state to PASSED
				ServiceModeNovramData::SetExhValveCalStatus(ServiceMode::PASSED);

				// set the service mode EV cal valid flag TRUE
				ServiceMode::SetExhValveCalValid(TRUE);
				
			}
		}

	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doLowToHighCalibration_(void)
//
//@ Interface-Description
//  This method takes no arguments and returns a Boolean value
//  It is called to run the low to high calibration where target pressure
//  is gradually increased and the DAC counts of the exhalation valve at
//  the desired pressure are recorded in the low to high portion of the 
//  calibration table.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09113]
//	Exh valve count gets updated based on the desired pressure which starts
//  from 0 and goes all the way up to max pressure and values read from 
//  exhalation pressure sensor, exhalation drv pressure sensor and the DAC
//  points are recorded in the calibration table for each target pressure.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean 
ExhValveCalibration::doLowToHighCalibration_()
{
	if ( TRUE == findStableLowPressure_() )
	{
		return collectCalPointsL2H_();
	}

	return FALSE;
	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doHighToLowCalibration_(void)
//
//@ Interface-Description
//  This method takes no arguments and returns a Boolean value
//  It is called to run the high to low calibration where target pressure
//  is gradually decreased and the DAC counts of the exhalation valve at
//  the desired pressure are recorded in the low to high portion of the 
//  calibration table.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09113]
//	Exh valve count gets updated based on the desired pressure which starts
//  from max and goes all the way down to 0 pressure and values read from 
//  exhalation pressure sensor, exhalation drv pressure sensor and the DAC
//  points are recorded in the calibration table for each target pressure.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ExhValveCalibration::doHighToLowCalibration_()
{
	if ( TRUE == InitHighToLow_() )
	{
		if ( TRUE == findStableHighPressure_() )
		{
			return collectCalPointsH2L_();
		}
		
	}
	return FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: testValvePressurization_(void)
//
//@ Interface-Description
//  Pressurizes the exhalation valve at different pressure points and 
//  runs the pressure sensor cross check for each pressure point to verify
//  pressure stability
//---------------------------------------------------------------------
//@ Implementation-Description
// Exhalation valve DAC gets updated gradually until the desired pressure
// is reached and upon reaching the requested pressure, pressure sensor 
// cross check is run.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ExhValveCalibration::testValvePressurization_()
{
	const Uint32 MAX_DAC_VALUE = 4095;	// 12 bit dac
	const Uint32 PRESSURE_STABILIZATION_TIME = 5000; // in ms

	currentDacCount = 1200;
	currentSampleTableIdx = 0;
	Uint32 currentIdx = 0;
	Real32 currentAvePressure = 0;
	
	for ( currentIdx = 0; currentIdx < NUM_TEST_PRESSURES; ++currentIdx )
	{
		currentTargetPressure = testPressure_[currentIdx];

		currentAvePressure = RTestData.getAveExhPress();

		// Keep incrementing the DAC count until desired pressure
		// is reached
		while ( currentAvePressure < currentTargetPressure )
		{
			currentDacCount += EXH_VALVE_COUNT_INCREMENTS;

			if ( currentDacCount > MAX_DAC_VALUE )
			{
				// TODO MS declare unable to pressurize failure.
				return FALSE;
			}

			RExhalationValve.commandValve( currentDacCount );
			
			Task::Delay( 0, 10 );

			currentAvePressure = RTestData.getAveExhPress();
		}

		// Wait for pressure to stablize before running the cross check test
		Task::Delay ( 0, PRESSURE_STABILIZATION_TIME );

		// TODO E600 MS. For debugging only.
		printReadValues();


		if ( checkPressureSensors_() != PRESSURE_SENSORS_OK )
		{
			// Declare pressure sensor cross check failure
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_8);

			return FALSE;
		}
	}

	// Depressurize the exhalation valve
	currentDacCount = 1000;
	Task::Delay(1);

	while ( currentAvePressure >= 1.00 )
	{
		currentDacCount -= EXH_VALVE_COUNT_INCREMENTS;

		if ( currentDacCount <= 0 )
		{
			// TODO E600 MS Generate a pressure error
			return FALSE;
		}
		
		RExhalationValve.commandValve( currentDacCount );
		Task::Delay ( 0, 500 );

		currentAvePressure = RTestData.getAveExhPress();

		// TODO E600 MS. For debugging only.
		printReadValues();
	}

	prevExhPressure_ = currentAvePressure;

	currentTargetPressure = 0;
	return TRUE;
	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: findStableLowPressure_(void)
//
//@ Interface-Description
//  Finds the DAC count at which the low exh pressure ( close to 0 ) is
//  stable
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ExhValveCalibration::findStableLowPressure_()
{

	DEBUG_MSG( "findStableLowPressure_" ) ;

	Real32 currentExhPressAve = RTestData.getAveExhPress();


	while (  ( currentExhPressAve - prevExhPressure_ ) < -0.1f  && 
		( currentExhPressAve - prevExhPressure_ ) > 0.1f )
	{
		prevExhPressure_ = currentExhPressAve;
		currentExhPressAve = RTestData.getAveExhPress();

		currentDacCount -= EXH_VALVE_COUNT_INCREMENTS;

		if ( currentDacCount <= 0 )
		{
			// TODO E600 MS. Handle the situation
			DEBUG_MSG ( "Cound not find stable low pressure" );
			return FALSE;
		}

		RExhalationValve.commandValve( currentDacCount );

		Task::Delay( 0, 500 );

		currentExhPressAve = RTestData.getAveExhPress();
	}

	exhValveTable_.lowToHighData.exhPressDrvSamples[currentSampleTableIdx] = 
		RExhDrvPressureSensor.getFilteredValue();

	exhValveTable_.lowToHighData.exhPressSamples[currentSampleTableIdx] = 
		RExhPressureSensor.getFilteredValue();

	exhValveTable_.lowToHighData.dacSamples[currentSampleTableIdx++] = currentDacCount;

	return TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: collectCalPointsL2H_(void)
//
//@ Interface-Description
//  Finds the DAC count for each desired pressure and records it in the
//  calibration table
//---------------------------------------------------------------------
//@ Implementation-Description
//  DAC counts get incremented until target pressure is reached.
//  The cycle continues until the data for the last target pressure is 
//  recorded or an error is found.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ExhValveCalibration::collectCalPointsL2H_()
{
	DEBUG_MSG( "collectCalPointsL2H_" );

	// Set the initial target pressure to cal step ( currently set to 1 )
	currentTargetPressure = EXH_CAL_STEP;

	OsTimeStamp timeStamp;
	Boolean timeoutOccured = FALSE;

	while ( TRUE )
	{
		Real32 currentExhPressAve = RTestData.getAveExhPress();
		Real32 currentExhDrvPressAve = RTestData.getAveExhDrvPress();

		if ( currentExhPressAve >= currentTargetPressure )
		{
			timeoutOccured = checkTimeout_ ( currentTargetPressure, timeStamp.getPassedTime() );

			// TODO E600 MS. For debugging only.
			printReadValues( TRUE, timeStamp.getPassedTime() );

			exhValveTable_.lowToHighData.exhPressDrvSamples[ currentSampleTableIdx ] = 
				currentExhDrvPressAve;

			exhValveTable_.lowToHighData.exhPressSamples[ currentSampleTableIdx ] = 
				currentExhPressAve;

			exhValveTable_.lowToHighData.dacSamples[ currentSampleTableIdx ] = 
				currentDacCount;

			if ( ++currentSampleTableIdx >= EXH_VALVE_SUBTABLE_SIZE  )
			{

				break;
			}
			else
			{
				currentTargetPressure += EXH_CAL_STEP;
			}

			timeStamp.now();
		}
		if( ++currentDacCount > MAX_DAC_VALUE ||
					TRUE == timeoutOccured )
		{
			// TODO E600 MS. Send appropriate error to the GUI
			DEBUG_MSG( "Unable to reach desired pressure\r\n" );
			return FALSE;
		}

		// TODO E600, check if timeout occured. If so generate a failure and break
		RExhalationValve.commandValve( currentDacCount );
		Task::Delay( 0, 100 );

	}

	DEBUG_MSG( " Done with collectCalPointsL2H_" );

	// TODO E600 MS At this point we may want to save the low to high calibration data.
	return TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitHighToLow_(void)
//
//@ Interface-Description
//  Prepares for the second part of the calibration where pressure is
//  gradually decreased from max to min. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ExhValveCalibration::InitHighToLow_()
{
	currentTargetPressure = ( ExhValveTable::EXH_VALVE_DATA_ARRAY_SIZE * EXH_CAL_STEP ) - EXH_CAL_STEP;
	currentSampleTableIdx = ExhValveTable::EXH_VALVE_DATA_ARRAY_SIZE - 1;

	RExhalationValve.commandValve( currentDacCount );

	Task::Delay( 1, 0 );


	Real32 averageExhPress = RTestData.getAveExhPress();
	prevExhPressure_ = RTestData.getAveExhPress();

	while ( averageExhPress >= currentTargetPressure)
	{

		prevExhPressure_ = averageExhPress;
		currentDacCount -= EXH_VALVE_COUNT_INCREMENTS;

		if ( currentDacCount == 0 )
		{
			// TODO E600 MS. Send an error to the GUI
			DEBUG_MSG( "High to initialization failed" );
			return FALSE;
		}

		RExhalationValve.commandValve( currentDacCount );
		Task::Delay( 0, 750);

		averageExhPress = RTestData.getAveExhPress();
	}

	DEBUG_MSG ( "Done doZeroDownH2L_" ) ;

	return TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: findStableHighPressure_(void)
//
//@ Interface-Description
//  Attemps to find a stable point at current high pressure.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ExhValveCalibration::findStableHighPressure_()
{
	DEBUG_MSG( "Starting findStableHighPressure_" );
	
	Real32 averageExhPress = RTestData.getAveExhPress();

	// Keep decrementing the DAC count until pressure gets close
	// enough to the target pressure.
	// TODO E600 MS. Wouldn't it be better to check for the
	// difference betwen the target and read pressures? Discuss this with controls
	while ( averageExhPress >= currentTargetPressure)
	{

		prevExhPressure_ = averageExhPress;
		currentDacCount -= EXH_VALVE_COUNT_INCREMENTS;

		if ( currentDacCount == 0 )
		{
			// TODO E600 MS. Send an error to the GUI
			DEBUGMSG( "Counld not find stable high pressure" );
			return FALSE;
		}

		RExhalationValve.commandValve( currentDacCount );
		Task::Delay( 0, 750);

		averageExhPress = RTestData.getAveExhPress();
	}

	// This is to make sure that the pressure is stable at the target
	while ( ABS_VALUE ( averageExhPress - prevExhPressure_ >= 0.1f ) )
	{
		prevExhPressure_ = averageExhPress;

		if ( currentDacCount == 0 )
		{
			// TODO E600 MS handle the situation
			DEBUGMSG( "Failure! Reached count of zero" ) ;
			return FALSE;
		}

		averageExhPress = RTestData.getAveExhPress();

		currentDacCount -= EXH_VALVE_COUNT_INCREMENTS;
		RExhalationValve.commandValve( currentDacCount );
		Task::Delay( 0, 500);
	}

	exhValveTable_.highToLowData.exhPressSamples[ currentSampleTableIdx ] = RTestData.getAveExhPress();
	exhValveTable_.highToLowData.exhPressDrvSamples[ currentSampleTableIdx ] = RTestData.getAveExhDrvPress();
	exhValveTable_.highToLowData.dacSamples[ currentSampleTableIdx ] = currentDacCount;

	currentSampleTableIdx--;
	currentTargetPressure -= EXH_VALVE_COUNT_INCREMENTS;

	DEBUG_MSG( "Done findStableHighPressure_"  );
	return TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: collectCalPointsH2L_(void)
//
//@ Interface-Description
//  Finds the DAC count for each desired pressure and records it in the
//  calibration table
//---------------------------------------------------------------------
//@ Implementation-Description
//  DAC count gets decremented until target pressure is reached.
//  The cycle continues until the data for the last target pressure is 
//  recorded or an error is found.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean ExhValveCalibration::collectCalPointsH2L_()
{

	DEBUG_MSG( "Starting collectCalPointsH2L_" );

	while ( currentSampleTableIdx >= 0 )
	{
		Real32 averageExhPress = RTestData.getAveExhPress();
		Real32 averageExhDrvPress = RTestData.getAveExhDrvPress();

		// Record the info in the table if target pressure's been rechead or exh valve
		// DAC value is 0
		if (  averageExhPress <= currentTargetPressure  || 
			currentDacCount == 0)
		{
			exhValveTable_.highToLowData.exhPressSamples[ currentSampleTableIdx ] = RTestData.getAveExhPress();
			exhValveTable_.highToLowData.exhPressDrvSamples[ currentSampleTableIdx ] = RTestData.getAveExhDrvPress();
			exhValveTable_.highToLowData.dacSamples[ currentSampleTableIdx ] = currentDacCount;

			// TODO E600 MS. For debugging only.
			printReadValues();

			--currentSampleTableIdx;

			if ( currentTargetPressure >= EXH_CAL_STEP )
			{
				currentTargetPressure -= EXH_CAL_STEP;
			}
		}
		
		else if ( currentDacCount > 0 )
		{
			currentDacCount -= EXH_VALVE_COUNT_INCREMENTS;
			RExhalationValve.commandValve( currentDacCount );
			Task::Delay ( 0, 100 );
		}
	}

	DEBUG_MSG( "Done collectCalPointsH2L_" );
	return TRUE;
}

// TODO E600 MS. This method is for debugging only. Needs to be remove when no longer needed
void ExhValveCalibration::printReadValues( Boolean printElapsedTime , Uint32 elapsedTime )
{
	std::stringstream ss;
	ss << std::setprecision(2);
	ss << std::fixed;
	ss << "Pd= " << RTestData.getAveExhDrvPress() << 
		" P2= " << RTestData.getAveExhPress() << " DAC = " << currentDacCount;
		

	if ( TRUE == printElapsedTime )
		ss << " Time = " << elapsedTime;

	DEBUG_MSG(ss.str().c_str());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkFlowSensors_
//
//@ Interface-Description
//	This method tests the flow sensors at the flow level that is to
//	used during the EV calibration.  It takes no arguments, returns
//	a result ID indicating status of the test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set default result ID of "flow cross-check passed".
//	Flush the system with air to ensure a accurate expiratory air
//	flow readings.
//	Establish an inspiratory-controlled air flow at the same flow
//	used for the calibration.  If flow doesn't stabilize within
//	the timeout, set result ID = "flow not stabilized" and skip
//	the flow cross-check phase.
//	If flow stability was reached, delay then read alpha-filtered
//	insp and exp air flows.   If flows are out of range, set
//	result ID "flow cross-check failed".
//	Stop flow and return the result ID.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ExhValveCalibration::FlowSensorTestResult
ExhValveCalibration::checkFlowSensors_ (void)
{
	CALL_TRACE("ExhValveCalibration::checkFlowSensors_ ( void)") ;

	// use same test flow as flow required for exh valve cal
	const Real32 TEST_FLOW  = 6.0 ;  // lpm
	const Uint32 STABLE_FLOW_WAIT_TIME = 5000; // milliseconds

	// TODO E600 MS. This may have to change later based on new numbers
	// from controls
	const Real32 EXH_INSP_FLOW_ERROR_LIMIT = 1.8;  // lpm

	// flush system with air to ensure good exp flow reading
	RSmUtilities.flushTubing(AIR);

	RSmFlowController.establishFlow( TEST_FLOW, DELIVERY, AIR);

	if (!RSmFlowController.waitForStableFlow( STABLE_FLOW_WAIT_TIME ))
	{
		DEBUGMSG( "Flow Sensor failed to establish flow" );
		return FLOW_NOT_ESTABLISHED;
	}

	// $[TI2]
	// delay for  alpha-filtering
	Task::Delay (0, 500);
	
	// read filtered flow values
	Real32 inspFlow = RAirFlowSensor.getFilteredValue();
	Real32 exhFlow = RExhFlowSensor.getFilteredValue();

	if ( ABS_VALUE ( inspFlow - exhFlow ) > EXH_INSP_FLOW_ERROR_LIMIT )
	{
		DEBUG_MSG( "Flow Sensor cross check failed" );
		return FS_CROSS_CHECK_FAILURE;
	}

	DEBUG_MSG( "Flow Sensor OK" );
	return FLOW_SENSORS_OK;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkPressureSensors_
//
//@ Interface-Description
//	This method tests the pressure sensors at various pressure levels.
//	It takes no arguments, returns a result ID indicating status of
//	the test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Calculate the worst-case pressurizing timeout.
//	Set default result ID = "pressure cross-check passed".
//	Close the exhalation valve.
//	For each test pressure:
//		Pressurize the system to the next test pressure.  If timeout
//		occurs, set result ID = "pressure not established" and exit loop.
//		Once the test pressure is reached, delay to let pressure waves
//		decay.
//		Read the insp and exp pressure sensors, and if delta P exceeds
//		the FAILURE limit, set result ID  = "pressure cross-check failed"
//		and exit loop.  If delta P exceeds the ALERT limit,
//		set result ID = "pressure cross-check alert" and continue.
//	Turn off the pressure controller, open the exhalation valve, and
//	return the result ID.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ExhValveCalibration::PressureSensorTestResult
ExhValveCalibration::checkPressureSensors_ (void)
{
	CALL_TRACE("ExhValveCalibration::checkPressureSensors_ ( void)") ;

	const Real32 REL_PRESSURE_ERROR = 0.031;
	const Real32 ABS_PRESSURE_ERROR = 0.35;

	Real32 inspPres = RInspPressureSensor.getFilteredValue();
	Real32 exhPres =  RExhPressureSensor.getFilteredValue();

	if ( ABS_VALUE(inspPres - exhPres) >
			(inspPres + exhPres) * REL_PRESSURE_ERROR +
			(2 * ABS_PRESSURE_ERROR) )
	{
		DEBUG_MSG ( "Pressure sensor cross check failure\r\n" );
		return PS_CROSS_CHECK_FAILURE;
	}
	else if ( ABS_VALUE(inspPres - exhPres) >
			(inspPres + exhPres) / 2.0 * REL_PRESSURE_ERROR +
			ABS_PRESSURE_ERROR )
	{
		DEBUG_MSG ( "Pressure sensor cross check alert\r\n" );
		return PS_CROSS_CHECK_ALERT;
	}
	DEBUG_MSG ( "Pressure sensor cross check passed\r\n" );

	return PRESSURE_SENSORS_OK;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkTimeout_
//
//@ Interface-Description
//	This method is used to check if a timeout occurred while attemping
//	to reach the next calibration pressure.  Two arguments: the current
//	calibration pressure and the current time.  Returns TRUE if a timeout
//	occurred, FALSE if not.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If this is the minimum calibration pressure:
//		If time equals or exceeds the timeout for the min cal pressure,
//		then set timeout-occurred flag TRUE, else set it FALSE.
//	If this a pressure other than the minimum calibration pressure:
//		If time equals or exceeds the timeout for non-min cal pressures,
//		then set timeout-occurred flag TRUE, else set it FALSE.
// 	Return the timeout-occurred flag.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
ExhValveCalibration::checkTimeout_( const Uint32 tgtPressure,
									const Int32 timer)
{
	CALL_TRACE("ExhValveCalibration::checkTimeout_( \
									const Uint32 tgtPressure, \
									const Int32 timer )") ;

	Boolean timeoutOccurred;

	if (tgtPressure == MIN_CALIB_PRESSURE)
	{
	// $[TI1]
		// check the timeout for the 1st calibration pressure
		if (timer >= INITIAL_PRESSURE_TIMEOUT)
		{
		// $[TI1.1]
			timeoutOccurred = TRUE;
		}
		else
		{
		// $[TI1.2]
			timeoutOccurred = FALSE;
		}		
		// end else
	}
	else
	{
	// $[TI2]
		// check the timeout for all other calibration pressures
		if (timer >= NORMAL_PRESSURE_TIMEOUT)
		{
		// $[TI2.1]
			timeoutOccurred = TRUE;
		}
		else
		{
		// $[TI2.2]
			timeoutOccurred = FALSE;
		}		
		// end else
	}		
	// end else

	return( timeoutOccurred);
}
