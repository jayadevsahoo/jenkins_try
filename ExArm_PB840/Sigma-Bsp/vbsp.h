/************************************************************************/
/* THIS INFORMATION IS PROPRIETARY TO                                   */
/* MICROTEC - A Mentor Graphics Company                                 */
/*----------------------------------------------------------------------*/
/* Copyright (c) 1993-1997 Microtec - A Mentor Graphics Company         */
/* All rights reserved                                                  */
/************************************************************************/
/*
**The software source code contained in this file and in the related
**header files is the property of Microtec. and may only be
**used under the terms and conditions defined in the Microtec
**license agreement. You may modify this code as needed but you must 
**retain the Microtec copyright notices, including this statement.
**Some restrictions may apply when using this software with non-Microtec
**software.  In all cases, Microtec reserves all rights.
*/
/************************************************************************/

/*
 *
 *	Module Name:		%M%
 *
 * 	Identification:		%Z% %I% %M%
 *
 *	Date:			%G%  %U%
 *
 *
 */

/*
 *
 * File:
 *
 *    vbsp.h 
 *
 * Author: 
 * 
 *    Eugene Walden
 *
 * Date: 
 * 
 *    3-25-92
 *
 * Contents:
 *
 *    This file contains all of the definitions used by VBSP 
 *    functions that are common across all devices (e.g. register 
 *    addresses, vector numbers, etc). The definitions in this file 
 *    depend neither on the board nor the device. They may be CPU-
 *    specific, in that I make the assumption that I/O devices are 
 *    accessible through memory reads and writes, as opposed to 
 *    special I/O CPU instructions.
 *
 * Revision History:
 *
 *    Name |   Date   | Description of modification
 *    -----+----------+--------------------------------------------
 *    EMW  | 03-25-92 | First version                              
 *    MWM  | 07-25-92 | Added multiple inclusion protection and return vals
 *    DAR  | 08-07-92 | Added invalid period definition
 *    SAM  | 10-22-92 | Added RX_OVERRUN and PRIME_PUMP definitions
 *
 */

#ifndef _VBSP_H_
#define _VBSP_H_

typedef long vbsp_return_t;     /* status info for all VBSP calls */

typedef long vbsp_dev_addr_t;   /* address for device registers   */

typedef void *vbsp_addr_t;
 
typedef int vbsp_size_t;

typedef long vbsp_vector_t;     /* interrupt vector number        */

typedef void (*vbsp_handler_t)();/* pointer to an interrupt hndlr */

typedef int vbsp_net_flavor_t;

typedef int vbsp_interrupt_t;	/* for saving interrupt state     */

/* 
 * VBSP needs the ability to enable and disable interrupts. Define 
 * the functions to point to the logio interrupt enable/disable 
 * functions. 
 */

/********* THESE MACROS SHOULD BE DELETED EVENTUALLY AND REPLACED BY DIRECT CALLS *****/
//#include <cpu.h>
#define vbsp_interrupt_enable(s) cpu_interrupt_restore(s)
#define vbsp_interrupt_disable() cpu_interrupt_disable()
/*********** END HACK *****************/

/* 
 * define supported flavors for requesting info about 
 * network device
 */

#define VBSP_NETINFO_ETHADDR 	0
#define VBSP_NETINFO_CHIPSTAT 	1

typedef char vbsp_netinfo_t;

/* VBSP return codes */

#define VBSP_SUCCESS			0x00
#define VBSP_FAILURE			0x01
#define VBSP_INVALID_DEV_ID		0x02
#define VBSP_INVALID_VECTOR		0x03
#define VBSP_NO_CHAR_PRESENT		0x04
#define VBSP_TX_BUFF_FULL		0x05
#define VBSP_INVALID_FLAVOR		0x06 
#define VBSP_NO_PACKET 			0x07
#define VBSP_INSUFFICIENT_MBUFS		0x08
#define VBSP_NO_DEVICE_BUFS		0x09
#define VBSP_INVALID_PRIORITY		0x0a
#define VBSP_ILLEGAL_CONFIG		0x0b
#define VBSP_INVALID_COMMAND		0x0c
#define VBSP_INVALID_ARBVAL		0x0d
#define VBSP_INVALID_PERIOD		0x0e
#define VBSP_INVALID_BASE		0x0f
#define VBSP_RX_OVERRUN			0x10
#define VBSP_SERIAL_PRIME_PUMP_NEEDED	0x11

struct vbsp_buf_info {
   vbsp_addr_t 		baseaddr;
   vbsp_size_t		bufsize;
   int			nbufs;
   };

typedef struct vbsp_buf_info vbsp_buf_info_t;


#endif	/* !VBSP_H */
