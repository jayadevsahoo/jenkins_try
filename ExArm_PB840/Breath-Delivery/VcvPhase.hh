#ifndef VcvPhase_HH
#define VcvPhase_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VcvPhase - Implements volume control ventilation inspiration.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VcvPhase.hhv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012  By: gfu     Date:  19-Mar-2002    DR Number: 5996
//  Project:  VTPC
//  Description:
//      Make getComplLimitFactorValue a public method.
//
//  Revision: 011  By: syw     Date:  07-Feb-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  eliminate compliance factor tables
//
//  Revision: 010  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 009  By:  syw    Date:  07-Jul-1998    DR Number: DCS 5158
//       Project:  Sigma (840)
//       Description:
//		 	Interface with insp pause maneuver.
//
//  Revision: 008  By: syw   Date:  15-Aug-1997    DR Number: 2388
//  	Project:  Sigma (840)
//		Description:
//			Remove 4 breath running average.
//
//  Revision: 007  By: syw   Date:  08-Jul-1997    DR Number: 2284
//  	Project:  Sigma (840)
//		Description:
//			Created EipBuffer_[] and EipIndex_ to be used in the average of
//			the last 4 LastValidEipForCc_.
//
//  Revision: 006 By: syw   Date: 07-Nov-1996   DR Number: DCS 1502 
//  	Project:  Sigma (R8027)
//		Description:
//			Added lastErrorSumUpdated_ flag to handle the updating of the
//			last error sum update instead of using plateau time.  The last
//			error sum update was not being called if plateau != 0 and the
//			VcvPhase was terminated early (due to disconnect).
//
//  Revision: 005 By: syw   Date: 18-Sep-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Reset volume controller if there is a mix change in the middle
//			of inspiration or if the mix change is greater than MIX_THRESHOLD_FOR_RESET.
//
//  Revision: 004 By: sp    Date: 6-May-1996   DR Number: DCS 600
//      Project:  Sigma (R8027)
//      Description:
//          Add unit test method setTotalVolumeMl.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes:
//			- use explicit zero to close psol instead of closeLevel.
//			- eliminated setActiveControllers().
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "BreathPhase.hh"
#include "BdDiscreteValues.hh"
#include "SettingId.hh"
#include "O2Mixture.hh"

//@ End-Usage

class VcvPhase : public BreathPhase
{
  public:
    VcvPhase( const SettingId::SettingIdType tidalVolumeId,
    		  const SettingId::SettingIdType minInspFlowId,
    		  const SettingId::SettingIdType plateauTimeId,
    		  const SettingId::SettingIdType flowPatternId,
    		  const SettingId::SettingIdType inspTimeId) ;
    virtual ~VcvPhase(void) ;

    static void SoftFault(const SoftFaultID softFaultID,
  const Uint32      lineNumber,
  const char*       pFileName  = NULL, 
  const char*       pPredicate = NULL) ;
 
    virtual void relinquishControl( const BreathTrigger& trigger) ;
    virtual void newCycle( void) ;
    virtual void newBreath(void) ;

    static inline Boolean IsPlateauActive(void);

    static Real32  getComplLimitFactorValue(void) ;

	// TODO E600_LL: temp setter for circuit compensation factor
	void setCktCompFactor(const Real32 factor)
	{ 
		tempCktCompFactor_ = factor;
		tempCktCompFactorSet_ = true;
	}

  protected:

  private:
    VcvPhase( void) ;         			// not implemented
    VcvPhase(const VcvPhase&) ;         // not implemented
    void operator=(const VcvPhase&) ;   // not implemented

    Boolean	checkResetConditions_( void) ;			
    void updateComplianceVolume_( void) ;
    Real32 calculateDesiredFlow_(void) ;
  


    //@ Data-Member: FlowPattern_
    // flow pattern for this inspiration
    static DiscreteValue FlowPattern_ ;

    //@ Data-Member: InspDeliveryTimeMs_
    // the total time to deliver flow (excludes plateau time)
    static Real32 InspDeliveryTimeMs_ ;

	//@ Data-Member: LastValidEipForCc_
    // currently measure eip for cc purposes
    static Real32 LastValidEipForCc_ ;

    //@ Data-Member: PrevAirCcPeakFlow_
    // previous value of air cCPeakFlow_
    static Real32 PrevAirCcPeakFlow_ ;

    //@ Data-Member: PrevO2CcPeakFlow_
    // previous value of o2 cCPeakFlow_
    static Real32 PrevO2CcPeakFlow_ ;
    
	//@ Data-Member: HcpOrHvpOccured_
	// set if HCP or HVP occured
	static Boolean HcpOrHvpOccured_ ;

	//@ Data-Member: Boolean MixChangeOccurred_
	// set if the o2 mix changes during a single breath
	static Boolean MixChangeOccurred_ ;

	//@ Data-Member: Real32 BeginningMix_
	// initial mix setting at the beginning of the breath
	static Real32 BeginningMix_ ;

	//@ Data-Member: Boolean IsPlateauActive_
	// set if the plateau is active in Vcv Phase
	static Boolean IsPlateauActive_;

    //@ Data-Member: qMin_
	// minimum flow for ramp waveshape
    Real32 	qMin_ ;

    //@ Data-Member: intervalNumber_
    // number of intervals that have elapsed so far
    Int32 	intervalNumber_ ;

    //@ Data-Member: numberVcvCycles_
    // total number of cycles for Volume control phase (excluding plateau)
    Int32 numberVcvCycles_ ;

    //@ Data-Member: inspCompVolumeMl_
    // compliance volume for inspiratory phase
    Real32 inspCompVolumeMl_ ;

    //@ Data-Member: tidalVolumeMl_
    // storage for the set tidal volume (either apnea or normal)
    Real32 tidalVolumeMl_ ;

    //@ Data-Member: plateauTimeMs_
    // stoarge for the set plateau duration (plateau for apnea is an internal BD
    // setting)
    Real32 plateauTimeMs_ ;

    //@ Data-Member: totalVolumeMl_
    // the sum of the tidal volume and the compliance volume
    Real32 totalVolumeMl_ ;

    //@ Data-Member: inspTimeMs_
    // the total inspiratory time which includes plateau time
    Real32 inspTimeMs_ ;

    //@ Data-Member: cCPeakFlow_
    // compliance compensated peak desired flow
    Real32 cCPeakFlow_ ;

    //@ Data-Member:  controlTimerMs_
    // synch control cycle to volume control cycle
    Int32  controlTimerMs_ ;

    //@ Data-Member: flowCommandLimit_
    // maximum flow command based on patient type
    Real32 flowCommandLimit_ ;

	//@ Data-Member: tidalVolumeId_
	// apnea or normal tidalVolumeId
	SettingId::SettingIdType tidalVolumeId_ ;

	//@ Data-Member: minInspFlowId_
	// apnea or normal minInspFlowId
	SettingId::SettingIdType minInspFlowId_ ;

	//@ Data-Member: plateauTimeId_
	// apnea or normal plateauTimeId
	SettingId::SettingIdType plateauTimeId_ ;

	//@ Data-Member: flowPatternId_
	// apnea or normal flowPatternId
	SettingId::SettingIdType flowPatternId_ ;

	//@ Data-Member: inspTimeId_
	// apnea or normal inspTimeId
	SettingId::SettingIdType inspTimeId_ ;

	//@ Data-Member: highCircuitPressure_
	// high circuit pressure setting
	Real32 highCircuitPressure_ ;

	//@ Data-Member: lastErrorSumUpdated_
	// set true if last error sum has been updated
	Boolean lastErrorSumUpdated_ ;

	//TODO E600_LL: temporary circuit comp factor
	Real32 tempCktCompFactor_;
	Boolean tempCktCompFactorSet_;
} ;

// Inlined methods
#include "VcvPhase.in"

#endif // VcvPhase_HH
