#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FsCrossCheckTest - Test the flow sensors and PSOLs
//---------------------------------------------------------------------
//@ Interface-Description
//  The ventilator has 3 flow sensors: air, O2, and exhalation.
//  This test compares the air flow sensor against the exhalation
//  flow sensor using the air table and the O2 flow sensor against the
//	exhalation flow sensor using the O2 table.  In addition,
//	the PSOL command current for each test flow level is tested and
//	PSOL liftoff calibrations are performed.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the flow sensor and PSOL test.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Two methods are provided for flow sensor cross-checking:
//	estTest() -- EST version of test, requires both air and O2.
//	sstTest() -- SST version of test, can test single gas supply if one
//	gas is not available.
//  A closed loop controller is used to control flow during testing.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/FsCrossCheckTest.ccv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011  By: quf    Date: 26-Dec-2000     DR Number: 5832
//  Project:  Baseline
//  Description:
//	- Extracted code from findLiftoff_() to create method
//	  buildPsolTables_() which performs a single iteration of the
//	  PSOL liftoff calibration for both gases.  Removed bug which
//	  allowed a partial (i.e. corrupt) calibration table build.
//	- findLiftoff_() now performs 3 PSOL liftoff calibrations per gas
//	  and averages the results.
//
//  Revision: 010  By: syw    Date: 13-Mar-2000     DR Number: 5684
//  Project:  NeoMode
//  Description:
//		Build PSOL lookup tables for flows < MAX_PSOL_LOOKUP_FLOW or
//		LIFTOFF_FLOW concurrently with the liftoff test.  Psol counts are
//		stored every FLOW_INCREMENT lpm.
//
//  Revision: 009  By: sah    Date: 30-Jun-1999     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Incorporation of NeoMode Project requirements.
//
//  Revision: 008  By: quf    Date: 13-Oct-1999     DR Number: 5540
//  Project:  ATC
//  Description:
//		PSOL current check modified to account for gas type and Patm.
//
//  Revision: 007  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//	Revision: 006  By:  quf    Date: 17-Nov-1997    DR Number: DCS 2513
//      Project:  Sigma (840)
//      Description:
//			Removed lowFlowTest() and highFlowTest().
//
//	Revision: 005  By:  quf    Date:  10-Oct-1997    DR Number: DCS 2453, 2455
//      Project:  Sigma (840)
//      Description:
//			Code and comment cleanup from code peer review.
//
//	Revision: 004  By:  quf    Date:  08-Oct-1997    DR Number: DCS 2326, 2443
//      Project:  Sigma (840)
//      Description:
//			Removed CONNECT_INSP_FILTER_PROMPT and added CONNECT_HUMIDIFIER_PROMPT
//			and BLOCK_WYE_PROMPT to sstTest().
//
//	Revision: 003  By:  syw    Date:  15-Aug-1997    DR Number: DCS 2386
//      Project:  Sigma (R8027)
//      Description:
//			Use getUnoffsettedValue() instead of getValue() method.  Use
//			getUnoffsettedFilteredValue() instead of getFilteredValue()
//			method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//			  Added comments to testFlowAndPsol_() for easier comparison
//            to 840 Self-Tests Spec.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "FsCrossCheckTest.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmFlowController.hh"
#include "SmManager.hh"
#include "SmGasSupply.hh"
#include "SmUtilities.hh"
#include "SmConstants.hh"

#include "Task.hh"

#include "ValveRefs.hh"
#include "MainSensorRefs.hh"
#include "VentObjectRefs.hh"
#include "ExhFlowSensor.h"
#include "Psol.hh"
#include "PsolLiftoff.hh"
#include "Compressor.hh"

#include "NovRamManager.hh"
#include "PhasedInContextHandle.hh"
#include "PatientCctTypeValue.hh"
#include "MathUtilities.hh"

#if defined(SIGMA_DEVELOPMENT)
#include <stdio.h>
#endif  // SIGMA_DEVELOPMENT

#ifdef SIGMA_DEBUG
#include "OsTimeStamp.hh"
#endif  // SIGMA_DEBUG

#ifdef SIGMA_UNIT_TEST
#include "FakeIo.hh"
#include "FakeControllers.hh"
#include "FsCrossCheckTest_UT.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FsCrossCheckTest()  [Default Constructor]
//
//@ Interface-Description
//  Constructor.  No arguments.  Sets up test parameter arrays.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize parameters for each flow test point -- flow level,
//	PSOL nominal command, PSOL command max deviation.
//	Test point #0 has highest target flow, and target flow decreases
//	with test point #.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

FsCrossCheckTest::FsCrossCheckTest(void)
{
// $[TI1]
	CALL_TRACE("FsCrossCheckTest()");

	flowTestPoint_ [TEST_120_LPM] = 120.0;
	commandTestPoint_ [TEST_120_LPM] = 431.0;
	commandTestLimit_ [TEST_120_LPM] = 77.41;

	flowTestPoint_ [TEST_60_LPM] = 60.0;
	commandTestPoint_ [TEST_60_LPM] = 308.5;
	commandTestLimit_ [TEST_60_LPM] = 56.78;

	flowTestPoint_ [TEST_20_LPM] = 20.0;
	commandTestPoint_ [TEST_20_LPM] = 213.25;
	commandTestLimit_ [TEST_20_LPM] = 38.53;

	flowTestPoint_ [TEST_5_LPM] = 5.0;
	commandTestPoint_ [TEST_5_LPM] = 165.5;
	commandTestLimit_ [TEST_5_LPM] = 37.16;

	flowTestPoint_ [TEST_1_LPM] = 1.0;
	commandTestPoint_ [TEST_1_LPM] = 145.0;
	commandTestLimit_ [TEST_1_LPM] = 35.78;

	flowTestPoint_ [TEST_0_LPM] = 0.0;
	commandTestPoint_ [TEST_0_LPM] = 0.0; // dummy value
	commandTestLimit_ [TEST_0_LPM] = 0.0; // dummy value
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FsCrossCheckTest()  [Destructor]
//
//@ Interface-Description
//  Takes no arguments, and, returns no value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

FsCrossCheckTest::~FsCrossCheckTest(void)
{
	CALL_TRACE("~FsCrossCheckTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: estTest
//
//@ Interface-Description
//	This method performs the EST flow sensor cross-check.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09037]  $[09028]  $[09169]
//	Perform O2 flow and PSOL checks, generate error conditions as appropriate.
//	Perform air flow and PSOL checks, generate error conditions as appropriate.
//  If no FAILUREs during air flow and PSOL checks, measure and store the air
//	PSOL liftoff value.
//  If no FAILUREs during O2 flow and PSOL checks, measure and store the O2
//	PSOL liftoff value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FsCrossCheckTest::estTest()
{
// $[TI1]
	CALL_TRACE("FsCrossCheckTest::estTest()");

#ifdef SIGMA_DEBUG
	PsolLiftoff psolLiftoff;
	NovRamManager::GetPsolLiftoffCurrents( psolLiftoff);

	printf("Set stored psol liftoffs to 813 counts? ");
	char myAnswer[80];
	scanf("%s", myAnswer);
	if (myAnswer[0] == 'y' || myAnswer[0] == 'Y')
	{
		RAirPsol.setLiftoff( 813);
		RO2Psol.setLiftoff( 813);
		psolLiftoff.setAirPsolLiftoff( 813);
		psolLiftoff.setO2PsolLiftoff( 813);
		NovRamManager::UpdatePsolLiftoffCurrents( psolLiftoff);
	}

	printf("RAirPsol.getLiftoff() = %d\n", RAirPsol.getLiftoff());
	printf("RO2Psol.getLiftoff() = %d\n", RO2Psol.getLiftoff());
	NovRamManager::GetPsolLiftoffCurrents( psolLiftoff);
	printf("air psol liftoff in novram = %d\n", psolLiftoff.getAirPsolLiftoff());
	printf("O2 psol liftoff in novram = %d\n", psolLiftoff.getO2PsolLiftoff());
#endif  // SIGMA_DEBUG

	// Perform O2 flow cross-checks and psol current checks
	Boolean o2Passed = testFlowAndPsol_( O2, TEST_120_LPM, TEST_0_LPM);

	// Set the test point index in case some O2 data points were skipped
	RSmManager.advanceTestPoint(
				SmDataId::FS_CROSS_CHECK_AIR_120_INSPIRATORY_FLOW );

	// Perform air flow cross-checks and psol current checks
	Boolean airPassed = testFlowAndPsol_( AIR, TEST_120_LPM, TEST_0_LPM);

	// TODO E600_LL: Debug output to know if Liftoff can be calculated for air, o2, or both.  To be removed later
	DEBUGMSG(1, (L"FsCrossCheckTest::estTest(): o2Passed=%d, airPassed=%d\r\n", o2Passed, airPassed));
	// Perform psol liftoff calibration for the gas(es) that passed the
	// flow and psol current tests
	findLiftoff_( airPassed, o2Passed);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sstTest
//
//@ Interface-Description
//	This method performs the SST flow sensor cross-check.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[10010]
//	Check for availability of at least one gas, generate FAILURE if no
//	gas is connected and exit.
//	If one gas is missing, generate an ALERT for that gas.
//	Prompt to connect insp filter.
//	Prompt to connect circuit and block wye.
//	If O2 is connected, perform O2 flow and PSOL checks, generate error
//	conditions as appropriate.
//	If air is connected, Perform air flow and PSOL checks, generate
//	error conditions as appropriate.
//  If air is connected and no FAILUREs during air flow and PSOL checks,
//	measure and store the air PSOL liftoff value.
//  If O2 is connected and no FAILUREs during O2 flow and PSOL checks,
//	measure and store the O2 PSOL liftoff value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FsCrossCheckTest::sstTest( void)
{
	CALL_TRACE("FsCrossCheckTest::sstTest( void)");

	if (!RSmGasSupply.isAnyGasPresent())
	{
	// $[TI1]
		// no gas connected
		RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_5);
	}
	else
	{	
	// $[TI2]
		Boolean o2Present = TRUE;
		Boolean airPresent = TRUE;

		if (!RSmGasSupply.isO2Present())
		{
		// $[TI2.1]
			RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT,
					SmStatusId::CONDITION_12);

			o2Present = FALSE;
		}
		else if (!RSmGasSupply.isAnyAirPresent())
		{
		// $[TI2.2]
			RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT,
					SmStatusId::CONDITION_13);

			airPresent = FALSE;
		}
		// $[TI2.3]
		// implied else: both air and O2 are connected
		
		if ( !RSmUtilities.messagePrompt(
						SmPromptId::ATTACH_CIRCUIT_PROMPT) )
		{
		// $[TI2.5]
			RSmManager.sendOperatorExitToServiceData();
		}
		else if ( !RSmUtilities.messagePrompt(
						SmPromptId::BLOCK_WYE_PROMPT) )
		{
		// $[TI2.4]
			RSmManager.sendOperatorExitToServiceData();
		}
		else
		
		{
		// $[TI2.6]
			// get current circuit type from Settings-Validation
			const DiscreteValue  CIRCUIT_TYPE =
				PhasedInContextHandle::GetDiscreteValue(
											 SettingId::PATIENT_CCT_TYPE
													   );

			TestFlowId  flowStartId = MAX_FS_TEST_POINTS; 	
			SmDataId::ItemizedTestDataId airTestPoint = SmDataId::NULL_ITEMIZED_TEST_DATA_ITEM ;
			SmDataId::ItemizedTestDataId o2TestPoint = SmDataId::NULL_ITEMIZED_TEST_DATA_ITEM ;
			
			switch (CIRCUIT_TYPE)
			{
				case PatientCctTypeValue::NEONATAL_CIRCUIT :
					// $[TI3]
					flowStartId = TEST_20_LPM;
					airTestPoint = SmDataId::SST_FS_CC_AIR_20_INSPIRATORY_FLOW ;
					o2TestPoint = SmDataId::SST_FS_CC_O2_20_INSPIRATORY_FLOW ;
					break;
					
				case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
				case PatientCctTypeValue::ADULT_CIRCUIT :
					// $[TI4]
					flowStartId = TEST_120_LPM;
					airTestPoint = SmDataId::SST_FS_CC_AIR_120_INSPIRATORY_FLOW ;
					o2TestPoint = SmDataId::SST_FS_CC_O2_120_INSPIRATORY_FLOW ;
					break;
					
				default :
					// unexpected circuit type...
					AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
					break;
			}

			// Move to the SST O2 flow sensor cross check
			RSmManager.advanceTestPoint( o2TestPoint) ;

			Boolean airPassed;
			Boolean o2Passed;

			// Test O2 flow and psol current if O2 is available
			if (o2Present)
			{
			// $[TI2.6.1]
#ifdef SIGMA_UNIT_TEST
				o2Passed = FsCrossCheckTest_UT::O2Passed;
#else
				o2Passed = testFlowAndPsol_( O2, flowStartId,
													TEST_0_LPM,
													STATE_SST );
#endif  // SIGMA_UNIT_TEST

			}
			else
			{
			// $[TI2.6.2]
				o2Passed = FALSE;
			}
			// end else

			// Move to the SST Air flow sensor cross check
			RSmManager.advanceTestPoint( airTestPoint) ;

			// Test air flow and psol current if air is available
			if (airPresent)
			{
			// $[TI2.6.3]
#ifdef SIGMA_UNIT_TEST
				airPassed = FsCrossCheckTest_UT::AirPassed;
#else
				airPassed = testFlowAndPsol_( AIR, flowStartId,
													TEST_0_LPM,
													STATE_SST );
#endif  // SIGMA_UNIT_TEST

			}
			else
			{
			// $[TI2.6.4]
				airPassed = FALSE;
			}
			// end else

			// Perform psol liftoff calibration for the gas(es)
			// that passed the flow and psol current tests
			findLiftoff_( airPassed, o2Passed);

			// prompt to connect humidifier only if a Failure did not
			// occur for the connected gas(es)
			Boolean airPassedOrNotConnected = !airPresent ||
											(airPresent && airPassed);
			Boolean o2PassedOrNotConnected = !o2Present ||
											(o2Present && o2Passed);
			if (airPassedOrNotConnected && o2PassedOrNotConnected)
			{
			// $[TI2.6.5]
				if ( !RSmUtilities.messagePrompt(
								SmPromptId::CONNECT_HUMIDIFIER_PROMPT) )
				{
				// $[TI2.6.5.1]
					RSmManager.sendOperatorExitToServiceData();
				}
				// $[TI2.6.5.2]
				// implied else
			}
			// $[TI2.6.6]
			// implied else
		}
		// end else
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
FsCrossCheckTest::SoftFault(const SoftFaultID  softFaultID,
			    const Uint32       lineNumber,
			    const char*        pFileName,
			    const char*        pPredicate)
{

  // Local declarations

  // Code

  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, FSCROSSCHECKTEST,
                          lineNumber, pFileName, pPredicate);

} // SoftFault


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: testFlowAndPsol_
//
//@ Interface-Description
//	This method performs the flow and PSOL checks for a specified gas
//	(air or O2).  Four arguments:
//		gas type (air or O2)
//		starting test flow ID
//		ending test flow ID
//		ventilator state (Service or SST)
//	If one or more FAILUREs occurred, returns FALSE, else returns TRUE.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Flush the circuit with the gas to be tested.
//	If air is the tested gas and if compressor is the air source,
//	delay to refill the compressor.
//	For each test flow level (except for 0 lpm):
//		Establish the required flow for the appropriate gas.
//		Delay for VIPER setup time and for alpha-filtering time.
//		Check if flow stability was reached:
//			If flow stability was not reached for highest test flow,
//			generate an ALERT, turn off flow and skip to next flow
//			level check.
//			If flow stability was not reached for any other test flow,
//			generate a FAILURE, turn off flow and exit.
//      Read and display the alpha-filtered inspiratory and unoffsetted 
//		expiratory flows, using the proper expiratory flow sensor gas table.
//      If flow delta is excessive, generate a FAILURE.
//		Read PSOL DAC command, then calculate and display the PSOL
//		command current.
//		If PSOL command current is out of range, generate a FAILURE.
//	For 0 lpm test flow:
//		Delay to allow residual flow to decay.
//		Read and display the inspiratory flow.
//		If flow is out of range, generate a FAILURE.
//		Delay for VIPER setup for next test flow.
//---------------------------------------------------------------------
//@ PreCondition
//	gas type must be air or O2
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
FsCrossCheckTest::testFlowAndPsol_( GasType testGas,
									const TestFlowId flowIdStart,
									const TestFlowId flowIdEnd,
									SigmaState state )
{
	CALL_TRACE("FsCrossCheckTest::testFlowAndPsol_( GasType testGas, \
									const TestFlowId flowIdStart, \
									const TestFlowId flowIdEnd, \
									SigmaState state )");

	CLASS_PRE_CONDITION( testGas == AIR || testGas == O2);
	CLASS_PRE_CONDITION( flowIdStart >= TEST_120_LPM && 
							flowIdStart <= TEST_0_LPM );
	CLASS_PRE_CONDITION( flowIdEnd >= TEST_120_LPM && 
							flowIdEnd <= TEST_0_LPM );

	Boolean highFlowAlert = FALSE;
	Boolean unstableFlow = FALSE;
	Boolean testPassed = TRUE;
	Boolean flowTestPassed = TRUE;
	Boolean commandTestPassed = TRUE;
	Boolean zeroInspFlowTestPassed = TRUE;

#ifdef SIGMA_UNIT_TEST
	FakeSensor *pInspFlowSensor;
	FakeSensor *pExhFlowSensor;
	FakePsol *pPsol;
#else
	FlowSensor *pInspFlowSensor;
	ExhFlowSensor_t *pExhFlowSensor;
	Psol *pPsol;
#endif  // SIGMA_UNIT_TEST

	// setup for gas type
	if (testGas == AIR)
	{
	// $[TI1]
		// flush system with air for good exh flow sensor readings
		RExhFlowSensor.setO2Percent(21.0f);
		RSmUtilities.flushTubing( AIR, state);
		pInspFlowSensor = &RAirFlowSensor;
		pExhFlowSensor = &RExhFlowSensor;
		pPsol = &RAirPsol;

		// if using compressor air, wait for compressor to recharge
		if ( !RSmGasSupply.isWallAirPresent()
			 && RCompressor.checkCompressorInstalled() )
		{
		// $[TI1.1]
#ifdef SIGMA_UNIT_TEST
			FsCrossCheckTest_UT::CompressorRecharged = TRUE;
#else
			Task::Delay( 0, COMPRESSOR_FILL_TIME);
#endif  // SIGMA_UNIT_TEST

		}
		// $[TI1.2]
		// implied else
	}
	else  // testGas == O2
	{
	// $[TI2]
		// flush system with O2 for good exh flow sensor readings
		RExhFlowSensor.setO2Percent(100.0f);
		RSmUtilities.flushTubing( O2, state);
		pInspFlowSensor = &RO2FlowSensor;

// E600 BDIO		pExhFlowSensor = &RExhO2FlowSensor;
		pExhFlowSensor = &RExhFlowSensor;			// keep it pointing to the same sensor

		pPsol = &RO2Psol;
	}
	// end else

	// perform the flow cross-checks and psol current checks
	for ( Int32 testIndex = flowIdStart;
		  testIndex <= flowIdEnd && !unstableFlow;
		  testIndex++ )
	{
	// $[TI3]
		// Check 0 lpm separately from rest of test flows
		if (testIndex != TEST_0_LPM)
		{
		// $[TI3.1]
			// ***** Non-zero flow test points only *****

#ifdef SIGMA_DEBUG
			Uint32 elapsedTime;
			OsTimeStamp timer;
#endif  // SIGMA_DEBUG

			// Set test gas to the test flow
			RSmFlowController.establishFlow( flowTestPoint_[testIndex],
											 DELIVERY, testGas);

// Skip the following delays during unit testing for greater
// execution speed
#ifndef SIGMA_UNIT_TEST
			// delay for VIPER setup
			Task::Delay(1, 500);
			RSmManager.signalTestPoint();

			// delay for alpha-filtering of flow values
			Task::Delay( 0, 300);

			// NOTE: waitForStableFlow(400) will add a minimum
			// of 200 msecs, max of 400 msecs extra delay while
			// testing for stable flow, for a total delay of
			// 2 secs min, 2.2 secs max between starting the
			// flow controller and checking the insp/exh flows.
#endif  // SIGMA_UNIT_TEST

			// check if flow is stable
			if (!RSmFlowController.waitForStableFlow(400))
			{
			// $[TI3.1.1]
				// check separately for stability at the highest test flow
				// vs all other non-zero test flows
				if (testIndex == TEST_120_LPM)
				{
				// $[TI3.1.1.1]
					// ***** Unstable flow for highest test flow *****

					// can't reach a stable high flow: flag an ALERT,
					// then continue testing other flow levels
					highFlowAlert = TRUE;

					// to stay in sync, send data points here also
					Real32 inspFlow = pInspFlowSensor->getValue();

					// TODO E600 MS, put this back in when it's ported and remove getValue call
					//E600_TO_CHECK Real32 exhFlow = pExhFlowSensor->getUnoffsettedValue();
					Real32 exhFlow = pExhFlowSensor->getValue();

					// read PSOL current
					Real32 psolMa = pPsol->getPsolCommand() * PSOL_COUNTS_TO_MA;

					RSmManager.sendTestPointToServiceData( inspFlow);
					RSmManager.sendTestPointToServiceData( exhFlow);
					RSmManager.sendTestPointToServiceData( psolMa);
				}
				else
				{
				// $[TI3.1.1.2]
					// ***** Unstable flow for non-highest non-zero test flow *****

					// can't reach a stable flow: bail out immediately
					unstableFlow = TRUE;
				}
				// end else

				// turn off gas
				RSmFlowController.stopFlowController();
			}
			else
			{
			// $[TI3.1.2]
				// ***** Stable flow was reached *****

#ifdef SIGMA_DEBUG
				elapsedTime = timer.getPassedTime();
				printf("elapsed time since establishFlow() = %u msec\n",
															elapsedTime);
#endif  // SIGMA_DEBUG

				// read alpha-filtered insp and exh flows
				Real32 inspFlow = pInspFlowSensor->getFilteredValue();

				// TODO E600 MS, put this back in when it's ported and remove getValue call
				//Real32 exhFlow = pExhFlowSensor->getUnoffsettedFilteredValue();
				Real32 exhFlow = pExhFlowSensor->getValue();


				// read PSOL current
				Real32 psolMa = pPsol->getPsolCommand() * PSOL_COUNTS_TO_MA;

				// turn off gas
				RSmFlowController.stopFlowController();

				// send data to gui
				RSmManager.sendTestPointToServiceData( inspFlow);
				RSmManager.sendTestPointToServiceData( exhFlow);
				RSmManager.sendTestPointToServiceData( psolMa);

				// calculate the max insp flow error for this test flow
				Real32 inspFlowError =
					RSmUtilities.flowError( flowTestPoint_[testIndex],
															DELIVERY);

				// calculate the max exh flow error for this test flow
				Real32 exhFlowError =
					RSmUtilities.flowError( flowTestPoint_[testIndex],
															EXHALATION);

				Real32 flowErr = ABS_VALUE(inspFlow - exhFlow);
				// check if the flow error is in range
				if (  flowErr >
					//inspFlow * inspFlowError + exhFlow * exhFlowError )
					// TODO E600_LL: triple the allowed error until new exhalation sensor work is completed
					3 * (inspFlow * inspFlowError + exhFlow * exhFlowError) )
				{
				// $[TI3.1.2.1]
					// flow error out of range
					flowTestPassed = FALSE;
				}
				// $[TI3.1.2.2]
				// implied else: flow cross-check passed

//TODO E600_LL: E600 hardware does not have feedback current to validate command flow.
//May need to validate another way or not do it altogether
#if 0				
				// check if PSOL command is in range for the test flow
				Real32 psolCurrentDiff = expectedPsolCurrent_(testGas,
							commandTestPoint_[testIndex]) - psolMa;

				if ( ABS_VALUE( psolCurrentDiff) >
					 commandTestLimit_[testIndex] )
				{
				// $[TI3.1.2.3]
					// PSOL command out of range
					commandTestPassed = FALSE;
				}
				// $[TI3.1.2.4]
				// implied else: PSOL command test passed
#endif
			}
			// end else
		}
		else  // 0 lpm insp flow check
		{
		// $[TI3.2]
			// ***** 0 lpm test point only *****

			// Allow time for flow to decay for accurate
			// VIPER reading at 0 flow
			Task::Delay(2);

			RSmManager.signalTestPoint();

			// Read insp flow with psol off
			Real32 inspFlow = pInspFlowSensor->getValue();
	
			RSmManager.sendTestPointToServiceData( inspFlow);

			if ( inspFlow > MAX_INSP_FLOW_PSOL_OFF )
			{
			// $[TI3.2.1]
				// 0 lpm check failed
				zeroInspFlowTestPassed = FALSE;
			}
			// $[TI3.2.2]
			// implied else: 0 lpm insp flow check passed
		}
		// end else
	}
	// end for

	// Generate errors messages based on errors detected above
	if (testGas == O2)
	{
	// $[TI4]
		// Generate O2 error messages

		if (unstableFlow)
		{
		// $[TI4.1]
			// Can't establish an O2 test flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_6);

			testPassed = FALSE;
		}
		// $[TI4.2]
		// implied else

		if (!flowTestPassed)
		{
		// $[TI4.3]
			// O2 Flow sensor cross-check failed
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_1);

			testPassed = FALSE;
		}
		// $[TI4.4]
		// implied else

		if (!commandTestPassed)
		{
		// $[TI4.5]
			// O2 PSOL current check failed
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_2);

			testPassed = FALSE;
		}
		// $[TI4.6]
		// implied else

		if (!zeroInspFlowTestPassed)
		{
		// $[TI4.7]
			// psol off insp flow is excessive
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_8);

			testPassed = FALSE;
		}
		// $[TI4.8]
		// implied else

		if (highFlowAlert)
		{
		// $[TI4.9]
			// can't reach stable high flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_10);
		}
		// $[TI4.10]
		// implied else
	}
	else  // testGas == AIR
	{
	// $[TI5]
		// Generate air error messages

		if (unstableFlow)
		{
		// $[TI5.1]
			// Can't establish an air test flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_7);

			testPassed = FALSE;
		}
		// $[TI5.2]
		// implied else

		if (!flowTestPassed)
		{
		// $[TI5.3]
			// Air Flow sensor cross-check failed
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_3);

			testPassed = FALSE;
		}
		// $[TI5.4]
		// implied else

		if (!commandTestPassed)
		{
		// $[TI5.5]
			// Air PSOL current check failed
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_4);

			testPassed = FALSE;
		}
		// $[TI5.6]
		// implied else

		if (!zeroInspFlowTestPassed)
		{
		// $[TI5.7]
			// psol off insp flow is excessive
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_9);

			testPassed = FALSE;
		}
		// $[TI5.8]
		// implied else

		if (highFlowAlert)
		{
		// $[TI5.9]
			// can't reach stable high flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_11);
		}
		// $[TI5.10]
		// implied else
	}
	// end else

	return( testPassed);

}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	 findLiftoff_
//
//@ Interface - Description
//	This method is used to find the liftoff for a given psol.
//	Two arguments:
//		Flag indicating if air flow/PSOL checks passed
//		Flag indicating if O2 flow/PSOL checks passed
//	No return value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	Read existing PSOL liftoff values from novram.
//	Perform PSOL liftoff calibration three times for each gas whose "passed" flag
//	is TRUE, then average the values from the three calibration runs.
//	For each PSOL liftoff calibration performed, update the liftoff value
//	in the appropriate PSOL object and in novram.
//-----------------------------------------------------------------------
//@ PreCondition
//  LIFTOFF_FLOW == MAX_PSOL_LOOKUP_FLOW
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================

void
FsCrossCheckTest::findLiftoff_( const Boolean airPassed,
								const Boolean o2Passed )
{
	CALL_TRACE("FsCrossCheckTest::findLiftoff_( \
								const Boolean airPassed, \
								const Boolean o2Passed )");

	CLASS_PRE_CONDITION( IsEquivalent( LIFTOFF_FLOW, MAX_PSOL_LOOKUP_FLOW, HUNDREDTHS)) ;
	
	Int32 idx;

	// Get psol liftoffs from novram
	PsolLiftoff psolLiftoff;
	NovRamManager::GetPsolLiftoffCurrents( psolLiftoff);

#if defined(GRAPHICS)
	printf("***** Before liftoff calibration *****\n");
	printf("air liftoff = %u\n", psolLiftoff.airLiftoff_);
	printf("o2 liftoff = %u\n", psolLiftoff.o2Liftoff_);
	printf("Table values (index, air, O2):\n");
	for( idx = 0; idx < 6; idx++)
	{
		printf( "%d, %lu, %lu\n", idx, psolLiftoff.airPsolTable_[idx],
										psolLiftoff.o2PsolTable_[idx] );
	}
	printf("\n");
#endif  // defined(GRAPHICS)

	// Skip the calibration runs if neither gas passed the flow
	// sensor cross-check
	if (airPassed || o2Passed)
	{
	// $[TI1]
		PsolLiftoff testTable[3];

		for (idx = 0; idx < 3; idx++)
		{
			testTable[idx] = psolLiftoff;
		}

		// Build 3 calibration tables per gas
		for (idx = 0; idx < 3; idx++)
		{
#if defined(GRAPHICS)
			printf("***** Table iteration #%d*****\n", idx);
#endif  // defined(GRAPHICS)

			buildPsolTables_( airPassed, o2Passed, testTable[idx]);
			testTable[idx].checkPsolTables();
		
			Task::Delay( 1);
		}

		// Average the tables
		psolLiftoff = (testTable[0] + testTable[1] + testTable[2]) / 3;
	}
	// $[TI2]
	
	// update psol objects
	Uint16 airLiftoffCount = psolLiftoff.getAirPsolLiftoff();
	Uint16 o2LiftoffCount = psolLiftoff.getO2PsolLiftoff();
	RAirPsol.setLiftoff( airLiftoffCount);
	RO2Psol.setLiftoff( o2LiftoffCount);

	// Update novram
	NovRamManager::UpdatePsolLiftoffCurrents( psolLiftoff);

	// Send liftoff counts to the gui
	RSmManager.advanceTestPoint( 
		SmDataId::FS_CROSS_CHECK_AIR_LIFTOFF_COMMAND);

	RSmManager.sendTestPointToServiceData( airLiftoffCount);
	RSmManager.sendTestPointToServiceData( o2LiftoffCount);

#if defined(GRAPHICS)
	printf("***** After liftoff calibration *****\n");
	printf("air liftoff = %u\n", psolLiftoff.airLiftoff_);
	printf("o2 liftoff = %u\n", psolLiftoff.o2Liftoff_);
	printf("Table values (index, air, O2):\n");
	for( idx = 0; idx < 6; idx++)
	{
		printf( "%d, %lu, %lu\n", idx, psolLiftoff.airPsolTable_[idx],
										psolLiftoff.o2PsolTable_[idx] );
	}
	printf("\n");
#endif  // defined(GRAPHICS)

}



// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	 buildPsolTables_
//
//@ Interface - Description
//	This method performs one PSOL liftoff calibration for both gases.
//	Three arguments:
//	Boolean indicating if air flow sensor cross-check passed
//	Boolean indicating if O2 flow sensor cross-check passed
//	PsolLiftoff object reference
//-----------------------------------------------------------------------
//@ Implementation - Description
//	Perform the PSOL liftoff calibration for each gas which passed
//	the flow sensor cross-check.  Return the calibration data via the
//	PsolLiftoff object reference.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
void
FsCrossCheckTest::buildPsolTables_( Boolean airPassed, Boolean o2Passed,
										PsolLiftoff& psolLiftoff )
{
	Real32 airFlow, o2Flow;

	Uint16 airPsolCount = LIFTOFF_STARTING_COUNT;
	Uint16 o2PsolCount = LIFTOFF_STARTING_COUNT;

	Uint32 airThresholdReachedCounter = 0;
	Uint32 o2ThresholdReachedCounter = 0;
	Uint16 airFlowIndex = 0 ;
	Uint16 o2FlowIndex = 0 ;

	Boolean airLiftoffDone;
	Boolean o2LiftoffDone;
	Boolean freezeAirPsolUpdate = FALSE ;
	Boolean freezeO2PsolUpdate = FALSE ;
	Boolean airTableDone = FALSE ;
	Boolean o2TableDone = FALSE ;
		
	Uint16 airLiftoffCount = psolLiftoff.getAirPsolLiftoff();
	Uint16 o2LiftoffCount = psolLiftoff.getO2PsolLiftoff();

	// perform psol liftoff calibration only for psol(s)
	// whose gas passed the flow and psol current checks
	if (!airPassed)
	{
	// $[TI1]
		// air failed: keep liftoff count from novram
		airLiftoffDone = TRUE;
	}
	else
	{
	// $[TI2]
		airLiftoffDone = FALSE;
		RAirPsol.updatePsol( airPsolCount);
	}

	if (!o2Passed)
	{
	// $[TI3]
		// O2 failed: keep liftoff count from novram
		o2LiftoffDone = TRUE;
	}
	else
	{
	// $[TI4]
		o2LiftoffDone = FALSE;
		RO2Psol.updatePsol( o2PsolCount);
	}

	while (!airLiftoffDone || !o2LiftoffDone)
	{
	// $[TI5]
		// delay one service mode cycle
		Task::Delay( 0, SM_CYCLE_TIME);

		airFlow = RAirFlowSensor.getValue();
		o2Flow = RO2FlowSensor.getValue();

#ifdef SIGMA_UNIT_TEST_DCS_5684

		static Uint32 ii = 0 ;

		ii++ ;

		airFlow = ii * 0.005 ;
		o2Flow = (ii + 5) * 0.005 ;

#endif // SIGMA_UNIT_TEST_DCS_5684

		if (!airLiftoffDone)
		{
		// $[TI5.1]
			freezeAirPsolUpdate = FALSE ;
			
			if (airFlow >= (airFlowIndex + 1) * FLOW_INCREMENT && !airTableDone)
			{
				// $[TI6]
				freezeAirPsolUpdate = TRUE ;
				psolLiftoff.setAirPsolTable( airFlowIndex, airPsolCount) ;
				airFlowIndex++ ;

				if (airFlowIndex >= PSOL_TABLE_SIZE)
				{
					// $[TI7]
					airTableDone = TRUE ;
				}
				// $[TI8]
			}
			// $[TI9]
			
			if (airFlow >= LIFTOFF_FLOW)
			{
			// $[TI5.1.1]
				// reached liftoff threshold, so increment the
				// threshold-reached counter
				airThresholdReachedCounter++;

				freezeAirPsolUpdate = TRUE ;

				// If liftoff flow threshold was reached
				// for two iterations, we're done
				if (airThresholdReachedCounter >= 2)
				{
				// $[TI5.1.1.1]
					airLiftoffCount = airPsolCount;
					RAirPsol.updatePsol(0);
					airLiftoffDone = TRUE;
					airTableDone = TRUE ;

					// fill in the remaining psol lookup table with the last count
					for (; airFlowIndex < PSOL_TABLE_SIZE; airFlowIndex++)
					{
					// $[TI5.1.1.1.1]
						psolLiftoff.setAirPsolTable( airFlowIndex, airPsolCount) ;
					}
					// $[TI5.1.1.1.2]
				}
				// $[TI5.1.1.2]
				// implied else: not done yet
			}

			if (!freezeAirPsolUpdate)
			{
			// $[TI5.1.2]
				// didn't reach liftoff threshold, so zero the
				// threshold-reached counter and increment psol count
				airThresholdReachedCounter = 0;
				RAirPsol.updatePsol( ++airPsolCount);

				// stop the calibration if the max psol liftoff
				// count is reached
				if (airPsolCount >= MAX_LIFTOFF_COUNT)
				{
				// $[TI5.1.2.1]
					airLiftoffCount = airPsolCount;
					RAirPsol.updatePsol(0);
					airLiftoffDone = TRUE;
					airTableDone = TRUE ;

					// fill in the remaining psol lookup table with MAX_LIFTOFF_COUNT
					for (; airFlowIndex < PSOL_TABLE_SIZE; airFlowIndex++)
					{
						// $[TI14]
						psolLiftoff.setAirPsolTable( airFlowIndex, airPsolCount) ;
					}
					// $[TI15]
				}
				// $[TI5.1.2.2]
				// implied else: not done yet
			}
			// end else
		}
		// $[TI5.2]
		// implied else: air psol liftoff calibration is done

		if (!o2LiftoffDone)
		{
		// $[TI5.3]
			freezeO2PsolUpdate = FALSE ;
			
			if (o2Flow >= (o2FlowIndex + 1) * FLOW_INCREMENT && !o2TableDone)
			{
				// $[TI10]
				freezeO2PsolUpdate = TRUE ;
				psolLiftoff.setO2PsolTable( o2FlowIndex, o2PsolCount) ;
				o2FlowIndex++ ;

				if (o2FlowIndex >= PSOL_TABLE_SIZE)
				{
					// $[TI11]
					o2TableDone = TRUE ;
				}
				// $[TI12]
			}
			// $[TI13]

			if (o2Flow >= LIFTOFF_FLOW)
			{
			// $[TI5.3.1]
				// reached liftoff threshold, so increment the
				// threshold-reached counter
				o2ThresholdReachedCounter++;

				freezeO2PsolUpdate = TRUE ;

				// If liftoff flow threshold was reached
				// for two iterations, we're done
				if (o2ThresholdReachedCounter >= 2)
				{
				// $[TI5.3.1.1]
					o2LiftoffCount = o2PsolCount;
					RO2Psol.updatePsol(0);
					o2LiftoffDone = TRUE;
					o2TableDone = TRUE ;

					// fill in the remaining psol lookup table with the last count
					for (; o2FlowIndex < PSOL_TABLE_SIZE; o2FlowIndex++)
					{
					// $[TI5.3.1.1.1]
						psolLiftoff.setO2PsolTable( o2FlowIndex, o2PsolCount) ;
					}
					// $[TI5.3.1.1.2]
				}
				// $[TI5.3.1.2]
				// implied else: not done yet
			}

			if (!freezeO2PsolUpdate)
			{
			// $[TI5.3.2]
				// didn't reach liftoff threshold, so zero the
				// threshold-reached counter and increment psol count
				o2ThresholdReachedCounter = 0;
				RO2Psol.updatePsol( ++o2PsolCount);

				// stop the calibration if the max psol liftoff
				// count is reached
				if (o2PsolCount >= MAX_LIFTOFF_COUNT)
				{
				// $[TI5.3.2.1]
					o2LiftoffCount = o2PsolCount;
					RO2Psol.updatePsol(0);
					o2LiftoffDone = TRUE;
					o2TableDone = TRUE ;

					// fill in the remaining psol lookup table with MAX_LIFTOFF_COUNT
					for (; o2FlowIndex < PSOL_TABLE_SIZE; o2FlowIndex++)
					{
						// $[TI16]
						psolLiftoff.setO2PsolTable( o2FlowIndex, o2PsolCount) ;
					}
					// $[TI17]
				}
				// $[TI5.3.2.2]
				// implied else: not done yet
			}
			// end else
		}
		// $[TI5.4]
		// implied else: o2 psol liftoff calibration is done
	}
	// end while

	psolLiftoff.setO2PsolLiftoff( o2LiftoffCount);
	psolLiftoff.setAirPsolLiftoff( airLiftoffCount);

#if defined(GRAPHICS)
	printf("air liftoff = %u\n", psolLiftoff.airLiftoff_);
	printf("o2 liftoff = %u\n", psolLiftoff.o2Liftoff_);
	printf("Table values (index, air, O2):\n");
	for( Int32 idx = 0; idx < PSOL_TABLE_SIZE; idx++)
	{
		printf( "%d, %lu, %lu\n", idx, psolLiftoff.airPsolTable_[idx],
										psolLiftoff.o2PsolTable_[idx] );
	}
	printf("\n");
#endif  // defined(GRAPHICS)

}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	 expectedPsolCurrent_
//
//@ Interface - Description
//	This method is used to calculate the expected PSOL current for a
//	given test flow, gas type, and atmospheric pressure. It takes the
//	gas type and nominal PSOL current as arguments and returns the
//	expected PSOL current.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	Read atmospheric pressure sensor and store value in psia.
//	Determine gas factor.
//	Calculate expected PSOL current from Patm, gas factor, and nominal
//	PSOL current.
//	Return the expected PSOL current.
//-----------------------------------------------------------------------
//@ PreCondition
//  testGas must be AIR or O2.
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
Real32
FsCrossCheckTest::expectedPsolCurrent_( const GasType testGas,
										const Real32 nominalPsolCurrent )
{
	CALL_TRACE("FsCrossCheckTest::expectedPsolCurrent_( \
									const GasType testGas, \
									const Real32 nominalPsolCurrent )");

	CLASS_PRE_CONDITION( testGas == AIR || testGas == O2);
	
	const Real32 PSOL_CURRENT_OFFSET = 167.43; // mA
	const Real32 ATM_PRESS_FACTOR = 0.0321;    // assumes Patm in psia
	const Real32 ATM_PRESS_OFFSET = 0.5257;    // assumes Patm in psia
	const Real32 AIR_FACTOR = 1.0;
	const Real32 O2_FACTOR = 0.951333;
	
	// Patm in psia
	Real32 atmPressure =
		RSmUtilities.atmPressureBounded() / PSI_TO_CMH20_FACTOR;
	
	Real32 gasFactor =
		(testGas == AIR) ? AIR_FACTOR : O2_FACTOR;
	// $[TI1] air
	// $[TI2] O2
	
	Real32 psolCurrent = nominalPsolCurrent +
		(nominalPsolCurrent - PSOL_CURRENT_OFFSET) *
		( 1 - (atmPressure * ATM_PRESS_FACTOR + ATM_PRESS_OFFSET) *
			gasFactor );  // mA

	return ( psolCurrent);
}
