#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FlowVolumePlot - A loop plot which plots Flow against
//                          volume.
//---------------------------------------------------------------------
//@ Interface-Description
//  FlowVolumePlot is derived from LoopPlot. This class only specifies
//  the data types to be plotted (Flow vs. Volume) as part of its
//  construction. All other calls are handled by the LoopPlot base
//  class.
//---------------------------------------------------------------------
//@ Rationale
//  This class adds features specific to flow-volume loop plots.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All plot functions are currently handled in the LoopPlot base 
//  class.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  N/A
//---------------------------------------------------------------------
//@ Invariants
//  N/A
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/FlowVolumePlot.ccv   25.0.4.0   19 Nov 2013 14:07:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc   Date:  05-Jan-2009     SCR Number: 6283
//  Project:  840S
//  Description:
//       Removed forwarding functions in favor of virtual functions.
//
//  Revision: 001   By: rhj   Date:  20-Sept-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//       Initial version.    
//=====================================================================

#include "FlowVolumePlot.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "WaveformIntervalIter.hh"
//@ End-Usage

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlowVolumePlot  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses setXYUnits to specify net volume and flow as the datum to be 
//  plotted in the LoopPlot.
// $[RM01002]
// $[RM12066]
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FlowVolumePlot::FlowVolumePlot(void)
{
    CALL_TRACE("FlowVolumePlot::FlowVolumePlot(void)");

    setXYUnits(PatientDataId::NET_VOL_ITEM, PatientDataId::NET_FLOW_ITEM );


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FlowVolumePlot  [Destructor]
//
//@ Interface-Description
// Destroys FlowVolumePlot.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FlowVolumePlot::~FlowVolumePlot(void)
{
    CALL_TRACE("FlowVolumePlot::~FlowVolumePlot(void)");

    // Do nothing
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void
FlowVolumePlot::SoftFault(const SoftFaultID  softFaultID,
                          const Uint32       lineNumber,
                          const char*        pFileName,
                          const char*        pPredicate)  
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, FLOWVOLUMEPLOT,
                            lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

