
#ifndef TrendPlot_HH
#define TrendPlot_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendPlot
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendPlot.hhv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
// 
//  Revision: 001  By:  ksg	   Date:  20-Jun-2007    SCR Number: 6237
//  Project:  TREND
//  Description:
//		Trend project initial version.
//====================================================================

#include "TrendPlotBasis.hh"

class TrendPlot : public TrendPlotBasis
{
public:
	TrendPlot();
	~TrendPlot(void);

	virtual void activate(void);
    virtual void update(const TrendDataSet& rTrendDataSet, Uint16 dataColumn);
    virtual void erase(void);
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:
	// these methods are purposely declared, but not implemented...
	TrendPlot(const TrendPlot&);		// not implemented...
	void   operator=(const TrendPlot&);	// not implemented...

	void updateEventMarkers_(const TrendDataSet& rTrendDataSet);

	//@ Data-Member: currX_
	// The current X pixel coordinate.
	Int32 currX_;

	//@ Data-Member: lastX_
	// The last X pixel coordinate, i.e. the one previous to the current.
	Int32 lastX_;

	//@ Data-Member: lineColorLeft_
	// Indicates the current plot line color 
	Colors::ColorS lineColorLeft_;

	//@ Data-Member: currY1_
	// The second current Y pixel coordinate.
	Int32 currY1_;

	//@ Data-Member: lastY1_
	// The second last Y pixel coordinate, i.e. the one previous to
	// the current.
	Int32 lastY1_;
};

#endif // TrendPlot_HH 
