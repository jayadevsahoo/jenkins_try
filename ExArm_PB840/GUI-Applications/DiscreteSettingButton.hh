#ifndef DiscreteSettingButton_HH
#define DiscreteSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: DiscreteSettingButton - A generic setting button designed
// for adjusting discrete settings.  Stores a list of the text strings
// corresponding to each of the discrete values of a setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DiscreteSettingButton.hhv   25.0.4.0   19 Nov 2013 14:07:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  gdc	   Date:  09-Apr-2007    SCR Number:  6237
//  Project:  Trend
//  Description:
//      Added vertical positioning logic for the value text.
//
//  Revision: 004  By:  sah	   Date:  09-Jun-2000    DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//      *  as part of the re-design, deleted 'addValue()' and added new
//         'setValueString_()' method
//      *  deleted unused 'setText_()' method
//
//  Revision: 003  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//
//====================================================================

#include "SettingButton.hh"

//@ Usage-Classes
#include "DiscreteValue.hh"
//@ End-Usage

class DiscreteSettingButton : public SettingButton
{
public:
	//@ struct:  ValueInfo
	//  Used for storing information about each discrete value for instances
	//  of this class.
	struct ValueInfo
	{
		//-------------------------------------------------------------
		//  NOTE:  This struct will be overlayed onto memory provided to
		//         instances of this class, via 'setValueInfo()'.  This
		//         means that, though declared as an array of one element,
		//         'arrValues[]' will actually be as long as 'numValues'
		//         elements, as initialized by the client of this class'
		//         instances.  An array declaration, rather than a pointer
		//         declaration, is needed for 'arrValues', because the
		//         elements will reside in the contiguous memory of this
		//         struct, whereas a pointer would point somewhere else.
		//-------------------------------------------------------------
		Uint      numValues;
		StringId  arrValues[1];
	};

	DiscreteSettingButton(Uint16 xOrg, Uint16 yOrg, Uint16 width,
					Uint16 height, ButtonType buttonType, Uint8 bevelSize,
					CornerType cornerType, BorderType borderType,
					StringId titleText,
					SettingId::SettingIdType settingId,
					SubScreen *pFocusSubScreen,
					StringId messageId, Uint valuePointSize,
					Boolean isMainSetting = FALSE);
	~DiscreteSettingButton(void);

	// provide this discrete setting with the information about its
	// set of possible values...
	inline void setValueInfo(const ValueInfo* pValueInfo);

	inline void setValueTextPosition(const Gravity gravity);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
                                const SettingSubject*         pSubject);

	//@ Data-Member:  pValueInfo_
	//  Pointer to an array of 'ValueInfo' structs, containing info about
	//  each of the setting's possible values.
	const ValueInfo*  pValueInfo_;

private:
	// these methods are purposely declared, but not implemented...
	DiscreteSettingButton(const DiscreteSettingButton&);		// not implemented...
	void   operator=(const DiscreteSettingButton&);	// not implemented...

	void  setValueString_(const DiscreteValue  settingValue,
					      const Boolean        isChanged,
					      TextField&           rTextField) const;

	//@ Data-Member:  VALUE_PT_SZ_
	// Point-size to be used for this button's setting values.
	const Uint  VALUE_PT_SZ_;

	//@ Data-Member: textValue_
	// The text field which displays the string corresponding to the
	// current adjusted discrete value.
	TextField  textValue_;

	//@ Data-Member: valueTextPosition_
	// The vertical positioning of the text field in the button container
	Gravity valueTextPosition_;
};


#include "DiscreteSettingButton.in"

#endif // DiscreteSettingButton_HH 
