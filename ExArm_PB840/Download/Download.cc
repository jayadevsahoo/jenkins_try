#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Nellcor Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: Download - provides capability to download  a Sigma
//       executable image file to a Sigma machine's flash memory.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class provides the main task for a download server and
//    a download client. The client side initializes the download
//    system environment to kick off three separate VRTX tasks to
//    perform various download functions. It provides methods
//    to establish communication between a download client and a server.
//---------------------------------------------------------------------
//@ Rationale
//   Encapsulate all the intelligence and data to drive and organize
//   a complete download operation between a download server and a client.
//---------------------------------------------------------------------
//@ Implementation-Description
//    A download Server and a client finds each other through the BOOTP
//    derivative protocol using the UDP broadcast messages.
//    After finding a specific download server, the client makes
//    a direct TCP/IP socket connection to the server and uses the
//    socket connection to download the flash image file.
//---------------------------------------------------------------------
//@ Fault-Handling
//  A standard pre-condition test is used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@(#) $Header:   /840/Baseline/Download/vcssrc/Download.ccv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 007   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline
//
//  Revision: 006  By: gdc  Date: 23-Feb-2005  DR Number: 6148
//    Project:  NIV1
//    Description:
//       DCS 6148 - move to solaris
//       Changed to conform to 840 naming conventions.
//
//  Revision: 005  By: gdc  Date: 22-Feb-2005  DR Number: 6148
//    Project:  NIV1
//    Description:
//       DCS 6148 - move to solaris
//       modified to instantiate Download as a static object instead of using
//       placement new which is not inherently supported by the GNU compiler/linker
//
//  Revision: 004  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh.
//
//  Revision: 002  By:  hct      Date:  25 APR 1997    DR Number: 1996
//       Project:  Sigma (R8027)
//       Description:
//             Added requirement numbers 00445, 00447, 00449, 00519,
//             00522.
//
//  Revision: 001  By:  jhv      Date:  13 DEC 1996    DR Number: N/A
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

//@ Code...

#include "Download.hh"
#include "BOOTP.hh"
#include "sockintf.hh"
#include "TargetAddr.hh"
#include "TftpSocket.hh"
#if	defined(SIGMA_CLIENT)
#include "Queue.hh"
#include "IoRegisterCons.hh"
#include "MemoryMap.hh"
#include "OsUtil.hh"
#elif	defined(SIGMA_SERVER)
#include <sys/time.h>
#include <strings.h>
#endif //defined(SIGMA_CLIENT)

extern TargetTypes BoardType (void);
extern "C" void ether_addr(short unit, char  etherAddr[]);
extern "C" void ResetTheSystem(void);
extern "C" void LightDiagnosticLED(Uint8 eCode);

#if	defined(SIGMA_SUN)
extern Boolean UseServerIpAddr;
#endif //defined(SIGMA_SUN)
extern TargetAddr *PTargetAddr;

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

Boolean Download::Initialized_ = FALSE;
#if	defined(SIGMA_SERVER)
char *Download::PGuiBootFile_ = NULL;
char *Download::PBdBootFile_ = NULL;
#endif //defined(SIGMA_SERVER)

ErrMsg Download::ErrTbl_[] =
{
	{ "Download Server sent an error code", 	DOWNLOAD_SERVER_ERROR },
	{ "Communicaiton Receive Error", 	COMM_RECV_ERROR  },
	{ "Communication Send Error", 	COMM_SEND_ERROR  },
	{ "Flash Memory checksum failure, bad flash memory",
			FLASH_CHECKSUM_FAILURE },
	{ "Flash Memory Erase failure, bad flash memory",
			FLASH_ERASE_FAILURE },
	{ "Downloaded data checksum failure, network data corrupted memory",
		    DOWNLOAD_DATA_CORRUPTION },
	{ "Out of download buffer to receive comm data, Flash memory burn too slow",
			OUT_OF_DOWNLOAD_BUFFER },
	{ "Write to Flash Memory failed", FLASH_MEMORY_WRITE_FAILED },
	{ "Wrong file block number received", OUT_OF_SYNC_COMM_DATA },
	{ "Network socket library failed", SOCKET_LIB_ERROR },
	{ "Illegal TFTP Op code received",  ILLEGAL_TFTP_OP },
	{ "Server is not transmitting data too long", SERVER_NOT_SENDING_DATA },
	{ "Download file in an incompatible data block size", INCOMPATIBLE_DATA_BLOCK_SIZE },
	{ "Target times out for a flash memory burning", FLASH_BURNING_TIMEOUT },
	{ "Flash memory checksum for the Serial number block failure ", SERVICE_DATA_CHECKSUM_FAILURE },
	{ NULL, 0}
};

#if	defined(SIGMA_SERVER)
static Boolean CloseCommand = FALSE;
char *BDExecFile = "BdLoad.abs";
char *GUIExecFile = "GuiLoad.abs";
#endif //defined(SIGMA_SERVER)

extern "C"  { void SetStackwareIpAddr(Uint32 my_ip_addr); }

#if	defined(SIGMA_CLIENT)
static Uint QMem_[(sizeof(Queue)+sizeof(Uint32)-1)/sizeof(Uint32)];
Queue &Download::RFlashMemQueue =  *((Queue *) QMem_);

Uint PMem_[(sizeof(FlashMem)+sizeof(Uint32)-1)/sizeof(Uint32)];
FlashMem &Download::RFlashMemBurner =  *((FlashMem *) PMem_);

#endif //defined(SIGMA_CLIENT)

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Download(void)  [Default Constructor]
//
//@ Interface-Description
//   Initialize all the member variables to its initial values.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Download::Download(void)
{
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Download(void)  [Destructor]
//
//@ Interface-Description
//   Default Destructor. Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 none.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Download::~Download(void)
{
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize(void)
//
//@ Interface-Description
//    Initialize the static objects of the Download class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Download::Initialize(void)
{
	if ( Download::Initialized_ ) // $[TI1]
	{
		return;
	}
	// else $[TI2]

#if	defined(SIGMA_SERVER)
	if ( !Download::PBdBootFile_ )
	{
		Download::PBdBootFile_ = BDExecFile;
	}
	if ( !Download::PGuiBootFile_ )
	{
		Download::PGuiBootFile_ = GUIExecFile;
	}
#elif defined(SIGMA_CLIENT)
	new ( &Download::RFlashMemQueue ) Queue(FLASH_MEMORY_Q);
	new ( &Download::RFlashMemBurner ) FlashMem();
#endif //defined(SIGMA_CLIENT)

	Download::GetDownloader();
	TftpSocket::Initialize();
	Download::Initialized_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDownloader(void)
//
//@ Interface-Description
//  Constructs the static instance of the Download class and returns
//  its reference. This is the only instantiation of the Download
//  class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Download&
Download::GetDownloader(void)
{
  static Download Downloader_;

  return(Downloader_);
}

#if	defined(SIGMA_CLIENT)
//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  InitializeSigma(void)
//
//@ Interface-Description
//  Initialize the download system and start download tasks.
//  This function is called at the end of kernel OS bootup and
//  initialization.  It then calls DownloadTask::CreateTasks() to create
//  all the tasks that download needs. Upon returning from this
//  function, vrtx_go() is called to start VRTX OS and kick off
//  download tasks.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Free-Function
//=====================================================================

#include <stdio.h>
#include "DownloadTask.hh"

extern "C" void
InitializeSigma(void)
{
	// $[TI1]
	Download::Initialize();

    // initialize/create init task
    DownloadTask::CreateTasks();

#ifdef E600_840_TEMP_REMOVED
    BoardInit();
#endif

    // vrtx_go upon return
}
#endif //defined(SIGMA_CLIENT)

#if	defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: obtainClientIpAddr_(void)
//
//@ Interface-Description
//    Get the IP address of this machine from the download
//    server by broadcasting a BOOTP request package. The download
//    server broadcasts a BOOTP response package that contains
//    the machine's designated IP address. It receives the packet
//    and initialize the given IP address to the TCP/IP software
//	  component( Stackware ).
//    $[00449]
//---------------------------------------------------------------------
//@ Implementation-Description
//    The method turns off the BD audio alarm when it finds a willing
//    Download server IP address.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
Download::obtainClientIpAddr_(void)
{
xsFDSet_t readMask;
BOOTPMsg reqBuf;
BOOTPMsg respBuf;
Saddr from;
int i = MAX_WAIT_LOOP;

	reqBuf.opCode = BOOTP_REQUEST;
	ether_addr((short)1,(char *) reqBuf.EtherAddr);
	reqBuf.targetType = ::BoardType();
	reqBuf.ClientIpAddr = 0;
	do
	{
		if ( broadcastSocket_.broadcastData( (char *) &reqBuf, sizeof(reqBuf),
				SERVER_BROADCAST_PORT ) != sizeof(reqBuf) ) // $[TI1]
		{
			ReportError(
				"Download::obtainClientIpAddr_():broadcastData() Bootp request Comm Error", errno);
			return(FAILURE);
		} // else $[TI2]
		FD_ZERO(&readMask);
		FD_SET(broadcastSocket_.getSocketId(), &readMask);
		if ( WaitForInput(&readMask, WAIT_DATA_INTERVAL ) > 0 ) // $[TI3]
		{
			if ( FD_ISSET( broadcastSocket_.getSocketId(), &readMask ) ) // $[TI3.1]
			{
				if ( broadcastSocket_.readDataFrom( &respBuf, sizeof(BOOTPMsg), &from.sa)
					!= sizeof(BOOTPMsg)) // $[TI3.1.1]
				{
					ReportError(
					  "Download::obtainClientIpAddr_():readDataFrom() Bootp reply Comm Error", errno);
				} // else $[TI3.1.2]
				CLASS_ASSERTION( respBuf.opCode ==  BOOTP_REPLY );
				if ( bcmp( (char *) reqBuf.EtherAddr,
						(char *) respBuf.EtherAddr, ETHERNET_ADDR_SIZE ) != 0 ) // $[TI3.1.3]
				{
					// This message is not for this client.
					continue;
				} // else $[TI3.1.4]
				CLASS_ASSERTION( respBuf.targetType == ::BoardType());
				if  ( respBuf.ClientIpAddr ) // $[TI3.1.5]
				{
					clientIpAddr_ = ntohl(respBuf.ClientIpAddr);
					SetStackwareIpAddr( clientIpAddr_ );
				}
				else // $[TI3.1.6]
				{
					ReportError(
					"Download::obtainClientIpAddr_():readDataFrom() Server sent an illegal client IP address 0", errno);
					return(FAILURE);
				}
				serverIpAddr_ = respBuf.ServerIpAddr;
				strcpy(bootImageFile_, respBuf.bootFileName);
				StopAudibleAlarm();
				return(SUCCESS);
			} // else $[TI3.2]
		} // else $[TI4]
	} while(i-- > 0 ); // more than 10 minutes has passed.
	ReportError(
				"Download::obtainClientIpAddr_():WaitForInput MAX_WAIT_LOOP no download Server",FINDING_SERVER_TIMEOUT );
	return(FAILURE);
}
#endif //defined(SIGMA_CLIENT)

#if  defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task(void)
//
//@ Interface-Description
//	  This is a static method that is directly called from the VRTX
//    OS to kick off the main download task, tftp task which carries
//    out high level protocol with a download server to actually
//    receive all the download image data.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Call the TFTP client instance task method on the system's
//   Downloader object.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Download::Task(void)
{
	// $[TI1]
	CLASS_ASSERTION( Download::Initialized_);

	Download::GetDownloader().clientTask();
}
#endif  //defined(SIGMA_CLIENT)

#if  defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clientTask(void)
//
//@ Interface-Description
//    The main client task method which initiates network communication
//    with a download server. It finds out the IP address of the client
//    machine and a TFTP server machine using the BOOTP protocol.
//    It initializes the machine's IP address in the Stackware with
//	  an IP address assigned by the server and makes a TCP/IP socket
//	  connection to the Tftp server. Using the dedicated socket
//    connection, it downloads the flash image via TFTP protocol.
//    $[00445] $[00447] $[00519] $[00522]
//---------------------------------------------------------------------
//@ Implementation-Description
//	 If downloading of the boot image file fails, retry up to
//   5 times after reopening the socket connection at each time.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Download::clientTask(void)
{
int iLoop;
int forever =1;

    wait_for_stackware();

	while(forever)
	{
		if ( broadcastSocket_.open( CLIENT_BROADCAST_PORT, 0) == -1 ) // no TI needed
		{
			ReportError("Download::clientTask():open() broadcast Socketopen failed",
					errno);
			CLASS_ASSERTION_FAILURE();
		} // else $[TI1]
		broadcastSocket_.computeBroadcastAddr(0);
		CLASS_ASSERTION( broadcastSocket_.avoidRouting() > 0 );

		if (obtainClientIpAddr_() != SUCCESS ) // $[TI2]
		{
			ReportError("Download::clientTask():obtainClientIpAddr_() Failed in establishing the Target IP address\n");
			::LightDiagnosticLED(SERVER_NOT_ACTIVE);

		} // else $[TI3]
		for (iLoop = 0; iLoop < DOWNLOAD_COMM_RETRY; iLoop++)
		{
			if ( connections_[0].open(DOWNLOAD_CLIENT_PORT, 0) < 0 ) // $[TI4]
                        {
				continue;
                        }  // else $[TI5]

			if ( connections_[0].connectToServer( serverIpAddr_,
				DOWNLOAD_SERVER_PORT, CONNECT_DELAY_INTERVAL ) < 0 ) // $[TI6]
			{
				connections_[0].close("Fail to make a socket connection to a download Server");
				continue;
			} // else $[TI7]

			if ( connections_[0].downloadFile(bootImageFile_) == SUCCESS ) // $[TI8]
			{
				DelayTask(ONE_SECOND);
				connections_[0].close(NULL);
				// Clear up the novram memory.
				bzero( (char *)NOVRAM_BASE, NOVRAM_LENGTH);
				::ResetTheSystem(); //reset the system.
			}
			else // $[TI9]
			{
				connections_[0].close("");
				continue;
			}
		} // no [TI] needed
		mainSocket_.close();
		broadcastSocket_.close();
		SetStackwareIpAddr(0); // Put the Stackware IP address for the BOOTP mode.
#ifdef SIGMA_UNIT_TEST
		break;
#endif // SIGMA_UNIT_TEST
	} // no [TI] needed
}
#endif  //defined(SIGMA_CLIENT)

#if	defined(SIGMA_SERVER)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNextClientIpAddr_(void)
//
//@ Interface-Description
//    Come up with a unique IP address that can be allocated for
//	  the next client. A client's IP address is derived from
//	  a download server machines's IP address. A client's IP address
//	  share common network id with the server IP address.
//	  The method returns a next available IP address for a client.
//	  The method returns a zero when it fails to allocate a valid
//	  client IP address.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  A client's IP address starts out one higher than the
//    server's IP address sharing a same network id.
//	  The first three bytes are considered to be a network
//	  id and they do not change. The last byte is a machine id
//	  and it is incremented by one and wraps around  when it
//	  reaches 255. The routine checks the other client's IP address
//	  to avoid any IP address conflicts.
//    This method assumes that next MAX_CONNECTIONS number of IP addresses
//    are allocated for the download.
//    On SUN environment or when a specific download Target machine
//    is specified, pre-allocated set of IP addresses are used for
//    the download clients.
//---------------------------------------------------------------------
//@ PreCondition
//		( serverIpAddr_ )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int32
Download::getNextClientIpAddr_(void)
{
Uint8 hostId;
Boolean found;
Int i, loopCount =0;

	CLASS_PRE_CONDITION(serverIpAddr_);

#if	defined(SIGMA_SUN )
	if ( (UseServerIpAddr  == TRUE) && ( !clientIpAddr_ ))
	{

			hostId =  serverIpAddr_ & ~NETWORK_ID_MASK;
			clientIpAddr_ = ( serverIpAddr_ & NETWORK_ID_MASK) |  ++hostId ;
	}
	else
#endif //defined(SIGMA_SUN)
	{
		if ( clientIpAddr_ )
		{
			if ( PTargetAddr )
			{
				if (( clientIpAddr_ == PTargetAddr->clientIpAddr ) &&
						PTargetAddr->pair )
				{
					clientIpAddr_ = PTargetAddr->clientIpAddr + 1;
				}
				else
				{
					clientIpAddr_ = PTargetAddr->clientIpAddr;
				}
			}
			else
			{
				hostId =  clientIpAddr_ & ~NETWORK_ID_MASK;
				clientIpAddr_ = (clientIpAddr_ & NETWORK_ID_MASK) |  ++hostId ;
			}
		}
		else
		{
			if ( PTargetAddr )
			{
				clientIpAddr_ = PTargetAddr->clientIpAddr;
			}
			else
			{
				clientIpAddr_ = CLIENT_START_IPADDR;
			}
		}
	}

	hostId =  clientIpAddr_ & ~NETWORK_ID_MASK;
	while( loopCount++ < MAX_CONNECTIONS )
	{
		found = FALSE;
		for(i=0; i < MAX_CONNECTIONS; i++)
		{
			if ( connections_[i].opened() &&
				( clientIpAddr_ ==  connections_[i].getClientIp()))
			{
				found = TRUE;
				break;
			}
		}
		if ( found == FALSE )
		{
#if		defined(SIGMA_DEBUG)
{
Uchar *cPtr = (Uchar *) &clientIpAddr_;
printf("Next client Ip address %u.%u.%u.%d\n", cPtr[0], cPtr[1], cPtr[2],cPtr[3]);
}
#endif //defined(SIGMA_DEBUG)
			return(clientIpAddr_);
		}
		clientIpAddr_ = (clientIpAddr_ & NETWORK_ID_MASK) |  ++hostId ;
	}
	return(0);
}
#endif	//defined(SIGMA_SERVER)

#if	defined(SIGMA_SERVER)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: broadcastClientIpAddr_(void)
//
//@ Interface-Description
//    Based on the BOOTP request broadcast packet from a TARGET
//	  machine, this method responds with a BOOTP reply packet
//	  which contains an IP address for a requesting TARGET machine
//    to use. The method make sures it allocates a unique IP
//	  address on the current local network.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If PTargetAddr pointer was set, respond to only to the  target
// 	  which has a same Ethernet address defined in the PTargetAddr
//    structure.
//---------------------------------------------------------------------
//@ PreCondition
//		broadcastSocket_.opened()
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
Download::broadcastClientIpAddr_(void)
{
Uint32 nextIpAddr;
BOOTPMsg respBuf;
Saddr from;

	CLASS_PRE_CONDITION(broadcastSocket_.opened());

	if ((nextIpAddr = getNextClientIpAddr_()) <= 0 )
	{
		ReportError(
		"Download::broadcastClientIpAddr_():getNextClientIpAddr_() Failed to allocate a valid IP address for the next target machine");
		return(FAILURE);
	}

	if ( broadcastSocket_.readDataFrom( &respBuf, sizeof(BOOTPMsg), &from.sa )
					!= sizeof(BOOTPMsg))
	{
		ReportError("Download::broadcastClientIpAddr_():readDataFrom() Failed in reading a BOOTP request from a client", errno);
		return(FAILURE);
	}

	if ( respBuf.opCode !=  BOOTP_REQUEST )
	{
		ReportError("Download::broadcastClientIpAddr_():readDataFrom() BOOTP_REQUEST Unexpected a BOOTP message from a client");
		return(FAILURE);
	}

	if (PTargetAddr)
	{
		// if PTargetAddr is set, serve only the specified target machine.
		if ( respBuf.targetType == BD_BOARD )
		{
			if ( bcmp( (char *) respBuf.EtherAddr,
					(char *) PTargetAddr->bdAddr, ETHERNET_ADDR_SIZE ) != 0 )
			{
				return(SUCCESS);
			}
		}
		else if ( respBuf.targetType == GUI_BOARD )
		{
			if ( bcmp( (char *) respBuf.EtherAddr,
					(char *) PTargetAddr->guiAddr, ETHERNET_ADDR_SIZE ) != 0 )
			{
				return(SUCCESS);
			}
		}
		else
		{
			CLASS_ASSERTION_FAILURE();
		}
	}
	respBuf.opCode =  BOOTP_REPLY;
	respBuf.ClientIpAddr  = nextIpAddr;
	respBuf.ServerIpAddr = serverIpAddr_;
	CLASS_ASSERTION((respBuf.targetType==BD_BOARD)||(respBuf.targetType==GUI_BOARD));

	if ( respBuf.targetType == BD_BOARD )
	{
		strcpy( respBuf.bootFileName, Download::PBdBootFile_ );
	}
	else if ( respBuf.targetType == GUI_BOARD )
	{
		strcpy( respBuf.bootFileName, Download::PGuiBootFile_ );
	}

	if ( broadcastSocket_.broadcastData((char *)&respBuf, sizeof(BOOTPMsg),
		CLIENT_BROADCAST_PORT) != sizeof(BOOTPMsg) )
	{
		ReportError("Download::broadcastClientIpAddr_():broadcastData() Failed in broadcasting a BOOTP reply package", errno);
		return(FAILURE);
	}
	return(SUCCESS);
}
#endif //defined(SIGMA_SERVER)

#if		defined(SIGMA_SERVER)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetBdBootFile(char *pFile)
//
//@ Interface-Description
//		Set the download boot image file name for the BD board.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	 (pFile)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Download::SetBdBootFile(char *pFile)
{
	CLASS_PRE_CONDITION(pFile);

	Download::PBdBootFile_ = pFile;
}
#endif //defined(SIGMA_SERVER)

#if		defined(SIGMA_SERVER)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetGuiBootFile(char *pFile)
//
//@ Interface-Description
//		Set the download boot image file name image for the GUI board.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	 (pFile)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Download::SetGuiBootFile(char *pFile)
{
	CLASS_PRE_CONDITION(pFile);

	Download::PGuiBootFile_ = pFile;
}
#endif //defined(SIGMA_SERVER)

#if		defined(SIGMA_SERVER)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: serverTask(void)
//
//@ Interface-Description
//    This is the main task for the TFTP server side of the Sigma
//	  download program.
//    Until the program gets terminated, the task goes on indefinitely
//    accepting new socket connections from tftp clients and
//    continually serving already opened tftp socket connections.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

extern Int MaxCount;

void
Download::serverTask(void)
{
xsFDSet_t readMask;
Int i, numConnections = 0;
Int downloadCount = 0;


	if (mainSocket_.open( (PortId) DOWNLOAD_SERVER_PORT, MAX_CONNECTIONS) == -1)
	{
		ReportError("Download::serverTask():open() main Socket open failed",
				errno);
		return;
	}

	if ( broadcastSocket_.open(SERVER_BROADCAST_PORT, 0) == -1 )
	{
		ReportError("Download::serverTask():open() broadcast Socket open failed",
				errno);
		return;
	}
	serverIpAddr_ = mainSocket_.getHostIpAddr();
	broadcastSocket_.computeBroadcastAddr(serverIpAddr_);

	while(!CloseCommand)
	{
		FD_ZERO(&readMask);
		FD_SET(mainSocket_.getSocketId(), &readMask);
		FD_SET(broadcastSocket_.getSocketId(), &readMask);
		numConnections = 0;
		for(i=0;i<MAX_CONNECTIONS;i++)
		{
			if ( connections_[i].opened() )
			{
				numConnections++;
				FD_SET(connections_[i].getSocketId(), &readMask);
			}
		}

		if ( WaitForInput(&readMask, WAIT_DATA_INTERVAL )>0 )
		{
			if ( numConnections < MAX_CONNECTIONS )
			{
				if ( FD_ISSET( mainSocket_.getSocketId(), &readMask))
				{
					acceptConnection_();
				}
				if (FD_ISSET( broadcastSocket_.getSocketId(), &readMask))
				{
					broadcastClientIpAddr_();
				}
			}

			for(i=0;i<MAX_CONNECTIONS;i++)
			{
				if ( connections_[i].opened() &&
			 		FD_ISSET( connections_[i].getSocketId(), &readMask))
				{
					if ( connections_[i].doCommand() < 0 )
					{
						numConnections--;
						if ( MaxCount )
						{
							if (( ++downloadCount >= MaxCount ) && ( !numConnections ))
							{
								return;
							}
						}
					}
				}
			}
		}

		for(i=0;i<MAX_CONNECTIONS;i++)
		{
			if ( connections_[i].opened() && connections_[i].isMaxInactive()) 
			{
				connections_[i].close("Client is inactive too long");
				numConnections--;
				if ( MaxCount )
				{
					if (( ++downloadCount >= MaxCount ) && (!numConnections ))
					{
						return;
					}
				}
			}
		}
	}	
	mainSocket_.close();
	broadcastSocket_.close();
	for(i=0;i<MAX_CONNECTIONS;i++)
	{
		if ( connections_[i].opened() )
		{
			connections_[i].close(NULL);
		}
	}
}
#endif //defined(SIGMA_SERVER)

#if	defined(SIGMA_SERVER)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptConnection_(void)
//
//@ Interface-Description
//    Accepts a socket connection from a TFTP client and opens up a
//    TFTP socket connection.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    Each TFTP socket connection maintains the last time this socket
//    received data from the other side. Calling the gotInput method
//    sets the last input time to a current time.
//--------------------------------------------------------------------- 
//@ PreCondition
// 		none
//---------------------------------------------------------------------
//@ PostCondition 
//		none
//@ End-Method 
//===================================================================== 

Int 
Download::acceptConnection_(void)
{
Int newSocket, retry, i;
Saddr sockaddr;
Int namelen;

	retry = DOWNLOAD_COMM_RETRY;
    while(( newSocket = ::accept(mainSocket_.getSocketId(), (struct sockaddr *)0, 0))<= 0)
    {
		if ( --retry < 0 )
		{
			ReportError("Download::acceptConnection_:accept()", errno);
			return(-1);
		}
    }
	for(i=0;i<MAX_CONNECTIONS;i++)
	{
		if ( !connections_[i].opened() )
		{
			connections_[i].setSocketId(newSocket);
			namelen = sizeof(struct sockaddr);
			if ( ::getpeername(newSocket, &sockaddr.sa, &namelen) < 0 )
			{
				ReportError("Download::acceptConnection_:getpeername()", errno);
				return(-1);
			}
			connections_[i].setClientIp((Uint32)sockaddr.in.sin_addr.s_addr);
			connections_[i].gotInput();
			return( newSocket );
		}
	}
	ReportError("Download::acceptConnection_:opened() MAX_CONNECTIONS More than Max Download Socket connection requested\n"); 
	return(-1);
}
#endif //defined(SIGMA_SERVER)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Download::DisplayMessage(char *msg)
//
//@ Interface-Description
//    Displays a TFTP client reported error message on the screen.
//--------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//	  none
//@ End-Method
//=====================================================================
void
Download::DisplayMessage(char *msg)
{
	// $[TI1]
	printf("%s", msg);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Download::DisplayClientError(char *errMsg, Uint error)
//
//@ Interface-Description
//    Displays a TFTP client reported error message.
//	  If this method is called from a server, the error messages
//	  is displayed on a Screen. For a client, the error code is
//	  displayed on the Target LED. 
//--------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//	  none
//@ End-Method
//=====================================================================
void
Download::DisplayClientError(char *errMsg, Uint error )
{
int i;
char tmpStr[200];
char *pStr = NULL ;

	for (i = 0; ErrTbl_[i].msg; i++) // no TI needed
	{
		if  ( error == ErrTbl_[i].code ) // $[TI1]
		{
			pStr =  ErrTbl_[i].msg;
			break;
		} // else $[TI2]
	} // no TI needed

	if ( pStr ) // $[TI3]
	{
		if ( errMsg ) // $[TI3.1]
		{
			sprintf(tmpStr, "%s : %s\n", errMsg, pStr);
		}
		else // $[TI3.2]
		{
			sprintf(tmpStr, "Client reported Error:  %s.\n", pStr);
		}
	}
	else // $[TI4]
	{
		if ( errMsg ) // $[TI4.1]
		{
			sprintf(tmpStr, "%s : unrecognized Error code 0x%X\n", errMsg, error );
		}
		else // $[TI4.2]
		{
			sprintf(tmpStr,
				"Client reported unrecognized Error code:  0x%X.\n", error );
		}
	}
#if	  defined(SIGMA_SERVER)
	DisplayMessage(tmpStr);

#elif defined(SIGMA_CLIENT)

#if	defined(SIGMA_DEVELOPMENT)
	printf("%s", tmpStr);
#endif //defined(SIGMA_DEVELOPMENT)

#endif //defined(SIGMA_CLIENT)
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Download::ReportError(char *errmsg, int error)
//
//@ Interface-Description
//    Displays an error message along with the error code to an 
//    appropriate medium.
//--------------------------------------------------------------------
//@ Implementation-Description
//	  none.
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//	  none
//@ End-Method
//=====================================================================
void
Download::ReportError(char *errmsg,  int error )
{
char tmpStr[200];

	if ( error ) // $[TI1]
	{
		sprintf(tmpStr,"%s : error code %d.\n", errmsg, error);
	}
	else // $[TI2]
	{
		sprintf(tmpStr,"%s.\n", errmsg);
	}

#if	defined(SIGMA_SERVER)
	DisplayMessage(tmpStr);
#endif //defined(SIGMA_SERVER)
	
#if	(defined(SIGMA_DEVELOPMENT) && defined(SIGMA_CLIENT)) || defined(SIGMA_UNIT_TEST)
	printf("\nDownload Error: %s errno: %d.\n", errmsg, error);
#endif //(defined(SIGMA_DEVELOPMENT) && defined(SIGMA_CLIENT)) || defined(SIGMA_UNIT_TEST)
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Download::ReportError( char *format, char *errmsg  )
//
//@ Interface-Description
//    Format the error message and report it to the system.
//--------------------------------------------------------------------
//@ Implementation-Description
//    After formatting the error message, pass it along to the
//	  ReportError(char *msg, Int errCode) method.
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//	  none
//@ End-Method
//=====================================================================
void
Download::ReportError(char *format, char *errmsg )
{
char tMsg[256];

	// $[TI1]
	sprintf(tMsg,format,errmsg);
	ReportError(tMsg, 0);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  WaitForInput(xsFDSet_t *pReadMask, const Int32 waitDuration)
//
//@ Interface-Description
//    This method is called to check if there is any incoming network 
//    data or a socket connection request.
//    If nothing arrives during the given "timeDuration" milli seconds,
//    a zero value is returned by the method. 
//    When there is input data or a socket connection attempt on any of
//    sockets set in the 'readMask', it returns the number of ready sockets.
//    On return, readMask reflects the socket bit mask of the ready sockets. 
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    A passed in socket is an blocked I/O socket, therefore the
//    'select'  network call will not return until either some input
//    comes in on the specified sockets or a given time duration passes. 
//    when a wait time duration is zero, the select call returns -1
//	  with the errno set to EWOULDBLOCK. If a given wait time duration
//    is non-zero, the select call returns 0 when it times out.
//--------------------------------------------------------------------- 
//@ PreCondition
//      pReadMask != NULL
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
Download::WaitForInput(xsFDSet_t *pReadMask, const Int32 waitDuration)
{
Int retry;
#ifdef SIGMA_SERVER
struct timeval waitTime;
#elif  SIGMA_CLIENT
xsInt32_t  waitTime = waitDuration;
#endif  //SIGMA_CLIENT
Int nready;

    CLASS_PRE_CONDITION(pReadMask != NULL);

    retry = DOWNLOAD_COMM_RETRY;
#if  defined(SIGMA_SUN)
	waitTime.tv_sec = waitDuration/ONE_SECOND;
	waitTime.tv_usec = waitDuration%ONE_SECOND;
#elif defined(SIGMA_TARGET )
    waitTime = waitDuration; 
#endif // defined(SIGMA_TARGET)

	while((nready = ::select(XS_NUM_FDS, pReadMask, NULL, NULL, &waitTime ))<= 0 ) // $[TI1]
	{
		// timeout 
		if ( ( nready == 0 ) || (errno == EWOULDBLOCK ) ) // $[TI1.1]
		{
			return(0);
		}
		else // $[TI1.2]
		{
			// communication error.
			if (retry--  <=   0) // $[TI1.2.1]
			{
				ReportError("Download::WaitForInput():select()", errno);
				return(-1);
			} // else $[TI1.2.2]
		}
	} // $[TI2]

	return(nready);
}

#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetInitialized(Boolean value)
//
//@ Interface-Description
//		Set the download initialized to the specified value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	 (pFile)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Download::SetInitialized(Boolean value)
{
	Download::Initialized_ = value;
}
#endif // SIGMA_UNIT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
Download::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  		DownloadFault::SoftFault(softFaultID, DOWNLOAD,
		DOWNLOAD_CLASS, lineNumber, pFileName, pPredicate);
}
