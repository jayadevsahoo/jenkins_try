# Takes a list of untranslated symbols and the current english (with the comments
# stripped from it) and outputs the english symbol and cheaptext for all the
# untranslated symbols.

# Load the array of untranslated symbols from the file ARGV[1]
BEGIN{
  arraySize = 1
  numOfUntranslatedSymbols = 1
  while (getline untranslatedLine <ARGV[1] > 0)
  {
    arrayOfUntranslatedSymbols[numOfUntranslatedSymbols] = untranslatedLine
    numOfUntranslatedSymbols++
  }
}

# Load an array with the english. Each array item contains the entire symbol name
# and cheaptext for the english.
{
	while (getline <ARGV[2] > 0)
	{
		line = $0
		while ( ($0 !~/";$/) && ($0 !~/" ;$/) && (getline <ARGV[2] > 0))
		{
			line = line "\n" $0 
		}
		StringIdArrayEnglish[arraySize] = line
		arraySize++
	}
}

# Loop through the untranslated symbols and when found in english, output the english
# symbol/cheaptext.
END {
  for (i=1;i<numOfUntranslatedSymbols;i++)
  {
    for (j=1;j<arraySize;j++)
    {
      pos = index(StringIdArrayEnglish[j], arrayOfUntranslatedSymbols[i])
      if (pos !=0)
      {
	print StringIdArrayEnglish[j] 
	j = arraySize + 1
      }
    }
  }
}
