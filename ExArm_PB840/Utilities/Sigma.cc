#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  Sigma - Standard System-Wide Declarations.
//---------------------------------------------------------------------
//@ Interface-Description
//  This declares the system-wide types, constants, and macros.  This
//  provides a system-wide header file ('Sigma.hh') that, itself,
//  includes the header files containing the standard type, constants,
//  call-tracing macros, and fault-handling mechanisms.
//---------------------------------------------------------------------
//@ Rationale
//  Central place for all of the type, constant, and macro definitions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The following files are included by 'Sigma.hh':  'SigmaTypes.hh',
//  'CompilerDirectives.hh', 'SigmaConstants.hh', 'FaultHandlerMacros.hh',
//  and 'NewOperator.hh'.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/Sigma.ccv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...
