#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OperationalTimeSubScreen - The screen selected by pressing the
// Operational Time Record button on the Upper Other Screens Subscreen.
// Displays operational times of the major ventilator system modules
// (GUI, vent head, and compressor).
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/OperationalTimeSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: gdc   Date:  19-Jul-2006    DR Number: 6115
//  Project:  RESPM
//  Description:
//      Updating value of system operational hours and compressor hours
//      regardless of the visibility state of sub-screen. Unnecessary 
//      for data with update frequency less than once per minute.
//
//  Revision: 008   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//
//  Revision: 007  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 006  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 005  By:  yyy    Date:  11-AUG-1997    DCS Number: 2402
//  Project:  Sigma (R8027)
//  Description:
//      Call novRamUpdateEventHappened() to display the initial operational time.
//
//  Revision: 004  By:  yyy    Date:  11-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Removed NovRamEventRegistrar::RegisterTarget() call from activate()
//		to constructor.
//
//  Revision:  003  By: yyy   Date:  20-May-1997      DR Number: DCS 2151
//    Project:    Sigma (R8027)
//    Description:
//      Registered the compressor and operational time with NovRam update
//		manager to enable an realtime update of the hours.
//
//  Revision: 002  By: yyy      Date: 13-May-1997  DR Number: 2077
//    Project:  Sigma (R8027)
//    Description:
//      Move the x coordinate for the label 50 pixel to the left to avoid
//		of operlapping of the display.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "OperationalTimeSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "UpperSubScreenArea.hh"
#include "UpperScreen.hh"
#include "ServiceUpperScreen.hh"
//@ End-Usage

#include "NovRamManager.hh"

//FORNOW
#include "ServiceUpperScreen.hh"
#include "ServiceUpperScreenSelectArea.hh"
#include "NovRamUpdateManager.hh"
#include "NovRamEventRegistrar.hh"
//@ Code...

static const Int32 VALUE_WIDTH_=50;
static const Int32 VALUE_HEIGHT_=50;
static const Int32 COL_X1_=100;
static const Int32 COL_X2_=390;
static const Int32 COL_X3_=450;
static const Int32 ROW_Y1_=100;
static const Int32 ROW_Y2_=150;
static const Int32 ROW_Y1_VALUE_=89;
static const Int32 ROW_Y2_VALUE_=139;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OperationalTimeSubScreen()  [Constructor]
//
//@ Interface-Description
//	Constructs a OperationalTimeSubScreen.  It is passed a pointer to the 
//	SubScreenArea.	
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Initializes data members; sets position, size and color of the upper
//	subscreen area; 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OperationalTimeSubScreen::OperationalTimeSubScreen(SubScreenArea* pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleArea_(MiscStrs::OPER_TIME_SUBSCREEN_TITLE, SubScreenTitleArea::SSTA_CENTER),
	compressorOperationalHoursLabel_(MiscStrs::COMPRESSOR_OPERATIONAL_HOURS_LABEL),
	compressorOperationalHoursUnit_(MiscStrs::COMPRESSOR_OPERATIONAL_HOURS_UNIT),
	compressorOperationalHoursValue_(TextFont::TWELVE, 6, RIGHT),
	systemOperationalHoursLabel_(MiscStrs::SYSTEM_OPERATIONAL_HOURS_LABEL),
	systemOperationalHoursUnit_(MiscStrs::SYSTEM_OPERATIONAL_HOURS_UNIT),
	systemOperationalHoursValue_(TextFont::TWELVE, 6, RIGHT)
{
	CALL_TRACE("OperationalTimeSubScreen::OperationalTimeSubScreen(pSubScreenArea)");
	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	// Display the screen title
	addDrawable(&titleArea_);

	// Set color, place, and add field labels.
	compressorOperationalHoursLabel_.setColor(Colors::WHITE);
	compressorOperationalHoursLabel_.setX(COL_X1_);
	compressorOperationalHoursLabel_.setY(ROW_Y1_);
	addDrawable(&compressorOperationalHoursLabel_);

	compressorOperationalHoursUnit_.setColor(Colors::WHITE);
	compressorOperationalHoursUnit_.setX(COL_X3_);
	compressorOperationalHoursUnit_.setY(ROW_Y1_);
	addDrawable(&compressorOperationalHoursUnit_);

	systemOperationalHoursLabel_.setColor(Colors::WHITE);
	systemOperationalHoursLabel_.setX(COL_X1_);
	systemOperationalHoursLabel_.setY(ROW_Y2_);
	addDrawable(&systemOperationalHoursLabel_);

	systemOperationalHoursUnit_.setColor(Colors::WHITE);
	systemOperationalHoursUnit_.setX(COL_X3_);
	systemOperationalHoursUnit_.setY(ROW_Y2_);
	addDrawable(&systemOperationalHoursUnit_);

	// Set color, place, and add numeric fields.
	compressorOperationalHoursValue_.setPrecision(ONES);
	compressorOperationalHoursValue_.setX(COL_X2_);
	compressorOperationalHoursValue_.setY(ROW_Y1_VALUE_);
	compressorOperationalHoursValue_.setWidth(VALUE_WIDTH_);
	compressorOperationalHoursValue_.setHeight(VALUE_HEIGHT_);
	compressorOperationalHoursValue_.setColor(Colors::WHITE);
	addDrawable(&compressorOperationalHoursValue_);

	systemOperationalHoursValue_.setPrecision(ONES);
	systemOperationalHoursValue_.setX(COL_X2_);
	systemOperationalHoursValue_.setY(ROW_Y2_VALUE_);
	systemOperationalHoursValue_.setWidth(VALUE_WIDTH_);
	systemOperationalHoursValue_.setHeight(VALUE_HEIGHT_);
	systemOperationalHoursValue_.setColor(Colors::WHITE);
	addDrawable(&systemOperationalHoursValue_);

	// Register all logs with NovRam for receiving event updates
	NovRamEventRegistrar::RegisterTarget(
				NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE, this);
	NovRamEventRegistrar::RegisterTarget(
				NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE, this);
														//	$[TI1]
}
                                                                      

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OperationalTimeSubScreen  [Destructor]
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OperationalTimeSubScreen::~OperationalTimeSubScreen(void)
{
	CALL_TRACE("OperationalTimeSubScreen::~OperationalTimeSubScreen(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()
//
//@ Interface-Description
// 	Activate the OperationalTimeSubScreen
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the screen title.  Retrieve compressorOperationalHours and
//  systemOperationalHours and set the values.
// $[01207] The operational time record subscreen shall display ...
// $[01208] All operational times shall be displayed in hours
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationalTimeSubScreen::activate(void)
{
	CALL_TRACE("OperationalTimeSubScreen::activate(void)");

	novRamUpdateEventHappened(NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE);
	novRamUpdateEventHappened(NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE);
															//	$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationalTimeSubScreen::deactivate(void)
{
	CALL_TRACE("OperationalTimeSubScreen::deactivate(void)");

	//FORNOW -- do nothing
}															//	$[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: novRamUpdateEventHappened
//
//@ Interface-Description
// Called by NovRam manager, this function updates the display with
// the latest, real-time data in the NovRam.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the event is SYSTEM_OPER_TIME_UPDATE, then we get the system operational
// hours value from NovRam and set the value field.
// If the event is COMPRESSOR_OPER_TIME_UPDATE, then we get the compressor
// operational hours value from NovRam and set the value field.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
OperationalTimeSubScreen::novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId updateTypeId)
{
	CALL_TRACE("OperationalTimeSubScreen::novRamUpdateEventHappened(updateTypeId)");

		Uint32 compressorOperationalHours;
		Uint32 systemOperationalHours;

		switch (updateTypeId)
		{ 														// $[TI1.1]
		case NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE: 		// $[TI1.1.1]
			systemOperationalHours = NovRamManager::GetSystemOperationalHours();
			systemOperationalHoursValue_.setValue((Real32) systemOperationalHours);
			break;
		case NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE: 	// $[TI1.1.2]
			compressorOperationalHours = NovRamManager::GetCompressorOperationalHours();
			compressorOperationalHoursValue_.setValue((Real32) compressorOperationalHours);
			break;
		default: 												// $[TI1.1.3]
			break;
		}

}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
OperationalTimeSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, OPERATIONALTIMESUBSCREEN,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

