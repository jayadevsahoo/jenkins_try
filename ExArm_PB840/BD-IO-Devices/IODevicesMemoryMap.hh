#ifndef IODevicesMemoryMap_HH
#define IODevicesMemoryMap_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@Struct: IODevicesMemoryMap - Memory map structures.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/IODevicesMemoryMap.hhv   25.0.4.0   19 Nov 2013 13:54:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.  Removed Sigma.hh, not needed.
//
//  Revision: 002  By:  syw   Date:  25-Jan-1996    DR Number: DCS 562
//      Project:  Sigma (R8027)
//      Description:
//			Renamed IODevicesMemoryMap to AIIOMap.  Renamed readWriteEeproms
//			to serialReadWriteEeproms.  Defined CpuIOMap.  Removed unused
//			structures.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

struct AIIOMap
{
	Uint16 spare1 ;
	Uint16 spare2 ;
	Uint16 serialReadWriteEeproms ;	// SERIAL_DEVICE_WRITE, SERIAL_DEVICE_READ
	Uint16 readIOPort ;				// SYSTEM_STATUS_READ
	Uint16 adcCounterPort ;			// ADC_OPERATIONS_COUNTER
	Uint16 spare3 ;
	Uint16 spare4 ;
	Uint16 writeIOPort ;			// DISCRETE_DIO
	Uint16 resyncPort ;
	Uint16 delayPort ;				// DELAY_COMMAND, DELAY_VALUE_REGISTER
	Uint16 freezePort ;
	Uint16 repeatPort ;
	Uint16 spare5 ;
	Uint16 softResetPort ;
	Uint16 spare6 ;
	Uint16 spare7 ;
	Uint16 airPsolDacPort ;
	Uint16 o2PsolDacPort ;
	Uint16 exhValveDacPort ;
	Uint16 eVDampingGainDacPort ;
	Uint16 dacWrapDacPort ;
	Uint16 metabolicsDacPort ;
	Uint16 spare8 ;
	Uint16 spare9 ;
	Uint16 buffer[8] ;
	Uint16 startAdcIo ;
} ;

struct CpuIOMap
{
	Uint16 compressorReadWritePort ;
	Uint16 spare1 ;
	Uint16 powerSupplyReadWritePort ;
	Uint16 spare2 ;
	Uint16 safetyNetControlAWritePort ;
	Uint16 safetyNetControlBWritePort ;
	Uint16 safetyNetStatusReadPort ;
	Uint16 spare3 ;
	Uint16 ventControlWritePort ;
} ;

struct AdcRead
{
	Uint16 counts : 12 ;
} ;


#endif // IODevicesMemoryMap_HH




