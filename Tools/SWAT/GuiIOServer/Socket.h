#include "TcpSocket.h"
#include "UdpSocket.h"

#ifdef TCP_COMMUNICATION
#define Socket TcpSocket
#else
#define Socket UdpSocket
#endif