#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CompensationBasedPhase - the base class for TcvPhase and PavPhase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class BreathPhase.  Two instances
//		of this class can exist (TcvPhase and PavPhase).  Only one instance can
//		be active at any given time.  
//
//		Virtual methods are implemented to initialize the phase, to perform
//		TC or PAV activities each controlled BD cycle, to allow for
//		internal clean up and graceful termination of the phase, access
//		methods to get and compute the resistive and elastance targets
//		along with the percentSupport level.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms for TCV and PAV inspirations
//---------------------------------------------------------------------
//@ Implementation-Description
//		Both TC and PA perform the same except for the trajectory calculations,
//		thus pure virtual functions (determineResistivePressure_() and
//		determineElasticPressure_()) are provided to distinguish the two
//		types of inspirations.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  	Only two instances are allowed
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/CompensationBasedPhase.ccv   10.7   08/17/07 09:35:46   pvcs  
//
//@ Modification-Log
//
//  Revision: 005  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software. 
// 
//  Revision: 004   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 003   By: gfu   Date:  27-July-2004    DR Number: 6137
//  Project:  PAV
//  Description:
//      Modified module to remove Pcomp alarm in PAV per SCDR #6137.  Based upon input from the 
//      Field Evaluations in Canada, the following PAV specific alarm system changes are needed:
//      Eliminate the Pcomp alarm, but retain the Pcomp limit in the PAV control algorithm.
//
//  Revision: 002   By: syw   Date:  21-Jun-2000    DR Number: 5795
//  Project:  PAV
//  Description:
//		Trace PA24011
//
//  Revision: 001   By: syw   Date:  21-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		Initial release.
//
//=====================================================================

#include "CompensationBasedPhase.hh"


#include "TriggersRefs.hh"
#include "ControllersRefs.hh"
#include "MainSensorRefs.hh"
#include "BreathMiscRefs.hh"
#include "ModeTriggerRefs.hh"
#include "ValveRefs.hh"

//@ Usage-Classes

#include "FlowController.hh"
#include "PidPressureController.hh"
#include "ExhValveIController.hh"
#include "PressureSensor.hh"
#include "HighCircuitPressureExpTrigger.hh"
#include "LungFlowExpTrigger.hh"
#include "Peep.hh"
#include "ExhalationValve.hh"
#include "BdAlarms.hh"
#include "PhasedInContextHandle.hh"
#include "LungData.hh"
#include "Tube.hh"
#include "DisconnectTrigger.hh"
#include "TimerBreathTrigger.hh"
#include "HighPressCompExpTrigger.hh"

//@ End-Usage

//@ Code...

//@ Constant: MIN_SUPPORT_PRESSURE
// The minimum peep pressure to be maintained
extern const Real32 MIN_SUPPORT_PRESSURE;

//@ Constant: PRESSURE_THRESHOLD
// pressure where safety valve anti cracking algorithm is engaged
extern const Real32 PRESSURE_THRESHOLD ;

static const Int32  MAX_PEEP_LOOP_COUNT = 100 / 5;	// CYCLE_TIME_MS;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CompensationBasedPhase()  
//
//@ Interface-Description
//		Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Register callback with O2Mixture and initialize data members.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

CompensationBasedPhase::CompensationBasedPhase(void)
 : BreathPhase(BreathPhaseType::INSPIRATION)
{
	CALL_TRACE("CompensationBasedPhase::CompensationBasedPhase(void)") ;

	// $[TI1]
	newBreath() ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CompensationBasedPhase()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

CompensationBasedPhase::~CompensationBasedPhase(void)
{
	CALL_TRACE("CompensationBasedPhase::~CompensationBasedPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
CompensationBasedPhase::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, COMPENSATIONBASEDPHASE,
                          lineNumber, pFileName, pPredicate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called to control the PSOLS and exhalation valve each TC or PAV
//		cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Lung flow and volume data are obtained and limits are computed.
//		The resistive and elastic components of the target are calculated
//		and limitted.  The target is then used by the PidPressureController
//		and ExhValveIController to determine the valve commands.
// $[PA24003]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
CompensationBasedPhase::newCycle( void)
{
	CALL_TRACE("CompensationBasedPhase::newCycle( void)") ;

	Real32 lungFlow = RLungData.getLungFlow() ;		// L/min	
	Real32 inspBtpsFactor = Btps::GetInspBtpsCf() ;

	if (inspBtpsFactor > 0.0)
	{
	  	// $[TI1]
		lungFlow /= inspBtpsFactor ;
	}
  	// $[TI2]
	
	Real32 lungVolume = RLungData.getInspLungBtpsVolume() ;  // ml

	// $[PA24007]
	// $[PA24008]
	Real32 elasticVolumeThreshold = 0.75F *
				PhasedInContextHandle::GetBoundedValue(SettingId::HIGH_INSP_TIDAL_VOL).value ;

	const Real32 IBW_VALUE =
				PhasedInContextHandle::GetBoundedValue(SettingId::IBW).value ;

	const Real32 MIN_THRESHOLD = 5.0F * IBW_VALUE ;
	const Real32 MAX_THRESHOLD = 15.0F * IBW_VALUE ;
	
	if (elasticVolumeThreshold < MIN_THRESHOLD)
	{
	  	// $[TI3]
		elasticVolumeThreshold = MIN_THRESHOLD ;
	}
	else if (elasticVolumeThreshold > MAX_THRESHOLD)
	{
	  	// $[TI4]
		elasticVolumeThreshold = MAX_THRESHOLD ;
	}
  	// $[TI5]
	
	if (lungVolume > elasticVolumeThreshold)
	{
	  	// $[TI6]
		lungVolume = elasticVolumeThreshold ;
	}
  	// $[TI7]

	static Real32 peepTarget = 0;

	if (loopCount_ < MAX_PEEP_LOOP_COUNT)
	{
	  	// $[TI8]
		loopCount_++;
		peepTarget = startingPeep_ +
					 ((peepTarget_ - startingPeep_) * loopCount_) /
					  MAX_PEEP_LOOP_COUNT;
		AUX_CLASS_PRE_CONDITION(loopCount_ <= MAX_PEEP_LOOP_COUNT &&
								loopCount_ >= 1, loopCount_) ;
	}
  	// $[TI9]
	
	// $[PA24004]
	determineResistivePressure_( lungFlow) ;
	determineElasticPressure_( lungVolume) ;
	
	// $[PA24009]

	Real32 temp ;
	
	if (totalTrajLimit_ > 45.0)
	{
	  	// $[TI10]
		temp = 45.0F - peepTarget ;
	}
	else
	{
	  	// $[TI11]
		temp = totalTrajLimit_ - peepTarget ;
	}
	const Real32 MIN_ELASTIC_PRESSURE = 0.0 ;
	const Real32 MAX_ELASTIC_PRESSURE = MIN_VALUE( 30.0F, temp) ;

	if (elasticPressure_ < MIN_ELASTIC_PRESSURE)
	{
	  	// $[TI12]
		elasticPressure_ = MIN_ELASTIC_PRESSURE ;
	}
	else if (elasticPressure_ > MAX_ELASTIC_PRESSURE)
	{
	  	// $[TI13]
		elasticPressure_ = MAX_ELASTIC_PRESSURE ;
	}
  	// $[TI14]

	// $[PA24010]
	if (totalTrajLimitReached_)
	{
	  	// $[TI15]
		elasticPressure_ = frozenElasticPressure_ ;
	}
  	// $[TI16]
	
	//	$[TC04012]
	// $[PA24004]
	Real32 target = resistivePressure_ + elasticPressure_ + peepTarget ;

	// Filter target to obtain the desiredPressure
	Real32 alpha = RLungData.getLungAlpha() ;
	//	$[TC04013] 
	// $[PA24005]
	desiredPressure_ = (1.0F - alpha) * target + alpha * desiredPressure_ ;

	// To avoid of circuit disconnect being declared due to the targeted
	// pressure is below 1.5 (MIN_SUPPORT_PRESSURE)
	// $[PA24006]
	desiredPressure_ = MAX_VALUE(desiredPressure_, peepTarget + MIN_SUPPORT_PRESSURE);

	//	$[TC04006]
	// $[TC05007]
	// $[PA24006]
	if (desiredPressure_ > totalTrajLimit_)
	{
	  	// $[TI17]
		// Enable totalTrajLimitReached_ only once 
		if (!totalTrajLimitReached_)
		{
		  	// $[TI18]
			//	$[TC04030] d	set totalTrajLimitReached_ to FALSE
			//	$[TC04004] 		set totalTrajLimitReached_ to TRUE
			totalTrajLimitReached_ = TRUE;
			RLungData.setTotalTrajLimitReached(totalTrajLimitReached_);

			// Declare alarm if in TC only
			if (PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE) ==
				SupportTypeValue::ATC_SUPPORT_TYPE)
			{
				RBdAlarms.postBdAlarm(BdAlarmId::BDALARM_COMPENSATION_LIMIT);
			}

			RExhValveIController.setTotalTrajLimitReached();
			frozenElasticPressure_ = elasticPressure_ ;
		}
	  	// $[TI19]
		
		desiredPressure_ = totalTrajLimit_ ;
		RHighPressCompExpTrigger.setHighPressCompLimitMet( TRUE) ;
	}
	else
	{
	  	// $[TI20]
		RHighPressCompExpTrigger.setHighPressCompLimitMet( FALSE) ;
	}

	// Setup the relief flow for the exhalation valve during inspiration
	Real32 desiredReliefFlow = 0.0f;
	Boolean newController = FALSE;
	Real32 factor = 1.0f;
	
	if (totalTrajLimitReached_)
	{
	  	// $[TI21]
		desiredReliefFlow = 5.0F;
	}
	else
	{
	  	// $[TI22]
		//	$[TC04030] a
		// Calculate factor, factorConditionMet so the ExhValveIController
		// can use these data to calculate flowControlOutput and trajectory
		// later.
		//	$[TC04030] e
		Real32 lungFlowLimit = RLungData.getLungFlowLimit();
		Real32 peakLungFlow = RLungData.getPeakInspLungFlow();
		newController = TRUE;
		
		//	$[TC04030] b
		if (peakLungFlow > 0.0f && lungFlow > lungFlowLimit)
		{
		  	// $[TI23]
			factor = lungFlow / peakLungFlow;

			//	$[TC04030] c
			if (factor < 0.25 && elapsedTimeMs_ > 100)
			{
			  	// $[TI24]
				RExhValveIController.setFactorConditionMet();
			}
		  	// $[TI25]
		}
	  	// $[TI26]
	}
	
	enableExhalationTriggers_() ;
	updateFlowControllerFlags_() ;

	// to setup the flowControlOutput and the calculation of the
	// trajectory
	// $[PA24012]
   	RExhValveIController.updateExhalationValve( desiredPressure_, desiredReliefFlow,
   												newController, factor) ;

	RHighCircuitPressureExpTrigger.updateTargetPressure(desiredPressure_);

	Real32 pressure = getFeedbackPressure_() ;
	
	//	$[TC04016] 
	//	$[TC04017] 
	// $[PA24011]
		
	// Calculate pressureError based on errorGain
	//	$[TC04015]
	Real32 pressureError = calculatePressureError_(desiredPressure_, pressure, errorGain_);

	RPidPressureController.updatePsol(pressureError, elapsedTimeMs_);

	// Lung flow trigger needs to know peakDesiredFlow, so set this up
	RLungFlowExpTrigger.setDesiredFlow( RAirFlowController.getDesiredFlow()
											 + RO2FlowController.getDesiredFlow()) ;
	RDisconnectTrigger.setDesiredFlow( RAirFlowController.getDesiredFlow()
											 + RO2FlowController.getDesiredFlow()) ;
	
	elapsedTimeMs_ += CYCLE_TIME_MS;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
// 		This method has no arguments and has no return value.  This method is
// 		called when inspiration starts.
//---------------------------------------------------------------------
//@ Implementation-Description
//		It initializes data members and request RPidPressureController
//		and RExhValveIController to initialize their data members.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
CompensationBasedPhase::newBreath( void)
{
	CALL_TRACE("CompensationBasedPhase::newBreath( void)") ;

	totalTrajLimitReached_ = FALSE;

   	highCircuitPressure_ = PhasedInContextHandle::GetBoundedValue( SettingId::HIGH_CCT_PRESS).value ;
	//	$[TC04006]

	Real32 PCOMP_OFFSET = 5.0F ;
	totalTrajLimit_ = highCircuitPressure_ - PCOMP_OFFSET ; // Pcomp alarm limit to HIP - 5

	//	$[TC04014] 
	errorGain_ = 1.0f;
	
	// $[PA24011]
	wf_ = 0.0f;
	Real32 exhPress = RExhPressureSensor.getValue();
	desiredPressure_ = RPeep.getActualPeep() + MIN_SUPPORT_PRESSURE;
	peepTarget_ = RPeep.getActualPeep();
	
	if (RPeep.isPeepChange())
	{
	  	// $[TI1]
		startingPeep_ = exhPress;
	}
	else
	{
	  	// $[TI2]
		startingPeep_ = peepTarget_;
	}

    RPeep.setPeepChange( FALSE) ;
    loopCount_ = 0;

	tubeId_ = RTube.getId();
	updatePercentSupport_() ;	

	//	$[TC04018] 
	Real32 k3 = (12.0F - 6.0F * percentSupport_) * (MIN_SUPPORT_PRESSURE -0.1F * tubeId_);

	if (k3 > 12.0)
	{
	  	// $[TI3]
		k3 = 12.0;
	}
	else if (k3 < 3.0)
	{
	  	// $[TI4]
		k3 = 3.0;
	}
  	// $[TI5]
	
	Real32 pressureError = calculatePressureError_(desiredPressure_, exhPress, errorGain_);

	const Real32 k1 = 0.2 ;
	const Real32 k2 = 1.0 ;

	//	$[TC04024]
	Real32 desiredFlow = RAirFlowController.getDesiredFlow() + RO2FlowController.getDesiredFlow() ;
	
	// $[PA24011]
	RPidPressureController.newBreath( desiredFlow,
									 k1, k2, k3, tubeId_ / 10.0F, pressureError);

	// $[PA24012]
    RExhValveIController.newBreath(totalTrajLimit_);
	AdiabaticFactor_ = 1.0 ;

	elapsedTimeMs_ = 0 ;
	RLungData.setTotalTrajLimitReached(totalTrajLimitReached_);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
//		This method has no arguments and has no return value. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
CompensationBasedPhase::relinquishControl(  const BreathTrigger& trigger)
{
	CALL_TRACE("CompensationBasedPhase::relinquishControl(  const BreathTrigger& trigger)") ;

	// Do nothing, but is required for pure virtual method
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePercentSupport_()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method
//		is called to update the percent support value with the setting
//		during TC.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Obtain the percentSupport_ from PhasedInContextHandle
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
CompensationBasedPhase::updatePercentSupport_( void)
{
	CALL_TRACE("CompensationBasedPhase::updatePercentSupport_( void)") ;

  	// $[TI1]
  	
	percentSupport_ = PhasedInContextHandle::GetBoundedValue( SettingId::PERCENT_SUPPORT).value / 100.0F ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableExhalationTriggers_()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to enable the triggers that will terminate inspiration.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The relavent triggers are enabled on the very first cycle.  This
//		method must be called BEFORE elapsedTimeMs_ is updated in newCycle().
// $[04034] Enable High ventilator pressure trigger
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
CompensationBasedPhase::enableExhalationTriggers_( void)
{
	CALL_TRACE("CompensationBasedPhase::enableExhalationTriggers_( void)") ;
	
	if (elapsedTimeMs_ == 0)
	{
	  	// $[TI1]
		Real32 ibw = PhasedInContextHandle::GetBoundedValue( SettingId::IBW).value ;

		Uint32 maxInspTimeMs = 1990 + (Int32)(ibw * 20.0F) ;

		((BreathTrigger&)RBackupTimeExpTrigger).enable();

		RBackupTimeExpTrigger.restartTimer( maxInspTimeMs ) ;
		RDisconnectTrigger.setTimeLimit( maxInspTimeMs ) ;

		RDisconnectTrigger.setFlowCmdLimit( RPidPressureController.getFlowCommandLimit()) ;

        ((BreathTrigger&)RLungFlowExpTrigger).enable() ;
        ((BreathTrigger&)RLungVolumeExpTrigger).enable() ;
		((Trigger&)RHighVentPressureExpTrigger).enable() ;
		((Trigger&)RHighCircuitPressureExpTrigger).enable() ;
		((Trigger&)RHighPressCompExpTrigger).enable() ;
	}
  	// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFeedbackPressure_()
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called to determine the feedback pressure used for control.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The errorGain_ and wf_ data members are manipulated depending on
//		the specified conditions.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
CompensationBasedPhase::getFeedbackPressure_( void)
{
	CALL_TRACE("CompensationBasedPhase::getFeedbackPressure_( void)") ;

	// $[PA24011]
	Real32 exhPress = RExhPressureSensor.getValue() ;
	Real32 inspPress = RInspPressureSensor.getValue() ;

	// $[PA24001]
	Real32 result1 = (inspPress - PRESSURE_THRESHOLD) * errorGain_ ;
	Real32 result2 = exhPress - desiredPressure_ ;

	// $[PA24002]
	if (inspPress > PRESSURE_THRESHOLD && result1 > result2)
	{
	  	// $[TI1]
		errorGain_ = 0.1 ;
		wf_ = 1.0f;
	}
	else if (exhPress > desiredPressure_ &&	result1 <= result2)
	{
	  	// $[TI2]
		errorGain_ = 1.0 ;
		wf_ = 0.0f;
	}
  	// $[TI3]
	
	return( inspPress * wf_ + exhPress * (1.0F - wf_)) ;
}
	
	




