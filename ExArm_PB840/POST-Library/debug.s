                  IFDEF  SIGMA_DEBUG      ;{
;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================
;
;
;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: debug.s - utilities for debugging (and Unit Test) of POST 
;
;---------------------------------------------------------------------
;@ Interface-Description
;  This contains the defintions required by the POST code.
;
;@ Implementation-Description
;
;  This module contains utility routines that assist in the of modules in POST.
;
;@ Restrictions
;  
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
;
;@(#) $Header:   /840/Baseline/POST-Library/vcssrc/debug.s_v   25.0.4.0   19 Nov 2013 14:16:38   pvcs  $
;
;@ Modification-Log
;
;  Revision: 002    By: Gary Cederquist  Date: 16-MAY-1997  DR Number: 2121
;    Project: Sigma (R8027)
;    Description:
;       Removed assembler options specified in source in favor of command
;       line options.
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================
;

                  INCLUDE  kernel.inc
                  INCLUDE  debug.inc

                  XDEF  DebugSendHexByte
                  XDEF  DebugSendHexWord
                  XDEF  DebugSendHexLong
                  XDEF  _VBR__Fv

;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: DebugSendHexByte - send a hex decoding of a byte to the debug port
;
;@ Interface-Description
;
;  This module decodes the specified data into hex digits and sends them to
;  the debugging port.
;
;    Inputs:  d0 - the byte to send
;
;    Outputs: NONE  
;
;---------------------------------------------------------------------
;@ Implementation-Description
;
;
;  REGISTER USAGE: d1 - saved on the stack and restored on exit
;
;---------------------------------------------------------------------
;@ PreCondition
;    None
;---------------------------------------------------------------------
;@ PostCondition
;    None
;
;@ End-Free-Function
;=====================================================================
               SECTION  code,,C

DebugSendHexByte:
               move.w   d1,-(sp)      ; freeup d1 as a scratch register
               move.b   d0,d1
               asr.b    #4,d1          ; top nibble first
               andi.b   #$f,d1
               add.b    #48,d1         ; offset to '0'
               cmp.b    #57,d1         ; > '9'?
               ble.s    no_offset0
               add.b    #7,d1          ;if so, offset to 'A'
no_offset0:    DEBUG_SEND_CHAR   d1 
               move.b   d0,d1
               andi.b   #$f,d1         ; now bottom nibble
               add.b    #48,d1         ; offset to '0'
               cmp.b    #57,d1         ; > '9'?
               ble.s    no_offset1
               add.b    #7,d1          ;if so, offset to 'A'
no_offset1:    DEBUG_SEND_CHAR   d1 
               move.w   (sp)+,d1       ;restore d1's original value
               rts




;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: DebugSendHexWord - send a hex decoding of a word to the debug port
;
;@ Interface-Description
;
;  This module decode the specified data into hex digits and sends them to
;  the debugging port.  It does this with calls to DebugSendHexByte.
;
;    Inputs:  d0 - the word to send
;
;    Outputs: NONE  
;
;---------------------------------------------------------------------
;@ Implementation-Description
;
;
;  REGISTER USAGE: d0 - modified internally but returned to it's entry value
;
;---------------------------------------------------------------------
;@ PreCondition
;    None
;---------------------------------------------------------------------
;@ PostCondition
;    None
;
;@ End-Free-Function
;=====================================================================
               SECTION  code,,C

DebugSendHexWord: REPT   2
                  rol.w #8,d0
                  jsr   DebugSendHexByte
                  ENDR
                  rts

;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: DebugSendHexWord - send a hex decoding of a long to the debug port
;
;@ Interface-Description
;
;  This module decode the specified data into hex digits and sends them to
;  the debugging port.  It does this with calls to DebugSendHexByte.
;
;    Inputs:  d0 - the long value to send
;
;    Outputs: NONE  
;
;---------------------------------------------------------------------
;@ Implementation-Description
;
;
;  REGISTER USAGE: d0 - modified internally but returned to it's entry value
;
;---------------------------------------------------------------------
;@ PreCondition
;    None
;---------------------------------------------------------------------
;@ PostCondition
;    None
;
;@ End-Free-Function
;=====================================================================
               SECTION  code,,C

DebugSendHexLong: REPT   4
                  rol.l #8,d0
                  jsr   DebugSendHexByte
                  ENDR
                  rts


_VBR__Fv:                  
                  link     A6,#0
                  movec.l  vbr,d0
                  unlk     A6
                  rts


                  ENDC                ;} //  SIGMA_DEBUG

               END

