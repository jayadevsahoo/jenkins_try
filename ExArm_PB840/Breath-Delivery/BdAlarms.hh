
#ifndef BdAlarms_HH
#define BdAlarms_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BdAlarms - provides interface with the Alarm-Analysis subsystem
//---------------------------------------------------------------------
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdAlarms.hhv   25.0.4.0   19 Nov 2013 13:59:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv    Date:   18-Nov-1997    DR Number: DCS 2610
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated the oldBpsQuality argument in PostBatteryAlarm().
//
//  Revision: 002 By: sp   Date:   06-Mar-1996    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  kam    Date:  07-Sep-1995    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "BdAlarmId.hh"
#include "Trigger.hh"

//@ Usage-Classes
#include "BdGuiCommSync.hh"
#include "OperatingParamEventName.hh"
#include "SystemBattery.hh"
//@ End-Usage


class BdAlarms
{
  public:
    BdAlarms(void);
    ~BdAlarms(void);

    static void SoftFault (const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

    inline BdGuiCommSync::CommState getAlarmCommState (void) const;
    inline void setAlarmCommState (const BdGuiCommSync::CommState state);

    static void PostBdAlarm (const BdAlarmId::BdAlarmIdType bdAlarmId,
                             const Boolean initInProg);
    static void PostBatteryAlarm(const SystemBattery::BpsQuality newBpsQuality,
                                 const Boolean initInProg);
    static void Initialize (void);

    void newCycle(void);
    void postBdAlarm (const BdAlarmId::BdAlarmIdType bdAlarmId);
    void postBdAlarmInitStatus (const BdAlarmId::BdAlarmIdType bdAlarmId);
    void alarmTriggerFilter (const Trigger::TriggerId triggerId);
    void resetAlarms (void);
    void sendStatusToAlarmAnalysis (const BdAlarmId::BdAlarmIdType bdAlarmId);
    void resyncAlarmStatus(void);

  protected:

  private:
    BdAlarms (const BdAlarms&);         // not implemented...
    void  operator= (const BdAlarms&);  // not implemented...

	void postAlarmToAlarmAnalysis(const BdAlarmId::BdAlarmIdType bdAlarmId);

    // Data-Member: bdAlarmToAlarmAnalysisMap_
    // Maps each of the BD Alarm IDs to the Alarm-Analysis Operating Parameter
    // event name
    OperatingParameterEventName bdAlarmToAlarmAnalysisMap_[BdAlarmId::NUM_TOTAL_BDALARMS];

    // Data-Member: hipThisBreath_
    // Indicates if a HIP alarm has occured and been reported to the Alarm-Analysis
    // subsystem during the current breath
    Boolean hipThisBreath_;

    // Data-Member: inStartupDisconnect_
    // Indicates if a start-up disconnect alarm is currently active
    Boolean inStartupDisconnect_;

    // Data-Member: procedureError_
    // Indicates if a procedure error has occurred
    Boolean procedureError_;

    // Data-Member: alarmCommState_
    // Indicates the status of communication of alarms with the Alarm-Analysis
    // subsystem
    BdGuiCommSync::CommState alarmCommState_;
};

//=========================================================================
//
//    Global Constants....
//
//=========================================================================

// Inlined methods...
#include "BdAlarms.in"


#endif // BdAlarms_HH 


