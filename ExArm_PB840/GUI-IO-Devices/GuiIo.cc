#include "stdafx.h"
// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: GuiIo.cc - Initialize GUI I/O device hardware/software
//---------------------------------------------------------------------
//@ Interface-Description
// Called from System Initialization prior to the GUI Application
// running, the functions in this file will setup and initialize the
// GUI I/O hardware and software. Also provides an interface to 
// SVO and VentInop LED's.
//---------------------------------------------------------------------
//@ Module-Decomposition
//	GuiIoDevicesInitialize
//	IsSVOLedOn
//	IsVentInopLedOn
//	TestFrontPanelLeds - DEVELOPMENT ONLY
//	TestKeyboardLeds - DEVELOPMENT ONLY
//---------------------------------------------------------------------
//@ Rationale
// Contains GUI CPU-specific initialization code
//---------------------------------------------------------------------
//@ Implementation-Description
// Functions in this file perform the GUI CPU-board initialization
// and retrieval of LED status.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// Must be called prior to any GUI Application execution
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-IO-Devices/vcssrc/GuiIo.ccv   25.0.4.0   19 Nov 2013 14:12:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  mjf    Date:  17-May-2007    DR Number: 6237
//       Project:  Trend
//       Description:
//             New CompactFlash class.
//
//  Revision: 004  By:  gdc    Date:  04-Jan-2001    DR Number: 5493
//       Project:  GuiComms
//       Description:
//             New TouchDriver.
//
//  Revision: 003  By:  gdc    Date:  28-MAY-1997    DR Number: 2008
//       Project:  Sigma (R8027)
//       Description:
//             Removed #include "Ipic.hh - obsolete.
//
//  Revision: 002  By:  gdc    Date:  28-MAY-1997    DR Number: 2018
//       Project:  Sigma (R8027)
//       Description:
//             Changed WaitMSec references to Task::Delay references 
//             per DCS 2018.
//
//  Revision: 001  By:  clw    Date:  28-Mar-1997    DR Number: <none>
//       Project:  Sigma (R8027)
//       Description:
//             Initial version to coding standards (Integration baseline).
//=====================================================================

#include "Sigma.hh"
#include "GuiIo.hh"
#include "TouchDriver.hh"
#include "Sound.hh" 
#include "CompactFlash.hh"


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  GuiIoDevicesInitialize
//
//@ Interface-Description
// We do all necessary board-specific initialization to support the
// GUI I/O hardware (keyboard, rotary knob and touch screen) prior
// to executing any GUI Application task(s).
// Note: No user input is permissable prior to this function's execution.
//---------------------------------------------------------------------
//@ Implementation-Description
// All GUI I/O devices use hardware-mapped I/O addresses.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Free-Function
//=====================================================================

void
GuiIoDevicesInitialize(void)
{
	KeysDrvTaskInit();
	KnobDrvTaskInit();
	TouchDriver::Initialize();
    Sound::Initialize();
    CompactFlash::Initialize();
				//$[TI1]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsSVOLedOn
//
//@  Interface-Description
// Return indication of whether Safety Valve Open front panel LED is on
//---------------------------------------------------------------------
//@ Implementation-Description
// The SVO LED status bit was added to pass 3 GUI board in I/O register
// 2, bit 4 (mask 0x10). 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Free-Function
//=====================================================================

Boolean
IsSVOLedOn(void)
{
	// TODO E600 port
  //return( ( *IO_REGISTER_2 & 0x10 ) ? TRUE : FALSE );

	return FALSE;
  	//$[TI1] TRUE
	//$[TI2] FALSE
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsVentInopLedOn
//
//@ Interface-Description
// Return indication of whether Vent Inoperative front panel LED is on
//---------------------------------------------------------------------
//@ Implementation-Description
// The VENT_INOP LED status bit was added to pass 3 GUI board in I/O register
// 1, bit 2 (mask 0x04).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Free-Function
//=====================================================================

Boolean
IsVentInopLedOn(void)
{
	// TODO E600 port
//  return( ( *IO_REGISTER_1 & 0x04 ) ? TRUE : FALSE );
  	//$[TI1] TRUE
	//$[TI2] FALSE

	return FALSE;
}



// ********************************************************************
// the remainder of this file contains diagnostic functions not part
// of production code
// ********************************************************************

#ifdef	SIGMA_DEVELOPMENT

# include "GuiIoLed.hh"
# include "Task.hh"

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//  Free-Function:  TestFrontPanelLeds
//
//  Interface-Description
// Test GUI-CPU front panel LEDS.  This function can be called at
// any time but it conflicts with the BlinkTask.  The function is
// only present if SIGMA_DEVELOPMENT is defined at compile time.
//---------------------------------------------------------------------
//  Implementation-Description
// Turn all front panel LEDs off then turn each one on, one at a time,
// for a period of 100ms.  Finally, turn all front panel LEDs on for
// 100ms, then off for 100ms.  Repeat this final sequence.  Leave the
// LEDs off before returning.  Notice one call to this function takes
// about 1.7s of elapsed time.  During tasking, the function doesn't
// block.
//---------------------------------------------------------------------
//  PreCondition
//	none
//---------------------------------------------------------------------
//  PostCondition
//	none
//
//  End-Free-Function
//=====================================================================

void
TestFrontPanelLeds(void)
{
	Int32	ii;

	*FP_LED = 0x0000;

	for(ii = 0; ii < 13; ii++)	// all bits used
	{
		*FP_LED = (Uint16)(1 << ii);
		Task::Delay(0,100 /*ms*/);
	}
	*FP_LED = 0x0000;

	for(ii = 0; ii < 2; ii++)
	{
		*FP_LED = 0xFFFF;		// turn 'em all on...
		Task::Delay(0,100 /*ms*/);
		*FP_LED = 0x0000;		// ...turn 'em all off
		Task::Delay(0,100 /*ms*/);
	}
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//  Free-Function:  TestKeyboardLeds
//
//  Interface-Description
// Test GUI-CPU keyboard LEDS.  This function can be called at
// any time but it conflicts with the BlinkTask.  The function is
// only present if SIGMA_DEVELOPMENT is defined at compile time.
//---------------------------------------------------------------------
//  Implementation-Description
// Turn all keyboard LEDs off then turn each one on, one at a time,
// for a period of 100ms.  Finally, turn all keyboard LEDs on for
// 100ms, then off for 100ms.  Repeat this final sequence.  Leave the
// LEDs off before returning.  Notice one call to this function takes
// about 0.7s of elapsed time.  During tasking, the function does not
// block
//---------------------------------------------------------------------
//  PreCondition
//	none
//---------------------------------------------------------------------
//  PostCondition
//	none
//
//  End-Free-Function
//=====================================================================

void
TestKeyboardLeds(void)
{
	Int32	ii;

	*KEYS_LED_1 = 0x00;	// turn 'em all off
	for(ii = 0; ii < NUMBER_OF_KB_LEDS; ii++)
	{
		*KEYS_LED_1 = (Uint8)(1 << ii);
		Task::Delay(0,100 /*ms*/);
	}
	*KEYS_LED_1 = 0x00;

	for(ii = 0; ii < 2; ii++)
	{
		*KEYS_LED_1 = 0xFF;	// turn 'em all on
		Task::Delay(0, 100 /*ms*/);
		*KEYS_LED_1 = 0x00;	// turn 'em all off
		Task::Delay(0, 100 /*ms*/);
	}
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//  Free-Function:  HeartBeatTask
//
//  Interface-Description
// A VRTX task that can be spawned to strobe the GUI keyboard LEDs.  This
// function is only present if SIGMA_DEVELOPMENT is defined at compile time.
//---------------------------------------------------------------------
//  Implementation-Description
// This sequence takes approximately 1 second of elapsed time per loop.
// Notice that HeartBeatTask should always be the lowest (worst) task
// priority so that it doesn't interfere with anything important.  This
// task is useful during development so as to indicate that at least
// one task continues to run
//---------------------------------------------------------------------
//  PreCondition
// HeartBeatTaskInit must have been called
//---------------------------------------------------------------------
//  PostCondition
//	none
//  End-Free-Function
//=====================================================================

void
HeartBeatTask(void)
{
	Int32	ii;
	Uint8	state;
	Int32	msecs = 1000 / NUMBER_OF_KB_LEDS;  // whole seq takes 1 sec

	for( ;; )
	{
		state = 1;
		for(ii = 0; ii < NUMBER_OF_KB_LEDS; ii++)
		{
			*KEYS_LED_1 = state;

			state <<= 1;
			Task::Delay(0, msecs);	// this is non-blocking
		}
	}
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//  Free-Function:  HeartBeatTaskInit
//
//  Interface-Description
// Initialization for HeartBeatTask.  This function is only present if
// SIGMA_DEVELOPMENT is defined at compile time.
//---------------------------------------------------------------------
//  Implementation-Description
//---------------------------------------------------------------------
//  PreCondition
//	none
//---------------------------------------------------------------------
//  PostCondition
//	none
//  End-Free-Function
//=====================================================================

void
HeartBeatTaskInit(void)
{
	TestKeyboardLeds();
}

#endif	// SIGMA_DEVELOPMENT
