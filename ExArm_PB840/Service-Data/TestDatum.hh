
#ifndef TestDatum_HH
#define TestDatum_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  TestDatum - data structure definition that holds
//            a Service mode data item.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Service-Data/vcssrc/TestDatum.hhv   10.7   08/17/07 10:35:04   pvcs  
//
//@ Modification-Log
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"

// Type: TestDatum
// TestDatum is the basic unit to hold the test values with
// the following information:
// testValueType: The structure for handling test data value.
// 
struct TestDatum
{
	union testType
	{
		Real32 rValue;
		Int iValue;
	} testValueType;
};

#endif // TestDatum_HH 
