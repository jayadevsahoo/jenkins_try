#ifndef SerialNumber_HH
#define SerialNumber_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 
//====================================================================
// Class: SerialNumber - a string that contains serial number information.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/SerialNumber.hhv   25.0.4.0   19 Nov 2013 13:54:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 

#if 0  
// for bug in genDD
#endif

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

//TODO E600 add #include "FlashMemCons.hh"

//@ End-Usage

#ifdef SIGMA_DEVELOPMENT
#include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT

class SerialNumber 
{
  public:
    SerialNumber();
    ~SerialNumber();
    SerialNumber(const SerialNumber& serial_number);
    SerialNumber(const char * pString);
    SerialNumber& operator=(const SerialNumber& serial_number);
    Boolean operator==(const SerialNumber& serial_number) const;
    Boolean operator!=(const SerialNumber& serial_number) const;

#ifdef SIGMA_DEVELOPMENT
    friend ostream& operator<< 
      (ostream& theStream, const SerialNumber& rNumber);
#endif // SIGMA_DEVELOPMENT

	const char * getString(void) const;
    char operator[](const Uint index) const;

    static void         SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName = NULL,
								  const char*        pPredicate = NULL);

  private:

    //TODO E600 temp pnly
#define SERIAL_NO_SIZE          (10)
    //@ Data-Member:    data_
    //  The serial number data itself.
    char   data_[SERIAL_NO_SIZE+1];

};
 

#endif // SerialNumber_HH
