#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LeakTest - Test for pneumatic leaks
//---------------------------------------------------------------------
//@ Interface-Description
//	This class implements the EST and SST leak tests.  Leak is
//	measured by pressurizing the system and checking for a maximum
//	pressure drop within a maximum test time.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the EST and SST leak tests.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method estTest() is responsible for testing leak during EST, and
//  the method sstTest() is responsible for testing leak during SST.
//-----------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class is allowed.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/LeakTest.ccv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 013  By: rhj    Date: 12-Apr-2011     SCR Number: 6760
//  Project:  PROX
//  Description:
//      Added code in sstTest() method to handle clear key press
//      which indicates the user has skipped the prox test.
// 
//  Revision: 012  By: rhj    Date: 31-Mar-2011     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added code to ensure that PProxiInterface is not null.
// 
//  Revision: 011  By: rhj    Date: 16-June-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added start and stop continuous mode calls.
// 
//  Revision: 010  By: rhj    Date: 06-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added prox prompts      
// 
//  Revision: 008  By: syw    Date: 17-Feb-2000     DR Number: 5647
//  Project:  NeoMode
//  Description:
//      Upon immediate test failure, send to service data the exit pressure.
//
//  Revision: 007  By: syw    Date: 02-Feb-2000     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Remove reference to PatientCctTypeValue.hh
//
//  Revision: 006  By: dosman    Date: 26-Feb-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	Implemented changes to get rid of compiler warnings during unit test.
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  quf    Date: 29-Jul-1997    DR Number: DCS 2266
//       Project:  Sigma (840)
//       Description:
//            Added final expiratory pressure check for the EST Leak
//            Test only, which generates Alert if final exp pressure
//            is out of range of the final insp pressure.
//
//  Revision: 003  By:  quf    Date: 28-Jul-1997    DR Number: DCS 2245
//       Project:  Sigma (840)
//       Description:
//            Removed high pressure exit threshold from
//            waitForPressureDrop_().
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "LeakTest.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmConstants.hh"
#include "SmExhValveController.hh"
#include "SmPressureController.hh"
#include "SmManager.hh"
#include "SmUtilities.hh"
#include "ProxiInterface.hh"
#include "ProxTest.hh"
#include "Task.hh"

#include "PhasedInContextHandle.hh"

#include "MainSensorRefs.hh"
#include "ValveRefs.hh"
#include "Psol.hh"
#include "FlowSensor.hh"
#include "PatientCctTypeValue.hh"

#if defined(SIGMA_DEVELOPMENT)
# include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"
# include "LeakTest_UT.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage


//@ Code...

//@ Constant: LEAK_TEST_BUILD_PRESSURE
// initial system pressure
const Real32 LEAK_TEST_BUILD_PRESSURE = 90.0;  // cm H2O

//@ Constant: LEAK_TEST_START_PRESSURE
// starting pressure for leak measurement
const Real32 LEAK_TEST_START_PRESSURE = 85.0;  // cm H2O

//@ Constant: EST_FAILURE_DELTA_P
// FAILURE pressure drop criteria for EST
const Real32 EST_FAILURE_DELTA_P = 5.0;   // cm H2O

//@ Constant: SST_ALERT_DELTA_P
// ALERT pressure drop criteria for SST
const Real32 SST_ALERT_DELTA_P = 10.0;    // cm H2O

//@ Constant: SST_FAILURE_DELTA_P
// FAILURE pressure drop criteria for SST
const Real32 SST_FAILURE_DELTA_P = 30.0;  // cm H2O

//@ Constant: LEAK_TEST_TIME
// max time allowed to drop pressure by the FAILURE amount
const Int32 LEAK_TEST_TIME = 10000;  // msec

extern ProxiInterface* PProxiInterface;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LeakTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LeakTest::LeakTest(void)
{
	CALL_TRACE("LeakTest::LeakTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LeakTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LeakTest::~LeakTest(void)
{
	CALL_TRACE("LeakTest::~LeakTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: estTest()
//
//@ Interface-Description
//  This method performs the EST leak test.  It takes no arguments
//  and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09085]
//	Perform the EST leak test via measureLeak_() and evaluate the
//	returned result ID to generate the appropriate error condition if
//	required.  If leak was not detected but final exp pressure is out
//	of range of the final insp pressure, generate an ALERT.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LeakTest::estTest( void)
{
	CALL_TRACE("LeakTest::estTest( void)");

	Real32 pressureDrop;
	LeakTestResult result = measureLeak_( STATE_SERVICE,
										  pressureDrop );

	CLASS_ASSERTION( result == PRESSURE_BUILD_FAILURE ||
					 result == UNDERSHOT_STARTING_PRESSURE ||
					 result == TIGHT_CIRCUIT ||
					 result == PROCEED );

	if (result == PRESSURE_BUILD_FAILURE)
	{
	// $[TI1]
		// Can't reach initial pressure
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_5);
	}
	else if ( (result == UNDERSHOT_STARTING_PRESSURE) ||
			  (result == PROCEED && pressureDrop >= EST_FAILURE_DELTA_P) )
	{
	// $[TI2]
		// System leak FAILURE
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_1);
	}
	else if ( ABS_VALUE(inspPressure_ - expPressure_) > 
				(inspPressure_ + expPressure_) * REL_PRESSURE_ERROR +
										 2.0 * ABS_PRESSURE_ERROR )
	{
	// $[TI4]
		// test circuit not connected to expiratory side
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_6);
	}
	else
	{
	// $[TI3]
		// PASS leak test -- make sure results are consistent
		CLASS_ASSERTION( (result == TIGHT_CIRCUIT) || 
						 (result == PROCEED &&
						  pressureDrop < EST_FAILURE_DELTA_P) );
	}
	// end else

#ifdef SIGMA_DEBUG
	printf("Leak Test: pressure drop = %f cm H20\n", pressureDrop);
#endif

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sstTest()
//
//@ Interface-Description
//  This method performs the SST leak test.  It takes no arguments
//  and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[10009]
//	Perform the SST leak test via measureLeak_() and evaluate the
//	returned result ID to generate the appropriate error condition if
//	required.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LeakTest::sstTest( void)
{
	CALL_TRACE("LeakTest::sstTest( void)");

	const DiscreteValue  CIRCUIT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	Boolean isNeoCircuit = (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT);

	if (isNeoCircuit)
	{
        if (PProxiInterface)
        {
            ProxiInterface& rProxiInterface = *PProxiInterface;

            if (rProxiInterface.isProxBoardInstalled())
            {
                // Suspend a purge in case the user
                // restart SST.
                rProxiInterface.suspendPurge();
                Task::Delay(1);
                if (RProxTest.doSensorTest())
                {
                    // Send Prox a continuous mode packet to grab 
                    // Barometric Pressure and a faster accumulator
                    // pressure during SST.
                    Task::Delay(2);
                    rProxiInterface.disableContinuousMode();
                    Task::Delay(2);
                    rProxiInterface.enableContinuousMode();
                }
            }
        }
	}



	Real32 pressureDrop;
	LeakTestResult result = measureLeak_( STATE_SST,
											  pressureDrop );

	CLASS_ASSERTION( result == PRESSURE_BUILD_FAILURE ||
					 result == UNDERSHOT_STARTING_PRESSURE ||
					 result == TIGHT_CIRCUIT ||
					 result == PROCEED );

	if (result == PRESSURE_BUILD_FAILURE)
	{
	// $[TI1]
		// Can't reach initial pressure
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_5);
	}
	else if ( result == UNDERSHOT_STARTING_PRESSURE ||
			  (result == PROCEED &&
				pressureDrop >= SST_FAILURE_DELTA_P) )
	{
	// $[TI2]
		// System leak FAILURE
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_1);
	}
	else if (result == PROCEED &&
				pressureDrop >= SST_ALERT_DELTA_P)
	{
	// $[TI3]
		// System leak ALERT
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_2);
	}
	else
	{
	// $[TI4]
		// leak test PASS -- make sure results are consistent
		CLASS_ASSERTION( (result == TIGHT_CIRCUIT) || 
						 (result == PROCEED &&
						   pressureDrop < SST_ALERT_DELTA_P) );
	}
	// end else

#ifdef SIGMA_DEBUG
	printf("Leak Test: pressure drop = %f cm H20\n", pressureDrop);
#endif

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
LeakTest::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
		    const char*        pFileName,
		    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, LEAKTEST,
                          lineNumber, pFileName, pPredicate);

}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: measureLeak_()
//
//@ Interface-Description
//  This method is used to test the leak amount.  Two arguments:
//	system state (Service or SST) and pressure drop (passed as reference).
//	The return value is a result ID, but because the pressure drop is
//	passed as a reference parameter, it is also returned to the calling
//	function.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Determine the appropriate test parameters based on the system state.
//	Pressurize the system to the initial pressure, exit indicating
//	"pressure build timeout" if pressure can't build within the timeout.
//	Delay to allow pressure waves to decay.
//	Wait for pressure to drop to leak measurement starting pressure, but
//	if pressure is already below this, exit indicating "leak failure".
//	If pressure doesn't drop to the starting pressure within the timeout,
//	exit indicating "tight circuit".
//	Once pressure drops to the starting pressure, wait until pressure
//	drops to the FAILURE level or until the maximum test time occurs.
//	If pressure drops to the FAILURE level within the max test time,
//	exit indicating "leak failure".
//	If max test time occurred, exit indicating "proceed", i.e. the
//	calling function must make the final determination of test pass
//	or test ALERT via the pressure drop amount.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LeakTest::LeakTestResult
LeakTest::measureLeak_( SigmaState state, Real32& pressureDrop)
{
	CALL_TRACE("LeakTest::measureLeak_( SigmaState state, \
									Real32& pressureDrop)");

#if !defined(SIGMA_UNIT_TEST)
	Uint32 exitTime1;
	Uint32 exitTime2;
	const Int32 INITIAL_PRESSURE_DROP_TIMEOUT = 10000;  // ms
	Uint32 timerOffset;
#endif // defined(SIGMA_UNIT_TEST)

	Real32 finalPressure, currentPressure;
	Real32 flowRate, exitPressure;
	Uint32 timeout;
	LeakTestResult testResult;

#ifdef SIGMA_DEBUG
#if !defined(SIGMA_UNIT_TEST)
	Real32 initialPressure;
	Uint32 elapsedTestTime, finalTime1, finalTime2;
#endif // !defined(SIGMA_UNIT_TEST)
#endif  // SIGMA_DEBUG

	// Set flow rate, pressure timeout, and leak test time
	// based on test type
	if (state == STATE_SERVICE)
	{
	// $[TI1]
		// EST
		flowRate = 2.0;  // lpm				

		timeout = RSmUtilities.pressureTimeout(
										STATE_SERVICE,
										LEAK_TEST_BUILD_PRESSURE,
										flowRate );

		exitPressure = LEAK_TEST_START_PRESSURE -
						EST_FAILURE_DELTA_P;
	}
	else
	{
	// $[TI2]
		// SST
		flowRate = 10.0;  // lpm

		timeout = RSmUtilities.pressureTimeout(
										STATE_SST,
										LEAK_TEST_BUILD_PRESSURE,
										flowRate );

		exitPressure = LEAK_TEST_START_PRESSURE -
						SST_FAILURE_DELTA_P;
	}
	// end else

#ifdef SIGMA_UNIT_TEST
	LeakTest_UT::FlowRate = flowRate;
#endif  // SIGMA_UNIT_TEST

	// make sure pressure sensors are zeroed
	RSmUtilities.autozeroPressureXducers();

	// Close the exh valve
	RSmExhValveController.close( TRUE );

	// set data item pointer to the bar graph data item
	RSmManager.advanceTestPoint (SmDataId::SM_LEAK_MONITOR_PRESSURE);

	// Establish a pressure a little above the test point pressure,
	RSmPressureController.establishPressure (
								LEAK_TEST_BUILD_PRESSURE,
								DELIVERY, EITHER, flowRate);

	// Display bar graph data while waiting for pressure to build
	if (!RSmPressureController.waitForPressure( timeout, TRUE))
	{
	// $[TI3]
		// Failed to reach initial pressure
		testResult = PRESSURE_BUILD_FAILURE;

		// Turn off the controller
		RSmPressureController.stopPressureController();

		RETAILMSG( TRUE, (L"Failed to pressurize\r\n") );

#if defined(SIGMA_DEBUG)
	printf("Leak Test: CAN'T ESTABLISH PRESSURE!!\n");
#endif  // defined(SIGMA_DEBUG)

	}
	else
	{
	// $[TI4]
		// Allow pressure to stabilize
		Task::Delay (0, 200);

		// Turn off pressure controller
		RSmPressureController.stopPressureController();

		// get pressure sample and send value to gui
		currentPressure = RInspPressureSensor.getValue();
		RSmManager.sendMonitorPointToServiceData (currentPressure);

		if (currentPressure < LEAK_TEST_START_PRESSURE)
		{
		// $[TI4.1]
			// Pressure dropped too quickly, i.e.
			// dropped below the test starting pressure
			// within the (very short) pressure
			// stabilization time, so there's an obvious
			// large leak

			testResult = UNDERSHOT_STARTING_PRESSURE;

			// force pressure drop to the failure criteria
			pressureDrop = LEAK_TEST_START_PRESSURE - exitPressure;

			RSmManager.sendMonitorPointToServiceData (exitPressure);

			RETAILMSG( TRUE, (L"Undershot\r\n") );

#if defined(SIGMA_DEBUG)
	printf("Leak Test: undershot starting pressure = FAILURE\n");
#endif  // defined(SIGMA_DEBUG)
		}
		else
		{
		// $[TI4.2]
			// Wait for pressure to drop to test starting pressure,
			// with timeout.  NOTE: timeout here means leak test
			// passes, since pressure can't drop to the test
			// starting pressure in a reasonable time.

			OsTimeStamp testTimer;

#ifdef SIGMA_UNIT_TEST
			currentPressure = LeakTest_UT::Pressure[0];
#else
			currentPressure = waitForPressureDrop_(
						LEAK_TEST_START_PRESSURE,
						INITIAL_PRESSURE_DROP_TIMEOUT,
						testTimer, exitTime1);

			RETAILMSG( TRUE, (L"Current pressure: %d\r\n", (Uint32)(currentPressure) ) );
#endif  // SIGMA_UNIT_TEST

#ifdef SIGMA_DEBUG
#if !defined(SIGMA_UNIT_TEST)
			finalTime1 = testTimer.getPassedTime();
#endif // !defined(SIGMA_UNIT_TEST)
#endif  // SIGMA_DEBUG

			// set data item pointer to initial pressure data item
			RSmManager.advanceTestPoint(
						SmDataId::SM_LEAK_INSPIRATORY_PRESSURE1);

			if (currentPressure > LEAK_TEST_START_PRESSURE)
			{
			// $[TI4.2.1]
				// Can't drop to the test starting pressure within
				// the timeout or pressure rose above the initial
				// build pressure: we have a tight circuit and/or
				// a sufficiently large psol leak to counteract
				// circuit leak.
				
				testResult = TIGHT_CIRCUIT;

				RETAILMSG( TRUE, (L"Tight circuit\r\t") );

				// Send data as if main test proceeded and passed
				// with no leak
				// Starting pressure
				RSmManager.sendTestPointToServiceData(currentPressure);
				// Bar graph data
				RSmManager.sendTestPointToServiceData(currentPressure);
				// Final pressure data
				RSmManager.sendTestPointToServiceData(currentPressure);
				// Pressure drop data
				pressureDrop = 0.0;
				RSmManager.sendTestPointToServiceData(pressureDrop);

#if defined(SIGMA_DEBUG)
	printf("Leak Test: Can't drop to starting pressure = TEST PASSED\n");
#endif  // defined(SIGMA_DEBUG)

			}
			else
			{
			// $[TI4.2.2]
				// Reached test starting pressure:
				// PROCEED WITH MAIN TEST

				testResult = PROCEED;
				
				RETAILMSG( TRUE, (L"Proceeding\r\t" ) );
				// Send the starting pressure to the gui
				RSmManager.signalTestPoint();
				RSmManager.sendTestPointToServiceData(
										LEAK_TEST_START_PRESSURE);

#ifdef SIGMA_DEBUG
#if !defined(SIGMA_UNIT_TEST)
				initialPressure = currentPressure;
				OsTimeStamp secondaryTimer;
#endif // !defined(SIGMA_UNIT_TEST)
#endif  // SIGMA_DEBUG

#ifdef SIGMA_UNIT_TEST
				currentPressure = LeakTest_UT::Pressure[1];
#else
				// calculate the time it took from the
				// last insp pressure sample to here
				timerOffset = testTimer.getPassedTime() - exitTime1;

				currentPressure = waitForPressureDrop_(
						exitPressure,
						LEAK_TEST_TIME - timerOffset,
						testTimer, exitTime2);

				RETAILMSG( TRUE, (L"Pressure after drop: %d\r\n", (Uint32)(currentPressure) ) );
#endif  // SIGMA_UNIT_TEST

#ifdef SIGMA_DEBUG
#if !defined(SIGMA_UNIT_TEST)
				finalTime2 = testTimer.getPassedTime();
				elapsedTestTime = secondaryTimer.getPassedTime();
				printf("timerOffset = %u msec\n", timerOffset);
				printf("exit time 1 = %u msec\n", exitTime1);
				printf("exit time 2 = %u msec\n", exitTime2);
				printf("final time 1 = %u msec\n", finalTime1);
				printf("final time 2 = %u msec\n", finalTime2);
				printf("secondaryTimer elapsed time = %u msec\n", elapsedTestTime);
				printf("pressure drop time = %u msec\n", exitTime2 - exitTime1);
				printf("pressure drop timeout = %u msec\n", LEAK_TEST_TIME - timerOffset);
#endif // !defined(SIGMA_UNIT_TEST)
#endif  // SIGMA_DEBUG

				// Get the final circuit pressure
				RSmManager.signalTestPoint();
				finalPressure = currentPressure;

				// Send final test point twice: once for the final graphic
				// and once for the displayed number.
				// 1st send goes to bar graph
				RSmManager.sendTestPointToServiceData(finalPressure);
				// 2nd send goes to displayed number
				RSmManager.sendTestPointToServiceData(finalPressure);

				// Send the pressure drop
				pressureDrop = LEAK_TEST_START_PRESSURE - finalPressure;
				RSmManager.sendTestPointToServiceData(pressureDrop);
			}
			// end else (main test)

#if defined(SIGMA_DEBUG)
#if !defined(SIGMA_UNIT_TEST)
	printf("Leak Test: true initial pressure = %f\n", initialPressure);
	printf("Leak Test: final pressure   = %f\n", finalPressure);
#endif // !defined(SIGMA_UNIT_TEST)
#endif  // SIGMA_DEBUG

		}
		// end else (large leak pre-test)
	}
	// end else (pressure build check)

	// Open the exhalation valve
	RSmExhValveController.open();

	// return the pressure reached status

	RETAILMSG( TRUE, (L"Test result: %d\r\n", testResult ) );
	return (testResult);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: waitForPressureDrop_()
//
//@ Interface-Description
//	This method is used to wait for pressure to drop to a low threshold,
//	rise to a high threshold, or until timeout occurs if neither threshold
//	is reached.  Four arguments: low pressure threshold, 
//	timeout in msecs, OsTimeStamp object used for test timing
//	(passed as reference), and exit time in msecs (passed as reference).
//	The final argument is actually used to pass an additional value
//	back to the calling function and need not be set when invoking
//	this method.
//  The final inspiratory pressure reading is the return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Delay until low threshold is reached, high threshold is reached,
//	or timeout occurs.  Return the inspiratory pressure and the exit time.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Real32
LeakTest::waitForPressureDrop_( const Real32 lowThreshold,
			const Uint32 timeout, OsTimeStamp& timer, Uint32& exitTime )
{
	CALL_TRACE("LeakTest::waitForPressureDrop_( const Real32 lowThreshold, \
		const Uint32 timeout, OsTimeStamp& timer, Uint32& exitTime )");

	Uint32 startTime = timer.getPassedTime();
	Uint32 currentTime;
	Real32 inspPressure;

	// wait for pressure to go out of the low bounds
	// or until timeout occurs
	do
	{
	// $[TI1]
		Task::Delay (0, SM_CYCLE_TIME);

		inspPressure = RInspPressureSensor.getValue();

		currentTime = timer.getPassedTime();

		// send bar graph data every 500 msec
		if (currentTime % 500 == 0)
		{
		// $[TI1.1]
			// send bar graph data to gui
			RSmManager.sendMonitorPointToServiceData(inspPressure);
		}
		// $[TI1.2]
		// implied else
	} while (
		inspPressure > lowThreshold &&
		timer.getPassedTime() - startTime < timeout );

	exitTime = currentTime;

	// store final insp and exp pressures for EST leak test to check
	// if test circuit is connected to expiratory side
	inspPressure_ = inspPressure;
	expPressure_ = RExhPressureSensor.getValue();
				
	return( inspPressure);
}
