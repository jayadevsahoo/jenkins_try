#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995-2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LogCell -  A container that handles the text formatting and display
// for a cell of ScrollableLog.
//---------------------------------------------------------------------
//@ Interface-Description
// Only one interface method is used and that is setText().  After construction
// of LogCell (coloring, sizing and position of LogCell is the responsibility
// of the owner) the contents of LogCell are set and can be reset via a call to
// setText().  There are various auto-formatting options selectable through
// this call in order to relieve the owner of some of text formatting burden.
// LogCells are designed to autoformat up to two lines of text though three or
// more lines than this can be displayed if the autoformatting options are
// bypassed and the textual contents of the cell are specified directly in
// cheap text.
//---------------------------------------------------------------------
//@ Rationale
// Handy object that takes care of text formatting.
//---------------------------------------------------------------------
//@ Implementation-Description
// LogCell contains three TextFields to do text display.  All the functionality
// is concentrated in setText() which receives a text render request and
// uses zero, one, two or three TextFields to display the result.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// When the non-cheap text option is used, the one or two strings that need
// auto-formatting cannot have any cheap text features embedded in them.
// For example, "hello" and "this is sample text" are valid but
// "Press the [ALARM_SYMBOL]" is not.  Also, only up to two strings can
// be autoformatted, the third string is used only in cheap text mode,
// typically when three lines of cheap text need displaying in a single cell.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LogCell.ccv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: gdc    Date: 27-May-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//  Changed setText to position the text fields by first rendering
//  the text field using TextField::setText and then positioning the
//  TextField container instead of modifying the X & Y values contained
//  in the cheap text string. Centered text based on the height of the
//  character above its baseline (ascent) instead of the height of the
//  entire text field. Alarm rendering that requires positioning its
//  text using the entire height of the text field does its own
//  rendering so this changes will not affect that. This eliminated the
//  incidence of TextFields with negative origins which caused many
//  problems in the area accumulation and rendering process. Negative
//  origins are now reset in TextField, but without this correction in
//  LogCell, the text was not positioned correctly.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc    Date:  08-OCT-1998  DCS Number: 5193
//  Project:  BiLevel
//  Description:
//    Initialized X and Y position of text fields prior to centering
//    them. setText() centers the text in the LogCell by adjusting the
//    text field's Y position. Subsequent setText() operations would then
//    use the adjusted Y position instead of the origin which 
//    caused the text to be mispositioned in the cell.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "LogCell.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LogCell()  [Constructor]
//
//@ Interface-Description
// Constructs LogCell.  No arguments needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just add the three TextFields to the container in preparation for display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LogCell::LogCell(void)
{
	CALL_TRACE("LogCell::LogCell(void)");

	// Add textfields to container
	addDrawable(&field1_);
	addDrawable(&field2_);
	addDrawable(&field3_);								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LogCell()  [Destructor]
//
//@ Interface-Description
// LogCell destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LogCell::~LogCell(void)
{
	CALL_TRACE("LogCell::~LogCell(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setText
//
//@ Interface-Description
// Call this to display text in the LogCell.  Text can either be specified
// as cheap text (where the owner has already formatted the text and all
// LogCell needs to do is display it) or as up to two lines of text specified,
// generally, without cheap text features along with other parameters
// which indicate how to format the lines.  Parameters are as follows:
// >Von
//	isCheapText		TRUE if cheap text is being passed.  In this case string1,
//					string2 and string3 provide the cheap text and are
//					positioned relative to the top left of the cell.
//					If FALSE, string1 and string2 (if set) are autoformatted
//					for display.
//	string1			The first line of auto-formatted text if isCheapText is
//					FALSE or else the cheap text string if isCheapText is
//					TRUE.
//	string2			The second piece of text.  Can be NULL_STRING_ID if only
//					one piece of text needs displaying. Default is
//					NULL_STRING_ID.
//	string3			The third piece of text. Defaulted to NULL_STRING_ID.
//	alignment		How the text should be aligned, LEFT, RIGHT or CENTER.
//					Defaults to CENTER.
//	fontStyle		The style of the font to use in auto-formatting, either
//					TextFont::NORMAL, THIN or ITALIC.  NORMAL is the default.
//	fontSize		The size of the font to use, the default is
//					TextFont::TWELVE.
//	margin			If alignment is LEFT then margin specifies the pixels
//					from the left of LogCell's container at which text should
//					be positioned.  If alignment is RIGHT then text is
//					right-aligned with that number of pixels gap between the
//					end of the text and the right of LogCell.  Ignored if
//					aligment is CENTER.  Defaults to 0.
//	string1Message	A help message that is associated with string1.  Used
//					only when string1 is specified as cheap text.  Typically,
//					used when string1 is just a symbol and it needs a
//					corresponding help message to be displayed in the message
//					area.
// >Voff
// All parameters after string1 are defaulted.  If the application wants
// to display an empty cell, simply set isCheapText to TRUE and string1
// to NULL_STRING_ID.  String arguments set to the empty string ("") are
// treated as if they are NULL_STRING_ID.
//---------------------------------------------------------------------
//@ Implementation-Description
// If isCheapText is TRUE then simply display all three string parameters
// otherwise just format string1 and string2 as specified by the parameters.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
LogCell::setText(Boolean isCheapText, StringId string1,
				StringId string2, StringId string3, Alignment alignment,
				TextFont::Style fontStyle, TextFont::Size fontSize,
				Uint16 margin, StringId string1Message)
{
	CALL_TRACE("LogCell::setText(Boolean isCheapText, StringId string1, "
				"StringId string2, StringId string3, Alignment alignment, "
				"TextFont::Style fontStyle, TextFont::Size fontSize, "
				"Uint16 margin, StringId string1Message)");

	field1_.setX(0);					// Make sure text is positioned
	field1_.setY(0);					//	at top left of container.
	field2_.setX(0);					// Make sure text is positioned
	field2_.setY(0);					//	at top left of container.
	field3_.setX(0);					// Make sure text is positioned
	field3_.setY(0);					//	at top left of container.

	// Check to see if we were passed cheap text
	if (isCheapText)
	{													// $[TI1]
		// We were passed cheap text so just display the three strings them

		field1_.setText(string1);			// Use cheap text
		field1_.setMessage(string1Message);	// Set help message for field
		field1_.setShow(TRUE);				// Display field1_

		field2_.setText(string2);			// Use cheap text
		field2_.setShow(TRUE);				// Display field2_

		field3_.setText(string3);			// Use cheap text
		field3_.setShow(TRUE);				// Display field3_
	}
	else
	{													// $[TI2]
		static wchar_t TmpBuffer_[256];		// Temporary buffer for sprintf'ing

		// Should only been passed string1 and/or string2
		CLASS_ASSERTION(NULL_STRING_ID == string3);

		field3_.setShow(FALSE);				// Hide field3_

		// From the fontStyle parameter, set the character used to identify
		// font style in cheap text strings, for later use.
		const wchar_t fontStyleId = TextFont::GetFontStyleChar(fontStyle);

		// Is second string parameter not set?
		if ((NULL_STRING_ID == string2) || (L'\0' == string2[0]))
		{												// $[TI6]
			// It's not set so don't show field 2
			field2_.setShow(FALSE);

			// Is the first string parameter not set?
			if ((NULL_STRING_ID == string1) || (L'\0' == string1[0]))
			{											// $[TI7]
				// First is also not set so don't show field 1 either
				field1_.setShow(FALSE);
			}
			else
			{
				Int32 x;
				Int32 y;
				Int32 width;

				Int ascent = TextFont::GetMaxAscent(fontSize, fontStyle);
	
				// First string only is set.  Let's display it.
				swprintf(TmpBuffer_, L"{p=%d,y=%d,%c:%s}", fontSize, ascent, fontStyleId, string1);
				field1_.setText(TmpBuffer_);
				width = field1_.getWidth();

				// The Y position of the top left of the text is set so as to
				// center the text pixels that are above the baseline within the logcell
				y = (getHeight() - ascent) / 2 - 1;

				// Now decide on where the X position is, depending on alignment
				switch (alignment)
				{
				case CENTER:
					// Center within logcell width.
					x = (getWidth() - width) / 2;
					break;

				case RIGHT:
					// Position so that 'margin' pixels are left at the right of text.
					x = getWidth() - margin - width;
					break;

				case LEFT:
					// Position text 'margin' pixels from left edge.
					x = margin;
					break;

				default:
					AUX_CLASS_ASSERTION_FAILURE(alignment);
					break;
				}

				field1_.setX(x);
				field1_.setY(y);
				field1_.setShow(TRUE);
			}
		}
		else
		{												// $[TI12]
			CLASS_ASSERTION(NULL_STRING_ID != string1);

			// Both string parameters are set.  Let's display them.		
			Int32 x1;
			Int32 y1;
			Int32 width1;
			Int32 x2;
			Int32 y2;
			Int32 width2;

			Int ascent = TextFont::GetMaxAscent(fontSize, fontStyle);

			swprintf(TmpBuffer_, L"{p=%d,y=%d,%c:%s}", fontSize, ascent, fontStyleId, string1);
			field1_.setText(TmpBuffer_);
			width1 = field1_.getWidth();

			swprintf(TmpBuffer_, L"{p=%d,y=%d,%c:%s}", fontSize, ascent, fontStyleId, string2);
			field2_.setText(TmpBuffer_);
			width2 = field2_.getWidth();

			// Each string should be positioned equidistant from each other
			// and the top and bottom of the logcell.  Let's calculate the
			// Y positions of the top left corner of each field based on
			// that.
			y1 = (getHeight() - ascent - ascent) / 3;
			y2 = 2 * y1 + ascent;

			// Now decide on where the X position of each field is, depending
			// on alignment
			switch (alignment)
			{
			case CENTER:								// $[TI13]
				// Center each within logcell width.
				x1 = (getWidth() - width1) / 2;
				x2 = (getWidth() - width2) / 2;
				break;

			case RIGHT:									// $[TI14]
				// Position so that 'margin' pixels are left at the right
				// of text.
				x1 = getWidth() - margin - width1;
				x2 = getWidth() - margin - width2;
				break;

			case LEFT:									// $[TI15]
				// Position text 'margin' pixels from left edge.
				x1 = margin;
				x2 = margin;
				break;

			default:
				AUX_CLASS_ASSERTION_FAILURE(alignment);
				break;
			}

			// position the fields
			field1_.setX(x1);
			field1_.setY(y1);

			field2_.setX(x2);
			field2_.setY(y2);

			// Display both fields.
			field1_.setShow(TRUE);
			field2_.setShow(TRUE);
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LogCell::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LOGCELL,
  				lineNumber, pFileName, pPredicate);
}
