#ifndef DibInfo_HH
#define DibInfo_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: DibInfo -  Contains control structure for 840 bitmap images
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/DibInfo.hhv   25.0.4.0   19 Nov 2013 14:37:28   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 002   By: srp    Date:  28-May-2002    DCS Number: 5905
//  Project: VCP
//  Description:  Indentation of data structure
//
//  Revision: 001   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
#include "BitmapInfoHeader.hh"
#include "RgbQuad.hh"
//@ End-Usage

struct DibInfo
{
    BitmapInfoHeader  biHeader;
    RgbQuad *         pRgbQuads;
    const Uint32 *    pBitmapBits;
    Boolean           isTransparent;
    Byte              transparentColor; 
    Boolean           areColorsAllocated;
};

#endif // DibInfo_HH 
