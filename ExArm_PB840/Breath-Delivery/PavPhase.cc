#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PavPhase - Implements proportional assist ventilation inspiration.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is derived from CompensationBasedPhase class.  Protected
//	virtual methods are implemented to support the base class.
//---------------------------------------------------------------------
//@ Rationale
//	This class implements the algorithms for proportional assist
//	inspirations.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The data members values from the base class are defined	in the pure
//		virtual methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/PavPhase.ccv   10.7   08/17/07 09:40:42   pvcs  
//
//@ Modification-Log
//
//  Revision: 003   By:   gdc    Date: 02-Jan-2011       SCR Number: 6727
//  Project:  XENA2
//  Description:
//        Added missing #include file: IpcId.hh.
//
//  Revision: 002   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 001   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV initial version.
//
//=====================================================================

#include "PavPhase.hh"

#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "Tube.hh"
#include "PavManager.hh"
#include "IpcIds.hh"
#include "MsgQueue.hh"
#include "BdQueuesMsg.hh"
#include "BreathTrigger.hh"
#include "BreathPhaseScheduler.hh"
#include "PhaseRefs.hh"

//@ End-Usage

//#include "PrintQueue.hh"

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PavPhase()  
//
//@ Interface-Description
//		Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Call base class constructor.  Register callback with O2Mixture
//		and initialize data members.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PavPhase::PavPhase(void) : CompensationBasedPhase()
{
	CALL_TRACE("PavPhase::PavPhase(void)") ;

	// $[TI1]
	RO2Mixture.registerBreathPhaseCallBack( this) ;
	percentSupport_ = 1.0 ; // 100%
	numPaBreaths_ = 1 ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PavPhase()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PavPhase::~PavPhase(void)
{
	CALL_TRACE("PavPhase::~PavPhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called at the beginning of each PA breath to initialize the breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Call PavManager class to wrap up calculations from previous breath
//		and notify BreathData class to send data to GUI via message
//		queue.  Initialize data and call base class method. 
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavPhase::newBreath( void)
{
	CALL_TRACE("PavPhase::newBreath( void)") ;
	
	// $[TI1]
	RPavManager.newInspiration() ;

	const PavState::Id  PAV_STATE = RPavManager.getPavState();

	switch (PAV_STATE)
	{
	case PavState::STARTUP :			// $[TI1]
		if (RPavManager.isFirstPavBreath())
		{
			// $[TI1.1] -- first PAV breath...
			MsgQueue::PutMsg(::BREATH_DATA_Q,
							 BdQueuesMsg::SEND_PAV_STARTUP_DATA);
		}
		else if (RPavManager.isPlateauDataReady())
		{
			// $[TI1.2] -- not first PAV breath, and plateau data is
			//             available...
			// following a PAV plateau maneuver, send over the new
			// plateau data items...
			MsgQueue::PutMsg(::BREATH_DATA_Q,
							 BdQueuesMsg::SEND_PAV_PLATEAU_DATA);

			// reset plateau flag...
			RPavManager.resetPlateauDataFlag();
		}
		// $[TI1.3] -- not first PAV breath, and no plateau data available...
		break;

	case PavState::CLOSED_LOOP :		// $[TI2]
		if (RPavManager.isPlateauDataReady())
		{
			// $[TI2.1] -- plateau data is available...
			// following a PAV plateau maneuver, send over the new
			// plateau data items...
			MsgQueue::PutMsg(::BREATH_DATA_Q,
							 BdQueuesMsg::SEND_PAV_PLATEAU_DATA);

			// reset plateau flag...
			RPavManager.resetPlateauDataFlag();
		}
		else
		{
			// $[TI2.2] -- no plateau data is available...
			// send over the PAV breath data items for all non-plateau
			// breaths, during closed-loop control only...
			MsgQueue::PutMsg(::BREATH_DATA_Q,
							 BdQueuesMsg::SEND_PAV_BREATH_DATA);
		}
		break;

	default :
		AUX_CLASS_ASSERTION_FAILURE(PAV_STATE);
		break;
	}

	numPaBreaths_++ ;
	CompensationBasedPhase::newBreath() ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method
//		is called at the end of the PA inspiration to "wrap" up the breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Inform PavManager class if the breath was abnormally terminated.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavPhase::relinquishControl(  const BreathTrigger& trigger)
{
	CALL_TRACE("CompensationBasedPhase::relinquishControl(  \
						const BreathTrigger& trigger)") ;

	const Trigger::TriggerId  trigId = trigger.getId() ;

	// $[PA24043] a
	// $[PA24043] b
	// $[PA24043] c
	// $[PA24043] d
	// $[PA24043] e
	if (trigId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP ||
			trigId ==Trigger::HIGH_VENT_PRESSURE_EXP ||
			trigId ==Trigger::LUNG_VOLUME_EXP ||
			trigId ==Trigger::HIGH_PRESS_COMP_EXP ||
			trigId ==Trigger::BACKUP_TIME_EXP)
	{
		// $[TI1]
		RPavManager.paBreathTruncated( TRUE) ;
	}
	else
	{
		// $[TI2]
		RPavManager.paBreathTruncated( FALSE) ;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  activate(pPrevSpontPhase, pLostSpontPhase)
//
//@ Interface-Description
//      This method is called every time there is a change from one phase to
//      another.  The 'pPrevSpontPhase' pointer indicates the prevous
//      inspiratory phase, if any.  This call is to notify this instance that
//      it is becoming the new phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		This virtual method is overridden to start the PAV alarm timers.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
PavPhase::activate(const BreathPhase* pPrevSpontPhase,
				   const BreathPhase* pLostSpontPhase)
{
	CALL_TRACE("activate(pPrevSpontPhase, pLostSpontPhase)") ;

	// we need to "resync" alarms, that is, start PAV alarm timers...
	RPavManager.resyncAlarms();

	if (pPrevSpontPhase == (BreathPhase*)&RPeepRecoveryPhase)
	{	// $[TI1] -- just finished up a PEEP recovery phase...
		if (pLostSpontPhase == this)
		{	// $[TI1.1] -- the PAV phase was active at the time of the trigger
			//             event that led to the PEEP recovery...
			const SchedulerId::SchedulerIdValue  PREV_SCHEDULER_ID
							= BreathPhaseScheduler::GetPreviousSchedulerId();

			if (PREV_SCHEDULER_ID != SchedulerId::DISCONNECT)
			{	// $[TI1.1.1]
				// $[PA24030] c
				// $[PA24030] d
				// $[PA24033]
				// clear all PAV states, and re-initialize PAV data members...
				RPavManager.reset();
			}
			else
			{	// $[TI1.1.2] -- transitioned from Disconnect scheduler...
				RPavManager.recoveryFromDisconnect() ;
			}
		}
		else
		{	// $[TI1.2] -- the PAV phase was NOT the active phase at the time
			//             of the trigger event that led to the PEEP recovery...
			// clear all PAV states, and re-initialize PAV data members...
			RPavManager.reset();
		}
	}
	else
	{	// $[TI2] -- previous phase was NOT a PEEP recovery phase...
		// $[PA24030] c
		// $[PA24033]
		RPavManager.reset() ;
	}

	// send over PAV state info...
	MsgQueue::PutMsg(::BREATH_DATA_Q,
					 BdQueuesMsg::SEND_PAV_STARTUP_DATA);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  deactivate(triggerId)
//
//@ Interface-Description
//      This method is called every time there is a change from one phase to
//      another.  The 'triggerId' parameter identifies the trigger event that
//		is causing the change away from this phase.  This call is to notify
//		this instance that it is no longer the current phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		This virtual method is overridden to ensure that the PAV alarms
//		are reset.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
PavPhase::deactivate(const Trigger::TriggerId triggerId)
{
	CALL_TRACE("deactivate(triggerId)") ;

	// always need to "reset" PAV alarms, that is, ensure that all PAV alarms
	// are off...
	RPavManager.resetAlarms();

	switch (triggerId)
	{
	case Trigger::APNEA :
	case Trigger::OCCLUSION :
	case Trigger::SETTING_CHANGE_MODE :
	case Trigger::SVO :					// $[TI1]
		// clear all PAV states, and re-initialize PAV data members...
		RPavManager.reset();
		break;
	case Trigger::DISCONNECT :			// $[TI2]
	default :
		// do nothing -- retain current PAV state...
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PavPhase::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PAVPHASE,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineResistivePressure_()
//
//@ Interface-Description
//		This method has lung flow as an argument and returns nothing.
//		This method is called by the base class to obtain the resistive
//		component of the pressure trajectory.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The resistive pressure is the pressure drop across the tube and
//		the lung resistance.  This pressure is scaled by the percent support
//		level.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavPhase::determineResistivePressure_( const Real32 lungFlow)
{
	CALL_TRACE("PavPhase::determineResistivePressure_( const Real32 lungFlow)") ;

	// $[PA24004]
	// $[PA24031]

	const PavState::Id state = RPavManager.getPavState() ;
	
	resistivePressure_ = RTube.getPressDrop(lungFlow) 
                        + RPavManager.getRPatient( state) * lungFlow ;
	resistiveTarget_   = resistivePressure_ ;

	// $[PA24026]
	if (state != PavState::STARTUP)
	{
		// $[TI1]
		resistivePressure_ *= percentSupport_ ;
	}
	// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineElasticPressure_()
//
//@ Interface-Description
//		This method has lung volume as an argument and returns nothing.
//		This method is called by the base class to obtain the elasic
//		component of the pressure trajectory.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The elastic pressure is the lung volume divided by compliance.
//		This pressure is scaled by the percent support level.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavPhase::determineElasticPressure_( const Real32 lungVolume)
{
	CALL_TRACE("PavPhase::determineElasticPressure_( const Real32 lungVolume)") ;

	// $[PA24004]
	// $[PA24031]
	const PavState::Id state = RPavManager.getPavState() ;
	
	elasticPressure_ = lungVolume * RPavManager.getEPatient( state) ;
	elasticTarget_   = elasticPressure_ ;

	// $[PA24026]
	if (state != PavState::STARTUP)
	{
		// $[TI1]
		elasticPressure_ *= percentSupport_ ;
	}
	// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePercentSupport_()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method
//		is called by the base class to update the percent suport level.
//---------------------------------------------------------------------
//@ Implementation-Description
//		If the current percent support is less than the setting, increase the
//		current setting by 10% every other breath until the setting value is
//		achieved.  Otherwise, update the current percent support with the
//		setting immediately.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavPhase::updatePercentSupport_( void)
{
	CALL_TRACE("PavPhase::::updatePercentSupport_( void)") ;
	
	Real32 phasedInValue = PhasedInContextHandle::GetBoundedValue(
				SettingId::PERCENT_SUPPORT).value / 100 ;

	// $[PA24044]
	if (percentSupport_ < phasedInValue)
	{
		// $[TI1]
		// $[PA24045]
		if (!(numPaBreaths_ % 2))
		{
			// $[TI2]
			percentSupport_ += 0.1 ;

			if (percentSupport_ > phasedInValue)
			{
				// $[TI3]
				percentSupport_ = phasedInValue ;
			}
			// $[TI4]
		}
		// $[TI5]
	}
	else
	{
		// $[TI6]
		percentSupport_ = phasedInValue ;
		numPaBreaths_ = 1 ;
	}
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


