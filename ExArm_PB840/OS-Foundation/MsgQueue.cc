#include "stdafx.h"
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MsgQueue - A handle class derived from IpcQueue class for
// passing Int32 messages between tasks.
//---------------------------------------------------------------------
//@ Interface-Description
//  The MsgQueue class can be used for inter-task communication,
//  synchronization and buffering using Int32's.  MsgQueues
//  are created with an Identification number, length, and name as defined 
//  in IpcTableDef.cc. When a MsgQueue is created it becomes a handle to 
//  the VRTX32 queue ( a managed fixed length buffer referenced by a queue 
//  ID number ). After a MsgQueue object is created, Int32's can be
//  safely transmitted between tasks, using putMsg() or putPriorityMsg() to
//  place a message in the queue or at the front of the queue, and 
//  using pendForMsg() or acceptMsg() to retrieve messages from the 
//  MsgQueue. If a queue full error is returned this is considered to be a
//  fatal error.
//---------------------------------------------------------------------
//@ Rationale
//  The MsgQueue class was written to hide the details of using the VRTX32  
//  queue mechanism, to isolate problems related to inter-task communication,
//  to create a safe and simple-to-use interface for passing messages between
//  tasks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The MsgQueue class publicly inherits from the IpcQueue class to take
//  advantage of the IpcQueue interface and functionality.
//  MsgQueue uses the VRTX32 sc_q* calls to create and communicate to 
//  queues, which live in the VRTX32 Workspace.  
//---------------------------------------------------------------------
//@ Fault-Handling
//  Methods that can produce recoverable errors, such as Ipc::NO_MESSAGE() 
//  from acceptMsg() or Ipc::TIMED_OUT from pendForMsg() will be handled
//  by the calling routine. Return codes are listed in enum Ipc::ReturnCode. 
//  If a message was successfully received or sent the Returncode Ipc::OK 
//  is returned. Assertions are used to catch fault conditions, along with 
//  pre and post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  (QID_ > 0 && QID_ < IpcId::MAX_QID)
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/MsgQueue.ccv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 002   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  djl    Date:  6-Oct-1993    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//=====================================================================

#include "Sigma.hh"
#include "OsFoundation.hh"
#include "MsgQueue.hh"

#ifdef SIGMA_DEVELOPMENT
#include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT

// ==============
// MsgQueue class
// ==============
 
//@ Usage-Classes
#include "IpcTable.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//@ Method: MsgQueue() 
//
//@ Interface-Description
//    Constructs a MsgQueue object which will be a handle to a
//    queue created at initialization by IpcQueue::Create_().
//---------------------------------------------------------------------
//@ Implementation-Description
//    Calls the IpcQueue base class constructor with a MSG_QUEUE type
//    and queue ID.  All the actual VRTX queues are created at
//    initialization time with IpcQueue::Create_(), and this constructor
//    calls IpcQueue's constructor that simply creates a handle to the queue.
//---------------------------------------------------------------------
//@ PreCondition
//  The qId will be checked for validity in IpcQueue.
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
MsgQueue::MsgQueue(const Int32 qId) : IpcQueue(Ipc::MSG_QUEUE, qId)
{
    error_ = Ipc::OK;		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MsgQueue 
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
MsgQueue::~MsgQueue()
{
    // don't delete Queue, somebody else might be using it.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
// This method creates all the IpcQueues of type MSG_QUEUE in the
// IpcTable[] array.  It calls the protected method IpcQueue::Create_()
// to actually create the VRTX32 queue.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls IpcQueue::Create with the id, length, and wait order specified
//  for each entry in IpcTable defined as a MSG_QUEUE type. Wait order
//  is defaulted to PRIORITY_ORDER.
//---------------------------------------------------------------------
//@ PreCondition
//    (IpcTable::IsInitialized() == TRUE)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void MsgQueueList::Initialize(void)
{
    CLASS_PRE_CONDITION( IpcTable::IsInitialized() );

    for (Int32 i=0; i<IpcTable::GetNumEntries(); i++)
    {
      const IpcTableEntry& ipcEntry = IpcTable::GetIpcTableEntry(i);
      if (ipcEntry.type == Ipc::MSG_QUEUE)
      {						// $[TI1]
          IpcQueueList::Create_(&ipcEntry);
      }						// $[TI2]
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: putMsg()
//
//@ Interface-Description
//  Puts the message on the end of the queue depending on the success of
//  the operation. Takes a constant Int32 msg as the input parameter. 
//  It returns Ipc::OK on success. If a queue full error is returned this
//  is considered to be a fatal error.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls WriteMsgQueue for "this" queue object handle
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32 MsgQueue::putMsg(const Int32 msg)
{
#if defined(_WIN32)	|| defined(_WIN32_WCE)
	HANDLE txHandle = getHandle()->txQhandle;
	AUX_CLASS_ASSERTION(txHandle != NULL, QID_)
	if(!WriteMsgQueue(txHandle, (LPVOID)&msg, IpcQueue::IPC_MSG_SIZE, Ipc::NO_WAIT, 0))
	{
		DWORD err = GetLastError();
		error_ = mapOsIpcError(err);
	}
	else
	{
		error_ = Ipc::OK;
	}
#else
	error_ = Ipc::BAD_QID;
#endif
	Uint32 auxErrorCode = error_ | QID_<<24;
    AUX_CLASS_ASSERTION(error_ == Ipc::OK, auxErrorCode );

    return error_;			// $[TI1]

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PutMsg()
//
//@ Interface-Description
//  Puts the message on the end of the queue.
//  Takes a constant Int32 msg and a constant Int32 qId and returns an Int32;
//  Ipc::OK on success. If a queue full error is returned this
//  is considered to be a fatal error.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls WriteMsgQueue for the queue object handle corresponding the the
//  passed ID in the argument
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32 MsgQueueList::PutMsg(const Int32 qId, const Int32 msg)
{
	Int32 error = Ipc::OK;

#if defined(_WIN32)	|| defined(_WIN32_WCE)
	HANDLE txHandle = IpcQueueList::GetQueueById(qId)->txQhandle; //this is faster than instanciating a MsgQueue
	AUX_CLASS_ASSERTION(txHandle != NULL, qId)
	if(!WriteMsgQueue(txHandle, (void*)&msg, IpcQueue::IPC_MSG_SIZE, Ipc::NO_WAIT, 0))
	{
		DWORD ret = GetLastError();
		error = IpcQueue::mapOsIpcError(ret);
	}
#else
	error = Ipc::BAD_QID;
#endif
	Uint32 auxErrorCode = error | qId<<24;
    AUX_CLASS_ASSERTION(error == Ipc::OK, auxErrorCode );

    return error;  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
MsgQueue::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
	FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::MSG_QUEUE,
							lineNumber, pFileName, pPredicate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  MsgQueueList::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
MsgQueueList::SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName, 
							const char*       pPredicate)
{
	FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::MSG_QUEUE,
							lineNumber, pFileName, pPredicate);
}
