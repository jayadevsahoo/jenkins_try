#ifndef Tube_HH
#define Tube_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Tube - The class for central location to store related tube 
//		data.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Tube.hhv   25.0.4.0   19 Nov 2013 14:00:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  yyy    Date:  07-Jan-1999    DR Number: DCS 5322
//       Project:  ATC (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "TubeDataType.hh"
#include "TubeTypeValue.hh"
#include "PhasedInContextHandle.hh"

//@ End-Usage


class Tube
{
  public:

    Tube(void) ;
    virtual ~Tube( void) ;

	static void SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL);

	void updateTube(void);
    Real32 getPressDrop(Real32 lungFlow) const;
    inline Real32 getId(void) const;
    inline TubeTypeValue::TubeTypeValueId getType(void) const;

  protected:

  private:
    Tube( const Tube&) ;    	// not implemented
    void operator=( const Tube&) ;	// not implemented

	inline void updateId_(void);
	inline void updateType_(void);
	
	//@ Data-Member: trachResistance_
	// To hold the value for trach resistance.
	Real32 trachResistance_;

	//@ Data-Member: tubeId_
	// To hold the tube inner diameter size.
	Real32 tubeId_;

	//@ Data-Member: tubeType_
	// To hold the value for tube type.
	TubeTypeValue::TubeTypeValueId tubeType_;

	//@ Data-Member: tubeIndex_
	// To hold the index of the TubeTable to retrieve the total
	// intercept and slope values.
	Uint32 tubeIndex_;

} ;

// Inlined methods
#include "Tube.in"

#endif // Tube_HH 
