#ifndef AlarmSetupSubScreen_HH
#define AlarmSetupSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmSetupSubScreen - The sub-screen in the Lower screen
// selected by pressing the Alarm Setup button.  It displays the
// five alarm sliders allowing the operator to adjust alarm limits.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmSetupSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 009   By:  rhj    Date: 15-Mar-2011   SCR Number: 6755 
//  Project:  PROX
//  Description:
//      Changed member variable isProxReady_ to isProxInStartupMode_    
// 
//  Revision: 008   By:  rhj    Date: 22-Dec-2010   SCR Number: 6631
//  Project:  PROX
//  Description:
//      Added isProxReady_ member variable.
//  
//  Revision: 007   By:   rhj   Date: 23-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Modified to display PROX symbols when PROX is enabled.
//
//  Revision: 006  By:  heatherw Date:  38-Mar-2002    DCS Number:  5790
//  Project:  VCP
//  Description:
//      Added a new method to change the text of the inspired tidal volume
//      slider.
//      
//  Revision: 005  By:  sph	   Date:  08-Feb-2000    DCS Number:  5504
//  Project:  NeoMode
//  Description:
//      Added recent-activity range to alarm bars whereby the standard deviation 
//      the last N consecutive breaths is displayed.
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By:  hhd	   Date:  01-Feb-1999    DCS Number: 
//  Project:  ATC
//  Description:
//	Initial version.
//		
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "AlarmSlider.hh"
#include "SubScreenTitleArea.hh"
#include "BatchSettingsSubScreen.hh"
#include "BdEventRegistrar.hh"
#include "BdEventTarget.hh"

//@ End-Usage

class AlarmSetupSubScreen : public BatchSettingsSubScreen,
	                        public BdEventTarget
{
public:
	AlarmSetupSubScreen(SubScreenArea *pSubScreenArea);
	~AlarmSetupSubScreen(void);

	// Redefine SubScreen methods
	virtual void acceptHappened(void);

	void hideAllDataPointers(void);
	void hideAllStdDeviationBoxes(void);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	virtual void alarmEventHappened(void);

	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	// BdEventTarget virtual method...
	virtual void  bdEventHappened(EventData::EventId     eventId,
								  EventData::EventStatus eventStatus,
								  EventData::EventPrompt eventPrompt =
												EventData::NULL_EVENT_PROMPT);

protected:
	// BatchSettingsSubScreen virtual method...
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									  );

private:
	// these methods are purposely declared, but not implemented...
	AlarmSetupSubScreen(void);						// not implemented...
	AlarmSetupSubScreen(const AlarmSetupSubScreen&);// not implemented...
	void   operator=(const AlarmSetupSubScreen&);	// not implemented...
	void setAlarmSetupTabTitleText_( StringId stringId );
	Real32 maximum_(Real32, Real32);
	Real32 minimum_(Real32, Real32);

	void hideChangedDataPointers_(void);
    void updateInspTidalVolumeSlider_(void);
    Boolean isProxActive_(void);

	//@ Data-Member: peakCircuitPressureSlider_
	// The alarm slider for adjusting the Maximum Circuit Pressure alarm.
	AlarmSlider peakCircuitPressureSlider_;

	//@ Data-Member: respiratoryRateSlider_
	// The alarm slider for adjusting the Maximum Respiratory Rate alarm.
	AlarmSlider respiratoryRateSlider_;

	//@ Data-Member: exhaledMinuteVolumeSlider_
	// The alarm slider for adjusting the Exhaled Minute Volume range
	// alarms.
	AlarmSlider exhaledMinuteVolumeSlider_;

	//@ Data-Member: exhaledMandTidalVolumeSlider_
	// The alarm slider for adjusting the Minimum Mandatory Exhaled Tidal
	// Volume and the Maximum Exhaled Tidal Volume alarms.
	AlarmSlider exhaledMandTidalVolumeSlider_;

	//@ Data-Member: exhaledSpontTidalVolumeSlider_
	// The alarm slider for adjusting the Minimum Spontaneous Exhaled Tidal
	// Volume and the Maximum Exhaled Tidal Volume alarms.  This slider
	// is not shown if the current Mode is Assist/Control.
	AlarmSlider exhaledSpontTidalVolumeSlider_;

	//@ Data-Member: inhaledSpontTidalVolumeSlider_
	// The alarm slider for adjusting the Minimum Spontaneous Inhaled Tidal
	// Volume and the Maximum Inhaled Tidal Volume alarms.  This slider
	// is not shown if the current Mode is Assist/Control.
	AlarmSlider inhaledSpontTidalVolumeSlider_;

	//@ Data-Member: sliderOffset_
	// X-offset to the first alarm slider in The AlarmSetupSubScreen
	Int32 sliderOffset_;

	//@ Data-Member: sliderGap_
	// Horizontal gap between alarm sliders in The AlarmSetupSubScreen
	Int32 sliderGap_;

	enum { MAX_ALARM_SLIDERS_ = 6 };

	//@ Data-Member:  arrSettingButtonPtrs_
	// The subscreen buttons.
	AlarmSlider*  arrAlarmSliderPtrs_[MAX_ALARM_SLIDERS_ + 1];

	//@ Data-Member: isProxInop_
	// Is proximal board inoperative?
	Boolean isProxInop_;

	//@ Data-Member: isProxEnabled_
	// Is prox enabled?
	Boolean isProxEnabled_;

	//@ Data-Member: isProxInStartupMode_
	// Is proximal board in startup mode
	Boolean isProxInStartupMode_;

};

#endif // AlarmSetupSubScreen_HH 
