#include "stdafx.h"
//----------------------------------------------------------------------------
//            Copyright (c) 2012 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

/// @file XReal.cc

#include "XReal.h"

using namespace swat;

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
YReal::YReal(float &m, int iSensorId)
    : m_iSensorId(iSensorId)
{
    // This Assertion is to catch if we are using the same sensor Id for different
    // Sensor Objects.  You can only instantiate one Sensor object per sensor Id.
    // Also, 0 is analogous to an INVALID Id and that is allowed to be used as many
    // time as desired.
    CLASS_PRE_CONDITION((VirtualSensorBuffer::m_data[m_iSensorId].pValue ==
                        &VirtualSensorBuffer::m_data[m_iSensorId].rValue) ||
                        (m_iSensorId == 0));

    VirtualSensorBuffer::m_data[m_iSensorId].pValue=(VirtualSensor::SensorData *)&m;
    VirtualSensorBuffer::m_data[m_iSensorId].bIsSet=false;
    VirtualSensorBuffer::m_data[m_iSensorId].bIsAdd=false;
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
YReal::~YReal()
{
    VirtualSensorBuffer::m_data[m_iSensorId].pValue=&VirtualSensor::g_rValue;
    VirtualSensorBuffer::m_data[m_iSensorId].bIsSet=false;
    VirtualSensorBuffer::m_data[m_iSensorId].bIsAdd=false;
}

//------------------------------------------------------------------------------
// update
//------------------------------------------------------------------------------
void YReal::update()
{
    if(VirtualSensorBuffer::m_data[m_iSensorId].bIsSet == true)
    {
        (VirtualSensorBuffer::m_data[m_iSensorId].pValue)->f
            =(&VirtualSensorBuffer::m_data[m_iSensorId].value)->f;
    }

    else if(VirtualSensorBuffer::m_data[m_iSensorId].bIsAdd == true)
    {
        (VirtualSensorBuffer::m_data[m_iSensorId].pValue)->f
            +=(&VirtualSensorBuffer::m_data[m_iSensorId].offset)->f;
    }
}

//------------------------------------------------------------------------------
// SOFT_FAULT_IMPL
//------------------------------------------------------------------------------
SOFT_FAULT_IMPL(YReal, SWAT, YREAL);


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
YRaw::YRaw(int &m, int iSensorId)
    : m_iSensorId(iSensorId)
{
    // This Assertion is to catch if we are using the same sensor Id for different
    // Sensor Objects.  You can only instantiate one Sensor object per sensor Id.
    // Also, 0 is analogous to an INVALID Id and that is allowed to be used as many
    // time as desired.
    CLASS_PRE_CONDITION((VirtualRawSensorBuffer::m_data[m_iSensorId].pValue ==
                         &VirtualRawSensorBuffer::m_data[m_iSensorId].rValue) ||
                        (m_iSensorId == 0));

    VirtualRawSensorBuffer::m_data[m_iSensorId].pValue=(VirtualSensor::SensorData *)&m;
    VirtualRawSensorBuffer::m_data[m_iSensorId].bIsSet=false;
    VirtualRawSensorBuffer::m_data[m_iSensorId].bIsAdd=false;
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
YRaw::~YRaw()
{
    VirtualRawSensorBuffer::m_data[m_iSensorId].pValue=&VirtualSensor::g_rValue;
    VirtualRawSensorBuffer::m_data[m_iSensorId].bIsSet=false;
    VirtualRawSensorBuffer::m_data[m_iSensorId].bIsAdd=false;
}

//------------------------------------------------------------------------------
// update
//------------------------------------------------------------------------------
void YRaw::update()
{
    if(VirtualRawSensorBuffer::m_data[m_iSensorId].bIsSet == true)
    {
        (VirtualRawSensorBuffer::m_data[m_iSensorId].pValue)->ui=
            (&VirtualRawSensorBuffer::m_data[m_iSensorId].value)->ui;
    }

    else if(VirtualRawSensorBuffer::m_data[m_iSensorId].bIsAdd == true)
    {
        (VirtualRawSensorBuffer::m_data[m_iSensorId].pValue)->ui+=
            (&VirtualRawSensorBuffer::m_data[m_iSensorId].offset)->i;
    }
}

//------------------------------------------------------------------------------
// SOFT_FAULT_IMPL
//------------------------------------------------------------------------------
SOFT_FAULT_IMPL(YRaw, SWAT, YRAW);
