 
#ifndef ShadowTraceEnableSetting_HH
#define ShadowTraceEnableSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  ShadowTraceEnableSetting - Used to enable/disable shadow traces
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ShadowTraceEnableSetting.hhv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: emccoy  Date:  14-Apr-2002    DR Number:  5848
//  Project:  VCP
//  Description:
//	Added for supporting the enabling/disabling of shadow traces.
//
//====================================================================

//@ Usage-Classes
#include "NonBatchDiscreteSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class ShadowTraceEnableSetting : public NonBatchDiscreteSetting, SettingObserver
{
  public:
    ShadowTraceEnableSetting(void);
    virtual ~ShadowTraceEnableSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getDefaultValue(void) const;

    // SettingObserver methods...
    virtual void  applicabilityUpdate(
			      const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject
				     );
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;

    virtual void  settingObserverInit(void);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    ShadowTraceEnableSetting(const ShadowTraceEnableSetting&); // not implemented...
    void  operator=(const ShadowTraceEnableSetting&);  // not implemented...
};


#endif // ShadowTraceEnableSetting_HH 
