#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SmDatakey - Service Mode Datakey functions
//---------------------------------------------------------------------
//@ Interface-Description
//
//	This class implements the datakey service functions, which set the
//	datakey contents to the current values stored in NOVRAM/FLASH as
//  well as setting the vent NOVRAM/FLASH to the datakey contents.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the datakey service functions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method setDatakeyData() is responsible for setting the
//  datakey to the current values in NOVRAM/FLASH.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created using the static
//  function SmDatakey::GetSmDatakey().
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmDatakey.ccv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  gdc    Date:  18-Feb-2009    SCR Number: 6480
//  Project:  840S
//  Description:
//		Implemented "set vent head operational hours" test.
//
//  Revision: 004  By: gdc    Date: 16-Feb-2009    SCR Number: 6019
//  Project:  840S
//  Description:
//         Modified to increase data key update reliability. Added
//         DataKeyPower class constructor to turn power on to device
//         for entire service mode operation. Modified to use new
//         updateCache() method instead of accessing DataKey private
//         member functions.
//
//  Revision: 003  By: erm    Date: 19-Dec-2002     DR Number: 6028
//  Project:  EMI
//  Description:
//         Added method setMfgDatakeyToProdDatakey() to convert
//         Mfg datakey to Production during service-mode
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 001  By:  gdc    Date: 23-Sep-1997    DR Number: 2513
//       Project:  Sigma (R8027)
//       Description:
//           Initial version to support service/manufacturing test.
//
//=====================================================================

#include "SmDatakey.hh"

//@ Usage-Classes
#include <stdio.h>              // for sscanf
#include "BackgroundMaintApp.hh"
#include "DataKey.hh"
#include "NovRamManager.hh"
#include "SerialNumber.hh"
#include "SmManager.hh"
#include "SmRefs.hh"
#include "Task.hh"
#include "VentObjectRefs.hh"
#include "BDIORefs.hh"
#include "Configuration.hh"
#include "SoftwareOptions.hh"

#ifdef SIGMA_DATAKEY_TEST
    #include <syslog.h>
    #define SYSLOG(x) syslog x
#else
    #define SYSLOG(x)
#endif

//@ End-Usage

//@ Code...
static const Uint32 MINUTES_PER_HOUR = 60;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmDatakey()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SmDatakey::SmDatakey(void)
{
	CALL_TRACE("SmDatakey::SmDatakey(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SmDatakey()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmDatakey::~SmDatakey(void)
{
	CALL_TRACE("SmDatakey::~SmDatakey(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSmDatakey() 
//
//@ Interface-Description
//	Static function returns a reference to the SmDatakey.
//  On the first call, instantiates SmDatakey and returns
//  a reference to the object. Subsequent calls return reference to
//  the instantiated SmDatakey.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

const SmDatakey &
SmDatakey::GetSmDatakey(void)
{
	CALL_TRACE("SmDatakey::GetSmDatakey(void)");

    static SmDatakey  datakey;

    // $[TI1]
    return datakey;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDatakeyData()
//
//@ Interface-Description
//	Copies data key values in FLASH and NOVRAM to the data key.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Check if datakey is present, if not, send "not installed"
//	message to gui and exit. Otherwise, use the DataKey class to 
//  write the NOVRAM/FLASH data to the data key.
//
//  $[08053] Sigma shall provide the capability to program the data key
//  from the values stored in the ventilator's memory (i.e. FLASH, NOVRAM)
//  upon request.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SmDatakey::setDatakeyData(void) const
{
	CALL_TRACE("SmDatakey::setDatakeyData(void)");

// $[TI2]
    BackgroundMaintApp::IssueNewSemphCommand( 
                                BackgroundMaintApp::REQUEST_TO_SUSPEND) ;
    // wait until task is suspended
    while (   BackgroundMaintApp::RetrieveCurrentSemphStatus()          
           != BackgroundMaintApp::TASK_SUSPENDED)
    {
        Task::Delay(0, 10) ;
    }


    Uint32 ventMinutes = NovRamManager::GetSystemOperationalMinutes();
    RDataKey.writeVentHeadOperationalHours(ventMinutes / MINUTES_PER_HOUR );

    SerialNumber  ventBdSerialNumber(
    						RSystemConfiguration.getFlashSerialNumber());
    RDataKey.setSerialNumber( DataKey::BD, ventBdSerialNumber );

    // initialize datakey serial number from flash S/N sent from GUI 
    SerialNumber  ventGuiSerialNumber( ServiceMode::GetGuiSerialNumber() );
    RDataKey.setSerialNumber(  DataKey::GUI , ventGuiSerialNumber );

    const SerialNumber& keyBdSN = RDataKey.getSerialNumber( DataKey::BD );
    const SerialNumber& keyGuiSN = RDataKey.getSerialNumber( DataKey::GUI );

    Uint32 keyHours = RDataKey.readVentHeadOperationalHours();
    BackgroundMaintApp::IssueNewSemphCommand( BackgroundMaintApp::TASK_RESUME);

    if (   keyBdSN  != ventBdSerialNumber
        || keyGuiSN != ventGuiSerialNumber
        || keyHours != ventMinutes / MINUTES_PER_HOUR )
    {
	// $[TI2.2]
         RSmManager.sendStatusToServiceData(
             SmStatusId::TEST_FAILURE,
             SmStatusId::CONDITION_1);
    }
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setMfgDatakeyToProdDatakey()
//
//@ Interface-Description
//  Accepts no parameters and returns nothing. This method, invoked
//  during service-mode from the development options sub-screen, 
//  converts the data key to a standard enhanced production data key
//  with no options enabled.
//	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Check if datakey is present, if not, send "not installed" message
//	to the GUI and exit. Otherwise, set the current software option
//  to a standard (enhanced) production key, produce and write the 
//  validation code. 
//
//  $[EM00400] change the data key type from Manufacturing to Enhanced
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SmDatakey::setMfgDatakeyToProdDatakey(void) const
{
	CALL_TRACE("SmDatakey::setMfgDatakeyToProdDatakey(void)");

	//TODO: BVP - E600 Need to change this code when the new SoftwareOptions
	//class is integrated. The new class allows setting the key type and generating
	//new key. What you store on the data key is the new generated string key,
	//not the binary form.
	//For now, just ignore the whole request.
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setVentHeadOperationalHours(Uint32)
//
//@ Interface-Description
//  Sets the vent head operational hours to the value specified. Writes
//  this value to non-volatile memory as well as the data key.
//	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Check if datakey is present, if not, send "not installed"
//	message to gui and exit. Otherwise, write the specified operational
//  hours to non-volatile memory and to the data key. Write the hours
//  to the data key using the DataKey::setVentHeadOperationalHours
//  method, then call DataKey::syncVentOperationalTimes to synchronize
//  the time stored in NOVRAM with the time stored on the data key.
//
//  $[LC08003] The ventilator shall set the vent head operational hours 
//             to the value provided by the PC upon request.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SmDatakey::setVentHeadOperationalHours( const char * pParameters ) const
{
	Uint32 hours = 0;
	sscanf( pParameters, "%d", &hours );

	// first - suspend the background update cycle
	BackgroundMaintApp::IssueNewSemphCommand(BackgroundMaintApp::REQUEST_TO_SUSPEND) ;
	// and wait until task is suspended
	while ( BackgroundMaintApp::RetrieveCurrentSemphStatus() 
			!= BackgroundMaintApp::TASK_SUSPENDED )
	{
		Task::Delay(0, 10) ;
	}

	// write the hours specified to the data key
	SigmaStatus status = RDataKey.writeVentHeadOperationalHours(hours);

	if ( FAILURE == status )
	{
		RSmManager.sendStatusToServiceData(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_1);
		SYSLOG((LOG_DEBUG, "Test Failure: data key write failed"));
	}
	else
	{
		// sync the NOVRAM minutes with the hours just written to the data key
		RDataKey.syncVentOperationalTimes();
	}
	// resume the background update cycle
	BackgroundMaintApp::IssueNewSemphCommand( BackgroundMaintApp::TASK_RESUME);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 
void
SmDatakey::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SERVICE_MODE, SMDATAKEY,
		                      lineNumber, pFileName, pPredicate);

}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
