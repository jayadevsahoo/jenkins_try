#include "stdafx.h"
// ExArm.cpp : Defines the class behaviors for the application.
//

#include "ExArm.h"
#include "MainFrm.h"
#ifdef SIGMA_GUIPC_CPU
//GUI HW IO Simulation
#include "GuiHwSimulation.h"
#endif
#include <iostream>
//#include "StandardScreen.h"
//#include "AdvancedScreen.h"
//#include "DevelopmentDlg.h"
//#include "StartupStateDlg.h"
//#include "SystemErrorAlarm.h"
//#include "GlobalObjects.h"
//#include "DebugZones.h"
//#include "ThreadPriorities.h"
//#include "AnalogInputThread.h"
//#include "TopBaseWindow.h"
//#include "TopLevelWindow.h"
//#include "LanguageStringDialog.h"
//#include "SoftwareReleaseDate.h"
//#include "AlarmsThread.h"
//#include "IntraVentCommunicationThread.h"
//#include "RealTimeTimerThread.h"
//#include "GUITimerThread.h"
//#include "MonitorTimerThread.h"
//#include "LanguageSetting.h"
//#include "ExceptionHandler.h"


#include "TextButton.hh"
#include "InitScreen.hh"
#include "MiscStrs.hh"
#include "BaseContainer.hh"

#ifdef SIGMA_GUIPC_CPU
#include <mmsystem.h> //To see PlaySound
#endif

#include "Sys_Init.hh"
#include "GuiFoundation.hh"
#include "BaseContainer.hh"
#include "ChangeStateMessage.hh"
#include "GuiApp.hh"
#include "NV_MemoryStorageThread.hh"

#if defined (SIGMA_GUIPC_CPU)
extern BOOL gShutdownRequested;
#endif


//DBGPARAM dpCurSettings =
//{
//    _T("ExArm"),
//	{
//		_T("Errors"), _T("Warnings"), _T("Init"), _T("Function"),
//		_T("Ioctl"), _T("Device"), _T("Activity"), _T(""),
//		_T(""),_T(""),_T(""),_T(""),
//		_T(""),_T(""),_T(""),_T("")
//	},
//    MASK_ERROR | MASK_WARN | MASK_INIT | MASK_IOCTL | MASK_DEVICE
//};



/////////////////////////////////////////////////////////////////////////////
// CExArmApp

BEGIN_MESSAGE_MAP(CExArmApp, CWinApp)
	//{{AFX_MSG_MAP(CExArmApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExArmApp construction

CExArmApp::CExArmApp()
	: CWinApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CExArmApp object

CExArmApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CExArmApp initialization


//******************************************************************
//
//  CExArmApp::ConditionallyUpdateStartup - If we return from this, we didn't
//      update the ExStartup executable.
//
//******************************************************************

// DO NOT DELETE
//#define NEW_STARTUP_EXE_FILE        _T("\\FlashFX Disk\\Startup\\New\\ExStartup.exe")
//#define STARTUP_EXE_FILE            _T("\\FlashFX Disk\\Startup\\ExStartup.exe")
//#define BACKUP_EXE_FILE             _T("\\FlashFX Disk\\Startup\\Backup.sav")
static const int                    UI_SLEEP_TIME =   1000;

void CExArmApp::ConditionallyUpdateStartup()
{
    // If it is there, we need to get rid of the old one and rename the new to the old.

	// DO NOT DELETE
    //FILE *Stream;
    //if ((Stream = _wfopen(NEW_STARTUP_EXE_FILE, _T("rt"))) == NULL)
    //{
    //    return;
    //}

    //fclose(Stream);

    //RETAILMSG(TRUE, (_T("Found new ExStartup.\r\n")));

    //// Even though it shouldn't be here, delete the backup file in case it's here
    //// If we can't delete it, get out, something is very wrong.
    //if ((Stream = _wfopen(BACKUP_EXE_FILE, _T("rt"))) != NULL)
    //{
    //    fclose(Stream);
    //    RETAILMSG(TRUE, (_T("Deleting backup version. (Shouldn't be here!!)\r\n")));

    //    if (SetFileAttributes(BACKUP_EXE_FILE, FILE_ATTRIBUTE_NORMAL) == 0)
    //    {
    //        DWORD Error = GetLastError();
    //        RETAILMSG(TRUE, (_T("Error %ld setting file attributes.\r\n"), Error));
    //        return;
    //    }
    //    if (DeleteFile(BACKUP_EXE_FILE) == 0)
    //    {
    //        DWORD Error = GetLastError();
    //        RETAILMSG(TRUE, (_T("Error %ld deleting.\r\n"), Error));
    //        return;
    //    }
    //}

    //// Move the current file to the backup. If this doesn't work, get out.
    //RETAILMSG(TRUE, (_T("Save the original file.\r\n")));
    //if (SetFileAttributes(STARTUP_EXE_FILE, FILE_ATTRIBUTE_NORMAL) == 0)
    //{
    //    DWORD Error = GetLastError();
    //    RETAILMSG(TRUE, (_T("Error %ld setting file attributes.\r\n"), Error));
    //    return;
    //}
    //if (MoveFile(STARTUP_EXE_FILE, BACKUP_EXE_FILE) == 0)
    //{
    //    DWORD Error = GetLastError();
    //    RETAILMSG(TRUE, (_T("Error %ld saving the original file.\r\n"), Error));
    //    return;
    //}

    //RETAILMSG(TRUE, (_T("Save successful.\r\n")));

    //RETAILMSG(TRUE, (_T("Moving new version.\r\n")));
    //if (SetFileAttributes(NEW_STARTUP_EXE_FILE, FILE_ATTRIBUTE_NORMAL) == 0)
    //{
    //    DWORD Error = GetLastError();
    //    RETAILMSG(TRUE, (_T("Error %ld setting file attributes.\r\n"), Error));

    //    RETAILMSG(TRUE, (_T("Restore the original file.\r\n"), Error));
    //    if (MoveFile(BACKUP_EXE_FILE, STARTUP_EXE_FILE) == 0)
    //    {
    //        DWORD Error = GetLastError();
    //        RETAILMSG(TRUE, (_T("Error %ld restoring the original file.\r\n"), Error));
    //    }
    //    return;
    //}
    //if (MoveFile(NEW_STARTUP_EXE_FILE, STARTUP_EXE_FILE) == 0)
    //{
    //    DWORD Error = GetLastError();
    //    RETAILMSG(TRUE, (_T("Error %ld moving.\r\n"), Error));

    //    RETAILMSG(TRUE, (_T("Restore the original file.\r\n"), Error));
    //    if (MoveFile(BACKUP_EXE_FILE, STARTUP_EXE_FILE) == 0)
    //    {
    //        DWORD Error = GetLastError();
    //        RETAILMSG(TRUE, (_T("Error %ld restoring the original file.\r\n"), Error));
    //    }
    //    return;
    //}

    //// Move worked, now we can delete the backup. Even if it fails, we can restart, because the new file is gone.
    //if (DeleteFile(BACKUP_EXE_FILE) == 0)
    //{
    //    DWORD Error = GetLastError();
    //    RETAILMSG(TRUE, (_T("Error %ld deleting backup.\r\n"), Error));
    //}

    //// Only do the restart if everything was successful!
    //RETAILMSG(TRUE, (_T("Move successful.\r\n")));

    //RETAILMSG(TRUE, (_T("ExStartup update complete.\r\n")));

    //InitiateSystemRestart();                                // If this returns, we have a failure
}


//******************************************************************
//
//  CExArmApp::InitInstance
//
//******************************************************************

BOOL CExArmApp::InitInstance()
{
//    int NextScreen;
//#ifdef SIGMA_GUIPC_CPU
//	InitializeCriticalSection(&cs);
//#endif

//	WaveSettings =new WaveSettings_t();
//    // First thing, determine if the ExStartup.EXE has a new version.
//
//    ConditionallyUpdateStartup();
//
//    MEMORYSTATUS    memInfo;
//#ifndef SIGMA_GUIPC_CPU
//    STORE_INFORMATION  si;
//#endif
//    memInfo.dwLength = sizeof(memInfo);
//    GlobalMemoryStatus(&memInfo);
//#ifndef SIGMA_GUIPC_CPU
//    GetStoreInformation(&si);
//#endif
//
//    #ifdef DEBUG
//    RETAILMSG(ZONE_INIT,(_T(">>>> Debug build!!!\r\n")));
//    #else
//    RETAILMSG(ZONE_INIT,(_T(">>>> Retail build!!!\r\n")));
//    #endif
//    RETAILMSG(ZONE_INIT,(_T(">>>> Software Version : %s.%s.%s ***\r\n"), SoftwareReleaseDate.Get_Month_String(), SoftwareReleaseDate.Get_Year_String(), SoftwareReleaseDate.Get_Day_String()));
//
////    TouchScreen.Initialize();
//    RETAILMSG(ZONE_INIT,(_T(">>>> Process ID: 0x%x ***\r\n"), GetCurrentProcessId()));
//    RETAILMSG(ZONE_INIT,(_T(">>>> Main Thread ID: 0x%x ***\r\n"), GetCurrentThreadId()));
//
//
//    // Create the various threads.
//    EventsThread.CreateThread(0);
//    RETAILMSG(ZONE_INIT,(_T(">>>> Events Thread ID:                 0x%x , Priority %3d ***\r\n"), EventsThread.GetThreadId(),EVENTS_THREAD_PRIORITY ));
//    AlarmsThread.CreateThread(0);
//    RETAILMSG(ZONE_INIT,(_T(">>>> Alarms Thread ID:                 0x%x , Priority %3d ***\r\n"), AlarmsThread.GetThreadId(), ALARMS_THREAD_PRIORITY));
//    MonitorsThread.CreateThread(0);
//    RETAILMSG(ZONE_INIT,(_T(">>>> Monitors Thread ID:               0x%x , Priority %3d ***\r\n"), MonitorsThread.GetThreadId(), MONITORS_THREAD_PRIORITY));
//	NvDataStoreThread.CreateThread(0);
//    RETAILMSG(ZONE_INIT,(_T(">>>> NV Data Store Thread ID:          0x%x , Priority %3d ***\r\n"), NvDataStoreThread.GetThreadId(), NV_DATA_STORE_THREAD_PRIORITY));
// 	IntraVentCommunicationThread.CreateThread(0);
//    RETAILMSG(ZONE_INIT,(_T(">>>> IntraVentCommunication Thread ID:          0x%x , Priority %3d ***\r\n"), IntraVentCommunicationThread.GetThreadId(), COMMUNICATION_THREAD_PRIORITY));
//
//	IntraVentApplicationThread.CreateThread(0);
//    RETAILMSG(ZONE_INIT,(_T(">>>> IntraVentApplicationThread Thread ID:          0x%x , Priority %3d ***\r\n"), IntraVentApplicationThread.GetThreadId(), COMMUNICATION_THREAD_PRIORITY));
//    //GUICommThread.CreateThread(0);
//    //RETAILMSG(ZONE_INIT,(_T(">>>> GUICommThread Thread ID:          0x%x , Priority %3d ***\r\n"), GUICommThread.GetThreadId(), COMMUNICATION_THREAD_PRIORITY));
//
//    EventsThread.SetPriority(EVENTS_THREAD_PRIORITY);
//	AlarmsThread.SetPriority(ALARMS_THREAD_PRIORITY);
//    MonitorsThread.SetPriority(MONITORS_THREAD_PRIORITY);
//    NvDataStoreThread.SetPriority(NV_DATA_STORE_THREAD_PRIORITY);
//    //GUICommThread.SetPriority(GUICOMM_THREAD_PRIORITY);
//	IntraVentCommunicationThread.SetPriority(COMMUNICATION_DATA_THREAD_PRIORITY);
//	IntraVentApplicationThread.SetPriority(COMMUNICATION_DATA_THREAD_PRIORITY);
//	//This thread pumps graphic data into the queue and drives graphic waves updating. 
//	//let the real time thread runs at 0
//	RealTimeTimerThread.CreateThread(0);
//	RealTimeTimerThread.SetPriority(REAL_TIME_TIMER_THREAD_PRIORITY);
//    RETAILMSG(ZONE_INIT,(_T(">>>> RealTimeTimerThread Thread ID:                 0x%x , Priority %3d ***\r\n"), RealTimeTimerThread.GetThreadId(),ANALOG_INPUT_THREAD_PRIORITY ));
//
//	GUITimerThread.CreateThread(0);
//	GUITimerThread.SetPriority(GUI_TIMER_THREAD_PRIORITY);
//	RETAILMSG(ZONE_INIT,(_T(">>>> GUITimerThread Thread ID:                 0x%x , Priority %3d ***\r\n"), GUITimerThread.GetThreadId(),ANALOG_INPUT_THREAD_PRIORITY ));
//
//	MonitorTimerThread.CreateThread(0);
//	MonitorTimerThread.SetPriority(ANALOG_INPUT_THREAD_PRIORITY);
//	RETAILMSG(ZONE_INIT,(_T(">>>> GUITimerThread Thread ID:                 0x%x , Priority %3d ***\r\n"), GUITimerThread.GetThreadId(),ANALOG_INPUT_THREAD_PRIORITY ));


	/*   I2cThread.CreateThread(0);
    RETAILMSG(ZONE_INIT,(_T(">>>> I2C Thread ID:                    0x%x , Priority %3d ***\r\n"), I2cThread.GetThreadId(), I2C_THREAD_PRIORITY));
    ServosThread.CreateThread(0);
   RETAILMSG(ZONE_INIT,(_T(">>>> Servos Thread ID:                 0x%x , Priority %3d ***\r\n"), ServosThread.GetThreadId(), SERVOS_THREAD_PRIORITY));
   BreathThread.CreateThread(0);
    RETAILMSG(ZONE_INIT,(_T(">>>> Breath Thread ID:                 0x%x , Priority %3d ***\r\n"), BreathThread.GetThreadId(), BREATH_THREAD_PRIORITY));

	SmartIOThread.CreateThread(0);
    RETAILMSG(ZONE_INIT,(_T(">>>> SmartIO Thread ID:                0x%x , Priority %3d ***\r\n"), SmartIOThread.GetThreadId(), SMARTIO_THREAD_PRIORITY));
    ButtonsThread.CreateThread(0);
    RETAILMSG(ZONE_INIT,(_T(">>>> Buttons Thread ID:                0x%x , Priority %3d ***\r\n"), ButtonsThread.GetThreadId(), BUTTONS_THREAD_PRIORITY));
    RealTimeDataThread.CreateThread(0);
    RETAILMSG(ZONE_INIT,(_T(">>>> RealTimeData Thread ID:           0x%x , Priority %3d ***\r\n"), RealTimeDataThread.GetThreadId(), REALTIME_DATA_THREAD_PRIORITY));

    //IntraVentCommunicationThread.CreateThread(0);
	//fan test
    CommunicationDataThread.CreateThread(0);
    RETAILMSG(ZONE_INIT,(_T(">>>> Communication Data Thread ID:          0x%x , Priority %3d ***\r\n"), CommunicationThread.GetThreadId(), COMMUNICATION_THREAD_PRIORITY));
    CommunicationThread.CreateThread(0);
    RETAILMSG(ZONE_INIT,(_T(">>>> Communication Thread ID:          0x%x , Priority %3d ***\r\n"), CommunicationThread.GetThreadId(), COMMUNICATION_THREAD_PRIORITY));*    PulseOximeterComDriver.CreateThread(0);
    RETAILMSG(ZONE_INIT,(_T(">>>> PulseOximeterComDriver Thread ID: 0x%x , Priority %3d ***\r\n"), PulseOximeterComDriver.GetThreadId(), PULSE_OXIMETER_DRIVER_THREAD_PRIORITY));

    EventsThread.SetEvent(MSG_EVENTS_POWER_UP, 0, 0);
*/
   // memInfo.dwLength = sizeof(memInfo);
   // GlobalMemoryStatus(&memInfo);
#ifndef SIGMA_GUIPC_CPU
    GetStoreInformation(&si);
#endif
/*    I2cThread.SetPriority(I2C_THREAD_PRIORITY);
    ServosThread.SetPriority(SERVOS_THREAD_PRIORITY);

	BreathThread.SetPriority(BREATH_THREAD_PRIORITY);
	*/
	
    //SmartIOThread.SetPriority(SMARTIO_THREAD_PRIORITY);
    //ButtonsThread.SetPriority(BUTTONS_THREAD_PRIORITY);
    //CommunicationDataThread.SetPriority(COMMUNICATION_DATA_THREAD_PRIORITY);
    //CommunicationThread.SetPriority(COMMUNICATION_THREAD_PRIORITY);
    //IntraVentCommunicationThread.SetPriority(COMMUNICATION_THREAD_PRIORITY);
    //PulseOximeterComDriver.SetPriority(PULSE_OXIMETER_DRIVER_THREAD_PRIORITY);

    // Create this after the servos thread is up and running
    //AnalogInputThread.CreateThread(0);
    //RETAILMSG(ZONE_INIT,(_T(">>>> Analog input Thread ID: 0x%x ***\r\n"), AnalogInputThread.GetThreadId()));
    //AnalogInputThread.SetPriority(ANALOG_INPUT_THREAD_PRIORITY);

    //BroadcastMessage(MSG_BROADCAST_WAKEUP);

    // Don't show the cursor while we are running
    //ShowCursor(FALSE);
	ShowCursor(TRUE);

    //AddFontResource(ARIAL_BD_FONT_FILE_NAME);
    //AddFontResource(ARIAL_1_30_FONT_FILE_NAME);
    //DEBUG_ASSERT(AddFontResource(MSGOTHIC_FONT_FILE_NAME));
    //DEBUG_ASSERT(AddFontResource(SIMSUN_FONT_FILE_NAME));

    //while (!NvDataStoreThread.NvDataInitialized())
    //{
    //    Sleep(10);
    //}

    //Language_t CurrentLanguage = (Language_t)LanguageSetting.Get();

    //// try to load the language string for the 1st time
    //if(!TranslationStrings.SetNewLanguage(CurrentLanguage)) // do we fail to load the language for the 1st time?
    //{
    //    RETAILMSG(true,(_T("Language String CRC test failed at power up! Try to load English.str file!\r\n")));

    //    EventsThread.SetEvent(MSG_EVENTS_FILE_CRC_FAILURE, CurrentLanguage, 0);      // record a language File CRC failure event

    //    // try to switch language to English and try to load language string again!
    //    if(CurrentLanguage != LANGUAGE_ENGLISH)
    //    {
    //        LanguageSetting.Set(LANGUAGE_ENGLISH);

    //        Language_t NewLanguage = (Language_t)LanguageSetting.Get();

    //        if(!TranslationStrings.SetNewLanguage(NewLanguage))
    //        {
    //            RETAILMSG(true,(_T("Language String CRC test failed at power up! Failed to load English.str file!\r\n")));

    //            #ifndef DEBUG // only activate system error alarm, device alert led and a new dialog with error message if the the software is a release build

    //            // must set to State_Setting since CheckAlarmCondition is only available in STATE_SETTING and STATE_VENTILATING mode
    //            VentilatorStateSetting.Set(STATE_SETTINGS);

    //            SystemErrorAlarm.LanguageStringFileCRCTestFailureOccurred();

    //            LanguageStringDialog_t LanguageStringDialog;

    //            LanguageStringDialog.DoModal();                           // display a dialog to tell user the language string can not be loaded

    //            while(true)
    //            {
    //                Sleep(UI_SLEEP_TIME);
    //            }

    //            #endif
    //        }
    //        else
    //        {
    //            RETAILMSG(true,(_T("Language String CRC test failed at power up! English.str file was loaded successfully!\r\n")));
    //        }
    //    }
    //    else // the Current langauge is English and we can not load the English string file for the 1st time
    //    {
    //         RETAILMSG(true,(_T("Language String CRC test failed at power up! Failed to load English.str file!\r\n")));

    //         #ifndef DEBUG // only activate system error alarm, device alert led and a new dialog with error message if the the software is a release build

    //         // must set to State_Setting since CheckAlarmCondition is only available in STATE_SETTING and STATE_VENTILATING mode
    //         VentilatorStateSetting.Set(STATE_SETTINGS);

    //         SystemErrorAlarm.LanguageStringFileCRCTestFailureOccurred();

    //         LanguageStringDialog_t LanguageStringDialog;

    //         LanguageStringDialog.DoModal();                             // display a dialog to tell user the language string can not be loaded

    //         while(true)
    //         {
    //            Sleep(UI_SLEEP_TIME);
    //         }

    //        #endif
    //    }

    //}else
    //{
    //    RETAILMSG(true,(_T("Language String CRC test passed at power up!\r\n")));
    //}

 //   TopLevelWindow = new TopLevelWindow_t;
	//TopBaseWindow = (TopBaseWindow_t *)TopLevelWindow;
 //   m_pMainWnd = TopLevelWindow;
 //   TopLevelWindow->Create(IDD_TOP_LEVEL);
	//TopLevelWindow->ShowWindow(SW_SHOWNORMAL);

    //memInfo.dwLength = sizeof(memInfo);
    //GlobalMemoryStatus(&memInfo);
#ifndef SIGMA_GUIPC_CPU
    GetStoreInformation(&si);
#endif

	m_pMainWnd = NULL;
	ptrLowerCDC_ = NULL; 
	ptrUpperCDC_ = NULL; 


	if(!CreateFrmView()) {
		return FALSE;
	}

	InitializeUpperViewCDC();
	InitializeLowerViewCDC();

	GUISystemInitalization();

    return TRUE;                                                // Start the message pump
}


int CExArmApp::ExitInstance()
{
int	Ret =0;

 //   VentilatorStateSetting.Set(STATE_STANDBY);                  // make sure servos stop
 //   Sleep(200);

 //	IntraVentCommunicationThread.Terminate();
 //	IntraVentCommunicationThread.ExitFromThread();
	////GUICommThread.ExitFromThread();
	//RealTimeTimerThread.ExitFromThread();
	//GUITimerThread.ExitFromThread();
	//MonitorTimerThread.ExitFromThread();
	//MonitorsThread.ExitFromThread();
	//AlarmsThread.ExitFromThread();
	//IntraVentApplicationThread.ExitFromThread();
	//EventsThread.ExitFromThread();
 //	NvDataStoreThread.ExitFromThread();
 //   Sleep(300);


 //	while (IntraVentCommunicationThread.KillThread()==259)
	//	{
	//	Sleep(10);
	//	}
	//while (RealTimeTimerThread.KillThread()==259)
	//	{
	//	Sleep(10);
	//	}
	//while (GUITimerThread.KillThread()==259)
	//	{
	//	Sleep(10);
	//	}
	////while (GUICommThread.KillThread()==259)
	////	{
	////	Sleep(10);
	////	}
	//while (MonitorTimerThread.KillThread()==259)
	//	{
	//	Sleep(10);
	//	}
	//while (MonitorsThread.KillThread()==259)
	//	{
	//	Sleep(10);
	//	}
	//while (AlarmsThread.KillThread()==259)
	//	{
	//	Sleep(10);
	//	}
	//while (IntraVentApplicationThread.KillThread()==259)
	//	{
	//	Sleep(10);
	//	}
	//while (EventsThread.KillThread()==259)
	//	{
	//	Sleep(10);
	//	}
 //	while (NvDataStoreThread.KillThread()==259)
	//	{
	//	Sleep(10);
	//	}
	//


	//delete TopLevelWindow;
 // // Show the cursor now that we're done
 //   ShowCursor(TRUE);

	////TerminateProcess(NULL,0);
	////ExitProcess(0);
	//delete 	(LPVOID)WaveSettings;

	if(ptrLowerCDC_ != NULL) {
		delete ptrLowerCDC_;
		ptrLowerCDC_  = NULL;
	} 
	if(ptrUpperCDC_ != NULL){
		delete ptrUpperCDC_;
		ptrUpperCDC_= NULL;
	} 

	Ret = CWinApp::ExitInstance();
#if defined(SIGMA_MEMCHECK)
	_CrtDumpMemoryLeaks();
#endif
	return Ret;
}

int CExArmApp::Run()
{
	int	Rtn;    // TODO: Add your specialized code here and/or call the base class

    //__try
    {

        Rtn = CWinApp::Run();
    }
    //__except(true)//ExceptionHandler.EvaluateException((EXCEPTION_POINTERS*)_exception_info()))
    {
		//int32_t Code;

        // Get exception info
    //    Rtn = _exception_code();
		//Code = Code;

        // Call exception handler
        //ExceptionHandler.VentilateWithoutUi(Code, "CWinAPP");
    }
#if defined (SIGMA_GUIPC_CPU)
	gShutdownRequested = TRUE;
	Sleep(100);
#endif

    return Rtn;
}

BOOL CExArmApp::OnIdle(LONG lCount)
{
	
	//return TopLevelWindow->OnIdle(lCount);
	return false;
}


BOOL CExArmApp::CreateFrmView()  {

	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object
	CMainFrame* pFrame = new CMainFrame;
	if (!pFrame)
		return FALSE;
	m_pMainWnd = pFrame;
	// create and load the frame with its resources
	pFrame->Create(L"CMainFrame",L"Simulator 880");

	// The one and only window has been initialized, so show and update it
	pFrame->ShowWindow(SW_SHOWDEFAULT);
	pFrame->UpdateWindow();

	//GUI IO HW Simulation on GUI
	#ifdef SIGMA_GUIPC_CPU
	m_pActiveWnd = NULL;
	GuiHwSimulationFrm* pFrame2 = new GuiHwSimulationFrm;
	m_pActiveWnd = pFrame2;
	pFrame2->Create(L"GuiHwSimulationFrm",L"HW Simulator 880");
	pFrame2->ShowWindow(SW_SHOWDEFAULT);
	pFrame2->UpdateWindow();
	#endif

	return TRUE;
}


void CExArmApp::InitializeLowerViewCDC()   {

	((CMainFrame*)m_pMainWnd)->m_ObjSplitterUper_Lower.SetActivePane(1,0,NULL);
	((CLowerBaseContainerView* )((CMainFrame*)m_pMainWnd)->m_ObjSplitterUper_Lower.GetActivePane())->CreateLowerBaseViewCDC();

}

void CExArmApp::InitializeUpperViewCDC()   {

	((CMainFrame*)m_pMainWnd)->m_ObjSplitterUper_Lower.SetActivePane(0,0,NULL);
	((CUpperBaseContainerView* )((CMainFrame*)m_pMainWnd)->m_ObjSplitterUper_Lower.GetActivePane())->CreateUpperBaseViewCDC();


}

void CExArmApp::GUISystemInitalization(void) {

		OutputDebugStringA("Hello World\n");
		//840 Code Initialization Beging ------>
		//initialize system, initialize thread table, create and launch 840 threads:
		Sys_Init::InitializeSigma();	//TODO this function runs POST_3 and some board init, it might not be needed

		// Initialize the structures that are expecting to be in NOVRAM or flash.
	    // These are here retrieved from the WinCE file system (on flash).
		NV_MemoryStorageThread::Initialize();

		//initialize tasks (threads) objects but do not create them yet..
		Sys_Init::InitializeTasks();
		//initialize subsystems, etc., then start the state-machine
		//Threads will be created in FsmState::Activate_() when it calls activate() for current-state
		// Note: for PC platform project, obviously not everything in Initialize exist or apply in PC!
		Sys_Init::Initialize();  

		//Initialize the GUI display and paint the initial screens
		GuiFoundation::GetUpperBaseContainer().repaint();
		GuiFoundation::GetLowerBaseContainer().repaint();
}
