//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file SocketServer.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"SocketServer.h"
#include"CLogger.h"

//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// SocketServer::SocketServer
//---------------------------
SocketServer::SocketServer(const char *port, const char *type):m_bListening(false),
    m_port(),
    m_socket_id(),
    m_socket_options(),
    m_sockaddr(),
    m_sockaddr_client()
{

    static bool b_winsock = false;
    if(b_winsock == false)
    {
        b_winsock = true;
        WSADATA info; 
        WSAStartup(MAKEWORD(1, 1), &info);
    }

    LOG_MESSAGE(LM_INFO, "SocketServer(%X)::SocketServer() constructor is called.",this ); 

    int sock_opt;

    m_socket_id = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (m_socket_id < 0)
    {
        LOG_MESSAGE(LM_CRITICAL, "SocketServer(%X)::SocketServer() failed to create a socket.",this ); 
        //TODO[ME]: Throw an error
    }

    sock_opt = 1;
    setsockopt(m_socket_id, SOL_SOCKET, SO_REUSEADDR, (char *)&sock_opt,
               sizeof(sock_opt));

    m_sockaddr.sin_family      = AF_INET;
    m_sockaddr.sin_port        = htons(atoi(port));
    m_sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* Bind socket to port */
    if (bind(m_socket_id, (struct sockaddr *)&m_sockaddr, sizeof(m_sockaddr)) < 0)
    {
        close();

        LOG_MESSAGE(LM_CRITICAL, "SocketServer(%X)::SocketServer() failed to bind socket to port %s.",this,port ); 
        //TODO[ME]: Throw an error
    }

    m_bListening = true;

    LOG_MESSAGE(LM_INFO, "SocketServer(%X)::SocketServer() is listening",this ); 

}

//---------------------------
// SocketServer::~SocketServer
//---------------------------
SocketServer::~SocketServer()
{
    LOG_MESSAGE(LM_INFO, "SocketServer(%X)::~SocketServer() destructor is called.",this ); 
    close();
}

//---------------------------
// SocketServer::recv
//---------------------------
ssize_t SocketServer::recv(char *pData, size_t length)
{
    socklen_t addrlen_cli;
    struct timeval wait_time;
    fd_set read_set;
    int iRet;

    if(m_bListening == false)
    {
        return 0;
    }

    FD_ZERO(&read_set);
    FD_SET(m_socket_id, &read_set); 
    wait_time.tv_sec  = 1;
    wait_time.tv_usec = 0;

    iRet = select(m_socket_id+1,
           &read_set,NULL,NULL, &wait_time);

    if(iRet == 0 || iRet == -1)
    {
        return 0;
    }

    addrlen_cli = (socklen_t) sizeof(m_sockaddr_client);

    return recvfrom(m_socket_id, pData, length, 0,
        (struct sockaddr *)&m_sockaddr_client, &addrlen_cli);
}

//---------------------------
// SocketServer::close
//---------------------------
void SocketServer::close()
{
    LOG_MESSAGE(LM_INFO, "SocketServer(%X):close() is called.",this ); 


    if(m_bListening == false)
    {
        return;
    }

    m_bListening = false;
    ::closesocket(m_socket_id);


}

