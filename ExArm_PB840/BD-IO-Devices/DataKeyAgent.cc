#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation
// of California claims exclusive right.  No part of this work may
// be used, disclosed, reproduced, sorted in an information retrieval
// system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written
// permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: DataKeyAgent - Interface Agent to the Datakey.
//---------------------------------------------------------------------
//@ Interface-Description
// This class provides the interface to Data Key
// hardware.  It contains methods to access the Datakey
// contents and can also be instantiated as an object containing
// the data key contents.  Task-Control uses the DataKeyAgent
// object to transport the contents of the Data Key to the GUI by
// instantiating it in the BdReadyMessage.
//
// This class provides support for the BD_CPU and the GUI_CPU.
//---------------------------------------------------------------------
//@ Rationale
// The DataKeyAgent class encapsulates the external interface to 
// the Data Key hardware.
//---------------------------------------------------------------------
//@ Implementation-Description
// The DataKeyAgent class is a collection of functions.  As
// an object, it contains the current contents of the Data Key.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
// 		rDataKey must be instantiated before this class is instantiated.
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/DataKeyAgent.ccv   25.0.4.0   19 Nov 2013 13:54:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 01-Feb-2000   DR Number: 5424
//  Project:  NeoMode
//  Description:
//      Modified to use new static interface to 'SoftwareOptions' class.
//
//  Revision: 004 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003  By:  syw    Date: 04-Dec-1997    DR Number: none
//      Project:   Sigma   (R8027)
//      Description:
//         BiLevel initial version.  Update options_.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "DataKeyAgent.hh"

//@ Usage-Classes

#if defined(SIGMA_BD_CPU)
#include "DataKey.hh"
#include "VentObjectRefs.hh"

#endif // defined(SIGMA_BD_CPU)

//@ End-Usage

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DataKeyAgent
//
//@ Interface-Description
// 		Constructor.  This method has no argument and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize data members with the contents of the DataKey object.
//---------------------------------------------------------------------
//@ PreCondition
// 		none
//---------------------------------------------------------------------
//@ PostCondition
// 		none
//@ End-Method
//=====================================================================

#if defined(SIGMA_BD_CPU)
DataKeyAgent::DataKeyAgent( void )
    : dataKeyType_(RDataKey.getDataKeyType()),
	  isInstalled_(RDataKey.isInstalled()),
      bdSerialNumber_(RDataKey.getSerialNumber( DataKey::BD )),
      guiSerialNumber_(RDataKey.getSerialNumber( DataKey::GUI )),
	  options_( SoftwareOptions::GetSoftwareOptions())
{
    CALL_TRACE("DataKeyAgent(void)");

	// $[TI1]
}

#endif // defined(SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DataKeyAgent [destructor]
//
//@ Interface-Description
// 		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
// 		none
//---------------------------------------------------------------------
//@ PostCondition
// 		none
//@ End-Method
//=====================================================================

DataKeyAgent::~DataKeyAgent(void)
{
    CALL_TRACE("~DataKeyAgent(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DataKeyAgent 
//
//@ Interface-Description
//		Copy constructor.  This method has a DataKeyAgent as an argument
//		and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Copy the contents of the object passed in to the newly instatiated
//		one.
//---------------------------------------------------------------------
//@ PreCondition
// 		none
//---------------------------------------------------------------------
//@ PostCondition
// 		none
//@ End-Method
//=====================================================================

DataKeyAgent::DataKeyAgent(const DataKeyAgent& rDataKeyAgent)
    : dataKeyType_(rDataKeyAgent.dataKeyType_),
      isInstalled_( rDataKeyAgent.isInstalled_),
      bdSerialNumber_(rDataKeyAgent.bdSerialNumber_),
      guiSerialNumber_(rDataKeyAgent.guiSerialNumber_),
      options_( rDataKeyAgent.options_)

{
    CALL_TRACE("DataKeyAgent::DataKeyAgent(const DataKeyAgent& rDataKeyAgent)");
	// $[TI2]
}
 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
DataKeyAgent::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES, DATAKEYAGENT, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================



