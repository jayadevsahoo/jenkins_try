REM	File:		PostLink.BAT
REM	Component:	AutoLaunch_v300_Compact7
REM	OS Version:	Windows Embedded Compact 7
REM	Project URL:	http://AutoLaunch4CE.Codeplex.COM
REM	
REM	Last updated:	July 26th, 2011
REM	Updated by:	Samuel Phung
REM

@echo off
echo.
echo Executing AutoLaunch_v300_Compact7 Postlink.bat
echo.

setlocal

set DEST1="%SG_OUTPUT_ROOT%\oak\target\%_TGTCPU%\%WINCEDEBUG%\"
set DEST2=%_FLATRELEASEDIR%

@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\clientshutdown.exe"
@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\CMAccept.exe"
@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\ConmanClient2.exe"
@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\DeviceAgentTransport.dll"
@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\eDbgTL.dll"
@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\TcpConnectionA.dll"

if "%_TGTCPU%"=="ARMV4" (@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\DeviceDMA.dll")
if "%_TGTCPU%"=="ARMV4" (@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\emulatorstub.exe")

if "%_TGTCPU%"=="ARMV4I" (@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\DeviceDMA.dll")
if "%_TGTCPU%"=="ARMV4I" (@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\emulatorstub.exe")

if "%_TGTCPU%"=="ARMV5" (@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\DeviceDMA.dll")
if "%_TGTCPU%"=="ARMV5" (@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\emulatorstub.exe")

if "%_TGTCPU%"=="ARMV6" (@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\DeviceDMA.dll")
if "%_TGTCPU%"=="ARMV6" (@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\emulatorstub.exe")

if "%_TGTCPU%"=="ARMV7" (@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\DeviceDMA.dll")
if "%_TGTCPU%"=="ARMV7" (@call :MakingCopies "%ProgramFiles%\Common Files\Microsoft Shared\CoreCon\1.0\Target\wce400\%_TGTCPU%\emulatorstub.exe")

endlocal
echo.
echo Exiting CoreCon_v300_Compact7 Postlink.bat
echo.
goto :eof

:MakingCopies
    xcopy /Y /F %1 %DEST1%
    xcopy /Y /F %1 %DEST2%
    
goto :eof
