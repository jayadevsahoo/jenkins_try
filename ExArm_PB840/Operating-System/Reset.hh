#ifndef Reset_HH
#define Reset_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 
//====================================================================
// Class:  Reset - System Reset/Reboot Control
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/Reset.hhv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//====================================================================
 
//@ Usage-Classes
#include "Sigma.hh"
#include "Post.hh"
//@ End-Usage

class Reset : public Post
{
  public:

    enum ResetCode
    {
         NONE           = 0
        ,GUIINOP        = 1
        ,GUIONLINE      = 2
        ,GUIONLINEINIT  = 3
        ,GUISERVICE     = 4
        ,GUISERVICEINIT = 5
        ,GUISST         = 6
        ,GUISSTINIT     = 7
        ,GUITIMEOUT     = 8
        ,BDINOP         = 9
        ,BDSERVICE      = 10
        ,BDSST          = 11
        ,SERVICEMODE    = 12
    };

    static void Initiate( ShutdownState shutdownState,  
                          ResetCode resetCode = NONE);

  private:
    Reset();    // not implemented
    ~Reset();   // not implemented
};

#endif // Reset_HH
