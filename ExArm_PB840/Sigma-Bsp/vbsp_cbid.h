/*
*
*		Copyright (c) 1992 READY SYSTEMS CORPORATION.
*
*	All rights reserved. READY SYSTEMS' source code is an 
*	unpublished work and the use of a copyright notice does not 
*	imply otherwise. This source code contains confidential, 
*	trade secret material of READY SYSTEMS. Any attempt or 
*	participation in deciphering, decoding, reverse engineering 
*	or in any way altering the source code is strictly prohib-
*	ited, unless the prior written consent of READY SYSTEMS is 
*	obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
*/


/*
 * MOD-NAME     : vbsp_cbid.h 
 * LONG-NAME    : This file implements the control block ID manage-
 *              : ment for VBSP
 * AUTHOR       : Eugene Walden
 * DEPARTMENT   : Ready Systems, Engineering
 * CREATION-DATE: 5-21-91 
 * SUBSYSTEM    : Part of VBSP. Used by device functions
 */

#ifndef _VBSP_CBID_H_
#define _VBSP_CBID_H_

#define VBSP_CBID_VERBITS 5  /* Use 5 Bit for the version counter */


/******************************************************************
*                      Constants and Macros                       *
******************************************************************/
#define VBSP_CBID_ALIGNMENT	(1 << VBSP_CBID_VERBITS)
#define VBSP_CBID_VER_MASK	\
    ((unsigned long) VBSP_CBID_ALIGNMENT - 1L)
#define VBSP_CBID_PTR_MASK	(~VBSP_CBID_VER_MASK)
#define VBSP_CBID_NULL		((void *) 0)
#define VBSP_CBID_PTR(id)	\
    ((vbsp_cbid_t *) ((unsigned long) id &  VBSP_CBID_PTR_MASK))

/******************************************************************
*                      Data types                                 *
******************************************************************/
typedef unsigned long vbsp_cbid_mag_t;  /* Type of magic number   */
typedef void *vbsp_cbidID_t;		/* Type of IDs as passed  */
                                        /* to the application     */

struct vbsp_cbid_s
  {
    vbsp_cbid_mag_t  vbsp_cbid_magic;    /* Magic number          */
    vbsp_cbidID_t    *vbsp_cbid_ID;      /* Addr to self w/ added */
                                         /* version cnt in least  */
				         /* significant           */
                                         /* VBSP_CBID_VERBITS     */
  } ;

typedef struct vbsp_cbid_s vbsp_cbid_t;


/*******************************************************************
*                    Exported Functions                            *
*******************************************************************/
/*
 * Transform four characters into a magic number
 */
#define VBSP_CBID_MAGIC(c1, c2, c3, c4) \
	((c1 << 24) | (c2 << 16) | (c3 << 8) | c4)

/*
 * Create a new control block ID
 */
#define VBSP_CBID_INIT(vbsp_cbid, magic)			   \
{								   \
    (vbsp_cbid)->vbsp_cbid_magic = (magic);			   \
}


/*
 * Get the pointer to the OS object and check the ID.
 * Returns VBSP_CBID_NULL if the ID is invalid.
 */
#define VBSP_CBID_GET(id, magic) (id)


/*
 * Get the ID for an existing OS object
 */
#define VBSP_CBID_ID(id) ((id)->vbsp_cbid_ID)


/*
 * Verify if pointer and ID still match. Returns TRUE if match.
 */
#define VBSP_CBID_VERIFY(vbsp_cbid, ID) \
	(((vbsp_cbid)->vbsp_cbid_ID == ID) ? TRUE : FALSE)


/*
 * Invalidate all IDs referencing an OS object
 */
#define VBSP_CBID_INVALIDATE(vbsp_cbid)				   \
{								   \
    (vbsp_cbid)->vbsp_cbid_magic = 0L;				   \
    (vbsp_cbid)->vbsp_cbid_ID = (vbsp_cbidID_t *)	       	   \
  ((unsigned long) (vbsp_cbid)->vbsp_cbid_ID & VBSP_CBID_VER_MASK);\
}

/*
 * Check if a given vbsp_cbid is invalidated.
 */
#define VBSP_CBID_ISINVALID(vbsp_cbid)   \
     ((vbsp_cbid)->vbsp_cbid_magic == 0L)

#endif /* _VBSP_VBSP_CBID_H_ */
  

