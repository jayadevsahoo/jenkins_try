#include "StdAfx.h"
#include "Socket.h"
#include <ws2tcpip.h>
#include <stdio.h>

TcpSocket::TcpSocket(void) :
	m_connectSocket(INVALID_SOCKET)
{
}

TcpSocket::~TcpSocket(void)
{
	if (m_connectSocket != INVALID_SOCKET)
		Close();
}

bool TcpSocket::Create()
{
	WSADATA wsaData;
	int iResult;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != 0) 
		return false;
	return true;
}

bool TcpSocket::Connect(const CHAR* addr, UINT port)
{
	m_port = port;
	if (!Create())
		return false;

	int iResult;
	struct addrinfo *result = NULL,
                *ptr = NULL,
                hints;

	ZeroMemory( &hints, sizeof(hints) );
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	char port_s[16];
	sprintf_s(port_s, "%d", port);
	iResult = getaddrinfo(addr, port_s, &hints, &result);
	if (iResult != 0) 
	{
	//	WSACleanup();
		return false;
	}

	m_connectSocket = INVALID_SOCKET;
	// Attempt to connect to the first address returned by
	// the call to getaddrinfo
	ptr=result;

	// Create a SOCKET for connecting to server
	m_connectSocket = socket(ptr->ai_family, ptr->ai_socktype, 
		ptr->ai_protocol);

	if (m_connectSocket == INVALID_SOCKET) 
	{
		freeaddrinfo(result);
	//	WSACleanup();
		return false;
	}

	// Connect to server.
	iResult = connect( m_connectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
	if (iResult == SOCKET_ERROR) 
	{
		closesocket(m_connectSocket);
		m_connectSocket = INVALID_SOCKET;
	}

	freeaddrinfo(result);
	if (m_connectSocket == INVALID_SOCKET) 
	{
	//	WSACleanup();
		return false;
	}

    // check if the server return "Already_Connected" in that case, we should 
	// close the socket and return error
    fd_set read_fds;
    struct timeval tv;
    tv.tv_sec = 0;   
    tv.tv_usec = 1000;  // 1ms wait

    // clear fd_set
    FD_ZERO(&read_fds);

    // add socket to monitor
    FD_SET(m_connectSocket, &read_fds);

    // see if any sockets are ready to read
    int socketHandles = select(m_connectSocket, &read_fds, NULL, NULL, &tv);
	if (socketHandles > 0)
	{
		char buf[100];
		if (recv (m_connectSocket, buf, 100, MSG_PEEK) > 0) // peek the message
		{
			if (strstr (buf, "<Already connected") > 0)
			{
				recv (m_connectSocket, buf, 100, 0); // clear buffer to avoid winsock sending RST
				                                     // on closesocke. Sending RST is not wrong, but
				                                     // our NetX stack may not handle it well.
				closesocket (m_connectSocket);
				return false;
			}
		}
	}
	return true;
}

void TcpSocket::Close()
{
	// shutdown the connection for sending since no more data will be sent
	// the client can still use the ConnectSocket for receiving data
	int iResult = shutdown(m_connectSocket, SD_SEND);

	// cleanup
	closesocket(m_connectSocket);
	//WSACleanup();
	m_connectSocket = INVALID_SOCKET;
}

INT TcpSocket::Send(const CHAR* sendbuf)
{
	int iResult;

	// Send an initial buffer
	iResult = send(m_connectSocket, sendbuf, (int) strlen(sendbuf), 0);
	if (iResult == SOCKET_ERROR) 
	{
		printf("send failed: %d\n", WSAGetLastError());
		closesocket(m_connectSocket);
		//WSACleanup();
		return false;
	}

	return true;
}

INT TcpSocket::Receive(CHAR* recvbuf, INT recvbuflen)
{
	int iResult = recv(m_connectSocket, recvbuf, recvbuflen - 1, 0);
	recvbuf[iResult] = NULL;

	//if (iResult > 0)
		//printf("Bytes received: %d\n", iResult);
	//else if (iResult == 0)
		//printf("Connection closed\n");
	//else
		//printf("recv failed: %d\n", WSAGetLastError());
	return iResult;
}

bool TcpSocket::ReceiveReady()
{
	if (m_connectSocket == INVALID_SOCKET)
		return false;

    fd_set read_fds;
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 0;

    // clear fd_set
    FD_ZERO(&read_fds);

    // add sockets to monitor
    FD_SET(m_connectSocket, &read_fds);

    // see if any sockets are ready to read
    int socketHandles = select(m_connectSocket, &read_fds, NULL, NULL, &tv);
	if (socketHandles == SOCKET_ERROR || socketHandles <= 0)
		return false;

	// check if socket has data to read
	if (FD_ISSET(m_connectSocket, &read_fds))
		return true;
	return false;
}
