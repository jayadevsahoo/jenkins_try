#ifndef NumericLimitSettingButton_HH
#define NumericLimitSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2005, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: NumericLimitSettingButton - A setting button which displays 
// a title (along with possible units text) and the value of a bounded 
// setting in a variable precision numeric field. It also handles the 
// special case of a setting value limit of "OFF".
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/NumericLimitSettingButton.hhv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 003   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 002   By: rhj    Date:  10-May-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//      RESPM related changes.
//
//  Revision: 001   By: gdc    Date:  15-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - NIV1
//      NIV1 project initial version
//====================================================================

#include "Colors.hh"

//@ Usage-Classes
#include "SettingButton.hh"
#include "NumericField.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class NumericLimitSettingButton : public SettingButton
{
public:
	NumericLimitSettingButton(Uint16 xOrg, Uint16 yOrg, Uint16 width,
				Uint16 height, ButtonType buttonType, Uint8 bevelSize,
				CornerType cornerType, BorderType borderType,
				TextFont::Size numberSize,
				Uint16 valueCenterX, Uint16 valueCenterY,
				StringId titleText, StringId unitsText,
				SettingId::SettingIdType settingId,
				SubScreen *pFocusSubScreen, StringId messageId,
				Boolean isMainSetting = FALSE,
				StringId auxTitleText = ::NULL_STRING_ID,
				Gravity  auxTitlePos  = ::GRAVITY_RIGHT);
	~NumericLimitSettingButton(void);

	void setValue(Real32 value, Precision precision);
    void setUnitsText(StringId unitsTitle);
	void setTimingResolutionEnabled(Boolean isEnabled);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
                                const SettingSubject*         pSubject);

	//@ Data-Member: offText_
	// The Text Field which displays this button's OFF setting
	//TextField offText_;

private:
	// these methods are purposely declared, but not implemented...
	NumericLimitSettingButton(const NumericLimitSettingButton&);	// not implemented...
	void   operator=(const NumericLimitSettingButton&);	// not implemented...

	enum
	{
	  //@ Constant: OFF_TEXT_LEN
	  // Maximum size for cheap text buffer to format "OFF" string for offText_
	  OFF_TEXT_LEN = 32
	};

	//@ Data-Member: numericValue_
	// Retains the point size of the numeric field.
	TextFont::Size numberSize_;

	//@ Data-Member: numericValue_
	// The Numeric Field which displays this button's setting value.
	NumericField numericValue_;

    //@ Data-Member: unitsText_
    // A TextField which displays the units of the numericValue_.
	TextField unitsText_;

    //@ Data-Member: offText_
    // A TextField which displays the formatted "OFF" text.
	TextField offText_;

    //@ Data-Member: unitsText_
    // A character array in which the OFF text is formatted for offText_
	wchar_t offString_[OFF_TEXT_LEN];  

    //@ Data-Member: isTimingResolutionEnabled_
    // TRUE when timing resolution/conversion is enabled 
	Boolean isTimingResolutionEnabled_;
};


#endif // NumericLimitSettingButton_HH 
