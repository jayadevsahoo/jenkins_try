
#ifndef DateFormatSetting_HH
#define DateFormatSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  DateFormatSetting -  Date Format Setting
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DateFormatSetting.hhv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: rhj    Date:  01-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Initial version.
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
//@ End-Usage


class DateFormatSetting : public BatchDiscreteSetting 
{
public:
	DateFormatSetting(void);
	virtual ~DateFormatSetting(void);

	virtual Applicability::Id  getApplicability(const Notification::ChangeQualifier qualifierId) const;

	virtual SettingValue  getNewPatientValue(void) const;


	static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL);
private:
	DateFormatSetting(const DateFormatSetting&);		// not implemented...
	void  operator=(const DateFormatSetting&);	// not implemented...
};


#endif // DateFormatSetting_HH 
