#ifndef ManeuverTimes_HH
#define ManeuverTimes_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


#include "Sigma.hh"

//@ Usage-Classes

//@ End-Usage

static const Int32 MAX_EXP_PAUSE_INTERVAL = 20000 ;

static const Int32 MAX_INSP_PAUSE_INTERVAL = 7000 ;

static const Int32 MAX_P100_MANEUVER_ACTIVE_TIME = 6000;


#endif


