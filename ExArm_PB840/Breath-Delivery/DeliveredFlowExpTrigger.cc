#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DeliveredFlowExpTrigger - Triggers exhalation in SPONT breath
//         based on End-Inspiratory flow method.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers exhalation, during spontaneous inspiration, based on
//    the desired flow and the operator set value for flow triggering
//    exhalation.  The trigger can be enabled or disabled, depending upon the
//    current breath phase and the applicability of the trigger.  When the flow
//    and pressure conditions calculated by this trigger are true, the trigger
//    is considered to have "fired".  The trigger is kept on a list contained
//    in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements the algorithm for detecting the end of 
//    inspiration by the delivered flow method.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of whether 
//    this trigger is enabled or not.  If the trigger is not enabled, 
//    it will always return a state of false.  If the trigger is enabled
//    and on the active list in the BreathTriggerMediator, the
//    condition monitored by the trigger is evaluated every BD cycle.
//    This trigger detects exhalation using End-Inspiratory flow and pressure 
//    methods. The End-Inspiratory flow method depends on desired flow, peak 
//    desired flow, relief flow target, peak desired flow and operator set 
//    flow sensitivity. The pressure condition is based 
//    on the inspiration pressure, peep, and effectivePressure.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DeliveredFlowExpTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 024   By: rhj    Date: 11-Feb-2009    SCR Number: 6470
//  Project:  840S
//  Description:
//       Compensate desiredFlow_ with leak to enhance adjustment 
//       sensitivity of exhalation leak rate.  
//       
//  Revision: 023   By: rhj    Date: 07-July-2008    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 022   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Set eSens = 0 form Vital Capacity inspiratory phase.
//
//  Revision: 021  By: cep     Date:  18-Apr-2002    DR Number: 5899
//  Project:  VCP
//  Description:
//		Changed implementation description per DCS 5552.
//
//  Revision: 020  By: syw     Date:  14-Feb-2000    DR Number: 5632
//  Project:  NeoMode
//  Description:
//		For neonatal circuit types, use lung flow for trigger condition.
//
//  Revision: 019  By: syw     Date:  26-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//		Always use exhalationSensitivity setting if neonatal circuit.
//
//  Revision: 018  By: syw     Date:  22-Oct-1999    DR Number: 5552
//  Project:  ATC
//  Description:
//      Changed exhalationSensitivity from 1% to 0% during all PEEP_HIGH.
//
//  Revision: 017  By: sah     Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//      Changed 'PEEP_HI' to 'PEEP_HIGH'.
//
//  Revision: 016  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 015  By:  iv     Date:  23-Oct-1998    DR Number: 5229
//       Project:  BiLevel
//       Description:
//          Added a bilevel trajectory condition to triggerCondition_().
//
//  Revision: 014  By:  syw    Date:  14-Jul-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//          Bilevel initial version.
//
//  Revision: 013  By:  iv    Date:  03-Sep-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//           Reworked per formal code review.
//
//  Revision: 012  By:  iv    Date:  22-Jul-1997    DR Number: 2409
//       Project:  Sigma (R8027)
//       Description:
//           Fixed a duplicate revision in header.
//
//  Revision: 011  By:  iv    Date:  18-Jul-1997    DR Number: 2299
//       Project:  Sigma (R8027)
//       Description:
//           Added a net flow trigger condition.
//
//  Revision: 010  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 009  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 008  By:  sp    Date:  25-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 007  By:  sp    Date:  31-May-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Add unit test methods getDesiredFlow, getReliefFlowTarget,
//             getExhalationSensitivity and getEffectivePressure.
//
//  Revision: 006  By:  sp    Date:  6-May-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Use data member exhalationSensitivity_ instead of the
//             the value from setting. Set exhalationSensitivity_ to 0
//             in method enable_.
//
//  Revision: 004  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 003  By:  sp    Date: 1-Feb-1996    DR Number:669
//       Project:  Sigma (R8027)
//       Description:
//             modify the trigger condition.
//
//  Revision: 002  By:  sp    Date: 26-Sep-1995    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             update per controller spec rev 4.0.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "DeliveredFlowExpTrigger.hh"

//@ Usage-Classes
#include "Peep.hh"

#include "BreathMiscRefs.hh"
#include "BreathRecord.hh"
#include "BreathPhase.hh"
#include "MainSensorRefs.hh"
#include "PhaseRefs.hh"
#include "PressureSensor.hh"
#include "ExhFlowSensor.hh"
#include "PhasedInContextHandle.hh"
#include "PatientCctTypeValue.hh"
#include "LungData.hh"
#include "LeakCompEnabledValue.hh"
#include "LeakCompMgr.hh"

//@ End-Usage

extern const Real32 PEEP_THRESHOLD;
const Real32 PEEP_THRESHOLD = 45.0F; // cmH2O

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DeliveredFlowExpTrigger()  
//
//@ Interface-Description
//	Default Constructor
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ using BreathTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DeliveredFlowExpTrigger::DeliveredFlowExpTrigger(void)
: BreathTrigger(Trigger::DELIVERED_FLOW_EXP)
{
  CALL_TRACE("DeliveredFlowExpTrigger()");
  // $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DeliveredFlowExpTrigger() 
//
//@ Interface-Description 
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DeliveredFlowExpTrigger::~DeliveredFlowExpTrigger(void)
{
  CALL_TRACE("~DeliveredFlowExpTrigger()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable  
//
//@ Interface-Description
//	This method takes no argument and has no return value. This method 
//      enables the trigger and initializes all private data members.
//---------------------------------------------------------------------
//@ Implementation-Description

//	The data member desiredFlow_ is set arbitrarily to 200
//	to prevent the method triggerCondition 
//	from returning true during the first cycle of inspiration,
//	before the breath phase had the chance to set the desiredFlow_. 
//	Any positive value will prevent the trigger from triggering on the 
//	first cycle since the data members peakDesiredFlow_, reliefFlowTarget_ 
//	and effectivePressure_ are set to 0. Set data member isEnabled_ to true.
//  pressureTrajectory_ is set to 0 which is a safe value that will prevent
//  from triggering. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
DeliveredFlowExpTrigger::enable(void)
{
  CALL_TRACE("DeliveredFlowExpTrigger()");

  // $[TI1]
  isEnabled_ = TRUE;
  desiredFlow_ = 200.0F;
  peakDesiredFlow_ = 0.0F;
  peakNetFlow_ = 0.0F;
  effectivePressure_ = 0.0F;
  reliefFlowTarget_ = 0.0F;
  pressureTrajectory_ = 0.0F;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDesiredFlow
//
//@ Interface-Description
//	This method takes one argument desiredFlow and has no return value.  
//	It is called by flow controller to update desired flow.
//	This method is called every BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set data member desiredFlow_ to the argument desiredFlow and
//	data member peakDesiredFlow_ is keeping track the maximum value
//	of the desiredFlow.  If Leak Comp is enabled, reduce the desired 
//  flow from the amount of leak detected from the LeakCompMgr class.
//	$[04020]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
DeliveredFlowExpTrigger::setDesiredFlow(
	const Real32 desiredFlow
)
{
  CALL_TRACE("setDesiredFlow(Real32 desiredFlow)");
  

  // If Leak Compensation is enabled, compensate desired flow for leak
  if (RLeakCompMgr.isEnabled())
  {
	  desiredFlow_ = desiredFlow -  RLeakCompMgr.getLeakFlow();
  }
  else
  {
	  desiredFlow_ = desiredFlow;
  }

  peakDesiredFlow_ = MAX_VALUE( desiredFlow_ ,peakDesiredFlow_);

} 




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
DeliveredFlowExpTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, DELIVEREDFLOWEXPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns 
//    false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Exhalation sensitivity is set to 0% if breath is a BiLevel mandatory and peep
//    high is greater than PEEP_THRESHOLD.  This is done to prevent premature
//    declaration of exhalation when in Bilevel transitioning from low to 
//    high peep, thus preventing auto cycling.
//    If the desired flow is less than or equal to the calculated flow trigger
//    value, or is less than or equal to the relief flow + 2 lpm, or the
//    net flow is less than or equal to the minimum net flow for triggering,
//    and the circuit pressure is greater than or equal to 90 percent of effective
//    pressure and peep, and the desired flow is less than or equal to 90 percent
//    of peak desired flow, then this trigger condition is met.
//    If Leak compensation is enabled, netflow is equal to lungflow and 
//    the desired flow condition is desired flow minus ten percent of the leak flow 
//    is less than or equal to 90 percent of peak desired flow.
//    For BiLevel mandatory inspiration, if (target pressure - pressure trajectory)
//    is greater than 0.5, the trigger will be held off.
// $[04014] $[04019] $[BL04065]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
DeliveredFlowExpTrigger::triggerCondition_(void)
{
	CALL_TRACE("triggerCondition_()");

	Boolean rtnValue = FALSE;
	Real32 exhalationSensitivity = PhasedInContextHandle::GetBoundedValue(
				SettingId::EXP_SENS).value;

	const BreathPhase* P_BREATH_PHASE = BreathPhase::GetCurrentBreathPhase();

	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE) ;


	Boolean biLevelTrajectoryCondition = TRUE;
	Real32 peep = RPeep.getActualPeep();

	if (P_BREATH_PHASE == (BreathPhase*)&RLowToHighPeepPhase)
	{
		// $[TI7]
		biLevelTrajectoryCondition =
				(peep + effectivePressure_ - pressureTrajectory_ <= 0.5); 
	}  // $[TI8]

	Boolean flowCondition = FALSE ;
	
	if (PATIENT_CCT_TYPE == PatientCctTypeValue::ADULT_CIRCUIT ||
			PATIENT_CCT_TYPE == PatientCctTypeValue::PEDIATRIC_CIRCUIT)
	{
		// $[TI3]
		if (BreathRecord::GetIsMandBreath() || P_BREATH_PHASE == (BreathPhase*)&RVcmPsvPhase)
		{
			// $[TI5]
            // $[RM12063] c. Esens = 0%
			exhalationSensitivity = 0; // %
		}
		// $[TI6]

		Real32 minFlowValue = peakDesiredFlow_ * exhalationSensitivity / 100.0F;
		Real32 netFlow = RO2FlowSensor.getValue() + RAirFlowSensor.getValue()
							 - RExhFlowSensor.getDryValue() ;
		// If Leak Compensation is enabled, use lungflow  
		// instead of the original calcuation of netflow.
        // $[LC24032]
		if (RLeakCompMgr.isEnabled())
		{
		    netFlow = RLungData.getLungFlow();
		}

		peakNetFlow_ = MAX_VALUE(peakNetFlow_, netFlow);
		Real32 minNetFlowValue = peakNetFlow_ * exhalationSensitivity / 100.0F;
		
		flowCondition = (desiredFlow_ <= minFlowValue ||
			desiredFlow_ <= reliefFlowTarget_ + 2.0F ||
			netFlow <= minNetFlowValue) ;
	}
	else if (PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
	{
		// $[TI4]
		Real32 minLungFlowValue = RLungData.getPeakInspLungFlow() * exhalationSensitivity / 100.0F ;
		
		flowCondition = RLungData.getLungFlow() <= minLungFlowValue ;
	}
	else
	{
		// unexpected circuit type value...
		AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE);
	}
	
	Real32 exhPressure = RExhPressureSensor.getFilteredValue();


	// [[LC24031] -- When the LeakCompON flag is true, use  LeakComp_LungFlow 
	// and Peak_LeakComp_LungFlow in the code...
	if (RLeakCompMgr.isEnabled())
	{
		if ( flowCondition &&
				(exhPressure >= (peep + (effectivePressure_ * 0.90F))) &&
			    ( (desiredFlow_ - (0.1F * RLeakCompMgr.getLeakFlow())) <= (peakDesiredFlow_  * 0.90F)) &&
				biLevelTrajectoryCondition)
		{
			// $[TI2]
			rtnValue = TRUE;
		}

	}
	else
	{
		if ( flowCondition &&
				(exhPressure >= (peep + (effectivePressure_ * 0.90F))) &&
				(desiredFlow_ <= (peakDesiredFlow_ * 0.90F)) &&
				biLevelTrajectoryCondition)
		{
			// $[TI2]
			rtnValue = TRUE;
		}

	}

	// $[TI1]

	return (rtnValue);
}



//=====================================================================
//
//  Private Methods...
//
//=====================================================================


