#include "stdafx.h"
//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#include"BatchExecuter.h"
#include"Actions.h"
#include"BatchMemoryManager.h"
#include <stdio.h>
namespace swat{

    TimeStamp BatchExecuter::m_iTime(0);


    /////////////////////////////////////////////////////////////
    // BatchExecuter::updateTime
    /////////////////////////////////////////////////////////////
    void BatchExecuter::updateTime()
    {
        ++m_iTime;
    }


    /////////////////////////////////////////////////////////////
    // BatchExecuter::nullContext
    /////////////////////////////////////////////////////////////
    int BatchExecuter::nullContext(BatchExecuter *pCMgr)
    {

        pCMgr->reset();

        return STATE_END;
    }


    /////////////////////////////////////////////////////////////
    // BatchExecuter::nullContext
    /////////////////////////////////////////////////////////////
    void BatchExecuter::reset()
    {

        m_iCurrentContextIndex = 0;

        if(m_pclBatchCommandInfo)
        {
            BatchMemoryManager::free(m_pclBatchCommandInfo);
            m_pclBatchCommandInfo = 0;
            m_pclBatchCommand     = 0;
        }

    }


    /////////////////////////////////////////////////////////////
    // BatchExecuter::BatchExecuter
    /////////////////////////////////////////////////////////////
    BatchExecuter::BatchExecuter():
        m_pclBatchCommandInfo    (0),
        m_pclBatchCommand        (0),
        m_iBatchLength           (0),
        m_iBatchCurrentIndex     (0),
        m_iCurrentContextIndex   (0)


    {
        //nullContext is always assigned to m_contextHandlar[0]
        m_contextHandlar[0] = nullContext;
    }



    /////////////////////////////////////////////////////////////
    // BatchExecuter::init
    /////////////////////////////////////////////////////////////
    void BatchExecuter::init(BatchCommandInfo *pclBatchCommandInfo)
    {

        this->reset();

        m_pclBatchCommand      = (BatchCommand *)(((char *)pclBatchCommandInfo) + sizeof(BatchCommandInfo));
        m_pclBatchCommandInfo  = pclBatchCommandInfo;
        m_iBatchLength         = pclBatchCommandInfo->iNCMD;
        m_iBatchCurrentIndex   = 0;

        m_contextHandlar[0]    = nullContext;
        m_contextHandlar[1]    = DispatchContext::continueAction;
        m_iCurrentContextIndex = 1;
    }

    /////////////////////////////////////////////////////////////
    // BatchExecuter::popContext
    /////////////////////////////////////////////////////////////
    bool BatchExecuter::popContext()
    {

        if( (m_iCurrentContextIndex == 0) && (m_iBatchCurrentIndex>=m_iBatchLength) )
        {
            return false;
        }

        m_iCurrentContextIndex--;
        m_iBatchCurrentIndex++;

        return true;
    }

    /////////////////////////////////////////////////////////////
    // BatchExecuter::pushContext
    /////////////////////////////////////////////////////////////
    bool BatchExecuter::pushContext(int (* pContextHandlar )(BatchExecuter *pCMgr))
    {

        m_iCurrentContextIndex++;

        if(m_iCurrentContextIndex < MAX_BATCH_DEPTH)
        {
            m_contextHandlar[m_iCurrentContextIndex]=pContextHandlar;
            return true;
        }

        return false;
    }

    /////////////////////////////////////////////////////////////
    // BatchExecuter::run
    /////////////////////////////////////////////////////////////
    int BatchExecuter::run()
    {
        unsigned int retval;
		for(;;)
        {
            retval =  m_contextHandlar[m_iCurrentContextIndex](this);

            switch(retval)
            {
                case STATE_CORETURN_CONTINUE:
                    if(popContext() == false)
                    {
                        return STATE_END;
                    }
                    continue;

                case STATE_CONTINUE:
                    continue;
                case STATE_ERROR:
                case STATE_CORETURN_TIMEOUT:
                case STATE_INVALID_BATCH_COMMAND:
                case STATE_END:      return STATE_END;
                case STATE_WAIT:     return STATE_WAIT;
                case STATE_RESETALL: return STATE_RESETALL;
            }
        }
    }


}
