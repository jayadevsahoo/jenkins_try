 
#ifndef SListC_OperandName_NUM_EVENT_HH
#define SListC_OperandName_NUM_EVENT_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SListC_OperandName_NUM_EVENT - Fixed Singly-Linked List of 'OperandName' Items.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedSListC.H_v   25.0.4.0   19 Nov 2013 14:03:48   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================
 
#include "AlarmConstants.hh"
#include "AlarmConstants.hh"

//@ Usage-Classes
#include "SListC_OperandName.hh"
#include "Heap_SNodeC_OperandName_NUM_EVENT.hh"
//@ End-Usage


class SListC_OperandName_NUM_EVENT : public SListC_OperandName
{
  public:
    SListC_OperandName_NUM_EVENT(const SListC_OperandName_NUM_EVENT& sList);
    SListC_OperandName_NUM_EVENT(void);
    virtual ~SListC_OperandName_NUM_EVENT(void);

    inline void  operator=(const SListC_OperandName_NUM_EVENT& list);

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    //@ Data-Member:  heapOfNodes_
    // A heap of nodes containing item of type 'OperandName'.
    Heap_SNodeC_OperandName_NUM_EVENT  heapOfNodes_;
};


// Inlined methods...
#include "SListC_OperandName_NUM_EVENT.in"


#endif  // SListC_OperandName_NUM_EVENT_HH
