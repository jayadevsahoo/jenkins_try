#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: HundredPercentO2Handler - Handles control of Hundred Percent O2
// (initiated by the Hundred Percent O2 off-screen key).
//---------------------------------------------------------------------
//@ Interface-Description
// This is a class dedicated to the control of the Hundred Percent O2.  The
// Hundred Percent O2 maneuver is begun by pressing down the Hundred Percent O2 
// off-screen key.
//
// The keyPanelPressHappened() and keyPanelReleaseHappened() methods
// inform us of Hundred Percent O2 key events. Because HundredPercentO2Handler
// class is a BdEventTarget, the virtual method bdEventHappened() is redefined 
// here and is invoked when the Breath-Delivery subsystem notifies us that 
// a Hundred Percent O2 event occurs.  Cancel() methods Notify the
// Breath-Delivery subsystem of the cancellation request.  This is a
// callBack method linked to the display panel.
//---------------------------------------------------------------------
//@ Rationale
// Something needs to handle the Hundred Percent O2 key and this is it.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply register for Hundred Percent O2 Key event callbacks and inform
// the Breath Delivery system via an ambassador object of any key press
// event.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/HundredPercentO2Handler.ccv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 016   By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Posting FIO2 suction event regardless of cct type or options.
//
//  Revision: 015   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Posting FIO2 suction event if Plus20 is enabled.
//
//  Revision: 014  By:  rpr    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 013   By: rpr    Date: 29-Jan-2009    SCR Number: 6463
//  Project:  840S
//  Description:
//      During CPU assertions cancel O2 manuever if present.
// 
//  Revision: 012   By: rpr    Date: 22-Jan-2009    SCR Number: 6435  
//  Project:  840S
//  Description:
//      Modified for code review comments.
// 
//  Revision: 011   By: rpr    Date: 22-Jan-2009    SCR Number: 6435  
//  Project:  840S
//  Description:
//      Modified logic for when a GUI exception takes place.  Fixes issue on 
//		first use of O2 suction after a GUI assertion remain time unknown was 
//		being displayed.
// 
//  Revision: 010   By: rpr    Date: 12-Jan-2009    SCR Number: 6435 
//  Project:  840S
//  Description:
//      Modified logic for when a GUI exception takes place.  +20 needs to 
// 		update display to accomodate O2 setting changes. Display puts up
//		remaining time uknown but does take into account possible O2 setting 
//		changes.
// 
//  Revision: 009   By: rpr    Date: 08-Jan-2009    SCR Number: 6057 
//  Project:  840S
//  Description:
//      Modified timerEventHappened to check if the elasped time is greater than
//		2 minutes.  If so the unknown time will be displayed.  This can occur if 
//		a BD reset occurs
// 
//  Revision: 008   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 007  By:  erm    Date:  13-Dec-2001    DCS Number: 5976
//  Project: GuiComms
//  Description:
//   Modified to use OsTimeStamp instead of TimeStamp
//
//  Revision: 006  By:  sah	   Date:  13-Apr-2000    DCS Number: 5691
//  Project:  NeoMode
//  Description:
//      Changed event status id sent to the 100% O2 panel, when its timer
//      needs to be updated.
//
//  Revision: 005  By:  hhd	   Date:  08-Feb-2000    DCS Number: 5504, 5631
//  Project:  NeoMode
//  Description:
//		Modified to provide additional off-screen key management, whereby 100% O2
//		can be cancelled.  Also, fix bug where there is no invalid sound when this
//		off-key is pressed without O2 flow.
//
//  Revision: 004  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  yyy    Date:  01-Sep-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//             Initial release.
//
//  Revision: 001  By:  hhd Date:  13-MAY-95    DR Number:
//  Project:  Sigma (R8027)
//  Description:
//      Integration baseline.
//====================================================================

#include "HundredPercentO2Handler.hh"

//@ Usage-Classes
#include "KeyPanel.hh"
#include "BdGuiEvent.hh"
#include "Sound.hh"
#include "BdEventRegistrar.hh"
#include "GuiTimerRegistrar.hh"
#include "MiscStrs.hh"
#include "MessageArea.hh"
#include "GuiApp.hh"
#include "LowerSubScreenArea.hh"
#include "OffKeysSubScreen.hh"
#include "LowerScreen.hh"
#include "Post.hh"
#include "PatientCctTypeValue.hh"
#include "Setting.hh"
#include "ConditionEvaluations.hh"
#include "SoftwareOptions.hh"
#include "PhasedInContextHandle.hh"
#include "TrendDataMgr.hh"
#include "TrendEvents.hh"

//@ End-Usage
//@ Code...
// 
// constants for use by class methods
//@ Constant: HUNDRED_PERCENT_DURATION_MS
//two minutes of 100% O2
const Int32 HUNDRED_PERCENT_DURATION_MS = 2*60*1000;

Boolean HundredPercentO2Handler::pauseCanceled_ = FALSE;
Boolean HundredPercentO2Handler::TimerRestartedFlag_ = FALSE;
Boolean HundredPercentO2Handler::TimerContFlag_ = FALSE;
Boolean HundredPercentO2Handler::hundredPercentO2KeyPressed_ = FALSE;
Boolean HundredPercentO2Handler::calibrateO2KeyPressed_ = FALSE;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: HundredPercentO2Handler()  [Default Constructor]
//
//@ Interface-Description
// Creates the Hundred Percent O2 Handler object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Register for Hundred Percent O2 Key event callbacks and Breath-Delivery's
// Hundred Percent O2 status events and O2 calibration timer tick event callbacks.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

HundredPercentO2Handler::HundredPercentO2Handler(void)
{
	CALL_TRACE("HundredPercentO2Handler::HundredPercentO2Handler(void)");

	// Register for Hundred Percent O2 key presses
	KeyPanel::SetCallback(KeyPanel::HUNDRED_PERCENT_O2_KEY, this);

	// Register for Breath-Delivery's Hundred Percent O2 status events
	BdEventRegistrar::RegisterTarget(EventData::PERCENT_O2, this); 	

	// Register for Breath-Delivery's Hundred Percent O2 status events
	BdEventRegistrar::RegisterTarget(EventData::CALIBRATE_O2, this);    

	// Register for transition to progress timer.
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::O2_PROGRESS_TIMER_TICK, this);

	const Setting*  pPatientCctType =
	SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
	pPatientCctType->getAdjustedValue();

	// NeoMode Update including CPAP and +20% O2
	isPlus20Enabled_ = SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_UPDATE) &&
					   PatientCctTypeValue::NEONATAL_CIRCUIT == PATIENT_CCT_TYPE_VALUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~HundredPercentO2Handler  [Destructor]
//
//@ Interface-Description
// Destroys the Hundred Percent O2 Handler object.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Does nothing 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

HundredPercentO2Handler::~HundredPercentO2Handler(void)
{
	CALL_TRACE("HundredPercentO2Handler::~HundredPercentO2Handler(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelPressHappened
//
//@ Interface-Description
// Called when the Hundred Percent O2 key is pressed.  
//---------------------------------------------------------------------
//@ Implementation-Description
// We notify the Breath-Delivery subsystem's User Event object that the user is
// requesting 100% O2 by calling one of its notification methods.
//
// Note: pressing of this key is invalid during service mode so just make
// the invalid entry sound in this case.
// $[01289] During SST, Alarm silence, alarm reset, manual inspiration ...
// $[07034] During service mide, Alarm silence, alarm reset, help, ...
// $[01006] The GUI must be able to display the meaning of ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HundredPercentO2Handler::keyPanelPressHappened(KeyPanel::KeyId)
{
	CALL_TRACE("HundredPercentO2Handler::keyPanelPressHappened(KeyPanel::KeyId)");

	calibrateO2KeyPressed_ = FALSE;

	// Display the offscreen key help message 
	if (isPlus20Enabled_)
	{
		GuiApp::PMessageArea->setMessage(
										MiscStrs::PLUS_20_PERCENT_O2_HELP_MESSAGE,
										MessageArea::MA_HIGH);
	}
	else
	{
		GuiApp::PMessageArea->setMessage(
										MiscStrs::HUNDREDPERCENT_O2_HELP_MESSAGE,
										MessageArea::MA_HIGH);
	}

	// Only react to key during normal ventilation mode.
	if (GuiApp::GetGuiState() == STATE_ONLINE &&
		!GuiApp::IsSettingsLockedOut() 
	   )
	{
		// and if neo mode and there is no air present it invalid
		if (isPlus20Enabled_ && ConditionEvaluations::c5() && 
			ConditionEvaluations::c140())			// no wall/compressor air 
		{
			// Illegal key press 
			Sound::Start(Sound::INVALID_ENTRY);
		}
		else
		{
			// $[TI1]
			hundredPercentO2KeyPressed_ = TRUE;

			// Notify Breath-Delivery subsystem
			BdGuiEvent::RequestUserEvent(EventData::PERCENT_O2);

			// [TR01179] Post related trending event 
			TrendDataMgr::PostEvent( TrendEvents::AUTO_EVENT_O2_SUCTION_BUTTON );
            
			// Inform TimerRegistrar of event
			GuiTimerRegistrar::StartTimer(GuiTimerId::O2_PROGRESS_TIMER_TICK);

			startTime_.now();
		}
	}
	else
	{													// $[TI2]
		// Illegal key press during service mode
		Sound::Start(Sound::INVALID_ENTRY);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calibrateO2PressHappened
//
//@ Interface-Description
// Called when the Hundred Percent O2 key is pressed.  
//---------------------------------------------------------------------
//@ Implementation-Description
// We notify the Breath-Delivery subsystem's User Event object that the user is
// requesting 100% O2 by calling one of its notification methods.
//
// Note: pressing of this key is invalid during service mode so just make
// the invalid entry sound in this case.
// $[01289] During SST, Alarm silence, alarm reset, manual inspiration ...
// $[07034] During service mide, Alarm silence, alarm reset, help, ...
// $[01006] The GUI must be able to display the meaning of ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HundredPercentO2Handler::calibrateO2PressHappened(void)
{
	CALL_TRACE("HundredPercentO2Handler::keyPanelPressHappened(KeyPanel::KeyId)");

	// Only react to key during normal ventilation mode.
	if (GuiApp::GetGuiState() == STATE_ONLINE &&
		!GuiApp::IsSettingsLockedOut() 
	   )
	{
		// and if neo mode and there is no air present make invalid sound
		if (isPlus20Enabled_ &&
			// no wall/compressor air
			( ConditionEvaluations::c5() && ConditionEvaluations::c140() )       
		   )
		{
			// Illegal key press 
			Sound::Start(Sound::INVALID_ENTRY);
		}
		else
		{
			hundredPercentO2KeyPressed_ = TRUE;
			calibrateO2KeyPressed_ = TRUE;

			// Notify Breath-Delivery subsystem to force O2 calibration
			BdGuiEvent::RequestUserEvent(EventData::PERCENT_O2, EventData::START, EventData::HUNDREDPERCENT);

			// [TR01179] Post related trending event 
			TrendDataMgr::PostEvent( TrendEvents::AUTO_EVENT_O2_SUCTION_BUTTON );
            
			// Inform TimerRegistrar of event
			GuiTimerRegistrar::StartTimer(GuiTimerId::O2_PROGRESS_TIMER_TICK);

			startTime_.now();
		}
	}
	else
	{													// $[TI2]
		// Illegal key press during service mode
		Sound::Start(Sound::INVALID_ENTRY);
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelReleaseHappened
//
//@ Interface-Description
// Called when the Hundred Percent O2 is released.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply turn off the offscreen help message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HundredPercentO2Handler::keyPanelReleaseHappened(KeyPanel::KeyId)
{
	CALL_TRACE("HundredPercentO2Handler::keyPanelReleaseHappened(KeyPanel::KeyId)");

	//								 $[TI1]
	// Clear the off screen key message
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the Alarm Silence timer runs out (the Alarm-Analysis subsystem
// handles the particular timing).  All that we have to do is switch off
// the Alarm Silence key panel LED and inform the Alarm-Analysis subsystem
// that alarm silence is cancelled.  Finally, reset all patient data value
// stored in PatientDataRegistrar.  Or when ALARM_PROGRESS_TIMER_TICK runs
// out, simply inform the offkey subscreen to update the timer bar.
//---------------------------------------------------------------------
//@ Implementation-Description
// Switch off the Alarm Silence key panel LED and inform the Alarm-Analysis
// subsystem that alarm silence is cancelled.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HundredPercentO2Handler::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("HundredPercentO2Handler::timerEventHappened(GuiTimerId::GuiTimerIdType)");

	if ((timerId == GuiTimerId::O2_PROGRESS_TIMER_TICK) &&
		(GuiTimerRegistrar::isActive(GuiTimerId::O2_PROGRESS_TIMER_TICK)))
	{	// $[TI1]
		// if the timer is going longer than the duration for O2
		// the BD might have reset and the remaining time left is unknown
		// otherwise update the display for the O2 progress bar with the elasped 
		// time
		if (startTime_.getPassedTime() <= HUNDRED_PERCENT_DURATION_MS)
		{
			LowerSubScreenArea::GetOffKeysSubScreen()->updatePanel(EventData::PERCENT_O2,
																   EventData::PENDING,
																   EventData::NULL_EVENT_PROMPT,
																   (startTime_.getPassedTime())/1000);
		}
		else
		{
			LowerSubScreenArea::GetOffKeysSubScreen()->updatePanel(EventData::PERCENT_O2, EventData::PENDING, EventData::NULL_EVENT_PROMPT,
																   ELAPSE_TIME_UNKNOWN);
		}
	}	// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
//  Called when Hundred Percent O2 event happens and its status is notified
//  by the Breath-Delivery subsystems.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method is called with the following applicable event status:
//  CANCEL or REJECTED		: Turn the LED off.  Generate an invalide
//							  entry sound.
//  COMPLETE				: Turn LED light off Generate an contack
//							  sound.
//  ACTIVE					: Turn LED light on
//  The associated display should be updated.
//  
// $[01246] If 100% O2 timeout has expired, or if the 100% O2 function ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
HundredPercentO2Handler::bdEventHappened(EventData::EventId eventId,
								EventData::EventStatus eventStatus,
								EventData::EventPrompt eventPrompt)
{
    CALL_TRACE("BdEventTarget::bdEventHappened(EventData::EventId "
		", EventData::EventStatus eventStatus)");

	if (eventId == EventData::CALIBRATE_O2)
	{
		calibrateO2KeyPressed_ = TRUE;
	}

	Boolean updateRequired = TRUE;
	switch (eventStatus)
	{
	case EventData::CANCEL:		
	case EventData::REJECTED:						// $[TI1]
		if (!pauseCanceled_)
		{											// $[TI1.1]
			Sound::Start(Sound::INVALID_ENTRY);
		}											// $[TI1.2]

		// Turn LED light off
		KeyPanel::SetLight(KeyPanel::HUNDRED_PERCENT_O2_KEY, FALSE);

		// Stop and clear the "alarm transition" timer in case it is going.
		GuiTimerRegistrar::CancelTimer(GuiTimerId::O2_PROGRESS_TIMER_TICK);
		break;

	case EventData::COMPLETE:						// $[TI2]
		Sound::Start(Sound::CONTACT);

		// Turn LED light off
		KeyPanel::SetLight(KeyPanel::HUNDRED_PERCENT_O2_KEY, FALSE);

		// Stop and clear the "alarm transition" timer in case it is going.
		GuiTimerRegistrar::CancelTimer(GuiTimerId::O2_PROGRESS_TIMER_TICK);
		break;

	case EventData::ACTIVE:							// $[TI3]
		if (!calibrateO2KeyPressed_)
		{	// Turn LED light on by offscreen keypanel pressed
			KeyPanel::SetLight(KeyPanel::HUNDRED_PERCENT_O2_KEY, TRUE);
		}
		else
		{	// else keep the offkey panel led off
			KeyPanel::SetLight(KeyPanel::HUNDRED_PERCENT_O2_KEY, FALSE);
		}
		break;

	default:										// $[TI4]
		updateRequired = FALSE;
		break;
	}

	// Restore cancel flag to original state.
	pauseCanceled_ = FALSE;

	if (updateRequired)
	{												// $[TI5.1]
		OffKeysSubScreen *offKeysSubScreen = LowerSubScreenArea::GetOffKeysSubScreen();

		// If there is a software assertion (which causes the GUI board to reset,
		// or if the GUI is reset by itself, we cancel existing O2 manuever
		if ( (Post::GetStartupState() == EXCEPTION || 
				((!GuiApp::IsPowerFailed()) && (hundredPercentO2KeyPressed_ == FALSE))
			 ) 
			 && !TimerContFlag_ && (eventStatus == EventData::ACTIVE)
		   ) 		// $[TI5.1.1], [TI5.1.2]
		{
			hundredPercentO2KeyPressed_ = TRUE;
			TimerContFlag_ = TRUE;
			Cancel();
		}
		else if ((GuiApp::IsPowerFailed() || GuiApp::IsUnexpectedReset()) && !TimerRestartedFlag_)
		{		// $[TI5.1.4], $[TI5.1.5]
 			// If there is a power failure or the BD board is reset, we restart the timer then
			// redisplay the timing bar, since the O2/CAL should be restarted on the BD board.

			// Cancel the old timer.
			GuiTimerRegistrar::CancelTimer(GuiTimerId::O2_PROGRESS_TIMER_TICK);

			// Inform TimerRegistrar of event
			GuiTimerRegistrar::StartTimer(GuiTimerId::O2_PROGRESS_TIMER_TICK);
			startTime_.now();

			TimerRestartedFlag_ = TRUE;
			
			// Update Offkeys subScreen
			offKeysSubScreen->updatePanel(eventId, eventStatus);
		}
		else
		{   // $[TI5.1.6]
				offKeysSubScreen->updatePanel(EventData::PERCENT_O2, eventStatus);
		}
	}												// $[TI5.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Cancel
//
//@ Interface-Description
// Called when the Hundred Percent O2 key is released.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply inform the Breath Deliverery subSystem of the cancel
//  request.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HundredPercentO2Handler::Cancel(void)
{
	CALL_TRACE("HundredPercentO2Handler::Cancel(void)");

	//								 $[TI1]
	// Stop and clear the "alarm transition" timer in case it is going.
	GuiTimerRegistrar::CancelTimer(GuiTimerId::O2_PROGRESS_TIMER_TICK);

	// Notify Breath-Delivery subsystem
	pauseCanceled_ = TRUE;

    // stop appropriate active event
    if (BdGuiEvent::GetEventStatus(EventData::PERCENT_O2) == EventData::ACTIVE)
    {
        BdGuiEvent::RequestUserEvent(EventData::PERCENT_O2, EventData::STOP);
    }
    else
    {
	    BdGuiEvent::RequestUserEvent(EventData::CALIBRATE_O2, EventData::STOP);
    }


}

	
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ResetTimerRestartedFlag
//
//@ Interface-Description
// Called when the TimerRestartedFlag needs to be reset.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the TimerRestartedFlag_ value to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
HundredPercentO2Handler::ResetTimerRestartedFlag(void)
{
	TimerRestartedFlag_ = FALSE;
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsCalibrateO2KeyPressed 
//
//@ Interface-Description
// This method provides a way to return the flag that designates whether
// the on screen calibration key has been pressed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply return calibratedO2KeyPressed_ 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean 
HundredPercentO2Handler::IsCalibrateO2KeyPressed (void)
{
	CALL_TRACE("HundredPercentO2Handler::IsCalibrateO2KeyPressed(void)");

	return(calibrateO2KeyPressed_);
}

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
HundredPercentO2Handler::SoftFault(const SoftFaultID  softFaultID,
								    const Uint32       lineNumber,
								    const char*        pFileName,
								    const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, 
							HUNDREDPERCENTO2HANDLER,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
