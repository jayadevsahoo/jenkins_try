/*
*
*		Copyright (c) 1992 READY SYSTEMS CORPORATION.
*
*	All rights reserved. READY SYSTEMS' source code is an 
*	unpublished work and the use of a copyright notice does not 
*	imply otherwise. This source code contains confidential, 
*	trade secret material of READY SYSTEMS. Any attempt or 
*	participation in deciphering, decoding, reverse engineering 
*	or in any way altering the source code is strictly prohib-
*	ited, unless the prior written consent of READY SYSTEMS is 
*	obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
*
*/

/*
 *
 * File:
 *
 *    vbuf.h 
 *
 * Author: 
 * 
 *    Eugene Walden
 * 
 * Date:
 *
 *    5-15-92 
 *
 * Contents:
 *
 *
 * Revision History:
 *
 *    Name |   Date   | Description of modification
 *    -----+----------+--------------------------------------------
 *    EMW  | 05-15-92 | First version                              
 *    -----+----------+--------------------------------------------
 *    SPR    02-11-92   added macro to avvoid multiple definitions
 *                                                                 
 */

#ifndef _VBUF_H_
#define _VBUF_H_

/*
 *
 *                       Generic Buffer Macros
 *
 * This module assumes the presence of a buffer allocator. The 
 * buffers that the allocator returns have headers and data  
 * sections. The headers contain links to other buffers and other 
 * lists of buffers, as well as a pointer to the data array, its 
 * length, etc. 
 *
 * In order to allow easy accomodation to different kinds of buffer
 * structures, all access to buffer objects in this module are made 
 * through macro calls. The macros make certain assumptions about 
 * the form of a list of message buffers that should fit most 
 * structures. The macros defined are:
 *
 *   vbuf_get_dataptr(bptr)     : get ptr to data part of buffer
 *   vbuf_get_datacnt(bptr)    	: get number of bytes of buffer spc
 *   vbuf_get_msgcnt(bptr)     	: get number of bytes of message
 *   vbuf_get_nextbuf(bptr)    	: get pointer to next buffer
 *   vbuf_get_nextpkt(bptr)    	: get pointer to next packet
 *   vbuf_set_dataptr(bptr,addr): assign pointer to data buffer
 *   vbuf_set_datacnt(bptr,cnt)	: assign count value for buffer spc
 *   vbuf_set_msgcnt(bptr,cnt)	: assign count value for message
 *   vbuf_set_nextbuf(bptr,addr): assign pointer to next buffer
 *   vbuf_set_nextpkt(bptr,addr): assign pointer to next packet
 *   vbuf_is_eop(bptr)		: is this buffer an end of packet?
 *   vbuf_is_eol(bptr)		: is this buffer the end of list?
 *                              : data part of the buffer
 *
 * So, any buffer structure is expected to have a structure similar
 * to:
 *                 +-----------------------+  
 *    +---------+  |      +---------+      |  +---------+
 *    | nextpkt |--+      | nextpkt |---...+->| nextpkt |----|
 *    +---------+         +---------+         +---------+ 
 *    | nextbuf |-------->| nextbuf |-| ...-->| nextbuf |----|
 *    +---------+         +---------+         +---------+     
 *    | len     |         | len     |         | len     |     
 *    +---------+         +---------+         +---------+     
 *    | dataP   |----+    | dataP   |----+    | dataP   |----+
 *    +---------+    |    +---------+    |    +---------+    |
 *                   |                   |                   |
 *                   V                   V                   V
 *                +------+            +------+            +------+
 *                | BUF1 |            | BUF2 |            | BUF3 |
 *                +------+            +------+            +------+
 *                                                                
 * Not all macros need to be defined for all buffer structures, but
 * this set of macros seemed reasonable.
 *
 * So, if the underlying buffer structure is to be changed, edit 
 * "vbuf.h". Change the macros to access the appropriate fields of 
 * the new buffer structure, and define vbuf_t to be the new buffer 
 * structure.
 *
 */
 

#ifdef MBUF

/* Using mbufs */
typedef struct mbuf vbuf_t;

/* define "get" functions */
#define vbuf_get_dataptr(bptr)  	((bptr)->m_dataP)  
#define vbuf_get_datacnt(bptr)  	MLEN  
#define vbuf_get_msgcnt(bptr)   	((bptr)->m_len)  
#define vbuf_get_nextbuf(bptr)  	((bptr)->m_next)  
#define vbuf_get_nextpkt(bptr)  	((struct mbuf *) 0)  

/* define "set" functions */
#define vbuf_set_dataptr(bptr,addr)   	(bptr)->m_dataP = addr;
#define vbuf_set_datacnt(bptr,cnt)	/* null macro */ 	
#define vbuf_set_msgcnt(bptr,cnt)	(bptr)->m_len = cnt;
#define vbuf_set_nextbuf(bptr,addr)	(bptr)->m_next = addr;
#define vbuf_set_nextpkt(bptr,addr)	/* null macro */

/* define predicates */
#define vbuf_is_eop(bptr)		((bptr)->m_type == 1)
#define vbuf_is_eol(bptr)      ((bptr)->m_next == (struct mbuf *) 0)

#else /* ifdef LOGIOBUFF */

/* Using logio buffers */
typedef logio_buff_t vbuf_t;

/* define "get" functions */
#define vbuf_get_dataptr(bptr)  	((bptr)->data)  
#define vbuf_get_datacnt(bptr)  	((bptr)->blen)  
#define vbuf_get_msgcnt(bptr)   	((bptr)->dlen)  
#define vbuf_get_nextbuf(bptr)  	((bptr)->next_buff)  
#define vbuf_get_nextpkt(bptr)  	((bptr)->next_pckt)  

/* define "set" functions */

#define vbuf_set_dataptr(bptr,addr)   	(bptr)->data = addr;
#define vbuf_set_datacnt(bptr,cnt)	(bptr)->blen = cnt; 
#define vbuf_set_msgcnt(bptr,cnt)	(bptr)->dlen = cnt;	
#define vbuf_set_nextbuf(bptr,addr)	(bptr)->next_buff = addr;
#define vbuf_set_nextpkt(bptr,addr)	(bptr)->next_pckt = addr;
/* define predicates */
#define vbuf_is_eop(bptr) ((bptr)->next_pckt == (logio_buff_t *) 0) 
#define vbuf_is_eol(bptr) ((bptr)->next_buff == (logio_buff_t *) 0)

#endif
#endif
