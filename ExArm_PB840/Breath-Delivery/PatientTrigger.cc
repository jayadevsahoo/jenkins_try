#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PatientTrigger - Abstract base class for all inspiratory patient
//  triggers. 
//---------------------------------------------------------------------
//@ Interface-Description
// This class is an abstract base class for all inspiratory 
// patient triggers.  Patient triggers are different from other 
// breath triggers because they
// are not activated immediately upon being enabled.  If enabled,they
// become active when specific conditions are met for allowing the patient
// to trigger a change in the active breath phase.  This class has a
// method for activating the trigger that will be inherited by all the
// patient trigger derived classes. The virtual enabling method is used
// to initialize the private data members.
//---------------------------------------------------------------------
//@ Rationale
//	All the patient triggers share the same activation conditions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The data member isTriggerActive_ is initialize to FALSE in the
//  beginning because all patient triggers have to be disabled for the
//  first PATIENT_MAX_DELAY_MS msec of exhalation.  The method activateTrigger will set 
//  isTriggerActive_ to TRUE after PATIENT_MAX_DELAY_MS msec.
//  The method enable is used to initialize private data.
// $[04002]
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a. 
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PatientTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 010  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 009  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 008  By:  sp    Date:  10-Oct-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed const MIN_EXH_TIME and updated traceability.
//
//  Revision: 007  By:  sp    Date:  25-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 006  By:  sp    Date:  26-Jun-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Implement new patient trigger's algorithm.
//
//  Revision: 005  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 004  By:  sp    Date:  19-Jan-1996    DR Number:600, 667 
//       Project:  Sigma (R8027)
//       Description:
//             Fix comment(600).
//             Remove dead code in method activate trigger(667).
//
//  Revision: 003  By:  iv    Date:  19-Jan-1996    DR Number:545, 1107 
//       Project:  Sigma (R8027)
//       Description:
//             Tracability changes per 11-01-96 SRS 2.4 review.
//
//  Revision: 002  By:  sp    Date:  26-Sep-1995    DR Number:545, 1107 
//       Project:  Sigma (R8027)
//       Description:
//             update per controller spec rev 4.0.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "PatientTrigger.hh"

//@ Usage-Classes
#  include "PhaseRefs.hh"

#include "BreathPhase.hh"

#include "BreathRecord.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PatientTrigger
//
//@ Interface-Description
//      This constructor takes triggerId as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This constructor uses its parent constructor BreathTrigger to initialize
//	triggerId_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PatientTrigger::PatientTrigger(const Trigger::TriggerId triggerId) 
: BreathTrigger(triggerId)
{
    CALL_TRACE("PatientTrigger(Trigger::TriggerId triggerId)");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PatientTrigger()
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PatientTrigger::~PatientTrigger(void)
{
    CALL_TRACE("~PatientTrigger()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable()
//
//@ Interface-Description
//	This method enables the trigger. This method also initializes data 
//	members that belong to patient trigger.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The state data member isEnabled_ is set to true, activating this trigger.
//	The data members prevDryExhFlow_ and isTriggerActive_ are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
PatientTrigger::enable(void)
{
  CALL_TRACE("enable(void)");
 
   // $[TI1]
  isEnabled_ = TRUE;
  prevDryExhFlow_ = 0.0F;
  isTriggerActive_ = FALSE;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PatientTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PATIENTTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateTrigger_
//
//@ Interface-Description
//  This method determines the trigger state (enabled or disabled)
//  depending upon the elapsed time. This
//  method is invoked every BD cycle after the trigger has been enabled
//  until the isTriggerActive_ state is TRUE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  if the time since start of exhalation > PATIENT_MAX_DELAY_MS msec, 
//  data member isTriggerActive_ is set to true.
// $[04004] $[04007]
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void 
PatientTrigger::activateTrigger_(void)
{
    CALL_TRACE("activateTrigger_()");

    if (((BreathPhase&)RExhalationPhase).getElapsedTimeMs() > MIN_EXH_TIME)
    {
        // $[TI2]
        isTriggerActive_ = TRUE;
    }
    // $[TI1]
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

