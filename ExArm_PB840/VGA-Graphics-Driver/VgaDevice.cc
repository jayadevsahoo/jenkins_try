#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: VgaDevice - Interface class for VGA device functions.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides for initializing the VGA devices including the
//  VGA graphics controller and the screen brightness and contrast.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class provides for the instantiation of the Vga objects and
//  initialization of the VGA controller by calling Vga::GetVga().
//  The screen contrast and brightness are set to initial values that
//  will make the "splash" screen visible.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaDevice.ccv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 004   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 003   By: gdc    Date:  10-Apr-1998    DCS Number: 
//  Project:  Color
//  Description:
//	Setting reasonable contrast value at initialization so the
//  initializing message is visible.
//
//  Revision: 002   By: sah    Date:  27-Oct-1997    DCS Number: 2585
//  Project:  Sigma (R8027)
//  Description:
//	Adding the setting of the screen brightness to its maximum, at
//	startup.
//
//  Revision: 001   By: sah    Date:  01-Aug-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Initial version to coding standards (Integration baseline).
//
//=====================================================================

#include "VgaDevice.hh"
#include "DisplayContext.hh"

//@ Usage-Classes
#include "VgaLine.hh"
#include "OsUtil.hh"
#include "Colors.hh"
//@ Usage-Classes

#define BACKLIGHT_ADRS     (0xFFBEA010)
#define P_COMMON_BACKLIGHT (Uint8*)(BACKLIGHT_ADRS)
#define P_UPPER_BACKLIGHT  (Uint8*)(BACKLIGHT_ADRS)
#define P_LOWER_BACKLIGHT  (Uint8*)(BACKLIGHT_ADRS+1)

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

//=====================================================================
//
//  Static Data Members...
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize
//
//@ Interface-Description
//  This method initializes the VGA device interface's internal mechanism
//  so that the application can use the VGA.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize each display screen and set the backlight brightness 
//	and constrast.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaDevice::Initialize(void)
{
	//TODO BVP - E600 We probably need to get rid of this class
	//for PB880, given that brightness and contrast are handled
	//very differently on this hardware, compared to how it was 
	//done in 840.
#if 0
  const Int32  PERCENT_BRIGHTNESS_ = 100;
  const Uint8  INIT_CONTRAST_VALUE_ = (Uint8)128;

  // this will instantiate and initialize both VGA displays
  DisplayContext & uDc = VgaGraphicsDriver::GetUpperDisplayContext();
  DisplayContext & lDc = VgaGraphicsDriver::GetLowerDisplayContext();

  // clear the screen
  uDc.fillSolid(0,0,640,480,Colors::MEDIUM_BLUE);
  lDc.fillSolid(0,0,640,480,Colors::MEDIUM_BLUE);

  // wait for screen to clear before turning on backlight
  uDc.accessDisplay();
  lDc.accessDisplay();

  // to ensure that the backlight turns on in cold temparatures, the backlight
  // is set to its maximum value...
  VgaDevice::SetBrightness(PERCENT_BRIGHTNESS_,PERCENT_BRIGHTNESS_);
  VgaDevice::SetContrast(INIT_CONTRAST_VALUE_, INIT_CONTRAST_VALUE_);

  // $[TI1]
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetContrast
//
//@ Interface-Description
//  Sets the lower and upper screen contrast values, respectively.
//  Contrast values range from 0 (no contrast) to 0xFF (maximum contrast).
//---------------------------------------------------------------------
//@ Implementation-Description
//  The hardware design requires writing the contrast values as a single
//  16-bit value into the screen contrast register; the LSByte for the
//  lower display, MSByte for the upper display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaDevice::SetContrast(const Uint8 lowerScreenContrast,
					   const Uint8 upperScreenContrast)
{
	// TODO E600 port
  // $[TI1]
  //*CONTRAST = ((Uint16)upperScreenContrast << 8) | (Uint16)lowerScreenContrast;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetBrightness
//
//@ Interface-Description
//  Sets the (single) lower and upper screen brightness (sometimes called
//  the "backlight intensity") value.  Brightness values range from 0
//  (backlight off) to 0xFF (maximum backlight intensity)
//---------------------------------------------------------------------
//@ Implementation-Description
//  Write the brightness value as a single 8-bit value into the backlight
//  intensity register.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaDevice::SetBrightness(const Int32 percentLowerBrightness,
                         const Int32 percentUpperBrightness)
{
	//TODO BVP - E600 We probably need to get rid of this class
	//for PB880, given that brightness and contrast are handled
	//very differently on this hardware, compared to how it was 
	//done in 840.
#if 0
  CLASS_ASSERTION(percentLowerBrightness >= 0 && percentLowerBrightness <= 100);
  CLASS_ASSERTION(percentUpperBrightness >= 0 && percentUpperBrightness <= 100);

  // $[TI1]
  if ( IsGuiCommsConfig() )
  {													// $[TI1.1]
	const Int32 MAX_DRIVE_VALUE = 80;
	// drive values range [0..80], 0 is the brightest
	// the periodicity of the PWM backlight is 5ms
	// for every 16 counts the ON time changes by 1ms
	// in the range [0..80]
	Int32 lowerDriveValue 
	  = MAX_DRIVE_VALUE - (percentLowerBrightness * MAX_DRIVE_VALUE / 100);
	Int32 upperDriveValue 
	  = MAX_DRIVE_VALUE - (percentUpperBrightness * MAX_DRIVE_VALUE / 100);

	*P_UPPER_BACKLIGHT = upperDriveValue;
	*P_LOWER_BACKLIGHT = lowerDriveValue;
  }
  else
  {													// $[TI1.2]
	const Int32 MAX_DRIVE_VALUE = 0xff;
	// drive values range [0..0xff], 0xff is the brightest
	Int32 driveValue = percentLowerBrightness * MAX_DRIVE_VALUE / 100;
	// or in all lower 4 bits for compatibility with 9.5 color panels
	driveValue |= 0xf;
	*P_COMMON_BACKLIGHT = driveValue;
  }
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaDevice::SoftFault(const SoftFaultID softFaultID,
		     const Uint32 lineNumber,
		     const char *pFileName,
		     const char *pPredicate)
{
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
			  VgaGraphicsDriver::VGA_DEVICE_ID, 
			  lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
