/*****************************************************************************/
/*   Module Name:    COM.c                                                   */
/*   Purpose:        Serial Communications Module                            */
/*                                                                           */
/*   Modification History:                                                   */
/*                                                                           */
/*   Description of Change			        By			      Date           */
/*   ---------------------					-----   		  ------         */
/*	 TestExecutive related functions are    Hamsavally(CGS)   20 Dec,2000    */
/* 	 removed and replaced with respective	                                 */
/*   functions.                                                              */
/*                                                                           */
/*   Added serial transmission monitoring   Dave Richardson   15 Nov, 2002   */
/*   with timeout to ComSend                                                 */
/*                                                                           */
/*   $Revision::   1.2      $                                                */
/*                                                                           */
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETRIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.

Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <formatio.h>
#include <rs232.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>

#include "com.h"
#include "commreg.h"
#include "version.h"

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

// support up to 32 com ports
// if set something other than 0 then that port is already connected.
// e.g. if comPortConnected[1] == 0 means com port 1 is not in use

static int comPortsConnected[MAX_SUPPORTED_COM_PORT + 1] = { 0 };

// all supported baud rates
static const int BAUD[] = {
    110,
    150,
    300,
    600,
    1200,
    2400,
    4800,
    9600,
    14400,
    19200,
    28800,
    38400,
    56000,
    57600,
    115200,
    128000,
    256000
};
int QtyBauds = sizeof(BAUD)/sizeof(int);

char *(szComParity[]) = {
	{"N"},
	{"O"},
	{"E"},
	{"M"},
	{"S"}
};

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     ComEstablish                                                    */
/*                                                                           */
/* Purpose:  Auto-detect the remote device's com port settings               */
/*           and what com port it's connected to.                            */
/*                                                                           */
/* Example Call:   int ComEstablish(COM_STRUCT* com, int noAutoDetect);      */
/*                                                                           */
/* Input:    com - a pointer to a com structure                              */
/*           noAutoDetect - if true don't do the auto-detect                 */
/*                                                                           */
/* Output:   int - SUCCESS or FAILURE                                        */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
int ComEstablish(COM_STRUCT* com, int noAutoDetect)
{
	int ret = FAILURE;

	// just do this call here, so that when a client needs this information
	// we will have already determined all the availible com ports.
	ComFindAllValidPorts();

	// read in last known good com parameters from Window registry
	if(ReadComConfig(com) == SUCCESS){
		// try to connect using just the last know good port parameters first
		if(Connect(com) == SUCCESS){
			// if pointer is valid, then callback UI letting know detect success
			if(com->pComDetectStatusFunc){
				com->pComDetectStatusFunc(com, ESTABLISH_COM_SUCCESS);
			}

			return SUCCESS;   // success
		}
	}

	// last known port parameters didn't work, so now if auto detect not disabled, 
	// try all other combinations of ports and settings
	if(!com->autoDetectDisabled && !noAutoDetect){
		ret = ConnectAllPortParams(com);
	}
	else{
		// if pointer is valid, then callback UI letting know detect failure
		if(com->pComDetectStatusFunc){
			com->pComDetectStatusFunc(com, ESTABLISH_COM_FAILURE);
		}
	}

	return ret;   
}


/*****************************************************************************/
/* Name:     ComResetEstablish                                                      
/*                                                                           
/* Purpose:  The com module remembers when a com port is already
/*  in use. So, call this routine to reset the com ports connected struct.
/*  Usefull when you want to redetect all remote instruments, call this
/*  before redetection.
/*                                          
/*                                                                           
/* Example Call:  void ComResetEstablish(void);                                    
/*                                                                           
/* Input:    input1 -	description          
/*                                                                           
/* Output:   None                                                       
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void ComResetEstablish(void)
{
	int i;

	for(i = 0; i < MAX_SUPPORTED_COM_PORT + 1; i++){
		comPortsConnected[i] = 0;
	}

	return;
}


/*****************************************************************************/
/* Name:    ComSend                                                   
/*                                                                           
/* Purpose:  Send a message to the remote device.
/*                                                                           
/* Example Call:   int ComSend(char* msg, int len, COM_STRUCT* com);                                    
/*                                                                           
/* Input:   msg - a pointer to the message being sent
/*  len - the length of the string pointed to by msg
/*  com - a pointer to a com structure          
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int ComSend(char* msg, int len, COM_STRUCT* com)
{
	double timeOut;

	#ifdef DEMOMODE
	return SUCCESS;
	#else
	int bytes;

	bytes = ComWrt(com->port, msg, len);
	if(bytes == len){
		timeOut = Timer() + com->timeout;
		while( (len = GetOutQLen(com->port)) > 0){
			if(len < bytes){
				timeOut = Timer() + com->timeout;
				bytes = len;
			}
			else if(Timer() >= timeOut){
				return FAILURE;
			}
		}
		return SUCCESS;   // success
	}
	else{
		return FAILURE;   // failure
	}
	#endif // end #ifdef DEMOMODE
}


/*****************************************************************************/
/* Name:     ComRecv                                                     
/*                                                                           
/* Purpose:  Receive a message from the remote device.
/*                                          
/*                                                                           
/* Example Call:   int ComRecv(char* msg, int len, int* actual, COM_STRUCT* com);                                    
/*                                                                           
/* Input:    msg - a pointer to string storage for the received msg
/*  len - the max length of the string that can be stored in msg
/*  actual - the number of bytes actually read
/*  com - a pointer to a com structure          
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int ComRecv(char* msg, int len, int* actual, COM_STRUCT* com)
{
	#ifdef DEMOMODE
	return SUCCESS;
	#else
	*actual = ComRd(com->port, msg, len);
	if(*actual){
		return SUCCESS;   // success
	}
	else{
		return FAILURE;   // failure
	}
	#endif // end #ifdef DEMOMODE
}


/*****************************************************************************/
/* Name:     ComTerminate                                                      
/*                                                                           
/* Purpose:  Close the communication connection with the 
/*  remote device.
/*                                          
/*                                                                           
/* Example Call:   int ComTerminate(COM_STRUCT* com);                                    
/*                                                                           
/* Input:    com - a pointer to a com structure         
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int ComTerminate(COM_STRUCT* com)
{
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors
	int retval;

	#ifdef DEMOMODE
	retval = SUCCESS;
	#else
	nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if testing for a non-existent port
	retval = CloseCom(com->port);
	SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
	#endif // end #ifdef DEMOMODE

	return retval;
}


/*****************************************************************************/
/* Name:     Connect                                                         
/*                                                                           
/* Purpose:  Try to connect to the remote device                             
/*           using the port parameters in the provided com struct.           
/*                                                                           
/* Example Call:   static int Connect(COM_STRUCT* com);                      
/*                                                                           
/* Input:    com - a pointer to a com structure                              
/*                                                                           
/* Output:   int - SUCCESS if connected, 
/*                 NO_COM_PORT if the com port did not exist 
/*                             (or is being used already by another device), 
/*              or TEST_STRING_FAILED if the message sent to the remote device 
/*                                    did not get a correct response back.   
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                                           
/*                                                                           
/* Return:   <None>                                                          
/*                                                                           
/*****************************************************************************/
static int Connect(COM_STRUCT* com)
{
	int err;
	int ret;
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors

	// only try connecting to a com port if it matches the unique ID (meaning
	// we already connected once on this port, but lets try again) or the 
	// com port is not connected to any device, or if autodetect is disabled just
	// do it!
	if(comPortsConnected[com->port] == com->uniqueID
	|| comPortsConnected[com->port] == 0
	|| com->autoDetectDisabled){
		// try to open the port using the params within the com struct	
		nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if testing for a non-existent port
		err = OpenComConfig(com->port, "", com->baud, com->parity, com->databits, 
		                    com->stopbits, com->inQueueSize, com->outQueueSize);

		#ifdef DEMOMODE
		err = SUCCESS;
		#endif

		if(!err){
			// how long we wait for chars to arrive
			SetComTime(com->port, com->timeout);

			// try the test string to see if we get any response
			if(TryTestStr(com) == SUCCESS){
				comPortsConnected[com->port] = com->uniqueID;
				ret = SUCCESS;   // success
			}
			else{
				// test string didn't work, close port
				CloseCom(com->port);
				ret = TEST_STRING_FAILED;
			}
		}
		else{
			ret = NO_COM_PORT;
		}
		SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
	}
	else{
		ret = NO_COM_PORT;	// port is being used by another device
	}

	return ret;
}


/*****************************************************************************/
/* Name:     ConnectAllPortParams                                                      
/*                                                                           
/* Purpose:  Cycle through the range of port parameters as 
/*  defined within the com struct trying to connect to remote instrument.
/*                                                                           
/* Example Call:  static int ConnectAllPortParams(COM_STRUCT* com);                                    
/*                                                                           
/* Input:    com - a pointer to a com structure          
/*                                                                           
/* Output:   int - SUCCESS if connected to instrument, otherwise FAILURE                                                        
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static int ConnectAllPortParams(COM_STRUCT* com)
{
	int baudCnt;
	int baudMinCnt;
	int baudMaxCnt;
	double stopTime;
	int connectRet;
    unsigned long comPortsBitMask;
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors

	// if pointer is valid, then callback UI to let know we are starting the 
	// instrument auto-detect process
	if(com->pComDetectStatusFunc){
		com->pComDetectStatusFunc(com, ESTABLISH_COM_STARTING);
	}

	// set the starting min baud index
	for(baudMinCnt = 0; baudMinCnt < QtyBauds; baudMinCnt++){
		if(com->baudMin == BAUD[baudMinCnt]){
			break;
		}
	}

	// set the starting max baud index
	for(baudMaxCnt = 0; baudMaxCnt < QtyBauds; baudMaxCnt++){
		if(com->baudMax == BAUD[baudMaxCnt]){
			break;
		}
	}

	// cycle through all possible port connections as provided by the caller in 
	// the min/max values in the COM_STRUCT, proceeding from lowest to highest
	// for those ports that actually exist
	for(com->port = com->portMin, comPortsBitMask = ComFindAllValidPorts();
	    com->port <= com->portMax;
	    com->port++, comPortsBitMask >>= 1){
	    // if port does not exist or is already taken, do not attempt to connect
		if((comPortsBitMask & 0x1L) == 0
		|| (comPortsConnected[com->port] != 0 && comPortsConnected[com->port] != com->uniqueID)){
			continue;
		}
		for(baudCnt = baudMaxCnt; baudCnt >= baudMinCnt; baudCnt--){
			com->baud = BAUD[baudCnt];
			// data bits go from max to min (e.g. try 8, then 7, then 6...)
			for(com->databits = com->databitsMax; com->databits >= com->databitsMin; com->databits--){
				for(com->stopbits = com->stopbitsMin; com->stopbits <= com->stopbitsMax; com->stopbits++){
					for(com->parity = com->parityMin; com->parity <= com->parityMax; com->parity++){
						// if pointer is valid, then callback UI to let know
						// another com port is being tested
						if(com->pComDetectStatusFunc){
							if(com->pComDetectStatusFunc(com, ESTABLISH_COM_IN_PROGRESS) != 0){
								return FAILURE;  // UI said to cancel detection
							}
						}
						connectRet = Connect(com);
						if(connectRet == SUCCESS){
							// write a new config file with the new connect params
							WriteComConfig(com);

							// if pointer is valid, then callback UI to let know
							// this com port was detected successfully
							if(com->pComDetectStatusFunc){
								com->pComDetectStatusFunc(com, ESTABLISH_COM_SUCCESS);
							}

							nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if testing for a non-existent port
							FlushInQ( com->port);
							FlushOutQ(com->port);
							SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
							Delay(0.1);   // wait for flush to occur

							return SUCCESS;   // success
						} // end if(connectRet == SUCCESS)
						else if(connectRet == TEST_STRING_FAILED){
							// delaying between com connect tries
							stopTime = Timer() + com->delayBetweenComTries;
							while(Timer() < stopTime){
								// let UI run every spin to catch button presses
								ProcessSystemEvents();  

								// if pointer is valid, then callback UI to let know
								// another com port is being tested
								if(com->pComDetectStatusFunc){
									if(com->pComDetectStatusFunc(com, ESTABLISH_COM_IN_PROGRESS) != 0){
										return FAILURE;  // UI said to cancel detection
									}
								}
							} // end while(Timer() < stopTime)
						} // end if(connectRet == TEST_STRING_FAILED)
						else if(connectRet == NO_COM_PORT){
							// delaying between com connect tries
							stopTime = Timer() + com->delayBetweenComTries;
							while(Timer() < stopTime){
								// let UI run every spin to catch button presses
								ProcessSystemEvents();  

								// if pointer is valid, then callback UI to let know
								// another com port is being tested
								if(com->pComDetectStatusFunc){
									if(com->pComDetectStatusFunc(com, ESTABLISH_COM_NO_PORT) != 0){
										return FAILURE;  // UI said to cancel detection
									}
								}
							} // end while(Timer() < stopTime)
						} // end if(connectRet == NO_COM_PORT)
					} // for(com->parity++ ...)
				} // end for(com->stopbits++ ...)
			} // end for(com->databits-- ...)
		} // end for(baudCnt++ ...)
	} // end for(com->port++ ...)

	// if pointer is valid, then callback UI to let know the detect sequence failed
	if(com->pComDetectStatusFunc){
		com->pComDetectStatusFunc(com, ESTABLISH_COM_FAILURE);
	}

	return FAILURE;
}


/*****************************************************************************/
/* Name:     TryTestStr                                                    
/*                                                                           
/* Purpose:  Try sending the test string to remote instrument to
/*  see if we get a correct response. If not, the com port setting must not
/*  be right or instrument is not connected.
/*                                          
/*                                                                           
/* Example Call:   static int TryTestStr(COM_STRUCT* com);                                    
/*                                                                           
/* Input:    com - a pointer to a com structure       
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static int TryTestStr(COM_STRUCT* com)
{
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors
	int bytes; // qty bytes written/read
	char in_buff[MAX_COM_STR] = { NULL };
	int retval = FAILURE;

	nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if testing for a non-existent port
	// send out test string 
	bytes = ComWrt(com->port, com->testStr, com->testStrLen);

	#ifdef DEMOMODE
	bytes = com->testStrLen;
	#endif

	if(bytes){
		// read response from remote device
		bytes = ComRd(com->port, in_buff, com->testStrResponseLen);
		#ifdef DEMOMODE
		if(strcmp(com->name, "PTS") == 0){
			bytes = 8;
			strcpy(in_buff, "Revision");
		}
		else if(strcmp(com->name, "840") == 0){
			bytes = 7;
			memcpy(in_buff, "\x06\x00\x04\x01\x01\x6b\x30", 7);
		}
		#endif // #ifdef DEMOMODE
		// if we got some number of bytes in, then check to see if 
		// the returned string is correct by calling the function pointed
		// to by the response valid pointer.
		if(bytes){
			if(com->pResponseValidFunc(in_buff, bytes) == SUCCESS){
				retval = SUCCESS;
			}
		}
	}
	SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag

	return retval;
}


/*****************************************************************************/
/* Name:     ReadComConfig                                                   
/*                                                                           
/* Purpose:  Read the last successful port connection settings.
/*  These will be tried first before auto-detecting using possible port
/*  settings.
/*                                                                                                                    
/* Example Call:   static int ReadComConfig(COM_STRUCT* com);                                    
/*                                                                           
/* Input:    com - a pointer to a com structure         
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int ReadComConfig(COM_STRUCT* com)
{
	RegComStruct readComParams;

	if(GetCommParams(&readComParams, com->registryKey) != 0){
		return FAILURE;
	}

	if(readComParams.port > com->portMax ||
	   readComParams.port < com->portMin ){
		return FAILURE;
	}

	com->port     = readComParams.port;
	com->baud     = readComParams.baud;
	com->databits = readComParams.databits;
	com->stopbits = readComParams.stopbits;
	com->parity   = readComParams.parity;
	com->autoDetectDisabled = readComParams.autoDetectDisabled;

	return SUCCESS;   // success, read file                
}


/*****************************************************************************/
/* Name:     WriteComConfig                                                   
/*                                                                           
/* Purpose:  Write the last successful port connection settings.
/* These will be tried first before auto-detecting using all possible port
/*  settings.
/*                                                                                                                    
/* Example Call:   static int WriteComConfig(COM_STRUCT* com);                                    
/*                                                                           
/* Input:   com - a pointer to a com structure          
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int WriteComConfig(COM_STRUCT* com)
{
	RegComStruct writeComParams;

	writeComParams.port     = com->port;
	writeComParams.baud     = com->baud; 
	writeComParams.databits = com->databits;
	writeComParams.stopbits = com->stopbits;
	writeComParams.parity   = com->parity;
	writeComParams.autoDetectDisabled = com->autoDetectDisabled;

	if(SetCommParams(&writeComParams, com->registryKey) != 0){
		return FAILURE;
	}

	return SUCCESS;   // success, write file                
}


/*****************************************************************************/
/* Name:     ComFlushInQ                                                     
/*                                                                           
/* Purpose:  Flush all characters from the in queue.
/*                                                                                                                   
/* Example Call:   int ComFlushInQ(COM_STRUCT* com);                                    
/*                                                                           
/* Input:    com - a pointer to a com structure          
/*                                                                           
/* Output:   int - the byte from the selected port, unless and error occurs. If an
/*      error occurs, the result is a negative CVI error code.                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int ComFlushInQ(COM_STRUCT* com)
{
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors
	int retval;

	#ifdef DEMOMODE
	retval = SUCCESS;
	#else
	nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if testing for a non-existent port
	retval = FlushInQ(com->port);
	SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
	#endif // #ifdef DEMOMODE

	return retval;
}

int CharsInQ(COM_STRUCT* com)
{
	int len = 0;
	int retval;

	#ifdef DEMOMODE
	retval = TRUE;
	#else
	if((len = GetInQLen(com->port)) > 0){
		retval =  TRUE;
	}
	else{
		retval = FALSE;
	}
	#endif // #ifdef DEMOMODE

	return retval;
}


/*****************************************************************************/
/* Name:    ComFlushOutQ                                                      
/*                                                                           
/* Purpose:  Flush all characters from the out queue.
/*                                          
/*                                                                           
/* Example Call:   int ComFlushOutQ(COM_STRUCT* com);                                    
/*                                                                           
/* Input:    com - a pointer to a com structure       
/*                                                                           
/* Output:   int - the byte from the selected port, unless and error occurs. If an
/*      error occurs, the result is a negative CVI error code.                                                        
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int ComFlushOutQ(COM_STRUCT* com)
{
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors
	int retval;

	#ifdef DEMOMODE
	retval = SUCCESS;
	#else
	nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if testing for a non-existent port
	retval = FlushOutQ(com->port);
	SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
	#endif // #ifdef DEMOMODE

	return retval;
}


/*****************************************************************************/
/* Name:     ComFindAllValidPorts                                                     
/*                                                                           
/* Purpose:  Return a bitmap mask of all the valid COM ports
/*  on the system, up to 32 port. A 1 means that port exists and 0 
/*  means it doesn't exist. This routine takes a second or so to complete,
/*  so only the initial call goes out gets the valid com ports. Subsequent
/*  call return the previously obtained information. 
/*                                          
/*                                                                           
/* Example Call:   unsigned long ComFindAllValidPorts(void);                                    
/*                                                                           
/* Input:    None        
/*                                                                           
/* Output:   unsigned long - a bit mask of all availible COM ports.
/*           LSB is COM 1 and MSB is COM 32.                                                        
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
unsigned long ComFindAllValidPorts(void)
{
	static int once = FALSE;
	static unsigned long bitmask = 0;
	int port;
	int err;
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors

	if(once == FALSE){
		nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if testing for a non-existent port
		for(port = 1; port <= 32; port++){
			// try to open each port using some default values
			err = OpenComConfig(port, "", 9600, 0, 8, 1, 32, 32);
			CloseCom(port);

			#ifdef DEMOMODE
			err = SUCCESS;
			#endif

			// if port opened successfully 'OR in' com port bit
			if(!err){
				bitmask |= 0x80000000;
			}

			if(port < 32){
				bitmask >>= 1;
			}
		}
		SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
		once = TRUE;
	}

	return bitmask;
}
