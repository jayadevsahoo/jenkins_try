
#ifndef BdSettingValues_HH
#define BdSettingValues_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: BdSettingValues - Manager of the Bd Setting Values.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BdSettingValues.hhv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

//@ Usage-Classes
#include "BatchValue.hh"
#include "SettingValueMgr.hh"

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
class BatchSettingValues;
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
//@ End-Usage


class BdSettingValues : public SettingValueMgr
{
  public:
    BdSettingValues(const BdSettingValues& bdValues);
    BdSettingValues(SettingValueMgr::ChangeCallbackPtr pInitCallback);
    BdSettingValues(void);
    ~BdSettingValues(void);

    const BoundedValue& getBoundedValue(
				const SettingId::SettingIdType boundedId
				       ) const;
    DiscreteValue       getDiscreteValue(
				const SettingId::SettingIdType discreteId
					) const;
  
    void  setBoundedValue (const SettingId::SettingIdType boundedId,
			   const BoundedValue& newValue);
    void  setDiscreteValue(const SettingId::SettingIdType discreteId,
			   const DiscreteValue newValue);

    // for copying between other 'BdSettingValues' instances...
    void  operator=(const BdSettingValues& bdValues);
    void  copyFrom (const BdSettingValues& bdValues);

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
    // for copying between 'BatchSettingValues' instances...
    void  operator=(const BatchSettingValues& batchValues);
    void  copyFrom (const BatchSettingValues& batchValues);
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

    Boolean         operator==(const BdSettingValues& bdValues) const;
    inline Boolean  operator!=(const BdSettingValues& bdValues) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
#if defined(SIGMA_DEVELOPMENT)
    Boolean  isAssumption1Valid_(void) const;
#endif // defined(SIGMA_DEVELOPMENT)

    //@ Data-Member:  arrBdValues_
    // Fixed array containing the values for each of the BD settings.
    BatchValue  arrBdValues_[::NUM_BD_SETTING_IDS];
};


// Inlined methods...
#include "BdSettingValues.in"


#endif // BdSettingValues_HH 
