
// PB840SettingsDoc.cpp : implementation of the CPB840SettingsDoc class
//

#include "stdafx.h"
#include "PB840FakeGui.h"

#include "PB840SettingsDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPB840SettingsDoc

IMPLEMENT_DYNCREATE(CPB840SettingsDoc, CDocument)

BEGIN_MESSAGE_MAP(CPB840SettingsDoc, CDocument)
	
END_MESSAGE_MAP()


// CPB840SettingsDoc construction/destruction

CPB840SettingsDoc::CPB840SettingsDoc()
{
	// TODO: add one-time construction code here
	memset( &arrXmitElems, 0, sizeof(arrXmitElems) );
}

CPB840SettingsDoc::~CPB840SettingsDoc()
{
}

BOOL CPB840SettingsDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	memset( &arrXmitElems, 0, sizeof(arrXmitElems) );

	for (Int32 i = 0; i < SettingTable::GetNumSettingEntries(); ++ i )
	{
		 const SettingEntry & settingEntry = SettingTable::GetSettingTableEntry(i);
		 
		 if ( settingEntry.boundedDefValue != NULL && settingEntry.type == SET_BOUNDED )
		 {	 
			 float val = static_cast<float>( atof(settingEntry.boundedDefValue) );
			 arrXmitElems[i].boundedValue = val;
			 arrXmitElems[i].boundedPrec = settingEntry.defParam1;
		 }
		 else if ( settingEntry.type == SET_DISCRETE )
		 {
			 // note defParam1 field is used for two purpose
			 arrXmitElems[i].discreteValue = settingEntry.defParam1;
		 }
		 
	}


	return TRUE;
}




// CPB840SettingsDoc serialization

void CPB840SettingsDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here

		for ( Uint i = 0; i < NUM_BD_SETTING_IDS; ++ i )
		{
			ar << arrXmitElems[i];
		}

		
	}
	else
	{
		for ( Uint i = 0; i < NUM_BD_SETTING_IDS; ++ i )
		{
			ar >> arrXmitElems[i];
		}
	}
}


// CPB840SettingsDoc diagnostics

#ifdef _DEBUG
void CPB840SettingsDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPB840SettingsDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


BOOL CPB840SettingsDoc::GetSettingElement(Uint32 idx, SettingXmitData & data)
{
	if ( idx >= 0 && idx < countof( arrXmitElems ) )
	{
		data = arrXmitElems[idx];
		return TRUE;
	}

	return FALSE;
}

BOOL CPB840SettingsDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	CFile		pb840setFile( lpszPathName, CFile::modeRead );  
    CArchive	ar( &pb840setFile, CArchive::load );

	Serialize( ar );

	return TRUE;
}

BOOL CPB840SettingsDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	return CDocument::OnSaveDocument( lpszPathName );
}

void CPB840SettingsDoc::SetSettingElement(Uint32 idx, const SettingXmitData & data)
{
	arrXmitElems[idx] = data;
	SetModifiedFlag(TRUE);
}

CPB840SettingsDoc * CPB840SettingsDoc::GetDocInstance()
{
	POSITION pos = ::AfxGetApp()->GetFirstDocTemplatePosition();

	CDocTemplate* pDocTemplate = ::AfxGetApp()->GetNextDocTemplate( pos );

	ASSERT( pDocTemplate );

	if ( pDocTemplate != NULL )
	{
		POSITION docPos = pDocTemplate->GetFirstDocPosition();
		CDocument* pDoc = pDocTemplate->GetNextDoc( docPos );

		CPB840SettingsDoc* p840SettingDoc = dynamic_cast<CPB840SettingsDoc*>( pDoc );

		// only one doc available, should not NULL
		ASSERT( p840SettingDoc );

		return p840SettingDoc;
	}

	return NULL;
}