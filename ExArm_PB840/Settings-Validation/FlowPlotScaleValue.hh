
#ifndef FlowPlotScaleValue_HH
#define FlowPlotScaleValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  FlowPlotScaleValue - Scales used for the Flow Waveform.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/FlowPlotScaleValue.hhv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: mnr    Date: 14-Jan-2009    SCR Number: 5987
//  Project:  NEO
//  Description:
//      Added new flow plot scale range -2 to 2 LPM.
//
//  Revision: 003   By: mnr    Date: 28-Dec-2009    DR Number: 5987
//  Project:  NEO
//  Description:
//	    New scale -5 to 5 added.
//
//  Revision: 002   By: sah    Date: 19-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  added additional scale for neonate's lower flows
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct FlowPlotScaleValue
{
  //@ Type:  FlowPlotScaleValueId
  // All of the possible values for FlowPlotScaleSetting.
  enum FlowPlotScaleValueId
  {
    // $[01159] -- values of the range of Flow-Time Plot Axis...
	MINUS_2_TO_2_LPM,
	MINUS_5_TO_5_LPM,
    MINUS_10_TO_10_LPM,
    MINUS_20_TO_20_LPM,
    MINUS_40_TO_40_LPM,
    MINUS_80_TO_80_LPM,
    MINUS_120_TO_120_LPM,
    MINUS_160_TO_160_LPM,
    MINUS_200_TO_200_LPM,

    TOTAL_FLOW_SCALE_VALUES
  };
};


#endif // FlowPlotScaleValue_HH 
