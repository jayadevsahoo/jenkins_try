#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  ServiceBaudRateSetting - Service-Mode Baud Rate Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the baud rate for
//  the ventilator's serial interface, with respect to the Service-Mode
//  external interface.  This class inherits from 'BatchDiscreteSetting'
//  and provides the specific information needed for the representation of
//  Service-Mode's baud rate.  This information includes the set of values
//  that this setting can have (see 'BaudRateValue.hh'), and this
//  batch setting's new-patient value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ServiceBaudRateSetting.ccv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: quf    Date: 22-Aug-2001    DR Number: 5769
//  Project:  GuiComms
//  Description:
//	Modified to exclude the 38400 baud rate setting from Service Mode.
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Corrected incomplete comment.
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "ServiceBaudRateSetting.hh"
#include "BaudRateValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ServiceBaudRateSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[07046] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ServiceBaudRateSetting::ServiceBaudRateSetting(void)
	     : BatchDiscreteSetting(SettingId::SERVICE_BAUD_RATE,
				    Setting::NULL_DEPENDENT_ARRAY_,
				    BaudRateValue::SERVICE_MODE_TOTAL_BAUD_RATES)
{
  CALL_TRACE("ServiceBaudRateSetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~ServiceBaudRateSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ServiceBaudRateSetting::~ServiceBaudRateSetting(void)
{
  CALL_TRACE("~ServiceBaudRateSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is ALWAYS changeable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ServiceBaudRateSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ServiceBaudRateSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  const AcceptedContext&  rAcceptedContext = ContextMgr::GetAcceptedContext();

  DiscreteValue  newPatientValue;

  if (rAcceptedContext.areBatchValuesInitialized())
  {   // $[TI1]
    // when the Accepted Context has previosly-initialized values, use the
    // previous value of this setting...
    newPatientValue = rAcceptedContext.getBatchDiscreteValue(
					       SettingId::SERVICE_BAUD_RATE
							    );
  }
  else
  {   // $[TI2]
    // $[07046] The setting's new-patient value ...
    newPatientValue = BaudRateValue::BAUD_9600_VALUE;
  }

  return(newPatientValue);
}


#if defined(SIGMA_DEVELOPMENT)

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceBaudRateSetting::SoftFault(const SoftFaultID  softFaultID,
				  const Uint32       lineNumber,
				  const char*        pFileName,
				  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          SERVICE_BAUD_RATE_SETTING, lineNumber, pFileName,
                          pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
