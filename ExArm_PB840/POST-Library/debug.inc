;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================
;
;
;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: debug.inc - includes misc items for debugging of POST 
;
;---------------------------------------------------------------------
;@ Interface-Description
;  This contains the defintions and macros used for debugging the POST code.
;
;@ Implementation-Description
;
;  This module contains all the EQU's, MACRO's, and other assembly language
;  constructs required for debugging and unit test of modules in POST.
;
;@ Restrictions
;  
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
;
;@(#) $Header:   /840/Baseline/POST-Library/vcssrc/debug.inv   25.0.4.0   19 Nov 2013 14:16:38   pvcs  $
;
;@ Modification-Log
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================
;
                  IFDEF SIGMA_DEBUG ;{

DEBUG_PORT        EQU   Z85230_COMMAND_A
;--------------------------------------------------------------------
DEBUG_TRACE_ON    MACRO ;{
                  andi  #$3fff,sr
                  ori   #$8000,sr
                  ENDM  ;DEBUG_TRACE_ON}

;--------------------------------------------------------------------
DEBUG_TRACE_OFF   MACRO ;{
                  andi  #$3fff,sr
                  ENDM  ;DEBUG_TRACE_ON}

;--------------------------------------------------------------------
Z85WRITE          MACRO  value,dst  ;{
                  move.b  #dst,DEBUG_PORT
                  move.b value,DEBUG_PORT
                  ENDM ;Z85WRITE}

;--------------------------------------------------------------------
Z85READ           MACRO   src,dst   ;{
                  move.b  #src,DEBUG_PORT
                  move.b  DEBUG_PORT,dst
                  ENDM ;Z85READ}

;--------------------------------------------------------------------
DELAY             MACRO count
                  REPT  count
                  nop
                  ENDR
                  ENDM

BAUD_RATE         EQU   $0a    ;$16=9.6K  $0a=19.2K  $04=38.4K  $00=115.2K   
;--------------------------------------------------------------------
DEBUG_INITIALIZE_PORT   MACRO ;{
                  Z85WRITE    #%01000000,9   ; reset channel b
                  DELAY    16                ; delay for reset recover
                  Z85WRITE    #%01010101,11  ; set all clocks to baud rate gen
                  Z85WRITE          #$00,13  ; set baud rate most  sig. byte
                  Z85WRITE    #BAUD_RATE,12  ; set baud rate least sig. byte
                  Z85WRITE    #%10000001,14  ; baud clock external and enabled
                  Z85WRITE    #%01000100,4   ; /16, 1 stop bit, odd parity disabled
                  Z85WRITE    #%11101010,5   ; Tx = 8 bits , enabled
                  Z85WRITE    #%11100001,3   ; Rx = 8 bits , enabled
                  ENDM ;DEBUG_INITIALIZE_PORT}

TxEMPTY           EQU         2
;--------------------------------------------------------------------
DEBUG_SEND_CHAR   MACRO the_char  ;{
                  LOCAL tx_char_wait
tx_char_wait:     clr.b    DEBUG_PORT          ; reset reg ptr and get reg 0
                  btst.b   #TxEMPTY,DEBUG_PORT ; ready to send?
                  beq.s    tx_char_wait
                  move.b   the_char,DEBUG_PORT+2  
                  ENDM  ;DEBUG_SEND_CHAR}

;--------------------------------------------------------------------
DEBUG_NEWLINE     MACRO
                  DEBUG_SEND_CHAR   #10
                  ENDM

;--------------------------------------------------------------------
DEBUG_SEND_STRING MACRO the_string  ;{ the_string must be an address register
                  LOCAL tx_char_wait,char_loop,exit
char_loop:        tst.b    (the_string)              ; at the end of the string?
                  beq.s    exit                      ; exit if so    
                  DEBUG_SEND_CHAR (the_string)+      ; send the char and advance
                  bra.s    char_loop
exit:
                  ENDM  ;DEBUG_SEND_STRING}

;--------------------------------------------------------------------
DEBUG_PRINT       MACRO value ;{
                  LOCAL skip,the_text
                  bra   skip
the_text:         DC.B  value,0
                  ALIGN 2
skip:
                  move.l   a0,-(sp)    ; freeup a pointer register
                  move.l   #the_text,a0
                  DEBUG_SEND_STRING a0
                  move.l   (sp)+,a0    ; restore a0's original value
                  ENDM  ;DEBUG_PRINT}


;--------------------------------------------------------------------
DEBUG_SEND_HEX    MACRO                   ;{
                  LOCAL exit,save_d0
                     move.l   d0,save_d0    ;use d0 to passing the value
                     IFC   \0,'l'
                        IFGT  NARG-1
                           move.l   \1,\2,d0
                        ELSEC
                           move.l   \1,d0
                        ENDC
                        jsr   DebugSendHexLong
                     ELSEC
                        IFC   \0,'w'
                           IFGT  NARG-1
                              move.w   \1,\2,d0
                           ELSEC
                              move.w   \1,d0
                           ENDC
                           jsr   DebugSendHexWord
                        ELSEC
                           IFC   \0,'b'
                              IFGT  NARG-1
                                 move.b   \1,\2,d0
                              ELSEC
                                 move.b   \1,d0
                              ENDC
                              jsr   DebugSendHexByte
                           ELSEC 
                              FAIL  1
                           ENDC  
                        ENDC
                     ENDC
                     move.l save_d0,d0      ;restore the original value of d0
                     bra    exit
save_d0:             DS.L  1
exit:
                  ENDM  ;DEBUG_SEND_HEX}



BRANCH_IF_GUI     MACRO   branch_address  ;{
                  btst.b   #7,IO_REGISTER_1     ;bit 7 = 0 means it's a GUI
                  beq      branch_address
                  ENDM                    ;}BRANCH_IF_GUI


DEBUG_TEST_POINT  MACRO    name        ;{
                  XDEF     DEBUG_&&name
DEBUG_&&name:
                  ENDM                 ;}DEBUG_TEST_POINT

                  ENDC    ;} // SIGMA_DEBUG 
