#include "stdafx.h"
// stdafx.cpp : source file that includes just the standard includes
// ExOmap.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

void WINAPIV NKDbgPrintfW(LPCWSTR lpszFmt, ...)
{

	
}

BOOL TransparentImage(HDC hdcDest, 
LONG DstX, 
LONG DstY, 
LONG DstCx, 
LONG DstCy,
HDC hSrc, 
LONG SrcX, 
LONG SrcY, 
LONG SrcCx, 
LONG SrcCy, 
COLORREF TransparentColor
)
{
	/*
HDC			hdcMask;
HBITMAP		MaskBits;
HBITMAP		OldMaskBits;
BOOL		Rtn;
COLORREF	backClr;

	hdcMask = CreateCompatibleDC(hdcDest);
	
	MaskBits = CreateBitmap(DstCx, DstCy, 1, 1, NULL);

	if (hdcMask && MaskBits)
	{
		OldMaskBits = (HBITMAP)SelectObject(hdcMask, MaskBits);


		// Set the background colour of the colour image to the colour
		// you want to be transparent.
		backClr=SetBkColor(hSrc, TransparentColor);

		//Create Mask of format - Need it to be white everywhere its suppose to be transparent
		//Sets every pixel set to the background color to white(FF) 
		//and every pixel not set to the background color to black(0)
		StretchBlt(hdcMask, 0, 0, DstCx, DstCy, hSrc, SrcX, SrcY, SrcCx, SrcCy,SRCCOPY);

		//And in the Mask with the image underlay which will cut out or turn black
		//everything that the format drew in color.			
		Rtn=::BitBlt(	hdcDest,
						DstX,
						DstY,
						DstCx,
						DstCy,
						hdcMask,
						0,
						0,
						SRCAND);

		//Paint in the format image which will only be painted in the black cutout areas		
		return StretchBlt(	hdcDest, 
							DstX, 
							DstY, 
							DstCx, 
							DstCy,
							hSrc, 
							SrcX, 
							SrcY, 
							SrcCx, 
							SrcCy, 
							SRCPAINT
							);
		
		SelectObject(hdcMask, (HBITMAP)OldMaskBits);
		DeleteObject(MaskBits);
		DeleteDC(hdcMask);
		SetBkColor(hSrc, backClr);


	}
	else
	{*/
		return TransparentBlt(	hdcDest, 
							DstX, 
							DstY, 
							DstCx, 
							DstCy,
							hSrc, 
							SrcX, 
							SrcY, 
							SrcCx, 
							SrcCy, 
							TransparentColor
							);
	//}
}
