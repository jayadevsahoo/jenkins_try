
#ifndef PressureBasedPhase_HH
#define PressureBasedPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PressureBasedPhase - Base class for all pressure based inspiration
//		phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureBasedPhase.hhv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: syw     Date:  14-Feb-2000    DR Number: 5639
//  Project:  NeoMode
//  Description:
//		Move ki_ data and access members to protected so that OscPcvPhase
//		can access.
//
//  Revision: 007  By: sah     Date:  22-Sep-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  added new, neonatal-specific member data items used in
//         the neonatal trajectory calculations
//
//  Revision: 006  By: yyy     Date:  11-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Make PRESSURE_THRESHOLD an extern constant.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Made determineMirrorGain_() protected and peep_.
//
//  Revision: 004 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes.
//
//  Revision: 003 By: syw    Date: 27-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added determineTargetPressure_() method since Settings will not
//			update the setting to the proper value as initially assumed.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "BreathPhase.hh"
#include "Resistance.hh"

//@ End-Usage

extern const Real32 PRESSURE_THRESHOLD ;

class PressureBasedPhase : public BreathPhase
{
  public:
    PressureBasedPhase( void) ;
    virtual ~PressureBasedPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

    virtual void relinquishControl( const BreathTrigger& trigger) ;
    virtual void newCycle( void) ;
    virtual void newBreath( void) ;

	void setPediatricKp( Real32 gain );
    void setAdultKp( Real32 gain );

    void setPediatricKi( Real32 gain );
    void setAdultKi( Real32 gain );

    Real32 getPediatricKp();
    Real32 getAdultKp();

    Real32 getPediatricKi();
    Real32 getAdultKi();

  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) = 0;
	virtual void determineTimeToTarget_( void) = 0 ;
	virtual void determineFlowAccelerationPercent_( void) = 0 ;
	virtual void enableExhalationTriggers_( void) = 0 ;
	virtual void determineKp_( void) ;
	virtual void determineKi_( void) ;
	virtual void updatePressureControllerWf_( void) ;
	virtual void determineTargetPressure_( void) ;
	virtual	Real32 determineMirrorGain_( void) ;

	//@ Data-Member: effectivePressure_
	// steady state pressure level above Peep
	Real32 effectivePressure_ ;

	//@ Data-Member: timeToTarget_
	// time when trajecory should reach effective pressure
	Real32 timeToTarget_ ;

	//@ Data-Member: fap_
	// flow acceleration percentage setting
	Real32 fap_ ;

	//@ Data-Member: biasOffset_
	// minimum pressure for effectivePressure
	Real32 biasOffset_ ;

	//@ Data-Member: kp_
	// proportional gain in lpm/cmH20
	Real32 kp_ ;

	//@ Data-Member: ki_
	// integral gain in lpm/cmH20/msec
	Real32 ki_ ;
	
	//@ Data-Member: targetPressure_
	// control pressure when wf = 1
	Real32 targetPressure_ ;

	//@ Data-Member: peep_
	// peep setting value
	Real32 peep_ ;

  private:
    PressureBasedPhase(const PressureBasedPhase&);		// not implemented...
    void   operator=(const PressureBasedPhase&);	// not implemented...

	void determineWf_( const Real32 refTarget) ;
	void updateKpGain_( const Real32 refTarget, const Real32 targetPressure) ;
	void determineDesPressFilter_( Real32 patPressExh) ;

	//@ Data-Member: ExhResistance_
	// exh side tubing resistance characteristics
	static Resistance ExhResistance_ ;

	//@ Data-Member: overshootOccured_
	// set if patPressInsp > targetPressure
	Boolean overshootOccured_ ;
	
	//@ Data-Member: mirrorGainSensitivity_
	// sensitivity used in mirror gain calculation
	Real32 mirrorGainSensitivity_ ;

	//@ Data-Member: desPressAlpha_
	// alpha constant for patient pressure alpha filter used 
	// in desiredPressure calculation
	Real32 desPressAlpha_ ;

	//@ Data-Member: desPressFilter_
	// output of alpha filter with exhalation pressure as input used
	// in desiredPressure calculation
	Real32 desPressFilter_ ;

	//@ Data-Member: trajectoryAlpha_
	// alpha constant for trajectory alpha filter
	Real32 trajectoryAlpha_ ;

	//@ Data-Member: trajectory_
	// computed pressure trajectory
	Real32 trajectory_ ;

	//@ Data-Member: kiNominal_
	// nominal integral gain in lpm/cmH20/msec
	Real32 kiNominal_ ;

	//@ Data-Member: kiOvershoot_
	// integral gain during an overshoot in lpm/cmH20/msec
	Real32 kiOvershoot_ ;

	//@ Data-Member: isDecrementAlpha_
	// flag set TRUE if conditions are met to begin decrementing pressureAlpha_
	Boolean isDecrementAlpha_ ;

	//@ Data-Member: isPatPressLessThanTraj_
	// flag set if patient pressure is less than trajectory
	Boolean isPatPressLessThanTraj_ ;

	//@ Data-Member: isFreezeGains_
	// flag set to freeze the integral gain
	Boolean isFreezeGains_ ;

	//@ Data-Member: initialRampPress_
	// Initial neonatal ramp trajectory pressure.
	Real32  initialRampPress_;

	//@ Data-Member: transitionTimeMs_
	// neonatal trajectory transition time
	Real32  transitionTimeMs_;

	//@ Data-Member: neoTrajectoryAlpha_
	// neonatal trajectory alpha filter factor
	Real32  neoTrajectoryAlpha_;

	//@ Data-Member: deltaPe_
	// pressure drop due to exp limb
	Real32 deltaPe_ ;

	//@ Data-Member: pediatricKp_
	// kp gain used for pediatric patient circuits
	Real32 pediatricKp_;

	//@ Data-Member: adultKp_
	// kp gain used for adult patient circuits
	Real32 adultKp_;

	//@ Data-Member: pediatricKi_
	// ki gain used for pediatric patient circuits
	Real32 pediatricKi_;

	//@ Data-Member: adultKi_
	// ki gain used for adult patient circuits
    Real32 adultKi_;	
} ;


#endif // PressureBasedPhase_HH 
