#ifndef AlarmDci_HH
#define AlarmDci_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Type: AlarmDci - Interface between Alarm-Analysis and DCI subsystems.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmDci.hhv   25.0.4.0   19 Nov 2013 13:51:14   pvcs  $
//
//@ Modification-Log
//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   502
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline).  Added class to support
//         DCI reporting mechanism.
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//
//   Revision 003   By:   rhj      Date: 07/11/06         DR Number:   5928
//      Project:   RESPM
//      Description:
//      Added support for the SNDF command, which can report
//      low, medium, and high urgency alarms.
//      
//   Revision 004   By:   gdc      Date: 27-Mar-2008      SCR Number:  6407
//      Project:  TREND2
//      Description:
//      Changed AlarmDciArray typedef from private to public to resolve
//      compiler warning.
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"


//@ Usage-Classes
#include "AlarmName.hh"
//@ End-Usage

class AlarmDci
{
  public:

    // Enumeration for DCI reports.
    enum AlarmDciStatus {ALARM_DCI_NULL = -1,   
                         ALARM_DCI_NORMAL, ALARM_DCI_ALARM, ALARM_DCI_RESET,
                         ALARM_DCI_LOW_URGENCY, ALARM_DCI_MEDIUM_URGENCY, 
                         ALARM_DCI_HIGH_URGENCY,
                         MAX_ALARM_DCI};

    typedef Int8 AlarmDciArray[MAX_NUM_ALARMS];

    static void Initialize(void);

#if defined(SIGMA_GUI_CPU) || defined(SIGMA_DEVELOPMENT)
    static AlarmDciStatus GetStatus(AlarmName alarmName);
    static void PutStatus(AlarmName alarmName, AlarmDciStatus alarmDCIStatus);

    static AlarmDciStatus GetStatusF(AlarmName alarmName);
    static void PutStatusF(AlarmName alarmName, AlarmDciStatus alarmDCIStatus);
#endif

    static void          SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName = NULL,
								   const char*        pPredicate = NULL);

  private:
#if defined(SIGMA_GUI_CPU) || defined(SIGMA_DEVELOPMENT)
    static AlarmDciArray& GetAlarmDciArray_(void);
    static AlarmDciArray& GetAlarmDciArrayF_(void);
#endif

    AlarmDci(void);             // Declared only
    AlarmDci(const AlarmDci&);  // Declared only
    ~AlarmDci(void);            // Declared only
    //operator=(const AlarmDci&); // Declared only

};


#endif // AlarmDci_HH 
