
#ifndef SystemDiagLog_HH
#define SystemDiagLog_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  SystemDiagLog - System Diagnostic Code Log.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/SystemDiagLog.hhv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' to non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
#include "DiagCodeLog.hh"
#include "CodeLogEntry.hh"

class  FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES);
//@ End-Usage


class SystemDiagLog : public DiagCodeLog
{
  public:
    SystemDiagLog(void);
    ~SystemDiagLog(void);

    NovRamStatus::InitializationId       initialize(void);
    inline NovRamStatus::VerificationId  verifySelf(void) const;

    void  logDiagnostic(const DiagnosticCode diagnosticCode);

    void  getCurrEntries(AbstractArray(CodeLogEntry)& rArrEntries) const;

    static void  SoftFault(const SoftFaultID softDiagID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  private:
    SystemDiagLog(const SystemDiagLog&);	// not implemented...
    void  operator=(const SystemDiagLog&);	// not implemented...

    //@ Data-Member:  arrEntries_
    // An array to contain each of this log's entries.
    CodeLogEntry  arrEntries_[::MAX_SYS_LOG_ENTRIES];
};


// Inlined methods...
#include "SystemDiagLog.in"


#endif // SystemDiagLog_HH 
