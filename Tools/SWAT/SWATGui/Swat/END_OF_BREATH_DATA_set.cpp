// END_OF_BREATH_DATA_set.h : Implementation of the C_END_OF_BREATH_DATA_set class



// C_END_OF_BREATH_DATA_set implementation

// code generated on Thursday, December 10, 2009, 4:18 PM

#include "stdafx.h"
#include "END_OF_BREATH_DATA_set.h"
IMPLEMENT_DYNAMIC(C_END_OF_BREATH_DATA_set, CRecordset)

C_END_OF_BREATH_DATA_set::C_END_OF_BREATH_DATA_set(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_event_sequence_id = 0;
	m_breathType = "";
	m_exhaledTidalVolume = 0.0;
	m_breathDuration = 0.0;
	m_endExpiratoryPressure = 0.0;
	m_ieRatio = 0.0;
	m_mandFraction = 0.0;
	m_dynamicCompliance = 0.0;
	m_dynamicResistance = 0.0;
	m_phaseType = "";
	m_previousBreathType = "";
	m_breathTypeDisplay = "";
	m_spontInspTime = 0.0;
	m_spontTiTtotRatio = 0.0;
	m_peakExpiratoryFlow = 0.0;
	m_endExpiratoryFlow = 0.0;
	m_nFields = 16;
	m_nDefaultType = dynaset;
}

CString C_END_OF_BREATH_DATA_set::GetDefaultConnect()
{
	return _T("DSN=PostgreSQL30;DATABASE=viking_integration_test;SERVER=127.0.0.1;PORT=5432;UID=postgres;PWD=its4me@1234;SSLmode=disable;ReadOnly=0;Protocol=7.4;FakeOidIndex=0;ShowOidColumn=0;RowVersioning=0;ShowSystemTables=0;ConnSettings=;Fetch=100;Socket=4096;UnknownSizes=0;MaxVarcharSize=255;MaxLongVarcharSize=8190;Debug=0;CommLog=0;Optimizer=0;Ksqo=1;UseDeclareFetch=0;TextAsLongVarchar=1;UnknownsAsLongVarchar=0;BoolsAsChar=1;Parse=0;CancelAsFreeStmt=0;ExtraSysTablePrefixes=dd_;;LFConversion=1;UpdatableCursors=1;DisallowPremature=0;TrueIsMinus1=0;BI=0;ByteaAsLongVarBinary=0;UseServerSidePrepare=0;LowerCaseIdentifier=0;XaOpt=1");
}

CString C_END_OF_BREATH_DATA_set::GetDefaultSQL()
{
	return _T("[PatientData].[END_OF_BREATH_DATA]");
}

void C_END_OF_BREATH_DATA_set::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// Macros such as RFX_Text() and RFX_Int() are dependent on the
// type of the member variable, not the type of the field in the database.
// ODBC will try to automatically convert the column value to the requested type
	RFX_BigInt(pFX, _T("[event_sequence_id]"), m_event_sequence_id);
	RFX_Text(pFX, _T("[breathType]"), m_breathType);
	RFX_Single(pFX, _T("[exhaledTidalVolume]"), m_exhaledTidalVolume);
	RFX_Single(pFX, _T("[breathDuration]"), m_breathDuration);
	RFX_Single(pFX, _T("[endExpiratoryPressure]"), m_endExpiratoryPressure);
	RFX_Single(pFX, _T("[ieRatio]"), m_ieRatio);
	RFX_Single(pFX, _T("[mandFraction]"), m_mandFraction);
	RFX_Single(pFX, _T("[dynamicCompliance]"), m_dynamicCompliance);
	RFX_Single(pFX, _T("[dynamicResistance]"), m_dynamicResistance);
	RFX_Text(pFX, _T("[phaseType]"), m_phaseType);
	RFX_Text(pFX, _T("[previousBreathType]"), m_previousBreathType);
	RFX_Text(pFX, _T("[breathTypeDisplay]"), m_breathTypeDisplay);
	RFX_Single(pFX, _T("[spontInspTime]"), m_spontInspTime);
	RFX_Single(pFX, _T("[spontTiTtotRatio]"), m_spontTiTtotRatio);
	RFX_Single(pFX, _T("[peakExpiratoryFlow]"), m_peakExpiratoryFlow);
	RFX_Single(pFX, _T("[endExpiratoryFlow]"), m_endExpiratoryFlow);

}
/////////////////////////////////////////////////////////////////////////////
// C_END_OF_BREATH_DATA_set diagnostics

#ifdef _DEBUG
void C_END_OF_BREATH_DATA_set::AssertValid() const
{
	CRecordset::AssertValid();
}

void C_END_OF_BREATH_DATA_set::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


