#ifndef ExpiratoryPauseHandler_HH
#define ExpiratoryPauseHandler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ExpiratoryPauseHandler - Handles control of Expiratory Pause
// (initiated by the Expiratory Pause off-screen key).
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ExpiratoryPauseHandler.hhv   25.0.4.0   19 Nov 2013 14:07:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: hhd	Date: 27-Apr-2000   DCS Number:  5657
//  Project:  Baseline
//  Description:
//	Modified to display Settings Locked Out prompt when the condition arises.
//
//  Revision: 004  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 003  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  yyy    Date:  31-Jan-1998    DR Number: None
//  Project:  Sigma (R8027)
//  Description:
//             Bilevel initial version.
//			   Implemented Exp Pause.
//
//  Revision: 001  By:  hhd    Date:  16-MAY-95    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//             Integration baseline.
//====================================================================

#include "KeyPanelTarget.hh"

//@ Usage-Classes
#include "GuiAppClassIds.hh"
#include "BdEventTarget.hh"
#include "GuiEventTarget.hh"

//@ End-Usage

class ExpiratoryPauseHandler :	public KeyPanelTarget, 
				public BdEventTarget, public GuiEventTarget
{
public:
	ExpiratoryPauseHandler(void);
	~ExpiratoryPauseHandler(void);

	// Key Panel Target virtual methods
	virtual void keyPanelPressHappened(KeyPanel::KeyId key);
	virtual void keyPanelReleaseHappened(KeyPanel::KeyId key);

	virtual void bdEventHappened(EventData::EventId evId,
				                EventData::EventStatus evStatus,
								EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);
	// GuiEventTarget virtual method
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

	static void Cancel(void);
	
	static void SoftFault(const SoftFaultID softFaultID,
                          const Uint32      lineNumber,
                          const char*       pFileName  = NULL,
                          const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	ExpiratoryPauseHandler(const ExpiratoryPauseHandler&);	// not implemented...
	void   operator=(const ExpiratoryPauseHandler&);	// not implemented...

	//@ Data-Member: pauseCanceled_
	// Flag indicates if pause is requested by user.
	static Boolean pauseCanceled_;
};


#endif // ExpiratoryPauseHandler_HH
