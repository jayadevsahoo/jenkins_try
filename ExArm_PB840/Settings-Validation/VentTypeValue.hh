
#ifndef VentTypeValue_HH
#define VentTypeValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2004, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  VentTypeValue - Enumerated values for Vent Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/VentTypeValue.hhv   25.0.4.0   19 Nov 2013 14:27:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Initial version - Setting values for new NIV vent-type setting.
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct VentTypeValue
{
  //@ Type:  VentTypeValueId 
  enum VentTypeValueId
  {
    // $[NI02007] -- setting values...
    INVASIVE_VENT_TYPE,
    NIV_VENT_TYPE,

    TOTAL_VENT_TYPE_VALUES
  };
};


#endif // VentTypeValue_HH 
