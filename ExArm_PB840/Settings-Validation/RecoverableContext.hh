
#ifndef RecoverableContext_HH
#define RecoverableContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: RecoverableContext - Context for the Recoverable Batch Settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/RecoverableContext.hhv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

//@ Usage-Classes
#include "BatchSettingValues.hh"
//@ End-Usage


class RecoverableContext
{
  public:
    RecoverableContext(void);
    ~RecoverableContext(void);

    inline Boolean  isValid   (void) const;
    inline void     invalidate(void);

#if defined(SIGMA_DEVELOPMENT)
    inline const BoundedValue&  getBoundedValue (
				    const SettingId::SettingIdType boundedId
						)  const;
    inline DiscreteValue        getDiscreteValue(
				    const SettingId::SettingIdType discreteId
						)  const;
#endif  // defined(SIGMA_DEVELOPMENT)

    inline const BatchSettingValues&  getStoredValues(void) const;

    void  storeAcceptedSettings(void);

#if defined(SIGMA_DEVELOPMENT)
    Boolean  areAllValuesValid(void) const;
#endif  // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    RecoverableContext(const RecoverableContext&);  // not implemented...
    void  operator=(const RecoverableContext&);	    // not implemented...

    //@ Data-Member: validityFlag_
    // Is this context valid?
    Boolean  validityFlag_;

    //@ Data-Member: batchValues_
    // The recoverable batch setting values.
    BatchSettingValues  batchValues_;
};


// Inlined methods
#include "RecoverableContext.in"


#endif // RecoverableContext_HH 
