#include "stdafx.h"
//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsConfig.cc
/// @brief Implementation of GainsConfig class
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "GainsConfig.hh"
#include "SigmaTypes.hh"

using namespace GainsManagerNS;

//--------------------------------------------------------------------
/// Constructor
//--------------------------------------------------------------------
GainsConfig::GainsConfig(void)
: m_name(GAINS_NAMES_NO_NAME),
  m_gain(0.0),
  m_originalGain(0.0),
  m_changed( FALSE )
{
}

//--------------------------------------------------------------------
/// Constructor
//--------------------------------------------------------------------
GainsConfig::GainsConfig( GainsNames name )
: m_name( name ),
  m_gain(0.0),
  m_originalGain(0.0),
  m_changed( FALSE )
{
}

//--------------------------------------------------------------------
/// Destructor
//--------------------------------------------------------------------
GainsConfig::~GainsConfig(void)
{

}

//--------------------------------------------------------------------
// operator=
//--------------------------------------------------------------------
GainsConfig& GainsConfig::operator=(const GainsConfig &rhs)
{
    if( this != &rhs )
    {
        m_name         = rhs.m_name;
        m_gain         = rhs.m_gain;
        m_originalGain = rhs.m_originalGain;
        m_changed      = rhs.m_changed;
    }

	return (*this);
}

//--------------------------------------------------------------------
// initialize
//--------------------------------------------------------------------
void GainsConfig::initialize(void)
{
    load();
    m_originalGain = m_gain;

    // The gain hasn't change from the perspective of the
    // controller so there isn't a need to distribute it
    m_changed = FALSE;
}

//--------------------------------------------------------------------
// restore
//--------------------------------------------------------------------
void GainsConfig::restore(void)
{
    m_changed = TRUE;
    m_gain = m_originalGain;
}

//--------------------------------------------------------------------
// isChanged
//--------------------------------------------------------------------
Boolean GainsConfig::isChanged(void) const
{
    return (m_changed);
}

//--------------------------------------------------------------------
// setChanged
//-------------------------------------------------------------------
void GainsConfig::setChanged( Boolean b )
{
    m_changed = b;
}

//--------------------------------------------------------------------
// setName
//-------------------------------------------------------------------
void GainsConfig::setGainName( GainsManagerNS::GainsNames name )
{
    m_name = name;
}

//--------------------------------------------------------------------
// getName
//-------------------------------------------------------------------
GainsManagerNS::GainsNames GainsConfig::getGainName(void) const
{
    return (m_name);
}

//--------------------------------------------------------------------
/// getGain
//--------------------------------------------------------------------
Real32 GainsConfig::getGain(void) const
{
    return (m_gain);
}

//--------------------------------------------------------------------
/// getGain
//--------------------------------------------------------------------
Real32 GainsConfig::getOriginalGain(void) const
{
    return (m_originalGain);
}

//--------------------------------------------------------------------
/// setGain
//--------------------------------------------------------------------
void GainsConfig::setGain( Real32 r )
{
    m_changed = TRUE;
    m_gain = r;
}
