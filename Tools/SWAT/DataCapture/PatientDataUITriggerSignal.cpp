//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUITriggerSignal.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"PatientDataUITriggerSignal.h"

#include<math.h>
#ifndef M_PI
#define M_PI 3.141592653589793238462643 
#endif

#ifndef M_2_PI
#define M_2_PI		0.63661977236758134308
#endif

//-------------------------------------
// C O D E
//-------------------------------------



bool TriggerSignal::isTrigger()
{
    return true;
}

bool TriggerSignal::setTriggerSignal(
        double dFrequency,
        double dPhase,
        double dMin,
        double dMax, 
        double trigger_high,
        double trigger_low)
{
    m_dMax     = dMax;
    m_dMin     = dMin;
    m_dFreqency= dFrequency;
    m_dPhase   = (dPhase*M_PI) / 180.0;
    m_dRange   = (dMax-dMin)   /   2.0;
    m_Trigger_high =  trigger_high; 
    m_Trigger_low  =  trigger_low ;


    m_bIsDesired  = true;
    UserInt::unsetValue();
    return true;
}



bool TriggerSignal::unsetValue()
{
    m_bIsDesired=false;
    UserInt::unsetValue();
    return true;
}

double TriggerSignal::getFrequency()
{
    return m_dFreqency;
}

double TriggerSignal::getPhase()
{
    return m_dPhase;
}

double TriggerSignal::getMax()
{
    return m_dMax;
}

double TriggerSignal::getMin()
{
    return m_dMin;
}

double TriggerSignal::getTriggerHigh()
{
    return m_Trigger_high;
}

double TriggerSignal::getTriggerLow ()
{
    return m_Trigger_low;
}


TriggerSignal::TriggerSignal(
        char *pName,
        unsigned int &uiTime, 
        double dFrequency,
        double dPhase,
        double dMin,
        double dMax,
        double trigger_low,
        double trigger_high
        ):
    UserInt(pName,0),
    m_uiTime(uiTime),
    m_dFreqency(dFrequency),
    m_dPhase((dPhase*M_PI)/180.0),
    m_dMin(dMax),
    m_dMax(dMax),
    m_Trigger_low(trigger_low), m_Trigger_high(trigger_high),
    m_bIsDesired(false),m_fLast(0)
{
    m_dRange=(dMax-dMin)/2.0;
}

void TriggerSignal::updatePacket(void *address)
{
    double dVal;

    if(m_bIsDesired == true)
    {
        m_dTime=(double)m_uiTime;

        dVal = fmod(
            ((M_PI*m_dFreqency*m_dTime)/50.0+m_dPhase/2.0),M_PI)
            *180.0
            *M_2_PI;

        if(dVal      >= m_Trigger_high)
        {
            m_iValue = 1;
        }
        else if(dVal >= 0.0 )
        {
            m_iValue = 0;
        }

        ((unsigned int *)address)[0]=htonl(*((unsigned int *)(&m_iValue)));

        m_fLast = dVal;

    }
    else if(UserInt::m_bIsDesired==true)
    {
        UserInt::updatePacket(address);
    }
}




#if 0
void TriggerSignal::updatePacket(void *address)
{
    double rv;
    int iVal;

    if(m_bIsDesired == true)
    {
        m_dTime=(double)m_uiTime;

        rv = m_dRange * 
            sin( ((2.0*M_PI*m_dFreqency*m_dTime)/(50.0) ) + m_dPhase )
            + m_dRange + m_dMin;
    
    if(m_fLast>rv){
        if(rv<=m_Trigger_low ){
            m_iValue = 0;
        }
    }
    else if(m_fLast<rv){
        if(rv>=m_Trigger_high){
            m_iValue = 1;
        }
    }

    m_fLast = rv;


        ((unsigned int *)address)[0]=htonl(*((unsigned int *)(&m_iValue)));
    }
    else if(UserInt::m_bIsDesired==true){
        UserInt::updatePacket(address);
    }
}
#endif
