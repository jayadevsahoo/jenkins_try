#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  BatchSequentialSetting - Batch Sequential Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the setting's framework for the Batch, Sequential
//  Settings.  All batch, sequential settings are derived from this setting.
//  This class overrides some of the virtual methods of 'SequentialSetting'
//  including those to:  get this batch sequential setting's new-patient
//  value, get this batch sequential setting's "accepted" and "adjusted"
//  value, activating the Transition Rules associated with this setting
//  (Main Control Settings only), responding to transition of the Main
//  Control Settings, calculate (update) this settings dependent settings,
//  and store a new value for this setting into the Adjusted Context.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to establish the batch sequential
//  setting's framework for the rest of the settings to build upon.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines no instances -- it just overrides some behavior.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BatchSequentialSetting.ccv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 21-Jan-2009    SCR Number: 6458
//  Project:  840S
//  Description:
//      Modified to use isNewlyApplicable() method to determine if
//		setting's applicability has changed from non-applicable to
//		an applicable state. Resolves problem of setting getting
//		into a changed state and not being able to get out of it when
//		even when the applicability reverts to non-applicable.
//
//  Revision: 007   By: gdc   Date:  08-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//		Modified to support changeable IBW.
//	
//  Revision: 006   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  using SafetyPcvSettingValue's new, generic interface
//	*  as part of adding soft-bound to Tube I.D., the violation
//	   id is now more managed
//
//  Revision: 005   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 004   By: dosman Date:  24-Feb-1998    DCS Number: 
//  Project:  BILEVEL
//  Description:
//      Inital BiLevel version.
//      Ensure that absolute max/min values are updated during 
//      update to new patient mechanism.
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.12.1.0) into Rev "BiLevel" (1.12.2.0)
//	Added the passing of new constructor parameter from caller
//	(derived class), to base class.  This new parameter is used for
//	allowing sequential settings to disable their bound.
//
//  Revision: 002   By: sah    Date:  07-May-1997    DCS Number: 2034
//  Project: Sigma (R8027)
//  Description:
//	As a result of this DCS, I'm making sure the same problem can't
//	happen with sequential settings, by supplying one new method
//	for resetting the minimum and maximum constraints for each batch,
//	sequential setting (see 'resetBoundConstraints()').
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "BatchSequentialSetting.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "AdjustedContext.hh"
#include "SafetyPcvSettingValues.hh"
#include "SequentialRange.hh"
//@ End-Usage

//@ Code...

//===================================================================
//
//  Public Method...
//
//===================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~BatchSequentialSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchSequentialSetting::~BatchSequentialSetting(void)
{
	CALL_TRACE("~BatchSequentialSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isChanged()  [const, virtual]
//
//@ Interface-Description
//  Has this setting been changed? When a batch setting's "adjusted"
//  value has been changed -- either auto-set or by the operator -- to a
//  value that is different than the setting's "accepted" value, or if 
//  the setting is now applicable in the adjusted context and it isn't
//  in the accepted context, this method will return TRUE, otherwise it
//  returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01038] -- is changed whenever the value is different than accepted...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean	BatchSequentialSetting::isChanged(void) const
{
	CALL_TRACE("isChanged()");

	Boolean  isChanged;

	if ( !getForcedChangeFlag_() )
	{	// $[TI1]
		// the override flag is NOT set, therefore the "change" status is based
		// on whether the accepted and adjusted values are different...
		const SequentialValue  ACCEPTED_VALUE = getAcceptedValue();
		const SequentialValue  ADJUSTED_VALUE = getAdjustedValue();

		isChanged = (ACCEPTED_VALUE != ADJUSTED_VALUE) || isNewlyApplicable();
	}
	else
	{	// $[TI2] -- the override flag IS set, therefore return 'TRUE'.
		isChanged = TRUE;
	}

	return(isChanged);
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is NOT to be overridden by any of the
//  derived classes.  This is only used to initialize the Accepted Context
//  to some set of valid values, BEFORE a New-Patient Setup has been run.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This just returns the setting's Safety-PCV value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	BatchSequentialSetting::getDefaultValue(void) const
{
	CALL_TRACE("getDefaultValue()");

	return(SafetyPcvSettingValues::GetValue(getId()));
}	// $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getNewPatientValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be defined by ALL batch,
//  sequential settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  acceptTransition(settingId, newValue, currValue)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all derived
//  classes that respond to Main Control Setting transitions.  This version
//  of this virtual method is NEVER to be called -- it asserts this fact.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BatchSequentialSetting::acceptTransition(const SettingId::SettingIdType,
											 const DiscreteValue,
											 const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");
	AUX_CLASS_ASSERTION_FAILURE(getId());

	// NEVER gets here...
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BatchSequentialSetting::SoftFault(const SoftFaultID  softFaultID,
									  const Uint32       lineNumber,
									  const char*        pFileName,
									  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							BATCH_SEQUENTIAL_SETTING, lineNumber,
							pFileName, pPredicate);
}


//======================================================================
//
//  Protected Methods...
//
//======================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method:  BatchSequentialSetting(settingId, arrDependentSettingIds, rSequentialRange, useWrappingMode)
//
//@ Interface-Description
//  Construct the sequential setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchSequentialId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchSequentialSetting::BatchSequentialSetting(
											  const SettingId::SettingIdType  settingId,
											  const SettingId::SettingIdType* arrDependentSettingIds,
											  SequentialRange&                rSequentialRange,
											  const Boolean                  useWrappingMode)
: SequentialSetting(settingId, arrDependentSettingIds,
					rSequentialRange, useWrappingMode)
{
	CALL_TRACE("BatchSequentialSetting(settingId, arrDependentSettingIds, rSequentialRange, useWrappingMode)");
	AUX_CLASS_PRE_CONDITION((SettingId::IsBatchSequentialId(settingId)),
							settingId);
}	// $[TI1]


//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method:  resolveDependentChange_(dependentId, isValueIncreasing)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	BatchSequentialSetting::resolveDependentChange_(
												   const SettingId::SettingIdType dependentId,
												   const Boolean                  isValueIncreasing
												   )
{
	CALL_TRACE("resolveDependentChange_(dependentId, isValueIncreasing)");

	const BoundStatus*  pBoundStatus;

	Setting*  pDependentSetting = SettingsMgr::GetSettingPtr(dependentId);

	// initially notify the dependent setting of the primary setting's change...
	pBoundStatus = pDependentSetting->acceptPrimaryChange(getId());

	// used to determine, and return, whether a change to this primary
	// setting was made; anytime a dependent setting's change is restricted,
	// the primary setting requires change...
	Boolean  didBoundViolationOccur = (pBoundStatus != NULL);

	if ( didBoundViolationOccur )
	{	// $[TI1] -- a dependent bound violation occurred...
		// get a reference to this primary setting's bound status...
		BoundStatus&  rBoundStatus = getBoundStatus_();

		// set this primary setting's bound status with the dependent bound
		// violation information...
		rBoundStatus.setBoundStatus(pBoundStatus->getBoundState(),
									pBoundStatus->getBoundId());
		rBoundStatus.setViolationId(pBoundStatus->getViolatedSettingId());

		SequentialValue  newPrimaryValue;

		// re-calculate the primary setting's value, based on the dependent
		// setting's "clipped" value...
		newPrimaryValue = calcBasedOnSetting_(dependentId,
											  BASED_ON_ADJUSTED_VALUES);

		// store the new primary setting value...
		setAdjustedValue(newPrimaryValue);

		//===================================================================
		//  re-update the dependent setting...
		//===================================================================

		// re-notify the dependent setting of the primary setting's
		// re-calculation...
		pBoundStatus = pDependentSetting->acceptPrimaryChange(getId());

		// this time NO bounds should be violated...
		SAFE_CLASS_ASSERTION((pBoundStatus == NULL));
	}	// $[TI2] -- no dependent bound was violated...

	return(didBoundViolationOccur);
}


//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method:  calcBasedOnSetting_(basedOnSettingId, basedOnCategory)  [virtual, const]
//
//@ Interface-Description
//  A virtual method that is to be overridden by all settings that have
//  dependent settings.  This method is called by 'resolveDependentChange_()'
//  to recalculate this setting's value, based on the value of
//  'basedOnSettingId'.  The 'basedOnCategory' parameter tells this method
//  where to get the values of the other settings needed for the calculation.
//
//  This class's version of this method is NEVER to be called -- it asserts
//  this restriction.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SequentialValue
	BatchSequentialSetting::calcBasedOnSetting_(
											   const SettingId::SettingIdType basedOnSettingId,
											   const BasedOnCategory          basedOnCategory
											   ) const
{
	CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, basedOnCategory)");

	AUX_CLASS_ASSERTION_FAILURE((Uint32((getId() << 16) | basedOnSettingId)));

	//-------------------------------------------------------------------
	// the remaining block of code is NEVER run...
	//-------------------------------------------------------------------

	SequentialValue  dummyValue;

	dummyValue = 0;

	return(dummyValue);
}


#if defined(SIGMA_DEVELOPMENT)

	#ifndef Ostream_HH
		#include "Ostream.hh"
	#endif // Ostream_HH

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  print_(ostr)  [const]
//
// Interface-Description
//  Print the contents of this batch sequential setting into 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Ostream&
	BatchSequentialSetting::print_(Ostream& ostr) const
{
	CALL_TRACE("print_(ostr)");

	SequentialSetting::print_(ostr);

	ostr << "  Adjusted Value:  " << (SequentialValue)getAdjustedValue() << endl;
	ostr << "  isChanged():     " << ((isChanged()) ? "TRUE" : "FALSE")
	<< endl;

	return(ostr);
}

#endif  // defined(SIGMA_DEVELOPMENT)
