/*****************************************************************************/
/*   Module Name:   Vtest.c                                                  */
/*                                                                           */
/*   Purpose:       This file contains routines to sequence and run          */
/*                  test functions required for 840VTS.                      */
/*                                                                           */
/*   Requirements:  This module implements requirements detailed in          */
/*                  <75359-93>                                               */
/*                                                                           */
/*   Modification History:                                                   */
/*                                                                           */
/*   Description of Change                  By                Date           */
/*   ---------------------                  -----             ------         */
/*   Removed the Metabolic Port Test.       Rhomere Jimenez   05 Oct,2007    */ 
/*                                                                           */                                                 
/*   The Error codes in ErrorMessage        Dwarkanath(CGS)   12 Dec,2000    */
/*   functions are changed.                                                  */
/*                                                                           */
/*   Updation of listbox for the Abort      Hamsavally(CGS)   29 Nov,2000    */
/*   functionality is added in the                                           */
/*   Looptest function.                                                      */
/*                                                                           */
/*   StartSeq in CurrentTest must be        Hamsavally(CGS)   29 Nov,2000    */
/*                                                                           */
/*   Revised routines that consumed         Dave Richardson   20 Nov, 2002   */
/*   and did not return memory                                               */
/*                                                                           */
/*   Updated comment blocks                                                  */
/*   for file and function descriptions                                      */
/*                                                                           */
/*   General cleanup of layout style                                         */
/*                                                                           */
/*   $Revision:   1.2  $                                                              */
/*                                                                           */
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/tests/Vtest.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:32   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   20 Nov 2002 13:44:48   Richardson
 * Updated per SCRs # 31, 32, 33

                                                                             */
/*****************************************************************************/

/*****************************************************************************  
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS  
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,      
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY 
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE  
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.                
                                                                                
Copyright (c) 2001-2002, Puritan-Bennett                        
All rights reserved.                                                            
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <formatio.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>

#include "globals.h"
#include "72361.h"
#include "840cmds.h" 
#include "cpvtmain.h"
#include "dbapi.h"
#include "ldb.h"
#include "MsgPopup.h"
#include "pb_util.h"
#include "sysmenu.h"
#include "testing.h"
#include "testpnl.h"
#include "testuir.h"  

/***********************************************/
/****************** EXTERNAL *******************/
/***********************************************/

char *TestName[NUM_OF_LANG][NUM_OF_TESTS] = {
		{
			{"Manual Tests"},
			{"Atmospheric Pressure Test"},
			{"Breath Delivery Alarm Test"},
			{"GUI Alarm Test"},
			{"Circuit Pressure Test"},
			{"Air Flow Test"},
			{"O2 Flow Test"},
			{"Firmware Revision Test"},
			{"Volume Performance Test"},
			{"Pressure Performance Test"},
			{"PEEP Performance Test"},
			{"O2 Performance Test"}
		},
		{
			{"Pruebas manuales"},
			{"Prueba de presi�n atmosf�rica"},
			{"Prueba de la alarma BDU"},
			{"Prueba de alarma IGU"},
			{"Prueba de presi�n del circuito"},
			{"Prueba de flujo de aire"},
			{"Prueba de flujo de O2"},
			{"Prueba de versi�n de firmware"},
			{"Prueba de funcionamiento del volumen"},
			{"Prueba de funcionamiento de la presi�n"},
			{"Prueba de funcionamiento PEEP "},
			{"Prueba de funcionamiento de O2"}
		},
		{
			{"Tests manuels"},
			{"Test Pression atmosph�rique"},
			{"Test Alarme insufflation d'air"},
			{"Test Alarme IGU"},
			{"Test Pression circuit"},
			{"Test D�bit d'air"},
			{"Test D�bit O2"},
			{"Test R�vision microprogramme"},
			{"Test Performances volume"},
			{"Test Performances pression"},
			{"Test Performances PEP"},
			{"Test Performances O2"} 
		},
		{
			{"Manuelle-Tests"},
			{"Luftdruck-Test"},
			{"Beatmungsalarm-Test"},
			{"GUI-Alarm-Test"},
			{"Schlauchsystemdruck-Test"},
			{"Air-Flow-Test"},
			{"O2-Flow-Test"},
			{"Firmware-Revisionstest"},
			{"Volumen-Funktionstest"},
			{"Druck-Funktionstest"},
			{"PEEP-Funktionstest"},
			{"O2-Funktionstest"}
		},
		{
			{"Test manuali"},
			{"Test pressione atmosferica"},
			{"Test allarme BDV"},
			{"Test allarme GUI"},
			{"Test pressione circuito"},
			{"Test flusso aria"},
			{"Test flusso O2"},
			{"Test revisione firmware"},
			{"Test volume"},
			{"Test pressione"},
			{"Test prestazioni PEEP"},
			{"Test prestazioni O2"}
		},
		{
			{"Testes manuais"},
			{"Teste de press�o atmosf�rica"},
			{"Teste de alarme de BD"},
			{"Teste de alarme de GUI"},
			{"Teste de press�o de circuito"},
			{"Teste de fluxo de ar"},
			{"Teste de fluxo de O2"},
			{"Teste de revis�o de firmware"},
			{"Teste de desempenho de volume"},
			{"Teste de desempenho de press�o"},
			{"Teste de desempenho de PEEP"},
			{"Teste de desempenho de O2"}
		}
	};

int gErrorFlag = FALSE;
tTestData tDataRec[NUM_OF_TESTS];
int iCurrentTest;

/*****************************************************/
/****************** TEST FUNCTIONS *******************/
/*****************************************************/

/*****************************************************************************/
/* Name:  RunSingleTest                                                      */
/*                                                                           */
/* Purpose:  Runs the selected test.                                         */
/*                                                                           */
/*                                                                           */
/* Example Call: RunSingleTest(void);                                        */
/*                                                                           */
/* Input: void.                                                              */
/*                                                                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return: void                                                              */
/*                                                                           */
/*****************************************************************************/
void RunSingleTest(void)
{
	int iIndex, i;
	int iTestResult;
//	char *cTestName = "\0";
	char *cTestName = NULL;
	char *cTemp_ptr = NULL;
	int iResult;

	// To stop executing the test when ventilator is not connected.
	iResult = StartSeq();
	if(!iResult){
		return;
	}

	SetSystemStatus(BLANK);
	GetCtrlIndex(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, &iIndex);

	if( (tDataRec[iIndex].sRunMode == RUN_MODE_SKIP ) || (sAbortFlag ==TRUE ) ){
		sAbortFlag = FALSE;
		return;
	}

	// Initialization of the data structure for the particular test.
	tDataRec[iIndex].result    = PASS;
	tDataRec[iIndex].outBuffer = NULL;
	tDataRec[iIndex].outBuff_localized = NULL;
	gErrorFlag = FALSE;

	if( (iIndex < MIN_TESTS) || (iIndex > MAX_TESTS) ){
		ErrorMessage(NO_TEST_ERROR);
		return;
	}

	UpdateTestListing(iIndex, TEST_RUNNING);
	iCurrentTest = iIndex;

	// Add the test name to the results box
//	cTestName = (char*)malloc(strlen(TestName[CurrLanguage][iIndex])+1);
//	sprintf(cTestName, "%s", TestName[CurrLanguage][iIndex]);
	cTestName = TestName[CurrLanguage][iIndex];

	// The corresponding test will be executed depending on the index value.

	CurrentTest(iIndex);

	// Formatting of outbuffer will be done and posted to the result window.
	//                                                          NO_DB  LOCAL             NO_JET
	cTemp_ptr = Tfmtdisplay(tDataRec[iIndex].outBuff_localized, FALSE, TRUE, NULL, NULL, FALSE, iIndex);
	if(cTemp_ptr == NULL && tDataRec[iIndex].result == ABORT){
		PostInfo2Window(iIndex, LDBf(Ldb_MSG_ABORT), cTestName/*(char*)(TestName[CurrLanguage][iIndex])*/);
	}
	if(cTemp_ptr != NULL){
		PostInfo2Window(iIndex, cTemp_ptr, cTestName/*(char*)TestName[CurrLanguage][iIndex]*/);
		free(cTemp_ptr);
		cTemp_ptr = NULL;
	}
	gStoppedInd = gPausedInd = 0;

	// The Listox will be updated in the testpanel.
	if(gErrorFlag){
		UpdateTestListing(iIndex, TEST_ERROR);
	}
	else if(tDataRec[iIndex].result == PASS){
		UpdateTestListing(iIndex, TEST_PASS);
	}
	else if(tDataRec[iIndex].result == SKIP){
		UpdateTestListing(iIndex, TEST_SKIP);
	}
	else if(tDataRec[iIndex].result == FAIL){
		UpdateTestListing(iIndex, TEST_FAIL);
	}
	else if(tDataRec[iIndex].result == ABORT){
		UpdateTestListing(iIndex, TEST_ABORT);
	}
	WriteTestResult(iIndex);
	FreeTestBuffers(tDataRec);

	return;

Error:
	FreeTestBuffers(tDataRec);

	return;
}

/*****************************************************************************/
/* Name:      FreeTestBuffers                                                */
/*                                                                           */
/* Purpose:  Free buffers allocated during execution of a test.              */
/*                                                                           */
/* Example Call:   void FreeTestBuffers( tTestData *tDataRec  );             */
/*                                                                           */
/* Input:    tDataRec - a test data record to clear                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   None                                                            */
/*                                                                           */
/*****************************************************************************/
void FreeTestBuffers(tTestData *tDataRec){

	if(tDataRec[iCurrentTest].outBuffer != NULL){
		free(tDataRec[iCurrentTest].outBuffer);
		tDataRec[iCurrentTest].outBuffer = NULL;
	}

	if(tDataRec[iCurrentTest].outBuff_localized != NULL){
		free(tDataRec[iCurrentTest].outBuff_localized);
		tDataRec[iCurrentTest].outBuff_localized = NULL;
	}

	return;
}

/*****************************************************************************/
/* Name:     LoopTest                                                        */
/*                                                                           */
/* Purpose:  Loop on single test                                             */
/*                                                                           */
/* Example Call:   void LoopTest(void);                                      */
/*                                                                           */
/* Input:   <None>                                                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:    none                                                           */
/*                                                                           */
/*****************************************************************************/
void LoopTest(void)
{
	int iIndex, iMaxNum, iMaxPosNum, iUntilFail, iCount, i;
	int iTestResult;
	int iFailed = FALSE;
	int iPasscnt = 0; 
	int iFailcnt = 0;
	char *cTemp_ptr = {0};
//	char *cTestName = "\0";
	char *cTestName = NULL;
	short sStatusDispVal;
	int iResult;

	// To stop executing the test when ventilator is not connected.
	iResult = StartSeq();
	if(!iResult){
		return;
	}

	SetTestButtonMode(TB_RUNNING);

	// Gets the number of times the loop must be executed and the total number of times a loop can run.
	GetCtrlVal(gPanelHandle[LOOPPANEL], LOOPPANEL_MAXNUMBER, &iMaxNum);
	GetCtrlAttribute(gPanelHandle[LOOPPANEL], LOOPPANEL_MAXNUMBER, ATTR_MAX_VALUE, &iMaxPosNum);

	GetCtrlIndex(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, &iIndex);

	// checking the runmode to execute the test
	if( (tDataRec[iIndex].sRunMode == RUN_MODE_SKIP) || (sAbortFlag == TRUE) ){
		sAbortFlag = FALSE;
		return;
	}

	if(iMaxNum > iMaxPosNum){
		iMaxNum = iMaxPosNum;
	}

	GetCtrlVal(gPanelHandle[LOOPPANEL], LOOPPANEL_UNTILFAIL, &iUntilFail);
	iTestResult = iUntilFail;

	RemovePopup(0);

	tDataRec[iIndex].result    = PASS;
	tDataRec[iIndex].outBuffer = NULL;
	tDataRec[iIndex].outBuff_localized = NULL;

	sAbortFlag = FALSE;
	gErrorFlag = FALSE;
	sStatusDispVal = STAT_LOOPING;
	gStoppedInd = 0;
	iCount = 0;

	// The selected test will be executed iMaxNum times 
	while( (iCount < iMaxNum) && (!iUntilFail || !iFailed) ){
		if(gStoppedInd){
			break;
		}

		if(gPausedInd){
			SetSysPanelMessage(LDBf(Ldb_PauseMsg1));
			SetTestButtonMode(TB_PAUSED);
			while(gPausedInd){
				ProcessSystemEvents();
			}
		}
		iCurrentTest = iIndex;
		if( (iIndex < MIN_TESTS) || (iIndex > MAX_TESTS) ){
			ErrorMessage(NO_TEST_ERROR);
			return;
		}

		tDataRec[iIndex].loopCount = iCount;
		UpdateTestListing(iIndex, TEST_LOOPING);
		ProcessSystemEvents();

		/* Run the test! */
		CurrentTest(iIndex);

//		cTestName = (char *)malloc(strlen(TestName[CurrLanguage][iIndex]) +1);
//		strcpy(cTestName ,TestName[CurrLanguage][iIndex]);
		cTestName = TestName[CurrLanguage][iIndex];

		//                                                          NO_DB  LOCAL             NO_JET
		cTemp_ptr = Tfmtdisplay(tDataRec[iIndex].outBuff_localized, FALSE, TRUE, NULL, NULL, FALSE, iIndex);
		if(cTemp_ptr != NULL){
			PostInfo2Window(iIndex, cTemp_ptr, cTestName/*(char *)TestName[CurrLanguage][iIndex]*/);
			free(cTemp_ptr);
			cTemp_ptr = NULL;
		}

		/* Clear the gDatabase buffer*/
		if(gDatabaseBuffer != NULL){
			free(gDatabaseBuffer);
			gDatabaseBuffer = NULL;
		}

		if(gErrorFlag){
			UpdateTestListing(iIndex, TEST_ERROR);
			iFailed = TRUE;
			iFailcnt++;
		}
		else{
			switch(tDataRec[iIndex].result){
				case PASS:
					UpdateTestListing(iIndex, TEST_PASS);
					iPasscnt++;
					break;

				case SKIP:
					iFailed = TRUE;
					UpdateTestListing(iIndex, TEST_SKIP);
					iFailcnt++;
					break;

				case FAIL:
					iFailed = TRUE;
					UpdateTestListing(iIndex, TEST_FAIL);
					iFailcnt++;
					break;

				case ABORT:
					sAbortFlag = 1;
					UpdateTestListing(iIndex, TEST_ABORT);
					break;

				default:
					break;
			} // end switch(tDataRec[iIndex].result)
		} // end if(gErrorFlag) else
		ProcessSystemEvents();
		iCount++;
		WriteTestResult(iIndex);
		FreeTestBuffers(tDataRec);

		if(sAbortFlag == TRUE){
			iCount = iMaxNum;
			SetCtrlVal(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, LDBf(Ldb_MSG_LOOP_ABORT));
		}
	} // end while((iCount < iMaxNum) && (!iUntilFail || !iFailed))

	if(gErrorFlag){
		goto Error;
	}

	sStatusDispVal = STAT_STOPPED;
	WriteStopReason(sAbortFlag, (iUntilFail && iFailed));

	GetCtrlIndex(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, &iIndex);
	SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_LOOPTEST_DEV, ATTR_CTRL_VAL, 0); /* turn off the test button */ 

	SetSystemStatus(BLANK);
	SetTestButtonMode(TB_NORMAL);

	return;

Error:
	sStatusDispVal = STAT_STOPPED;
	GetCtrlIndex(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, &iIndex);
	SetTestButtonMode(TB_NORMAL);

	return;
}

/*****************************************************************************/
/* Name:     RunTestUUT                                                      */
/*                                                                           */
/* Purpose:  Run all test                                                    */
/*                                                                           */
/* Example Call:void RunTestUUT(void);                                       */
/*                                                                           */
/* Input:   <None>                                                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/* Return:    none                                                           */
/*                                                                           */
/*****************************************************************************/
void RunTestUUT(void)
{
	int i;
	int iFailStop = 0;
	int iIndex, iSaveIndex;
	int iTestResult;
	char cSerialNum[20];
	char cUutRevNum[20];
	int iStatus;
	short sStatusDispVal;
	int iResult;

	gOverallpass = PASS;
	sAbortFlag   = FALSE;
	gErrorFlag   = FALSE;
	SST          = 0 ;

	// the testpanel will be updated.
	ResetSeqDisplay();
	
	// To stop executing the test when ventilator is not connected.
	iResult = StartSeq();
	if(!iResult){
		return;
	}

	// All other Test Buttons will be deselected
	SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTUUT_DEV,  ATTR_DIMMED, DISABLE);
	SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_RUNTEST_DEV,  ATTR_DIMMED, DISABLE);
	SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_LOOPTEST_DEV, ATTR_DIMMED, DISABLE);

	sStatusDispVal = STAT_RUNNING;
	
	while(TRUE){
		SetSystemStatus(BLANK);
		gOverallpass = PASS;
		iFailStop = 0;
		InitTestReport();

		sStatusDispVal = STAT_RUNNING;
		WriteSeqHeader(cSerialNum, cUutRevNum);

		if(SetupDB()){
			CPVTMessagePopup(LDBf(Ldb_Database_error), LDBf(Ldb_Err_in_db_2), OK_ONLY_DLG, NO_HELP);
			goto Error;
		}

		// *** Run all tests in the test sequence ***
		gOverallpass = RunSequence(&iFailStop, Vent.Bd.serial, Vent.Bd.version);

		if(WriteRunDB(cSerialNum)){
			CPVTMessagePopup(LDBf(Ldb_Database_error), LDBf(Ldb_Err_in_db_2), OK_ONLY_DLG, NO_HELP);
			goto Error;
		}

		if(gErrorFlag){
			gOverallpass = FAIL;
		}

		sStatusDispVal = STAT_STOPPED;

		WriteStopReason(sAbortFlag, iFailStop);

		if(sAbortFlag){
			StatusReportString(2);
			SetSystemStatus(PABORT);
		}
		else{
			if(gOverallpass == PASS){
				StatusReportString(2);
				SetSystemStatus(PASSED);
			}
			else{
				if(gOverallpass == FAIL){
					StatusReportString(2);
					SetSystemStatus(PFAIL);
				}
			}
		}

		return;
	} /* end of forever while loop */

Error:
	sStatusDispVal = STAT_STOPPED;
	FreeTestBuffers(tDataRec);

	return;
}

/*****************************************************************************/
/* Name:     RunSequence                                                     */ 
/*                                                                           */
/* Purpose:  Run just one test in the sequence                               */
/*                                                                           */
/* Example Call:   int RunSequence(int *failStop,                            */
/*                       char *serialNum, char *revNum);                     */
/*                                                                           */
/* Input:  failStop     - 1 0r 0.                                            */
/*         serialNum    - serial number                                      */
/*		   revNum	    - revision number                                    */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */ 
/* Return:   none                                                            */
/*                                                                           */
/*****************************************************************************/
int RunSequence(int *failStop, char *serialNum, char *revNum)
{
	int IsLooping;
	int iIndex, iSaveIndex;
	int iTestResult;
	char *cTemp_ptr = NULL;
//	char *cTestName ="\0";
	char *cTestName = NULL;
	char *sNum = NULL;
	char *cTempSerial, *cTempRevision, *rNum;

	gOverallpass = PASS;
	*failStop    = FALSE;
	sAbortFlag   = FALSE;

	gSequenceRunning = TRUE;

	if(gDatabaseBuffer != NULL){
		free(gDatabaseBuffer);
		gDatabaseBuffer = NULL;
	}

	// All the tests are performed in sequence in a loop
	
	for(iIndex = 0; iIndex < (gNumTests) && !(sAbortFlag) && !(*failStop); iIndex++){

		SetupJetOATest();
		tDataRec[iIndex].result    = PASS;
		tDataRec[iIndex].outBuffer = NULL;
		tDataRec[iIndex].outBuff_localized = NULL;
		if(tDataRec[iIndex].sRunMode == RUN_MODE_SKIP){
			tDataRec[iIndex].result = SKIP;
			UpdateTestListing(iIndex, TEST_SKIP);
			WriteOATestDB_Jet(iIndex+1, DB_SKIP);
			WriteTestResult(iIndex );
			continue;
		}

		ProcessSystemEvents();
		if(gStoppedInd){
			break;
		}

		if(gPausedInd){
			SetSysPanelMessage(LDBf(Ldb_PauseMsg1));
			SetTestButtonMode(TB_PAUSED);
			while(gPausedInd){
				ProcessSystemEvents();
			}
		}

		iSaveIndex = iIndex;
		UpdateTestListing(iSaveIndex, TEST_RUNNING);

		SetCtrlIndex(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, iIndex);
		ProcessSystemEvents();

		iCurrentTest = iIndex;
//		cTestName = (char *)malloc(strlen(TestName[CurrLanguage][iIndex ]) +1);
//		strcpy(cTestName, TestName[CurrLanguage][iIndex]);
		cTestName = TestName[CurrLanguage][iIndex];

		CurrentTest(iIndex);

		iTestResult= tDataRec[iIndex].result;

		cTempSerial = StrDup(serialNum);
		strtok(cTempSerial, ":");
		sNum = strtok(NULL, ":");

		cTempRevision = StrDup(revNum);
		strtok(cTempRevision, ":");
		rNum = strtok(NULL, ":");

		//                                                  YES_DB NO_LOCAL          NO_JET
		cTemp_ptr = Tfmtdisplay(tDataRec[iIndex].outBuffer, TRUE, FALSE, sNum, rNum, FALSE, iIndex);
		if(cTemp_ptr != NULL){
			free(cTemp_ptr);
			cTemp_ptr = NULL;
		}

		//                                                          NO_DB  LOCAL             JET
		cTemp_ptr = Tfmtdisplay(tDataRec[iIndex].outBuff_localized, FALSE, TRUE, sNum, rNum, TRUE, iIndex);

		free(cTempSerial);
		free(cTempRevision);

		if(cTemp_ptr == NULL && tDataRec[iIndex].result == ABORT){
			PostInfo2Window(iIndex, LDBf(Ldb_MSG_ABORT), cTestName/*(char *)TestName[CurrLanguage][iIndex]*/);
		}

		if(cTemp_ptr != NULL){
			PostInfo2Window(iIndex, cTemp_ptr, cTestName/*(char *)TestName[CurrLanguage][iIndex]*/);
			free(cTemp_ptr);
			cTemp_ptr = NULL;
			if(WriteStepDB()){
				CPVTMessagePopup(LDBf(Ldb_Database_error), LDBf(Ldb_Err_in_db_2), OK_ONLY_DLG, NO_HELP);
			}
		}
		if(gDatabaseBuffer != NULL){
			free(gDatabaseBuffer);
			gDatabaseBuffer = NULL;
		}

		if(gErrorFlag){
			gOverallpass = FAIL;
			UpdateTestListing(iSaveIndex, TEST_ERROR);
		}
		else{
			switch(tDataRec[iIndex].result){

				case PASS:
					UpdateTestListing(iSaveIndex,TEST_PASS);
					WriteOATestDB_Jet(iIndex+1, DB_PASS);
					break;

				case ABORT:
					UpdateTestListing(iSaveIndex, TEST_ABORT);
					WriteOATestDB_Jet(iIndex+1, DB_ABORT);
					sAbortFlag = TRUE;
					gOverallpass = FAIL;
					break;

				case FAIL:
					UpdateTestListing(iSaveIndex,TEST_FAIL);
					WriteOATestDB_Jet(iIndex+1, DB_FAIL);
					gOverallpass = FAIL;
					break;
			} // end switch(tDataRec[iIndex].result)
		}

		sStatusDispVal = STAT_RUNNING;
		ProcessSystemEvents();

		WriteTestResult(iSaveIndex);
		if(sAbortFlag){
			SetCtrlVal(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY,/* isSinglePass ? LDBf(Ldb_MSG_SINGLE_ABORT) :*/ LDBf(Ldb_MSG_UUT_ABORT));
		}

		if(gErrorFlag){
			*failStop = TRUE;
			SetCtrlVal(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY,/* isSinglePass ? LDBf(Ldb_MSG_SINGLE_RTE) :*/ LDBf(Ldb_MSG_UUT_RTE));
		}

		if(tDataRec[iIndex].result == FAIL && sStopOnFail){
			*failStop = TRUE;
			gOverallpass = FAIL;
			SetCtrlVal(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY,/* isSinglePass ? LDBf(Ldb_MSG_SINGLE_FAIL) :*/ LDBf(Ldb_MSG_UUT_FAIL));
		}
		FreeTestBuffers(tDataRec);
		SetSysPanelMessage("");
	} // end for(iIndex = 0; iIndex < (gNumTests) .../* end for loop */

	if(gStoppedInd == 1){
		gOverallpass = DB_ABORT;
		sAbortFlag = 1;
	}
	gPausedInd = gStoppedInd = 0;
	gSequenceRunning = FALSE;
//	free(cTestName);

	return gOverallpass;
}

/*****************************************************************************/
/* Name:   UpdateTestListing                                                 */
/*                                                                           */
/* Purpose:  update the Sequence Display for one test                        */
/*                                                                           */ 
/* Example Call:   void UpdateTestListing(int iIndex,int status);            */
/*                                                                           */
/* Input:    iIndex - iIndex of test to update                               */
/*			 status - status of test(pass, fail, skip, etc)                  */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   None                                                            */
/*                                                                           */
/*****************************************************************************/
void UpdateTestListing(int iIndex, int iStatus)
{
	char cOutputString[75] = {0};
	char cOutputString2[75] = {0};
	char cTestModeStr[3];
//	char *cTestName = "\0";
	char *cTestName = NULL;
	char *cTestInfo[8] = {0};
	int i;
	int iLoopCount;
	short sTestMode;
	short sLimitSpec;

	iCurrentTest = iIndex;
	sTestMode = tDataRec[iIndex].sRunMode;
	if( (iIndex < MIN_TESTS) || (iIndex > MAX_TESTS) ){
	   ErrorMessage(NO_TEST_ERROR);
	}

	// The list box is formatted.
	SetCtrlAttribute(gPanelHandle[TESTPANEL], ResArray[iIndex], ATTR_TEXT_JUSTIFY, VAL_RIGHT_JUSTIFIED);

	switch(sTestMode){
		case RUN_MODE_FORCE_PASS:
			strcpy(cTestModeStr, "P");
			break;

		case RUN_MODE_FORCE_FAIL:
			strcpy(cTestModeStr, "F");
			break;

		case RUN_MODE_SKIP:
			strcpy(cTestModeStr, "S");
			break;

		default:
			strcpy(cTestModeStr, " ");
			break;
	}

//	cTestName =(char *)malloc(strlen(TestName[CurrLanguage][iIndex]) +1);
//	strcpy(cTestName,TestName[CurrLanguage][iIndex]);
	cTestName = TestName[CurrLanguage][iIndex];

	// The selected test status is updated in the test panel

	switch(iStatus){
		case TEST_CLEAR:
			Fmt(cOutputString, "%s", cTestName);
			break;

		case TEST_RUNNING:
			SetCtrlAttribute(gPanelHandle[TESTPANEL], ResArray[iIndex], ATTR_TEXT_COLOR, RUNNING_COLOR);
			Fmt(cOutputString2, "%s%s", RUNNING_FMT, cTestName);
			Fmt(cOutputString,  "%s%s", FRIGHT,      LDBf(Ldb_MSG_RUNNING));
			break;

		case TEST_PASS:
			#ifndef PF_ICONS
			SetCtrlAttribute(gPanelHandle[TESTPANEL], ResArray[iIndex], ATTR_TEXT_COLOR, PASS_COLOR);
			Fmt(cOutputString2, "%s%s", PASS_FMT, cTestName);
			if( (iIndex < MIN_TESTS) || (iIndex > MAX_TESTS) ){
				ErrorMessage(NO_TEST_ERROR);
				return;
			}
			if(sLimitSpec == SPEC_LOG || sLimitSpec == SPEC_NONE){
				Fmt(cOutputString, "%s%s", FRIGHT, LDBf(Ldb_MSG_NONE));
			}
			else{
				Fmt(cOutputString, "%s%s", FRIGHT, LDBf(Ldb_MSG_PASS));
			}
			#else

			#endif
			break;

		case TEST_FAIL:
			SetCtrlAttribute(gPanelHandle[TESTPANEL], ResArray[iIndex], ATTR_TEXT_COLOR, FAIL_COLOR);
			Fmt(cOutputString2, "%s%s", FAIL_FMT, cTestName);
			Fmt(cOutputString,  "%s%s", FRIGHT,   LDBf(Ldb_MSG_FAIL));
			break;

		case TEST_SKIP:
			SetCtrlAttribute(gPanelHandle[TESTPANEL], ResArray[iIndex], ATTR_TEXT_COLOR, SKIPPED_COLOR);
			Fmt(cOutputString2, "%s%s", SKIPPED_FMT, cTestName);
			Fmt(cOutputString,  "%s%s", FRIGHT,      LDBf(Ldb_MSG_SKIP));
			break;

		case TEST_LOOPING:
			SetCtrlAttribute(gPanelHandle[TESTPANEL], ResArray[iIndex], ATTR_TEXT_COLOR, LOOPING_COLOR);
			if( (iIndex < MIN_TESTS ) || (iIndex > MAX_TESTS) ){
				ErrorMessage(NO_TEST_ERROR);
				return;
			}

			iLoopCount = tDataRec[iIndex].loopCount;
			Fmt(cOutputString2, "%s%s",   LOOPING_FMT,           cTestName);
			Fmt(cOutputString,  "%s(%i)", LDBf(Ldb_MSG_LOOPING), iLoopCount);
			break;

		case TEST_ABORT:
			SetCtrlAttribute(gPanelHandle[TESTPANEL], ResArray[iIndex], ATTR_TEXT_COLOR, ERROR_COLOR);
			Fmt(cOutputString2, "%s%s", ERROR_FMT, cTestName);
			Fmt(cOutputString,  "%s",   LDBf(Ldb_MSG_ABORT));
			break;

		case TEST_ERROR:
			SetCtrlAttribute(gPanelHandle[TESTPANEL], ResArray[iIndex], ATTR_TEXT_COLOR, ERROR_COLOR);
			Fmt(cOutputString2, "%s%s", ERROR_FMT, cTestName);
			Fmt(cOutputString,  "%s",   LDBf(Ldb_MSG_ERROR));
			break;

		default:
			break;
	} // end switch(iStatus)
	
	SetCtrlVal(gPanelHandle[TESTPANEL], ResArray[iIndex], "");

	// Test status indicators are displayed next to the test name depending on the status of test.

	SetCheckXTest(iIndex, CX_CLEAR);

	if(iStatus == TEST_CLEAR){
		ReplaceListItem(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, iIndex, cTestName, iIndex);
	}
	else{
		if(iStatus == TEST_PASS){
			SetCheckXTest(iIndex, CX_PASS);
		}
		else if(iStatus==TEST_FAIL){
			SetCheckXTest(iIndex, CX_FAIL);
		}
		else{
			SetCtrlVal(gPanelHandle[TESTPANEL], ResArray[iIndex], cOutputString);
		}
		ReplaceListItem(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, iIndex, cOutputString2, iIndex);
	}
//	free(cTestName);

	return;
}

/*****************************************************************************/
/* Name:     ResetSeqDisplay                                                 */
/*                                                                           */
/* Purpose:  Redisplay tests in Sequence Display list box on main panel.     */
/*                                                                           */
/* Example Call:   void ResetSeqDisplay(void);                               */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   None                                                            */
/*                                                                           */
/*****************************************************************************/
void ResetSeqDisplay(void)
{
	int i;
	int iRunMode;

	// Redraw the list of tests
	//  noting which tests, if any, are to be skipped

	GetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, ATTR_VISIBLE_LINES, &gNumTests);
	ClearListCtrl(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY);

	for(i = 0; i < gNumTests; i++){ 

		InsertListItem(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, i, "", i);

		iRunMode = tDataRec[i].sRunMode;
		if(iRunMode == RUN_MODE_SKIP){
			iRunMode = TEST_SKIP;
		}
		else{
			iRunMode = TEST_CLEAR;
		}
		UpdateTestListing(i, iRunMode);
	}

	return;
}

/*****************************************************************************/
/* Name:     ErrorMessage                                                    */
/*                                                                           */
/* Purpose:  Displays Error Message                                          */
/*                                                                           */
/* Example Call: void ErrorMessage(int errorcode);                           */
/*                                                                           */
/* Input:   errorcode = Code number of the Error.                            */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:    none                                                           */
/*                                                                           */
/*****************************************************************************/
void ErrorMessage(int errorcode, ...)
{
	va_list parmInfo;
	char message[256];
	char cviErrMsg[256];
	int testNum;
	int int1, int2;
	char *str1, *str2;

	switch(errorcode){
		case NO_TEST_ERROR:
			CPVTMessagePopup(LDBf(Ldb_Engine_Error), LDBf(Ldb_No_Tests), OK_ONLY_DLG, NO_HELP);
			break;
			
		case COM_PTS_ERROR:
			CPVTMessagePopup(LDBf(Ldb_Com_Error), LDBf(Ldb_PTS_Com_Error), OK_ONLY_DLG, NO_HELP);
			sAbortFlag = TRUE;
			break;

		case COM_840_ERROR:
			CPVTMessagePopup(LDBf(Ldb_Com_Error), LDBf(Ldb_840_Com_Error), OK_ONLY_DLG, NO_HELP);
			sAbortFlag =TRUE;
			break;

		case PRESSURE_TEST_FAILURE_ERROR:
		case ATM_PRESS_TEST_FAILURE_ERROR:
		case GAS_FLOW_TEST_FAILURE_ERROR:
			break;  // Nothing required here. message already displayed.

		case WRITING_TO_FILE_ERROR:
			str1 = va_arg(parmInfo, char *);
			sprintf(message, LDBf(Ldb_Cant_Write_File), str1);
			CPVTMessagePopup(LDBf(Ldb_Engine_Error), message, OK_ONLY_DLG, NO_HELP);
			break;

		case UNABLE_TO_OPEN_FILE_ERROR:
			str1 = va_arg(parmInfo, char *);
			sprintf(message, LDBf(Ldb_Cant_Open_file), str1);
			CPVTMessagePopup(LDBf(Ldb_Engine_Error), message, OK_ONLY_DLG, NO_HELP);
			break;

		default:
			sprintf(message, LDBf(Ldb_Unknown_Error), errorcode);
			CPVTMessagePopup(LDBf(Ldb_Engine_Error), message, OK_ONLY_DLG, NO_HELP);
			break;
	}

	return;
}

/*****************************************************************************/
/* Name:     CurrentTest                                                     */
/*                                                                           */
/* Purpose:  This function performs the test as indicated by the             */
/*           index value.                                                    */
/*                                                                           */
/*                                                                           */
/* Example Call:   CurrentTest(int index)                                    */
/*                                                                           */
/* Input:    index - Test number                                             */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           None                                                            */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void CurrentTest(int index)
{
	switch(index){

		case 0:
			ManualPerformanceTest();
			break;

		case 1:
			AtmospherePressureTest();
			break;

		case 2:
			BreathDeliveryAlarmTest();
			break;

		case 3:
			GuiAlarmTest();
			break;

		case 4:
			CircuitPressureTest();
			break;

		case 5:
			GasFlowPerformanceTest(1);
			break;

		case 6:
			GasFlowPerformanceTest(2);
			break;

		case 7:
			PostRevision();
			break;

		case 8:
			VolumePerformanceTest();
			break;

		case 9: 
			PressureControlPerformanceTest();
			break;

		case 10:
			PEEPPerformanceTest();
			break;

		case 11:
			FIO2PerformanceTest();
			break;

		default:
			break;
	} // end switch(index)
}

