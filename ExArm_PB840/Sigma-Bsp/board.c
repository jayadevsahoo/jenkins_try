/***************************************************************************
*
*		Copyright (c) 1992-1996 MICROTEC RESEARCH INCORPORATED.
*
*	All rights reserved. MICROTEC RESEARCH's source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	MICROTEC RESEARCH. Any attempt or participation in deciphering,
*	decoding, reverse engineering or in any way altering the source code
*	is strictly prohibited, unless the prior written consent of
*	MICROTEC RESEARCH is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
****************************************************************************
*/

/*
** File:
**
**    board.c
**
** Contents:
**
**    This file will define the devices that will be used in the BFORM
**    board.
**
**    For every device on the board, five general items are required:
**       1) ROM (configuration)
**       2) RAM (device descriptor and device specific RAM)
**       3) method (an association of interface code and device descriptor RAM)
**       4) id ( a pointer to a method)
**       5) install function (called in logio_board_init() )
**
**    The board is treated like a device.  This device is special in that it
**    does not generally require RAM.
**
**    This file will create the structures required for board. Device specific 
**    structures are initialzed in the file "devcnfg.c".  
**    logio_board_init() function calls logio_device_install() which in turn should
**    perform device specific install for each & every device.This
**    file will define seven logio functions for the board.
**
**
**    These sections exist:
**       include files
**       board device 
**       board functions
**    
**
** History:
**
**   Date        Name              Description
**   ---------------------------------------------------------------------- 
**   11-28-94    Siprian Rodrigues divided into TWO files: board.c and devcnfg.c 
*/


/* ----------------------------------------------------------------------------
**   INSTRUCTION:  update the following comments with board specifics
**   This is just good things to do for people in here after you
** --------------------------------------------------------------------------*/

/* 
** The board logio devices currently supported are: 
** 
**    1. TIMER_1
**    2. ETHER_1
**    3. SERIAL_1
**    4. SERIAL_2
**    5. SERIAL_3
**    6. SERIAL_4
** 
** The board has the following devices: 
** 
**    1. EFORM Ethernet controller
**    2. SFORM serial controller chip #1 (has n ports)
**    3. SFORM serial controller chip #2 (has n ports)
** 
** The mapping of physical devices to logical devices is as follows:
** 
**    1. TIMER_1....... TIFORM
**    2. ETHER_1....... EFORM
**    3. SERIAL_1...... SFORM
**    4. SERIAL_2...... SFORM
**    5. SERIAL_3...... SFORM
**    6. SERIAL_4...... SFORM
** 
** Each *physical device* has associated with it a "device 
** descriptor". 
**
** The declarations below are for the 4 *physical devices* listed 
** above.
** 
** If you wish to see what information the "device descriptors" 
** have in them, refer to the following libdevice files:
** 
**    1. For info on ETHER, look at eform.h and eform.c
**    2. For info on SERIAL, look at sform.h and sform.c
**    3. For info on TIMER, look at tform.h and tiform.c
*/


/******************************************************************************
****************************  INCLUDE FILES       *****************************
******************************************************************************/
/* ----------------------------------------------------------------------------
**   INSTRUCTION:  modifications are required where this banner
**   appears throughout this file.
**   The conditionals for SERIAL_DEVICE, ETHER_DEVICE, TIMER_DEVICE 
**   and SOFTWARE_TIMER_DEVICE are defined in the makefiles for
**   serial & ethernet testing.  These are intended to allow one
**   board config file to be used for a varitey of device implementations.
**   For example, to start with the serial device will be used with
**   a software timer and then an ethernet device will be added and
**   finally a real timer.
** --------------------------------------------------------------------------*/

#include "ansiprot.h"         /* ANSI prototypes                */
#include "logio.h"            /* logio defines and typedefs     */
#include "vbsp_cbid.h"        /* VBSP control block typedef     */
#include "vbsp.h"             /* VBSP return codes and typedefs */
#include "logio_charbuf.h"    /* circular character buffers     */


/******************************************************************************
****************************  BOARD DEVICE        *****************************
******************************************************************************/


/* ***********************************************************
** External Functions ***********************************
*************************************************************/

logio_status_t logio_device_install(void);


logio_status_t logio_board_init(_ANSIPROT1(logio_device_id_t));

logio_status_t logio_board_read(
	_ANSIPROT3( 
	logio_device_id_t,
	char *,
	int *));

logio_status_t logio_board_write(
	_ANSIPROT3(
	logio_device_id_t,
	char *,
	int *));

logio_status_t logio_board_control(
	_ANSIPROT3(
	logio_device_id_t,
	logio_control_t, 
	void *));

logio_status_t logio_board_getmsg(_ANSIPROT2(logio_device_id_t,logio_buff_t **));

logio_status_t logio_board_putmsg(
   _ANSIPROT2(
      logio_device_id_t, 
      logio_buff_t *));

logio_status_t logio_board_poll(
   _ANSIPROT3(
      logio_device_id_t, 
      logio_control_t, 
      void *));

const logio_fops_t logio_board_fops = {
   logio_board_init,
   logio_board_read,
   logio_board_write,
   logio_board_control,
   logio_board_getmsg,
   logio_board_putmsg,
   logio_board_poll
};

/*   INSTRUCTION:  
**   The logio_board_xxx functions in the structure above  are not used.
**   Default function return  LOGIO_STATUS_NOTSUPPORTED.
**   single function that returns LOGIO_STATUS_NOTSUPPORTED if desired.
**   They are left for completeness.  If you decide to remove them
**   the structure logio_board_fops will need to be modified to replace
**   read,write,poll,getmsg,putmsg & control, as below.
*/


/*
const logio_fops_t logio_board_fops =
{
    (logio_status_t(*)(logio_device_id_t)) logio_board_init,
    (logio_status_t(*)(logio_device_id_t, char *, int *)) logio_not_supported,
    (logio_status_t(*)(logio_device_id_t, char *, int *)) logio_not_supported,
    (logio_status_t(*)(logio_device_id_t, logio_control_t, void *)) logio_board_co
ntrol,
    (logio_status_t(*)(logio_device_id_t, logio_buff_t **)) logio_not_supported,
    (logio_status_t(*)(logio_device_id_t, logio_buff_t *)) logio_not_supported,
    (logio_status_t(*)(logio_device_id_t, logio_control_t, void *)) logio_not_supp
orted,
};
*/



const logio_method_t logio_board_method = {
   &logio_board_fops,
   0
};

const logio_device_id_t logio_board_id = &logio_board_method;



/******************************************************************************
****************************  BOARD FUNCTIONS     *****************************
******************************************************************************/
/* ----------------------------------------------------------------------------
**   INSTRUCTION:  The following logio_board_xxx functions won't need to 
**   be modified as they return not supported.  They can be replaced with a 
**   single function that returns LOGIO_STATUS_NOTSUPPORTED if desired.
**   They are left for completeness.  If you decide to remove them 
**   the structure logio_board_fops will need to be modified to replace
**   read,write,poll,getmsg,putmsg & control. Most of the BSPs use the logio
**   function logio_not_supported().
** --------------------------------------------------------------------------*/
/*
 *
 * Function name:
 *
 *     logio_board_init
 *
 * Description:
 *
 *     This function initializes the BOARD device and brings it to 
 *     a known state. The "known state" means that the board is 
 *     "ready to run", which means that any devices that need to be 
 *     initialized before the specific logio initialization have 
 *     been initialized. This accomplished through the call to
 *     logio_device_install().
 *
 * Inputs:
 *
 *     id:   ID of the target device
 *
 * Outputs:
 *
 *     NONE
 *
 * Return codes:
 *
 *    LOGIO_STATUS_OK
 *    LOGIO_STATUS_RESOURCES
 *
 * Method:
 *
 *    The BOARD has the following devices on board:
 *
 *
 * Side-effects:
 * Global variables:
 *
 */
logio_status_t logio_board_init(id)
logio_device_id_t id;
{
    logio_status_t ret;
	
    if( (ret =  logio_device_install()) )
	return(ret);

#if defined(VME_SM)
    board_common_mmap_init();
#endif

    return(ret);

}


/*
 *
 * Function name:
 *
 *    logio_board_read
 *
 * Description:
 *
 *      This operation is not supported for the BOARD device, so 
 *      this call will always return LOGIO_STATUS_NOTSUPPORTED.
 *
 * Inputs:
 *
 *      id:      The ID of the target device
 *      buflenp:   On input, this contains a pointer to an 
 *                      integer that contains the number of bytes 
 *                      in the input buffer
 *
 * Outputs:
 *
 *      buf:      The data that were read
 *      buflenp:   The number of bytes read
 *
 * Return codes:
 *
 *      LOGIO_STATUS_OK
 *      LOGIO_STATUS_NOTSUPPORTED
 *
 * Side-effects:
 * Global variables:
 *
 */

logio_status_t logio_board_read(id, buf, buflenp)
logio_device_id_t   id;
char         *buf;
int         *buflenp;
{
   return(LOGIO_STATUS_NOTSUPPORTED);
}


/*
 * Function name:
 *
 *    logio_board_write
 *
 * Description:
 *
 *      This operation is not supported for the BOARD device, so 
 *      this call will always return LOGIO_STATUS_NOTSUPPORTED.
 *
 * Inputs:
 *
 *      id:      The ID of the target device
 *      buf:      The data to be written
 *      buflenp:   On input, this contains a pointer to an 
 *                      integer that contains the number of bytes 
 *                      in the input buffer
 *
 * Outputs:
 *
 *      buflenp:   The number of bytes written
 *
 * Return codes:
 *
 *      LOGIO_STATUS_NOTSUPPORTED
 *
 * Side-effects:
 * Global variables:
 *
 */

logio_status_t logio_board_write(id, buf, buflenp)
logio_device_id_t   id;
char         *buf;
int         *buflenp;
{
return(LOGIO_STATUS_NOTSUPPORTED);
}


/*
 * Function name:
 *
 *    logio_board_control
 *
 * Description:
 * logio_device_control() is used for device specific commands.
 * The last parameter "arg" is a pointer to data to be used by the
 * function or to be filled in by the function.  Other functions
 * may ignore arg or may treat it directly as a data item; they
 * may, for example, be passed an int value.
 * 
 * For each device, the following controls are required for the
 * BSP developer to provide.
 * 
 * Control LOGIO_CONTROL_GETDEVNAME can be used for getting the
 * name of a device. In this case "arg" is a buffer up to
 * LOGIO_DEV_NAME_LEN, currently 16 bytes.
 * 
 * Control LOGIO_CONTROL_GETDEVTYPE can be used to get the type of
 * the underlying physical device. In this case "arg" points to a
 * variable of type logio_device_type_t. The only device type
 * currently defined is LOGIO_DEV_TYPE_GENERIC.
 * 
 * Controls LOGIO_CONTROL_GETCOUNTTIME and
 * LOGIO_CONTROL_SETCOUNTTIME get and set the number of
 * nanoseconds per timer count increment.
 * 
 * For LOGIO_CONTROL_GETCOUNTTIME, logio_device_control returns
 * the number of nanoseconds per timer count increment in "arg".
 * 
 * For LOGIO_CONTROL_SETCOUNTTYPE, logio_device_control sets the
 * number of nanoseconds that per counter increment to "arg".
 * 
 * If logio_device_control cannot set the timer count increment
 * time to "arg", then it makes the best attempt, writes into
 * "arg" the actual value that was set, and returns
 * LOGIO_STATUS_OK.
 * 
 * If this parameter cannot be modified at all, then
 * logio_device_control returns LOGIO_STATUS_NOTSUPPORTED.
 * 
 * Controls LOGIO_CONTROL_GETTIMERPRELOAD and
 * LOGIO_CONTROL_SETTIMERPRELOAD get and set the timer's preload
 * value, in counts.
 * 
 * For LOGIO_CONTROL_GETTIMERPRELOAD, logio_device_control returns
 * the number of counts per clock rollover in "arg".
 * 
 * For LOGIO_CONTROL_SETTIMERPRELOAD, logio_device_control sets
 * the number of counts per clock rollover to "arg".
 * 
 * If logio_device_control cannot set the preload value to "arg",
 * then it makes the best attempt, writes into "arg" the actual
 * value that was set, and returns LOGIO_STATUS_OK.
 * 
 * If this parameter cannot be modified at all, then
 * logio_device_control returns LOGIO_STATUS_NOTSUPPORTED.
 * 
 * Control LOGIO_CONTROL_GETTIMERROLLOVER gets the value at which
 * the timer count will be reloaded with the preload value.
 * logio_device_control returns the rollover value, in counts, in
 * "arg".
 * 
 * Control LOGIO_CONTROL_READTIMER gets the current value of the
 * timer's counter, in counts. logio_device_control returns the
 * counter value in "arg".
 * 
 * Controls LOGIO_CONTROL_SETHWADDR and LOGIO_CONTROL_GETHWADDR
 * can be used for devices that support the notion of a physical
 * address. In this case "arg" is a structure.
 * 
 * #define LOGIO_HW_ADDR_LEN 6
 * 
 * typedef struct { 
 *            int bufflen; 
 *            char addr[LOGIO_HW_ADDR_LEN]; 
 *            }
 *    logio_hw_addr_t;
 * 
 * Control LOGIO_CONTROL_RESET can be used to reset the device
 * when applicable.  The result is device dependent.
 * 
 * Control LOGIO_CONTROL_INTINFOGET is used to get information
 * about a specific interrupt.  Currently the only information is
 * "flags".  In this case "arg" points to a structure of type
 * logio_int_entry_t defined in logio.h by
 * 
 * typedef struct { 
 *            logio_int_event_t event; 
 *            int flags; 
 *            }
 *         logio_int_entry_t;
 * 
 * "event" is the only input parameter. "flags" values currently
 * supported are LOGIO_INT_ENABLED and LOGIO_INT_DISABLED.
 * 
 * Control LOGIO_CONTROL_INTINFOSET is used to set the values
 * specified in the structure.  It returns the old values. The
 * action specified by the control option is also performed at the
 * device level. LOGIO_INT_DISABLED will disable delivery to the
 * CPU of interrupts for the given <device,event> while
 * LOGIO_INT_ENABLED will enable them.
 * 
 * Other device specific control command can be added to the list
 * of available commands. Ready Systems reserves the use of
 * constants in the range 0-0xffff.
 * 
 * This function is nonblocking. Certain operation may result the
 * status LOGIO_STATUS_INPROGRESS. In this case, the function
 * logio_device_poll() can be used to find out the status of this
 * operation.
 * 
 *
 *
 * Inputs:
 *
 *      id:      The ID of the target device
 *      control:   The control command
 *      arg:       A pointer to data used by the function or 
 *                      to be filled in by the function
 *
 * Outputs:
 *
 *      arg:       A pointer to data used by the function or 
 *                      to be filled in by the function
 *
 * Return codes:
 *
 *    LOGIO_STATUS_OK
 *    LOGIO_STATUS_NOTSUPPORTED
 *    LOGIO_STATUS_INPROGRESS
 *
 * Side-effects:
 * Global variables:
 *
 */
int logio_board_control(id, control, arg)
logio_device_id_t id;
logio_control_t control;
void *arg;
{
   logio_status_t   ret;
   logio_int_entry_t *info;

   ret = LOGIO_STATUS_OK;

   switch(control)
      {
      case LOGIO_CONTROL_GETDEVNAME:
      case LOGIO_CONTROL_GETDEVTYPE:
      case LOGIO_CONTROL_RESET:
        break;

#if defined(VME_SM)
      case LOGIO_CONTROL_MMAP:  /* Called ONLY when shared memory configuration is used */
         ret = board_common_mmap((logio_mmap_t *)arg);
         break;
#endif
      
      case LOGIO_CONTROL_INTINFOSET:
        info = (logio_int_entry_t *) arg;
        if (info->flags == LOGIO_INT_ENABLED)
           {
           /* ENABLE BOARD INTERRUPTS HERE */
           ret = LOGIO_STATUS_OK;
           }
        else if (info->flags == LOGIO_INT_DISABLED)
           {
           /* DISABLE BOARD INTERRUPTS HERE */
           ret = LOGIO_STATUS_OK;
           } 
        else
           ret = LOGIO_STATUS_NOTSUPPORTED;
        break;
      
      case LOGIO_CONTROL_INTINFOGET:
        #if OTHER_INIT == 1
        info = (logio_int_entry_t *) arg;
        /* info->flags = GET_BOARD_INTERRUPTS_HERE */
        if (info->flags)
           info->flags = LOGIO_INT_ENABLED;
        else
           info->flags = LOGIO_INT_DISABLED;
      
        ret = LOGIO_STATUS_OK;
        #else
        ret = LOGIO_STATUS_OK;
        #endif
        break;
      
      default:
        return(LOGIO_STATUS_NOTSUPPORTED);
      }
      
   return(ret);
}



/*
 *
 * Function name:
 *
 *    logio_board_poll
 *
 * Description:
 *
 *      This operation is not supported for the BOARD device, so 
 *      this call will always return LOGIO_STATUS_NOTSUPPORTED.
 *
 * Inputs:
 *
 *      id:      The ID of the target device
 *      control:   The control code
 *
 * Outputs:
 *
 *      arg:      Pointer to data that contain the result. 
 *                      The data are valid when logio_device_poll 
 *                      returns LOGIO_STATUS_OK.
 *
 * Return codes:
 *
 *    LOGIO_STATUS_OK
 *    LOGIO_STATUS_INPROGRESS
 *      LOGIO_STATUS_NOTSUPPORTED
 *
 * Side-effects:
 * Global variables:
 *
 */

logio_status_t logio_board_poll(id, control, arg)
logio_device_id_t         id;
logio_control_t      control;
void          *arg;
{
return(LOGIO_STATUS_NOTSUPPORTED);
}

/*
 *
 * Function name:
 *
 *    logio_board_getmsg
 *
 * Description:
 *
 *      This operation is not supported for the BOARD device, so 
 *      this call will always return LOGIO_STATUS_NOTSUPPORTED.
 *
 * Inputs:
 *
 *      id:      The ID of the target device
 *
 * Outputs:
 *
 *    **bufpp:          Received message
 *
 * Return codes:
 *
 *    LOGIO_STATUS_NOTSUPPORTED
 *
 * Side-effects:
 * Global variables:
 *
 */

logio_status_t logio_board_getmsg(id, bufpp)
logio_device_id_t      id;
logio_buff_t      **bufpp;
{
return(LOGIO_STATUS_NOTSUPPORTED);
}

 
/*
 *
 * Function name:
 *
 *    logio_board_putmsg
 *
 * Description:
 *
 *      This operation is not supported for the BOARD device, so 
 *      this call will always return LOGIO_STATUS_NOTSUPPORTED.
 *
 * Inputs:
 *
 *      id:      The ID of the target device
 *    *bufp:      buffer to send
 *
 * Outputs:
 *
 *      NONE
 *
 * Return codes:
 *
 *    LOGIO_STATUS_NOTSUPPORTED
 *
 * Side-effects:
 * Global variables:
 *
 */

logio_status_t logio_board_putmsg(id, bufp)
logio_device_id_t         id;
logio_buff_t      *bufp;
{
return(LOGIO_STATUS_NOTSUPPORTED);
}


#if defined(VME_SM)

/*
 * The following functions must be added to every BSP to support shared
 * memory. The support address translation, and any other board support 
 * function necessary for a complete BSP. Since board devices are not 
 * like binary device drivers, if a function is needed in every BSP (like
 * address translation for all VME boards), then that function can exist 
 * in namespace, and be callable directly without an fops entry (it could 
 * become a device control OP for a board).
 *
 * A null pointer returned indicates a bad parameter.
 */

#include <shmem_1.h>

typedef struct {
        logio_extmem_t type;
        char *sysbase;
        char *systop;
        char *localbase;
        char *localtop;
} logio_addr_translation_t;

/*
 * memory map for each board and memory map each board that has VMEbus short 
 * IO space  16*2 
 */

#define MAXBDS	16*2

logio_addr_translation_t logio_addrs[MAXBDS];
static int board_numaddrs;

/* translate an address from system memory space to local space */

void *board_s2l_addr(addr, memtype)
void *addr;
logio_extmem_t memtype;
{
	int i;
	logio_addr_translation_t *addr_trans;
	char *newaddr;

	for (i=0, addr_trans = logio_addrs; i < board_numaddrs; i++,addr_trans++) {
		/* match type and valid range */
		if ((addr_trans->type == memtype) &&
		    ((char *)addr >= addr_trans->sysbase) &&
		    ((char *)addr <= addr_trans->systop)) {
			newaddr = ((char *)addr - addr_trans->sysbase) +
				addr_trans->localbase;
			return (void *)newaddr;
		}
	}
	/* NO MATCH so return error */
	return (void *)0;
}

/* translate an address from local memory space to system space */

void *board_l2s_addr(addr, memtype)
void *addr;
logio_extmem_t memtype;
{
	int i;
	logio_addr_translation_t *addr_trans;
	char *newaddr;

	for (i=0, addr_trans = logio_addrs; i < board_numaddrs;
	     i++,addr_trans++) {
		/* match type and valid range */
		if ((addr_trans->type == memtype) &&
		    ((char *)addr >= addr_trans->localbase) &&
		    ((char *)addr <= addr_trans->localtop)) {
			newaddr = ((char *)addr - addr_trans->localbase) +
				addr_trans->sysbase;
			return (void *)newaddr;
		}
	}
	/* NO MATCH so return error */
	return (void *)0;
}

logio_status_t board_common_mmap(mmap)
logio_mmap_t *mmap;
{
	int idx, i;
	cpu_interrupt_t sr;

	sr = cpu_interrupt_disable();
	if ((idx = board_numaddrs) >= MAXBDS) {
		cpu_interrupt_restore(sr);
		return LOGIO_STATUS_RESOURCES;
	}
	else
		board_numaddrs++;
	for (i = 0; i < idx; i++) {
		/* verify that address translation does not already exist */
		if (logio_addrs[i].type == mmap->memtype &&
		    logio_addrs[i].sysbase == mmap->sysbase &&
		    logio_addrs[i].localbase == mmap->localbase &&
		    logio_addrs[i].systop == 
		    (char *)mmap->sysbase + mmap->size - 1) {
			board_numaddrs--;
			cpu_interrupt_restore(sr);
			return LOGIO_STATUS_OK;	/* better than an error */
		}
	}
	logio_addrs[idx].type = LOGIO_EXTMEM_UNKNOWN;
	cpu_interrupt_restore(sr);	
	logio_addrs[idx].sysbase = mmap->sysbase;
	logio_addrs[idx].systop = (char *)mmap->sysbase + mmap->size - 1;
	logio_addrs[idx].localbase = mmap->localbase;
	logio_addrs[idx].localtop = (char *)mmap->localbase + mmap->size - 1;
	/* set memtype last to prevent race condition */
	logio_addrs[idx].type = mmap->memtype;

	return LOGIO_STATUS_OK;
}


logio_status_t board_common_mmap_init()
{
	board_numaddrs = 0;
	return LOGIO_STATUS_OK;
}
#endif
