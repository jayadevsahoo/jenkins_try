#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//====================================================================
//@ Class:  SmcpResponseBuffer - SMCP Message Response Buffer
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is a SerialBuffer that buffers serial port output data 
//  from the application task to the Serial-Interface task.
//  It contains the packet memory and packet map storage required by 
//  the base class SerialBuffer. All methods for this class are located 
//  in the base class.
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the SMCP Response Buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SmcpResponseBuffer.ccv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: quf       Date: 16-Aug-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//  Introduced encapsulated version of class IDs.
//
//  Revision: 001    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//====================================================================
 
#include "SmcpResponseBuffer.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmcpResponseBuffer [constructor]
//
//@ Interface-Description
//  Instantiates a SerialBuffer with the storage space and parameters
//  defined in this derived class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
SmcpResponseBuffer::SmcpResponseBuffer(void)
    :   SerialBuffer(  SERIAL_OUTPUT_MT
	                 , packetMemory_
	                 , SMCP_RESPONSE_SIZE
	                 , SMCP_RESPONSE_COUNT 
	                 , packetMap_ )
	                 
{
	//$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SmcpResponseBuffer [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
SmcpResponseBuffer::~SmcpResponseBuffer()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
SmcpResponseBuffer::SoftFault(const SoftFaultID softFaultID,
	                         const Uint32      lineNumber,
	                         const char*       pFileName,
	                         const char*       pPredicate)
{
  FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE, 
	  GUI_Serial_Interface::SMCP_RESPONSE_BUFFER_CLASS,
	  lineNumber, pFileName, pPredicate);
}


