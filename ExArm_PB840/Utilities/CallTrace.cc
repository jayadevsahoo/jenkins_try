#include "stdafx.h"

#if defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
// Class: CallTrace - Call-Tracing Utility -- for debug use only.
//---------------------------------------------------------------------
// Interface-Description
//	To use this class, the client would place a call-trace macro
//	('CALL_TRACE("someFuncName(andIts, arguments)")') at the beginning
//	of the client's method/function.  When the code is compiled, it
//	must be compiled in debug mode -- this will place the constructor
//	of this class every place that the aforementioned macro is placed.
//	Then, at run-time, the call-trace debug flag needs to be set -- this
//	will actually print (to standard output) each function/method entry
//	that contains the call-trace macro, and each of their corresponding
//	exits.
//
//	There are two modes of tracking provided by this class.  Silent
//	tracking mode (the default) is used to "silently" (nothing printed
//	out) track the call stack.  A method is provided to "dump" out the
//	current call trace.  This method ('CallTrace::DumpTrace()') will
//	print out an unformatted listing of the current call stack.  Upon
//	detection of a fault, the fault handler will automatically "dump"
//	out the current stack before re-starting.  The other tracking mode,
//	Verbose Tracking Mode is used to print out each function entry and
//	exit -- in a formatted way -- at the time of the entry/exit.  There
//	is a static method of this class that can be used to set the tracking
//	mode.  This method ('CallTrace::ChangeVerboseMode(newVerboseFlag)')
//	takes a boolean flag that indicates whether Verbose Tracking Mode is
//	to be set.
//
//	A macro is provided for the entry/exit tracing so that all uses of
//	this facility can be "compiled away" when NOT in debug mode.
//---------------------------------------------------------------------
// Rationale
//	This debugging tool can be very handy for tracing function calls.
//---------------------------------------------------------------------
// Implementation-Description
//	This mechanism depends on the C++'s issuance of a constructor (at
//	definition time) and destructor (at scope exit).
//---------------------------------------------------------------------
// Fault-Handling
//	None.
//---------------------------------------------------------------------
// Restrictions
//	None.
//---------------------------------------------------------------------
// Invariants
//	None.
//---------------------------------------------------------------------
// End-Preamble
//
// Version-Information
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/CallTrace.ccv   25.0.4.0   19 Nov 2013 14:35:12   pvcs  $
//
// Modification-Log
//  
//  Revision: 002   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 001  By:  sah    Date:  09-10-93    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//=====================================================================

#include "CallTrace.hh"

// Usage-Classes
#include "Ostream.hh"
#include "MutEx.hh"
// End-Usage

// Code...

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================

// This provides a block of memory big enough to hold an instance of
// 'MutEx', and will be used for that very thing...
static Uint  pMutEx_[(sizeof(MutEx) + sizeof(Uint) - 1) / sizeof(Uint)];

// This is the static reference to the mutual-exclusion semaphore used
// to protect the static data of the call-trace class.
static MutEx&  rMutEx_ = *((MutEx*)::pMutEx_);


//=====================================================================
//
//  Static Member Initialization...
//
//=====================================================================

const CallTrace*  CallTrace::PLast_         = NULL;
Boolean           CallTrace::IsVerboseMode_ = FALSE;
Uint32            CallTrace::Depth_         = 0u;


//=====================================================================
//
//  Static Function Definition...
//
//=====================================================================

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function:  operator new(size_t, rMutEx)  [static]
//
// Interface-Description
//	This is to ONLY be called by 'CallTrace::Initialize()'; this
//	special operator new does NOT have a call-trace macro in it so
//	it can be used to initialize '::rMutEx_'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	none
// End-Free-Function
//=====================================================================

inline void*
operator new(size_t, MutEx& rMutEx)
{
  // run a constructor on the memory held by 'rMutEx'...
  return((void*)&rMutEx);
}


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  Initialize()  [static]
//
// Interface-Description
//	Initialize the static members of this class.
//---------------------------------------------------------------------
// Implementation-Description
//	Initialize the call-trace's mutual-exclusion semaphore by using
//	the statically defined 'operator new' that is defined above.
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	none
// End-Method
//=====================================================================

void
CallTrace::Initialize(void)
{
  // run the constructor on '::rMutEx_'...
  SAFE_FREE_PRE_CONDITION((sizeof(::pMutEx_) >= sizeof(MutEx)),
			  UTILITIES, CALL_TRACE_CLASS);
PRAGMA_PUSH_WARN(88); // "expression has no side-effects..."
  new (::rMutEx_) MutEx(0);  // local operator new...
PRAGMA_POP_WARN(); // 88...

  CALL_TRACE("Initialize()");  // must be called AFTER initialization...
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  ChangeVerboseMode(newVerboseFlag)  [static]
//
// Interface-Description
//      Change the verbose mode flag to the value of 'newVerboseFlag'.
//	Irregardless of whether verbose mode is already turned on or off,
//	call-tracing will begin with the next call of 'CALL_TRACE("...")'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//      none
//---------------------------------------------------------------------
// PostCondition
//      none
// End-Method
//=====================================================================

void
CallTrace::ChangeVerboseMode(const Boolean newVerboseFlag)
{
  if (newVerboseFlag) {
    // verbose mode is being turned on, therefore reset the depth counter...
    CallTrace::Depth_ = 0u; 
  } else {
    // verbose mode is being turned off, therefore clear out the last instance
    // pointer so that the next instance is set to be the last instance in
    // the linked-list of instances...
    CallTrace::PLast_ = NULL; 
  }

  CallTrace::IsVerboseMode_ = newVerboseFlag;
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  DumpTrace()  [static]
//
// Interface-Description
//      If verbose mode is NOT set, then whatever call-trace information
//	has been collected since the last time verbose mode was set will be
//	printed to standard output.  If verbose mode is on, then nothing
//	will be printed.  Both cases will cause the output stream to be
//	flushed.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//      none
//---------------------------------------------------------------------
// PostCondition
//      none
// End-Method
//=====================================================================

void
CallTrace::DumpTrace(void)
{
  ::rMutEx_.request();  // request mutual access to this class...

  if (!CallTrace::IsVerboseMode_) {
    // perform a stack dump by following the instance links...
    const CallTrace*  pInst;

    for (pInst = CallTrace::PLast_; pInst != NULL; pInst = pInst->pNext_) {
      // for each instance, starting from the last instance instantiated,
      // print out their function name...
      ::cout << pInst->FUNC_NAME_ << '\n';
    }
  }

  ::cout.flush();  // flush the output stream...

  ::rMutEx_.release();  // release this class...
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  InsertMarker(pMarkerText)  [static]
//
// Interface-Description
//      Insert 'pMarkerText' into the call trace output stream.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//      none
//---------------------------------------------------------------------
// PostCondition
//      none
// End-Method
//=====================================================================

void
CallTrace::InsertMarker(const char* pMarkerText)
{
  ::rMutEx_.request();  // request mutual access to this class...

  if (Utilities::IsDebugOn(::CALL_TRACE_CLASS)) {
    ::cout << "<<< MARKER:  " << pMarkerText << ">>>\n";
  }

  ::rMutEx_.release();  // release this class...
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  processEntry_(fileName, lineNumber)
//
// Interface-Description
//	Process the entry of a function.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	none
// End-Method
//=====================================================================

void
CallTrace::processEntry_(const char*  fileName,
			 const Uint32 lineNumber)
{
  ::rMutEx_.request();  // request mutual access to this class...

  if (CallTrace::IsVerboseMode_) {
    // precede the entry line with a "> " for each depth that this occurs
    // at...
    for (Uint32 cnt = 0; cnt < CallTrace::Depth_; cnt++) {
      ::cout << "> ";
    }

    // begin the entry with a '{' for easier browsing with the 'vi' editor...
    ::cout << "{  " << FUNC_NAME_ << ", " << fileName
	   << ":" << lineNumber << '\n';
    CallTrace::Depth_++;
  } else {
    // link this instance to the previously-instantiated instance...
    pNext_ = CallTrace::PLast_;
    CallTrace::PLast_ = this;
  }

  ::rMutEx_.release();  // release this class...
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  processExit_()
//
// Interface-Description
//	Process the exit of a function.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	none
// End-Method
//=====================================================================

void
CallTrace::processExit_(void)
{
  ::rMutEx_.request();  // request mutual access to this class...

  if (CallTrace::IsVerboseMode_) {
    if (CallTrace::Depth_ != 0u) {
      CallTrace::Depth_--;

      // precede the entry line with a "  " for each depth that this occurs
      // at...
      for (Uint32 cnt = 0u; cnt < CallTrace::Depth_; cnt++) {
	::cout << "  ";
      }

      // end the entry with a '}' for easier browsing with the 'vi' editor...
      ::cout << "}  " << FUNC_NAME_ << '\n';

      if (CallTrace::Depth_ == 0u) {
	// flush the buffer every time depth '0' is reached; depth '0' is the
	// very beginning of the tracing...
	::cout.flush();
      }
    }
  } else {
    // set 'CallTrace::PLast_' to the next instance, thereby removing this
    // instance from the linked list of instances...
    CallTrace::PLast_ = pNext_;
  }

  ::rMutEx_.release();  // release this class...
}

#endif // defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)
