
// E600Util.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "E600Util.h"
#include "E600UtilDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CE600UtilApp

BEGIN_MESSAGE_MAP(CE600UtilApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
	ON_COMMAND(ID_FILE_OPENFAKEGUI, &CE600UtilApp::OnFileOpenfakegui)
END_MESSAGE_MAP()


// CE600UtilApp construction

CE600UtilApp::CE600UtilApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CE600UtilApp object

CE600UtilApp theApp;
HANDLE m_hOneInstance;

// CE600UtilApp initialization

BOOL CE600UtilApp::InitInstance()
{
	m_hOneInstance = ::CreateMutex(NULL, FALSE, L"E600_UTILITY_PROGRAM");
	if(GetLastError() == ERROR_ALREADY_EXISTS)
	{
		AfxMessageBox(L"Application already running!", MB_ICONERROR);
		return FALSE;
	}

	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
#if 0
	CE600UtilDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
		m_bWait = FALSE;
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
#else
	CE600UtilDlg *pUtilDlg = new CE600UtilDlg;
	m_pMainWnd = pUtilDlg;
    pUtilDlg->Create(IDD_E600UTIL_DIALOG);	// create modeless dialog
	pUtilDlg->ShowWindow(SW_SHOWNORMAL);
	return TRUE;
#endif
}

int CE600UtilApp::ExitInstance()
{
	m_bWait = FALSE;
	CloseHandle(m_hOneInstance);
	return CWinApp::ExitInstance();
}

int CE600UtilApp::Run() 
{
#if 1
	return CWinApp::Run();
#else
	// wait till user clicks on status bar before proceeding	
	MSG msg;
	BOOL bIdle=TRUE;
	LONG lIdleCount = 0;
	CWinApp* pApp=AfxGetApp();

	AfxMessageBox(L"Into wait loop.");
	m_bWait=TRUE;
	while (m_bWait)
	{
		// idle loop waiting for messages
		while (bIdle && !::PeekMessage(&msg, NULL, NULL, NULL, PM_NOREMOVE))
		{
			if (!pApp->OnIdle(lIdleCount++)) bIdle = FALSE;
		}

		// process new messages
		do {
			// pump messages
			pApp->PumpMessage();

			// if we're done, let's go...
			if (!m_bWait)
				break;

			// otherwise keep looping
			if (pApp->IsIdleMessage(&msg))
			{
        			bIdle = TRUE;
					lIdleCount = 0;
			}
		} while (::PeekMessage(&msg, NULL, NULL, NULL, PM_NOREMOVE));
	}
	AfxMessageBox(L"Out of wait loop.");
	
	return 0;
#endif
}

void CE600UtilApp::OnFileOpenfakegui()
{
	CString FileName(L"PB840FakeGui.exe");
	wchar_t PathName[100];
	wchar_t *pszFileName[100];
	
	DWORD Ret = GetFullPathName(FileName, 100, (LPWSTR)PathName, (LPWSTR *)pszFileName);
	
	PROCESS_INFORMATION pi;
	STARTUPINFO sui;

	//set up the STARTUPINFO structure,
	// then call CreateProcess to try and start the new exe.
	sui.cb = sizeof (STARTUPINFO);
	sui.lpReserved = 0;
	sui.lpDesktop = NULL;
	sui.lpTitle = NULL;
	sui.dwX = 0;
	sui.dwY = 0;
	sui.dwXSize = 0;
	sui.dwYSize = 0;
	sui.dwXCountChars = 0;
	sui.dwYCountChars = 0;
	sui.dwFillAttribute = 0;
	sui.dwFlags = 0;
	sui.wShowWindow = 0;
	sui.cbReserved2 = 0;
	sui.lpReserved2 = 0;

	if(!CreateProcess(PathName, NULL, NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &sui, &pi))
	{
		UINT ErrCode = GetLastError();
		AfxMessageBox(L"Failed executing PB840FakeGui.exe");
	}
}
