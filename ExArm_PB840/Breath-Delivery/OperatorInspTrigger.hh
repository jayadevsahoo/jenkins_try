#ifndef OperatorInspTrigger_HH
#define OperatorInspTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: OperatorInspTrigger - Triggers inspiration based on operator request.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OperatorInspTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  sp    Date:  26-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Derive from PeepRecoveryInspTrigger class.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "PeepRecoveryInspTrigger.hh"

//@ End-Usage

class OperatorInspTrigger : public PeepRecoveryInspTrigger {
  public:
    OperatorInspTrigger(void);
    virtual ~OperatorInspTrigger(void);


    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  protected:
    virtual void triggerAction_(const Boolean trigCond);

  private:
    OperatorInspTrigger(const OperatorInspTrigger&);		// not implemented...
    void   operator=(const OperatorInspTrigger&);	// not implemented...

};


#endif // OperatorInspTrigger_HH 
