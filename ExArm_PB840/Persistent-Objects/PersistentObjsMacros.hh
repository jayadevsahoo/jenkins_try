
#ifndef PersistentObjsMacros_HH
#define PersistentObjsMacros_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PersistentObjsMacros - The template macros for this subsystem.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/PersistentObjsMacros.hhv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//  
//====================================================================

#include "TemplateMacros.hh"


//@ Macro:  DoubleBuff(TYPE)
// This provides a C++ template-like representation of the name of the
// double buffer template, where 'TYPE' is replaced with the specific
// type of the instantiated template.
#define  DoubleBuff(TYPE)		name2(DblBuff_,TYPE)


#endif // PersistentObjsMacros_HH 
