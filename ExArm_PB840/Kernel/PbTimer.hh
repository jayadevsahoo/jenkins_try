#ifndef PbTimer_HH
#define PbTimer_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1998, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename:  PbTimer - Address definitions for PB timers.
//
//---------------------------------------------------------------------
// 
//@ End-Preamble
//
//@ Version
//
//@(#) $Header:   /840/Baseline/Kernel/vcssrc/PbTimer.hhv   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 001    By: Gary Cederquist  Date: 26-Oct-1998  DCS Number: 5311
//    Project: 840 Cost Reduction
//    Description:
//       Initial version
//
//=====================================================================

#include "SigmaTypes.hh"

// PB Timer Register Definitions

#define P_PBTIMER_1_PRELOAD ((volatile Uint32 *)(0xFFBE8100))
#define P_PBTIMER_2_PRELOAD ((volatile Uint32 *)(0xFFBE8110))
#define P_PBTIMER_3_PRELOAD ((volatile Uint32 *)(0xFFBE8120))

#define P_PBTIMER_1_COUNTER ((volatile Uint32 *)(0xFFBE8104))
#define P_PBTIMER_2_COUNTER ((volatile Uint32 *)(0xFFBE8114))
#define P_PBTIMER_3_COUNTER ((volatile Uint32 *)(0xFFBE8124))

#define P_PBTIMER_1_CONTROL ((volatile Uint8 *)(0xFFBE8108))
#define P_PBTIMER_2_CONTROL ((volatile Uint8 *)(0xFFBE8118))
#define P_PBTIMER_3_CONTROL ((volatile Uint8 *)(0xFFBE8128))

// PB Timer Control Register Commands
#define PBTIMER_EN  (0x01)

// 22 bit max for each timer
#define PBTIMER_MAX (0x003fffff)

#endif // ifndef PbTimer_HH
