#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ServiceInitializationSubScreen - Activated by checking
// GuiApp::IsServiceInitialStateOK() status.  If at startup, the system
// failed any one of the following criteria then this subscreen will be
// activated depending the failing factors.  The list below are the criteria
// we checked: IsFlowSensorInfoRequired, IsBdAndGuiFlashTheSame,
// AreSerialNumberMatches, and IsBdVentInopTestInProgress.
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen is activated by checking GuiApp::IsServiceInitialStateOK()
// status.  Only one instance of this class is created by LowerSubScreenArea.
// The interface is pretty generic, as a SubScreen it defines the activate()
// and deactivate() methods and then automatically starts/ends the test.
//---------------------------------------------------------------------
//@ Rationale
// Groups the components of the ServiceInitialization subscreen in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The processing of the ServiceInitialization is based on a run-time-defined
// internal table.  Upon the activation of this subscreen, we need to set the 
// appropriated test table based on the system startup criteria.  The test
// shall be executed automatically based on the sequence established on the
// internal table.  The subsequent test flow shall be controled by keyboard
// input, and test control input until all tests in the internal table are
// exhausted.  The test prompts, status and errors are displayed to communicate
// the test information to the operator.
// During Gui and Bd vent inop tests, the testing status will be updated to
// VentTestSummarySubScreen.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only be
// displayed in the LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceInitializationSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date:  26-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
// 		Removed SUN prototype code.
//
//  Revision: 010   By: hhd    Date:  30-Mar-2000    DCS Number: 5675
//  Project: Neo-Mode
//  Description:
// 		Modified to display Serial Number Setup Subscreen when data key is missing.
//
//  Revision: 009   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 008  By:  syw    Date: 23-Nov-1998    DR Number: DCS 5218
//       Project:  BiLevel
//			Added GuiApp::IsSetFlashSerialNumber() condition in nextTest_().
//
//  Revision: 007  By:  syw    Date: 27-Jul-1998    DR Number: DCS 5082
//       Project:  Sigma (840)
//       Description:
//          Added FS_NOT_COMPATIBLE_WITH_SOFTWARE to conditionText_[] and centered
//			text position.
//
//  Revision: 006  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 005  By:  yyy    Date:  08-Oct-1997    DR Number:   1865
//    Project:  Sigma (R8027)
//    Description:
//      Vent inop test cleanup.
//		08-Oct-1997 Continued working on this DCS.
//
//  Revision: 004  By:  yyy    Date:  26-SEP-1997    DR Number:   1861
//    Project:  Sigma (R8027)
//    Description:
//      Updated access method for vent inop test due to changes in service
//		mode.
//
//  Revision: 003  By: gdc      Date: 23-Sep-1997  DR Number: 2513
//    Project:  Sigma (R8027)
//    Description:
//     Cleaned up Service-Mode interfaces to support remote test.
//
//  Revision: 002  By: yyy      Date: 04-Aug-1997  DR Number: 2312
//    Project:  Sigma (R8027)
//    Description:
//      Used INIT_PROMPT_ACCEPT_CLEAR_CANCEL_S.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ServiceInitializationSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "PromptStrs.hh"
#include "SmTestId.hh"
#include "ServiceUpperScreen.hh"
#include "ServiceUpperScreenSelectArea.hh"
#include "ServiceLowerScreen.hh"
#include "LowerSubScreenArea.hh"
#include "ServiceLowerScreenSelectArea.hh"
#include "ServiceDataRegistrar.hh"
#include "SerialNumSetupSubScreen.hh"
#include "UpperScreen.hh"

#if defined FAKE_SM
#	include "FakeServiceModeManager.hh"
#   define SmManager FakeServiceModeManager
#else		
#	include "SmManager.hh"
#endif	// FAKE_SM

#include "AdjustPanel.hh"
#include "PromptArea.hh"
#include "SubScreenArea.hh"
#include "Sound.hh"
#include "Colors.hh"
#include "SoftwareOptions.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 STATUS_LABEL_X_ = 379;
static const Int32 STATUS_LABEL_Y_ = 0;
static const Int32 STATUS_WIDTH_ = 255;
static const Int32 STATUS_HEIGHT_ = 31;
static const Int32 ERR_DISPLAY_AREA_X1_ = 200;
static const Int32 ERR_DISPLAY_AREA_Y1_ = 100;
static const Int32 ERR_DISPLAY_ROW_WIDTH_ = 25;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceInitializationSubScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the ServiceInitialization Subscreen.  The given `pSubScreenArea' is 
// the SubScreenArea in which it will be displayed (in this case the upper 
// subscreen area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Creates, positions, and initializes all the graphical components of this
// subscreen:  title and a number of static and dynamic textual items.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceInitializationSubScreen::ServiceInitializationSubScreen(SubScreenArea *pSubScreenArea) :
		SubScreen(pSubScreenArea),
		serviceInitializationStatus(STATUS_LABEL_X_, STATUS_LABEL_Y_,
						STATUS_WIDTH_, STATUS_HEIGHT_,
						MiscStrs::EXH_V_CAL_STATUS_LABEL,
						NULL_STRING_ID),
		errorHappened_(FALSE),
		errorIndex_(0),
		promptId_(-1),
		keyAllowedId_(SmPromptId::NULL_KEYS_ALLOWED_ID),
		userKeyPressedId_(SmPromptId::NULL_ACTION_ID),
		testManager_(this),
		titleArea_(MiscStrs::FLOW_SENSOR_CALIB_SUBSCREEN_TITLE),
		maxServiceModeTest_(0)
{
	CALL_TRACE("ServiceInitializationSubScreen::ServiceInitializationSubScreen(pSubScreenArea)");


#if defined FORNOW
printf("ServiceInitializationSubScreen::ServiceInitializationSubScreen() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW
	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	for (int i=0; i<MAX_CONDITION_ID; i++)
	{
		errDisplayArea_[i].setColor(Colors::WHITE);
		errDisplayArea_[i].setY(ERR_DISPLAY_AREA_Y1_+(i*ERR_DISPLAY_ROW_WIDTH_));
		errDisplayArea_[i].setX(ERR_DISPLAY_AREA_X1_);

		// Hide the following drawables
		errDisplayArea_[i].setShow(FALSE);

		// Add the following drawables
		addDrawable(&errDisplayArea_[i]);
	}

	// Add the title area
	addDrawable(&titleArea_);
	
	// Add status line text objects to main container
	addDrawable(&serviceInitializationStatus);

	setTestPromptTable_();	

	// Associate the testResults object with the pointer to the test result
	// array.
	testManager_.setupTestResultDataArray();
	testManager_.setupTestCommandArray();
										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceInitializationSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys the ServiceInitialization subscreen.  Needs to do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceInitializationSubScreen::~ServiceInitializationSubScreen(void)
{
	CALL_TRACE("ServiceInitializationSubScreen::~ServiceInitializationSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called by ServiceLowerScreen just before this subscreen is displayed
// on the screen.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The first thing is to blank the LowerScreenSelectArea tab buttons.
// Followed by registering all general possible test results for test events.
// Then set the appropriate test table and start the testing.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::activate(void)
{
	CALL_TRACE("ServiceInitializationSubScreen::activate(void)");


#if defined FORNOW
printf("ServiceInitializationSubScreen::activate() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Hide the lower screen select area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);
	//
	// Register all general possible test results for test events, but first
	// we have to initialize ServiceDataRegistrar.
	//
	testManager_.registerTestResultData();
	testManager_.registerTestCommands();

	// Let the service mode  manager knows the type of Service mode.
	SmManager::DoFunction(SmTestId::SET_TEST_TYPE_MISC_ID);

	// Set the appropriated test table.
	setTestTable_();
	
	nextTest_();
										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called at the end of ServiceInitialization test to deactivate this
// subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Release the AdjustPanel target.  Dismiss all prompts.  Show the upper and
// lower screen select tab buttons. And deactivate this subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::deactivate(void)
{
	CALL_TRACE("ServiceInitializationSubScreen::deactivate(void)");


#if defined FORNOW
printf("ServiceInitializationSubScreen::deactivate() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompt
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);

	// Show the upper screen select area.
	ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->setBlank(FALSE);

	// Show the lower screen select area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);
										// $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator release the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the ServiceInitialization
// process.  
//---------------------------------------------------------------------
//@ Implementation-Description
// Invoke the test manager's method to restore the prompts displayed before
// the focus is taken.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("ServiceInitializationSubScreen::adjustPanelRestoreFocusHappened(void)");


#if defined FORNOW
printf("ServiceInitializationSubScreen::adjustPanelRestoreFocusHappened() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	testManager_.restoreTestPrompt();
										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Accept key to signal the
// accepting of continuing the ServiceInitialization process. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply clear the Adjust Panel focus and associated prompts, and inform
// the Service Test Manager of the userKeyPressedId_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("ServiceInitializationSubScreen::adjustPanelAcceptPressHappened(void)");

#if defined FORNOW
printf("adjustPanelAcceptPressHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	if (keyAllowedId_ == SmPromptId::ACCEPT_AND_CANCEL_ONLY
		|| keyAllowedId_ == SmPromptId::ACCEPT_KEY_ONLY
	   )
	{		// $[TI1]
		// Service mode had request either an ACCEPT or a CLEAR prompt action.
		// Make sure that nothing in this subscreen keeps focus.
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Clear the Primary prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
					 PromptArea::PA_HIGH, NULL_STRING_ID);
			
		// Clear the Advisory prompts
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
					 PromptArea::PA_HIGH, NULL_STRING_ID);

		// Clear the Secondary prompt
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_HIGH, NULL_STRING_ID);


		// Reset the keyAllowedId_
		keyAllowedId_ = SmPromptId::NULL_KEYS_ALLOWED_ID;
		userKeyPressedId_ = SmPromptId::KEY_ACCEPT;

		// Set all prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::EST_TESTING_P);

		// Signal the Service Test Manager to continue the test.
#ifndef FAKE_SM
		if (userKeyPressedId_ != SmPromptId::NULL_ACTION_ID)
#endif	// !FAKE_SM
		{	// $[TI2]
			SmManager::OperatorAction(serviceInitalTestId_[currentTestId_],
						  userKeyPressedId_);
		}	// $[TI3]
	}		// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses the Clear key. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply clear the Adjust Panel focus and associated prompts, and inform
// the Service Test Manager of the userKeyPressedId_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("ServiceInitializationSubScreen::adjustPanelClearPressHappened(void)");

#if defined FORNOW
printf("adjustPanelClearPressHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	if (keyAllowedId_ == SmPromptId::ACCEPT_AND_CANCEL_ONLY)
	{		// $[TI1]
		// Service mode had request a CLEAR prompt action.
		// Make sure that nothing in this subscreen keeps focus.
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Clear the Primary prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
					 PromptArea::PA_HIGH, NULL_STRING_ID);
			
		// Clear the Advisory prompts
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
					 PromptArea::PA_HIGH, NULL_STRING_ID);

		// Clear the Secondary prompt
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_HIGH, NULL_STRING_ID);

		// Reset the keyAllowedId_
		keyAllowedId_ = SmPromptId::NULL_KEYS_ALLOWED_ID;
		userKeyPressedId_ = SmPromptId::KEY_CLEAR;

		// Set all prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::EST_TESTING_P);

		// Signal the Service Test Manager to continue the test.
#ifndef FAKE_SM
		if (userKeyPressedId_ != SmPromptId::NULL_ACTION_ID)
#endif	// !FAKE_SM
		{		// $[TI2]
			SmManager::OperatorAction(serviceInitalTestId_[currentTestId_],
						  userKeyPressedId_);
		}		// $[TI3]

	}		// $[TI4]

}


//============================ m e t h o d   d e s c r i p t i o n ====
//@ Method: processTestPrompt
//
//@ Interface-Description
//  Called when we are informed of a change in test prompts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First, we grab the Adjust Panel focus, then we update the prompt area
//  and the status area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("ServiceInitializationSubScreen::processTestPrompt(Int command)");

	// Remember the most recent prompt ID
	promptId_ = command;
	
	// Search the corresponding prompt string according to the promptId_

	Int idx = 0;
	for (idx = 0;
			 idx < NUM_TEST_PROMPT_MAX && testPromptId_[idx] != promptId_;
			 idx++);
	
	SAFE_CLASS_ASSERTION(idx < NUM_TEST_PROMPT_MAX);

#if defined FORNOW
printf("ServiceInitializationSubScreen::processTestPrompt() clear advisory prompt\n");
#endif	// FORNOW

	// Clear advisory prompt
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
						NULL_STRING_ID);

	switch (keyAllowedId_)
	{
	case SmPromptId::ACCEPT_AND_CANCEL_ONLY:	// $[TI1]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Set the Secondary prompt to "When done: press Accept, to skip test: press Clear."
		// "To cancel EST: touch EXIT EXT, then Accept."
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
						PromptStrs::INIT_PROMPT_ACCEPT_CLEAR_CANCEL_S);
		break;

	case SmPromptId::NO_KEYS:					// $[TI2]
		// do nothing
		break;
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

	// The primary is set after the call to the
	// AdjustPanel::TakeNonPersistentFocus(this, TRUE)
	// to remove any previous keyboard set adjustPanel focus.
	//
	// Set primary prompt according to the promptId_
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
							testPromptName_[idx]);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//  Process keyAllowed command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply remember the expected user key response ID.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::processTestKeyAllowed(Int keyAllowed)
{
	CALL_TRACE("ServiceInitializationSubScreen::processTestKeyAllowed(Int command)");

	// Remember the expected user key response ID
	keyAllowedId_ = (SmPromptId::KeysAllowedId) keyAllowed;
											// $[TI1]	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//  Process the test result status given by Service Mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dispatch the given command and make the corresponding action,
//  for TEST_END: reset the test error status tabel, and start next test.
//  for TEST_ALERT or TEST_FAILURE: flag the errorHappened_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("ServiceInitializationSubScreen::processTestResultStatus_"
								"(Int resultStatus Int testResultId)");


#if defined FORNOW
printf("ServiceInitializationSubScreen::processTestResultStatus() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	switch (resultStatus)
	{
	case SmStatusId::TEST_END:			// $[TI1]
		setTestResultStatus_();
		nextTest_();
		break;

	case SmStatusId::TEST_START:		// $[TI2]
		break;

	case SmStatusId::TEST_ALERT:		// $[TI3]
	case SmStatusId::TEST_FAILURE:		// $[TI4]
		errorHappened_ = TRUE;
		break;

	case SmStatusId::NOT_INSTALLED:		
		// Do nothing FORNOW
		break;

	default:
		SAFE_CLASS_ASSERTION(FALSE);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//  Process test result condition passed by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Display the text for the corresponding result condition on the screen
//  and update the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//  Result condition has to be in the range [0, MAX_CONDITION_ID]
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("ServiceInitializationSubScreen::processTestResultCondition(Int resultCondition)");


#if defined FORNOW
printf("ServiceInitializationSubScreen::processTestResultCondition() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	SAFE_CLASS_ASSERTION((resultCondition >= 0) && (resultCondition <= MAX_CONDITION_ID));

	if (resultCondition > 0)
	{			// $[TI1]
		errDisplayArea_[errorIndex_].setText(conditionText_[resultCondition-1]);
		errDisplayArea_[errorIndex_].setShow(TRUE);
		errorIndex_++;

		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, NULL_STRING_ID);

		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, PromptStrs::VENT_INOP_SEE_OP_MANUAL_S);
	}			// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  Process data given by Service-Mode
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this subscreen is designed not to display test data,
//  this remains an empty method
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("ServiceInitializationSubScreen::processTestData(Int dataIndex, TestResult *pResult)");
#if defined FORNOW
printf("ServiceInitializationSubScreen::processTestData() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Empty method
	// Exhalation Calibration SubScreen doesn't display test data
	SAFE_CLASS_ASSERTION(FALSE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTestTable_
//
//@ Interface-Description
//  Initialize the serviceInitalTestId, and serviceInitalTestName_ tables.
//  The serviceInitalTestId array contains Service-Initialization test ids.
//  The serviceInitalTestName_ table contains translation text of the test Name.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables based
//  on the serviceInitialization criteria.  The following are the sequence
//  we are checking to determine if the test is needed to run or not.
//  TEST ID                           TEST CRITERIA
//  GUI_VENT_INOP_ID                  IsGuiVentInopTestInProgress() 
//  BD_VENT_INOP_ID                   IsBdVentInopTestInProgress() 
//  INITIALIZE_FLOW_SENSORS_ID        IsFlowSensorInfoRequired() 
//  CAL_INFO_DUPLICATION_ID           IsBdAndGuiFlashTheSame() 
// $[07057] Entering service mode shall cause the ventilator to update ...
// $[07059] After the Accept key is pressed to start the Force Safe test ...
// $[07064] Entering service mode shall cause the ventilator to display ...
// $[07067] Entering Service mode shall cause the ventilator to display ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::setTestTable_(void)
{
	Int testIndex = 0;
	Boolean buildFlashTable = FALSE;
	Boolean ventInopTesting = FALSE;
	
	CALL_TRACE("ServiceInitializationSubScreen::setTestTable_(void)");


#if defined FORNOW
printf("ServiceInitializationSubScreen::setTestTable_() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// First clear each entry in the array.
	for (Int32 i = 0; i < INITIAL_TEST_MAX; i++)
	{
		serviceInitalTestId_[i]	= SmTestId::TEST_NOT_START_ID;
		serviceInitalTestName_[i] = NULL_STRING_ID;
	}

	if (GuiApp::IsGuiVentInopTestInProgress())
	{		// $[TI1]
#if defined FORNOW
printf("GuiApp::IsGuiVentInopTestInProgress() = %d\n", GuiApp::IsGuiVentInopTestInProgress());
#endif	// FORNOW

		// Setup service test id and corresponding test title.
		serviceInitalTestId_[testIndex]			= SmTestId::GUI_VENT_INOP_ID;
		serviceInitalTestName_[testIndex++]		= MiscStrs::VENT_INOP_TEST_SUBSCREEN_TITLE;

		// Setup appropriated test status.
		ventInopTesting = TRUE;

	}

	if (GuiApp::IsBdVentInopTestInProgress())
	{		// $[TI2]
#if defined FORNOW
printf("GuiApp::IsBdVentInopTestInProgress() = %d\n", GuiApp::IsBdVentInopTestInProgress());
#endif	// FORNOW

		// Setup service test id and corresponding test title.
		serviceInitalTestId_[testIndex]			= SmTestId::BD_VENT_INOP_ID;
		serviceInitalTestName_[testIndex++]		= MiscStrs::VENT_INOP_TEST_SUBSCREEN_TITLE;

		// Setup appropriated test status.
		ventInopTesting = TRUE;
	}

	if (GuiApp::IsFlowSensorInfoRequired())
	{		// $[TI3]
#if defined FORNOW
printf("GuiApp::IsFlowSensorInfoRequired() = %d\n", GuiApp::IsFlowSensorInfoRequired());
#endif	// FORNOW

		// Setup service test id and corresponding test title.
		serviceInitalTestId_[testIndex]			= SmTestId::INITIALIZE_FLOW_SENSORS_ID;
		serviceInitalTestName_[testIndex++]		= MiscStrs::FLOW_SENSOR_CALIB_SUBSCREEN_TITLE;

		// Setup appropriated test status.
		buildFlashTable = TRUE;
	}

	// If INITIALIZE_FLOW_SENSORS_ID is required test, then
	// CAL_INFO_DUPLICATION_ID has to be the next test followed it,
	// to make sure the flash table will be transferred over from
	// BD to GUI board.
	if (!GuiApp::IsBdAndGuiFlashTheSame() || buildFlashTable)
	{		// $[TI4,5]
#if defined FORNOW
printf("GuiApp::IsBdAndGuiFlashTheSame() = %d\n", GuiApp::IsBdAndGuiFlashTheSame());
#endif	// FORNOW

		// Setup service test id and corresponding test title.
		serviceInitalTestId_[testIndex]			= SmTestId::CAL_INFO_DUPLICATION_ID;
		serviceInitalTestName_[testIndex++]		= MiscStrs::CAL_INFO_DUP_SUBSCREEN_TITLE;

		// Setup appropriated test status.
		buildFlashTable = TRUE;
	}		// $[TI6]

	maxServiceModeTest_ = testIndex;

#ifdef SIGMA_DEVELOPMENT

	// Make sure total test count is within the maximum allowed.
	SAFE_CLASS_ASSERTION(maxServiceModeTest_ <= INITIAL_TEST_MAX);

	// Do some safe assertions that all entries in the array have been filled
	for (i = 0; i < maxServiceModeTest_; i++)
	{
		SAFE_CLASS_ASSERTION(serviceInitalTestId_[i]	!= SmTestId::TEST_NOT_START_ID);
		SAFE_CLASS_ASSERTION(serviceInitalTestName_[i] 	!= NULL_STRING_ID);
	}
#endif // SIGMA_DEVELOPMENT

	
	if (!buildFlashTable && !ventInopTesting)
	{		// $[TI7]
		// Check if Serial number subscreen is required.
		if (GuiApp::SERIAL_NUMBER_OK == GuiApp::GetSerialNumberInfoState())
		{		// $[TI8]
#if defined FORNOW
printf("GuiApp::GetSerialNumberInfoState() = %d\n", GuiApp::GetSerialNumberInfoState());
#endif	// FORNOW

			// The reason we visited this subscreen is based on 
			// GuiApp::IsServiceInitialStateOK().
			// An assertion has to be generated if we can't find any test that
			// failed the initial state.
			CLASS_ASSERTION_FAILURE();
		}		// $[TI9]
	}		// $[TI10]

	// Setup the first test id.
	currentTestId_ = INITIAL_NO_TEST_ID;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTestPromptTable_
//
//@ Interface-Description
//  Initialize the testPromptId_, and testPromptName_ tables.  
//  The testPromptId_ array contains ServiceInitialization prompt ids.
//  The testPromptName_ table contains translation text of 
//	ServiceInitialization prompt code.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::setTestPromptTable_(void)
{
	CALL_TRACE("ServiceInitializationSubScreen::setTestPromptTable_(void)");

	// First clear each entry in the array.
	for (Int32 i = 0; i < NUM_TEST_PROMPT_MAX; i++)
	{
		testPromptId_[i]	= SmPromptId::NULL_PROMPT_ID;
		testPromptName_[i] 	= NULL_STRING_ID;
	}

	Int testIndex = 0;
	
	testPromptId_[testIndex]	= SmPromptId::POWER_DOWN_PROMPT;
	testPromptName_[testIndex++] = PromptStrs::SM_POWER_DOWN_P;

	testPromptId_[testIndex]	= SmPromptId::GUI_SIDE_TEST_PROMPT;
	testPromptName_[testIndex++] = PromptStrs::SM_GUI_SIDE_TEST_P;

	testPromptId_[testIndex]	= SmPromptId::VENT_INOP_A_TEST_PROMPT;
	testPromptName_[testIndex++] = PromptStrs::SM_VENT_INOP_A_TEST_P;
	
	testPromptId_[testIndex]	= SmPromptId::VENT_INOP_B_TEST_PROMPT;
	testPromptName_[testIndex++] = PromptStrs::SM_VENT_INOP_B_TEST_P;

	testPromptId_[testIndex]	= SmPromptId::TEN_SEC_A_TEST_PROMPT;
	testPromptName_[testIndex++] = PromptStrs::SM_TEN_SEC_A_TEST_P;

	testPromptId_[testIndex]	= SmPromptId::TEN_SEC_B_TEST_PROMPT;
	testPromptName_[testIndex++] = PromptStrs::SM_TEN_SEC_B_TEST_P;

	testPromptId_[testIndex]	= SmPromptId::SOUND_PROMPT;
	testPromptName_[testIndex++] = PromptStrs::SM_SOUND_P;

	testPromptId_[testIndex]	= SmPromptId::VENTILATOR_INOPERATIVE_PROMPT;
	testPromptName_[testIndex++] = PromptStrs::SM_VENTILATOR_INOPERATIVE_P;

	testPromptId_[testIndex]	= SmPromptId::SAFETY_VALVE_OPEN_PROMPT;
	testPromptName_[testIndex++] = PromptStrs::SM_SAFETY_VALVE_OPEN_P;

#ifdef SIGMA_DEVELOPMENT

#if defined FORNOW
printf("setTestPromptTable_ at line  %d, testIndex= %d, NUM_TEST_PROMPT_MAX = %d\n",
				 __LINE__, testIndex, NUM_TEST_PROMPT_MAX);
#endif	// FORNOW

	// Make sure total test count is within the maximum allowed.
	SAFE_CLASS_ASSERTION(testIndex == NUM_TEST_PROMPT_MAX);

	// Do some safe assertions that all entries in the array have been filled
	for (i = 0; i < NUM_TEST_PROMPT_MAX; i++)
	{
		SAFE_CLASS_ASSERTION(testPromptId_[i] != SmPromptId::NULL_PROMPT_ID);
		SAFE_CLASS_ASSERTION(testPromptName_[i] != NULL_STRING_ID);
	}
#endif // SIGMA_DEVELOPMENT
										// $[TI1]
}

		
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setErrorTable_
//
//@ Interface-Description
// 	Sets the error text in the error table.  Any condition that arises
//	has a unique id which is used as an index into this table of condition
//	text.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assign condition text (non-cheap text) to error table.  Different 
//	conditions will arise depending on the current running test ID.
//	Therefore, different sets of error conditions are used according
//  to which test is in progress.
//	We also initialize the error display area's show flag.
// $[07060] The Force Safe State Test subscreen shall provide a display ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::setErrorTable_(void)
{
	Int testIndex = 0;
	
	CALL_TRACE("ServiceInitializationSubScreen::setErrorTable_(void)");


#if defined FORNOW
printf("ServiceInitializationSubScreen::setErrorTable_() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Reset status flag
	errorHappened_ = 0;
	errorIndex_ = 0;

	testIndex = 0;

	if (serviceInitalTestId_[currentTestId_] ==SmTestId::INITIALIZE_FLOW_SENSORS_ID)
	{							// $[TI1]
		conditionText_[testIndex++] = MiscStrs::FS_UNABLE_TO_READ_AIR_FLOW_SENSOR;
		conditionText_[testIndex++] = MiscStrs::FS_UNABLE_TO_READ_O2_FLOW_SENSOR;
		conditionText_[testIndex++] = MiscStrs::FS_UNABLE_TO_READ_AIR_TABLE;
		conditionText_[testIndex++] = MiscStrs::FS_UNABLE_TO_READ_O2_TABLE;
		conditionText_[testIndex++] = MiscStrs::FS_UNABLE_TO_PROGRAM_FLASH;
		conditionText_[testIndex]   = MiscStrs::FS_NOT_COMPATIBLE_WITH_SOFTWARE;
	}
	else if (serviceInitalTestId_[currentTestId_] ==SmTestId::CAL_INFO_DUPLICATION_ID)
	{							// $[TI2]
		conditionText_[testIndex++] = MiscStrs::CAL_INFO_DUP_ERROR_RECEIVE_FLASH_DATA;
		conditionText_[testIndex]   = MiscStrs::CAL_INFO_DUP_ERROR_BURNING_FLASH;
	}
	else if (serviceInitalTestId_[currentTestId_] ==SmTestId::GUI_VENT_INOP_ID)
	{							// $[TI3]
	}
	else if (serviceInitalTestId_[currentTestId_] ==SmTestId::BD_VENT_INOP_ID)
	{							// $[TI4]
		conditionText_[testIndex] = MiscStrs::BD_VENT_INOP_CONDITION1;
	}
	else
	{
		CLASS_ASSERTION_FAILURE();
	}

	// Make sure total test count is within the maximum allowed.
	CLASS_ASSERTION(testIndex <= MAX_CONDITION_ID);

	for (int i=0; i < MAX_CONDITION_ID; i++)
	{	
		errDisplayArea_[i].setShow(FALSE);
	}	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTest_
//
//@ Interface-Description
//	Preparation step for a test to start.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the condition text (error) table.  Display prompts and status.
//	Finally, tell Service Mode Manager to do the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::startTest_(Int currentTestId)
{
	CALL_TRACE("ServiceInitializationSubScreen::startTest_(Int currentTestId)");

#if defined FORNOW
printf("startTest_, currentTestId = %d at line  %d\n", currentTestId, __LINE__);
#endif	// FORNOW

	currentTestId_ = currentTestId;

	// Set the appropriated error message table.
	setErrorTable_();
	
	// Set all prompts
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EST_TESTING_P);

	// Display the overall testing status on the service status area
	serviceInitializationStatus.setStatus(MiscStrs::EXH_V_CAL_RUNNING_MSG);

	// Setup for transfer flash calibration information to GUI board.
	titleArea_.setTitle(serviceInitalTestName_[currentTestId_]);

	// Start the test.
	SmManager::DoTest(serviceInitalTestId_[currentTestId_]);

	if (serviceInitalTestId_[currentTestId_] == (Int) SmTestId::GUI_VENT_INOP_ID ||
		serviceInitalTestId_[currentTestId_] == (Int) SmTestId::BD_VENT_INOP_ID)
	{								// $[TI1.1]
		UpperScreen::RUpperScreen.getUpperSubScreenArea()->updateVentTestSummaryScreen();
	}								// $[TI1.2]
}

		
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTestResultStatus_
//
//@ Interface-Description
//  Set the appropriate test status flags to enforce the consistent of the
//  test condition.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Based on the test results we either set or reset the appropriate
//  status flag.
// $[07060] The Force Safe State Test subscreen shall provide a display ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::setTestResultStatus_(void)
{
	CALL_TRACE("ServiceInitializationSubScreen::setTestResultStatus_(void)");

#if defined FORNOW
printf("setTestResultStatus_ at line  %d\n", __LINE__);
#endif	// FORNOW

	if (serviceInitalTestId_[currentTestId_] == SmTestId::INITIALIZE_FLOW_SENSORS_ID)
	{		// $[TI1]
		if (errorHappened_)
		{		// $[TI2]
			// Update the ServiceInitializationInfoRequiredFlag.
			GuiApp::SetFlowSensorInfoRequiredFlag(TRUE);

			serviceInitializationStatus.setStatus(MiscStrs::EXH_V_CAL_FAILED_MSG);
		}
		else
		{		// $[TI3]
			// Update the ServiceInitializationInfoRequiredFlag.
			GuiApp::SetFlowSensorInfoRequiredFlag(FALSE);
		}
	}
	else if (serviceInitalTestId_[currentTestId_] == SmTestId::CAL_INFO_DUPLICATION_ID)
	{		// $[TI4]
		if (errorHappened_)
		{	// $[TI5]
			// Update the BdAndGuiFlashTheSame_.
			GuiApp::SetBdAndGuiFlash(FALSE);

			serviceInitializationStatus.setStatus(MiscStrs::CAL_INFO_DUP_FAILED_MSG);
		}
		else
		{	// $[TI6]	
			// Update the BdAndGuiFlashTheSame_.
			GuiApp::SetBdAndGuiFlash(TRUE);
		}
	}
	else if (serviceInitalTestId_[currentTestId_] == SmTestId::GUI_VENT_INOP_ID)
	{		// $[TI7]
		if (errorHappened_)
		{	// $[TI8]	
			serviceInitializationStatus.setStatus(MiscStrs::VENT_INOP_FAILED_MSG);
			GuiApp::SetVentInopMajorFailureFlag(TRUE);
			UpperScreen::RUpperScreen.getUpperSubScreenArea()->updateVentTestSummaryScreen();
		}
		else
		{	// $[TI9]
			// Update the BdAndGuiFlashTheSame_.
			GuiApp::SetGuiVentInopTestInProgressFlag(FALSE);
		}
	}
	else if (serviceInitalTestId_[currentTestId_] == SmTestId::BD_VENT_INOP_ID)
	{		// $[TI10]
		if (errorHappened_)
		{	// $[TI11]
			serviceInitializationStatus.setStatus(MiscStrs::VENT_INOP_FAILED_MSG);
			GuiApp::SetVentInopMajorFailureFlag(TRUE);
			UpperScreen::RUpperScreen.getUpperSubScreenArea()->updateVentTestSummaryScreen();
		}	// $[TI12]
	}
	else
	{
		CLASS_ASSERTION_FAILURE();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nextTest_
//
//@ Interface-Description
//	Handles the test and interface control at the end of each 
//	ServiceInitialization test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If test failed then display the appropriate prompt to inform the
//  operator how to proceed from this point.
//  Else, if this is not the last test then we proceed to the next test.
//  If this is the last test, then there are two conditions we need to
//  take of.
//  1. If serial numbers matche then we simply  deactivate this subscreen.
//  2. If serial number do not matche then we activate the serialNumSetupSubScreen
//  to perform the serialNumber test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::nextTest_(void)
{
	CALL_TRACE("ServiceInitializationSubScreen::nextTest_(void)");

#if defined FORNOW
printf("nextTest_ at line  %d, currentTestId_ = %d, maxServiceModeTest_ = %d\n",
					__LINE__, currentTestId_, maxServiceModeTest_);
#endif	// FORNOW

	if (errorHappened_)
	{		// $[TI1]
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::FS_POWER_OFF_TO_RESTART_P);
	}
	else
	{		// $[TI2]
		// Increment to next test Id
		currentTestId_++;

		if (currentTestId_ == maxServiceModeTest_)
		{		// $[TI3]
#if defined FORNOW
printf("GuiApp::IsExhValveCalRequired() = %d\n", GuiApp::IsExhValveCalRequired());
#endif	// FORNOW

			if ( !GuiApp::IsDataKeyInstalled() || (GuiApp::SERIAL_NUMBER_OK != GuiApp::GetSerialNumberInfoState() &&
				SoftwareOptions::GetSoftwareOptions().isSetFlashSerialNumber()))
			{	// $[TI4]
#if defined FORNOW
printf("GuiApp::GetSerialNumberInfoState() = %d\n", GuiApp::GetSerialNumberInfoState());
#endif	// FORNOW
				// Get the Serial number service Lower subScreen area address.
				LowerSubScreenArea *pLowerSubScreenArea =
								ServiceLowerScreen::RServiceLowerScreen.getLowerSubScreenArea();
				ServiceLowerScreenSelectArea *pServiceLowerScreenSelectArea =
								ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea();

				// Activate this subscreen.
				pLowerSubScreenArea->activateSubScreen(
						LowerSubScreenArea::GetSerialNumSetupSubScreen(),
						pServiceLowerScreenSelectArea->getServiceLowerOtherScreensTabButton());
			}
			else
			{	// $[TI5]
				// Deactivate this subscreen.
				getSubScreenArea()->deactivateSubScreen();
			}
		}
		else
		{		// $[TI6]
			// Start of next test.
			startTest_(currentTestId_);
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ServiceInitializationSubScreen::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("ServiceInitializationSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							SERVICEINITIALIZATIONSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}

