#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: RecoverableContext - Context for the Recoverable Settings.
//---------------------------------------------------------------------
//@ Interface-Description
//  A context is a storage pool for different kinds of setting values.  The
//  settings, themselves, do not contain their values, the contexts do.  This
//  context is used to store batch setting values for later recovery.  Only
//  batch setting values are stored in this context, and the methods
//  provided by this context are for the storage (from the Accepted Context)
//  and "recovery" (to the Adjusted Context) of this context's setting values.
//  This context contains setting values that the user accepted just prior
//  to the last Vent-Setup.
//
//  This context is available of the GUI CPU side, only.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for the recoverable context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Privately, this context contains an instance of 'BatchSettingValues'
//  to store all of the values managed by this context.  This values
//  manager has a callback mechanism that allows for the registering of
//  callbacks that are activated upon changes to itself, but this mechanism
//  is NOT used by this class -- no callback notification is needed.
//
//  The Recoverable Context, also, has a boolean flag to represent the
//  validity state of this class.  That validity can be queried and cleared
//  (set to 'FALSE') via methods of this class.  When a Vent-Setup is
//  stored into this context, there is a time limit of which these values
//  can be recovered, also, has a boolean flag to represent the
//  validity state of this class.  Therefore, the ability to "clear" the
//  validity of this class is provided such that a client (e.g., GUI-Apps)
//  of this class can invalidate a recovery when a timer runs out.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/RecoverableContext.ccv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "RecoverableContext.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RecoverableContext()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default accepted settings context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

RecoverableContext::RecoverableContext(void)
				       : validityFlag_(FALSE),
					 batchValues_(NULL)
{
  CALL_TRACE("RecoverableContext()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~RecoverableContext()  [Destructor]
//
//@ Interface-Description
//  Destroy this recoverable settings context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

RecoverableContext::~RecoverableContext(void)
{
  CALL_TRACE("~RecoverableContext()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  storeAcceptedSettings()
//
//@ Interface-Description
//  Stores the accepted settings for later recovery.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (isValid())
//@ End-Method
//=====================================================================

void
RecoverableContext::storeAcceptedSettings(void)
{
  CALL_TRACE("storeAcceptedSettings()");

  // copy ALL of the batch setting values from the accepted context into
  // this context; no callbacks are activated...
  batchValues_ = ContextMgr::GetAcceptedContext().getAcceptedBatchValues();

  // this context is now in a valid state...
  validityFlag_ = TRUE;
}   // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	    		[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
RecoverableContext::SoftFault(const SoftFaultID  softFaultID,
			      const Uint32       lineNumber,
			      const char*        pFileName,
			      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  RECOVERABLE_CONTEXT, lineNumber, pFileName,
			  pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
