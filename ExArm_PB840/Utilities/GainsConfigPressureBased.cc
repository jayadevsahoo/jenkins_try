#include "stdafx.h"
//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsConfigPressureBased.cc
/// @brief Implementation of GainsConfigPressureBased class
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "GainsConfigPressureBased.hh"
#include "PhaseRefs.hh"
#include "PcvPhase.hh"
#include "SigmaTypes.hh"

using namespace GainsManagerNS;

//--------------------------------------------------------------------
// Constructor
//--------------------------------------------------------------------
GainsConfigPressureBased::GainsConfigPressureBased(void)
: m_pbpPtr(0),
  m_breathPhase(NO_PHASE),
  m_gainName( NO_PBP_GAIN )
{

} // Constructor

//--------------------------------------------------------------------
// Constructor
//--------------------------------------------------------------------
GainsConfigPressureBased::GainsConfigPressureBased( GainsNames name)
: GainsConfig( name ),
  m_pbpPtr(0),
  m_breathPhase(NO_PHASE),
  m_gainName( NO_PBP_GAIN )
{

} // Constructor

//--------------------------------------------------------------------
// Copy constructor
//--------------------------------------------------------------------
GainsConfigPressureBased::GainsConfigPressureBased(const GainsConfigPressureBased& rhs )
{
    (*this) = rhs;
} // Copy constructor

//--------------------------------------------------------------------
/// Destructor
//--------------------------------------------------------------------
GainsConfigPressureBased::~GainsConfigPressureBased(void)
{
    // do not call delete on m_pbpPtr. this isn't our object to delete.
} // Destructor

//--------------------------------------------------------------------
// operator=
//--------------------------------------------------------------------
GainsConfigPressureBased& GainsConfigPressureBased::operator=(const GainsConfigPressureBased &rhs)
{
    if( this != &rhs )
    {
        GainsConfig::operator=(rhs);

        m_pbpPtr = rhs.m_pbpPtr;
        m_breathPhase = rhs.m_breathPhase;
        m_gainName = rhs.m_gainName;
    }

	return (*this);
} // operator=

//--------------------------------------------------------------------
// distribute
//--------------------------------------------------------------------
GainsConfig::GainsConfigOpStatus GainsConfigPressureBased::distribute(void)
{
    if( m_pbpPtr == 0 || m_gainName == NO_PBP_GAIN || m_breathPhase == NO_PHASE )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           "distribute", "Object is not initialized properly" );*/
        return GainsConfig::FAILURE;
    }

    if( isChanged() == TRUE )
    {
        setChanged( FALSE );
        switch(m_gainName)
        {
            case PEDIATRIC_CIRCUIT_KP:
                m_pbpPtr->setPediatricKp( getGain() );
                break;
            case ADULT_CIRCUIT_KP:
                m_pbpPtr->setAdultKp( getGain() );
                break;
            case PEDIATRIC_CIRCUIT_KI:
                m_pbpPtr->setPediatricKi( getGain() );
                break;
            case ADULT_CIRCUIT_KI:
                m_pbpPtr->setAdultKi( getGain() );
                break;
            case NO_PBP_GAIN:
            default:
                return GainsConfig::FAILURE;
        }
    }
    return GainsConfig::SUCCESS;
} // set

//--------------------------------------------------------------------
// load
//--------------------------------------------------------------------
GainsConfig::GainsConfigOpStatus GainsConfigPressureBased::load(void)
{
    if( m_pbpPtr == 0 || m_gainName == NO_PBP_GAIN || m_breathPhase == NO_PHASE )
    {
        return GainsConfig::FAILURE;
    }

    switch(m_gainName)
    {
        case PEDIATRIC_CIRCUIT_KP:
            setGain( m_pbpPtr->getPediatricKp() );
            break;
        case ADULT_CIRCUIT_KP:
            setGain(  m_pbpPtr->getAdultKp() );
            break;
        case PEDIATRIC_CIRCUIT_KI:
            setGain(  m_pbpPtr->getPediatricKi() );
            break;
        case ADULT_CIRCUIT_KI:
            setGain(  m_pbpPtr->getAdultKi() );
            break;
        case NO_PBP_GAIN:
        default:
            return GainsConfig::FAILURE;
    }

    return GainsConfig::SUCCESS;
} // load

//--------------------------------------------------------------------
// setup
//--------------------------------------------------------------------
GainsConfig::GainsConfigOpStatus GainsConfigPressureBased::setup(void)
{
    GainsNames nm = getGainName();
    GainsConfigOpStatus ret = GainsConfig::SUCCESS;

    switch( nm )
    {
        case PCV_PED_KP:
            m_gainName = PEDIATRIC_CIRCUIT_KP;
            m_breathPhase = PCV;
            m_pbpPtr = &RPcvPhase;
            break;
        case PCV_ADU_KP:
            m_gainName = ADULT_CIRCUIT_KP;
            m_breathPhase = PCV;
            m_pbpPtr = &RPcvPhase;
            break;
        case PCV_PED_KI:
            m_gainName = PEDIATRIC_CIRCUIT_KI;
            m_breathPhase = PCV;
            m_pbpPtr = &RPcvPhase;
            break;
        case PCV_ADU_KI:
            m_gainName = ADULT_CIRCUIT_KI;
            m_breathPhase = PCV;
            m_pbpPtr = &RPcvPhase;
            break;
        case PEEP_KI:
        case FLOW_A_FACTOR:
        case FLOW_B_FACTOR:
        case FLOW_C_FACTOR:
        case FLOW_D_FACTOR:
        case PRESSURE_KP:
        case PRESSURE_KI:
		case PID_KP:
        case PID_KI:
        case PID_KD:
        default:
            ret = GainsConfig::FAILURE;
            break;
    }

    return (ret);
} // setup

//--------------------------------------------------------------------
// getPhase
//--------------------------------------------------------------------
PressureBasedPhases GainsConfigPressureBased::getPhase(void) const
{
    return m_breathPhase;
} // getPhase

//--------------------------------------------------------------------
// getGainType
//--------------------------------------------------------------------
PressureBasedPhaseGainsNames GainsConfigPressureBased::getGainType(void) const
{
    return m_gainName;
} // getGainType

