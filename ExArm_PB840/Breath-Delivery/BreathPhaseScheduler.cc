#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BreathPhaseScheduler - the base class for all breath phase schedulers
//---------------------------------------------------------------------
//@ Interface-Description
// This is an abstract class with pure virtual methods to implement the
// following functionality: -- A method to determine the next breath phase
// based on the active breath trigger.	-- A method to instruct the scheduler
// to relinquish Control. When a scheduler is instructed to relinquish
// control, by a mode trigger, it performs all neccessary processing to ensure
// a correct transition to the next scheduler. This method determines which is
// the next scheduler. -- A method to instruct the scheduler to take control.
// Once instructed to the take contol, the scheduler registers as the active
// scheduler.	-- A method to signify a change in specific settings. The class
// provides public services to other objects in the form of static methods to
// get the current and the set scheduler, and a non static method to get the
// scheduler id. Static method to initialize the BD sub-system is provided as
// well as a static method to report the occurence of a user event.
//---------------------------------------------------------------------
//@ Rationale
//	There are several pre-defined breath scheduling modes built into
//	Sigma. For example, A/C mode, SIMV mode, etc,. The set of specific
//	scheduling rules for each of these modes is captured by the classes derived
//	from BreathPhaseScheduler. The BreathPhaseScheduler provides a common
//	interface to all derived schedulers. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The class provides access (get and set methods) to the current breath
// scheduler which is stored as a static pointer to BreathPhaseScheduler
// object. The pointer to the current scheduler is initialized to NULL (compile time).
// On system initialization it is initialized to point to the power-up
// scheduler. The method that returns the set scheduler does it by checking for
// the current setting value of mode and returns a pointer to the matching
// scheduler. The enumeration for scheduler id is defined by this class.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// This is an abstract base class and can not be instanciated.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathPhaseScheduler.ccv   25.0.4.0   19 Nov 2013 13:59:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 033   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 032   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 031   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 030   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added methods to accept and reject RM maneuvers. 
//
//  Revision: 029  By: yyy     Date:  22-Jun-1999    DR Number: 5442
//  Project:  ATC
//  Description:
//      Used the PendingContextHandle to get mode value instead of
//		PhasedContextHandle to sync with the other parameters checking.
//
//  Revision: 028  By: sah     Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//      Added in new 'PEEP_HIGH_TIME' setting.
//
//  Revision: 027  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 026  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added code to manage inspiratory pause event.
//             Added methods: RejectInspiratoryPause_(), AcceptInspiratoryPause_().
//
//  Revision: 025  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 024  By:  iv    Date:  03-Apr-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per unit test peer review:
//             Changed TI numbers to avoid duplicate items.
//             Rework per formal code review for BD Schedulers. 
//
//  Revision: 023  By:  iv    Date:  27-Mar-1997    DR Number: 1876
//       Project:  Sigma (840)
//       Description:
//             Removed non used unit test methods.
//
//  Revision: 022  By:  iv    Date:  28-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Fixed unit test per formal code review.
//
//  Revision: 021  By:  iv    Date:  21-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 020  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 019 By:  iv   Date:   06-Feb-1997    DR Number: DCS 1737 
//       Project:  Sigma (840)
//       Description:
//             Added initialization to new static member: IsStandbyRequired.
//
//  Revision: 018  By:  iv    Date:  23-Jan-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 017  By:  iv    Date:  18-Dec-1996    DR Number: DCS 1632
//       Project:  Sigma (R8027)
//       Description:
//             Cancel pause in method RejectExpiratoryPause_.
//
//  Revision: 016  By:  iv    Date:  04-Dec-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Cleanup for code inspections.
//
//  Revision: 015  By:  iv    Date:  17-Nov-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed ReportEventStatus():
//             Eliminated the call to Fio2Monitor::SetAlarmResetStatus().
//             Moved it to UserEvent.cc.
//             Eliminated the call to VentStatus to handle GUI fault.
//
//  Revision: 014  By:  iv    Date:  08-Oct-1996    DR Number: DCS 1395
//       Project:  Sigma (R8027)
//       Description:
//             Added Gui Fault event in ReportEventStatus
//
//  Revision: 013  By:  iv    Date:  07-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added in ReportEventStatus, a call:
//                  Fio2Monitor::SetAlarmResetStatus();
//
//  Revision: 012  By:  iv    Date:  04-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changes for unit test.
//
//  Revision: 011 By:  iv   Date:   13-Aug-1996    DR Number: DCS 1218
//       Project:  Sigma (R8027)
//       Description:
//		       Changed the range of SettingId values as checked in a setting change
//             call back assertion.
//
//  Revision: 010 By:  iv   Date:   16-Jul-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//		       Added a call rPressureController.setInitIntegratorValue(0.0)
//             in takeControl().
//
//  Revision: 009 By:  iv   Date:   02-Jul-1996    DR Number: DCS 1110, 1058
//       Project:  Sigma (R8027)
//       Description:
//		       Set BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::OFF
//             in takeControl().
//
//  Revision: 008 By:  iv   Date:   05-Jun-1996    DR Number: DCS 713, 967 
//       Project:  Sigma (R8027)
//       Description:
//             Modified takeControl() to handle apnea active and safety pcv active flags.
//             Handle rate changes only if peep recovery is NOT in process.
//
//  Revision: 007 By:  iv   Date:   07-May-1996    DR Number: DCS 967 
//       Project:  Sigma (R8027)
//       Description:
//             Qualify rate changes with the state of PeepRecovery_.
//
//  Revision: 006 By:  iv   Date:   18-Apr-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Added a method void getId(char *, SchedulerId::SchedulerIdValue)
//             for development only.
//
//  Revision: 005 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 004  By:  iv   Date:  13-Dec-1995    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Changed the method that checks if apnea is possible from protected to public.
//
//  Revision: 003  By:  kam   Date:  07-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed takeControl() to be a virtual vs. a pure virtual method so
//             that common functionality can be added to the base class takeControl();
//             all schedulers invoke the base class takeControl() method from their
//             takeControl() method.
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BreathPhaseScheduler.hh"
#include "SchedulerRefs.hh"
#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "BreathMiscRefs.hh"
#include "ControllersRefs.hh"
#include "ManeuverRefs.hh"
#include "BdDiscreteValues.hh"

//@ Usage-Classes
#include "ModeTriggerMediator.hh"
#include "Trigger.hh"
#include "SmSwitchConfirmation.hh"
#include "PressureController.hh"
#include "BdSystemStateHandler.hh"
#include "BreathPhase.hh"
#include "PhasedInContextHandle.hh"
#include "PendingContextHandle.hh"
#include "InspPauseManeuver.hh"
#include "ExpPauseManeuver.hh"
#include "NifManeuver.hh"
#include "P100Maneuver.hh"
#include "VitalCapacityManeuver.hh"
#include "SettingConstants.hh"
//@ End-Usage

#ifdef SIGMA_DEVELOPMENT
#include <string.h>
#include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT

// Initialization of static class members
BreathPhaseScheduler* BreathPhaseScheduler::PCurrentBreathPhaseScheduler_ 
        = (BreathPhaseScheduler*)NULL;
BreathPhaseScheduler* BreathPhaseScheduler::PPreviousScheduler_
        = (BreathPhaseScheduler*)NULL;
Boolean BreathPhaseScheduler::RateChange_ = FALSE;
Boolean BreathPhaseScheduler::ModeChange_ = FALSE;
Boolean BreathPhaseScheduler::IsRateChangePending_ = FALSE;
Boolean BreathPhaseScheduler::IsBLInspTimeChangePending_ = FALSE;
Boolean BreathPhaseScheduler::IsApneaRateChangePending_ = FALSE;
Boolean BreathPhaseScheduler::IsModeChangePending_ = FALSE;
Boolean BreathPhaseScheduler::IsSpontTypeChangePending_ = FALSE;
Boolean BreathPhaseScheduler::IsStandbyRequired_ = FALSE;
SettingId::SettingIdType BreathPhaseScheduler::ImmediateChangeSettingId_ = SettingId::NULL_SETTING_ID;
BreathPhaseScheduler::PeepRecoveryState BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::OFF;
BreathPhaseScheduler::BlTransitionState
 BreathPhaseScheduler::BlPeepTransitionState_ = BreathPhaseScheduler::NULL_TRANSITION;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathPhaseScheduler
//
//@ Interface-Description
//		Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// The member schedulerId_ is set to the argument passed in
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
BreathPhaseScheduler::BreathPhaseScheduler(
								const SchedulerId::SchedulerIdValue schedulerId
										  ) : schedulerId_(schedulerId)
{
// $[TI1]
	CALL_TRACE("BreathPhaseScheduler::BreathPhaseScheduler( \
									const SchedulerId::SchedulerIdValue id)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathPhaseScheduler [Destructor]
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
BreathPhaseScheduler::~BreathPhaseScheduler(void)
{
  CALL_TRACE("~BreathPhaseScheduler()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EvaluateSetScheduler
//
//@ Interface-Description
//	This static method returns a reference to the set scheduler. This method
//	overloads the empty argument method by adding a DiscreteValue argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method checks the current mode setting and returns a reference
//	to the user selected scheduler based on that setting (e.g. SIMV, A/C).
//---------------------------------------------------------------------
//@ PreCondition
//	(mode < ModeValue::TOTAL_MODE_VALUES && mode > 0)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
BreathPhaseScheduler&
BreathPhaseScheduler::EvaluateSetScheduler(const DiscreteValue mode)
{
	CALL_TRACE("EvaluateSetScheduler(const DiscreteValue mode)");
	
	BreathPhaseScheduler* pScheduler = NULL;
	
	if(mode == ModeValue::AC_MODE_VALUE)
	{
	// $[TI1]
	    pScheduler = (BreathPhaseScheduler*)&RAcScheduler;
	}
	else if(mode == ModeValue::SIMV_MODE_VALUE)
	{
	// $[TI2]
	    pScheduler = (BreathPhaseScheduler*)&RSimvScheduler;
	}
	else if ((mode == ModeValue::SPONT_MODE_VALUE) || (mode == ModeValue::CPAP_MODE_VALUE))
	{
	// $[TI3]
		pScheduler = (BreathPhaseScheduler*)&RSpontScheduler;
	}
	else if(mode == ModeValue::BILEVEL_MODE_VALUE)
	{
	// $[TI5]
		pScheduler = (BreathPhaseScheduler*)&RBiLevelScheduler;
	}
	else
	{
	// $[TI4]
		CLASS_PRE_CONDITION( mode < ModeValue::TOTAL_MODE_VALUES && mode > 0);
	}
	return (*pScheduler);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EvaluateSetScheduler
//
//@ Interface-Description
//	This static method takes no argument and returns a reference to the set
//	scheduler. There is only one set scheduler defined at any given time.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method checks the current mode setting and returns a reference
//	to the user selected scheduler based on that setting (e.g. SIMV, A/C).
//	To get the current scheduler the overloaded EvaluateSetScheduler method
//	is called with the current mode setting as an argument.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
BreathPhaseScheduler&
BreathPhaseScheduler::EvaluateSetScheduler(void)
{
// $[TI5]
	CALL_TRACE("EvaluateSetScheduler(void)");
	
	DiscreteValue mode = PhasedInContextHandle::GetDiscreteValue(SettingId::MODE);
	BreathPhaseScheduler &rScheduler = EvaluateSetScheduler(mode);
	return (rScheduler);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//	This static method takes no argument and returns no value. It initializes
//	the current BreathPhaseScheduler pointer to the power up scheduer. All
//	settings in need of a callback, register with the pending setting
//	context. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	PCurrentBreathPhaseScheduler_ is initialized with a pointer to the
//	PowerupScheduler. Settings for mode and rate register for a callback
//	with the class PendingContextHandle.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BreathPhaseScheduler::Initialize(void)
{
// $[TI1]
	CALL_TRACE("BreathPhaseScheduler::Initialize(void)");

	PCurrentBreathPhaseScheduler_ = (BreathPhaseScheduler*)&RPowerupScheduler;

	// register callbacks for mode and respiratory rate setting changes 
	PendingContextHandle::RegisterSettingEvent(SettingId::MODE,
								BreathPhaseScheduler::SettingEventHappened);
	PendingContextHandle::RegisterSettingEvent(SettingId::SUPPORT_TYPE,
								BreathPhaseScheduler::SettingEventHappened);
	PendingContextHandle::RegisterSettingEvent(SettingId::RESP_RATE,
								BreathPhaseScheduler::SettingEventHappened);
	PendingContextHandle::RegisterSettingEvent(SettingId::APNEA_RESP_RATE,
								BreathPhaseScheduler::SettingEventHappened);
	PendingContextHandle::RegisterSettingEvent(SettingId::PEEP_HIGH_TIME,
								BreathPhaseScheduler::SettingEventHappened);

	//$[LC02015] the following settings shall be phased in immediately when Ta=OFF...
	// special case settings that are phased in immediately when Ta = OFF
	PendingContextHandle::RegisterSettingEvent(SettingId::FLOW_SENS,
								BreathPhaseScheduler::SettingEventHappened);
	PendingContextHandle::RegisterSettingEvent(SettingId::OXYGEN_PERCENT,
								BreathPhaseScheduler::SettingEventHappened);
	PendingContextHandle::RegisterSettingEvent(SettingId::PEEP,
								BreathPhaseScheduler::SettingEventHappened);
	PendingContextHandle::RegisterSettingEvent(SettingId::LEAK_COMP_ENABLED,
								BreathPhaseScheduler::SettingEventHappened);
	PendingContextHandle::RegisterSettingEvent(SettingId::DISCO_SENS,
								BreathPhaseScheduler::SettingEventHappened);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReportEventStatus
//
//@ Interface-Description
// This static method invokes the non static method to report a user event.
// It takes a EventId and a Boolean that signifies the event status
// as arguments and returns a EventStatus.
//---------------------------------------------------------------------
//@ Implementation-Description
// The data member that points to the current scheduler is used to invoke the
// method reportEventStatus_.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::ReportEventStatus(const EventData::EventId id,
										const Boolean eventStatus)
{
// $[TI1]
	CALL_TRACE("BreathPhaseScheduler::ReportEventStatus( \
				EventData::EventId id,	Boolean eventStatus)");

	EventData::EventStatus status;
	status = PCurrentBreathPhaseScheduler_->reportEventStatus_(id, eventStatus);

	return (status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SettingEventHappened
//
//@ Interface-Description
// This static method takes a SettingIdType and has no return value.
// It sets a flag to indicate that a setting change is pending. This
// method is called by the task responsible to receive settings from the
// operator, but it does not need a Mutex since it writes only one byte and
// therefore is protected. Rate and insp time changes are considered only
// if peep recovery is off.
//---------------------------------------------------------------------
//@ Implementation-Description
// The data member indicating if setting change is pending, is set to true.
// The static member PeepRecovery_ is checked for peep recovery state.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BreathPhaseScheduler::SettingEventHappened(SettingId::SettingIdType id)
{
	CALL_TRACE("BreathPhaseScheduler::SettingEventHappened(SettingId::SettingIdType id)");

    BreathPhase *pBreathPhase = BreathPhase::GetCurrentBreathPhase();
	BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();

	if(id == SettingId::MODE && phase != BreathPhaseType::EXPIRATORY_PAUSE) 
	{
		// $[TI1]
		IsModeChangePending_ = TRUE;
	}
	else if (PeepRecovery_ == BreathPhaseScheduler::OFF)
	{
		// $[TI2]
		Int32 apneaIntervalSetValue =
			(Int32)(PendingContextHandle::GetBoundedValue(SettingId::APNEA_INTERVAL).value);
		Boolean isApneaDetectionOn = (apneaIntervalSetValue != DEFINED_UPPER_ALARM_LIMIT_OFF);
		if(id == SettingId::RESP_RATE)
		{
		// $[TI2.1]
			IsRateChangePending_ = TRUE;
		}
		else if (id == SettingId::PEEP_HIGH_TIME)
		{
		// $[TI2.2]
			IsBLInspTimeChangePending_ = TRUE;
		}
		else if(id == SettingId::APNEA_RESP_RATE)
		{
		// $[TI2.3]
			IsApneaRateChangePending_ = TRUE;
		}
		else if (id == SettingId::SUPPORT_TYPE)
		{
		// $[TI2.4]
			IsSpontTypeChangePending_ = TRUE;
		}
		else if (!isApneaDetectionOn)
		{
			ImmediateChangeSettingId_ = id;
		}
		else
		{
		// $[TI2.5]
			AUX_CLASS_ASSERTION((id >= SettingId::LOW_BATCH_BD_ID_VALUE &&
								 id <= SettingId::HIGH_BATCH_BD_ID_VALUE), id);
		}
	}
	// $[TI3]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NewCycle
//
//@ Interface-Description
// This static method takes no argument and has no return value.  It invokes a
// non static method to notify about a user request to change a setting that
// may affect the timing of the current breath.  Mode, rate, and insp time
// setting changes are handled such that only one setting change can be
// processed with mode change having a high precedence than rate change and
// rate change having a higher precedence than insp time.  Apnea rate change
// is handled separately.
//---------------------------------------------------------------------
//@ Implementation-Description
// The data member that points to the current scheduler is used to invoke the
// method settingChangeHappened_.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BreathPhaseScheduler::NewCycle(void)
{
	CALL_TRACE("BreathPhaseScheduler::NewCycle(void)");

	if(IsModeChangePending_)
	{
	// $[TI1]
	// A rate change, if pending, will be phased-in when the pending mode setting
	// will.
		PCurrentBreathPhaseScheduler_->settingChangeHappened_(SettingId::MODE);
		IsModeChangePending_ = FALSE;
		IsRateChangePending_ = FALSE;
		IsBLInspTimeChangePending_ = FALSE;
	}
	else if(IsRateChangePending_)
	{
	// $[TI2]
		PCurrentBreathPhaseScheduler_->settingChangeHappened_(SettingId::RESP_RATE);
		IsRateChangePending_ = FALSE;
	}
	else if(IsBLInspTimeChangePending_)
	{
	// $[TI6]
		PCurrentBreathPhaseScheduler_->settingChangeHappened_(SettingId::PEEP_HIGH_TIME);
		IsBLInspTimeChangePending_ = FALSE;
	}
	// $[TI4]
	
	if(IsApneaRateChangePending_)
	{
	// $[TI3]
		PCurrentBreathPhaseScheduler_->settingChangeHappened_(SettingId::APNEA_RESP_RATE);
		IsApneaRateChangePending_ = FALSE;
	}
	// $[TI5]

	if(IsSpontTypeChangePending_)
	{
	// $[TI7]
		PCurrentBreathPhaseScheduler_->settingChangeHappened_(SettingId::SUPPORT_TYPE);
		IsSpontTypeChangePending_ = FALSE;
	}
	// $[TI8]

	if (ImmediateChangeSettingId_ != SettingId::NULL_SETTING_ID)
	{
		PCurrentBreathPhaseScheduler_->settingChangeHappened_(ImmediateChangeSettingId_);
		ImmediateChangeSettingId_ = SettingId::NULL_SETTING_ID;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description

//  This method is invoked from the derived schedulers' takeControl() methods
//  to perform work that is the same for all schedulers.  This method invokes
//  the SmSwitchConfirmation::SetIsOkayToEnterSm() to notify the
//  SmSwitchConfirmation class whether or not the ventilator is in a state to
//  allow the user to enter SST.  The method sets the previous scheduler and
//  the peep recovery variables.  The active state of apnea and safety pcv is
//  updated in the nov ram instance of BdSyatemState.  The pressure controller
//  integrators are initalized here.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method SmSwitchConfirmation::SetIsOkayToEnterSm() is invoked
//  to notify the SmSwitchConfirmation class whether or not the
//  ventilator is in a state to allow the user to enter SST
//  depending on the schedulerId.
//  Static methods of BdSystemStateHandler are used to set the active
//  state of apnea and safety pcv. The static member PPreviousScheduler_
//  is used to save a pointer to the previous scheduler.
//---------------------------------------------------------------------
//@ PreCondition
// scheduler Id must be valid
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BreathPhaseScheduler::takeControl(const BreathPhaseScheduler& prevScheduler) 
{
	CALL_TRACE("BreathPhaseScheduler::takeControl(const BreathPhaseScheduler& prevScheduler)");

	PPreviousScheduler_ = (BreathPhaseScheduler*)&prevScheduler;
	PeepRecovery_ = BreathPhaseScheduler::OFF;

	RPressureController.setInitIntegratorValue(0.0);
	
	switch (schedulerId_)
	{
	    case SchedulerId::SIMV:
	    case SchedulerId::SPONT:
	    case SchedulerId::BILEVEL:
	    case SchedulerId::AC:	// $[TI1]

	        // Cannot enter Service Mode from these schedulers
	        SmSwitchConfirmation::SetIsOkayToEnterSm(FALSE);
		    // reset the apnea active flag in nov ram
   			BdSystemStateHandler::UpdateApneaActiveStatus(FALSE);
   			BdSystemStateHandler::UpdateSafetyPcvActiveStatus(FALSE);

	        break;

 	    case SchedulerId::SAFETY_PCV:	// $[TI2]
	        // Cannot enter Service Mode from this scheduler
	        SmSwitchConfirmation::SetIsOkayToEnterSm(FALSE);
   			BdSystemStateHandler::UpdateApneaActiveStatus(FALSE);

			break;
			
	    case SchedulerId::APNEA:	// $[TI3]
	        // Cannot enter Service Mode from this scheduler
	        SmSwitchConfirmation::SetIsOkayToEnterSm(FALSE);
   			BdSystemStateHandler::UpdateSafetyPcvActiveStatus(FALSE);

			break;
			
	    case SchedulerId::OCCLUSION:	// $[TI4]
	        // Cannot enter Service Mode from this scheduler
	        SmSwitchConfirmation::SetIsOkayToEnterSm(FALSE);
	        BlPeepTransitionState_ = NULL_TRANSITION;

	        break;

	    case SchedulerId::POWER_UP:	// $[TI5]
	        // Can only enter Service Mode from power up if no patient is attached
	        SmSwitchConfirmation::SetIsOkayToEnterSm(TRUE);
	        break;

	    case SchedulerId::DISCONNECT:
	    case SchedulerId::STANDBY:
        case SchedulerId::SVO:	// $[TI6]
            // Do not change the state of the SmSwitch
	        BlPeepTransitionState_ = NULL_TRANSITION;
            break;
            
        case SchedulerId::TOTAL_SCHEDULER_IDS: 
        default:	// $[TI7]
            // Not a valid schedulerId
            AUX_CLASS_ASSERTION_FAILURE(schedulerId_);
            break;
    }
} 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CheckIfApneaPossible
//
//@ Interface-Description
// The method takes no argument and returns a Boolean indicating if apnea is possible.
// Apnea is possible if the breath duration is longer than the apnea interval
// duration or if Spont is the current mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// Mode, rate, and apnea interval are all read from the pending setting
// context. If the breath interval is greater than the apnea interval or spont
// is the set mode, than the method returns TRUE. Otherwise, it returns FALSE.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
Boolean
BreathPhaseScheduler::CheckIfApneaPossible(void)
{
	// get the set mode:
	DiscreteValue mode = PendingContextHandle::GetDiscreteValue(SettingId::MODE);
	// get the set respiratory rate from the pending context. The pending context is used
	// to prevent brief transitions to or from apnea ventilation.
	const BoundedValue& rRespiratoryRate =
					PendingContextHandle::GetBoundedValue(SettingId::RESP_RATE);
	Int32 breathInterval = (Int32)(60.0F*1000.0F/rRespiratoryRate.value);
	// get the set apnea interval:
	Int32 apneaIntervalSetValue =
        (Int32)(PendingContextHandle::GetBoundedValue(SettingId::APNEA_INTERVAL).value);
	Boolean isApneaDetectionOn = (apneaIntervalSetValue != DEFINED_UPPER_ALARM_LIMIT_OFF);

	return ( (breathInterval > apneaIntervalSetValue) ||
			 (mode == ModeValue::SPONT_MODE_VALUE) || 
			 (mode == ModeValue::CPAP_MODE_VALUE && isApneaDetectionOn) );
			 // return TRUE: $[TI1]
			 // return FALSE: $[TI2]
}


#ifdef SIGMA_DEBUG

void
BreathPhaseScheduler::getId(char* idString, SchedulerId::SchedulerIdValue id) const
{
    if(id == SchedulerId::APNEA)
        strcpy(idString,"ApneaScheduler");
    else if(id == SchedulerId::AC)
        strcpy(idString,"AcScheduler");
    else if(id == SchedulerId::SPONT)
        strcpy(idString,"SpontScheduler");
    else if(id == SchedulerId::SIMV)
        strcpy(idString,"SimvScheduler");
    else if(id == SchedulerId::BILEVEL)
        strcpy(idString,"BilevelScheduler");
    else if(id == SchedulerId::DISCONNECT)
        strcpy(idString,"DisconnectScheduler");
    else if(id == SchedulerId::OCCLUSION)
        strcpy(idString,"OscScheduler");
    else if(id == SchedulerId::SAFETY_PCV)
        strcpy(idString,"SafetPcvScheduler");
    else if(id == SchedulerId::POWER_UP)
        strcpy(idString,"PowerupScheduler");
    else if(id == SchedulerId::SVO)
        strcpy(idString,"SvoScheduler");
    else if(id == SchedulerId::STANDBY)
        strcpy(idString,"StandbyScheduler");
    else
        strcpy(idString, "!!!---NoScheduler---!!!");
}

#endif // SIGMA_DEBUG


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BreathPhaseScheduler::SoftFault(const SoftFaultID  softFaultID,
			        const Uint32       lineNumber,
			        const char*        pFileName,
			        const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BREATHPHASESCHEDULER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VerifyModeSynchronization_
//
//@ Interface-Description
// The method takes a SchedulerId as an argument and has no return value.
// It checks that the current scheduler matches the set mode value.
// If no match is detected, the setting change mode trigger is enabled,
// the mode change flag is set to true and the rate change flag is set to false.
//---------------------------------------------------------------------
//@ Implementation-Description
// The mode value setting is retrieved from the PhasedInConntextHandle.
// It is compared against the value associated with the currnet scheduler.
// If the comparison fails, the instance rSettingChangeModeTrigger is enabled,
// the flag ModeChange_ is set to TRUE and the flag RateChange_ is Set to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
// The mode setting value and the scheduler id are checked to be in the
// proper range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathPhaseScheduler::VerifyModeSynchronization_(const SchedulerId::SchedulerIdValue id)
{
    CALL_TRACE("BreathPhaseScheduler::VerifyModeSynchronization_(const SchedulerId id)");

    DiscreteValue mode;
    Boolean isModeInSynch = TRUE;
    mode = PhasedInContextHandle::GetDiscreteValue(SettingId::MODE);

	switch (id)
	{
	    case SchedulerId::SIMV:
            if(mode != ModeValue::SIMV_MODE_VALUE)
            {
            // $[TI1]
                isModeInSynch = FALSE;
                CLASS_ASSERTION(mode == ModeValue::AC_MODE_VALUE ||
                                mode == ModeValue::SPONT_MODE_VALUE ||
                                mode == ModeValue::CPAP_MODE_VALUE ||
                                mode == ModeValue::BILEVEL_MODE_VALUE )
            }
            // $[TI2]

            break;
	    case SchedulerId::AC:
            if(mode != ModeValue::AC_MODE_VALUE)
            {
            // $[TI3]
                isModeInSynch = FALSE;   
                CLASS_ASSERTION(mode == ModeValue::SIMV_MODE_VALUE ||
                                mode == ModeValue::SPONT_MODE_VALUE ||
                                mode == ModeValue::CPAP_MODE_VALUE ||
                                mode == ModeValue::BILEVEL_MODE_VALUE )
            }
            // $[TI4]

            break;
	    case SchedulerId::SPONT:
            if ((mode != ModeValue::SPONT_MODE_VALUE) && (mode != ModeValue::CPAP_MODE_VALUE))
            {
            // $[TI5]
                isModeInSynch = FALSE;   
                CLASS_ASSERTION(mode == ModeValue::AC_MODE_VALUE ||
                                mode == ModeValue::SIMV_MODE_VALUE ||
                                mode == ModeValue::BILEVEL_MODE_VALUE )
            }
            // $[TI6]

            break;
	    case SchedulerId::BILEVEL:
            if(mode != ModeValue::BILEVEL_MODE_VALUE)
            {
            // $[TI10]
                isModeInSynch = FALSE;   
                CLASS_ASSERTION(mode == ModeValue::AC_MODE_VALUE ||
                                mode == ModeValue::SIMV_MODE_VALUE ||
                                mode == ModeValue::CPAP_MODE_VALUE ||
                                mode == ModeValue::SPONT_MODE_VALUE )
            }
            // $[TI11]

            break;
        default:
        // $[TI7]
            // Not a valid scheduler id
            CLASS_ASSERTION_FAILURE();
            break;
    }

    if(isModeInSynch == FALSE)
    {
    // $[TI8]
        ((Trigger&)RSettingChangeModeTrigger).enable();
    }
    // $[TI9]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: settingChangeHappened_
//
//@ Interface-Description
// The method takes a SettingId as an argument and has no return value.  This
// method is implemented for all these scheduler that are not effected by a
// setting change.
//---------------------------------------------------------------------
//@ Implementation-Description
// No action (other than the class assertion) is taken by this method
//---------------------------------------------------------------------
//@ PreCondition
// The setting id is within the folowing range:
//   id >= SettingId::LOW_BATCH_BD_ID_VALUE && id <= SettingId::HIGH_BATCH_BD_ID_VALUE
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathPhaseScheduler::settingChangeHappened_(const SettingId::SettingIdType id)
{
// $[TI1]

	CALL_TRACE("BreathPhaseScheduler::settingChangeHappened_( \
										const SettingId::SettingIdType id)");

	CLASS_ASSERTION( id >= SettingId::LOW_BATCH_BD_ID_VALUE &&
	                 id <= SettingId::HIGH_BATCH_BD_ID_VALUE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RejectManualInspiration_
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// for user event status. It rejects a manual inspiration request.
//---------------------------------------------------------------------
//@ Implementation-Description
// When the eventStatus is TRUE, the method returns EventData::REJECTED status.
//---------------------------------------------------------------------
//@ PreCondition
// The eventStatus can only be TRUE.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::RejectManualInspiration_(const Boolean eventStatus)
{
	EventData::EventStatus rtnStatus = EventData::IDLE;
	if (eventStatus)
	{
	// $[TI1]
		rtnStatus = EventData::REJECTED;
	}
	else
	{
	// $[TI2]
		// manual inspiration can not be disabled
		CLASS_PRE_CONDITION(eventStatus);
	}
	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AcceptManualInspiration_
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// for user event status. It accepts a manual inspiration request.
//---------------------------------------------------------------------
//@ Implementation-Description
// When the eventStatus is TRUE, the object rOperatorInspTrigger is enabled
// and the method returns EventData::PENDING status.
//---------------------------------------------------------------------
//@ PreCondition
// The eventStatus can only be TRUE.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::AcceptManualInspiration_(const Boolean eventStatus)
{
	EventData::EventStatus rtnStatus = EventData::IDLE;
	if (eventStatus)
	{
	// $[TI1]
		((Trigger&)ROperatorInspTrigger).enable();
		rtnStatus = EventData::PENDING;
	}
	else // manual inspiration can not be disabled
	{
	// $[TI2]
			CLASS_PRE_CONDITION(eventStatus);
	}
	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RejectExpiratoryPause_
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// for user event status. It rejects an expiratory pause request.
//---------------------------------------------------------------------
//@ Implementation-Description
// When the eventStatus is TRUE, the method returns a REJECTED status and asserts that
// the current event status can't be PENDING or ACTIVE. When the eventStatus is FALSE,
// the method returns a REJECTED status.
//---------------------------------------------------------------------
//@ PreCondition
// For eventStatus == TRUE: (currentStatus != EventData::PENDING) &&
//							(currentStatus != EventData::ACTIVE) 
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::RejectExpiratoryPause_(const Boolean eventStatus)
{
	EventData::EventStatus rtnStatus = EventData::IDLE;
	const EventData::EventStatus currentStatus =
									RExpiratoryPauseEvent.getEventStatus();
	if (eventStatus)
	{
	// $[TI1]
		rtnStatus = EventData::REJECTED;
		CLASS_PRE_CONDITION( (currentStatus != EventData::PENDING) &&
							(currentStatus != EventData::ACTIVE) );
	}
	else
	{
	// $[TI2]
		rtnStatus = EventData::CANCEL;
	}
	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RejectInspiratoryPause_
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// for user event status. It rejects an inspiratory pause request.
//---------------------------------------------------------------------
//@ Implementation-Description
// When the eventStatus is TRUE, the method returns a REJECTED status and asserts that
// the current event status can't be PENDING or ACTIVE. When the eventStatus is FALSE,
// the method returns a REJECTED status.
//---------------------------------------------------------------------
//@ PreCondition
// For eventStatus == TRUE: (currentStatus != EventData::PENDING) &&
//							(currentStatus != EventData::ACTIVE) 
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::RejectInspiratoryPause_(const Boolean eventStatus)
{
	EventData::EventStatus rtnStatus = EventData::IDLE;
	const EventData::EventStatus currentStatus =
									RInspiratoryPauseEvent.getEventStatus();
	if (eventStatus)
	{
	// $[TI1]
		rtnStatus = EventData::REJECTED;
		CLASS_PRE_CONDITION( (currentStatus != EventData::PENDING) &&
							(currentStatus != EventData::ACTIVE) );
	}
	else
	{
	// $[TI2]
		rtnStatus = EventData::REJECTED;
	}
	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AcceptExpiratoryPause_
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// for user event status. It accepts an expiratory pause request. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call expiratory pause maneuver's method handleExpPause() to
// decide the fate of the expiratory pause.
//---------------------------------------------------------------------
//@ PreCondition
// N/A
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::AcceptExpiratoryPause_(const Boolean eventStatus)
{
	EventData::EventStatus rtnStatus = RExpPauseManeuver.handleExpPause(eventStatus);
	return(rtnStatus);
	// $[TI1] $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AcceptInspiratoryPause_
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// from the Inspiratory pause maneuver for user event status .
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call inspiratory pause maneuver's method handleInspPause() to
// decide the fate of the inspiratory pause.
//---------------------------------------------------------------------
//@ PreCondition
// N/A
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::AcceptInspiratoryPause_(const Boolean eventStatus)
{
	EventData::EventStatus rtnStatus = RInspPauseManeuver.handleInspPause(eventStatus);
	return(rtnStatus);
	// $[TI1] $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RejectNifManeuver_
//
//@ Interface-Description
// The method takes a boolean isStartRequested as an argument and returns an enum
// for user event status. It rejects an NIF maneuver request.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::RejectNifManeuver_(const Boolean isStartRequested)
{
	EventData::EventStatus rtnStatus = EventData::IDLE;
	const EventData::EventStatus currentStatus = RNifManeuverEvent.getEventStatus();

	if (isStartRequested)
	{
		rtnStatus = EventData::REJECTED;
		CLASS_PRE_CONDITION( (currentStatus != EventData::PENDING) &&
							 (currentStatus != EventData::ACTIVE) );
	}
	else
	{
		rtnStatus = EventData::REJECTED;
	}
	return (rtnStatus);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RejectP100Maneuver_
//
//@ Interface-Description
// The method takes a boolean isStartRequested as an argument and returns an enum
// for user event status. It rejects an P100 maneuver request.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::RejectP100Maneuver_(const Boolean isStartRequested)
{
	EventData::EventStatus rtnStatus = EventData::IDLE;
	const EventData::EventStatus currentStatus = RP100ManeuverEvent.getEventStatus();

	if (isStartRequested)
	{
		rtnStatus = EventData::REJECTED;
		CLASS_PRE_CONDITION( (currentStatus != EventData::PENDING) &&
							 (currentStatus != EventData::ACTIVE) );
	}
	else
	{
		rtnStatus = EventData::REJECTED;
	}
	return (rtnStatus);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RejectVitalCapacityManeuver_
//
//@ Interface-Description
// The method takes a boolean isStartRequested as an argument and returns an enum
// for user event status. It rejects an SVC maneuver request.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::RejectVitalCapacityManeuver_(const Boolean isStartRequested)
{
	EventData::EventStatus rtnStatus = EventData::IDLE;
	const EventData::EventStatus currentStatus = RVitalCapacityManeuverEvent.getEventStatus();

	if (isStartRequested)
	{
		rtnStatus = EventData::REJECTED;
		CLASS_PRE_CONDITION( (currentStatus != EventData::PENDING) &&
							 (currentStatus != EventData::ACTIVE) );
	}
	else
	{
		rtnStatus = EventData::REJECTED;
	}
	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AcceptNifManeuver_
//
//@ Interface-Description
// The method takes a boolean isStartRequested as an argument and returns an enum
// from the NIF maneuver for user event status.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call NIF maneuver's method handleManeuver() to
// decide the fate of the NIF maneuver.
//---------------------------------------------------------------------
//@ PreCondition
// N/A
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::AcceptNifManeuver_(const Boolean isStartRequested)
{
	return(RNifManeuver.handleManeuver(isStartRequested));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AcceptP100Maneuver_
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// from the P100 maneuver for user event status .
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call P100 maneuver's method handleManeuver() to
// decide the fate of the P100 maneuver.
//---------------------------------------------------------------------
//@ PreCondition
// N/A
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::AcceptP100Maneuver_(const Boolean isStartRequested)
{
	return(RP100Maneuver.handleManeuver(isStartRequested));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AcceptVitalCapacityManeuver_
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// from the SVC maneuver for user event status .
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call SVC maneuver's method handleManeuver() to
// decide the fate of the SVC maneuver.
//---------------------------------------------------------------------
//@ PreCondition
// N/A
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::AcceptVitalCapacityManeuver_(const Boolean isStartRequested)
{
	return(RVitalCapacityManeuver.handleManeuver(isStartRequested) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AcceptAlarmReset_
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// for user event status. It accepts an alarm reset request. To initiate alarm reset,
// the manual reset mode trigger is enabled and the method returns a COMPLETE status.
//---------------------------------------------------------------------
//@ Implementation-Description
// The eventStatus argument is set to TRUE to initiate alarm reset.  In that case,
// the mode trigger instance rManualResetTrigger is enabled and the method
// returns a COMPLETE user event status. An assertion is made that alarm reset can
// not be disabled.
//---------------------------------------------------------------------
//@ PreCondition
// eventStatus can't be FALSE.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
EventData::EventStatus
BreathPhaseScheduler::AcceptAlarmReset_(const Boolean eventStatus)
{
	EventData::EventStatus rtnStatus = EventData::IDLE;
	if (eventStatus)
	{
	// $[TI1]
		((Trigger&)RManualResetTrigger).enable();
		// The GUI receives a completion status right away.
		rtnStatus = EventData::COMPLETE;
	}
	else // alarm reset can not be disabled
	{
	// $[TI2]
		CLASS_PRE_CONDITION(eventStatus);
	}
	return (rtnStatus);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================




