#ifndef TrendTimeScaleMenuItems_HH
#define TrendTimeScaleMenuItems_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendTimeScaleMenuItems -  Trend time scale menu items
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendTimeScaleMenuItems.hhv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc    Date: 10-Jan-2007    SCR Number: 6237
//  Project:  TREND
//  Description:
//      Initial version.
//
//====================================================================

//@ Usage-Classes
#include "GuiAppClassIds.hh"
#include "SettingMenuItems.hh"
#include "DiscreteSettingButton.hh"
//@ End-Usage

class TrendTimeScaleMenuItems : public SettingMenuItems
{
public:
	static TrendTimeScaleMenuItems& GetMenuItems(void);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
private:
	TrendTimeScaleMenuItems(void);
	virtual ~TrendTimeScaleMenuItems(void);

    // these methods are purposely declared, but not implemented...
    TrendTimeScaleMenuItems(const TrendTimeScaleMenuItems&);				// not implemented...
    void   operator=(const TrendTimeScaleMenuItems&);			// not implemented...
};


#endif // TrendTimeScaleMenuItems_HH
