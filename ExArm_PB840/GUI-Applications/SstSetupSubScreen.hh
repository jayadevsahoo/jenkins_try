#ifndef SstSetupSubScreen_HH
#define SstSetupSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SstSetupSubScreen - Activated by selecting SST Test button on
// the lower subScreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SstSetupSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 004  By:  hhd	   Date:  28-Jan-1999    DCS Number: 
//  Project:  ATC
//  Description:
//		Initial version.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  01-Oct-97    DR Number: 2522
//    Project:  Sigma (R8027)
//    Description:
//      Added executeSettingTransactionHappene to receive and transaction
//		successful message before test starts.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "BatchSettingsSubScreen.hh"

//@ Usage-Classes
#include "DiscreteSettingButton.hh"
#include "NumericSettingButton.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
//@ End-Usage

class SstSetupSubScreen : public BatchSettingsSubScreen
{
public:
	SstSetupSubScreen(SubScreenArea *pSubScreenArea);
	~SstSetupSubScreen(void);

	// Overload SubScreen methods
	virtual void acceptHappened(void);
	
	void executeSettingTransactionHappened(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
												Boolean byOperatorAction);

	// BatchSettingsSubScreen virtual method
	void adjustPanelRestoreFocusHappened(void);

	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// BatchSettingsSubScreen virtual method...
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									  );

private:
	// these methods are purposely declared, but not implemented...
	SstSetupSubScreen(void);							// not implemented..
	SstSetupSubScreen(const SstSetupSubScreen&);	// not implemented..
	void operator=(const SstSetupSubScreen&);			// not implemented..

	// Constant: MAX_SETTING_BUTTONS_ 
	// Maximum number of buttons that can be displayed in this subscreen.
	// Must be modified when new buttons are added.
	enum { MAX_SETTING_BUTTONS_ = 3 };

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member: humidifierTypeButton_
	// The humidifierType setting button.
	DiscreteSettingButton humidifierTypeButton_;

	//@ Data-Member: humidifierVolumeButton_
	// The Humidifier Volume setting button.
	NumericSettingButton humidifierVolumeButton_;

	//@ Data-Member: patientCircuitTypeButton_
	// The patientCircuitType setting button.
	DiscreteSettingButton patientCircuitTypeButton_;

	//@ Data-Member: verifyHumidVolMsg_
	// The message which appears when the user changes patient circuit
	// type and the humidifier volume is button is shown.
	TextField verifyHumidVolMsg_;

	//@ Data-Member: exitButton_
	// The Exit button is used for exiting the SST test mode.
	TextButton exitButton_;

	//@ Data-Member: pCurrentButton_
	// The pCurrentButton_ is used for remembering the previously button
	// which was pressed by the user.
	Button *pCurrentButton_;

	//@ Data-Member: sstSubScreenDivideLine_
	// The line divides the SST Lower Sub-screen Area in to two logical area.
	Line sstSubScreenDivideLine_;

	//@ Data-Member:  arrSettingButtonPtrs_
	// The subscreen buttons.
	SettingButton*  arrSettingButtonPtrs_[MAX_SETTING_BUTTONS_ + 1];
};


#endif // SstSetupSubScreen_HH 
