#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BoundStrs - Matches advisory prompt string ids to the setting
// bound ids of the Settings-Validation subsystem.
//---------------------------------------------------------------------
//@ Interface-Description
// BoundStrs is a static class that provides a method, GetSettingBoundString(),
// which returns the advisory message (stored in the PromptStrs class) for a
// specified Settings-Validation subsystem setting bound id.  The advisory message
// is used for display in the Prompt Area.
//---------------------------------------------------------------------
//@ Rationale
// The intention of this class is to localize the cross-referencing between
// advisory messages and setting bound ids.
//---------------------------------------------------------------------
//@ Implementation-Description
// The array SettingBoundStrs_[] stores a pointer to each advisory message.
// A setting bound id is used as an index into this array to retrieve the
// appropriate message.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// An object of this class cannot be instantiated.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BoundStrs.ccv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 027   By:   rhj    Date: 13-July-2010  SCR Number: 6581 
//  Project:  XENA2
//  Description:
//       Added bound strings for disconnectivity sensitivity based on
//       neoNatal.
//  
//  Revision: 026   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  S840
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 025  By: rhjimen  Date:  02-Feb-2007    SCR Number: 6237
//  Project:  Trend
//  Description:  
//       Added Trend Cursor Position Limits
//          
//  Revision: 024  By: gdc  Date:  27-Feb-2005    DR Number: 6170
//  Project:  NIV2
//  Description:  
//      Removed Insp Too Long alarm for NIV. Added High Ti spont alert.
//    
//  Revision: 023  By: gdc  Date:  15-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:  
//      added bound strings for NIV
//  
//  Revision: 022  By: gdc  Date:  15-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:  
//      added bound strings for disconnect sensitivity based on vent-type
//      added bound strings for new low circuit pressure limit setting
//      added bound string for high circuit pressure limit bounded by low circuit 
//      pressure limit
//
//  Revision: 021  By: gfu  Date:  11-Aug-2004    DR Number: 6132
//  Project:  PAV
//  Description:  Modified code per SDCR #6132.  Also added keyword "Log" as requested
//                during code review.
//
//  Revision: 020  By: scottp  Date:  28-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//      Added VT/Vti/Vsupp setting interactions/limitations for VC+ and VS.
//
//  Revision: 019  By: hlg    Date: 04-SEP-2001   DCS Number:  5931
//  Project:  GuiComms
//  Description:
//      Removed unused functionality for setting bounds on the comm
//      port configuration value.
//
//  Revision: 018  By: hct    Date: 09-OCT-2000   DCS Number:  5493
//  Project:  GuiComms
//  Description:
//      Added message for ComPortConfigValue bound.
//
//  Revision: 017  By: sah     Date:  11-Sep-2000    DR Number: 5766
//  Project:  VTPC
//  Description:
//      Added bound message for Vti-spont's new soft limit.
//
//  Revision: 016  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new bound messages for peak insp flow's VC+ constraints,
//         and the new target support volume setting
//
//  Revision: 015  By: sah    Date: 27-Apr-2000   DCS Number:  5713
//  Project:  NeoMode
//  Description:
//      Added message for new PEEP soft-bound.
//
//  Revision: 014  By: sah    Date: 30-Jun-1999   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Initial NeoMode Project implementation added.
//
//  Revision: 013  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 012  By: sah  Date: 08-Feb-1999  DR Number: 5314
//  Project:  ATC (R8027)
//  Description:
//      Added new BiLevel-specific settings' bound ids, and reformatted
//	for better readability.
//
//  Revision: 011  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 010   By: sah   Date: 06-Jan-1999  DCS Number:  5322
//  Project:  ATC
//  Description:
//		Added bound ids for new ATC settings.
//
//  Revision: 009  By:  dosman   Date:  23-Nov-1998    DCS Number: 5285 
//  Project:  BiLevel (R8027)
//  Description:
//		Put back string HB_INSP_TIME_MIN_BASED_PLAT_TIME_A, which is used
//
//  Revision: 008  By:  btray    Date:  03-Nov-1998    DCS Number: 5251 
//  Project:  BiLevel (R8027)
//  Description:
//		Removed unused string HB_INSP_TIME_MIN_BASED_PLAT_TIME_A
//
//  Revision: 007  By:  dosman    Date:  29-Apr-1998    DR Number: 34
//  Project:  Sigma (R8027)
//  Description:
//	added prompts for TH, TL, TH:TL min & max
//     Merged /840/Baseline/GUI-Applications/vcssrc/BoundStrs.ccv   1.18.1.0   07/30/98 10:12:22   gdc
//
//  Revision: 006  By:  dosman    Date:  17-Feb-1998    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//		adding use of peep-high specific max/min prompt strings
//		added prompt for hitting min HighCctPressbasedon peepHigh
//
//  Revision: 005  By:  dosman    Date:  26-Jan-1998    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//		initial BiLevel version
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003  By:  sah    Date:  26-Sep-1997    DR Number: 2410
//  Project:  Sigma (R8027)
//  Description:
//      Added bounds for new Atmospheric Pressure Setting.
//
//  Revision: 002  By:  sah    Date:  25-Sep-1997    DR Number: 1825
//  Project:  Sigma (R8027)
//  Description:
//      Added bounds for new logic of DCI Parity Mode and DCI Data Bits
//      Settings.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "BoundStrs.hh"
#include "PromptStrs.hh"
#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

// Define the array of setting bound strings.
StringId BoundStrs::SettingBoundStrs_[NUM_SETTING_BOUND_IDS];


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [public, static]
//
//@ Interface-Description
// Should be called once at GUI-App initialize time.  Allows for initialization
// of internal data.
//---------------------------------------------------------------------
//@ Implementation-Description
// Places the setting bound string pointer for each setting bound id into the
// correct place in the SettingBoundStrs_[] array.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// Each entry in the array must have a non-null value.
//@ End-Method
//=====================================================================

void
BoundStrs::Initialize(void)
{
	CALL_TRACE("BoundStrs::Initialize(void)");

	// First clear each entry in the array.
	for ( Int32 i = 0; i < NUM_SETTING_BOUND_IDS; i++ )
	{
		SettingBoundStrs_[i] = NULL_STRING_ID;
	}

	// Now set each entry in the array specifically.
	SettingBoundStrs_[ALARM_VOL_MIN_ID] = PromptStrs::HB_ALARM_VOL_MIN_A;
	SettingBoundStrs_[ALARM_VOL_MAX_ID] = PromptStrs::HB_ALARM_VOL_MAX_A;

	SettingBoundStrs_[APNEA_FLOW_PATT_BASED_IE_MAX_ID] = PromptStrs::HB_APNEA_FLOW_PATT_BASED_IE_MAX_A;
	SettingBoundStrs_[APNEA_FLOW_PATT_BASED_TI_MAX_ID] = PromptStrs::HB_APNEA_FLOW_PATT_BASED_TI_MAX_A;
	SettingBoundStrs_[APNEA_FLOW_PATT_BASED_TI_MIN_ID] = PromptStrs::HB_APNEA_FLOW_PATT_BASED_TI_MIN_A;

	SettingBoundStrs_[APNEA_IE_RATIO_MAX_ID] = PromptStrs::HB_APNEA_IE_RATIO_MAX_A;

	SettingBoundStrs_[APNEA_INSP_PRESS_MIN_ID] = PromptStrs::HB_APNEA_INSP_PRESS_MIN_A;
	SettingBoundStrs_[APNEA_INSP_PRESS_MAX_BASED_PEEP_ID] = PromptStrs::HB_APNEA_INSP_PRESS_MAX_BASED_PEEP_A;
	SettingBoundStrs_[APNEA_INSP_PRESS_MAX_BASED_PEEPL_ID] = PromptStrs::HB_APNEA_INSP_PRESS_MAX_BASED_PEEPL_A;
	SettingBoundStrs_[APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEP_ID] = PromptStrs::HB_APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEP_A;
	SettingBoundStrs_[APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEPL_ID] = PromptStrs::HB_APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEPL_A;
	SettingBoundStrs_[APNEA_INSP_PRESS_MAX_ID] = PromptStrs::HB_APNEA_INSP_PRESS_MAX_A;

	SettingBoundStrs_[APNEA_INSP_TIME_MAX_ID] = PromptStrs::HB_APNEA_INSP_TIME_MAX_A;
	SettingBoundStrs_[APNEA_INSP_TIME_MIN_ID] = PromptStrs::HB_APNEA_INSP_TIME_MIN_A;

	SettingBoundStrs_[APNEA_INTERVAL_MIN_ID] = PromptStrs::HB_APNEA_INTERVAL_MIN_A;
	SettingBoundStrs_[APNEA_INTERVAL_MIN_BASED_APNEA_RR_ID] = PromptStrs::HB_APNEA_INTERVAL_MIN_BASED_APNEA_RR_A;
	SettingBoundStrs_[APNEA_INTERVAL_MAX_ID] = PromptStrs::HB_APNEA_INTERVAL_MAX_A;

	SettingBoundStrs_[APNEA_O2_MIN_ID] = PromptStrs::HB_APNEA_O2_MIN_A;
	SettingBoundStrs_[APNEA_O2_MIN_BASED_O2_ID] = PromptStrs::HB_APNEA_O2_MIN_BASED_O2_A;
	SettingBoundStrs_[APNEA_O2_MAX_ID] = PromptStrs::HB_APNEA_O2_MAX_A;

	SettingBoundStrs_[APNEA_PEAK_FLOW_MIN_BASED_CCT_TYPE_ID] = PromptStrs::HB_APNEA_PEAK_FLOW_MIN_BASED_CCT_TYPE_A;
	SettingBoundStrs_[APNEA_PEAK_FLOW_MAX_BASED_CCT_TYPE_ID] = PromptStrs::HB_APNEA_PEAK_FLOW_MAX_BASED_CCT_TYPE_A;

	SettingBoundStrs_[APNEA_RESP_RATE_MIN_ID] = PromptStrs::HB_APNEA_RESP_RATE_MIN_A;
	SettingBoundStrs_[APNEA_RESP_RATE_MIN_BASED_INTERVAL_ID] = PromptStrs::HB_APNEA_RESP_RATE_MIN_BASED_INTERVAL_A;
	SettingBoundStrs_[APNEA_RESP_RATE_MAX_ID] = PromptStrs::HB_APNEA_RESP_RATE_MAX_A;

	SettingBoundStrs_[APNEA_TIDAL_VOL_MIN_ID] = PromptStrs::HB_APNEA_TIDAL_VOL_MIN_A;
	SettingBoundStrs_[APNEA_TIDAL_VOL_MIN_BASED_IBW_ID] = PromptStrs::HB_APNEA_TIDAL_VOL_MIN_BASED_IBW_A;
	SettingBoundStrs_[APNEA_TIDAL_VOL_SOFT_MIN_BASED_IBW_ID] = PromptStrs::HB_APNEA_TIDAL_VOL_SOFT_MIN_BASED_IBW_A;
	SettingBoundStrs_[APNEA_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID] = PromptStrs::HB_APNEA_TIDAL_VOL_SOFT_MAX_BASED_IBW_A;
	SettingBoundStrs_[APNEA_TIDAL_VOL_MAX_BASED_IBW_ID] = PromptStrs::HB_APNEA_TIDAL_VOL_MAX_BASED_IBW_A;
	SettingBoundStrs_[APNEA_TIDAL_VOL_MAX_ID] = PromptStrs::HB_APNEA_TIDAL_VOL_MAX_A;

	SettingBoundStrs_[ATM_PRESSURE_MIN_ID] = PromptStrs::HB_ATM_PRESSURE_MIN_A;
	SettingBoundStrs_[ATM_PRESSURE_MAX_ID] = PromptStrs::HB_ATM_PRESSURE_MAX_A;

	SettingBoundStrs_[DCI_DATA_BITS_BASED_PARITY_ID] = PromptStrs::HB_DCI_DATA_BITS_BASED_PARITY_A;

	SettingBoundStrs_[DCI_PARITY_MODE_BASED_BITS_ID] = PromptStrs::HB_DCI_PARITY_MODE_BASED_BITS_A;

	SettingBoundStrs_[DISPLAY_BRIGHTNESS_MIN_ID] = PromptStrs::HB_DISPLAY_BRIGHTNESS_MIN_A;
	SettingBoundStrs_[DISPLAY_BRIGHTNESS_MAX_ID] = PromptStrs::HB_DISPLAY_BRIGHTNESS_MAX_A;

	SettingBoundStrs_[DISPLAY_CONTRAST_SCALE_MIN_ID] = PromptStrs::HB_DISPLAY_CONTRAST_SCALE_MIN_A;
	SettingBoundStrs_[DISPLAY_CONTRAST_SCALE_MAX_ID] = PromptStrs::HB_DISPLAY_CONTRAST_SCALE_MAX_A;

	SettingBoundStrs_[DISPLAY_CONTRAST_DELTA_MIN_ID] = PromptStrs::HB_DISPLAY_CONTRAST_DELTA_MIN_A;
	SettingBoundStrs_[DISPLAY_CONTRAST_DELTA_MAX_ID] = PromptStrs::HB_DISPLAY_CONTRAST_DELTA_MAX_A;

	SettingBoundStrs_[DISCO_SENS_MIN_ID] = PromptStrs::HB_DISCO_SENS_MIN_A;
	SettingBoundStrs_[DISCO_SENS_VENT_BASED_MAX_ID] = PromptStrs::HB_DISCO_SENS_VENT_BASED_MAX_A;
	SettingBoundStrs_[DISCO_SENS_MAX_ID] = PromptStrs::HB_DISCO_SENS_MAX_A;
	SettingBoundStrs_[DISCO_SENS_NEONATAL_SOFT_MAX_ID] = PromptStrs::HB_DISCO_SENS_NEONATAL_SOFT_MAX_A;	

	SettingBoundStrs_[EXP_SENS_MIN_ID] = PromptStrs::HB_EXP_SENS_MIN_A;
	SettingBoundStrs_[EXP_SENS_MAX_ID] = PromptStrs::HB_EXP_SENS_MAX_A;

	SettingBoundStrs_[EXP_TIME_MIN_ID] = PromptStrs::HB_EXP_TIME_MIN_A;

	SettingBoundStrs_[FLOW_ACCEL_PERCENT_MIN_ID] = PromptStrs::HB_FLOW_ACCEL_PERCENT_MIN_A;
	SettingBoundStrs_[FLOW_ACCEL_PERCENT_MAX_ID] = PromptStrs::HB_FLOW_ACCEL_PERCENT_MAX_A;

	SettingBoundStrs_[FLOW_PATTERN_BASED_IE_MAX_ID] = PromptStrs::HB_FLOW_PATTERN_BASED_IE_MAX_A;
	SettingBoundStrs_[FLOW_PATTERN_BASED_TI_MAX_ID] = PromptStrs::HB_FLOW_PATTERN_BASED_TI_MAX_A;
	SettingBoundStrs_[FLOW_PATTERN_BASED_TI_MIN_ID] = PromptStrs::HB_FLOW_PATTERN_BASED_TI_MIN_A;
	SettingBoundStrs_[FLOW_PATTERN_BASED_TE_MIN_ID] = PromptStrs::HB_FLOW_PATTERN_BASED_TE_MIN_A;

	SettingBoundStrs_[FLOW_PLOT_SCALE_MAX_ID] = PromptStrs::HB_FLOW_PLOT_SCALE_MAX_A;
	SettingBoundStrs_[FLOW_PLOT_SCALE_MIN_ID] = PromptStrs::HB_FLOW_PLOT_SCALE_MIN_A;

	SettingBoundStrs_[FLOW_SENS_MIN_ID] = PromptStrs::HB_FLOW_SENS_MIN_A;
	SettingBoundStrs_[FLOW_SENS_MAX_ID] = PromptStrs::HB_FLOW_SENS_MAX_A;

	SettingBoundStrs_[HIGH_CIRC_MIN_BASED_LOW_ID] = PromptStrs::HB_HIGH_CIRC_MIN_BASED_LOW_A;
	SettingBoundStrs_[HIGH_CIRC_MIN_BASED_PEEP_ID] = PromptStrs::HB_HIGH_CIRC_MIN_BASED_PEEP_A;
	SettingBoundStrs_[HIGH_CIRC_MIN_BASED_PEEPL_ID] = PromptStrs::HB_HIGH_CIRC_MIN_BASED_PEEPL_A;
	SettingBoundStrs_[HIGH_CIRC_MIN_BASED_PSUPP_PEEP_ID] = PromptStrs::HB_HIGH_CIRC_MIN_BASED_PSUPP_PEEP_A;
	SettingBoundStrs_[HIGH_CIRC_MIN_BASED_PSUPP_PEEPL_ID] = PromptStrs::HB_HIGH_CIRC_MIN_BASED_PSUPP_PEEPL_A;
	SettingBoundStrs_[HIGH_CIRC_MIN_BASED_INSP_PRESS_PEEP_ID] = PromptStrs::HB_HIGH_CIRC_MIN_BASED_INSP_PRESS_PEEP_A;
	SettingBoundStrs_[HIGH_CIRC_MIN_BASED_PEEPH_ID] = PromptStrs::HB_HIGH_CIRC_MIN_BASED_PEEPH_A;
	SettingBoundStrs_[HIGH_CIRC_MAX_ID] = PromptStrs::HB_HIGH_CIRC_MAX_A;

	SettingBoundStrs_[HIGH_EXH_MIN_VOL_MIN_ID] = PromptStrs::HB_HIGH_EXH_MIN_VOL_MIN_A;
	SettingBoundStrs_[HIGH_EXH_MIN_VOL_MIN_BASED_LOW_ID] = PromptStrs::HB_HIGH_EXH_MIN_VOL_MIN_BASED_LOW_A;
	SettingBoundStrs_[HIGH_EXH_MIN_VOL_MAX_ID] = PromptStrs::HB_HIGH_EXH_MIN_VOL_MAX_A;

	SettingBoundStrs_[HIGH_EXH_TIDAL_VOL_MIN_ID] = PromptStrs::HB_HIGH_EXH_TIDAL_VOL_MIN_A;
	SettingBoundStrs_[HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_MAND_ID] = PromptStrs::HB_HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_MAND_A;
	SettingBoundStrs_[HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_SPONT_ID] = PromptStrs::HB_HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_SPONT_A;
	SettingBoundStrs_[HIGH_EXH_TIDAL_VOL_MAX_ID] = PromptStrs::HB_HIGH_EXH_TIDAL_VOL_MAX_A;

	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MIN_MAND_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MIN_MAND_A;
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_MAND_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_MAND_A;
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_MAND_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_MAND_A;
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MAX_MAND_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MAX_MAND_A;

	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MIN_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MIN_A;
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_A;
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_A;
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_A;
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MAX_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MAX_A;

	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MIN_SPONT_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MIN_SPONT_A;
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_SPONT_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_SPONT_A;
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_SPONT_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_SPONT_A;
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MAX_SPONT_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MAX_SPONT_A;

	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL_ID] = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL;  
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL_MAND_ID]  = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL_MAND;   
	SettingBoundStrs_[HIGH_INSP_TIDAL_VOL_MIN_BASED_VOL_SUP_SPONT_ID]  = PromptStrs::HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_VOL_SUP_SPONT;   


	SettingBoundStrs_[HIGH_RESP_RATE_MIN_ID] = PromptStrs::HB_HIGH_RESP_RATE_MIN_A;
	SettingBoundStrs_[HIGH_RESP_RATE_MAX_ID] = PromptStrs::HB_HIGH_RESP_RATE_MAX_A;

	SettingBoundStrs_[HL_RATIO_SOFT_MAX_ID] = PromptStrs::HB_HL_RATIO_SOFT_MAX_A;
	SettingBoundStrs_[HL_RATIO_MAX_ID] = PromptStrs::HB_HL_RATIO_MAX_A;

	SettingBoundStrs_[HUMID_VOLUME_MIN_ID] = PromptStrs::HB_HUMID_VOLUME_MIN_A;
	SettingBoundStrs_[HUMID_VOLUME_MAX_ID] = PromptStrs::HB_HUMID_VOLUME_MAX_A;

	SettingBoundStrs_[IBW_MIN_BASED_CCT_TYPE_ID] = PromptStrs::HB_IBW_MIN_BASED_CCT_TYPE_A;
	SettingBoundStrs_[IBW_SOFT_MIN_BASED_CCT_TYPE_ID] = PromptStrs::HB_IBW_SOFT_MIN_BASED_CCT_TYPE_A;
	SettingBoundStrs_[IBW_SOFT_MAX_BASED_CCT_TYPE_ID] = PromptStrs::HB_IBW_SOFT_MAX_BASED_CCT_TYPE_A;
	SettingBoundStrs_[IBW_MAX_BASED_CCT_TYPE_ID] = PromptStrs::HB_IBW_MAX_BASED_CCT_TYPE_A;

	SettingBoundStrs_[IE_RATIO_SOFT_MAX_ID] = PromptStrs::HB_IE_RATIO_SOFT_MAX_A;
	SettingBoundStrs_[IE_RATIO_MAX_ID] = PromptStrs::HB_IE_RATIO_MAX_A;

	SettingBoundStrs_[INSP_PRESS_MIN_ID] = PromptStrs::HB_INSP_PRESS_MIN_A;
	SettingBoundStrs_[INSP_PRESS_MAX_BASED_PEEP_ID] = PromptStrs::HB_INSP_PRESS_MAX_BASED_PEEP_A;
	SettingBoundStrs_[INSP_PRESS_MAX_BASED_PCIRC_PEEP_ID] = PromptStrs::HB_INSP_PRESS_MAX_BASED_PCIRC_PEEP_A;
	SettingBoundStrs_[INSP_PRESS_MAX_ID] = PromptStrs::HB_INSP_PRESS_MAX_A;

	SettingBoundStrs_[INSP_TIME_MIN_ID] = PromptStrs::HB_INSP_TIME_MIN_A;
	SettingBoundStrs_[INSP_TIME_MIN_BASED_PLAT_TIME_ID] = PromptStrs::HB_INSP_TIME_MIN_BASED_PLAT_TIME_A;
	SettingBoundStrs_[INSP_TIME_MAX_ID] = PromptStrs::HB_INSP_TIME_MAX_A;

	SettingBoundStrs_[LOW_CIRC_MIN_ID] = PromptStrs::HB_LOW_CIRC_MIN_A;
	SettingBoundStrs_[LOW_CIRC_MIN_BASED_PEEP_ID] = PromptStrs::HB_LOW_CIRC_MIN_BASED_PEEP_A;
	SettingBoundStrs_[LOW_CIRC_MAX_BASED_HIGH_ID] = PromptStrs::HB_LOW_CIRC_MAX_BASED_HIGH_A;
	SettingBoundStrs_[LOW_CIRC_MAX_ID] = PromptStrs::HB_LOW_CIRC_MAX_A;

	SettingBoundStrs_[LOW_EXH_MAND_TIDAL_MIN_ID] = PromptStrs::HB_LOW_EXH_MAND_TIDAL_MIN_A;
	SettingBoundStrs_[LOW_EXH_MAND_TIDAL_MAX_BASED_HIGH_ID] = PromptStrs::HB_LOW_EXH_MAND_TIDAL_MAX_BASED_HIGH_A;
	SettingBoundStrs_[LOW_EXH_MAND_TIDAL_MAX_ID] = PromptStrs::HB_LOW_EXH_MAND_TIDAL_MAX_A;

	SettingBoundStrs_[LOW_EXH_MIN_VOL_MIN_ID] = PromptStrs::HB_LOW_EXH_MIN_VOL_MIN_A;
	SettingBoundStrs_[LOW_EXH_MIN_VOL_SOFT_MIN_ID] = PromptStrs::HB_LOW_EXH_MIN_VOL_SOFT_MIN_A;
	SettingBoundStrs_[LOW_EXH_MIN_VOL_MAX_BASED_HIGH_ID] = PromptStrs::HB_LOW_EXH_MIN_VOL_MAX_BASED_HIGH_A;
	SettingBoundStrs_[LOW_EXH_MIN_VOL_MAX_ID] = PromptStrs::HB_LOW_EXH_MIN_VOL_MAX_A;

	SettingBoundStrs_[LOW_EXH_SPONT_TIDAL_MIN_ID] = PromptStrs::HB_LOW_EXH_SPONT_TIDAL_MIN_A;
	SettingBoundStrs_[LOW_EXH_SPONT_TIDAL_MAX_BASED_HIGH_ID] = PromptStrs::HB_LOW_EXH_SPONT_TIDAL_MAX_BASED_HIGH_A;
	SettingBoundStrs_[LOW_EXH_SPONT_TIDAL_MAX_ID] = PromptStrs::HB_LOW_EXH_SPONT_TIDAL_MAX_A;

	SettingBoundStrs_[O2_PERCENT_MIN_ID] = PromptStrs::HB_O2_PERCENT_MIN_A;
	SettingBoundStrs_[O2_PERCENT_MAX_ID] = PromptStrs::HB_O2_PERCENT_MAX_A;

	SettingBoundStrs_[PEAK_INSP_FLOW_MIN_BASED_CCT_TYPE_ID] = PromptStrs::HB_PEAK_INSP_FLOW_MIN_BASED_CCT_TYPE_A;
	SettingBoundStrs_[PEAK_INSP_FLOW_MAX_BASED_CCT_TYPE_ID] = PromptStrs::HB_PEAK_INSP_FLOW_MAX_BASED_CCT_TYPE_A;
	SettingBoundStrs_[PEAK_INSP_FLOW_MIN_BASED_VCP_ID] = PromptStrs::HB_PEAK_INSP_FLOW_MIN_BASED_VCP_A;
	SettingBoundStrs_[PEAK_INSP_FLOW_MAX_BASED_VCP_ID] = PromptStrs::HB_PEAK_INSP_FLOW_MAX_BASED_VCP_A;

	SettingBoundStrs_[PERCENT_SUPPORT_MIN_ID]      = PromptStrs::HB_PERCENT_SUPPORT_MIN_A;
	SettingBoundStrs_[PERCENT_SUPPORT_MAX_ID]      = PromptStrs::HB_PERCENT_SUPPORT_MAX_A;
	SettingBoundStrs_[PERCENT_SUPPORT_SOFT_MAX_ID] = PromptStrs::HB_PERCENT_SUPPORT_SOFT_MAX_A;

	SettingBoundStrs_[PLATEAU_MIN_ID] = PromptStrs::HB_PLATEAU_MIN_A;
	SettingBoundStrs_[PLATEAU_MAX_ID] = PromptStrs::HB_PLATEAU_MAX_A;

	SettingBoundStrs_[PEEP_MIN_ID] = PromptStrs::HB_PEEP_MIN_A;
	SettingBoundStrs_[PEEP_SOFT_MIN_BASED_ACCEPTED_ID] = PromptStrs::HB_PEEP_SOFT_DELTA_LIMIT_A;
	SettingBoundStrs_[PEEP_MAX_BASED_PCIRC_ID] = PromptStrs::HB_PEEP_MAX_BASED_PCIRC_A;
	SettingBoundStrs_[PEEP_MAX_BASED_PCIRC_PI_ID] = PromptStrs::HB_PEEP_MAX_BASED_PCIRC_PI_A;
	SettingBoundStrs_[PEEP_MAX_BASED_PI_ID] = PromptStrs::HB_PEEP_MAX_BASED_PI_A;
	SettingBoundStrs_[PEEP_MAX_BASED_PCIRC_PSUPP_ID] = PromptStrs::HB_PEEP_MAX_BASED_PCIRC_PSUPP_A;
	SettingBoundStrs_[PEEP_MAX_BASED_PSUPP_ID] = PromptStrs::HB_PEEP_MAX_BASED_PSUPP_A;
	SettingBoundStrs_[PEEP_SOFT_MAX_BASED_ACCEPTED_ID] = PromptStrs::HB_PEEP_SOFT_DELTA_LIMIT_A;
	SettingBoundStrs_[PEEP_MAX_ID] = PromptStrs::HB_PEEP_MAX_A;

	SettingBoundStrs_[PEEP_HIGH_MIN_ID] = PromptStrs::HB_PEEP_HIGH_MIN_A;
	SettingBoundStrs_[PEEP_HIGH_MIN_BASED_PEEPL_ID] = PromptStrs::HB_PEEP_HIGH_MIN_BASED_PEEPL_A;
	SettingBoundStrs_[PEEP_HIGH_MAX_BASED_PCIRC_ID] = PromptStrs::HB_PEEP_HIGH_MAX_BASED_PCIRC_A;
	SettingBoundStrs_[PEEP_HIGH_MAX_ID] = PromptStrs::HB_PEEP_HIGH_MAX_A;

	SettingBoundStrs_[PEEP_HIGH_TIME_MIN_ID] = PromptStrs::HB_PEEP_HIGH_TIME_MIN_A;
	SettingBoundStrs_[PEEP_HIGH_TIME_MAX_ID] = PromptStrs::HB_PEEP_HIGH_TIME_MAX_A;

	SettingBoundStrs_[PEEP_LOW_MIN_ID] = PromptStrs::HB_PEEP_LOW_MIN_A;
	SettingBoundStrs_[PEEP_LOW_MAX_BASED_PCIRC_ID] = PromptStrs::HB_PEEP_LOW_MAX_BASED_PCIRC_A;
	SettingBoundStrs_[PEEP_LOW_MAX_BASED_PCIRC_PSUPP_ID] = PromptStrs::HB_PEEP_LOW_MAX_BASED_PCIRC_PSUPP_A;
	SettingBoundStrs_[PEEP_LOW_MAX_BASED_PSUPP_ID] = PromptStrs::HB_PEEP_LOW_MAX_BASED_PSUPP_A;
	SettingBoundStrs_[PEEP_LOW_MAX_BASED_PEEPH_ID] = PromptStrs::HB_PEEP_LOW_MAX_BASED_PEEPH_A;
	SettingBoundStrs_[PEEP_LOW_MAX_ID] = PromptStrs::HB_PEEP_LOW_MAX_A;

	SettingBoundStrs_[PEEP_LOW_TIME_MIN_ID] = PromptStrs::HB_PEEP_LOW_TIME_MIN_A;

	SettingBoundStrs_[PRESS_PLOT_SCALE_MAX_ID] = PromptStrs::HB_PRESS_PLOT_SCALE_MAX_A;
	SettingBoundStrs_[PRESS_PLOT_SCALE_MIN_ID] = PromptStrs::HB_PRESS_PLOT_SCALE_MIN_A;

	SettingBoundStrs_[PRESS_SENS_MIN_ID] = PromptStrs::HB_PRESS_SENS_MIN_A;
	SettingBoundStrs_[PRESS_SENS_MAX_ID] = PromptStrs::HB_PRESS_SENS_MAX_A;

	SettingBoundStrs_[PRESS_SUPP_MIN_ID] = PromptStrs::HB_PRESS_SUPP_MIN_A;
	SettingBoundStrs_[PRESS_SUPP_MAX_BASED_PEEP_ID] = PromptStrs::HB_PRESS_SUPP_MAX_BASED_PEEP_A;
	SettingBoundStrs_[PRESS_SUPP_MAX_BASED_PEEPL_ID] = PromptStrs::HB_PRESS_SUPP_MAX_BASED_PEEPL_A;
	SettingBoundStrs_[PRESS_SUPP_MAX_BASED_PCIRC_PEEP_ID] = PromptStrs::HB_PRESS_SUPP_MAX_BASED_PCIRC_PEEP_A;
	SettingBoundStrs_[PRESS_SUPP_MAX_BASED_PCIRC_PEEPL_ID] = PromptStrs::HB_PRESS_SUPP_MAX_BASED_PCIRC_PEEPL_A;
	SettingBoundStrs_[PRESS_SUPP_MAX_ID] = PromptStrs::HB_PRESS_SUPP_MAX_A;

	SettingBoundStrs_[PRESS_VOL_LOOP_BASE_MIN_ID] = PromptStrs::HB_PRESS_VOL_LOOP_BASE_MIN_A;
	SettingBoundStrs_[PRESS_VOL_LOOP_BASE_MAX_ID] = PromptStrs::HB_PRESS_VOL_LOOP_BASE_MAX_A;

	SettingBoundStrs_[RESP_RATE_MIN_ID] = PromptStrs::HB_RESP_RATE_MIN_A;
	SettingBoundStrs_[RESP_RATE_MAX_ID] = PromptStrs::HB_RESP_RATE_MAX_A;

	SettingBoundStrs_[TIDAL_VOL_MIN_ID] = PromptStrs::HB_TIDAL_VOL_MIN_A;
	SettingBoundStrs_[TIDAL_VOL_MIN_BASED_IBW_ID] = PromptStrs::HB_TIDAL_VOL_MIN_BASED_IBW_A;
	SettingBoundStrs_[TIDAL_VOL_SOFT_MIN_BASED_IBW_ID] = PromptStrs::HB_TIDAL_VOL_SOFT_MIN_BASED_IBW_A;
	SettingBoundStrs_[TIDAL_VOL_SOFT_MAX_BASED_IBW_ID] = PromptStrs::HB_TIDAL_VOL_SOFT_MAX_BASED_IBW_A;
	SettingBoundStrs_[TIDAL_VOL_MAX_BASED_IBW_ID] = PromptStrs::HB_TIDAL_VOL_MAX_BASED_IBW_A;
	SettingBoundStrs_[TIDAL_VOL_MAX_ID] = PromptStrs::HB_TIDAL_VOL_MAX_A;

	SettingBoundStrs_[TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL_ID] = PromptStrs::HB_TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL;  
	SettingBoundStrs_[TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL_MAND_ID]  = PromptStrs::HB_TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL_MAND;   

	SettingBoundStrs_[TIME_PLOT_SCALE_MAX_ID] = PromptStrs::HB_TIME_PLOT_SCALE_MAX_A;
	SettingBoundStrs_[TIME_PLOT_SCALE_MIN_ID] = PromptStrs::HB_TIME_PLOT_SCALE_MIN_A;

	SettingBoundStrs_[TUBE_ID_MIN_ID] = PromptStrs::HB_TUBE_ID_MIN_A;
	SettingBoundStrs_[TUBE_ID_SOFT_MIN_ID] = PromptStrs::HB_TUBE_ID_SOFT_MIN_A;
	SettingBoundStrs_[TUBE_ID_SOFT_MAX_ID] = PromptStrs::HB_TUBE_ID_SOFT_MAX_A;
	SettingBoundStrs_[TUBE_ID_MAX_ID] = PromptStrs::HB_TUBE_ID_MAX_A;

	SettingBoundStrs_[VOLUME_PLOT_SCALE_MAX_ID] = PromptStrs::HB_VOLUME_PLOT_SCALE_MAX_A;
	SettingBoundStrs_[VOLUME_PLOT_SCALE_MIN_ID] = PromptStrs::HB_VOLUME_PLOT_SCALE_MIN_A;

	SettingBoundStrs_[VOLUME_SUPP_MIN_ID] = PromptStrs::HB_VOLUME_SUPP_MIN_A;
	SettingBoundStrs_[VOLUME_SUPP_MIN_BASED_IBW_ID] = PromptStrs::HB_VOLUME_SUPP_MIN_BASED_IBW_A;
	SettingBoundStrs_[VOLUME_SUPP_SOFT_MIN_BASED_IBW_ID] = PromptStrs::HB_VOLUME_SUPP_SOFT_MIN_BASED_IBW_A;
	SettingBoundStrs_[VOLUME_SUPP_SOFT_MAX_BASED_IBW_ID] = PromptStrs::HB_VOLUME_SUPP_SOFT_MAX_BASED_IBW_A;
	SettingBoundStrs_[VOLUME_SUPP_MAX_BASED_IBW_ID] = PromptStrs::HB_VOLUME_SUPP_MAX_BASED_IBW_A;
	SettingBoundStrs_[VOLUME_SUPP_MAX_ID] = PromptStrs::HB_VOLUME_SUPP_MAX_A;

	SettingBoundStrs_[VOL_SUP_MAX_BASED_HIGH_INSP_TIDAL_VOL_SPONT_ID]  = PromptStrs::HB_VOL_SUP_MAX_BASED_HIGH_INSP_TIDAL_VOL;   


	SettingBoundStrs_[YEAR_MAX_ID] = PromptStrs::HB_YEAR_MAX_A;
	SettingBoundStrs_[YEAR_MIN_ID] = PromptStrs::HB_YEAR_MIN_A;

	SettingBoundStrs_[HIGH_SPONT_INSP_TIME_MIN_ID] = PromptStrs::HB_HIGH_SPONT_INSP_TIME_MIN_A;
	SettingBoundStrs_[HIGH_SPONT_INSP_TIME_MAX_BASED_IBW_ID] = PromptStrs::HB_HIGH_SPONT_INSP_TIME_MAX_BASED_IBW_A;

	SettingBoundStrs_[TREND_CURSOR_POSITION_MIN_ID] = PromptStrs::HB_TREND_CURSOR_POSITION_MIN_ID;
	SettingBoundStrs_[TREND_CURSOR_POSITION_MAX_ID] = PromptStrs::HB_TREND_CURSOR_POSITION_MAX_ID;

	// changeable (after new-patient setup) IBW bounds
	SettingBoundStrs_[IBW_MAX_BASED_ON_APNEA_TIDAL_VOL_ID] = PromptStrs::HB_IBW_MAX_BASED_ON_APNEA_TIDAL_VOL_ID;
	SettingBoundStrs_[IBW_MIN_BASED_ON_APNEA_TIDAL_VOL_ID] = PromptStrs::HB_IBW_MIN_BASED_ON_APNEA_TIDAL_VOL_ID;
	SettingBoundStrs_[IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID] = PromptStrs::HB_IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID;
	SettingBoundStrs_[IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID] = PromptStrs::HB_IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID;
	SettingBoundStrs_[IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_ID] = PromptStrs::HB_IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_ID;
	SettingBoundStrs_[IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_ID] = PromptStrs::HB_IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_ID;
	SettingBoundStrs_[IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID] = PromptStrs::HB_IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID;
	SettingBoundStrs_[IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID] = PromptStrs::HB_IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID;
	SettingBoundStrs_[IBW_MAX_BASED_ON_TIDAL_VOL_ID] = PromptStrs::HB_IBW_MAX_BASED_ON_TIDAL_VOL_ID;
	SettingBoundStrs_[IBW_MIN_BASED_ON_TIDAL_VOL_ID] = PromptStrs::HB_IBW_MIN_BASED_ON_TIDAL_VOL_ID;
	SettingBoundStrs_[IBW_MAX_BASED_ON_VOLUME_SUPP_ID] = PromptStrs::HB_IBW_MAX_BASED_ON_VOLUME_SUPP_ID;
	SettingBoundStrs_[IBW_MIN_BASED_ON_VOLUME_SUPP_ID] = PromptStrs::HB_IBW_MIN_BASED_ON_VOLUME_SUPP_ID;
	SettingBoundStrs_[IBW_MIN_BASED_ON_ATC_ID] = PromptStrs::HB_IBW_MIN_BASED_ON_ATC_ID;
	SettingBoundStrs_[IBW_MIN_BASED_ON_PAV_ID] = PromptStrs::HB_IBW_MIN_BASED_ON_PAV_ID;

#ifdef SIGMA_DEVELOPMENT
	// Do some safe assertions that all entries in the array have been filled
	for ( i = 0; i < NUM_SETTING_BOUND_IDS; i++ )
	{
		SAFE_AUX_CLASS_ASSERTION((NULL_STRING_ID != SettingBoundStrs_[i]),
								 i);
	}
#endif // SIGMA_DEVELOPMENT
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSettingBoundString [public, static]
//
//@ Interface-Description
// Returns the setting bound advisory message string id for a particular
// setting bound id.  Passed 'settingBoundId'.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the specified setting bound id is null then return the null string
// id otherwise use the setting bound id to index into the string id array
// SettingBoundStrs_[] and return the value there.
//---------------------------------------------------------------------
//@ PreCondition
// The specified setting bound id must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

StringId
BoundStrs::GetSettingBoundString(SettingBoundId settingBoundId)
{
	CALL_TRACE("BoundStrs::GetSettingBoundString(SettingBoundId settingBoundId)");

	CLASS_PRE_CONDITION(((settingBoundId >= 0) &&
						 (settingBoundId < NUM_SETTING_BOUND_IDS))
						|| (settingBoundId == NULL_SETTING_BOUND_ID));

	// Return appropriate string id for the setting bound
	if ( settingBoundId == NULL_SETTING_BOUND_ID )
	{													// $[TI1]
		return(NULL_STRING_ID);
	}
	else
	{													// $[TI2]
		return(SettingBoundStrs_[settingBoundId]);
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
BoundStrs::SoftFault(const SoftFaultID  softFaultID,
					 const Uint32       lineNumber,
					 const char*        pFileName,
					 const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, BOUNDSTRS,
							lineNumber, pFileName, pPredicate);
}
