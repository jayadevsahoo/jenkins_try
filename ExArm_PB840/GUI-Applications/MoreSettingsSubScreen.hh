#ifndef MoreSettingsSubScreen_HH
#define MoreSettingsSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MoreSettingsSubScreen - The subscreen selected by the
// More Settings button in the Lower Other Screens subscreen.
// Allows adjusting of the Expiratory Sensitivity, Humidification Type and
// O2 Sensor Enable settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MoreSettingsSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S
//  Description:
//      Modifications to provide for transitioning of tube 
//		I.D. to new patient value when spontaneous type is changed to 
//		PAV with an incompatible tube I.D.. This includes changes to 
//		the user interface to verify transitioned value with flashing
//		verify arrow icon. Change better supports verification icon
//		used for tube type and I.D. as well as humidification type and
//		volume.
//
//  Revision: 009   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 008   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 007   By: gdc   Date:  15-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changed disconnect sensitivity setting to a NumericLimitSettingButton
//      allowing it to be turned OFF (for NIV)
//
//  Revision: 006   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 	Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 005  By:  hhd	   Date:  16-Nov-1999    DCS Number:  5412
//  Project:  ATC
//  Description:
//	Modified to add attention icon to Humid. Volume button when Humid. Type changes.	
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By:  hhd	   Date:  27-Jan-1999    DCS Number: 
//  Project:  ATC
//  Description:
//		Initial version.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "BatchSettingsSubScreen.hh"
#include "DiscreteSettingButton.hh"
#include "NumericSettingButton.hh"
#include "NumericLimitSettingButton.hh"
#include "SubScreenTitleArea.hh"
#include "OffKeysSubScreen.hh"
//@ End-Usage

class MoreSettingsSubScreen : public BatchSettingsSubScreen
{
public:
	MoreSettingsSubScreen(SubScreenArea *pSubScreenArea);
	~MoreSettingsSubScreen(void);

	// ButtonTarget virtual
	virtual void buttonDownHappened(Button *pButton, Boolean isByOperatorAction);

	// Overload SubScreen methods
	virtual void acceptHappened(void);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
protected:
	// BatchSettingsSubScreen virtual method...
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ tranistionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									  );

private:
	// these methods are purposely declared, but not implemented...
	MoreSettingsSubScreen(void);		       		// not implemented..
	MoreSettingsSubScreen(const MoreSettingsSubScreen&);	// not implemented..
	void operator=(const MoreSettingsSubScreen&);  		// not implemented..

	// Constant: MORE_SETTINGS_MAX_BUTTONS_ 
	// Maximum number of buttons that can be displayed in this subscreen.
	// Must be modified when new buttons are added.
	enum { MORE_SETTINGS_MAX_BUTTONS_ = 7 };

	//@ Data-Member: verifySettingsMsg_
	// This message appears when any of the visible setting buttons
	// are in the verification-needed state.
	TextField verifySettingsMsg_;

	//@ Data-Member: verifySettingsIcon_
	// This icon appears when any of the visible setting buttons
	// are in the verification-needed state.
	Bitmap verifySettingsIcon_;

	//@ Data-Member: disconnectionSensitivityButton_
	// The disconnection sensitivity setting button.
	NumericLimitSettingButton disconnectionSensitivityButton_;

	//@ Data-Member: humidificationTypeButton_
	// The Humidification Type setting button.
	DiscreteSettingButton humidificationTypeButton_;

	//@ Data-Member: humidifierVolumeButton_
	// The Humidifier Volume setting button.
	NumericSettingButton humidifierVolumeButton_;

	//@ Data-Member: o2SensorEnableButton_
	// The FiO2 Enabled setting button.
	DiscreteSettingButton o2SensorEnableButton_;
	
	//@ Data-Member: tubeTypeButton_
	// The Patient Tube Type setting button.
	DiscreteSettingButton tubeTypeButton_;

	//@ Data-Member: tubeIdButton_
	// The Tube Id setting button.
	NumericSettingButton tubeIdButton_;

	//@ Data-Member: leakCompEnableButton_
	// The leak compensation enable button.
	DiscreteSettingButton leakCompEnableButton_;

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member:  arrSettingButtonPtrs_
	// The subscreen buttons.
	SettingButton*  arrSettingButtonPtrs_[MORE_SETTINGS_MAX_BUTTONS_ + 1];

	//@ Data-Member:  previousfio2Enabled_
	// Stores the previous FIO2 enabled setting
	DiscreteValue previousfio2Enabled_;
};


#endif // MoreSettingsSubScreen_HH 
