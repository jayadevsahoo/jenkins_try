// e600_BenchmarkDlg.cpp : implementation file
//

#include "stdafx.h"
#include "e600_Benchmark.h"
#include "e600_BenchmarkDlg.h"
#include "AioDioDriver.h"
#include "Plot.h"
#include "GraphObjectManager.h"
#include "RgnHelper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Ce600_BenchmarkDlg dialog

Ce600_BenchmarkDlg::Ce600_BenchmarkDlg(CWnd* pParent /*=NULL*/)
: CDialog(Ce600_BenchmarkDlg::IDD, pParent), m_MMTimer( MM_TIMER_PERIOD )
, m_dwLastMonitorTime( 0 ), m_bMonitorRedraw( true )/*, m_MonitorRedrawCount(0)*/

#ifdef DIRECT_DRAW
, m_pDDCanvas( NULL )
#endif

{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Ce600_BenchmarkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Ce600_BenchmarkDlg, CDialog)
#if defined(_DEVICE_RESOLUTION_AWARE) && !defined(WIN32_PLATFORM_WFSP)
	ON_WM_SIZE()
#endif
	//}}AFX_MSG_MAP
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_MESSAGE(WM_WHEEL_UPDATE, OnVentWheelUpdate)
	ON_MESSAGE(WM_DATAGEN_TIMER, OnDataGenTimer)
	ON_MESSAGE(WM_MONITOR_REAL_TIMER, OnMonitorRealTimer)
	ON_MESSAGE(WM_MYINVALIDATE_TIMER, OnMyInvalidateTimer)
	ON_COMMAND_RANGE(IDC_CHECK1, IDC_CHECK10, OnCheckButtons)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// Ce600_BenchmarkDlg message handlers

BOOL Ce600_BenchmarkDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	// when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// Hide the Windows task bar
	HWND taskBarHwnd;
	taskBarHwnd = ::FindWindow(_T("HHTaskBar"), NULL);
	::ShowWindow(taskBarHwnd, SW_SHOW);

	// Get the screen dimension

	m_rcDesktop.left = GetSystemMetrics(SM_XVIRTUALSCREEN);
	m_rcDesktop.right = m_rcDesktop.left + GetSystemMetrics(SM_CXVIRTUALSCREEN);
	m_rcDesktop.top = GetSystemMetrics(SM_YVIRTUALSCREEN);
	m_rcDesktop.bottom = m_rcDesktop.top + GetSystemMetrics(SM_CYVIRTUALSCREEN);

	// Full screen current Window
	SetWindowPos( NULL,	m_rcDesktop.left,	m_rcDesktop.top, m_rcDesktop.right-m_rcDesktop.left,
		m_rcDesktop.bottom-m_rcDesktop.top,	SWP_NOZORDER );

	// initialize these canvas parameters
	m_pCurrentCheckBtn = NULL;
	m_monitorRows = 2;
	m_monitorColumns = 8;
	
	m_baseX = 0;
	m_baseY = 140;

	m_cellWidth = 128;
	m_cellHeight = 72;
	m_paintCount = 0;
	m_dwTickCount1ST = 0;
	m_bIsLButtonDown = false;

	m_monitorsAreaHeight = m_monitorRows * m_cellHeight;

	// layout the buttons and change the buttons' font
	layoutButtons_();

	m_GlobalInvalidateRgn.CreateRectRgn(0, 0, 0, 0);
	m_MonitorDataRgn.CreateRectRgn(0, 0, 0, 0);

	initMonitorsRect_();

	//m_pPlot1 = new Plot(300, 150, 10, 10);
	int nTimeLen = 3;
	for (int i=0; i<PLOT_COUNT; i++)
	{
		Plot* pPlot = new Plot(m_hWnd, PLOT_WIDTH, PLOT_HEIGHT, 3, 50);
		m_Plots.push_back(pPlot);
	}

	// initialize brush and pens
	m_bkGndBrush.CreateSolidBrush( MonitorBkgndColor );
	m_gridVerticalPen.CreatePen(PS_SOLID, 4, MonitorGridColor );
	m_gridHorizontalPen.CreatePen(PS_SOLID, 2, MonitorGridColor );

	/////////////////////////////////////// create memory dc and bmp buffers ////////////////////////////////////////////

	CDC* pDC = GetDC();

	m_dcMemMonitorArea.CreateCompatibleDC( pDC );
	m_bmpMonitorArea.CreateCompatibleBitmap( pDC, m_rcDesktop.Width(), m_monitorsAreaHeight);
	m_dcMemMonitorArea.SelectObject(&m_bmpMonitorArea);

	m_dcMemHeaderArea.CreateCompatibleDC( pDC );
	m_bmpHeaderArea.CreateCompatibleBitmap( pDC, m_rcDesktop.Width(), m_baseY);
	m_dcMemHeaderArea.SelectObject(&m_bmpHeaderArea);

	m_dcMemMonitorBkgnd.CreateCompatibleDC( pDC );
	m_bmpMonitorBkgnd.CreateCompatibleBitmap(pDC, m_rcDesktop.Width(), m_monitorsAreaHeight );
	m_dcMemMonitorBkgnd.SelectObject(&m_bmpMonitorBkgnd);

	ReleaseDC(pDC);

#ifdef DIRECT_DRAW
	// Initialize direct draw
	m_pDDCanvas = new DDrawCanvas( m_hWnd );
	m_pDDCanvas->createOffscreenSurface(m_rcDesktop.Width(), m_monitorsAreaHeight, m_pMonitorSurface);
	m_pPrimarySurface = m_pDDCanvas->getPrimarySurface();
#endif

	//////////////////////////////////////////// prepare monitor canvas //////////////////////////////////////////////////
	// with grid and titile text
	prepareMonitorCanvas_();

	//////////////////////////////////////////// begin knob thread ///////////////////////////////////////////////////////

	// begin knob event thread
	AfxBeginThread(KnobEventDetectThreadProc, m_hWnd);

	//////////////////////////////////////////// start timers ////////////////////////////////////////////////////////////
	m_MMTimer.setEvent( DATA_SPEED, DataGenTimerCallback, (DWORD)m_hWnd );
	m_MMTimer.setEvent( 333, MonitorRealTimerCallback, (DWORD)m_hWnd );
	m_MMTimer.setEvent( DRAW_CYCLE, InvalidateRealTimerCallback, (DWORD)m_hWnd );

	m_dwTickCount1ST = GetTickCount();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

#if defined(_DEVICE_RESOLUTION_AWARE) && !defined(WIN32_PLATFORM_WFSP)
void Ce600_BenchmarkDlg::OnSize(UINT /*nType*/, int /*cx*/, int /*cy*/)
{
	if (AfxIsDRAEnabled())
	{
		DRA::RelayoutDialog(
			AfxGetResourceHandle(), 
			this->m_hWnd, 
			DRA::GetDisplayMode() != DRA::Portrait ? 
			MAKEINTRESOURCE(IDD_E600_BENCHMARK_DIALOG_WIDE) : 
		MAKEINTRESOURCE(IDD_E600_BENCHMARK_DIALOG));
	}
}
#endif

Ce600_BenchmarkDlg::~Ce600_BenchmarkDlg()
{
	m_GlobalInvalidateRgn.DeleteObject();
	m_MonitorDataRgn.DeleteObject();
}


void Ce600_BenchmarkDlg::OnPaint()
{	
	CPaintDC dc(this); // device context for painting
	
	////////////////////////////////////////// draw monitors /////////////////////////////////////
	if ( SHOW_MONITORS && m_bMonitorRedraw )
	{
		m_strMonitorDrawPeriod.Format(L"MonitorRedrawPeriod: %d", GetTickCount() - m_dwLastMonitorTime);

		drawMonitorData_();

#ifdef DIRECT_DRAW
		CRect rc;
		SetRect(&rc, m_baseX, m_baseY, m_baseX+m_rcDesktop.Width(), m_baseY+m_monitorsAreaHeight);
		ClientToScreen( &rc );
		m_pPrimarySurface->Blt( &rc, m_pMonitorSurface, NULL, DDBLT_WAITNOTBUSY, NULL );
#else
		dc.BitBlt(m_baseX, m_baseY, m_rcDesktop.Width(), m_monitorsAreaHeight, &m_dcMemMonitorArea, 0, 0, SRCCOPY);
#endif
		m_bMonitorRedraw = false;
		m_dwLastMonitorTime = GetTickCount();
		
	}
	

// 	m_MonitorRedrawCount += 1;
// 	m_MonitorRedrawCount = m_MonitorRedrawCount % 6;

	// paint background
	m_dcMemHeaderArea.FillSolidRect(0, 0, m_rcDesktop.Width(), m_baseY, MonitorBkgndColor);

	/////////////////////////////////////////// calculate Frame per second /////////////////////////////////////////
	m_paintCount ++;
	DWORD tcSpan = GetTickCount() - m_dwTickCount1ST;

	double paintRate = (double)m_paintCount/ ((double)tcSpan * 0.001);
	CString strPaintRate;
	strPaintRate.Format(_T("FPS: %f"), paintRate);
	CRect rcRate(10, 10, 300, 100);
	m_dcMemHeaderArea.SetBkMode(TRANSPARENT);
	m_dcMemHeaderArea.DrawText(strPaintRate, &rcRate, DT_LEFT|DT_VCENTER);

	m_dcMemHeaderArea.DrawText(m_strMonitorDrawPeriod, &rcRate, DT_LEFT|DT_TOP);

	dc.BitBlt(0, 0, m_rcDesktop.Width(), m_baseY, &m_dcMemHeaderArea, 0, 0, SRCCOPY);

	//////////////////////////////////////// draw all graphs //////////////////////////////////////////////////////

	for (int i = 0; i < PLOT_COUNT; ++ i)
	{
		m_Plots[i]->draw(0 + PLOT_WIDTH * (i%GRAPH_COLUMNS), i / GRAPH_COLUMNS * PLOT_HEIGHT + m_baseY + m_monitorsAreaHeight, m_data[i], m_oldData[i]);
	}
	

	// Do not call CDialog::OnPaint() for painting messages
}

BOOL Ce600_BenchmarkDlg::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}


void Ce600_BenchmarkDlg::OnCheckButtons( UINT nId )
{
	for (int id=IDC_CHECK1; id<=IDC_CHECK10; ++id)
	{
		CButton* pButton = static_cast<CButton*>(GetDlgItem(id));
		if (id != nId)
		{
			pButton->SetCheck(BST_UNCHECKED);
		}
		else
		{
			UINT status = pButton->GetCheck();
			if (status != BST_CHECKED)
				m_pCurrentCheckBtn = NULL;
			else
				m_pCurrentCheckBtn = pButton;
		}
	}
}


HBRUSH Ce600_BenchmarkDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	if ( pWnd->GetDlgCtrlID() >= IDC_CHECK1 && pWnd->GetDlgCtrlID() <= IDC_CHECK10 )
	{
		pDC->SetTextColor( MonitorTextColor );
	}

	return m_bkGndBrush;

	// TODO:  Return a different brush if the default is not desired
	// return hbr;
}

void Ce600_BenchmarkDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect rectCell;

	// first we need to decide whether this point is inner monitor area for some cell
	if ( point.y >= m_baseY && point.y <= m_baseY + m_monitorsAreaHeight
		&& point.x >= m_baseX && point.x <= m_baseX + m_monitorColumns * m_cellWidth )
	{
		// we are in monitor area now, figure out which cell is being dragged
		int row = (point.y - m_baseY) / m_cellHeight;
		int column = (point.x - m_baseX) / m_cellWidth;

		rectCell.left = column * m_cellWidth + m_baseX;
		rectCell.top = row * m_cellHeight+ m_baseY;
		rectCell.right = rectCell.left + m_cellWidth;
		rectCell.bottom = rectCell.top + m_cellHeight;

		// must be in this rectangle
		ASSERT(rectCell.PtInRect(point));
		CDC* pDC = GetDC();
	 
		CDC dcMem;
		CBitmap bitmapTemp;
		bitmapTemp.CreateCompatibleBitmap(pDC, rectCell.Width(), rectCell.Height());
		
		dcMem.CreateCompatibleDC(pDC);
		CBitmap* pOldBmp = dcMem.SelectObject(&bitmapTemp);
		// copy the cell image from buffer
		dcMem.BitBlt(0, 0, rectCell.Width(), rectCell.Height(), pDC, rectCell.left, rectCell.top, SRCCOPY);

		/* // for transparent
		int startX = 0;
		for (int line = 0; line < rectCell.Height(); line ++)
		{
			startX += 1;
			startX %= 2;

			for (int x=startX; x < rectCell.Width(); x+=2)
			{
				dcMem.SetPixel(x, line, RGB(255,255,254));
			}
		}
		*/
		
		dcMem.SelectObject(pOldBmp);
		dcMem.DeleteDC();

		ReleaseDC(pDC);

		m_bIsLButtonDown = true;
 		m_ptDragOffset.x = point.x - rectCell.left;
 		m_ptDragOffset.y = point.y - rectCell.top;

		// destroy the old image list first
		if ( m_hImageList != NULL )
			ImageList_Destroy(m_hImageList);

		// create new image list
		m_hImageList = ImageList_Create(rectCell.Width(), rectCell.Height(), ILC_COLORDDB/*|ILC_MASK*/, 0, 1);

		ASSERT((m_hImageList != NULL));
		if ( m_hImageList == NULL )
		{
			DWORD dwError = ::GetLastError();
		}
	
		// int nNumber = ImageList_Add(m_hImageList, bitmapTemp, bitmapTemp);
		int nNumber = ImageList_AddMasked(m_hImageList, bitmapTemp, RGB(0,0,0));

		if ( !ImageList_BeginDrag(m_hImageList, 0, m_ptDragOffset.x, m_ptDragOffset.y) )
		{
			DWORD dwError = ::GetLastError();

			ASSERT(FALSE);
		}

		CPoint pt = point;
		ClientToScreen(&pt);
		if ( !ImageList_DragEnter(NULL, pt.x, pt.y))
		{
			DWORD dwError = ::GetLastError();
		}

		// make sure we are receiving all the mouse event
		SetCapture();
	}

	CDialog::OnLButtonDown(nFlags, point);
}

void Ce600_BenchmarkDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if ( m_bIsLButtonDown )
	{
		// build up the region that we don't need to invalidate
		m_rcCurrentDragIcon.left = point.x - m_ptDragOffset.x;
		m_rcCurrentDragIcon.top = point.y - m_ptDragOffset.y;
		m_rcCurrentDragIcon.bottom = m_rcCurrentDragIcon.top + m_cellHeight;
		m_rcCurrentDragIcon.right = m_rcCurrentDragIcon.left + m_cellWidth;

		CPoint pt = point;
		ClientToScreen(&pt);

		if ( !ImageList_DragMove(pt.x, pt.y) )
		{
			DWORD dwError = ::GetLastError();

			ASSERT(FALSE);
		}
	}

	CDialog::OnMouseMove(nFlags, point);
}

void Ce600_BenchmarkDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (m_bIsLButtonDown)
	{
		m_bIsLButtonDown    = false;  

		if ( !ImageList_DragLeave(m_hWnd))
		{
			DWORD dwError = ::GetLastError();
		}

		ImageList_EndDrag();

		ReleaseCapture();  
	}

	CDialog::OnLButtonUp(nFlags, point);
}

LRESULT Ce600_BenchmarkDlg::OnVentWheelUpdate( WPARAM WheelCount, LPARAM WheelDir )
{
	if (m_pCurrentCheckBtn != NULL)
	{
		CString strText;
		m_pCurrentCheckBtn->GetWindowText(strText);
		int currentNumber = _wtoi(strText);
		if(!WheelDir)
			currentNumber += WheelCount;
		else
			currentNumber -= WheelCount;

		strText.Format(_T("%d"), currentNumber);
		m_pCurrentCheckBtn->SetWindowText(strText);
	}
	return 0;
}

LRESULT Ce600_BenchmarkDlg::OnDataGenTimer( WPARAM wParam, LPARAM lParam )
{
	int data = m_dataGen.get(200);
	for (int i = 0; i < PLOT_COUNT; ++ i)
	{
		CRect rcWave;
		m_Plots[i]->getWaveRect(rcWave);

		rcWave.OffsetRect((i%GRAPH_COLUMNS)*PLOT_WIDTH, m_baseY + m_monitorsAreaHeight + i/GRAPH_COLUMNS*PLOT_HEIGHT);
		
		m_Plots[i]->pushData(data);
		
		// cache the wave rectangle to let it refresh next cycle
		cacheInvalidateRgn_(&rcWave);		
	}

	return 0;
}

LRESULT Ce600_BenchmarkDlg::OnMonitorRealTimer( WPARAM wParam, LPARAM lParam )
{
//	vector<CRect>::iterator dataIt = m_vMonitorCellDatas.begin();

// 	for ( ; dataIt != m_vMonitorCellDatas.end(); ++ dataIt )
// 	{
// 		CRect rcData = *dataIt;
// 		rcData.OffsetRect(m_baseX, m_baseY);
// 		cacheInvalidateRgn_(&rcData);
// 	}

	CRect rcRate(10, 10, 300, 100);
	cacheInvalidateRgn_(&rcRate);

	return 0;
}

LRESULT Ce600_BenchmarkDlg::OnMyInvalidateTimer( WPARAM wParam, LPARAM lParam )
{
	static int MONITOR_COUNT = 0;
	if ( MONITOR_COUNT == 0 )
	{
		m_bMonitorRedraw = true;
		
		RgnHelper::appendRgn( &m_GlobalInvalidateRgn, &m_MonitorDataRgn );
	}
	MONITOR_COUNT ++;
	MONITOR_COUNT %= (350 / DRAW_CYCLE);

	if ( m_bIsLButtonDown )
	{
		RgnHelper::substractRgn(&m_GlobalInvalidateRgn, &m_rcCurrentDragIcon);
	}

	InvalidateRgn(&m_GlobalInvalidateRgn, FALSE);

	RgnHelper::setEmpty(&m_GlobalInvalidateRgn);

	return 0;
}


void Ce600_BenchmarkDlg::cacheInvalidateRgn_( LPRECT lpRect )
{
	RgnHelper::appendRgn( &m_GlobalInvalidateRgn, lpRect );
}


void Ce600_BenchmarkDlg::drawMonitorData_(  )
{
#ifdef DIRECT_DRAW
	HDC hDC = NULL;
	m_pMonitorSurface->GetDC( &hDC );
	BitBlt( hDC, 0, 0, m_rcDesktop.Width(), m_monitorsAreaHeight, m_dcMemMonitorBkgnd, 0, 0, SRCCOPY );
	
	SetTextColor(hDC, MonitorTextColor);
	SetBkMode(hDC, TRANSPARENT);
	SelectObject(hDC, *GlobalObjects::getInstance()->getArial_24_Bold());

	vector<CRect>::iterator dataIt = m_vMonitorCellDatas.begin();

	for ( ; dataIt != m_vMonitorCellDatas.end(); ++ dataIt )
	{
		CString strTickCount;
		strTickCount.Format(_T("%d"), GetTickCount());
		CString strNew;
		strNew.Format(_T("%s%s"), strTickCount.Right(5), strTickCount.Right(5));
		DrawText(hDC, strNew, -1, &*dataIt, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	}
	
	m_pMonitorSurface->ReleaseDC(hDC);

#else
	m_dcMemMonitorArea.BitBlt(0, 0, m_rcDesktop.Width(), m_monitorsAreaHeight, &m_dcMemMonitorBkgnd, 0, 0, SRCCOPY );

	int savedDC = m_dcMemMonitorArea.SaveDC();
	m_dcMemMonitorArea.SetBkMode( TRANSPARENT );
	m_dcMemMonitorArea.SetTextColor( MonitorTextColor );
	m_dcMemMonitorArea.SelectObject( GlobalObjects::getInstance()->getArial_24_Bold() );

	vector<CRect>::iterator dataIt = m_vMonitorCellDatas.begin();

	for ( ; dataIt != m_vMonitorCellDatas.end(); ++ dataIt )
	{
		CString strTickCount;
		strTickCount.Format(_T("%d"), GetTickCount());
		CString strNew;
		strNew.Format(_T("%s%s"), strTickCount.Right(5), strTickCount.Right(5));
		m_dcMemMonitorArea.DrawText(strNew, *dataIt, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	}

	m_dcMemMonitorArea.RestoreDC( savedDC );
	#endif
}

void Ce600_BenchmarkDlg::initMonitorsRect_()
{
	m_vMonitorCellTitles.clear();
	m_vMonitorCellDatas.clear();

	for (int cellIndex = 0; cellIndex < m_monitorRows*m_monitorColumns; cellIndex ++)
	{
		int cellIndexX = cellIndex % m_monitorColumns;
		int cellIndexY = cellIndex / m_monitorColumns;

		// calculate the cell rectangle
		CRect rcCell;
		rcCell.left = cellIndexX * m_cellWidth;
		rcCell.top = cellIndexY * m_cellHeight;
		rcCell.right = rcCell.left + m_cellWidth;
		rcCell.bottom = rcCell.top + m_cellHeight;

		// calculate the cell title rectangle
		CRect rcCellTitle;
		rcCellTitle.left = rcCell.left + 2;
		rcCellTitle.top = rcCell.top + 1;
		rcCellTitle.right = rcCellTitle.left + m_cellWidth - 4;
		rcCellTitle.bottom = rcCellTitle.top + m_cellHeight / 2;

		m_vMonitorCellTitles.push_back( rcCellTitle );

		// calculate the cell data rectangle
		CRect rcData;
		rcData.left = rcCellTitle.left;
		rcData.top = rcCellTitle.bottom;
		rcData.right = rcData.left + m_cellWidth - 4;
		rcData.bottom = rcData.top + m_cellHeight / 2;

		m_vMonitorCellDatas.push_back( rcData );
		
		// append the rcData to monitor data rgn object
		RgnHelper::appendRgn(&m_MonitorDataRgn, &rcData);
	}

	m_MonitorDataRgn.OffsetRgn(m_baseX, m_baseY);
}

void Ce600_BenchmarkDlg::prepareMonitorCanvas_()
{
	int savedDC = m_dcMemMonitorBkgnd.SaveDC();

	// drag background
	CRect	rcMonitorArea(0, 0, m_rcDesktop.Width(), 0 + m_monitorsAreaHeight);
	m_dcMemMonitorBkgnd.FillRect(&rcMonitorArea, &m_bkGndBrush);

	// draw grid
	m_dcMemMonitorBkgnd.SelectObject(&m_gridVerticalPen);
	for (int i = 0; i < m_monitorColumns + 1; ++ i)
	{
		CPoint ptBegin, ptEnd;
		ptBegin.x = i * m_cellWidth;
		ptBegin.y = rcMonitorArea.top;
		ptEnd.x = ptBegin.x;
		ptEnd.y	= ptBegin.y + m_monitorsAreaHeight;

		m_dcMemMonitorBkgnd.MoveTo(ptBegin);
		m_dcMemMonitorBkgnd.LineTo(ptEnd);
	}

	m_dcMemMonitorBkgnd.SelectObject(&m_gridHorizontalPen);
	for (int j = 0; j < m_monitorRows + 1; ++ j)
	{
		CPoint ptBegin, ptEnd;
		ptBegin.x = 0;
		ptBegin.y = rcMonitorArea.top + j * m_cellHeight;
		ptEnd.x = m_cellWidth * m_monitorColumns;
		ptEnd.y = ptBegin.y;

		m_dcMemMonitorBkgnd.MoveTo(ptBegin);
		m_dcMemMonitorBkgnd.LineTo(ptEnd);
	}

	m_dcMemMonitorBkgnd.SetTextColor( MonitorTextColor );
	m_dcMemMonitorBkgnd.SelectObject( GlobalObjects::getInstance()->getArial_24_Bold() );
	m_dcMemMonitorBkgnd.SetBkMode(TRANSPARENT);

	int cellIndex = 0;
	vector<CRect>::iterator titleIt = m_vMonitorCellTitles.begin();

	for ( ; titleIt != m_vMonitorCellTitles.end(); ++ titleIt )
	{
		CString str;
		str.Format(_T("Cell Title %2d"), cellIndex);	
		m_dcMemMonitorBkgnd.DrawText(str, *titleIt, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
		cellIndex ++;
	}

	m_dcMemMonitorBkgnd.RestoreDC( savedDC );
}


void Ce600_BenchmarkDlg::layoutButtons_() 
{
	int buttonPerLine = 5;
	int buttonDistance = 20;
	int buttonWidth = static_cast<int>( ( (double) m_rcDesktop.Width() * 2 / 3 
		- buttonDistance * ( buttonPerLine - 1 ) ) / buttonPerLine - 2 );
	int buttonHeight = 50;
	int buttonLeftMost = static_cast<int>( (double) m_rcDesktop.Width() * 1 / 3 );
	int buttonIndex = 0;
	for ( UINT nID = IDC_CHECK1; nID <= IDC_CHECK10; nID ++ )
	{
		CWnd* pWnd = GetDlgItem( nID );
		CButton* pButton = static_cast<CButton*>( pWnd );

		CRect rcButton;
		rcButton.left = buttonLeftMost + ( buttonIndex % buttonPerLine ) 
			* ( buttonWidth + buttonDistance );

		rcButton.right = rcButton.left + buttonWidth;
		rcButton.top = 10 + ( buttonIndex / buttonPerLine ) * ( buttonHeight + 10 );
		rcButton.bottom = rcButton.top + buttonHeight;
		pButton->MoveWindow(&rcButton, TRUE);
		pButton->SetFont( GlobalObjects::getInstance()->getArial_48_Bold(), TRUE );

		buttonIndex += 1;
	}
}
UINT KnobEventDetectThreadProc( LPVOID pParam )
{
	HWND hWnd = (HWND)pParam;

	AioDioDriver_t driver;
	while(true)
	{
		unsigned char WheelCount,WheelDir;
		bool WheelRtn = driver.ReadKnob(&WheelCount, &WheelDir);
		if (WheelCount > 0)
		{
			::PostMessage(hWnd, WM_WHEEL_UPDATE, WheelCount, WheelDir);
		}
		Sleep(25);
	}

	return 0;
}

void DataGenTimerCallback( UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2 )
{
	if ( dwUser != NULL)
	{
		HWND hWnd = (HWND)dwUser;
		::PostMessage(hWnd, WM_DATAGEN_TIMER, 0, 0);
	}
}

void MonitorRealTimerCallback( UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2 )
{
	if ( dwUser != NULL)
	{
		HWND hWnd = (HWND)dwUser;
		::PostMessage(hWnd, WM_MONITOR_REAL_TIMER, 0, 0);
	}
}

void InvalidateRealTimerCallback( UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2 )
{
	if ( dwUser != NULL )
	{
		HWND hWnd = (HWND)dwUser;

		::PostMessage(hWnd, WM_MYINVALIDATE_TIMER, 0, 0);
	}
}
