#ifndef HighPressCompExpTrigger_HH
#define HighPressCompExpTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: HighPressCompExpTrigger - Triggers if the pressure exceeds the high  
// 		pressure compensation limit for a duration greater than the IBW based
//		time limit.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/HighPressCompExpTrigger.hhv   10.7   08/17/07 09:37:26   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  syw    Date:  14-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "BreathTrigger.hh"

//@ End-Usage

class HighPressCompExpTrigger : public BreathTrigger {
  public:
    HighPressCompExpTrigger( void) ;
    ~HighPressCompExpTrigger( void) ;

    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;

	virtual void enable( void) ;
	void setHighPressCompLimitMet( const Boolean met) ;

  protected:
    virtual Boolean triggerCondition_(void);

  private:
    HighPressCompExpTrigger( const HighPressCompExpTrigger&) ;  			// not implemented...
    void   operator=( const HighPressCompExpTrigger&) ; 	// not implemented...

	//@ Data-Member: count_
    // number of times high pressure compensation condition is met
	Int32 count_ ;

	//@ Data-Member: maxCount_
    // maximum count based on ibw
	Uint32 maxCount_ ;
} ;

#endif // HighPressCompExpTrigger_HH 
