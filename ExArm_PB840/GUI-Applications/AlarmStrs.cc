#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================
// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmStrs - Language-independent repository for
// alarm message text displayed on the ventilator screens.
//---------------------------------------------------------------------
//@ Interface-Description
// This class exists for encapsulation purposes only.  All language dependent
// alarm message text that can be displayed on the ventilator are contained in 
// the static private member Text_ of this class.
//---------------------------------------------------------------------
//@ Rationale
// Convenient encapsulator for strings and provides for language independence.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmStrs.ccv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 016   By:   Boris Kuznetsov      Date: 05-May-2003       DR Number: 6036
//  Project:   Russian
//  Description:
//      Addded include file for PolishAlarmStrs
//
//  Revision: 015   By:   S. Peters      Date: 16-Oct-2002       DR Number: 6029
//  Project:   Polish
//  Description:
//      Addded include file for PolishAlarmStrs
//
//  Revision: 014  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//   Revision 013   By:  hhd	Date: 09-Apr-1998        DR Number:  
//   Project:   Sigma   (R8027)
//   Description:
//		Included JapaneseAlarmStrs.in for Japanese version.
//
//   Revision 012   By:  sah    Date: 15-Jan-1998        DR Number:  5004
//   Project:   Sigma   (R8027)
//   Description:
//       Moved from Alarm-Analysis subsystem, and renamed to 'AlarmStrs',
//       from 'AlarmMessages'.
//
//   Revision 011   By:   hhd    Date: 09/11/97         DR Number:  2265
//      Project:   Sigma   (R8027)
//      Description:
//			Refered to PortugueseAlarmStrs file when called for. 
//
//   Revision 010   By:   hhd    Date: 08/05/97         DR Number:  2265
//      Project:   Sigma   (R8027)
//      Description:
//			Refered to ItalianAlarmStrs file when called for. 
//  
//   Revision 009   By:   hhd    Date: 08/05/97         DR Number:  2310
//      Project:   Sigma   (R8027)
//      Description:
// 		  	Changed how Alarm messages are retrieved due to change in Alarm
//			TextInfo_ structure.  Added InitAlarmTextInfo().
//
//   Revision 008   By:   hhd    Date: 07/30/97         DR Number:  2265
//      Project:   Sigma   (R8027)
//      Description:
//		   Modified for language independency.
//  
//   Revision 007   By:   hct    Date: 06/19/97         DR Number:  2097
//      Project:   Sigma   (R8027)
//      Description:
//         Add analysis messages for HIGH Pvent.
//  
//   Revision 006   By:   hct    Date: 02/26/97         DR Number:  1793
//      Project:   Sigma   (R8027)
//      Description:
//         Delete WEAK BATTERY alarm and change analysis messages for 
//         INOPERATIVE BATTERY and LOW BATTERY.
//  
//   Revision 005   By:   hct    Date: 01/17/97         DR Number:  1654
//      Project:   Sigma   (R8027)
//      Description:
//         Added alarm message "Power loss & recovery occurred with a
//         pre-existing DEVICE ALERT." and "Check Alarm Log.  EST required.".
//  
//   Revision 004   By:   hct    Date: 05/23/96         DR Number:   648
//      Project:   Sigma   (R8027)
//      Description:
//         Added alarm message "No ventilation.".
//  
//   Revision 003   By:   hct    Date: 05/22/95         DR Number:   7
//      Project:   Sigma   (R8027)
//      Description:
//         Changed Non-Technical Alarm text and structure as per changes to SRS.
//  
//   Revision 002   By:   hct    Date: 05/22/95         DR Number:   392
//      Project:   Sigma   (R8027)
//      Description:
//         Changed Technical Alarm text as per changes to SRS.
//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

#include "AlarmStrs.hh"

//@ Usage-Classes

#ifdef SIGMA_ASIA
	#include "AsiaAlarmStrs.in"
#elif SIGMA_EUROPE
	#include "EuropeAlarmStrs.in"
#elif SIGMA_CHINESE
	#include "ChineseAlarmStrs.in"
#elif SIGMA_ENGLISH
	#include "EnglishAlarmStrs.in"
#elif SIGMA_FRENCH
	#include "FrenchAlarmStrs.in"
#elif SIGMA_GERMAN
	#include "GermanAlarmStrs.in"
#elif SIGMA_ITALIAN
	#include "ItalianAlarmStrs.in"
#elif SIGMA_JAPANESE
	#include "JapaneseAlarmStrs.in"
#elif SIGMA_PORTUGUESE
	#include "PortugueseAlarmStrs.in"
#elif SIGMA_POLISH
	#include "PolishAlarmStrs.in"
#elif SIGMA_RUSSIAN
	#include "RussianAlarmStrs.in"
#elif SIGMA_SPANISH
	#include "SpanishAlarmStrs.in"
#endif

//@ End-Usage


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize  [static]
//
//@ Interface-Description
//  Verify that all alarm messages are in the proper location.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Looping through the array of Alarm-Analysis TextInfo_ and verify that the
//	text ID matches the index location of the string.  AUX_CLASS_ASSERTION
//	occurs if this isn't the case.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AlarmStrs::Initialize (void)
{
	for (Int i=0; i < MAX_NUM_ALARM_MESSAGES; i++)
	{
       // Message name index must match TextInfo_'s entry index
	   AUX_CLASS_ASSERTION((AlarmStrs::TextInfo_[i].messageName == i), i); 
	}
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
AlarmStrs::SoftFault(const SoftFaultID  softFaultID,
					 const Uint32 lineNumber,
					 const char  *pFileName,
					 const char  *pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMSTRS,
  						  lineNumber, pFileName, pPredicate);
}
