#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation
// of California claims exclusive right.  No part of this work may
// be used, disclosed, reproduced, sorted in an information retrieval
// system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written
// permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: ConfigurationAgent - Interface Agent to the Configuration class.
//---------------------------------------------------------------------
//@ Interface-Description
// 		This class provides the interface to Configuration class.  It contains
// 		methods to access the Configuration contents and can also be instantiated as
// 		an object containing the configuration contents.  Task-Control uses the
// 		ConfigurationAgent object to transport the contents of the configuration to
// 		the GUI by instantiating it in the BdReadyMessage.  This class is
//		intended to be instantiated locally and any data obtained from this
//		class MUST be copied over to a local copy before it goes out of scope.
//
//		This class provides support for the BD_CPU and the GUI_CPU.
//---------------------------------------------------------------------
//@ Rationale
// 		The ConfigurationAgent class encapsulates the external interface to
// 		the configuration class.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The ConfigurationAgent class is a collection of functions.  As
// 		an object, it contains the current contents of the Configuration class.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
// 		rSystemConfiguration, rCompressor and rCompressorEeprom must be
//		instantiated by BD-IO-Devices subsystem before this class is
//		instantiated.
//---------------------------------------------------------------------
//@ Invariants
// 		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ConfigurationAgent.ccv   25.0.4.0   19 Nov 2013 13:54:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: mnr    Date: 24-Mar-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX Serial Number activated.
//
//  Revision: 005  By: mnr    Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX firmware revision and Serial Number being sent to GUI via
//		BdReadyMsg.
//
//  Revision: 004 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  syw    Date: 30-Jan-1997    DR Number: DCS 1555
//      Project:   Sigma   (R8027)
//      Description:
//         Added bdNovRamSerialNumber_ to class.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "ConfigurationAgent.hh"

// Usage-Classes

#include "Configuration.hh"
#include "BDIORefs.hh"
#include "VentObjectRefs.hh"
#include "NovRamManager.hh"

#if defined(SIGMA_BD_CPU)
//TODO E600_LL will revisit after decision whether to keep Compressor.*
//#include "Compressor.hh"
#endif // defined(SIGMA_BD_CPU)

#ifdef SIGMA_DEBUG
#include <stdio.h>
#endif // SIGMA_DEBUG

// End-Usage

//=====================================================================
//
//      Public Methods...
//
//=====================================================================
#if defined(SIGMA_BD_CPU)  
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ConfigurationAgent
//
//@ Interface-Description
// 		Constructor.  This method has no arguments and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize all data members with the Configuration object.  Get the
//		compressor serial number along with the software revision number.
//---------------------------------------------------------------------
//@ PreCondition
// 		none
//---------------------------------------------------------------------
//@ PostCondition
// 		none
//@ End-Method
//=====================================================================

ConfigurationAgent::ConfigurationAgent( void )
	: flashSerialNumber_( RSystemConfiguration.getFlashSerialNumber()),				// $[TI1]
	  exhValveSignature_( RSystemConfiguration.getExhValveSignature()),
	  exhValveCrc_( RSystemConfiguration.getExhValveCrc()),
	  airFlowSensorSN_( RSystemConfiguration.getAirFlowSensorSN()),
	  airFlowSensorCrc_( RSystemConfiguration.getAirFlowSensorCrc()),
	  o2FlowSensorSN_( RSystemConfiguration.getO2FlowSensorSN()),
	  o2FlowSensorCrc_( RSystemConfiguration.getO2FlowSensorCrc()),
	  exhFlowSensorSN_( RSystemConfiguration.getExhFlowSensorSN()),
	  exhFlowSensorCrc_( RSystemConfiguration.getExhFlowSensorCrc())
{
    CALL_TRACE("ConfigurationAgent(void)");

	SerialNumber blankSn( "") ;
#if 0 //TODO E600_LL will revisit after decision whether to keep Compressor.*
	if (RCompressor.isInstalled())
	{
		// $[TI2.1]
		compressorSerialNumber_ = RCompressorEeprom.getSerialNumber() ;
	}
	else
#endif
	{
		// $[TI2.2]
		compressorSerialNumber_ = blankSn ;
	}

	memcpy(softwareRevNum_, SoftwareRevNum::GetInstance().GetAsShortString(), 
							SoftwareRevNum::REV_NUM_SIZE);

	NovRamManager::GetUnitSerialNumber( bdNovRamSerialNumber_ );

	NovRamManager::GetLastSstProxFirmwareRev( proxFirmwareRev_ );

	NovRamManager::GetLastSstProxSerialNum( proxSerialNum_ );

	isProxInstalled_ = ProxiInterface::isProxBoardInstalled();

}

#endif // defined(SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ConfigurationAgent [destructor]
//
//@ Interface-Description
// 		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
// 		none
//---------------------------------------------------------------------
//@ PostCondition
// 		none
//@ End-Method
//=====================================================================

ConfigurationAgent::~ConfigurationAgent(void)
{
    CALL_TRACE("~ConfigurationAgent(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ConfigurationAgent 
//
//@ Interface-Description
//		Copy constructor.  This method has an ConfigurationAgent as an
//		argument and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Copy each data member from the object passed in to the newly
//		instantiated one.
//---------------------------------------------------------------------
//@ PreCondition
// 		none
//---------------------------------------------------------------------
//@ PostCondition
// 		none
//@ End-Method
//=====================================================================

ConfigurationAgent::ConfigurationAgent(const ConfigurationAgent& rConfigurationAgent)
    : flashSerialNumber_(rConfigurationAgent.flashSerialNumber_),						// $[TI3]
	  exhValveSignature_( rConfigurationAgent.exhValveSignature_),
	  exhValveCrc_( rConfigurationAgent.exhValveCrc_),
	  airFlowSensorSN_( rConfigurationAgent.airFlowSensorSN_),
	  airFlowSensorCrc_( rConfigurationAgent.airFlowSensorCrc_),
	  o2FlowSensorSN_( rConfigurationAgent.o2FlowSensorSN_),
	  o2FlowSensorCrc_( rConfigurationAgent.o2FlowSensorCrc_),
	  exhFlowSensorSN_( rConfigurationAgent.exhFlowSensorSN_),
	  exhFlowSensorCrc_( rConfigurationAgent.exhFlowSensorCrc_),
	  compressorSerialNumber_(rConfigurationAgent.compressorSerialNumber_),
	  bdNovRamSerialNumber_(rConfigurationAgent.bdNovRamSerialNumber_),
	  proxFirmwareRev_(rConfigurationAgent.proxFirmwareRev_),
	  proxSerialNum_(rConfigurationAgent.proxSerialNum_),
	  isProxInstalled_(rConfigurationAgent.isProxInstalled_)
	  
{
    CALL_TRACE("~ConfigurationAgent( const ConfigurationAgent& rConfigurationAgent )");
    
	// $[TI4]

	memcpy(softwareRevNum_, rConfigurationAgent.softwareRevNum_, SoftwareRevNum::REV_NUM_SIZE);
}

#ifdef SIGMA_DEBUG

void
ConfigurationAgent::dumpDataMembers( void) const
{
	cout << "flashSerialNumber_ = " << flashSerialNumber_ << endl ;
	printf( "exhValveSignature_ = %X\n", exhValveSignature_) ;
	printf( "exhValveCrc_ = %X\n", exhValveCrc_) ;
	printf( "airFlowSensorSN_ = %X\n", airFlowSensorSN_) ;
	printf( "airFlowSensorCrc_ = %X\n", airFlowSensorCrc_) ;
	printf( "o2FlowSensorSN_ = %X\n", o2FlowSensorSN_) ;
	printf( "o2FlowSensorCrc_ = %X\n", o2FlowSensorCrc_) ;
	printf( "exhFlowSensorSN_ = %X\n", exhFlowSensorSN_) ;
	printf( "exhFlowSensorCrc_ = %X\n", exhFlowSensorCrc_) ;
	printf( "exhO2FlowSensorSN_ = %X\n", exhO2FlowSensorSN_) ;
	printf( "exhO2FlowSensorCrc_ = %X\n", exhO2FlowSensorCrc_) ;
	printf( "softwareRevNum_ = %s\n", softwareRevNum_) ;
	cout << "compressorSerialNumber_ = " << compressorSerialNumber_ << endl ;
	cout << "proxFirmwareRev_ = " << proxFirmwareRev_ << endl;
	printf( "proxSerialNum_ = %d\n", proxSerialNum_) ;
	printf( "isProxInstalled_ = %d\n", isProxInstalled_) ;

}

#endif // SIGMA_DEBUG
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
ConfigurationAgent::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES, CONFIGURATIONAGENT, 
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================














