// softd.h : main header file for the SOFTD DLL
//

#if !defined(AFX_SOFTD_H__607BB362_026F_11D1_9F84_00805FCAC1CC__INCLUDED_)
#define AFX_SOFTD_H__607BB362_026F_11D1_9F84_00805FCAC1CC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "BPServer.h"

//forward declaration

class BootPServer;

//end forward declaration

/////////////////////////////////////////////////////////////////////////////
// CSoftdApp
// See softd.cpp for the implementation of this class
//

class CSoftdApp : public CWinApp
{
private:
	BootPServer Server;

public:
	CSoftdApp();
	int GetLastError(void);
	int StartServer(BootInfo *startUp);
	void EndServerSession(void);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSoftdApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSoftdApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOFTD_H__607BB362_026F_11D1_9F84_00805FCAC1CC__INCLUDED_)
