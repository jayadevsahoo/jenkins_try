
// PB840SettingsView.h : interface of the CPB840SettingsView class
//


#pragma once

#include "PB840SettingsCUG.h"

class CPB840SettingsView : public CView
{
protected: // create from serialization only
	CPB840SettingsView();
	DECLARE_DYNCREATE(CPB840SettingsView)

// Attributes
public:
	CPB840SettingsDoc* GetDocument() const;

// Operations
public:
	

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();

protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CPB840SettingsView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CPB840SettingsCUG	SettingGrid_;

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};

#ifndef _DEBUG  // debug version in PB840SettingsView.cpp
inline CPB840SettingsDoc* CPB840SettingsView::GetDocument() const
   { return reinterpret_cast<CPB840SettingsDoc*>(m_pDocument); }
#endif

