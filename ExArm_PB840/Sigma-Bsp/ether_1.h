/***************************************************************************
*
*		Copyright (c) 1995 MICROTEC RESEARCH INCORPORATED.
*
*	All rights reserved. MICROTEC RESEARCH's source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	MICROTEC RESEARCH. Any attempt or participation in deciphering,
*	decoding, reverse engineering or in any way altering the source code
*	is strictly prohibited, unless the prior written consent of
*	MICROTEC RESEARCH is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
****************************************************************************
*/


/*
** ether.h
**
** Contents:
**
** History:
**
**    Date        Name              Description
**    ---------------------------------------------------------------------- 
**    09-18-92    Roger Cole        Created file
**
*/
#ifndef _ETHER_1_H_
#define _ETHER_1_H_

#include <ansiprot.h>
#include <logio.h>
#include <compiler.h>
#include <vbuf.h>
#include <extio_1.h>

typedef struct {
    unsigned char interface_revision_level;
#define ETHER_1_INTERFACE 1
    
    /* some ethernet controllers can use only a part of system memory */
    /* and consequently, may not be able to access system memory */
    int      f_use_system_buffers;
    
    /* the next two variables must be set if f_use_system_buffers is false */
    char *        localMemoryBase; /* pointer to the start of memeory */       
    unsigned long memorySize;		 /* size of the memory */
    
    /* if this flag is set, then the interface will call pkt_recv until all
     ** of the packets have been received.  Otherwise, only the first
     ** packet will be returned. */
    int      f_get_all_packets;
    
    /* interrupt vectors must be known even if not programmable */
    unsigned char tx_int_vector;
    unsigned char rx_int_vector;
    unsigned char err_int_vector;
    unsigned char init_int_vector;
    
    /* buffer variables */
    int 	num_rbufs;        /* number of receive buffers	  */
    int 	num_tbufs;        /* number of transmit buffers  */
    int   num_free_tbufs;   /* number of free trans bufs  */
    
    void    (*board_get_ether_address)(_ANSIPROT1(unsigned char *));
    void    (*board_set_ether_address)(_ANSIPROT1(unsigned char *));
    unsigned char     ether_address[6];
    
    void     *specific;
} ether_1_config_t;

struct ether_1_dev_desc_t;

typedef struct {
    /* functions supplied by the board */
    
    vbsp_return_t (*get_ether_address)(_ANSIPROT1(unsigned char *)); 
    
    /* functions specific to the device */
    vbsp_return_t (*init)(_ANSIPROT1(struct ether_1_dev_desc_t*));
    logio_status_t (*int_info_set)(_ANSIPROT2
				   (struct ether_1_dev_desc_t*,
				    logio_int_entry_t *));
    logio_status_t (*int_info_get)(_ANSIPROT2
				   (struct ether_1_dev_desc_t*,
				    logio_int_entry_t *));
    vbsp_return_t (*pkt_recv)(_ANSIPROT2
			      (struct ether_1_dev_desc_t*,vbuf_t **));
   vbsp_return_t (*pkt_send)(_ANSIPROT2
			     (struct ether_1_dev_desc_t*,vbuf_t *));

    /* interrupt service routines */
    int (*ether_rx_tst)(_ANSIPROT1(logio_interrupt_entry_t *));
    int (*ether_tx_tst)(_ANSIPROT1(logio_interrupt_entry_t *));
    int (*ether_err_tst)(_ANSIPROT1(logio_interrupt_entry_t *));
    int (*ether_init_tst)(_ANSIPROT1(logio_interrupt_entry_t *));
    
    void     *specific;
    
} ether_1_fops_t;

/* NOTE NOTE NOTE! this structure will become the ether_1_dev_desc_t */
/* 
 * This is the device descriptor for the AM7990. 
 */
typedef struct ether_1_dev_desc_t {
    void              *map;
    ether_1_config_t  config;
    ether_1_fops_t    fops; 
    extern_fops_t     externio;
    /* external register read/write & intrpt functions */
    void              *specific;
} ether_1_dev_desc_t;

extern const logio_fops_t logio_ether_1_fops;
extern int logio_get_free_list ();
#endif

