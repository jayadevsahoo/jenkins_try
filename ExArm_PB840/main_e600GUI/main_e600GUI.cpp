#include "stdafx.h"
// main_e600GUI.cpp : Defines the class behaviors for the application.
//

#include "main_e600GUI.h"

#include "LowerBaseContainerView.h"
#include "UpperBaseContainerView.h"
#include "MainFrm.h"
#include "GuiFoundation.hh"
#include "BaseContainer.hh"
#include "Sys_Init.hh"
#include "NV_MemoryStorageThread.hh"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cmain_e600GUIApp

BEGIN_MESSAGE_MAP(Cmain_e600GUIApp, CWinApp)
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()



// Cmain_e600GUIApp construction
Cmain_e600GUIApp::Cmain_e600GUIApp()
	: CWinApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only Cmain_e600GUIApp object
Cmain_e600GUIApp theApp;

// Cmain_e600GUIApp initialization

BOOL Cmain_e600GUIApp::InitInstance()
{

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	m_pMainWnd = NULL;
	ptrLowerCDC_ = NULL; 
	ptrUpperCDC_ = NULL; 


	if(!CreateFrmView()) {
		return FALSE;
	}

	InitializeUpperViewCDC();
	InitializeLowerViewCDC();

	GUISystemInitalization();

	return TRUE;
}



BOOL Cmain_e600GUIApp::CreateFrmView()  {

	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object
	CMainFrame* pFrame = new CMainFrame;
	if (!pFrame)
		return FALSE;
	m_pMainWnd = pFrame;
	// create and load the frame with its resources
	pFrame->Create(L"CMainFrame",L"Puritan Bennett");

	// The one and only window has been initialized, so show and update it
	pFrame->ShowWindow(SW_SHOWNORMAL);
	pFrame->UpdateWindow();

	return TRUE;
}


void Cmain_e600GUIApp::InitializeLowerViewCDC()   {

	((CMainFrame*)m_pMainWnd)->m_ObjSplitterUper_Lower.SetActivePane(1,0,NULL);
	((CLowerBaseContainerView* )((CMainFrame*)m_pMainWnd)->m_ObjSplitterUper_Lower.GetActivePane())->CreateLowerBaseViewCDC();

}

void Cmain_e600GUIApp::InitializeUpperViewCDC()   {

	((CMainFrame*)m_pMainWnd)->m_ObjSplitterUper_Lower.SetActivePane(0,0,NULL);
	((CUpperBaseContainerView* )((CMainFrame*)m_pMainWnd)->m_ObjSplitterUper_Lower.GetActivePane())->CreateUpperBaseViewCDC();


}


void Cmain_e600GUIApp::GUISystemInitalization(void) {

		OutputDebugString(_T("Hello World\n"));
		//840 Code Initialization Beging ------>
		//initialize system, initialize thread table, create and launch 840 threads:
		Sys_Init::InitializeSigma();	//TODO this function runs POST_3 and some board init, it might not be needed

		// Initialize the structures that are expecting to be in NOVRAM or flash.
	    // These are here retrieved from the WinCE file system (on flash).
		NV_MemoryStorageThread::Initialize();

		//initialize tasks (threads) objects but do not create them yet..
		Sys_Init::InitializeTasks();
		//initialize subsystems, etc., then start the state-machine
		//Threads will be created in FsmState::Activate_() when it calls activate() for current-state
		
		Sys_Init::Initialize(); // TODO E600 port - Need to check --> Project is crashing if this API is executed. Not Analyzed.

		//Initialize the GUI display and paint the initial screens
		GuiFoundation::GetUpperBaseContainer().repaint();
		GuiFoundation::GetLowerBaseContainer().repaint();
}



int Cmain_e600GUIApp::ExitInstance()
{

	if(ptrLowerCDC_ != NULL) {
		delete ptrLowerCDC_;
		ptrLowerCDC_  = NULL;
	} 
	if(ptrUpperCDC_ != NULL){
		delete ptrUpperCDC_;
		ptrUpperCDC_= NULL;
	} 

	return CWinApp::ExitInstance();
}
