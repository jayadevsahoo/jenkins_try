#ifndef BackgroundMsg_HH
#define BackgroundMsg_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// File: BackgroundMsg - Definition for Background message structure
//                       and ID
//---------------------------------------------------------------------
// Version-Information
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundMsg.hhv   25.0.4.0   19 Nov 2013 14:20:36   pvcs  $
//
// Modification-Log
//
//  Revision: 003  By: gdc   Date:  08-Jan-2001    DR Number: 5493
//    Project:  GuiComms
//    Description:
//       Modified for new TouchDriver interface.
//
//  Revision: 002  By: iv    Date:  07-Jan-1998    DR Number: DCS 5000
//    Project:  Sigma (840)
//    Description:
//       Added a QueueInfo struct to BackgroundMsg.
//
//  Revision: 001  By:  by    Date:  12-Dec-1995  DR Number: None
//       Project:  Sigma (840)
//       Description:
//           Initial version.
//=====================================================================

#include "InterTaskMessage.hh"
//TODO E600 GUI only
#if defined(SIGMA_GUI_CPU)
#include "TouchDriver.hh"
#endif

// Type: BackgroundMsg
union BackgroundMsg
{
    enum BKEventType
    {
        PERIODIC_TIMER_EVENT    = FIRST_APPLICATION_MSG,
        BK_EVENT_MSG,
        LOG_DIAGNOSTIC_MSG
    };

    //@ Type:  QueueInfo
    // This defines the bit layout of a background
    // queue message.
    struct QueueInfo
    {
      Uint8             msgId;
      Uint8             eventName;
      Uint16            errorCode;
    };

    InterTaskMessage    tskMsg;
    QueueInfo           queueInfo;  
    Uint32              queueData;
};

#endif // BackgroundMsg_HH
