#include "StdAfx.h"
#include "GuiLog.hh"

static CCriticalSection csLog;

deque<const char *>  GuiLog::loglist_;

GuiLog::GuiLog(void)
{
}

GuiLog::~GuiLog(void)
{
}


void GuiLog::printf(const char * format, ... )
{
// #define HAS_RECEIVER

#define BUFSIZE	512	
	
	// buffer will be released when the message is processed
#if defined(HAS_RECEIVER)
	 char* p = new char[BUFSIZE];
#else
	// there's no receiver, allocate memory on stack
	char p[BUFSIZE];
#endif

	va_list vl;
	va_start(vl, format);

	sprintf_s(p, BUFSIZE, format, vl);

	va_end(vl);

#if defined(HAS_RECEIVER)
	// notify GUI thread directly, PUSH method
	PostMessage(*AfxGetApp()->m_pMainWnd, WM_PRINTF_LOG, 0, (LPARAM) p);

	// TODO:: start a thread to write log to files, REMEMBER: free the pointer after write
#endif
}


void GuiLog::append_(const char* p)
{
	csLog.Lock();
	{
		loglist_.push_back( p );
	}
	csLog.Unlock();
}


const char* GuiLog::retrieve()
{
	const char* p = NULL;

	csLog.Lock();
	{
		if ( !loglist_.empty() )
		{
			p = loglist_.front();
			loglist_.pop_front();
		}
	}
	csLog.Unlock();

	return p;
}