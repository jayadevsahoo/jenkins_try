#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LungVolumeExpTrigger - Triggers exhalation based on compensated
//      inspired volume.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers exhalation, based on compensated inspired volume
//    and the operator set value for volume limit. The trigger can be
//    enabled or disabled, depending upon the current breath phase and the
//    applicability of the trigger.  When the compensated inspired volume
//    is greater than the set volume limit, the trigger is considered to
//    have "fired".  The trigger is kept on a list contained
//    in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of whether 
//    this trigger is enabled or not.  If the trigger is not enabled, it will 
//    always return state of false.  If it is enabled and on the active list
//    in the BreathTriggerMediator, the condition monitored 
//    by the trigger will be evaluated every BD cycle. The condition detected 
//    by this trigger if inspiration lung btps volume is greater than high
//    inspiration tidal volume.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/LungVolumeExpTrigger.ccv   10.7   08/17/07 09:38:28   pvcs  
//
//@ Modification-Log
//
//  Revision: 003  By:  rhj    Date:  10-Sept-2010   SCR Number: 6436 
//       Project:  PROX
//       Description:
//			Added Prox Vti. 
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  syw    Date:  14-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial version.
//
//=====================================================================

#include "LungVolumeExpTrigger.hh"

//@ Usage-Classes
//@ End-Usage
#include "PhasedInContextHandle.hh"
#include "BreathRecord.hh"
#include "LungData.hh"
#include "ProxManager.hh"
//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LungVolumeExpTrigger()  
//
//@ Interface-Description
//		Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

LungVolumeExpTrigger::LungVolumeExpTrigger(void)
	:BreathTrigger(Trigger::LUNG_VOLUME_EXP)
{
	CALL_TRACE("LungVolumeExpTrigger::LungVolumeExpTrigger(void)") ;
	
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LungVolumeExpTrigger()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

LungVolumeExpTrigger::~LungVolumeExpTrigger(void)
{
	CALL_TRACE("LungVolumeExpTrigger::~LungVolumeExpTrigger(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
LungVolumeExpTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, LUNGVOLUMEEXPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns 
//    false.  Triggers if inspired lung volume exceeds operator set 
//    volume limit.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If the compensated inspiration volume is greater than or equal to the 
//    set volume limit then this method returns true else returns false.
//	$[TC04039]
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
LungVolumeExpTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

  Boolean rtnValue = FALSE;
  Real32 inspLungBtpsVolume = RLungData.getInspLungBtpsVolume();

  if (RProxManager.isAvailable())
  {
	  inspLungBtpsVolume = RProxManager.getInspiredVolume(); 
  }

  Real32 volumeLimit =
    PhasedInContextHandle::GetBoundedValue(SettingId::HIGH_INSP_TIDAL_VOL).value;
 
  if ( inspLungBtpsVolume > volumeLimit )
  {
	// $[TI2]
        rtnValue = TRUE;
  }
  // $[TI1]
 
  return (rtnValue);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
