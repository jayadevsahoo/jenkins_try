#ifndef sockintf_HH
#define sockintf_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: sockintf - Socket library function interface.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/sockintf.hhv   25.0.4.0   19 Nov 2013 14:02:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  jhv    Date:  2-Feb-96   DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "DownloadTypes.hh"

#include "sys/types.h"
#include "errno.h"
#if   defined(SIGMA_SUN)
#include "time.h"
#include <sys/socket.h>
#include "stdlib.h"
#elif  defined(SIGMA_TARGET)

//TODO E600 removed
//#include "ioctl.h"
#include "cdefs.h"
#include "xsconf.h"
#include "xssys.h"

#ifdef SIGMA_UNIT_TEST
extern "C" xsFlag_t use_local_Download__obtainClientIpAddr_isSet;
#undef FD_ISSET
#define FD_ISSET(fd, fdset) \
	(use_local_Download__obtainClientIpAddr_isSet ? 0 : ((fdset)->fdItem[FD_GETITEM(fd)] & (1 << FD_GETBIT(fd))))
#endif // SIGMA_UNIT_TEST

#include "xskmac.h"
#include "xsappmac.h"
#include "socket.h"
#endif   // defined(SIGMA_TARGET)

//TODO E600 removed
//#include <netinet/in.h>

#if     defined(SIGMA_SUN)
typedef		long  			xsInt32_t;
typedef		unsigned long   xsUInt32_t;
typedef 	unsigned char   xsUchar_t;
typedef 	fd_set 			xsFDSet_t;
#define 	XS_NUM_FDS		16
typedef   	struct fd_set 	xsFDSet_t;
#endif   // defined(SIGMA_SUN)

extern "C"
{
	Int close(Int fd);
#if     defined(SIGMA_SUN)
	extern Int errno;
#elif defined(SIGMA_TARGET)
	extern short  soError;
    Int ioctl ( int s, unsigned long  cmd, char *data);
	//Int listen(Int fd, Int backlog);
	//Int socket(Int domain, Int type, Int protocol);
	//Int accept (Int sd, struct sockaddr * name, Int *anamelen);
	//Int send(Int fd, char *buf, Int len, Int flags);
	//Int recv(Int fd, char *buf, Int len, Int flags);
	//Int sendto(Int fd, char *buf, Int len,
	//		 Int flags, struct sockaddr * to, Int tolen);
	//Int recvfrom(Int fd, char *buf, Int len,
	//		Int flags, struct sockaddr * from, Int *fromlen);
	//Int select(Int nfd, xsFDSet_t *read, xsFDSet_t *write,
	//	xsFDSet_t *except, xsInt32_t *timeout);
	//Int setsockopt(Int s, Int level, Int optname,
	//			const xsVoidp_t optval, Int optlen);
	//Int bind (Int s, struct sockaddr * name, Int namelen);
	//Int connect (Int sd, struct sockaddr * name,
	//						Int namelen);
	//Int getsockname( Int s, struct sockaddr *name, Int *namelen);
	//Int getpeername( Int s, struct sockaddr *name, Int *namelen);
	void wait_for_stackware(void);
	Uint my_get_moto_ip_addr(Int targetNumber);
#endif //defined(SIGMA_TARGET)
}

#endif // sockintf_HH 
