#include "StdAfx.h"
#include "SettingTable.h"
#include "SettingId.hh"


const SettingColumnEntry SettingTable::ColumnTable_[] = 
{
	{ SETTING_COLUMN_NAME, _T("BD Setting Name"), true,  230 },

#if defined(SHOW_UNIT_COLUMN)
	{ SETTING_COLUMN_UNIT, _T("Unit"),			  true,  100 },
#endif

	{ SETTING_COLUMN_VALUE, _T("Value"),		  false, 100 },
	{ SETTING_COLUMN_PRECISION, _T("Precision"),  false, 140 },
	{ SETTING_COLUMN_DESC, _T("Description"),	  true,  500 }
};

const SettingEntry SettingTable::Table_[] = 
{
	{ SettingId::APNEA_FLOW_ACCEL_PERCENT,	_T("APNEA_FLOW_ACCEL_PERCENT"), SET_BOUNDED, _T("[50~50] %"),		_T("Apnea Flow Accel. Percent"),	ONES, "50.0" },
	{ SettingId::APNEA_INSP_PRESS,			_T("APNEA_INSP_PRESS"),			SET_BOUNDED, _T("[5~90] cmH2O"),	_T("Apnea Insp. Press."),			ONES, "15.0" },
	{ SettingId::APNEA_INSP_TIME,			_T("APNEA_INSP_TIME"),			SET_BOUNDED, _T("[200~8k] ms"),		_T("Apnea Inspiratory Time"),		TENS, "1000.0" },
	{ SettingId::APNEA_INTERVAL,			_T("APNEA_INTERVAL"),			SET_BOUNDED, _T("[3k~60k] ms"),		_T("Apnea Interval Time"),			THOUSANDS, "10000.0" },
	{ SettingId::APNEA_MIN_INSP_FLOW,		_T("APNEA_MIN_INSP_FLOW"),		SET_BOUNDED, _T("[0.8~10k] mL/min"),_T("Apnea Minimum Inspiratory Flow"), HUNDREDTHS, "2.20" },
	{ SettingId::APNEA_O2_PERCENT,			_T("APNEA_O2_PERCENT"),			SET_BOUNDED, _T("[21~100] %"),		_T("Apnea Oxygen Percentage"),		ONES, "100.0" },
	{ SettingId::APNEA_PEAK_INSP_FLOW,		_T("APNEA_PEAK_INSP_FLOW"),		SET_BOUNDED, _T("[1~150] L/min"),	_T("Apnea Peak Inspiratory Flow"),	ONES, "22.0" },
	{ SettingId::APNEA_PLATEAU_TIME,		_T("APNEA_PLATEAU_TIME"),		SET_BOUNDED, _T("[0~0] s"),			_T("Apnea Plateau Time"),			HUNDREDS, "0.0" },
	{ SettingId::APNEA_RESP_RATE,			_T("APNEA_RESP_RATE"),			SET_BOUNDED, _T("[2~40] 1/min"),	_T("Apnea Respiratory Rate"),		ONES, "16.0" },
	{ SettingId::APNEA_TIDAL_VOLUME,		_T("APNEA_TIDAL_VOLUME"),		SET_BOUNDED, _T("[2~2.5k] mL"),		_T("Apnea Tidal Volume"),			ONES, "360.0" },
	{ SettingId::DISCO_SENS,				_T("DISCO_SENS"),				SET_BOUNDED, _T("[1-95] L/min"),	_T("Disconnection Sensitivity"),	ONES, "75.0" },
	{ SettingId::EXP_SENS,					_T("EXP_SENS"),					SET_BOUNDED, _T("[1~80] %"),		_T("Expiratory Sensitivity"),		ONES, "25.0" },
	{ SettingId::FLOW_ACCEL_PERCENT,		_T("FLOW_ACCEL_PERCENT"),		SET_BOUNDED, _T("[1~100] %"),		_T("Flow Acceleration Percent"),	ONES, "50.0" },
	{ SettingId::FLOW_SENS,					_T("FLOW_SENS"),				SET_BOUNDED, _T("[0.1~20] L/min"),	_T("Flow Sensitivity"),				TENTHS, "2.0" },
	{ SettingId::HIGH_CCT_PRESS,			_T("HIGH_CCT_PRESS"),			SET_BOUNDED, _T("[0~100] cmH2O"),	_T("High Circuit Pressure"),		ONES, "20.0" },
	{ SettingId::HIGH_INSP_TIDAL_VOL,		_T("HIGH_INSP_TIDAL_VOL"),		SET_BOUNDED, _T("[6~6k] mL"),		_T("High Inspired Tidal Volume (ATC/PA/VS/VC+)"), ONES, "50.0"  },
	{ SettingId::HUMID_VOLUME,				_T("HUMID_VOLUME"),				SET_BOUNDED, _T("[100~1k] mL?"),	_T("Humidifier Volume (ATC)"),		TENS, "480.0" },
	{ SettingId::IBW,						_T("IBW"),						SET_BOUNDED, _T("[0.3~150] kg"),	_T("Ideal Body Weight"),			TENTHS, "7.0" },
	{ SettingId::INSP_PRESS,				_T("INSP_PRESS"),				SET_BOUNDED, _T("[5~90] cmH2O"),	_T("Inspiratory Pressure"),			ONES, "10.0" },
	{ SettingId::INSP_TIME,					_T("INSP_TIME"),				SET_BOUNDED, _T("[200~8k] ms"),		_T("Inspiratory Time"),				TENS, "1000.0" },
	{ SettingId::MIN_INSP_FLOW,				_T("MIN_INSP_FLOW"),			SET_BOUNDED, _T("[0.8~10k] mL/min"),_T("Minimum Inspiratory Flow"),		HUNDREDTHS, "2.20" },
	{ SettingId::OXYGEN_PERCENT,			_T("OXYGEN_PERCENT"),			SET_BOUNDED, _T("[21~100] %"),		_T("Oxygen Percentage"),			ONES, "100.0" },
	{ SettingId::PEAK_INSP_FLOW,			_T("PEAK_INSP_FLOW"),			SET_BOUNDED, _T("[0~150] L/min?"),	_T("Peak Inspiratory Flow"),		ONES, "22.0" },
	{ SettingId::PERCENT_SUPPORT,			_T("PERCENT_SUPPORT"),			SET_BOUNDED, _T("[5~100] %"),		_T("Percent (Work) Support (ATC)"), ONES, "100.00" },
	{ SettingId::PLATEAU_TIME,				_T("PLATEAU_TIME"),				SET_BOUNDED, _T("[0~2k] ms"),		_T("Plateau Time"),					HUNDREDS, "0.0" },
	{ SettingId::PEEP,						_T("PEEP"),						SET_BOUNDED, _T("[0~45] cmH2O"),	_T("Positive End-Expiratory Press"),TENTHS, "3.0" },
	{ SettingId::PEEP_HIGH,					_T("PEEP_HIGH"),				SET_BOUNDED, _T("[5~90] cmH2O"),	_T("High PEEP (BiLevel)"),			TENTHS, "18.0" },
	{ SettingId::PEEP_HIGH_TIME,			_T("PEEP_HIGH_TIME"),			SET_BOUNDED, _T("[200~30k] ms"),	_T("High PEEP time (BiLevel)"),		TENS, "1000.0" },
	{ SettingId::PEEP_LOW,					_T("PEEP_LOW"),					SET_BOUNDED, _T("[0~45] cmH2O"),	_T("Low PEEP (BiLevel)"),			TENTHS, "3.0" },
	{ SettingId::PRESS_SENS,				_T("PRESS_SENS"),				SET_BOUNDED, _T("[0.1~20] %?"),		_T("Pressure Sensitivity"),			TENTHS, "2.0" },
	{ SettingId::PRESS_SUPP_LEVEL,			_T("PRESS_SUPP_LEVEL"),			SET_BOUNDED, _T("[0~70] %"),		_T("Pressure Support Level"),		ONES, "0.0" },
	{ SettingId::RESP_RATE,					_T("RESP_RATE"),				SET_BOUNDED, _T("[1~150] 1/min"),	_T("Respiratory Rate"),				ONES, "16.0" },
	{ SettingId::TIDAL_VOLUME,				_T("TIDAL_VOLUME"),				SET_BOUNDED, _T("[2~2.5k] mL"),		_T("Tidal Volume"),					ONES, "360.0" },
	{ SettingId::TUBE_ID,					_T("TUBE_ID"),					SET_BOUNDED, _T("[4.5~10] mm"),		_T("Tube Id (ATC)"),				TENTHS, "10.0" },
	{ SettingId::VOLUME_SUPPORT,			_T("VOLUME_SUPPORT"),			SET_BOUNDED, _T("[0.1~2.5k] mL"),	_T("Volume Support (VTPC)"),		ONES, "360.0" },
	{ SettingId::HIGH_SPONT_INSP_TIME,		_T("HIGH_SPONT_INSP_TIME"),		SET_BOUNDED, _T("[200~10k] ms"),	_T("High spontaneous inspiratory time (NIV)"), TENS, "3.00" },

	// discrete start
	{ SettingId::APNEA_FLOW_PATTERN, _T("APNEA_FLOW_PATTERN"), SET_DISCRETE, _T(""), _T("Apnea Flow Pattern"), FlowPatternValue::SQUARE_FLOW_PATTERN },
	{ SettingId::APNEA_MAND_TYPE, _T("APNEA_MAND_TYPE"), SET_DISCRETE, _T(""), _T("Apnea Mandatory Type"), MandTypeValue::PCV_MAND_TYPE },
	{ SettingId::FIO2_ENABLED, _T("FIO2_ENABLED"), SET_DISCRETE, _T(""), _T("FIO2 Option Enabled"), Fio2EnabledValue::YES_FIO2_ENABLED },
	{ SettingId::FLOW_PATTERN, _T("FLOW_PATTERN"), SET_DISCRETE, _T(""), _T("Flow Pattern"), FlowPatternValue::SQUARE_FLOW_PATTERN },
	{ SettingId::HUMID_TYPE, _T("HUMID_TYPE"), SET_DISCRETE, _T(""), _T("Humidification Type"), HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER },
	{ SettingId::NOMINAL_VOLT, _T("NOMINAL_VOLT"), SET_DISCRETE, _T(""), _T("Nominal Line Voltage"), NominalLineVoltValue::VAC_120 },
	{ SettingId::PATIENT_CCT_TYPE, _T("PATIENT_CCT_TYPE"), SET_DISCRETE, _T(""), _T("Patient Circuit Type"), PatientCctTypeValue::ADULT_CIRCUIT },
	{ SettingId::VENT_TYPE, _T("VENT_TYPE"), SET_DISCRETE, _T(""), _T("Vent Type (NIV)"), VentTypeValue::INVASIVE_VENT_TYPE },
	{ SettingId::MODE, _T("MODE"), SET_DISCRETE, _T(""), _T("Mode"), ModeValue::AC_MODE_VALUE },
	{ SettingId::MAND_TYPE, _T("MAND_TYPE"), SET_DISCRETE, _T(""), _T("Mandatory Type"), MandTypeValue::PCV_MAND_TYPE },
	{ SettingId::SUPPORT_TYPE, _T("SUPPORT_TYPE"), SET_DISCRETE, _T(""), _T("Support Type"), SupportTypeValue::PSV_SUPPORT_TYPE },
	{ SettingId::TRIGGER_TYPE, _T("TRIGGER_TYPE"), SET_DISCRETE, _T(""), _T("Trigger Type"), TriggerTypeValue::PRESSURE_TRIGGER },
	{ SettingId::TUBE_TYPE, _T("TUBE_TYPE"), SET_DISCRETE, _T(""), _T("Tube Type (ATC)"), TubeTypeValue::ET_TUBE_TYPE },
	{ SettingId::PROX_ENABLED, _T("PROX_ENABLED"), SET_DISCRETE, _T(""), _T("PROX enabled"), ProxEnabledValue::PROX_DISABLED },
	{ SettingId::LEAK_COMP_ENABLED, _T("LEAK_COMP_ENABLED"), SET_DISCRETE, _T(""), _T("Leak Compensation enabled"), LeakCompEnabledValue::LEAK_COMP_DISABLED }

};

const Uint SettingTable::NumEntries_ = countof( SettingTable::Table_ );
const Uint SettingTable::NumColumns_ = countof( SettingTable::ColumnTable_ );

const SettingEntry* SettingTable::PSettingById_[SettingId::TOTAL_SETTING_IDS + 1];
const SettingColumnEntry* SettingTable::PColumnById_[TOTAL_SETTING_COLUMNS + 1];
	
SettingTable::SettingTable(void)
{
}

SettingTable::~SettingTable(void)
{
}


void SettingTable::Initialize()
{
	for (int i=0; i < SettingTable::NumEntries_; i++)
    {
        const SettingEntry & settingEntry = SettingTable::GetSettingTableEntry(i);

        PSettingById_[settingEntry.id] = &settingEntry;
    }

	for (int i=0; i < SettingTable::NumColumns_; i++)
    {
        const SettingColumnEntry & columnEntry = SettingTable::GetColumnEntry(i);

        PColumnById_[columnEntry.id] = &columnEntry;
    }
}

const SettingEntry & SettingTable::GetSettingTableEntry(const Int32 index)
{
	return SettingTable::Table_[index];
}

const SettingEntry & SettingTable::GetSettingEntryById(const Int32 id)
{
	return *SettingTable::PSettingById_[id];
}

const SettingColumnEntry & SettingTable::GetColumnEntry(const Int32 index)
{
	return SettingTable::ColumnTable_[index];
}

const SettingColumnEntry & SettingTable::GetColumnEntryById(const Int32 id)
{
	return *SettingTable::PColumnById_[id];
}

Int32 SettingTable::GetNumSettingEntries()
{
	return SettingTable::NumEntries_;
}

Int32 SettingTable::GetNumColumnEntries()
{
	return SettingTable::NumColumns_;
}

