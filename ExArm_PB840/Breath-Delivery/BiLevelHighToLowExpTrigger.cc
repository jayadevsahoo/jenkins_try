#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BiLevelHighToLowExpTrigger - Triggers exhalation to implement the
//    transition from peep high to peep low in bilevel mode.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers exhalation, during bilevel inspiration, based on
//    the time out triggered exhalation.  The trigger can be enabled or disabled,
//    depending upon the current breath phase and the applicability of the
//    trigger.  When time out occured, the trigger is considered to have "fired".
//    The trigger is kept on a list contained in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements the algorithm for detecting the transition
//    from peep high to peep low.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of whether 
//    this trigger is enabled or not.  If the trigger is not enabled, 
//    it will always return a state of false.  If the trigger is enabled
//    and on the active list in the BreathTriggerMediator, the
//    condition monitored by the trigger is evaluated every BD cycle.
//    This trigger detects exhalation based on the timeUp_ flag maintained
//    by the BiLevelScheduler.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BiLevelHighToLowExpTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  11-Dec-1997    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BiLevelHighToLowExpTrigger.hh"

//@ Usage-Classes
#include "PhasedInContextHandle.hh"
#include "BreathRecord.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BiLevelHighToLowExpTrigger()  
//
//@ Interface-Description
//	Default Constructor
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ using BreathTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BiLevelHighToLowExpTrigger::BiLevelHighToLowExpTrigger(void)
 : BreathTrigger(Trigger::BL_HIGH_TO_LOW_EXP)
{
  CALL_TRACE("BiLevelHighToLowExpTrigger()");

  timeUp_ = FALSE;
  // $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BiLevelHighToLowExpTrigger() 
//
//@ Interface-Description 
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BiLevelHighToLowExpTrigger::~BiLevelHighToLowExpTrigger(void)
{
  CALL_TRACE("~BiLevelHighToLowExpTrigger()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable  
//
//@ Interface-Description
//	This method takes no argument and has no return value. This method 
//      enables the trigger and initializes all private data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set data member isEnabled_ to true.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BiLevelHighToLowExpTrigger::enable(void)
{
  CALL_TRACE("BiLevelHighToLowExpTrigger()");

  // $[TI1]
  isEnabled_ = TRUE;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BiLevelHighToLowExpTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BILEVELHIGHTOLOWEXPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns 
//    false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If the timeUp_ is true then this trigger condition is met.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
BiLevelHighToLowExpTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

  Boolean rtnValue = FALSE;
  
  if ( timeUp_ )
  {
    // $[TI1]
    rtnValue = TRUE;
    timeUp_ = FALSE;
  }
  // $[TI2]

  return (rtnValue);
}



//=====================================================================
//
//  Private Methods...
//
//=====================================================================


