//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientData.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"Convert.h"
#include"CDatabase.h"
#include"NetworkMsgHeader.hh"
#include"CLogger.h"
#include"IEventLogger.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------
class PatientDataDBHandler
{
    public:

    static bool register_types(IEventLogger *pIEventLogger, void *pUser);


    private:

    static bool insert_event_data(void *pUser, const char *pEventName, 
            EpochTimeStamp &ts, std::string &event_sequence_id);
    static void *check_header(boost::shared_ptr<Message> pMessage,
            const char *message_id, const unsigned int expected_len, char *pClassName=NULL);

    static bool end_of_breath_data            (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool realtime_breath_data          (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool averaged_breath_data          (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool peep_recovery_msg             (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool fio2_for_gui_msg              (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool paused_peend_data             (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool paused_piend_data             (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool nif_pressure_data             (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool p100_pressure_data            (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool vital_capacity_data           (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool computed_insp_pause_data      (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool end_exp_pause_pressure_state  (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool vtpcv_state_data              (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool bilevel_mand_data             (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool bilevel_mand_exh_data         (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool pav_breath_data               (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool pav_plateau_data              (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool pav_startup_data              (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool waveform_breath_data          (boost::shared_ptr<Message> pMessage, void *pUser);
	
	static bool send_event_msg_id          (boost::shared_ptr<Message> pMessage, void *pUser);
};




//---------------------------
// insert_event_data    
//---------------------------
bool PatientDataDBHandler::insert_event_data(void *pUser, const char *pEventName, EpochTimeStamp &ts, std::string &event_sequence_id)
{
    bool bRetval = true;

    boost::shared_ptr<char> g_buffer(new char[18000]);

    sprintf(g_buffer.get(),
    "INSERT INTO \"PatientData\".event_data (event_descriptor, event_time) "
        "VALUES ('%s',  (TIMESTAMP WITH TIME ZONE 'epoch' + %f * INTERVAL '1 second')) RETURNING event_sequence_id",
     pEventName,
     ts.getEpochTimeAsDouble()
    );

    CResult *r = ((CDatabase *)pUser)->select_query(g_buffer.get());   

    std::vector<CRow> v=r->getCRowVector();
    if(v.size()!=1)
    {
        LOG_MESSAGE(LM_ERROR,"Failed to insert record into PatientData.event_data");
        bRetval = false;
    }
    else 
    {
          event_sequence_id = (v.begin())->getCol("event_sequence_id").get().c_str();

    }
    delete r;

    return bRetval;
}

//---------------------------
// check_header    
//---------------------------
void *PatientDataDBHandler::check_header(boost::shared_ptr<Message> pMessage, const char *message_id, const unsigned int expected_len, char *pClassName)
{
    unsigned int len = ntohs(((NetworkMsgHeader *)(( pMessage.get()->get() ).get()))->pktSize);

    if( ((pMessage.get()->getLength()-sizeof(NetworkMsgHeader)) != len) || 
            (len != expected_len )
    )
    {
		if(pClassName == NULL)
		{
			LOG_MESSAGE(LM_ERROR, "Invalid message size, message_id=%i message_length=%i"
				,message_id, pMessage.get()->getLength());
		}
		else
		{
			LOG_MESSAGE(LM_ERROR, "Invalid message size for %s, message_id=%i message_length=%i"
				,pClassName, message_id, pMessage.get()->getLength());
		}


        return NULL;
    }

    return (void *) ((char *)((NetworkMsgHeader *)
                    (( pMessage.get()->get() ).get()))+sizeof(NetworkMsgHeader));

}

//---------------------------
// end_of_breath_data    
//---------------------------
bool PatientDataDBHandler::end_of_breath_data (boost::shared_ptr<Message> pMessage, void *pUser)
{

    std::string event_sequence_id;
    bool status;

    BreathDataPacket *bdPktPtr = (BreathDataPacket *)
        check_header(pMessage, "END_OF_BREATH_DATA", sizeof(BreathDataPacket),"end_of_breath_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(
        pUser, "END_OF_BREATH_DATA", pMessage->getTimeStamp(), 
        event_sequence_id
    );
    
    if(status == true) 
    {
        boost::shared_ptr<char> g_buffer(new char[18000]);
     
        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"END_OF_BREATH_DATA\" ("
            "event_sequence_id,"
            "\"breathType\","
            "\"exhaledTidalVolume\","
            "\"breathDuration\","
            "\"endExpiratoryPressure\","
            "\"ieRatio\","
            "\"mandFraction\","
            "\"dynamicCompliance\","
            "\"dynamicResistance\","
            "\"phaseType\","
            "\"previousBreathType\","
            "\"breathTypeDisplay\","
            "\"spontInspTime\","
            "\"spontTiTtotRatio\","
            "\"peakExpiratoryFlow\","
            "\"endExpiratoryFlow\""
            ") VALUES (%s,'%s',%f,%f,%f,%f,%f,%f,%f,'%s','%s','%s',%f,%f,%f,%f)",
                event_sequence_id.c_str(),
                convert(bdPktPtr->breathType),                 
                convert(bdPktPtr->exhaledTidalVolume),        
                convert(bdPktPtr->breathDuration),            
                convert(bdPktPtr->endExpiratoryPressure),     
                convert(bdPktPtr->ieRatio),                   
                convert(bdPktPtr->mandFraction),              
                convert(bdPktPtr->dynamicCompliance),         
                convert(bdPktPtr->dynamicResistance),        
                convert(bdPktPtr->phaseType),            
                convert(bdPktPtr->previousBreathType),        
                convert(bdPktPtr->breathTypeDisplay),         
                convert(bdPktPtr->spontInspTime),             
                convert(bdPktPtr->spontTiTtotRatio),          
                convert(bdPktPtr->peakExpiratoryFlow),        
                convert(bdPktPtr->endExpiratoryFlow)          
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }

    return status;
}

//---------------------------
// realtime_breath_data     
//---------------------------
bool PatientDataDBHandler::realtime_breath_data (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    RealTimeDataPacket *bdPktPtr = (RealTimeDataPacket *)
        check_header(pMessage, "REALTIME_BREATH_DATA", sizeof(RealTimeDataPacket),"realtime_breath_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(
        pUser, "REALTIME_BREATH_DATA", pMessage->getTimeStamp(), 
        event_sequence_id
    );
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"REALTIME_BREATH_DATA\" ("
            "event_sequence_id,"
            "\"breathType\","
            "\"endInspPressure\","
            "\"phaseType\","
            "\"inspiredlungVolume\","
            "\"supportType\","
            "\"mandType\","
            "\"breathTypeDisplay\","
            "\"peakCircuitPressure\","
            "\"spontInspTime\","
            "\"ventType\","
            "\"breathTriggerId\","
            "\"dynamicCompliance\","
            "\"dynamicResistance\","
            "\"peakSpontInspFlow\""
            ") VALUES (%s,'%s',%f,'%s',%f,'%s','%s','%s',%f,%f,'%s','%s',%f,%f,%f)",
                event_sequence_id.c_str(), //%s
                convert(bdPktPtr->breathType),                 
                convert(bdPktPtr->endInspPressure),        
                convert(bdPktPtr->phaseType),            
                convert(bdPktPtr->inspiredlungVolume),     
                convert(bdPktPtr->supportType),                   
                convert(bdPktPtr->mandType),              
                convert(bdPktPtr->breathTypeDisplay),         
                convert(bdPktPtr->peakCircuitPressure),        
                convert(bdPktPtr->spontInspTime),            
                convert(bdPktPtr->ventType),        
                convert(bdPktPtr->breathTriggerId),         
                convert(bdPktPtr->dynamicCompliance),             
                convert(bdPktPtr->dynamicResistance),          
                convert(bdPktPtr->peakSpontInspFlow)        
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }


    return status;
}

//---------------------------
// averaged_breath_data     
//---------------------------
bool PatientDataDBHandler::averaged_breath_data (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    AveragedBDPacket *bdPktPtr = (AveragedBDPacket *)
        check_header(pMessage, "AVERAGE_BREATH_DATA", sizeof(AveragedBDPacket),"averaged_breath_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(
        pUser, "AVERAGE_BREATH_DATA", 
        pMessage->getTimeStamp(), event_sequence_id
    );
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"AVERAGE_BREATH_DATA\" ("
            "event_sequence_id,"
            "\"exhaledMinuteVol\","
            "\"exhaledSpontMinuteVol\","
            "\"totalRespRate\","
            "\"meanCircuitPressure\","
            "\"spontFtotVtRatio\""
            ") VALUES (%s,%f,%f,%f,%f,%f)",
                event_sequence_id.c_str(), //%s
                convert(bdPktPtr->exhaledMinuteVol),  
                convert(bdPktPtr->exhaledSpontMinuteVol),  
                convert(bdPktPtr->totalRespRate),  
                convert(bdPktPtr->meanCircuitPressure),  
                convert(bdPktPtr->spontFtotVtRatio)  
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());
    }

    return status;
}

//---------------------------
// peep_recovery_msg     
//---------------------------
bool PatientDataDBHandler::peep_recovery_msg                   (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    unsigned int len = ntohs(((NetworkMsgHeader *)(( pMessage.get()->get() ).get()))->pktSize);

    //TODO[ME]: Is there a size that should be associated with packet
    if( (pMessage.get()->getLength()-sizeof(NetworkMsgHeader)) != len)
    {
        LOG_MESSAGE(LM_ERROR,"Failed to insert record into PatientData.event_data");
        return false;
    }

    bool status = insert_event_data(pUser, "PEEP_RECOVERY_MSG", pMessage->getTimeStamp(), event_sequence_id);

    return status;
}          

//---------------------------
// fio2_for_gui_msg      
//---------------------------
bool PatientDataDBHandler::fio2_for_gui_msg (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    static float last_value = 0;
    static float count      = 50;
    bool status = true;

    float *bdPktPtr = (float *)
        check_header(pMessage, "FI02_FOR_GUI_MSG", sizeof(float),"fio2_for_gui_msg");

    if(bdPktPtr==NULL)
    {
        return false;
    }


    if( count == 50 || last_value != convert(*bdPktPtr) ) 
    {
        std::string event_sequence_id;

        status = insert_event_data(
            pUser, "FI02_FOR_GUI_MSG", 
            pMessage->getTimeStamp(), event_sequence_id
        );

        if(status) 
        {

            boost::shared_ptr<char> g_buffer(new char[18000]);

            sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"FI02_FOR_GUI_MSG\" ("
                "event_sequence_id,"
                "\"fio2_data\""
                ") VALUES (%s,%f)",
                    event_sequence_id.c_str(),
                    convert(*bdPktPtr)
                    );
            status     = ((CDatabase *)pUser)->insert_query(g_buffer.get());
            last_value = convert(*bdPktPtr);
        }

        if(count == 50)
        {
            count = 0;
        } 
    }

    count++;
    return status;
}

//---------------------------
// paused_peend_data      
//---------------------------
bool PatientDataDBHandler::paused_peend_data                   (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    float *bdPktPtr = (float *)
        check_header(pMessage, "PAUSE_PEEND_DATA", sizeof(float),"paused_peend_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(
        pUser, "PAUSE_PEEND_DATA", 
        pMessage->getTimeStamp(), event_sequence_id
    );
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);
        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"PAUSE_PEEND_DATA\" ("
            "event_sequence_id,"
            "\"paused_peend_data\""
            ") VALUES (%s,%f)",
                event_sequence_id.c_str(),
                convert(*bdPktPtr)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }


    return status;
}

//---------------------------
// paused_piend_data      
//---------------------------
bool PatientDataDBHandler::paused_piend_data (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    float *bdPktPtr = (float *)check_header(pMessage, "PAUSE_PIEND_DATA", sizeof(float),"paused_piend_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "PAUSE_PIEND_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"PAUSE_PIEND_DATA\" ("
            "event_sequence_id,"
            "\"paused_piend_data\""
            ") VALUES (%s,%f)",
                event_sequence_id.c_str(),
                convert(*bdPktPtr)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }

    return status;
}

//---------------------------
// nif_pressure_data      
//---------------------------
bool PatientDataDBHandler::nif_pressure_data                   (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    float *bdPktPtr = (float *)check_header(pMessage, "NIF_PRESSURE_DATA", sizeof(float),"nif_pressure_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "NIF_PRESSURE_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"NIF_PRESSURE_DATA\" ("
            "event_sequence_id,"
            "\"nif_pressure_data\""
            ") VALUES (%s,%f)",
                event_sequence_id.c_str(),
                convert(*bdPktPtr)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }

    return status;
} 

//---------------------------
// p100_pressure_data      
//---------------------------
bool PatientDataDBHandler::p100_pressure_data                  (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    float *bdPktPtr = (float *)check_header(pMessage, "P100_PRESSURE_DATA", sizeof(float),"p100_pressure_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "P100_PRESSURE_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"P100_PRESSURE_DATA\" ("
            "event_sequence_id,"
            "\"p100_pressure_data\""
            ") VALUES (%s,%f)",
                event_sequence_id.c_str(),
                convert(*bdPktPtr)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }


    return status;
} 

//---------------------------
// vital_capacity_data      
//---------------------------
bool PatientDataDBHandler::vital_capacity_data                 (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    float *bdPktPtr = (float *)check_header(pMessage, "VITAL_CAPACITY_DATA", sizeof(float),"vital_capacity_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "VITAL_CAPACITY_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"VITAL_CAPACITY_DATA\" ("
            "event_sequence_id,"
            "\"vital_capacity_data\""
            ") VALUES (%s,%f)",
                event_sequence_id.c_str(),
                convert(*bdPktPtr)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }

    return status;
} 

//---------------------------
// computed_insp_pause_data      
//---------------------------
bool PatientDataDBHandler::computed_insp_pause_data (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    ComputedInspPauseDataPacket *bdPktPtr = (ComputedInspPauseDataPacket *)check_header(pMessage, "COMPUTED_INSP_PAUSE_DATA", sizeof(ComputedInspPauseDataPacket), "computed_insp_pause_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "COMPUTED_INSP_PAUSE_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);
        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"COMPUTED_INSP_PAUSE_DATA\" ("
            "event_sequence_id,"
            "\"staticCompliance\","
            "\"staticComplianceValid\","
            "\"staticResistance\","
            "\"staticResistanceValid\""
            ") VALUES (%s,%f,'%s',%f,'%s')",
                event_sequence_id.c_str(), //%s
                convert(bdPktPtr->staticCompliance),  
                convert(bdPktPtr->staticComplianceValid),  
                convert(bdPktPtr->staticResistance),  
                convert(bdPktPtr->staticResistanceValid) 
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }

    return status;
}

//---------------------------
// end_exp_pause_pressure_state      
//---------------------------
bool PatientDataDBHandler::end_exp_pause_pressure_state        (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    PauseTypes::PpiState  *bdPktPtr = (PauseTypes::PpiState  *)check_header(pMessage, "END_EXP_PAUSE_PRESSURE_STATE", sizeof(PauseTypes::PpiState), "end_exp_pause_pressure_state");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "END_EXP_PAUSE_PRESSURE_STATE", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);
        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"END_EXP_PAUSE_PRESSURE_STATE\" ("
            "event_sequence_id,"
            "\"end_exp_pause_pressure_state\""
            ") VALUES (%s,'%s')",
                event_sequence_id.c_str(),
                 convert(*bdPktPtr)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }

    return status;
} 

//---------------------------
// vtpcv_state_data      
//---------------------------
bool PatientDataDBHandler::vtpcv_state_data                    (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    VtpcvStateDataPacket  *bdPktPtr = (VtpcvStateDataPacket *)check_header(pMessage, "VTPCV_STATE_DATA", sizeof(VtpcvStateDataPacket),"vtpcv_state_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "VTPCV_STATE_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);
        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"VTPCV_STATE_DATA\" ("
            "event_sequence_id,"
            "\"vtpcvState\""
            ") VALUES (%s,'%s')",
                event_sequence_id.c_str(),
                 convert(bdPktPtr->vtpcvState)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }

    return status;
} 

//---------------------------
// bilevel_mand_data      
//---------------------------
bool PatientDataDBHandler::bilevel_mand_data                   (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    BiLevelMandDataPacket  *bdPktPtr = (BiLevelMandDataPacket *)check_header(pMessage, "BILEVEL_MAND_DATA", sizeof(BiLevelMandDataPacket), "bilevel_mand_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "BILEVEL_MAND_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"BILEVEL_MAND_DATA\" ("
            "event_sequence_id,"
            "\"breathType\","
            "\"endExpiratoryPressure\","
            "\"peakCircuitPressure\","
            "\"phaseType\","
            "\"breathTypeDisplay\""
            ") VALUES (%s,'%s',%f,%f,'%s','%s')",
                event_sequence_id.c_str(),
                 convert(bdPktPtr->breathType),
                 convert(bdPktPtr->endExpiratoryPressure),
                 convert(bdPktPtr->peakCircuitPressure),
                 convert(bdPktPtr->phaseType),
                 convert(bdPktPtr->breathTypeDisplay)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }

    return status;
} 

//---------------------------
// bilevel_mand_exh_data      
//---------------------------
bool PatientDataDBHandler::bilevel_mand_exh_data               (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    BiLevelMandExhDataPacket  *bdPktPtr = (BiLevelMandExhDataPacket *)check_header(pMessage, "BILEVEL_MAND_EXH_DATA", sizeof(BiLevelMandExhDataPacket), "bilevel_mand_exh_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "BILEVEL_MAND_EXH_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"BILEVEL_MAND_EXH_DATA\" ("
            "event_sequence_id,"
            "\"breathType\","
            "\"phaseType\","
            "\"breathTypeDisplay\","
            "\"peakCircuitPressure\""
            ") VALUES (%s,'%s','%s','%s',%f)",
                event_sequence_id.c_str(),
                 convert(bdPktPtr->breathType),
                 convert(bdPktPtr->phaseType),
                 convert(bdPktPtr->breathTypeDisplay),
                 convert(bdPktPtr->peakCircuitPressure)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }

    return status;
} 

//---------------------------
// pav_breath_data      
//---------------------------
bool PatientDataDBHandler::pav_breath_data                     (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    PavBreathDataPacket  *bdPktPtr = (PavBreathDataPacket *)check_header(pMessage, "PAV_BREATH_DATA", sizeof(PavBreathDataPacket), "pav_breath_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "PAV_BREATH_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"PAV_BREATH_DATA\" ("
            "event_sequence_id,"
            "\"patientWorkOfBreathing\","
            "\"totalWorkOfBreathing\","
            "\"elastanceWob\","
            "\"resistiveWob\","
            "\"intrinsicPeep\","
            "\"pavState\""
            ") VALUES (%s,%f,%f,%f,%f,%f,'%s')",
                event_sequence_id.c_str(), //%s
                convert(bdPktPtr->patientWorkOfBreathing),  
                convert(bdPktPtr->totalWorkOfBreathing),  
                convert(bdPktPtr->elastanceWob),  
                convert(bdPktPtr->resistiveWob),  
                convert(bdPktPtr->intrinsicPeep),
                convert(bdPktPtr->pavState)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }
    
    return status;
} 

//---------------------------
// pav_plateau_data      
//---------------------------
bool PatientDataDBHandler::pav_plateau_data                    (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    PavPlateauDataPacket  *bdPktPtr = (PavPlateauDataPacket *)check_header(pMessage, "PAV_PLATEAU_DATA", sizeof(PavPlateauDataPacket), "pav_plateau_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "PAV_PLATEAU_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"PAV_PLATEAU_DATA\" ("
            "event_sequence_id,"
            "\"lungElastance\","
            "\"lungCompliance\","
            "\"patientResistance\","
            "\"totalResistance\","
            "\"pavState\""
            ") VALUES (%s,%f,%f,%f,%f,'%s')",
                event_sequence_id.c_str(), //%s
                convert(bdPktPtr->lungElastance),  
                convert(bdPktPtr->lungCompliance),  
                convert(bdPktPtr->patientResistance),  
                convert(bdPktPtr->totalResistance),  
                convert(bdPktPtr->pavState)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }


    return status;
} 

//---------------------------
// pav_startup_data       
//---------------------------
bool PatientDataDBHandler::pav_startup_data                    (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;
    bool status;

    PavStartupDataPacket  *bdPktPtr = (PavStartupDataPacket *)check_header(pMessage, "PAV_STARTUP_DATA", sizeof(PavStartupDataPacket), "pav_startup_data");

    if(bdPktPtr==NULL)
    {
        return false;
    }

    status = insert_event_data(pUser, "PAV_STARTUP_DATA", pMessage->getTimeStamp(), event_sequence_id);
    
    if(status == true) 
    {

        boost::shared_ptr<char> g_buffer(new char[18000]);
        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"PAV_STARTUP_DATA\" ("
            "event_sequence_id,"
            "\"pavState\""
            ") VALUES (%s,'%s')",
                event_sequence_id.c_str(),
                 convert(bdPktPtr->pavState)
                );

        status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

    }


    return status;

} 
//---------------------------
// SEND_EVENT_MSG_ID       
//---------------------------
bool PatientDataDBHandler::send_event_msg_id	(boost::shared_ptr<Message> pMessage, void *pUser)
{
    bool status = true;


    return status;

}

//---------------------------
// waveform_breath_data       
//---------------------------
bool PatientDataDBHandler::waveform_breath_data                (boost::shared_ptr<Message> pMessage, void *pUser)
{
  
  std::string event_sequence_id;
  bool status;

  unsigned int size = 
        min(ntohs(((NetworkMsgHeader *)(( pMessage.get()->get() ).get()))->pktSize), 
                pMessage.get()->getLength()-sizeof(NetworkMsgHeader));

  WaveformData *pWaveform = (WaveformData *)check_header(pMessage, "WAVEFORM_BREATH_DATA", size,"waveform_breath_data");

  if(pWaveform == NULL)
  {
    return false;
  }

  status = insert_event_data(
        pUser, "WAVEFORM_BREATH_DATA", pMessage->getTimeStamp(), 
        event_sequence_id
    );

  if(status == false)
  {
    return false;
  }


  Uint  nEntries = size/sizeof(WaveformData);


  for (Uint32 i = 0; i< nEntries; i++)
  {
        boost::shared_ptr<char> g_buffer(new char[18000]);

        sprintf(g_buffer.get(),"INSERT INTO \"PatientData\".\"WAVEFORM_BREATH_DATA\" ("
            "event_sequence_id,"
            "event_index,"
            "\"circuitPressure\","
            "\"lungPressure\"," 
            "\"netFlow\"," 
            "\"lungFlow\"," 
            "\"netVolume\","
            "\"lungVolume\"," 
            "\"bdPhase\"," 
            "\"breathType\"," 
            "\"isPeepRecovery\"," 
            "\"isApneaActive\"," 
            "\"isErrorState\"," 
            "\"isAutozeroActive\"," 
            "\"modeSetting\"," 
            "\"supportTypeSetting\","
			"\"timeStamp\""
            ") VALUES (%s,%i,%f,%f,%f,%f,%f,%f,'%s','%s',%s,%s,%s,%s,'%s','%s','%s')",
                event_sequence_id.c_str(),i,
            convert(pWaveform[i].circuitPressure),
            convert(pWaveform[i].lungPressure),
            convert(pWaveform[i].netFlow),
            convert(pWaveform[i].lungFlow),
            convert(pWaveform[i].netVolume),
            convert(pWaveform[i].lungVolume),
            convert(pWaveform[i].bdPhase),
            convert(pWaveform[i].breathType),
            convert(pWaveform[i].isPeepRecovery),
            convert(pWaveform[i].isApneaActive),
            convert(pWaveform[i].isErrorState),
            convert(pWaveform[i].isAutozeroActive),
            "",//convert(pWaveform[i].modeSetting),
            "",//convert(pWaveform[i].supportTypeSetting)
			""//TODO e600			convert(pWaveform[i].timeStamp)
                );

            status = ((CDatabase *)pUser)->insert_query(g_buffer.get());
  }
    return true;


}

//---------------------------
// PatientData::register_types       
//---------------------------
bool PatientDataDBHandler::register_types(IEventLogger *pIEventLogger, void *pCDatabase )
{

    pIEventLogger->addMessageHandler(END_OF_BREATH_DATA           
            ,PatientDataDBHandler::end_of_breath_data,          pCDatabase); //tested
 
    pIEventLogger->addMessageHandler(REALTIME_BREATH_DATA         
            ,PatientDataDBHandler::realtime_breath_data,        pCDatabase); //tested      

    pIEventLogger->addMessageHandler(AVERAGED_BREATH_DATA         
            ,PatientDataDBHandler::averaged_breath_data,        pCDatabase); //tested      

    pIEventLogger->addMessageHandler(PEEP_RECOVERY_MSG            
            ,PatientDataDBHandler::peep_recovery_msg,           pCDatabase);   

    pIEventLogger->addMessageHandler(FIO2_FOR_GUI_MSG             
            ,PatientDataDBHandler::fio2_for_gui_msg,            pCDatabase); //tested  

    pIEventLogger->addMessageHandler(PAUSED_PIEND_DATA            
            ,PatientDataDBHandler::paused_piend_data,           pCDatabase); //tested

    pIEventLogger->addMessageHandler(PAUSED_PEEND_DATA            
            ,PatientDataDBHandler::paused_peend_data,           pCDatabase); //tested  

    pIEventLogger->addMessageHandler(NIF_PRESSURE_DATA            
            ,PatientDataDBHandler::nif_pressure_data,           pCDatabase);   

    pIEventLogger->addMessageHandler(P100_PRESSURE_DATA           
            ,PatientDataDBHandler::p100_pressure_data,          pCDatabase);   

    pIEventLogger->addMessageHandler(COMPUTED_INSP_PAUSE_DATA     
            ,PatientDataDBHandler::computed_insp_pause_data,    pCDatabase);   

    pIEventLogger->addMessageHandler(END_EXP_PAUSE_PRESSURE_STATE 
            ,PatientDataDBHandler::end_exp_pause_pressure_state,pCDatabase);   

    pIEventLogger->addMessageHandler(VITAL_CAPACITY_DATA          
            ,PatientDataDBHandler::vital_capacity_data,         pCDatabase);   

    pIEventLogger->addMessageHandler(VTPCV_STATE_DATA             
            ,PatientDataDBHandler::vtpcv_state_data,            pCDatabase);   

    pIEventLogger->addMessageHandler(BILEVEL_MAND_DATA            
            ,PatientDataDBHandler::bilevel_mand_data,           pCDatabase);   

    pIEventLogger->addMessageHandler(BILEVEL_MAND_EXH_DATA        
            ,PatientDataDBHandler::bilevel_mand_exh_data,       pCDatabase);   

    pIEventLogger->addMessageHandler(PAV_BREATH_DATA              
            ,PatientDataDBHandler::pav_breath_data,             pCDatabase);   

    pIEventLogger->addMessageHandler(PAV_PLATEAU_DATA             
            ,PatientDataDBHandler::pav_plateau_data,            pCDatabase);   

    pIEventLogger->addMessageHandler(PAV_STARTUP_DATA             
            ,PatientDataDBHandler::pav_startup_data,            pCDatabase);   

    pIEventLogger->addMessageHandler(WAVEFORM_BREATH_DATA         
            ,PatientDataDBHandler::waveform_breath_data,        pCDatabase);     

    pIEventLogger->addMessageHandler(SEND_EVENT_MSG_ID         
            ,PatientDataDBHandler::send_event_msg_id,        pCDatabase);       

    return true;

}



