#ifndef SmPressureController_HH
#define SmPressureController_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: SmPressureController - Pressure controller for Service Mode
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmPressureController.hhv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		Added getPressureTarget() if development options are allowed
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date: 24-Aug-1995    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "SmFlowController.hh"

#include "PressureSensor.hh"

#ifdef SIGMA_UNIT_TEST
# include "FakeSensor.hh"
#endif  // SIGMA_UNIT_TEST
 
//@ End-Usage


class SmPressureController : public SmFlowController
{
  public:
    SmPressureController(void);
    ~SmPressureController(void);

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);

    Real32 inline getPressureTarget(void) const;

    virtual void newCycle( void);
    void establishPressure( Real32           desiredPressure,
                            MeasurementSide  desiredSide,
                            GasType          desiredGas,
                            Real32 			 flowRate);
    void stopPressureController( void);
    Boolean waitForPressure( Int32 timeoutMS, Boolean displayData = FALSE);

  protected:

  private:
    SmPressureController(const SmPressureController&);	// not implemented...
    void   operator=(const SmPressureController&);	// not implemented...

    //@ Data-Member:  pressureTarget_
    // The requested pressure target
    Real32		pressureTarget_;

    //@ Data-Member:  requestForPressureControl_
    // pressure controller request flag
    Boolean		requestForPressureControl_;

    //@ Data-Member:  pressureControllerActive_
    // pressure controller active flag
    Boolean		pressureControllerActive_;

    //@ Data-Member:  pPressureSensor_
    // The pressure sensor being used to control flow
#ifndef SIGMA_UNIT_TEST
    PressureSensor* pPressureSensor_;
#else
	FakeSensor* pPressureSensor_;
#endif  // SIGMA_UNIT_TEST

	//@ Data-Member: pressureControllerDone_
	// flag signalling pressure target was reached
	Boolean pressureControllerDone_;
};

// Inlined methods...
#include "SmPressureController.in"

#endif // SmPressureController_HH 
