
#ifndef ApneaInterval_IN
#define ApneaInterval_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: ApneaInterval - manages changes to the actual apnea interval.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ApneaInterval.inv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: gdc     Date:  23-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal nCPAP. 
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  iv    Date:  19-Aug-1997    DR Number: DCS 1656
//       Project:  Sigma (840)
//       Description:
//             Capitalize all global references per coding standard.
//
//  Revision: 003  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal Breath Delivery code review.
//
//  Revision: 002  By:  iv    Date:  04-Oct-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Deleted method setElapsedInterval().
//
//  Revision: 001  By:  syw    Date:  26-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: extendInterval
//
//@ Interface-Description
// The actual apnea interval is extended by the Int32 argument value. 
// The target time for the apnea timer is set for the new actual apnea
// interval. This service is called by a client that needs to extend the
// current apnea interval.
//---------------------------------------------------------------------
//@ Implementation-Description
// The private data member actualApneaInterval_ is incremented by the intervalDuration
// value. The timer reference RApneaTimer is instructed to set a new target time.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline void
ApneaInterval::extendInterval(const Int32 intervalDuration)
{
// $[TI1]
	CALL_TRACE("extendInterval(Int32 intervalDuration)");

	if (!isApneaDetectionOff_)
	{
		actualApneaInterval_ += intervalDuration;
		RApneaTimer.setTargetTime(actualApneaInterval_);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getApneaInterval
//
//@ Interface-Description
//	The method provides access to the value of the apnea interval as it was
//	before the interval correction by the delta time. It takes no
//	arguments and returns an Int 32.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method returns the value of the private data member actualApneaInterval_
//	minus DELTA_.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Int32
ApneaInterval::getApneaInterval(void) const
{
// $[TI1]
	CALL_TRACE("ApneaInterval::getApneaInterval(void)");

	return (actualApneaInterval_ - DELTA_);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#endif // ApneaInterval_IN 







