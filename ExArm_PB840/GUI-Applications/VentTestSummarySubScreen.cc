#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VentTestSummarySubScreen - The screen selected by pressing the
// Ventilator Test Summary button on the Upper Other Screens subscreen.
// Displays a summary of the last run of EST and SST
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen displays a summary of the last run of EST, SST
// The information is presented in the form of
// a table with 2 rows and 3 columns.  There is a row for EST, SST and
// The 3 columns display the test name, the time and date when the test
//	was last run and the outcome of the last run.
// If the test has never been run then the last run column will be blank
// and the outcome column contains "NEVER RUN".
// The failed testing status for Exh. valve, Flow sensor, Atm pressure
// transducer calibations will be displayed.
// The progressing or failed  vent Inop test status will be displayed also.
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// getLogEntryColumn() method is used for retrieving the textual content
// of each entry in the table display.
//---------------------------------------------------------------------
//@ Rationale
// A subscreen was needed to display test summary information.
//---------------------------------------------------------------------
//@ Implementation-Description
// A ScrollableLog is used to display the table information.  It retrieves
// the content of each table cell from the getLogEntryColumn() and formats
// the text before displaying it.
//---------------------------------------------------------------------
//@ Fault-Handling
// Normal logic assertions.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VentTestSummarySubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By:  gdc 	   Date:  26-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 008  By:  btray	   Date:  03-Aug-1998    DCS Number: 5492
//  Project: ATC 
//  Description:
//	Moved X-position so that changes to German translations fit correctly.	
//
//  Revision: 007  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 006  By:  clw    Date: 19-May-1998    DR Number:
//  Project:  Sigma (840)
//  Description:
//      For Japanese project: for japanese only (conditional compilation) the
//      date format has been changed to Japanese format.
//
//  Revision: 005  By:  yyy    Date: 16-Oct-1997    DR Number: DCS 2565
//  Project:  Sigma (840)
//  Description:
//		Mapped new SRS.
//
//  Revision: 004  By:  yyy    Date:  26-SEP-1997    DR Number:   1861
//    Project:  Sigma (R8027)
//    Description:
//      Display more clear diagnostic information for all types of calibration
//		tests and vent inop test.
//
//  Revision: 003  By:  yyy    Date:  01-JULY-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      Modified the code to allow the awk program to extract the log information.
//
//  Revision: 002  By: sah    Date:  23-Jun-1997    DCS Number: 2214
//  Project:  Sigma (R8027)
//  Description:
//      Fixed problem where Test Summary screen was not showing the
//      correct overall result, by replacing the call to 'getResultOfCurrRun()'
//      with call to 'getResultId()'.  Also removed unneeded assertion
//      (done by client code).
//
//  Revision: 001  By:  mpm    Date:  18-AUG-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//
//=====================================================================

#include "VentTestSummarySubScreen.hh"

//@ Usage-Classes
#include "NovRamManager.hh"
#include "TextUtil.hh"

#include "Array_ResultTableEntry_MAX_TEST_ENTRIES.hh"
#include "MiscStrs.hh"
#include "ScrollableLog.hh"
#include "UpperSubScreenArea.hh"
#include "GuiApp.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 TABLE_X_ = 42;
static const Int32 TABLE_Y_ = 83;
static const Int32 TABLE_ROW_HEIGHT_ = 38;
static const Int32 TABLE_NUM_ROWS_ = 2;
static const Int32 TABLE_NUM_COLUMNS_ = 3;
static const Int32 TABLE_COLUMN1_WIDTH_ = 100;
static const Int32 TABLE_COLUMN2_WIDTH_ = 157;
static const Int32 TABLE_COLUMN3_WIDTH_ = 283;
static const Int32 ERROR_LABEL_X_ = (TABLE_X_+ TABLE_COLUMN1_WIDTH_ - 40);
static const Int32 ERROR_HEIGHT_ = 23;
static const Int32 ERROR_VALUE_X_ =
							TABLE_X_+
							TABLE_COLUMN1_WIDTH_+
							TABLE_COLUMN2_WIDTH_+
							TABLE_COLUMN3_WIDTH_/2;
static const Int32 ERROR_Y_ =
							TABLE_Y_+
							TABLE_ROW_HEIGHT_*
							TABLE_NUM_ROWS_ +
							ERROR_HEIGHT_;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VentTestSummarySubScreen()  [Constructor]
//
//@ Interface-Description
// Default constructor.  Passed a pointer to the subscreen area which creates
// it (the upper subscreen area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and sets the size and color of the subscreen.  Adds
// the title area to the subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VentTestSummarySubScreen::VentTestSummarySubScreen(SubScreenArea* pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleArea_(MiscStrs::VENT_TEST_SUMMARY_TITLE, SubScreenTitleArea::SSTA_CENTER),
	pSummaryTable_(NULL),
	calWarningValue_(MiscStrs::VTS_CAL_WARNING_VALUE),
	calWarningLabel_(MiscStrs::VTS_CAL_WARNING_LABEL),
	flowSensorWarningValue_(MiscStrs::VTS_CAL_WARNING_VALUE),
	flowSensorWarningLabel_(MiscStrs::VTS_FLOW_SENSOR_WARNING_LABEL),
	testVentInopWarningValue_(MiscStrs::VTS_CAL_WARNING_VALUE),
	testVentInopWarningLabel_(MiscStrs::VTS_TEST_VENT_INOP_WARNING_LABEL),
	pressureXducerCalWarningValue_(MiscStrs::ATM_PRESSURE_NOT_CALIBRATED_MSG),
	pressureXducerCalWarningLabel_(MiscStrs::VTS_PRESSURE_XDUCER_CAL_WARNING_LABEL)
{
	CALL_TRACE("VentTestSummarySubScreen::VentTestSummarySubScreen(pSubScreenArea)");

	// Size and position the area
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	// Display the screen title
	addDrawable(&titleArea_);

	calWarningValue_.setShow(FALSE);
	calWarningLabel_.setShow(FALSE);
	flowSensorWarningValue_.setShow(FALSE);
	flowSensorWarningLabel_.setShow(FALSE);
	testVentInopWarningValue_.setShow(FALSE);
	testVentInopWarningLabel_.setShow(FALSE);
	pressureXducerCalWarningValue_.setShow(FALSE);
	pressureXducerCalWarningLabel_.setShow(FALSE);

	// Add the following drawables
	addDrawable(&calWarningValue_);
	addDrawable(&calWarningLabel_);
	addDrawable(&flowSensorWarningValue_);
	addDrawable(&flowSensorWarningLabel_);
	addDrawable(&testVentInopWarningValue_);
	addDrawable(&testVentInopWarningLabel_);
	addDrawable(&pressureXducerCalWarningValue_);
	addDrawable(&pressureXducerCalWarningLabel_);

	calWarningValue_.setColor(Colors::WHITE);
	calWarningLabel_.setColor(Colors::WHITE);
	flowSensorWarningValue_.setColor(Colors::WHITE);
	flowSensorWarningLabel_.setColor(Colors::WHITE);
	testVentInopWarningValue_.setColor(Colors::WHITE);
	testVentInopWarningLabel_.setColor(Colors::WHITE);
	pressureXducerCalWarningValue_.setColor(Colors::WHITE);
	pressureXducerCalWarningLabel_.setColor(Colors::WHITE);

								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VentTestSummarySubScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VentTestSummarySubScreen::~VentTestSummarySubScreen(void)
{
	CALL_TRACE("VentTestSummarySubScreen::~VentTestSummarySubScreen(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate		[virtual]
//
//@ Interface-Description
// Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
// First create the scrollable log used for the information display.
// Initialize the log and display and activate it.
// Display all possible error messages.
// $[01341] The date/time and outcome of the most recent SST shall ...
// $[01343] The date/time and outcome of the most recent EST ...
// $[01364] The Test Summary subscreen shall provide an indication to the...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentTestSummarySubScreen::activate(void)
{
	CALL_TRACE("VentTestSummarySubScreen::activate(void)");

	// Create the log
	pSummaryTable_ = ScrollableLog::New(this, TABLE_NUM_ROWS_, TABLE_NUM_COLUMNS_,
										TABLE_ROW_HEIGHT_, FALSE);

	// Setup summary table stuff
	pSummaryTable_->setX(TABLE_X_);
	pSummaryTable_->setY(TABLE_Y_);

	// Setup column and entry info for the table's ScrollableLog
	 
	pSummaryTable_->setColumnInfo(0, L"", TABLE_COLUMN1_WIDTH_, CENTER, 2,
						TextFont::NORMAL, TextFont::TWELVE, TextFont::TWELVE);
	pSummaryTable_->setColumnInfo(1, MiscStrs::VTS_LAST_RUN_COLUMN_TITLE,
						TABLE_COLUMN2_WIDTH_, CENTER, 2,
						TextFont::NORMAL, TextFont::TWELVE, TextFont::TWELVE);
	pSummaryTable_->setColumnInfo(2, MiscStrs::VTS_OUTCOME_COLUMN_TITLE,
						TABLE_COLUMN3_WIDTH_, CENTER, 2,
						TextFont::NORMAL, TextFont::TWELVE, TextFont::TWELVE);
	pSummaryTable_->setEntryInfo(0, TABLE_NUM_ROWS_);

	// Display and activate the log
	addDrawable(pSummaryTable_);
	pSummaryTable_->activate();

	Int32 errorCount = 0;

	if (!GuiApp::IsExhValveCalSuccessful())
	{		// $[TI1.1]
		updateErrorMsg_(calWarningLabel_, calWarningValue_, errorCount);

		// Show the calibration warning message Only when the
		// previous calibration failed.
		calWarningValue_.setShow(TRUE);
		calWarningLabel_.setShow(TRUE);
	}		// $[TI1.2]

	if (!GuiApp::IsFlowSensorCalPassed())
	{		// $[TI2.1]
		updateErrorMsg_(flowSensorWarningLabel_, flowSensorWarningValue_, errorCount);

		// Show the calibration warning message Only when the
		// previous calibration failed.
		flowSensorWarningValue_.setShow(TRUE);
		flowSensorWarningLabel_.setShow(TRUE);
	}		// $[TI2.2]

	if (GuiApp::IsBdVentInopTestInProgress() || GuiApp::IsGuiVentInopTestInProgress())
	{		// $[TI3.1]
		testVentInopWarningValue_.setText(MiscStrs::EXH_V_CAL_RUNNING_MSG);
		updateErrorMsg_(testVentInopWarningLabel_, testVentInopWarningValue_, errorCount);

		// Show the calibration warning message Only when the
		// previous calibration failed.
		testVentInopWarningValue_.setShow(TRUE);
		testVentInopWarningLabel_.setShow(TRUE);
	}
	else if (GuiApp::IsVentInopMajorFailure())
	{		// $[TI3.2]
		testVentInopWarningValue_.setText(MiscStrs::VTS_CAL_WARNING_VALUE);
		updateErrorMsg_(testVentInopWarningLabel_, testVentInopWarningValue_, errorCount);

		// Show the calibration warning message Only when the
		// previous calibration failed.
		testVentInopWarningValue_.setShow(TRUE);
		testVentInopWarningLabel_.setShow(TRUE);
	}		// $[TI3.3]

	if (!GuiApp::IsAtmPresXducerCalSuccessful())
	{		// $[TI4.1]
		testVentInopWarningValue_.setText(MiscStrs::VTS_CAL_WARNING_VALUE);
		updateErrorMsg_(pressureXducerCalWarningLabel_,
						pressureXducerCalWarningValue_, errorCount);

		// Show the calibration warning message Only when the
		// previous calibration failed.
		pressureXducerCalWarningValue_.setShow(TRUE);
		pressureXducerCalWarningLabel_.setShow(TRUE);
	}		// $[TI4.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate		[virtual]
//
//@ Interface-Description
// Called when this subscreen is removed from the display allowing any
// needed cleanup to be done.
//---------------------------------------------------------------------
//@ Implementation-Description
// Remove the scrollable log from the display.  Hide all error messages.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentTestSummarySubScreen::deactivate(void)
{
	CALL_TRACE("VentTestSummarySubScreen::deactivate(void)");

	calWarningValue_.setShow(FALSE);
	calWarningLabel_.setShow(FALSE);
	flowSensorWarningValue_.setShow(FALSE);
	flowSensorWarningLabel_.setShow(FALSE);
	testVentInopWarningValue_.setShow(FALSE);
	testVentInopWarningLabel_.setShow(FALSE);
	pressureXducerCalWarningValue_.setShow(FALSE);
	pressureXducerCalWarningLabel_.setShow(FALSE);

	pSummaryTable_->deactivate();

	// Remove scrollable log
	removeDrawable(pSummaryTable_);						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of a summary table entry.  Passed the following
// parameters:
// >Von
//	entryNumber			The index of the log entry
//	columnNumber		The column number of the log entry
//	rIsCheapText		Output parameter.  A reference to a flag which
//						determines whether the text content of the log
//						entry is specified as cheap text or not.
//	rString1			String id of the first string. Can be
//						set to NULL_STRING_ID to indicate an empty entry
//						column.
//	rString2			String id of the second string.
//	rString3			String id of the third string.  Not used if
//						rIsCheapText is FALSE.
//	rString1Message		A help message that will be displayed in the
//						Message Area when rString1 is touched.  This only
//						works when rIsCheapText is TRUE.
// >Voff
// We retrieve information for the appropriate column, make sure it's
// formatted correctly and return it to ScrollableLog.
//---------------------------------------------------------------------
//@ Implementation-Description
// Depending on which column is requested we return the textual content of the
// specified menu entry.  Column 0 is the test column (specifies SST, EST 
// column 1 is the time/date on which the test was last
// run and column 2 is the test outcome column.  The automatic text formatting
// feature of ScrollableLog is used for all columns.
//---------------------------------------------------------------------
//@ PreCondition
// The entry and column number parameters must be within range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentTestSummarySubScreen::getLogEntryColumn(Uint16 entry, Uint16 column,
				Boolean &rIsCheapText, StringId &rString1, StringId &rString2,
				StringId &, StringId &)
{
	CALL_TRACE("VentTestSummarySubScreen::getLogEntryColumn(Uint16 entry, Uint16 column, "
				"Boolean &rIsCheapText, StringId &rString1, "
				"StringId &rString2, StringId &rString3, StringId &rString1Message)");

	CLASS_PRE_CONDITION((entry < TABLE_NUM_ROWS_)
						&& (column < TABLE_NUM_COLUMNS_));

	ResultEntryData overallResultData;
	static wchar_t timeBuf[100];  
	TestResultId testResultId;
	
	// No cheap text is used for any menu information
	rIsCheapText = FALSE;
	rString2 = NULL_STRING_ID;

	switch (entry)
	{
	case 0:												// $[TI1]
		// SST entry

		// Get the overall testing status
		NovRamManager::GetOverallSstResult(overallResultData);
		rString1 = MiscStrs::VTS_SST_LABEL;
		break;

	case 1:												// $[TI2]
		// EST entry

		// Get the overall testing status
		NovRamManager::GetOverallEstResult(overallResultData);
		rString1 = MiscStrs::VTS_EST_LABEL;
		break;

	default:					// Impossible!
		CLASS_ASSERTION(FALSE);
		break;
	}

	// Get the overall test result.
	testResultId = overallResultData.getResultId();
	
	// Check for Last Run time/date column
	if (1 == column)
	{
		if (UNDEFINED_TEST_RESULT_ID  == testResultId ||
			INCOMPLETE_TEST_RESULT_ID == testResultId)
		{												// $[TI5]
			rString1 = NULL_STRING_ID;
		}
		else
		{												// $[TI6]
			const TimeStamp & timeStamp = overallResultData.getTimeOfResult();

			wchar_t format[30];   
			swprintf(format, L"%%T  %s", MiscStrs::LOG_DATE_FORMAT);
			TextUtil::FormatTime(timeBuf, format, timeStamp);
			rString1 = timeBuf;
		}
	}
	// Check for Outcome column
	else if (2 == column)
	{													// $[TI7]
		rString1 = getOutcomeString_(testResultId);
	}													// $[TI8]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getOutcomeString_	[private]
//
//@ Interface-Description
// For a particular TestResultId (the only parameter) this method returns
// the corresponding language string.
//---------------------------------------------------------------------
//@ Implementation-Description
// Examine the value of 'resultId' and return the corresponding string.
// $[01342] If SST outcome is OVERRIDEN, then ...
// $[01344] If EST outcome is OVERRIDEN, then ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

StringId
VentTestSummarySubScreen::getOutcomeString_(TestResultId resultId)
{
	CALL_TRACE("VentTestSummarySubScreen::getOutcomeString_(TestResultId resultId)");
	StringId stringId = NULL_STRING_ID;
	
	switch (resultId)
	{
	case PASSED_TEST_ID:								// $[TI1]
		stringId = MiscStrs::VTS_PASSED_LABEL;
		break;

	case OVERRIDDEN_TEST_ID:							// $[TI2]
		stringId = MiscStrs::VTS_OVERRIDDEN_LABEL;
		break;

	case MINOR_TEST_FAILURE_ID:							// $[TI3]
		stringId = MiscStrs::VTS_MINOR_FAILURE_LABEL;
		break;

	case MAJOR_TEST_FAILURE_ID:							// $[TI4]
		stringId = MiscStrs::VTS_MAJOR_FAILURE_LABEL;
		break;

	case INCOMPLETE_TEST_RESULT_ID:
		stringId = MiscStrs::VTS_INCOMPLETE_TEST_LABEL;
		break;

	case UNDEFINED_TEST_RESULT_ID:						// $[TI5]
		stringId = MiscStrs::VTS_UNDEFINED_RESULT_LABEL;
		break;

	case NOT_APPLICABLE_TEST_RESULT_ID:
	default:
		CLASS_ASSERTION(FALSE);
		break;
	}
	
	return(stringId);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateErrorMsg_	[private]
//
//@ Interface-Description
// Display the error label and error message as request by
// errorCount.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the X, Y coordinates for the error label and message.  Increment
// the error count.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentTestSummarySubScreen::updateErrorMsg_(TextField &warningLabel, TextField &warningValue, Int32 &errorCount)
{
	CALL_TRACE("VentTestSummarySubScreen::updateErrorMsg_(TextField warningLabel, TextField warningValue, Int32 errorCount)");

	warningLabel.setX(ERROR_LABEL_X_);
	warningLabel.setY(ERROR_Y_ + ERROR_HEIGHT_* errorCount);
	
	warningValue.setX(ERROR_VALUE_X_);
	warningValue.setY(ERROR_Y_ + ERROR_HEIGHT_* errorCount);

	errorCount++;
								// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
VentTestSummarySubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							VENTTESTSUMMARYSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
