
#ifndef TimersRefs_HH
#define TimersRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// File: TimersRefs - All the external references of various Timers 
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/TimersRefs.hhv   25.0.4.0   19 Nov 2013 14:00:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  rpr   Date:  17-Oct-2008    SCR Number: 6435
//       Project:  840S
//       Description:
//             Added RO2MixTimer Timer ref in support of +20
//
//  Revision: 005   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM maneuver timers.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 002  By:  kam   Date:  06-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added Service Mode Switch Confirmation Timer ref
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

class IntervalTimer;
extern IntervalTimer& RApneaTimer;
extern IntervalTimer& RBackupTimeExpTimer;
extern IntervalTimer& RSimvInspTimer;
extern IntervalTimer& RInspTimer;
extern IntervalTimer& RPauseTimeoutTimer;
extern IntervalTimer& RO2Timer;
extern IntervalTimer& RO2MixTimer;
extern IntervalTimer& RPressureXducerAutozeroTimer;
extern IntervalTimer& ROscInspTimer;
extern IntervalTimer& RSvcTimer;
extern IntervalTimer& RSimvCycleTimer;
extern IntervalTimer& RBiLevelCycleTimer;
extern IntervalTimer& ROscBackupInspTimer;
extern IntervalTimer& RAvgBreathDataTimer;
extern IntervalTimer& RSmSwitchConfirmationTimer;
extern IntervalTimer& RInspPauseManeuverTimer;
extern IntervalTimer& RExpPauseManeuverTimer;
extern IntervalTimer& RNifManeuverTimer;
extern IntervalTimer& RP100ManeuverTimer;
extern IntervalTimer& RVitalCapacityManeuverTimer;

class TimerMediator;
extern TimerMediator& RTimerMediator;

class CycleTimer;
extern CycleTimer& RCycleTimer;

#endif // TimersRefs_HH 
