#ifndef ModeTrigger_HH
#define ModeTrigger_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: ModeTrigger -  This class is an abstract base class for all
//              mode triggers the Breath Delivery system may need
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ModeTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "Trigger.hh"

//@ End-Usage


class ModeTrigger : public Trigger
{
  public:

    ModeTrigger(const Trigger::TriggerId id);
    virtual ~ModeTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);

    inline void setIsEnableRequested(const Boolean isEnableRequested);
    inline void setIsDisableRequested(const Boolean isDisableRequested);
    inline Boolean isEnableRequested(void) const;
    inline Boolean isDisableRequested(void) const;

	
  protected:
    virtual void triggerAction_(const Boolean trigCond);

  private:
	
    ModeTrigger(void);
    ModeTrigger(const ModeTrigger&);    // Declared but not implemented
    void operator=(const ModeTrigger&);   // Declared but not implemented

    //@ Data-Member: isEnableRequested_
    // request this trigger to be enabled 
    Boolean isEnableRequested_;

    //@ Data-Member: isDisableRequested_
    // request this trigger to be disabled 
    Boolean isDisableRequested_;

};

// Inlined methods
#include "ModeTrigger.in"

#endif // ModeTrigger_HH 
