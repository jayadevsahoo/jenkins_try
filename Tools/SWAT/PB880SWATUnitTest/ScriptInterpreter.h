#pragma once
#include "PB880SWATUnitTest.h"
#include "PB880SWATUnitTestDlg.h"

typedef void (CPB880SWATUnitTestDlg::*PMFN_HOSTLOG)( const TCHAR* pLogMsg );

class HostLogFunction
{
private:
	CPB880SWATUnitTestDlg*	pHostDlg_;
	PMFN_HOSTLOG				pLogFunc_;

public:
	HostLogFunction( CPB880SWATUnitTestDlg* pDlg, PMFN_HOSTLOG pLogFunc );

	void operator() ( const TCHAR* pLogMsg );
};

struct lua_State;
class ScriptInterpreter
{
private:
	lua_State* L_;
	static HostLogFunction logFunc_;

	int SetLuaPath( lua_State* L, const char* path );
	static int host_log( lua_State* L );

public:

	ScriptInterpreter(const HostLogFunction& logFunc);
	~ScriptInterpreter(void);

	bool RunLuaFile(const TCHAR* fileName, CString& strMsg);
};
