
//*****************************************************************************
//
//  Crc.h - Header file for CRC functions.
//
//*****************************************************************************

#ifndef CRC_H

#define CRC_H

    // CRC using start address and number of bytes
    DWORD CalculateCrc(UINT8 *Buffer, DWORD NumberOfBytes);          // Calculate CRC

    // CRC using start and end addresses for the data
    DWORD CalculateCrc(UINT8 *BeginBuffer, UINT8 *StopAddress);

    // Crc, continuing a previous CRC calc, using a single byte of input
    DWORD CalculateCrc(DWORD CalculatedCrc, UINT8 Byte);

    // Crc, continuing a previous CRC calc, using a start address and number of bytes
    DWORD CalculateCrc(DWORD CalculatedCrc, UINT8 *Buffer, int NumberOfBytes);

#endif

