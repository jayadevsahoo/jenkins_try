/************************************************************************/
/* THIS INFORMATION IS PROPRIETARY TO                                   */
/* MICROTEC - A Mentor Graphics Company                                 */
/*----------------------------------------------------------------------*/
/* Copyright (c) 1992-1997 Microtec - A Mentor Graphics Company         */
/* All rights reserved                                                  */
/************************************************************************/
/*
**The software source code contained in this file and in the related
**header files is the property of Microtec. and may only be
**used under the terms and conditions defined in the Microtec
**license agreement. You may modify this code as needed but you must 
**retain the Microtec copyright notices, including this statement.
**Some restrictions may apply when using this software with non-Microtec
**software.  In all cases, Microtec reserves all rights.
*/
/************************************************************************/

/*
 *
 *	Module Name:		%M%
 * 
 *	Identification:		%Z% %I% %M%
 *
 *	Date:			%G%  %U%
 *
 */

/*
 * z8530.h
 *
 * This file contains all of the definitions specific to the 
 * Zilog Z8530 Serial Communications Controller. It contains no 
 * information specific to a Z8530's configuration on a specific
 * target (e.g. register addresses). It can be used for a Z8530 
 * on any target board without modification.
 *
 * History:
 *
 * Date       Name                             Description
 * ---------------------------------------------------------------------
 * 10-23-92   Sanjeev Mahajan     Added externio fops
 * 02-02-93   Cindy Gin           Added an extra field to the z8530_specific_t
 *                                structure. This field is to determine
 *                                what the clock source is RTcX or baud rate.
 * 02-04-93   Roger Cole          changed zscc and z8530 to zi8530
 * 03-12-96   Makoto Saito        Fixed WR3 and WR3 definitions for bits/char
 * 08-11-10   Gary Cederquist     API related changes to support serial channel
 *                                reset. SCR 6663. Removed zi8530_tty_rx_txt_7().
 *                                                                 
 */

#include <serial_2.h>
#include <extio_1.h>

typedef struct {
    volatile unsigned char *ctrl_port;	/* port  control register address */
    volatile unsigned char *data_port;	/* port  data register address */
} zi8530_reg_map_t;

typedef struct {
    unsigned char	vector;
    unsigned char	zi8530_wr1;	/* shadow copy of port A/B wr1 */
    unsigned char	clock_src;	/* set clock as the baud rate or as the RTxC */
    unsigned char   channel;  // channel A or B
} zi8530_specific_t;

typedef struct {
    serial_config_t	sconf;
    zi8530_reg_map_t	regs;
    extern_fops_t	externio;       /* external reg read/write & intrpt functions */
    zi8530_specific_t	specific;
} zi8530_config_t;

/* **** Determines what the clock source is baud rate or as the RTxC */
#define DPLL_ENABLE  1
#define BAUD_RATE_GENERATOR 2

/* dual channel UART */
#define SCC_CHANNEL_A   0
#define SCC_CHANNEL_B   1

/* **** Bit equates for read register 0 */

#define RR0_RXCH        0x01 /* Rx character available */
#define RR0_ZRCNT       0x02 /* zero count */
#define RR0_TXBE        0x04 /* Tx buffer empty */
#define RR0_DCD         0x08 /* dcd */
#define RR0_SYNC        0x10 /* sync/hunt */
#define RR0_CTS         0x20 /* cts */
#define RR0_TXUND       0x40 /* Tx underrun/eom */
#define RR0_BRK         0x80 /* break/abort */


/* **** Bit equates for read register 1 */

#define RR1_SENT        0x01 /* all sent */
#define RR1_RC2         0x02 /* residue code 2 */
#define RR1_RC1         0x04 /* residue code 1 */
#define RR1_RC0         0x08 /* residue code 0 */
#define RR1_PARERR      0x10 /* parity error */
#define RR1_RXOVR       0x20 /* Rx overrun error */
#define RR1_FRERR       0x40 /* framing error (asynch mode) */
#define RR1_CRCERR      0x40 /* crc error (synch/SDLC mode) */
#define RR1_EOF         0x80 /* end of frame (SDLC mode) */


/* **** Bit equates for read register 3 */

#define RR3_BESIP       0x01 /* channel B ext/stat IP */
#define RR3_BTXIP       0x02 /* channel B Tx IP */
#define RR3_BRXIP       0x04 /* channel B Rx IP */
#define RR3_AESIP       0x08 /* channel A ext/stat IP */
#define RR3_ATXIP       0x10 /* channel A Tx IP */
#define RR3_ARXIP       0x20 /* channel A Rx IP */


/* **** Bit equates for read register 10 */

#define RR10_ONLP       0x02 /* on loop */
#define RR10_LPSND      0x10 /* loop sending */
#define RR10_2CLKMS     0x40 /* two clocks missing */
#define RR10_1CLKMS     0x80 /* one clock missing */


/* **** Bit equates for read register 15 */

#define RR15_ZEROIE     0x02 /* baud-count-zero int enable */
#define RR15_DCDIE      0x08 /* DCD int enable  */
#define RR15_SYNCIE     0x10 /* sync int enable (asynch mode) */
#define RR15_HUNTIE     0x10 /* hunt int enable (synch mode) */
#define RR15_CIE        0x20 /* cts int enable */
#define RR15_TXUIE      0x40 /* Tx underrun int enable */
#define RR15_BRKIE      0x80 /* break/abort int enable */


/* **** Bit equates for write register 0 */

#define WR0_R0          0x00 /* select R/W reg 0 */
#define WR0_R1          0x01 /* select R/W reg 1 */
#define WR0_R2          0x02 /* select R/W reg 2 */
#define WR0_R3          0x03 /* select R/W reg 3 */
#define WR0_R4          0x04 /* select R/W reg 4 */
#define WR0_R5          0x05 /* select R/W reg 5 */
#define WR0_R6          0x06 /* select R/W reg 6 */
#define WR0_R7          0x07 /* select R/W reg 7 */
#define WR0_R8          0x08 /* select R/W reg 8 */
#define WR0_R9          0x09 /* select R/W reg 9 */
#define WR0_R10         0x0A /* select R/W reg 10 */
#define WR0_R11         0x0B /* select R/W reg 11 */
#define WR0_R12         0x0C /* select R/W reg 12 */
#define WR0_R13         0x0D /* select R/W reg 13 */
#define WR0_R14         0x0E /* select R/W reg 14 */
#define WR0_R15         0x0F /* select R/W reg 15 */
#define WR0_NULL        0x00 /* null code */
#define WR0_PTHIGH      0x08 /* point high */
#define WR0_EXTRES      0x10 /* reset ext/stat interrupts */
#define WR0_SNDAB       0x18 /* send abort (SDLC) */
#define WR0_NEXTRX      0x20 /* enable int on next Rx character */
#define WR0_TXRES       0x28 /* reset Txint pending */
#define WR0_ERRES       0x30 /* error reset */
#define WR0_HIRES       0x38 /* reset highest IUS */
#define WR0_RCRCRES     0x40 /* reset Rx crc checker */
#define WR0_TCRCRES     0x80 /* reset Tx crc generator */
#define WR0_TXURES      0xC0 /* reset Tx underrun/eom latch */


/* **** Bit equates for write register 1 */

#define WR1_EXTIE       0x01 /* ext int enable */
#define WR1_TXIE        0x02 /* Tx int enable */
#define WR1_PARIE       0x04 /* parity error int enable */
#define WR1_FIRSTRX     0x08 /* Rx int on 1st char/special cond */
#define WR1_RXIE        0x10 /* int on all Rx char/special cond */
#define WR1_SPCL        0x18 /* Rx int on special condition only */
#define WR1_WDRXTX      0x20 /* wait/dma request on receive/xmit */
#define WR1_WDREQ       0x40 /* wait/dma request function */
#define WR1_WDENA       0x80 /* wait/dma request enable */


/* **** Bit equates for write register 3 */

#define WR3_RXENA       0x01	/* Rx enable */
#define WR3_SSYNC       0x02	/* strip synch chars (load inhibit) */
#define WR3_ADDSRCH     0x04	/* address search mode (SDLC) */
#define WR3_RXCRC       0x08	/* Rx crc enable */
#define WR3_HUNT        0x10	/* enter hunt mode */
#define WR3_AUTOENA     0x20	/* auto enables */
#define WR3_RX5BIT      0x00	/* Rx 5 bits/character */
#define WR3_RX7BIT      0x40	/* Rx 7 bits/character */
#define WR3_RX6BIT      0x80	/* Rx 6 bits/character */
#define WR3_RX8BIT      0xC0	/* Rx 8 bits/character */


/* **** Bit equates for write register 4 */

#define WR4_PARITY      0x01 /* parity enable */
#define WR4_ODDP        0x00 /* use odd parity if enabled */
#define WR4_EVENP       0x02 /* use even parity if enabled */
#define WR4_SYNC        0x00 /* sync mode enable */
#define WR4_1_STOP      0x04 /* 1 stop bit/char (asynch) */
#define WR4_15_STOP     0x08 /* 1 1/2 stop bits/char (asynch) */
#define WR4_2_STOP      0x0C /* 2 stop bits/char (asynch) */
#define WR4_MONO        0x00 /* monosync mode */
#define WR4_BISYNC      0x10 /* bisync mode */
#define WR4_SDLC        0x20 /* SDLC mode */
#define WR4_EXT         0x30 /* external sync mode */
#define WR4_1CLK        0x00 /* 1/1 clock mode */
#define WR4_16CLK       0x40 /* 1/16 clock mode */
#define WR4_32CLK       0x80 /* 1/32 clock mode */
#define WR4_64CLK       0xC0 /* 1/64 clock mode */


/* **** Bit equates for write register 5 */

#define WR5_TXCRC       0x01	/* Tx CRC enable */
#define WR5_RTS         0x02	/* request to send */
#define WR5_CRC16       0x04	/* use CRC-16 */
#define WR5_TXENA       0x08	/* Tx enable */
#define WR5_SNDBRK      0x10	/* send break */
#define WR5_TX5BIT      0x00	/* Tx 5 (or less) bits/char */
#define WR5_TX7BIT      0x20	/* Tx 7 bits/char */
#define WR5_TX6BIT      0x40	/* Tx 6 bits/char */
#define WR5_TX8BIT      0x60	/* Tx 8 bits/char */
#define WR5_DTR         0x80	/* data terminal ready */


/* **** Bit equates for write register 7 Prime */

#define WR7_EXTREAD     0x40 /* enable extended read register */

/* **** Bit equates for write register 9 */

#define WR9_VIS         0x01 /* vector includes status */
#define WR9_NOVECT      0x02 /* no vector returned */
#define WR9_LCDIS       0x04 /* disable lower interrupt chain */
#define WR9_MASTER      0x08 /* master interrupt enable */
#define WR9_HISTAT      0x10 /* status in high bits of vector */
#define WR9_BRES        0x40 /* channel reset B */
#define WR9_ARES        0x80 /* channel reset A */
#define WR9_HWRES       0xC0 /* force hardware reset */


/* **** Bit equates for write register 10 */

#define WR10_6BSYNC     0x01 /* 6 bit sync character */
#define WR10_LOOP       0x02 /* loop mode */
#define WR10_UABORT     0x04 /* abort on underrun */
#define WR10_MARK       0x08 /* mark idle line with 1's */
#define WR10_GOACT      0x10 /* go active on poll */
#define WR10_NRZ        0x00 /* NRZ encoding */
#define WR10_NRZI       0x20 /* NRZI encoding */
#define WR10_FM1        0x40 /* FM1 (transition = 1) */
#define WR10_FM0        0x60 /* FM0 (transition = 1) */
#define WR10_CRCPS1     0x80 /* crc preset to 1 */


/* **** Bit equates for write register 11 */

#define WR11_TOXTAL     0x00 /* TRxC out = xtal output */
#define WR11_TOCLK      0x01 /* TRxC out = transmit clock */
#define WR11_TOBRG      0x02 /* TRxC out = br generator output */
#define WR11_TODPLL     0x03 /* TRxC out = dpll output */
#define WR11_TRXCO      0x04 /* TRxC pin is output */
#define WR11_TCRT       0x00 /* transmit clock = RTxC pin */
#define WR11_TCTR       0x08 /* transmit clock = TRxC pin */
#define WR11_TCBRG      0x10 /* transmit clock = br generator O/P */
#define WR11_TCDPLL     0x18 /* transmit clock = dpll output */
#define WR11_RCRT       0x00 /* receive clock = RTxC pin */
#define WR11_RCTR       0x20 /* receive clock = TRxC pin */
#define WR11_RCBRG      0x40 /* receive clock = br generator O/P */
#define WR11_RCDPLL     0x60 /* receive clock = dpll output */
#define WR11_XTAL       0x80 /* RTxC input is xtal type signal */


/* **** Bit equates for write register 14 */

#define WR14_BRGENA     0x01 /* baud rate generator enable */
#define WR14_PCLK       0x02 /* PCLK pin is BR generator source */
#define WR14_DTRREQ     0x04 /* DTR/request function */
#define WR14_ECHO       0x08 /* auto echo mode */
#define WR14_LLOOP      0x10 /* local loop back */
#define WR14_SRCH       0x20 /* enter search mode */
#define WR14_MCRES      0x40 /* reset missing clock */
#define WR14_DPDIS      0x60 /* disable DPLL */
#define WR14_DSBRG      0x80 /* DPLL source = br generator */
#define WR14_DSRTXC     0xA0 /* DPLL source = RTxC */
#define WR14_FM         0xC0 /* DPLL FM mode */
#define WR14_NRZI       0xE0 /* DPLL NRZI mode */


/* **** Bit equates for write register 15 */

#define WR15_WR7PENA    0x01 /* enable WR7' */
#define WR15_ZEROIE     0x02 /* baud-count-zero int enable */
#define WR15_DCDIE      0x08 /* DCD int enable  */
#define WR15_SYNCIE     0x10 /* sync int enable (asynch mode) */
#define WR15_HUNTIE     0x10 /* hunt int enable (synch mode) */
#define WR15_CIE        0x20 /* cts int enable */
#define WR15_TXUIE      0x40 /* Tx underrun int enable */
#define WR15_BRKIE 0x80 /* break/abort int enable */

/* register handling functions */

#ifdef SPARC
#define REG_WRITE_B(devid,address,data) zi8530_write_b(address,data)
#define REG_WRITE_W(devid,address,data) zi8530_write_w(address,data)
#define REG_WRITE_L(devid,address,data) zi8530_write_l(address,data)
#define REG_READ_B(devid,address)       zi8530_read_b(address)
#define REG_READ_W(devid,address)       zi8530_read_w(address)
#define REG_READ_L(devid,address)       zi8530_read_l(address)

void zi8530_write_b(_ANSIPROT2(BYTE *, BYTE));
void zi8530_write_w(_ANSIPROT2(WORD *, WORD));
void zi8530_write_l(_ANSIPROT2(LONG *, LONG));
BYTE zi8530_read_b(_ANSIPROT1(BYTE *));
WORD zi8530_read_w(_ANSIPROT1(WORD *));
LONG zi8530_read_l(_ANSIPROT1(LONG *));
#endif

/* begin delete these */
vbsp_return_t zi8530_tty_strt_xmitter(_ANSIPROT1(
serial_dev_desc_t	*devid			/* in  */));

vbsp_return_t zi8530_pkt_strt_xmitter(_ANSIPROT1(
serial_dev_desc_t	*devid			/* in  */));

vbsp_return_t zi8530_pkt_xmitter_off(_ANSIPROT1(
serial_dev_desc_t	*devid			/* in  */));

vbsp_return_t zi8530_tty_xmitter_off(_ANSIPROT1(
serial_dev_desc_t	*devid			/* in  */));

/* end delete these */
vbsp_return_t zi8530_install(_ANSIPROT2(        /* config, dev */
                zi8530_config_t *, serial_dev_desc_t *));

vbsp_return_t zi8530_init(_ANSIPROT1(
serial_dev_desc_t	*devid  		/* in */));

void zi8530_port_init(_ANSIPROT1(
serial_dev_desc_t     	*devid             	/* in  */));

vbsp_return_t zi8530_pgetc(_ANSIPROT2(
serial_dev_desc_t 	*devid,	  		/* in  */
unsigned char 		*rxchr                 	/* out */));

vbsp_return_t zi8530_pgetc_7(_ANSIPROT2(
serial_dev_desc_t 	*devid,	  		/* in  */
unsigned char 		*rxchr                 	/* out */));

vbsp_return_t zi8530_pputc(_ANSIPROT2(
serial_dev_desc_t 	*devid,  		/* in */
unsigned char		outchr                 	/* in */));

vbsp_return_t zi8530_int_info_get(_ANSIPROT2(
serial_dev_desc_t        *devid,                /* in  */
logio_int_entry_t        *intrpt                /* in  */));

vbsp_return_t zi8530_int_info_set(_ANSIPROT2(
serial_dev_desc_t        *devid,                /* in  */
logio_int_entry_t        *intrpt                /* in  */));

int zi8530_tty_rx_tst(_ANSIPROT1(
logio_interrupt_entry_t	*entry			/* in  */));

int zi8530_tty_rx_tst_7(_ANSIPROT1(
logio_interrupt_entry_t	*entry			/* in  */));

int zi8530_pkt_rx_tst(_ANSIPROT1(
logio_interrupt_entry_t	*entry			/* in  */));

int zi8530_tty_tx_tst(_ANSIPROT1(
logio_interrupt_entry_t	*entry			/* in  */));

int zi8530_pkt_tx_tst(_ANSIPROT1(
logio_interrupt_entry_t	*entry			/* in  */));

int zi8530_err_tst(_ANSIPROT1(
logio_interrupt_entry_t	*entry			/* in  */));

vbsp_return_t zi8530_start_transmitter(_ANSIPROT1(
serial_dev_desc_t	*devid			/* in  */));

vbsp_return_t zi8530_port_disable(_ANSIPROT1(serial_dev_desc_t *devid));

vbsp_return_t zi8530_com_init(_ANSIPROT1(
serial_dev_desc_t *devid			/* in  */));

vbsp_return_t zi8530_set_params(_ANSIPROT2(
serial_dev_desc_t        *devid,                /* in  */
serial_params_t          *arg                   /* in  */));

