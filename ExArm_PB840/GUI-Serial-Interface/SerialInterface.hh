#ifndef	SerialInterface_HH
#define SerialInterface_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  SerialInterface - Serial Data Interface.
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SerialInterface.hhv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 009   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 008    By: gdc        Date: 13-Jan-2009  SCR Number: 5944
//  Project: 840S
//  Description:
//  	Modified to allow changes to service mode baud rate without
//  	requiring power cycle.
//
//  Revision: 007    By: erm    Date: 07-Oct-2008  SCR Number: 6441
//  Project: 840S
//  Description:
//   Added support for to send wavesform data out to serial port
// 
//   Revision: 006    By: gdc    Date: 07-Mar-2008  SCR Number: 6430
//  Project: TREND2
//  Description:
//  Created Start*Interface static methods called from the Task method
//  to reduce stack usage. Prior to this modification, stack space was
//  allocated to all three interfaces in each task. With this change
//  stack is allocated only when the Start*Interface is called. This 
//  allows tasks that don't instantiate the SmcpInterface to reduce
//  their stack size to only accomodate the much smalled DciInterface and
//  PrinterInterfaces.
//
//  Revision: 005    By: erm           Date: 23-Jan-2002  DR Number: 5984
//  Project: GUIComms
//  Description:
//   Add External LoopBack support
//
//  Revision: 004    By: quf           Date: 13-Sep-2001  DR Number: 5493
//  Project: GUIComms
//  Description:
//	Print implementation changes:
//	- Added IsPrintInProgress() and IsPrintInProgress_.
//	- Removed items relating to printer configuration setting.
//
//  Revision: 003    By: hct           Date: 29-JUL-1999  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//
//  Revision: 002    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//  Revision  1.X	By: jdm	Date: X/XX/96	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "SettingObserver.hh"

//@ Usage-Classes
//@ End-Usage

class SerialPort;
class MsgQueue;

class SerialInterface :  public SettingObserver
{
	public:

		//@ Type: SerialCommandCodes
		//  application originated commands to SerialInterface::Task
		enum SerialCommandCodes
		{
			SERIAL_OUTPUT_AVAILABLE
		};

		//@ Type: PortNum
		//  Enumerators for the three serial ports.
		enum PortNum
		{
			SERIAL_PORT_NULL = -1,
			SERIAL_PORT_1 = 1,
			SERIAL_PORT_2 = 2,
			SERIAL_PORT_3 = 3,
			MAX_NUM_SERIAL_PORTS
		};

		//@ Type: SerialFailureCodes
		//  reason codes for SERIAL_FAILURE_EVENT messages
		enum SerialFailureCodes
		{
			// input buffer overflow 
			SERIAL_FAILURE_OVFL = 1,
			//	parity error 
			SERIAL_FAILURE_PARITY,
			//	external source sent a NAK
			SERIAL_FAILURE_NAK,
			//	external interface timeout
			SERIAL_FAILURE_TOUT
		};


		SerialInterface( SerialPort & rSerialPort, 
						 SerialInterface::PortNum portNum );

		virtual ~SerialInterface();


		static  void  Initialize(void);
		static  SerialPort& GetAuxSerialPort(SerialInterface::PortNum portNum);
		static  SerialPort& GetSerialPort(SerialInterface::PortNum portNum);
		static  void  Task(SerialInterface::PortNum portNum);
		static  void  Task1(void);
		static  void  Task2(void);
		static  void  Task3(void);
		static  SigmaStatus SetupSerialPort(void);
		static  Uint  Write( const void * pBuffer, Uint count, 
							 Uint milliseconds = 0, 
							 SerialInterface::PortNum portNum = SerialInterface::SERIAL_PORT_1);
		static  Uint  Read( void * pBuffer, Uint count, Uint milliseconds = 0,
							SerialInterface::PortNum portNum = SerialInterface::SERIAL_PORT_1);
		static  void  Start( void );

		static  void  EnableLoopback(
									SerialInterface::PortNum portNum = SerialInterface::SERIAL_PORT_1);
		static  void  DisableLoopback(
									 SerialInterface::PortNum portNum = SerialInterface::SERIAL_PORT_1);
		static  void  EnableExternalLoopback(
											SerialInterface::PortNum portNum = SerialInterface::SERIAL_PORT_1);
		static  void  DisableExternalLoopback(
											 SerialInterface::PortNum portNum = SerialInterface::SERIAL_PORT_1);
		static  SerialInterface * GetPCurrentPrinter(void);

		static Boolean StartPrint(void);
		static void CancelPrint(void);
		static Boolean IsPrintRequested(void);
		static Boolean IsPrinterConfigured(void);
		static Boolean IsPrintInProgress(void);

		static void   SoftFault(const SoftFaultID  softFaultID,
								const Uint32       lineNumber,
								const char*        pFileName = NULL,
								const char*        pPredicate = NULL);

	protected:

		virtual void driver_(void);
		virtual void processRequests_(void) = 0;
		virtual void setCurrentPrinter_(void);
		virtual void setSerialParameters_(void);
		virtual void attachToSubjects_(void);
		virtual void detachFromSubjects_(void);

		virtual Uint write(const void * pBuffer, Uint count, Uint milliseconds = 0);
		virtual Uint read( void * pBuffer, Uint count, Uint milliseconds = 0);
		virtual void enableLoopback(void);
		virtual void disableLoopback(void);
		virtual void enableExternalLoopback(void);
		virtual void disableExternalLoopback(void);
		virtual void start(void);
		virtual void valueUpdate( const Notification::ChangeQualifier qualifierId,
								  const SettingSubject*               pSubject);
		virtual void purgeline(void);
		virtual Boolean startPrint(void);
		virtual void cancelPrint(void);
		virtual void endPrint(void);

		// VSET packet protocol methods
		Boolean isChecksumValid_(const unsigned char* msg, Uint16 length) const;

		//@ Data-Member: rSerialPort_
		//  Reference to the SerialPort that this SerialInterface controls
		SerialPort  & rSerialPort_;

		//@ Data-Member: portNum_
		//  Holds the port number associated with this instance
		SerialInterface::PortNum  portNum_;

		//@ Data-Member: isSerialPortActivated_
		//  Set TRUE by the Start method indicating the application wants 
		//  the serial interface activated
		Boolean     isSerialPortActivated_;

		//@ Data-Member: areSettingsChanged_
		//  Set TRUE when the SettingsObserver notices a settings change on
		//  this interface. Results in the interface being reconstructed.
		Boolean     areSettingsChanged_;

		//@ Data-Member: isPrintRequested_
		//  Set TRUE when a print operation is requested on this interface.
		Boolean     isPrintRequested_;

		//@ Data-Member: isPacketProtocolEnabled_
		//  TRUE when VSET packet protocol is enabled.
		Boolean     isPacketProtocolEnabled_;

		//@ Data-Member: IsPrintInProgress_
		//  TRUE when print is in progress
		static Boolean IsPrintInProgress_;

	private:

		static  void  StartSmcpInterface_( SerialInterface::PortNum portNum );
		static  void  StartPrinterInterface_( SerialInterface::PortNum portNum );
		static  void  StartDciInterface_( SerialInterface::PortNum portNum );
		static  void  StartSensorInterface_( SerialInterface::PortNum portNum );
		static  void  StartVsetInterface_( SerialInterface::PortNum portNum );

		//@ Data-Member: comConfigId_
		//  The ID for this interface's com configuration setting
		SettingId::SettingIdType  comConfigId_;

		//@ Data-Member: baudRateId_
		//  The ID for this interface's baud rate setting
		SettingId::SettingIdType  baudRateId_;

		//@ Data-Member: dataBitsId_
		//  The ID for this interface's data bits setting
		SettingId::SettingIdType  dataBitsId_;

		//@ Data-Member: parityId_
		//  The ID for this interface's parity setting
		SettingId::SettingIdType  parityId_;

		//@ Data-Member: isCurrentPrinter_
		//  TRUE when this SerialInterface is the currently assigned print
		//  destination.
		Boolean isCurrentPrinter_;

};

#endif	// ! SerialInterface_HH
