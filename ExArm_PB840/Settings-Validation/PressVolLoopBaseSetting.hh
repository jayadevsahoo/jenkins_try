
#ifndef PressVolLoopBaseSetting_HH
#define PressVolLoopBaseSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PressVolLoopBaseSetting - Waveform Loop baseline Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PressVolLoopBaseSetting.hhv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah   Date:  18-Apr-2000    DR Number: 5704
//  Project:  NeoMode
//  Description:
//      Changed to fixed upper and lower constraints, therefore no
//      need for 'updateConstraints_()' method.
//
//  Revision: 004   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "NonBatchBoundedSetting.hh"
#include "BoundedRange.hh"
//@ End-Usage


class PressVolLoopBaseSetting : public NonBatchBoundedSetting 
{
  public:
    PressVolLoopBaseSetting(void); 
    virtual ~PressVolLoopBaseSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getDefaultValue(void) const;

    virtual const BoundStatus*  acceptPrimaryChange(
				   const SettingId::SettingIdType primaryId
						   );

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    // these methods are NOT implemented...
    PressVolLoopBaseSetting(const PressVolLoopBaseSetting&);
    void  operator=(const PressVolLoopBaseSetting&);
};


#endif // PressVolLoopBaseSetting_HH 
