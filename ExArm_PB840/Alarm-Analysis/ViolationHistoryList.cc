#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ViolationHistoryList - Derived from Foundation container class. 
//---------------------------------------------------------------------
//@ Interface-Description
// ViolationHistoryList is derived from a Foundation container class;
// specifically, a singly linked list of copies of class ViolationHistory
// objects.  It is derived from the Foundation 
// SListC_ViolationHistory_NUM_VIO class and all list methods are 
// available.
//---------------------------------------------------------------------
//@ Rationale
// ViolationHistoryList provides a simple naming convention for files accessing
// a Foundation macro class.
//
// Additionally, if C++ Templates replace the macro classes in the future,
// these List classes will be the only ones affected by the change.
//---------------------------------------------------------------------
//@ Implementation-Description
// See the SListC_ViolationHistory_NUM_VIO Foundation class.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//    none
//---------------------------------------------------------------------
//@ Invariants
//    none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/ViolationHistoryList.ccv   25.0.4.0   19 Nov 2013 13:51:28   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "ViolationHistoryList.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...
//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ViolationHistoryList
//
//@ Interface-Description
//    Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

ViolationHistoryList::ViolationHistoryList(void)
{
  CALL_TRACE("ViolationHistoryList(void)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ViolationHistoryList
//
//@ Interface-Description
//    Copy contructor.
//
// >Von
//    rList  Reference to the object to be copied from.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

ViolationHistoryList::ViolationHistoryList(const ViolationHistoryList& rList) :
         FixedSListC(ViolationHistory,NUM_VIO)(rList)
{
  CALL_TRACE("ViolationHistoryList(rList)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ViolationHistoryList
//
//@ Interface-Description
//    Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

ViolationHistoryList::~ViolationHistoryList(void)
{
  CALL_TRACE("~ViolationHistoryList(void)");
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//    This method is called when a software fault is detected by the
//    fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//    'lineNumber' are essential pieces of information.  The 'pFileName'
//    and 'pPredicate' strings may be defaulted in the macro to reduce
//    code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method receives the call for the SoftFault, adds it sub-system
//    and class name ID and sends the message to the non-member function
//    SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition    
//    none
//@ End-Method
//=====================================================================

void
ViolationHistoryList::SoftFault(const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::VIOLATIONHISTORYLIST, lineNumber,
                          pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

