#ifndef SstLeakSubScreen_HH
#define SstLeakSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SstLeakSubScreen - The subscreen is activated when running SST 
// leak test.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SstLeakSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "SubScreen.hh"

//@ Usage-Classes
#include "SubScreenTitleArea.hh"
#include "LeakGauge.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class SstLeakSubScreen : public SubScreen
{
public:
	SstLeakSubScreen(SubScreenArea *pSubScreenArea);
	~SstLeakSubScreen(void);

	// Overload SubScreen methods
	virtual void activate(void);
	virtual void deactivate(void);

	void setValue(Real32 value);
	
	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	SstLeakSubScreen(void);							// not implemented..
	SstLeakSubScreen(const SstLeakSubScreen&);	// not implemented..
	void operator=(const SstLeakSubScreen&);			// not implemented..

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member: leakGauge_
	// The flag indicates to display the bar graph or not.
	LeakGauge leakGauge_;
	
};

#endif // SstLeakSubScreen_HH 
