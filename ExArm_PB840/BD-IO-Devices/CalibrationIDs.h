#ifndef CALIBRATION_IDS_H
#define CALIBRATION_IDS_H

enum CalibrationID_t
{
	CAL_ID_P1 = 0,
	CAL_ID_P2,
	CAL_ID_PDRIVE,
	CAL_ID_PAIR,
	CAL_ID_PO2,
	CAL_ID_AIR_VALVE,
	CAL_ID_O2_VALVE,
	// TODO E600 MS. Exh valve calibration can now be run in service mode.
	// Remove this ID for good when it is determined that the old e600 exhlation
	// valve calibration mechanism is no longer needed.
	//CAL_ID_EXH_VALVE,
	CAL_ID_EXH_FLOW_SENSOR,
	CAL_ID_O2_SENSOR,
	CAL_ID_CIRCUIT,
	CAL_ID_PSUPLY,
	NUM_CAL_IDS
};

#endif

