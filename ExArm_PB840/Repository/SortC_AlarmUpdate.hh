
#ifndef SortC_AlarmUpdate_HH
#define SortC_AlarmUpdate_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SortC_AlarmUpdate - Abstract Sorted List of 'AlarmUpdate' Copies.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSortC.H_v   25.0.4.0   19 Nov 2013 14:03:44   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "DListC_AlarmUpdate.hh"
//@ End-Usage


class SortC_AlarmUpdate
{
    //@ Friend:  SortIterC_AlarmUpdate
    // To allow access to 'getFirst_()', 'getCurrent_()', and 'getLast_()'.
    friend class  SortIterC_AlarmUpdate;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&                   ostr,
				       const SortC_AlarmUpdate& sortList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  public:
    virtual ~SortC_AlarmUpdate(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    virtual Boolean  isSorted(void) const = 0;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    SigmaStatus  search(const AlarmUpdate& item);

    void  addItem(const AlarmUpdate& newItem);

    SigmaStatus         removeItem(const AlarmUpdate& oldItem);
    inline SigmaStatus  removeCurrent(void);
    inline void         clearList(void);

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);
    inline SigmaStatus  goPrev (void);
    inline SigmaStatus  goLast (void);

    inline const AlarmUpdate&  currentItem(void) const;

    inline void  operator=(const SortC_AlarmUpdate& sortList);

    inline Boolean  operator==(const SortC_AlarmUpdate& sortList) const;
    inline Boolean  operator!=(const SortC_AlarmUpdate& sortList) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName = NULL,
			   const char*       pBoolTest = NULL);

  protected:
    inline SortC_AlarmUpdate(DListC_AlarmUpdate* pList);

    const DNodeC_AlarmUpdate*  findNode_(const AlarmUpdate& item) const;

    Boolean  isLegalNode_(const DNodeC_AlarmUpdate* pNode) const;

    inline const DNodeC_AlarmUpdate*  getFirst_  (void) const;
    inline const DNodeC_AlarmUpdate*  getCurrent_(void) const;
    inline const DNodeC_AlarmUpdate*  getLast_   (void) const;

    inline DNodeC_AlarmUpdate*  getFirst_  (void);
    inline DNodeC_AlarmUpdate*  getCurrent_(void);
    inline DNodeC_AlarmUpdate*  getLast_   (void);

    inline void  setCurrent_(DNodeC_AlarmUpdate* pNewCurr);

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    SortC_AlarmUpdate(const SortC_AlarmUpdate&);	// not implemented...
    SortC_AlarmUpdate(void);				// not implemented...

    //@ Data-Member:  rElemList_
    // A reference to the abstract list of the elements.
    DListC_AlarmUpdate&  rElemList_;
};


// Inlined methods...
#include "SortC_AlarmUpdate.in"


#endif  // SortC_AlarmUpdate_HH
