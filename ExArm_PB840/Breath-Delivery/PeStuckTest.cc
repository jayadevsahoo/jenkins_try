#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: PeStuckTest - BD Safety Net Test for the detection of
//                       expiratory pressure transducer reading stuck 
//                       due to a malfunctioning solenoid
//---------------------------------------------------------------------
//@ Interface-Description
//  The checkCriteria() method of this class is called from the
//  newCycle() method of the SafetyNetTestMediator class.  The
//  checkCriteria() method invokes methods in the Sensor class to obtain 
//  the value of the expiratory pressure and flow sensor readings.
//---------------------------------------------------------------------
//@ Rationale
//  Used to verify that the expiratory pressure tranducer is NOT stuck.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The safety net test is defined in the checkCriteria() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PeStuckTest.ccv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 009 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code when fault is detected.
//
//  Revision: 008  By:  iv    Date: 22-Oct-1997     DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//           Reworked per formal code review.
//
//  Revision: 007  By:  iv    Date: 14-Oct-1997     DR Number:DCS 1649
//       Project:  Sigma (R8027)
//       Description:
//           Revise detection algorithm to eliminate dependency on disconnect.
//
//  Revision: 006  By: iv    Date:  16-Sep-1997    DR Number: 2461
//  	Project:  Sigma (840)
//		Description:
//			Further increased the threshold for detecting PeStuck.
//          The new limit : 2.7 cm is equivalent to a flow of 10 lpm. 
//
//  Revision: 005  By: iv    Date:  09-Sep-1997    DR Number: 2461
//  	Project:  Sigma (840)
//		Description:
//			Revise conditions for Pe stuck.
//
//  Revision: 004  By: iv    Date:  02-Sep-1997    DR Number: 2358
//  	Project:  Sigma (840)
//		Description:
//			Revise conditions for Pe stuck.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  iv    Date:  20-May-1997    DR Number: DCS 1572
//       Project:  Sigma (840)
//       Description:
//             In checkCriteria(), check for Pe stuck only during normal exhalation.
//
//  Revision: 001  By:  by    Date:  05-Sep-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "PeStuckTest.hh"
#include "MainSensorRefs.hh"

//@ Usage-Classes
#include <math.h>
#include "BreathPhaseScheduler.hh"
#include "BreathRecord.hh"
#include "BreathMiscRefs.hh"
#include "PressureSensor.hh"
#include "PressureXducerAutozero.hh"
#include "ExhFlowSensor.h"
#include "SchedulerId.hh"
#include "BreathPhase.hh"
#include "PhaseRefs.hh"

//@ End-Usage

//@ Code...

// locals static constants for Pe stuck 
static const Real32 MINIMUM_BACK_PRESSURE_FLOW = 5.0F; //lpm
static const Real32 MIN_PRESSURE_FOR_PE_STUCK = 20.0F; // cmH2O
static const Int16 MIN_PE_STUCK_DETECTION_EXH_DELAY = 320; //msec
static const Int16 MIN_PE_STUCK_DETECTION_INSP_DELAY = 220; //msec

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PeStuckTest()  
//
//@ Interface-Description
//  Default Constructor.  Passes two arguments to the base class
//  constructor to define the error code and time criteria.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The backgndEventId_ and maxCyclesCriteriaMet_ data members are
//  set by the base class constructor.  The background event id is
//  a unique identifier that is placed in the diagnostic log if the
//  background check detects a problem.  The maximum Cycles that the
//  criteria can be met before the Background subsystem is notified
//  is MAX_PRESSURE_STUCK_TEST_CYCLES.  Data members are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

PeStuckTest::PeStuckTest( void ) :
SafetyNetTest( BK_PE_STUCK, MAX_PRESSURE_STUCK_TEST_CYCLES )
{
    // $[TI1]    
    CALL_TRACE("PeStuckTest(void)");

    peStuckFlag_ = FALSE;
    bkEventReported_ = FALSE;
    phaseTime_ = 0;  
    detectionDisqualified_ = FALSE;
    previousPeStuckStatus_ = FALSE;
    phaseType_ = BreathPhaseType::NON_BREATHING;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PeStuckTest() 
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

PeStuckTest::~PeStuckTest(void)
{
    CALL_TRACE("~PeStuckTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCriteria()
//
//@ Interface-Description
//  This method takes no arguments and returns a BkEventName.
//  This method is responisble for the detection of the expiratory
//  pressure transducer reading stuck to zero (atmospheric pressure).
//  If the background check criteria have been met for the required
//  number of cycles, the backgndEventId for the class is returned;
//  otherwise BK_NO_EVENT is returned to indicate to the caller that
//  the Background subsystem should not be notified. The declaration of
//  a background fault is held up during the first
//  MIN_PE_STUCK_DETECTION_EXH_DELAY msec of exhalation and
//  during the first MIN_PI_STUCK_DETECTION_INSP_DELAY msec
//  of inspiration.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method first checks the status of the backgndCheckFailed_ data member.
//  If it is TRUE, the test is no longer run since Background events are not
//  auto-resettable and the Background subsystem only needs to be informed of
//  an event once.  In this case, BK_NO_EVENT is returned to the caller.
//  To avoid false detection of a fault, the test is not run during an autozero maneuver.
//  The test runs only during inspiration or exhalation and never
//  runs during a non breathing mode.  The BreathRecord::GetPhaseType is invoked
//  to determine whether the ventilator is in inspiration or expiration.
//  The method also calls the getValue() methods for the expiratory pressure and flow
//  sensors.
//  The method checks for the start of either exhalation or inspiration to reset local
//  counters: numCyclesCriteriaMet_, phaseTime_, and local flags: PhaseType,
//  detectionDisqualified_.
//
//  Check during inspiration:
//  -------------------------
//  If the Pe stuck criteria is met during the first
//  MIN_PE_STUCK_DETECTION_INSP_DELAY without being disqualified or at any time
//  after the first MIN_PE_STUCK_DETECTION_INSP_DELAY msec of inspiration,
//  without being disqualified, then a fault is declared.
//
//  Check during exhalation (excluding expiratory pause):
//  -------------------------
//  Pe stuck is never declared during the first exhalation during which it was
//  detected.  Once Pe stuck criteria is detected without being disqualified, a
//  flag: peStuckFlag_ is set - this breath is the 'qualified breath'.  On the
//  subsequent exhalation - the 'verified breath', if the Pe stuck criteria is
//  detected again without being disqualified and phaseTime_ >=
//  MIN_PE_STUCK_DETECTION_EXH_DELAY then a Pe stuck fault is declared.  To set
//  the peStuckFlag_ on the 'qualified breath', the Pe stuck criteria should be
//  met without being disqualified.
//
//  The Pe stuck criteria is the same for inspiration and exhalation and is as follows:
//  {     fabs( exhPressure ) < MAX_PRES_SENSOR_STUCK_LIMIT
//       AND
//       exhFlow > MINIMUM_BACK_PRESSURE_FLOW
//       AND
//       BreathRecord::GetAirwayPressure() > MIN_PRESSURE_FOR_PE_STUCK
//  } for maxCyclesCriteriaMet_ cycles.
//     
//  The disqualifying criteria is as follows:
//  {     fabs( exhPressure ) > MAX_PRES_SENSOR_STUCK_LIMIT
//       OR
//       Pe stuck condition is false
//       OR
//       Non breathing mode become active
//  } for one occurence.
//  $[06060]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
PeStuckTest::checkCriteria( void )
{
    CALL_TRACE("PeStuckTest::checkCriteria (void)") ;

    BkEventName rtnValue = BK_NO_EVENT;

    ////////////////////////////////////////////////////////////
    // Only run the check if it hasn't already failed and
    // not currently in autozero manuever and only during
    // inspiration or expiration phase
    ////////////////////////////////////////////////////////////
    if ( ! backgndCheckFailed_ && ! RPressureXducerAutozero.getIsAutozeroInProgress() )
    {
    // $[TI1]
        Real32 exhPressure = RExhPressureSensor.getValue();
        Real32 exhFlow = RExhFlowSensor.getValue();
        SchedulerId::SchedulerIdValue schedulerId = (BreathPhaseScheduler::GetCurrentScheduler()).getId();

        if ( BreathPhaseType::INSPIRATION == BreathRecord::GetPhaseType() &&
             schedulerId != SchedulerId::OCCLUSION )
        {
        // $[TI1.1]
            if ( phaseType_ != BreathPhaseType::INSPIRATION )
            {
            // $[TI1.1.1]
                phaseType_ = BreathPhaseType::INSPIRATION;
                numCyclesCriteriaMet_ = 0;
                detectionDisqualified_ = FALSE;
                phaseTime_ = 0;
            }
            // $[TI1.1.2]
            
            // check expiratory pressure transducer stuck criteria
            if ( ( fabs( exhPressure ) < MAX_PRES_SENSOR_STUCK_LIMIT ) &&
                 ( exhFlow > MINIMUM_BACK_PRESSURE_FLOW ) &&
                 ( BreathRecord::GetAirwayPressure() > MIN_PRESSURE_FOR_PE_STUCK ) &&
                   ! detectionDisqualified_ )
            {
            // $[TI1.1.3]
                if ( ++numCyclesCriteriaMet_ >= maxCyclesCriteriaMet_ &&
                    (phaseTime_ >= MIN_PE_STUCK_DETECTION_INSP_DELAY) )
                {
                // $[TI1.1.3.1]
                    errorCode_ = (Uint16)(exhPressure*100.0F);
                    backgndCheckFailed_ = TRUE;
                }
                // $[TI1.1.3.2]
            }
            else
            {
            // $[TI1.1.4]
                numCyclesCriteriaMet_ = 0;
            }

            if ( fabs( exhPressure ) > MAX_PRES_SENSOR_STUCK_LIMIT )
            {
            // $[TI1.1.5]
                resetCriteria_();
            }
            // $[TI1.1.6]
            phaseTime_ += SECONDARY_CYCLE_TIME_MS;
        }
        else if ( (BreathPhase *)&RExhalationPhase == BreathPhase::GetCurrentBreathPhase() &&
                  schedulerId != SchedulerId::OCCLUSION )
        {
        // $[TI1.2]
            if ( phaseType_ != BreathPhaseType::EXHALATION )
            {
            // $[TI1.2.1]
                phaseType_ = BreathPhaseType::EXHALATION;
                numCyclesCriteriaMet_ = 0;
                detectionDisqualified_ = FALSE;
                phaseTime_ = 0;
                if ( peStuckFlag_ )
                {
                // $[TI1.2.1.1]
                    previousPeStuckStatus_ = TRUE;
                    peStuckFlag_ = FALSE;
                }
                // $[TI1.2.1.2]
            }
            // $[TI1.2.2]

            // check expiratory pressure transducer stuck criteria
            if ( ( fabs( exhPressure ) < MAX_PRES_SENSOR_STUCK_LIMIT ) &&
                 ( exhFlow > MINIMUM_BACK_PRESSURE_FLOW ) &&
                 ( BreathRecord::GetAirwayPressure() > MIN_PRESSURE_FOR_PE_STUCK ) &&
                   ! detectionDisqualified_ )
            {
            // $[TI1.2.3]
                if ( ++numCyclesCriteriaMet_ >= maxCyclesCriteriaMet_ )
                {
                // $[TI1.2.3.1]
                    if ( previousPeStuckStatus_ && (phaseTime_ >= MIN_PE_STUCK_DETECTION_EXH_DELAY) )
                    {
                    // $[TI1.2.3.1.1]
                        errorCode_ = (Uint16)(exhPressure*100.0F);
                        backgndCheckFailed_ = TRUE;
                    }
                    else
                    {
                    // $[TI1.2.3.1.2]
                        peStuckFlag_ = TRUE;
                    }
                }    
                // $[TI1.2.3.2]
            }
            else
            {
            // $[TI1.2.4]
                numCyclesCriteriaMet_ = 0;
            }

            if ( fabs( exhPressure ) > MAX_PRES_SENSOR_STUCK_LIMIT )
            {
            // $[TI1.2.5]
                resetCriteria_();
            }
            // $[TI1.2.6]
            phaseTime_ += SECONDARY_CYCLE_TIME_MS;
        }
        else if ( BreathPhaseType::EXPIRATORY_PAUSE != BreathRecord::GetPhaseType() )
        {
        // $[TI1.3]
            phaseType_ = BreathPhaseType::NON_BREATHING;
            resetCriteria_();
        }
        else
        {
        // $[TI1.4]
            phaseType_ = BreathPhaseType::EXPIRATORY_PAUSE;
        }
    }
    // $[TI2]

    // Report Pe stuck conditon to Safety-Net if not already reported
    if ( ( TRUE == backgndCheckFailed_ ) && ( FALSE == bkEventReported_ ) )
    {
    // $[TI3]
        bkEventReported_ = TRUE;
        rtnValue = backgndEventId_;
    }
    // $[TI4]

    return( rtnValue );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetCriteria_()
//
//@ Interface-Description
//  The method has no arguments and no return value.  It resets the Pe
//  stuck condition by reseting the appropriate flags and counters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Private data memebrs are assigned initial reset values. 
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
PeStuckTest::resetCriteria_( void )
{
	CALL_TRACE("PeStuckTest::resetCriteria_( void )");
    // $[TI1]

    detectionDisqualified_ = TRUE;
    numCyclesCriteriaMet_ = 0;
    peStuckFlag_ = FALSE;
    previousPeStuckStatus_ = FALSE;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
PeStuckTest::SoftFault( const SoftFaultID  softFaultID,
                        const Uint32       lineNumber,
                        const char*        pFileName,
                        const char*        pPredicate )
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)");

    FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PESTUCKTEST,
                             lineNumber, pFileName, pPredicate );
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================



