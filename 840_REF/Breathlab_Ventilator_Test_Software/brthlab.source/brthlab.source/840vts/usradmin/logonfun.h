/*****************************************************************************/
/*   Module Name:   logonfun.h                                            
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _LOGONFUN
#define _LOGONFUN

//#define CANCELED
#define NOUSER
//#define NOVENT
//#define NOPTS

#define MAX_LINE 50
#define USERFILE "./userlist.txt"
#define TEMPFILE "./tempfile.txt"

typedef struct login_info{
	char* 	name;
	char* 	encrypted_pw;
	int		failure;
}struct login_info;

void login(int ventdetected, int ptsdetected);

#endif // #ifndef _LOGONFUN
