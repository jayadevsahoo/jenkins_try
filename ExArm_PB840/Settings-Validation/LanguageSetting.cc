#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Covidien corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Covidien Corporation of California.
//
//            Copyright (c) 2012, Covidien Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  LanguageSetting - Language Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the language
//  to be used for the GUI. 
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /data/840/Baseline/Settings-Validation/vcssrc/LanguageSetting.ccv   27.0   27 Aug 2013 10:42:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date:  06-Aug-2012    SCR Number: 6778   
//  Project: ZHO1
//  Description:
//	Initial version.
//
//=====================================================================

#include "LanguageSetting.hh"
#include "LanguageValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  LanguageSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LanguageSetting::LanguageSetting(void)
    : BatchDiscreteSetting(SettingId::LANGUAGE,
                           Setting::NULL_DEPENDENT_ARRAY_,
                           LanguageValue::TOTAL_LANGUAGE_VALUES)
{
    // construct
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~LanguageSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LanguageSetting::~LanguageSetting(void)    
{
    // destruct
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Pressure units is ALWAYS changeable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
    LanguageSetting::getApplicability(const Notification::ChangeQualifier) const
{
    // this setting is only changeable in Chinese until multi-language 
    // feature is implemented in other languages
    #if defined(SIGMA_CHINESE) || defined(SIGMA_EUROPE) || defined(SIGMA_ASIA)
    return Applicability::CHANGEABLE;
    #else
    return Applicability::VIEWABLE;
    #endif
}   // $[TI1]

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  isEnabledValue()  [const, virtual]
//
//@ Interface-Description
//  Is this setting's value given by 'value', a currently-enabled value?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean LanguageSetting::isEnabledValue(const DiscreteValue value) const
{
    Boolean  isEnabled = FALSE;

#ifdef SIGMA_ASIA
	isEnabled = ( (value == LanguageValue::ENGLISH) 
			|| (value == LanguageValue::CHINESE) 
			|| (value == LanguageValue::JAPANESE) );
#elif SIGMA_EUROPE
	isEnabled = ( (value == LanguageValue::ENGLISH) 
			|| (value == LanguageValue::FRENCH) 
			|| (value == LanguageValue::GERMAN)
			|| (value == LanguageValue::ITALIAN)
			|| (value == LanguageValue::POLISH)
			|| (value == LanguageValue::PORTUGUESE)
			|| (value == LanguageValue::SPANISH) );
#elif SIGMA_CHINESE
	isEnabled = ( (value == LanguageValue::CHINESE)
			|| (value == LanguageValue::ENGLISH) );
#elif SIGMA_ENGLISH
	isEnabled = (value == LanguageValue::ENGLISH);
#elif SIGMA_FRENCH
    isEnabled = (value == LanguageValue::FRENCH);
#elif SIGMA_GERMAN
    isEnabled = (value == LanguageValue::GERMAN);
#elif SIGMA_ITALIAN
    isEnabled = (value == LanguageValue::ITALIAN);
#elif SIGMA_JAPANESE
    isEnabled = (value == LanguageValue::JAPANESE);
#elif SIGMA_POLISH
    isEnabled = (value == LanguageValue::POLISH);
#elif SIGMA_PORTUGUESE
    isEnabled = (value == LanguageValue::PORTUGUESE);
#elif SIGMA_RUSSIAN
    isEnabled = (value == LanguageValue::RUSSIAN);
#elif SIGMA_SPANISH
    isEnabled = (value == LanguageValue::SPANISH);
#endif
    return isEnabled;
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[ZH02002] - language new patient value
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
    LanguageSetting::getNewPatientValue(void) const
{
    const AcceptedContext&  rAcceptedContext = ContextMgr::GetAcceptedContext();

    DiscreteValue  newPatientValue;

    if (rAcceptedContext.areBatchValuesInitialized())
    {
        // when the Accepted Context has previosly-initialized values, use the
        // previous value of this setting...
        newPatientValue = rAcceptedContext.getBatchDiscreteValue(SettingId::LANGUAGE);
    }
    else
    {
#ifdef SIGMA_CHINESE
    	newPatientValue = LanguageValue::ENGLISH;
#elif SIGMA_ENGLISH
        newPatientValue = LanguageValue::ENGLISH;
#elif SIGMA_FRENCH
    	newPatientValue = LanguageValue::FRENCH;
#elif SIGMA_GERMAN
    	newPatientValue = LanguageValue::GERMAN;
#elif SIGMA_ITALIAN
    	newPatientValue = LanguageValue::ITALIAN;
#elif SIGMA_JAPANESE
    	newPatientValue = LanguageValue::JAPANESE;
#elif SIGMA_POLISH
    	newPatientValue = LanguageValue::POLISH;
#elif SIGMA_PORTUGUESE
    	newPatientValue = LanguageValue::PORTUGUESE;
#elif SIGMA_RUSSIAN
    	newPatientValue = LanguageValue::RUSSIAN;
#elif SIGMA_SPANISH
    	newPatientValue = LanguageValue::SPANISH;
#endif
    }

    return(newPatientValue);
}


#if defined(SIGMA_DEVELOPMENT)

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
    LanguageSetting::SoftFault(const SoftFaultID  softFaultID,
                               const Uint32       lineNumber,
                               const char*        pFileName,
                               const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

    FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                            LANGUAGE_SETTING, lineNumber, pFileName,
                            pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
