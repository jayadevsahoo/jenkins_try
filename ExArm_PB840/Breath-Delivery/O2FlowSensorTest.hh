
#ifndef O2FlowSensorTest_HH
#define O2FlowSensorTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: O2FlowSensorTest - BD Safety Net Test for the detection of
//                            O2 flow sensor reading OOR
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/O2FlowSensorTest.hhv   10.7   08/17/07 09:39:14   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  by    Date:  19-Sep-1996    DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "SafetyNetTest.hh"

//@ Usage-Classes
#include "Background.hh"
//@ End-Usage

class O2FlowSensorTest : public SafetyNetTest
{
    public:

        O2FlowSensorTest( void );
        ~O2FlowSensorTest( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL,
			       const char*       pPredicate = NULL );

        virtual BkEventName checkCriteria( void );
  
    protected:

    private:

        // these methods are purposely declared, but not implemented...
        O2FlowSensorTest(const O2FlowSensorTest&);  // not implemented...
        void  operator=(const O2FlowSensorTest&);       // not implemented...

        //@ Data-Member: numCyclesOorHigh_
        // The number of consecutive cycles that the check has indicated that
        // the O2 flow sensor signal is out of range on the high
        // end; this test must differentiate between OOR-High and OOR-Low and
        // therefore does not use the numCyclesCriteriaMet_ data attribute of
        // the base class
        Int16 numCyclesOorHigh_;
 
        //@ Data-Member: numCyclesOorLow_
        // The number of consecutive cycles that the check has indicated that
        // the O2 flow sensor signal is out of range on the low
        // end; this test must differentiate between OOR-High and OOR-Low and
        // therefore does not use the numCyclesCriteriaMet_ data attribute of
        // the base class
        Int16 numCyclesOorLow_;
};


#endif // O2FlowSensorTest_HH 
