
#ifndef ProxEnabledValue_HH
#define ProxEnabledValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Covidien
//====================================================================

//====================================================================
// Class:  ProxEnabledValue - Values of the Prox Enabled Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ProxEnabledValue.hhv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: rhj    Date:  25-Jan-2010    SCR Number: 6436
//  Project: PROX
//  Description:
//		Initial version.
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct ProxEnabledValue
{
	//@ Type:  ProxEnabledValueId 
	// All of the possible values of the Prox Enabled Setting.
	enum ProxEnabledValueId
	{
		// Values of the range of Prox Enabled Setting...
		PROX_DISABLED,
		PROX_ENABLED,

		TOTAL_PROX_ENABLED_VALUES
	};
};


#endif // ProxEnabledValue_HH 
