#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: HiLevelPsvPhase - Implements PSV phase during the high
//		peep level in BiLevel.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from PsvPhase class.  No public
//		methods are defined by this class since base class methods are used.
//		Protected virtual methods are implemented to support the base class.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the PSV algorithms during high peep level in
//		BiLevel.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The data members values from the base class are defined	in the pure
//		virtual methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/HiLevelPsvPhase.ccv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//      Added support for new PEEP low setting.
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.
//
//=====================================================================

#include "HiLevelPsvPhase.hh"

#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "PhasedInContextHandle.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: HiLevelPsvPhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

HiLevelPsvPhase::HiLevelPsvPhase( void)
 : PsvPhase() 			// $[TI1]
{
	CALL_TRACE("HiLevelPsvPhase::HiLevelPsvPhase( void)") ;
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~HiLevelPsvPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

HiLevelPsvPhase::~HiLevelPsvPhase(void)
{
	CALL_TRACE("HiLevelPsvPhase::~HiLevelPsvPhase(void)") ;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
HiLevelPsvPhase::SoftFault( const SoftFaultID  softFaultID,
                   	 const Uint32       lineNumber,
					 const char*        pFileName,
		   			 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, HILEVELPSVPHASE,
                             lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineEffectivePressureAndBiasOffset_
//
//@ Interface-Description
							//		This method has no arguments and has no return value.  This method is
//		called by the base class, PressureBasePhase, to determine the
//		effective pressure and the bias offset. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		Obtain the effectivePressure_ based on pressure support settings.
//		The new effectivePressure_ is the maximum between biasOffset_ and
//		peepLo + effectivePressure_ - peepHi.
// 		$[BL04031]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
HiLevelPsvPhase::determineEffectivePressureAndBiasOffset_( void)
{
	CALL_TRACE("HiLevelPsvPhase::determineEffectivePressureAndBiasOffset_( void)") ;
	
	// $[TI1]

	PsvPhase::determineEffectivePressureAndBiasOffset_() ;

	Real32 peepHi = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_HIGH).value ;
	Real32 peepLo = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_LOW).value ;

	effectivePressure_ = MAX_VALUE( biasOffset_,
							peepLo + effectivePressure_ - peepHi) ;
}
		

//=====================================================================
//
//  Private Methods...
//
//=====================================================================











