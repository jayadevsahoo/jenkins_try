#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbEventReceiver
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the central interface through which other 
//  classes and subsystems may post events to the trending event log.
//  This class also receives status events from the Bd subsystem and 
//  NovRam subsystem.
// 
//---------------------------------------------------------------------
//@ Rationale
//  To provide a centralized location where all trend-related events
//  are processed and maintained.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
// 
//  This class is a BdEventTarget and a NovRamEventTarget. As such, it 
//  overloads the BdEventHappened() and the novRamUpdateEventHappened() 
//  functions. 
//  This class registers for the Apnea, the Circuit Disconnect, and the 
//  Circuit Occlusion Bd events and the Time Change NovRam events. 
//  
//  These events are monitored in order to trend them accurately in the 
//  trending event log and also may affect decisions made by the 
//  TrendDataMgr.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//@(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbEventReceiver.ccv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: mnr    Date: 26-Feb-2010     SCR Number: 6436
//  Project:  PROX4
//  Description:
//      Trending events for PROX enable/disable added.
//
//  Revision: 001  By:  ksg    Date:  17-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//
//=====================================================================

#include <string.h>

#include "EventData.hh"             // BD-IO-Devices
#include "BdEventRegistrar.hh"      // GUI-Applications
#include "NovRamEventRegistrar.hh"  // GUI-Applications
#include "BitUtilities.hh"          // Utilities
#include "CriticalSection.hh"		// Sys-Init
#include "IpcIds.hh"				// Sys-Init

#include "TrendDataMgr.hh"
#include "TrendDbEventReceiver.hh"

#include "ProxEnabledValue.hh"
#include "AcceptedContextHandle.hh"

//@ Code...



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbEventReceiver [constructor]
//
//@ Interface-Description
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbEventReceiver::TrendDbEventReceiver( void )
	:	BdEventTarget(),
		NovRamEventTarget(),
		isPatientConnected_( FALSE ),
		isPatientCircuitOccluded_( FALSE ),
		isPatientApnea_( FALSE ),
		isPatientSetupComplete_( FALSE )
{
	TREND_MGR_DBG4( 2, "Trend Event Receiver constructor ", hex, this, dec );

	// Clear all of the event data to zero
	memset( eventSetData_, 0x00, sizeof( eventSetData_ ) ); 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbEventReceiver [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbEventReceiver::~TrendDbEventReceiver( void )
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: postEvent
//
//@ Interface-Description
//  Add the provided event to the trend log at the next 10-sec timestamp
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the postEventValue passing no value
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::postEvent( TrendEvents::EventId event )
{
	static TrendValue noValue;
	postEventValue( event, noValue );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: postEventValue
//
//@ Interface-Description
//  Add the provided event and its value to the trend log at the next 10-sec timestamp
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function sets the bit and value corresponding to the provided event in the
//  bit vector and value array. Since the user and auto bit vectors are contiguous in
//  memory, the below function also sets the auto events correctly.
// 
//  Post the event bit and value into the non-frozen event set, verifies that the set
//  didn't change asynchronously via the TREND_EVENT_MT Mutex
//
//  This function sets the value corresponding to the provided event in the
//  value array. 
// 
//---------------------------------------------------------------------
//@ PreCondition
//	event must be valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::postEventValue( TrendEvents::EventId event, const TrendValue & value )
{
	AUX_CLASS_PRE_CONDITION( (event < TrendEvents::TOTAL_EVENT_IDS), event );
	CriticalSection mutex( TREND_EVENT_MT );
	EventSets nonFrozen = (frozenEventSet_ == EVENT_SET_A ? EVENT_SET_B : EVENT_SET_A);
	SetBit( &(eventSetData_[nonFrozen].eventBits_[USER_EVENT]), event );
	eventSetData_[nonFrozen].eventValue_[event] = value;

	TREND_MGR_DBG6( 8, "Set Event value ", value.data.realValue,
					   ", event ", event, " into set ", frozenEventSet_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearEvents
//
//@ Interface-Description
//  clears auto and user bit vectors. Called after data is sampled for 
//  the 10 second interval.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clears the bits from the frozen set
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::clearEvents(void)
{
	TREND_MGR_DBG2( 8, "Clear event set ", frozenEventSet_ );
	eventSetData_[frozenEventSet_].eventBits_[AUTO_EVENT] = 0;
	eventSetData_[frozenEventSet_].eventBits_[USER_EVENT] = 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: freezeEvents
//
//@ Interface-Description
//  Frezes the current event set
//---------------------------------------------------------------------
//@ Implementation-Description
//  The current set will be frozen and all future updates will be on the 
//  non-frozen set.  Use a Mutex to ensure that events are not being posted
//  at this time.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::freezeEvents(void)
{
	CriticalSection mutex( TREND_EVENT_MT );
	frozenEventSet_ = (frozenEventSet_ == EVENT_SET_A ? EVENT_SET_B : EVENT_SET_A);
	TREND_MGR_DBG2( 8, "Freeze Event set ", frozenEventSet_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerTargets
//
//@ Interface-Description
//  Register to the BdEventRegistrar and NovRamEventRegistrar
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Can be called more than once
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::registerTargets( void )
{
	// Register for status events
	BdEventRegistrar::RegisterTarget(EventData::OCCLUSION, this);
	BdEventRegistrar::RegisterTarget(EventData::APNEA_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::PATIENT_CONNECT, this );

	// Register for PROX FAULT and SAFETY VENT events
	BdEventRegistrar::RegisterTarget(EventData::SAFETY_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::PROX_FAULT, this);

	// Register for Novram event
	NovRamEventRegistrar::RegisterTarget( NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE,
										  this );

	TREND_MGR_DBG1( 2, "Trend Event Receiver register targets" );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
//  Called from the BdEventRegistrar on vent status change upon entry
//  or exit from Apnea, Circuit Disconnect, Occlusion or when PROX faults.
//  This function posts an auto-event in the trending event log and 
//  updates the local vars that store the status of these events.
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  A call to TrendDataMgr::SystemContextChange() ensures that the 
//  display recognizes the change in the bd event status since these events 
//  impact the trending parameter applicability.
// 
//  Satisfies requirement: 
//  [[TR011179] The system shall automatically generate the following events...
//  [PX04003] Post trending events for PROX enable/disable
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::bdEventHappened( EventData::EventId eventId,
											EventData::EventStatus eventStatus,
											EventData::EventPrompt eventPrompt )
{
	// Output the Status Event id and value
	TREND_MGR_DBG4( 8, "Received Bd event ", eventId,
					", status ", eventStatus );

	if (eventId == EventData::PATIENT_CONNECT)
	{
		if (eventStatus == EventData::ACTIVE)
		{
			TREND_MGR_DBG1( 8, "Patient is connected" );
			setPatientConnected( TRUE );
			TrendDataMgr::SystemContextChange();	// Force a DB refresh
		}
		else
		{
			TREND_MGR_DBG1( 8, "Patient is not connected" );
			postEvent(TrendEvents::AUTO_EVENT_PATIENT_DISCONNECT);
			setPatientConnected( FALSE );
			TrendDataMgr::SystemContextChange();	// Force a DB refresh
		}
	}
	if (eventId == EventData::OCCLUSION)
	{
		if (eventStatus == EventData::ACTIVE)
		{
			TREND_MGR_DBG1( 8, "Patient circuit is occluded" );
			setPatientOcclusion( TRUE );
			postEvent(TrendEvents::AUTO_EVENT_PATIENT_OCCLUSION);
			TrendDataMgr::SystemContextChange();	// Force a DB refresh
		}
		else
		{
			TREND_MGR_DBG1( 8, "Patient circuit is not occluded" );
			setPatientOcclusion( FALSE );
			TrendDataMgr::SystemContextChange();	// Force a DB refresh
		}
	}
	if (eventId == EventData::APNEA_VENT)
	{
		if (eventStatus == EventData::ACTIVE)
		{
			TREND_MGR_DBG1( 8, "Patient is apneac" );
			setPatientApnea( TRUE );
			postEvent(TrendEvents::AUTO_EVENT_APNEA);
			TrendDataMgr::SystemContextChange();	// Force a DB refresh
		}
		else
		{
			TREND_MGR_DBG1( 2, "Patient is not apneac" );
			setPatientApnea( FALSE );
			TrendDataMgr::SystemContextChange();	// Force a DB refresh
		}   
	}
	if ( eventId == EventData::PROX_FAULT ||
		 eventId == EventData::SAFETY_VENT )
	{
        const DiscreteValue PROX_ENABLED_VALUE =
			AcceptedContextHandle::GetDiscreteValue(SettingId::PROX_ENABLED);
		
		Boolean isProxEnabled = (PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED);

		if (eventStatus == EventData::ACTIVE) 
		{
			if( isProxEnabled ) {
				// [PX04003] Post related trending event 
				postEvent(TrendEvents::AUTO_EVENT_PROX_DISABLED);
			}
		}
		else
		{
			if( isProxEnabled ) {
				// [PX04003] Post related trending event 
				postEvent(TrendEvents::AUTO_EVENT_PROX_ENABLED);
			}
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: novRamUpdateEventHappened
//
//@ Interface-Description
//  Called from the NovRamEventRegistrar on REAL_TIME_CLOCK_UPDATE when
//  the user adjusts the real time clock.
//  Generates an auto event in the trend event log.
//  
//---------------------------------------------------------------------
//@ Implementation-Description
// 
//  Satisfies requirements:
//  [[TR011179] The system shall automatically generate the following
//  events...
//  [[TR01142] [PRD 1485, 1531, 1532] If the operator modifies the real
//  time clock setting...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbEventReceiver::novRamUpdateEventHappened( NovRamUpdateManager::UpdateTypeId updateId )
{
	TREND_MGR_DBG2( 8, "Received NovRam event ", updateId );

	if (updateId == NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE)
	{
		// Post the time change event
		TrendDataMgr::PostEvent( TrendEvents::AUTO_EVENT_TIME_CHANGE );
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbEventReceiver::SoftFault(const SoftFaultID  softFaultID,
									 const Uint32       lineNumber,
									 const char*        pFileName,
									 const char*        pPredicate)  
{

	CALL_TRACE("TrendDbEventReceiver::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_EVENT_RECEIVER,
							lineNumber, pFileName, pPredicate);
}

