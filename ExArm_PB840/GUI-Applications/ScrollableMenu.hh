#ifndef ScrollableMenu_HH
#define ScrollableMenu_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ScrollableMenu -  Scrollable menu class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScrollableMenu.hhv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc	   Date:  15-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Initial version.
//
//====================================================================

#include "GuiAppClassIds.hh"
#include "GuiTable.H"
#include "SettingId.hh"
#include "SettingScrollbar.hh"

//@ Usage-Classes
//@ End-Usage

static const int MAX_MENU_ROWS = 10;
static const int MAX_MENU_COLS = 2;

class LogTarget;
class Setting;

class ScrollableMenu 
:   public GuiTable<MAX_MENU_ROWS,MAX_MENU_COLS>
{
public:
	ScrollableMenu( SettingId::SettingIdType settingId,
					LogTarget& settingMenuItems,
					Uint16 numDisplayRows,
					Uint16 numDisplayColumns, 
					Uint16 rowHeight,
					Boolean isDisplayScrollbar);

	~ScrollableMenu(void);

	static ScrollableMenu& GetTrendSelectMenu(void);
	static ScrollableMenu& GetTrendTimeScaleMenu(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:
	// these methods are purposely declared, but not implemented...
	ScrollableMenu(const ScrollableMenu&);		 // not implemented...
	void   operator=(const ScrollableMenu&);	 // not implemented...

	virtual void refreshCell_(Uint16 rowNumber, Uint16 columnNumber);

	//@ Data-Member: scrollbar_
	// The small scrollbar on the right of the menu
	SettingScrollbar scrollbar_;

	//@Data-Member: pSetting_
	// Pointer to the setting that this menu refers to determine if the 
	// menu item is enabled or disabled for the current option set
	const Setting* pSetting_;

	//@Data-Member: rLogTarget
	// A LogTarget that provides the data for each cell in the menu
	LogTarget& rLogTarget_;
};

#endif // ScrollableMenu_HH
