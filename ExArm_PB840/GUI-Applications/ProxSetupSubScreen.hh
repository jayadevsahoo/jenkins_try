#ifndef ProxSetupSubScreen_HH
#define ProxSetupSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ProxSetupSubScreen - Subscreen for activating or 
//                              deactivating a Prox Setup Subscreen
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ProxSetupSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: rhj   Date: 10-Aug-2010     SCR Number: 6638
//  Project:  PROX
//  Description:
//      Added notAvailableMsg_.
// 
//  Revision: 003   By: rhj   Date: 09-Aug-2010     SCR Number: 6601
//  Project:  PROX
//  Description:
//      Added Prox errorMsg_ and errorMsg2_.
// 
//  Revision: 002   By: mnr   Date: 24-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added isProxInstalled() and isProxInstalled_.
//
//  Revision: 001   By: rhj   Date:  26-Jan-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//       PROX Project Initial Version
//
//====================================================================

#include "SubScreen.hh"

//@ Usage-Classes
#include "SubScreenTitleArea.hh"
#include "GuiAppClassIds.hh"
#include "TextButton.hh"
#include "BdEventTarget.hh"
#include "EventData.hh"
#include "Line.hh"
#include "SubScreenTitleArea.hh"
#include "TextField.hh"
#include "TouchableText.hh"
#include "PatientDataTarget.hh"
#include "GuiTimerTarget.hh"
#include "GuiTimerRegistrar.hh"
#include "BatchSettingsSubScreen.hh"
#include "DiscreteSettingButton.hh"
#include "BdEventRegistrar.hh"
#include "SettingContextHandle.hh"
#include "OsTimeStamp.hh"
#include "Box.hh"
#include "Array_ResultTableEntry_MAX_TEST_ENTRIES.hh"
#include "SstTestResultData.hh"


//@ End-Usage

class ProxSetupSubScreen : 
public BdEventTarget,
	public BatchSettingsSubScreen
{
public:
	ProxSetupSubScreen(SubScreenArea *pSubScreenArea);
	~ProxSetupSubScreen(void);

	// BdEventTarget virtual method
	virtual void bdEventHappened(EventData::EventId eventId,
								 EventData::EventStatus eventStatus,
								 EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);


	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
									Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
								  Boolean byOperatorAction);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


	// Overload SubScreen methods
	virtual void acceptHappened(void);

	Boolean isProxInstalled(void);

protected:
	// BatchSettingsSubScreen virtual method...
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(
									  const BatchSettingsSubScreen::TransitionId_ tranistionId,
									  const Notification::ChangeQualifier         qualifierId,
									  const ContextSubject*                       pSubject);
	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

protected:

	private:
	// these methods are purposely declared, but not implemented...
	ProxSetupSubScreen(void);							// not implemented..
	ProxSetupSubScreen(const ProxSetupSubScreen&);	// not implemented..
	void operator=(const ProxSetupSubScreen&);			// not implemented..

	//@ Data-Member: proxEnableButton_
	// The prox enable button.
	DiscreteSettingButton proxEnableButton_;

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top of the screen
	SubScreenTitleArea titleArea_;

	//@ Data-Member: controlPanel_
	// A container for the manual purge button
	Container controlPanel_;

	//@ Data-Member: settingsPanel_
	// A container for the setting buttons
	Container settingsPanel_;

	//@ Data-Member: panelMsg_
	// A message which informs the user of the manual purge status.
	TextField panelMsg_;

	//@ Data-Member: panelTitle_
	// A title text for this control panel
	TextField panelTitle_;

	//@ Data-Member: startButton_
	// The start command button for manual purge
	TextButton startButton_;

	//@ Data-Member: eventStatus_
	// Stores the current event status
	EventData::EventStatus eventStatus_;

	//@ Data-Member: startTime_
	// OsTime stamp for the time when manual purge is requested.
	OsTimeStamp startTime_;

	//@ Data-Member: isProxInop_
	// Is proximal board inoperative?
	Boolean isProxInop_;

	//@ Data-Member: isProxEnabled_
	// Is prox setting enabled?
	Boolean isProxEnabled_;

	//@ Data-Member: isProxManualPurgeActive_
	// Is Prox manual purge active?
	Boolean isProxManualPurgeActive_;

	//@ Data-Member: timeProgressBox_
	// The colored box for which the width of the box represents the passed
	// time period of manual purge.
	Box timeProgressBox_;

	//@ Data-Member: timeBox_
	// The colored box for which the width of the box represents the duration of
	// manual purge.
	Box timeBox_;

	//@ Data-Member: isProxSstPassedOverridden_
	// did PROX pass SST or was OVERRIDDEN
	Boolean isProxSstPassedOverridden_;

	//@ Data-Member: isProxInstalled_
	// Is prox board installed?
	Boolean isProxInstalled_;

	//@ Data-Member: sstResTableEntries_
	// The result table for SST.
	FixedArray(ResultTableEntry, MAX_TEST_ENTRIES) sstResTableEntries_;

	//@ Data-Member: errorMsg_
	// An error text for this setting panel
	TextField errorMsg_;

	//@ Data-Member: errorMsg2_
	// An extended error text for this setting panel
	TextField errorMsg2_;

	//@ Data-Member: notAvailableMsg_
	// Not available message for this setting panel
	TextField notAvailableMsg_;

};

#endif // ProxSetupSubScreen_HH 
