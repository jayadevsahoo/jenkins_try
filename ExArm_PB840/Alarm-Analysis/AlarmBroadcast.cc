#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Type: AlarmBroadcast - Holds AlarmUpdate info to be sent across enet.
//---------------------------------------------------------------------
//@ Interface-Description
//  Struct AlarmBroadcast is designed to hold the information needed for
//  broadcasting alarm information.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmBroadcast.ccv   25.0.4.0   19 Nov 2013 13:51:14   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 07/26/95         DR Number:   571
//      Project:   Sigma   (R8027)
//      Description:
//         Struct AlarmBroadcast is designed to hold the information needed for
//         broadcasting alarm information.
//  
//   Revision 002   By:   gbs      Date: 05/17/96         DR Number:   995
//      Project:   Sigma   (R8027)
//      Description:
//         Fix CALL_TRACE and add passed parameter comments.
//         These changes do not affect Unit Test.
//  
//   Revision 003   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "AlarmBroadcast.hh"

//@ Usage-Classes
#include "AlarmConstants.hh"
#include "AlarmUpdate.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmBroadcast
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

AlarmBroadcast::AlarmBroadcast(void)
{
  CALL_TRACE("AlarmBroadcast(void)");
  name = ALARM_NAME_NULL;

  for (Int32 ii = 0; ii < NUM_MESSAGE; ii++)
  {   
    currentAnalysisMessage[ii] = MESSAGE_NAME_NULL;
    currentRemedyMessage[ii] = MESSAGE_NAME_NULL;
  } 
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmBroadcast
//
//@ Interface-Description
//   Copy constructor.
//
// >Von
//   alarmBroadcast   Reference to AlarmBroadcast object to be copied from.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
AlarmBroadcast::AlarmBroadcast(const AlarmBroadcast& alarmBroadcast)
{
  CALL_TRACE("AlarmBroadcast(alarmBroadcast)");
  
  whenOccurred    = alarmBroadcast.whenOccurred;
  name            = alarmBroadcast.name;
  baseMessage     = alarmBroadcast.baseMessage;
  updateType      = alarmBroadcast.updateType;
  currentUrgency  = alarmBroadcast.currentUrgency;
  currentPriority = alarmBroadcast.currentPriority;
  analysisIndex   = alarmBroadcast.analysisIndex;
  remedyIndex     = alarmBroadcast.remedyIndex;

  CLASS_ASSERTION(analysisIndex <= NUM_MESSAGE);
  CLASS_ASSERTION(remedyIndex   <= NUM_MESSAGE);
  Int32 jj = 0;
  for (jj = 0; jj < analysisIndex; jj++)
  {   
    currentAnalysisMessage[jj] = alarmBroadcast.currentAnalysisMessage[jj];
  } 

  for (jj = 0; jj < remedyIndex; jj++)
  {   
    currentRemedyMessage[jj] = alarmBroadcast.currentRemedyMessage[jj];
  }
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmBroadcast
//
//@ Interface-Description
//   Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

AlarmBroadcast::~AlarmBroadcast(void)
{
  CALL_TRACE("~AlarmBroadcast(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//   Assignment operator.
// >Von
//   rAlarmUpdate     Reference to AlarmUpdate object to be assigned from.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmBroadcast::operator=(const AlarmUpdate& rAlarmUpdate)
{
  CALL_TRACE("operator=(rAlarmUpdate)");
  
  //TODO E600 VM: true that TimeOfDay is all premitive data, but will be safer
  //to create an overloaded assignment operator for TimeOfDay
  whenOccurred    = rAlarmUpdate.getWhenOccurred().getTimeOfDay();
  name            = rAlarmUpdate.getName();
  baseMessage     = rAlarmUpdate.getBaseMessage();
  updateType      = rAlarmUpdate.getUpdateType();
  currentUrgency  = rAlarmUpdate.getCurrentUrgency();
  currentPriority = rAlarmUpdate.getCurrentPriority();
  analysisIndex   = rAlarmUpdate.getAnalysisIndex();
  remedyIndex     = rAlarmUpdate.getRemedyIndex();

  CLASS_ASSERTION(analysisIndex <= NUM_MESSAGE);
  CLASS_ASSERTION(remedyIndex   <= NUM_MESSAGE);
  MessageNameArray tempAnalysisMessage = rAlarmUpdate.getAnalysisMessage();
  MessageNameArray tempRemedyMessage   = rAlarmUpdate.getRemedyMessage();
  Int32 jj = 0;
  for (jj = 0; jj < analysisIndex; jj++)
  {   
    currentAnalysisMessage[jj] = tempAnalysisMessage[jj];
  } 

  for (jj = 0; jj < remedyIndex; jj++)
  {   
    currentRemedyMessage[jj] = tempRemedyMessage[jj];
  }
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: convHtoN
//
//@ Interface-Description
//   Converts the endianness of the data from Host to Network.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void AlarmBroadcast::convHtoN(void)
{
	SocketWrap sock;
	whenOccurred.milliseconds = sock.HTONS(whenOccurred.milliseconds);
	name = (Int32)sock.HTONL((Uint32)name);
    baseMessage = (Int16)sock.HTONS((Uint16)baseMessage);
    currentPriority = (Int16)sock.HTONS((Uint16)currentPriority);
	for (Int32 ii = 0; ii < NUM_MESSAGE; ii++)
	{
		currentAnalysisMessage[ii] = (Int16)sock.HTONS((Uint16)currentAnalysisMessage[ii]);
		currentRemedyMessage[ii] = (Int16)sock.HTONS((Uint16)currentRemedyMessage[ii]);
	} 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: convNtoH
//
//@ Interface-Description
//   Converts the endianness of the data from Network to Host.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void AlarmBroadcast::convNtoH(void)
{
	SocketWrap sock;
	whenOccurred.milliseconds = sock.NTOHS(whenOccurred.milliseconds);
	name = (Int32)sock.NTOHL((Uint32)name);
    baseMessage = (Int16)sock.NTOHS((Uint16)baseMessage);
    currentPriority = (Int16)sock.NTOHS((Uint16)currentPriority);
	for (Int32 ii = 0; ii < NUM_MESSAGE; ii++)
	{
		currentAnalysisMessage[ii] = (Int16)sock.NTOHS((Uint16)currentAnalysisMessage[ii]);
		currentRemedyMessage[ii] = (Int16)sock.NTOHS((Uint16)currentRemedyMessage[ii]);
	} 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================
void
AlarmBroadcast::SoftFault(const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::ALARMBROADCAST, lineNumber,  
                          pFileName, pPredicate);
}
