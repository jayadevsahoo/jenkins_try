#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: InspPressureSensorTest - BD Safety Net Test for the detection
//                            of inspiratory pressure sensor reading OOR
//---------------------------------------------------------------------
//@ Interface-Description
//  The checkCriteria() method of this class is called from the
//  newCycle() method of the SafetyNetTestMediator class.  The
//  checkCriteria() method invokes methods in the Sensor class to obtain 
//  the value of the inspiratory and expiratory pressure sensor readings.
//---------------------------------------------------------------------
//@ Rationale
//  Used to verify that the inspiratory pressure is reading within 
//    the specified tolerance.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The safety net test is defined in the checkCriteria() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/InspPressureSensorTest.ccv   10.7   08/17/07 09:37:54   pvcs  
//
//@ Modification-Log
//
//  Revision: 005  By: syw     Date:  06-Jan-2000    DR Number: 5590
//  Project:  NeoMode
//  Description:
//      In checkCriteria(), avoid checking condition if during an autozero.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code when fault is detected.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  20-Sep-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "InspPressureSensorTest.hh"
#include "MainSensorRefs.hh"

//@ Usage-Classes
#include "BreathRecord.hh"
#include "PressureSensor.hh"
#include "BreathMiscRefs.hh"
#include "BreathPhaseScheduler.hh"
#include "PressureXducerAutozero.hh"
//@ End-Usage

//@ Code...

#if defined( SIGMA_SAFETY_NET_DEBUG )
#include <stdio.h>
#endif // defined( SIGMA_SAFETY_NET_DEBUG )

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InspPressureSensorTest()  
//
//@ Interface-Description
//  Default Constructor.  Passes two arguments to the base class
//  constructor to define the error code and time criterion.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The backgndEventId_ and maxCyclesCriteriaMet_ data members are
//  set by the base class constructor.  The background event id is
//  a unique identifier that is placed in the diagnostic log if the
//  background check detects a problem.  The maximum Cycles that the
//  criteria can be met before the Background subsystem is notified
//  is MAX_PRESSURE_OOR_CYCLES.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

InspPressureSensorTest::InspPressureSensorTest( void ):
SafetyNetTest( BK_INSP_PRESS_OOR, MAX_PRESSURE_OOR_CYCLES )
{
    // $[TI1]    
    CALL_TRACE("InspPressureSensorTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~InspPressureSensorTest() 
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

InspPressureSensorTest::~InspPressureSensorTest(void)
{
    CALL_TRACE("~InspPressureSensorTest(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCriteria()
//
//@ Interface-Description
//  This method takes no arguments and returns the a BkEventName.
//  This method is responsible for detecting an inspiratory pressure
//  sensor reading OOR.
//  If the background check criteria have been met for the required
//  number of cycles, the backgndEventId for the class is returned;
//  otherwise BK_NO_EVENT is returned to indicate to the caller that
//  the Background subsystem should not be notified.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method first checks the status of the backgndCheckFailed_
//  data member.  If it is TRUE, the test is no longer run since
//  this Background event is not auto-resettable and the Background
//  subsystem only needs to be informed of an event once.  In this
//  case, BK_NO_EVENT is returned to the caller.
//
//  The method calls the getValue() method for the inspiratory
//  pressure sensors.
//
//  The following criteria is tested:
//
//      ( Inspiratory Pressure < MIN_PRES_TEST_LIMIT
//        and
//        Expiratory Pressure > PRESSURE_SENSOR_STUCK_CRITERIA )
//      OR 
//      Inspiratory Pressure > MAX_PRES_TEST_LIMIT 
//
//  If the condition is met, the numCyclesCriteriaMet_ data item is
//  incremented and compared to the maxCyclesCriteriaMet_ item to
//  determine if the background subsystem needs to be informed.
//
//  $[06078] $[06079]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
InspPressureSensorTest::checkCriteria( void )
{
    CALL_TRACE("InspPressureSensorTest::checkCriteria( void )") ;

    BkEventName rtnValue = BK_NO_EVENT;

    const Real32 inspPressure = RInspPressureSensor.getValue();
    const Real32 expPressure = RExhPressureSensor.getValue();

    // Only run the check if it hasn't already failed
    // Check to see not in OSC scheduler since occlusion may 
    // cause the exhalation pressure sensor to appear OOR 
	// if a suctioning manuever is taking place ignore checking for
	// background failure for the next 10 seconds
	// allows for transient spikes to settle and the pressure transducers
	// to track closer together again.
    if ( ( ! backgndCheckFailed_ ) &&
    		! RPressureXducerAutozero.getIsAutozeroInProgress() &&
         ( (BreathPhaseScheduler::GetCurrentScheduler()).getId()
            != SchedulerId::OCCLUSION ) &&
         !IsSuctioning(expPressure,  inspPressure) )
    {
    // $[TI1]
        
        if ( ( ( inspPressure < MIN_PRES_TEST_LIMIT ) &&
               ( expPressure > PRESSURE_SENSOR_STUCK_CRITERIA ) ) ||
             ( inspPressure > MAX_PRES_TEST_LIMIT ) )
        {
        // $[TI1.1]
            if ( ++numCyclesCriteriaMet_ >= maxCyclesCriteriaMet_ )
            {
            // $[TI1.1.1]
                errorCode_ = (Uint16)(inspPressure*100.0F);
                backgndCheckFailed_ = TRUE;
                rtnValue = backgndEventId_;
            }
            // $[TI1.1.2]
        }
        else
        {
        // $[TI1.2]
            numCyclesCriteriaMet_ = 0;
        }
    }
    else  // criteria not met
    {
    // $[TI2]
        numCyclesCriteriaMet_ = 0;
    }
    
    return( rtnValue );
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
InspPressureSensorTest::SoftFault( const SoftFaultID  softFaultID,
                                      const Uint32       lineNumber,
                                      const char*        pFileName,
                                      const char*        pPredicate )
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

    FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, INSPPRESSURESENSORTEST,
                             lineNumber, pFileName, pPredicate );
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


