#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//         Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Alarm_Analysis - The Alarm-Analysis subsystem class.
//---------------------------------------------------------------------
//@ Interface-Description
// Alarm_Analysis provides public methods for initializing the Alarm-Analysis
// subsystem.
//---------------------------------------------------------------------
//@ Rationale
// Each subsystem must define a class which contains an enumeration of all
// classes in the subsystem.  Along with this enumeration, other enumerations
// common to alarm handling are included in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class defines and implements the Alarm-Analysis Initialize method which
// is called during startup processing.  The Initialize method constructs all
// necessary alarm objects and initializes the Alarm-Analysis subsystem.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/Alarm_Analysis.ccv   25.0.4.0   19 Nov 2013 13:51:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  <who>    Date:  dd-mmm-yy    DR Number: 
//   Project:  Sigma (R8027)
//   Description:
//      Initial version (Integration baseline).
//
//   Revision 001   By:   hct      Date: 07/01/95         DR Number:   499
//      Project:   Sigma   (R8027)
//      Description:
//         Added class enumerators ALARMCOMM and ALARMCOMMLIST.
//  
//   Revision 002   By:   hct      Date: 07/26/95         DR Number:   571
//      Project:   Sigma   (R8027)
//      Description:
//         Added class enumerators ALARMBROADCAST and ALARMBROADCASTLIST.
//         Added call to AlarmBroadcast::Initialize().
//  
//   Revision 003   By:   hct      Date: 01/01/95         DR Number:   502
//      Project:   Sigma   (R8027)
//      Description:
//         Added class enumerators ALARMDCI and ALARMDCILIST.
//         Added call to AlarmDci::Initialize().
//  
//   Revision 004   By:   hct      Date: 12/18/96         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.
//  
//   Revision 005   By:   hct      Date: 08/19/98         DR Number:  5142
//      Project:   Sigma   (R8027)
//      Description:
//         Added call to AlarmRepository::Initialize().
//  
//   Revision 006   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "Alarm.hh"
#include "AlarmAnnunciator.hh"
#include "AlarmBroadcastArray.hh"
#include "AlarmDci.hh"
#include "AlarmManager.hh"
#include "AlarmRepository.hh"
#include "ConditionEvaluations.hh"
#include "OperatingCondition.hh"
#include "OperatingGroup.hh"

#ifdef SIGMA_DEVELOPMENT
#  include "MsgTimer.hh"
#endif
//@ End-Usage


//@ Code...

//=====================================================================
//
//  Static Member Definition...
//
//=====================================================================


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize
//
//@ Interface-Description
//   Initialize the Alarm subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
Alarm_Analysis::Initialize(void)
{
  CALL_TRACE("Initialize(void)");

  // initialization of this subsystem's classes/data...
#ifdef SIGMA_GUI_CPU
  Alarm::Initialize();
  AlarmAnnunciator::Initialize();
  AlarmOperand::Initialize();
  ConditionEvaluations::Initialize();;
  OperatingGroup::Initialize();
  ViolationHistory::Initialize();
  AlarmRepository::Initialize();
#endif // SIGMA_GUI_CPU

  AlarmManager::Initialize();

#ifdef SIGMA_GUI_CPU
  AlarmDci::Initialize();
  AlarmBroadcastArray::Initialize();
#endif // SIGMA_GUI_CPU
  // $[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
Alarm_Analysis::SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName,
						  const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::ALARM_ANALYSIS_CLASS, lineNumber,
                          pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

