#ifndef	SendSocket_HH
#define	SendSocket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: SendSocket - Class definition for a Send only 
//   TCP/IP socket connection to the other Sigma board.  This class
//   implments the main part of the Network Application protocol
//   between the GUI and BD boards.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/SendSocket.hhv   25.0.4.0   19 Nov 2013 14:14:36   pvcs  $
//@  Modification-Log
//
//  Revision: 003  By: gdc     Date: 30-Dec-2010  SCR Number: 6717
//  Project:  XENA2
//  Description:
//      Corrected creation and initialization of all socket types.
//      Changed lastXmitTime_ to private member data.
//
//  Revision: 002  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  06-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

#include "Sigma.hh"
#include "NetworkSocket.hh"
#include "NetworkMsgHeader.hh"
#include "NetworkAppClassIds.hh"
#include "OsTimeStamp.hh"

class SendSocket : public NetworkSocket
{
  public:
    SendSocket(void);	
    virtual Int sendMsg( XmitDataMsgId msgId, char *buf, Uint nbytes);
	virtual SigmaStatus verifyConnection(XmitDataMsgId thisActive,
			 	XmitDataMsgId otherActive);
	virtual void down(void);
	virtual Boolean anyLostMsg() const;
	void sendAliveMsg(XmitDataMsgId msgId);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
  protected:

  private:
    SendSocket(const SendSocket&);       	// not implemented...
    void   operator=(const SendSocket&);	// not implemented...
    ~SendSocket(void);                      // not implemented...

	//@ Data-Member: lastXmitTime_
	//  Maintains the last time that a network message is transmitted 
	//  to the other board.
	OsTimeStamp lastXmitTime_;
};

#endif // SendSocket_HH 
