#ifndef GuiTimerTarget_HH
#define GuiTimerTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: GuiTimerTarget - A target to handle GUI Timer events.
// Classes can specify this class as an additional parent class register to be
// informed of timer events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiTimerTarget.hhv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


//@ Usage-Classes
#include "GuiTimerId.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class GuiTimerTarget
{
public:
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	GuiTimerTarget(void);
	virtual ~GuiTimerTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	GuiTimerTarget(const GuiTimerTarget&);	// not implemented...
	void operator=(const GuiTimerTarget&);	// not implemented...
};


#endif // GuiTimerTarget_HH 
