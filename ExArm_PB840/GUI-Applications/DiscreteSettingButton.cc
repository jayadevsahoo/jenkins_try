#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DiscreteSettingButton - A generic setting button designed
// for adjusting discrete settings.  Stores a list of the text strings
// corresponding to each of the discrete values of a setting.
//---------------------------------------------------------------------
//@ Interface-Description
// The Discrete Setting Button allows the operator to adjust "discrete"
// settings, settings which have discrete values rather than a floating point
// value.  The Discrete Setting Button normally consists of a title which
// identifies the setting and which is displayed in the upper half of the
// button.  The lower half of the button identifies the current or adjusted
// value of the setting.  Current values are displayed in normal text, adjusted
// values in italic text.
//
// Once created (creation defines the size and title of the button, among other
// things), the strings of the valid values are setup via 'setValueInfo()'.
// This should be done at initialization time, before the button is first displayed.
//---------------------------------------------------------------------
//@ Rationale
// Contains all the functionality needed to allow the operator to
// view and adjust a discrete setting in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Most of the interaction with the Settings-Validation subsystem is handled in
// the parent SettingButton class.  All this class must do is store the text
// that it must display and react to changes to the value of its setting.
//
// A pointer to an array of 'ValueInfo_' structs is to be set by the client
// of this instance, and used by this class to determine string to be displayed.
// The pointer is initialized via 'setValueInfo()'.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// This class's value information must be set via 'setValueInfo()' prior
// to display of the button.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DiscreteSettingButton.ccv   25.0.4.0   19 Nov 2013 14:07:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: gdc    Date: 26-Jan-2009    SCR Number: 6461
//  Project:  840S
//  Description:
//      Implemented #define workaround for compiler bug.
//
//  Revision: 008  By:  gdc	   Date:  09-Apr-2007    SCR Number:  6237
//  Project:  Trend
//  Description:
//      Added automated vertical positioning logic for the value text.
//
//  Revision: 007  By:  sah	   Date:  09-Jun-2000    DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//      *  as part of the re-design, deleted 'addValue()' and added new
//         'setValueString_()' method
//      *  deleted unused 'setText_()' method
//
//  Revision: 006  By:  hhd	   Date:  24-May-1999    DR Number: 5369 
//    Project:  Sigma (R8027)
//    Description:
//		Removed references to Button::setButtonType() (empty) method.
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 004  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/DiscreteSettingButton.ccv   1.7.1.0   07/30/98 10:12:52   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "DiscreteSettingButton.hh"
#include "Colors.hh"

//@ Usage-Classes
#include "SettingSubject.hh"
//@ End-Usage

static const DiscreteSettingButton::ValueInfo  DEFAULT_VALUE_INFO_ =
{
	0,
	NULL   // uses NULL instead of NULL_STRING_ID as workaround to compiler bug
};


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DiscreteSettingButton()  [Constructor]
//
//@ Interface-Description
// Constructs a Discrete Setting Button.  The arguments are as follows:
// >Von
//	xOrg			The X coordinate at which this button is displayed.
//	yOrg			The Y coordinate at which this button is displayed.
//	width			The width of the button.
//	height			The height of the button.
//	buttonType		Type of the button, either Button::LIGHT,
//					Button::LIGHT_NON_LIT or Button::DARK.
//	bevelSize		Width of the button's internal border.
//	cornerType		Tells whether its corners are rounded or not, can be
//					Button::ROUND or Button::SQUARE.
//	borderType		The border type (whether it has an external flash border
//					or not).
//	titleText		The string id of the text to be displayed in the button
//					(positioned at 0, 0 of the button's label container).
//	settingId		The setting id which gives us a handle on the discrete
//					setting in the Settings-Validation subsystem.
//	pFocusSubScreen	The subscreen to which we pass on presses of the Accept key
//					(when this button has the Adjust Panel focus).
//	messageId		The string id of a message to be displayed in the message
//					area when this button is pressed down.  This is normally a
//					message which explains the symbol displayed as the title of
//					the button.
//	isMainSetting	Flag indicating whether this button is a main setting
//					button (displayed in the Main Setting Area).
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// All of the arguments are passed on to SettingButton.  The constructor
// simply adds the TextField used to display the value of the discrete
// setting to the label container of this button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DiscreteSettingButton::DiscreteSettingButton(Uint16 xOrg, Uint16 yOrg,
											 Uint16 width, Uint16 height, 
											 Button::ButtonType buttonType, 
											 Uint8 bevelSize, 
											 Button::CornerType cornerType, 
											 Button::BorderType borderType,
											 StringId titleText, 
											 SettingId::SettingIdType settingId,
											 SubScreen *pFocusSubScreen, 
											 StringId messageId,
											 Uint valuePointSize, 
											 Boolean isMainSetting) 
:   SettingButton(xOrg, yOrg, width, height, buttonType, bevelSize,
				  cornerType, borderType, titleText, settingId,
				  pFocusSubScreen, messageId, isMainSetting,
				  ::NULL_STRING_ID, ::GRAVITY_RIGHT),
	pValueInfo_(&::DEFAULT_VALUE_INFO_),
	VALUE_PT_SZ_(valuePointSize),
	textValue_(NULL_STRING_ID),
	valueTextPosition_(::GRAVITY_BOTTOM)
{
	CALL_TRACE("DiscreteSettingButton::DiscreteSettingButton(Uint16 xOrg, "
			   "Uint16 yOrg, Uint16 width, Uint16 height, "
			   "ButtonType buttonType, Uint8 bevelSize, "
			   "CornerType cornerType, BorderType borderType, "
			   "StringId titleText, SettingId::SettingIdType settingId, "
			   "SubScreen *pFocusSubScreen, StringId messageId, "
			   "Boolean isMainSetting)");

	// Append value text to interior of button
	addLabel(&textValue_);						 // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DiscreteSettingButton  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DiscreteSettingButton::~DiscreteSettingButton(void)
{
	CALL_TRACE("DiscreteSettingButton::~DiscreteSettingButton(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [Protected, virtual]
//
//@ Interface-Description
// Called when the display of this setting button needs to be updated with
// the latest setting value.  Passed a setting context id informing us the
// setting context from which we should retrieve the new setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
// We display the new value of the setting.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void DiscreteSettingButton::updateDisplay_(Notification::ChangeQualifier qualifierId,
										   const SettingSubject*         pSubject)
{
	CALL_TRACE("updateDisplay_(qualifierId, pSubject)");

	// Get the current discrete value
	DiscreteValue  settingValue;

	if (qualifierId == Notification::ACCEPTED)
	{											 // $[TI1]
		settingValue = pSubject->getAcceptedValue();
	}
	else if (qualifierId == Notification::ADJUSTED)
	{											 // $[TI2]
		settingValue = pSubject->getAdjustedValue();
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(qualifierId);
	}

	// Decide whether the value is current or proposed
	const Boolean  IS_CHANGED =
		(qualifierId == Notification::ADJUSTED  &&  pSubject->isChanged());

	setValueString_(settingValue, IS_CHANGED, textValue_);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void DiscreteSettingButton::SoftFault(const SoftFaultID  softFaultID,
									  const Uint32       lineNumber,
									  const char*        pFileName,
									  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, DISCRETESETTINGBUTTON,
							lineNumber, pFileName, pPredicate);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValueString_
//
//@ Interface-Description
//  Set 'rTextField' with a cheap-text string representing the value and
//  value qualifier given by its parameters, and the value format string
//  given in this button's value information.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void DiscreteSettingButton::setValueString_(const DiscreteValue settingValue,
											const Boolean       isChanged,
											TextField&          rTextField) const
{
	CALL_TRACE("setValueString_(settingValue, isChanged, rTextField)");

	wchar_t  tmpString[TextField::MAX_STRING_LENGTH+1];

	tmpString[0] = L'\0';

	if (settingValue < pValueInfo_->numValues  &&
		pValueInfo_->arrValues[settingValue] != NULL_STRING_ID)
	{											 // $[TI1]
		wchar_t formatChar = isChanged ? L'I' : L'N';

		// we compute the Y offset using the normal font style to achieve 
		// the same positioning whether displaying italic or normal font
		Int  yOffset = TextFont::GetMaxAscent(TextFont::Size(VALUE_PT_SZ_), TextFont::NORMAL);

		swprintf(tmpString, pValueInfo_->arrValues[settingValue], VALUE_PT_SZ_, yOffset, formatChar);
	}											 // $[TI2]

	// the textField is contained in the label container in the Button
	// any positioning of the text field is done relative to 
	// the position of the label container

	rTextField.setY(0);
	rTextField.setText(tmpString);
	rTextField.setHighlighted(isChanged);

	// position horizontally
	rTextField.positionInContainer(::GRAVITY_CENTER);

	// position vertically
	rTextField.positionInContainer(valueTextPosition_);

}
