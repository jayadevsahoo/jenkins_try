#ifndef ModeTriggerMediator_HH
#define ModeTriggerMediator_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: ModeTriggerMediator - Maintains a pointer to the active ModeTrigger list.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ModeTriggerMediator.hhv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"

#include "Breath_Delivery.hh"

//@ Usage-Class

class ModeTrigger;

//@ End-Usage

class ModeTriggerMediator
{
  public:
    ModeTriggerMediator(void);
    ~ModeTriggerMediator(void);

    static void SoftFault(const SoftFaultID softFaultID,
		   const Uint32      lineNumber,
		   const char*       pFileName  = NULL,
		   const char*       pPredicate = NULL);

    inline void changeModeTriggerList(ModeTrigger** pModeList); 
    inline void setActiveTriggerList(ModeTrigger** pModeList);
    void newCycle(void);
    void resetTriggerList(void);

  protected:

  private:
    ModeTriggerMediator(const ModeTriggerMediator&);          // Declared but not implemented
    void operator=(const ModeTriggerMediator&);   // Declared but not implemented

    Boolean checkActiveTriggers_(void) const;
    void setTriggerStatus_(void);
    void changeListPtr_(void);
	
    //@ Data-Member: pModeList_
    // a pointer to an array of pointers to mode triggers
    ModeTrigger** pModeList_;

    //@ Data-Member: pNextList_
    // A pointer to the next mode trigger list
    ModeTrigger** pNextList_;

};

// Inlined methods
#include "ModeTriggerMediator.in"

#endif // ModeTriggerMediator_HH 
