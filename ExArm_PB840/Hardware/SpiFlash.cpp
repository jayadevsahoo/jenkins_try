#include "stdafx.h"

//*****************************************************************************
//
//  SpiFlash.cpp - Implementation file for SpiFlash_t class.
//
//*****************************************************************************

#include "GlobalObjects.h"
#include "SpiFlash.h"
#include "sdk_spi.h"
#include "DebugAssert.h"

// TODO E600 remove unnecessary and dead code later

static const Uint8 FLASH_CMD_READ_ARRAY           = 0x0b;
static const Uint8 FLASH_CMD_READ_ARRAY_SLOW      = 0x03;
static const Uint8 FLASH_CMD_BLOCK_ERASE_4KB      = 0x20;
static const Uint8 FLASH_CMD_BLOCK_ERASE_32KB     = 0x52;
static const Uint8 FLASH_CMD_BLOCK_ERASE_64KB     = 0xd8;
static const Uint8 FLASH_CMD_CHIP_ERASE           = 0x60;
static const Uint8 FLASH_CMD_PROGRAM              = 0x02;
static const Uint8 FLASH_CMD_WRITE_ENABLE         = 0x06;
static const Uint8 FLASH_CMD_WRITE_DISABLE        = 0x04;
static const Uint8 FLASH_CMD_PROTECT_SECTOR       = 0x36;
static const Uint8 FLASH_CMD_UNPROTECT_SECTOR     = 0x39;
static const Uint8 FLASH_CMD_READ_SECTOR_PROTECT  = 0x3c;
static const Uint8 FLASH_CMD_READ_STATUS_REG      = 0x05;
static const Uint8 FLASH_CMD_WRITE_STATUS_REG     = 0x01;
static const Uint8 FLASH_CMD_READ_MFG_DEV_ID      = 0x9f;
static const Uint8 FLASH_CMD_DEEP_POWER_DOWN      = 0xb9;
static const Uint8 FLASH_CMD_DEEP_POWER_UP        = 0xab;

// Index 0 is always the command
static const int COMMAND_INDEX                      = 0;

//
static const int ADDRESS_HIGH_BYTE_INDEX            = 1;
static const int ADDRESS_MIDDLE_BYTE_INDEX          = 2;
static const int ADDRESS_LOW_BYTE_INDEX             = 3;

static const int WRITE_DATA_PACKET_HEADER_WRITE_SIZE = 4;
static const int READ_DATA_PACKET_HEADER_WRITE_SIZE = 4;

//*****************************************************************************
//
//  SpiFlash_t::SpiFlash_t - Constructor
//
//*****************************************************************************

SpiFlash_t::SpiFlash_t(void)
{
    DeviceExistsInitialized = false;
}
//*****************************************************************************
//
//  SpiFlash_t::SpiFlashInitComplete -
//
//*****************************************************************************
Boolean SpiFlash_t::SpiFlashInitComplete()
{
    return true;  //SpiFlashThread.SpiFlashInitComplete();
}
//*****************************************************************************
//
//  SpiFlash_t::OpenDevice -
//
//*****************************************************************************
Boolean SpiFlash_t::OpenDevice()
{
    DWORD   LastSystemError = NULL;
    Boolean    ReturnValue = true;

#ifdef SIGMA_GUI_CPU
    hActiveSPI = CreateFile(L"SPI3:", GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    LastSystemError = GetLastError();
    CS = 1;
#endif
#ifdef SIGMA_BD_CPU
    hActiveSPI = CreateFile(L"SPI4:", GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    CS = 0;
#endif
    if (hActiveSPI == INVALID_HANDLE_VALUE)
    {
        LastSystemError = GetLastError();
        DEBUGMSG(true, (_T("Error - SpiFlash_t::OpenDevice - %d.\r\n"), LastSystemError));
        ReturnValue = false;
    }
    DEBUGMSG(true, (_T("SpiFlash_t::OpenDevice - ReturnValue=%d LastError=%d.\r\n"), ReturnValue, LastSystemError));
    return ReturnValue;
}
//*****************************************************************************
//
//  SpiFlash_t::ConfigureDevice - This function is used to manipuate the status 
//                                register on the OMAP processor.  We are using 
//                                the status register to control the chip 
//                                select (CS) signal.  If we don't control it, 
//                                the driver de-asserts it after a byte is sent.
//
//*****************************************************************************
Boolean SpiFlash_t::ConfigureDevice(Boolean AssertChipSelect)
{
    //DWORD   LastSystemError;
   // DWORD   BytesReturned = 0;
   // Boolean    ReturnValue = true;
   // Boolean    ReturnStatus = false;

       // To configure SPI
    IOCTL_SPI_CONFIGURE_IN spi_config  = {CS, 0x001103E0};
    if (AssertChipSelect)
    {
// SPI Flash BD                                                                                              111098 7654 3210
//static const IOCTL_SPI_CONFIGURE_IN spi4_CH0_Flash_config = {0, 0x001103E0};		//0000 0000 0000 0001 0000 0011 1110 0000
//																			FORCE (20)		:	1		- FORCE
//																			DPE0 (16)		:	00		- DPE0
//																			TRM (13:12)		:	00		- TXRX
//																			WL (11:7)		:	07   	- 8 bits
//																			EPOL (6)		:	1		- SPIM_CSX held low during active state
//																			CLKD (5:2)		:	8		- Divide Ref Clock of 48Mbps by 256 = 188 Kbps
//																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
//																		
          spi_config.config  = 0x001103E0;
    }
    else
    {
//static const IOCTL_SPI_CONFIGURE_IN spi3_CH1_Flash_config = {1, 0x000103E0};		//0001 0000 0000 0001 0000 0011 1110 0000
//																			FORCE (20)		:	0		- FORCE
//																			DPE0 (16)		:	00		- DPE0
//																			TRM (13:12)		:	00		- TXRX
//																			WL (11:7)		:	07   	- 8 bits
//																			EPOL (6)		:	1		- SPIM_CSX held low during active state
//																			CLKD (5:2)		:	8		- Divide Ref Clock of 48Mbps by 256 = 188 Kbps
//																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
          spi_config.config  = 0x000103E0;
    }
	
	if(!DeviceIoControl(hActiveSPI, IOCTL_SPI_CONFIGURE, (LPVOID)&spi_config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL))
	{
		return false;
	}

    if(!DeviceExists())
    {
		return false;
    }

    return true;
}

//*****************************************************************************
//
//  SpiFlash_t::DeviceExists -
//
//*****************************************************************************

Boolean SpiFlash_t::DeviceExists()
{
    // This only needs to be called once
    if (DeviceExistsInitialized)
    {
        return SavedDeviceExists;
    }
    // Determine if the EEPROM is installed
    static const Uint8 EXPECTED_MANUFACTURER_ID = 0x1f;
    static const Uint8 EXPECTED_DEVICE_ID_1 = 0x47;
    static const Uint8 EXPECTED_DEVICE_ID_2 = 0x0;
    static const Uint8 EXPECTED_EXTENDED_INFO_LENGTH = 0x0;
    Uint8 ManufacturerId;
    Uint8 DeviceId1;
    Uint8 DeviceId2;
    Uint8 ExtendedInfoLength;

    DEBUG_ASSERT(ReadManufacturerAndDeviceId(ManufacturerId, DeviceId1, DeviceId2, ExtendedInfoLength));

    if ((ManufacturerId     == EXPECTED_MANUFACTURER_ID) &&
        (DeviceId1          == EXPECTED_DEVICE_ID_1)     &&
        (DeviceId2          == EXPECTED_DEVICE_ID_2)     &&
        (ExtendedInfoLength == EXPECTED_EXTENDED_INFO_LENGTH))
    {
        SavedDeviceExists = true;
    }
    else
    {
        SavedDeviceExists = false;
    }

    DeviceExistsInitialized = true;

    return SavedDeviceExists;
}



//*****************************************************************************
//
//  SpiFlash_t::ReadManufacturerAndDeviceId - This returns the device info.
//
//*****************************************************************************

Boolean SpiFlash_t::ReadManufacturerAndDeviceId(Uint8 &ManufacturerId, Uint8 &DeviceId1, Uint8 &DeviceId2, Uint8 &ExtendedInfoLength)
{
    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;

    static const int READ_MANUFACTURER_ID_PACKET_SIZE = 5;

    Uint8 Output[READ_MANUFACTURER_ID_PACKET_SIZE] = {0, 0, 0, 0, 0};
    Uint8 OpCode[READ_MANUFACTURER_ID_PACKET_SIZE] = {FLASH_CMD_READ_MFG_DEV_ID, 0, 0, 0, 0};

    if (hActiveSPI == INVALID_HANDLE_VALUE)
    {        
        DEBUGMSG(true, (_T("Error bad handle - SpiFlash_t::ReadManufacturerAndDeviceId.\r\n")));
    }

    ConfigureDevice(true);                         // Assert CS
   
	ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &Output, READ_MANUFACTURER_ID_PACKET_SIZE, &OpCode, READ_MANUFACTURER_ID_PACKET_SIZE, &BytesReturned, NULL))?true:false;
  
	if (!ReturnStatus)
    {
        DWORD LastSystemError = GetLastError();
        DEBUGMSG(true, (_T("SpiFlash_t::ReadID - ReturnStatus=%d LastError=%d ManufacturerId=%x %x %x %x.\r\n"), ReturnStatus, LastSystemError, Output[1], Output[2], Output[3], Output[4]));
    }
    ConfigureDevice(false);                        // De-assert CS
    ManufacturerId      = Output[1];
    DeviceId1           = Output[2];
    DeviceId2           = Output[3];
    ExtendedInfoLength  = Output[4];

    return ReturnStatus;
}


//*****************************************************************************
//
//  SpiFlash_t::ReadByte - Read one byte at the specified addres
//
//*****************************************************************************

Boolean SpiFlash_t::ReadByte(uint32_t FlashAddress, Uint8 &ReadData)
{
    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;

    static const int READ_BYTE_PACKET_SIZE = 5;
    Uint8 OpcodeAndAddress[READ_BYTE_PACKET_SIZE] = {FLASH_CMD_READ_ARRAY_SLOW};
    Uint8 TempReadData[READ_BYTE_PACKET_SIZE] = {0, 0, 0, 0, 0};

    OpcodeAndAddress[ADDRESS_HIGH_BYTE_INDEX]    = (Uint8)(FlashAddress >> 16);
    OpcodeAndAddress[ADDRESS_MIDDLE_BYTE_INDEX]  = (Uint8)(FlashAddress >> 8);
    OpcodeAndAddress[ADDRESS_LOW_BYTE_INDEX]     = (Uint8)FlashAddress;

    ConfigureDevice(true);                             // Assert CS

    ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &TempReadData, READ_BYTE_PACKET_SIZE, &OpcodeAndAddress, READ_BYTE_PACKET_SIZE, &BytesReturned, NULL))?true:false;
    
	if (!ReturnStatus)
    {
        DWORD LastSystemError = GetLastError();
        DEBUGMSG(true, (_T("SpiFlash_t::ReadBytes - ReturnStatus=%d LastError=%d.\r\n"), ReturnStatus, LastSystemError));
    }
    ConfigureDevice(false);                            // De-assert CS

    ReadData = TempReadData[READ_BYTE_PACKET_SIZE-1];

    return ReturnStatus;
}
//*****************************************************************************
//
//  SpiFlash_t::ReadBytes - Read the specified number of bytes, up to 32 bytes
//      (MAX_SPI_FLASH_ITEMS - READ_DATA_PACKET_HEADER_WRITE_SIZE).
//      This is used by the Read function to set up and call the SPI transfer
//      function.
//
//*****************************************************************************

Boolean SpiFlash_t::ReadBytes(uint32_t FlashAddress, Uint8 *ReadData, int NumberOfBytes)
{
    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;

    Uint8 OpcodeAndAddress[MAX_SPI_FLASH_ITEMS] = {FLASH_CMD_READ_ARRAY_SLOW};
    Uint8 TempReadData[MAX_SPI_FLASH_ITEMS];

    OpcodeAndAddress[ADDRESS_HIGH_BYTE_INDEX]    = (Uint8)(FlashAddress >> 16);
    OpcodeAndAddress[ADDRESS_MIDDLE_BYTE_INDEX]  = (Uint8)(FlashAddress >> 8);
    OpcodeAndAddress[ADDRESS_LOW_BYTE_INDEX]     = (Uint8)FlashAddress;

    ConfigureDevice(true);                             // Assert CS

    ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &TempReadData, NumberOfBytes + READ_DATA_PACKET_HEADER_WRITE_SIZE, &OpcodeAndAddress, NumberOfBytes + READ_DATA_PACKET_HEADER_WRITE_SIZE, &BytesReturned, NULL))?true:false;
    
	if (!ReturnStatus)
    {
        DWORD LastSystemError = GetLastError();
        DEBUGMSG(true, (_T("SpiFlash_t::ReadBytes - ReturnStatus=%d LastError=%d.\r\n"), ReturnStatus, LastSystemError));
    }
    ConfigureDevice(false);                            // De-assert CS

    int i;
    for (i=0; i<NumberOfBytes; i++)
    {
        ReadData[i] = TempReadData[i + READ_DATA_PACKET_HEADER_WRITE_SIZE];
    }

    return ReturnStatus;
}


//*****************************************************************************
//
//  SpiFlash_t::Read - Read the specified number of bytes
//
//*****************************************************************************


Boolean SpiFlash_t::Read(uint32_t FlashAddress, Uint8 *ReadData, int NumberOfBytes)
{
    // This is the max number of bytes that can be read on a given call to ReadBytes()
    // or, put another way, the number per SPI transmission.
    static const int MAX_DATA_BYTES_PER_TRANSMIT = MAX_SPI_FLASH_ITEMS - READ_DATA_PACKET_HEADER_WRITE_SIZE;

    int TotalBytesRead = 0;
    int BytesToReceive = 0;
    Uint8 *NextReadData = ReadData;
    uint32_t BeginFlashAddress = FlashAddress;
    uint32_t LastFlashAddress = FlashAddress;

    // Loop until all byte are received
    while (TotalBytesRead++ < NumberOfBytes)
    {
        if (++BytesToReceive >= MAX_DATA_BYTES_PER_TRANSMIT)                        // have we reached the per transmit limit?
        {
            if (!ReadBytes(BeginFlashAddress, NextReadData, BytesToReceive))        // read the bytes
            {
                return false;
            }
            BeginFlashAddress = ++LastFlashAddress;                                 // Move them both to 1 past the last flash address written
            NextReadData = &NextReadData[BytesToReceive];                           // point to the next byte to be written
            BytesToReceive = 0;
        }
        else
        {
            LastFlashAddress++;
        }
    }

    // unless we had a multiple of MAX_DATA_BYTES_PER_TRANSMIT, there will be some data
    // left to be sent
    if (BytesToReceive)
    {
        return ReadBytes(BeginFlashAddress, NextReadData, BytesToReceive);          // read the bytes that are left
    }
    else
    {
        return true;
    }
}


//*****************************************************************************
//
//  SpiFlash_t::WriteBytes - Write the specified number of bytes.
//      This is used by the Write function to set up and call the SPI transfer
//      function.
//
//*****************************************************************************

Boolean SpiFlash_t::WriteBytes(uint32_t FlashAddress, Uint8 *WriteData, int NumberOfBytes)
{
    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;

    //DEBUGMSG(true, (_T("SpiFlash_t::WriteBytes - Address=%d NumberOfBytes=%d.\r\n"), FlashAddress, NumberOfBytes));
    if (!WriteEnable())
    {
        return false;
    }

    Uint8 Output = 0;
    Uint8 WriteDataPacket[MAX_SPI_FLASH_ITEMS] = {FLASH_CMD_PROGRAM};

    WriteDataPacket[ADDRESS_HIGH_BYTE_INDEX]    = (Uint8)(FlashAddress >> 16);
    WriteDataPacket[ADDRESS_MIDDLE_BYTE_INDEX]  = (Uint8)(FlashAddress >> 8);
    WriteDataPacket[ADDRESS_LOW_BYTE_INDEX]     = (Uint8)FlashAddress;

    int i;
    int j = WRITE_DATA_PACKET_HEADER_WRITE_SIZE;
    for (i=0; i<NumberOfBytes; i++)
    {
        WriteDataPacket[j++] = WriteData[i];
    }
    ConfigureDevice(true);                          // Assert CS

         ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &Output, (WRITE_DATA_PACKET_HEADER_WRITE_SIZE + NumberOfBytes), 
                        &WriteDataPacket, (WRITE_DATA_PACKET_HEADER_WRITE_SIZE + NumberOfBytes), &BytesReturned, NULL))?true:false;

    if (!ReturnStatus)
    {
        DWORD LastSystemError = GetLastError();
        DEBUGMSG(true, (_T("SpiFlash_t::WriteBytes - ReturnStatus=%d LastError=%d.\r\n"), ReturnStatus, LastSystemError));
    }
    ConfigureDevice(false);                        // De-assert CS

    // We'll return once the write is complete
    static const Uint8 WRITE_TIMEOUT = 10;                                            // ms
    if (ReturnStatus)                                                                   // Don't bother if there was already a failure
    {
        ReturnStatus = WaitForOperationCompletion(WRITE_TIMEOUT);
    }

    return ReturnStatus;
}


//*****************************************************************************
//
//  SpiFlash_t::Write - Write the specified number of bytes.
//      This must break the request into write blocks that the low level driver
//      and hardware can handle (16 deep FIFO). Initial implementation is for
//      byte wide.
//
//      It is assumed the addressed memory is already erased!
//
//      This also handles the separation required when going over a 256 byte
//      page boundary.
//
//*****************************************************************************

Boolean SpiFlash_t::Write(uint32_t FlashAddress, Uint8 *WriteData, int NumberOfBytes)
{
    // This is the max number of bytes that can be written on a given call to WriteBytes()
    // or, put another way, the number per SPI transmission.
    static const int MAX_DATA_BYTES_PER_TRANSMIT = MAX_SPI_FLASH_ITEMS - WRITE_DATA_PACKET_HEADER_WRITE_SIZE;

    int TotalBytesWritten = 0;
    int BytesToTransmit = 0;
    Uint8 *NextWriteData = WriteData;
    uint32_t BeginFlashAddress = FlashAddress;
    uint32_t LastFlashAddress = FlashAddress;

    DEBUGMSG(true, (_T("SpiFlash_t::Write - Address=%d NumberOfBytes=%d.\r\n"), FlashAddress, NumberOfBytes));
    // Loop until all byte are transmitted
    while (TotalBytesWritten++ < NumberOfBytes)
    {
        if ((++BytesToTransmit >= MAX_DATA_BYTES_PER_TRANSMIT) ||                   // have we reached the per transmit limit?
            ((Uint8)LastFlashAddress == 0xff))                                    // or this is the end of a page?
        {
            if (!WriteBytes(BeginFlashAddress, NextWriteData, BytesToTransmit))     // write the bytes
            {
                return false;
            }
            BeginFlashAddress = ++LastFlashAddress;                                 // Move them both to 1 past the last flash address written
            NextWriteData = &NextWriteData[BytesToTransmit];                        // point to the next byte to be written
            BytesToTransmit = 0;
        }
        else
        {
            LastFlashAddress++;
        }
    }

    // unless we had a multiple of MAX_DATA_BYTES_PER_TRANSMIT, there will be some data
    // left to be sent
    if (BytesToTransmit)
    {
        return WriteBytes(BeginFlashAddress, NextWriteData, BytesToTransmit);       // write the bytes that are left
    }
    else
    {
        return true;
    }
}

//*****************************************************************************
//
//  SpiFlash_t::EraseBlock -
//
//*****************************************************************************

Boolean SpiFlash_t::EraseBlock(uint32_t FlashAddress, SpiFlashEraseBlockSize_t BlockSize)
{
    if (!WriteEnable())
    {
        return false;
    }

    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;
    static const int ERASE_BLOCK_PACKET_SIZE = 4;
    Uint8 OpCode[ERASE_BLOCK_PACKET_SIZE];
    Uint8 FlashOutput[ERASE_BLOCK_PACKET_SIZE] = {0,0,0,0};

    switch (BlockSize)
    {
        case ERASE_4KB:
            OpCode[COMMAND_INDEX] = FLASH_CMD_BLOCK_ERASE_4KB;
            break;
        case ERASE_32KB:
            OpCode[COMMAND_INDEX] = FLASH_CMD_BLOCK_ERASE_32KB;
            break;
        case ERASE_64KB:
            OpCode[COMMAND_INDEX] = FLASH_CMD_BLOCK_ERASE_64KB;
            break;
        default:
			bool status = false;
            DEBUG_ASSERT(status);
    }

    OpCode[ADDRESS_HIGH_BYTE_INDEX]    = (Uint8)(FlashAddress >> 16);
    OpCode[ADDRESS_MIDDLE_BYTE_INDEX]  = (Uint8)(FlashAddress >> 8);
    OpCode[ADDRESS_LOW_BYTE_INDEX]     = (Uint8)FlashAddress;

    ConfigureDevice(true);                         // Assert CS

    ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &FlashOutput, ERASE_BLOCK_PACKET_SIZE, &OpCode, ERASE_BLOCK_PACKET_SIZE, &BytesReturned, NULL))?true:false;
    
	if (!ReturnStatus)
    {
        DWORD LastSystemError = GetLastError();
        DEBUGMSG(true, (_T("SpiFlash_t::EraseBlock 1 - ReturnStatus=%d LastError=%d.\r\n"), ReturnStatus, LastSystemError));
    }
    ConfigureDevice(false);                        // De-assert CS

    // We'll return once the erase is complete
    static uint16_t const ERASE_BLOCK_TIMEOUT = 300;                                // ms
    if (ReturnStatus)                                                                   // Don't bother if there was already a failure
    {
        ReturnStatus = WaitForOperationCompletion(ERASE_BLOCK_TIMEOUT);
        if (!ReturnStatus)
        {
            DEBUGMSG(true, (_T("SpiFlash_t::EraseBlock 2 - ReturnStatus=%d.\r\n"), ReturnStatus));
        }

    }

    return ReturnStatus;
}


//*****************************************************************************
//
//  SpiFlash_t::EraseChip -
//
//*****************************************************************************

Boolean SpiFlash_t::EraseChip(void)
{
    if (!WriteEnable())
    {
        return false;
    }

    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;
    static const int ERASE_BLOCK_PACKET_SIZE = 1;
    Uint8 OpCode[ERASE_BLOCK_PACKET_SIZE];
    Uint8 FlashOutput[ERASE_BLOCK_PACKET_SIZE] = {0};
    OpCode[COMMAND_INDEX] = FLASH_CMD_CHIP_ERASE;

   ConfigureDevice(true);                         // Assert CS

    ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &FlashOutput, ERASE_BLOCK_PACKET_SIZE, &OpCode, ERASE_BLOCK_PACKET_SIZE, &BytesReturned, NULL))?true:false;
    
	if (!ReturnStatus)
    {
        DWORD LastSystemError = GetLastError();
        DEBUGMSG(true, (_T("SpiFlash_t::EraseChip 1 - ReturnStatus=%d LastError=%d.\r\n"), ReturnStatus, LastSystemError));
    }
    ConfigureDevice(false);                        // De-assert CS

    // We'll return once the erase is complete
    static uint16_t const ERASE_CHIP_TIMEOUT = 2000;        // ms
    if (ReturnStatus)                                       // Don't bother if there was already a failure
    {
        ReturnStatus = WaitForOperationCompletion(ERASE_CHIP_TIMEOUT);
        if (!ReturnStatus)
        {
            DEBUGMSG(true, (_T("SpiFlash_t::EraseChip 2 - ReturnStatus=%d.\r\n"), ReturnStatus));
        }

    }

    return ReturnStatus;
}


//*****************************************************************************
//
//  SpiFlash_t::WaitForOperationCompletion - This will read the status register and
//          return true when the busy bit clears. It returns false if it times out.
//
//*****************************************************************************

Boolean SpiFlash_t::WaitForOperationCompletion(int Timeout)
{
    Uint8 Status;
    int Timer = 0;

    do
    {
        if (!ReadStatus(Status))
        {
            return false;
        }
        Sleep(1);
    }
    while (((Status & READ_STATUS_MASK_RDY_BSY) == READ_STATUS_MASK_RDY_BSY) &&     // Still busy?
           (++Timer < Timeout));                                                    // but not timed out?

    // If it didn't ever finish?
    if ((Status & READ_STATUS_MASK_RDY_BSY) == READ_STATUS_MASK_RDY_BSY)
    {
        return false;
    }

    return true;
}

//*****************************************************************************
//
//  SpiFlash_t::WriteDisable -
//
//*****************************************************************************

Boolean SpiFlash_t::WriteDisable()
{

    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;
    static const int WRITE_DISABLE_PACKET_SIZE = 1;

    Uint8 FlashOutput[WRITE_DISABLE_PACKET_SIZE] = {0};
    Uint8 OpCode[WRITE_DISABLE_PACKET_SIZE] = {FLASH_CMD_WRITE_DISABLE};

    if (hActiveSPI == INVALID_HANDLE_VALUE)
    {
        DEBUGMSG(true, (_T("SpiFlash_t::WriteEnable - bad handle.\r\n")));
    }
    else
    {
        ConfigureDevice(true);                     // Assert CS

        ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &FlashOutput, WRITE_DISABLE_PACKET_SIZE, &OpCode, WRITE_DISABLE_PACKET_SIZE, &BytesReturned, NULL))?true:false;
        
		if (!ReturnStatus)
        {
            DWORD LastSystemError = GetLastError();
            DEBUGMSG(true, (_T("SpiFlash_t::WriteDisable - ReturnStatus=%d LastError=%d.\r\n"), ReturnStatus, LastSystemError));
        }
        ConfigureDevice(false);                    // De-assert CS
    }

    return ReturnStatus;
}


//*****************************************************************************
//
//  SpiFlash_t::WriteEnable -
//
//*****************************************************************************

Boolean SpiFlash_t::WriteEnable()
{

    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;
    static const int WRITE_ENABLE_PACKET_SIZE = 1;

    Uint8 FlashOutput[WRITE_ENABLE_PACKET_SIZE] = {0};
    Uint8 OpCode[WRITE_ENABLE_PACKET_SIZE] = {FLASH_CMD_WRITE_ENABLE};

    if (hActiveSPI == INVALID_HANDLE_VALUE)
    {
        DEBUGMSG(true, (_T("SpiFlash_t::WriteEnable - bad handle.\r\n")));
    }
    else
    {
        ConfigureDevice(true);                     // Assert CS

        ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &FlashOutput, WRITE_ENABLE_PACKET_SIZE, &OpCode, WRITE_ENABLE_PACKET_SIZE, &BytesReturned, NULL))?true:false;
        
		if (!ReturnStatus)
        {
            DWORD LastSystemError = GetLastError();
            DEBUGMSG(true, (_T("SpiFlash_t::WriteEnable - ReturnStatus=%d LastError=%d.\r\n"), ReturnStatus, LastSystemError));
        }
        ConfigureDevice(false);                    // De-assert CS
    }

    return ReturnStatus;
}


//*****************************************************************************
//
//  SpiFlash_t::ReadStatus -
//
//*****************************************************************************

Boolean SpiFlash_t::ReadStatus(Uint8 &Status)
{
    static const int READ_STATUS_PACKET_SIZE = 3;

    static const Uint8 ALL_ONES = 0xFF;
    static const Uint8 ALL_ZEROS = 0x00;

    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;
    Uint8 FlashOutput[READ_STATUS_PACKET_SIZE] = {0, 0, 0};
    Uint8 OpCode[READ_STATUS_PACKET_SIZE] = {FLASH_CMD_READ_STATUS_REG, 0, 0};

    if (hActiveSPI == INVALID_HANDLE_VALUE)
    {
        DEBUGMSG(true, (_T("SpiFlash_t::ReadStatus - bad handle.\r\n")));
    }
    else
    {
        //SpiFlash.ConfigureDevice(true);                     // Assert CS
//        ConfigureDevice(true);                     // Assert CS

        ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &FlashOutput, READ_STATUS_PACKET_SIZE, &OpCode, READ_STATUS_PACKET_SIZE, &BytesReturned, NULL))?true:false;
       
		if (!ReturnStatus)
        {
            DWORD LastSystemError = GetLastError();
            DEBUGMSG(true, (_T("SpiFlash_t::ReadStatus - ReturnStatus=%d LastError=%d StatusReg=%x.\r\n"), ReturnStatus, LastSystemError, FlashOutput[1]));
        }
        //SpiFlash.ConfigureDevice(false);                    // De-assert CS
 //       ConfigureDevice(false);                    // De-assert CS

        Status = FlashOutput[1];
    }
    return ReturnStatus;
}


//*****************************************************************************
//
//  SpiFlash_t::ReadProtection -
//
//*****************************************************************************

Boolean SpiFlash_t::ReadProtection(uint32_t FlashAddress, Uint8 &Status)
{
    static const int READ_PROTECTION_PACKET_SIZE = 5;

    static const Uint8 ALL_ONES = 0xFF;
    static const Uint8 ALL_ZEROS = 0x00;

    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;
    Uint8 FlashOutput[READ_PROTECTION_PACKET_SIZE] = {0, 0, 0, 0, 0};
    Uint8 OpCode[READ_PROTECTION_PACKET_SIZE] = {FLASH_CMD_READ_SECTOR_PROTECT, 0, 0, 0, 0};

    OpCode[ADDRESS_HIGH_BYTE_INDEX]    = (Uint8)(FlashAddress >> 16);
    OpCode[ADDRESS_MIDDLE_BYTE_INDEX]  = (Uint8)(FlashAddress >> 8);
    OpCode[ADDRESS_LOW_BYTE_INDEX]     = (Uint8)FlashAddress;

    if (hActiveSPI == INVALID_HANDLE_VALUE)
    {
        DEBUGMSG(true, (_T("SpiFlash_t::ReadStatus - bad handle.\r\n")));
    }
    else
    {
        ConfigureDevice(true);                     // Assert CS

        ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &FlashOutput, READ_PROTECTION_PACKET_SIZE, &OpCode, READ_PROTECTION_PACKET_SIZE, &BytesReturned, NULL))?true:false;
        
		if (!ReturnStatus)
        {
            DWORD LastSystemError = GetLastError();
            DEBUGMSG(true, (_T("SpiFlash_t::ReadStatus - ReturnStatus=%d LastError=%d StatusReg=%x.\r\n"), ReturnStatus, LastSystemError, FlashOutput[1]));
        }
        ConfigureDevice(false);                    // De-assert CS

        Status = FlashOutput[4];
    }
    return ReturnStatus;
}
//*****************************************************************************
//
//  SpiFlash_t::WriteStatus -
//
//*****************************************************************************

Boolean SpiFlash_t::WriteStatus(Uint8 Status)
{
    if (!WriteEnable())
    {
        DEBUGMSG(true, (_T("WriteStatus !WriteEnable.\r\n")));
        return false;
    }

    static const int WRITE_STATUS_PACKET_SIZE = 2;
    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;
    Uint8 OpCode[WRITE_STATUS_PACKET_SIZE] = {FLASH_CMD_WRITE_STATUS_REG};
    Uint8 FlashOutput[WRITE_STATUS_PACKET_SIZE];

    OpCode[1]    = Status;

    if (hActiveSPI == INVALID_HANDLE_VALUE)
    {
        DEBUGMSG(true, (_T("SpiFlash_t::WriteStatus - bad handle.\r\n")));
    }
    else
    {
        ConfigureDevice(true);                     // Assert CS

        ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &FlashOutput, WRITE_STATUS_PACKET_SIZE, &OpCode, WRITE_STATUS_PACKET_SIZE, &BytesReturned, NULL))?true:false;
        
		if (!ReturnStatus)
        {
            DWORD LastSystemError = GetLastError();
            DEBUGMSG(true, (_T("SpiFlash_t::WriteStatus - ReturnStatus=%d LastError=%d.\r\n"), ReturnStatus, LastSystemError));
        }
        ConfigureDevice(false);                    // De-assert CS
    }
    return ReturnStatus;
}


//*****************************************************************************
//
//  SpiFlash_t::GlobalProtect - Per the data sheet, the protect is dependent
//      on the current state of the SPRL bit. So, we simply write to the status
//      register twice. The first write ensures the SPRL bit is 0 (even if it
//      already was 0). Then the second write does the protect (if it wasn't
//      already 0).
//
//*****************************************************************************

Boolean SpiFlash_t::GlobalProtect()
{
    static const Uint8    GLOBAL_PROTECT = 0x7f;

   if (!WriteStatus(GLOBAL_PROTECT))                   // first write
    {
        return false;
    }

    return WriteStatus(GLOBAL_PROTECT);                // second write and return
}


//*****************************************************************************
//
//  SpiFlash_t::GlobalUnprotect - Per the data sheet, the unprotect is dependent
//      on the current state of the SPRL bit. So, we simply write to the status
//      register twice. The first write ensures the SPRL bit is 0 (even if it
//      already was 0). Then the second write does the unprotect (if it wasn't
//      already 0).
//
//*****************************************************************************

Boolean SpiFlash_t::GlobalUnprotect()
{
    static const Uint8    GLOBAL_UNPROTECT = 0;

    if (!WriteStatus(GLOBAL_UNPROTECT))                // first write
    {
        return false;
    }

    return WriteStatus(GLOBAL_UNPROTECT);              // second write and return
}

//*****************************************************************************
//
//  SpiFlash_t::LockController - Set SPI controller in single channel master mode.
//
//*****************************************************************************

Boolean SpiFlash_t::LockController()
{

    static const int DUMMY_PACKET_SIZE = 4;

    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;

    DWORD Output = 0;
    DWORD OpCode = 0;

    if (hActiveSPI == INVALID_HANDLE_VALUE)
    {
        DEBUGMSG(true, (_T("SpiFlash_t::LockController - bad handle.\r\n")));
    }
    else
    {

        ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_LOCK_CTRL, &Output, DUMMY_PACKET_SIZE, &OpCode, DUMMY_PACKET_SIZE, &BytesReturned, NULL))?true:false;
        
		if (!ReturnStatus)
        {
            DWORD LastSystemError = GetLastError();
            DEBUGMSG(true, (_T("SpiFlash_t::LockController - ReturnStatus=%d LastError=%d.\r\n"), ReturnStatus, LastSystemError));
        }
    }
    return ReturnStatus;
}

//*****************************************************************************
//
//  SpiFlash_t::UnLockController - Clear SPI controller single channel master mode.
//
//*****************************************************************************

Boolean SpiFlash_t::UnLockController()
{

    static const int DUMMY_PACKET_SIZE = 1;
    Uint8 Output = 0;
    Uint8 OpCode = 0;

    Boolean    ReturnStatus = false;
    DWORD   BytesReturned = 0;

    if (hActiveSPI == INVALID_HANDLE_VALUE)
    {
        DEBUGMSG(true, (_T("SpiFlash_t::LockController - bad handle.\r\n")));
    }
    else
    {

        ReturnStatus = (DeviceIoControl(hActiveSPI, IOCTL_SPI_UNLOCK_CTRL, &Output, DUMMY_PACKET_SIZE, &OpCode, DUMMY_PACKET_SIZE, &BytesReturned, NULL))?true:false;
        
		if (!ReturnStatus)
        {
            DWORD LastSystemError = GetLastError();
            DEBUGMSG(true, (_T("SpiFlash_t::UnLockController - ReturnStatus=%d LastError=%d.\r\n"), ReturnStatus, LastSystemError));
        }
    }
    return ReturnStatus;
}
