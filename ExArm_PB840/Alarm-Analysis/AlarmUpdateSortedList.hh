#ifndef AlarmUpdateSortedList_HH
#define AlarmUpdateSortedList_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmUpdateSortedList - derived from template class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmUpdateSortedList.hhv   25.0.4.0   19 Nov 2013 13:51:18   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "AlarmUpdate.hh"
#include "MutableC_AlarmUpdate_NUM_ALARM.hh"
//@ End-Usage


class AlarmUpdateSortedList : public FixedMutableSortC(AlarmUpdate,NUM_ALARM) 
{

  public:

    AlarmUpdateSortedList(void);
    AlarmUpdateSortedList(const AlarmUpdateSortedList& rList);

    ~AlarmUpdateSortedList(void);

    void   operator=(const AlarmUpdateSortedList& rList);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

  
  protected:

  private:

};


// Inlined methods
#include "AlarmUpdateSortedList.in"


#endif // AlarmUpdateSortedList_HH 
