/*****************************************************************************/
/*   Module Name:    840Msgs.h                                          
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*  
/*   Description of Change                  By                Date           
/*   ---------------------                  -----             ------         
/*   Added UPLOAD_ALARM_LOG command.        RHJ               27 Nov,2007    
/*
/*****************************************************************************/
/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _840_MESSAGES_H
#define _840_MESSAGES_H

#include "common.h"

#define MAX_RETURN_VALUE  64
#define POST_MAX 256
#define STOP_GAS_FLOW 64
#define MAX_PACKET_SIZE 256
#define NO_PROMPT_ALLOWED -2
#define NO_KEYS_ALLOWED   -5

enum { MESSAGE_LENGTH = 1, SEQ_NUMBER, MESSAGE_ID, MESSAGE_TYPE } ;

enum RemoteTestId 
{
      REMOTE_TEST_NO_START_ID = -1
    , REMOTE_TEST_GAS_SUPPLY_ID = 0
    , REMOTE_TEST_GUI_KEYBOARD_ID = 1
    , REMOTE_TEST_GUI_KNOB_ID = 2
    , REMOTE_TEST_GUI_LAMP_ID = 3
    , REMOTE_TEST_BD_LAMP_ID = 4
    , REMOTE_TEST_GUI_AUDIO_ID = 5
    , REMOTE_TEST_BD_AUDIO_ID = 6
    , REMOTE_TEST_PSOL_LOOPBACK_ID = 7
    , REMOTE_TEST_LOW_FLOW_FS_CC_ID = 8
    , REMOTE_TEST_HIGH_FLOW_FS_CC_ID = 9
    , REMOTE_TEST_CIRCUIT_PRESSURE_ID = 10
    , REMOTE_TEST_SAFETY_SYSTEM_ID = 11
    , REMOTE_TEST_EXH_VALVE_LOOPBACK_ID = 12
    , REMOTE_TEST_EXH_VALVE_SEAL_ID = 13
    , REMOTE_TEST_EXH_VALVE_ID = 14
    , REMOTE_TEST_EXH_VALVE_CALIB_ID = 15
    , REMOTE_TEST_SM_LEAK_ID = 16
    , REMOTE_TEST_EXH_HEATER_ID = 17
    , REMOTE_TEST_COMPRESSOR_ID = 18
    , REMOTE_TEST_COMPRESSOR_LEAK_ID = 19
    , REMOTE_TEST_GENERAL_ELECTRONICS_ID = 20
    , REMOTE_TEST_GUI_TOUCH_ID = 21
    , REMOTE_TEST_GUI_SERIAL_PORT_ID = 22
    , REMOTE_TEST_BATTERY_ID = 23
    , REMOTE_TEST_FS_CALIBRATION_ID = 24
    , REMOTE_TEST_EXH_VALVE_VELOCITY_XDUCER_ID = 25
    , REMOTE_TEST_GUI_NURSE_CALL_ID = 26
    , REMOTE_TEST_ATMOS_PRESSURE_READ_ID = 27
    , REMOTE_TEST_SET_COMPRESSOR_SN_ID = 28
    , REMOTE_TEST_SET_COMPRESSOR_TIME_ID = 29
    , REMOTE_TEST_SET_DATAKEY_TO_VENT_DATA_ID = 30
    , REMOTE_TEST_SET_AIR_FLOW_ID = 31
    , REMOTE_TEST_SET_O2_FLOW_ID = 32
    , REMOTE_TEST_PURGE_WITH_AIR_ID = 33
    , REMOTE_TEST_PURGE_WITH_O2_ID = 34
    , REMOTE_TEST_MAX
};

enum OutgoingCommandId
{
    NULL_OUTGOING_COMMAND_ID=-1,

    LAPTOP_CONNECTED_COMMAND=0,
    COMMUNICATION_GRANTED_COMMAND=1,
    MESSAGE_ACK_COMMAND=2,
    SOFTWARE_VERSION_COMMAND=3,
    UNIT_SERIAL_NUMBER_COMMAND=4,
    INVENTORY_INFO_COMMAND=5,
    UNIT_RUNTIME_INFO_COMMAND=6,
    
    // Commands used by UPLOAD_SYSTEM_LOG, UPLOAD_COMMUNICATION_LOG,
    // and UPLOAD_EST_SST_LOG.
    //
    START_UPLOAD_COMMAND=7,
    DATA_UPLOAD_COMMAND=8,
    END_UPLOAD_COMMAND=9,
    
    // Commands used by TEST_EVENT
    //
    PROMPT_COMMAND=10,
    KEY_ALLOWED_COMMAND=11,
    STATUS_COMMAND=12,
    CONDITION_COMMAND=13,
    DATA_COMMAND=14,

    // Commands used when invalid event occured.
    //
    INVALID_MSG_COMMAND=15,
    NUMBER_OF_OUTGOING_COMMAND=16   // UPDATE this field is NECESSARY,
                                    // whenever the enum is added or deleted.
};

enum PromptId
{
    NULL_PROMPT_ID = -1,
    CONNECT_INSP_FILTER_PROMPT, 
    REMOVE_INSP_FILTER_PROMPT,
    ATTACH_CIRCUIT_PROMPT,
    UNBLOCK_WYE_PROMPT,
    BLOCK_WYE_PROMPT,
    UNBLOCK_INLET_PORT_PROMPT,
    BLOCK_INLET_PORT_PROMPT,
    DISCONNECT_AIR_PROMPT,
    DISCONNECT_O2_PROMPT,
    CONNECT_AIR_PROMPT,
    CONNECT_AIR_IF_AVAIL_PROMPT,
    CONNECT_O2_PROMPT,
    DISCNT_TUBE_BFR_EXH_FLTR_PROMPT,   
    CONCT_TUBE_BFR_EXH_FLTR_PROMPT,
    DISCONNECT_TUBE_FROM_INLET,
    CONNECT_TUBE_TO_INLET,
    ATTACH_GOLD_STD_TEST_TUBING_PROMPT,
    CONNECT_WALL_AIR_PROMPT,              
    KEY_ACCEPT_PROMPT,                       
    KEY_CLEAR_PROMPT,
    KEY_INSP_PAUSE_PROMPT,
    KEY_EXP_PAUSE_PROMPT,
    KEY_MAN_INSP_PROMPT,
    KEY_HUNDRED_PERCENT_O2_PROMPT,
    KEY_INFO_PROMPT,
    KEY_ALARM_RESET_PROMPT,
    KEY_ALARM_SILENCE_PROMPT,
    KEY_ALARM_VOLUME_PROMPT,
    KEY_SCREEN_BRIGHTNESS_PROMPT,
    KEY_SCREEN_CONTRAST_PROMPT,
    KEY_SCREEN_LOCK_PROMPT,
    KEY_LEFT_SPARE_PROMPT,
    TURN_KNOB_LEFT_PROMPT,
    TURN_KNOB_RIGHT_PROMPT,        
    NORMAL_PROMPT,                   
    VENTILATOR_INOPERATIVE_PROMPT,   
    SAFETY_VALVE_OPEN_PROMPT,
    LOSS_OF_GUI_PROMPT,
    BATTERY_BACKUP_READY_PROMPT,
    COMPRESSOR_READY_PROMPT,
    COMPRESSOR_OPERATING_PROMPT,
    COMPRESSOR_MOTOR_ON_PROMPT,
    COMPRESSOR_MOTOR_OFF_PROMPT,
    HUNDRED_PERCENT_O2_PROMPT,
    ALARM_SILENCE_PROMPT,
    SCREEN_LOCK_PROMPT,             
    HIGH_ALARM_PROMPT,
    MEDIUM_ALARM_PROMPT,
    LOW_ALARM_PROMPT,
    ON_BATTERY_POWER_PROMPT,
    SOUND_PROMPT,                   
    POWER_DOWN_PROMPT,              
    GUI_SIDE_TEST_PROMPT,
    VENT_INOP_A_TEST_PROMPT,
    VENT_INOP_B_TEST_PROMPT,
    TEN_SEC_A_TEST_PROMPT,
    TEN_SEC_B_TEST_PROMPT,
    NURSE_CALL_TEST_PROMPT,
    NURSE_CALL_ON_PROMPT,
    NURSE_CALL_OFF_PROMPT,
    EV_CAL_PRESSURE_SENSOR_ALERT_PROMPT,
    CONNECT_AC_PROMPT,
    CONNECT_AIR_AND_O2_PROMPT,
    ACCEPT_TO_STOP_FLOW_PROMPT,
    CONNECT_HUMIDIFIER_PROMPT,
    NUM_PROMPT_ID
};

enum KeysAllowedId
{
    NULL_KEYS_ALLOWED_ID = -1,
    ACCEPT_KEY_ONLY,
    ACCEPT_AND_CANCEL_ONLY,
    NO_KEYS,
    ACCEPT_PASS_AND_CANCEL_FAIL_ONLY,
    NUM_KEYS_ALLOWED_ID
};

//@ Type: ActionId
// This enum lists all of keys plus an exit via the touch screen
enum ActionId
{
    NULL_ACTION_ID = -1,
    USER_EXIT,
    KEY_ACCEPT,
    KEY_CLEAR,
    NUM_ACTION_ID
};

enum TestStatusId
{
    TEST_END,
    TEST_START,
    TEST_ALERT,
    TEST_FAILURE,
    TEST_OPERATOR_EXIT,
    NOT_INSTALLED,
    NUM_TEST_DATA_STATUS,
    TEST_STATUS_ID_ERROR = NUM_TEST_DATA_STATUS
};


enum TestConditionId
{
    NULL_CONDITION_ITEM = 0,                             
    CONDITION_1,
    CONDITION_2,
    CONDITION_3,
    CONDITION_4,
    CONDITION_5,
    CONDITION_6,
    CONDITION_7,
    CONDITION_8,
    CONDITION_9,
    CONDITION_10,
    CONDITION_11,
    CONDITION_12,
    CONDITION_13,
    CONDITION_14,
    CONDITION_15,
    CONDITION_16,
    CONDITION_17,
    CONDITION_18,
    CONDITION_19,
    CONDITION_20,
    NUM_CONDITION_ITEM
};

enum ServiceModeTestId
{
    TEST_NOT_START_ID = -1,
    INITIALIZE_FLOW_SENSORS_ID,
    GAS_SUPPLY_TEST_ID,
    GS_CIRCUIT_ATTACHED_TEST_ID,
    WYE_BLOCKED_TEST_ID,
    SST_FILTER_TEST_ID,
    GUI_KEYBOARD_TEST_ID,
    GUI_KNOB_TEST_ID,
    GUI_LAMP_TEST_ID,
    BD_LAMP_TEST_ID,
    GUI_AUDIO_TEST_ID,
    BD_AUDIO_TEST_ID,
    PSOL_LOOPBACK_TEST_ID,
    FS_CROSS_CHECK_TEST_ID,
    SST_FS_CC_TEST_ID,
    LOW_FLOW_FS_CC_TEST_ID,
    HIGH_FLOW_FS_CC_TEST_ID,
    CIRCUIT_PRESSURE_TEST_ID,
    SAFETY_SYSTEM_TEST_ID,
    EXH_VALVE_LOOPBACK_TEST_ID,
    EXH_VALVE_SEAL_TEST_ID,
    EXH_VALVE_TEST_ID,
    EXH_VALVE_CALIB_TEST_ID,
    SM_LEAK_TEST_ID,
    SST_LEAK_TEST_ID,
    COMPLIANCE_CALIB_TEST_ID,
    EXH_HEATER_TEST_ID,
    COMPRESSOR_TEST_ID,
    COMPRESSOR_LEAK_TEST_ID,
    REMOTE_COMPRESSOR_LEAK_TEST_ID,
    GENERAL_ELECTRONICS_TEST_ID,
    GUI_TOUCH_TEST_ID,
    GUI_SERIAL_PORT_TEST_ID,
    BD_EXTENDED_MEMORY_TEST_ID,
    GUI_EXTENDED_MEMORY_TEST_ID,
    BATTERY_TEST_ID,
    BD_INIT_SERIAL_NUMBER_ID,
    GUI_INIT_SERIAL_NUMBER_ID,
    CAL_INFO_DUPLICATION_ID,
    BD_VENT_INOP_ID,
    GUI_VENT_INOP_ID,
    RESTART_BD_ID,
    RESTART_GUI_ID,
    BD_INIT_DEFAULT_SERIAL_NUMBER_ID,
    GUI_INIT_DEFAULT_SERIAL_NUMBER_ID,
    NUM_SERVICE_TEST_ID
};

enum ServiceModeTestTypeId
{
    SM_TYPE_NOT_SET = -1,
    SM_EST_TYPE,
    SM_SST_TYPE,
    SM_EXTERNAL_TYPE,
    SM_MISC_TYPE,
    NUM_SM_TEST_TYPE
};

enum FunctionId
{
      FUNCTION_ID_START = -1
    , EST_PASSED_ID = 0
    , SET_TEST_TYPE_EST_ID = 1
    , SET_TEST_TYPE_SST_ID = 2
    , SET_TEST_TYPE_REMOTE_ID = 3
    , SET_TEST_TYPE_MISC_ID = 4
    , EST_OVERRIDE_ID = 5
    , SST_OVERRIDE_ID = 6
    , EST_COMPLETE_ID = 7
    , SST_COMPLETE_ID = 8
    , BD_DOWNLOAD_ID = 9
    , GUI_DOWNLOAD_ID = 10
    , FORCE_EST_OVERIDE_ID = 11
    , FORCE_SST_OVERIDE_ID = 12
    , ENABLE_VENT_INOP_ID = 13
    , DISABLE_VENT_INOP_ID = 14
    , DEFAULT_FLASH_ID = 15
    , DELETE_SYSTEM_DIAGNOSTIC_LOG_ID = 16
    , DELETE_SYSTEM_INFORMATION_LOG_ID = 17
    , DELETE_SELFTEST_LOG_ID = 18

    , FUNCTION_ID_MAX
};

enum LTEventId
{
    NULL_EVENT_ID = -1,

    // Event generated.
    LAPTOP_CONNECTED=0,
    COMMUNICATION_GRANTED=1,
    MESSAGE_ACK=2,
    SOFTWARE_VERSION=3,
    UNIT_SERIAL_NUMBER=4,
    INVENTORY_INFO=5,
    UNIT_RUNTIME_INFO=6,
    UPLOAD_SYSTEM_LOG=7,
    UPLOAD_SYS_INFO_LOG=8,
    UPLOAD_EST_SST_LOG=9,
    TEST_EVENT=10,
    FUNCTION_EVENT=11,
    SOFTWARE_OPTIONS=12,
    UPLOAD_ALARM_LOG = 13,
    NUMBER_OF_EVENT=14          // UPDATE this field is NECESSARY,
                                // whenever the enum is added or deleted.
};

#endif // #ifndef _840_MESSAGES_H
