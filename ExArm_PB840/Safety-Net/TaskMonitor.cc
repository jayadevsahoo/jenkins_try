#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: TaskMonitor - Monitor system tasks and strobe the watchdog.
//---------------------------------------------------------------------
//@ Interface-Description
//  Monitor tasks and strobes the hardware watchdog.  The task table is
//  used to determine whether a task is to be monitored, or not.  This
//  table also indicates whether a monitored task is an event-driven
//  task, or a periodic task.
//
//  Event-driven tasks run at indeterminate times, and therefore must be
//  "pinged" with a message to report back that the task is still alive.
//  They are identified by those tasks that have a non-zero period, and
//  a non-NULL queue ID, in the task table.  Based on the period extracted
//  from the task table, this class will post a message to the monitored,
//  event-driven task every period amount, and expects a response (via its
//  'Report()' method) before that period has passed, again.
//
//  Periodic tasks are identified by those tasks that have a non-zero period,
//  but with a NULL queue ID.  These tasks are expected to run at (around)
//  the period extracted from the table, and, therefore, aren't pinged.  It
//  is the responsibility of these tasks to call this class' 'Report()'
//  method every time it runs.  If a periodic task's period has ended, a
//  second period counter is decremented until either the task responds
//  back, or this second period ends.
//
//  If a task doesn't report back within its time limit, a diagnostic is
//  logged, indicating which task didn't respond, and the board is forced
//  into a reset state.
//---------------------------------------------------------------------
//@ Rationale
//  To protect against "dead" tasks and to strobe the hardware Watchdog.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class uses an internal table to keep track of the execution of
//  the monitored tasks.  When a task's 'faultCounter' is less than one,
//  it means that the task has failed to respond for over twice its
//  expected period.  This is the indication of a failed task.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with pre and
//  post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//  The hardware Watchdog has to be strobed at least every 62.5ms or a
//  CPU reset may occur.
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/TaskMonitor.ccv   25.0.4.0   19 Nov 2013 14:20:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 009  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 008    By: syw  Date: 09-APR-1998  DR Number: 5048
//    Project: Sigma (R8027)
//    Description:
//       Eliminate TIMING_TEST code.
//
//  Revision: 007   By: sah   Date: 09-Sep-1997   DR Number: 2278
//  Project:  Sigma (R8027)
//  Description:
//	Fixed problem of inaccurate period monitoring by re-doing the
//	internal monitoring mechanism.
//
//  Revision: 006   By: sah   Date: 14-Jul-1997   DR Number: 2293
//  Project:  Sigma (R8027)
//  Description:
//	Allow ability to disable (default) or enable task monitoring
//	during DEVELOPMENT.
//
//  Revision: 005   By: sah   Date: 20-Jun-1997   DR Number: 2221
//  Project:  Sigma (R8027)
//  Description:
//	In MonitorTask(), disabling of the 10-second POST timers was
//	modified to implement the 10-second timer test phases of the
//	Service Mode vent inop test.
//
//  Revision: 004   By: sah   Date: 14-Apr-1997   DR Number: 2062
//  Project:  Sigma (R8027)
//  Description:
//	Fixed problem where after 24.8 days of continuous use, the
//	accuracy of the monitoring can be degraded (because both the
//	'EXPECTED_RUNNING_TOTAL' and 'ACTUAL_RUNNING_TOTAL' constants
//	will overflow).
//
//  Revision: 003   By: sah   Date: 16-Apr-1997   DR Number: 1963
//  Project:  Sigma (R8027)
//  Description:
//	Fixed problem with monitoring tighter than the monitor periods.
//
//  Revision: 002   By: sah   Date: 14-Apr-1997   DR Number: 1935
//  Project:  Sigma (R8027)
//  Description:
//	Fixed incorrect calculation of 'EXPECTED_RUNNING_TOTAL' following
//	initial pass through loop.  Also, doubled all monitored times for
//	DEVELOPMENT.
//
//  Revision: 001   By: sah   Date: 26-Mar-1997   DR Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//	Initial version to coding standards (Integration baseline).
//
//=====================================================================

//TODO E600 added only to enable RETAILMSG. Remove if/when not needed
#if defined (_WIN32_WCE) || defined(WIN32)
#include "stdafx.h"	//for "winbase.h" and "tchar.h"
#endif

#include "TaskMonitor.hh"
#include "TaskTable.hh"
//TODO E600 port? #include "Watchdog.hh"
#include "InitiateReboot.hh"
#include "Background.hh"
#include "TaskControlAgent.hh"
#include "Post.hh"

#ifdef SIGMA_BD_CPU
#  include "VentStatus.hh"
#endif // SIGMA_BD_CPU

//@ Usage-Classes
#include "MsgQueue.hh"
#include "TaskMonitorQueueMsg.hh"
#include "TaskControlQueueMsg.hh"
#include "DiagnosticCode.hh"
#include "OsTimeStamp.hh"

#ifdef SIGMA_BD_CPU
#  include "ForceVentInopTest.hh"
#endif // SIGMA_BD_CPU
//@ End-Usage


//@ Code...

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================

MonTaskEntry  TaskMonitor::MonTaskTable_[];
Uint          TaskMonitor::NumTasksMonitored_ = 0;
const Uint    TaskMonitor::MONITOR_CYCLE_TIME_ = 30; // milliseconds...

#if defined(SIGMA_DEVELOPMENT)
Boolean  TaskMonitor::DoNotMonitor = TRUE;
#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [Static]
//
//@ Interface-Description
//  Initialize this class' static data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Load MonTaskTable_ with tasks that will be monitored. This is
//  accomplished by loading only the tasks with non-zero periods.
//  The faultCounter will be initialized to a value that is based
//  off a time out period for non-response.
//  NumTasksMonitored_ is set to the number of monitored tasks.
//---------------------------------------------------------------------
//@ PreCondition
//  (TaskTable::IsInitialized())
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskMonitor::Initialize(void)
{
  CALL_TRACE("TaskMonitor::Initialize()");
  CLASS_PRE_CONDITION(TaskTable::IsInitialized());

#if defined(SIGMA_DEVELOPMENT)  &&  defined(MONITOR)
  cout << endl;
  TaskMonitor::Print();
#endif // defined(SIGMA_DEVELOPMENT)  &&  defined(MONITOR)

  TaskMonitor::NumTasksMonitored_ = 0;

  for (Int32 taskId = 1; taskId <= TaskTable::GetNumEntries(); taskId++)
  {   // $[TI1] -- always takes this path
    const TaskInfo&  R_TASK_INFO = TaskInfo::GetTaskInfo(taskId);

    const Uint  PERIOD = R_TASK_INFO.GetMonitorPeriod();

    if (PERIOD > 0)
    {   //$[TI1.1] -- this task is to be monitored...
      SAFE_AUX_CLASS_ASSERTION((PERIOD >= MONITOR_CYCLE_TIME_), taskId);

      // get a pointer to the next unused entry in the table...
      MonTaskEntry*  pMonTaskEntry =
			(TaskMonitor::MonTaskTable_ + NumTasksMonitored_++);

      pMonTaskEntry->pTaskName       = R_TASK_INFO.GetName();
      pMonTaskEntry->taskId          = R_TASK_INFO.GetIndex();//this is the Task number, not necessarely the actual thread ID
      pMonTaskEntry->queueId         = R_TASK_INFO.GetMonitorQueue();
      pMonTaskEntry->expectedPeriod  = PERIOD;
      pMonTaskEntry->maximumPeriod   = (2 * PERIOD);  // twice the period...
      pMonTaskEntry->activePingState = FALSE;

      new (&(pMonTaskEntry->elapsedTime)) OsTimeStamp();

#if defined(SIGMA_DEVELOPMENT)  &&  defined(MONITOR)
      // print out this new entry...
      TaskMonitor::Print(TaskMonitor::NumTasksMonitored_);
#endif // defined(SIGMA_DEVELOPMENT)  &&  defined(MONITOR)
    }   //$[TI1.2] -- this task is NOT monitored...
  }

#if defined(SIGMA_DEVELOPMENT)  &&  defined(MONITOR)
  cout << "Strobing WD:      ENABLED" << endl;
  cout << "Monitoring Tasks: ENABLED" << endl;
#endif // defined(SIGMA_DEVELOPMENT)  &&  defined(MONITOR)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  MonitorTask
//
//@ Interface-Description
//  The main task loop for the TaskMonitor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First, this task clears the rolling thunder counter, then enters a
//  strobe-and-delay loop, waiting for the system to transition a non-
//  startup state.
//
//  The turning off of the remote alarm on the GUI CPU, and the deactivation
//  of the POST 10-second timer on the BD CPU, is done after transitioning
//  out of the initialization states.
//
//  Broadcast pings to event-based tasks.  Delay for MONITOR_CYCLE_TIME_ to
//  allow messages to be posted to TASK_MONITOR_Q. Retrieve messages off
//  the queue. Call FaultTester to check for faults. No faults, then
//  strobe the watchdog.
//---------------------------------------------------------------------
//@ PreCondition
//  Sys_Init::IsTaskingOn()
//  TaskMonitor::NumTasksMonitored_ > 0
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TaskMonitor::MonitorTask(void)
{
	CLASS_PRE_CONDITION(Sys_Init::IsTaskingOn());
#  if defined(SIGMA_PRODUCTION)
  // must have at least one task being monitored in PRODUCTION mode...
  CLASS_PRE_CONDITION(TaskMonitor::NumTasksMonitored_ > 0);
#  endif // defined(SIGMA_PRODUCTION)

  // Reset rolling thunder count
  Post::ClearRollingThunderCount();

  MsgQueue  taskMonitorQ(TASK_MONITOR_Q);

  // until the system is up-and-running...
  while (TaskControlAgent::GetMyState() == STATE_START  ||
         TaskControlAgent::GetMyState() == STATE_INIT)
  {   // $[TI1] -- ALWAYS takes this path at least once...
#ifdef E600_840_TEMP_REMOVED
    Watchdog::Strobe();
#endif
    taskMonitorQ.flush();  // to prevent the queue from overflowing...
    Task::Delay(0, TaskMonitor::MONITOR_CYCLE_TIME_);
  }

#if defined(SIGMA_GUI_CPU)
  // Turn off remote alarm left on during OS initialization

  // TODO E600 port or remove
  //ClearRemoteAlarm();
#endif // defined(SIGMA_GUI_CPU)

#if defined(SIGMA_BD_CPU)
  // turn off one or both of the 10-second POST timers, depending on
  // force vent inop test state
  ForceVentInopTest::DisablePostTimers();
#endif // defined(SIGMA_BD_CPU)

  OsTimeStamp  cycleTimer;
  Int32        cycleTimeDelta;
  Uint32       numTimesThroughLoop = 0u;

  const Uint32  MAX_TIMES_THROUGH_LOOP =
#if !defined(SIGMA_UNIT_TEST)
	      // maximum number of periods that can fit within 31 bits,
	      // minus some number of times for padding...
	      ((Int32)0x7fffffff / TaskMonitor::MONITOR_CYCLE_TIME_) - 10;
#else  // defined(SIGMA_UNIT_TEST)
					50u; // to test logic easier...
#endif // !defined(SIGMA_UNIT_TEST)

  for(;;)
  {   // $[TI2] -- will always enter this path...
#ifdef E600_840_TEMP_REMOVED
    Watchdog::Strobe();  // $[00307]
#endif

#if defined(SIGMA_DEVELOPMENT)
    // allow monitoring to be turned on or off during DEVELOPMENT...
    if (TaskMonitor::DoNotMonitor)
    {
      taskMonitorQ.flush();
      Task::Delay(0, TaskMonitor::MONITOR_CYCLE_TIME_);
      continue;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    // send out "ping" messages to those event-driven tasks whose time
    // has come...
    TaskMonitor::SendPingMsg_();

    if (numTimesThroughLoop >= MAX_TIMES_THROUGH_LOOP)
    {   // $[TI2.3] -- max. times through loop; reset counter (DCS #2062)...
      numTimesThroughLoop = 0u;
    }   // $[TI2.4] -- NOT maximum times through loop...

    //-------------------------------------------------------------------
    // NOTE:  because of the fact that this task uses a delay between its
    //        cycles, and the timing of this task is important, a timer
    //        is used to adjust the ongoing delay time, based on the "expected"
    //        running time (see below).
    //-------------------------------------------------------------------

    if (numTimesThroughLoop == 0)
    {   // $[TI2.1] -- this is the first time through the loop...
      // set the start of the timer to "now", and set the delta to zero...
      cycleTimer.now();
      cycleTimeDelta = 0;
    }
    else
    {   // $[TI2.2] -- this is NOT the first time through the loop...
      const Int32  EXPECTED_RUNNING_TOTAL =
		    (numTimesThroughLoop * TaskMonitor::MONITOR_CYCLE_TIME_);
      const Int32  ACTUAL_RUNNING_TOTAL = cycleTimer.getPassedTime();

      cycleTimeDelta = EXPECTED_RUNNING_TOTAL - ACTUAL_RUNNING_TOTAL;
    }

    numTimesThroughLoop++;

    // delay to allow other tasks to run...
    Task::Delay(0, (TaskMonitor::MONITOR_CYCLE_TIME_ + cycleTimeDelta));

    // check for responses back from any of the monitored tasks...
    TaskMonitor::RcvRespMsg_();

    // test for failures of any task to respond in time...
    TaskMonitor::FaultTester_();
  }  // end of infinite loop...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Report [Static]
//
//@ Interface-Description
//  Send a response message to the TASK_MONITOR_Q.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskMonitor::Report(void)
{
  CALL_TRACE("TaskMonitor::Report()");

  // TODO E600 MS Put back when task monitor is enabled
  return;
 // const TaskMonitorQueueMsg  responseMsg(TaskMonitorQueueMsg::RESPONSE_MSG_ID);

 // MsgQueue::PutMsg(TASK_MONITOR_Q, responseMsg.getRawMsg());
}   // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method: Print [Static]
//
// Interface-Description
//  Print out MonTaskTable_. Takes an Int32 and returns nothing.
//---------------------------------------------------------------------
// Implementation-Description
//  Use a index to choose which task that will be displayed. Case 0 prints
//  the header.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
TaskMonitor::Print(const Int32 taskId)
{
  if (taskId >= 0)
  {
    cout << TaskMonitor::MonTaskTable_[taskId].pTaskName << "\t"
	 << TaskMonitor::MonTaskTable_[taskId].taskId << "\t"
	 << TaskMonitor::MonTaskTable_[taskId].queueId << "\t"
	 << TaskMonitor::MonTaskTable_[taskId].expectedPeriod << "\t"
	 << TaskMonitor::MonTaskTable_[taskId].maximumPeriod << endl;
  }
  else
  {
    // print the header...
    cout << "Name\t\tTid\tQueue\tExp. Period\tMax. Period" << endl;
  }
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SendPingMsg_()  [static]
//
//@ Interface-Description
//  Pings the monitored tasks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Traverse through MonTaskTable_. If the counter field equals one
//  then check for a queue. If a queue exists then post a ping message
//  to that queue. If the counter is equal to or less than one decrement
//  fault count for that task. For each task entry, decrement its counter.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskMonitor::SendPingMsg_(void)
{
  CALL_TRACE("TaskMonitor::SendPingMsg_(pingMsg)");

  static Boolean  IsFirstTimeThrough_ = TRUE;

  const TaskMonitorQueueMsg  PING_MSG(TaskMonitorQueueMsg::PING_MSG_ID);

  for (Uint entryIdx = 0; entryIdx < NumTasksMonitored_; entryIdx++)
  {   // $[TI1] -- will always enter here...
    // get a pointer to the entry at 'entryIdx'...
    MonTaskEntry*  pMonTaskEntry = (TaskMonitor::MonTaskTable_ + entryIdx);

    if (IsFirstTimeThrough_)
    {   // $[TI1.3] -- very first time through this loop; reset timers...
      pMonTaskEntry->elapsedTime.now();
    }   // $[TI1.4] -- not the first time through this loop...

    if (!pMonTaskEntry->activePingState   &&
      pMonTaskEntry->elapsedTime.getPassedTime() >= pMonTaskEntry->expectedPeriod)
    {   // $[TI1.1] -- enter into an active-ping state...
      pMonTaskEntry->activePingState = TRUE;

      if (pMonTaskEntry->queueId != NULL_ID)
      {   // $[TI1.1.1] -- this is an event-driven task, therefore ping it...
	MsgQueue  monTaskQ(pMonTaskEntry->queueId);

	if (!monTaskQ.isFull())
	{   // $[TI1.1.1.1] -- the task's queue is NOT full, ping it...
	  monTaskQ.putMsg(PING_MSG.getRawMsg());
	}
	else
	{   // $[TI1.1.1.2] -- the task's queue is full, register failure...
#if defined(SIGMA_DEVELOPMENT)  ||  defined(SIGMA_UNIT_TEST)
          // allow restoration of the maximum period during DEVELOPMENT, just
	  // in case somebody's test overrides 'InitiateReboot()' and allows
	  // execution to continue...
	  const Int  SAVED_MAX_PERIOD = pMonTaskEntry->maximumPeriod;
#endif // defined(SIGMA_DEVELOPMENT)  ||  defined(SIGMA_UNIT_TEST)

	  // set the maximum period to zero, to force a failure...
	  pMonTaskEntry->maximumPeriod = 0;

	  // process the fault...
	  TaskMonitor::FaultTester_();

#if defined(SIGMA_DEVELOPMENT)  ||  defined(SIGMA_UNIT_TEST)
	  // restore maximum period and reset timer...
	  pMonTaskEntry->maximumPeriod = SAVED_MAX_PERIOD;
	  pMonTaskEntry->elapsedTime.now();
#endif // defined(SIGMA_DEVELOPMENT)  ||  defined(SIGMA_UNIT_TEST)
	}
      }   // $[TI1.1.2] -- this is a periodic task...
    }   // $[TI1.2] -- this entry's countdown is still in progress...
  }   // end of 'for ()'...

  // clear this static flag...
  IsFirstTimeThrough_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RcvRespMsg_()  [static]
//
//@ Interface-Description
//  Receive any report messages from the monitored tasks that may be on
//  the Task Monitor queue.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Retrieve the message from the queue. Call GetEntryId with the
//  the message data. Use the index that is returned to reset the
//  fault counter and counter of the corresponding task. A bad message
//  causes an assertion.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskMonitor::RcvRespMsg_(void)
{
  CALL_TRACE("RcvRespMsg_()");

  MsgQueue       taskMonitorQ(::TASK_MONITOR_Q);
  MonTaskEntry*  pMonTaskEntry;
  Int32          rawMsg = 0;

  while(taskMonitorQ.acceptMsg(rawMsg) == Ipc::OK)
  {   // $[TI1] -- at least one message waiting...
    const TaskMonitorQueueMsg  responseMsg(rawMsg);

    AUX_CLASS_ASSERTION(
	  (responseMsg.getMsgId() == TaskMonitorQueueMsg::RESPONSE_MSG_ID),
	  responseMsg.getMsgId()
		       );

    // get table entry...
    pMonTaskEntry = GetEntryId_(responseMsg.getRespondingTaskId());

    // reset timer and flag...
    pMonTaskEntry->activePingState = FALSE;
    pMonTaskEntry->elapsedTime.now();
  }   //$[TI2] -- no messages waiting...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FaultTester_ [Static]
//
//@ Interface-Description
//  Check for faults and reset the CPU if any are detected.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Traverse the MonTaskTable_ and check for faultCounter to be less
//  than one.  If a value less than one is detected then reset the
//  board.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
TaskMonitor::FaultTester_(void)
{
  CALL_TRACE("TaskMonitor::FaultTester_()");

  //TODO E600 enable
/*
  for (Uint entryIdx = 0u; entryIdx < NumTasksMonitored_; entryIdx++)
  {   // $[TI1] -- will always enter here...
    // get a pointer the entry at 'entryIdx'...
    MonTaskEntry*  pMonTaskEntry = (TaskMonitor::MonTaskTable_ + entryIdx);

    if (pMonTaskEntry->elapsedTime.getPassedTime() >= pMonTaskEntry->maximumPeriod)
    {   // $[TI1.1] -- a task has failed to respond in its period...
#if defined(SIGMA_PRODUCTION)  ||  defined(SIGMA_UNIT_TEST)
      DiagnosticCode  taskMonitorCode;

      // log a diagnostic indicating a "dead" task...
      taskMonitorCode.setBackgroundTestCode(::MINOR_TEST_FAILURE_ID,
					    (Uint16)::BK_TASK_MONITOR_FAIL,
					    (Uint32)pMonTaskEntry->taskId);
      FaultHandler::LogDiagnosticCode(taskMonitorCode);

      // reboot this board...
      InitiateReboot();
#else  // DEVELOPMENT  &&  !UNIT_TEST
      // print out failure message...

#ifdef E600_840_TEMP_REMOVED
	  cout << "FAILED ";
      TaskMonitor::Print(entryIdx);
#endif

      // reset timer...
      pMonTaskEntry->elapsedTime.now();
#endif // defined(SIGMA_PRODUCTION)
    }   // $[TI1.2] -- a task has no fault condition...
  }   // end of 'for ()'...
*/
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEntryId_ [Static]
//
//@ Interface-Description
//  Get the entry from MonTaskTable_. Takes an task ID and returns a pointer
//  to the corresponding entry in the table.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

MonTaskEntry*
TaskMonitor::GetEntryId_(const IpcId taskId)
{
  CALL_TRACE( "TaskMonitor::GetEntryId_(taskId)" );

  Uint entryIdx;
  for (entryIdx = 0; entryIdx < NumTasksMonitored_; entryIdx++)
  {   // $[TI1] -- will always enter here...
    if (TaskMonitor::MonTaskTable_[entryIdx].taskId == taskId)
    {   // $[TI1.1] -- match found at 'entryIdx'...
      // break out of this loop...
      break;
    }   // $[TI1.2] -- entry at 'entryIdx' NOT the requested task...
  }   // end of 'for ()'...

  // make sure that an entry for 'taskId' was found...
  AUX_CLASS_ASSERTION((entryIdx < TaskMonitor::NumTasksMonitored_), taskId);

  return(TaskMonitor::MonTaskTable_ + entryIdx);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskMonitor::SoftFault(const SoftFaultID  softFaultID,
		       const Uint32       lineNumber,
		       const char*        pFileName,
		       const char*        pPredicate)
{
    FaultHandler::SoftFault(softFaultID, SAFETY_NET, TASKMONITOR,
                            lineNumber, pFileName, pPredicate);
}
