
#ifndef TimeMonitor_HH
#define TimeMonitor_HH

//=====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett Inc of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor Puritan Bennett Inc of California.
//
//            Copyright (c) 1996, Nellcor Puritan Bennett Inc
//=====================================================================

//====================================================================
// Class: TimeMonitor - BD Monitor Data
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/TimeMonitor.hhv   27.0.1.0   20 Nov 2013 17:33:04   rhjimen  $
//
//@ Modification-Log
// 
//  Revision: 001  By: rpr    Date:  6-July-2011    SCR Number: 6765
//  Project:  BI
//  Description:
//      Initial version.  Got rid of BdMonitorClass and kept the 5 ms time check
//      test testFiveMsTimer_.
// 
//=====================================================================

#include "Sigma.hh"

class TimeMonitor 
{
    public:

        TimeMonitor(void);
        ~TimeMonitor(void);

        void newCycle(void);
        void testFiveMsTimer_(void);

        static void SoftFault(const SoftFaultID softFaultID,
                              const Uint32      lineNumber,
                              const char*       pFileName  = NULL, 
                              const char*       pPredicate = NULL);

    protected:

    private:
        TimeMonitor(const TimeMonitor&);        // not implemented...
        void   operator=(const TimeMonitor&);   // not implemented...

        void initialize_(void);

        //@ Data-Member: prevCounterValue_
        // FPGA counter value used to compare against current value every cycle
        Uint32 prevCounterValue_;

        //@ Data-Member: isXena2Config_
        // Used to determine whether Xena II config or not for invoking proper check
        Boolean isXena2Config_;

        //@ Data-Member: isFirstTime_
        // Variable used to perform initial reading of FPGA counter variable.
		Boolean isFirstTime_;

};



#endif // TimeMonitor_HH 



