#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: Post.cc - Phase 2 Power On Self Test
//
//---------------------------------------------------------------------
//@ Interface-Description
//    This class contains the "executive" methods for both Phase 2 and
//    Phase 3 POST.  It also contains accessor methods to POST NOVRAM
//    data and static data.
//---------------------------------------------------------------------
//@ Rationale
//    This provides a common scheduling and error handling features of
//    BD and GUI POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Sigma POST consists of BD and GUI portions managed by a common
//    scheduling interface.  This class provides this using the
//    PostTest structure defined for the GUI and BD post modules.
//    Tests are designed in a heirarchy where dependent hardware is
//    tested prior to being used for devices under test.
//
//---------------------------------------------------------------------
//@ Fault-Handling
//    Faults during POST cause the test in progress to fail.
//---------------------------------------------------------------------
//@ Restrictions
//    Note: CALL_TRACE _cannot_ be used in POST since I/O services are
//    not initialized during POST.  In addition, CALL_TRACE cannot be
//    used in POST methods called from interrupt handlers.  Its use
//    in POST methods will cause very unpredictable results therefore
//    CALL_TRACE is not included in any POST method.
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/Post.ccv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 011    By: Gary Cederquist  Date: 04-JAN-2000  DR Number: 5588
//    Project: 840 NeoMode
//    Description:
//		Checks and corrects system clock before using it.
//
//  Revision: 010    By: Gary Cederquist  Date: 09-Aug-1999  DR Number: 5496
//    Project: 840 ATC
//    Description:
//		Removed pre-condition check for differing compiler versions.
//
//  Revision: 009    By: Gary Cederquist  Date: 05-MAY-1999  DR Number: 5373
//    Project: 840 ATC
//    Description:
//       Deconflicted LONG enumerator by defining a "classed" enumerator
//		 for PostType.
//
//  Revision: 008    By: sah   Date: 14-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007   By: Gary Cederquist   Date: 14-Oct-1998  DR Number: 5105
//    Project:   BiLevel
//    Description:
//       Added ClearStrikes() method to reset the "three strikes" log
//		 for development purposes.
//
//  Revision: 006   By: Gary Cederquist   Date: 08-MAY-1998  DR Number: 5084
//    Project:   Sigma   (R8027)
//    Description:
//       POST Phase 2 nows logs SERVICE_REQUIRED as a MINOR POST error but
//       then stops POST execution as it does for major MAJOR POST errors.
//
//  Revision: 005    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 004    By: Gary Cederquist  Date: 09-JUN-1997  DR Number: 1902
//    Project: Sigma (R8027)
//    Description:
//       Removed POST alert when NOVRAM verification fails following
//       a download (when the FLASH serial number is not set).
//
//  Revision: 003    By: Gary Cederquist  Date: 06-JUN-1997  DR Number: 2183
//    Project: Sigma (R8027)
//    Description:
//       Added logging of sense information in auxiliary results log
//       for MINOR test failures.
//
//  Revision: 002    By: hct              Date: 14-MAY-1997  DR Number: 1996
//    Project: Sigma (R8027)
//    Description:
//       Added SRS requirement number 11027.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================


#include "Post.hh"

//@ Usage-Classes
#include "PostErrors.hh"
#include "CpuDevice.hh"
#include "ServiceSwitch.hh"
#include "PostNovram.hh"
#include "TimeStamp.hh"
//TODO E600 #include "TimeDay.hh"
#include "Phase3.hh"
#include "Post_Library.hh"
#include <string.h>
#include "tables.hh"
#include "BD_IO_Devices.hh"
//@ End-Usage

#ifdef SIGMA_GUI_CPU
#  include "GuiPost.hh"
#  include "GuiIoUtil.hh"
#else
#  include "BdPost.hh"
#  include "BdIoUtil.hh"
#endif

#ifdef SIGMA_DEBUG
#  include "misc.hh"
#endif

#ifdef SIGMA_DEVELOPMENT
#  include <stdio.h>
#  include "Ostream.hh"
#endif

#define P_PROMTOC  ((PromTableOfContents*)BOOT_PROM_TOC)

PostNovram * PPostNovram;

//=====================================================================
//
//      Static Data...
//
//=====================================================================

static Boolean        SysInitComplete_;
static Boolean        PostActive_;
//LL_E600 static Boolean        Initialized_;
static Boolean        Initialized_ = TRUE; //TODO E600 remove TRUE when initialization is ready
//LL_E600 static ShutdownState  StartupState_;
static ShutdownState  StartupState_ = ACSWITCH;
static Post::PostType PostType_;

#ifdef SIGMA_DEVELOPMENT
static Boolean        ServiceModeRequested_;
#endif

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: Executive
//
//@ Interface-Description
//  This is the Phase 2 POST executive routine.  After verifying NOVRAM
//  contents, it schedules tests from PostTestItem structure.  Each
//  test is executed and returns a status.  If the test returns a MAJOR
//  failure, the executive stops testing and transfers control to the
//  MajorPostFailure.  If the test returns a SERVICE_REQUIRED status,
//  the executive logs a POST MINOR and then stops testing by calling
//  MajorPostFailure.  A MINOR failure is simply logged in the
//  POST results log for later retreival by the application.  For each
//  test, the executive displays the test number in the LED just prior
//  to calling the test.
//  $[11069] $[11070] $[11071] $[11072] $[11088] $[11077] $[11078] $[11027]
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    The Phase 1 POST (Kernel) NOVRAM version must be the same as
//    what was used to compile Phase 2 POST:
//     ( PKernelNovram->versionCode == KERNEL_NOVRAM_VERSION );
//
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

void
Post::Executive(void)
{
   PostActive_      = TRUE;       // any faults handled as POST faults

   CLASS_PRE_CONDITION( PKernelNovram->versionCode == KERNEL_NOVRAM_VERSION );

   Criticality testResult;

   Initialized_     = TRUE;
   SysInitComplete_ = FALSE;

   // ensure the clock is set to a valid time before using it
   TimeStamp::Initialize();
   TimeStamp timeNow;

//TODO E600    if ( !timeNow.isBounded() )
   {  // $[TI4.1]
//TODO E600       TimeOfDay setTime = TimeStamp::GetBeginEpoch().getTimeOfDay();
//TODO E600 port      SetTimeOfDay( &setTime );
   }  // $[TI4.2]

   //  NOVRAM test must be called prior to any functions that modify NOVRAM
   SetPostStep( POST_ERROR_POST_NOVRAM );
   testResult = Post::TestNovram_();

UNIT_TEST_POINT(POSTUT_34_1)
   SetTestResult( testResult );

   //  once NOVRAM checked/initialized, we can set the power up time
   Post::GetPowerUpTime();

#if defined ( SIGMA_DEBUG )
   InitializeSerialPorts();
   dprintf("PostExecutive Started\n");
#endif

   //  release LAN reset -
   //  done here since short POST does not run Ethernet test
#ifdef E600_840_TEMP_REMOVED
   CpuDevice::SetBitsIoReg1( LAN_RESET_BIT );
#endif

#ifdef SIGMA_GUI_CPU
   const PostTestItem * pTestItem = GuiPost::GetTestItems();
#endif

#ifdef SIGMA_BD_CPU
   const PostTestItem * pTestItem = BdPost::GetTestItems();
UNIT_TEST_POINT(POSTUT_38_2)
#endif

   // Turn on panel lights
   SwitchLeds( TRUE );

UNIT_TEST_POINT(POSTUT_41_1)

   while (pTestItem->pTestProcedure != NULL)
   {
      // $[TI1.1]
      testResult = PASSED;

      SetPostStep(pTestItem->testNumber);

#if defined ( SIGMA_DEBUG )
      dprintf("Start of test: %s\n", pTestItem->testName);
#endif

      // Execute the POST test
      testResult = pTestItem->pTestProcedure();


UNIT_TEST_POINT(POSTUT_TEST_RETURN)

#if defined ( SIGMA_DEBUG )
      if (pTestItem->testNumber == POST_ERROR_DRAM_MEMORY)
         InitializeSerialPorts();    // must redo after memory test
#endif

      SetTestResult(testResult);

      //  declare POST error for major failures - stops POST
      if (testResult == MAJOR || testResult == SERVICE_REQUIRED)
      {
          // $[TI2.1]
          MajorPostFailure();
      }
          // $[TI2.2]

      if ( testResult != PASSED )
      {
          // $[TI3.1]
#ifdef E600_840_TEMP_REMOVED
          AuxResult  auxResult(  PKernelNovram->lastTestInProgress
                               , 0
                               , *NMI_REGISTER
                               , PKernelNovram->errorCode
                               , (Byte*)GetSense() );

          AuxResultLog::GetPostAuxResultLog().add(auxResult);
#endif
	  }
          // $[TI3.2]

      pTestItem++;
   }
   // $[TI1.2]

   // Turn off panel lights
   SwitchLeds( FALSE );

UNIT_TEST_POINT(POSTUT_41_2)

   Initialized_     = FALSE;
   // Return to PROM POST for OS BOOT
}


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: ClearRollingThunderCount
//
//@ Interface-Description
//  Clears the "Rolling Thunder" counter.  This count tracks how many
//  times POST is entered without successfully transferring to the
//  Sigma application.  This method should be called by the
//  Task Monitor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
Post::ClearRollingThunderCount(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    PKernelNovram->rollingThunderCount = 0;
    // $[TI1]
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: ClearResultsLog
//
//@ Interface-Description
//  Clears the POST results log.  Called by Persistent-Objects subsystem
//  after it transfers the results to the diagnostic NOVRAM log.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
Post::ClearResultsLog(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    for (int i = 0; i<countof(PKernelNovram->results); i++)
    {
        PKernelNovram->results[i] = PASSED;
    }
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [public, static] - DEVELOPMENT ONLY
//
//@ Interface-Description
//  This method is called during subsystem initialization.  It is
//  used during development to initialize POST data to default values
//  for testing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::Initialize(void)
{
#ifdef SIGMA_DEVELOPMENT

    CLASS_PRE_CONDITION( PKernelNovram->versionCode == KERNEL_NOVRAM_VERSION );

    Initialized_ = TRUE;

    CLASS_PRE_CONDITION(   (char*)PKernelNovram + sizeof(KernelNovram)
                        <= (char*)PPostNovram);

    Post::SetPostType_( Post::POST_LONG );
    Post::SetStartupState_( ACSWITCH );
    Post::SetShutdownState_( UNKNOWN );
    Post::SetBackgroundFailed( PASSED );
    Post::SetBdFailed( FALSE );
    ServiceModeRequested_ = FALSE;
    Post::SetPostFailed( FALSE );

    PPostNovram->versionCode      = POST_NOVRAM_VERSION_CODE;

#ifdef SIGMA_BD_CPU
    Post::SetPowerDownTime_();
    Post::GetDowntimeStatus();

    //  during development - push service button prior to "go"
    //  to enter service mode
    Uint8  * const SAFETY_NET_STATUS_REG   = (Uint8 *)(0xFFBEB00C);

    ServiceModeRequested_ = (Boolean)(*SAFETY_NET_STATUS_REG & 0x01);
#endif // SIGMA_BD_CPU

#endif // SIGMA_DEVELOPMENT

    // $[TI1]
}




//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: Phase3Exec
//
//@ Interface-Description
//
//  INPUTS:  NONE
//  OUTPUTS: NONE
//  POST phase 3 executive.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  This scheduling routine operates on a structure containing
//  testing information.  Each test is called from this structure,
//  PostExecutive is responsible for lighting the diagnostic LED
//  indicator, calling the appropriate test and reporting the results.
//---------------------------------------------------------------------
//@ PreCondition
//   ( PKernelNovram->versionCode == KERNEL_NOVRAM_VERSION );
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
void
Post::Phase3Exec(void)
{
   CLASS_PRE_CONDITION( PKernelNovram->versionCode == KERNEL_NOVRAM_VERSION );
   Initialized_ = TRUE;

   Criticality testResult;

   const PostTestItem * pTestItem = Phase3::GetTestItems();

   while (pTestItem->pTestProcedure != NULL)
   {
      // $[TI1.1]
      SetPostStep(pTestItem->testNumber);

      testResult = pTestItem->pTestProcedure();

UNIT_TEST_POINT(POSTUT_P3TEST_RETURN)

      SetTestResult(testResult);

      //  declare POST error for major failures - stops POST
      if (testResult == MAJOR || testResult == SERVICE_REQUIRED)
      {
         // $[TI2.1]
         MajorPostFailure();
      }
         // $[TI2.2]

      pTestItem++;
   }
      // $[TI1.2]

   SetPostStep(POST_ERROR_APPLICATION_BOOT);
   PostActive_  = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPostType [public, static]
//
//@ Interface-Description
//    This method returns the PostType that was executed during last
//    cycle of POST.  PostType can be either POST_LONG or POST_SHORT.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//    Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Post::PostType
Post::GetPostType(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    return PostType_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetStartupState [public, static]
//
//@ Interface-Description
//    This method returns the processor startup state that indicates
//    the reason POST was entered.  The reset umpire sets this state
//    to the previous shutdown state during its test.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ShutdownState
Post::GetStartupState(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    return StartupState_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetShutdownState_ [private, static]
//
//@ Interface-Description
//    This method returns the current shutdown state for the processor.
//    POST uses this state to register strikes in the reset umpire
//    test.  The reset umpire resets this state to UNKNOWN after
//    processing the previous shutdown state.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//    Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ShutdownState
Post::GetShutdownState_(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    return ShutdownState(PKernelNovram->shutdownState);
}

#ifdef SIGMA_BD_CPU
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDowntimeStatus [public, static]
//
//@ Interface-Description
//  This static function provides public access to the downtime
//  status.  It uses function static to initialize its local static
//  which is returned to the caller.  It calls the private
//  GetDowntimeStatus_ the first time to initialize the local static.
//  On subsequent calls, it simply returns the value of the local
//  static variable.  This essentially caches the status so subsequent
//  calls do not incurr the overhead of computing the downtime status
//  every time.  The downtime status returned indicates whether the
//  power has been off longer or shorter than 5 minutes.  This method
//  is defined only for the BD since the GUI does not have a power
//  fail interrupt as does the BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DowntimeStatus
Post::GetDowntimeStatus(void)
{
    CLASS_PRE_CONDITION(Initialized_);

    static const DowntimeStatus status = GetDowntimeStatus_();

UNIT_TEST_POINT(POSTUT_38_1)
UNIT_TEST_POINT(POSTUT_47_1)
    // $[TI1]
    return status;
}
#endif // SIGMA_BD_CPU


#ifdef SIGMA_BD_CPU
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPowerDownTime [private, static]
//
//@ Interface-Description
//  Returns a TimeStamp reference for the power down time stored in
//  NOVRAM by the power fail interrupt.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const TimeStamp &
Post::GetPowerDownTime(void)
{
 //TODO E600 enable when ready
	//CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    //return PPostNovram->powerDownTime;

	//TODO E600 temp, remove and enable above
	TimeStamp val;
	return val;
}
#endif // SIGMA_BD_CPU

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPowerUpTime [private, static]
//
//@ Interface-Description
//  This method returns a reference to the local static TimeStamp
//  for when power was recovered.  The first time called at the
//  beginning of Phase 2 POST, the local static variable is initialized
//  to the current time.  On subsequent calls, it only returns a
//  reference to the local TimeStamp.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const TimeStamp &
Post::GetPowerUpTime(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    static TimeStamp powerUpTime;

    // $[TI1]
    return powerUpTime;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetPostFailed [public, static]
//
//@ Interface-Description
//  Sets the POST failure flag stored in NOVRAM.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::SetPostFailed(Boolean val)
{
    CLASS_PRE_CONDITION(Initialized_);
    PKernelNovram->postFailed = val;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsPostFailed [public, static]
//
//@ Interface-Description
//  Returns the POST failure flag stored in NOVRAM.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
Post::IsPostFailed(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    return PKernelNovram->postFailed;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetBackgroundFailed [public, static]
//
//@ Interface-Description
//  Sets persistent state of background failures for the processor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::SetBackgroundFailed(Criticality status)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    PPostNovram->backgroundFailed = status;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBackgroundFailed [public, static]
//
//@ Interface-Description
//  Returns state of background failures for the processor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Criticality
Post::GetBackgroundFailed(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
	
	// TODO E600 MS This is a hack. Remove when background tests are ported
	// and uncomment return PPostNovram->backgroundFailed;
	return PASSED;
    //return PPostNovram->backgroundFailed;
}

#ifdef SIGMA_BD_CPU
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsServiceModeRequested [public, static]
//
//@ Interface-Description
//  Returns TRUE if the operator pressed the service switch during
//  long POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the ServiceSwitch class which encapsulates all information
//  regarding service requests.
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
Post::IsServiceModeRequested(void)
{
    CLASS_PRE_CONDITION(Initialized_);

    return   ServiceSwitch::IsServiceModeRequested();
}

#endif // SIGMA_BD_CPU

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsSysInitComplete [public, static]
//
//@ Interface-Description
//  Returns the current state of system initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
Post::IsSysInitComplete(void)
{
    // $[TI1]
    return   SysInitComplete_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetSysInitComplete [public, static]
//
//@ Interface-Description
//  Sets the current state of system initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::SetSysInitComplete(Boolean val)
{
    // $[TI1]
    SysInitComplete_ = val;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetDownloadScheduled [public, static]
//
//@ Interface-Description
//  Sets the download request flag.  If set TRUE, Kernel POST will
//  start the download process on the next cycle through POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Post::SetDownloadScheduled(Boolean val)
{
    CLASS_PRE_CONDITION(Initialized_);
    if (val)
    {
        // $[TI1.1]
        PKernelNovram->downloadScheduled = SCHEDULE_DOWNLOAD;
    }
    else
    {
        // $[TI1.2]
        PKernelNovram->downloadScheduled = 0;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsDownloadScheduled [public, static]
//
//@ Interface-Description
//  Returns TRUE if the download request flag is requesting download of
//  new application code, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
Post::IsDownloadScheduled(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    if ( PKernelNovram->downloadScheduled == SCHEDULE_DOWNLOAD )
    {
        // $[TI1.1]
        return TRUE;
    }
    else
    {
        // $[TI1.2]
        return FALSE;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetOperationalTime [public, static]
//
//@ Interface-Description
//  Sets the operational time stored in persistent memory to the hours
//  specified.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::SetOperationalTime(Uint32 hours)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    PPostNovram->operationalTime = hours;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetOperationalTime [public, static]
//
//@ Interface-Description
//  Returns the operational hours stored in persistent memory.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Uint32
Post::GetOperationalTime(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    return PPostNovram->operationalTime;
}

#ifdef SIGMA_BD_CPU
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetAcLineVoltage [public, static]
//
//@ Interface-Description
//  This method sets the AC line voltage stored in persistent memory
//  to the specified setting.  It stores the enumerated setting along
//  with a "magic cookie" so a zero enumerator is not treated as a
//  valid setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::SetAcLineVoltage(NominalLineVoltValue::NominalLineVoltValueId setting)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    PPostNovram->acLineVoltage = NLV_COOKIE | setting;
}
#endif // SIGMA_BD_CPU

#ifdef SIGMA_BD_CPU
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetAcLineVoltage [public, static]
//
//@ Interface-Description
//  Returns the enumerated value for the current AC line voltage
//  setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NominalLineVoltValue::NominalLineVoltValueId
Post::GetAcLineVoltage(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    return NominalLineVoltValue::NominalLineVoltValueId(
                    PPostNovram->acLineVoltage & ~NLV_COOKIE );
}
#endif // SIGMA_BD_CPU



//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: GetKernelPartNumber
//
//@ Interface-Description
//  Returns pointer to null terminated string in BOOT EPROM containing
//  the kernel part number.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

const char *
Post::GetKernelPartNumber(void)
{
    // $[TI1]
#ifdef E600_840_TEMP_REMOVED
    return  P_PROMTOC->pKernelPartNumber;
#endif
	//return 0;  //TODO E600 - hack-8 - commented - avoid to return 0 - as it crashes.
	return "KrNoPB880"; //TODO E600 - hack-8 - added - Dummy kernelPartNumber_ is added.
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: IsPostActive
//
//@ Interface-Description
//  Returns TRUE if POST is currently running, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

Boolean
Post::IsPostActive(void)
{
    // $[TI1]
    return  PostActive_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetBdFailed [public, static]
//
//@ Interface-Description
//  Sets latched state for TUV Monitor declared failure of the BD unit.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::SetBdFailed(Boolean val)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]
    PPostNovram->bdFailed = val;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsBdFailed [public, static]
//
//@ Interface-Description
//  Returns latched state for TUV Monitor declared failure of the BD unit.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
Post::IsBdFailed(void)
{
    CLASS_PRE_CONDITION(Initialized_);
    // $[TI1]

	// TODO E600 MS Hacked this to return FALSE for now.
	return FALSE;
//    return PPostNovram->bdFailed;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsNovramInitialized [public, static]
//
//@ Interface-Description
//  Returns the state of NOVRAM initialization.  If a download has
//  reset the serial number (in FLASH) to X's then the NOVRAM state is
//  defined as uninitialized as well.  This method is called when there
//  is a NOVRAM verification failure to determine if the cause of the
//  failure is a just completed download operation.  This is indicated
//  by X's in the processor's serial number located in FLASH.  Until
//  the serial number is set, NOVRAM verification failures can be
//  ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
Post::IsNovramInitialized(void)
{
    static const Uint32 SERIAL_NO_INIT_PATTERN = 0x55555555;  // U's
    // $[TI1.1]  $[TI1.2]
    return ( *(Uint32*)FLASH_SERIAL_NO_ADDR != SERIAL_NO_INIT_PATTERN );
}

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: InitializeNovram_
//
//@ Interface-Description
//  Initializes Post NOVRAM to default values.  Called by the NOVRAM
//  test when the version number in NOVRAM doesn't match the version used
//  to compile this module.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
Post::InitializeNovram_(void)
{
    CLASS_PRE_CONDITION(Initialized_);

    PostNovram * pPN = PPostNovram;  // so compiler can optimize to register

    pPN->versionCode             = POST_NOVRAM_VERSION_CODE;
    // initialize time of power down to the millenium to initiate long POST
    new (&pPN->powerDownTime) TimeStamp(1995,1,1,0,0,0);
    pPN->backgroundFailed        = PASSED;
    pPN->operationalTime         = 0;

    // set nominal voltage invalid so AC test faults prior to setting
    // the correct nominal voltage
    pPN->acLineVoltage
        = NLV_COOKIE | NominalLineVoltValue::TOTAL_NOMINAL_VOLT_VALUES;

    for (Uint i = 0; i < UMPIRE_QUEUE_DEPTH; i++)
    {
        pPN->strikeHour[i] = 0xffffffff;
    }
    pPN->strikeIndex = 0;
    // $[TI1]
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: TestNovram_
//
//@ Interface-Description
//  This function verifies the integrity of the NOVRAM data used by
//  POST Phase 2 processing.  This includes verifying the Power Down
//  Information (shutdown state and time/date) generated during a
//  power failure.  It sets a default value and declares a MINOR
//  failure if it detects a value out of range.
//  $[11024] $[11025]
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Criticality
Post::TestNovram_(void)
{
    CLASS_PRE_CONDITION(Initialized_);

    Criticality  status = PASSED;
    PostNovram * pPN = PPostNovram;  // so compiler can optimize to register

UNIT_TEST_POINT(POSTUT_34)

    if ( pPN->versionCode != POST_NOVRAM_VERSION_CODE )
    {
        // $[TI1.1]
        InitializeNovram_();
        if ( Post::IsNovramInitialized() )
        {
            // $[TI2.1]
            return MINOR;
        }
        else
        {
            // $[TI2.2]
            //  clear the Kernel NOVRAM verification failure as well
            //  doing this here obviates another PROM revision
            PKernelNovram->results[POST_ERROR_NOVRAM] = PASSED;
            return PASSED;
        }
    }
        // $[TI1.2]

//TODO E600     if (!pPN->powerDownTime.isBounded())  // $[11024] $[11025]
    {
        // $[TI3.1]
        // reset time for a short powerfail condition
//TODO E600         new (&pPN->powerDownTime) TimeStamp;
//TODO E600         status = MINOR;
    }
        // $[TI3.2]

    if (   pPN->backgroundFailed != PASSED
        && pPN->backgroundFailed != MINOR
        && pPN->backgroundFailed != SERVICE_REQUIRED
        && pPN->backgroundFailed != MAJOR
       )
    {
        // $[TI4.1]
        pPN->backgroundFailed = PASSED;
        status = MINOR;
    }
        // $[TI4.2]

    if (pPN->strikeIndex >= UMPIRE_QUEUE_DEPTH)
    {
        // $[TI5.1]
        for (Uint i = 0; i < UMPIRE_QUEUE_DEPTH; i++)
        {
            pPN->strikeHour[i] = 0xffffffff;
        }
        pPN->strikeIndex = 0;
        status = MINOR;
    }
        // $[TI5.2]

    switch ( pPN->acLineVoltage )
    {
       case (NLV_COOKIE | NominalLineVoltValue::VAC_100):
       case (NLV_COOKIE | NominalLineVoltValue::VAC_120):
       case (NLV_COOKIE | NominalLineVoltValue::VAC_220):
       case (NLV_COOKIE | NominalLineVoltValue::VAC_230):
       case (NLV_COOKIE | NominalLineVoltValue::VAC_240):
       case (NLV_COOKIE | NominalLineVoltValue::TOTAL_NOMINAL_VOLT_VALUES):
           // $[TI6.1]
           break;
       default:
           // $[TI6.2]
           pPN->acLineVoltage
               = (NLV_COOKIE | NominalLineVoltValue::TOTAL_NOMINAL_VOLT_VALUES);
           status = MINOR;
    }

    return status;
}

#ifdef SIGMA_BD_CPU
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetPowerDownTime [private, static]
//
//@ Interface-Description
//  The power fail interrupt calls this method to instantiate
//  a TimeStamp for the current time into NOVRAM.  Defined for BD only.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::SetPowerDownTime_(void)
{
    CLASS_PRE_CONDITION(Initialized_);

    new (&PPostNovram->powerDownTime) TimeStamp;
    // $[TI1]
}
#endif // SIGMA_BD_CPU

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetStartupState_ [protected, static]
//
//@ Interface-Description
//  Sets the local StartupState_ to the specified state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::SetStartupState_(ShutdownState state)
{
    CLASS_PRE_CONDITION(Initialized_);
    StartupState_ = state;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetShutdownState_ [protected, static]
//
//@ Interface-Description
//  Sets the persistent shutdown state to the specified state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::SetShutdownState_(ShutdownState state)
{
    CLASS_PRE_CONDITION(Initialized_);
    PKernelNovram->shutdownState = (Uint8)state;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetPostType_ [private, static]
//
//@ Interface-Description
//  Sets the local post type to the type specified.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::SetPostType_(Post::PostType postType)
{
    CLASS_PRE_CONDITION(Initialized_);
    PostType_ = postType;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ClearStrikesLog [static]
//
//@ Interface-Description
//  Resets the "Three Strikes" log to a no strikes condition. Used for
//  development purposes only. Invoked when the developer touches the
//  "Override EST" button on the development options sub-screen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Post::ClearStrikesLog(void)
{
    PostNovram * pPN = PPostNovram;  // so compiler can optimize to register

    for (Uint i = 0; i < UMPIRE_QUEUE_DEPTH; i++)
    {
        pPN->strikeHour[i] = 0xffffffff;
    }
    pPN->strikeIndex = 0;
}

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

#ifdef SIGMA_BD_CPU
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDowntimeStatus_ [private, static]
//
//@ Interface-Description
//  Calculates the downtime status based on the time power was lost
//  and the time it was restored.  An invalid time for either forces
//  a DOWN_LONG return.  This method is private since the public
//  GetDowntimeStatus caches the state for faster access. This
//  method is defined for the BD only.  $[11026]
//---------------------------------------------------------------------
//@ Implementation-Description
//  See interface description.
//---------------------------------------------------------------------
//@ PreCondition
//  Initialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DowntimeStatus
Post::GetDowntimeStatus_(void)
{
    CLASS_PRE_CONDITION(Initialized_);

    static const Int32 SHORT_POWERFAIL_MS = 5*60*1000;
    const TimeStamp & timeDown = Post::GetPowerDownTime();
    const TimeStamp & timeUp   = Post::GetPowerUpTime();
    const Int32 timeElapsed    = timeUp - timeDown;
    DowntimeStatus status      = DOWN_LONG;

UNIT_TEST_POINT(POSTUT_47)
//TODO E600     if ( !timeDown.isBounded() || !timeUp.isBounded() )
    {
        // $[TI1.1]
//TODO E600         status = DOWN_LONG;
    }
//TODO E600     else
    {
        // $[TI1.2]
        if ( timeElapsed >= 0 && timeElapsed < SHORT_POWERFAIL_MS )
        {
            // $[TI2.1]
            status = DOWN_SHORT;
        }
        else
        {
            // $[TI2.2]
            status = DOWN_LONG;
        }
    }
    return status;
}
#endif // SIGMA_BD_CPU


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
Post::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  FaultHandler::SoftFault(softFaultID, POST,
                          Post::ID_POST,
                          lineNumber, pFileName, pPredicate);
}
