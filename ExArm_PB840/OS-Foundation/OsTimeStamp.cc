#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OsTimeStamp - A time marker based off the VRTX32 clock.
//---------------------------------------------------------------------
//@ Interface-Description
//  A time stamp that is used for interval timing. The time is from the
//  VRTX32 clock. The return value is in VRTX32 tick`s.
//  The VRTX32 clock is initialized at system power on. The clock starts
//  after a VRTX_GO has been issued.
//---------------------------------------------------------------------
//@ Rationale
//  Satisfied a need for a clock that was independent of the time of day 
//  clock. This resolves the problem of the time of day clock being changed
//  and providing incorrect time for comparisons.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the VRTX32 real time clock. The VRTX32 system call sc_gtime()
//  returns the clock's time in VRTX32 ticks.
//
//  Note: Comparison operators are not guaranteed to account for a 
//  clock rollover. The user should use diffTime or getPassedTime to
//  insure that if a rollover occurs it is taken in to account.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with pre and
//  post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/OsTimeStamp.ccv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//       Per rework instructions, added logic in differencing operators
//       to return MAX_UINT when difference exceeds 49 days instead of 
//       allowing arithmetic overflow.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
#include "Sigma.hh"
#include "OsTimeStamp.hh"
#include <limits.h>


#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT

//@ Usage-Classes
//@ End-Usage

static const Uint WRAP_AROUND = 0xffffffff;
static const Uint MAX_TICKS = UINT_MAX / MSEC_PER_TICK;

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OsTimeStamp  [Default Constructor]
//
//@ Interface-Description
//  Initializes osTimeStamp to current time. If the time returns zero then
//  osTimeStamp will be assigned one. Zero is invalid.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the public method now().
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//  isValid() == TRUE
//@ End-Method
//=====================================================================

OsTimeStamp::OsTimeStamp(void)
{
  CALL_TRACE("OsTimeStamp::OsTimeStamp(void)");

  now();  // $[TI1]

  CLASS_POST_CONDITION( isValid(), OsTimeStamp );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OsTimeStamp  [Copy Constructor]
//
//@ Interface-Description
//  Copies a OsTimeStamp object to another created OsTimeStamp object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
OsTimeStamp::OsTimeStamp(const OsTimeStamp& rTime)
{
        CALL_TRACE("OsTimeStamp::OsTimeStamp(rTime)");
 
        *this = rTime;		// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OsTimeStamp  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OsTimeStamp::~OsTimeStamp(void)
{
	CALL_TRACE("OsTimeStamp::~OsTimeStamp(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: now
//
//@ Interface-Description
//  Gets and stores the current OS tick count. If the current tick is
//  zero then set the osStamp_ to one.
//---------------------------------------------------------------------
//@ Implementation-Description
//  retrieve the OS clock tick count.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
OsTimeStamp::now(void)
{
	SAFE_CLASS_PRE_CONDITION( Task::IsTaskingOn() );
#if ( defined(_WIN32_WCE) || defined(WIN32) )	//ported to WinCE oe WIN32 PC app
    osStamp_ = (Uint32) GetTickCount();
	if(FALSE == isValid())
	{
		osStamp_ = 1;
	}
#else
	osStamp_ = 1;
#endif
    							// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CurrentTime [public, static]
//
//@ Interface-Description
//  Gets and stores the current VRTX32 tick count. Returns an 
//  OsTimeStamp object.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
OsTimeStamp
OsTimeStamp::CurrentTime(void)
{
  OsTimeStamp  time;

  return  time;		// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: diffTime
//
//@ Interface-Description
//  This private method returns the difference (in milliseconds) 
//  between this OsTimeStamp and the time value parameter.  It is 
//  called by the public methods diffTime and getPassedTime to calculate
//  the elapsed time.  The difference between this OsTimeStamp and the
//  time stamp parameter is calcualted assuming the time stamp parameter
//  is the most recent time.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the VRTX32 clock has wrapped around, the correct time difference will
//  be returned. The WRAP_AROUND value is 0xffffffff. The VRTX32 clock will
//  wrap around every 49.7 days. One is added to the return value when
//  a wrap around occurs to account for zero.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
Uint32
OsTimeStamp::diffTime_(const Uint32 compareStamp) const
{
    Uint32  ticks;
 
    if ( compareStamp >= osStamp_ )
    {                                                          // $[TI1]
        ticks = compareStamp - osStamp_;
    }
    else
    {                                                          // $[TI2]
        ticks = ( WRAP_AROUND - osStamp_ ) + compareStamp + 1;
    }
 
    //  if elapsed ticks exceeds 49.7 days then return UINT_MAX instead
    //  of executing the multiply operation that will cause an overflow
    //  and return an unpredictable result
    if ( ticks > MAX_TICKS )
    {                                                          // $[TI3]
        return UINT_MAX;
    }                                                          // $[TI4]
 
    return ( ticks * MSEC_PER_TICK );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: diffTime
//
//@ Interface-Description
//  Takes a OsTimeStamp reference which is the most recent time.
//  This method will determine the elapsed time. Returns
//  a Uint32 for time difference. The difference will be in milliseconds.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the private diffTime_ with the time value from the OsTimeStamp
//  parameter.
//---------------------------------------------------------------------
//@ PreCondition
//  ( isValid() && rTime.isValid() )
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Uint32
OsTimeStamp::diffTime(const OsTimeStamp& rTime) const
{
    CLASS_PRE_CONDITION( isValid() && rTime.isValid() );

    return  diffTime_( rTime.osStamp_ );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPassedTime
//
//@ Interface-Description
//  Returns the time difference between this osStamp_ and the
//  the current VRTX32 time in milliseconds.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The VRTX32 tick is updated every 10 milliseconds.
//  On Unix, time of the day clock is updated every second.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Uint32
OsTimeStamp::getPassedTime() const
{
#if ( defined(_WIN32_WCE) || defined(WIN32) )	//ported to WinCE oe WIN32 PC app
	Uint32 currentTick = (Uint32) GetTickCount();
    return  diffTime_( currentTick );
#else
	return 0;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: invalidate
//
//@ Interface-Description
//  Invalidates OsTimeStamp object. Returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets osStamp_ to zero. Zero is used as an invalid value because at
//  system startup the VRTX32 clock is initialized to zero. The clock
//  is not started until a VRTX_GO is issued. Zero will also be encountered
//  if the clock wraps around.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//  ( ! isValid() )
//@ End-Method
//=====================================================================
 
void
OsTimeStamp::invalidate(void)
{
  osStamp_ = 0; 
  CLASS_POST_CONDITION( !isValid(), OsTimeStamp );
  // $[TI1]
}

#ifdef	SIGMA_DEVELOPMENT
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Print
//
//@ Interface-Description
//  Prints osStamp_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
OsTimeStamp::print(void) const
{
  cout << (Uint32) osStamp_ << " Ticks " << endl;
}

#endif	// SIGMA_DEVELOPMENT


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
OsTimeStamp::SoftFault(const SoftFaultID  softFaultID,
                                     const Uint32       lineNumber,
                                     const char*        pFileName,
                                     const char*        pPredicate)
{
  CALL_TRACE("TimeStamp::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::OSTIMESTAMP,
                                       lineNumber, pFileName, pPredicate);
}
