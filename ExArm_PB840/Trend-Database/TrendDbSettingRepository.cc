#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbSettingRepository
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is the central interface to store and retrieve settings data.
//  On the client side, the storage and retrieval are processed through the 
//  TrendDataMgr. 
//  On the data store side, this class interfaces with the ContextSubject
//  class in the Settings-Validation subsystem (to sample updated settings
//  data), and the TrendDataMgr. 
//  The TrendDataMgr enables an interface with the TrendDbTable settings
//  table, and TrendDbApplicability (to check the validity of each trended
//  setting parameter within the current settings context).
// 
//---------------------------------------------------------------------
//@ Rationale
//  To provide a layer of abstraction for the storage and retrieval of 
//  trended settings.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The storage scenario executes every 10 seconds when the Task() method
//  in TrendDataMgr calls sample(). Sample() iterates through all of the
//  trended settings and resamples the settings that have been modified.
//  If any of the settings have been modified, then those settings are
//  checked for validity (determined through the isApplicable() interface
//  to the TrendDbApplicability class) and all of the current settings are
//  appended to the settings table in the database. Updates to the table
//  trigger updates to the table index, which is used to determine which
//  settings changes correspond to which patient data records.
// 
//  The retrieval scenario executes every 60 seconds, or more often if
//  requested by the GUI, with a call to processRequest(). This function
//  interprets the request by the client to determine parameters requested,
//  starting point, timescale, etc. loadRecordData_() then interfaces with 
//  the compact flash in a loop, retrieving the requested settings records,
//  and parsing them to extract the requested settings, until the request
//  is fulfilled. The TrendDataSet is then returned to the client.
// 
//  The records are retrieved using the FindLocation() helper function that
//  looks up the requested sequence number in the table index to determine 
//  the location of the settings record in the database. The index is 
//  parsed from newest to oldest settings changes.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSettingRepository.ccv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: rhj   Date: 25-Oct-2010     SCR Number: 6603
//  Project:  PROX
//  Description:
//      Added the ability to log prox enable and disable events.
// 
//  Revision: 002   By: rpr    Date: 22-Jan-2009    SCR Number: 6435  
//  Project:  840S
//  Description:
//      Modified for Leak Compensation.
// 
//  Revision: 001  By:  ksg    Date:  5-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
//
//=====================================================================

#include "TimeStamp.hh"             // OS-Foundation
#include "SettingId.hh"             // Setting-Validation
#include "ContextSubject.hh"        // Setting-Validation
#include "TrendDbSettingRepository.hh"
#include "TrendDbPatientRepository.hh"
#include "TrendDbApplicability.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSettingRepository [constructor]
//
//@ Interface-Description
//  This function constructs the database for the settings data. Each 
//  parameter gets assigned a configuration slot in the database. Each 
//  parameter gets a last-sample collection object, which will be stored
//  in the TrendDbRecord, and associated to a SettingId
// 
//  Since the Settings TrendDbTable stores changes only, the changes are
//  stored at a variable rate of up to 6 times a minute at 10 second
//  intervals. All the settings are stored whenever any one of them is
//  modified. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Allocates the memory. The initialization of the sample data objects
//  and table is deferred to the initialize() method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSettingRepository::TrendDbSettingRepository( void ) :   
	ContextObserver(),
	// Sample Data objects, hold the incoming values
	setExpSensSample_( SettingId::EXP_SENS,
					   TrendSelectValue::TREND_SET_EXPIRATORY_SENSITIVITY ),
	setRespRateSample_( SettingId::RESP_RATE,
						TrendSelectValue::TREND_SET_RESPIRATORY_RATE ),
	setPeakInspFlowSample_( SettingId::PEAK_INSP_FLOW,
							TrendSelectValue::TREND_SET_PEAK_INSPIRATORY_FLOW ),
	setIeRatioSample_( SettingId::IE_RATIO,
					   TrendSelectValue::TREND_SET_I_E_RATIO ),
	setO2PercentSample_( SettingId::OXYGEN_PERCENT,
						 TrendSelectValue::TREND_SET_OXYGEN_PERCENTAGE ),
	setTcSupportSample_( SettingId::PERCENT_SUPPORT,
						 TrendSelectValue::TREND_SET_PERCENT_SUPPORT ),
	setPeepSample_( SettingId::PEEP,
					TrendSelectValue::TREND_SET_PEEP ),
	setPeepHighSample_( SettingId::PEEP_HIGH,
						TrendSelectValue::TREND_SET_PEEP_HIGH ),
	setPeepLowSample_( SettingId::PEEP_LOW,
					   TrendSelectValue::TREND_SET_PEEP_LOW ),
	setInspPressureSample_( SettingId::INSP_PRESS,
							TrendSelectValue::TREND_SET_INSPIRATORY_PRESSURE ),
	setPressSupportSample_( SettingId::PRESS_SUPP_LEVEL,
							TrendSelectValue::TREND_SET_PRESSURE_SUPPORT_LEVEL ),
	setFlowAccelSample_( SettingId::FLOW_ACCEL_PERCENT,
						 TrendSelectValue::TREND_SET_FLOW_ACCELERATION_OVER_RISE_TIME ),
	setFlowSensSample_( SettingId::FLOW_SENS,
						TrendSelectValue::TREND_SET_FLOW_SENSITIVITY ),
	setPressSensSample_( SettingId::PRESS_SENS,
						 TrendSelectValue::TREND_SET_PRESSURE_SENSITIVITY ),
	setPeepHighTimeSample_( SettingId::PEEP_HIGH_TIME,
							TrendSelectValue::TREND_SET_PEEP_HIGH_TIME ),
	setThTlRatioSample_( SettingId::HL_RATIO,
						 TrendSelectValue::TREND_SET_TH_TL_RATIO ),
	setInspTimeSample_( SettingId::INSP_TIME,
						TrendSelectValue::TREND_SET_INSPIRATORY_TIME ),
	setPeepLowTimeSample_( SettingId::PEEP_LOW_TIME,
						   TrendSelectValue::TREND_SET_PEEP_LOW_TIME ),
	setTidalVolumeSample_( SettingId::TIDAL_VOLUME,
						   TrendSelectValue::TREND_SET_TIDAL_VOLUME ),
	setVolumeSupportSample_( SettingId::VOLUME_SUPPORT,
							 TrendSelectValue::TREND_SET_VOLUME_SUPPORT_LEVEL_VS ),

	// Construct the configuration object that maps incoming data items 
	// to their slots in the database
	dbConfig_( TrendDataMgr::TREND_SETTING_MAX_SLOTS ),

	// Object used to read/write data to the Compact Flash
	compactFlashInterface_( CompactFlash::GetCompactFlash() ),

	// The storage table for the settings
	table_(  TrendDataMgr::SETTING_TABLE_ID, settingRecord_, compactFlashInterface_ )
{
	TREND_MGR_DBG1( 2, "Trend Setting Respository constructor" );

	// Configure the slots of the database
	dbConfig_.configSlot( &setExpSensSample_,       0 );
	dbConfig_.configSlot( &setRespRateSample_,      1 );
	dbConfig_.configSlot( &setPeakInspFlowSample_,  2 );
	dbConfig_.configSlot( &setIeRatioSample_,       3 );
	dbConfig_.configSlot( &setO2PercentSample_,     4 );
	dbConfig_.configSlot( &setTcSupportSample_,     5 );
	dbConfig_.configSlot( &setPeepSample_,          6 );
	dbConfig_.configSlot( &setPeepHighSample_,      7 );
	dbConfig_.configSlot( &setPeepLowSample_,       8 );
	dbConfig_.configSlot( &setInspPressureSample_,  9 );
	dbConfig_.configSlot( &setPressSupportSample_,  10 );
	dbConfig_.configSlot( &setFlowAccelSample_,     11 );
	dbConfig_.configSlot( &setFlowSensSample_,      12 );
	dbConfig_.configSlot( &setPressSensSample_,     13 );
	dbConfig_.configSlot( &setPeepHighTimeSample_,  14 );
	dbConfig_.configSlot( &setThTlRatioSample_,     15 );
	dbConfig_.configSlot( &setInspTimeSample_,      16 );
	dbConfig_.configSlot( &setPeepLowTimeSample_,   17 );
	dbConfig_.configSlot( &setTidalVolumeSample_,   18 );
	dbConfig_.configSlot( &setVolumeSupportSample_, 19 );

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSettingRepository [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSettingRepository::~TrendDbSettingRepository( void )
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
//  Sets initial conditions for data collection. Must be called before 
//  data storage or retrievals begin
//  Returns SigmaStatus
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clears all of the TrendDbSampleData objects, initializes the refresh array
//  and the Settings TrendDbTable. Initializes the table index
//---------------------------------------------------------------------
//@ PreCondition
//	Verifies sufficient space on the compact flash
//---------------------------------------------------------------------
//@ PostCondition
//
//@ End-Method
//=====================================================================
SigmaStatus TrendDbSettingRepository::initialize( void )
{
	TREND_MGR_DBG1( 8, "Initialze the setting repository" );

	// Clear each SampleData object
	for (Uint8 slotNumber = 0; slotNumber < dbConfig_.getMaxSlotNumber(); slotNumber++)
	{
		TrendDbSampleData * ptrSampleData = dbConfig_.getSampleData(slotNumber);
		if (ptrSampleData)
		{
			ptrSampleData->clear();
		}
	}

	// Initialize the elements of the refresh array to 1. This will force every
	// parameter to be sampled and tested for validity.
	memset( isRefreshedArray_, 1, sizeof(isRefreshedArray_) );

	// configure the compact flash block location for the settings table
	table_.config( TrendDataMgr::TREND_SETTING_START_BLOCK,
				   TrendDataMgr::TREND_SETTING_DB_CAPACITY );

	// Verify the size of the setting database
	Uint32 endBlock = table_.getStartLba() + table_.getCapacityLba();
	TREND_MGR_DBG4( 8, "Setting database capacity ", table_.getTableCapacity(),
					", Index capacity ", TABLE_INDEX_CAPACITY );
	TREND_MGR_DBG6( 8, "Setting database end block ", endBlock,
					", start block ", table_.getStartLba(),
					", capacity ", table_.getCapacityLba() );

	AUX_CLASS_PRE_CONDITION( (endBlock <= TrendDataMgr::TREND_SETTING_END_BLOCK),
							 ((endBlock << 16) | TrendDataMgr::TREND_SETTING_END_BLOCK ) );
	AUX_CLASS_PRE_CONDITION( (table_.getTableCapacity() <= TABLE_INDEX_CAPACITY),
							 ((table_.getTableCapacity() << 16) | TABLE_INDEX_CAPACITY ) );

	// Clear the table index
	memset( tableIndex_, 0, sizeof(tableIndex_) );

	// Initialize the Index table and return the status
	TREND_MGR_DBG1( 2, "  Initialize Index table" );
	return( loadTableIndex_() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerTarget
//
//@ Interface-Description
//  Register to the Setting-Validation for updates to batch and non-batch
//  settings.
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRepository::registerTarget( void )
{
	// Register for batch and non-batch settings changes
	TREND_MGR_DBG1( 4, "Trend Setting Repository register to Context Observer" );
	attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID, Notification::BATCH_CHANGED);
	attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID, Notification::NON_BATCH_CHANGED);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetDatabase
//
//@ Interface-Description
//  Clear out the database to default empty
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clear/Reset the setting table
// 	Clear the Index lookup table
//  Clear each of the SampleData objects
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRepository::resetDatabase( void )
{
	TREND_MGR_DBG1( 4, "Reset setting repository" );

	// Clear the table and reset the table Index
	table_.clear();

	// Clear the look up table index
	TREND_MGR_DBG1( 8, "Clear setting look up table" );
	memset( tableIndex_, 0, sizeof(tableIndex_) );

	// Clear each of the SampleData objects
	for (Uint8 slotNumber = 0; slotNumber < dbConfig_.getMaxSlotNumber(); slotNumber++)
	{
		TrendDbSampleData * ptrSampleData = dbConfig_.getSampleData(slotNumber);
		if (ptrSampleData)
		{
			ptrSampleData->clear();
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: batchSettingUpdate
//
//@ Interface-Description
//  Batch setting change callback called from the Settings-Validation Context
//  Observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Updates the isRefreshedArray_ at the offset of the modified setting
//  If any of the system context settings are modified, let Trend
//  Data Mgr know about it.
//---------------------------------------------------------------------
//@ PreCondition
//	Verifies the modified settingId is a valid settingId
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRepository::batchSettingUpdate(
												 const Notification::ChangeQualifier qualId,
												 const ContextSubject *              pContextSub,
												 const SettingId::SettingIdType      setId )
{
	AUX_CLASS_PRE_CONDITION( (setId < SettingId::TOTAL_SETTING_IDS), setId );

	TREND_MGR_DBG4( 64, "Change on Batch Setting Id ", setId,
					", Qual ", qualId );

	if (qualId == Notification::ACCEPTED)
	{
		isRefreshedArray_[setId] = TRUE;

		// Check for a system context change
		if ((setId == SettingId::MODE) ||
			(setId == SettingId::VENT_TYPE)||
			(setId == SettingId::TRIGGER_TYPE)||
			(setId == SettingId::MAND_TYPE)||
			(setId == SettingId::SUPPORT_TYPE)||
			(setId == SettingId::LEAK_COMP_ENABLED)||
			(setId == SettingId::FIO2_ENABLED) ||
            (setId == SettingId::PROX_ENABLED) 
           )
		{
			TREND_MGR_DBG1( 4, "Change in context" );
			// Force DB refresh
			TrendDataMgr::SystemContextChange();    
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nonBatchSettingUpdate
//
//@ Interface-Description
//  Non-Batch setting change callback called from the Settings-Validation
//  Context Observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Updates the isRefreshedArray_ at the offset of the modified setting
//---------------------------------------------------------------------
//@ PreCondition
//	Verifies the modified settingId is a valid settingId
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRepository::nonBatchSettingUpdate(
													const Notification::ChangeQualifier qualId,
													const ContextSubject *              pContextSub,
													const SettingId::SettingIdType      setId )
{
	AUX_CLASS_PRE_CONDITION( (setId < SettingId::TOTAL_SETTING_IDS), setId );

	TREND_MGR_DBG4( 64, "Change on Nonbatch Setting Id ", setId,
					", Qual ", qualId );

	if (qualId == Notification::ACCEPTED)
	{
		isRefreshedArray_[setId] = TRUE;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resyncAll
//
//@ Interface-Description
//  This function is called by the TrendDataMgr to force all of the 
//  trended settings to be re-sampled and re-checked for validity. This 
//  is called when the system context is modified (user updates one of
//  the context settings).
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets all of the boolean values in the isRefreshedArray_ to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRepository::resyncAll( void )
{
	memset( isRefreshedArray_, 1, sizeof(isRefreshedArray_) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sample
//
//@ Interface-Description
//  This function samples the current settings Data through its interface
//  with the ContextObserver in the Settings-Validation subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function loops through each of the trended settings determining
//  which paramters need to be resampled (indicated by the isRefreshedArray_).
//  Each of the modified settings is then checked for validity
//  through a call to the isValidContext() method. 
//  If any of the settings is updated, all of the current setting values are
//  appended to the Settings table in the database through a call to 
//  updateTable_()
// 
//  Satisfies requirement:
//  [[TR01140] [PRD 1493, 1483, 1484, 1485]  The system shall maintain Trend
//  data for the current patient up to 72 hours...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRepository::sample( Uint32 seqNumber, const TimeStamp & timeStamp )
{
	TrendValue newValue;
	Boolean isTableUpdateRequired = FALSE;


	TREND_MGR_DBG2( 8, "Trend Setting Repository sample, frame# ", seqNumber );

	// For each configured slot, check if value has changed, get updated value
	for (Uint8 slotNumber = 0;
		slotNumber < dbConfig_.getMaxSlotNumber();
		slotNumber++)
	{
		TrendDbSampleData * ptrSampleData = dbConfig_.getSampleData( slotNumber );
		if (ptrSampleData)
		{
			SettingId::SettingIdType setId = ptrSampleData->getId().setting;
			TREND_MGR_DBG4( 64, "  Slot ", slotNumber, ", setting Id ", setId );

			// if setting has been updated, sample
			if (isRefreshedArray_[setId])
			{
				const ContextSubject *pContextSub = getSubjectPtr_( ContextId::ACCEPTED_CONTEXT_ID );

				if (SettingId::IsBoundedId( setId ))
				{
					// sample current bounded value
					BoundedValue bv = pContextSub->getSettingValue( setId );
					// is settingId applicable? 
					newValue.isValid = isValidContext( setId );
					if (newValue.isValid)
					{
						// store real value
						newValue.data.realValue = bv;
					}
					else
					{
						// store gap
						newValue.data.realValue = 0;
					}
					TREND_MGR_DBG2( 64, "  value ", newValue.data.realValue );
				}
				else
				{
					// sample current discrete value
					DiscreteValue dv = pContextSub->getSettingValue(setId);
					// is settingId applicable? 
					newValue.isValid = isValidContext( setId );
					if (newValue.isValid)
					{
						// store discrete value
						newValue.data.discreteValue = dv;
					}
					else
					{
						// store gap
						newValue.data.discreteValue = 0;
					}
					TREND_MGR_DBG2( 64, "  value ", newValue.data.discreteValue );
				}
				// Update the value in the sample collection
				// Settings uses only 1 table, the default table
				ptrSampleData->setValue( newValue );
				isRefreshedArray_[setId] = FALSE;

				// Indicate updated data to append to table
				isTableUpdateRequired = TRUE;
			}
			else
			{
				// No trended settings have changed. Don't refresh.
				TREND_MGR_DBG1( 64, "  Data not changed" );
			}
		} // config slot
	} // for each slot

	TREND_MGR_DBG2( 8, "Setting update table ", isTableUpdateRequired );

	// Trended settings have changed. Refresh Table.
	if (isTableUpdateRequired)
	{
		// Update the table
		if (updateTable_( seqNumber, timeStamp ) == FAILURE)
		{
			// Failed to write settings data to compact flash. Disable trending.
			TREND_MGR_DBG1( 1, "Setting sample update - compact flash failed" );
			TrendDataMgr::Disable();
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateTable
//
//@ Interface-Description
//  Append the sampled data values to the settings table
//  Returns SigmaStatus SUCCESS if no write errors
//  Returns SigmaStatus FAILURE otherwise
//---------------------------------------------------------------------
//@ Implementation-Description
//  Return success of the operation
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus TrendDbSettingRepository::updateTable_( Int32 seqNumber, const TimeStamp & timeStamp )
{
	SigmaStatus rtn = SUCCESS;
	TREND_MGR_DBG1( 8, "Update Setting's table" );

	// Setup the next record in the table
	if (table_.setNextRecord( seqNumber, timeStamp ) == SUCCESS)
	{
		// Update each trended settingId and append it to the table
		for (Uint8 slotNumber = 0;
			slotNumber < dbConfig_.getMaxSlotNumber();
			slotNumber++)
		{
			// Get updated setting value 
			TrendDbSampleData *pSampleData = dbConfig_.getSampleData(slotNumber);
			if (pSampleData)
			{
				TREND_MGR_DBG4( 64, "  Append data ", slotNumber, ", id ", pSampleData->getId().setting );
				// Append the setting value to the output-to-flash record.
				table_.appendData( slotNumber, pSampleData->getValue() );
			}
		}
		// Close the table
		table_.closeRecord();
		Uint32 tableRecCount = table_.getRecordCount();
		Uint32 tableRecPosition = table_.getRecordPosition();
		TREND_MGR_DBG4( 8, "Setting Table count ", tableRecCount, ", position ", tableRecPosition );

		// Update the index
		tableIndex_[ tableRecPosition ] = seqNumber;
		TREND_MGR_DBG4( 32, "Table Index ", tableRecPosition, " maps seq# ", tableIndex_[ tableRecPosition ] );
	}
	else
	{
		// Failed to write the data to the settings table. Return write error.
		TREND_MGR_DBG1( 1, "Failed to Update Setting's table with new data" );
		rtn = FAILURE;
	}

	// return SigmaStatus
	return( rtn );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isValidContext
//
//@ Interface-Description
//  Return TRUE is the patientData Item is valid within the current 
//  setting context.
//  Return FALSE otherwise
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function determines the validity of the given settingId through
//  a call to the isApplicable() method in the TrendDbApplicability class.
// 
//  Satisfies requirements: 
//  [[TR01127] Trending shall display Gaps for Trend Parameters that are
//  not applicable...
//  [[TR01203] Trending shall display Gaps for samples acquired during
//  Apnea Ventilation
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendDbSettingRepository::isValidContext( const SettingId::SettingIdType setId )
{
	TREND_MGR_DBG2( 64, "Setting isValidContext ", setId );
	const TrendDbApplicability & trendApplicability = TrendDataMgr::GetApplicability();
	return( trendApplicability.isApplicable(setId) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processRequest
//
//@ Interface-Description
//  This function is called by the TrendDataMgr to process a data
//  retrieval request from the GUI.
//  Returns a TrendDataSet::Status :
// 	EMPTY_SET if no data in the database
//  PROCESS_ERROR if problem reading from the database, or 
//  PROCESS_OK if operation completed successfully.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function extracts the request information from the provided
//  TrendDataSet (position, size, timescale, etc), ensures that the Settings
//  table contains data, and adjusts the request parameters as necessary
//  to frame the request as the database interface expects it.
//  
//  The function then iteratively requests and parses the data through a call 
//  to loadRecordData_(). This loop continues until the request is fulfilled.
//  The data is loaded into the provided TrendDataSet.
// 
//  This function expects that the settings database has correctly loaded
//  the target seq numbers and timestamps.
// 
//  Satisfies requirement:
//  [[TR01139] [PRD 1493] If a Trend data integrity verification fails the
//  system shall display that Trend value as a Gap...
//---------------------------------------------------------------------
//@ PreCondition
//	Verifies pTrendDataSet exists.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDataSet::Status TrendDbSettingRepository::processRequest( TrendDataSet * pTrendDataSet )
{
	AUX_CLASS_PRE_CONDITION( pTrendDataSet, (Uint32) pTrendDataSet );

	TrendDataSet::Status isSuccessful = TrendDataSet::PROCESS_OK;
	Int16 rows = 0;

	// Get the Trend Data Set request information
	TrendRequestInfo & reqInfo = pTrendDataSet->getRequestInfo_();
	Int32 startPosition = 0;
	Uint16 requestSize =  reqInfo.requestSize_; 
	Uint32 trendSize = table_.getRecordCount();

	TREND_MGR_DBG1( 4, "Trend Setting Repository Request" );
	TREND_MGR_DBG6( 4, "  position ", startPosition, ", size ", requestSize, ", database size ", trendSize );

	// If either the request or the settings table is empty, return PROCESS_OK.
	// Since the Settings table may be empty when patient data exists, don't 
	// set the EMPTY_SET status.
	if (requestSize == 0 || trendSize == 0)
	{
		TREND_MGR_DBG1( 1, "Settings No data to load" );
		return( isSuccessful );
	}

	// Setup the initial values for the transfer
	Boolean isXferDone = FALSE; 
	TrendDbSettingRecord loadRecord;
	TrendDbTableBuffer loadBuffer( TrendDataMgr::GetReadBuffer(),
								   TrendDataMgr::TREND_READ_BUFFER_SIZE,
								   sizeof( CompactFlash::Block ),
								   loadRecord.size() );

	// Continue to load the Settings data from the table one buffer at a time
	while (!isXferDone)
	{
		// Get the target seq number
		Int32 seqNumber = pTrendDataSet->getSequenceNumber( rows );
		TREND_MGR_DBG4( 16, "Target sequence number ", seqNumber, ", row ", rows );

		// Find the start position
		// Parse the data for the requested setting values
		startPosition = findPosition_( seqNumber );
		if (startPosition != -1)
		{
			// Determine transfer size based on the buffer size and database size. 
			Uint32 dataSize = loadBuffer.getRecordCapacity();

			// If request is larger than the data in the table, adjust request size
			if (dataSize >= (trendSize - startPosition))
			{
				TREND_MGR_DBG6( 16, "Setting Repository load at end, data size", dataSize,
								", table size, ", trendSize,
								", load position ", startPosition );
				dataSize = trendSize - startPosition;
			}
			TREND_MGR_DBG4( 16, "Load Setting's Position ", startPosition, ", size (records) ", dataSize );

			if (table_.loadRecordSet( loadBuffer, startPosition, dataSize ) == SUCCESS)
			{
				// Load and parse the setting data from the buffer
				rows = loadRecordData_( loadBuffer, pTrendDataSet, rows );

				// Check error status of loadRecordData_()
				if (rows == -1)
				{
					// Error loading record data
					TREND_MGR_DBG1( 1, "Setting Repository found error" );
					isSuccessful = TrendDataSet::PROCESS_ERROR;
				}
				// Request completed?
				else if (rows >= requestSize || (startPosition + dataSize) >= trendSize)
				{
					isXferDone = TRUE;
					isSuccessful = TrendDataSet::PROCESS_OK;
					TREND_MGR_DBG1( 4, "Setting Repository Load Record Set Done" );
				}

				TREND_MGR_DBG4( 16, "  isXferDone ", isXferDone,
								", rows ",  rows );
			}
			else
			{
				// Error loading record set
				TREND_MGR_DBG1( 1, "Trend Setting Repository Load Error" );
				TREND_MGR_DBG4( 1, "  position ", startPosition,
								", size ", dataSize ); 
				isXferDone = TRUE;
				isSuccessful = TrendDataSet::PROCESS_ERROR;
			}
		}
		else
		{
			// frame seq number not found
			// Position not found so no more data to load
			TREND_MGR_DBG4( 16, "Settings record not found for ", seqNumber,
							", data set row ", rows );
			isXferDone = TRUE;
			isSuccessful = TrendDataSet::PROCESS_OK;
		}
	}

	TREND_MGR_DBG4( 4, "Setting Load Records done status ", isSuccessful, ", rows ", rows );

	// return TrendDataSet::Status
	return( isSuccessful );
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hasSettingIds_
//
//@ Interface-Description
//  Used to check if a request includes settingId's in the Setting database
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns TRUE if settingId's requested
//  Return FALSE if all requested data is patient data.
//  
//---------------------------------------------------------------------
//@ PreCondition
//	pTrendDataSet exists
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendDbSettingRepository::hasSettingIds( TrendDataSet * pTrendDataSet )
{
	AUX_CLASS_PRE_CONDITION( pTrendDataSet, (Uint32) pTrendDataSet );

	Boolean hasSettingIds = FALSE;

	// Get the request information from the data set
	TrendRequestInfo & reqInfo = pTrendDataSet->getRequestInfo_();

	// Check each column
	for (Int16 idx = 0; idx < TrendDataMgr::MAX_COLUMNS && !hasSettingIds; idx++)
	{
		if (dbConfig_.getSlotNumber( reqInfo.requestIds_[ idx ] ) != -1)
		{
			// Found Setting data
			hasSettingIds = TRUE;
			TREND_MGR_DBG4( 32, "Settings found for Column ", idx, ", trend select Id ", reqInfo.requestIds_[ idx ] );
		}
	}

	return( hasSettingIds );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: findPosition_
//
//@ Interface-Description
//  This function locates the position of the settings block corresponding
//  to the given sequence number.
//  Returns the position of the settings.
//  Returns -1 if no position is found
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since the settings are only captured when they are modified, we can have
//  a single settings record valid for many patient data records. When we go
//  to load settings and patient data to return to the client, we need to ensure
//  that the settings we provide were actually active at the timestamps requested.
// 
//  This is handled by looping through the settings record from newest to 
//  oldest until the first record with a sequence number less than the 
//  provided sequence number is located. Earlier records are no longer active
//  at the requested timestamp and later records are not yet active.
//  Handles the table wrap-around.
//---------------------------------------------------------------------
//@ PreCondition
//	pLoadMap and pTrendDataSet exist
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Int32 TrendDbSettingRepository::findPosition_( Int32 seqNumber )
{
	Uint32 trendSize = table_.getRecordCount();
	Int32 idx = table_.getRecordPosition();
	Int32 num = 0;
	Boolean hasFoundPosition = FALSE;

	TREND_MGR_DBG4( 16, "Find ", seqNumber, ", database size ", trendSize );

	// loop through all the stored setting records, newest to oldest
	while (!hasFoundPosition && num < trendSize)
	{
		if (idx >= 0 && idx < TABLE_INDEX_CAPACITY)
		{
			// If seqNumber is less than the target, this position has our setting. Return it.
			if (tableIndex_[idx] != 0 && tableIndex_[idx] < seqNumber)
			{
				TREND_MGR_DBG6( 16, "Found at count ", num,
								", at idx ", idx,
								", value ", tableIndex_[idx] );
				hasFoundPosition = TRUE;
			}
		}
		if (!hasFoundPosition)
		{
			// Update the counts
			num++;
			idx--;
			if (idx < 0)
			{
				// Wrap around
				idx = trendSize - 1;
			}
		}
	}
	// Position not found. Return -1
	if (!hasFoundPosition)
	{
		num = -1;
	}
	return( num );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loadTableIndex_
//
//@ Interface-Description
//  Load the Index table from the data stored on the compact flash
//---------------------------------------------------------------------
//@ Implementation-Description
//  The table must be configured before this call. (Initialize())
//  Returns SigmaStatus
//  The index is only loaded from the Compact Flash up to the capacity
//  limit, which may be smaller than the buffer due to records existing
//  in RAM
//---------------------------------------------------------------------
//@ PreCondition
//	Verifies that the table position is valid.
//---------------------------------------------------------------------
//@ PostCondition
//
//@ End-Method
//=====================================================================
SigmaStatus TrendDbSettingRepository::loadTableIndex_( void )
{
	// Get the size and position of the data out on the compact flash
	Boolean isSuccessful = TRUE;
	Uint32 tableRecCount = table_.getRecordCount();
	Uint32 tableRecPosition = table_.getRecordPosition();
	Uint32 loadPosition = 0;	// most current
	Int32 lastFrame = 0;	  // will be set the first pass through the loop

	// Verify that the table position is in a valid range
	AUX_CLASS_PRE_CONDITION( (tableRecPosition < TABLE_INDEX_CAPACITY),
							 ((tableRecPosition << 16) | TABLE_INDEX_CAPACITY ) );

	// Get a buffer from the TrendDataMgr to load the data from the compact flash
	TrendDbSettingRecord loadRecord;
	TrendDbTableBuffer loadBuffer( TrendDataMgr::GetReadBuffer(),
								   TrendDataMgr::TREND_READ_BUFFER_SIZE,
								   sizeof( CompactFlash::Block ),
								   loadRecord.size() );

	TREND_MGR_DBG4( 8, "Load Index table - Count ", tableRecCount,
					", Pos/Index ", tableRecPosition );

	// Keep refilling buffer until out of records or error
	while ((loadPosition < tableRecCount) && isSuccessful)
	{
		Uint32 dataSize = loadBuffer.getRecordCapacity();
		// Adjust data size if request is larger than available data.
		if (dataSize > (tableRecCount - loadPosition))
		{
			dataSize = tableRecCount - loadPosition;
		}
		TREND_MGR_DBG4( 16, "Load size (records) ", dataSize,
						", position ", loadPosition );

		if (table_.loadRecordSet( loadBuffer, loadPosition, dataSize ) == SUCCESS)
		{
			// Check seq/frame numbers from record
			Int16 loopCount = 0;
			Boolean isMoreToLoad = TRUE;
			loadBuffer.setLastRecord( loadRecord );

			while (isMoreToLoad && isSuccessful)
			{
				// verify record checksum
				if (loadRecord.isValid())
				{
					Int32 frameSeqNum = loadRecord.getSeqNumber();

					// If this is the first pass, set the last frame
					if (!lastFrame)
					{
						lastFrame = frameSeqNum;
						TREND_MGR_DBG2( 16, "Set setting start frame ", lastFrame );
					}

					// Make sure that the settings frames are in order
					if (lastFrame >= frameSeqNum)
					{
						tableIndex_[tableRecPosition] = frameSeqNum;
						lastFrame = frameSeqNum;
					}
					else
					{
						// Frames are out of order. Return error
						TREND_MGR_DBG6( 1, "Setting record out of order at ", tableRecPosition,
										", last frame ", lastFrame,
										", current frame ", frameSeqNum );
						isSuccessful = FALSE;
					}
				}
				else
				{
					// Invlaid frames. Return error
					TREND_MGR_DBG4( 1, "Index Table load error at ", tableRecPosition,
									", valid ", loadRecord.isValid() );
					isSuccessful = FALSE;
				}

				// Move to the next record and update the counts
				isMoreToLoad = 
					(loadBuffer.gotoNextRecord( loadRecord, FALSE ) == SUCCESS ? TRUE : FALSE);
				loopCount++;

				// Update the position. Check for wrap.
				if (tableRecPosition)
				{
					tableRecPosition--;
				}
				else
				{
					tableRecPosition = TABLE_INDEX_CAPACITY - 1;
				}
			}

			TREND_MGR_DBG4( 16, "Loaded Index loops ", loopCount,
							", status ", isSuccessful );

			// Update the load position based on the number of records parsed
			loadPosition += loopCount;
		}
		else
		{
			// Error loading record.
			TREND_MGR_DBG1( 1, "Error initialize of the Index Table" );
			isSuccessful = FALSE;
		}
	}
	TREND_MGR_DBG2( 2, "Loaded Setting Index table, count ", tableRecCount );

	// return SigmaStatus
	return( isSuccessful ? SUCCESS : FAILURE );
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loadRecordData_
//
//@ Interface-Description
//  Retrieves the data from the database tables.
//  Returns the number of rows loaded (>=1), if operation completed without load errors. 
//  Returns -1, otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function extracts data from the TrendDbTable one row-based
//  record at a time. This includes all of the trended settings.
//  The dbConfig mapping is used to extract the requested settings from
//  the record. The extracted data is loaded into the provided TrendDataSet
//  to be returned to the client.
//---------------------------------------------------------------------
//@ PreCondition
//	pLoadMap and pTrendDataSet exist
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Int16 TrendDbSettingRepository::loadRecordData_( TrendDbTableBuffer & inputBuffer,
												 TrendDataSet * pTrendDataSet,
												 Uint16 firstRow )
{
	AUX_CLASS_PRE_CONDITION( pTrendDataSet, (Uint32) pTrendDataSet );

	TrendDbSettingRecord loadRecord;
	Uint16 row = firstRow;
	Boolean isMoreToLoad = TRUE;
	Int32 sequenceNumber = 0;
	Boolean isRecordValid = TRUE;
	TrendRequestInfo & reqInfo = pTrendDataSet->getRequestInfo_();

	TREND_MGR_DBG2( 16, "Setting Load record data first row ", row );

	// Load the data from the record into the trend data set in reverse
	// order, such that the newest data is first to go into the data set
	inputBuffer.setLastRecord( loadRecord );
	// verify record checksum
	isRecordValid = loadRecord.isValid();
	while (isMoreToLoad)
	{
		// Get the target sequence number from the Trend Data Set
		sequenceNumber = pTrendDataSet->getSequenceNumber( row );

		// Check to see if data is valid (determined with TrendDbApplicability when stored)
		if (isRecordValid)
		{
			// Check to see if this setting was active at the given timestamp.
			// If the sequence number is greater than the record sequence number, the setting was
			// not active at our timestamp. Keep looking for older settings (lower sequence numbers).
			if (sequenceNumber >= loadRecord.getSeqNumber())
			{
				// Found the active set. 
				for (Uint16 column = 0; column < TrendDataMgr::MAX_COLUMNS; column++)
				{
					// Copy the column data.
					TrendDataValues & columnData = pTrendDataSet->getTrendDataValues_( column );
					TrendSelectValue::TrendSelectValueId columnId = reqInfo.requestIds_[ column ];
					Int16 slotNumber = dbConfig_. getSlotNumber( columnId );
					if (slotNumber != -1)
					{
						// Copy data to the data set
						columnData.setValue_( row, loadRecord.getTrendValue( slotNumber ) );
						columnData.setId_( columnId );
					}
				}
				// data transferred into this row, move to the next
				row++;
			}
			else
			{
				// Didn't find the active set. Move on to the next record in the buffer
				// in the revese direction (newest to oldest).
				isMoreToLoad = 
					(inputBuffer.gotoNextRecord( loadRecord, FALSE ) == SUCCESS ? TRUE : FALSE);
				// verify record checksum
				isRecordValid = loadRecord.isValid();
			}
		}
		else
		{
			// Record is invalid, force a stop
			TREND_MGR_DBG4( 1, "Setting Record is invalid at row ", row, ", target seq# ", sequenceNumber );
			isMoreToLoad = FALSE;
		}

		// Check to see if more records need to be read.
		if (isMoreToLoad && row >= pTrendDataSet->getDataSetSize())
		{
			// Done
			isMoreToLoad = FALSE;
		}
	}

	TREND_MGR_DBG4( 16, "Setting Load Setting done, rows ", row,
					", valid records ", isRecordValid);
	return( (isRecordValid) ? row : -1 );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sampleSimulatedData
//
//@ Interface-Description
//  Fill the database with simulated data
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Loop through each of the configured slots for patient data. For each of these
//  slots, load the data from the associated offset in the input array.
//  Follow with a call to ScheduleTableUpdates to load data into all timescales.
//---------------------------------------------------------------------
//@ PreCondition
//	
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSettingRepository::sampleSimulatedData( Int32 seqPosNumber,
													const TimeStamp & trendTimeStamp,
													const TrendValue * pTrendValues )
{
	// Load simulated data into each configured slot
	for (Int16 slotNumber = 0; slotNumber < dbConfig_.getMaxSlotNumber(); slotNumber++)
	{
		TrendDbSampleData * pSampleData = dbConfig_.getSampleData( slotNumber );
		if (pSampleData)
		{
			pSampleData->setValue( pTrendValues[slotNumber] );

			TREND_MGR_DBG6( 64, "Setting simulated sample ", slotNumber,
							", value ", pSampleData->getValue().data.realValue,
							", flag ", pSampleData->getValue().isValid );
		}
	}

	// update the table
	if (!updateTable_( seqPosNumber, trendTimeStamp ))
	{
		TREND_MGR_DBG1( 1, "Failed to load sim data into setting's" );
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbSettingRepository::SoftFault(const SoftFaultID  softFaultID,
										 const Uint32       lineNumber,
										 const char*        pFileName,
										 const char*        pPredicate)  
{
	CALL_TRACE("TrendDbSettingRepository::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_SETTING_REPOSITORY,
							lineNumber, pFileName, pPredicate);
}
