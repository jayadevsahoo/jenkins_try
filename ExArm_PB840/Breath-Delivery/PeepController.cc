#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@Class: PeepController - Controller for maintaining peep during exhalation
//			phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		Methods are implemented to update the PSOL and the exhalation valve
//		such that the desired peep level is achieved, and to initialize the
//		controller.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms to control peep during exhalation
//		phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		This controller is invoked every BD cycle during exhalation to
//		control the pressure to peep.  Psol commands are computed to
//		deliver flow back into the patient circuit if the pressure drops
//		below peep.  newBreath() method is implemented to initialize the
//		controller at the beginning of exhalation.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//      Only one instance can be instanciated.
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PeepController.ccv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015   By:   rhj    Date: 17-Mar-2009      SCR Number:   6494   
//  Project:  840S2
//  Description:
//       Added an extra condition to the closed loop peep control
//       to enhance the peep regulation.
//
//  Revision: 014   By:   rhj    Date: 19-Jan-2009      SCR Number:   6452   
//  Project:  840S
//  Description:
//       Changed the way to retreive the disconnect sensitivity setting
//       by using RLeakCompMgr.getDiscoSensitivity() instead of using the
//       PhasedInContextHandle::GetBoundedValue() method.
//             
//  Revision: 013   By: rhj    Date: 07-Nov-2008     SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 012  By: syw     Date:  07-Dec-2000    DR Number: 5821
//  Project:  Metabolics
//  Description:
//		Resume base flow at the beginning of exhalation.
//
//  Revision: 011  By: syw     Date:  31-Oct-2000    DR Number: 5793
//  Project:  Metabolics
//  Description:
//		New puffless controller implementation.
//
//  Revision: 010  By: syw     Date:  09-Mar-2000    DR Number: 5684
//  Project:  NeoMode
//  Description:
//		Added call to FlowController::DeterminePsolLookupFlags() and added the
//		results of these flags as arguments to FlowController::updatePsol().
//
//  Revision: 009  By: sah     Date:  22-Sep-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  added neonatal-specific 'k3_' values
//      *  added neonatal-specific controller changes
//      *  cleaned up some implementation
//
//  Revision: 008  By:  syw    Date:   02-Nov-1999    DR Number: DCS 5573
//       Project:  ATC
//       Description:
//			Changed alpha_ = CROSSING_ALPHA
//			Changed lowerLimit_
//			Update filteredLungFlow_, secondMinPress_, filteredLungVolume_ and
//				isPeepControllerFrozen_.
//			Set error = 0.0 during exh autozero && FLOW_TRIGGER
//			Call RNetFlowInspTrigger.setLungFlowConditionMet() if conditions are met.
//			Prevent over writing of feedbackPressure_ in determineFeedbackPressure_()
//				when filtered value is used.
//			Add OFFSET to setting fuzzyGain = 0.0 condition.
//			Set boostFlow = flowTarget_ during low to high peep in BiLevel.
//			Removed lowerLimit_ condition to be less than peep_ - 1.0
//			Limit lowerLimit_ to peep_ if bilevel mand breath
//
//  Revision: 007  By:  iv    Date:   27-Aug-1999    DR Number: DCS 5514
//       Project:  ATC
//       Description:
//			Modfied method updatePsolAndExhalationValves() to set desiredFlow_
//			to flowTarget_ only for pressure trigger and limit desiredFlow_ to
//          exhFlow throughout the exhalation phase.
//
//  Revision: 006  By:  syw    Date:  24-Jun-1999    DR Number: DCS 5449
//       Project:  ATC
//       Description:
//			Added noise band to determine fuzzy gain.  Removed openLoopTraj_
//			argument from calculateFuzzyGain_().
//
//  Revision: 005  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 004  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5446
//       Project:  ATC
//       Description:
//			Initialize flowOffset_ in newBreath() based on trigger type.
//			Eliminate 0.125 offset in openLoopTraj calculation.
//			Changed prevTarget_ <= peep_ condition.
//			Changed exhFlow < airFlow + o2Flow condition.
//			Changed desiredFlow_ <= flowTarget_ && openLoopTraj <
//				feedbackPressure_ && error > -0.25 condition.
//
//  Revision: 003  By:  syw    Date:  22-Jun-1999    DR Number: DCS 5444
//       Project:  ATC
//       Description:
//			Changed KF to 0.00075
//
//  Revision: 002  By:  syw    Date:  15-Jun-1999    DR Number: DCS 5432
//       Project:  ATC
//       Description:
//			Use Pe for feedback except during autozero.  Change k3_ gain,
//			lungVolumeConditionMet_ conditions, and boostFlow values.
//
//  Revision: 001  By:  syw    Date:  20-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial version.
//
//=====================================================================

#include "PeepController.hh"

#include <math.h> //TODO E600 added this to compile
#include "BreathMiscRefs.hh"
#include "ValveRefs.hh"
#include "MainSensorRefs.hh"
#include "ControllersRefs.hh"
#include "TriggersRefs.hh"

//@ Usage-Classes

#include "ExhalationValve.hh"
#include "PressureSensor.hh"
#include "ExhFlowSensor.h"
#include "O2Mixture.hh"
#include "FlowController.hh"
#include "PressureXducerAutozero.hh"
#include "NovRamManager.hh"
#include "PhasedInContextHandle.hh"
#include "BreathRecord.hh"
#include "TriggerTypeValue.hh"
#include "PressureInspTrigger.hh"
#include "PhaseRefs.hh"
#include "ExhalationPhase.hh"
#include "Peep.hh"
#include "ModeValue.hh"
#include "LeakCompEnabledValue.hh"
#include "BiLevelScheduler.hh"
#include "SchedulerRefs.hh"
#include "MandTypeValue.hh"
#include "ManeuverRefs.hh"
#include "P100Maneuver.hh"
#include "TriggerTypeValue.hh"
#include "DiscreteValue.hh"
#include "SettingId.hh"
#include "PatientCctTypeValue.hh"
#include "LeakCompMgr.hh"
#include "PressureSetInspTrigger.hh"
#include "MathUtilities.hh"
#include "AcScheduler.hh"
#include "Breath_Delivery.hh"
//@ End-Usage

//@ Code...
// 
//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

static const Real32 ALPHA_BIAS_FLOW = .005F;
static const Real32 KI = 0.005 ;

// static local variables for a Moving Average window for the peep controller
static	Real32 proxP1 = 0;
static	Real32 proxP2 = 0;
static	Real32 proxP3 = 0;
static	Real32 proxP4 = 0;
static	Real32 proxP5 = 0;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PeepController()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Obtain inspiratory and exhalation tube resistances and
//      initialize leak compensation variables.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PeepController::PeepController(void)
{
	CALL_TRACE("PeepController::PeepController(void)") ;

   	// $[TI1]

	NovRamManager::GetInspResistance( inspResistance_) ;
    NovRamManager::GetExhResistance( exhResistance_) ;

	leakAdd_ = 0.0F;
	biasFlowDelta_ = 0.0F;
	filteredBiasFlowDelta_ = 0.0F;
	ki_ = KI;
	proxPressRef_ = 0.0F;     
	targetPressAdjusted_ = 0.0F;
	proxPressCorr_ = 0.0F;
	proxPressCorrPrev_ = 0.0F;
	peepCtrlFfPrev_ = 0;
	proxPressRefPrev_ = 0.0F;
	pressDriveRefFiltPrev_ = 0.0F;
	prePeepBaseErr_ = 0.0F;
	peepBaseErr_ = 0.0F;

	isPeepChanged_ = FALSE;
	prePeep_ = 0.0F;
	elapsedTimeMs_ = 0;
	boundPprox_ = 0.0F;
	proxPAvg_ = 0.0F;
	nStable_ = 0;
	nPeepDrop_ = 0;
	nTranBreath_ = 0;
	peepVcvTran_ = FALSE;
	filterCoeff_ = 0.0F;
	nStableBreath_ = 0;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PeepController()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

PeepController::~PeepController(void)
{
	CALL_TRACE("PeepController::~PeepController(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getKi
//
//@ Interface-Description
//      Returns the ki gain value currently in use by the controller
//---------------------------------------------------------------------
//@ Implementation-Description
//      Accessor method
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
PeepController::getKi(void) const
{
    CALL_TRACE("PeepController::getKi()") ;
    return (ki_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setKi
//
//@ Interface-Description
//      Takes in a floating point gain, no return
//---------------------------------------------------------------------
//@ Implementation-Description
//      Sets the ki gain used by the controller
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
PeepController::setKi( Real32 gain )
{
    CALL_TRACE("PeepController::setKi( Real32 gain )") ;
    ki_ = gain;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//		This method has peep and the base flow as arguments and returns
//		nothing.  This method is called at the beginning of each exhalation
//		to initialize the controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Data members are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
PeepController::newBreath( const Real32 peep, const Real32 flowTarget)
{
	CALL_TRACE("PeepController::newBreath( \
				const Real32 peep, const Real32 flowTarget)") ;
		
    prePeep_ = peep_;
	peep_ = peep ;

	if(ABS_VALUE(peep_ - prePeep_) >= 3.0)
		isPeepChanged_ = TRUE;
	else
		isPeepChanged_ = FALSE;

	flowTarget_ = flowTarget ;

	// $[NP24012]
	pressureTerm_ = 0.0 ;

	// $[NP24013]
	noiseBand_ = 0.18;
	
	// $[NP24011]
	feedbackPressure_ = RInspPressureSensor.getValue() - inspResistance_.calculateDeltaP( flowTarget_) ;

	// $[NP24013]
	isControllerFrozen_ = FALSE ;
		
	Real32 eip = BreathRecord::GetAirwayPressure() ;
    DiscreteValue triggerType = PhasedInContextHandle::GetDiscreteValue( SettingId::TRIGGER_TYPE) ;

	// $[NP24002]
    if (triggerType == TriggerTypeValue::FLOW_TRIGGER)
    {
	   	// $[TI1]
		baseTraj_ = peep_ + 0.03F * ABS_VALUE(eip - peep_) + 0.1F ;
	}
    else if (triggerType == TriggerTypeValue::PRESSURE_TRIGGER)
	{
	   	// $[TI2]
		baseTraj_ = peep_ + 0.03F * ABS_VALUE(eip - peep_) + 0.3F ;
	}
    else
    {
    	CLASS_PRE_CONDITION( triggerType == TriggerTypeValue::FLOW_TRIGGER
        			|| triggerType == TriggerTypeValue::PRESSURE_TRIGGER) ;
    }

	// $[NP24006]
	prevGeometricTraj_ = eip ;

	// $[NP24013]
	steadyStateCounter_ = 0;
	isClosedLoopControl_ = FALSE ;
	leakAdd_ = RExhalationPhase.getLeakAdd();
	biasFlowDelta_ = 0.0F;
	filteredBiasFlowDelta_ = 0.0F;
	if (IsEquivalent(prePeepBaseErr_, 0.0, HUNDREDTHS) || (peepBaseErr_ > 0.0) || !IsEquivalent(prePeep_, peep_, HUNDREDTHS) || 
		(IsEquivalent(prePeep_, peep_, HUNDREDTHS) && (peepBaseErr_ < 0.0) && fabs(peepBaseErr_) <= fabs(prePeepBaseErr_)))
		prePeepBaseErr_ = peepBaseErr_;
	else if (peepBaseErr_ <= -0.1)
		prePeepBaseErr_ = 0.3 * peepBaseErr_;

	const DiscreteValue  mandType =
		PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);
	
	if ((mandType == MandTypeValue::VCV_MAND_TYPE) && (!RAcScheduler.isPeepRecovery()))
	{
		if ((peep_ - prePeep_) >= 3.0)
			peepVcvTran_ = TRUE;
	}
	else if (RAcScheduler.isPeepRecovery())
	{
		peepVcvTran_ = TRUE;
	}
	else
	{
		if ((peep_ - prePeep_) >= 20.0)
			peepVcvTran_ = TRUE;
	}

	if (peepVcvTran_ == TRUE)
	{
		nTranBreath_ += 1;
		if (ABS_VALUE(prePeepBaseErr_) >= 1.0 || nTranBreath_ == 1)  //during transition
		{
			if (ABS_VALUE(prePeepBaseErr_) <= 4.0 && ABS_VALUE(prePeepBaseErr_) >= 1.0)
				nStableBreath_ += 1;
			else
				nStableBreath_ = 0;

			if (nStableBreath_ >= 2)
			{
				proxPressCorr_ = proxPressCorrPrev_ + prePeepBaseErr_;
				targetPressAdjusted_ = peep_ + proxPressCorr_;
			}
			else
			{				
				proxPressCorr_ = 0.0F;
				targetPressAdjusted_ = peep_ + proxPressCorr_;
			}
		}
		else        //after transition
		{
			peepVcvTran_ = FALSE;
			nTranBreath_ = 0;
			nStableBreath_ = 0;
			proxPressCorr_ = proxPressCorrPrev_ + prePeepBaseErr_;
			targetPressAdjusted_ = peep_ + proxPressCorr_;
		}
	}
	else
	{
			proxPressCorr_ = proxPressCorrPrev_ + prePeepBaseErr_;
			targetPressAdjusted_ = peep_ + proxPressCorr_;
	}

	MathUtils::clampValue(targetPressAdjusted_, peep_ - 1.0f, peep_ + 5.0f);

	if (targetPressAdjusted_ < 0.1)
		targetPressAdjusted_ = 0.1;

	if (peep_ < 3.0)   //for PEEP below 3 cmH2O
	{
		if ((eip - peep_) <= 20.0)
			filterCoeff_ = 16.0;
		else if ((eip - peep_) > 20.0 && (eip - peep_) <= 30.0)
			filterCoeff_ = 20.0;
		else 
			filterCoeff_ = 30.0;
	}
	else              //for PEEP above or equal to 3 cmH2O
		filterCoeff_ = 8.0;

	proxPressRef_ = ((filterCoeff_ - 1)*eip + targetPressAdjusted_)/filterCoeff_;

	proxPressCorrPrev_ = proxPressCorr_;
    proxPressRefPrev_ = proxPressRef_;
	elapsedTimeMs_ = 0;
	if (triggerType == TriggerTypeValue::PRESSURE_TRIGGER)
		boundPprox_ = PhasedInContextHandle::GetBoundedValue(SettingId::PRESS_SENS).value;
	else
		boundPprox_ = 2.0F;                   //for non pressure triggering
	proxPAvg_ = peep_;
	nStable_ = 0;
	nPeepDrop_ = 0;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePsolAndExhalationValves()
//
//@ Interface-Description
//		This method has elapsed time as an argument and returns	nothing.
//		This method is called each exhalation cycle to control the pressure
//		to peep.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The controller is initially in open loop control (isClosedLoopControl_
//		== FALSE).  The open loop traj is the minimum betwwen the base traj
//		and the geometric traj.  When the condition for closed loop control
//		is met, the open loop traj is peep plus the integral of the error.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
PeepController::updatePsolAndExhalationValves( const Uint32 elapsedTimeMs)
{
	CALL_TRACE("PeepController::updatePsolAndExhalationValves( const Uint32 elapsedTimeMs)") ;
	UNUSED_SYMBOL(elapsedTimeMs);	

    Real32 exhFlow = RExhFlowSensor.getDryValue() ;
	Real32 error = 0.0 ;
	Real32 openLoopTraj = 0.0 ;
	
	determineFeedbackPressure_() ;
	
	const DiscreteValue  mandType =
		PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);

	const DiscreteValue  circuitType =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);


	if (isClosedLoopControl_ == FALSE)
	{
	   	// $[TI1]
		// $[NP24005]
		Real32 geometTraj = 0.25F * peep_ + 0.75F * feedbackPressure_ ;
		
		// $[NP24006]
		if (geometTraj > prevGeometricTraj_ - 0.02)
		{
		   	// $[TI2]
			geometTraj = prevGeometricTraj_ - 0.02F ;
		}
	   	// $[TI3]
		
		if (geometTraj < peep_)
		{
		   	// $[TI4]	
			geometTraj = peep_ ;
		}
	   	// $[TI5]	
		
		prevGeometricTraj_ = geometTraj ;

		// $[NP24003]
		baseTraj_ -= 0.01 ;
		
		if (baseTraj_ < peep_)
		{
		   	// $[TI6]	
			baseTraj_ = peep_ ;
		}
	   	// $[TI7]	

		// $[NP24001]
		// $[NP24004]
		openLoopTraj = MIN_VALUE( geometTraj, baseTraj_ - exhResistance_.calculateDeltaP( exhFlow)) ;

		// $[NP24007]
		if (openLoopTraj < peep_ - 3.0)
		{
		   	// $[TI8]	
			openLoopTraj = peep_ - 3.0F ;
		}
	   	// $[TI9]	
		
		error = 0.0 ;

        // $[LC24045] When Leak Comp is ON, changed the condition for the flag 
		// for closed loop control for peep...
		if (RLeakCompMgr.isEnabled()) 		
		{
            // When Leak Comp is ON, modify the condition for the flag 
		    // to begin closed loop peep control to enhance timely peep regulation.
		    if (  (((feedbackPressure_ - peep_) < 1.0f) || (RBreathData.getExhaledTidalVolume() < 50.0f)) &&
				 (circuitType == PatientCctTypeValue::NEONATAL_CIRCUIT)
			   )
			{
				isClosedLoopControl_ = TRUE ;
			}


			if ( (mandType == MandTypeValue::VCV_MAND_TYPE) &&  
				 (BreathRecord::GetEndInspPressurePatData() < RPeep.getActualPeep() + 3.0f)
			   )
			{
				isClosedLoopControl_ = TRUE ;
			}

		}
        else
		{
			// $[NP24008]
			if (feedbackPressure_ - peep_ < 0.35)
			{
				// $[TI10]	
				isClosedLoopControl_ = TRUE ;
			}
		}
	
	   	// $[TI11]	
	}
	else
	{
	   	// $[TI12]	
		// $[NP24009]
		error = peep_ - feedbackPressure_ ;

		// $[NP24018]
		openLoopTraj = peep_ ;
	}

	Real32 errorGain = calculateErrorGain_( error) ;
	
	// $[NP24012]
	pressureTerm_ += error * errorGain * CYCLE_TIME_MS ;

	static const Real32 MAX_OUTPUT = 2.0 ;

	// $[NP24014]
	// $[NP24015]
	if (ki_ * pressureTerm_ > MAX_OUTPUT)
	{
	   	// $[TI13]	
		pressureTerm_ = MAX_OUTPUT / ki_ ;
	}
	else if (ki_ * pressureTerm_ < -MAX_OUTPUT)
	{
	   	// $[TI14]	
		pressureTerm_ = -MAX_OUTPUT / ki_ ;
	}
   	// $[TI15]	

	// $[NP24018]
	Real32 target = openLoopTraj + ki_ * pressureTerm_ ;

	// $[NP24016]
	desiredFlow_ = flowTarget_ ;

	ExhalationValve::DampingMode dampingMode = ExhalationValve::NORMAL ;
	
	// $[NP24017]
	if (exhFlow < 20.0)
	{
	   	// $[TI18]	
		dampingMode = ExhalationValve::SLOWEST ;
	}
   	// $[TI19]	
	Real32 newSample = BreathRecord::GetAirwayPressure();
	//5 prox pressure samples
	proxP5 = proxP4;
	proxP4 = proxP3;
	proxP3 = proxP2;
	proxP2 = proxP1;
	proxP1 = newSample;  
	 
	Real32 minPprox;
	Real32 maxPprox;
	 //find the min value of the 5 samples
	minPprox = min(proxP1, proxP2);
	minPprox = min(minPprox, proxP3);
	minPprox = min(minPprox, proxP4);
	minPprox = min(minPprox, proxP5);
	// find the max value of the 5 samples
	maxPprox = max(proxP1, proxP2);
	maxPprox = max(maxPprox, proxP3);
	maxPprox = max(maxPprox, proxP4);
	maxPprox = max(maxPprox, proxP5);
	// the delta between the max and min values
	Real32 deltaPprox = maxPprox - minPprox;
	//optimize the PEEP baseline
	if (deltaPprox <= boundPprox_)
	{
		nStable_ += 1;
		if (nStable_ >=5)
		{
			if (deltaPprox >= 0.8 * boundPprox_)
				boundPprox_ = deltaPprox;
			else
				boundPprox_ = 0.8 * boundPprox_;
			if(boundPprox_ <= 0.05)
				boundPprox_ = 0.05;
		}
		proxPAvg_ = 0.20*(proxP1 + proxP2 + proxP3 + proxP4 + proxP5) - boundPprox_;
	}
	else
	{
		nStable_ = 0;
	//	if unstable, the baseline is not updated
	}

	peepBaseErr_ = peep_ - proxPAvg_;

	if ((peep_ < 3.0) && (newSample > peep_ + 5.0))   //for PEEP below 3 cmH2O 
	{
		if (newSample < proxPressRefPrev_)
			proxPressRef_ = ((filterCoeff_ - 1) * newSample + targetPressAdjusted_)/filterCoeff_;
		else	
			proxPressRef_ = ((filterCoeff_ - 1) * proxPressRefPrev_ + targetPressAdjusted_)/filterCoeff_;
	}
	else
		proxPressRef_ = ((filterCoeff_ - 1) * proxPressRefPrev_ + targetPressAdjusted_)/filterCoeff_;

	proxPressRefPrev_ = proxPressRef_;

	Real32 DP = exhResistance_.calculateDeltaP(exhFlow);
	Real32 DPfinal;
	if (peep_ < 3.0)
		DPfinal = 0.4F * DP;//0.5 * DP;  //will refine more when resistance calibration is implemented 
	else if (peep_ == 3.0)
		DPfinal = 0.3F * DP; //0.7 * DP;
	else if (peep_ == 4.0)
		DPfinal = 0.2F * DP;//0.8 * DP;
	else
		DPfinal = 0.1F * DP;
	Real32 expPressRef = proxPressRef_ - DPfinal;
	Real32 drvPressTgt = RExhalationValve.getPdrvFromPexh(expPressRef);
	
	Real32 drvPressPrev = RExhalationValve.getPdrvFromUdac((Real32)peepCtrlFfPrev_);

	Real32 drvPress = RExhDrvPressureSensor.getValue();
	Real32 drvPressErr = drvPress - drvPressPrev;
	Real32 drvPressRef = drvPressTgt - drvPressErr;

	Uint32 Tc = 15;
	Real32 drvPressRefFilt = ((Tc-1) * pressDriveRefFiltPrev_ + drvPressRef)/Tc;
    pressDriveRefFiltPrev_ = drvPressRefFilt;

	if (drvPressRefFilt > 100)
		drvPressRefFilt = 100;
	else if (drvPressRefFilt < 0)
		drvPressRefFilt = 0;

	Real32 FeedForwardDac = RExhalationValve.getUdacFromPdrv(drvPressRefFilt);
	
	Real32 FeedForward = FeedForwardDac * 2.5F/4095;
	Real32 FeedForwardCorr;

	if (peep_ < 10.0)
		FeedForwardCorr = 0.005F * peep_;
	else 
		FeedForwardCorr = 0.05;

	Real32 FeedForwardFilt = FeedForward + FeedForwardCorr;
	peepCtrlFfPrev_ = (Uint32)FeedForwardDac;
	Real32 PdrvDac = FeedForwardFilt * 4095/2.5F;

	RExhalationValve.commandValve((Uint32)PdrvDac);
	elapsedTimeMs_ += CYCLE_TIME_MS ;
//end of PEEP controller
	

	// When leak compensation is enabled, calculate a new flow target based
	// on leak.
	if (RLeakCompMgr.isEnabled())
	{
	
		Real32 baseFlow = 0.0F;
		const Real32 BASE_FLOW_MIN = 1.50F;	 // LPM 
		const Real32 PURGE_FLOW = 1.0F ;
	
		DiscreteValue triggerType = PhasedInContextHandle::GetDiscreteValue( SettingId::TRIGGER_TYPE) ;

		// $[LC24026] Calculate baseflow based on trigger type. 
		if (triggerType == TriggerTypeValue::FLOW_TRIGGER)
		{
			baseFlow = PhasedInContextHandle::GetBoundedValue(SettingId::FLOW_SENS).value + BASE_FLOW_MIN ;
	
		}
		else if (triggerType == TriggerTypeValue::PRESSURE_TRIGGER)
		{
		
			baseFlow = PURGE_FLOW ;
		}
		else
		{
			CLASS_PRE_CONDITION( triggerType == TriggerTypeValue::FLOW_TRIGGER
						|| triggerType == TriggerTypeValue::PRESSURE_TRIGGER) ;
		}

        // $[LC24027]If K1Leak and/or K2Leak are modified during a breath (under stability test conditions), 
		// then a new LeakADD is calculated...
		leakAdd_ = leakAdd_ - filteredBiasFlowDelta_;
		filteredBiasFlowDelta_ = filteredBiasFlowDelta_ * (1 - ALPHA_BIAS_FLOW) + (ALPHA_BIAS_FLOW * biasFlowDelta_);
		leakAdd_ = leakAdd_ + filteredBiasFlowDelta_;
	
		
	    // Limit leakAdd
		if (leakAdd_ < ( - baseFlow))
		{
			leakAdd_ = ( - baseFlow);
		}

		// Calculate a new desired flow with leak
		desiredFlow_ = baseFlow + leakAdd_ ;

		const BoundedValue PEEP_LOW_VALUE = PhasedInContextHandle::GetBoundedValue(SettingId::PEEP_LOW);
		const BoundedValue PEEP_HIGH_VALUE = PhasedInContextHandle::GetBoundedValue(SettingId::PEEP_HIGH);
		const DiscreteValue MODE_VALUE = PhasedInContextHandle::GetDiscreteValue(SettingId::MODE);

		const Real32 DISCO_SENS_VALUE = RLeakCompMgr.getDiscoSensitivity();


		// $[LC24028]  The maximum LeakADD will be limited by the 
		// Disconnect Sensitivity setting.
		if ( (MODE_VALUE == ModeValue::BILEVEL_MODE_VALUE) &&
			  BreathRecord::IsHighPeep())
		{	
			const Real32 MAX_FLOW = (Real32)(baseFlow + (DISCO_SENS_VALUE * 
				pow( (PEEP_HIGH_VALUE/MAX_VALUE(PEEP_LOW_VALUE,0.3F)) , 0.5f)));
			if (desiredFlow_ > MAX_FLOW)
			{
				desiredFlow_ = MAX_FLOW;
			}

		}
	
		// Limit the amount of leak added to flow target based
		// on disconnect sensitivity plus base flow
		if (desiredFlow_ > (baseFlow +  DISCO_SENS_VALUE) ) // Liters / min.
		{
		    desiredFlow_ = baseFlow +  DISCO_SENS_VALUE; 
		}


        // During a P100 maneuver, flowtarget shall be purge flow,
		// else it is desired flow.
		if (RP100Maneuver.getManeuverState() == PauseTypes::PENDING)
		{
			flowTarget_ = PURGE_FLOW;
		}
		else
		{
		    flowTarget_ = desiredFlow_;
		}

		RExhalationPhase.setFlowTarget(flowTarget_);
    }
	else
	{
         leakAdd_ = 0.0F;
         filteredBiasFlowDelta_ = 0.0F;
	}

	Real32 desiredO2Flow = desiredFlow_ * RO2Mixture.calculateO2Fraction( desiredFlow_) ;
    Real32 desiredAirFlow = desiredFlow_ - desiredO2Flow ;

	Boolean useAirLookup = FALSE ;
	Boolean useO2Lookup = FALSE ;

	FlowController::DeterminePsolLookupFlags( desiredAirFlow, desiredO2Flow,
												useAirLookup, useO2Lookup) ;

    RAirFlowController.updatePsol( desiredAirFlow, useAirLookup) ;
   	RO2FlowController.updatePsol( desiredO2Flow, useO2Lookup) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PeepController::SoftFault(	const SoftFaultID  softFaultID,
								const Uint32       lineNumber,
		   						const char*        pFileName,
		   						const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PEEPCONTROLLER,
                             lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================



//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineFeedbackPressure_()
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is called
//		to determine the feedback pressure for control.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The feedbackPressure_ is a filtered inspiratory pressure.  During
//		an INSP side autozero or if a Pi stuck is detected, the feedbackPressure_
//		uses an estimate of Pi based on Pe.  If Leak Compensation is 
//      enabled, feedbackPressure_ uses an estimate of Pi based on Pe
//      minus leak loss.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
	PeepController::determineFeedbackPressure_( void)
{
	CALL_TRACE("PeepController::determineFeedbackPressure_( void)") ;




	if (RPressureXducerAutozero.getIsAutozeroInProgress() &&
		RPressureXducerAutozero.getAutozeroSide() == PressureXducerAutozero::EXH)
	{
		Real32 inspPress = RInspPressureSensor.getValue() ;

		// $[TI1]	
		// $[NP24011]
		static const Real32 ALPHA = 0.7 ;
		feedbackPressure_ = ALPHA * feedbackPressure_ + (1.0F - ALPHA) * (inspPress
																		 - inspResistance_.calculateDeltaP( flowTarget_ )) ;
	}
	else
	{
		Real32 exhPress = RExhPressureSensor.getValue() ;

		// $[LC24041] -- For peep control and graphics purposes, when Leak Comp is On,
		//  use the estimated wye pressure using the exhalation pressure...
		if (RLeakCompMgr.isEnabled())
		{

			feedbackPressure_ = exhPress + exhResistance_.calculateDeltaP( flowTarget_ - RExhalationPhase.getLeakAdd()) ;
		}
		else
		{

			// $[TI2]	
			// $[NP24010]
			feedbackPressure_ = exhPress + exhResistance_.calculateDeltaP( flowTarget_) ;
		}

	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateErrorGain_()
//
//@ Interface-Description
//		This method has the pressure error as argument
//		and returns the error gain.  This method is called to determine the
//		error gain.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The errorGain is determined based on certain conditions.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Real32 
PeepController::calculateErrorGain_( const Real32 error)
{
	CALL_TRACE("PeepController::calculateErrorGain_( const Real32 error)") ;
	
	Real32 errorGain = 0.0 ;

	const Real32 OFFSET = 0.18 ;
		
	// $[NP24013]
	if (flowTarget_ + 0.5 > RExhFlowSensor.getDryValue() &&
			(error < -noiseBand_ + OFFSET || error > -noiseBand_ - OFFSET))
	{
	   	// $[TI1]	
		steadyStateCounter_++ ;
	}
	else
	{
	   	// $[TI2]	
		steadyStateCounter_ = 0;
	}

	if (steadyStateCounter_ > 50)
	{
	   	// $[TI3]	
		errorGain = 0.0 ;
		noiseBand_  = 0.36 ;
		isControllerFrozen_ = TRUE ;
	}  	
	else
	{
	   	// $[TI4]	
		noiseBand_  = 0.18 ;
		errorGain = 1.0 ;
	}

	return( errorGain) ;
}



