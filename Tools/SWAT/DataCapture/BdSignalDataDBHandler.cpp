//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file BdSignalDataDBHandler.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------

#include"Convert.h"
#include"CDatabase.h"
#include"NetworkMsgHeader.hh"
#include"CLogger.h"
#include"TelemetryTypes.hh"
#include"BdSignalEnum.hh"
#include"IEventLogger.h"



//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class BdSignalDataDBHandler
{

    public:
    static bool register_types(IEventLogger *pIEventLogger, void *user);


    private:
    static bool insert_event_data(void *pUser, const char *pEventName,
        EpochTimeStamp &ts, std::string &event_sequence_id);

    static void *check_header(boost::shared_ptr<Message> pMessage, 
        const char *message_id, const unsigned int expected_len);

    static bool bdsignal (boost::shared_ptr<Message> pMessage, void *pUser);

};


//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// insert_event_data
//---------------------------
bool BdSignalDataDBHandler::insert_event_data(void *pUser, const char *pEventName,
        EpochTimeStamp &ts, std::string &event_sequence_id)
{
    bool bRetval = true;

    boost::shared_ptr<char> g_buffer(new char[18000]);

    sprintf(g_buffer.get(),
    "INSERT INTO \"PatientData\".event_data (event_descriptor, event_time) "
        "VALUES ('%s',  "
        "(TIMESTAMP WITH TIME ZONE 'epoch' + %f * INTERVAL '1 second')) "
        "RETURNING event_sequence_id",
     pEventName,
     ts.getEpochTimeAsDouble()
    );

    CResult *r = ((CDatabase *)pUser)->select_query(g_buffer.get());   

    std::vector<CRow> v=r->getCRowVector();
    if(v.size()!=1)
    {
        LOG_MESSAGE(LM_ERROR,
                "Failed to insert record into PatientData.event_data");

        bRetval = false;
    }
    else 
    {
          event_sequence_id = 
              (v.begin())->getCol("event_sequence_id").get().c_str();
    }
    delete r;

    return bRetval;
}

//---------------------------
// check_header
//---------------------------
void *BdSignalDataDBHandler::check_header(boost::shared_ptr<Message> pMessage, 
        const char *message_id, const unsigned int expected_len)
{
    unsigned int len = ntohs(((NetworkMsgHeader *)
                (( pMessage.get()->get() ).get()))->pktSize);

    if( ((pMessage.get()->getLength()-sizeof(NetworkMsgHeader)) != len) || 
            (len != expected_len )
    )
    {
//        LOG_MESSAGE(LM_ERROR,"Invalid message size, message_id=%i message_length=%i"
//                ,message_id, pMessage.get()->getLength());

//        return NULL;
    }

    return (void *) ((char *)((NetworkMsgHeader *)
        (( pMessage.get()->get() ).get()))
        + sizeof(NetworkMsgHeader));

}

//---------------------------
// bdsignal
//---------------------------
bool BdSignalDataDBHandler::bdsignal (boost::shared_ptr<Message> pMessage, void *pUser)
{

    std::string event_sequence_id;
    bool status;

    TelemetryFlatPacket<NUM_BDSIGS>* bdPktPtr = (TelemetryFlatPacket<NUM_BDSIGS>*)
        check_header(pMessage, "BDSIGNAL", sizeof(TelemetryFlatPacket<NUM_BDSIGS>));

    if(bdPktPtr==NULL)
    {
        return false;
    }


    status = insert_event_data(pUser, "BDSIGNAL", pMessage->getTimeStamp(), 
            event_sequence_id
            );
 

    if(status == true) 
    {

        for(int i=0;i<MAX_SIZE;i++)
        {

            boost::shared_ptr<char> g_buffer(new char[8000]);

            sprintf(g_buffer.get(),
			"INSERT INTO \"PatientData\".\"BDSIGNAL\" ("
            "event_sequence_id,"
            "event_index_id,"
            "\"inspflowsensor\","
			"\"exhflowsensoroffset\","
			"\"exhflowsensornooffset\","
			"\"lungflow\","
            "\"insppressuresensor\","
            "\"exhpressuresensor\","
            "\"presswyeinspest\","
            "\"InspFlowControllerDesiredFlow\","
            "\"pavphaseelasticplusresistivepressure\","
            "\"pavphaseelsticpressure\","
            "\"pavphaseresistivepressure\","
            "\"pavmgrcraw\","
            "\"c_patient\","
            "\"r_raw\","
            "\"r_et\","
            "\"r_patient\","
            "\"patientresistivewob\","
            "\"patientelastancewob\","
            "\"pressurecontrollerdesiredpressure\","
            "\"exhalationvalvepressurecmd\","
            "\"peepcntrlrfeedbackpressure\","
            "\"insplungbtpsvolume\","
            "\"exhlungbtpsvolume\","
            "\"exhvalvdacportregisterimage\","
            "\"volumetargetedcontrollercpatient\","
            "\"volumetargetedcontrollercpatientfiltered\","
            "\"fio2monitorcount\","

			"\"atmosphericpressuresensorvalue\","

            "\"atmosphericpressuresensorcount\","
            "\"smflowcontrollerflowtarget\","
            "\"smpressurecontrollerpressuretarget\","
            "\"InspVolumeControllerDesiredFlow\","
            "\"InspPsolCmd\","
            "\"revdampinggaindacportregisterimg\","
            "\"InspGasTmpSensorPredictedTemp\","
		   "\"exhgastmpsensorpredictedtemp\","
            "\"phasetype\""
            ") VALUES (%s,%i,"
 /*8*/      "%f,%f,%f,%f,%f,%f,%f,%f,"
            "%f,%f,%f,%f,%f,%f,%f,%f,"
            "%f,%f,%f,%f,%f,%f,%f,%f,"
            "%f,%f,%f,%f,%f,%f,%f,%f,"
			"%f,%f,%f,%f,"
			"'%s'"
            ")",
                    event_sequence_id.c_str(),
					i,
					convert(bdPktPtr->data[BDSIG_InspFlowSensor].s[i]),
					convert(bdPktPtr->data[BDSIG_ExhFlowSensorOffset].s[i]),
					convert(bdPktPtr->data[BDSIG_ExhFlowSensorNoOffset].s[i]),
                    convert(bdPktPtr->data[BDSIG_LungFlow].s[i]),
                    convert(bdPktPtr->data[BDSIG_InspPressureSensor].s[i]),
                    convert(bdPktPtr->data[BDSIG_ExhPressureSensor].s[i]),
                    convert(bdPktPtr->data[BDSIG_PressWyeInspEst].s[i]),
                    convert(bdPktPtr->data[BDSIG_InspFlowControllerDesiredFlow].s[i]), 
                    convert(bdPktPtr->data[BDSIG_PavPhaseElasticPlusResistivePressure].s[i]),
                    convert(bdPktPtr->data[BDSIG_PavPhaseElsticPressure].s[i]),
                    convert(bdPktPtr->data[BDSIG_PavPhaseResistivePressure].s[i]),
                    convert(bdPktPtr->data[BDSIG_PavMgrCraw].s[i]),
                    convert(bdPktPtr->data[BDSIG_C_Patient].s[i]),
                    convert(bdPktPtr->data[BDSIG_R_raw].s[i]),
                    convert(bdPktPtr->data[BDSIG_R_ET].s[i]),
                    convert(bdPktPtr->data[BDSIG_R_Patient].s[i]),
                    convert(bdPktPtr->data[BDSIG_PatientResistiveWob].s[i]),
                    convert(bdPktPtr->data[BDSIG_PatientElastanceWob].s[i]),
                    convert(bdPktPtr->data[BDSIG_PressureControllerDesiredPressure].s[i]),
                    convert(bdPktPtr->data[BDSIG_ExhalationValvePressureCmd].s[i]),
                    convert(bdPktPtr->data[BDSIG_PeepCntrlrFeedbackPressure].s[i]),
                    convert(bdPktPtr->data[BDSIG_InspLungBtpsVolume].s[i]),
                    convert(bdPktPtr->data[BDSIG_ExhLungBtpsVolume].s[i]),
                    convert(bdPktPtr->data[BDSIG_ExhValvDacPortRegisterImage].s[i]),
                    convert(bdPktPtr->data[BDSIG_VolumeTargetedControllerCPatient].s[i]),
                    convert(bdPktPtr->data[BDSIG_VolumeTargetedControllerCPatientFiltered].s[i]),
                    convert(bdPktPtr->data[BDSIG_Fio2MonitorCount].s[i]),

					convert(bdPktPtr->data[BDSIG_AtmosphericPressureSensorValue].s[i]),

                    convert(bdPktPtr->data[BDSIG_AtmosphericPressureSensorCount].s[i]),
                    convert(bdPktPtr->data[BDSIG_SmFlowControllerFlowTarget].s[i]),
                    convert(bdPktPtr->data[BDSIG_SmPressureControllerPressureTarget].s[i]),
                    convert(bdPktPtr->data[BDSIG_InspVolumeControllerDesiredFlow].s[i]),
                    convert(bdPktPtr->data[BDSIG_InspPsolCmd].s[i]),
                    convert(bdPktPtr->data[BDSIG_REVDampingGainDacPortRegisterImg].s[i]),
                    convert(bdPktPtr->data[BDSIG_InspGasTmpSensorPredictedTemp].s[i]), 
					convert(bdPktPtr->data[BDSIG_ExhGasTmpSensorPredictedTemp].s[i]),
					convertPhaseType(bdPktPtr->data[BDSIG_PhaseType].s[i])
                );





					

                status = ((CDatabase *)pUser)->insert_query(g_buffer.get());

                if(status == false)
                {
                    LOG_MESSAGE(LM_ERROR,"Failed to insert BDSIGNAL data. Try manualy thru PGAdmin");
				    break;
                }

            }
    }


    return status;
}

//---------------------------
// register_types
//---------------------------
bool BdSignalDataDBHandler::register_types(IEventLogger *pIEventLogger, void *pCDatabase )
{
    pIEventLogger->addMessageHandler(9950                               ,bdsignal,                    pCDatabase);

    return true;
}
