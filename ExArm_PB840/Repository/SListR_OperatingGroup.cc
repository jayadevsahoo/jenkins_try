#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SListR_OperatingGroup - Abstract Singly-Linked List of 'OperatingGroup' References.
//---------------------------------------------------------------------
//@ Interface-Description
//  This provides an abstraction for a singly-linked list of generic
//  data items each of type 'OperatingGroup'.  The data items of the list are
//  references to the data items that are added to the list.  There
//  are non-constant, public methods to iterate through the contents
//  of the list, and there's also an iterator class (see
//  'SIterR_OperatingGroup') to allow iteration of a constant list.  This
//  is an abstract base class and is used with
//  'FixedSListR<OperatingGroup,SIZE>'.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a standard abstraction for storing data items
//  in a dynamic collection, where initial placement within the
//  collection is quick and easy.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each list has an internal heap containing the singly-linked
//  nodes ('SNodeC_MemPtr').  The nodes are allocated from
//  the heap to contain pointers to the inserted data.  A reference
//  to a singly-linked list of memory pointers is used internally
//  for the storage of the references.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions have to do with the data type that is chosen for
//  this list.  The data type must provide the following mechanisms,
//  to be allowed to be used in this list:
//>Von
//      OperatingGroup::operator==(const OperatingGroup& aType) const;// equivalence operator...
//      OperatingGroup::operator!=(const OperatingGroup& aType) const;// inequivalence operator...
//>Voff
//  For primitive (built-in) types this is already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//      Since these lists contain pointers to the inserted items, those
//      items that are inserted into these lists must have a lifetime that
//      is longer than the lifetime of these lists' reference to them.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSListR.C_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SListR_OperatingGroup.hh"
#include "OperatingGroup.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SListR_OperatingGroup()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this list of references, but not the items that are
//  being referenced by this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The 'rPtrList_' alias can NOT be used from within this destructor,
//  because by the time this destructor is run the derived class's
//  destructor has already destroyed the reference list.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SListR_OperatingGroup::~SListR_OperatingGroup(void)
{
  CALL_TRACE("~SListR_OperatingGroup()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  search(item, startFrom, foundWhen)
//
//@ Interface-Description
//  Search the current list for a node that is a match of 'item'.
//  'startFrom' indicates whether the search is to begin from the
//  first node ('FROM_FIRST'), or the current node ('FROM_CURRENT').
//  If 'foundWhen' is 'EQUIV_OBJECTS', then a match is found if
//  'item' is equivalent to an object referenced within this list.
//  If 'foundWhen' is 'EQUIV_REFERENCES', then a match is found when
//  the reference to 'item' is found.  If a match is found, then
//  'SUCCESS' is returned, and the current node is left pointing to
//  the matching node.  If NO match is found, then 'FAILURE' is
//  returned, and the list is left unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Near the end of this method a pointer to a constant node,
//  'pFoundNode', has its const-ness cast away.  This is due to the
//  fact that 'findNode_()' is a constant method (to allow its use
//  in constant public methods) that returns a pointer.  To ensure
//  consistent const-ness of 'findNode_()', the pointer that is
//  returned is a pointer to a CONSTANT node.  The const-ness of
//  the returned node is not necessary in the design of 'findNode_()',
//  but rather a consistency of design of that method.  Therefore,
//  when this method casts the const-ness of 'pFoundNode' away, it
//  is an acceptable case of working properly with a proper design.
//---------------------------------------------------------------------
//@ PreConditions
//  (startFrom == FROM_FIRST  ||  startFrom == FROM_CURRENT)
//  (foundWhen == EQUIV_OBJECTS  ||  equivWhen == EQUIV_REFERENCES)
//---------------------------------------------------------------------
//@ PostConditions
//  (status == SUCCESS) ==> (currentItem() == item)
//  (status == FAILURE) ==> (*this == OLD{*this})
//@ End-Method
//=====================================================================

SigmaStatus
SListR_OperatingGroup::search(OperatingGroup&                 item,
			     const SearchFrom      startFrom,
			     const EquivResolution foundWhen)
{
  CALL_TRACE("search(item, startFrom, foundWhen)");

  SigmaStatus  status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore search for a node that contains
    // an item equivalent to 'item'...
    const SNodeC_MemPtr*  pFoundNode = NULL;

    // set 'pFoundNode' to the node that is to be the starting point for
    // this search...
    switch (startFrom)
    {
    case FROM_FIRST:		// $[TI1.1]
      pFoundNode = getFirst_();
      break;
    case FROM_CURRENT:		// $[TI1.2]
      pFoundNode = getCurrent_();
      break;
    case FROM_LAST:
    default:
      AUX_CLASS_ASSERTION_FAILURE(((startFrom     << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(OperatingGroup)));	    // bottom 12 bits...
      break;
    }

    // this is ALWAYS checked by 'findNode_()'...
    SAFE_CLASS_PRE_CONDITION((foundWhen == EQUIV_OBJECTS  ||
			      foundWhen == EQUIV_REFERENCES));

    // use 'findNode_()' to search, starting from 'pFoundNode'...
    pFoundNode = findNode_(item, pFoundNode, foundWhen);

    if (pFoundNode != NULL)
    {   // $[TI1.3]
      // the matching node is found, therefore set the current node to the
      // node that is found...

      // warning:  "const cast away: ..."
      rPtrList_.setCurrent_((SNodeC_MemPtr*)pFoundNode);
      status = SUCCESS;
    }   // $[TI1.4] -- nothing found...
  }   // $[TI2] -- this list is currently empty...

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeItem(item, foundWhen)
//
//@ Interface-Description
//  Remove the first node in this list that is a match of 'item', and
//  return 'SUCCESS'.  If there is NO match, then 'FAILURE' is
//  returned, and the list is unchanged..  When a node is removed, the
//  "next" node is set as the "current" node.  If the removed node is
//  the "last" node in the list, the "current" node is set to the
//  "previous" node.  If the removed node is the only node in this list,
//  the list is left empty.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses 'search()' to find 'item' and set the current
//  node to it, then uses 'removeCurrent()' to remove the current
//  node.  This method returns 'SUCCESS' when 'removeCurrent()' is
//  called.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SigmaStatus
SListR_OperatingGroup::removeItem(OperatingGroup&                 item,
				 const EquivResolution foundWhen)
{
  CALL_TRACE("removeItem(item, foundWhen)");

  SigmaStatus  status;

  // if this list is not empty and a search for 'item' is successful, then
  // remove the item that was found and return 'SUCCESS', otherwise return
  // 'FAILURE'...
  status = (!isEmpty()  &&  search(item, FROM_FIRST, foundWhen) == SUCCESS)
	    ? rPtrList_.removeCurrent()		// $[TI1]
	    : FAILURE;				// $[TI2]

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEquivTo(sList, equivWhen)  [const]
//
//@ Interface-Description
//  Determine the equivalence between this list and 'sList', and
//  return a boolean to indicate whether they're NOT equivalent.  Two
//  lists are equivalent if they have the same number of nodes, and
//  each of the items in one list is equivalent to the corresponding
//  item of the other list.  When 'equivWhen' is 'EQUIV_OBJECTS', then
//  equivalence is based on the referenced objects of each node being
//  equivalent, but when 'equivWhen' is 'EQUIV_REFERENCES', then
//  equivalence is based on the references of each node being
//  equivalent.  (NOTE:  equivalence of two lists does NOT depend on
//  the current nodes of the two lists.)
//---------------------------------------------------------------------
//@ Implementation-Description
//  If looking for an equivalent reference ('EQUIV_REFERENCE'), use
//  the equivalence test of 'rPtrList_'.
//---------------------------------------------------------------------
//@ PreConditions
//  (equivWhen == EQUIV_OBJECTS  ||  equivWhen == EQUIV_REFERENCES)
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
SListR_OperatingGroup::isEquivTo(const SListR_OperatingGroup& sList,
				const EquivResolution       equivWhen) const
{
  CALL_TRACE("isEquivTo(sList, equivWhen)");

  Boolean  isEquiv;

  // the two lists are equivalent if they are at the same address
  // location, OR they're both empty...$[TI1] (TRUE) $[TI2] (FALSE)
  isEquiv = (this == &sList  ||  (isEmpty()  &&  sList.isEmpty()));

  if (!isEquiv  &&  !isEmpty()  &&  !sList.isEmpty())
  {   // $[TI3]
    // both lists are NOT empty, therefore continue checking for
    // equivalence...
    switch (equivWhen)
    {
    case EQUIV_OBJECTS:		// $[TI3.1]
      {
	// check to see if each item is equivalent...
	const SNodeC_MemPtr*  pNode1 = getFirst_();
	const SNodeC_MemPtr*  pNode2 = sList.getFirst_();

	do
	{   // $[TI3.1.1]
	  // while the two lists are equivalent and there are still
	  // more nodes in the lists, compare their referenced items
	  // for equivalence...
	  isEquiv = (*((const OperatingGroup*)(pNode1->getItem())) ==
		     *((const OperatingGroup*)(pNode2->getItem())));

	  if (isEquiv)
	  {   // $[TI3.1.1.1]
	    // the current nodes are equivalent, therefore move both
	    // to the next nodes in their respective lists...
	    pNode1 = pNode1->getNextPtr();
	    pNode2 = pNode2->getNextPtr();
	  }   // $[TI3.1.1.2] -- an in-equivalence is found...
	} while (isEquiv  &&  pNode1 != NULL  &&  pNode2 != NULL);

	if (isEquiv)
	{   // $[TI3.1.2]
	  // all of the items so far have been equivalent, now, since at
	  // least one of the lists is at its end, make sure the other is,
	  // too...
	  isEquiv = (pNode1 == pNode2);  // $[TI3.1.2.1] (T) $[TI3.1.2.2] (F)
	}   // $[TI3.1.3] -- an in-equivalence is found...
      }

      break;
    case EQUIV_REFERENCES:	// $[TI3.2]
      // use the equivalence operator of 'rPtrList_' to compare the
      // reference of each list lists...
      isEquiv = (rPtrList_ == sList.rPtrList_);  // $[TI3.2.1] T $[TI3.2.2] F
      break;
    default:
      AUX_CLASS_ASSERTION_FAILURE(((equivWhen     << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(OperatingGroup)));	    // bottom 12 bits...
      break;
    }
  }   // $[TI4]

  return(isEquiv);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                  [static]
//
//@ Interface-Description
//  Report the software fault that occured within the source code
//  of this class.  The fault, indicated by 'softFaultID', occured
//  at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
SListR_OperatingGroup::SoftFault(const SoftFaultID softFaultID,
				const Uint32      lineNumber,
				const char*       pFileName,
				const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, ABSTRACT_SLISTR,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  findNode_(item, pStartNode, foundWhen)  [const]
//
//@ Interface-Description
//  Starting from 'pStartNode' search for the node that matches
//  'item', then return the pointer to that node.  If no match
//  is found before the end of the list, then return 'NULL'.  If
//  'foundWhen' is 'EQUIV_OBJECTS', then a match occurs when 'item'
//  is equivalent to a referenced item in this list.  If 'foundWhen'
//  is 'EQUIV_REFERENCES', then a match occurs when the reference to
//  'item' is found within this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (foundWhen == EQUIV_OBJECTS  ||  foundWhen == EQUIV_REFERENCES)
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

const SNodeC_MemPtr*
SListR_OperatingGroup::findNode_(OperatingGroup&                       item,
				const SNodeC_MemPtr* pStartNode,
				const EquivResolution       foundWhen) const
{
  CALL_TRACE("findNode_(item, pStartNode, foundWhen)");
  SAFE_CLASS_PRE_CONDITION((isLegalNode_(pStartNode)));

  switch (foundWhen)
  {
  case EQUIV_OBJECTS:  // find matching object...$[TI1]
    // for each node from 'pStartNode' until the end of the list or until
    // a match is found, compare the nodes with 'item'...
    while (pStartNode != NULL  &&
	   *((const OperatingGroup*)(pStartNode->getItem())) != item)
    {   // $[TI1.1] -- at least one iteration...
      pStartNode = pStartNode->getNextPtr();
    }   // $[TI1.2] -- no iterations...

    break;
  case EQUIV_REFERENCES:  // find matching reference...$[TI2]
    if (pStartNode != NULL)
    {   // $[TI2.1]
      // get the reference (address) stored by the "start" node...
      const OperatingGroup* const  P_START_REF = (const OperatingGroup*)(pStartNode->getItem());
 
      if (P_START_REF != &item)
      {   // $[TI2.1.1]
        // find the matching reference...
        void*  pMem = &item;  // avoiding:  "initializer for non-const ref..." 
        // use the reference list's 'findNode_()' to find the reference
        // to 'item'...
        pStartNode = rPtrList_.findNode_(pMem, pStartNode);
      }   // $[TI2.1.2] -- 'pStartNode' already points to a matching node...
    }   // $[TI2.2] -- 'pStartNode' is 'NULL'...

    break;
  default:
    AUX_CLASS_ASSERTION_FAILURE(((foundWhen     << 22) |  // top 10 bits...
				 (getMaxItems() << 12) |  // middle 10 bits...
				 sizeof(OperatingGroup)));	  // bottom 12 bits...
    break;
  }

  return(pStartNode);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  print_(ostr)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by all
//  derived classes, and defined to dump the contents of an instance
//  to 'ostr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented
