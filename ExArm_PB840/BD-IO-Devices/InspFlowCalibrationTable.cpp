#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: InspValveCalibrationTable_t - Implements inspiratory valve calibration.
//---------------------------------------------------------------------
//@ Interface-Description
//      
//---------------------------------------------------------------------
//@ Rationale
//      
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      None
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//*********************************************************************
#include "InspFlowCalibrationTable.h"
#include "GlobalObjects.h"
#include "UtilityFunctions.h"
#include "ValveRefs.hh"
#include "Psol.hh"

static uint16_t PSOL_LIFTOFF_INDEX	= 1;
static float ACCEPTABLE_LEAK_THRESHOLD	= 0.050f;

InspValveCalibrationTable_t::InspValveCalibrationTable_t(CalibrationID_t CalID, AnalogOutputs_t Output) : CalibrationTable_t(CalID)
{
	AnalogOutput = Output;
	if(bCalibrated)
		MinFlowDac = (uint16_t)pTable[INSP_VALVE_TABLE_SIZE / NUM_INSP_VALVE_SUBTABLES];
	else
		MinFlowDac = 0;

	FlowLevel = 0;
	FlowIndex = 0;
	SampleCount = 0;
	FlowSum = 0;
	MinFlowDacCaptured = false;
	ZeroFlowCount = 0;
				
	for(int i=0; i<INSP_VALVE_SUBTABLE_SIZE; i++)
	{
		DacCount[i];
		FlowLPM[i];
	}
	for(int i=0; i<NUM_SAMPLE_AVG; i++)
	{
		InFlow[NUM_SAMPLE_AVG];
	}

	InspValveTable_t *pPsolTable = (InspValveTable_t *)pTable;
	LiftoffValue = (uint16_t)( pPsolTable->DacTable[PSOL_LIFTOFF_INDEX]);

	switch(CalID)
	{
	case CAL_ID_AIR_VALVE:
		RAirPsol.setLiftoff(LiftoffValue) ;
		break;
	case CAL_ID_O2_VALVE:
		RO2Psol.setLiftoff(LiftoffValue) ;
		break;
	// TODO E600 MS. Remove the entire e600 exhalation valve calibration when no longer needed.
	//case CAL_ID_EXH_VALVE:
		//RExhPsol.setLiftoff(LiftoffValue) ;
		break;
	default:
		RDesiredFlowPsol.setLiftoff(LiftoffValue) ;
		break;
	}
}

uint16_t InspValveCalibrationTable_t::GetPsolLiftoff()
{
	return LiftoffValue;
}

CalibrationStatus_t InspValveCalibrationTable_t::Calibrate(Flow_t Flow, bool DataValid)
{
	if(CalibrationStatus != CAL_PENDING)
	{
		CalibrationStatus = CAL_PENDING;
		CalibrationDone = false;
		FdelCalState = FD_INIT;
		FlowDacCommand = 0;
		// TODO E600_LL: to change the way of updating psol throughout file later
		//AioDioDriver.Write(DAC_EXHALATION_VALVE, 0);
		ControlPSOL(DAC_EXHALATION_VALVE, 0);
	}

	DoCalibration(Flow, DataValid);
	
	return CalibrationStatus;
}

bool InspValveCalibrationTable_t::DoCalibration(Flow_t FilteredFlow, bool DataValid)
{
	Flow_t NewFlow;
	WCHAR StrFilAirFlow[10];
	DWORD ElapsedTickCount, CurrentTickCount;
	float *pCalData = pTable;
	UNUSED_SYMBOL(DataValid);

	switch(FdelCalState)
	{
	case FD_INIT :
		if((++FlowDacCommand > MAX_DAC_VALUE) || (FilteredFlow > INSP_CAL_MIN_FLOW))
		{
			wsprintf(StrFilAirFlow, L"%3.1f", FilteredFlow);
			RETAILMSG(1, (_T("Max Del Flow = %d, %s\r\n"), FlowDacCommand,StrFilAirFlow));
			if(FilteredFlow < 10.0)
			{
				CalibrationDone = true;
				CalibrationStatus = CAL_FAILED;
				//AioDioDriver.Write(AnalogOutput, 0);
				ControlPSOL(AnalogOutput, 0);
				RETAILMSG(1, (L"FD_INIT ERROR: Low Flow\r\n"));
			}
			else
			{
				FdelCalState = FD_ZERO;
				FdelCalDone = false;
				FlowLevel = 0;
				FlowIndex = 0;
				SampleCount = 0;
				FlowSum = 0;
				MinFlowDacCaptured = false;
				ZeroFlowCount = 0;
			}
		}
		else
		{
			//AioDioDriver.Write(AnalogOutput, FlowDacCommand);
			ControlPSOL(AnalogOutput, FlowDacCommand);
		}
		break;
	case FD_ZERO :
		if(--FlowDacCommand == 0)
		{
			wsprintf(StrFilAirFlow, L"%3.2f", FilteredFlow);
			RETAILMSG(1, (_T("Min Del Flow = %d, %s\r\n"), FlowDacCommand,StrFilAirFlow));

			if(FilteredFlow > ACCEPTABLE_LEAK_THRESHOLD)
			{
				CalibrationDone = true;
				CalibrationStatus = CAL_FAILED;
				FdelCalState = FD_INIT;
				//AioDioDriver.Write(AnalogOutput, 0);
				ControlPSOL(AnalogOutput, 0);
				RETAILMSG(1, (L"Valve Leaking > %d cc\r\n", (int)(ACCEPTABLE_LEAK_THRESHOLD * 1000)));
			}
			else
			{
				FdelCalState = FD_CHECK;
				FlowDacCommand = DacCount[0];
			}
		}
		else
		{
			if(!MinFlowDacCaptured && (FilteredFlow <= 0.005))
			{
				if(++ZeroFlowCount > 2)
				{
					MinFlowDac = FlowDacCommand;		// capture 0 flow DAC here
					MinFlowDacCaptured = true;
					DacCount[0] = FlowDacCommand;
					FlowLPM[0] = FilteredFlow;

					wsprintf(StrFilAirFlow, L"%3.2f", FilteredFlow);
					RETAILMSG(1, (_T("Zero Flow DAC = %d, Flow = %s\r\n"), FlowDacCommand, StrFilAirFlow));					
				}
			}
			//AioDioDriver.Write(AnalogOutput, FlowDacCommand);
			ControlPSOL(AnalogOutput, FlowDacCommand);
		}
		break;
	case FD_CHECK :
		if(FlowIndex == 0)
		{
			FlowLevel += 1;
			FlowIndex++;
			SampleCount = 0;
			FlowSum = 0;
		}
		else if((FilteredFlow >= FlowLevel) || (SampleCount > 0))
		{
			wsprintf(StrFilAirFlow, L"%3.2f",FilteredFlow);
			RETAILMSG(1, (_T("%d, %s\r\n"), FlowDacCommand,StrFilAirFlow));
			
			FlowSum += FilteredFlow;
			InFlow[SampleCount] = FilteredFlow;
			if(++SampleCount == NUM_SAMPLE_AVG)
			{
				if(DacCount[FlowIndex - 1] > FlowDacCommand)
					DacCount[FlowIndex] = DacCount[FlowIndex - 1];
				else
					DacCount[FlowIndex] = FlowDacCommand;
				
				NewFlow = FlowSum / SampleCount;
				if(FlowLPM[FlowIndex - 1] > NewFlow)
					FlowLPM[FlowIndex] = FlowLPM[FlowIndex - 1];
				else
					FlowLPM[FlowIndex] = NewFlow;
				FlowIndex++;
			
				if(FlowLevel >= INSP_HIGH_LOW_LIMIT)
					FlowLevel += INSP_HIGH_FLOW_CAL_STEP;
				else
					FlowLevel++;
				SampleCount = 0;
				FlowSum = 0;
			}

			if(FlowIndex >= INSP_VALVE_SUBTABLE_SIZE)
			{
				FdelCalDone = true;
			}
		}
		
		if(!FdelCalDone)
		{
			if(FlowDacCommand >= MAX_DAC_VALUE)	// if reaching max value for 12-bit DAC
			{
				FdelCalDone = true;
				for(int i = FlowIndex; i < INSP_VALVE_SUBTABLE_SIZE; i++)
				{
					DacCount[i] = MAX_DAC_VALUE;
					FlowLPM[i] = FlowLPM[i - 1];
				}
			}
			else if(SampleCount == 0)	// only increment DAC count at the beginning of each index
				FlowDacCommand += 1;
						
			//AioDioDriver.Write(AnalogOutput, FlowDacCommand);
			ControlPSOL(AnalogOutput, FlowDacCommand);
			PreviousTickCount = GetTickCount();
			FdelCalState = FD_WAIT;
		}

		if(FdelCalDone)
		{
			RETAILMSG(1, (L"\r\n\r\nconst float InspFlowTab[INSP_VALVE_SUBTABLE_SIZE] = {"));
			for(int i=0; i < INSP_VALVE_SUBTABLE_SIZE; i++)
			{
				if((i % 10) == 0)
					RETAILMSG(1, (L"\r\n"));
				
				wsprintf(StrFilAirFlow, L"%3.2f", FlowLPM[i]);
				if(i < INSP_VALVE_SUBTABLE_SIZE - 1)
					RETAILMSG(1, (L"%s, ", StrFilAirFlow));
				else
					RETAILMSG(1, (L"%s };\r\n\r\n", StrFilAirFlow));
			}
			
			RETAILMSG(1, (L"const float InspDacTab[INSP_VALVE_SUBTABLE_SIZE] = {"));
			for(int i=0; i < INSP_VALVE_SUBTABLE_SIZE; i++)
			{				
				if((i % 10) == 0)
					RETAILMSG(1, (L"\r\n"));
				
				if(i < INSP_VALVE_SUBTABLE_SIZE - 1)
					RETAILMSG(1, (L"%d, ", DacCount[i]));
				else
					RETAILMSG(1, (L"%d };\r\n\r\n", DacCount[i]));
			}

			//AioDioDriver.Write(AnalogOutput, 0);
			ControlPSOL(AnalogOutput, 0);
			FdelCalState = FD_SAVE;
		}
		
		break;

		case FD_WAIT :
			CurrentTickCount = GetTickCount();
			ElapsedTickCount = CurrentTickCount - PreviousTickCount;
			if(ElapsedTickCount > FDEL_CAL_WAIT_TIME)
			{
				FdelCalState = FD_CHECK;
			}
			break;
		case FD_SAVE :
			for(int i = 0; i < INSP_VALVE_SUBTABLE_SIZE; i++)
				*pCalData++ = FlowLPM[i];
			for(int i = 0; i < INSP_VALVE_SUBTABLE_SIZE; i++)
				*pCalData++ = (float)DacCount[i];

			if(SaveCalibrationTable())
				CalibrationStatus = CAL_PASSED;
			else
				CalibrationStatus = CAL_FAILED;

			CalibrationDone = true;
			FdelCalState = FD_INIT;
			break;
		default :
			break;
	}

	return CalibrationDone;
}

uint16_t InspValveCalibrationTable_t::SendFlow(Flow_t Flow)
{
	static float FlowDac = 0;
	static Flow_t PrevFlow = 3333;	// weird value to make sure it sends flow the first time being called

	InspValveTable_t *pInspValveTab = (InspValveTable_t *)pTable;
	float *pFlowTab = pInspValveTab->FlowTable;
	float *pDacTab = pInspValveTab->DacTable;
	
	if(PrevFlow == Flow)
	{
		return (uint16_t)FlowDac;
	}
	PrevFlow = Flow;
	if(Flow == 0)
	{
		//AioDioDriver.Write(AnalogOutput, 0);
		ControlPSOL(AnalogOutput, 0);
	}
	else
	{
		LookUpFloat(&FlowDac, pDacTab, Flow, pFlowTab, INSP_VALVE_SUBTABLE_SIZE-1);
		//AioDioDriver.Write(AnalogOutput, (uint16_t)FlowDac);
		ControlPSOL(AnalogOutput, (uint16_t)FlowDac);
	}
	return (uint16_t)FlowDac;
}
