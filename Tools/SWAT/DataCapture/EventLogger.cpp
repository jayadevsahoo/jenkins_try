//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file EventLogger.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"EventLogger.h"
#include"CLogger.h"

//---------------------------
// ThreadEntranceProc
//---------------------------
void EventLogger::ThreadEntranceProc()
{
        boost::shared_ptr<Message> pMessage;
        bool bNotGetData=false;

        m_bRunning = true;

        LOG_MESSAGE(LM_INFO,"EventLogger(%X)::ThreadEntranceProc() thread started.",this);

        while(m_bRunning)
        {

                if(m_MessageListSemaphore->timedwait(1))
                {

                    unsigned int id;

                    pMessage=dequeueEvent();


                    if(bNotGetData == true)
                    {
                        LOG_MESSAGE(LM_INFO,"EventLogger(%X)::ThreadEntranceProc() getting bd data.",this);
                        bNotGetData=false;
                    }

                  
                    //TODO[ME]: Make sure length makes sense
                    id=ntohs(((NetworkMsgHeader *)(( pMessage.get()->get() ).get()))->msgId);

                    std::map<unsigned int,boost::shared_ptr<MessageHandlerCallBack> >::iterator cur;
                    cur = m_CallBack.find(id);

                    if(m_bActive && (cur != m_CallBack.end()) )
                    {
                        (cur->second)->execute(pMessage);
                    }
                    else 
                    {
                    // TODO[ME]: Should 
                    //   1) log message
                    //   2) regester a dummy callback for data item.

                    //    LOG_MESSAGE(LM_INFO,"EventLogger::ThreadEntranceProc could not log %i.",id);
                        //Do Nothing
                    }
                }
                else
                {
                        if(bNotGetData == false )
                        {
                                LOG_MESSAGE(LM_WARNING,"EventLogger(%X)::ThreadEntranceProc() no bd data avalable.",this);
                                bNotGetData = true;
                        }
                }


        }

        LOG_MESSAGE(LM_INFO,"EventLogger(%X)::ThreadEntranceProc() thread stoped.",this);
}

//---------------------------
// ThreadEntranceProc
//---------------------------
void *EventLogger::ThreadEntranceProc    (void *pVoid)
{
        ((EventLogger* )pVoid)->ThreadEntranceProc ();  

    Thread::exit();

        return NULL;
}

//---------------------------
// enqueueEvent
//---------------------------
bool EventLogger::enqueueEvent(boost::shared_ptr<Message> pMessage)
{

    static unsigned int nlength=0;
    static unsigned int ncnt = 0;
    unsigned int length;
    unsigned int count;
    bool bLogit = false;

    m_MessageListMutex->lock();

        m_MessageList.push_back(pMessage);

    length=m_MessageList.size();

    if(length>nlength)
    {
        nlength = length;
        bLogit  = true;
    }

    count = ++ncnt;

    if(ncnt>=1000)
    {
        ncnt=0;
    }

    m_MessageListMutex->unlock();


    m_MessageListSemaphore->post();
        

    if(bLogit)
    {
        LOG_MESSAGE(LM_NOTICE,"EventLogger(%X)::enqueueEvent() high_watter_mark=%i.",this,length);
    }

    else if( (count == 1000) && (length >1) )
    {
        LOG_MESSAGE(LM_NOTICE,"EventLogger(%X)::enqueueEvent() current_watter_mark=%i.",this,length);
    }

        return true;
}

//---------------------------
// dequeueEvent
//---------------------------
boost::shared_ptr<Message> EventLogger::dequeueEvent()
{

        boost::shared_ptr<Message>  pMessage; 

    m_MessageListMutex->lock();

        pMessage=m_MessageList.front();

        m_MessageList.erase(m_MessageList.begin()); 

    m_MessageListMutex->unlock();
        
        return pMessage;
}

//---------------------------
// EventLogger
//---------------------------
EventLogger::EventLogger():m_bRunning(false),m_bActive(true),
        m_MessageList(),
        m_CallBack()
{

    LOG_MESSAGE(LM_INFO,"EventLogger(%X)::EventLogger() constructor called.",this);


        m_MessageListMutex     = new Mutex();
    m_MessageListSemaphore = new Semaphore();

    m_pThread = new Thread(ThreadEntranceProc, (void *)this);

}



//---------------------------
// ~EventLogger
//---------------------------
EventLogger::~EventLogger()
{

    LOG_MESSAGE(LM_INFO,"EventLogger(%X)::~EventLogger() destructor called.",this);


        m_bRunning = false;
        m_pThread->join();

    delete m_pThread;
    delete m_MessageListSemaphore;
    delete m_MessageListMutex;

}

//---------------------------
// addMessageHandler
//---------------------------
bool EventLogger::addMessageHandler(unsigned int id, 
        bool (*function)(boost::shared_ptr<Message> msg, void *pUser), void *pUser)
{

    LOG_MESSAGE(LM_INFO,"EventLogger(%X)::addMessageHandler() called with id=%i",this,(int)id);


    std::pair<std::map<unsigned int,boost::shared_ptr<MessageHandlerCallBack> >::iterator , bool> cur;
    

    boost::shared_ptr<MessageHandlerCallBack> m(new MessageHandlerCallBack(function, pUser));

    cur = m_CallBack.insert(
            std::pair<unsigned int,boost::shared_ptr<MessageHandlerCallBack> >(id, m)
    );

    return cur.second;

}

bool EventLogger::enable()
{	
	m_bActive = true;
	return true;
}

bool EventLogger::disable()
{	
	m_bActive = false;
	return true;
}


