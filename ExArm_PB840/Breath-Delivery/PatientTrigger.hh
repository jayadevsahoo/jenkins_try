
#ifndef PatientTrigger_HH
#define PatientTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PatientTrigger - Abstract base class for all inspiratory patient
//  triggers.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PatientTrigger.hhv   25.0.4.0   19 Nov 2013 14:00:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 006  By:  sp    Date:  10-Oct-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed const MIN_EXH_TIME.
//
//  Revision: 005  By:  sp    Date:  25-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 004  By:  sp    Date:  26-Jun-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Implement new patient trigger's algorithm.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp    Date:  26-Sep-1995    DR Number:545, 1107
//       Project:  Sigma (R8027)
//       Description:
//             update per controller spec rev 4.0.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "BreathTrigger.hh"

//@ End-Usage

class PatientTrigger : public BreathTrigger{
  public:
    PatientTrigger(const Trigger::TriggerId triggerid);
    virtual ~PatientTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
    virtual void enable(void);

  protected:
    void activateTrigger_(void);

    //@ Data member: prevDryExhFlow_
    // Previous dry exhalation flow.
    Real32 prevDryExhFlow_;

    //@ Data member: isTriggerActive_
    // Patient triggers are not active until PATIENT_MAX_DELAY_MS ms after being enabled. 
    Boolean isTriggerActive_;

  private:
    PatientTrigger(const PatientTrigger&);		// not implemented...
    void   operator=(const PatientTrigger&);	// not implemented...
    PatientTrigger(void);		// not implemented...

};


#endif // PatientTrigger_HH 
