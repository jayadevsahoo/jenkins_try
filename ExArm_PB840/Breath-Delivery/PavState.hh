#ifndef PavState_HH
#define PavState_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PavState - State of PAV delivery.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PavState.hhv   25.0.4.0   19 Nov 2013 14:00:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 001   By: sah   Date:  28-Feb-2001    DR Number: 5805
//  Project:  PAV
//  Description:
//      Created to eliminate need for include of 'PavState' just to
//      get to the 'PavState'.
//
//====================================================================


//@ Usage-Classes
//@ End-Usage


struct PavState
{
  //@ Type:  Id
  // All of the possible state values of PAV.
  typedef enum Id_enum
  {
	STARTUP,
	CLOSED_LOOP
  } Id;
};


#endif // PavState_HH
 
