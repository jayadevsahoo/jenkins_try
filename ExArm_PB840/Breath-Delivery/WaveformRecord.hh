
#ifndef WaveformRecord_HH
#define WaveformRecord_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: WaveformRecord - A record contains breath parameters used to display
//	user selectable waveforms.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/WaveformRecord.hhv   25.0.4.0   19 Nov 2013 14:00:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: rhj   Date:  20-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added proxPatientPressure_ & proxManuever_
// 
//  Revision: 009   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//       Clone PAV  rev 6.0.1.5
//      Added new 'isApneaActive_' and 'isErrorState_' fields.
//
//  Revision: 008   By: syw   Date:  01-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added lung pressure and lung flow (or wye), lung volume.
//		Added isPeepRecovery
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006  By:  gdc   Date:  25-Jun-1998    DR Number:
//       Project:  Color
//       Description:
//             Added breath type to waveform record.
//
//  Revision: 005  By:  sp    Date:  17-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 004  By:  sp    Date:  10-Oct-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             friend SpiroWfTask::WaveformTask.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp   Date:  20-Feb-1995    DR Number: 718
//       Project:  Sigma (R8027)
//       Description:
//             Add autozero as a part of the waveform data.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "BreathRecord.hh"
#  include "BreathPhaseType.hh"
#  include "BreathType.hh"
#  include "SpiroWfTask.hh"
#  include "DiscreteValue.hh"

//@ End-Usage


class WaveformRecord {
    //@ Friend: void BreathRecord::newCycle(void) 
    friend void BreathRecord::newCycle(void);

    //@ Friend: void SpiroWfTask::WaveformTask(void) 
    friend void SpiroWfTask::WaveformTask(void);
  public:
    WaveformRecord(void);
    ~WaveformRecord(void);
    void   operator=(const WaveformRecord&);

    static void SoftFault(const SoftFaultID softFaultID,
  const Uint32      lineNumber,
  const char*       pFileName  = NULL, 
  const char*       pPredicate = NULL);
  
  protected:

  private:
    WaveformRecord(const WaveformRecord&);		// not implemented...

    //@ Data-Member:  patientPressure_
    // patient circuit's pressure
    Real32  patientPressure_;	

	//@ Data-Member: lungPressure_
	// estimated pressure in lung
	Real32 lungPressure_ ;

    //@ Data-Member:  netFlow_
    // patient wye measurements made with flow sensors
    Real32  netFlow_;	

	//@ Data-Member: lungFlow_
	// estimated flow into the lung (or wye flow)
	Real32 lungFlow_ ;

    //@ Data-Member:  netVolume_
    // inspired volume has positive value and expired volume has negative value
    Real32  netVolume_;

    //@ Data-Member: lungVolume_
    // estimated lung volume
    Real32 lungVolume_ ;

    //@ Data-Member:  phaseType_
    // Phase type
    BreathPhaseType::PhaseType phaseType_;

    //@ Data-Member:  breathType_
    // Breath type
    BreathType breathType_;

    //@ Data-Member:  isAutozeroActive_
    // Set to true if auto zero is in progress
    Boolean isAutozeroActive_;

    //@ Data-Member: isPeepRecovery_
    // Set to true if current breath is a peep recovery breath
    Boolean isPeepRecovery_;
    
   //@ Data-Member: isApneaActive_
    // Set to true if current breath is an apnea breath.
    Boolean  isApneaActive_;

    //@ Data-Member: isErrorState
    // Set to true if currently in an error state (e.g., OSC, disconnetc, etc.).
    Boolean  isErrorState_;


    //@ Data-Member: proxManuever_
    // Set to true if prox is in auto zero or purge
	Uint8     proxManuever_;

    //@ Data-Member:  proxPatientPressure_
    // prox circuit's pressure
    Real32  proxPatientPressure_;

	//@ Data-Member: modeSetting_
	// current mode setting
	DiscreteValue modeSetting_ ;

	//@ Data-Member: supportTypeSetting_
	// current support type setting
	DiscreteValue supportTypeSetting_ ;

#ifdef SIGMA_UNIT_TEST
public:
void write(void) const;
#endif //SIGMA_UNIT_TEST

};


#endif // WaveformRecord_HH 
