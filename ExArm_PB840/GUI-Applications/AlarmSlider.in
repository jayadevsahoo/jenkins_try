#ifndef AlarmSlider_IN
#define AlarmSlider_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: AlarmSlider - Displays a slider for setting high and low
// alarm limits, containing buttons for setting the limits and a
// pointer displaying the current value of the associated patient datum.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmSlider.inv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      As part of the new dynamic applicability mechanism, this class needs
//	to provide access methods to its upper and lower setting ids.
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================


//@ Code...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getUpperSettingId
//
//@ Interface-Description
//  Return the setting id of the upper limit setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline SettingId::SettingIdType
AlarmSlider::getUpperSettingId(void) const
{
	CALL_TRACE("getUpperSettingId()");
	return(UPPER_SETTING_ID_);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLowerSettingId
//
//@ Interface-Description
//  Return the setting id of the lower limit setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline SettingId::SettingIdType
AlarmSlider::getLowerSettingId(void) const
{
	CALL_TRACE("getLowerSettingId()");
	return(LOWER_SETTING_ID_);
}  // $[TI1]


#endif // AlarmSlider_IN 
