#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Tube - The class for central location to store related tube 
//		data.
//---------------------------------------------------------------------
//@ Interface-Description
//      This is the class with methods to provide various tube information
//		for other client object usage.
//---------------------------------------------------------------------
//@ Rationale
// 		This class serves as a central control to provide the tube
//		information for the BD sub-system.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The class provides access (get methods) to the tube resistance,
//      inner diameter, and type.  the updateTube() method updates the
//		latest tube id, type, and index into the tube table.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Tube.ccv   25.0.4.0   19 Nov 2013 14:00:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 003  By:  yyy    Date:  28-Jun-1999    DR Number: DCS 5452
//       Project:  ATC
//       Description:
//			Fixed calculation for tubeResistance in getPressDrop();
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  yyy    Date:  07-Jan-1999    DR Number: DCS 5322
//       Project:  ATC (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Tube.hh"

//@ Usage-Classes

#include "BreathMiscRefs.hh"
#include "SettingConstants.hh"
#include "LungData.hh"
#include "Peep.hh"
#include "O2Mixture.hh"
#include "Barometer.hh"

//@ End-Usage

#ifdef SIGMA_UNIT_TEST
#include <string.h>
#include "Ostream.hh"
#endif //SIGMA_UNIT_TEST

//@ Code...

//@ Constant 
static TubeDataType EndoTrachealTubeTable[SettingConstants::TOTAL_TUBE_ID_VALUES] = {
		//	total intercept		total slope
		{0.1145,	 0.0116},		// 4.5 
		{0.0501,	 0.0074},		// 5.0 
		{0.0667,	 0.0059},		// 5.5 
		{0.0514,	 0.0038},		// 6.0 
		{0.0362,	 0.0026},		// 6.5 
		{0.0337,	 0.0020},		// 7.0 
		{0.0230,	 0.0015},		// 7.5 
		{0.0196,	 0.0012},		// 8.0 
		{0.0152,	 0.0009},		// 8.5 
		{0.0114,	 0.0007},		// 9.0 
		{0.0089,	 0.0006},		// 9.5 
		{0.0078,	 0.0004}};		// 10.0 
		
static TubeDataType TracheostomyTubeTable[SettingConstants::TOTAL_TUBE_ID_VALUES] = {
		//	total intercept		total slope
		{0.04854,	 0.00810},		// 4.5 
		{0.02249,	 0.00570},		// 5.0 
		{0.01799,	 0.00374},		// 5.5 
		{0.00835,	 0.00304},		// 6.0 
		{0.02164,	 0.00196},		// 6.5 
		{0.01220,	 0.00159},		// 7.0 
		{0.01420,	 0.00130},		// 7.5 
		{0.00838,	 0.00095},		// 8.0 
		{0.00707,	 0.00075},		// 8.5 
		{0.00701,	 0.00063},		// 9.0 
		{0.00518,	 0.00049},		// 9.5 
		{0.00448,	 0.00040}};		// 10.0 


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Tube
//
//@ Interface-Description
//	Default Contstructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The trachResistance_ member is initialized.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Tube::Tube(void)
{
	CALL_TRACE("Tube::Tube(const TubeDataType::PhaseType phase)");

   	// $[TI1]
	trachResistance_ = 0.0f;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Tube 
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Tube::~Tube(void)
{
	CALL_TRACE("Tube::~Tube()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateTube()
//
//@ Interface-Description
// 		This method has no arguments and has no return value.  This method is
// 		called by the breath phase when a new breath is started.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Update the tube type and inner diameter and use the tubeId_ to
//		calculate the tubeIndex_ into the tube table.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
Tube::updateTube( void)
{
	CALL_TRACE("Tube::updateTube( void)");

	// $[TI1]
	updateType_();
	updateId_();

	tubeIndex_ = Int((tubeId_ - SettingConstants::MIN_TUBE_ID_VALUE) * 2.0 + 0.5f);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPressDrop()
//
//@ Interface-Description
// 		This method has one argument lungFlow and returns the trachResistance.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Retrieve the total intercept and slope to the appropriate
//		tube type table.  The trach resistance is calculated based on
//		lungFlow and the total slope and intercept.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
Tube::getPressDrop(Real32 lungFlow) const
{
	CALL_TRACE("Tube::getPressDrop( void)");

	Real32 totalIntercept = 0;
	Real32 totalSlope = 0;
	
	AUX_CLASS_ASSERTION((tubeType_< SettingConstants::TOTAL_TUBE_ID_VALUES && tubeType_ >= 0), tubeType_) ;

	if (tubeType_ == TubeTypeValue::ET_TUBE_TYPE)
	{									// $[TI1.1]
		totalIntercept = EndoTrachealTubeTable[tubeIndex_].totalIntercept;
		totalSlope =  EndoTrachealTubeTable[tubeIndex_].totalSlope;
	}
	else if (tubeType_ == TubeTypeValue::TRACH_TUBE_TYPE)
	{									// $[TI1.2]
		totalIntercept = TracheostomyTubeTable[tubeIndex_].totalIntercept;
		totalSlope =  TracheostomyTubeTable[tubeIndex_].totalSlope;
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE( tubeType_) ;
	}

	//	$[TC04011]
	Real32 absLungFlow = ABS_VALUE(lungFlow);
	Real32 tubeResistance = absLungFlow * totalSlope + totalIntercept;

	Real32 uncompTrachPressDrop = absLungFlow * tubeResistance;
	Real32 o2Corr = (RO2Mixture.getAppliedO2Percent() + 769.0F) / 790.0F;
	const Real32 humidCorr = 1.1;
	const Real32 measuredError = 0.935;
    const Real32 atmPressureCal = 1032.3;
    const Real32 constantFactor = atmPressureCal * measuredError * humidCorr;
    Real32 atmPressure = RAtmosphericPressureSensor.getValue() ;
	Real32 peep  = RPeep.getActualPeep();
    
	Real32 trachPressDrop = (uncompTrachPressDrop * constantFactor * o2Corr) /
							(atmPressure + peep);

	// cmH2O/L/min
	return (trachPressDrop);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
Tube::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("LungData::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, TUBE,
							lineNumber, pFileName, pPredicate);
}


