#ifndef  TftpSocket_HH
#define	TftpSocket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@  Class: TftpSocket - Provides functionality of the network
//   socket that supports the TFTP file transfer protocol.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/TftpSocket.hhv   25.0.4.0   19 Nov 2013 14:02:52   pvcs  $ 
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  <jhv>    Date:  05-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================


#include "DownloadTypes.hh"
#include "sockintf.hh"
#include "NetSocket.hh"
#include "DownloadError.hh"

#if	defined(SIGMA_SERVER)
#include "BootFile.hh"
#include "TimeTick.hh"
#elif defined(SIGMA_CLIENT)
#include "BufQueue.hh"
#include "FlashMem.hh"
#endif //defined(SIGMA_CLIENT)

	enum TftpOpcode
	{
		Tftp_MIN_OP = 8001,
		OP_RRQ = Tftp_MIN_OP,
		OP_DATA,
		OP_DATA_ACK,
		OP_END_OF_FILE,
		OP_DOWNLOAD_SUCCESS,
		OP_DATA_REQUEST,
		OP_ERROR,
		Tftp_MAX_OP = OP_ERROR
	};

	enum TftpState
	{
		STATES_MIN = 1,
		RRQ_SENT = STATES_MIN,
		RRQ_RECEIVED,
		INITIAL_STATE,
		DATA_SENT,
		DATA_RECEIVED,
		DATA_WAITING,
		ACK_WAITING,
		ACK_RECEIVED,
		END_OF_FILE_REACHED,
		STATES_MAX = ACK_RECEIVED
	};

	enum DataBlockIndex
	{
		CURRENT_BLOCK,
		NEXT_BLOCK
	};

	struct TftpMsgHeader
	{
		Uint16 opCode;
		Uint16 pktSize;
		Uint16 blockNum;
	};

	struct RRQMsg
	{
		TargetTypes type;
		char fileName[MAX_FILE_NAME_LENGTH ];
	};

class TftpSocket : public NetSocket 
{
  public:
    TftpSocket(void);
    ~TftpSocket(void);
	static void Initialize(void);
	void close(char *msg);
#if	defined(SIGMA_CLIENT)
	SigmaStatus downloadFile(char *filename);
	Int connectToServer(Uint32 serverIpAddr, PortId tftpServerPort,
		Uint32 waitDuration );

    //@ Data-Member:  RBufQueue
    //  Data block buffer manager object.
	static BufQueue &RBufQueue;

#endif //defined(SIGMA_CLIENT)
#if defined(SIGMA_SERVER)
	char *getTargetName(void);
	Int doCommand(void);
	void gotInput(void) { inputTime_.now(); }
	Boolean isMaxInactive(void)
		{ return( inputTime_.getPassedTime() > MAX_INACTIVE_INTERVAL); }
	inline Uint32 getClientIp(void);
	inline void setClientIp(Uint32 IpAddr);
	void setSocketId(Int socketId);
#endif //defined(SIGMA_SERVER)
	Int sendMsg(Uint16 opCode, char *buf, int dataSize, int blockNo);
#ifdef SIGMA_UNIT_TEST
	void setSocketIdOnly(Int socketId);
	SigmaStatus callFinishFlashBurning(void);
	void callHandleClientError(DownloadError errorCode);
	SigmaStatus callRecvDataBlock(TftpMsgHeader *pHeader);
	void callSendErrMsg(DownloadError errorCode);
	Int callSendRRQ(char *pFileName);
	Int getFileBlockNum(void);
	void setFileBlockNum(Int passedFileBlockNum);
#endif // SIGMA_UNIT_TEST

    static void SoftFault(const FaultType softFaultID,
		const Uint32 lineNumber,const char*pFileName  = NULL, 
		const char*       pPredicate = NULL);

  protected:

  private:
    TftpSocket(const TftpSocket&);			// not implemented...
    void   operator=(const TftpSocket&);	// not implemented...
	void sendErrMsg_(DownloadError errorCode);

	void handleClientError_(DownloadError eCode);
#if	defined(SIGMA_SERVER)
	Int sendDataBlock_(DataBlockIndex);
	Int doRRQ_(TftpMsgHeader *header);
#elif defined(SIGMA_CLIENT)
	SigmaStatus finishFlashBurning_(void);
	SigmaStatus recvDataBlock_(TftpMsgHeader *header);
	Int sendRRQ_(char *fileName);
#endif //defined(SIGMA_CLIENT)

	//@Data-Member: clientIPAddr_
	//  Ip address of the client of this TftpSocket connection.
		Uint32 clientIPAddr_;
	
#if	defined(SIGMA_SERVER)

	//@Data-Member: bootFile_
	//  boot file that contains a downloadable Flash memory image.
		BootFile bootFile_;
	
	//@Data-Member: netBuf_
	//  Buffer area for a network data block.
		Uint32 netBuf_[DATA_BLOCK_SIZE/sizeof(Int)];
	
	//@Data-Member: targetCPU_
	// CPU type of the target machine this TFTP socket is connected to.
		TargetTypes targetCPU_;
	
	//@Data-Member: inputTime_
	//  Maintains the last time this socket received any data.
		TimeTick inputTime_;

#elif defined(SIGMA_CLIENT)

	//@Data-Member: chkSum_
	//  Buffer area for a POST check sum table block
		EepromStruct  chkSum_;

	//@Data-Member: dataRequest_
	// keeps track of number of times that client requested data from
	// the server for a same block of data.
		Int  dataRequest_;

#endif //defined( SIGMA_CLIENT )

	//@Data-Member: fileBlockNum_
	// current block number of the Tftp file.
		Int fileBlockNum_;
	
	//@Data-Member: state_
	// Tftp Socket state
		TftpState state_;
};
//include inline methods
#include "TftpSocket.in"

#endif // TftpSocket_HH 
