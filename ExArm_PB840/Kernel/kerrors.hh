#ifndef kerrors_HH
#define kerrors_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: kerrors.hh   
//
//  Description:
//     This file contains the LED codes that are produced during
//     POST Kernel tests.  The binary equivalents are seen by the user.
//     The hardware uses inverted logic, the codes indicate the pattern
//     that is lit, ie - a 3 indicates 00000011 where bits 0,1 are lit. 
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/kerrors.hhv   25.0.4.0   19 Nov 2013 14:13:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

enum KernelTests
{
    // 0-6 are defined in kernel.inc for assembly level tests
      POST_ERROR_CPU_UNINITIALIZED        =  0x00    // set only by HW
    , POST_ERROR_PROCESSOR_INITIALIZATION =  0x01 
    , POST_ERROR_INTEGER_UNIT             =  0x02 
    , POST_ERROR_DRAM_REFRESH_TIMER       =  0x03 
    , POST_ERROR_DRAM_STACK               =  0x04 
    , POST_ERROR_BOOT_EPROM               =  0x05 
    , POST_ERROR_PHASE_2                  =  0x06 

    // used in the kernel executive
    , POST_ERROR_ADDRESS_MODE             =  0x07     
    , POST_ERROR_NOVRAM                   =  0x08           
    , POST_ERROR_ROLLING_THUNDER          =  0x09  
    , POST_ERROR_INTERRUPT                =  0x0a 
    , POST_ERROR_TOD                      =  0x0b 
    , POST_ERROR_TIMER                    =  0x0c 
    , POST_ERROR_WATCHDOG                 =  0x0d 
    , POST_ERROR_ALARM                    =  0x0e 
    , POST_ERROR_EEPROMCHECKSUM           =  0x0f 

    , POST_ERROR_UNUSED                   =  0x10 

    // $20-$2F reserved for PostErrorCodes
    // $30-$4F reserved for downloader
    // $50-$6F defined in PostErrors.hh

    , POST_ERROR_DOWNLOAD_BOOT            =  0x80 
    , POST_ERROR_SIGMA_OS_BOOT            =  0x81 
    , POST_ERROR_PBMON_BOOT               =  0x82 
    , POST_ERROR_APPLICATION_BOOT         =  0x83 

    , POST_ERROR_MAX_TEST                 =  0x8f
};


enum PostErrorCodes
{
    // Since these codes are displayed in the LED at the same time as
    // the test step, the enumerators in PostErrorCodes, KernelTests,
    // and PostTests should be unique.

    // $20-$2F reserved for POST exception handler
      POST_ERROR_NMI_OTHER                =  0x20   // $20 either cpu
    , POST_ERROR_NMI_ETHER_PARITY         =  0x21   // $21 either cpu
    , POST_ERROR_NMI_DRAM_PARITY          =  0x22   // $22 either cpu
    , POST_ERROR_NMI_ANALOG               =  0x23   // $23 bd cpu
    , POST_ERROR_NMI_POWER_FAIL           =  0x24   // $24 bd cpu
    , POST_ERROR_NMI_5V_OVER              =  0x25   // $25 gui cpu
    , POST_ERROR_NMI_12V_OVER             =  0x26   // $26 gui cpu
    , POST_ERROR_NMI_12V_UNDER            =  0x27   // $27 gui cpu
    , POST_ERROR_NMI_SAAS                 =  0x28   // $28 gui cpu
    , POST_ERROR_ACCESS_FAULT             =  0x2e   // $2E bus error
    , POST_ERROR_UNEXPECTED_INTERRUPT     =  0x2f   // $2F generic exception

    , POST_ERROR_INTERNAL_FAILURE         =  0xaa
    , POST_ERROR_TEST_FAILURE             =  0xcc
    , POST_ERROR_POWER_FAIL_WATCHDOG      =  0xa4   // $24 | $80
};

#endif  // kerrors_HH
