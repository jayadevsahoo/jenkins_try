#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TextUtil - Low-level text utility routines for word-wrapping, etc.
//---------------------------------------------------------------------
//@ Interface-Description
// A class wrapper for static methods and string buffers that are used to
// do generic text manipulation.  Currently only one utility method is
// provied, WordWrap(), used to word wrap a C string.  The method
// Initialize() must be called before any other.
//---------------------------------------------------------------------
//@ Rationale
// This code contains useful routines for commonly used text formatting
// requirements.
//---------------------------------------------------------------------
//@ Implementation-Description
// See individual methods for particular implementation descriptions.
//---------------------------------------------------------------------
//@ Fault-Handling
// Usual use of asserts to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TextUtil.ccv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 010  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 009  By:  rhj    Date:  29-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//           Added Date Localization.
// 
//  Revision: 008   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 007  By:  clw	   Date:  24-Aug-1998    DCS Number: 5127
//  Project:  Color
//  Description:
//		Peer review rework - added comment.
//
//  Revision: 006  By:  clw	   Date:  06-Aug-1998    DCS Number: 5127
//  Project:  Color
//  Description:
//		Updated the WordWrap() method to handle cheap-text strings in addition to
//      "bare" strings.  This enables translation of the SST test names into Japanese. 
//
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 004  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 003  By:  hhd	   Date:  24-SEP-97    DR Number:  2321
//       Project:  Sigma (R8027)
//       Description:
//				Corrected last modification log.
//
//  Revision: 002  By:  hhd	   Date:  22-AUG-97    DR Number:  2321
//       Project:  Sigma (R8027)
//       Description:
//       		Added method ConvertPointToComma().
//
//  Revision: 001  By:  mpm    Date:  01-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

//
// Sigma includes.
//
#include "TextUtil.hh"
#include "MiscStrs.hh"
#include "TimeStamp.hh"
#include "AcceptedContext.hh"
#include "DateFormatValue.hh"
#include "ContextMgr.hh"
#include <string.h>

//@ Usage-Classes
//@ End-Usage

//@ Code...

//
// Static member initializations.
//
wchar_t TextUtil::CheapText_[] = { L'\0' };
wchar_t TextUtil::Buf_[] = { L'\0' };
Int  TextUtil::TextFieldMemory_[] = { 0 };
TextField &TextUtil::RTextField_ = *((TextField *)TextFieldMemory_);

//@ Constant: FORMAT_OVERHEAD
// A local constant that is the worst case length of the printf() format
// strings used to convert raw C strings to Cheap Text format.  This is
// used to insure that we don't overflow our fixed-size text rendering
// buffers.
static const Int32 FORMAT_OVERHEAD = 22;

//@ Constant: TEXT_BASELINE
// The Y baseline for positioning cheap text strings when finding the dimensions
// of the string.
static const Int32 TEXT_BASELINE = 500;

static StringId *ShortMonthNames[] =
{
	&MiscStrs::MONTH_INVALID,
	&MiscStrs::MONTH_JAN,
	&MiscStrs::MONTH_FEB,
	&MiscStrs::MONTH_MAR,
	&MiscStrs::MONTH_APR,
	&MiscStrs::MONTH_MAY_ABBREV,
	&MiscStrs::MONTH_JUN,
	&MiscStrs::MONTH_JUL,
	&MiscStrs::MONTH_AUG,
	&MiscStrs::MONTH_SEP,
	&MiscStrs::MONTH_OCT,
	&MiscStrs::MONTH_NOV,
	&MiscStrs::MONTH_DEC
};

static StringId *LongMonthNames[] =
{
	&MiscStrs::MONTH_INVALID,
	&MiscStrs::MONTH_JANUARY,
	&MiscStrs::MONTH_FEBRUARY,
	&MiscStrs::MONTH_MARCH,
	&MiscStrs::MONTH_APRIL,
	&MiscStrs::MONTH_MAY,
	&MiscStrs::MONTH_JUNE,
	&MiscStrs::MONTH_JULY,
	&MiscStrs::MONTH_AUGUST,
	&MiscStrs::MONTH_SEPTEMBER,
	&MiscStrs::MONTH_OCTOBER,
	&MiscStrs::MONTH_NOVEMBER,
	&MiscStrs::MONTH_DECEMBER
};

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize	[public, static]
//
//@ Interface-Description
// Initializes the text utility code.  Must be the first method called
// in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Construct the TextField object we use for calculating the dimensions of
// cheap text strings.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextUtil::Initialize(void)
{
	CALL_TRACE("TextUtil::Initialize(void)");

	new (&RTextField_) TextField;				
										 // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WordWrap	[public, static]
//
//@ Interface-Description
// Converts a single C string into 1 or 2 formatted word-wrapped cheap text
// strings.  The cheap text is word-wrapped to fit within a specified area with
// each line of text being centered vertically and formatted according to
// specifications horizontally. Parameters are as follows:
// >Von
//	pString			Pointer 
//	pResultString1	Pointer to the first result buffer that will contain
//					wrapped cheap text.
//	pResultString2	Pointer to the second result buffer that will contain
//					wrapped cheap text.
//	fontStyle		The font style (NORMAL, THIN, ITALIC) of the rendered text.
//	fontSize		The point size of the rendered text.
//	areaWidth		The width of the area in which the text should be rendered.
//	areaHeight		The height of the area in which the text should be rendered.
//	aligment		How the text should be aligned horizontally within the area,
//					LEFT, RIGHT or CENTER.
//	margin			When the alignment is LEFT or RIGHT, this specifies the
//					margin in pixels to leave at the edge of the area when
//					left or right justifying text.  When aligment is CENTER
//					it should be set to twice the margin needed at each side
// 					of the text, e.g. when centering text you may demand that
//					at least 2 pixels be unused at either side of the text so
//					set margin to be 4.
// >Voff
// The return value is the number of lines used to render the string.
//---------------------------------------------------------------------
//@ Implementation-Description
// Processing occurs in three phases:
// >Von
//	1.	Test render each individual white-space delimited token of pString so
//		as to determine the width of the token.
//	2.	Perform word wrapping to pack each output line with the maximum
//		number of tokens.
//	3.	Position word-wrapped lines according to specified formatting
//		parameters.
// >Voff
// Phase 1: Perform test rendering of tokens.  For each token (a token
// being a string of characters unbroken by white space) in the pString input
// parameter, convert it to Cheap Text format, render the Cheap Text to
// determine its width, then remember the location of the token and its width.
//
// Phase 2:  Perform word wrapping.
// Now that we have pre-computed the sizes of each individual token we execute
// a straightforward loop to combine the individual tokens into one large Cheap
// Text string which represents the contents of each line, up to a maximum of
// two lines.
//
// Phase 3: Position lines
// Once the text content of the lines has been finalized we can then position
// the lines according to the formatting parameters 'alignment' and 'margin'.
// These parameters refer to the horizontal alignment of the lines, lines
// being automatically centered vertically within 'areaHeight'.
//
// If all phrases are rendered successfully, return the number of lines
// actually used to render all phrases (1, 2, or 3).
//---------------------------------------------------------------------
//@ PreCondition
// The 'pString', 'pResultString1' and 'pResultString2' arguments must not
// be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int32
TextUtil::WordWrap(
					wchar_t* pString, wchar_t* pResultString1, wchar_t* pResultString2,
					TextFont::Style fontStyle, TextFont::Size fontSize,
					Int32 areaWidth, Int32 areaHeight, Alignment alignment,
					Int32 margin)
{
	CALL_TRACE("TextUtil::WordWrap("
				"char* pString, char* pResultString1, char* pResultString2, "
				"TextFont::Style fontStyle, TextFont::Size fontSize, "
				"Int32 areaWidth, Int32 areaHeight, Alignment alignment, "
				"Int32 margin)");

	wchar_t* resultMsgs[2] = {0};			// Hold pointers to the result messages
	Int32 resultMsgsHeight[2] = {0};		// Result messages absolute height
	Int32 resultMsgsWidth[2] = {0};		// Result messages width
	Int32 resultMsgsDescent[2] = {0};		// Number of pixels that result messages
									// extend below the text baseline
	Int32 resultMsgsY[2] = {0};			// Result messages top left Y position
	Real32 phraseGap = 0;				// Number of pixels there should be
									// between each message so that messages
									// are evenly spaced vertically.  Needs to
									// be floating point to avoid rounding errs.
	wchar_t *pValuePlace = NULL;				// Used for finding the place in a cheap
									// text string where an X or Y value
									// needs to be put (see below for info).
	wchar_t tmpValueBuf[10] = {0};			// Temporary buffer.
	const wchar_t X_PLACE_MARKER = L' ';// This character marks the place in a
									// cheap text string where the X value
									// is placed.
	const wchar_t Y_PLACE_MARKER = L'*';// This character marks the place in a
									// cheap text string where the Y value
									// is placed.
	wchar_t fontStyleIndicator;		// The cheap text character (N,T,I)
									// corresponding to the fontStyle parameter.


	// If the incoming string starts with a brace, assume
	// it is already formatted to fit in the specified space; simply pass it back
	// in the first result string.
	if (*pString=='{')
	{	                                                         // $[TI11]
		// Determine the rendered size of the passed cheap-text string:
		 wcscpy(Buf_, pString);
		int initialWidth=0;  // initial width
		int initialHeight=0;
		RTextField_.setText(Buf_);
		initialWidth = RTextField_.getWidth();
		initialHeight = RTextField_.getHeight();

		// The passed cheap text string must fit in the area:
		CLASS_ASSERTION(initialWidth < areaWidth && initialHeight < areaHeight);
		
		// if it fits, simply copy it to the first output buffer
		 wcscpy(pResultString1, pString);
		return 1; // the number of lines (output buffers) used is always one in this case.
	}	                                                         // $[TI12]

	// Check for illegal null parameters
	CLASS_PRE_CONDITION(pString != NULL);
	CLASS_PRE_CONDITION(pResultString1 != NULL);
	CLASS_PRE_CONDITION(pResultString2 != NULL);

	// Point resultMsgs[] to result string parameters.
	resultMsgs[0] = pResultString1;
	resultMsgs[1] = pResultString2;

	// Initialize the strtok() tokenizer for the pre-processed token buffer
	// Buf_[].  The first token is a special case for strtok().
	 wcscpy(Buf_, pString);

	wchar_t* pTok = wcstok(Buf_, L" \t\n");
	CLASS_ASSERTION(NULL != pTok);

	Int32 		spaceWidth = 0;			// pre-computed width of rendered space
	wchar_t	  	spaceToken[4];			// used by tokenPtrs[]

	// Test render a space character to figure out how wide a space is,
	// saving the "token" in spaceToken[].
	 wcscpy(spaceToken, L" ");
	  swprintf(CheapText_, L"{p=%d,y=%d: }", fontSize, fontSize + 2);
	RTextField_.setText(CheapText_);
	spaceWidth = RTextField_.getWidth();

	const Int32 MAX_TOKENS = 100;				// includes words and spaces
	wchar_t* tokenPtrs[MAX_TOKENS];		// table of ptrs into
	Int32 tokenWidths[MAX_TOKENS];		// table of rendered widths
	Int32 numTokens = 0;				// number of table entries

	// Initialize the tokenWidths[] and tokenPtrs[] arrays with bogus values.

	Int32 i;
	for (i = 0; i < MAX_TOKENS; i++)
	{
		tokenPtrs[i] = NULL;
		tokenWidths[i] = -1;
	}

	// Check which font is desired and set fontStyleIndicator accordingly.
	switch (fontStyle)
	{
	case TextFont::NORMAL:								// $[TI1]
		fontStyleIndicator = L'N';
		break;

	case TextFont::THIN:								// $[TI2]
		fontStyleIndicator = L'T';
		break;

	case TextFont::ITALIC:								// $[TI3]
		fontStyleIndicator = L'I';
		break;

	case TextFont::NUM_STYLE:
	default:
		fontStyleIndicator = L'T';
//		CLASS_ASSERTION(FALSE);		// illegal font style
		break;
	}

	// Test render one token at a time.  Okay, each iteration of this loop
	// takes a token (space delimited string) from Buf_[] (a copy of pString)
	// and renders it in the specified font allowing us to find its width.  A
	// pointer to the token and its width is then stored in tokenPtrs[]
	// and tokenWidths[].
	do
	{
		// Convert raw token string to Cheap Text token string.
		CLASS_ASSERTION(  wcslen(pTok) + FORMAT_OVERHEAD <
													sizeof(CheapText_));

		  swprintf(CheapText_, L"{p=%d,y=%d,%c:%s}",
				fontSize, fontSize + TEXT_BASELINE, fontStyleIndicator, pTok);
		RTextField_.setText(CheapText_);

		// Save the token and its width in the rendered token list.
		CLASS_ASSERTION(numTokens < MAX_TOKENS);
		tokenPtrs[numTokens] = pTok;
		tokenWidths[numTokens++] = RTextField_.getWidth();
	} while ((pTok = wcstok(NULL, L" \t\n")) != NULL);


	// Begin the second step -- word wrapping.  At this point, tokenPtrs[]
	// contains a list of tokens and tokenWidths[] contains the rendered width
	// of each token.  Now, we must pack each output line with the maximum
	// number of tokens that will fit on that line.
	Int32 currentLineNum = 0;
	Int32 currentToken = 0;
	do
	{
		CLASS_ASSERTION(currentLineNum < 2);		// We only support two lines
		SAFE_CLASS_ASSERTION(NULL != resultMsgs[currentLineNum]);
		resultMsgsWidth[currentLineNum] = 0;

		// Copy the initial cheap text into the current line.  Store place
		// markers for the X and Y values.  We will later overwrite values
		// at these places.
		swprintf(resultMsgs[currentLineNum], L"{p=%d,%c     ,y=%c   ,%c:",
				fontSize, X_PLACE_MARKER, Y_PLACE_MARKER, fontStyleIndicator);

		// Copy as many tokens as will fit the current line.
		while (currentToken < numTokens)
		{
			SAFE_CLASS_ASSERTION(tokenPtrs[currentToken] != NULL);
			SAFE_CLASS_ASSERTION(tokenWidths[currentToken] != -1);

			// Special case check to make sure width of a *single* token
			// does not exceed the max allowable line width.  If we hit
			// this, we cannot continue because no amount of word wrapping
			// will solve this problem (hyphenation is out of the question!).
			CLASS_ASSERTION(tokenWidths[currentToken] + margin <= areaWidth);

			// Check to see if the current token can fit on the current line
			// (include a space at the end).
			if (resultMsgsWidth[currentLineNum]
					+ tokenWidths[currentToken] + spaceWidth + margin
																<= areaWidth)
			{											// $[TI4]
				// It can, add it to the current line with a trailing space
				// and accumulate its width in the line's width.
				 wcscat(resultMsgs[currentLineNum], tokenPtrs[currentToken]);
				 wcscat(resultMsgs[currentLineNum], L" ");
				resultMsgsWidth[currentLineNum] +=
							tokenWidths[currentToken] + spaceWidth;
				currentToken++;
			}
			else
			{											// $[TI5]
				// Current token would cause us to exceed max line length, so
				// we're done for the current line.
				CLASS_ASSERTION(wcslen(resultMsgs[currentLineNum]) <
											TextField::MAX_STRING_LENGTH);
				break;
			}
		}

		// Finish the Cheap Text string (while removing trailing space).
		resultMsgs[currentLineNum][wcslen(resultMsgs[currentLineNum]) - 1]
																		= L'}';
		resultMsgsWidth[currentLineNum] -= spaceWidth;

		// Let's go find the height and descent of the current line.  Insert
		// a large Y baseline value into the Y position of the cheap text
		// string (guaranteeing that the string can be rendered) then get
		// the height.

		// Find where to place the Y value in the cheap text.
		pValuePlace = wcschr(resultMsgs[currentLineNum], Y_PLACE_MARKER);
		SAFE_CLASS_ASSERTION(pValuePlace);

		// Put large Y baseline value there.
		  swprintf(tmpValueBuf, L"%04d", TEXT_BASELINE);
		wcsncpy(pValuePlace, tmpValueBuf, wcslen(tmpValueBuf));

		// Put cheap text into a text field and get the height and descent
		// of the string.
		RTextField_.setText(resultMsgs[currentLineNum]);
		resultMsgsHeight[currentLineNum] = RTextField_.getHeight()
									- RTextField_.getUnusedVerticalHeight();
		resultMsgsDescent[currentLineNum] =
									RTextField_.getHeight() - TEXT_BASELINE;

		// Replace original Y place marker in string so that we can find the
		// place later to put in the final Y position of the text's baseline.
		  swprintf(tmpValueBuf, L"%c   ", Y_PLACE_MARKER);
		wcsncpy(pValuePlace, tmpValueBuf, wcslen(tmpValueBuf));

		// On to next line
		currentLineNum++;
	}
	while (currentToken < numTokens);

	// Let's calculate the Y positions of the top left of each line so that
	// the lines are centered vertically within the prescribed area.  Algorithm
	// differs based on how many lines we are rendering.
	switch (currentLineNum)
	{
	case 1:												// $[TI6]
		// 1 line centering
		resultMsgsY[0] = (areaHeight - resultMsgsHeight[0]) / 2;
		break;

	case 2:												// $[TI7]
		// 2 line centering
		phraseGap = (areaHeight - resultMsgsHeight[0] - resultMsgsHeight[1])
																		/ 3.0;
		resultMsgsY[0] = Int32(phraseGap);
		resultMsgsY[1] = Int32(resultMsgsHeight[0] + 2 * phraseGap);
		break;

	default:
		SAFE_CLASS_ASSERTION(FALSE);
		break;
	}

	// Okay, time to finalize the X and Y positions of each string and stuff
	// the values into the cheap text.
	for (i = 0; i < currentLineNum; i++)
	{
		Int32 msgX;		// X position at which each string should be positioned
						// so as to align the string according to the
						// 'alignment' parameter.

		// Calculate the X position at which the cheap text should be positioned
		// according to what alignment is desired.
		switch (alignment)
		{
		case LEFT:										// $[TI8]
			msgX = margin;
			break;

		case RIGHT:										// $[TI9]
			msgX = areaWidth - margin - resultMsgsWidth[i];
			break;

		case CENTER:									// $[TI10]
			msgX = (areaWidth - resultMsgsWidth[i]) / 2;
			break;

		default:
			msgX = margin;
//			CLASS_ASSERTION(FALSE);
			break;
		}

		// Okay, let's stuff the correct X and Y position values into the
		// current line.  Remember resultMsgsY[] stores the Y position of
		// the top left of each string whereas the Y position that needs to
		// be inserted into the cheap text is Y position of the *baseline*
		// of the text so we need to do a funky calculation to find
		// the correct Y baseline so as to achieve the Y position stored in
		// resultMsgsY[].

		// Do the X first.  Look for X place marker.
		pValuePlace = wcschr(resultMsgs[i], X_PLACE_MARKER);
		SAFE_CLASS_ASSERTION(pValuePlace);
		  swprintf(tmpValueBuf, L"x=%04d", msgX);
		wcsncpy(pValuePlace, tmpValueBuf, wcslen(tmpValueBuf));

		// Now do the Y baseline value.  Look for Y place marker.
		pValuePlace = wcschr(resultMsgs[i], Y_PLACE_MARKER);
		SAFE_CLASS_ASSERTION(pValuePlace);
		  swprintf(tmpValueBuf, L"%04d",
					resultMsgsY[i] + resultMsgsHeight[i] - resultMsgsDescent[i]);
		wcsncpy(pValuePlace, tmpValueBuf, wcslen(tmpValueBuf));
	}

	// Return number of lines rendered
	return (currentLineNum);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FormatTime	[public, static]
//
//@ Interface-Description
// This method is used for general-purpose formatting of a time/date string.
// The formatting of the time/date display is under control of the caller in a
// way similar to the operation of sprintf().  The arguments are as follows:
//>Von
//	pResultString		A pointer to a character buffer that will be filled
//						with the formatted time/date string.
//	pFormat				A string specifying the time/date format.
//	timeStamp			A reference to a TimeStamp object containing the
//						time/date to be formatted.
//>Voff
// Each character in pFormat is copied directly into pResultString unless
// the character is %.  In this case the following character specifies a
// field descriptor which is replaced by its corresponding value.  The list of
// valid field descriptors is:
//>Von
//	%		The % character
//	m		Month of year in the format 01 to 12
//	n		Month of year in the format Jan, Feb, Mar ...
//	o		Month of year in the format January, February, March ...
//	d		Day of month, 01 to 31
//	e		Day of month, 1 to 31 (no zero padding)
//	t		The time, equivalent to "%H:%M:%S"
//	y		The year, 00 to 99
//	z		The year, 1995 to 2094
//	D		Complete date, equivalent to "%d%n%y"
//	H		Hour, 00 to 23
//	M		Minute, 00 to 59
//	S		Second, 00 to 59
//	T		The time, equivalent to "%H:%M"
//>Voff
// For example, if the pFormat is specified as "Time %T, Date %d %o %z, 10%%"
// then a possible value for pResultString would be "Time 10:24, Date 03
// January 1995, 10%".
//---------------------------------------------------------------------
//@ Implementation-Description
// Go through each character of pFormat, copying it straight into pResultString
// unless the character is '%'.  In that case use a switch statement on the
// value of the next character and copy the corresponding value into
// pResultString.  Stop when a null character is copied to pResultString.
// 
// $[00639] At first release, Sigma must support the following 24-hour time ...
// $[00640] All HH, MM, and SS fields shall be zero filled.
// $[00641] If the time display does not show seconds, the seconds ....
// $[00642] All YY and DD fields shall be zero-filled
// $[00643] Sigma must support the following short date format
// $[00644] Sigma must support the following long data formats ...
//---------------------------------------------------------------------
//@ PreCondition
// pResultString and pFormat must be non-null
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextUtil::FormatTime(  wchar_t *pResultString
                     , const wchar_t *pFormat
                     , const TimeStamp &timeStamp)
{
	CALL_TRACE("TextUtil::FormatTime(char *pResultString, char *pFormat, "
													"TimeStamp &timeStamp)");

	char curChar;
	Int formatIndex = 0;
	Int resultIndex = 0;

    Uint32  month = timeStamp.getMonth();

    if ( month < 1 || month > 12 )
    {	 
        month = 0;
    }	
    
    AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

    DiscreteValue dateFormat = rAccContext.getBatchDiscreteValue(SettingId::DATE_FORMAT );

    Uint32 year = timeStamp.getYear();
    year = year < 2000 ? year - 1900 : year - 2000;



	CLASS_PRE_CONDITION((pResultString != NULL) && (pFormat != NULL));

	// Go through each character in pFormat one by one
	do
	{													// $[TI1]
		curChar = pFormat[formatIndex++];

		// Check for the field descriptor preceder
		if (curChar == '%')
		{												// $[TI2]
			// Got a field descriptor in the next character, let's check what
			// one it is.
			curChar = pFormat[formatIndex++];
			switch (curChar)
			{
			case '%':									// $[TI3]
				// %% gives the % character
				pResultString[resultIndex++] = L'%';
				break;

			case 'm':									// $[TI4]
				// Numeric month, 01 to 12
				resultIndex +=   swprintf(pResultString + resultIndex,
												L"%02d", timeStamp.getMonth());
				break;

			case 'n':									// $[TI5]
            {
				// 3 letter month (Jan, Feb ...)

				resultIndex +=   swprintf(pResultString + resultIndex,
								L"%s", *ShortMonthNames[month]);
				break;
            }

			case 'o':									// $[TI6]
            {
				// Month written out in full.

				resultIndex +=   swprintf(pResultString + resultIndex,
								L"%s", *LongMonthNames[month]);
				break;
            }

			case 'd':									// $[TI7]
				resultIndex +=   swprintf(pResultString + resultIndex,
								L"%02d", timeStamp.getDayOfMonth());
				break;

			case 'e':									// $[TI8]
				resultIndex +=   swprintf(pResultString + resultIndex,
								L"%d", timeStamp.getDayOfMonth());
				break;

			case 't':									// $[TI9]
				resultIndex +=   swprintf(pResultString + resultIndex,
												L"%02d:%02d:%02d", timeStamp.getHour(),
												timeStamp.getMinutes(),
												timeStamp.getSeconds());
				break;

			case 'y':									// $[TI10]
            {

				resultIndex +=   swprintf(pResultString + resultIndex,
													L"%02d", year);
				break;
            }

			case 'z':									// $[TI11]
				resultIndex +=   swprintf(pResultString + resultIndex,
								L"%04d", timeStamp.getYear() );
				break;

			case 'D':									// $[TI12]
            {
				// Full date in format '%d%n%y'
				resultIndex +=   swprintf(pResultString + resultIndex,
						L"%02d%s%02d", timeStamp.getDayOfMonth(),
						*ShortMonthNames[month],  (timeStamp.getYear() % 100));
				break;
            }

			case 'H':									// $[TI13]
				resultIndex +=   swprintf(pResultString + resultIndex,
													L"%02d", timeStamp.getHour());
				break;

			case 'M':									// $[TI14]
				resultIndex +=   swprintf(pResultString + resultIndex,
													L"%02d", timeStamp.getMinutes());
				break;

			case 'S':									// $[TI15]
				resultIndex +=   swprintf(pResultString + resultIndex,
													L"%02d", timeStamp.getSeconds());
				break;

			case 'T':									// $[TI16]
				resultIndex +=   swprintf(pResultString + resultIndex,
												L"%02d:%02d", timeStamp.getHour(),
												timeStamp.getMinutes());
				break;

            case 'P': // Prefered Month, Day and (2 digit) Year
                switch ( dateFormat )
                {

                    case DateFormatValue::DATE_FORMAT_DD_MMM_YY:

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%02d%s'%02d",  
                                                        timeStamp.getDayOfMonth(),
                                                        *ShortMonthNames[month],
                                                        year);

                        break;
                    case DateFormatValue::DATE_FORMAT_DD_MM_YY: 

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%02d/%02d/'%02d",  
                                                        timeStamp.getDayOfMonth(),
                                                        timeStamp.getMonth(),
                                                        year);
                        break;
                    case DateFormatValue::DATE_FORMAT_MM_DD_YY_WITH_DASH:
                    case DateFormatValue::DATE_FORMAT_MM_DD_YY_WITH_SLASH:

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%02d/%02d/'%02d",  
                                                        timeStamp.getMonth(),
                                                        timeStamp.getDayOfMonth(),
                                                        year);
                        break;
                    case DateFormatValue::DATE_FORMAT_YY_MM_DD:

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"'%02d/%02d/%02d",  
                                                        year,
                                                        timeStamp.getMonth(),
                                                        timeStamp.getDayOfMonth()
                                                        );
                        break;
                    case DateFormatValue::DATE_FORMAT_YY_MMM_DD:

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"'%02d%s%02d",  
                                                        year,
                                                        *ShortMonthNames[month],
                                                        timeStamp.getDayOfMonth()
                                                        );
                        break;

                    default:
                        AUX_CLASS_ASSERTION_FAILURE(dateFormat);
                        break;
                }

                break;
            case 'p': // Prefered Month & Day only

                switch ( dateFormat )
                {

                    case DateFormatValue::DATE_FORMAT_DD_MMM_YY:
                    case DateFormatValue::DATE_FORMAT_DD_MM_YY: 

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%02d.%02d",  
                                                        timeStamp.getDayOfMonth(),
                                                        timeStamp.getMonth());
                        break;
                    case DateFormatValue::DATE_FORMAT_MM_DD_YY_WITH_DASH:
                    case DateFormatValue::DATE_FORMAT_YY_MM_DD:
                    case DateFormatValue::DATE_FORMAT_YY_MMM_DD:
                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%02d-%02d",  
                                                        timeStamp.getMonth(),
                                                        timeStamp.getDayOfMonth());
                        break;
                    case DateFormatValue::DATE_FORMAT_MM_DD_YY_WITH_SLASH:
                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%02d/%02d",  
                                                        timeStamp.getMonth(),
                                                        timeStamp.getDayOfMonth());
                        break;
                    default:
                        AUX_CLASS_ASSERTION_FAILURE(dateFormat);
                        break;
                }

                break;


            case 'W': // Wall clock - Month, Day and (4 digit) Year
                switch ( dateFormat )
                {

                    case DateFormatValue::DATE_FORMAT_DD_MMM_YY:

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%02d %s %04d",  
                                                        timeStamp.getDayOfMonth(),
                                                        *ShortMonthNames[month],
                                                        timeStamp.getYear());

                        break;
                    case DateFormatValue::DATE_FORMAT_DD_MM_YY: 

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%02d/%02d/%04d",  
                                                        timeStamp.getDayOfMonth(),
                                                        timeStamp.getMonth(),
                                                        timeStamp.getYear());
                        break;
                    case DateFormatValue::DATE_FORMAT_MM_DD_YY_WITH_DASH:
                    case DateFormatValue::DATE_FORMAT_MM_DD_YY_WITH_SLASH:

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%02d/%02d/%04d",  
                                                        timeStamp.getMonth(),
                                                        timeStamp.getDayOfMonth(),
                                                        timeStamp.getYear());
                        break;
                    case DateFormatValue::DATE_FORMAT_YY_MM_DD:

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%04d/%02d/%02d",  
                                                        timeStamp.getYear(),
                                                        timeStamp.getMonth(),
                                                        timeStamp.getDayOfMonth()
                                                        );
                        break;
                    case DateFormatValue::DATE_FORMAT_YY_MMM_DD:

                        resultIndex +=   swprintf(pResultString + resultIndex,
                                                        L"%04d %s %02d",  
                                                        timeStamp.getYear(),
                                                        *ShortMonthNames[month],
                                                        timeStamp.getDayOfMonth()
                                                        );
                        break;

                    default:
                        AUX_CLASS_ASSERTION_FAILURE(dateFormat);
                        break;
                }

                break;


            default:
				AUX_CLASS_ASSERTION_FAILURE(curChar);
				break;
			}
		}
		else
		{												// $[TI17]
			pResultString[resultIndex++] = curChar;
		}
	}
	while (curChar != '\0');
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
TextUtil::SoftFault(const SoftFaultID  softFaultID,
					   const Uint32       lineNumber,
					   const char*        pFileName,
					   const char*        pPredicate)  
{
	CALL_TRACE("TextUtil::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TEXTUTIL,
							lineNumber, pFileName, pPredicate);
}

