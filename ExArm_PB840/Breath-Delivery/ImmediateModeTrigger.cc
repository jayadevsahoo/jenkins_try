#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ImmediateModeTrigger - Triggers immediately upon being enabled.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers a change in the mode on the next immediate BD
//    cycle.  It is enabled by any object detecting the need to change
//    the mode immediately (eg. operator presses alarm reset...).
//    This class disables itself after firing.
//---------------------------------------------------------------------
//@ Rationale
//    This class is used to trigger a mode change on the next BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of 
//    whether this trigger is enabled or not.  If the trigger is not 
//    enabled, it will always return a state of false.  If enabled,
//    and on the active trigger list in the ModeTriggerMediator, it 
//    will always return true.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//   This class must disable itself after firing.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ImmediateModeTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ImmediateModeTrigger.hh"

//@ Usage-Classes
#include "BreathPhaseScheduler.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ImmediateModeTrigger
//
//@ Interface-Description
//	This constructor takes triggerId as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ using ModeTrigger constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ImmediateModeTrigger::ImmediateModeTrigger(
	const Trigger::TriggerId triggerId
) : ModeTrigger(triggerId)
{
  CALL_TRACE("ImmediateModeTrigger(Trigger::TriggerId triggerId)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ImmediateModeTrigger()  [Destructor]
//
//@ Interface-Description
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ImmediateModeTrigger::~ImmediateModeTrigger(void)
{
  CALL_TRACE("~ImmediateModeTrigger()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ImmediateModeTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, IMMEDIATEMODETRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  It always returns true.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method always returns true.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
ImmediateModeTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");
 
  // TRUE: $[TI1]
  return (TRUE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerAction_
//
//@ Interface-Description
//  This method takes a boolean as an argument and has no return type.
//  Called by determineState to do whatever actions are required,
//  depending on the trigger condition. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Check the trigger condition and if it is true then pass itself to 
//  the breath phase scheduler.  Disable this trigger.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ImmediateModeTrigger::triggerAction_(const Boolean trigCond)
{
  CALL_TRACE("triggerAction_(trigCond)");

  if (trigCond)
  {
    //$[TI1]
    (BreathPhaseScheduler::GetCurrentScheduler()).relinquishControl(*this);
    disable();
  }
  // it is not reachable because once it is enabled, it always return TRUE $[TI2]
}



//=====================================================================
//
//  Private Methods...
//
//=====================================================================

