#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmAugmentation - Holds the common info for an Alarm Matrix row.
//---------------------------------------------------------------------
//@ Interface-Description
// Each OperatingGroup that augments an Alarm references an AlarmAugmentation,
// which, in turn references the augmented Alarm.  When the Operating Parameter
// hierarchy is being updated, each time a group which augments an alarm
// changes state, the OperatingGroup requests the Alarm reference from the
// AlarmAugmentation and messages the Alarm to mark itself for update.
//
// When the Alarm Analysis cluster evaluates marked Alarm objects, all
// OperatingGroup objects referenced through the AlarmAugmentation objects by
// the marked Alarm are messaged to determine their status and the
// AlarmAugmentation's status is updated accordingly.  Active AlarmAugmentation
// objects return their referenced AlarmAnalysisInformation which is merged
// into the current alarm annunciation information.
//---------------------------------------------------------------------
//@ Rationale
// AlarmAugmentation is a virtual base class implementing the common functions
// for the DependentAugmentation and ConditionAugmentation derived classes.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class contains a Boolean which indicates whether the augmentation is
// active or inactive, based on the state of the OperatingGroup that references
// the AlarmAugmentation.  It also contains a field, role_, that indicates to
// users of the class whether the instantiation being messaged is a
// DependentAugmentation or a ConditionAugmentation.
//
// AlarmAugmentation references an AlarmAnalysisInformation class which
// contains information to be annunciated to the operator.
//
// Finally, AlarmAugmentation contains a reference to the Alarm it is
// augmenting.  For DependentAugmentation objects, this is a reference to the
// superior alarm in the superior Alarm/DependentAugmentation/dependent Alarm
// relstionship and for ConditionAugmentation objects it is the alarm object
// part of the OperatingGroup/ConditionAugmentation/Alarm relationship.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmAugmentation.ccv   25.0.4.0   19 Nov 2013 13:51:12   pvcs  $
//
//@ Modification-Log

//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "AlarmAugmentation.hh"

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmAugmentation
//
//@ Interface-Description
//      Constructor.
//
// >Von
//      role    Used to determine if the derived object is a 
//              DependentAugmentation or a ConditionAugmentation.
//      rInfo   Reference to AlarmAnalysisInformation object which contains 
//              analysis and remedy message enumerators, urgency/priority 
//              information and pointers to the System Response and Reset 
//              System Response associated with the augmentation in the Alarm 
//              Matrices.
//      rAlarm  Reference to the superior Alarm in the superior Alarm 
//              DependentAugmentation / dependent Alarm relationship or the
//              Alarm in the OperatingGroup / ConditionAugmentation / Alarm
//              relationship.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// ((role_ == AlarmAugmentation::CONDITION_AUGMENTATION) ||
//  (role_ == AlarmAugmentation::DEPENDENT_AUGMENTATION))
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
AlarmAugmentation::AlarmAugmentation(AlarmAugmentation::AugmentationRole role,
                                     const AlarmAnalysisInformation& rInfo,
                                     Alarm& rAlarm):
                                     role_  (role),
                                     rInfo_ (rInfo),
                                     rAlarm_(rAlarm)
{
  CALL_TRACE("AlarmAugmentation(role, rInfo, rAlarm");
  CLASS_PRE_CONDITION((role_ == AlarmAugmentation::CONDITION_AUGMENTATION) ||
                       (role_ == AlarmAugmentation::DEPENDENT_AUGMENTATION));
  isActive_ = FALSE;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmAugmentation
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
AlarmAugmentation::~AlarmAugmentation(void)
{
  CALL_TRACE("~AlarmAugmentation(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator==
//
//@ Interface-Description
//   Equality operator.
//
// >Von
//   rAug  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  Two AlarmAugmentation objects are equal if their role_, isActive_, rInfo_ 
//  and rAlarm_ attributes are equal.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
Boolean
AlarmAugmentation::operator==(const AlarmAugmentation& rAug) const
{
  CALL_TRACE("operator==(rAug)");

  return((role_     == rAug.role_) && 
         (isActive_ == rAug.isActive_) &&
         (&rInfo_   == &rAug.rInfo_) &&
         (&rAlarm_  == &rAug.rAlarm_)); // $[TI1] $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator!=
//
//@ Interface-Description
//    Inequality operator.
//
// >Von
//    rAug  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    Two AlarmAugmentation objects are equal if their role_, isActive_, 
//    rInfo_ or rAlarm_ attributes are  not equal.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
Boolean
AlarmAugmentation::operator!=(const AlarmAugmentation& rAug) const
{
  CALL_TRACE("operator!=(rAug)");

  return (!(*this == rAug)); // $[TI1] $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
AlarmAugmentation::SoftFault(const SoftFaultID  softFaultID,
                             const Uint32 lineNumber,
                             const char  *pFileName,
                             const char  *pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::ALARMAUGMENTATION,
                          lineNumber, pFileName, pPredicate);
}
