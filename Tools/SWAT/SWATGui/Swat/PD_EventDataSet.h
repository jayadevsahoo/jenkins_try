// PD_EventDataSet.h : Declaration of the CPD_EventDataSet

#pragma once

// code generated on Friday, December 11, 2009, 9:35 AM

class CPD_EventDataSet : public CRecordset
{
public:
	CPD_EventDataSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CPD_EventDataSet)

// Field/Param Data

// The string types below (if present) reflect the actual data type of the
// database field - CStringA for ANSI datatypes and CStringW for Unicode
// datatypes. This is to prevent the ODBC driver from performing potentially
// unnecessary conversions.  If you wish, you may change these members to
// CString types and the ODBC driver will perform all necessary conversions.
// (Note: You must use an ODBC driver version that is version 3.5 or greater
// to support both Unicode and these conversions).

	LONGLONG	m_event_sequence_id;
	CStringA	m_event_descriptor;
	CTime	m_event_time;
	CStringA	m_save_data;

// Overrides
	// Wizard generated virtual function overrides
	public:
	virtual CString GetDefaultConnect();	// Default connection string

	virtual CString GetDefaultSQL(); 	// default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX support

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};


