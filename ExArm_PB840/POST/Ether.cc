#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Ether  - Power on self test, Ethernet Test
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains the methods for testing the Intel 82596
//  Ethernet controller.
//
//  The ethernet test consists of sending the self test command to 
//  the 82596 by one routine and checked later by another
//  routine after enough time has elapsed for the test to complete.
//
//  The 82596 has an extensive self test function that is documented in
//  its reference manuals.  This checksums the internal ROM, checks
//  internal registers, tests its Bus Throttle Timers, and uses an
//  internal version of the diagnose command to test the serial
//  sub-system. The 82696 resets itself following the self-test,
//  therefore initialization and configuration follows the self-test.
// 
//  According to Intel, the self test command takes 4500 CPU clocks +
//  1100 serial clocks, plus the time it takes to post the results to
//  main memory.  Since the results are just a few bytes in this case
//  it is insignificant (1-5 CPU clocks).  Our CPU clock is 24Mhz, our
//  serial clock is based on a 20Mhz clock, divided in half to produce
//  a 10mb/sec communication rate.
// 
//  (4500 clocks * 1/24Mhz) + (1100 clocks * 1/10mhz) = 2.975 x 10-4 sec
//---------------------------------------------------------------------
//@ Rationale
//  These tests satisfy the LAN test functions of POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//    If the registers contain invalid data or the self test fails 
//    or times out the test fails MINOR.
//---------------------------------------------------------------------
//@ Restrictions
//    None
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/Ether.ccv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 002    By: Gary Cederquist  Date: 25-APR-1997  DR Number: 1996
//    Project: Sigma (R8027)
//    Description:
//       Added missing requirement number.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Ether.hh"
#include "CpuDevice.hh"
#include "LanDevice.hh"


// lantest is relocated and aligned to 16 byte boundary by linker
#pragma  option -NZlantest

// LAN_SELF_TEST_RESULTS must be aligned on a 16 byte boundary
static volatile Uint32 LanSelfTestResults_[2];

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: TestStart(void)
//
//@ Interface-Description
//  Resets the LAN and sends the internal self test command to the
//  82596.  Since the test takes approximately .3ms, this module returns
//  immediately after starting the test.  The TestEnd method checks the
//  results.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Criticality 
Ether::TestStart(void)
{
UNIT_TEST_POINT(POSTUT_18)

   const Uint32  LAN_CMD = (Uint32)&LanSelfTestResults_ | LAN_SELF_TEST_CMD;

   //  Release LAN reset
#ifdef E600_840_TEMP_REMOVED
   CpuDevice::SetBitsIoReg1( LAN_RESET_BIT );
#endif

   // ************************************************************
   // **** Initialize the locations where the LAN will write. ****
   // ************************************************************

   LanSelfTestResults_[0] = 0xffffffff;
   LanSelfTestResults_[1] = 0xffffffff;

   // ********** issue the Self Test command to the LAN Chip ***********
   //  Accesses to 82596 CPU PORT must be made as 16-bit accesses
   //  lower part of command word (D15-D0) written first to the
   //  first word of the command port then the upper part (D31-D16)
   //  to the second word of the command port.  As an alternative,
   //  the command can be written twice to the command port as a 32-bit
   //  quantity with the upper and lower parts of the command swapped.

   Uint32 CMD_OUT =   ((LAN_CMD & 0x0000ffff) << 16)
                    | ((LAN_CMD & 0xffff0000) >> 16);

   *P_LAN_PORT     = CMD_OUT;
   *P_LAN_PORT     = CMD_OUT;

   // $[TI1]
   return PASSED;
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: TestEnd (void)
//
//@ Interface-Description
//  This method checks the results of the LAN self test command issued
//  by StartTest.  Returns a MINOR failure if the self test failed.
//  $[11093]
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//  This module checks the results of a LAN Self Test command issued
//  by another module.  The LAN writes to the specified address the 
//  following:
//
//        -----------------------------------------------------------------
//addr+0: |              Signature of the ROM Content                     |
//        |---------------------------------------------------------------|
//addr+4: |0 0 0 S 0 0 0 0 0 0 D T M R 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0| 
//        -----------------------------------------------------------------
//               |             | | | |
//               |             | | | |
//               |             | | | |-ROM Content Result  0 = Pass, 1 = Fail
//               |             | | | 
//               |             | | |---Register    Result  0 = Pass, 1 = Fail
//               |             | |               
//               |             | |-----Bus Timer   Result  0 = Pass, 1 = Fail
//               |             |     
//               |             |-------Diagnose    Result  0 = Pass, 1 = Fail
//               |
//               |---------------------Self-Test   Result  0 = Pass, 1 = Fail
//
//---------------------------------------------------------------------
//@ PreCondition
// Another routine must have started the Self Test at least .3 milliseconds
// before this routine is called.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
#define  LAN_STATUS_OK     (0)
#define  LAN_ROM_SIGNATURE (0x25AE3209L)

Criticality 
Ether::TestEnd (void)
{
UNIT_TEST_POINT(POSTUT_18_1)

   if ( LanSelfTestResults_[0] != LAN_ROM_SIGNATURE )
   {
      // $[TI1.1]
      return MINOR;
   }
      // $[TI1.2]

   if ( LanSelfTestResults_[1] != LAN_STATUS_OK )
   {
      // $[TI2.1]
      return MINOR;
   }
      // $[TI2.2]

   return PASSED;
}

