#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PcvPhase - Implements pressure control ventilation phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from PressureBasedPhase class.  No public
//		methods are defined by this class since base class methods are used.
//		Protected virtual methods are implemented to support the base class.
//---------------------------------------------------------------------
//@ Rationale
// 		This class implements the algorithms for pressure controlled
// 		inspirations.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The data members values from the base class are defined	in the pure
//		virtual methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PcvPhase.ccv   25.0.4.0   19 Nov 2013 14:00:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: jja     Date:  31-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//      Added logic to handle a test breath for VTPC.
//
//  Revision: 005  By: sah     Date:  22-Sep-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  optimized implementation of 'timeToTarget_' calculation, to
//         eliminate unnecessary run-time calculations
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "PcvPhase.hh"

#include "TriggersRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "PhasedInContextHandle.hh"
#include "BreathTrigger.hh"
#include "O2Mixture.hh"
#include "BD_IO_Devices.hh"
#include "MandTypeValue.hh"
#include "Peep.hh"
#include "BreathPhaseScheduler.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PcvPhase()
//
//@ Interface-Description
//		Constructor.  This method has the inspiratory pressure id, inspiratory
//      time id, and the flow acceleration percent as arguments.  The rO2Mixture
//		object should be constructed before this class is constructed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	The setting id's (inspPressId, inspTimeId, and flowAccelPercentId)
//  	are initialized to the values passed in.  Register breath phase to
//		O2Mixture.  The base class constructor is called.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PcvPhase::PcvPhase( const SettingId::SettingIdType inspPressId,
    		    const SettingId::SettingIdType inspTimeId,
    		    const SettingId::SettingIdType flowAccelPercentId)
 : PressureBasedPhase()  	// $[TI1]
{
	CALL_TRACE("PcvPhase::PcvPhase( SettingId inspPressId,\
    		  		SettingId inspTimeId,\
    		  		SettingId flowAccelPercentId)") ;

  	// $[TI2]
	inspPressId_ = inspPressId ;
	inspTimeId_ = inspTimeId ;
	flowAccelPercentId_ = flowAccelPercentId ;

	RO2Mixture.registerBreathPhaseCallBack( this) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PcvPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PcvPhase::~PcvPhase(void)
{
	CALL_TRACE("PcvPhase::~PcvPhase(void)") ;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PcvPhase::SoftFault( const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
		   			 const char*        pFileName,
		   			 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PCVPHASE,
                          	 lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineEffectivePressureAndBiasOffset_
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to determine the
//		effective pressure and the bias offset. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		The biasOffset = 0.  The effective pressure is the sum of
//		InspPress and biasOffset_.
// 		$[04173]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PcvPhase::determineEffectivePressureAndBiasOffset_( void)
{
	CALL_TRACE("PcvPhase::determineEffectivePressureAndBiasOffset_( void)") ;

	biasOffset_ = 0.0F ;

	// Special case for VTPC setup -- VCV test breaths failed criteria, so
	// we try a PC breath until criteria is met

	if (PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE) ==
			MandTypeValue::VCP_MAND_TYPE &&
		BreathPhaseScheduler::GetCurrentScheduler().getId() != SchedulerId::APNEA)
	{
	  	// $[TI1]
		// $[VC24015]
		Real32 hcp = PhasedInContextHandle::GetBoundedValue( SettingId::HIGH_CCT_PRESS).value ;
		Real32 peep = RPeep.getActualPeep() ;
		
		effectivePressure_ = MIN_VALUE( 15.0F, hcp - 3.0F - peep) + biasOffset_ ;
	}
	else
	{
	  	// $[TI2]
		const BoundedValue& rInspPress =
					PhasedInContextHandle::GetBoundedValue( inspPressId_) ;

		effectivePressure_ = rInspPress.value + biasOffset_ ;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineTimeToTarget_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to	determine the time to
//      target.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The time to target is 2/3 * inspiratory time or 2000 msec, whichever
// 		is less.
// 		$[04175]	
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PcvPhase::determineTimeToTarget_( void)
{
	CALL_TRACE("PcvPhase::determineTimeToTarget_( void)") ;
	
  	// $[TI1]
	const Real32  INSP_TIME_VALUE =
					PhasedInContextHandle::GetBoundedValue(inspTimeId_).value ;

	static const Real32  FACTOR_ = (2.0f / 3.0f);

	timeToTarget_ = MIN_VALUE( (FACTOR_ * INSP_TIME_VALUE), 2000.0F) ;

	inspTimeMs_ = INSP_TIME_VALUE ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineFlowAccelerationPercent_( void)
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to	determine the flow
//      acceleration percent.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Obtain fap from phased in settings.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PcvPhase::determineFlowAccelerationPercent_( void)
{
	CALL_TRACE("PcvPhase::determineFlowAccelerationPercent_( void)") ;
	
  	// $[TI1]
	const BoundedValue& rFlowAccelPercentage =
					PhasedInContextHandle::GetBoundedValue( flowAccelPercentId_) ;

	fap_ = rFlowAccelPercentage.value ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableExhalationTriggers_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to	enable exhalation
//      triggers.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		ImmediateExpTrigger is enabled one BD cycle before the inspiratory
// 		time has expired.
//		$[04026] $[04293] 
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PcvPhase::enableExhalationTriggers_()
{
	CALL_TRACE("PcvPhase::enableExhalationTriggers_()") ;
	
	if (elapsedTimeMs_ >= inspTimeMs_ - CYCLE_TIME_MS)
	{
	  	// $[TI1.1]
   		((BreathTrigger&)RImmediateExpTrigger).enable() ;
   	} 	// implied else $[TI1.2]
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================













