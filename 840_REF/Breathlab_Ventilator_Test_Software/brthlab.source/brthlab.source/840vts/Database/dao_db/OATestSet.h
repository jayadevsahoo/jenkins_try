#if !defined(AFX_OATESTSET_H__617D124F_54A5_11D3_9C97_004005519CCD__INCLUDED_)
#define AFX_OATESTSET_H__617D124F_54A5_11D3_9C97_004005519CCD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OATestSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COATestSet DAO recordset

class COATestSet : public CDaoRecordset
{
public:
	COATestSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(COATestSet)

// Field/Param Data
	//{{AFX_FIELD(COATestSet, CDaoRecordset)
	long	m_TID;
	long	m_TName;
	long	m_TPFInd;
	long	m_Sequence;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COATestSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OATESTSET_H__617D124F_54A5_11D3_9C97_004005519CCD__INCLUDED_)
