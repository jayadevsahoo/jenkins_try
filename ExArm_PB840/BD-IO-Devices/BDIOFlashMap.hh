#ifndef BDIOFlashMap_HH
#define BDIOFlashMap_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct: BDIOFlashMap - Map layout of the contents of flash memory for
//		   the calibration data.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BDIOFlashMap.hhv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "FlowSensorCalInfo.hh"
#include "ExhValveTable.hh"
#include "CrcUtilities.hh"
#include "CalInfoFlashSignature.hh"
#include "CalibrationTable.h"

//	CHANGES TO THE ASSUMPTIONS BELOW REQUIRES CHANGES TO CalInfoFlashBlock CLASS.

struct BDIOFlashMap
{
	// the serial number must be the first 12 bytes in this struct

	char serialNumber[12] ;

	// 	CalInfoFlashBlock class assumes that airCalInfo is the first data
	//	type in this structure and exhO2CalInfoCrc is the last data type for
	// 	the flow sensor calibration data block.

	FlowSensorCalInfo airCalInfo ;
	CrcValue airCalInfoCrc ;

	FlowSensorCalInfo o2CalInfo ;
	CrcValue o2CalInfoCrc ;

    ExhSensorTableStorage_t exhCalInfo ;
	CrcValue exhCalInfoCrc ;

	//  CalInfoFlashBlock class assumes that exhValveCalInfoSignature is the
	//	first data type and exhValveCalInfoCrc is the last data type for the
	//	exhalation valve calibration data block.

	CalInfoFlashSignature::Signature
	exhValveCalInfoSignature ;
	ExhValveTable exhValveCalInfo ;
	CrcValue exhValveCalInfoCrc ;

} ;

#endif // BDIOFlashMap_HH
