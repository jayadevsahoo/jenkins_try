#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Trigger - A trigger is an object that monitors the system for 
//    a specific condition that, when it occurs, causes the state of 
//    the system to change.  This class is an abstract base class for 
//    any type of trigger the Breath Delivery system may need.
//---------------------------------------------------------------------
//@ Interface-Description
//    A Trigger may be enabled or disabled.  When enabled, it notifies 
//    its TriggerMediator when the condition that it has been set up 
//    to detect has occured.  Depending upon the type of Trigger, it 
//    will also notify other types of objects when its condition is true.
//---------------------------------------------------------------------
//@ Rationale
//    There are many types of conditions in the breath delivery system 
//    that may trigger changes in the state of the system.  Examples 
//    include BreathTriggers that cause the BreathPhase to change, 
//    and ModeTriggers that cause the mode of breathing to change.  
//    This class provides an abstract base class for all types of 
//    triggers.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Trigger has a boolean state data member that is used to determine 
//    if the trigger is active.  There are methods for setting the state 
//    (enabling and disabling the Trigger).   The method for checking 
//    the condition that the trigger is set up to detect is a pure virtual 
//    function, and must be defined for each derived trigger class.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    This is an abstract base class for many types of triggers.  
//    It can not be instanciated.  A trigger does not have a newCycle(), 
//    so it must be instructed to determine its state by a mediator.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Trigger.ccv   25.0.4.0   19 Nov 2013 14:00:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  29-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection Rework.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Trigger.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

#ifdef SIGMA_UNIT_TEST
 
#include "Ostream.hh"
 
#endif //SIGMA_UNIT_TEST

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Trigger 
//
//@ Interface-Description
//	This constructor takes unique trigger id as an argument.  All triggers
//  are constructed to be in a disabled state.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize data member triggerId_ to the argument id.	
//	Disable the trigger by setting data member isEnabled_ to false.
//---------------------------------------------------------------------
//@ PreCondition
//      The triggerId must be in the valid range of id's.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Trigger::Trigger(const TriggerId id)
{
  CALL_TRACE("Trigger(TriggerId id)");

  CLASS_PRE_CONDITION( id >= Trigger::FIRST_BREATH_TRIGGER &&
	id < Trigger::NUM_TRIGGER);

  // $[TI1]
  triggerId_ = id;
  isEnabled_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Trigger 
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Trigger::~Trigger()
{
  CALL_TRACE("~Trigger()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable()
//
//@ Interface-Description
//     This method takes no parameters and has no return value.
//     By setting the isEnabled_ data member to true, this trigger becomes 
//     enabled, and is able to detect the trigger condition it is set up 
//     to monitor.  The object enabling a trigger must contain the 
//     algorithm for determining when it should be active.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The state data member isEnabled_ is set to true, activating this 
//     trigger.  This method is virtual because some triggers may require 
//     initialization.  The triggers that require initialization can have
//     their own enable methods.
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//     none
//@ End-Method
//=====================================================================
void
Trigger::enable(void)
{
  CALL_TRACE("enable(void)");

  // $[TI1]
  isEnabled_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disable
//
//@ Interface-Description
//     This method takes no parameters and has no return value.
//     By setting the isEnabled_ data member to false, this trigger
//     becomes disabled, and is no longer able to detect the trigger
//     condition it is set up to monitor.  The object disabling this
//     trigger must contain the algorithm for determining when it
//     should be inactive.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The state data member isEnabled_ is set to false, deactivating
//     this trigger.
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//     none
//@ End-Method
//=====================================================================
void
Trigger::disable(void)
{
  CALL_TRACE("disable(void)");
 
  // $[TI1]
  isEnabled_ = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineState
//
//@ Interface-Description
//    This method takes no parameters.  It returns a Boolean, indicating
//    the state of the trigger condition.  The condition is only checked
//    if the trigger is enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If this trigger is disabled, this method will always return a state
//    of false.  It the trigger is enabled, the trigger condition will be
//    checked.  The state of the condition will then be returned through
//    this method and passed to the method triggerAction.
//---------------------------------------------------------------------
//@ PreCondition
//              none
//---------------------------------------------------------------------
//@ PostCondition
//              none
//@ End-Method
//=====================================================================
Boolean
Trigger::determineState(void)
{
  CALL_TRACE("determineState(void)");
 
  Boolean rtnValue = FALSE;
  if (isEnabled_)
  {
    // $[TI1]
    rtnValue = triggerCondition_();
    triggerAction_(rtnValue);
  }
  // $[TI2]
 
  //TRUE: $[TI3]
  //FALSE: $[TI4]
  return (rtnValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
Trigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, TRIGGER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
// Method: write 
//
// Interface-Description
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void
Trigger::write(void)
{
  CALL_TRACE("write(void)");

  cout << "triggerId_:" << (int)triggerId_ << endl; 
  cout << "isEnabled_:" << (int)isEnabled_ << endl; 
}

#endif //SIGMA_UNIT_TEST
