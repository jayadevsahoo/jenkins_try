#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OscPcvPhase - Implements OSC PCV phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from PressureBasedPhase class.  No public
//		methods are defined by this class since base class methods are used.
//		Protected virtual methods are implemented to support the base class.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms for pressure controlled
//		inspirations during OSC.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The data members values from the base class are defined	in the pure
//		virtual methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OscPcvPhase.ccv   25.0.4.0   19 Nov 2013 13:59:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: syw     Date:  14-Feb-2000    DR Number: 5639
//  Project:  NeoMode
//  Description:
//		Implement new gains (kp and ki) during neonatal OSC.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added determineTargetPressure_() method since Settings will not
//			update the setting to the proper value as initially assumed.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "OscPcvPhase.hh"

#include "TriggersRefs.hh"
#include "ControllersRefs.hh"

//@ Usage-Classes

#include "BreathTrigger.hh"
#include "PressureController.hh"
#include "BD_IO_Devices.hh"
#include "PhasedInContextHandle.hh"
#include "PatientCctTypeValue.hh"

//@ End-Usage

//@ Code...

// $[04205]
//@ Constant: OSC_INSP_TIME_MS
// insp time during OscPcvPhase
static const Real32 OSC_INSP_TIME_MS = 2500.0F ;

//@ Constant: OSC_INSP_PRESS
// insp pressure during OscPcvPhase
static const Real32 OSC_INSP_PRESS = 15.0F ;

//@ Constant: OSC_FLOW_ACCEL_PERCENT
// fap during OscPcvPhase
static const Real32 OSC_FLOW_ACCEL_PERCENT = 100.0F ;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OscPcvPhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Call base class constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OscPcvPhase::OscPcvPhase(void)
 : PressureBasedPhase()  	// $[TI1]
{
	CALL_TRACE("OscPcvPhase::OscPcvPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OscPcvPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OscPcvPhase::~OscPcvPhase(void)
{
	CALL_TRACE("OscPcvPhase::~OscPcvPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
OscPcvPhase::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, OSCPCVPHASE,
                          	 lineNumber, pFileName, pPredicate) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineEffectivePressureAndBiasOffset_
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to determine the effective
//      pressure and the biasOffset. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		The biasOffset = 0.  The effective pressure is the sum of
//		OSC_INSP_PRESS and biasOffset_.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//     	none
//@ End-Method
//=====================================================================

void
OscPcvPhase::determineEffectivePressureAndBiasOffset_( void)
{
	CALL_TRACE("OscPcvPhase::determineEffectivePressureAndBiasOffset_( void)") ;
	
  	// $[TI1]
	biasOffset_ = 0.0F ;
	effectivePressure_ = OSC_INSP_PRESS + biasOffset_ ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineTimeToTarget_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to	determine the time to
//      target.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The time to target is 2/3 * inspiratory time.  The inspiratory time
// 		is OSC_INSP_TIME minus safety valve close time.		
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscPcvPhase::determineTimeToTarget_( void)
{
	CALL_TRACE("OscPcvPhase::determineTimeToTarget_( void)") ;
	
  	// $[TI1]
	timeToTarget_ = 2.0F / 3.0F * (OSC_INSP_TIME_MS - SV_CLOSE_TIME_MS) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineFlowAccelerationPercent_( void)
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to	determine the flow
//      acceleration percent.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The flow acceleration percentage is set to OSC_FLOW_ACCEL_PERCENT.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscPcvPhase::determineFlowAccelerationPercent_( void)
{
	CALL_TRACE("OscPcvPhase::determineFlowAccelerationPercent_( void)") ;
	
  	// $[TI1]
	fap_ = OSC_FLOW_ACCEL_PERCENT ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableExhalationTriggers_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to	enable exhalation
//      triggers.
//---------------------------------------------------------------------
//@ Implementation-Description
//		rImmediateExpTrigger is enabled one BD cycle before the inspiratory
//		time has expired.  The inspiratory time is OSC_INSP_TIME minus the
//		safety valve close time.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscPcvPhase::enableExhalationTriggers_()
{
	CALL_TRACE("OscPcvPhase::enableExhalationTriggers_()") ;
	
	if (elapsedTimeMs_ >= OSC_INSP_TIME_MS - SV_CLOSE_TIME_MS - CYCLE_TIME_MS)
	{
	  	// $[TI1.1]
   		((BreathTrigger&)RImmediateExpTrigger).enable() ;
   	}	// implied else $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineKp_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to determine the
//      proportional gain.  This method will overload the base class defined 
//      method.
// 		$[04307]	
//---------------------------------------------------------------------
//@ Implementation-Description
//		The proportional gain is set to zero for ADULT and PEDIATRIC
//		circuit types, 0.5 for NEONATAL.
//---------------------------------------------------------------------
//@ PreCondition
//      PATIENT_CCT_TYPE == ADULT_CIRCUIT || PEDIATRIC_CIRCUIT || NEONATAL_CIRCUIT
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscPcvPhase::determineKp_( void)
{
	CALL_TRACE("OscPcvPhase::determineKp_( void)") ;
	
	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	switch (PATIENT_CCT_TYPE)
	{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :
		  	// $[TI1]
			kp_ = 0.0F ;
			break ;

		case PatientCctTypeValue::NEONATAL_CIRCUIT :
		  	// $[TI2]
			kp_ = 0.5F ;
			break ;

		default :
			// unexpected circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE);
			break;
	}		
} 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineKi_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called to update the integral gain.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The integral gain value is dependent upon patient circuit type.
// 		$[04183]	
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscPcvPhase::determineKi_( void)
{
	CALL_TRACE("PressureBasedPhase::determineKi_( void)") ;

    const DiscreteValue  CIRCUIT_TYPE =
        PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	// call base class to get gains and override value for NEONATAL_CIRCUIT
	PressureBasedPhase::determineKi_() ;

	if (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
	{
	  	// $[TI1]
		ki_ = 0.008 ;
	}
  	// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePressureControllerWf_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method
//      will set the pressureController weighting factor (wf) such that the 
//      pressure will use the inspiratory pressure signal for feedback. 
//---------------------------------------------------------------------
//@ Implementation-Description
//      The pressure controller's wf is set to one.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscPcvPhase::updatePressureControllerWf_( void)
{
	CALL_TRACE("OscPcvPhase::updatePressureControllerWf_( void)") ;
	
  	// $[TI1]
	RPressureController.setWf( 1.0F) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineTargetPressure_()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to determine the target pressure.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The targetPressure_ is set to OSC_INSP_PRESS. 
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
OscPcvPhase::determineTargetPressure_( void)
{
	CALL_TRACE("PressureBasedPhase::determineTargetPressure_( void)") ;

  	// $[TI1]
	targetPressure_ = OSC_INSP_PRESS ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================











