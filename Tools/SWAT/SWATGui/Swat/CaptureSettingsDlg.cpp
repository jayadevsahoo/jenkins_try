// CaptureSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Swat.h"
#include "CaptureSettingsDlg.h"

#define REFRESH_ID 200

// CCaptureSettingsDlg dialog

IMPLEMENT_DYNAMIC(CCaptureSettingsDlg, CDialog)

CCaptureSettingsDlg::CCaptureSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCaptureSettingsDlg::IDD, pParent)
{

}

CCaptureSettingsDlg::~CCaptureSettingsDlg()
{
}

void CCaptureSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SETTINGS, m_List);
}


BEGIN_MESSAGE_MAP(CCaptureSettingsDlg, CDialog)
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_SETTINGS, OnCustomDrawList1)

	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CCaptureSettingsDlg::OnBnClickedButtonSave)
END_MESSAGE_MAP()


// CCaptureSettingsDlg message handlers

BOOL CCaptureSettingsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	//m_List.SetExtendedStyle( LVS_EX_FULLROWSELECT | LVS_EX_ONECLICKACTIVATE | LVS_EX_UNDERLINEHOT | LVS_EX_GRIDLINES );

	m_List.SetExtendedStyle( LVS_EX_FULLROWSELECT | LVS_EX_ONECLICKACTIVATE  | LVS_EX_GRIDLINES | LVS_EDITLABELS );

	InsertColumns();

	Refresh();
	
	SetTimer(REFRESH_ID, 500, NULL);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}




void CCaptureSettingsDlg::InsertColumns()
{
	LV_COLUMN Column;
	int nCol;

	Column.mask = LVCF_TEXT|LVCF_WIDTH ;
	CString szText;
	
	szText = "Parameter";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	
	Column.cx = 170;
	nCol = m_List.InsertColumn( 0, &Column );


	Column.mask = LVCF_TEXT|LVCF_WIDTH ;
	
	szText = "Current Value";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	
	Column.cx = 165;
	nCol = m_List.InsertColumn( 1, &Column );

	szText = "Last Value";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	
	Column.cx = 160;
	nCol = m_List.InsertColumn( 2, &Column );


}

void CCaptureSettingsDlg::Refresh()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	if (! ::IsWindow(m_hWnd)) return;

	m_List.DeleteAllItems();

	CString strFromSettingCapture = pApp->m_strSettings0; //Latest settings
	for(int n = 1; n < 59; n++)
	{
		int nNewLine = strFromSettingCapture.Find('\n');
		if( nNewLine == -1) break;

		DisplayValues(strFromSettingCapture.Left(nNewLine), false, 0); //Display each line
		strFromSettingCapture = strFromSettingCapture.Right(strFromSettingCapture.GetLength()-nNewLine -1);// -1 for new line
	}

	strFromSettingCapture = pApp->m_strSettings1; //old settings
	for(int n = 1; n < 59; n++) //max 50 values
	{
		int nNewLine = strFromSettingCapture.Find('\n');
		if( nNewLine == -1) break;

		DisplayValues(strFromSettingCapture.Left(nNewLine), true, 2); //update the "old" column only
		strFromSettingCapture = strFromSettingCapture.Right(strFromSettingCapture.GetLength()-nNewLine -1);// -1 for new line
	}
	pApp->m_bNewSettingsRcvd = false;


}
void CCaptureSettingsDlg::DisplayValues(CString strLine, bool bOnlyValue, int nColumnNumber)
{
	//takes one line item and shows in 

	if (! ::IsWindow(m_hWnd)) return;

	CString strID, strParam, strType, strValue;

	int n = strLine.Find(',');
	strID = strLine.Left(n);
	strLine = strLine.Right(strLine.GetLength()-n-1);

	n = strLine.Find(',');
	strParam = strLine.Left(n);
	strLine = strLine.Right(strLine.GetLength()-n-1);

	n = strLine.Find(',');
	strType = strLine.Left(n);
	strValue = strLine.Right(strLine.GetLength()-n-1);
	
	//AddListEntry(m_List.GetItemCount(), strParam, strValue);

	if(bOnlyValue) //Update only old values column
	{
		
		m_List.SetItemText(atoi(strID), nColumnNumber, strValue);

	}
	else //insert new line items
	{
		int nIndex = atoi(strID);
		m_List.InsertItem(nIndex, strParam);
		m_List.SetItemText(nIndex, 1, strValue);

	}



}









void CCaptureSettingsDlg::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
	CDialog::PostNcDestroy();
}

void CCaptureSettingsDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	if(nIDEvent == REFRESH_ID && pApp->m_bNewSettingsRcvd == true)
	{
		pApp->m_bNewSettingsRcvd = false;
		Refresh();
	}

	CDialog::OnTimer(nIDEvent);
}





void CCaptureSettingsDlg::OnCustomDrawList1 ( NMHDR* pNMHDR, LRESULT* pResult )
{
	
	//SAMPLE: for this notification, the structure is actually a
	// NMLVCUSTOMDRAW that tells you what's going on with the custom
	// draw action. So, we'll need to cast the generic pNMHDR pointer.
	LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	//SAMPLE: The first notification we'll receive is CDDS_PREPAINT,
	// right at the beginning of the paint cycle. We'll want to return
	// item-specific notifications so we can deal with things the right
	// way.

	if (lplvcd->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	// otherwise, we might be really getting a notification about
	// painting something.
	else if (lplvcd->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	// Because we returned CDRF_NOTIFYITEMDRAW in response to
    // CDDS_PREPAINT, CDDS_ITEMPREPAINT is sent when the control is
    // about to paint an item.
	{
		// You can select a font here, if you want:
		//		::SelectObject(lplvcd->mncd.hdc, hYourFont);

		// For this example, we'll just see which row we're in.
		// if it's an even-numbered row, we'll paint yellow on
		// a blue background. If it's an odd-numbered row,
		// we'll let the control do whatever it wants.

		// Note that, if we make a change, we have to express the
		// change to the control.

		//if (lplvcd->nmcd.dwItemSpec == 3)

		if(m_List.GetItemText(lplvcd->nmcd.dwItemSpec, 2).IsEmpty())// No last value to compare with return
		{
			*pResult = 0;
			return;
		}


		// If column 1 and 2 doesn't match, show row in red
		if(m_List.GetItemText(lplvcd->nmcd.dwItemSpec, 1) != m_List.GetItemText(lplvcd->nmcd.dwItemSpec, 2))
		{
            lplvcd->clrText = RGB(255, 0, 0);
           // lplvcd->clrTextBk = RGB(0, 10, 103);
            *pResult = CDRF_NEWFONT;
		}
		else
		{
			// no change; tell the control to do the default
			*pResult = CDRF_DODEFAULT;
        }
	}
	else
	{
		// it wasn't a notification that was interesting to us.
		// just return zero.
		*pResult = 0;
	}
}

void CCaptureSettingsDlg::OnBnClickedButtonSave()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(m_List.GetSelectedCount() < 1) return;

	int nSelected = m_List.GetNextItem(-1, LVNI_SELECTED );

	
	CString strLine = m_List.GetItemText(nSelected, 0 );
	if(strLine.IsEmpty()) return; 

	//copy strLine to the clipboard
	HGLOBAL hMem;
	hMem = GlobalAlloc(GMEM_FIXED, strLine.GetLength()+1);
	if(hMem == NULL) { 
		pApp->m_SWAT_log_file.WriteString("GlobalAlloc failed.");
		pApp->m_SWAT_log_file.Flush();
		return;
	}
	strcpy((char *) GlobalLock(hMem), strLine.GetBuffer());
	strLine.ReleaseBuffer();
	GlobalUnlock(hMem);
	::OpenClipboard(m_hWnd);
	EmptyClipboard();
	SetClipboardData(CF_TEXT, hMem);
	CloseClipboard();
}
