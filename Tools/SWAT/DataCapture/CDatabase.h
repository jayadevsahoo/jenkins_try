//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file CDatabase.h
//----------------------------------------------------------------------------

#ifndef CDATABASE_H
#define CDATABASE_H

#include<string>
#include<vector>
#include<map>
#include"Mutex.h"

/// @brief Predecl of pg_conn.
struct pg_conn;

/// @brief Renaming of pg_conn. 
typedef struct pg_conn PGconn;


/// @brief  
/// CCol is a container for a single data element.
class CCol
{
    public:

    /// @brief CCol
    CCol();


    /// @brief CCol
    /// 
    /// @param std::string column value 
    CCol(std::string);

    /// @brief &get
    /// 
    /// @return 
    std::string &get();

    private:

    /// @brief 
    std::string m_string;
};



/// @brief 
/// A CRow is used to contain a database row.
class CRow
{
    public:

    /// @brief CRow
    /// 
    /// @param rColNameMap 
    CRow(std::vector<std::string> &rColNameMap);


    /// @brief &getCol
    /// 
    /// @param name 
    /// 
    /// @return 
    CCol &getCol(std::string name);


    /// @brief setCol
    /// 
    /// @param name 
    /// @param value 
    /// 
    /// @return 
    bool setCol(std::string name, std::string value);


    /// @brief operator=
    /// 
    /// @param rCRow 
    /// 
    /// @return 
    CRow& operator=(const CRow& rCRow);

    private:


    /// @brief 
    static CCol m_Col;

    /// @brief 
    std::vector<std::string> &m_ColNameMap;


    /// @brief 
    std::map<unsigned int,CCol> m_CellVector;
};



/// @brief 
/// A CResult is made up a a full result set from the database.
class CResult
{
    public:

    /// @brief CResult
    CResult();


    /// @brief addColumn
    /// 
    /// @param colName 
    /// 
    /// @return 
    bool addColumn(std::string colName);


    /// @brief &getCRowVector
    /// 
    /// @return 
    const std::vector<CRow> &getCRowVector() const;


    /// @brief *newCRow
    /// 
    /// @return 
    CRow *newCRow();

    private:

    /// @brief 
    std::vector<std::string> m_ColNameMap;


    /// @brief 
    std::vector<CRow> m_CRowVector;

    /// @brief
    ///TODO[ME]: Impliment a way to retreive status.
    ///Rusult should indicate failure/success and type of success
    unsigned m_CResult;
    
};



/// @brief 
/// A CDatabase is a interface to the database.
class CDatabase
{
    public:

    /// @brief CDatabase
    /// 
    /// @param ConnectionInfoString 
    CDatabase(std::string ConnectionInfoString);

    /// @brief ~CDatabase
    ~CDatabase();

    /// @brief insert_query
    /// 
    /// @param query 
    /// 
    /// @return 
    bool insert_query(std::string query);


    /// @brief select_query
    /// 
    /// @param query 
    /// 
    /// @return 
    CResult*  select_query(std::string query);


    /// @brief cleanup 
    /// 
    /// @param query 
    /// 
    /// @return 
    bool cleanup(std::string query);

    private:
    CDatabase(const CDatabase&);  //Not implimented
    CDatabase operator=(const CDatabase&);  //Not implimented

    /// @brief 
    PGconn *m_pPGconn;

    /// @brief 
    /// TODO[ME] Add logic to recover connection, and throw on compleate loss.
    bool m_bConnected;

    /// @brief 
    Mutex *m_DatabaseMutex;
};


#endif
