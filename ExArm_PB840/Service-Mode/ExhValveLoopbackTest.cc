#include "stdafx.h"
//=======================================================================
// This is a proprietary work to which Puritan-Bennett Corporation
// claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or,
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or, otherwise with the prior written permission of
// Puritan-Bennett Corporation.
//
//		Copyright (c) 1996, Puritan-Bennett Corporation
//
//=======================================================================


// ============================= C L A S S  D E S C R I P T I O N =======
//@ Class:  ExhValveLoopbackTest - Tests the Exhalation Valve
//	loopback current.
//-----------------------------------------------------------------------
//@ Interface-Description
//  The class implements the exhalation valve loopback test,
//	which compares the EV command current to the EV loopback current
//  at various EV command levels.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the exhalation valve loopback test.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The  method runTest() is responsible for testing the exhalation valve
//  loopback current.
//-----------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//-----------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//-----------------------------------------------------------------------
//@ Invariants
//-----------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ExhValveLoopbackTest.ccv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//          Initial version (Integration baseline).
//
//=======================================================================

#include "ExhValveLoopbackTest.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmManager.hh"
#include "SmUtilities.hh"

#include "Task.hh"

#include "ValveRefs.hh"
#include "ExhalationValve.hh"

#ifdef SIGMA_DEVELOPMENT
# include <stdio.h>
#endif  // SIGMA_DEVELOPMENT

#ifdef SIGMA_UNIT_TEST
#include "SmUnitTest.hh"
#include "FakeIo.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//@ Constant: EV_LOOPBACK_MAX_GAIN_ERROR
// max differential gain error, EV driver vs EV loopback
const Real32 EV_LOOPBACK_MAX_GAIN_ERROR = 0.1;

//@ Constant: EV_LOOPBACK_MAX_OFFSET_ERROR
// max differential offset error, EV driver vs EV loopback
const Real32 EV_LOOPBACK_MAX_OFFSET_ERROR = 82.0;  // mA

//@ Constant: NOMINAL_EV_DRIVER_OFFSET
// nominal EV driver current offset
const Real32 NOMINAL_EV_DRIVER_OFFSET = 60.0;  // mA


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  ExhValveLoopbackTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.  No arguments. Initializes the test point array.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the EV DAC commands for each test point.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
ExhValveLoopbackTest::ExhValveLoopbackTest(void)
{
// $[TI1]
	CALL_TRACE("ExhValveLoopbackTest::ExhValveLoopbackTest(void)");

	exhValveCommand_[0] = 300;
	exhValveCommand_[1] = 1500;
	exhValveCommand_[2] = 3000;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  ~ExhValveLoopbackTest(void)
//
//@ Interface-Description
//  Destructor
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
ExhValveLoopbackTest::~ExhValveLoopbackTest(void)
{
  CALL_TRACE("ExhValveLoopbackTest::~ExhValveLoopbackTest(void)");
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  runTest(void)
//
//@ Interface-Description
//  This method is used to perform the exhalation valve loopback test.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09062]
//	Set EV damping gain to test level.
//	For each test point:
//      Ramp the EV to the next test point.
//      Wait for the valve to stabilize.
//      Read the EV loopback current.
//		Calculate the nominal EV command current.
//      If the EV loopback current is out of range with respect to the
//		EV command current, generate an ALERT.
//	Open the exhalation valve (i.e. set EV damping gain to 26
//	and EV command to 0).
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
void
ExhValveLoopbackTest::runTest(void)
{
	CALL_TRACE("ExhValveLoopbackTest::runTest(void)");

	Boolean testPassed = TRUE;
	
// E600 BDIO	REVDampingGainDacPort.writeRegister(205);

	for (Int32 testNumber = 0;
	     testNumber < EVLT_NUMBER_OF_TEST_POINTS;
	     testNumber++)
	{
	// $[TI1]
		// Close the exh valve to the test point
		RSmUtilities.commandExhValve (exhValveCommand_[testNumber]);

		// Allow exh valve to settle
		Task::Delay(0, 200);

		// Read the loopback current
		Real32 loopbackCurrent = RExhalationValve.getExhValveCurrent();

		RSmManager.sendTestPointToServiceData(loopbackCurrent);

		// Compute the nominal EV command current
		Real32 commandCurrent = exhValveCommand_[testNumber] * EV_COUNTS_TO_MA
								- NOMINAL_EV_DRIVER_OFFSET;

		// Compute the max current difference
		Real32 maxCurrentDiff = loopbackCurrent * EV_LOOPBACK_MAX_GAIN_ERROR +
							EV_LOOPBACK_MAX_OFFSET_ERROR;

		if (ABS_VALUE(loopbackCurrent - commandCurrent) > maxCurrentDiff)
		{
		// $[TI1.1]
			testPassed = FALSE;
		}
		// $[TI1.2]
		// implied else

#ifdef SIGMA_DEBUG
		printf("EV command = %d cts\n", exhValveCommand_[testNumber]);
		printf("nominal EV command current = %7.3f mA\n", commandCurrent);
		printf("EV loopback current = %7.3f mA\n", loopbackCurrent);
		printf("current difference = %7.3f mA\n",
			ABS_VALUE(loopbackCurrent - commandCurrent) );
		printf("MAX current diff = %7.3f mA\n", maxCurrentDiff);
#endif  // SIGMA_DEBUG

	}

	// Open exhalation valve
	RSmUtilities.openExhValve();

	if (!testPassed)
	{
	// $[TI2]
		// Minor failure, Condition 1
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_1);
	}
	
	// $[TI3]
	// implied else

#ifdef SIGMA_UNIT_TEST
	SmUnitTest::ExhValveLoopbackTestPass = testPassed;
#endif  // SIGMA_UNIT_TEST

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the fault 
//  macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and 
//  'lineNumber' are essential pieces of information.  The 'pFileName' 
//  and 'pPredicate' strings may be defaulted in the macro to reduce code 
//  space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ExhValveLoopbackTest::SoftFault(const SoftFaultID  softFaultID,
			        const Uint32       lineNumber,
			        const char*        pFileName,
			        const char*        pPredicate)
{
  CALL_TRACE("ExhValveLoopbackTest::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, EXHVALVELOOPBACKTEST,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
