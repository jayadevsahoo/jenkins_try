//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file EventReader.h
//----------------------------------------------------------------------------

#ifndef CEVENTREADER_H
#define CEVENTREADER_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "EventLogger.h"
#include "SocketServer.h"
#include "Thread.h"
#include <vector>

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class EventReader
{
public:
    EventReader(EventLogger *pEventLogger, SocketServer *pSocketServer);
    EventReader(EventLogger **pEventLogger, SocketServer *pSocketServer);
    ~EventReader();

private:
    void ThreadEntranceProc();
    static void *ThreadEntranceProc   (void *pVoid);

    EventReader(const EventReader&);
    EventReader operator=(const EventReader&);

    std::vector<EventLogger  *> m_pEventLoggerList;
    SocketServer *m_pSocketServer;
    bool          m_bRunning;
    Thread       *m_pThread;
    
};

#endif

