#ifndef PressureXducerCalibrationSubScreen_HH
#define PressureXducerCalibrationSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PressureXducerCalibrationSubScreen - Activated by selecting 
// Pressure transducer calibration button on the service lower other subScreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PressureXducerCalibrationSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  hhd	   Date:  24-Feb-1999    DCS Number:  5326
//  Project:  ATC
//  Description:
//	Added buttonUpHappened() method.
//
//  Revision: 003  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  yyy    Date:  01-Oct-97    DR Number: 2410
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "BatchSettingsSubScreen.hh"
#include "AdjustPanelTarget.hh"
#include "GuiTestManagerTarget.hh"
#include "GuiTestManager.hh"
#include "ServiceStatusArea.hh"
#include "SmPromptId.hh"
#include "NumericSettingButton.hh"
#include "SubScreenTitleArea.hh"
//@ End-Usage

class PressureXducerCalibrationSubScreen : public BatchSettingsSubScreen,
					   public GuiTestManagerTarget 
{
public:
	PressureXducerCalibrationSubScreen(SubScreenArea *pSubScreenArea);
	~PressureXducerCalibrationSubScreen(void);

	// Overload SubScreen methods
	virtual void acceptHappened(void);

	// Execute a successful setting transaction event
	void executeSettingTransactionHappened(void);

	// GuiTestMangerTarget virtual methods
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId=-1);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelRestoreFocusHappened(void);

	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// BatchSettingsSubScreen virtual method...
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*  pSubject);
	// Button target virtual method.								
	virtual void buttonUpHappened(Button *pButton,
					 Boolean byOperatorAction);

	
private:
	// these methods are purposely declared, but not implemented...
	PressureXducerCalibrationSubScreen(void);							// not implemented..
	PressureXducerCalibrationSubScreen(const PressureXducerCalibrationSubScreen&);	// not implemented..
	void operator=(const PressureXducerCalibrationSubScreen&);			// not implemented..

	enum CalScreenId
	{
		PRESSURE_CAL_INIT = -1,
		PRESSURE_CAL_UPDATING,
		PRESSURE_CAL_SETTING_PRE_SET,
		PRESSURE_CAL_SETUP,
		PRESSURE_CAL_EXECUTING,
		MAX_CAL_SCREENS
	};

	enum CalConditionId
	{
		MAX_CONDITION_ID = 1
	};

	void layoutScreen_(CalScreenId calScreenId);
	void setErrorTable_(void);
	void startTest_(SmTestId::ServiceModeTestId currentTestId,
				Boolean testRepeat = FALSE,
				const char * const pParameters = NULL);

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member: pressureSettingButton_
	// The humidifierType setting button.
	NumericSettingButton pressureSettingButton_;

	//@ Data-Member: currentScreenId_
	// The screen ID for currently displayed screen.
	CalScreenId currentScreenId_;

	//@ Data-Member: currentTestId_
	// The service mode test ID for currently running test.
	SmTestId::ServiceModeTestId currentTestId_;

	//@ Data-Member: measuredAdjustedPressure_
	// A place holder for offset adjusted pressure measured by service mode sent by
	// service data.
	Real32 measuredAdjustedPressure_;
	
	//@ Data-Member: guiTestMgr_ 
	// Object instance of GuiTestManager
	GuiTestManager guiTestMgr_;

	//@ Data-Member: calStatus_
	// A container for displaying the current Exh. Calibration status.
	ServiceStatusArea calStatus_;

	//@ Data-Member: errDisplayArea_
	// An array of TextField objects used to display test conditions in the
	// lower screen
	TextField errDisplayArea_[MAX_CONDITION_ID];
	
	//@ Data-Member: conditionText_
	// Text for error condition prompts
	StringId conditionText_[MAX_CONDITION_ID];

	//@ Data-Member: errorHappened_
	// Flag that indicates status of test
	Int	errorHappened_;
	
	//@ Data-Member: errorIndex_
	// Keep tracks of index into TextFields in the error display area
	Int	errorIndex_;
	
	//@ Data-Member: keyAllowedId_
	Int keyAllowedId_;

	//@ Data-Member: userKeyPressedId_
	// Hold the operator pressed key Id.
	SmPromptId::ActionId userKeyPressedId_;
};

#endif // PressureXducerCalibrationSubScreen_HH 
