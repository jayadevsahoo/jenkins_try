#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VolumeController - Implements an inspiratory flow controller
//		for volume based breaths.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class FlowController.
//		This class manipulates the PSOL so that the proper flow/volume
//		is delivered out of the PSOL.  There is one instance of this class
//      for each psol.  Methods are implemented to initialize
//		the controller, to control the PSOL each cycle so the desired flow is
//		achieved, to update the summers for the last cycle in inspiration,
//		to set and get the controller type, to overwrite the flag that determines
//		if two breaths are alike and to obtain the status of the flag,
//		to update the error band which is used to see if two breaths are
//		alike, and to store the peak desired flow for the coming breath.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms to deliver volume to the
//		patient.
//---------------------------------------------------------------------
//@ Implementation-Description
//		This controller is invoked every VOLUME_CYCLE_TIME_MS msecs to
//		deliver flow/volume	to the patient.  The controller corrects for
//		errors in flow on a	breath to breath basis.  The information of
//		past breaths for each interval are stored every
//		VOLUME_CYCLE_TIME_MS msec (called summers).  There are three
//		controller types: FF_PLUS_I (Feedforward plus integral),
//		INIT_SUMMERS_FF_PLUS_I (Feedforward plus integral, store summers),
//		and FF_PLUS_SUMMERS (Feedforward plus integral plus summers, integral
//		is enabled if breaths are not alike).  The controllerType progresses
//		from FF_PLUS_I to INIT_SUMMERS_FF_PLUS_I to FF_PLUS_SUMMERS
//		on subsequent breaths if certain conditions are met.
//
//		The data members ControllerType_ and BreathLikenessStatus_ are static
//		to ensure that all the instances of this class (air and O2) are
//		synchronized as far as the ControllerType_ and BreathLikenessStatus_
//		are concerned.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VolumeController.ccv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: syw     Date:  09-Mar-2000    DR Number: 5684
//  Project:  NeoMode
//  Description:
//		Added PsolSide to constructor arguments.
//		Use base method computeFeedForward_() to determine ff component of
//			the controller.
//		Added Boolean argument to updatePsol().
//		Added Boolean argument in calling computeKi_().
//
//  Revision: 007  By: syw     Date:  11-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Call computeKi_() to update the integral gain
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005 By: syw    Date: 16-Jul-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added controllerShutdown_ to shutdown controller when desired flow
//			is zero.
//
//  Revision: 004 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added call to base class method safetyNetPsolStuckOpenTest_().
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes:
//			- eliminated isControllerActive since the controller will
//			  handle based on desiredFlow.
//			- eliminate closeLevel_.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "VolumeController.hh"

//@ Usage-Classes

#include "Psol.hh"
#include "FlowSensor.hh"
#include "BD_IO_Devices.hh"

//@ End-Usage

//@ Code...

//@ Constant: VOLUME_CYCLE_TIME_MS
// control cycle time for volume controller
const Int32 VOLUME_CYCLE_TIME_MS = 20 ;      // millisecond

Boolean VolumeController::BreathLikenessStatus_ = FALSE ;
VolumeController::ControllerTypes VolumeController::ControllerType_ = FF_PLUS_I ; 

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VolumeController()
//
//@ Interface-Description
//		Constructor.  This method has Psol& and FlowSensor& as arguments.
//		This method returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		A reference to a Psol and a FlowSensor and an activation state are
//		passed in to initialize a FlowController.  Other data members
//		are	also initialized.
//---------------------------------------------------------------------
//@ PreCondition
//		MAX_INSP_TIME_MS / VOLUME_CYCLE_TIME_MS + 1 = MAX_INSP_CYCLES
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
VolumeController::VolumeController( Psol& psol, const FlowSensor& flowSensor,
									const PsolLiftoff::PsolSide side)
: FlowController( psol, flowSensor, side)		// $[TI1]
{  
	CALL_TRACE("VolumeController::VolumeController( Psol& psol, \
				const FlowSensor& flowSensor, \
				const PsolLiftoff::PsolSide side)") ;

	// $[TI2]
	CLASS_PRE_CONDITION( (Int32)(MAX_INSP_TIME_MS / VOLUME_CYCLE_TIME_MS + 1)
							 == MAX_INSP_CYCLES
					   ) ;

	errorSumGain_ = 0.0F ;
	errorGain_ = 0.0F ;
	maxAbsoluteError_ = 0.0F ;	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VolumeController()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
VolumeController::~VolumeController( void)
{
	CALL_TRACE("VolumeController::~VolumeController( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to initialize gains and status flags depending on the controller
//		type.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialization of controller along with gains and flags based on
//		ControllerType_.  
// 		$[04068] $[04080] $[04083] $[04184] $[04285]
//		$[04095] Cg is equivalent to errorSumGain_
//---------------------------------------------------------------------
//@ PreCondition
//		ControllerType_ must be one of the following:
//			FF_PLUS_I, INIT_SUMMERS_FF_PLUS_I, or FF_PLUS_SUMMERS
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
VolumeController::newBreath( void)
{   
	CALL_TRACE("VolumeController::newBreath( void)") ;

	intervalNumber_ = 0 ;
	integrator_ = 0.0F ;

	kff_ = 12.0F ;
	liftoff_ = rPsol_.getLiftoff() ;
	
	switch( ControllerType_)
	{
		case FF_PLUS_I:
			// $[TI1.1]
			ki_ = INTEGRAL_GAIN ;
			break ;
      
		case INIT_SUMMERS_FF_PLUS_I:
			// $[TI1.2]
			ki_ = INTEGRAL_GAIN ;
			intervalLikenessStatus_ = TRUE ;
			BreathLikenessStatus_ = TRUE ;
			determineErrorSumGain_() ;
			break ;

		case FF_PLUS_SUMMERS:
			// $[TI1.3]
			errorGain_ = 0.0F ;	// freeze integration
			ki_ = 0.0F ;			// no integral control
			intervalLikenessStatus_ = TRUE ;
			BreathLikenessStatus_ = TRUE ;
			determineErrorSumGain_() ;
			break ; 
			
		default:
			CLASS_PRE_CONDITION( ControllerType_ == FF_PLUS_I
									|| ControllerType_ == INIT_SUMMERS_FF_PLUS_I
									|| ControllerType_ == FF_PLUS_SUMMERS) ;
	}  // switch

	// be sure this is executed after determineErrorSumGain_() is called 
	maxAbsoluteError_ = 0.0F ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePsol()
//
//@ Interface-Description
//      This method has desired flow and usePsolLookup flag as an arguments
//		and has no return value.
// 		This method is called every VOLUME_CYCLE_TIME_MS msecs during the
//		inspiratory phase of a volume based breath to control the PSOL so
//		the desiredFlow is delivered out of the PSOL for the current
//		VOLUME_CYCLE_TIME_MS msec interval.  updateErrorBand() needs to be
//		called by VcvPhase::newBreath() before this method is called.
//---------------------------------------------------------------------
//@ Implementation-Description
//		For every breath, the errors for each control interval are stored
//		in an array, controlIntervalError_[], so comparisons between
//		two consecutive breaths can be made.
//
//		The following describes the three types of controllers:
//
//		ControllerType_ = FF_PLUS_I is defined to be a feedforward plus
//			integral type of control:  No integral contribution is used on
//			the	first control interval.
//
//		ControllerType_ = INIT_SUMMERS_FF_PLUS_I is defined as the same type
//			of controller as controller type FF_PLUS_I.  The breath to breath
//			summers, errorSum_[], are initialized in this phase only.
//
//		ControllerType_ = FF_PLUS_SUMMERS is a feedforward plus integral
//			and breath to breath type of controller.  The integral is disabled 
//			until a disturbance is detected.  The breath to breath
//			summers, errorSum_[], are summed in this phase only.
// 		$[04068] $[04080] $[04083] $[04084] $[04085] $[04086] $[04088] $[04089]
//		$[04090] $[04092] $[04093] $[04094] $[04095] $[04097] $[04098] $[04099]
//		$[04100] $[04309] 
//---------------------------------------------------------------------
//@ PreCondition
//		intervalNumber_ < MAX_INSP_CYCLES
//		ki_ > 0.0
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
VolumeController::updatePsol( const Real32 desiredFlow, const Boolean usePsolLookup) 
{  
	CALL_TRACE("VolumeController::updatePsol( const Real32 desiredFlow, \
											  const Boolean usePsolLookup)") ;
		
	CLASS_PRE_CONDITION( intervalNumber_ < MAX_INSP_CYCLES) ;

	Real32	previousBreathIntervalError ; 
	Int32   psolCommand = 0;

	desiredFlow_ = desiredFlow ;

	if (desiredFlow_ < ZERO_FLOW && controllerShutdown_)
	{
		// $[TI1.1]
		psolCommand = 0 ;
		integrator_ = 0.0F ;
		controlIntervalError_[intervalNumber_] = 0.0F ;
		errorSum_[intervalNumber_] = 0.0F ;
	}
	else
	{
		Real32 ff = computeFeedForward_( usePsolLookup) ;
		
		// $[TI1.2]
		switch( ControllerType_)
		{
			case FF_PLUS_I:
				// $[TI2.1]
				controlIntervalError_[intervalNumber_] = desiredFlow -
															rFlowSensor_.getValue() ;
				controlIntervalError_[0] = 0.0F ; // first integral term is 0.
			
				maxAbsoluteError_ = MAX_VALUE(
								ABS_VALUE(controlIntervalError_[intervalNumber_]),
								maxAbsoluteError_) ;
												
				computeKi_( usePsolLookup) ;
				integrator_ += controlIntervalError_[intervalNumber_]
									* VOLUME_CYCLE_TIME_MS ;

				psolCommand = (Int32)(integrator_ * ki_	+ ff) ;
				break ;

			case INIT_SUMMERS_FF_PLUS_I:
				// $[TI2.2]
				previousBreathIntervalError = controlIntervalError_[intervalNumber_] ;
				controlIntervalError_[intervalNumber_] = desiredFlow -
														rFlowSensor_.getValue() ;
				controlIntervalError_[0] = 0.0F ;	// first integral term is 0.

				// check if two consecutive breaths are alike by comparing each of the
				// interval errors of the present breath to the corresponding interval
				// errors of the previous breath.  If two consecutive intervals fail the
				// alikeness condition, then the breath is declared to be not alike.  
			
				if (BreathLikenessStatus_ == TRUE)	// check if breaths are still alike
				{
					// $[TI3.1]
					if (ABS_VALUE(previousBreathIntervalError -
								controlIntervalError_[intervalNumber_]) - 0.3F <=
									0.25 * desiredFlow)
					{
						// $[TI4.1]
						intervalLikenessStatus_ = TRUE ;	// breath intervals are alike
					}
					else
					{
						// $[TI4.2]
						// breath interval not alike... check whether first or second
						// occurance
						if (intervalLikenessStatus_ == TRUE) // first disturbance
						{
							// $[TI5.1]
							intervalLikenessStatus_ = FALSE ;
						}
						else	// second disturbance
						{
							// $[TI5.2]
							BreathLikenessStatus_ = FALSE ;
						}
					} 	
					// obtain maximum error for breath
					maxAbsoluteError_ = MAX_VALUE(
									ABS_VALUE(controlIntervalError_[intervalNumber_]),
									maxAbsoluteError_) ;
				} 	// implied else $[TI3.2]
			
    	        Real32	prevIntegrator ;
        	    prevIntegrator = integrator_ ;
            
				computeKi_( usePsolLookup) ;
            	integrator_ += controlIntervalError_[intervalNumber_]
            						* VOLUME_CYCLE_TIME_MS ;
	            // initialize summers to the integral command from last cycle and the
    	        // present error

        	    errorSum_[intervalNumber_] = ki_ * prevIntegrator
            				+ errorSumGain_ * controlIntervalError_[intervalNumber_] ;

	            psolCommand = (Int32)(integrator_ * ki_ + ff) ;
        	   	break ;

			case FF_PLUS_SUMMERS:
				// $[TI2.3]
				controlIntervalError_[intervalNumber_] = desiredFlow -
															rFlowSensor_.getValue() ;
				controlIntervalError_[0] = 0.0F ;	// first integral term is 0.
			
				if (BreathLikenessStatus_ == TRUE)	// check if breaths are still alike
				{
					// $[TI6.1]
					if (ABS_VALUE(controlIntervalError_[intervalNumber_]) <=
							errorBand_)
					{
						// $[TI7.1]
						intervalLikenessStatus_ = TRUE ;	// breath intervals are alike
						errorGain_ = 0.0F ;	// disable integration 
					}
					else
					{
						// $[TI7.2]
						// breath interval not alike... check whether first or second
						// occurance
						if (intervalLikenessStatus_ == TRUE)	// first disturbance
						{
							// $[TI8.1]
							intervalLikenessStatus_ = FALSE ;
							integrator_ = 0.0F ;	// reset integrator
							errorGain_ = 1.0F ;	// // enable integration
						}
						else	// second disturbance
						{
							// $[TI8.2]
							ki_ = INTEGRAL_GAIN ;			// initialize ki_ to avoid divide by zero
							BreathLikenessStatus_ = FALSE ;
							computeKi_( usePsolLookup) ;
						}
					}
					// obtain maximum error during breath
					maxAbsoluteError_ = MAX_VALUE(
									ABS_VALUE(controlIntervalError_[intervalNumber_]),
									maxAbsoluteError_) ;
				}
				else
				{
					// $[TI6.2]
					computeKi_( usePsolLookup) ;
				}
			
    	        integrator_ += errorGain_* controlIntervalError_[intervalNumber_]
        	    					* VOLUME_CYCLE_TIME_MS ;

            	errorSum_[intervalNumber_] += errorSumGain_
            						* controlIntervalError_[intervalNumber_] ;
				psolCommand = (Int32)(integrator_ * ki_	+ errorSum_[intervalNumber_ + 1] + ff) ;
        	    break ;

			default:
				CLASS_PRE_CONDITION( ControllerType_ == FF_PLUS_I
										|| ControllerType_ == INIT_SUMMERS_FF_PLUS_I
										|| ControllerType_ == FF_PLUS_SUMMERS) ;
		}	// switch

		// check for windup conditions
	
		if (psolCommand > MAX_COUNT_VALUE)
		{
			// $[TI9.1]
			if (ControllerType_ == FF_PLUS_I || ControllerType_ == INIT_SUMMERS_FF_PLUS_I)
			{
				// $[TI10.1]
				CLASS_PRE_CONDITION( ki_ > 0.0F) ;
				integrator_ = (MAX_COUNT_VALUE - ff) / ki_ ;
			}
			else	// controllerType = FF_PLUS_SUMMERS
			{
				// $[TI10.2]
				if (BreathLikenessStatus_ == TRUE)
				{
					// $[TI11.1]
					errorSum_[intervalNumber_] = MAX_COUNT_VALUE - ff ;
				}
				else
				{
					// $[TI11.2]
					CLASS_PRE_CONDITION( ki_ > 0.0F) ;
					integrator_ = (	MAX_COUNT_VALUE - ff - errorSum_[intervalNumber_ + 1]) / ki_ ;
				}
			}
			psolCommand = MAX_COUNT_VALUE ;
		}
		else if (psolCommand < 0)
		{
			// $[TI9.2]
			if (ControllerType_ == FF_PLUS_I || ControllerType_ == INIT_SUMMERS_FF_PLUS_I)
			{
				// $[TI12.1]
				CLASS_PRE_CONDITION( ki_ > 0.0F) ;
				integrator_ = -ff / ki_ ;
			}
			else	// controllerType = FF_PLUS_SUMMERS
			{
				// $[TI12.2]
				if (BreathLikenessStatus_ == TRUE)
				{
					// $[TI13.1]
					errorSum_[intervalNumber_] =  -ff ;
				}
				else
				{
					// $[TI13.2]
					CLASS_PRE_CONDITION( ki_ > 0.0F) ;
					integrator_ = - (errorSum_[intervalNumber_ + 1]	+ ff) / ki_ ;
				}
			}
			psolCommand = 0 ;
		} 	// implied else $[TI9.3]
	}

	intervalNumber_ ++ ;
	rPsol_.updatePsol( (Int16)psolCommand) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateLastErrorSum()
//
//@ Interface-Description
//		This method has desired flow as an argument and has no return value.
//		This method is called only at the beginning of exhalation to update
//		the errorSum[].  The errorSum update runs into the first cycle of
//		exhalation since the controller accesses the errorSum[] one interval
//		ahead. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		If the controllerType = INIT_SUMMERS_FF_PLUS_I then the errorSum_[] is
//		initialized.
//		If the controllerType = FF_PLUS_SUMMERS then the errorSum_[] is summed.
// 		$[04084]
//---------------------------------------------------------------------
//@ PreCondition
//		intervalNumber_ < MAX_INSP_CYCLES
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
VolumeController::updateLastErrorSum( const Real32 desiredFlow)
{
	CALL_TRACE("VolumeController::updateLastErrorSum( Real32 desiredFlow)") ;
	
	CLASS_PRE_CONDITION( intervalNumber_ < MAX_INSP_CYCLES) ;
	
	desiredFlow_ = desiredFlow ;

	if (desiredFlow_ < ZERO_FLOW && controllerShutdown_)
	{
		// $[TI1.1]
		controlIntervalError_[intervalNumber_] = 0.0F ;
		errorSum_[intervalNumber_] = 0.0F ;
	}
	else
	{
		// $[TI1.2]
		controlIntervalError_[intervalNumber_] = desiredFlow - rFlowSensor_.getValue() ;

		if (ControllerType_ == INIT_SUMMERS_FF_PLUS_I)
		{
			// $[TI2.1]
			// the integrator_ value is the previous integrator when this method is called
		    errorSum_[intervalNumber_] = ki_ * integrator_
    	                                    + errorSumGain_ * controlIntervalError_[intervalNumber_] ;
	    }
    	else if (ControllerType_ == FF_PLUS_SUMMERS)
	    {
			// $[TI2.2]
    		errorSum_[intervalNumber_] += errorSumGain_
    										* controlIntervalError_[intervalNumber_] ;
		}	// implied else $[TI2.3]
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateErrorBand()
//
//@ Interface-Description
//		This method has desired peak flow as an argument and has no return
//		value.  This method is called to update the error band.  This method
//		must be called at the beginning of each inspiration by
//		VcvPhase::newBreath() before VolumeController::newBreath() is called.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The errorBand_ is computed based on the ControllerType.  The error
//		band is set to the maximum of the computed errorBand_ or 0.1 *
//		desiredPeakFlow or 0.2.
// 		$[04090] $[04091]
//---------------------------------------------------------------------
//@ PreCondition
//		ControllerType_ == FF_PLUS_I || ControllerType_ == INIT_SUMMERS_FF_PLUS_I
//		|| ControllerType_ == FF_PLUS_SUMMERS
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
VolumeController::updateErrorBand( const Real32 desiredPeakFlow) 
{   
	CALL_TRACE("VolumeController::updateErrorBand( const Real32 desiredPeakFlow)") ;

	switch( ControllerType_)
	{
		case FF_PLUS_I:
			// $[TI1.1]
			// no code for this case
			break ;
			
		case INIT_SUMMERS_FF_PLUS_I:
			// $[TI1.2]
			errorBand_ = maxAbsoluteError_ ;
			break ;
			
		case FF_PLUS_SUMMERS:
			// $[TI1.3]
		   	errorBand_ = (errorBand_ + maxAbsoluteError_) / 2.0F ;
      		break ;
			
		default:
			CLASS_PRE_CONDITION( ControllerType_ == FF_PLUS_I
									|| ControllerType_ == INIT_SUMMERS_FF_PLUS_I
									|| ControllerType_ == FF_PLUS_SUMMERS) ;
	}
		   	
	errorBand_ = MAX_VALUE( errorBand_, 0.1F * desiredPeakFlow) ;
   	errorBand_ = MAX_VALUE( errorBand_, 0.2F) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
VolumeController::SoftFault(const SoftFaultID  softFaultID,
                   			const Uint32       lineNumber,
						   	const char*        pFileName,
							const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, VOLUMECONTROLLER,
    	                     lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineErrorSumGain_()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called only by the newBreath() method before maxAbsoluteError_ is zeroed
//		to determine the error sum gain for the coming volume breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//		errorSumGain_ is determined based on the range of peakDesiredFlow_.
// 		$[04095] $[04284] $[04285] 
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
VolumeController::determineErrorSumGain_( void)
{
	CALL_TRACE("VolumeController::determineErrorSumGain_( void)") ;

	if (peakDesiredFlow_ > 0.0F && peakDesiredFlow_ <= 3.0F)
	{
		// $[TI1.1]
		errorSumGain_ = kff_ * (1.0F + maxAbsoluteError_ / peakDesiredFlow_) ;

		if (errorSumGain_ > 50.0F)
		{
			// $[TI2.1]
			errorSumGain_ = 50.0F ;
		}	// implied else $[TI2.2]
	}
	else
	{
		// $[TI1.2]
		errorSumGain_ = kff_ ;
	}
}












