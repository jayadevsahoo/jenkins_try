#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  DListR_Alarm - Abstract Doubly-Linked List of 'Alarm' References.
//---------------------------------------------------------------------
//@ Interface-Description
//  This provides an abstraction for a doubly-linked list of generic
//  data items each of type 'Alarm'.  The data items of the list are
//  references to -- not copies of -- the data items that are added to
//  the list.  There are non-constant, public methods to iterate
//  through the contents of the list, and there's also an iterator
//  class (see 'DIterR_Alarm') to allow iteration of a constant
//  list.  This is an abstract base class and is used with
//  'FixedDListR<Alarm,SIZE>'.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a standard abstraction for storing data items
//  in a dynamic collection, where initial placement within the
//  collection is quick and easy.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each list has an internal heap containing the doubly-linked
//  nodes ('DNodeC_MemPtr').  The nodes are allocated from
//  the heap to contain references to the inserted data.  This list
//  uses (HAS-A) doubly-linked list of copies, internally.  The
//  internal copy list is used to store the list of references to
//  to items of this list.  This is why the nodes for this type of
//  list are of type 'DNodeC_MemPtr', instead of some new node
//  class template specifically designed to hold references to typed
//  items (e.g. 'DLinkedNodeR<Alarm>' -- which does NOT exist).
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions have to do with the data type that is chosen for
//  this list.  The data type must provide the following mechanisms,
//  to be allowed to be used in this list:
//>Von
//      Alarm::operator==(const Alarm& aType) const;// equivalence operator...
//      Alarm::operator!=(const Alarm& aType) const;// inequivalence operator...
//>Voff
//  For primitive (built-in) types this is already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//  Since these lists contain pointers to the inserted items, those
//  items that are inserted into these lists must have a lifetime that
//  is longer than the lifetime of these lists' reference to them.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractDListR.C_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "DListR_Alarm.hh"
#include "Alarm.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DListR_Alarm()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this list of references, but NOT the objects that are
//  referenced.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The 'rPtrList_' alias can NOT be used from within this destructor,
//  because by the time this destructor is called the derived class's
//  destructor has already destroyed the list of references.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

DListR_Alarm::~DListR_Alarm(void)
{
  CALL_TRACE("~DListR_Alarm()");
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  search(item, startFrom, direction, foundWhen)
//
//@ Interface-Description
//  Search the current list for a node that is a match of 'item'.
//  'startFrom' indicates whether to start the search from the first
//  node ('FROM_FIRST'), the current node ('FROM_CURRENT'), or the
//  last node ('FROM_LAST') in this list.  If 'direction' is
//  'FORWARD', search forward, otherwise, if 'direction' is 'BACKWARD',
//  search backward.  If 'foundWhen' is 'EQUIV_OBJECTS', then a match
//  occurs when 'item' is equivalent to an object referenced within this
//  list.  If 'foundWhen' is 'EQUIV_REFERENCES', then a match occurs when
//  the reference of 'item' is found within this list.  If a match is
//  found, then 'SUCCESS' is returned, and the current node is left
//  pointing to the matching node.  If no match is found, then
//  'FAILURE' is returned, and the list is unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Near the end of this method a pointer to a constant node,
//  'pFoundNode', has its const-ness cast away.  This is due to the
//  fact that 'findNode_()' is a constant method (to allow its use
//  in constant public methods) that returns a pointer.  To ensure
//  consistent const-ness of 'findNode_()', the pointer that is
//  returned is a pointer to a CONSTANT node.  The const-ness of
//  the returned node is not necessary in the design of 'findNode_()',
//  but rather a consistency of design of that method.  Therefore,
//  when this method casts the const-ness of 'pFoundNode' away, it
//  is an acceptable case of working properly with a proper design.
//---------------------------------------------------------------------
//@ PreConditions
//  (startFrom == FROM_FIRST  ||  startFrom == FROM_CURRENT  ||
//   startFrom == FROM_LAST)
//  (direction == FORWARD  ||  direction == BACKWARD)
//  (foundWhen == EQUIV_OBJECTS  ||  foundWhen == EQUIV_REFERENCES)
//---------------------------------------------------------------------
//@ PostConditions
//  (status == SUCCESS) ==> (currentItem() == item)
//  (status == FAILURE) ==> (pCurrent_ == OLD{pCurrent_})
//@ End-Method
//=====================================================================

SigmaStatus
DListR_Alarm::search(Alarm&                 item,
			     const SearchFrom      startFrom,
			     const SearchDirection direction,
			     const EquivResolution foundWhen)
{
  CALL_TRACE("search(item, startFrom, direction, foundWhen)");

  SigmaStatus status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore search the nodes of this list
    // for an item that matches 'item'...
    const DNodeC_MemPtr*  pFoundNode = NULL;

    // set 'pFoundNode' to the node that is to be the start of this
    // search...
    switch (startFrom)
    {
    case FROM_FIRST:		// $[TI1.1]
      pFoundNode = getFirst_();
      break;
    case FROM_CURRENT:		// $[TI1.2]
      pFoundNode = getCurrent_();
      break;
    case FROM_LAST:		// $[TI1.3]
      pFoundNode = getLast_();
      break;
    default:
      AUX_CLASS_ASSERTION_FAILURE(((startFrom     << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(Alarm)));	    // bottom 12 bits...
      break;
    }

    // these are "safe", because 'findNode_()' will ALWAYS check this...
    SAFE_CLASS_PRE_CONDITION((direction == FORWARD  ||
			      direction == BACKWARD));
    SAFE_CLASS_PRE_CONDITION((foundWhen == EQUIV_OBJECTS  ||
			      foundWhen == EQUIV_REFERENCES));

    // use 'findNode_()' to search, starting from 'pFoundNode'...
    pFoundNode = findNode_(item, pFoundNode, direction, foundWhen);

    if (pFoundNode != NULL)
    {   // $[TI1.4]
      // the matching node is found, therefore set the current node to the
      // found node...

      // warning:  "const cast away: ..."
      rPtrList_.setCurrent_((DNodeC_MemPtr*) pFoundNode);
      status = SUCCESS;
    }   // $[TI1.5]
  }   // $[TI2] -- this list is currently empty...

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeItem(item, foundWhen)
//
//@ Interface-Description
//  Remove the first node in this list that is a match of 'item'.
//  If 'foundWhen' is 'EQUIV_OBJECTS', then a match occurs when 'item'
//  is equivalent to an item that is referenced by this list.  If
//  'foundWhen' is 'EQUIV_REFERENCES', then a match occurs when a
//  reference to 'item' is found.  If there is a match, then the
//  matching node is removed, and 'SUCCESS' is returned.  If there
//  is no match, then the list is left unchanged, and 'FAILURE' is
//  returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses 'search()' to find 'item' and set the current
//  node to it, then uses 'removeCurrent()' to remove the current
//  node.  This method returns 'SUCCESS' when 'removeCurrent()' is
//  called.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SigmaStatus
DListR_Alarm::removeItem(Alarm&                 item,
				 const EquivResolution foundWhen)
{
  CALL_TRACE("removeItem(item, foundWhen)");

  // if this list is not empty and a search for 'item' is successful, then
  // remove the item that was found and return 'SUCCESS', otherwise return
  // 'FAILURE'...
  return((!isEmpty()  &&
	  search(item, FROM_FIRST, FORWARD, foundWhen) == SUCCESS)
	  ? rPtrList_.removeCurrent()		// $[TI1]
	  : FAILURE);				// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEquivTo(dList, equivWhen)  [const]
//
//@ Interface-Description
//  Determine the equivalence between this list and 'dList', and
//  return a boolean to indicate whether they're equivalent.  Two
//  lists are equivalent when they both have the same number of nodes,
//  and each of the items in one list is equivalent to the
//  corresponding item in the other.  When 'equivWhen' is
//  'EQUIV_OBJECTS', then equivalence is based on the referenced
//  objects of each node being equivalent, but when 'equivWhen' is
//  'EQUIV_REFERENCES', then equivalence is based on the references
//  of each of the nodes being equivalent.  (NOTE:  equivalence does
//  NOT depend on the current nodes of the lists.)
//---------------------------------------------------------------------
//@ Implementation-Description
//  If looking for an equivalent reference ('EQUIV_REFERENCE'), use
//  the equivalence test of 'rPtrList_'.
//---------------------------------------------------------------------
//@ PreConditions
//  (equivWhen == EQUIV_OBJECTS  ||  equivWhen == EQUIV_REFERENCES)
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
DListR_Alarm::isEquivTo(const DListR_Alarm& dList,
				const EquivResolution       equivWhen) const
{
  CALL_TRACE("isEquivTo(dList, equivWhen)");

  Boolean isEquiv;

  // the two lists are equivalent if they are the same list, OR they're
  // both empty...$[TI1] (TRUE)  $[TI2] (FALSE)
  isEquiv = (this == &dList  ||  (isEmpty()  &&  dList.isEmpty()));

  if (!isEquiv  &&  !isEmpty()  &&  !dList.isEmpty())
  {   // $[TI3]
    // both lists are NOT empty, therefore continue checking for
    // equivalence...
    switch (equivWhen)
    {
    case EQUIV_OBJECTS:		// $[TI3.1]
      {
	// starting with the first node from each list, make sure each
	// of the items that are referenced by each node are equivalent.
	const DNodeC_MemPtr*  pNode1 = getFirst_();
	const DNodeC_MemPtr*  pNode2 = dList.getFirst_();

	do
	{   // $[TI3.1.1]
	  // while the two lists are equivalent and there are still
	  // more nodes in the lists, compare their referenced items
	  // for equivalence...
	  isEquiv = (*((const Alarm*)(pNode1->getItem())) ==
		     *((const Alarm*)(pNode2->getItem())));

	  if (isEquiv)
	  {   // $[TI3.1.1.1]
	    // move each node pointer to the next node in each list...
	    pNode1 = pNode1->getNextPtr();
	    pNode2 = pNode2->getNextPtr();
	  }   // $[TI3.1.1.2] -- an in-equivalence is found...
	} while (isEquiv  &&  pNode1 != getFirst_()  &&
			pNode2 != dList.getFirst_());

	if (isEquiv)
	{   // $[TI3.1.2]
	  // they must BOTH be pointing to their first pointers,
	  // respectively, for the two lists to be equivalent...
	  // $[TI3.1.2.1] (TRUE)  $[TI3.1.2.2] (FALSE)
	  isEquiv = (pNode1 == getFirst_()  &&  pNode2 == dList.getFirst_());
	}   // $[TI3.1.3] -- no match was found...
      }
      break;
    case EQUIV_REFERENCES:	// $[TI3.2]
      // use the equivalence test of the reference lists, themselves to
      // test for equivalence of the references that they contain...
      isEquiv = (rPtrList_ == dList.rPtrList_);  // $[TI3.2.1] $[TI3.2.2]
      break;
    default:
      AUX_CLASS_ASSERTION_FAILURE(((equivWhen     << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(Alarm)));	    // bottom 12 bits...
      break;
    }
  }   // $[TI4] -- testing against oneself, OR one, or both, are empty...

  return(isEquiv);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                  [static]
//
//@ Interface-Description
//  Report the software fault that occured within the source code
//  of this class.  The fault, indicated by 'softFaultID', occured
//  at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
DListR_Alarm::SoftFault(const SoftFaultID softFaultID,
				const Uint32      lineNumber,
				const char*       pFileName,
				const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, ABSTRACT_DLISTR,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  findNode_(item, pStartNode, direction, foundWhen)  [const]
//
//@ Interface-Description
//  Starting from 'pStartNode' search, in the direction indicated
//  by 'direction', for the node that matches 'item', then return
//  the pointer to that node.  If no match is found before the end
//  of the list, then return 'NULL'.  (See 'search()' for more on
//  when a match occurs.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (direction == FORWARD  ||  direction == BACKWARD)
//  (foundWhen == EQUIV_OBJECTS  ||  equivWhen == EQUIV_REFERENCES)
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

const DNodeC_MemPtr*
DListR_Alarm::findNode_(Alarm&                       item,
				const DNodeC_MemPtr* pStartNode,
				const SearchDirection       direction,
				const EquivResolution       foundWhen) const
{
  CALL_TRACE("findNode_(item, pStartNode, direction, foundWhen)");
  SAFE_CLASS_PRE_CONDITION((isLegalNode_(pStartNode)));

  switch (foundWhen)
  {
  case EQUIV_OBJECTS:		// $[TI1]
    // find the matching object...
    switch (direction)
    {
    case FORWARD:		// $[TI1.1]
      while (pStartNode != NULL &&
	     *((const Alarm*)(pStartNode->getItem())) != item)
      {   // $[TI1.1.1] -- at least one iteration...
	// while there are still nodes left and a node with an item
	// equivalent to 'item' has not been found, search forward
	// until the end of this list is reached...
	pStartNode = (pStartNode->getNextPtr() != getFirst_())
		      ? pStartNode->getNextPtr()	// $[TI1.1.1.1]
		      : NULL;				// $[TI1.1.1.2]
      }   // $[TI1.1.2] -- no iterations...

      break;
    case BACKWARD:		// $[TI1.2]
      while (pStartNode != NULL &&
	     *((const Alarm*)(pStartNode->getItem())) != item)
      {   // $[TI1.2.1] -- at least one iteration...
	// while there are still nodes left and a node with an item
	// equivalent to 'item' has not been found, search backward
	// until the beginning of this list is reached...
	pStartNode = (pStartNode != getFirst_())
		      ? pStartNode->getPrevPtr()	// $[TI1.2.1.1]
		      : NULL;				// $[TI1.2.1.2]
      }   // $[TI1.2.2] -- no iterations...

      break;
    default:
      AUX_CLASS_ASSERTION_FAILURE(((direction     << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(Alarm)));	    // bottom 12 bits...
      break;
    }  // switch (direction)...

    break;
  case EQUIV_REFERENCES:  // $[TI2]
    {
      // get the reference (address) stored by the "start" node...
      const Alarm* const  P_START_REF = (const Alarm*)(pStartNode->getItem());

      if (P_START_REF != &item)
      {   // $[TI2.1]
	// find the matching reference...
	void*  pMem = &item;  // avoiding:  "initializer for non-const ref..."

	// use the reference list's 'findNode_()' to find the reference
	// to 'item'...
	pStartNode = rPtrList_.findNode_(pMem, pStartNode, direction);
      }   // $[TI2.2] -- 'pStartNode' already points to a matching node...
    }

    break;
  default:
    AUX_CLASS_ASSERTION_FAILURE(((foundWhen     << 22) |  // top 10 bits...
				 (getMaxItems() << 12) |  // middle 10 bits...
				 sizeof(Alarm)));	  // bottom 12 bits...
    break;
  }  // switch (foundWhen)...

  return(pStartNode);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  print_(ostr)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that provides a means for a derived
//  class to dump the contents of an instance to 'ostr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented
