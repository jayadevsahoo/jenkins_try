#ifndef TaskInfo_HH
#define TaskInfo_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TaskInfo - Used to store and retrieve Task data
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/TaskInfo.hhv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

#include "Sigma.hh"
#include "IpcIds.hh"

//@ Usage-Classes
//@ End-Usage

#ifdef	SIGMA_DEVELOPMENT
  // forward reference
  class Ostream;
#endif  //SIGMA_DEVELOPMENT


//@ Type: TaskState
// This enumerated list defines the possible task states.
enum TaskState 
{ 
    TASK_INACTIVE,
    TASK_RUNNING,
    TASK_EXITED,
    TASK_ABORTED,
    TASK_ASSERT_FAILED,
    TASK_OUT_OF_BOUNDS,
    TASK_COMPILER_CHECK,
    TASK_DELETED,
    TASK_RETURNED
};


typedef void (*EntryPtr)(void);

struct TaskTableEntry;

class TaskInfo 
{
  public:

	//@ Constant: MAX_NUM_TASKS
    // Maximum number of tasks.
    enum { MAX_NUM_TASKS = 27 };

	//enum for thread ID special values for internal use
	enum TaskIds { NULL_TASK_ID = -1 };

	//@ Type: Priority
	//  Enumeration defines the highest and lowest tasking priorities.
	enum Priority
	{
		INVALID_PRIORITY = -1,
		HIGHEST_PRIORITY = 0,
		MIDRANGE_PRIORITY = 127,
		LOWEST_PRIORITY_NOT_IDLE  = 254,
		IDLE_PRIORITY = 255,
		LOWEST_PRIORITY = IDLE_PRIORITY,
	};

    TaskInfo(const Int32 taskId, const TaskTableEntry& entry);
 
    void                Create(void);
    void                Resume(void); 

	//TaskInfo is an object (instance) for the task base setup (initial) parameters.
	//Do not confuse some of them with the current OS value during run time
	//Do not confuse some of these APIs (specifically getState and getPriority)
	//with actual OS thread functions..

	// <------- TaaskInfo object (instance) APIs ------------>
    inline Int32               GetIndex(void) const;
    inline const char *        GetName(void) const;
    inline EntryPtr            GetEntryPtr(void) const;
    inline Uint32              GetFlags(void) const;
    inline TaskState           GetState(void) const;
    inline Int32               GetBasePriority(void) const;
    inline unsigned char *     GetStack(void) const;
    inline size_t              GetStackSize(void) const;
    inline IpcId               GetQueue(void) const;
    inline Uint32              GetMonitorPeriod(void) const;
    inline IpcId               GetMonitorQueue(void) const;
    inline Boolean             IsFlagOn(const Uint32 mask) const;
#ifdef	SIGMA_DEVELOPMENT
    inline void                setDebugFlags(const Uint32 debugFlags);
    inline Uint32              getDebugFlags(void) const;
    inline Boolean             isDebugOn(const Uint32 mask) const;
#endif // SIGMA_DEVELOPMENT
   
	// <------ static functions, not specific to an instance ------->
    static void				Initialize(void);
	static inline TaskInfo&	GetTaskInfo(const Int32 index);
	static inline Uint32	GetTaskCount(void);
	static Int32			MapOsPriority(Int32 priority); //helper API
	static inline Int32		FindThreadIndex(Uint32 thrdId); //helper API

    static void	SoftFault( const SoftFaultID softFaultID,
		         			const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL);
  
  protected:

  private:
    // these methods are purposely declared, but not implemented...
    TaskInfo(void);
    ~TaskInfo(void);
    TaskInfo(const TaskInfo&);		// not implemented...
    void   operator=(const TaskInfo&);	// not implemented...

	// Helper function to set priority of a thread cretaed for this TaskInfo
	Boolean	SetThreadPriority_(Int32 priority);

    //@ Data-Member: taskId_
    // Internal variable that holds the uniquie index number for the task.
	// For certain RTOS like VRTX in which the caller to create the thread
	// determines the ID, this number is considered the thread ID.
	// In OS like Windows or Linux, etc, the OS returns a thread ID to us,
	// so this number is not the thread ID but only and internal index to
	// reference the thread with.
    Int32 taskIdx_;

	//@ Data-Member: taskId_
    // variable that holds the uniquie thread I.D. returned by the OS
	// Some OS may assign for us the thread ID vs. taking an assigned ID 
    Uint32 osTaskId_;

	//@ Data-Member: pThread_
    // unique handle to the created thread of "this" TaskInfo object.
	// Returned by the OS if applicable (depending on the OS, may be inapplicable)
	Uint32* pThread_;

    //@ Data-Member: name_
    // Pointer to the task name
    const char* pName_;

    //@ Data-Member: entryPoint_
    // Internal variable that holds a pointer to the function that runs
    // as the task.
    EntryPtr entryPoint_;

    //@ Data-Member: basePriority_
    // Internal variable that holds the priority as given at creation time
    // of this task.
    Int32 basePriority_;

    //@ Data-Member: pStack_;
    // Internal variable pointing to the beginning of the 
    // task's stack area
    Byte * pStack_;

    //@ Data-Member: stackSize_;
    // Internal variable containing the task's stack size
    // task's user stack area
    size_t stackSize_;

    //@ Data-Member: debugFlags_
    // Internal variable that represents 32 bits of debug information.
    Uint32 debugFlags_;

    //@ Data-Member: flags_
    // Internal variable that represents 32 bits of information
    // initialized from task table
    Uint32 flags_;

	//TODO E600, VM: this state should reflect the actual returned state from 
	//the OS call not what we like to put in it! because the OS may simply 
	//change the state of this task while this parameter is unaware and not reflecting
	//the new change, then what if someone liked to get it thinking it's actually the current state?!
    //@ Data-Member: state_
    // Internal variable that holds the state of the task.
    TaskState state_;

    //@ Data-Member: queue_
    // Internal variable containing the queue ID of the queue
    // which receives task control messages for this task
    IpcId queue_;

    //@ Data-Member: monitorPeriod_
    // Internal variable containing the Task-Monitor reporting
    // period (milliseconds) for this task.
    Uint32 monitorPeriod_;

    //@ Data-Member: monitorQueue_
    // Internal variable containing the queue ID of the
    // queue which receives Task-Monitor messages for this task.
    IpcId monitorQueue_;


	// <----- static helper functions, not specific to the instance -------->
	static inline TaskInfo&		GetTaskInfoByIndex_(const Int32 idx);
	static inline Boolean		IsValidIndex_(const Int32 idx);

	//@ Data-Member: Initialized_
    // Private data member set true after Initialize member function
    // called.
    static Boolean Initialized_;

    //@ Data-Member: PTaskInfoObjects_
    // Internal array containing pointers to all Task objects
    static TaskInfo *PTaskInfoObjects_;

    //@ Data-Member: TaskCount_
    // This is an internal variable that holds the count of how
    // many tasks have been created.
    static Int32 TaskCount_;
};

#ifdef	SIGMA_DEVELOPMENT
Ostream & operator << (Ostream &os,const TaskInfo &rTask);
#endif // SIGMA_DEVELOPMENT

// Inlined methods
#include "TaskInfo.in"

#endif // TaskInfo_HH 
