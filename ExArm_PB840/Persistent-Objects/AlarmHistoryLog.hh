
#ifndef AlarmHistoryLog_HH
#define AlarmHistoryLog_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  AlarmHistoryLog - Alarm History Log.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/AlarmHistoryLog.hhv   25.0.4.0   19 Nov 2013 14:18:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah    Date:  08-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.  Also, removed
//	unneeded unit test code.
//
//  Revision: 002  By:  sah    Date:  15-Mar-1997    DCS Number: 1832
//  Project:  Sigma (R8027)
//  Description:
//      Fixed problem with detecting when to initialize, by adding
//	back the 'MAX_ENTRIES_' data member.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "PersistentObjsId.hh"
#include "TemplateMacros.hh"
#include "NovRamStatus.hh"

//@ Usage-Classes
#include "AlarmLogEntry.hh"

// forward declarations...
class FixedArray(AlarmLogEntry,MAX_ALARM_ENTRIES);
class AlarmAction;
//@ End-Usage


class AlarmHistoryLog
{
  public:
    AlarmHistoryLog(void);
    ~AlarmHistoryLog(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    NovRamStatus::InitializationId  initialize(void);
    NovRamStatus::VerificationId    verifySelf(void) const;

    void  getCurrEntries(
	      FixedArray(AlarmLogEntry,MAX_ALARM_ENTRIES)& rArrEntries
			) const;

    void  logAlarmAction(const AlarmAction& alarmEvent);

    void  clearAlarmEntries(void);

    static void  SoftFault(const SoftFaultID softDiagID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  private:
    AlarmHistoryLog(const AlarmHistoryLog&);	// not implemented...
    void  operator=(const AlarmHistoryLog&);	// not implemented...

    inline Uint32  getNextIndex_(const Uint32 fromIndex) const;
    inline Uint32  getPrevIndex_(const Uint32 fromIndex) const;

    inline size_t  getEntriesOffset_(void) const;

    //@ Data-Member:  MAX_ENTRIES_
    // Maximum number of entries of this log.  Even though this class can
    // know this value without this member (i.e., it knows to use
    // '::MAX_ALARM_ENTRIES'), this member is still needed because it
    // can tell the difference between zeroed out NOVRAM, and previously-
    // initialized NOVRAM.
    const Uint32  MAX_ENTRIES_;

    //@ Data-Member:  headIndex_
    // Index to the head of the log, where the next logged entry will be
    // placed.
    Uint32  headIndex_;

    //@ Data-Member:  tailIndex_
    // Index to the tail of the log.
    Uint32  tailIndex_;

    //@ Data-Member:  arrEntries_
    // An array to contain each of this log's entries.
    AlarmLogEntry  arrEntries_[::MAX_ALARM_ENTRIES];
};


// Inlined methods...
#include "AlarmHistoryLog.in"


#endif // AlarmHistoryLog_HH 
