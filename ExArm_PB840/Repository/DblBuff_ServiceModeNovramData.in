
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//	      Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class:  DblBuff_ServiceModeNovramData - Double-Buffered Data Class.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/DoubleBufferData.I_v   25.0.4.0   19 Nov 2013 14:18:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By:  sah    Date:  26-Jun-1997    DCS Number: 1908
//  Project:  Sigma (R8027)
//  Description:
//     Add mechanism to allow different semaphores to be used with different
//     instances of this class.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSemaphoreId_()
//
//@ Interface-Description
//  Returns this instance's semaphore ID.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Extracts it from the internal structure, and casts to the proper
//  enum type.
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

inline NovRamSemaphore::AccessIdType
DblBuff_ServiceModeNovramData::getSemaphoreId_(void) const
{
  CALL_TRACE("getSemaphoreId_()");
  return((NovRamSemaphore::AccessIdType)semaphoreLayout_.semaphoreId);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateSemaphoreId_(semaphoreId)
//
//@ Interface-Description
//  Updates this instance's internal semaphore value with the value given
//  by 'semaphoreId'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  First, this method sets up a local copy of 'SemaphoreLayout_' with
//  both the new semaphore ID, and its corresponding checksum.  Then,
//  in a single, non-interruptable long word write, this instance's
//  semaphore values are updated.
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  (getSemaphoreId_() == semaphoreId  &&  isSemaphoreIdValid_())
//@ End-Method 
//===================================================================== 

inline void
DblBuff_ServiceModeNovramData::updateSemaphoreId_(
			      const NovRamSemaphore::AccessIdType semaphoreId
				      )
{
  CALL_TRACE("updateSemaphoreId_(semaphoreId)");
  SemaphoreLayout_  newSemaphoreLayout;
  newSemaphoreLayout.semaphoreId = (Uint16)semaphoreId;
  newSemaphoreLayout.checksum    = (Uint16)semaphoreId;
  semaphoreMemory_ = *((Uint32*)&newSemaphoreLayout);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isSemaphoreIdValid_()
//
//@ Interface-Description
//  Returns a boolean indicating whether this instance's semaphore ID
//  is valid.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Ensure that the checksum is the inverse of the semaphore ID.
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

inline Boolean
DblBuff_ServiceModeNovramData::isSemaphoreIdValid_(void) const
{
  CALL_TRACE("isSemaphoreIdValid_()");
  return(semaphoreLayout_.semaphoreId == semaphoreLayout_.checksum);
}   // $[TI1] (TRUE)  $[TI2] (FALSE)
