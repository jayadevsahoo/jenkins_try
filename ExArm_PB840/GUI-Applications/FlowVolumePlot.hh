#ifndef FlowVolumePlot_HH
#define FlowVolumePlot_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: FlowVolumePlot - A loop plot which plots flow against
//                         volume.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/FlowVolumePlot.hhv   25.0.4.0   19 Nov 2013 14:07:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc   Date:  05-Jan-2009     SCR Number: 6283
//  Project:  840S
//  Description:
//       Removed forwarding functions in favor of virtual functions.
//
//  Revision: 001   By: rhj   Date:  20-Sept-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//       Initial version.    
//====================================================================

#include "LoopPlot.hh"

//@ Usage-Classes
//@ End-Usage


class FlowVolumePlot : public LoopPlot
{
public:
    FlowVolumePlot(void);
    ~FlowVolumePlot(void);

    static void SoftFault(const SoftFaultID softFaultID,
                            const Uint32      lineNumber,
                            const char*       pFileName  = NULL, 
                            const char*       pPredicate = NULL);

private:
    // these methods are purposely declared, but not implemented...
    FlowVolumePlot(const FlowVolumePlot&);      // not implemented...
    void   operator=(const FlowVolumePlot&);    // not implemented...


};

#endif // FlowVolumePlot_HH 
