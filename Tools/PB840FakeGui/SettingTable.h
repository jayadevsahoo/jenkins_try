#pragma once

enum SettingColumnId
{
	SETTING_COLUMN_NAME,
#if defined(SHOW_UNIT_COLUMN)
	SETTING_COLUMN_UNIT,
#endif
	SETTING_COLUMN_VALUE,
	SETTING_COLUMN_PRECISION,
	SETTING_COLUMN_DESC,
	TOTAL_SETTING_COLUMNS
};

// describe a setting column
struct SettingColumnEntry
{
	SettingColumnId id;
	const TCHAR* name;			// the setting column name
	bool	readonly;			// readonly or not
	int		width;				// the width of the column
};

// setting entry type
enum SettingEntryType
{
	SET_BOUNDED,				// show as edit box when editing
	SET_DISCRETE				// show as drop list when editing
};

struct SettingEntry
{
	const UINT			id;		// SettingId type
	const TCHAR*		name;	// setting name
	SettingEntryType	type;	// setting entry type

	const TCHAR*		unit;	// setting unit
	const TCHAR*		desc;	// setting description
	
	

	const UINT			defParam1;	// used for default PRECISION for bounded value 
									// used for default VALUE for DISCRETE value
	const TCHAR*		boundedDefValue;
	
};

class SettingTable
{
public:
	SettingTable(void);
	~SettingTable(void);

	static void							Initialize(void);

	static const SettingEntry &			GetSettingTableEntry(const Int32 index);
    static const SettingEntry &			GetSettingEntryById(const Int32 id);

	static const SettingColumnEntry &			GetColumnEntry(const Int32 index);
    static const SettingColumnEntry &			GetColumnEntryById(const Int32 id);

    static Int32						GetNumSettingEntries(void);
	static Int32						GetNumColumnEntries(void);

private:
	static const Uint					NumEntries_;
	static const Uint					NumColumns_;

	static const SettingEntry*			PSettingById_[];
	static const SettingColumnEntry*	PColumnById_[];

	static const SettingEntry			Table_[];
	static const SettingColumnEntry		ColumnTable_[];
};
