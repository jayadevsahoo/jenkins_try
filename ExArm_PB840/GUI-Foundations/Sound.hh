#ifndef Sound_HH
#define Sound_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Sound - Interface to the Windows sound system API's, allowing the activation
// of the various sounds that the ventilator needs to make.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Sound.hhv   25.0.4.0   19 Nov 2013 14:11:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 004  By: yyy      Date: 01-Dec-1998  DCR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding sound.
//
//  Revision: 003  By: ceb      Date: 08-Mar-1996  DCR Number: 1953
//    Project:  Sigma (R8027)
//    Description:
//      Added RELEASE & KNOB enumerators to SoundId.
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "Keys.hh"
#include "resource.h"

//@ Usage-Classes
//@ End-Usage


class Sound
{
public:
	//@ Type: SoundId
	// An enumerated type listing the different sounds 
	// The last three sounds are continuous.
	enum SoundId
	{
		CONTACT ,				// sound of key being pressed
		ACCEPT ,				// valid input
		INVALID_ENTRY ,			// invalid data

		ALARM_VOLUME ,			// continuous
		LOW_URG_ALARM ,			// one time
		MEDIUM_URG_ALARM ,		// continuous
		HIGH_URG_ALARM ,		// continuous

		RELEASE ,				// sound of valid key being released
		KNOB ,					// rotary knob moved
		REMIND ,				// something has timed out

		NO_SOUND 
	};

	enum AlarmSoundRange
	{
		MIN_ALARM_VOL = 1,
		MAX_ALARM_VOL = 10
	};

	enum UserSoundRange
	{
		MIN_USER_VOL = 1,
		MAX_USER_VOL = 10,
		DEFAULT_USER_VOL = 5
	};

	static void		Initialize( void);
	static void Start(SoundId soundId);
	static void CancelAlarm(void);
	static void CancelAlarmVolume(void);
	static void SetAlarmVolume(Int32 alarmVolume);
	static void SetUserVolume(Int32 userSoundVolume);

	static void	KeyPressSound( GuiKeys);
	static void	KeyReleaseSound( GuiKeys);
	static void	KnobRotateSound( void);


	// configure specific sounds

	inline static void	EnableKnobRotateSound( void);
	inline static void	DisableKnobRotateSound( void);
	inline static void	EnableKeySound( GuiKeys key);
	inline static void	DisableKeySound( GuiKeys key);
	
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	Sound(void);					// Declared but not implemented
	~Sound(void);					// Declared but not implemented
	Sound(const Sound&);			// Declared but not implemented
	void operator=(const Sound&);	// Declared but not implemented

	//@ Data-Member: KeySoundFlag_
	// Array of key sounds enabled (TRUE) or not (FALSE)
	static Boolean 		KeySoundFlag_[NUMBER_OF_GUI_KEYS];

	//@ Data-Member: KnobSoundFlag_
	// knob sound enabled (TRUE)
	static Boolean 		KnobSoundFlag_;

	//@ Data-Member: AlarmSound_
	// Id of current alarm
	static SoundId	AlarmSound_;

	//@ Data-Member: VolumeAlarmFlag_
	// Indicate the playing of Volume sound
	static Boolean 		VolumeAlarmFlag_;

	//@ Data-Member: AudibleFailureCount_
	// Indicate the audible failure count
	static Uint32     AudibleFailureCount_;

	//@ Data-Member: SavedUserSoundVolume_
	// Saved user sound volume
	static Uint32 		SavedUserSoundVolume_;

	//@ Data-Member: SavedAlarmSoundVolume_
	// Saved Alarm sound volume
	static Uint32 		SavedAlarmSoundVolume_;

};

// Inlined methods

#include "Sound.in"

#endif // Sound_HH 
