#ifndef ScrollableLog_HH
#define ScrollableLog_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995-2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ScrollableLog - Generic scrollable log class which allows
// the viewing of information in a tabular form.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScrollableLog.hhv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc	   Date:  06-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Changed to LargeContainer from old Container.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "LargeContainer.hh"
#include "ButtonTarget.hh"
#include "LogTarget.hh"
#include "ScrollbarTarget.hh"

//@ Usage-Classes
#include "Box.hh"
#include "Line.hh"
#include "LogCell.hh"
#include "Scrollbar.hh"
#include "TextField.hh"
#include "TextFont.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class ScrollableLog : public LargeContainer, public ScrollbarTarget,
						public ButtonTarget
{
public:
	ScrollableLog(LogTarget *pTarget, Uint16 numDisplayRows,
						Uint16 numDisplayColumns, Uint16 rowHeight,
						Boolean displayScrollbar = TRUE);
    ~ScrollableLog(void);

	static ScrollableLog *New(LogTarget *pTarget, Uint16 numDisplayRows,
						Uint16 numDisplayColumns, Uint16 rowHeight,
						Boolean displayScrollbar = TRUE);

	void setColumnInfo(Uint16 columnNumber, StringId title,
						Uint16 width, Alignment alignment = CENTER,
						Uint16 margin = 0,
						TextFont::Style fontStyle = TextFont::NORMAL,
						TextFont::Size oneLineFontSize = TextFont::TWELVE,
						TextFont::Size twoLineFontSize = TextFont::TWELVE);

	virtual void activate(void);
	virtual void deactivate(void);

	void setEntryInfo(Uint16 firstDisplayedEntry, Uint16 numEntries);
	void setSelectedEntry(Uint16 newSelectedEntry);
	void clearSelectedEntry(void);

	Uint16 getFirstDisplayedEntry(void);
	SigmaStatus getSelectedEntry(Uint16 &rSelectedEntry);

	void updateEntry(Uint16 changedEntry);
	void updateColumn(Uint16 columnNumber);
	void updateEntryColumn(Uint16 changedEntry, Uint16 columnNumber);

	void setUserScrollable(Boolean isUserScrollable);
	void setUserSelectable(Boolean isUserSelectable);

	// Inherited from being a ScrollbarTarget
	virtual void scrollHappened(Uint16 firstDisplayedEntry);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
												Boolean byOperatorAction);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
protected:

private:
    // these methods are purposely declared, but not implemented...
    ScrollableLog(const ScrollableLog&);				// not implemented...
    void   operator=(const ScrollableLog&);		// not implemented...

	void refreshAll_(void);
	void refreshRow_(Uint16 rowNumber);
	void refreshCell_(Uint16 rowNumber, Uint16 columnNumber);
	void updateSelectButtons_(void);
	void highlightRow_(Uint16 rowNumber);
	void unhighlightRow_(Uint16 rowNumber);
	Uint16 displayRowToCellRow_(Uint16 displayRow);
	Uint16 cellRowToDisplayRow_(Uint16 cellRow);

	enum
	{
		//@ Constant: MAX_DISPLAY_ROWS_
		// The maximum number of rows that ScrollableLog can display.
		MAX_DISPLAY_ROWS_ = 12,
	
		//@ Constant:
		// The maximum number of columns that ScrollableLog can display.
		MAX_DISPLAY_COLUMNS_ = 6
	};

	// Structure for storing information regarding default text formatting
	// features particular to each column of ScrollableLog.
	struct ColumnInfo_
	{
		Uint16 width;						// Column width
		Alignment alignment;				// Text alignment, RIGHT/LEFT/CENTER
		Uint16 margin;						// Left or right margin in pixels.
		TextFont::Style fontStyle;			// Font style o
		TextFont::Size oneLineFontSize;
		TextFont::Size twoLineFontSize;
	};

	//@ Data-Member: pTarget_
	// A pointer to the object from which ScrollableLog retrieves the textual
	// content of each cell in the log.  Also the object which is informed
	// by any user selection/deselection of a row of the log.
	LogTarget* pTarget_;

	//@ Data-Member: cells_
	// An array of LogCell object which look after rendering the textual
	// content of each cell.
	LogCell cells_[MAX_DISPLAY_ROWS_][MAX_DISPLAY_COLUMNS_];

	//@ Data-Member: horizLines_
	// An array of horizontal lines that separate each row.
	Line horizLines_[MAX_DISPLAY_ROWS_];

	//@ Data-Member: vertLines_
	// An array of horizontal lines that separate each column.
	Line vertLines_[MAX_DISPLAY_COLUMNS_ + 1];

	//@ Data-Member: columnTitles_
	// An array of TextFields for displaying column titles.
	TextField columnTitles_[MAX_DISPLAY_COLUMNS_];

	//@ Data-Member: columnInfos_
	// An array of ColumnInfo_ structures for storing text formatting
	// information on each column.
	ColumnInfo_ columnInfos_[MAX_DISPLAY_COLUMNS_];

	//@ Data-Member: titleSeparator_
	// Thick line which separates the column titles from the log cells.
	Box titleSeparator_;

	//@ Data-Member: scrollbar_
	// The scrollbar.
	Scrollbar scrollbar_;

    //@ Constant: NUM_DISPLAY_ROWS_
    // The number of rows displayed at one time.
    const Uint16 NUM_DISPLAY_ROWS_;
 
    //@ Constant: NUM_DISPLAY_COLUMNS_
    // The number of columns displayed.
    const Uint16  NUM_DISPLAY_COLUMNS_;
 
    //@ Constant: ROW_HEIGHT_
    // The height of each row in pixels (includes the one pixel of
    // the horizontal row dividing line).
    const Uint16 ROW_HEIGHT_;
 
	//@ Data-Member: firstDisplayedEntry_
	// The index into the overall log of the log entry that is displayed on
	// the first row of ScrollableLog.
	Uint16 firstDisplayedEntry_;

	//@ Data-Member: numEntries_
	// The number of entries in the whole log.
	Uint16 numEntries_;

	//@ Data-Member: selectedEntry_
	// The index of the currently selected entry (-1 if there is no selection).
	// Selected entry is always displayed onscreen.
	Int16 selectedEntry_;

	//@ Data-Member: firstCellRow_
	// This variable keeps track of the row index of the cells that appear
	// on the first row of ScrollableLog.  This starts off at 0.  Then,
	// for example, when the user scrolls down a row, cells on row 1 are
	// moved to row 0, hence firstCellRow_ goes to 1.
	Uint16 firstCellRow_;

	//@ Data-Member: displayScrollbar_
	// Flag which is set to TRUE if a scrollbar was chosen to be displayed
	// alongside ScrollableLog (this is the normal choice).
	Boolean displayScrollbar_;

	//@ Data-Member: isUserScrollable_
	// Flag which determines if the user can use the scrollbar or not.  If
	// FALSE both buttons on the scrollbar enter the flat state.
	Boolean isUserScrollable_;

	//@ Data-Member: isUserSelectable_
	// If TRUE row select button appear at the right of the last column
	// of ScrollableLog allowing the user to select/deselect a specific
	// row.
	Boolean isUserSelectable_;

	//@ Data-Member: selectRowButtons_
	// An array of pointers to row select buttons, allowing easier manipulation
	// of the buttons (cannot make an array of button objects as the Button
	// class has no default constructor).
	Button *selectRowButtons_[MAX_DISPLAY_ROWS_];

	//@ Data-Member: selectRow0Button_
	// The row select button used on row 0.
	Button selectRow0Button_;

	//@ Data-Member: selectRow1Button_
	// The row select button used on row 1.
	Button selectRow1Button_;

	//@ Data-Member: selectRow2Button_
	// The row select button used on row 2.
	Button selectRow2Button_;

	//@ Data-Member: selectRow3Button_
	// The row select button used on row 3.
	Button selectRow3Button_;

	//@ Data-Member: selectRow4Button_
	// The row select button used on row 4.
	Button selectRow4Button_;

	//@ Data-Member: selectRow5Button_
	// The row select button used on row 5.
	Button selectRow5Button_;

	//@ Data-Member: selectRow6Button_
	// The row select button used on row 6.
	Button selectRow6Button_;

	//@ Data-Member: selectRow7Button_
	// The row select button used on row 7.
	Button selectRow7Button_;

	//@ Data-Member: selectRow8Button_
	// The row select button used on row 8.
	Button selectRow8Button_;

	//@ Data-Member: selectRow9Button_
	// The row select button used on row 9.
	Button selectRow9Button_;

	//@ Data-Member: selectRow10Button_
	// The row select button used on row 10.
	Button selectRow10Button_;

	//@ Data-Member: selectRow11Button_
	// The row select button used on row 11.
	Button selectRow11Button_;

};

#endif // ScrollableLog_HH
