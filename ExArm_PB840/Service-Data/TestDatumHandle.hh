
#ifndef TestDatumHandle_HH
#define TestDatumHandle_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class:  TestDatumHandle -  Provides a dedicated data access handle 
//          for each specific service data item.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/TestDatumHandle.hhv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "TestDataId.hh"
#include "TestInfo.hh"
#include "ServiceDataClassIds.hh"

class TestDatumHandle
{
  public:
    TestDatumHandle(const TestDataId::TestDataItemId testId);
    ~TestDatumHandle(void);
    
	// Methods to retrieve the data value.
	inline const Real32 &getDataValue(void);

	// Methods to retrieve the command value.
	inline const Int &getCommandValue(void);

	// Methods to retrieve the status value.
	inline const Int &getStatusValue(void);

	// Methods to query for valid data type
	inline Boolean isData(void);

	// Methods to query for valid command type
	inline Boolean isCommand(void);

	// Methods to query for valid status type
	inline Boolean isStatus(void);
	
    static void  SoftFault(const SoftFaultID softFaultID,
				  const Uint32      lineNumber,
				  const char*       pFileName  = NULL, 
				  const char*       pPredicate = NULL);

  private:
    TestDatumHandle(void); 					// not implemented...
    TestDatumHandle(const TestDatumHandle&); // not implemented...
    void operator=(const TestDatumHandle&);  // not implemented.

   //@Data-Member : pAgent_;
   // A pointer to a data processing agent object for the service item
   // data type being handled.
    TestInfo *pAgent_;
};


// Inlined methods...
#include "TestDatumHandle.in"
#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

#endif // TestDatumHandle_HH 
