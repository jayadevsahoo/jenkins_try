
#ifndef TrendPresetTypeSetting_HH
#define TrendPresetTypeSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  TrendPresetTypeSetting - Trend Preset Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TrendPresetTypeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: rhj    Date:  05-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Initial version
//====================================================================

//@ Usage-Classes
#include "NonBatchDiscreteSetting.hh"
//@ End-Usage


class TrendPresetTypeSetting : public NonBatchDiscreteSetting
{
public:
	TrendPresetTypeSetting(void);
	virtual ~TrendPresetTypeSetting(void);

	virtual Applicability::Id  getApplicability(const Notification::ChangeQualifier qualifierId) const;

	virtual SettingValue  getDefaultValue(void) const;

	static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL);

private:
	TrendPresetTypeSetting(const TrendPresetTypeSetting&); // not implemented...
	void  operator=(const TrendPresetTypeSetting&);	 // not implemented...
};


#endif // TrendPresetTypeSetting_HH 
