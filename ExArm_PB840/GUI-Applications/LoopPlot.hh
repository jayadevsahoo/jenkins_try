#ifndef LoopPlot_HH
#define LoopPlot_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LoopPlot - A graphical class that can plot a stream of
// waveform data against another stream of patient data for use in the
// Waveforms subscreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LoopPlot.hhv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By: yyy      Date: 14-Oct-1997  DR Number: 2421
//    Project:  Sigma (R8027)
//    Description:
//      Removed all references related to auto zero.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "WaveformPlot.hh"

//@ Usage-Classes
#include "BreathPhaseType.hh"
#include "PatientDataId.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class WaveformIntervalIter;

class LoopPlot : public WaveformPlot
{
public:
	LoopPlot(void);
	~LoopPlot(void);

	virtual void activate(void);
	virtual void update(WaveformIntervalIter& segmentIter);
	virtual void endPlot(void);
	virtual void erase(void);

	inline void setXYUnits(PatientDataId::PatientItemId xUnits,
					PatientDataId::PatientItemId yUnits);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	LoopPlot(const LoopPlot&);		// not implemented...
	void   operator=(const LoopPlot&);	// not implemented...

	//@ Data-Member: xUnits_
	// The X units patient data id which specifies which patient data we
	// should plot on the X axis.
	PatientDataId::PatientItemId xUnits_;

	//@ Data-Member: yUnits_
	// The Y units patient data id which specifies which patient data we
	// should plot on the Y axis.
	PatientDataId::PatientItemId yUnits_;

	//@ Data-Member: currX_
	// The current X pixel coordinate.
	Uint32 currX_;

	//@ Data-Member: currY_
	// The current Y pixel coordinate.
	Uint32 currY_;

	//@ Data-Member: lastX_
	// The last X pixel coordinate, i.e. the one previous to the current.
	Int32 lastX_;

	//@ Data-Member: lastY_
	// The last Y pixel coordinate, i.e. the one previous to the current.
	Int32 lastY_;

	//@ Data-Member: isLastPointSet_
	// Indicates if there is a valid point stored in lastX_ and lastY_.
	Boolean isLastPointSet_;

	//@ Data-Member: lineColor_
	// Indicates the current plot line color 
	Colors::ColorS lineColor_;

	//@ Data-Member: currPhaseType_
	// Indicates the breath phase type for the current sample
	BreathPhaseType::PhaseType currPhaseType_; 

	//@ Data-Member: prevPhaseType_
	// Indicates the breath phase type for the previous sample
	BreathPhaseType::PhaseType prevPhaseType_;
};

// Inlined methods
#include "LoopPlot.in"

#endif // LoopPlot_HH 
