//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file SocketClient.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"SocketClient.h"
#include<stdio.h>
#include"CLogger.h"

//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// SocketClient::SocketClient
//---------------------------
SocketClient::SocketClient(const char *ip, const char *port, const char *type):m_bListening(false),
    m_port(),
    m_socket_id(),
    m_socket_options(),
    m_sockaddr(),
    m_sockaddr_client()
{

    static bool b_winsock = false;
    if(b_winsock == false)
    {
        b_winsock = true;
        WSADATA info; 
        //WSAStartup(MAKELONG(1, 1), &info);
		if (WSAStartup(MAKEWORD( 2, 2 ), &info) != 0)
		{
			int nRetCode = WSAGetLastError();
		    LOG_MESSAGE(LM_ERROR, "SocketClient::SocketClient( ) - failed for WSAStartup with error code %d", nRetCode); 
		}
    }

    LOG_MESSAGE(LM_INFO, "SocketClient(%X)::SocketClient() constructor is called.",this ); 

    m_socket_id = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (m_socket_id  == INVALID_SOCKET)
    {
        LOG_MESSAGE(LM_CRITICAL, "SocketClient(%X)::SocketClient() failed to create a socket.",this ); 
        //TODO[ME]: Throw an error
    }




    m_sockaddr.sin_addr.s_addr  = inet_addr (ip);

    m_sockaddr.sin_family      = AF_INET;
    m_sockaddr.sin_port        = htons(atoi(port));

    m_bListening = true;

    m_bUpdate=false;

    LOG_MESSAGE(LM_INFO, "SocketClient(%X)::SocketClient() is created",this ); 


}

bool SocketClient::updateConfigureation(char *ip, char *port)
{
	
	m_sockaddr_new.sin_addr.s_addr  = inet_addr (ip);
	m_sockaddr_new.sin_family      = AF_INET;
	m_sockaddr_new.sin_port        = htons(atoi(port));

	m_bUpdate=true;


    LOG_MESSAGE(LM_INFO, "SocketClient(%X)::SocketClient() configuration is updated.",this ); 

	return true;
}




//---------------------------
// SocketClient::~SocketClient
//---------------------------
SocketClient::~SocketClient()
{
    LOG_MESSAGE(LM_INFO, "SocketClient(%X):~SocketClient() destructor is called.",this ); 
    close();
}

//---------------------------
// SocketClient::recv
//---------------------------
ssize_t SocketClient::send(char *pData, size_t length)
{
    int iRet;

    if(m_bUpdate)
    {
        memcpy(&m_sockaddr,&m_sockaddr_new,sizeof(m_sockaddr));
        m_bUpdate=false;
    }
	


    iRet = sendto(m_socket_id,pData,length,0,(struct sockaddr *)&m_sockaddr,sizeof(m_sockaddr));


    return iRet;

}

//---------------------------
// SocketClient::close
//---------------------------
void SocketClient::close()
{
     LOG_MESSAGE(LM_INFO, "SocketClient(%X)::close() is called.",this ); 


    if(m_bListening == false)
    {
        return;
    }

    m_bListening = false;
    ::closesocket(m_socket_id);

}

