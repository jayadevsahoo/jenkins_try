#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaIntervalSetting - Apnea Interval Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the interval
//  (in milliseconds), between the start of an inspiration and the start of
//  the next inspiration, that when exceeded, shall activate apnea
//  ventilation.  This class inherits from 'BatchBoundedSetting' and provides
//  the specific information needed for representation of apnea interval.
//  This information includes the interval and range of this setting's values,
//  this batch setting's new-patient value (see 'getNewPatientValue()'), and
//  the contraints of this setting.
//
//  This class provides a static method to indicate whether apnea is 
//  possible based on the current (accepted or adjusted) value of apnea
//  interval and the non-apnea respiratory rate.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class also an 'updateConstraints_()' method for updating the
//  constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaIntervalSetting.ccv   25.0.4.0   19 Nov 2013 14:27:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: rhj    Date: 11-Oct-2010     SCR Number: 6679
//  Project: PROX
//  Description:
//     Modified the minimum of the apnea interval setting down to 3
//     in neo modes only.
// 
//  Revision: 008   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 007   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 006   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//	*  modified to use new circuit-specific new-patient values
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 004   By: sah  Date:  27-Oct-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	As part of the unit test peer review, it was noticed that
//	the 'BASED_ON_NEW_PATIENT_VALUE' part of 'IsApneaPossible()'
//	is never called, therefore that block is being removed as part
//	of the review re-work.
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: ssyw    Date:  06-Jan-1997    DR Number:
//  Project:  BILEVEL
//  Description:
//	Bilevel baseline.  Handle BiLevel case.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaIntervalSetting.hh"
#include "SettingConstants.hh"
#include "ModeValue.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "RespRateSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

#define DEFAULT_MAX_VALUE_  Real32(60000.0f)

//  $[02067] -- The setting's range ...
//  $[02069] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	3000.0f,		// absolute minimum...
	0.0f,		// unused...
	THOUSANDS,		// unused...
	NULL
};
static const BoundedInterval  NODE1_ =
{
	DEFAULT_MAX_VALUE_,		// absolute maximum...
	1000.0f,		// resolution...
	THOUSANDS,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	DEFINED_UPPER_ALARM_LIMIT_OFF,	// absolute maximum...
	(DEFINED_UPPER_ALARM_LIMIT_OFF - DEFAULT_MAX_VALUE_),	// resolution...
	THOUSANDS,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaIntervalSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BatchBoundedSetting.  The method passes to
//  BatchBoundedSetting it's default information. Also, the value interval
//  is initialized.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaIntervalSetting::ApneaIntervalSetting(void)
: BatchBoundedSetting(SettingId::APNEA_INTERVAL,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  APNEA_INTERVAL_MAX_ID, // maxConstraintId...
					  APNEA_INTERVAL_MIN_ID) // minConstraintId...
{
	CALL_TRACE("ApneaIntervalSetting()");
	isATimingSetting_ = TRUE;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaIntervalSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaIntervalSetting::~ApneaIntervalSetting(void)
{
	CALL_TRACE("~ApneaIntervalSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Apnea interval is ALWAYS changeable.
//
//  $[01096] -- apnea breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	ApneaIntervalSetting::getApplicability(const Notification::ChangeQualifier) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	return(Applicability::CHANGEABLE);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  IsApneaPossible(basedOnCategory)  [static]
//
//@ Interface-Description
//  Is apnea currently possible with regards to the "proposed" apnea
//  interval and respiratory rate?  Only the adjusted context can be
//  queried (during PRODUCTION).
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean ApneaIntervalSetting::IsApneaPossible(BasedOnCategory basedOnCategory)
{
	CALL_TRACE("IsApneaPossible(basedOnCategory)");

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const Setting*  pApneaInterval =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_INTERVAL);

	Boolean  isApneaPossible;

	DiscreteValue  modeValue;
	BoundedValue   apneaInterval;

	switch ( basedOnCategory )
	{
		case BASED_ON_ADJUSTED_VALUES :	  // $[TI1]
			// get the "adjusted" values...
			modeValue     = pMode->getAdjustedValue();
			apneaInterval = pApneaInterval->getAdjustedValue();
			break;
		case BASED_ON_ACCEPTED_VALUES :
			// fall through to the 'default' case, if not DEVELOPMENT...
#if defined(SIGMA_DEVELOPMENT)
			// get the "accepted" value...
			modeValue     = pMode->getAcceptedValue();
			apneaInterval = pApneaInterval->getAcceptedValue();
			break;
#endif // defined(SIGMA_DEVELOPMENT)
		case BASED_ON_NEW_PATIENT_VALUES :
		default :
			// unexpected 'basedOnCategory' value...
			AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
			break;
	};

	switch ( modeValue )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :
		case ModeValue::BILEVEL_MODE_VALUE :  // $[TI2]
			{
				// in the A/C, SIMV and BILEVEL modes, apnea is only possible if the apnea
				// interval is less than the non-apnea breath period...
				Real32  breathPeriodValue;

				// get the breath period value...
				breathPeriodValue = RespRateSetting::GetBreathPeriod(basedOnCategory);

				// $[TI2.1] (TRUE)  $[TI2.2] (FALSE)...
				isApneaPossible = (apneaInterval.value < breathPeriodValue);
			}  // end of block ([TI1])...
			break;
		case ModeValue::SPONT_MODE_VALUE :	  // $[TI3]
			// in SPONT mode, apnea is ALWAYS possible...
			isApneaPossible = TRUE;
			break;
		case ModeValue::CPAP_MODE_VALUE : 
			isApneaPossible = (apneaInterval != DEFINED_UPPER_ALARM_LIMIT_OFF);
			break;
		default :
			// invalid 'modeValue'...
			AUX_CLASS_ASSERTION_FAILURE(modeValue);
			break;
	};

	return(isApneaPossible);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02068] -- new-patient value...
//  $[LC02002]\c\ -- in CPAP, Apnea Interval shall be set to OFF
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue ApneaIntervalSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	BoundedValue  newPatient;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			newPatient.value = 10000.0f;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			newPatient.value = 15000.0f;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			newPatient.value = 20000.0f;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const DiscreteValue MODE = pMode->getAdjustedValue();

	if (MODE == ModeValue::CPAP_MODE_VALUE)
	{
		newPatient.value = DEFINED_UPPER_ALARM_LIMIT_OFF;
	}

	newPatient.precision = THOUSANDS;

	return(newPatient);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  The setting is only interested in the transition of mode type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Transition rules.
//  $[LC02001]\a\ when exiting CPAP, if setting is OFF, set to new patient value
//---------------------------------------------------------------------
//@ PreCondition
//  settingId == SettingId::MODE
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void ApneaIntervalSetting::acceptTransition(const SettingId::SettingIdType settingId,
											const DiscreteValue  newValue,
											const DiscreteValue  currValue)
{
	AUX_CLASS_PRE_CONDITION(settingId == SettingId::MODE, settingId);

	if (ModeValue::CPAP_MODE_VALUE == newValue ||
		(ModeValue::CPAP_MODE_VALUE == currValue && 
		 DEFINED_UPPER_ALARM_LIMIT_OFF == BoundedValue(getAdjustedValue())))
	{
		updateConstraints_();

		SettingValue apneaInterval = getNewPatientValue();

		// using this setting's constraints, round/clip new patient value
		// into the apnea interval's current valid range
		getBoundedRange_().warpValue(apneaInterval);

		// store the transition value into the adjusted context...
		setAdjustedValue(apneaInterval);
	}
}

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void ApneaIntervalSetting::SoftFault(const SoftFaultID  softFaultID,
									 const Uint32       lineNumber,
									 const char*        pFileName,
									 const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							APNEA_INTERVAL_SETTING, lineNumber, pFileName,
							pPredicate);
}


//===================================================================
//
//  Protected Methods...
//
//===================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the maximum lower bound, and set the lower limit to it.
//  $[LC02003]\f\ in CPAP, OFF is an allowable setting for Ta
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void ApneaIntervalSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	const Real32 ABSOLUTE_MIN = getAbsoluteMinValue_();

	const BoundedSetting*  pApneaRespRate =
		SettingsMgr::GetBoundedSettingPtr(SettingId::APNEA_RESP_RATE);

	BoundedRange::ConstraintInfo  minConstraintInfo;

	BoundedValue  apneaBreathPeriodBasedMin;

	// calculate the apnea breath period-based minimum...
	apneaBreathPeriodBasedMin.value =
		(SettingConstants::MSEC_TO_MIN_CONVERSION /
		 BoundedValue(pApneaRespRate->getAdjustedValue()).value);

	if ( apneaBreathPeriodBasedMin.value > ABSOLUTE_MIN )
	{	// $[TI1]
		// reset this range's minimum constraint to allow for "warping" of a
		// new minimum value...
		getBoundedRange_().resetMinConstraint();

		// since this is a lower-bound, the calculated bound value needs to
		// be warped "up"...
		getBoundedRange_().warpValue(apneaBreathPeriodBasedMin,
									 BoundedRange::WARP_UP);

		// the breath-period-based minimum is more restricive, therefore set it
		// as the lower bound of apnea interval...
		minConstraintInfo.id    = APNEA_INTERVAL_MIN_BASED_APNEA_RR_ID;
		minConstraintInfo.value = apneaBreathPeriodBasedMin.value;
	}
	else
	{	// $[TI2]
		// the absolute minimum is more restricive, therefore set it as the
		// lower bound of apnea interval...
		minConstraintInfo.id    = APNEA_INTERVAL_MIN_ID;
		minConstraintInfo.value = ABSOLUTE_MIN;
	}

	// this setting has no minimum soft bounds...
	minConstraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMinConstraint(minConstraintInfo);


	// calculate and set the maximum constraint

	const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

	BoundedRange::ConstraintInfo  maxConstraintInfo;

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const DiscreteValue MODE = pMode->getAdjustedValue();

	// apnea interval is constrained to upper bound of 60 sec with
	// non-CPAP mode, otherwise the absolute max OFF is used
	if ( MODE != ModeValue::CPAP_MODE_VALUE)
	{	// $[TI1]
		BoundedValue modeBasedMax;
		modeBasedMax.value = DEFAULT_MAX_VALUE_;

		BoundStatus  dummyStatus(getId());

		// reset this range's maximum constraint to allow for "warping" of a
		// new maximum value...
		getBoundedRange_().resetMaxConstraint();

		// warp the calculated value to a "click" boundary...
		getBoundedRange_().warpValue(modeBasedMax);

		// the apnea interval is constrained by the mode
		// therefore save its value and id as the maximum bound value and id...
		maxConstraintInfo.id    = APNEA_INTERVAL_MAX_ID;
		maxConstraintInfo.value = modeBasedMax.value;
	}
	else
	{
		// the absolute maximum (OFF) is applicable, therefore save its value
		// and id as the maximum bound value and id...
		maxConstraintInfo.id    = APNEA_INTERVAL_MAX_ID;
		maxConstraintInfo.value = ABSOLUTE_MAX;
	}

	// this setting has no maximum soft bounds...
	maxConstraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);

}

//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Use this to return the absolute minimum value that is currently available
//  from this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02111] -- The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	ApneaIntervalSetting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");

	Real32  absoluteMinValue;

	const DiscreteValue  CIRCUIT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAcceptedValue();

    switch (CIRCUIT_TYPE)
    {
    case PatientCctTypeValue::ADULT_CIRCUIT :
    case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
        absoluteMinValue = 10000.0F;  // absolute minimum...
        break;
    case PatientCctTypeValue::NEONATAL_CIRCUIT :
        absoluteMinValue = 3000.0F;  // absolute minimum...
        break;
    default:
        // Invalid CIRCUIT_TYPE Setting
        AUX_CLASS_ASSERTION_FAILURE( CIRCUIT_TYPE );
        break;
    }

	return(absoluteMinValue);
}


