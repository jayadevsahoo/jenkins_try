#ifndef	XmitData_HH
#define	XmitData_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename: XmitData.hh - indirect way of including NetworkApp.hh
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/XmitData.hhv   25.0.4.0   19 Nov 2013 14:14:36   pvcs  $
//@ Modification-Log
//
//  Revision: 001  By:  <jhv>    Date:  06-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================


#ifndef NetworkApp_HH
#include "NetworkApp.hh"
#endif //NetworkApp_HH

#endif	// XmitData_HH
