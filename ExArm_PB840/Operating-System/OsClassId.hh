
#ifndef OsClassId_HH
#define OsClassId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  OsClassId - Defines ids for all of the Operating System's modules.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/OsClassId.hhv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: rhj    Date:  13-April-2006    DR Number: 6192
//  Project:  XENA
//  Description:
//       Reorganized the code to support the XENA Spansion Flash memory,
//       and the older Flash memory chips.    
//  
//  Revision: 001   By: sah    Date:  15-Oct-1997    DR Number: 1529
//  Project: Sigma (R8027)
//  Description:
//	Initial version 
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage

//@ Type:  OsClassId
// Ids for all of this sub-system's classes/modules.
enum OsClassId
{
  TIME_DAY_ID		= 0,
  KEYBOARD_ID		= 1,
  FLASH_PROGRAMMER	= 2,
  NUM_OS_CLASSES	= 3
};


#endif // OsClassId_HH 
