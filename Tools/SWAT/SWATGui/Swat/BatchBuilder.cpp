
#include "stdafx.h"
#include "BatchBuilder.h"
#include "SwatMessage.h"
#include "BatchCommandInfo.h"
#include "NetworkMessageHeader.h"
#include <string.h>
#include <winsock.h>


//namespace swat{

    char BatchBuilder::m_clEnableSwatNetworkMessageHeader [sizeof(CommandInfo)+sizeof(NetworkMessageHeader)];
    char BatchBuilder::m_clDisableSwatNetworkMessageHeader[sizeof(CommandInfo)+sizeof(NetworkMessageHeader)];


    inline float my_htonl(float x){
        unsigned int y;
        y = htonl(*((unsigned int *)(&x)));
        return *((float *)(&y));
    }

    inline unsigned int my_htonl(unsigned int x){
        unsigned int y;
        y = htonl(*((unsigned int *)(&x)));
        return *((unsigned int *)(&y));
    }


    inline int my_htonl(int x){
        unsigned int y;
        y = htonl(*((unsigned int *)(&x)));
        return *((int *)(&y));
    }


    float my_nothl(float x){
        unsigned int y;
        y = ntohl(*((unsigned int *)(&x)));
        return  *((float *)(&y));
    }


    //So that Visual Studio does not cast away negative numbers.
    unsigned int toUnsigned(int x)
    {
        unsigned int *val = (unsigned int*)&x;

        return *val;
    }


    void BatchBuilder::mark()
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_MARK)));
    }

    void BatchBuilder::mark      (unsigned int iEventId)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_MARKEVENT),            my_htonl(iEventId)));
    }

    void BatchBuilder::setindex   (int iIndex)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_SETINDEX),             my_htonl(iIndex)));
    }

    void BatchBuilder::skipindex  (int iIndex)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_SKIPINDEX),            my_htonl(iIndex)));
    }

    void BatchBuilder::repeatn    (unsigned int iCount)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_REPEATN),              my_htonl(iCount)));
    }

    void BatchBuilder::repeattime (unsigned int iTime )
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_REPEATTIME),           my_htonl(iTime )));
    }

    void BatchBuilder::waittime   (unsigned int iTime )
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_WAITTIME),             my_htonl(iTime )));
    }

    void BatchBuilder::repeatevent(unsigned int iEventId, unsigned int iTimeExpire)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_REPEATEVENT),          my_htonl(iEventId), my_htonl(iTimeExpire)));
    }

    void BatchBuilder::waitevent  (unsigned int iEventId, unsigned int iTimeExpire)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_WAITEVENT),            my_htonl(iEventId),my_htonl(iTimeExpire)));
    }

    void BatchBuilder::signalevent(unsigned int iEventId)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_SIGNALEVENT),          my_htonl(iEventId)));
    }

    void BatchBuilder::setsensor  (unsigned int iSensorId)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_SETSENSORBUFFERVALUE), my_htonl(iSensorId)));
    }

    void BatchBuilder::setsensor  (unsigned int iSensorId, float fValue)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_SETSENSOR),            my_htonl(iSensorId),my_htonl(fValue)));
    }

    void BatchBuilder::addsensor  (unsigned int iSensorId, float fValue)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_ADDSENSOR),            my_htonl(iSensorId),my_htonl(fValue)));
    }

    void BatchBuilder::unsetsensor(unsigned int iSensorId)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_UNSETSENSOR),          my_htonl(iSensorId)));
    }

    void BatchBuilder::end()
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_END)));
    }

    void BatchBuilder::resetall()
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_RESETALL)));
    } 

    void BatchBuilder::setrawsensor  (unsigned int iSensorId, unsigned int uiValue)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_SETRAWSENSOR),         my_htonl(iSensorId),my_htonl(uiValue)));
    }


    void BatchBuilder::unsetrawsensor(unsigned int iSensorId)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_UNSETRAWSENSOR),       my_htonl(iSensorId)));
    }

    void BatchBuilder::addrawsensor(unsigned int iSensorId, unsigned int uiValue)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_ADDRAWSENSOR),       my_htonl(iSensorId), my_htonl(uiValue)));
    }

    void BatchBuilder::simulateguitouch(unsigned int x, unsigned int y)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_SIMULATE_TOUCH),      my_htonl(x), my_htonl(y)));
    }

    void BatchBuilder::simulatebezelkey(unsigned int keyId, unsigned int onOff)
    {
        m_cnt++;
        m_BatchCommands.push_back(BatchCommand(my_htonl(BatchCommand::CMD_SIMULATE_BEZEL),      my_htonl(keyId), my_htonl(onOff)));
    }

    inline int BatchBuilder::getLength()
    {
        return (sizeof(BatchCommandInfo)+sizeof(BatchCommand)*(m_cnt)+sizeof(NetworkMessageHeader));
    }

    char *BatchBuilder::getBatch()
    {
        BatchCommand *pCmd;

        if(m_pData)
        {
            delete [] m_pData;
        }
        m_pData = new char[getLength()];


        ((NetworkMessageHeader *)m_pData)->iLength=my_htonl(getLength());
        ((NetworkMessageHeader *)m_pData)->iOffset=0;

        BatchCommandInfo *pBatch;
        pBatch= (BatchCommandInfo *)(m_pData+sizeof(NetworkMessageHeader));
        pBatch->iNCMD = my_htonl(m_cnt);
        memcpy(pBatch->clCommandInfo.TypeUUID, SwatMessage::EXECUTESCRIPT, sizeof(uuid_t_));
        pCmd = (BatchCommand *)(((char *)pBatch)+sizeof(BatchCommandInfo));

        std::vector<BatchCommand>::iterator it=m_BatchCommands.begin(); 

        for(int i=0;it!=m_BatchCommands.end() && i<m_cnt;it++,i++)
        {
            pCmd[i]=*it;
        }

        return (char *)m_pData;
    }



    char *BatchBuilder::generateSwatEnable()
    {
        return (char *)m_clEnableSwatNetworkMessageHeader;
    }
    char *BatchBuilder::generateSwatDisable()
    {
        return (char *)m_clDisableSwatNetworkMessageHeader;
    }


    BatchBuilder::BatchBuilder():m_pData(0),m_cnt(0)
    {
        ((NetworkMessageHeader*)m_clEnableSwatNetworkMessageHeader) ->iLength=my_htonl(sizeof(NetworkMessageHeader)+sizeof(CommandInfo));
        ((NetworkMessageHeader*)m_clDisableSwatNetworkMessageHeader)->iLength=my_htonl(sizeof(NetworkMessageHeader)+sizeof(CommandInfo));
        ((NetworkMessageHeader*)m_clEnableSwatNetworkMessageHeader) ->iOffset=0;
        ((NetworkMessageHeader*)m_clDisableSwatNetworkMessageHeader)->iOffset=0;

        CommandInfo *pTemp;

        pTemp = (CommandInfo *)(((char *)m_clDisableSwatNetworkMessageHeader)+sizeof(NetworkMessageHeader));
        memcpy((char *)pTemp->TypeUUID, SwatMessage::DISABLESWAT, sizeof(uuid_t_));

        pTemp=(CommandInfo *)(((char *)m_clEnableSwatNetworkMessageHeader)+sizeof(NetworkMessageHeader));
        memcpy((char *)pTemp->TypeUUID, SwatMessage::ENABLESWAT, sizeof(uuid_t_));
    }

    BatchBuilder::~BatchBuilder()
    {
        if(m_pData)
        {
            delete [] m_pData;
            m_pData = 0;
        }
    }


//}
