#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: DciReport - Format DCI output report(s)
//---------------------------------------------------------------------
//@ Interface-Description
//  This file contains the formatting functions for the Dci class.
//  When called the formatting function creates an ASCII string and
//  appends it to the output MISCA report.
//---------------------------------------------------------------------
//@ Rationale
//  Isolate report production and formatting from the rest of the Dci class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Module-Decomposition
//	error()
//	reportHeader_()
//	reportATrailer_()
//	reportSNIDTrailer_()
//	reportSLMTTrailer_()
//	reportSERTrailer_()
//	initReport_()
//	appendStringToReport_()
//	appendNoCommaStringToReport_()
//	float61_()
//	float62_()
//	ieRatio62_()
//	int6_()
//	string2_()
//	string3_()
//	string4_()
//	string5_()
//	string6_()
//	string9_()
//	string12_()
//	string16_()
//	string18_()
//---------------------------------------------------------------------
//@ Fault-Handling
//  CLASS_PRE_CONDITION for any DCI report data that is out of range.
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/DCI/vcssrc/DciReport.ccv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010    By: rhj              Date: 11-Jul-2006  DR Number: 5928
//  Project: RESPM 
//  Description:
//      Added float63_ function to enable output of decimal numbers 
//      to the thousandths place. 
//
//  Revision: 009    By: ljs              Date: 8-Dec-2003  DR Number: 6127
//  Project: Baseline
//  Description:
//      Modified to make Serial Communications more robust by:
//      - Log error into NOVRAM if it was the first error of that type,
//        or if using an Engineering/Production DataKey.
//
//  Revision: 008    By: hlg           Date: 07-SEP-2001  DR Number: 5954
//  Project: GuiComms
//  Description:
//      Modified Method error to give unique DCI error codes for each port.
//      Error codes correspond to those listed in CommFaultId.hh.
//
//  Revision: 007    By: quf           Date: 13-JUL-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Modified to support GuiComms functionality.
//
//  Revision: 006    By: hct           Date: 20-OCT-2000  DR Number: 5757
//  Project: Baseline
//  Description:
//      Added functionality to support Marquette monitors.
//
//  Revision: 005    By: hct              Date: 03-Nov-1998  DR Number: 5252
//  Project: Sigma (R8027)
//  Description:
//      Added back in requirement number 473 deleted in revision 1.14.
//
//  Revision: 004    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2676
//  Project: Sigma (R8027)
//  Description:
//      (DCS 2676) Corrected MISCA field 101.
//
//  Revision: 003	By: Gary Cederquist  Date: 03-Dec-1997  Number: 2605
//	Project:  Sigma (R8027)
//	Description:
//		Renamed private Error_() method to public Error() since it is called 
//      from an external subsystem (GUI-Serial-Interface).
//
//  Revision: 002	By: Gary Cederquist  Date: 30-SEP-1997  Number: 2527
//	Project:  Sigma (R8027)
//	Description:
//		Changed PCV Control field length to 9 characters from 6.    
//
//  Revision: 1.0	By: jdm	Date: 7/26/95	DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "Dci.hh"
#include "FaultHandler.hh"
#include "DiagnosticCode.hh"
#include "CommFaultId.hh"
#include <stdio.h>
#include <string.h>

// allocate space for static class's data storage (declared in Dci.hh)

// additional allocated space (used only within this file)
static	const	char *	DciStx_ = "\002";
static	const	char *	DciEtx_ = "\003";
static	const	char *	DciReportTerminator_ = "\015";	// carriage return
static	const	char *	DciBlankedCRC_ = "    ";		// blanked CRC string

	// fields not supported shall be blank		// $[00473]
static	const	char *	NotValidString2_ = "  ";
static	const	char *	NotValidString3_ = "   ";
static	const	char *	NotValidString4_ = "    ";
static	const	char *	NotValidString5_ = "     ";
static	const	char *	NotValidString6_ = "      ";
static	const	char *	NotValidString9_ = "         ";
static	const	char *	NotValidString12_ = "            ";
static	const	char *	NotValidString16_ = "                ";
static	const	char *	NotValidString18_ = "                  ";

static	const	struct
{
	Dci::DciErrorCode	code;		// code for DCI error report
	CommFaultID		fault;		// code for Sigma Comm log
} DciCommFaults_[] =
{
	// DciErrorCode				CommFaultId
	//-------------------------------	--------------------------------
	Dci::PARITY_ERROR,			DCI_PARITY_ERROR,
	Dci::INPUT_BUFFER_OVERFLOW_ERROR,	DCI_INPUT_BUFFER_OVERFLOW_ERROR,
	Dci::NON_SPECIFIC_ERROR,		DCI_NON_SPECIFIC_ERROR,
};

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  error
//
//@ Interface-Description
// Pass an null-terminated input buffer containing invalid input and
// produce a DCI error report indicating same.  Also log the error to
// the Sigma Communciations diagnostic log.
//---------------------------------------------------------------------
//@ Implementation-Description
// Return the most recently seen 4 input characters, not counting
// terminating carriage returns, as part of the report.  If less than
// 4 characters were seen, blank-pad on the right of the invalid input
// field
//
//  $[00465] In response to any unknown messages a communications error
//  report ("??") shall be returned.
//  $[00469] The code 7081 shall be returned when the communication
//  error is detected to be a parity error and 7088 when an overflow
//  of the input buffer occurs.
//  $[00470] All other error shall return the 7031 error code.
//  $[00471] The last four characters prior to the <CR> in the host
//  message shall be contained within square brackets of the Error Data
//  field.
//  $[00475] An overflow of the input buffer shall result in an "??"
//  message being transmitted and the input buffer being flushed
//  (emptied).
//  $[00477] A Communications Diagnostic Code Log entry shall be 
//  created for each occurrance of a DCI error code.
//---------------------------------------------------------------------
//@ PreCondition
// Invalid DCI host input was received
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::error(const char * pString, DciErrorCode code, Boolean b_log)
{
	DiagnosticCode	dCode;
	char		errorReport[SCRATCH_BUFFER_SIZE];
	//  fault calculation should correspond to the ennumeration in CommFaultId.hh in subsystem Utilities
	CommFaultID	fault = (CommFaultID)(DCI_UNKNOWN_ERROR + portNum_ * 10);          //account for port number
	Int32		written = 0;

	written = sprintf(errorReport, "??,%6.6s,%-6d,[%-4.4s]\xD",
		getTime_(), code, pString);		//	$[00468] $[00471]
	CLASS_ASSERTION( written <= sizeof(errorReport) );

	output_(errorReport);

    if (TRUE == b_log)
    {
    	// look up the corresponding Sigma fault code to log
    	for(Int32 ii = 0; ii < NUMBER_OF_DCI_ERRORS; ii++)
    	{						// $[TI1.1]
    		if(DciCommFaults_[ii].code == code)
    		{					// $[TI2.1]
    			//  fault calculation should correspond to the ennumeration in CommFaultId.hh in subsystem Utilities
    			fault = (CommFaultID)(DciCommFaults_[ii].fault + portNum_ * 10);  //account for port number
    			break;
    		}
    							// $[TI2.2]
    	}						// $[TI1.2]
	
    	// make an entry in the Sigma Communications diagnostic log
    	dCode.setCommunicationCode(fault);

		// TODO E600
    	//FaultHandler::LogDiagnosticCode(dCode);			//	$[00477]
    							// $[TI1]
    } // end if().                            
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  reportHeader_
//
//@ Interface-Description
// Initialize the MISCA report and append MISCA header info to the
// report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::reportHeader_(const char* reportId, const char* header)
{
	// begin a report
	initReport_();

	appendStringToReport_(reportId);

	if (header != NULL)  // $[TI1]
	{
	  appendStringToReport_(header);
	  appendStringToReport_(DciStx_);
	} // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  reportATrailer_
//
//@ Interface-Description
// Append the MISCA report termination sequence into the report buffer
// and output the report (subject to flow control) via the DCI serial
// port
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::reportATrailer_(void)
{
	// finish off a MISCA report
	appendStringToReport_(DciEtx_);
	appendStringToReport_(DciReportTerminator_);

	output_(outputBuffer_);
							// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  reportSNIDTrailer_
//
//@ Interface-Description
// Append the SNID report termination sequence into the report buffer
// and output the report (subject to flow control) via the DCI serial
// port
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::reportSNIDTrailer_(void)
{
	// finish off an SNID report
	appendStringToReport_(DciEtx_);
	appendNoCommaStringToReport_(DciBlankedCRC_);
	appendNoCommaStringToReport_(DciReportTerminator_);

	output_(outputBuffer_);
							// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  reportSLMTTrailer_
//
//@ Interface-Description
// Append the SLMT report termination sequence into the report buffer
// and output the report (subject to flow control) via the DCI serial
// port
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::reportSLMTTrailer_(void)
{
	// finish off an SLMT report
	appendStringToReport_(DciEtx_);
	appendNoCommaStringToReport_(DciBlankedCRC_);
	appendNoCommaStringToReport_(DciReportTerminator_);

	output_(outputBuffer_);
							// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  reportSERTrailer_
//
//@ Interface-Description
// Append the SER report termination sequence into the report buffer
// and output the report (subject to flow control) via the DCI serial
// port
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::reportSERTrailer_(void)
{
	// finish off an SER report
	appendNoCommaStringToReport_(DciReportTerminator_);

	output_(outputBuffer_);
							// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initReport_
//
//@ Interface-Description
// Discard and reset output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the buffer pointer to 0 and null out the next character to fill.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::initReport_(void)
{
	outputBuffer_[nextOut_ = 0] = NULL;
							// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  appendStringToReport_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to append onto
// the current report output buffer with proper delimitation.
//---------------------------------------------------------------------
//@ Implementation-Description
// If this isn't the first thing appended, pre-pend a reportfield
// delimiter to the string.  Null-terminate the resulting output buffer
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::appendStringToReport_(const char * pString)
{
	if(nextOut_ && !(ctrlChar_))
	{						// $[TI1.1]
		// add a field delimiter if this isn't the first field
		// and not STX or ETX
		outputBuffer_[nextOut_++] = FIELD_DELIMITER;
	}
							// $[TI1.2]

	// trap buffer misuse (i.e.: overflow)
	CLASS_ASSERTION((strlen(pString) + nextOut_ - 1) < sizeof(outputBuffer_));

	if(*pString == 0x02 || *pString == 0x03)
	{						// $[TI2.1]
		ctrlChar_ = TRUE;
	}
	else
	{						// $[TI2.2]
		ctrlChar_ = FALSE;
	}

	while(*pString)
	{						// $[TI3.1]
		outputBuffer_[nextOut_++] = *pString++;
	}
							// $[TI3.2]
	outputBuffer_[nextOut_] = 0;	// null-terminated
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  appendNoCommaStringToReport_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to append onto
// the current report output buffer with no delimitation.
//---------------------------------------------------------------------
//@ Implementation-Description
// Append the input string onto the report output buffer, null-terminate
// the resulting output buffer.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::appendNoCommaStringToReport_(const char * pString)
{
	// trap buffer misuse (i.e.: overflow)
	CLASS_ASSERTION((strlen(pString) + nextOut_) < sizeof(outputBuffer_));

	while(*pString)
	{						// $[TI1.1]
		outputBuffer_[nextOut_++] = *pString++;
	}
							// $[TI1.2]
	outputBuffer_[nextOut_] = 0;	// null-terminated
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  float61_
//
//@ Interface-Description
// Pass a pointer to a Real32 floating-point value to be formatted into
// an "SXXX.X" format, where 'S' can be a unary sign.  Append the formatted
// value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::float61_(void * pData)
{
	Real32		number;
	DciNumber *	pNumber = (DciNumber *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pNumber->get(&number);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-6.1f", number);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:   // $[TI1.2]
		pOutput = NotValidString6_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  float62_
//
//@ Interface-Description
// Pass a pointer to a Real32 floating-point value to be formatted into
// an "SXX.XX" format, where 'S' can be a unary sign.  Append the formatted
// value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::float62_(void * pData)
{
	Real32		number;
	DciNumber *	pNumber = (DciNumber *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pNumber->get(&number);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-6.2f", number);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED: // $[TI1.2]
		pOutput = NotValidString6_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  float63_
//
//@ Interface-Description
// Pass a pointer to a Real32 floating-point value to be formatted into
// an "SXX.XX" format, where 'S' can be a unary sign.  Append the formatted
// value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::float63_(void * pData)
{
	Real32		number;
	DciNumber *	pNumber = (DciNumber *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pNumber->get(&number);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-6.3f", number);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED: // $[TI1.2]
		pOutput = NotValidString6_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ieRatio62_
//
//@ Interface-Description
//  This method appends an I:E ratio string to the MISCA report from
//  the specified floating-point I:E ratio. For I>E it generates
//  the string format X.XX:1. For I<E, it generates the string format
//  1:X.XX. Neither string contains padding in the floating point portion.
//  The formatted string must not exceed 6 characters total so the 
//  floating point part is always truncated to 4 characters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An invalid or not-supported value is blank-filled.  Additionally,
//  during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Dci::ieRatio62_(void * pData)
{
	Real32		 ieRatio;
	const char * pOutput;

	DciNumber *	 pNumber = (DciNumber *)pData;
	DciValueStatus::DciValueStatusId status = pNumber->get( &ieRatio );

	switch(status)
	{
		case DciValueStatus::OK:			// $[TI1.1]
		{
			Real32  ePortion = computeEofIeRatio_( ieRatio );
			Real32  iPortion = computeIofIeRatio_( ieRatio );
			Int32   written = 0;
			char    temp[20];

			pOutput = scratchBuffer_;

			if ( ePortion == 1.0 )
			{
				// $[TI2.1]
				written = sprintf(temp, "%.2f", iPortion);
				CLASS_ASSERTION( written <= sizeof(temp) );
				written = sprintf(scratchBuffer_, "%.4s:1", temp);
			}
			else
			{
				// $[TI2.2]
				written = sprintf(temp, "%.2f", ePortion);
				CLASS_ASSERTION( written <= sizeof(temp) );
				written = sprintf(scratchBuffer_, "1:%.4s", temp);
			}

			CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
			break;
		}

		case DciValueStatus::INVALID:
		case DciValueStatus::NOT_SUPPORTED:
		case DciValueStatus::NOT_IN_THIS_RELEASE:
		case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
			pOutput = NotValidString6_;
			break;
		default:	// invalid status
			CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  int6_
//
//@ Interface-Description
// Pass a pointer to an Int32 integer value to be formatted into
// an "SXXXXX" format, where 'S' can be a unary sign.  Append the formatted
// value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::int6_(void * pData)
{
	Real32		number;
	DciNumber *	pNumber = (DciNumber *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pNumber->get(&number);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-6d", (Int32)number);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
		pOutput = NotValidString6_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  string2_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to be formatted into
// an "XX" format, left-adjusted and blank-filled.  Append the formatted
// value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::string2_(void * pData)
{
	char		string[MAX_DCI_STRING_SIZE];
	DciString *	pString = (DciString *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pString->get(string);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-2.2s", string);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:	
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
		pOutput = NotValidString2_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  string3_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to be formatted into
// an "XXX" format, left-adjusted and blank-filled.  Append the formatted
// value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::string3_(void * pData)
{
	char		string[MAX_DCI_STRING_SIZE];
	DciString *	pString = (DciString *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pString->get(string);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-3.3s", string);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:	
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
		pOutput = NotValidString3_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  string4_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to be formatted into
// an "XXXX" format, left-adjusted and blank-filled.  Append the formatted
// value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::string4_(void * pData)
{
	char		string[MAX_DCI_STRING_SIZE];
	DciString *	pString = (DciString *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pString->get(string);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-4.4s", string);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:	
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
		pOutput = NotValidString4_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  string5_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to be formatted into
// an "XXXXX" format, left-adjusted and blank-filled.  Append the formatted
// value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::string5_(void * pData)
{
	char		string[MAX_DCI_STRING_SIZE];
	DciString *	pString = (DciString *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pString->get(string);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-5.5s", string);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:	
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
		pOutput = NotValidString5_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  string6_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to be formatted into
// an "XXXXXX" format, left-adjusted and blank-filled.  Append the formatted
// value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::string6_(void * pData)
{
	char		string[MAX_DCI_STRING_SIZE];
	DciString *	pString = (DciString *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pString->get(string);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-6.6s", string);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:	
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
		pOutput = NotValidString6_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  string9_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to be formatted into
// an "XXXXXXXXX" format, left-adjusted and blank-filled.  Append the formatted
// value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::string9_(void * pData)
{
	char		string[MAX_DCI_STRING_SIZE];
	DciString *	pString = (DciString *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pString->get(string);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-9.9s", string);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
		pOutput = NotValidString9_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  string12_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to be formatted into
// an "XXXXXXXXXXXX" format, left-adjusted and blank-filled.  Append the
// formatted value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::string12_(void * pData)
{
	char		string[MAX_DCI_STRING_SIZE];
	DciString *	pString = (DciString *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pString->get(string);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-12.12s", string);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
		pOutput = NotValidString12_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  string16_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to be formatted into
// an "XXXXXXXXXXXXXXXX" format, left-adjusted and blank-filled.  Append the
// formatted value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::string16_(void * pData)
{
	char		string[MAX_DCI_STRING_SIZE];
	DciString *	pString = (DciString *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pString->get(string);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-16.16s", string);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
		pOutput = NotValidString16_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  string18_
//
//@ Interface-Description
// Pass a pointer to a null-terminated character string to be formatted into
// an "XXXXXXXXXXXXXXXXXX" format, left-adjusted and blank-filled.  Append the
// formatted value onto the current output report buffer
//---------------------------------------------------------------------
//@ Implementation-Description
// An invalid or not-supported value is blank-filled.  Addtionally,
// during testing, to-be-determined and DCS'd values are also blank-filled
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::string18_(void * pData)
{
	char		string[MAX_DCI_STRING_SIZE];
	DciString *	pString = (DciString *)pData;
	const char *	pOutput;
	DciValueStatus::DciValueStatusId status;
	Int32		written = 0;

	status = pString->get(string);
	switch(status)
	{
	case DciValueStatus::OK:			// $[TI1.1]
		pOutput = scratchBuffer_;
		written = sprintf(scratchBuffer_, "%-18.18s", string);
		CLASS_ASSERTION( written <= sizeof(scratchBuffer_) );
		break;
	case DciValueStatus::INVALID:
	case DciValueStatus::NOT_SUPPORTED:
	case DciValueStatus::NOT_IN_THIS_RELEASE:
	case DciValueStatus::TO_BE_DETERMINED:		// $[TI1.2]
		pOutput = NotValidString18_;
		break;
	default:	// invalid status
		CLASS_ASSERTION_FAILURE();
	}
	appendStringToReport_(pOutput);
}

