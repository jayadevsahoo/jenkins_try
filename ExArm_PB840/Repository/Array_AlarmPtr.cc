#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Array_AlarmPtr - Abstract Array of 'AlarmPtr' Elements.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is an abstraction of an array of generic elements, where
//  each element is of type 'AlarmPtr'.  Safe element access is provided
//  with this class, along with access to the size of the array,
//  copying one array into another, setting all of the elements to
//  a particular value, and comparing two arrays.  This is the
//  abstract base class of all of the fixed arrays
//  ('FixedArray<AlarmPtr,SIZE>'), and provides the basic framework, and
//  functionality of all of the fixed arrays.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a "safe" array for use with both built-in types,
//  and user-defined types.  The client of this array, as opposed
//  to a 'C'-like array, will not have to deal with pointers, and
//  cannot access elements that are out of the bounds of one of these
//  arrays.  Methods for copying and comparing are also provided by
//  this class.  The rationale for providing a templated class, as
//  opposed to writing specific array classes for each new array, is
//  that the same tested, fine-tuned code can be reused many times.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A 'C' array of a constant length is used internally by this class.
//  This class acts as a basic framework for an array class, where
//  the derived classes ('FixedArray<AlarmPtr,SIZE>') provides the
//  array pointer, and the number of elements.  This division of
//  implementation among a base array, and the fixed derived array
//  is done to reduce code explosion -- many different sizes of the
//  same type ('AlarmPtr') without having an immense growth in executable
//  size.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The one restriction on this class that limits its use as compared
//  to the built-in 'C'-like array is that a static initialization
//  list can't be used.  There are also restrictions having to do
//  with the data type ('AlarmPtr') used in this array.  The type that is
//  picked for 'AlarmPtr' must have the following functionality:
//>Von
//  AlarmPtr::AlarmPtr(void);				// default constructor...
//  AlarmPtr::operator=(const AlarmPtr& aType);		// assignment operator...
//  AlarmPtr::operator==(const AlarmPtr& aType) const;	// equivalence operator...
//  AlarmPtr::operator!=(const AlarmPtr& aType) const;	// inequivalence operator...
//>Voff
//  Any type that lacks any of those methods will cause a compile-time
//  error.  (NOTE:  all built-in types already have all of this
//  functionality.)
//---------------------------------------------------------------------
//@ Invariants
//  anArray[0..N] provides a legal instance of type 'AlarmPtr', where
//  'anArray' is an instance of this class, and 'N' represents
//  the number of elements in this array.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractArray.C_v   25.0.4.0   19 Nov 2013 14:03:40   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "Array_AlarmPtr.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~Array_AlarmPtr()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this array, and its elements.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Array_AlarmPtr::~Array_AlarmPtr(void)
{
  CALL_TRACE("~Array_AlarmPtr()");
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  cleanAllElems(newElemValue)
//
//@ Interface-Description
//  Set all of the elements in this array to 'newElemValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
Array_AlarmPtr::cleanAllElems(const AlarmPtr& newElemValue)
{
  for (Uint32 idx = 0; idx < getNumElems(); idx++)
  {
    pElems_[idx] = newElemValue;
  }
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(array)  [Assignment Operator]
//
//@ Interface-Description
//  Copy all of the elements from 'array' into this array.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (getNumElems() == array.getNumElems())
//---------------------------------------------------------------------
//@ PostCondition
//  (*this == array)
//@ End-Method
//=====================================================================

void
Array_AlarmPtr::operator=(const Array_AlarmPtr& array)
{
  CALL_TRACE("operator=(array)");

  if (this != &array)
  {	// $[TI1]
    AUX_CLASS_PRE_CONDITION((getNumElems() == array.getNumElems()),
			    ((getNumElems()       << 22) |  // top 10 bits...
			     (array.getNumElems() << 12) |  // middle 10 bits...
			     sizeof(AlarmPtr)));		    // bottom 12 bits...

    for (Uint32 idx = 0; idx < getNumElems(); idx++)
    {
      pElems_[idx] = array.pElems_[idx];
    }
  }	// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	                [static]
//
//@ Interface-Description
//	Catch the synchronous faults generated by this class, and pass
//	them to 'FaultHandler'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

void
Array_AlarmPtr::SoftFault(const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName,
			       const char*       pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, ABSTRACT_ARRAY,
			  lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEquivTo_(array)  [const]
//
//@ Interface-Description
//  Is this array equivalent to 'array'.  Two arrays are equivalent
//  if each has the same number of elements, and each of their
//  corresponding elements are equivalent.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
Array_AlarmPtr::isEquivTo_(const Array_AlarmPtr& array) const
{
  Boolean  isEquiv = (NUM_ELEMS_ == array.NUM_ELEMS_);

  if (isEquiv)
  {	// $[TI1]
    // each array has the same number of elements...
    const AlarmPtr  *pLEFT = pElems_;
    const AlarmPtr  *pRIGHT = array.pElems_;

    Uint32  cnt;

    // check to see if each element in this array ('pLEFT') is equivalent
    // to the corresponding element in the other array ('pRIGHT')...
    for (cnt = 0; cnt < NUM_ELEMS_  &&  (*pLEFT++ == *pRIGHT++); cnt++)
    {	// $[TI1.1] -- at least one iteration...
      // do nothing...
    }	// $[TI1.2] -- no iterations...

    isEquiv = (cnt == NUM_ELEMS_);	// $[TI1.3] (TRUE) $[TI1.4] (FALSE)
  }	// $[TI2]

  return(isEquiv);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  print_(ostr)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method which provides derived classes with
//  a method for "dumping" the contents of an instance to 'ostr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented
