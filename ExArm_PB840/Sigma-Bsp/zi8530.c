/************************************************************************/
/* THIS INFORMATION IS PROPRIETARY TO                                   */
/* MICROTEC - A Mentor Graphics Company                                 */
/*----------------------------------------------------------------------*/
/* Copyright (c) 1995-1997 Microtec - A Mentor Graphics Company         */
/* All rights reserved                                                  */
/************************************************************************/
/*
**The software source code contained in this file and in the related
**header files is the property of Microtec. and may only be
**used under the terms and conditions defined in the Microtec
**license agreement. You may modify this code as needed but you must 
**retain the Microtec copyright notices, including this statement.
**Some restrictions may apply when using this software with non-Microtec
**software.  In all cases, Microtec reserves all rights.
*/
/************************************************************************/

/*
 *
 *	Module Name:		%M%
 *
 *	Identification:		%Z% %I% %M%
 *
 *	Date:			%G%  %U%
 */

/*****************************************************************************
**
** zi8530.c
**
** Contents:
**
**    This file contains all of the device-dependent, board-independent
**    functions to control the zi8530 serial device.  Packet (pkt)
**    and tty (character) i/o are supported.
**
**    This file is portable to any board with a zi8530 type serial chip.
**    The only things that change from board to board are the board-dependent
**    function parameters.
**
**    In order to remove board dependencies from this file, all routines
**    access the zi8530 through a zi8530_dev_desc_t object which
**    is initialized by a call to zi8530_init. A pointer to this descriptor
**    is passed to all zi8530_x type functions to provide access to
**    the device.
**
**    zi8530 fops table functions (interface between serial.c and device)
**
**       Non-Test Functions (non-ISR functions)
**       zi8530_init............ Initialize
**       zi8530_int_info_get.... get information on interrupts for device
**       zi8530_int_info_set.... set information on interrupts for device
**       zi8530_pgetc........... get a character (polled) from the zi8530
**       zi8530_pputc........... write a character (polled) to the zi8530
**       zi8530_set_params...... set the baud,parity,bits and stop bits
**
**       Test (interrupt service routines) fops:
**       zi8530_tty_rx_tst...... tty receive character interrupt service routine
**       zi8530_tty_tx_tst...... tty transmit character interrupt service routine
**       zi8530_tty_err_tst..... tty receive error routine
**
**       zi8530_pkt_rx_tst...... pkt receive character interrupt service routine
**       zi8530_pkt_tx_tst...... pkt transmit character interrupt service routine
**       zi8530_pkt_err_tst..... pkt receive error routine
**
** History:
** Date        Name           Description
** --------------------------------------------------------------------------
** 10-28-92    Roger Cole     completed modifications
** 02-02-93    Cindy Gin      In the function zi8530_port_init: added another 
**                            case to check the register 14 for DPLL or baud
**                            rate generator as a clock source.  Changed
**                            the baud rate calculation.
** 02-04-93   Roger Cole          changed zscc and z8530 to zi8530
** 03-20-96   Makoto Saito    Fixed to support 7bit & even/odd parity for
**                            serial console
** 08-11-10   Gary Cederquist Modified port_init to reset the serial channel
**                            associated with the port. SCR 6663.
*****************************************************************************/
/* #define DEBUG */

#include <compiler.h>
#include <ansiprot.h>
#include <logio.h>
#include <vbsp.h>
#include "zi8530.h"

#ifdef DEBUG
   #include <stdio.h>
char *b0 = (char *)0x100000;
char *b1 = (char *)0x100100;
char *b2 = (char *)0x100200;
char *b3 = (char *)0x100300;
char *b4 = (char *)0x100400;
char *b5 = (char *)0x100500;
char *b6 = (char *)0x100600;
char *b7 = (char *)0x100700;
char *b8 = (char *)0x100800;
char *b9 = (char *)0x100900;
char *bp;

int  pputc_full = 0;
int  pputc_out = 0;
int  pgetc_cnt = 0;
char *pktrxp = (char *)0x101000;
char rxcnt = 0;
char *pkttxp = (char *)0x102000;
char txcnt = 0;
#endif

/*#define DEBUG1 1 */
#if DEBUG1 == 1
char dout0[50];
char dout1[150];
char dout2[1000];
   #define DEBUG_OUT0(x,a)             sprintf(x,a)            
   #define DEBUG_OUT1(x,a,b)           sprintf(x,a,b)          
   #define DEBUG_OUT2(x,a,b,c)         sprintf(x,a,b,c)        
   #define DEBUG_OUT3(x,a,b,c,d)       sprintf(x,a,b,c,d)      
   #define DEBUG_OUT4(x,a,b,c,d,e)     sprintf(x,a,b,c,d,e)    
   #define DEBUG_OUT5(x,a,b,c,d,e,f)   sprintf(x,a,b,c,d,e,f)  
   #define DEBUG_OUT6(x,a,b,c,d,e,f,g) sprintf(x,a,b,c,d,e,f,g)

   #define DEBUG_OUT0_INC(z,x,a)             {sprintf(x,a);             x+=z;}
   #define DEBUG_OUT1_INC(z,x,a,b)           {sprintf(x,a,b);           x+=z;}
   #define DEBUG_OUT2_INC(z,x,a,b,c)         {sprintf(x,a,b,c);         x+=z;}
   #define DEBUG_OUT3_INC(z,x,a,b,c,d)       {sprintf(x,a,b,c,d);       x+=z;}
   #define DEBUG_OUT4_INC(z,x,a,b,c,d,e)     {sprintf(x,a,b,c,d,e);     x+=z;}
   #define DEBUG_OUT5_INC(z,x,a,b,c,d,e,f)   {sprintf(x,a,b,c,d,e,f);   x+=z;}
   #define DEBUG_OUT6_INC(z,x,a,b,c,d,e,f,g) {sprintf(x,a,b,c,d,e,f,g); x+=z;}
#else
   #define DEBUG_OUT0(x,a)             
   #define DEBUG_OUT1(x,a,b)           
   #define DEBUG_OUT2(x,a,b,c)         
   #define DEBUG_OUT3(x,a,b,c,d)       
   #define DEBUG_OUT4(x,a,b,c,d,e)     
   #define DEBUG_OUT5(x,a,b,c,d,e,f)   
   #define DEBUG_OUT6(x,a,b,c,d,e,f,g) 

   #define DEBUG_OUT0_INC(z,x,a)             
   #define DEBUG_OUT1_INC(z,x,a,b)           
   #define DEBUG_OUT2_INC(z,x,a,b,c)         
   #define DEBUG_OUT3_INC(z,x,a,b,c,d)       
   #define DEBUG_OUT4_INC(z,x,a,b,c,d,e)     
   #define DEBUG_OUT5_INC(z,x,a,b,c,d,e,f)   
   #define DEBUG_OUT6_INC(z,x,a,b,c,d,e,f,g) 

#endif              

#define TRUE   1
#define FALSE   0
#define SETTLE_DELAY 100
#define RESET_DELAY 1000

/*************************************************************************
** zi8530_install
**
** This function initializes both the ports to a disabled state, fills
** in the serial_dev_desc structure.
**
** Inputs:
**     cfg         zi8530 config structure
**     devid       zi8530 device descriptor
** Outputs:
**     none
** Return codes:
**     VBSP_SUCCESS
**
*/
#ifdef __STDC__
vbsp_return_t 
zi8530_install(zi8530_config_t *cfg, serial_dev_desc_t *devid)
#else
vbsp_return_t 
zi8530_install(cfg, devid)
zi8530_config_t *cfg;
serial_dev_desc_t *devid;
#endif
{
	DEBUG_OUT0(dout0, "install");

	/* install register map */
	devid->map = (void *)&cfg->regs;

	/* copy config information */
	devid->config = cfg->sconf;	 /* ASSUMPTION ANSI */
	devid->config.specific = (void *)&cfg->specific;

	/* install common fops */
	devid->fops.init                     = zi8530_init;
	devid->fops.int_info_get             = zi8530_int_info_get;
	devid->fops.int_info_set             = zi8530_int_info_set;
	devid->fops.pputc                    = zi8530_pputc;
	devid->fops.pgetc                    = zi8530_pgetc;
	devid->fops.start_transmitter        = zi8530_start_transmitter;
	devid->fops.set_params               = zi8530_set_params;

	devid->externio.reg_write_b          = cfg->externio.reg_write_b;
	devid->externio.reg_write_w          = cfg->externio.reg_write_w;
	devid->externio.reg_write_l          = cfg->externio.reg_write_l;
	devid->externio.reg_read_b           = cfg->externio.reg_read_b;
	devid->externio.reg_read_w           = cfg->externio.reg_read_w;
	devid->externio.reg_read_l           = cfg->externio.reg_read_l;
	devid->externio.interrupt_start      = cfg->externio.interrupt_start;
	devid->externio.interrupt_end        = cfg->externio.interrupt_end;
	devid->externio.brd_spec_int_disable = cfg->externio.brd_spec_int_disable;
	devid->externio.brd_spec_int_enable  = cfg->externio.brd_spec_int_enable;

	/* init buffers and install specific fops */
	if ( devid->config.flavor == serial_packet_device )
	{
		/* initialize the packet structures */
		devid->buf.pkt.nxt_pkt = logio_buff_NULL;

		/* initialize the receive packet structures */
		devid->buf.pkt.rx.curr     = logio_buff_NULL;
		devid->buf.pkt.rx.byte_cnt = 0;

		/* initialize the transmit packet structures */
		devid->buf.pkt.tx.curr     = logio_buff_NULL;
		devid->buf.pkt.tx.byte_cnt = 0;

		/* get the preamble for packet processing */
		pkt_get_serial_preamble(&devid->buf.pkt.preamble);
		devid->fops.tx_tst   = zi8530_pkt_tx_tst;
		devid->fops.rx_tst   = zi8530_pkt_rx_tst;
		devid->fops.err_tst  = zi8530_err_tst;
	}
	else
	{
		/* initialize logio_charbufs for in and out */
		logio_charbuf_init(&devid->buf.tty.inbuf);
		logio_charbuf_init(&devid->buf.tty.outbuf);
		devid->fops.tx_tst   = zi8530_tty_tx_tst;
		devid->fops.err_tst  = zi8530_err_tst;
		if ( devid->config.bits == BITS_7 )
		{
			devid->fops.rx_tst   = zi8530_tty_rx_tst_7;
			devid->fops.pgetc    = zi8530_pgetc_7;
		}
		else
			devid->fops.rx_tst	 = zi8530_tty_rx_tst;
	}

	/* disable the interrupts of ports and initialize to a known state */
	zi8530_port_disable(devid);

	return(LOGIO_STATUS_OK);
}

/*****************************************************************************
**                                                                           
** zi8530_init
**
**    This routine initializes the registers specific to the zi8530.
**                                                                           
** Instructions:
**    At each comments beginning with the word "INSTRUCTION:" insert
**    the code reqeusted.
**    
** Inputs:                                                                   
** Outputs:                                                                  
**           none                                                            
**                                                                           
** Return codes:                                                             
**           VBSP_SUCCESS             The call succeeded                     
**                                                                           
**           VBSP_INVALID_VECTOR      The call failed, vector not in range   
**           VBSP_INVALID_ARBVAL      The call failed, arbval not in range   
**           VBSP_ILLEGAL_CONFIG      The call failed, config illegal        
** Side-effects:                                                             
** Global variables:                                                         
**                                                                           
******************************************************************************/
#ifdef __STDC__
vbsp_return_t 
zi8530_init(serial_dev_desc_t *devid)
#else
vbsp_return_t 
zi8530_init(devid)
serial_dev_desc_t *devid;
#endif
{
#ifdef DEBUG
	sprintf(b0,"init:");
#endif
	unsigned char *ctrlp = (unsigned char *)((zi8530_reg_map_t *)devid->map)->ctrl_port;

	/* check parity/size/stop combination to see if supported */
	if ( (devid->config.parity == PARITY_NONE && devid->config.bits == BITS_7) ||
		 (devid->config.stop == STOP_BITS_2) )
		return(VBSP_ILLEGAL_CONFIG);


	/* initialize portA & portB */
	zi8530_port_init(devid);
	REG_WRITE_B(devid,ctrlp,WR0_R1); /* select register WR1 */
	REG_WRITE_B(devid,ctrlp,00); /* Tx/Rx interrupt & mode NO DMA, RX and TX disabled */ 

	return(VBSP_SUCCESS);
}

/*****************************************************************************
**                                                                           
** zi8530_pputc
**
**    Polled putc function.  This function transmits a character to the
**    zi8530 serial device
**                                                                           
**    It is called from an output polling routine.
**
** Inputs:                                                                   
**           dev             The  descriptor  of  the  device  returned  by  
**                           zi8530_init.                               
**                                                                           
**           outchr          The character to transmit.                      
** Outputs:                                                                  
** Return codes:                                                             
**           VBSP_SUCCESS    The call succeeded                              
**                                                                           
**           VBSP_TX_BUFF_FULL                                               
**                           The transmit buffer of the port is full         
**                                                                           
** Side-effects:                                                             
** Global variables:                                                         
**                                                                           
******************************************************************************/
#ifdef __STDC__
vbsp_return_t 
zi8530_pputc(serial_dev_desc_t *devid, unsigned char outchr)
#else
vbsp_return_t 
zi8530_pputc(devid, outchr)
serial_dev_desc_t *devid;
unsigned char outchr;
#endif
{
	zi8530_reg_map_t *reg = (zi8530_reg_map_t *)devid->map;

	if ( REG_READ_B(devid,reg->ctrl_port) & RR0_TXBE )
	{

#ifdef DEBUG
		pputc_out++;
		sprintf(b5,"pputc: full %d out %d",pputc_full,pputc_out);
#endif

#ifndef DATA_PORT_ACCESSIBLE
		REG_WRITE_B(devid,reg->ctrl_port,WR0_R8);
		REG_WRITE_B(devid,reg->ctrl_port,outchr);
#else
		REG_WRITE_B(devid,reg->data_port,outchr);
#endif
		return(VBSP_SUCCESS);
	}
	else
	{
	#ifdef DEBUG
		pputc_full++;
		sprintf(b5,"pputc: full %d out %d",pputc_full,pputc_out);
	#endif
		return(VBSP_TX_BUFF_FULL);
	}
}

/*****************************************************************************
**                                                                           
** zi8530_pgetc  Polled getc function
**
**    This function returns a character from the zi8530
**
** Inputs:                                                                   
**           dev             The  descriptor  of  the  device  returned  by  
**                           zi8530_init.                               
** Outputs:                                                                  
**           rxchr           The character received                          
** Return codes:                                                             
**           VBSP_SUCCESS    The call succeeded                              
**                                                                           
**           VBSP_NO_CHAR_PRESENT                                            
**                           No character was present                        
** Side-effects:                                                             
** Global variables:                                                         
**                                                                           
******************************************************************************/
#ifdef __STDC__
vbsp_return_t 
zi8530_pgetc(serial_dev_desc_t *devid, unsigned char *rxchr)
#else
vbsp_return_t 
zi8530_pgetc(devid, rxchr)
serial_dev_desc_t *devid;
unsigned char *rxchr;
#endif
{
	char rx_stat;
	zi8530_reg_map_t *reg = (zi8530_reg_map_t *)devid->map;

	/* get status of port */
	rx_stat = REG_READ_B(devid,reg->ctrl_port);

	/* if char available then get it */
	if ( rx_stat & RR0_RXCH )
	{
#ifndef DATA_PORT_ACCESSIBLE
		REG_WRITE_B(devid,reg->ctrl_port,WR0_R8);
		*rxchr = REG_READ_B(devid,reg->ctrl_port);
#else
		*rxchr = REG_READ_B(devid,reg->data_port);
#endif

#ifdef DEBUG
		sprintf(b4,"pgetc:cnt = %d, ch=%x",*rxchr,pgetc_cnt);
#endif

		/* check for overrun */
		REG_WRITE_B(devid,reg->ctrl_port,WR0_R1);
		rx_stat = REG_READ_B(devid,reg->ctrl_port);
		if ( rx_stat & RR1_RXOVR )
		{
			REG_WRITE_B(devid,reg->ctrl_port,WR0_ERRES);   
			return(VBSP_RX_OVERRUN);
		}
		return(VBSP_SUCCESS);
	}
	return(VBSP_NO_CHAR_PRESENT);
}

#ifdef __STDC__
vbsp_return_t 
zi8530_pgetc_7(serial_dev_desc_t *devid, unsigned char *rxchr)
#else
vbsp_return_t 
zi8530_pgetc_7(devid, rxchr)
serial_dev_desc_t *devid;
unsigned char *rxchr;
#endif
{
	char rx_stat;
	zi8530_reg_map_t *reg = (zi8530_reg_map_t *)devid->map;

	/* get status of port */
	rx_stat = REG_READ_B(devid,reg->ctrl_port);

	/* if char available then get it */
	if ( rx_stat & RR0_RXCH )
	{
#ifndef DATA_PORT_ACCESSIBLE
		REG_WRITE_B(devid,reg->ctrl_port,WR0_R8);
		*rxchr = REG_READ_B(devid,reg->ctrl_port) & 0x7f;
#else
		*rxchr = REG_READ_B(devid,reg->data_port) & 0x7f;
#endif

#ifdef DEBUG
		sprintf(b4,"pgetc:cnt = %d, ch=%x",*rxchr,pgetc_cnt);
#endif

		/* check for overrun */
		REG_WRITE_B(devid,reg->ctrl_port,WR0_R1);
		rx_stat = REG_READ_B(devid,reg->ctrl_port);
		if ( rx_stat & RR1_RXOVR )
		{
			REG_WRITE_B(devid,reg->ctrl_port,WR0_ERRES);   
			return(VBSP_RX_OVERRUN);
		}
		return(VBSP_SUCCESS);
	}
	return(VBSP_NO_CHAR_PRESENT);
}

/*****************************************************************************
**                                                                           
** zi8530_start_transmitter
**
**    This routine will execute any code necessary to start the serial channel
**    transmitter EXCEPT TRANSMIT A CHARACTER.
**
** Inputs:                                                                   
**       dev             The  descriptor  of  the  device  returned  by  
**                       zi8530_init.                               
** Outputs:
**       none
** Return codes:                                                             
**       VBSP_SUCCESS                     Transmission started
**       VBSP_SERIAL_PRIME_PUMP_NEEDED    Character output needed
**                                         
** Side-effects:                                                             
** Global variables:                                                         
**                                                                           
******************************************************************************/
#ifdef __STDC__
vbsp_return_t
zi8530_start_transmitter(serial_dev_desc_t *devid)
#else
vbsp_return_t
zi8530_start_transmitter(devid)
serial_dev_desc_t *devid;
#endif
{
	return( VBSP_SERIAL_PRIME_PUMP_NEEDED );
}

/*****************************************************************************
**                                                                         
** zi8530_int_info_get
**
**    This function returns interrupt information that tells the 
**    caller which interrupts are enabled for a particular device.
**
** Inputs:
**           dev          the serial device descriptor
**           intrpt         which interrupt actually requested
** Outputs:
**           intrpt         flags field is updated 
**                                                                         
** Return codes:                                                           
**           LOGIO_STATUS_OK          The call succeeded                   
**           LOGIO_STATUS_NOTSUPPORTED
**           LOGIO_STATUS_EPARAM
**                                                                         
** Side-effects:                                                           
** Global variables:                                                       
**                                                                         
******************************************************************************/
#ifdef __STDC__
vbsp_return_t 
zi8530_int_info_get(serial_dev_desc_t *dev, logio_int_entry_t *intrpt)
#else
vbsp_return_t 
zi8530_int_info_get(dev, intrpt)
serial_dev_desc_t *dev;
logio_int_entry_t *intrpt;
#endif
{
	int ret;

	/* test that event requested is the same as supported */
	switch ( intrpt->event )
	{
		case LOGIO_INT_EVENT_TTYERROR:
		case LOGIO_INT_EVENT_TTYTXRDY:
		case LOGIO_INT_EVENT_TTYRXCHR:
			if ( dev->config.flavor == serial_packet_device )
				return(LOGIO_STATUS_EPARAM);
			break;

		case LOGIO_INT_EVENT_PCKTERROR:
		case LOGIO_INT_EVENT_PCKTTXRDY:
		case LOGIO_INT_EVENT_PCKTRXPCKT:
			if ( dev->config.flavor == serial_tty_device )
				return(LOGIO_STATUS_EPARAM);
			break;

		default:
			return(LOGIO_STATUS_EPARAM);
	}

	switch ( intrpt->event )
	{
		/* error events are same */
		case LOGIO_INT_EVENT_PCKTERROR:
		case LOGIO_INT_EVENT_TTYERROR:
			if ( !dev->fops.err_tst )
				ret = LOGIO_STATUS_NOTSUPPORTED;
			else
			{
				intrpt->flags = ((zi8530_specific_t *)
								 dev->config.specific)->zi8530_wr1 & WR1_RXIE ?
								LOGIO_INT_ENABLED : LOGIO_INT_DISABLED;
			}
			break;

		case LOGIO_INT_EVENT_PCKTTXRDY:
		case LOGIO_INT_EVENT_TTYTXRDY:
			intrpt->flags = ((zi8530_specific_t *)
							 dev->config.specific)->zi8530_wr1 & WR1_TXIE ?
							LOGIO_INT_ENABLED : LOGIO_INT_DISABLED;
			break;

		case LOGIO_INT_EVENT_PCKTRXPCKT:
		case LOGIO_INT_EVENT_TTYRXCHR:
			intrpt->flags = ((zi8530_specific_t *)
							 dev->config.specific)->zi8530_wr1 & WR1_RXIE ?
							LOGIO_INT_ENABLED : LOGIO_INT_DISABLED;
			break;
	}

	return(VBSP_SUCCESS);
}

/*****************************************************************************
**                                                                           
** zi8530_int_info_set
**
**    This function controls a specific event/id pair by enabling or
**    disabling the specific event requested.
**
** Inputs:
**           dev          the serial device descriptor
**           intrpt         which event to set
** Outputs:
**           intrpt         flags field is updated to old value
**                                                                           
** Return codes:                                                             
**           LOGIO_STATUS_OK          The call succeeded                     
**           LOGIO_STATUS_NOTSUPPORTED
**           LOGIO_STATUS_EPARAM
**                                                                           
** Side-effects:                                                             
** Global variables:                                                         
**                                                                           
******************************************************************************/
#ifdef __STDC__
vbsp_return_t 
zi8530_int_info_set(serial_dev_desc_t *devid, logio_int_entry_t *intrpt)
#else
vbsp_return_t 
zi8530_int_info_set(devid, intrpt)
serial_dev_desc_t *devid;
logio_int_entry_t *intrpt;
#endif
{
	unsigned char *ctrlp = (unsigned char *)((zi8530_reg_map_t *)devid->map)->ctrl_port;
	unsigned char val;
	zi8530_specific_t *spf;
	logio_status_t ret = LOGIO_STATUS_OK;
	int new_val = intrpt->flags;


	if ( (ret=zi8530_int_info_get(devid,intrpt)) != LOGIO_STATUS_OK )
		return( ret );

	if ( new_val == intrpt->flags )		/* do nothing */
		return(ret);

	spf = (zi8530_specific_t *)devid->config.specific;
	if ( new_val == LOGIO_INT_ENABLED )
	{
		BOARD_SPEC_INT_ENABLE(devid, intrpt);
		switch ( intrpt->event )
		{
			case LOGIO_INT_EVENT_TTYTXRDY:
			case LOGIO_INT_EVENT_PCKTTXRDY:

				val = spf->zi8530_wr1;
				val |= WR1_TXIE;
				spf->zi8530_wr1 = val;

				REG_WRITE_B(devid,ctrlp,WR0_R1); /* select register WR1 */
				REG_WRITE_B(devid,ctrlp,val);
				break;

			case LOGIO_INT_EVENT_TTYRXCHR:
			case LOGIO_INT_EVENT_PCKTRXPCKT:

				val = spf->zi8530_wr1;
				val |= WR1_RXIE;
				spf->zi8530_wr1 = val;

				REG_WRITE_B(devid,ctrlp,WR0_R1);/* select register WR1 */
				REG_WRITE_B(devid,ctrlp,val);
				break;

			case LOGIO_INT_EVENT_TTYERROR:
			case LOGIO_INT_EVENT_PCKTERROR:

				val = spf->zi8530_wr1;
				val |= WR1_RXIE;
				spf->zi8530_wr1 = val;

				REG_WRITE_B(devid,ctrlp,WR0_R1);  /* select register WR1 */
				REG_WRITE_B(devid,ctrlp,val);
				break;

			default:
				return(LOGIO_STATUS_NOTSUPPORTED);
		}
	}
	else							 /* disable interrupts */
	{
		switch ( intrpt->event )
		{
			case LOGIO_INT_EVENT_TTYTXRDY:
			case LOGIO_INT_EVENT_PCKTTXRDY:

				val = spf->zi8530_wr1;
				val &= ~WR1_TXIE;
				spf->zi8530_wr1 = val;

				REG_WRITE_B(devid,ctrlp,WR0_R1);  /* select register WR1 */
				REG_WRITE_B(devid,ctrlp,val);
				break;

			case LOGIO_INT_EVENT_TTYRXCHR:
			case LOGIO_INT_EVENT_PCKTRXPCKT:

				val = spf->zi8530_wr1;
				val &= ~WR1_RXIE;
				spf->zi8530_wr1 = val;

				REG_WRITE_B(devid,ctrlp,WR0_R1);  /* select register WR1 */
				REG_WRITE_B(devid,ctrlp,val);
				break;

			case LOGIO_INT_EVENT_TTYERROR:
			case LOGIO_INT_EVENT_PCKTERROR:

				val = spf->zi8530_wr1;
				val &= ~WR1_RXIE;
				spf->zi8530_wr1 = val;

				REG_WRITE_B(devid,ctrlp,WR0_R1);  /* select register WR1 */
				REG_WRITE_B(devid,ctrlp,val);
				break;

			default:
				return(LOGIO_STATUS_NOTSUPPORTED);
		}
		BOARD_SPEC_INT_DISABLE(devid, intrpt);
	}

#ifdef DEBUG
	sprintf(b1,"info_set flags=0x%x wr1=0x%x:\n",intrpt->flags,val);
#endif

	return(ret);
}

/*************************************************************************
** zi8530_pkt_tx_tst
**
** This function is called by the logio_default_int_handler in order to
** determine whether or not a packet was transmitted.
**
** Inputs:
**     entry: pointer to entry in interrupt table
** Outputs:
** Return codes:
**   1: serial  trsansmitted a packet
**
*/
#ifdef __STDC__
int 
zi8530_pkt_tx_tst(logio_interrupt_entry_t *entry)
#else
int 
zi8530_pkt_tx_tst(entry)
logio_interrupt_entry_t *entry;
#endif
{
	serial_dev_desc_t *devid = (serial_dev_desc_t *)entry->id->data;
	zi8530_reg_map_t *regs     = (zi8530_reg_map_t *)devid->map;
	int ret;

#ifdef DEBUG
	sprintf(b6,"pkt_tx_tst");
#endif

	INTERRUPT_START(devid, SERIAL_PKT_TX_INT );
	/* if everything has been transmitted, ret = 1, turn off transmitter interrupts */
	if ( (ret = logio_serial_packet_tx(devid)) )
	{
		REG_WRITE_B(devid,regs->ctrl_port,WR0_TXRES);
	}
	INTERRUPT_END(devid, SERIAL_PKT_TX_INT);
	REG_WRITE_B(devid,regs->ctrl_port,WR0_HIRES);
	return( ret );
}

/*************************************************************************
** zi8530_pkt_rx_tst
**
** This function is called by the logio_default_int_handler in order to
** determine whether or not a packet was received.
**
** Inputs:
**     entry: pointer to entry in interrupt table
** Outputs:
** Return codes:
**   1: serial received a character
**
*/
#ifdef __STDC__
int 
zi8530_pkt_rx_tst(logio_interrupt_entry_t *entry)
#else
int 
zi8530_pkt_rx_tst(entry)
logio_interrupt_entry_t *entry;
#endif
{                                
	serial_dev_desc_t *devid = (serial_dev_desc_t *)entry->id->data;
	zi8530_reg_map_t *regs = (zi8530_reg_map_t *)devid->map;
	register serial_pkt_tst_info_t *rx_info;
	unsigned char stat, chr;
	int ret=FALSE;

	INTERRUPT_START(devid, SERIAL_PKT_RX_INT );
	rx_info = &devid->buf.pkt.rx;

	/* check for over run */
	REG_WRITE_B(devid,regs->ctrl_port,WR0_R1);
	stat = REG_READ_B(devid,regs->ctrl_port);
	if ( stat & RR1_RXOVR )
	{
		/* check to see if we have a character */
		stat = REG_READ_B(devid,regs->ctrl_port);
		if ( stat & RR0_RXCH )
		{
#ifndef DATA_PORT_ACCESSIBLE

			REG_WRITE_B(devid,regs->ctrl_port,WR0_R8);
			chr = REG_READ_B(devid,regs->ctrl_port);
#else
			chr = REG_READ_B(devid,regs->data_port);
#endif
		}
		/* clear over run by error reset command */
		REG_WRITE_B(devid,regs->ctrl_port,WR0_ERRES);
		resync_pkt(rx_info, &devid->buf.pkt.preamble);
		REG_WRITE_B(devid,regs->ctrl_port,WR0_HIRES);
		INTERRUPT_END(devid, SERIAL_PKT_RX_INT);
		return(FALSE);
	}

	/* no over run */
	stat = REG_READ_B(devid,regs->ctrl_port);
	if ( (stat & RR0_RXCH) )
	{
		/* The while loop is to ensure that the 3 byte */
		/* chip fifo is flushed out */

#ifndef DATA_PORT_ACCESSIBLE
		REG_WRITE_B(devid,regs->ctrl_port,WR0_R8);
		chr = REG_READ_B(devid,regs->ctrl_port);
#else
		chr = REG_READ_B(devid,regs->data_port);
#endif

#ifdef DEBUG
		pktrxp[rxcnt++] = chr;
#endif

		ret = logio_serial_packet_rx(devid, chr);
		stat = REG_READ_B(devid,regs->ctrl_port);
	}
	REG_WRITE_B(devid,regs->ctrl_port,WR0_HIRES);

	INTERRUPT_END(devid, SERIAL_PKT_RX_INT);
	return(ret);
}

/*************************************************************************
** zi8530_err_tst
**
** This function is called by the logio_default_int_handler in order to
** determine whether or not an error occurred while receiving a character.
**
** Inputs:
**     entry: pointer to entry in interrupt table
** Outputs:
** Return codes:
**   1: received a character with error
**
*/
#ifdef __STDC__
int 
zi8530_err_tst(logio_interrupt_entry_t *entry)
#else
int 
zi8530_err_tst(entry)
logio_interrupt_entry_t *entry;
#endif
{
	serial_dev_desc_t *devid = (serial_dev_desc_t *)entry->id->data;
	zi8530_reg_map_t *regs = (zi8530_reg_map_t *)devid->map;
	register serial_pkt_tst_info_t *rx_info;
	unsigned char stat;

#ifdef DEBUG
	static int b7cnt = 0;
	sprintf(b7,"err_tst: cnt=%d stat = 0x%x\n",b7cnt,stat);
	b7cnt++;
#endif

	REG_WRITE_B(devid,regs->ctrl_port,WR0_R1);
	stat = REG_READ_B(devid,regs->ctrl_port);
	if ( (stat & RR1_RXOVR) || (stat & RR1_PARERR) )
	{

		if ( devid->config.flavor == serial_packet_device )
		{
			rx_info = &devid->buf.pkt.rx;
			resync_pkt(rx_info, &devid->buf.pkt.preamble);
		}
		REG_WRITE_B(devid,regs->ctrl_port,WR0_ERRES);
		REG_WRITE_B(devid,regs->ctrl_port,WR0_HIRES);

		return(1);
	}
	else
		return(FALSE);
}

/*************************************************************************
** zi8530_tty_tx_tst
**
** This function is called by the logio_default_int_handler in order to
** determine whether or not a character was transmitted.
**
** Inputs:
**     entry: pointer to entry in interrupt table
** Outputs:
** Return codes:
**   1: serial transmitted a character
**
*/
#ifdef __STDC__
int 
zi8530_tty_tx_tst(logio_interrupt_entry_t *entry)
#else
int 
zi8530_tty_tx_tst(entry)
logio_interrupt_entry_t *entry;
#endif
{
	serial_dev_desc_t *devid = (serial_dev_desc_t *)entry->id->data;
	zi8530_reg_map_t *regs = (zi8530_reg_map_t *)devid->map;
	register logio_charbuf_t *outbuf = &devid->buf.tty.outbuf;
	unsigned char ch;

	INTERRUPT_START(devid, SERIAL_TTY_TX_INT);
	if ( outbuf->cbuf_cnt > 0 )
	{
		ch = logio_charbuf_read(outbuf);
		outbuf->cbuf_start = logio_charbuf_inc(outbuf,outbuf->cbuf_start);
		outbuf->cbuf_cnt--;

#ifndef DATA_PORT_ACCESSIBLE
		REG_WRITE_B(devid,regs->ctrl_port,WR0_R8);
		REG_WRITE_B(devid,regs->ctrl_port,ch);
#else
		REG_WRITE_B(devid,regs->data_port,ch);
#endif
	}
	else
		REG_WRITE_B(devid,regs->ctrl_port,WR0_TXRES);

	/* clear highest priority interrupt */
	REG_WRITE_B(devid,regs->ctrl_port,WR0_HIRES);
	INTERRUPT_END(devid, SERIAL_TTY_TX_INT);

	return(1);
}

/*************************************************************************
** zi8530_tty_rx_tst
**
** This function is called by the logio_default_int_handler in order to
** determine whether or not a character was received.
**
** Inputs:
**     entry: pointer to entry in interrupt table
** Outputs:
** Return codes:
**   1: serial received a character
**
*/
#ifdef __STDC__
int 
zi8530_tty_rx_tst(logio_interrupt_entry_t *entry)
#else
int 
zi8530_tty_rx_tst(entry)
logio_interrupt_entry_t *entry;
#endif
{
	serial_dev_desc_t *devid = (serial_dev_desc_t *)entry->id->data;
	zi8530_reg_map_t *regs = (zi8530_reg_map_t *)devid->map;
	logio_charbuf_t *inbuf;
	unsigned char ch, rx_stat;

#ifdef DEBUG
	sprintf(b6,"tty_rx_tst");
#endif

	INTERRUPT_START(devid, SERIAL_TTY_RX_INT);
	rx_stat = REG_READ_B(devid,regs->ctrl_port);
	if ( rx_stat & RR0_RXCH )
	{

#ifndef DATA_PORT_ACCESSIBLE
		REG_WRITE_B(devid,regs->ctrl_port,WR0_R8);
		ch = REG_READ_B(devid,regs->ctrl_port);
#else
		ch = REG_READ_B(devid,regs->data_port);
#endif
	}
	else
	{
		/* clear highest priority interrupt */
		REG_WRITE_B(devid,regs->ctrl_port,WR0_HIRES);
		INTERRUPT_END(devid, SERIAL_TTY_RX_INT);
		return(0);
	}

	inbuf = &devid->buf.tty.inbuf;
	/* store character to input buffer */
	if ( logio_charbuf_notfull(inbuf) )
	{
		logio_charbuf_write(inbuf, ch);
		inbuf->cbuf_cnt++;
		inbuf->cbuf_end = logio_charbuf_inc(inbuf, inbuf->cbuf_end);
	}

	/* clear highest priority interrupt */
	REG_WRITE_B(devid,regs->ctrl_port,WR0_HIRES);
	INTERRUPT_END(devid, SERIAL_TTY_RX_INT);

	return(1);
}

#ifdef __STDC__
int 
zi8530_tty_rx_tst_7(logio_interrupt_entry_t *entry)
#else
int 
zi8530_tty_rx_tst_7(entry)
logio_interrupt_entry_t *entry;
#endif
{
	serial_dev_desc_t *devid = (serial_dev_desc_t *)entry->id->data;
	zi8530_reg_map_t *regs = (zi8530_reg_map_t *)devid->map;
	logio_charbuf_t *inbuf;
	unsigned char ch, rx_stat;

#ifdef DEBUG
	sprintf(b6,"tty_rx_tst");
#endif

	INTERRUPT_START(devid, SERIAL_TTY_RX_INT);
	rx_stat = REG_READ_B(devid,regs->ctrl_port);
	if ( rx_stat & RR0_RXCH )
	{

#ifndef DATA_PORT_ACCESSIBLE
		REG_WRITE_B(devid,regs->ctrl_port,WR0_R8);
		ch = REG_READ_B(devid,regs->ctrl_port) & 0x7f;
#else
		ch = REG_READ_B(devid,regs->data_port) & 0x7f;
#endif
	}
	else
	{
		/* clear highest priority interrupt */
		REG_WRITE_B(devid,regs->ctrl_port,WR0_HIRES);
		INTERRUPT_END(devid, SERIAL_TTY_RX_INT);
		return(0);
	}

	inbuf = &devid->buf.tty.inbuf;
	/* store character to input buffer */
	if ( logio_charbuf_notfull(inbuf) )
	{
		logio_charbuf_write(inbuf, ch);
		inbuf->cbuf_cnt++;
		inbuf->cbuf_end = logio_charbuf_inc(inbuf, inbuf->cbuf_end);
	}

	/* clear highest priority interrupt */
	REG_WRITE_B(devid,regs->ctrl_port,WR0_HIRES);
	INTERRUPT_END(devid, SERIAL_TTY_RX_INT);

	return(1);
}

/*************************************************************************
** zi8530_port_disable 
**
** Disables port of the Zilog zi8530
**
** Inputs:
**    devid           The ID returned by the device initialization
**                    function zi8530_init.
** Outputs:
** Return codes:
**    VBSP_SUCCESS    The call succeeded
**
*/

char zi8530_port_disable_table[] =
{
	/*
	register value     comment
	-------- ------    -------------------*/
	0x0B, 0x50,	   /* clock mode control - tx & rx clock = B.R. generator output */
	0x0C, 0x0E,	   /* BAUD low  byte 9600 */
	0x0D, 0x00,	   /* BAUD high byte 9600 */
	0x0E, 0x62,	   /* misc control bits, BAUD source clk = PCLK, Disable Baud rate gen & PLL */
	0x0E, 0x03,	   /* misc control bits, BAUD source clk = PCLK, ENABLE Baud rate gen */ 
	0x04, 0x44,	   /* Tx/Rx Params/modes X16 CLOCK MODE, 2 STOP BITS, NO PARITY */
	0x03, 0xC1,	   /* Rx Params/controls RX 8 BITS/CHAR., RX ENABLE */
	0x0F, 0x00,	   /* Ext/Status Int NO INTERRUPT ON EXTERNAL STATUS */
	0x05, 0xEA,	   /* Tx params/controls DTR, TX 8 BITS/CHAR., TX enabled, RTS */
	0x01, 0x00,	   /* Tx/Rx interrupt & mode NO DMA, RX and TX disabled */
	0xFF, 0xFF	   /* END OF TABLE */
};

#ifdef __STDC__
vbsp_return_t
zi8530_port_disable(serial_dev_desc_t *devid)
#else
vbsp_return_t
zi8530_port_disable(devid)
serial_dev_desc_t *devid;
#endif
{
	unsigned char *itable = (unsigned char *)zi8530_port_disable_table;
	unsigned char *reg = (unsigned char *)((zi8530_reg_map_t *)devid->map)->ctrl_port;

	while ( *itable != 0xFF )
	{
		REG_WRITE_B(devid,reg,*itable);
		itable++;
	}
	return(VBSP_SUCCESS);
}

/*************************************************************************
** zi8530_com_init 
**
** Initializes the common registers of port A&B of the Zilog zi8530
**
** Inputs:
**    reg           The port register pointer for initialization
** Outputs:
** Return codes:
**    VBSP_SUCCESS    The call succeeded
**
*/

char zi8530_com_init_table[] =
{
	/*
	register value     comment
	-------- ------    -------------------*/
	0x09, 0xC9,	   /* MASTER INTERRUPT CONTROL, force h/w reset, master interrupt */
	/* ENABLE, STATUS AFFECTS VECTOR, STATUS LOW  */
	0x02, 0x00,	   /* INTERRUPT VECTOR: The vector "00" is filled by zi8530_init */
	0xFF, 0xFF	   /* END OF TABLE */
};

#ifdef __STDC__
vbsp_return_t
zi8530_com_init(serial_dev_desc_t *devid)
#else
vbsp_return_t
zi8530_com_init(devid)
serial_dev_desc_t *devid;
#endif
{
	unsigned char *itable = (unsigned char *) zi8530_com_init_table;
	unsigned char *reg = (unsigned char *)((zi8530_reg_map_t *)devid->map)->ctrl_port;
	volatile int i;

	while ( *itable != 0xFF )
	{
		REG_WRITE_B(devid,reg,*itable);
		itable++;
		for ( i = 0; i < SETTLE_DELAY; i++ ) ;
	}
	return(VBSP_SUCCESS);
}

char zi8530_init_table[] =
{
	/*
	register value     comment
	-------- ------    -------------------*/
	0x04, 0x44,		   /* Tx/Rx Params/modes X16 CLOCK MODE, 2 STOP BITS,NO PARITY */
	0x02, 0x00,        /* INTERRUPT VECTOR, SCC VECTOR NUMBER supplied by device config */
	0x03, 0xC0,		   /* Rx Params/controls RX 8 BITS/CHAR., RX DISABLE */
	0x05, 0xE2,		   /* Tx params/controls DTR, TX 8 BITS/CHAR., TX DISABLE, RTS */
	0x0A, 0x00,		   /* NRZ */
	0x0B, 0x50,		   /* clock mode control - tx & rx clock = B.R. generator o/p */
	0x0C, 0x0E,		   /* BAUD low  byte calculated by output routine 9600 value place holder*/
	0x0D, 0x00,		   /* BAUD high byte calculated by output routine  */
	0x0E, 0x62,		   /* misc control bits, BAUD source clk = PCLK,Dis. BR gen & PLL */
	0x0E, 0x03,		   /* misc control bits, BAUD source clk = PCLK, ENABLE BR gen */
	0x03, 0xC1,		   /* Rx Params/controls RX 8 BITS/CHAR., RX ENABLE */
	0x05, 0xEA,		   /* Tx params/controls DTR, TX 8 BITS/CHAR., TX enabled, RTS */
	0x01, 0x00, 	   /* tx/rx interrupts - place holder - uses cached value set in int_info_set */
	0x0F, 0x00,		   /* Ext/Status Int NO INTERRUPT ON EXTERNAL STATUS */
    0x09, WR9_MASTER|WR9_VIS,         // master interrupt enable, vector includes status
    0x00, WR0_TCRCRES,                // reset tx CRC errors
    0x00, WR0_EXTRES,                 // reset external interrupts
    0x00, WR0_EXTRES,                 // reset external interrupts twice per spec 
	0xFF, 0xFF		   /* END OF TABLE */
};

/***********************************************************************
** zi8530_port_init
**
** This routine initializes the zi8530 chip.
** The initialization values are taken from a default rom table and
** the user modifiable values given in the config table of device id
** are used to override the default values of the rom table.
**
** Input:
**    devid        device id
** Output:
** Return code:
**    VBSP_SUCCESS
**
*/
#ifdef __STDC__
void
zi8530_port_init(serial_dev_desc_t *devid)
#else
void
zi8530_port_init(devid)
serial_dev_desc_t *devid;
#endif
{
	unsigned char *reg = (unsigned char *)((zi8530_reg_map_t *)devid->map)->ctrl_port;
	zi8530_specific_t *specific = (zi8530_specific_t *)devid->config.specific;
	unsigned short baud_cnt,val;
	unsigned char val1;
	unsigned char vec;
	volatile int i;
    unsigned char zero = 0;

	/* determines what clock source is: DPLL or Baud rate */
	unsigned char clock_src;            

	unsigned char *tabp = (unsigned char *)zi8530_init_table;

   #ifdef DEBUG1
	char *b0p = dout2;
   #endif

	/*
	** The formula for the baud rate time constant is
	** input_clock_speed is in hz.
	**  baud_cnt = ((input_clock_speed) / (2 * baud_rate * clock_mode ) ) - 2
	**
	**  We always assume that the clock mode is equal to 16.  NOTE: this
	**  should be a configuration parameter later on.
	*/


	baud_cnt = (unsigned short)
			   ( (devid->config.input_clock_speed) /
				 ( 2 * devid->config.baud_rate * 16)) - 2;


	DEBUG_OUT1_INC(16,b0p,"baud=0x%x",baud_cnt);

	/* reset the state machine */
	REG_WRITE_B(devid,reg,zero);
	REG_WRITE_B(devid,reg,zero);

	/* reset any errors or external interrupts */
	REG_WRITE_B(devid,reg,WR0_ERRES);
	REG_WRITE_B(devid,reg,WR0_EXTRES);

	/* reset specified channel */
	REG_WRITE_B(devid,reg,WR0_R9);

	if ( specific->channel == SCC_CHANNEL_A )
	{
		REG_WRITE_B(devid,reg,WR9_ARES);
	}
	else
	{
		REG_WRITE_B(devid,reg,WR9_BRES);
	}

	for ( i = 0; i < RESET_DELAY; i++ )	;
	/* initialize the vector here */
	vec = ((zi8530_specific_t *)devid->config.specific)->vector;
	REG_WRITE_B(devid,reg,WR0_R2);
	REG_WRITE_B(devid,reg,vec);

	/* get the clock source */
	clock_src = ((zi8530_specific_t *)devid->config.specific)->clock_src;


	while ( *tabp != 0xFF )
	{
		REG_WRITE_B(devid,reg,*tabp);
		for ( i = 0; i < SETTLE_DELAY; i++ ) ;
		switch ( *tabp )
		{
			case WR0_R1:
				tabp++;
				val1 = specific->zi8530_wr1;		   // use the cached value for R1 set in int_info_set
				REG_WRITE_B(devid,reg,val1);
				break;

			case WR0_R2:
				tabp++;
				val1 = specific->vector;			   /* in the device specific initialization */
				REG_WRITE_B(devid,reg,val1);
				DEBUG_OUT1_INC(16,b0p,"WR0_R2=0x%x ",val1);
				break;

			case WR0_R3:				 /* receiver bits per char */
				tabp++;
				val1 = *tabp;
				switch ( devid->config.bits )
				{
					case BITS_5:
						val1 &= ~WR3_RX8BIT;   /* remove earlier value */
						val1 |= WR3_RX5BIT;
						break;
					case BITS_6:
						val1 &= ~WR3_RX8BIT;   /* remove earlier value */
						val1 |= WR3_RX6BIT;
						break;
					case BITS_7:
						val1 &= ~WR3_RX8BIT;   /* remove earlier value */
						val1 |= WR3_RX7BIT;
						break;
					case BITS_8:
						val1 |= WR3_RX8BIT;
						break;
					default:
						break;
				}
				REG_WRITE_B(devid,reg,val1);
				DEBUG_OUT1_INC(16,b0p,"WR0_R3=0x%x ",val1);
				break;

			case WR0_R4:	   /* parity and stop bits */
				tabp++;
				val1 = *tabp;
				switch ( devid->config.parity )
				{
					case PARITY_ODD:
						val1 &= ~(WR4_PARITY | WR4_EVENP);
						val1 |= (WR4_PARITY | WR4_ODDP);
						break;
					case PARITY_EVEN:
						val1 |= (WR4_PARITY | WR4_EVENP);
						break;
					case PARITY_NONE:
						val1 &= ~(WR4_PARITY | WR4_EVENP);
						break;
					default:
						break;
				}

				switch ( devid->config.stop )
				{
					case STOP_BITS_1:
						val1 &= ~WR4_2_STOP;
						val1 |= WR4_1_STOP;
						break;
					case STOP_BITS_1_AND_HALF:
						val1 &= ~WR4_2_STOP;
						val1 |= WR4_15_STOP;
						break;
					case STOP_BITS_2:
						val1 |= WR4_2_STOP;
						break;
					default:
						break;
				}

				DEBUG_OUT1_INC(16,b0p,"WR0_R4=0x%x ",val1);
				REG_WRITE_B(devid,reg,val1);
				break;

			case WR0_R5:	   /* xmitter bits per char */
				tabp++;
				val1 = *tabp;
				switch ( devid->config.bits )
				{
					case BITS_5:
						val1 &= ~WR5_TX8BIT;	  /* remove earlier value */
						val1 |= WR5_TX5BIT;
						break;
					case BITS_6:
						val1 &= ~WR5_TX8BIT;	  /* remove earlier value */
						val1 |= WR5_TX6BIT;
						break;
					case BITS_7:
						val1 &= ~WR5_TX8BIT;
						val1 |= WR5_TX7BIT;
						break;
					case BITS_8:
						val1 |= WR5_TX8BIT;
						break;
					default:
						break;
				}
				REG_WRITE_B(devid,reg,val1);
				DEBUG_OUT1_INC(16,b0p,"WR0_R5=0x%x ",val1);
				break;

			case WR0_R12:
				val = baud_cnt & 0xff;
				REG_WRITE_B(devid,reg,val);
				tabp++;
				DEBUG_OUT1_INC(16,b0p,"WR0_R12=0x%x ",val);
				break;

			case WR0_R13:
				val = (baud_cnt >> 8) & 0xff;
				REG_WRITE_B(devid,reg,val);
				tabp++;
				DEBUG_OUT1_INC(16,b0p,"WR0_R13=0x%x ",val);
				break;

			case WR0_R14:
				tabp++;
				val1 = *tabp;       
				if ( clock_src == DPLL_ENABLE )
				{
					val1 &= ~(WR14_PCLK | WR14_DPDIS);		/* remove earlier value*/
					val1 |= WR14_DSRTXC;			   /* enable RTxC as source */
				}
				REG_WRITE_B(devid,reg,val1);
#ifdef DEBUG
				DEBUG_OUT1_INC(16,b0p,"WR0_R14=0x%x ",val1);
#endif
				break;


			default:
				tabp++;
				REG_WRITE_B(devid,reg,*tabp);
				break;
		}
		tabp++;
		for ( i = 0; i < SETTLE_DELAY; i++ ) ;
	}  /* end while (*tabp != 0xFF) */
	logio_charbuf_init(&devid->buf.tty.inbuf);
	logio_charbuf_init(&devid->buf.tty.outbuf);
}

/***********************************************************************
** zi8530_set_params
**
** This routine sets the various serial parameters such as baud rate,
** stop bits, parity, number of bits/character
**
** Inputs:
**     devid       serial device descriptor
**     arg         control argument flags and value
** Outputs
**     arg         old value
** Return code
**     VBSP_SUCCESS
**     VBSP_NOTSUPPORTED
**
*/
#ifdef __STDC__
vbsp_return_t
zi8530_set_params(serial_dev_desc_t *devid, serial_params_t *arg)
#else
vbsp_return_t
zi8530_set_params(devid, arg)
serial_dev_desc_t *devid;
serial_params_t *arg;
#endif
{
	DEBUG_OUT4(dout1,"baud %d, parity %d, stop %d, bits %d",
			   arg->baud,
			   arg->parity,
			   arg->stop_bits,
			   arg->bits
			  );
	devid->config.baud_rate = arg->baud;
	devid->config.parity    = arg->parity;
	devid->config.stop      = arg->stop_bits;
	devid->config.bits      = arg->bits;
	zi8530_port_init(devid );
	return(VBSP_SUCCESS);
}


