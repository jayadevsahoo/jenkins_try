#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: WaveformsSubScreen - The screen selected by pressing the
// Waveforms button on the Upper Screen.  Consists of two screens,
// the main screen displaying selected patient data waveforms with another
// screen to select which waveforms to display.
//---------------------------------------------------------------------
//@ Interface-Description
// The Waveforms subscreen is made up of two screens, the actual waveform
// display screen and the waveform plot setup screen.  By default
// the waveform display screen is displayed first when this subscreen is
// activated, displaying two waveforms, Pressure v. Time and Flow v. Time.
// The waveform display screen can display one or two time plots or a
// single loop (Pressure v. Volume) plot.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// buttonDownHappened() is called when certain buttons in the subscreen are
// selected.  Changes to settings are indicated via nonBatchSettingUpdate().
// Changes to patient breath data are detected in patientDataChangeHappened().
//
// The update() method is called periodically to synchronize the waveform
// display with the latest waveform data from the Patient-Data subsystem.
// The isOnWaveDisplay() is called from WaveformsTabButton and allows
// it to check if this subscreen is currently displaying either waveforms
// or the plot setup screen.
//
// The updateLog() method is called whenever a user had changed the x or
// y coordinates for either the plots.  When nonBatchSettingUpdate()
// causes the plot to redraw, the priority of the upper plot will be
// raised to force an immediate update.  Plotting of the second plot will
// be at a lower priority.  So if the user continued spinning the knob
// the plot1 will be redrawed for each knob click.  But the plot2 will
// be redrawed only when its priority is reached.
//
// Two methods are provided for the special needs of the Pause
// functions.  pauseRequested() should be called when Expiratory or
// Inspiratory Pause is requested (this subscreen must be displayed first)
// and pauseActive() is called once the Expiratory or Inspiratory Pause has
// been accepted by Breath-Delivery and is active.
//---------------------------------------------------------------------
//@ Rationale
// This class gathers all the functionality of the Waveforms subscreen
// into one place.
//---------------------------------------------------------------------
//@ Implementation-Description
// The isOnWaveDisplay_ member indicates which screen we are currently
// displaying, the waveform display (TRUE) or the waveform plot setup
// screen (FALSE).  The method layoutScreen_() looks at this member
// and forms the graphical layout of the Waveforms subscreen appropriately.
//
// The subscreen is laid out when it is activated and also when the
// Plot Setup button is pressed on the waveform display or the Continue
// button is pressed on the plot setup screen.  These two buttons switch
// between the two possible waveform screens.
//
// The most complicated function of this subscreen is to keep track of
// waveform data as it is being received from the Patient Data subsystem
// Various WaveformIntervalIter objects are used for this purpose.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformsSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 050   By: rhj    Date: 15-Mar-2011   SCR Number: 6755 
//  Project:  PROX
//  Description:
//      Display 840's waveform labels during waiting for patient 
//      connect even if prox is enabled.
//  
//  Revision: 049   By: rhj    Date: 22-Feb-2011   SCR Number: 6746 & 6730
//  Project:  PROX
//  Description:
//     Redesign isProxInStartupMode_ algorithim to fix the 
//     Prox infinite startup and no pressure data bug. 
//  
//  Revision: 048   By: rhj    Date: 13-Jan-2010   SCR Number: 6631
//  Project:  PROX
//  Description:
//     Added isProxReady() method.
//  
//  Revision: 047   By: rhj   Date:  18-Oct-2010    SCR Number: 6691
//  Project:  PROX
//  Description:
//      Fixed the prox status message to be displayed up to the 
//      value of PROX_STATUS_TIMEOUT timer.
//
//  Revision: 046   By: rhj   Date:  10-Sept-2010    SCR Number: 6631
//  Project:  PROX
//  Description:
//      Prevent displaying PROX waveform data during a PROX startup.
//
//  Revision: 045   By: rhj   Date:  16-July-2010    SCR Number: 6593
//  Project:  PROX
//  Description:
//      Moved the proxAutoZeroPurgeStatusMsg_ to WaveformPlot class
//
//  Revision: 044   By: rhj   Date:  20-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added Prox pressure to the real time waveform serial data.
//
//  Revision: 043   By: rhj   Date:  18-Mar-2010    SCR Number: 6436
//  Project:  PROX
//     Added a timer for the prox status
// 
//  Revision: 042   By: rhj   Date:  26-Jan-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//     Added Prox symbols and implemented the "pen-up" feature.
// 
//  Revision: 041  By: mnr    Date: 19-Jan-2009    SCR Number: 5987
//  Project:  NEO
//  Description:
//     Divisions increased to 2 for Flow scale -10 to 10 to avoid 
//     assertion mentioned in SCR 5987 Verification Note.
//
//  Revision: 040  By: mnr    Date: 18-Jan-2009    SCR Number: 5987
//  Project:  NEO
//  Description:
//     Comments updated acc. to code review feedback.
//   
//  Revision: 039  By: mnr    Date: 13-Jan-2009    SCR Number: 5987
//  Project:  NEO
//  Description:
//     Updates to incorporate new plot scales, remove PEEP and PEEP_LOW from
//     pressure scale adjustments and improve the PV loop display.
//
//  Revision: 038   By: mnr    Date: 28-Dec-2009     SCR Number: 5987
//  Project:  NEO
//  Description:
//    Added new Flow Plot scale -5 to 5.
//
//  Revision: 037   By: mnr    Date: 23-Nov-2009     SCR Number: 5987
//  Project:  NEO
//  Description:
//    Added new Volume Plot scale -5 to 10.
//
//  Revision: 036   By: erm    Date: 15-Apr-2009    SCR Number: 6498
//  Project:  840S
//  Description:
//    Removed PatientData::ActivateWaveform() from the constructor
//
//  Revision: 035   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 034  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 033   By: erm  Date: 30-Oct-2008    SCR Number: 6441
//  Project:  840S
//  Description:
//   Added support for to transmit waveform data to Serial Buffers
// 
//  Revision: 032   By: gdc  Date: 21-Oct-2008    SCR Number: 6435 
//  Project:  840S
//  Description:
//  Modified to support neo nCPAP. Continuous time plot waveforms.
//  
//  Revision: 031   By: rhj  Date: 16-Aug-2007    SCR Number: 6406 
//  Project:  Trend
//  Description:
//  Fixed a bug where the state of the PLOT SET, FREEZE, and 
//  PRINT/CANCEL buttons can enter a mixed-up state when 
//  dismissing and re-displaying this subscreen.
//
//  Revision: 030   By: rhj  Date: 24-Jul-2007    SCR Number: 6394 
//  Project:  Trend
//  Description:
//  Fixed a bug where an extra arrow controls can appear on the 
//  Flow-Volume Loop screen. 
//
//  Revision: 029   By: rhj  Date: 23-Jul-2007    SCR Number: 6386
//  Project:  Trend
//  Description:
//  Fixed a bug where the printing on this subscreen has stopped
//  working correctly. 
//
//  Revision: 028   By: gdc  Date: 26-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//  Removed SUN prototype code.
//
//  Revision: 027   By: gdc  Date: 10-May-2007    SCR Number: 6375
//  Project:  Trend
//  Description:
//  Added support for 30 second waveform for NIF.
//
//  Revision: 026   By: rhj  Date: 01-Dec-2006    SCR Number: 6291/6295
//  Project:  RESPM
//  Description:
//  Added conditional algorithms to fix 6295 when transitioning in or out of PAV.  
//  Called setDisableNovRamUpdate() function to prevent waveform settings getting 
//  stored into NOVRAM for SCR 6291.
//
//  Revision: 025   By: rhj  Date: 03-Feb-2005    SCR Number: 6155
//  Project:  NIV
//  Description:
//	Fixed bug in which waveform data is plotted on non-Waveforms sub-screens.
//
//  Revision: 024   By: jja  Date: 26-Apr-2002    DR Number: 6000
//  Project:  VCP
//  Description:
//	Corrected possible missing VC+ or VS Startup messages when switching from VS active to
// 	VC+ Startup or VC+ active to VS Startup.
//
//  Revision: 023   By: erm  Date: 23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//      added shadow trace enable/disable button
// 
//  Revision: 022   By: heatherw Date:  09-Apr-2002    DR Number: 5809, 5811, 5881
//  Project:  VCP
//  Description:
//      Restored pause maneuver functionality back to waveform displays.
//
//  Revision: 021   By: heatherw Date:  28-Mar-2002    DR Number: 5922
//  Project:  VCP
//  Description:
//      Added flag to differentiate between EXP_PAUSE that is updated
//      due to changed patient data and when it is updated due to 
//      End of Expiratory Pause Maneuver, the latter could mean data
//      has become unstable.
//
//  Revision: 020   By: yakovb   Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Handle VC+/VS Startup messages.
//
//  Revision: 019  By: quf     Date:  04-Oct-2001    DCS Number: 5963
//  Project:  GUIComms
//  Description:
//	Fixed PRINT/CANCEL button display issues as follows:
//	- batchSettingUpdate() was modified to fix PRINT/CANCEL button blanking
//	  issues during com2 and com3 setting changes and during a print-in-progress.
//	- printDone() was modified to ensure correct display/non-display of
//	  the PRINT button after a print job is completed.
//	- layoutScreen_() was modified to ensure correct display/non-display
//	  of the PRINT button when the waveforms upper subscreen is selected
//	  while the waveform is in a frozen state.
//	- freeze_() was modified to ensure correct display/non-display of
//	  the PRINT button when the waveform frozen state is entered.
//	
//  Revision: 018  By: hlg     Date:  01-Oct-2001    DCS Number: 5966
//  Project:  GUIComms
//  Description:
//	Added mapping to SRS print requirements.
//
//  Revision: 017  By: quf     Date:  13-Sep-2001    DCS Number: 5493
//  Project:  GUIComms
//  Description:
//	Print implementation changes:
//	- Cleaned up the print cancel and print done mechanisms.
//
//  Revision: 016   By: sah    Date:  11-Jul-2000    DCS Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added display of PAV-based compliance, resistance and intrinsic
//         PEEP, during PAV breaths
//
//  Revision: 015   By: sah    Date:  11-Jul-2000    DCS Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added support for new shadow traces
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//      *  changed to a (auxillary) state machine, with arrays of drawables
//         for each state; this significantly reduces the work of adding new
//         artifacts and states
//
//  Revision: 014  By: hct     Date:  29-MAR-2000    DCS Number: 5493
//  Project:  GUIComms
//  Description:
//	GUIComms initial revision. 
//
//  Revision: 013  By: hhd     Date:  09-Aug-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Modified to support new Flow and Volume scaling values.
//
//  Revision: 012   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//      Optimized graphics library. Implemented Direct Draw mode for drawing
//      outside of BaseContainer refresh cycle.
//
//  Revision: 011  By: yyy     Date:  27-Sep-1999    DR Number: 5535
//  Project:  ATC
//  Description:
//      Mapped SRS 1358 to code.
//
//  Revision: 010  By: yyy     Date:  08-Jul-1999    DR Number: 5459
//  Project:  ATC
//  Description:
//      When either inspiratory or expiratory pause is requested, isFrozen_
//		flag should not be checked during update() to avoid of calling
//		the freeze_() and the adjust panel target should be reset.
//
//  Revision: 009  By: yyy    Date: 16-Mar-1999  DCS Number: 5345
//  Project:  ATC
//  Description:
//	  Added warning message when exp. pause pressure is not stable.
//	  Added warning message when pause is canceled by alarm or patient
//	  trigger.
//
//  Revision: 008  By: yyy    Date: 3-Nov-1998  DCS Number: 5256
//  Project: BiLevel (R8027)
//  Description:
//	  Removed unused compliance and resistance error titles
//
//  Revision: 007  By: yyy      Date: 15-Oct-1998  DR Number: 5209
//    Project:  BiLevel (R8027)
//    Description:
//     Added checking for isFrozen_ in displayPlateauPressure_() when displaying
//     plateau is requested.
//
//  Revision: 006  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/WaveformsSubScreen.ccv   1.41.1.0   07/30/98 10:22:54   gdc
//
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 004  By:  gdc    Date:  13-May-1998   DR Number: XXXX
//    Project:  Sigma (R8027)
//    Description:
//      Ignore GUI events so waveform continues display during 
//		occlusion and patient disconnect.
//
//  Revision: 003  By:  yyy    Date:  22-JUNE-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      Renamed waveformsSubScreen related help message for translation.
//
//  Revision: 002  By: yyy      Date: 15-May-1997  DR Number: 2110
//    Project:  Sigma (R8027)
//    Description:
//      Added SRS 01165 mapping.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "WaveformsSubScreen.hh"

//@ Usage-Classes
#include "AcceptedContext.hh"
#include "AcceptedContextHandle.hh"
#include "AdjustPanel.hh"
#include "BaseContainer.hh"
#include "BdEventRegistrar.hh"
#include "BdGuiEvent.hh"
#include "Bitmap.hh"
#include "BreathDatumHandle.hh"
#include "ComPortConfigValue.hh"
#include "ContextMgr.hh"
#include "EventData.hh"
#include "FlowPlotScaleValue.hh"
#include "GuiEventRegistrar.hh"
#include "GuiFoundation.hh"
#include "Image.hh"
#include "LowerScreen.hh"
#include "MandTypeValue.hh"
#include "MathUtilities.hh"
#include "MiscStrs.hh"
#include "PatientDataMgr.hh"
#include "PatientDataRegistrar.hh"
#include "PavState.hh"
#include "Plot1TypeValue.hh"
#include "Plot2TypeValue.hh"
#include "PressPlotScaleValue.hh"
#include "SafetyPcvSettingValues.hh"
#include "SerialInterface.hh"
#include "Setting.hh"
#include "SettingContextHandle.hh"
#include "SettingSubject.hh"
#include "SettingsMgr.hh"
#include "ShadowTraceEnableValue.hh"
#include "SupportTypeValue.hh"
#include "Task.hh"
#include "TimePlotScaleSetting.hh"
#include "TimePlotScaleValue.hh"
#include "UpperScreen.hh"
#include "UpperScreenSelectArea.hh"
#include "UpperSubScreenArea.hh"
#include "VolumePlotScaleValue.hh"
#include "VtpcvState.hh"
#include "ProxEnabledValue.hh"
//@ End-Usage


//@ Code...

static const Int32 MAX_TIME_INTERVAL_ = 48;

static const Int32 TIME_PLOT_X_ = 0;
static const Int32 PV_PLOT_X_ = 70;
static const Int32 PLOT1_Y_ = 32;
static const Int32 PLOT2_Y_ = 157;
static const Int32 TIME_PLOT_WIDTH_ = 630;
static const Int32 BIG_TIME_PLOT_HEIGHT_ = 240;
static const Int32 SMALL_TIME_PLOT_HEIGHT_ = 130;
static const Int32 PV_PLOT_WIDTH_ = 630;
static const Int32 PV_PLOT_HEIGHT_ = 240;

static const Int32 TIME_PLOT_GRID_X_ = 75;
static const Int32 TIME_PLOT_GRID_Y_ = 5;
static const Int32 TIME_PLOT_GRID_WIDTH_ = 495;
static const Int32 BIG_TIME_PLOT_GRID_HEIGHT_ = 210;
static const Int32 SMALL_TIME_PLOT_GRID_HEIGHT_ = 105;

static const Int32 PV_PLOT_GRID_X_ = 110;
static const Int32 PV_PLOT_GRID_Y_ = 5;
static const Int32 PV_PLOT_GRID_WIDTH_ = 315;
static const Int32 PV_PLOT_GRID_HEIGHT_ = 210;

static const Int32 PLOT_SETUP_X_ = 1;
static const Int32 PLOT_SETUP_Y_ = 1;
static const Int32 PLOT_SETUP_WIDTH_ = 60;
static const Int32 PLOT_SETUP_HEIGHT_ = 28;

static const Int32 WAVE_DISPLAY_BUTTON_BORDER_ = 2;

static const Int32 FREEZE_X_ = PLOT_SETUP_X_ + PLOT_SETUP_WIDTH_;
static const Int32 FREEZE_Y_ = PLOT_SETUP_Y_;
static const Int32 FREEZE_WIDTH_ = PLOT_SETUP_WIDTH_;
static const Int32 FREEZE_HEIGHT_ = PLOT_SETUP_HEIGHT_;

static const Int32 PRINT_X_ = PLOT_SETUP_X_ + PLOT_SETUP_WIDTH_ + FREEZE_WIDTH_;
static const Int32 PRINT_Y_ = PLOT_SETUP_Y_;
static const Int32 PRINT_WIDTH_ = PLOT_SETUP_WIDTH_;
static const Int32 PRINT_HEIGHT_ = PLOT_SETUP_HEIGHT_;

static const Int32 X_SCALE_BUTTON_WIDTH_ = 44;
static const Int32 X_SCALE_BUTTON_HEIGHT_ = 24;
static const Int32 Y_SCALE_BUTTON_WIDTH_ = 24;
static const Int32 Y_SCALE_BUTTON_HEIGHT_ = 43;
static const Int32 X_AXIS_BITMAP_XPOS = 2;
static const Int32 X_AXIS_BITMAP_YPOS = 2;
static const Int32 Y_AXIS_BITMAP_XPOS = 3;
static const Int32 Y_AXIS_BITMAP_YPOS = 1;

static const Int32 TIME_X_SCALE_BUTTON_X_ = 584;
static const Int32 TIME_X_SCALE_BUTTON_1PLOT_Y_ = 237;
static const Int32 TIME_X_SCALE_BUTTON_2PLOTS_Y_ = 140;
static const Int32 TIME_Y_SCALE_BUTTON_X_ = 595;
static const Int32 TIME_Y_SCALE_BUTTON_SMALL_PLOT1_Y_ = PLOT1_Y_
					+ TIME_PLOT_GRID_Y_ 
					+ (SMALL_TIME_PLOT_GRID_HEIGHT_
						- Y_SCALE_BUTTON_HEIGHT_) / 2;
static const Int32 TIME_Y_SCALE_BUTTON_BIG_PLOT1_Y_ = PLOT1_Y_
					+ TIME_PLOT_GRID_Y_ 
					+ (BIG_TIME_PLOT_GRID_HEIGHT_ 
						- Y_SCALE_BUTTON_HEIGHT_) / 2;
static const Int32 TIME_Y_SCALE_BUTTON_PLOT2_Y_ = PLOT2_Y_ + TIME_PLOT_GRID_Y_
					+ (SMALL_TIME_PLOT_GRID_HEIGHT_ 
						- Y_SCALE_BUTTON_HEIGHT_) / 2;

static const Int32 PV_X_SCALE_BUTTON_X_ = 554;
static const Int32 PV_X_SCALE_BUTTON_Y_ = 238;
static const Int32 PV_Y_SCALE_BUTTON_X_ = 565;
static const Int32 PV_Y_SCALE_BUTTON_Y_ = PLOT1_Y_ + PV_PLOT_GRID_Y_
					+ (PV_PLOT_GRID_HEIGHT_ - Y_SCALE_BUTTON_HEIGHT_) / 2;

static const Int32 TIME_X_UNITS_X_ = 575;
static const Int32 TIME_X_UNITS_BIG_PLOT_Y_ = 221;
static const Int32 TIME_X_UNITS_SMALL_PLOT_Y_ = 116;
static const Int32 TIME_Y_UNITS_X_ = 0;
static const Int32 TIME_Y_UNITS_Y_ = 0;

static const Int32 PV_X_UNITS_X_ = 429;
static const Int32 PV_X_UNITS_Y_ = 203;
static const Int32 PV_Y_UNITS_X_ = 37;
static const Int32 PV_Y_UNITS_Y_ = 0;

static const Int32 BASELINE_BUTTON_WIDTH_ = 88;
static const Int32 BASELINE_BUTTON_HEIGHT_ = 45;
static const Int32 BASELINE_BUTTON_X_ = 36;
static const Int32 BASELINE_BUTTON_Y_ = PLOT1_Y_ + PV_PLOT_GRID_Y_
					+ (PV_PLOT_GRID_HEIGHT_ - BASELINE_BUTTON_HEIGHT_) / 2;
static const Int32 BASELINE_VALUE_CENTER_X_ = 29;
static const Int32 BASELINE_VALUE_CENTER_Y_ = 25;

static const Int32 PLOT_SETTING_BUTTON_WIDTH_ = 156;
static const Int32 PLOT_SETTING_BUTTON_HEIGHT_ = 46;
static const Int32 PLOT_SETTING_BUTTON_BORDER_ = 3;
static const Int32 SHADOW_SETTING_BUTTON_X_ = 
		(::SUB_SCREEN_AREA_WIDTH - PLOT_SETTING_BUTTON_WIDTH_) / 2;
static const Int32 SHADOW_SETTING_BUTTON_Y_ = 45;
static const Int32 PLOT_SETTING_BUTTON1_X_ = 
		(::SUB_SCREEN_AREA_WIDTH / 2) - PLOT_SETTING_BUTTON_WIDTH_ - 10;
static const Int32 PLOT_SETTING_BUTTON2_X_ = 
		(::SUB_SCREEN_AREA_WIDTH / 2) + 10;
static const Int32 PLOT_SETTING_BUTTON_Y_ =
		(SHADOW_SETTING_BUTTON_Y_ + PLOT_SETTING_BUTTON_HEIGHT_ + 10);

static const Int32 CONTINUE_BUTTON_X_ = 547;
static const Int32 CONTINUE_BUTTON_Y_ = 240;
static const Int32 CONTINUE_BUTTON_WIDTH_ = 84;
static const Int32 CONTINUE_BUTTON_HEIGHT_ = 34;
static const Int32 CONTINUE_BUTTON_BORDER_ = 3;

static const Int32 VALUE_WIDTH_  = 60 ;
static const Int32 VALUE_HEIGHT_ = 20 ;

static const Int32 INTRINSIC_PEEP_SYMBOL_X_ = 300;
static const Int32 INTRINSIC_PEEP_VALUE_X_ = 350;
static const Int32 INTRINSIC_PEEP_UNIT_X_ = 417;

static const Int32 TOTAL_PEEP_SYMBOL_X_ = 465;
static const Int32 TOTAL_PEEP_VALUE_X_ = 537;
static const Int32 TOTAL_PEEP_UNIT_X_ = 600;

static const Int32 NIF_PRESSURE_SYMBOL_X_ = 465;
static const Int32 NIF_PRESSURE_VALUE_X_ = 537;
static const Int32 NIF_PRESSURE_UNIT_X_ = 600;

static const Int32 P100_PRESSURE_SYMBOL_X_ = 465;
static const Int32 P100_PRESSURE_VALUE_X_ = 537;
static const Int32 P100_PRESSURE_UNIT_X_ = 600;

static const Int32 VITAL_CAPACITY_SYMBOL_X_ = 465;
static const Int32 VITAL_CAPACITY_VALUE_X_ = 537;
static const Int32 VITAL_CAPACITY_UNIT_X_ = 600;

static const Int32 COLUMN1_SYMBOL_X_ = 187 ;
static const Int32 COLUMN1_VALUE_X_ = 242 ;
static const Int32 COLUMN1_UNIT_X_ = 302 ;

static const Int32 COLUMN2_SYMBOL_X_ = 347 ;
static const Int32 COLUMN2_VALUE_X_ = 400 ;
static const Int32 COLUMN2_UNIT_X_ = 450 ;

static const Int32 COLUMN3_SYMBOL_X_ = 520 ;
static const Int32 COLUMN3_VALUE_X_ = 555 ;	// was 558;  tweaking the position SCR ID6053
static const Int32 COLUMN3_UNIT_X_ = 610 ;

static const Int32 PAUSE_CANCEL_BY_ALARM_TEXT_X_ = 205 ;

static Uint  Plot1InfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
			   (sizeof(StringId) * (Plot1TypeValue::TOTAL_PLOT1_TYPES - 1)))];
static Uint  Plot2InfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
			   (sizeof(StringId) * (Plot2TypeValue::TOTAL_PLOT2_TYPES - 1)))];
static Uint  ShadowEnableInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		   (sizeof(StringId) * (ShadowTraceEnableValue::TOTAL_VALUES - 1)))];
static Uint  TimeScaleInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
   (sizeof(StringId) * (TimePlotScaleValue::TOTAL_TIME_SCALE_VALUES - 1)))];
static Uint  PressScaleInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
   (sizeof(StringId) * (PressPlotScaleValue::TOTAL_PRESS_SCALE_VALUES - 1)))];
static Uint  VolumeScaleInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
 (sizeof(StringId) * (VolumePlotScaleValue::TOTAL_VOLUME_SCALE_VALUES - 1)))];
static Uint  FlowScaleInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
   (sizeof(StringId) * (FlowPlotScaleValue::TOTAL_FLOW_SCALE_VALUES - 1)))];

static DiscreteSettingButton::ValueInfo*  PPlot1TypeInfo_ =
					(DiscreteSettingButton::ValueInfo*)::Plot1InfoMemory_;
static DiscreteSettingButton::ValueInfo*  PPlot2TypeInfo_ =
					(DiscreteSettingButton::ValueInfo*)::Plot2InfoMemory_;
static DiscreteSettingButton::ValueInfo*  PShadowEnableInfo_ =
				   (DiscreteSettingButton::ValueInfo*)::ShadowEnableInfoMemory_;
                                     
static DiscreteSettingButton::ValueInfo*  PTimeScaleInfo_ =
					(DiscreteSettingButton::ValueInfo*)::TimeScaleInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PPressScaleInfo_ =
					(DiscreteSettingButton::ValueInfo*)::PressScaleInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PVolumeScaleInfo_ =
					(DiscreteSettingButton::ValueInfo*)::VolumeScaleInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PFlowScaleInfo_ =
					(DiscreteSettingButton::ValueInfo*)::FlowScaleInfoMemory_;

  //============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WaveformsSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructs the Waveforms subscreen.  Only one instance of this
// class should be created (by UpperSubScreenArea).  It is passed a
// pointer to UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// We construct all data members.  We make all setting buttons unclearable (all
// setting changes go into effect as soon as the setting is adjusted).  The
// subscreen is sized and colored.  We setup the valid discrete values of all
// discrete setting buttons.  We register as a target for most settings
// adjusted in this subscreen.  We register for callbacks from numerous
// buttons.  We register for patient data events as well as GUI state change
// events.  We size, position and color any graphics that need it.
//
// $[01173] The Waveforms subscreen shall provide a freeze button ...
// $[GC01002] The Waveforms subscreen shall provide a print button ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformsSubScreen::WaveformsSubScreen(SubScreenArea* pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleArea_(MiscStrs::WAVEFORMS_PLOT_SETUP_TITLE,
			   SubScreenTitleArea::SSTA_CENTER),
	timePlot1_(TRUE),
	timePlot2_(FALSE),
	isOnWaveDisplay_(TRUE),
	isStartOfWaveformFound_(FALSE),
	isLastSampleExhalation_(FALSE),
	isFreezing_(FALSE),
	isFrozen_(FALSE),
	lengthOfTimeWaveform_(0),
    isPlateauPressureDisplayed_( FALSE),
	areComplianceDisplayed_(FALSE),    
	areResistanceDisplayed_(FALSE),
    isExpiratoryPauseState_(FALSE),
    isNifPauseState_(FALSE),
    isP100PauseState_(FALSE),
    isSvcPauseState_(FALSE),
    isInspiratoryPauseState_( FALSE), 
	isLastSampleExpiratoryPause_(FALSE),
	plotSetupButton_(PLOT_SETUP_X_, PLOT_SETUP_Y_,
			PLOT_SETUP_WIDTH_, PLOT_SETUP_HEIGHT_,
			Button::LIGHT, WAVE_DISPLAY_BUTTON_BORDER_, Button::SQUARE,
			Button::NO_BORDER, MiscStrs::PLOT_SETUP_BUTTON_TITLE),
	freezeButton_(FREEZE_X_, FREEZE_Y_, FREEZE_WIDTH_,
			FREEZE_HEIGHT_, Button::LIGHT, WAVE_DISPLAY_BUTTON_BORDER_,
			Button::SQUARE, Button::NO_BORDER,
			MiscStrs::FREEZE_BUTTON_FREEZE_TITLE),
	printButton_(PRINT_X_, PRINT_Y_, PRINT_WIDTH_,
			PRINT_HEIGHT_, Button::LIGHT, WAVE_DISPLAY_BUTTON_BORDER_,
			Button::SQUARE, Button::NO_BORDER,
			MiscStrs::PRINT_BUTTON_TITLE),
	timeScaleButton_(TIME_X_SCALE_BUTTON_X_, TIME_X_SCALE_BUTTON_1PLOT_Y_,
			X_SCALE_BUTTON_WIDTH_, X_SCALE_BUTTON_HEIGHT_,
			Button::LIGHT, WAVE_DISPLAY_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER,
			NULL_STRING_ID,   // MiscStrs::HORIZONTAL_SCALE_BUTTON_TITLE,
			SettingId::TIME_PLOT_SCALE, NULL,
			MiscStrs::TIME_SCALE_HELP_MSG, 18),
	xPressureScaleButton_(PV_X_SCALE_BUTTON_X_, PV_X_SCALE_BUTTON_Y_,
			X_SCALE_BUTTON_WIDTH_, X_SCALE_BUTTON_HEIGHT_,
			Button::LIGHT, WAVE_DISPLAY_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER,
			NULL_STRING_ID,
			SettingId::PRESSURE_PLOT_SCALE, NULL,
			MiscStrs::PRESSURE_SCALE_HELP_MSG, 18),
	yPressureScaleButton_(TIME_Y_SCALE_BUTTON_X_,
			TIME_Y_SCALE_BUTTON_SMALL_PLOT1_Y_,
			Y_SCALE_BUTTON_WIDTH_, Y_SCALE_BUTTON_HEIGHT_,
			Button::LIGHT, WAVE_DISPLAY_BUTTON_BORDER_, Button::ROUND,
			Button::NO_BORDER,
			NULL_STRING_ID,
			SettingId::PRESSURE_PLOT_SCALE, NULL,
			MiscStrs::PRESSURE_SCALE_HELP_MSG, 18),
	volumeScaleButton_(TIME_Y_SCALE_BUTTON_X_,
			TIME_Y_SCALE_BUTTON_SMALL_PLOT1_Y_,
			Y_SCALE_BUTTON_WIDTH_, Y_SCALE_BUTTON_HEIGHT_,
			Button::LIGHT, WAVE_DISPLAY_BUTTON_BORDER_, Button::ROUND,
			Button::NO_BORDER,
			NULL_STRING_ID,
			SettingId::VOLUME_PLOT_SCALE, NULL,
			MiscStrs::VOLUME_SCALE_HELP_MSG, 18),
	xVolumeScaleButton_(PV_X_SCALE_BUTTON_X_, PV_X_SCALE_BUTTON_Y_,
            X_SCALE_BUTTON_WIDTH_, X_SCALE_BUTTON_HEIGHT_,
			Button::LIGHT, WAVE_DISPLAY_BUTTON_BORDER_, Button::ROUND,
			Button::NO_BORDER,
			NULL_STRING_ID,
			SettingId::VOLUME_PLOT_SCALE, NULL,
			MiscStrs::VOLUME_SCALE_HELP_MSG, 18),

	flowScaleButton_(TIME_Y_SCALE_BUTTON_X_,
			TIME_Y_SCALE_BUTTON_SMALL_PLOT1_Y_,
			Y_SCALE_BUTTON_WIDTH_, Y_SCALE_BUTTON_HEIGHT_,
			Button::LIGHT, WAVE_DISPLAY_BUTTON_BORDER_, Button::ROUND,
			Button::NO_BORDER,
			NULL_STRING_ID,
			SettingId::FLOW_PLOT_SCALE, NULL,
			MiscStrs::FLOW_SCALE_HELP_MSG, 18),
	baselineButton_(BASELINE_BUTTON_X_, BASELINE_BUTTON_Y_,
			BASELINE_BUTTON_WIDTH_, BASELINE_BUTTON_HEIGHT_,
			Button::LIGHT, WAVE_DISPLAY_BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, TextFont::TWELVE,
			BASELINE_VALUE_CENTER_X_, BASELINE_VALUE_CENTER_Y_,
			GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
				MiscStrs::BASELINE_BUTTON_TITLE :
				MiscStrs::BASELINE_BUTTON_HPA_TITLE,	// $[TI1], $[TI2]
			NULL,
			SettingId::PRESS_VOL_LOOP_BASE, NULL,
			MiscStrs::BASELINE_HELP_MSG),

	plot1SetupSettingButton_(PLOT_SETTING_BUTTON1_X_,
			PLOT_SETTING_BUTTON_Y_, PLOT_SETTING_BUTTON_WIDTH_,
			PLOT_SETTING_BUTTON_HEIGHT_,
			Button::LIGHT, PLOT_SETTING_BUTTON_BORDER_, Button::ROUND,
			Button::NO_BORDER,
			MiscStrs::PLOT1_SETTING_BUTTON_TITLE,
			SettingId::PLOT1_TYPE, NULL,
			MiscStrs::PLOT1_SETTING_HELP_MSG, 12),

	plot2SetupSettingButton_(PLOT_SETTING_BUTTON2_X_,
			PLOT_SETTING_BUTTON_Y_, PLOT_SETTING_BUTTON_WIDTH_,
			PLOT_SETTING_BUTTON_HEIGHT_,
			Button::LIGHT, PLOT_SETTING_BUTTON_BORDER_, Button::ROUND,
			Button::NO_BORDER,
			MiscStrs::PLOT2_SETTING_BUTTON_TITLE,
			SettingId::PLOT2_TYPE, NULL,
			MiscStrs::PLOT2_SETTING_HELP_MSG, 12),

	shadowEnableSettingButton_(SHADOW_SETTING_BUTTON_X_,
			SHADOW_SETTING_BUTTON_Y_, PLOT_SETTING_BUTTON_WIDTH_,
			PLOT_SETTING_BUTTON_HEIGHT_,
			Button::LIGHT, PLOT_SETTING_BUTTON_BORDER_, Button::ROUND,
			Button::NO_BORDER,
			MiscStrs::SHADOW_TRACE_ENABLE_TITLE,
			SettingId::SHADOW_TRACE_ENABLE, NULL,
			MiscStrs::SHADOW_TRACE_ENABLE_MSG, 12),

	continueButton_(CONTINUE_BUTTON_X_, CONTINUE_BUTTON_Y_,
			CONTINUE_BUTTON_WIDTH_, CONTINUE_BUTTON_HEIGHT_,
			Button::LIGHT, CONTINUE_BUTTON_BORDER_, Button::SQUARE,
			Button::NO_BORDER, MiscStrs::CONTINUE_BUTTON_TITLE),

	freezingText_(MiscStrs::FREEZING_TITLE),
	printingText_(MiscStrs::PRINTING_TITLE),
	//-----------------------------------------------------------------
	// $[PA01010] -- C, R and  PEEPi shall be displayed...
	//-----------------------------------------------------------------
	intrinsicPeepSymbol_(MiscStrs::TOUCHABLE_INTRINSIC_PEEP_LABEL,
			MiscStrs::TOUCHABLE_INTRINSIC_PEEP_MSG),
	intrinsicPeepValue_(TextFont::EIGHTEEN, 0, CENTER),
	intrinsicPeepUnit_(
			GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
						MiscStrs::TOUCHABLE_INTRINSIC_PEEP_UNIT :
						MiscStrs::TOUCHABLE_INTRINSIC_PEEP_HPA_UNIT),
														// $[TI3], $[TI4]
	intrinsicPeepMarkerText_(MiscStrs::INTRINSIC_PEEP_MARKER_LABEL),

	totalPeepSymbol_(MiscStrs::TOUCHABLE_TOTAL_PEEP_LABEL,
			MiscStrs::TOUCHABLE_TOTAL_PEEP_MSG),
	totalPeepValue_(TextFont::EIGHTEEN, 0, CENTER),
	totalPeepUnit_(
			GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
						MiscStrs::TOUCHABLE_TOTAL_PEEP_UNIT :
						MiscStrs::TOUCHABLE_TOTAL_PEEP_HPA_UNIT),
														// $[TI5], $[TI6]
	totalPeepMarkerText_(MiscStrs::TOTAL_PEEP_MARKER_LABEL),

	//-----------------------------------------------------------------
	//  $[RM12061] $[RM12061] $[RM12065] -- NIF, P100, and Vital 
    //                                      Capacity shall be displayed...
	//-----------------------------------------------------------------

	nifPressureSymbol_(MiscStrs::TOUCHABLE_NIF_PRESSURE_LABEL,
			MiscStrs::TOUCHABLE_NIF_PRESSURE_MSG),
	nifPressureValue_(TextFont::EIGHTEEN, 0, CENTER),
	nifPressureUnit_(
			GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
						MiscStrs::TOUCHABLE_NIF_PRESSURE_UNIT :
						MiscStrs::TOUCHABLE_NIF_PRESSURE_HPA_UNIT),
	nifPressureMarkerText_(MiscStrs::NIF_PRESSURE_MARKER_LABEL),

	p100PressureSymbol_(MiscStrs::TOUCHABLE_P100_PRESSURE_LABEL,
			MiscStrs::TOUCHABLE_P100_PRESSURE_MSG),
	p100PressureValue_(TextFont::EIGHTEEN, 0, CENTER),
	p100PressureUnit_(
			GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
						MiscStrs::TOUCHABLE_P100_PRESSURE_UNIT :
						MiscStrs::TOUCHABLE_P100_PRESSURE_HPA_UNIT),
	p100PressureMarkerText_(MiscStrs::P100_PRESSURE_MARKER_LABEL),   

	vitalCapacitySymbol_(MiscStrs::TOUCHABLE_VITAL_CAPACITY_LABEL,
			MiscStrs::TOUCHABLE_VITAL_CAPACITY_MSG),
	vitalCapacityValue_(TextFont::EIGHTEEN, 0, CENTER),
	vitalCapacityUnit_(MiscStrs::TOUCHABLE_VITAL_CAPACITY_UNIT),
	vitalCapacityMarkerText_(MiscStrs::VITAL_CAPACITY_MARKER_LABEL),    

	plateauPressureMarkerText_(MiscStrs::PRESS_MARKER_LABEL),
	plateauPressureSymbol_(MiscStrs::TOUCHABLE_PLATEAU_PRESSURE_LABEL,
						   MiscStrs::TOUCHABLE_PLATEAU_PRESSURE_MSG),
	plateauPressureValue_(TextFont::EIGHTEEN, 0, CENTER),
	plateauPressureUnit_(
			GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
						MiscStrs::TOUCHABLE_PLATEAU_PRESSURE_UNIT :
						MiscStrs::TOUCHABLE_PLATEAU_PRESSURE_HPA_UNIT),
	//-----------------------------------------------------------------
	// $[PA01010] -- C, R and  PEEPi shall be displayed...
	//-----------------------------------------------------------------
	lungComplianceMarkerText_(MiscStrs::R_AND_C_MARKER_LABEL),
	lungComplianceSymbol_(),
	lungComplianceValue_(TextFont::EIGHTEEN, 0, CENTER),
	lungComplianceUnit_(
			GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
						MiscStrs::TOUCHABLE_LUNG_COMPLIANCE_UNIT :
						MiscStrs::TOUCHABLE_LUNG_COMPLIANCE_HPA_UNIT),
														// $[TI5], $[TI6]
	//-----------------------------------------------------------------
	// $[PA01010] -- C, R and  PEEPi shall be displayed...
	//-----------------------------------------------------------------
	airwayResistanceMarkerText_(MiscStrs::R_AND_C_MARKER_LABEL),
	airwayResistanceSymbol_(),
	airwayResistanceValue_(TextFont::EIGHTEEN, 0, CENTER),
	airwayResistanceUnit_(
			GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
						MiscStrs::TOUCHABLE_AIRWAY_RESISTANCE_UNIT :
						MiscStrs::TOUCHABLE_AIRWAY_RESISTANCE_HPA_UNIT),
														// $[TI5], $[TI6]
	//-----------------------------------------------------------------
	// $[PA01005] -- display PAV Startup messages
	//-----------------------------------------------------------------
	pavStatusMsg_(MiscStrs::PAV_STARTUP_STATUS_SMALL_MSG),

	//-----------------------------------------------------------------
	// $[VC01006] -- display VCP/VS Startup messages
	// $[VC01007]
	//-----------------------------------------------------------------
	vcpStatusMsg_(MiscStrs::VCP_STARTUP_STATUS_SMALL_MSG),
	vsStatusMsg_(MiscStrs::VS_STARTUP_STATUS_SMALL_MSG),


	staticResistanceStatusValue_(PauseTypes::VALID),
	staticComplianceStatusValue_(PauseTypes::VALID),
	staticComplianceProblemLabel_(),
	staticResistanceProblemLabel_(),
	peepStabilityProblemLabel_(),
	staticResistanceNotAvailableText_(MiscStrs::DASH_LABEL),
	staticResistanceInadequateText_(MiscStrs::STAR_LABEL),
	staticComplianceInadequateText_(MiscStrs::STAR_LABEL),
	pauseCancelByAlarmText_(MiscStrs::MANEUVER_CANCELED),
	isInErrorState_(FALSE),
	timeScaleAxisBitmap_( Image::RXAxisIcon ),
	xPressureScaleAxisBitmap_( Image::RXAxisIcon ),
	yPressureScaleAxisBitmap_( Image::RYAxisIcon ),
	volumeScaleAxisBitmap_( Image::RYAxisIcon ),
	xVolumeScaleAxisBitmap_( Image::RXAxisIcon ),
	flowScaleAxisBitmap_( Image::RYAxisIcon ),

	currAuxInfoState_(STANDARD_),
	lastAuxInfoState_(STANDARD_),

	arePavSymbolsAllowed_(TRUE),
	arePavValuesAllowed_(TRUE),
	startOnInsp_(FALSE),
    updateCount_(0)
{
	CALL_TRACE("WaveformsSubScreen::WaveformsSubScreen(pSubScreenArea)");

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);


	// set up values for plot 1 type....
	::PPlot1TypeInfo_->numValues = Plot1TypeValue::TOTAL_PLOT1_TYPES;
	::PPlot1TypeInfo_->arrValues[Plot1TypeValue::PRESSURE_VS_TIME] =
										MiscStrs::PRESSURE_VS_TIME_VALUE;
	::PPlot1TypeInfo_->arrValues[Plot1TypeValue::VOLUME_VS_TIME] =
										MiscStrs::VOLUME_VS_TIME_VALUE;
	::PPlot1TypeInfo_->arrValues[Plot1TypeValue::FLOW_VS_TIME] =
										MiscStrs::FLOW_VS_TIME_VALUE;
	::PPlot1TypeInfo_->arrValues[Plot1TypeValue::PRESSURE_VS_VOLUME] =
										MiscStrs::PRESSURE_VS_VOLUME_VALUE;
	::PPlot1TypeInfo_->arrValues[Plot1TypeValue::FLOW_VS_VOLUME] =
										MiscStrs::FLOW_VS_VOLUME_VALUE;
	plot1SetupSettingButton_.setValueInfo(::PPlot1TypeInfo_);
	plot1SetupSettingButton_.setDropDownTextSize(10);


	// set up values for plot 2 type....
	::PPlot2TypeInfo_->numValues = Plot2TypeValue::TOTAL_PLOT2_TYPES;
	::PPlot2TypeInfo_->arrValues[Plot2TypeValue::PRESSURE_VS_TIME] =
										MiscStrs::PRESSURE_VS_TIME_VALUE;
	::PPlot2TypeInfo_->arrValues[Plot2TypeValue::VOLUME_VS_TIME] =
										MiscStrs::VOLUME_VS_TIME_VALUE;
	::PPlot2TypeInfo_->arrValues[Plot2TypeValue::FLOW_VS_TIME] =
										MiscStrs::FLOW_VS_TIME_VALUE;
    // ---------------------------------------------------------------
	// $[PA01011] -- Work-of-breathing plots shall only be selectable 
    //            when PA is the Spontaneous Type and for Plot 2 only
    // ---------------------------------------------------------------
	::PPlot2TypeInfo_->arrValues[Plot2TypeValue::WOB_GRAPHIC] =
										MiscStrs::WOB_GRAPHIC_VALUE;
	::PPlot2TypeInfo_->arrValues[Plot2TypeValue::NO_PLOT2] =
										MiscStrs::NO_PLOT2_VALUE;
	plot2SetupSettingButton_.setValueInfo(::PPlot2TypeInfo_);
	plot2SetupSettingButton_.setDropDownTextSize(10);


	// set up values for shadow trace enable....
	::PShadowEnableInfo_->numValues = ShadowTraceEnableValue::TOTAL_VALUES;
	::PShadowEnableInfo_->arrValues[ShadowTraceEnableValue::DISABLE_SHADOW] =
										MiscStrs::DISABLE_SHADOW_TRACE_VALUE;
	::PShadowEnableInfo_->arrValues[ShadowTraceEnableValue::ENABLE_SHADOW] =
										MiscStrs::ENABLE_SHADOW_TRACE_VALUE;
	shadowEnableSettingButton_.setValueInfo(::PShadowEnableInfo_);


	Uint  idx;

	// Initialize valid discrete value settings for Time Scale button.
	// Note: this setting button does not display changing discrete values
	// when adjusted (hence the NULL strings which are added with each
	// value).  Changes to this setting can be seen by the dynamically
	// updated X Axis scale values on the waveform time plots.  The same
	// applies for all scale adjustment buttons.
	::PTimeScaleInfo_->numValues = TimePlotScaleValue::TOTAL_TIME_SCALE_VALUES;
	for (idx = 0u; idx < TimePlotScaleValue::TOTAL_TIME_SCALE_VALUES; idx++)
	{
		// initialize to 'NULL_STRING_ID', because no values are shown on
		// this scaling button...
		::PTimeScaleInfo_->arrValues[idx] = NULL_STRING_ID;
	}

	timeScaleButton_.setValueInfo(::PTimeScaleInfo_);
	timeScaleAxisBitmap_.setX(X_AXIS_BITMAP_XPOS);
	timeScaleAxisBitmap_.setY(X_AXIS_BITMAP_YPOS);
	timeScaleButton_.addLabel( &timeScaleAxisBitmap_ );

	// setup the values for the X and Y pressure scales...
	::PPressScaleInfo_->numValues =
								PressPlotScaleValue::TOTAL_PRESS_SCALE_VALUES;
	for (idx = 0u; idx < PressPlotScaleValue::TOTAL_PRESS_SCALE_VALUES; idx++)
	{
		// initialize to 'NULL_STRING_ID', because no values are shown on
		// this scaling button...
		::PPressScaleInfo_->arrValues[idx] = NULL_STRING_ID;
	}

	xPressureScaleButton_.setValueInfo(::PPressScaleInfo_);
	xPressureScaleAxisBitmap_.setX(X_AXIS_BITMAP_XPOS);
	xPressureScaleAxisBitmap_.setY(X_AXIS_BITMAP_YPOS);
	xPressureScaleButton_.addLabel( &xPressureScaleAxisBitmap_ );

	yPressureScaleButton_.setValueInfo(::PPressScaleInfo_);
	yPressureScaleAxisBitmap_.setX(Y_AXIS_BITMAP_XPOS); //3 
	yPressureScaleAxisBitmap_.setY(Y_AXIS_BITMAP_YPOS); //1
	yPressureScaleButton_.addLabel( &yPressureScaleAxisBitmap_ );

	// Initialize valid discrete value settings for the Volume Scale button
	::PVolumeScaleInfo_->numValues =
								VolumePlotScaleValue::TOTAL_VOLUME_SCALE_VALUES;
	for (idx = 0u; idx < VolumePlotScaleValue::TOTAL_VOLUME_SCALE_VALUES; idx++)
	{
		// initialize to 'NULL_STRING_ID', because no values are shown on
		// this scaling button...
		::PVolumeScaleInfo_->arrValues[idx] = NULL_STRING_ID;
	}

	volumeScaleButton_.setValueInfo(::PVolumeScaleInfo_);
	volumeScaleAxisBitmap_.setX(Y_AXIS_BITMAP_XPOS); //3 
	volumeScaleAxisBitmap_.setY(Y_AXIS_BITMAP_YPOS); //1
	volumeScaleButton_.addLabel( &volumeScaleAxisBitmap_ );

	xVolumeScaleButton_.setValueInfo(::PVolumeScaleInfo_);
	xVolumeScaleAxisBitmap_.setX(X_AXIS_BITMAP_XPOS);
	xVolumeScaleAxisBitmap_.setY(X_AXIS_BITMAP_YPOS);
	xVolumeScaleButton_.addLabel( &xVolumeScaleAxisBitmap_ );


	// Initialize valid discrete value settings for the Flow Scale button
	::PFlowScaleInfo_->numValues = FlowPlotScaleValue::TOTAL_FLOW_SCALE_VALUES;
	for (idx = 0u; idx < FlowPlotScaleValue::TOTAL_FLOW_SCALE_VALUES; idx++)
	{
		// initialize to 'NULL_STRING_ID', because no values are shown on
		// this scaling button...
		::PFlowScaleInfo_->arrValues[idx] = NULL_STRING_ID;
	}

	flowScaleButton_.setValueInfo(::PFlowScaleInfo_);
	flowScaleAxisBitmap_.setX(Y_AXIS_BITMAP_XPOS); //3 
	flowScaleAxisBitmap_.setY(Y_AXIS_BITMAP_YPOS); //1
	flowScaleButton_.addLabel( &flowScaleAxisBitmap_ );

	// Trap button events on the Plot Setup, Freeze and Continue buttons.
	plotSetupButton_.setButtonCallback(this);
	freezeButton_.setButtonCallback(this);
	printButton_.setButtonCallback(this);
	continueButton_.setButtonCallback(this);

	// Position and size waveform plot objects
	timePlot1_.setX(TIME_PLOT_X_);
	timePlot1_.setY(PLOT1_Y_);
	timePlot1_.setGridArea(TIME_PLOT_GRID_X_, TIME_PLOT_GRID_Y_,
					TIME_PLOT_GRID_WIDTH_, SMALL_TIME_PLOT_GRID_HEIGHT_);
	timePlot1_.setWidth(TIME_PLOT_WIDTH_);
	timePlot1_.setHeight(SMALL_TIME_PLOT_HEIGHT_);

	timePlot2_.setX(TIME_PLOT_X_);
	timePlot2_.setY(PLOT2_Y_);
	timePlot2_.setGridArea(TIME_PLOT_GRID_X_, TIME_PLOT_GRID_Y_,
					TIME_PLOT_GRID_WIDTH_, SMALL_TIME_PLOT_GRID_HEIGHT_);
	timePlot2_.setWidth(TIME_PLOT_WIDTH_);
	timePlot2_.setHeight(SMALL_TIME_PLOT_HEIGHT_);

	wobGraphic_.setX(TIME_PLOT_X_);
	wobGraphic_.setY(PLOT2_Y_);
	wobGraphic_.setWidth(TIME_PLOT_WIDTH_);
	wobGraphic_.setHeight(SMALL_TIME_PLOT_HEIGHT_);

	pressureVolumePlot_.setX(PV_PLOT_X_);
	pressureVolumePlot_.setY(PLOT1_Y_);
	pressureVolumePlot_.setGridArea(PV_PLOT_GRID_X_, PV_PLOT_GRID_Y_,
					PV_PLOT_GRID_WIDTH_, PV_PLOT_GRID_HEIGHT_);
	pressureVolumePlot_.setWidth(PV_PLOT_WIDTH_);
	pressureVolumePlot_.setHeight(PV_PLOT_HEIGHT_);
	pressureVolumePlot_.setXUnitsString(
					GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
								MiscStrs::PRESSURE_PLOT_UNITS :
								MiscStrs::PRESSURE_PLOT_HPA_UNITS,
														// $[TI7], $[TI8]
					PV_X_UNITS_X_, PV_X_UNITS_Y_);
	pressureVolumePlot_.setYUnitsString(MiscStrs::VOLUME_PLOT_UNITS,
									PV_Y_UNITS_X_, PV_Y_UNITS_Y_);


	flowVolumePlot_.setX(PV_PLOT_X_);
	flowVolumePlot_.setY(PLOT1_Y_);
	flowVolumePlot_.setGridArea(PV_PLOT_GRID_X_, PV_PLOT_GRID_Y_,
					PV_PLOT_GRID_WIDTH_, PV_PLOT_GRID_HEIGHT_);
	flowVolumePlot_.setWidth(PV_PLOT_WIDTH_);
	flowVolumePlot_.setHeight(PV_PLOT_HEIGHT_);
	flowVolumePlot_.setXUnitsString(MiscStrs::VOLUME_PLOT_UNITS,
									 PV_X_UNITS_X_, PV_X_UNITS_Y_);

	flowVolumePlot_.setYUnitsString(MiscStrs::BIG_FLOW_PLOT_UNITS,
									PV_Y_UNITS_X_, PV_Y_UNITS_Y_);


	// Attach waveform plot iterators to the appropriate waveform data buffers.
	currentFullPlotIter_.attachToBuffer(&currentDataBuffer_);
	currentSegmentIter_.attachToBuffer(&currentDataBuffer_);
	frozenTimePlotIter_.attachToBuffer(&frozenDataBuffer_);
	frozenLoopPlotIter_.attachToBuffer(&frozenDataBuffer_);

	// Register for changes to Intrinsic and Total PEEP patient data
	PatientDataRegistrar::RegisterTarget(PatientDataId::PEEP_INTRINSIC_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::PEEP_TOTAL_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::PLATEAU_PRESSURE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::END_EXP_PAUSE_PRESSURE_STATE_ITEM, this);

	// Register for changes to respiratory mechanics maneuver data
	PatientDataRegistrar::RegisterTarget(PatientDataId::NIF_PRESSURE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::P100_PRESSURE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::VITAL_CAPACITY_ITEM, this);

	// Register for changes to STATIC_LUNG_COMPLIANCE_ITEM and
	// STATIC_AIRWAY_RESISTANCE_ITEM patient data
	PatientDataRegistrar::RegisterTarget(PatientDataId::STATIC_LUNG_COMPLIANCE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::STATIC_AIRWAY_RESISTANCE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::STATIC_LUNG_COMPLIANCE_VALID_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::STATIC_AIRWAY_RESISTANCE_VALID_ITEM, this);

	// Register for changes to the PAV information...
	PatientDataRegistrar::RegisterTarget(PatientDataId::PAV_LUNG_COMPLIANCE_ITEM, this); 
	PatientDataRegistrar::RegisterTarget(PatientDataId::TOTAL_AIRWAY_RESISTANCE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::PAV_PEEP_INTRINSIC_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::PAV_STATE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::IS_PEEP_RECOVERY_ITEM, this);

	// Register for changes to the VTPCV information...
	PatientDataRegistrar::RegisterTarget(PatientDataId::VTPCV_STATE_ITEM, this);
	
	// Initialize freeze button and freezing text colors.
	freezeButton_.setTitleText(MiscStrs::FREEZE_BUTTON_FREEZE_TITLE);
	freezeButton_.setTitleColor(Colors::BLACK);

	// Initialize print button and printing text colors.
	printButton_.setTitleText(MiscStrs::PRINT_BUTTON_TITLE);
	printButton_.setTitleColor(Colors::BLACK);

	freezingText_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
	printingText_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);

	freezingText_.setShow(FALSE);
	printingText_.setShow(FALSE);


	//-----------------------------------------------------------------
	// setup Exp Pause artifacts...
	//-----------------------------------------------------------------

	intrinsicPeepSymbol_.setColor(Colors::WHITE);
	intrinsicPeepValue_.setColor(Colors::WHITE);
	intrinsicPeepUnit_.setColor(Colors::LIGHT_BLUE);
	intrinsicPeepSymbol_.setX(INTRINSIC_PEEP_SYMBOL_X_);
	intrinsicPeepValue_.setX(INTRINSIC_PEEP_VALUE_X_);
	intrinsicPeepValue_.setWidth(VALUE_WIDTH_);
	intrinsicPeepValue_.setHeight(VALUE_HEIGHT_);
	intrinsicPeepUnit_.setX(INTRINSIC_PEEP_UNIT_X_);
	intrinsicPeepMarkerText_.setColor(Colors::WHITE);
	intrinsicPeepMarkerText_.setX(intrinsicPeepSymbol_.getX() +
								  intrinsicPeepSymbol_.getWidth());
	intrinsicPeepMarkerText_.setY(intrinsicPeepSymbol_.getY());

	totalPeepSymbol_.setColor(Colors::WHITE);
	totalPeepValue_.setColor(Colors::WHITE);
	totalPeepUnit_.setColor(Colors::LIGHT_BLUE);
	totalPeepSymbol_.setX(TOTAL_PEEP_SYMBOL_X_);
	totalPeepValue_.setX(TOTAL_PEEP_VALUE_X_);
	totalPeepValue_.setWidth(VALUE_WIDTH_);
	totalPeepValue_.setHeight(VALUE_HEIGHT_);
	totalPeepUnit_.setX(TOTAL_PEEP_UNIT_X_);
	totalPeepMarkerText_.setColor(Colors::WHITE);
	totalPeepMarkerText_.setX(totalPeepSymbol_.getX() +
							  totalPeepSymbol_.getWidth());
	totalPeepMarkerText_.setY(totalPeepSymbol_.getY());

    isEndofExpPauseManeuver_= FALSE;
	
	peepStabilityProblemLabel_.setColor(Colors::WHITE);
	peepStabilityProblemLabel_.setX(INTRINSIC_PEEP_SYMBOL_X_);

	pauseCancelByAlarmText_.setColor(Colors::WHITE);
	pauseCancelByAlarmText_.setX(PAUSE_CANCEL_BY_ALARM_TEXT_X_);

	//-----------------------------------------------------------------
	// initialize array of Exp Pause drawable pointers...
	//-----------------------------------------------------------------

	idx = 0u;
	arrExpPausePtrs_[idx++] = &intrinsicPeepSymbol_;
	arrExpPausePtrs_[idx++] = &intrinsicPeepValue_;
	arrExpPausePtrs_[idx++] = &intrinsicPeepUnit_;
	arrExpPausePtrs_[idx++] = &intrinsicPeepMarkerText_;
	arrExpPausePtrs_[idx++] = &totalPeepSymbol_;
	arrExpPausePtrs_[idx++] = &totalPeepValue_;
	arrExpPausePtrs_[idx++] = &totalPeepUnit_;
	arrExpPausePtrs_[idx++] = &totalPeepMarkerText_;
	arrExpPausePtrs_[idx++] = &peepStabilityProblemLabel_;
	arrExpPausePtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_EXP_PAUSE_ARTIFACTS_), idx);

	for (idx = 0u; arrExpPausePtrs_[idx] != NULL; idx++)
	{
		// always start out with all info drawables turned off...
		arrExpPausePtrs_[idx]->setShow(FALSE);
	}

	//-----------------------------------------------------------------
	// setup NIF Pause display items...
	//-----------------------------------------------------------------

	nifPressureSymbol_.setColor(Colors::WHITE);
	nifPressureValue_.setColor(Colors::WHITE);
	nifPressureUnit_.setColor(Colors::LIGHT_BLUE);
	nifPressureSymbol_.setX(NIF_PRESSURE_SYMBOL_X_);
	nifPressureValue_.setX(NIF_PRESSURE_VALUE_X_);
	nifPressureValue_.setWidth(VALUE_WIDTH_);
	nifPressureValue_.setHeight(VALUE_HEIGHT_);
	nifPressureUnit_.setX(NIF_PRESSURE_UNIT_X_);
	nifPressureMarkerText_.setColor(Colors::WHITE);
	nifPressureMarkerText_.setX(nifPressureSymbol_.getX() +
							    nifPressureSymbol_.getWidth());
	nifPressureMarkerText_.setY(nifPressureSymbol_.getY());

	isEndofNifPauseManeuver_= FALSE;

	idx = 0u;
	arrNifPausePtrs_[idx++] = &nifPressureSymbol_;
	arrNifPausePtrs_[idx++] = &nifPressureValue_;
	arrNifPausePtrs_[idx++] = &nifPressureUnit_;
	arrNifPausePtrs_[idx++] = &nifPressureMarkerText_;
	arrNifPausePtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_NIF_PAUSE_ARTIFACTS_), idx);

	for (idx = 0u; arrNifPausePtrs_[idx] != NULL; idx++)
	{
		// always start out with all info drawables turned off...
		arrNifPausePtrs_[idx]->setShow(FALSE);
	}

	//-----------------------------------------------------------------
	// setup P100 Pause display items...
	//-----------------------------------------------------------------

	p100PressureSymbol_.setColor(Colors::WHITE);
	p100PressureValue_.setColor(Colors::WHITE);
	p100PressureUnit_.setColor(Colors::LIGHT_BLUE);
	p100PressureSymbol_.setX(P100_PRESSURE_SYMBOL_X_);
	p100PressureValue_.setX(P100_PRESSURE_VALUE_X_);
	p100PressureValue_.setWidth(VALUE_WIDTH_);
	p100PressureValue_.setHeight(VALUE_HEIGHT_);
	p100PressureUnit_.setX(P100_PRESSURE_UNIT_X_);
	p100PressureMarkerText_.setColor(Colors::WHITE);
	p100PressureMarkerText_.setX(p100PressureSymbol_.getX() +
								p100PressureSymbol_.getWidth());
	p100PressureMarkerText_.setY(p100PressureSymbol_.getY());

	isEndofNifPauseManeuver_= FALSE;

	idx = 0u;
	arrP100PausePtrs_[idx++] = &p100PressureSymbol_;
	arrP100PausePtrs_[idx++] = &p100PressureValue_;
	arrP100PausePtrs_[idx++] = &p100PressureUnit_;
	arrP100PausePtrs_[idx++] = &p100PressureMarkerText_;
	arrP100PausePtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_P100_PAUSE_ARTIFACTS_), idx);

	for (idx = 0u; arrP100PausePtrs_[idx] != NULL; idx++)
	{
		// always start out with all info drawables turned off...
		arrP100PausePtrs_[idx]->setShow(FALSE);
	}

	//-----------------------------------------------------------------
	// setup SVC Pause display items...
	//-----------------------------------------------------------------

	vitalCapacitySymbol_.setColor(Colors::WHITE);
	vitalCapacityValue_.setColor(Colors::WHITE);
	vitalCapacityUnit_.setColor(Colors::LIGHT_BLUE);
	vitalCapacitySymbol_.setX(VITAL_CAPACITY_SYMBOL_X_);
	vitalCapacityValue_.setX(VITAL_CAPACITY_VALUE_X_);
	vitalCapacityValue_.setWidth(VALUE_WIDTH_);
	vitalCapacityValue_.setHeight(VALUE_HEIGHT_);
	vitalCapacityUnit_.setX(VITAL_CAPACITY_UNIT_X_);
	vitalCapacityMarkerText_.setColor(Colors::WHITE);
	vitalCapacityMarkerText_.setX(vitalCapacitySymbol_.getX() +
								vitalCapacitySymbol_.getWidth());
	vitalCapacityMarkerText_.setY(vitalCapacitySymbol_.getY());

	isEndofNifPauseManeuver_= FALSE;

	idx = 0u;
	arrSvcPausePtrs_[idx++] = &vitalCapacitySymbol_;
	arrSvcPausePtrs_[idx++] = &vitalCapacityValue_;
	arrSvcPausePtrs_[idx++] = &vitalCapacityUnit_;
	arrSvcPausePtrs_[idx++] = &vitalCapacityMarkerText_;
	arrSvcPausePtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_VC_PAUSE_ARTIFACTS_), idx);

	for (idx = 0u; arrSvcPausePtrs_[idx] != NULL; idx++)
	{
		// always start out with all info drawables turned off...
		arrSvcPausePtrs_[idx]->setShow(FALSE);
	}

	//-----------------------------------------------------------------
	// setup Insp Pause artifacts...
	//-----------------------------------------------------------------

	plateauPressureSymbol_.setColor(Colors::WHITE);
	plateauPressureValue_.setColor(Colors::WHITE);
	plateauPressureUnit_.setColor(Colors::LIGHT_BLUE);
	plateauPressureSymbol_.setX(COLUMN3_SYMBOL_X_);
	plateauPressureValue_.setX(COLUMN3_VALUE_X_);
	plateauPressureValue_.setWidth(VALUE_WIDTH_);
	plateauPressureValue_.setHeight(VALUE_HEIGHT_);
	plateauPressureUnit_.setX(COLUMN3_UNIT_X_);
	plateauPressureMarkerText_.setColor(Colors::WHITE);
	plateauPressureMarkerText_.setX(plateauPressureSymbol_.getX() +
								    plateauPressureSymbol_.getWidth() + 5);
	plateauPressureMarkerText_.setY(plateauPressureValue_.getY());
    
	lungComplianceSymbol_.setColor(Colors::WHITE);
	lungComplianceValue_.setColor(Colors::WHITE);
	lungComplianceUnit_.setColor(Colors::LIGHT_BLUE);
	lungComplianceSymbol_.setX(COLUMN1_SYMBOL_X_);
	lungComplianceValue_.setX(COLUMN1_VALUE_X_);
	lungComplianceValue_.setWidth(VALUE_WIDTH_);
	lungComplianceValue_.setHeight(VALUE_HEIGHT_);
	lungComplianceUnit_.setX(COLUMN1_UNIT_X_);
	lungComplianceMarkerText_.setColor(Colors::WHITE);
	lungComplianceMarkerText_.setX(lungComplianceSymbol_.getX() +
								   lungComplianceSymbol_.getWidth() + 37);
	lungComplianceMarkerText_.setY(lungComplianceValue_.getY());

	staticComplianceInadequateText_.setColor(Colors::WHITE);
	staticComplianceInadequateText_.setX(lungComplianceValue_.getX());
	staticComplianceProblemLabel_.setColor(Colors::WHITE);
	staticComplianceProblemLabel_.setX(lungComplianceSymbol_.getX());
	
	airwayResistanceSymbol_.setColor(Colors::WHITE);
	airwayResistanceValue_.setColor(Colors::WHITE);
	airwayResistanceUnit_.setColor(Colors::LIGHT_BLUE);
	airwayResistanceSymbol_.setX(COLUMN2_SYMBOL_X_);
	airwayResistanceValue_.setX(COLUMN2_VALUE_X_);
	airwayResistanceValue_.setWidth(VALUE_WIDTH_);
	airwayResistanceValue_.setHeight(VALUE_HEIGHT_);
	airwayResistanceUnit_.setX(COLUMN2_UNIT_X_);
	airwayResistanceMarkerText_.setColor(Colors::WHITE);
	airwayResistanceMarkerText_.setX(airwayResistanceSymbol_.getX() +
								     airwayResistanceSymbol_.getWidth() + 35);
	airwayResistanceMarkerText_.setY(airwayResistanceValue_.getY());
	staticResistanceNotAvailableText_.setColor(Colors::WHITE);
	staticResistanceNotAvailableText_.setX(airwayResistanceValue_.getX());
	staticResistanceInadequateText_.setColor(Colors::WHITE);
	staticResistanceInadequateText_.setX(airwayResistanceValue_.getX());
	staticResistanceProblemLabel_.setColor(Colors::WHITE);
	staticResistanceProblemLabel_.setX(airwayResistanceSymbol_.getX());

    //-----------------------------------------------------------------
	// initialize array of Insp Pause drawable pointers...
	//-----------------------------------------------------------------

	idx = 0u;
	arrInspPausePlateauPtrs_[idx++] = &plateauPressureSymbol_;
	arrInspPausePlateauPtrs_[idx++] = &plateauPressureValue_;
	arrInspPausePlateauPtrs_[idx++] = &plateauPressureUnit_;
	arrInspPausePlateauPtrs_[idx++] = &plateauPressureMarkerText_;
    arrInspPausePlateauPtrs_[idx]   = NULL;
    AUX_CLASS_ASSERTION((idx <= MAX_INSP_PLATEAU_ARTIFACTS_), idx);

	for (idx = 0u; arrInspPausePlateauPtrs_[idx] != NULL; idx++)
	{
		// always start out with all info drawables turned off...
		arrInspPausePlateauPtrs_[idx]->setShow(FALSE);
	}

    idx = 0u;
	arrInspPauseCompliancePtrs_[idx++] = &lungComplianceSymbol_;
	arrInspPauseCompliancePtrs_[idx++] = &lungComplianceValue_;
	arrInspPauseCompliancePtrs_[idx++] = &lungComplianceUnit_;
	arrInspPauseCompliancePtrs_[idx++] = &lungComplianceMarkerText_;
	arrInspPauseCompliancePtrs_[idx++] = &staticComplianceProblemLabel_;
    arrInspPauseCompliancePtrs_[idx]   = NULL;
    AUX_CLASS_ASSERTION((idx <= MAX_INSP_COMPLIANCE_ARTIFACTS_), idx);

	for (idx = 0u; arrInspPauseCompliancePtrs_[idx] != NULL; idx++)
	{
		// always start out with all info drawables turned off...
		arrInspPauseCompliancePtrs_[idx]->setShow(FALSE);
	}

    idx = 0u;
	arrInspPauseResistancePtrs_[idx++] = &airwayResistanceSymbol_;
	arrInspPauseResistancePtrs_[idx++] = &airwayResistanceValue_;
	arrInspPauseResistancePtrs_[idx++] = &airwayResistanceUnit_;
	arrInspPauseResistancePtrs_[idx++] = &airwayResistanceMarkerText_;
	arrInspPauseResistancePtrs_[idx++] = &staticResistanceNotAvailableText_;
	arrInspPauseResistancePtrs_[idx++] = &staticResistanceInadequateText_;
    arrInspPauseResistancePtrs_[idx]   = NULL;
    AUX_CLASS_ASSERTION((idx <= MAX_INSP_RESISTANCE_ARTIFACTS_), idx);

	for (idx = 0u; arrInspPauseResistancePtrs_[idx] != NULL; idx++)
	{
		// always start out with all info drawables turned off...
		arrInspPauseResistancePtrs_[idx]->setShow(FALSE);
	}


	//-----------------------------------------------------------------
	// initialize array of PAV drawable pointers...
	//-----------------------------------------------------------------

	pavStatusMsg_.setX(::COLUMN3_SYMBOL_X_);

	idx = 0u;
	arrPavPtrs_[idx++] = &lungComplianceSymbol_;
	arrPavPtrs_[idx++] = &lungComplianceValue_;
	arrPavPtrs_[idx++] = &lungComplianceUnit_;
	arrPavPtrs_[idx++] = &lungComplianceMarkerText_;
	arrPavPtrs_[idx++] = &airwayResistanceSymbol_;
	arrPavPtrs_[idx++] = &airwayResistanceValue_;
	arrPavPtrs_[idx++] = &airwayResistanceUnit_;
	arrPavPtrs_[idx++] = &airwayResistanceMarkerText_;
	arrPavPtrs_[idx++] = &pavStatusMsg_;
	arrPavPtrs_[idx++] = &intrinsicPeepSymbol_;
	arrPavPtrs_[idx++] = &intrinsicPeepValue_;
	arrPavPtrs_[idx++] = &intrinsicPeepUnit_;
	arrPavPtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_PAV_ARTIFACTS_), idx);

	for (idx = 0u; arrPavPtrs_[idx] != NULL; idx++)
	{
		// always start out with all info drawables turned off...
		arrPavPtrs_[idx]->setShow(FALSE);
	}


	//-----------------------------------------------------------------
	// initialize array of VC+ drawable pointers...
	//-----------------------------------------------------------------
	vcpStatusMsg_.setX(::COLUMN3_SYMBOL_X_);

	idx = 0u;
	arrVcpPtrs_[idx++] = &vcpStatusMsg_;
	arrVcpPtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_VCP_ARTIFACTS_), idx);
	vcpStatusMsg_.setShow(FALSE);
    
	//-----------------------------------------------------------------
	// initialize array of VS drawable pointers...
	//-----------------------------------------------------------------
	vsStatusMsg_.setX(::COLUMN3_SYMBOL_X_);

	idx = 0u;
	arrVsPtrs_[idx++] = &vsStatusMsg_;
	arrVsPtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_VS_ARTIFACTS_), idx);
	vsStatusMsg_.setShow(FALSE);

	//-----------------------------------------------------------------
	// initialize array of "display" setting button pointers...
	//-----------------------------------------------------------------

	// add display buttons to array...
	idx = 0u;
	arrDisplayBtnPtrs_[idx++] = &timeScaleButton_;
	arrDisplayBtnPtrs_[idx++] = &xPressureScaleButton_;
	arrDisplayBtnPtrs_[idx++] = &yPressureScaleButton_;
	arrDisplayBtnPtrs_[idx++] = &volumeScaleButton_;
	arrDisplayBtnPtrs_[idx++] = &xVolumeScaleButton_;
	arrDisplayBtnPtrs_[idx++] = &flowScaleButton_;
	arrDisplayBtnPtrs_[idx++] = &baselineButton_;
	arrDisplayBtnPtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_DISPLAY_BUTTONS_), idx);

	//-----------------------------------------------------------------
	// add "display" setting buttons to container...
	//-----------------------------------------------------------------

	for (idx = 0u; arrDisplayBtnPtrs_[idx] != NULL; idx++)
	{
		// all of these buttons attach to this subscreen directly...
		addDrawable(arrDisplayBtnPtrs_[idx]);

		// All Upper screen setting buttons are unclearable, i.e. the Clear key
		// is an invalid user choice when adjusting these settings.
		(arrDisplayBtnPtrs_[idx])->makeUnclearable();

		// All Upper screen setting buttons must use the persistent Adjust Panel
		// focus so as to be insulated from the Lower screens focus.
		(arrDisplayBtnPtrs_[idx])->usePersistentFocus();
	}

	//-----------------------------------------------------------------
	// initialize array of "setup" setting button pointers...
	//-----------------------------------------------------------------

	// add display buttons to array...
	idx = 0u;
	arrSetupBtnPtrs_[idx++] = &plot1SetupSettingButton_;
	arrSetupBtnPtrs_[idx++] = &plot2SetupSettingButton_;
	arrSetupBtnPtrs_[idx++] = &shadowEnableSettingButton_;
	arrSetupBtnPtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_SETUP_BUTTONS_), idx);

	//-----------------------------------------------------------------
	// add "setup" setting buttons to container...
	//-----------------------------------------------------------------

	for (idx = 0u; arrSetupBtnPtrs_[idx] != NULL; idx++)
	{
		// all of these buttons attach to this subscreen directly...
		addDrawable(arrSetupBtnPtrs_[idx]);

		// All Upper screen setting buttons are unclearable, i.e. the Clear key
		// is an invalid user choice when adjusting these settings.
		(arrSetupBtnPtrs_[idx])->makeUnclearable();

		// All Upper screen setting buttons must use the persistent Adjust Panel
		// focus so as to be insulated from the Lower screens focus.
		(arrSetupBtnPtrs_[idx])->usePersistentFocus();
	}

	// Register for Breath-Delivery status events
	BdEventRegistrar::RegisterTarget(EventData::VENT_INOP, this);	
	BdEventRegistrar::RegisterTarget(EventData::PATIENT_CONNECT, this);	
	BdEventRegistrar::RegisterTarget(EventData::SVO, this);	
	BdEventRegistrar::RegisterTarget(EventData::OCCLUSION, this);
	BdEventRegistrar::RegisterTarget(EventData::APNEA_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::SAFETY_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::PROX_FAULT, this);
	BdEventRegistrar::RegisterTarget(EventData::PROX_READY, this);

	// begin monitoring ALL setting changes to the accepted context...
	attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID,
					 Notification::BATCH_CHANGED);
	attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID,
					 Notification::NON_BATCH_CHANGED);
												// $[TI1]

    // Save current waveform settings for NIF Maneuver
    AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

    plot1SaveSetting_ = rAccContext.getNonBatchDiscreteValue(SettingId::PLOT1_TYPE);
    plot2SaveSetting_ = rAccContext.getNonBatchDiscreteValue(SettingId::PLOT2_TYPE);

    pressurePlotScaleSaveSetting_ = rAccContext.getNonBatchDiscreteValue(
                                             SettingId::PRESSURE_PLOT_SCALE);

    volumePlotScaleSaveSetting_ = rAccContext.getNonBatchDiscreteValue(
                                             SettingId::VOLUME_PLOT_SCALE);

    flowPlotScaleSaveSetting_   = rAccContext.getNonBatchDiscreteValue(
                                             SettingId::FLOW_PLOT_SCALE);

    timePlotScaleSaveSetting_   = rAccContext.getNonBatchDiscreteValue(
                                             SettingId::TIME_PLOT_SCALE);

    shadowTraceEnableSaveSetting_   = rAccContext.getNonBatchDiscreteValue(
                                             SettingId::SHADOW_TRACE_ENABLE);

    // Set default status values
	isNifWaveformFoisting_ = FALSE;
    isVcWaveformFoisting_  = FALSE;
    updatePlot_ = TRUE;

	isPressureVolumeLoopUpdated_ = TRUE;
	isFlowVolumeLoopUpdated_ = TRUE;


	const SettingPtr * pSettingPtr = SettingsMgr::GetArrayOfSettingPtrs();
	idx = 0u;
	arrPlotSettingPtr_[idx++] = pSettingPtr[SettingId::PLOT1_TYPE];
	arrPlotSettingPtr_[idx++] = pSettingPtr[SettingId::PLOT2_TYPE];
	arrPlotSettingPtr_[idx++] = pSettingPtr[SettingId::PRESSURE_PLOT_SCALE];
	arrPlotSettingPtr_[idx++] = pSettingPtr[SettingId::VOLUME_PLOT_SCALE];
	arrPlotSettingPtr_[idx++] = pSettingPtr[SettingId::FLOW_PLOT_SCALE];
	arrPlotSettingPtr_[idx++] = pSettingPtr[SettingId::TIME_PLOT_SCALE];
	arrPlotSettingPtr_[idx++] = pSettingPtr[SettingId::SHADOW_TRACE_ENABLE];
	arrPlotSettingPtr_[idx]   = NULL;
    AUX_CLASS_ASSERTION((idx <= MAX_WAVEFORM_PLOT_SETTING), idx);

	pTimeScaleSetting_ = (TimePlotScaleSetting*)(pSettingPtr[SettingId::TIME_PLOT_SCALE]);

	isProxInop_ = FALSE;
	isProxEnabled_ = FALSE;
	isProxDisplayTimerOn_ = FALSE;
    isProxInStartupMode_ = FALSE;
    clearWaveform_ = FALSE;
    isWaveformErased_ = FALSE;
    proxManueverType_ = 0;
    isVentNotInStartupOrWaitForPT_ = FALSE;
    displayProxStartupMsgOnce_ = TRUE;

	// Register for timer event callbacks.
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::PROX_STATUS_TIMEOUT, this);



}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~WaveformsSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys the Waveforms subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformsSubScreen::~WaveformsSubScreen(void)
{
	CALL_TRACE("WaveformsSubScreen::~WaveformsSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()
//
//@ Interface-Description
// Called by UpperSubScreenArea just before this subscreen is displayed
// allowing us to do any necessary display setup.  Takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes members so that we begin on the waveform plot display
// with a clean sheet, waiting for the next inspiration to begin plotting
// waveforms.  However, if the waveform subscreen was previously exited
// while in the frozen state then we begin by displaying the previous
// frozen waveform.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::activate(void)
{
	CALL_TRACE("WaveformsSubScreen::activate(void)");

	isOnWaveDisplay_ = TRUE;		// Display waveform plot screen first

	// Reset flags to indicate that we must wait for the beginning of
	// the next inspiration to begin plotting.
	isLastSampleExhalation_ = FALSE;
	isStartOfWaveformFound_ = FALSE;

	// Reset the current plot iterators.
	currentFullPlotIter_.reset();
	currentSegmentIter_.beginInterval();
	currentSegmentIter_.endInterval();

	layoutScreen_();				// Layout the subscreen appropriately

 
    // If the Waveforms sub-screen is frozen and
    // visible to the user, then redisplay the frozen plot.
    if (isFrozen_ && isVisible())  
	{													// $[TI1]
		// We need a little hack here to draw the frozen waveforms.  Since
		// the Waveform subscreen is not actually displayed and redrawn
		// until after this call to activate() we must take matters into
		// our own hands and redraw the subscreen now so as to allow us to
		// plot the frozen waveform on top of the waveform grids.  We
		// display this subscreen by appending it to its subscreen area
		// and then repainting the screen.  This then allows the waveforms
		// to be plotted.
		getSubScreenArea()->addDrawable(this);
		getBaseContainer()->repaint();

		// Plot the appropriate waveforms (time plot 1 and possibly time plot 2
		// or else just the pressure-volume plot).
		if (timePlot1_.isVisible())
		{												// $[TI2]
			isPlot1Updated_ = FALSE;
			GuiApp::RaiseGuiEventPriority(GuiApp::GUI_PLOT1_EVENT,
											GuiApp::GUI_HIGHEST_PRIORITY);
			
			if (timePlot2_.isVisible())
			{											// $[TI3]
				isPlot2Updated_ = FALSE;
				GuiApp::RaiseGuiEventPriority(GuiApp::GUI_PLOT2_EVENT,
												GuiApp::GUI_FOURTH_HIGHEST_PRIORITY);
			}											// $[TI4]
		}
        else if(flowVolumePlot_.isVisible())
        {
            isFlowVolumeLoopUpdated_ = FALSE;
			GuiApp::RaiseGuiEventPriority(GuiApp::GUI_PLOT1_EVENT,
											GuiApp::GUI_HIGHEST_PRIORITY);
        }
		else
		{												// $[TI5]
			SAFE_CLASS_ASSERTION(pressureVolumePlot_.isVisible());

            isPressureVolumeLoopUpdated_ = FALSE;
			GuiApp::RaiseGuiEventPriority(GuiApp::GUI_PLOT1_EVENT,
											GuiApp::GUI_HIGHEST_PRIORITY);
		}
        
	}	 
      											// $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()
//
//@ Interface-Description
// Called by UpperSubScreenArea just after this subscreen is removed from the
// display allowing us to do any necessary cleanup.  Takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// Inform Patient Data that it should stop accumulating waveform data.
// Release the Adjust Panel persistent focus.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::deactivate(void)
{
	CALL_TRACE("WaveformsSubScreen::deactivate(void)");

  if (SerialInterface::IsPrintRequested())
  {
  // $[TI1]
	cancelPrintRequest();
  }
  // $[TI2]
  
	// Deactivate all setting buttons...
	operateOnButtons_(arrDisplayBtnPtrs_, &SettingButton::deactivate);
	operateOnButtons_(arrSetupBtnPtrs_,   &SettingButton::deactivate);

	if (currAuxInfoState_ == EXP_PAUSE_ ||
		currAuxInfoState_ == INSP_PAUSE_ ||
		currAuxInfoState_ == NIF_PAUSE_ ||
		currAuxInfoState_ == P100_PAUSE_ ||
		currAuxInfoState_ == VC_PAUSE_)
	{	// $[TI1]
		// reset back to non-maneuver state for next display...

        updateAuxInfoState_(lastAuxInfoState_);
	}	// $[TI2]

	// Clear persistent focus of Adjust Panel
	AdjustPanel::TakePersistentFocus(NULL);

    // if the waveform screen is freezing during an RM Maneuver,
    // restore the User-Defined waveform settings.
    if(isFreezing_) 
    {
        
        if((isNifWaveformFoisting_ || isVcWaveformFoisting_)) 
        {

            // Do not allow to update the screen due to a setting change.
            disableSettingsUpdate_ = TRUE;

            // Restore User-Defined waveform settings.
            restoreWaveformSettings();


            // Re-enable screen updates due to a setting change.
            disableSettingsUpdate_ = FALSE;
        }

        // Clear the P100 Flag if P100 maneuver was activated.
        if(isP100WaveformFoisting_)
        {
            isP100WaveformFoisting_ = FALSE;
        }

    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
// Called periodically by the GUI-App task loop during normal ventilation.
// Allows this subscreen to react to the latest waveform data accumulated by
// the patient data subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
// We first accumulate all the latest waveform data into 'currentDataBuffer_'.
// If the subscreen is not visible or in the frozen state or not displaying
// waveforms then simply return.   Otherwise let's process the new waveform
// data.
//
// If we haven't already found the first inspiration which triggers the
// beginning of waveform plotting then look for it in the latest interval
// of waveform data.  If we find it then, for time waveforms, jump back a
// half second of data and that's where we start plotting.
//
// If we're in the middle of plotting then we must search for the end of
// the plot.  For time waveforms there is a set number of samples that
// can be displayed, depending on the time scale.  For the pressure-volume
// plot it is the end of the current breath.
//
// We then update the appropriate waveforms with the correct interval of
// data.
//
// $[01150] While the Safety valve is open and the waveform subscreen ...
// $[01153] If two time curves are displayed, they shall be separate and ...
// $[01154] Multiple time curves shall be arranged vertically, ...
// $[01155] The method of tracing the time curve shall follow these ...
// $[01331] When the time scale of a time plot is changed by the user ...
// $[01174] For time curves, freeze shall allow the breath tracing to ...
// $[01175] Tracing shall remain frozen until the user deselects ...
// $[01176] Freeze shall be simultaneously applied to all waveforms ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
WaveformsSubScreen::update(void)
{
	CALL_TRACE("WaveformsSubScreen::update(void)");

	SigmaStatus status;
	Boolean isCurrSampleExhalation;
	Boolean isReset;
	WaveformData waveformData;
	const Uint32 ZERO_ACTIVE = 7;
	const Uint32 PURGE_ACTIVE = 6;

    isVentNotInStartupOrWaitForPT_ = UpperScreen::RUpperScreen.getVitalPatientDataArea()->
                                  isVentNotInStartupOrWaitForPT();
    

	// Store the latest waveform data samples collected by the Patient-Data
	// subsystem.	
	while (SUCCESS == PatientDataMgr::TakeWaveformData(&waveformData))
	{													// $[TI1]
        // $[PX01003] During Autozero or Purge events, set the 
		// clearWaveform_ flag to start the pen-up
		if (isProxEnabled_ && !isProxInop_ && (waveformData.proxManuever != 0 || isProxInStartupMode_)  )
		{
			clearWaveform_ = TRUE;

			proxManueverType_ = waveformData.proxManuever; 

			if (!isProxDisplayTimerOn_ && proxManueverType_ != 0 && !isProxInStartupMode_)
			{
                // Start the prox status timer.
                isProxDisplayTimerOn_ = TRUE;
                GuiTimerRegistrar::StartTimer(GuiTimerId::PROX_STATUS_TIMEOUT);
			}

		}
		else
		{
			// Once the autozero or purge event are not active,
			// consume the rest of the waveform packet and
			// refresh this screen.
			if (clearWaveform_ && !isProxDisplayTimerOn_ && !isProxInStartupMode_ && !isFrozen_ && isOnWaveDisplay_)
			{
				clearWaveform_ = FALSE;	
				while (SUCCESS == PatientDataMgr::TakeWaveformData(&waveformData))
				{
					// consume the rest of the waveform packet
				}

				// Re-enable scale buttons 
				for (Uint8 idx = 0u; arrDisplayBtnPtrs_[idx] != NULL; idx++)
				{
					 if ((arrDisplayBtnPtrs_[idx])->isVisible())
					 {
						 (arrDisplayBtnPtrs_[idx])->setToUp();
					 }
				}

				if (isVisible())
				{
					deactivate();
					activate();

				}

				return(FALSE);
			}
			else
			{
				updateSensorBufferViaWaveform_ ( &waveformData );									
				currentDataBuffer_.addSample(&waveformData);
			}

		}
	}													// $[TI2]

	// If this subscreen is not being displayed or if the subscreen is in
	// the frozen state or on the plot setup screen then we don't need to
	// update any waveform plots so just return!
	if (!isVisible() || isFrozen_ || !isOnWaveDisplay_)
	{													// $[TI3]
		return(FALSE);
	}													// $[TI4]


	// $[PX01003] During Autozero or Purge events the 840 Application 
	// Waveform Subscreen shall do a Pen-Up 
	if (clearWaveform_)
	{

		// Erase the plots once per event.
		if (!isWaveformErased_)
		{
			// Check which waveform plots are being displayed and erase them.
			if (timePlot1_.isVisible())
			{											// $[TI11]
				// Erase time plots
				timePlot1_.erase();
				if (timePlot2_.isVisible())
				{										// $[TI12]
					timePlot2_.erase();
				}										// $[TI13]

				// For time plots we must display the plot beginning a half
				// second before the beginning of the first breath.
				(void)currentSegmentIter_.beginIntervalAtOffset(-500);

				if (!isProxInStartupMode_)
				{	
				    if (proxManueverType_ == ZERO_ACTIVE)
					{
						timePlot1_.displayMessage(MiscStrs::PROX_AUTO_ZERO_STATUS_MSG, TRUE);
					}
					else if (proxManueverType_ == PURGE_ACTIVE)
					{
						timePlot1_.displayMessage(MiscStrs::PROX_PURGE_STATUS_MSG, TRUE);
					}
				}
			}
			else if(flowVolumePlot_.isVisible())
			{
				flowVolumePlot_.erase();

				if (!isProxInStartupMode_)
				{	
					if (proxManueverType_ == ZERO_ACTIVE)
					{
						flowVolumePlot_.displayMessage(MiscStrs::PROX_AUTO_ZERO_STATUS_MSG, TRUE);
					}
					else if (proxManueverType_ == PURGE_ACTIVE)
					{
						flowVolumePlot_.displayMessage(MiscStrs::PROX_PURGE_STATUS_MSG, TRUE);
					}
                }
			}
			else
			{											// $[TI14]
				SAFE_CLASS_ASSERTION(pressureVolumePlot_.isVisible());

				// Erase pressure-volume plot
				pressureVolumePlot_.erase();

				if (!isProxInStartupMode_)
				{	
    				if (proxManueverType_ == ZERO_ACTIVE)
	       			{
		      			pressureVolumePlot_.displayMessage(MiscStrs::PROX_AUTO_ZERO_STATUS_MSG, TRUE);
			     	}
				    else if (proxManueverType_ == PURGE_ACTIVE)
				    {
    					pressureVolumePlot_.displayMessage(MiscStrs::PROX_PURGE_STATUS_MSG, TRUE);
	       			}
                }
			}											// $[TIxx]


			isWaveformErased_ = TRUE;

			AdjustPanel::TakePersistentFocus(NULL);// Release focus      

			// Disable scale buttons during the autozero or 
			// purge event
			for (Uint8 idx = 0u; arrDisplayBtnPtrs_[idx] != NULL; idx++)
			{
				if ((arrDisplayBtnPtrs_[idx])->isVisible())
				{
					(arrDisplayBtnPtrs_[idx])->setToUp();
					(arrDisplayBtnPtrs_[idx])->setToFlat();
				}

			}

		}

		
        if (isVentNotInStartupOrWaitForPT_ && isProxInStartupMode_ && displayProxStartupMsgOnce_)
		{
			if (timePlot1_.isVisible())
			{											
			    timePlot1_.displayMessage(MiscStrs::PROX_STARTUP_SMALL_MSG, TRUE);
			}
			else if (flowVolumePlot_.isVisible())
			{
				flowVolumePlot_.displayMessage(MiscStrs::PROX_STARTUP_SMALL_MSG, TRUE);
			}
			else
			{
				SAFE_CLASS_ASSERTION(pressureVolumePlot_.isVisible());
				pressureVolumePlot_.displayMessage(MiscStrs::PROX_STARTUP_SMALL_MSG, TRUE);
			}

			displayProxStartupMsgOnce_ = FALSE;
		}

		isPlot1Updated_ = TRUE;
		isPlot2Updated_ = TRUE;
		isPressureVolumeLoopUpdated_ = TRUE;
		isFlowVolumeLoopUpdated_ = TRUE;
		isReset = TRUE;
        return(FALSE);
	}
	else
	{
        displayProxStartupMsgOnce_ = TRUE;

		isWaveformErased_ = FALSE;

		// Hide the prox status messages.
		if (timePlot1_.isVisible())
		{            
            timePlot1_.displayMessage(MiscStrs::EMPTY_STRING, FALSE);

		}
		else if(flowVolumePlot_.isVisible())
		{

            flowVolumePlot_.displayMessage(MiscStrs::EMPTY_STRING, FALSE);

		}
		else
		{											
			SAFE_CLASS_ASSERTION(pressureVolumePlot_.isVisible());
            pressureVolumePlot_.displayMessage(MiscStrs::EMPTY_STRING, FALSE);
		}										
	    
	}


	isPlot1Updated_ = TRUE;
	isPlot2Updated_ = TRUE;
	isPressureVolumeLoopUpdated_ = TRUE;
	isFlowVolumeLoopUpdated_ = TRUE;
	
	isReset = FALSE;

	// OK, let's get the newest interval of waveform data, if any
	if (SUCCESS == currentSegmentIter_.setToNextInterval())
	{									// $[TI5]
		// If we haven't found the start of the next waveform yet then go look
		// for it.
		if (!isStartOfWaveformFound_)
		{												// $[TI6]
			const Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);
			const DiscreteValue  CURR_MODE = pSetting->getAcceptedValue();

			if (CURR_MODE == ModeValue::CPAP_MODE_VALUE)
			{
				isStartOfWaveformFound_ = (SUCCESS == currentSegmentIter_.goFirst());
			}
			else
			{
				// A waveform plot always begins when the next new breath occurs
				// so let's look for a transition from exhalation to inspiration.
				for (status = currentSegmentIter_.goFirst();
					SUCCESS == status;
					status = currentSegmentIter_.goNext())
				{
					// Store whether current sample is part of an exhalation.
					isCurrSampleExhalation =
						(currentSegmentIter_.getDiscreteValue(PatientDataId::BREATH_PHASE_ITEM)
						 != BreathPhaseType::INSPIRATION);
	
					// Test for transition from exhalation to inspiration
					// or expiratory pause
					if( (isLastSampleExhalation_ && !isCurrSampleExhalation) ||
						(currAuxInfoState_ == EXP_PAUSE_) ||
						(currAuxInfoState_ == NIF_PAUSE_) ||
						(currAuxInfoState_ == P100_PAUSE_) ||
						(currAuxInfoState_ == VC_PAUSE_) )      
					{										// $[TI7]
						// We found the start of the next breath.  We can
						// begin waveform display now.
						isLastSampleExhalation_ = FALSE;
						isStartOfWaveformFound_ = TRUE;
	
						// Make the current sample the first one in the interval
						// as we're not interested (yet) in samples before this one.
						currentSegmentIter_.syncFirstWithCurrent(currentSegmentIter_);
						break;
					}										// $[TI8]
	
					// Store the value of the current sample and go on to the next.
					isLastSampleExhalation_ = isCurrSampleExhalation;
				}
	
			}
			// Check to see if the search for the start of the next waveform
			// was unsuccessful
			if (!isStartOfWaveformFound_)
			{											// $[TI9]
				// It was so just return.
				return(FALSE);
			}											// $[TI10]

			// Found the start of the waveform so make sure that we're now
			// allowed to freeze!
			freezeButton_.setShow(TRUE);

			// Check which waveform plots are being displayed and erase them.
			if (timePlot1_.isVisible())
			{											// $[TI11]
				// Erase time plots
				timePlot1_.erase();
				if (timePlot2_.isVisible())
				{										// $[TI12]
					timePlot2_.erase();
				}										// $[TI13]

				// For time plots we must display the plot beginning a half
				// second before the beginning of the first breath.
				(void)currentSegmentIter_.beginIntervalAtOffset(-500);
			}
            else if(flowVolumePlot_.isVisible())
            {
                flowVolumePlot_.erase();
            }
			else
			{											// $[TI14]
				SAFE_CLASS_ASSERTION(pressureVolumePlot_.isVisible());

				// Erase pressure-volume plot
				pressureVolumePlot_.erase();
			}											// $[TIxx]

			// Make the current full plot iterator equal the current segment
			// iterator because all we currently have is the first segment
			// of the plot.
			currentFullPlotIter_.sync(currentSegmentIter_);
		}												// $[TI15]

		// Synchronize the end of the current full plot iterator with the
		// end of the current segment, thereby incorporating the latest segment
		// of data into the full plot.
		currentFullPlotIter_.syncLast(currentSegmentIter_);

		//   If the pause button was pressed while the waveforms screen 
		//   was not visible, freeze_() should not execute...
		if ((currAuxInfoState_ == EXP_PAUSE_  ||
			 currAuxInfoState_ == NIF_PAUSE_  ||
			 currAuxInfoState_ == P100_PAUSE_  ||
			 currAuxInfoState_ == VC_PAUSE_  ||
			 currAuxInfoState_ == INSP_PAUSE_)  &&  !isFreezing_)
		{												// $[TI16]
			freeze_();
		}												// $[TI17]

		// Draw the latest segment on the appropriate plots.
		if (timePlot1_.isVisible())
		{												// $[TI18]
			// Check to see if we're at the end (or past the end) of the full
			// plot.  For time waveforms the end is determined by having enough
			// samples to fill the currently selected time scale.  Check to see
			// if the time span of the current full plot is greater or equal to
			// the current time scale.
			if (currentFullPlotIter_.getLength() >= lengthOfTimeWaveform_)
			{											// $[TI19]
				// We've got a full waveform so we must now go back to looking
				// for the start of the next waveform!
				isStartOfWaveformFound_ = FALSE;

				// The current full plot may have too many samples.  Make sure
				// it has exactly the right amound.
				currentFullPlotIter_.goFirst();
				currentFullPlotIter_.endIntervalAtOffset(lengthOfTimeWaveform_);

				// Make sure the current segment ends at the same point as
				// the full plot so that we don't end up drawing too big
				// an end segment.
				currentSegmentIter_.syncLast(currentFullPlotIter_);

				// Freeze the current data in case user wants to freeze.
				frozenDataBuffer_ = currentDataBuffer_;
			}											// $[TI20]

			// Draw the latest segment of data.
			timePlot1_.update(currentSegmentIter_);
			if (timePlot2_.isVisible())
			{											// $[TI21]
				timePlot2_.update(currentSegmentIter_);
			}											// $[TI22]
		}
        else if(flowVolumePlot_.isVisible())
        {
			// A flow-volume plot is being displayed.  First let's look
			// for the end of the plot by searching for a transition between
			// exhalation and inspiration.
			// or if we have entered expiratory pause
			Boolean isCurrSampleExpiratoryPause = FALSE;


			for (status = currentSegmentIter_.goFirst();
				SUCCESS == status;
				status = currentSegmentIter_.goNext())
			{
				// Store whether current sample is part of an exhalation.
				isCurrSampleExhalation =
				    currentSegmentIter_.
				    getDiscreteValue(PatientDataId::BREATH_PHASE_ITEM)
				    != BreathPhaseType::INSPIRATION;

				// Test for transition from exhalation to inspiration
				// or expiratory pause
				if(isLastSampleExhalation_ && !isCurrSampleExhalation)
				{										// $[TI24]
					// Found beginning of an exhalation so we're at the end
					// of the loop plot. We've got a full waveform.
					isStartOfWaveformFound_ = FALSE;
					isLastSampleExhalation_ = FALSE;

					// Ah ha.  The current sample is one beyond the end of
					// breath.  We shall end the full plot at one sample
					// before the current point.
					currentFullPlotIter_.
						syncLastWithCurrent(currentSegmentIter_);
					currentFullPlotIter_.goLast();
					currentFullPlotIter_.goPrev();
					currentFullPlotIter_.
					    syncLastWithCurrent(currentFullPlotIter_);

					// Make the end of the current segment the same as the
					// full plot's one (we have to have advanced past the
					// first sample in the current segment to do this).
					if (currentSegmentIter_.goPrev())
					{									// $[TI25]
						currentSegmentIter_.syncLast(currentFullPlotIter_);
					}
					else
					{									// $[TI26]
						// $[01165] The loop shall be plotted every other breath ...
						// Current segment is unusable so don't display it.
						isReset = TRUE;
					}

					// Store the current data in the frozen buffer in case
					// the user wants to freeze before the beginning of the
					// next trace.
					frozenDataBuffer_ = currentDataBuffer_;
					break;
				}										// $[TI27]

				// Store the value of the current sample and go on to the next.
				isLastSampleExhalation_ = isCurrSampleExhalation;
				isLastSampleExpiratoryPause_ = isCurrSampleExpiratoryPause;
			}

			// Update the plot with the latest segment of data.
			if(!isReset )
			{											// $[TI28]
			   flowVolumePlot_.update(currentSegmentIter_);
			}											// $[TI29]
        }
		else
		{												// $[TI23]
			// A pressure-volume plot is being displayed.  First let's look
			// for the end of the plot by searching for a transition between
			// exhalation and inspiration.
			// or if we have entered expiratory pause
			Boolean isCurrSampleExpiratoryPause = FALSE;


			for (status = currentSegmentIter_.goFirst();
				SUCCESS == status;
				status = currentSegmentIter_.goNext())
			{
				// Store whether current sample is part of an exhalation.
				isCurrSampleExhalation =
				    currentSegmentIter_.
				    getDiscreteValue(PatientDataId::BREATH_PHASE_ITEM)
				    != BreathPhaseType::INSPIRATION;

				// Test for transition from exhalation to inspiration
				// or expiratory pause
				if(isLastSampleExhalation_ && !isCurrSampleExhalation)
				{										// $[TI24]
					// Found beginning of an inspiration so we're at the end
					// of the loop plot. We've got a full waveform.
					isStartOfWaveformFound_ = FALSE;
					isLastSampleExhalation_ = FALSE;

					// Ah ha.  The current sample is one beyond the end of
					// breath.  We shall end the full plot at one sample
					// before the current point.
					currentFullPlotIter_.
						syncLastWithCurrent(currentSegmentIter_);
					currentFullPlotIter_.goLast();
					currentFullPlotIter_.goPrev();
					currentFullPlotIter_.
					    syncLastWithCurrent(currentFullPlotIter_);

					// Make the end of the current segment the same as the
					// full plot's one (we have to have advanced past the
					// first sample in the current segment to do this).
					if (currentSegmentIter_.goPrev())
					{									// $[TI25]
						currentSegmentIter_.syncLast(currentFullPlotIter_);
					}
					else
					{									// $[TI26]
						// $[01165] The loop shall be plotted every other breath ...
						// Current segment is unusable so don't display it.
						isReset = TRUE;
					}

					// Store the current data in the frozen buffer in case
					// the user wants to freeze before the beginning of the
					// next trace.
					frozenDataBuffer_ = currentDataBuffer_;
					break;
				}										// $[TI27]

				// Store the value of the current sample and go on to the next.
				isLastSampleExhalation_ = isCurrSampleExhalation;
				isLastSampleExpiratoryPause_ = isCurrSampleExpiratoryPause;
			}

			// Update the plot with the latest segment of data.
			if(!isReset )
			{											// $[TI28]
			   pressureVolumePlot_.update(currentSegmentIter_);
			}											// $[TI29]
		}

		// Check to see if we've just discovered the end of the current plot.
		if (!isStartOfWaveformFound_)
		{												// $[TI30]
			// We hit the end of the plot(s) so inform each displayed plot
			// of that so that they can do tidy up.
			if (timePlot1_.isVisible())
			{											// $[TI31]
				timePlot1_.endPlot();
				if (timePlot2_.isVisible())
				{										// $[TI32]
					timePlot2_.endPlot();
				}										// $[TI33]
			}
            else if(flowVolumePlot_.isVisible())
            {
                flowVolumePlot_.endPlot();
            }
			else
			{											// $[TI34]
				SAFE_CLASS_ASSERTION(pressureVolumePlot_.isVisible());
				pressureVolumePlot_.endPlot();
			}

			// We're at the end so if the user was requesting a freeze then
			// freeze!
			if( isFreezing_ )
			{											// $[TI35]
				freeze_();
			}											// $[TI36]
		}												// $[TI37]
	}
	else
	{													// $[TI38]
		// No new segment of data was available.  Make sure current segment
		// is up to date with the latest data in the current data buffer.
		currentSegmentIter_.beginInterval();
		currentSegmentIter_.endInterval();
	}

	return(TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePlot
//
//@ Interface-Description
// Takes care of redrawing the time plots.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call redrawPlot_ to do the job.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
WaveformsSubScreen::updatePlot(void)
{
	CALL_TRACE("WaveformsSubScreen::updatePlot(void)");


	// If this subscreen is not being displayed then we don't need to
	// update the second plots so just return!
	if (!isVisible())
	{													// $[TI1]
		return(FALSE);
	}													// $[TI2]

	if (!isPlot1Updated_)
	{													// $[TI3]
		isPlot1Updated_ = TRUE;
		redrawPlot_(timePlot1_);
		return(TRUE);		
	}
	else if (!isPlot2Updated_)
	{													// $[TI4]
		isPlot2Updated_ = TRUE;
		redrawPlot_(timePlot2_);
		return(TRUE);		
	}
    else if (!isFlowVolumeLoopUpdated_)
    {
        isFlowVolumeLoopUpdated_ = TRUE;
        redrawPlot_(flowVolumePlot_);
        return(TRUE);
    }
    else if (!isPressureVolumeLoopUpdated_)
    {
        isPressureVolumeLoopUpdated_ = TRUE;
        redrawPlot_(pressureVolumePlot_);
        return(TRUE);
    }

	else
	{													// $[TI5]
		return(FALSE);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: pauseRequested
//
//@ Interface-Description
// Called when the user presses the Expiratory or Inspiratory Pause key.
// We make sure the Waveforms subscreen is displayed and is displaying
// waveforms and that the screen is unfrozen.
//---------------------------------------------------------------------
//@ Implementation-Description
// If we're on the plot setup screen then go to the waveform subscreen.
// If freezing or in the frozen state then unfreeze the waveforms.
//---------------------------------------------------------------------
//@ PreCondition
// We must have previously been displayed.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::pauseRequested(void)
{
	CALL_TRACE("pauseRequested()");

	// If freezing or frozen then unfreeze.
	if (isFreezing_ || isFrozen_)
	{													// $[TI5]
		unFreeze_();
	}													// $[TI6]

	// If this subscreen is not visible then we must activate ourselves
	if (!isVisible())
	{													// $[TI1]
	    // Activate this subscreen
	    getSubScreenArea()->activateSubScreen(this,
	       UpperScreen::RUpperScreen.getUpperScreenSelectArea()->
												   getWaveformsTabButton());
	}													// $[TI4]

	// If we are displaying the plot setup screen then go to the waveforms
	// screen.
	if (!isOnWaveDisplay_)
	{													// $[TI7]
		AdjustPanel::TakePersistentFocus(NULL);		// Release focus
		isOnWaveDisplay_ = TRUE;					// Go to the wave display
		layoutScreen_();							// Layout that screen
	}													// $[TI8]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: pauseCancelByAlarm
//
//@ Interface-Description
//  Called when we want to cancel waveform maneuver.  
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::pauseCancelByAlarm(void)
{
	CALL_TRACE("pauseCancelByAlarm()");
	if (isVisible())
	{								// $[TI1]
      addAuxInfo_(arrAlarmCancelPtrs_);

        // Pauses are only cancelled when active.
        if ( isInspiratoryPauseState_ )
        { 
            isPlateauPressureDisplayed_= FALSE;

            removeAuxInfo_(arrInspPauseResistancePtrs_);
            removeAuxInfo_(arrInspPauseCompliancePtrs_);
            removeAuxInfo_(arrInspPausePlateauPtrs_);
            
            addDrawable(&pauseCancelByAlarmText_);

            // transition to last state...
            updateAuxInfoState_(INSP_PAUSE_);
        }					 			// $[TI2]
        else if ( isExpiratoryPauseState_ ) 
        {
            removeAuxInfo_(arrExpPausePtrs_);
 
            addDrawable(&pauseCancelByAlarmText_);
            
            // transition to last state...        
            updateAuxInfoState_(EXP_PAUSE_);
        }
        else if ( isNifPauseState_)
        {

            removeAuxInfo_(arrNifPausePtrs_);
 
            addDrawable(&pauseCancelByAlarmText_);
            
            // transition to last state...        
            updateAuxInfoState_(NIF_PAUSE_);
        }
        else if ( isP100PauseState_)
        {
            removeAuxInfo_(arrP100PausePtrs_);
 
            addDrawable(&pauseCancelByAlarmText_);
            
            // transition to last state...        
            updateAuxInfoState_(P100_PAUSE_);
        }
        else if ( isSvcPauseState_)
        {
            removeAuxInfo_(arrSvcPausePtrs_);
 
            addDrawable(&pauseCancelByAlarmText_);
            
            // transition to last state...        
            updateAuxInfoState_(VC_PAUSE_);
        }
        else
        {
			// do nothing
		}
        
    }// $[T45]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: pauseActive
//
//@ Interface-Description
//@ Interface-Description
// Called when an Expiratory or Inspit`ratory Pause Active message is
// received from the Breath-Delivery system.  For expiratory pause: looks
// after displaying Intrinsic and Total PEEP as well as freezing the current
// waveform.  For inspiratory pause: looks after displaying Compliance, 
// Resistance, and Plateau pressure as well as freezing the current waveform.
// Does nothing if the plot setup screen is currently being displayed.
// For Nif maneuver: looks after displaying NIF and freezing the 
// current waveform.  For P100 maneuver: looks after displaying P100
// and freezing the current waveform.  For VC maneuver: looks after
// displaying VC and freezing the current waveform.
// $[RM12102] $[RM12202] $[RM12302] 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::pauseActive(BreathPhaseType::PhaseType phaseType)
{
	CALL_TRACE("pauseActive(phaseType)");
	// remove "cancel" message...
	removeDrawable(&pauseCancelByAlarmText_);
 
	if (isOnWaveDisplay_)
	{													// $[TI1]
		AdjustPanel::TakePersistentFocus(NULL);		// Release focus

		// Freeze the current waveform if not already doing so (as long as
		// waveforms are currently being plotted).
		if ((!isFrozen_ || !isFreezing_) && freezeButton_.isVisible())
		{												// $[TI2]
			// $[BL01006] freeze the waveform
			freeze_();
		}												// $[TI3]

		staticResistanceStatusValue_ = PauseTypes::VALID;
		staticComplianceStatusValue_ = PauseTypes::VALID;

        if (phaseType == BreathPhaseType::EXPIRATORY_PAUSE)
        {                                               // $[TI4]
    		// Display Intrinsic and Total PEEP
	    	// $[01358] While the expiratory pause is in progress, the total ...
            isExpiratoryPauseState_= TRUE;
            updateAuxInfoState_(EXP_PAUSE_);
    	}
        else if (phaseType == BreathPhaseType::INSPIRATORY_PAUSE)
        {                                               // $[TI5]
			
            if (!isInspiratoryPauseState_) 
            {
                // Display static lung compliance and static airway resistance                 
                isInspiratoryPauseState_=    TRUE;
                areResistanceDisplayed_=     TRUE;
                areComplianceDisplayed_=     TRUE;
                isPlateauPressureDisplayed_= TRUE;
            }
			updateAuxInfoState_(INSP_PAUSE_);
    	}
        else if (phaseType == BreathPhaseType::NIF_PAUSE)
        {
    		// Display NIF Pressure 
	    	// $[RM12058] While the NIF pause is in progress, the NIF pressure is displayed...
            isNifPauseState_= TRUE;
            updateAuxInfoState_(NIF_PAUSE_);
    	}
        else if (phaseType == BreathPhaseType::P100_PAUSE)
        {
    		// Display P100 Pressure 
	    	// $[RM12061] While the P100 pause is in progress, the P100 pressure is displayed...
            isP100PauseState_= TRUE;
            updateAuxInfoState_(P100_PAUSE_);
    	}
        else if (phaseType == BreathPhaseType::VITAL_CAPACITY_INSP)
        {
    		// Display VC
	    	// $[RM12065] While the VC pause is in progress...
            isSvcPauseState_= TRUE;
            updateAuxInfoState_(VC_PAUSE_);
    	}
	    else
	    {                                               // $[TI6]
   			AUX_CLASS_ASSERTION_FAILURE(phaseType);
	    }
	}													// $[TI7]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: batchSettingUpdate
//
//@ Interface-Description
//  Called when a setting value is changed.  Passed a qualifierId,
//  a pointer to the ContextSubject and a settingId.
//---------------------------------------------------------------------
//@ Implementation-Description
//	We are only interested in values that are ACCEPTED.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::batchSettingUpdate(
							const Notification::ChangeQualifier qualifierId,
							const ContextSubject* 				pSubject,
							const SettingId::SettingIdType      settingId
									  )
{
	CALL_TRACE("batchSettingUpdate(qualifierId, pSubject, settingId)");

	if (qualifierId == Notification::ACCEPTED)
	{  // $[TI1] -- this is a change to the setting's accepted value...
		const Boolean  IS_FULL_BATCH_CHANGE =
									(settingId == SettingId::NUM_BATCH_IDS);

		if (settingId == SettingId::PROX_ENABLED)
		{

			const ContextSubject *const  P_ACCEPTED_SUBJECT =
									getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

			const DiscreteValue PROX_ENABLED_VALUE =
						pSubject->getSettingValue(settingId);

			isProxEnabled_ = (PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED);

            // Clear the waveform screen when prox is disabled.           
            if ( !isProxEnabled_ || (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_))
            {           
                deactivate();
                activate();
            }

			return;
		}

		if (IS_FULL_BATCH_CHANGE)
		{  // $[TI1.1] -- this is a change to the baseline pressure...
			nonBatchSettingUpdate(qualifierId, pSubject, settingId);
        }
	    else if (settingId == SettingId::COM1_CONFIG)
	    {  // $[TI1.2]
    		// com1 device setting has changed -- if a print job is not in
	    	// progress then blank the PRINT button.
		    // It is the responsibility of the GUI serial task to
		    // subsequently send a PRINTER_ASSIGNMENT_EVENT message
    		// if it is appropriate to display the PRINT button.
    		// If a print job is in progress, make sure NOT to blank the
    		// PRINT button -- actually a CANCEL button at this point --
    		// since we want to be able to cancel the print job.
    		if (!SerialInterface::IsPrintRequested())
           	{  // $[TI1.2.1]
      		  printButton_.setShow(FALSE);
	    	}  // $[TI1.2.2]
        }
        else
        {}  // $[TI1.3]

		if (IS_FULL_BATCH_CHANGE  ||
			settingId == SettingId::MODE  ||
			settingId == SettingId::SUPPORT_TYPE  ||
			settingId == SettingId::MAND_TYPE)
		{  // $[TI1.3] -- need to check PAV's applicability...
			const Boolean  IS_SPONT_TYPE_APPLIC =
				(SettingContextHandle::GetSettingApplicability(ContextId::ACCEPTED_CONTEXT_ID,
						SettingId::SUPPORT_TYPE  ) != Applicability::INAPPLICABLE);

			const DiscreteValue  SPONT_TYPE_VALUE =
				(GuiApp::IsVentStartupSequenceComplete())
				 // $[TI1.3.4] (TRUE)  $[TI1.3.5] (FALSE)...
				 ? pSubject->getSettingValue(SettingId::SUPPORT_TYPE)
				 : SafetyPcvSettingValues::GetValue(SettingId::SUPPORT_TYPE);
                 
                 
			// PAV is active if spont type is applicable, AND spont type's value
			// is 'PA'...
			const Boolean  IS_PAV_ACTIVE =
					 (IS_SPONT_TYPE_APPLIC  &&
					  SPONT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE);

			// VS is active if spont type is applicable, AND spont type's value
			// is 'VS'...
			const  Boolean IS_VS_ACTIVE = ( IS_SPONT_TYPE_APPLIC  &&
				  DiscreteValue(pSubject->getSettingValue(SettingId::SUPPORT_TYPE)) ==
					  		 SupportTypeValue::VSV_SUPPORT_TYPE);			   

			const  Boolean IS_VCP_ACTIVE = ( DiscreteValue(pSubject->getSettingValue(SettingId::MAND_TYPE)) ==
					  		 MandTypeValue::VCP_MAND_TYPE);			   
			
            // Define the desired state to go to next
			AuxInfoState_ nextAuxInfoState;
			if (IS_PAV_ACTIVE)
			{
				nextAuxInfoState = PAV_ACTIVE_;
			}
			else if (IS_VS_ACTIVE)
			{
				nextAuxInfoState = VS_ACTIVE_;
			}
			else if (IS_VCP_ACTIVE)
			{
				nextAuxInfoState = VCP_ACTIVE_;
			}
			else if (currAuxInfoState_ == EXP_PAUSE_ || 
					 currAuxInfoState_ == INSP_PAUSE_ ||
					 currAuxInfoState_ == NIF_PAUSE_ ||
					 currAuxInfoState_ == P100_PAUSE_ ||
					 currAuxInfoState_ == VC_PAUSE_ )
			{
				nextAuxInfoState = currAuxInfoState_;
			}
			else
			{
				nextAuxInfoState = STANDARD_;
			}

			if (IS_PAV_ACTIVE)
			{
				nextAuxInfoState = PAV_ACTIVE_;
			}
			else if (IS_VS_ACTIVE)
			{
				nextAuxInfoState = VS_ACTIVE_;
			}
			else if (IS_VCP_ACTIVE)
			{
				nextAuxInfoState = VCP_ACTIVE_;
			}
            else if (currAuxInfoState_ == EXP_PAUSE_ || 
					 currAuxInfoState_ == INSP_PAUSE_ ||
					 currAuxInfoState_ == NIF_PAUSE_ ||
					 currAuxInfoState_ == P100_PAUSE_ ||
					 currAuxInfoState_ == VC_PAUSE_ )
			{
				nextAuxInfoState = currAuxInfoState_;
			}
			else
			{
				nextAuxInfoState = STANDARD_;
			}

            
            // Getting ready to switch from the current state to 
            // next state
            switch (currAuxInfoState_)
            {
            case STANDARD_:{
                switch (nextAuxInfoState)
                {
                case VCP_ACTIVE_:
                case VS_ACTIVE_:
                case PAV_ACTIVE_:{
                    // A SPONT mode is newly-active...
                    updateAuxInfoState_(nextAuxInfoState);
                	}break;
                
                case STANDARD_:
                case EXP_PAUSE_:
                case INSP_PAUSE_:
                default:{ // Do nothing
                	}break;
                }// end switch (nextAuxInfoState) 			 
            	}break;// end STANDARD_ switch (nextAuxInfoState)
            //------------------------------------------------
            case EXP_PAUSE_: // $[TI1.3.2]
            case NIF_PAUSE_:
            case P100_PAUSE_:
            case VC_PAUSE_:
            case INSP_PAUSE_: {
                switch (nextAuxInfoState)
                {
                case VCP_ACTIVE_:
                case VS_ACTIVE_:
                case PAV_ACTIVE_:{
                // This maybe a little confusing.  Pause functions are 
                // handled differently in the function updateAuxInfoState().
                // It assigns/restores the lastAuxInfoState to currentState
                // when Pause is done.  This is done because Pause is considered
                // a temporary operation and will always return to it's 
                // prior state.
                
                    // goto original SPONT mode when pause is done...
                    lastAuxInfoState_ = nextAuxInfoState;
                	}break;
                
                case STANDARD_:
                case EXP_PAUSE_:
                case NIF_PAUSE_:
                case P100_PAUSE_:
                case VC_PAUSE_:
                case INSP_PAUSE_:
                default:{
                	 // go to STANDARD when pause is done...
                     lastAuxInfoState_ = STANDARD_;
                	}break;
                }// end switch (nextAuxInfoState) 			 
            	}break; // end EXP_PAUSE_,INSP_PAUSE_ switch (nextAuxInfoState)
            //------------------------------------------------
            case VS_ACTIVE_:
            case VCP_ACTIVE_:
            case PAV_ACTIVE_:{
                // Based on the piror implementation that any state
                // change should switch to STANDARD_ first.
                
                // Current SPONT mode is no longer active...
                if(currAuxInfoState_ != nextAuxInfoState)
                {
                   updateAuxInfoState_(STANDARD_);
                }     
                switch (nextAuxInfoState)
                {
                case VCP_ACTIVE_:
                case VS_ACTIVE_:
                case PAV_ACTIVE_:{
                     // go to next SPONT mode
                     updateAuxInfoState_(nextAuxInfoState);
                	}break;
                
                case STANDARD_:
                case EXP_PAUSE_:
                case NIF_PAUSE_:
                case P100_PAUSE_:
                case VC_PAUSE_:
                case INSP_PAUSE_:
                default:{ // Do nothing
                	}break;
                }// end switch (nextAuxInfoState)			 
            	}break; // end VCP_ACTIVE_,VS_ACTIVE_,PAV_ACTIVE_ switch (nextAuxInfoState)
            //------------------------------------------------
            default:{
				// unexpected info state...
				AUX_CLASS_ASSERTION_FAILURE(currAuxInfoState_);
            	}break;
            } // end switch (currAuxInfoState_)
		}  // $[TI1.4] -- only interested in changes to mode or spont type...
	}  // $[TI2] -- only interested in accepted value changes...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nonBatchSettingUpdate
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
// We first check which setting changed.  If it's the Plot 1 Type then simply
// set the visibility of the plot 2 setting button according to the plot 1
// type.
//
// If a scale setting changed then we first have to decode from the particular
// discrete enum value of the setting what the upper and lower numerical limits
// of the new scale are.  These new scales are passed on to the appropriate
// plot(s).
//
// If the pressure baseline value changed then we pass the new value straight
// to the Y-axis position of the Pressure-Volume plot.
//
// If a waveform plot is adjusted then we redraw it.
// $[NE01013] Upon acceptance of a change to patient circuit type, ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::nonBatchSettingUpdate(
							const Notification::ChangeQualifier qualifierId,
							const ContextSubject* 				pSubject,
							const SettingId::SettingIdType      settingId
										 )
{
	CALL_TRACE("nonBatchSettingUpdate(qualifierId, pSubject, settingId)");

    // Disable settings update which allows to change settings without
    // updating the waveform.
    if(disableSettingsUpdate_)
    {
        return;
    }

	Int32 upperLimit;					// Lower limit of scale
	Int32 lowerLimit;					// Upper limit of scale
	Int32 division;						// How often grid lines should be
										// drawn between lower and upper limits
	const ContextSubject *const  P_ACCEPTED_SUBJECT =
							getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

	const DiscreteValue PROX_ENABLED_VALUE =
				P_ACCEPTED_SUBJECT->getSettingValue(SettingId::PROX_ENABLED);

	isProxEnabled_ = (PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED);


	// Check which setting changed
	switch (settingId)
	{
	case SettingId::PRESS_VOL_LOOP_BASE:				// $[TI9]
		{
			const BoundedValue  NEW_BASELINE =
										pSubject->getSettingValue(settingId);

			// Set the new Y axis position on the Pressure-Volume plot to
			// be the new baseline value.
			pressureVolumePlot_.setYAxisValue(NEW_BASELINE.value);
		}
		break;

	case SettingId::TIME_PLOT_SCALE:					// $[TI10]
		{
			const DiscreteValue  NEW_SCALE_VALUE =
										pSubject->getSettingValue(settingId);

			// Time scale
			lowerLimit = 0;			// Lower time limit always 0.

			// Decode enumerated value
			switch (NEW_SCALE_VALUE)
			{
			case TimePlotScaleValue::FROM_0_TO_3_SECONDS:	// $[TI11]
				upperLimit = 3;
				division = 1;
				break;

			case TimePlotScaleValue::FROM_0_TO_6_SECONDS:	// $[TI12]
				upperLimit = 6;
				division = 1;
				break;

			case TimePlotScaleValue::FROM_0_TO_12_SECONDS:	// $[TI13]
				upperLimit = 12;
				division = 2;
				break;

			case TimePlotScaleValue::FROM_0_TO_24_SECONDS:	// $[TI14]
				upperLimit = 24;
				division = 4;
				break;

			// 30 second waveform enabled in TimePlotScaleSetting for a NIF waveform only
			case TimePlotScaleValue::FROM_0_TO_30_SECONDS:
				upperLimit = 30;
				division = 5;
				break;

			case TimePlotScaleValue::FROM_0_TO_48_SECONDS:	// $[TI15]
				upperLimit = 48;
				division = 8;
				break;

			default:
				AUX_CLASS_ASSERTION_FAILURE(NEW_SCALE_VALUE);
				break;
			}
		}
		break;

	case SettingId::PRESSURE_PLOT_SCALE:				// $[TI16]
		// Pressure scale
		{
			const DiscreteValue  NEW_SCALE_VALUE =
					pSubject->getSettingValue(SettingId::PRESSURE_PLOT_SCALE);

			lowerLimit = -20;			// Lower pressure limit always -20

			// Decode enumerated value
			switch (NEW_SCALE_VALUE)
			{
			case PressPlotScaleValue::MINUS_2_TO_10_CMH2O:
				upperLimit = 10;						// $[01160]
				lowerLimit = -2;
				division = 1;
				break;

			case PressPlotScaleValue::MINUS_2_TO_16_CMH2O:
				upperLimit = 16;						// $[01160]
				lowerLimit = -2;
				division = 2;
				break;

			case PressPlotScaleValue::MINUS_2_TO_20_CMH2O:
				upperLimit = 20;						// $[01160]
				lowerLimit = -2;
				division = 2;
				break;

			case PressPlotScaleValue::MINUS_5_TO_30_CMH2O:
				upperLimit = 30;
				lowerLimit = -5;						// $[01160]
				division = 5;
				break;

			case PressPlotScaleValue::MINUS_20_TO_20_CMH2O:
				upperLimit = 20;						// $[TI18]
				division = 5;
				break;

			case PressPlotScaleValue::MINUS_20_TO_40_CMH2O:
				upperLimit = 40;						// $[TI19]
				division = 20;
				break;

			case PressPlotScaleValue::MINUS_20_TO_60_CMH2O:
				upperLimit = 60;						// $[TI20]
				division = 20;
				break;

			case PressPlotScaleValue::MINUS_20_TO_80_CMH2O:
				upperLimit = 80;						// $[TI21]
				division = 20;
				break;

			case PressPlotScaleValue::MINUS_20_TO_100_CMH2O:
				upperLimit = 100;						// $[TI22]
				division = 20;
				break;

			case PressPlotScaleValue::MINUS_20_TO_120_CMH2O:
				upperLimit = 120;						// $[TI23]
				division = 20;
				break;
            case PressPlotScaleValue::MINUS_60_TO_20_CMH2O:
                upperLimit = 20;						// $[TI23]
                division = 10;
                lowerLimit = -60;
                break;

			default:
				AUX_CLASS_ASSERTION_FAILURE(NEW_SCALE_VALUE);
				break;
			}
		}
		break;

	case SettingId::VOLUME_PLOT_SCALE:					// $[TI24]
		{
			const DiscreteValue  NEW_SCALE_VALUE =
										pSubject->getSettingValue(settingId);

			// Decode enumerated value
			switch (NEW_SCALE_VALUE)
			{
			case VolumePlotScaleValue::MINUS_1_TO_2_ML:	// $[01161]
				upperLimit = 2;
				lowerLimit = -1;
				division   = 1;
				break;

			case VolumePlotScaleValue::MINUS_1_TO_4_ML:	// $[01161]
				upperLimit = 4;
				lowerLimit = -1;
				division   = 1;
				break;

			case VolumePlotScaleValue::MINUS_2_TO_6_ML:	// $[01161]
				upperLimit = 6;
				lowerLimit = -2;
				division   = 2;
				break;

			case VolumePlotScaleValue::MINUS_2_TO_10_ML:	// $[01161]
				upperLimit = 10;
				lowerLimit = -2;
				division   = 2;
				break;

            case VolumePlotScaleValue::MINUS_5_TO_10_ML:	// $[01161]
				upperLimit = 10;
				lowerLimit = -5;
				division   = 5;
				break;

			case VolumePlotScaleValue::MINUS_5_TO_25_ML:	// $[TI62]
				upperLimit = 25;
				lowerLimit = -5;
				division   = 5;
				break;

			case VolumePlotScaleValue::MINUS_5_TO_50_ML:	// $[TI61]
				upperLimit = 50;
				lowerLimit = -5;
				division   = 5;
				break;

			case VolumePlotScaleValue::MINUS_10_TO_100_ML:	// $[TI25]
				upperLimit = 100;
				lowerLimit = -10;
				division   = 10;
				break;

			case VolumePlotScaleValue::MINUS_20_TO_200_ML:	// $[TI26]
				upperLimit = 200;
				lowerLimit = -20;
				division   = 20;
				break;

			case VolumePlotScaleValue::MINUS_50_TO_500_ML:	// $[TI27]
				upperLimit = 500;
				lowerLimit = -50;
				division   = 50;
				break;

			case VolumePlotScaleValue::MINUS_100_TO_1000_ML:// $[TI28]
				upperLimit = 1000;
				lowerLimit = -100;
				division   = 100;
				break;

			case VolumePlotScaleValue::MINUS_200_TO_2000_ML:// $[TI29]
				upperLimit = 2000;
				lowerLimit = -200;
				division   = 200;
				break;

			case VolumePlotScaleValue::MINUS_300_TO_3000_ML:// $[TI30]
				upperLimit = 3000;
				lowerLimit = -300;
				division   = 300;
				break;
            case VolumePlotScaleValue::MINUS_2000_TO_6000_ML:// $[TI30]
                upperLimit = 6000;
                lowerLimit = -2000;
                division   = 1000;
                break;

			default:
				AUX_CLASS_ASSERTION_FAILURE(NEW_SCALE_VALUE);
				break;
			}
		}
		break;

	case SettingId::FLOW_PLOT_SCALE:					// $[TI31]
		{
			const DiscreteValue  NEW_SCALE_VALUE =
										pSubject->getSettingValue(settingId);

			// Decode enumerated value
			switch (NEW_SCALE_VALUE)
			{
			case FlowPlotScaleValue::MINUS_2_TO_2_LPM:	// $[TI38]
				upperLimit = 2;
				lowerLimit = -2;
				division   = 1;
				break;

			case FlowPlotScaleValue::MINUS_5_TO_5_LPM:	// $[TI38]
				upperLimit = 5;
				lowerLimit = -5;
				division   = 1;
				break;

			case FlowPlotScaleValue::MINUS_10_TO_10_LPM:	// $[TI38]
				upperLimit = 10;
				lowerLimit = -10;
				division   = 2;
				break;

			case FlowPlotScaleValue::MINUS_20_TO_20_LPM:	// $[TI32]
				upperLimit = 20;
				lowerLimit = -20;
				division   = 5;
				break;

			case FlowPlotScaleValue::MINUS_40_TO_40_LPM:	// $[TI33]
				upperLimit = 40;
				lowerLimit = -40;
				division   = 10;
				break;

			case FlowPlotScaleValue::MINUS_80_TO_80_LPM:	// $[TI34]
				upperLimit = 80;
				lowerLimit = -80;
				division   = 20;
				break;

			case FlowPlotScaleValue::MINUS_120_TO_120_LPM:	// $[TI35]
				upperLimit = 120;
				lowerLimit = -120;
				division   = 30;
				break;

			case FlowPlotScaleValue::MINUS_160_TO_160_LPM:	// $[TI36]
				upperLimit = 160;
				lowerLimit = -160;
				division   = 40;
				break;

			case FlowPlotScaleValue::MINUS_200_TO_200_LPM:	// $[TI37]
				upperLimit = 200;
				lowerLimit = -200;
				division   = 50;
				break;

			default:
				AUX_CLASS_ASSERTION_FAILURE(NEW_SCALE_VALUE);
				break;
			}
		}
		break;

	case SettingId::PLOT2_TYPE :							// $[TI99]
		if (isOnWaveDisplay_)
		{	// $[TI99.1]
			// internal change to Plot #2 type, while on the waveforms screen
			// should re-activate/refresh the screen...
			deactivate();
			activate();
		}	// $[TI99.2]
		return;

	default :											// $[TI65]
		// not interested in changes to this setting...
		return;
	}

	// Now let's take care of adjusting and redrawing the plots depending
	// on which setting changed.

	const DiscreteValue  PLOT1_TYPE_VALUE =
							pSubject->getSettingValue(SettingId::PLOT1_TYPE);
	const DiscreteValue  PLOT2_TYPE_VALUE =
							pSubject->getSettingValue(SettingId::PLOT2_TYPE);


	// We have to do slightly different stuff if we're adjusting a loop
	// plot.  Check whether the Flow-Volume plot is currently selected.	
	if (PLOT1_TYPE_VALUE == Plot1TypeValue::FLOW_VS_VOLUME)
	{		 

		// $[PX01002] Add the prox symbol when PROX is enabled.
		if (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_)
		{
			flowVolumePlot_.setXUnitsString(MiscStrs::PROXIMAL_VOLUME_PLOT_UNITS,
											 PV_X_UNITS_X_, PV_X_UNITS_Y_);
	
			flowVolumePlot_.setYUnitsString(MiscStrs::PROXIMAL_BIG_FLOW_PLOT_UNITS,
											PV_Y_UNITS_X_, PV_Y_UNITS_Y_);

		}
		else
		{
			
			flowVolumePlot_.setXUnitsString(MiscStrs::VOLUME_PLOT_UNITS,
											 PV_X_UNITS_X_, PV_X_UNITS_Y_);
	
			flowVolumePlot_.setYUnitsString(MiscStrs::BIG_FLOW_PLOT_UNITS,
											PV_Y_UNITS_X_, PV_Y_UNITS_Y_);
		}

		// Check whether the flow or volume scales have changed or else
		// the flow baseline value.  
		if ((settingId == SettingId::FLOW_PLOT_SCALE)
			|| (settingId == SettingId::VOLUME_PLOT_SCALE))
		{												// $[TI39]
			// If the pressure scale changed then we should adjust the
			// X scale.
			if ((settingId == SettingId::FLOW_PLOT_SCALE))
			{											// $[TI40]
				flowVolumePlot_.setYLimits(upperLimit, lowerLimit, division,
                                           TRUE);
			}
			// If the volume scale changed then we should adjust the
			// Y scale.
			else if (settingId == SettingId::VOLUME_PLOT_SCALE)
			{											// $[TI41]
				flowVolumePlot_.setXLimits(upperLimit, lowerLimit,
																	division);
			}											// $[TI62]
            flowVolumePlot_.erase();
            isFlowVolumeLoopUpdated_ = FALSE;
			GuiApp::RaiseGuiEventPriority(GuiApp::GUI_PLOT1_EVENT,
											GuiApp::GUI_HIGHEST_PRIORITY);

		}												// $[TI44]

    }
	// We have to do slightly different stuff if we're adjusting a loop
	// plot.  Check whether the Pressure-Volume plot is currently selected.	
	else if (PLOT1_TYPE_VALUE == Plot1TypeValue::PRESSURE_VS_VOLUME)
	{													// $[TI38]

		// $[PX01002] Add the prox symbol when PROX is enabled.
		if (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_)
		{

			pressureVolumePlot_.setXUnitsString(
							GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
										MiscStrs::PROXIMAL_PRESSURE_PLOT_UNITS :
										MiscStrs::PROXIMAL_PRESSURE_PLOT_HPA_UNITS,
																// $[TI7], $[TI8]
							PV_X_UNITS_X_, PV_X_UNITS_Y_);
			pressureVolumePlot_.setYUnitsString(MiscStrs::PROXIMAL_VOLUME_PLOT_UNITS,
											PV_Y_UNITS_X_, PV_Y_UNITS_Y_);

		}

		else
		{
			pressureVolumePlot_.setXUnitsString(
							GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
										MiscStrs::PRESSURE_PLOT_UNITS :
										MiscStrs::PRESSURE_PLOT_HPA_UNITS,
																// $[TI7], $[TI8]
							PV_X_UNITS_X_, PV_X_UNITS_Y_);
			pressureVolumePlot_.setYUnitsString(MiscStrs::VOLUME_PLOT_UNITS,
											PV_Y_UNITS_X_, PV_Y_UNITS_Y_);

		}

		// Check whether the pressure or volume scales have changed or else
		// the pressure baseline value.
		if ( (settingId == SettingId::PRESSURE_PLOT_SCALE)
			|| (settingId == SettingId::VOLUME_PLOT_SCALE)
			|| (settingId == SettingId::PRESS_VOL_LOOP_BASE)
			)
		{												// $[TI39]
			// If the pressure scale changed then we should adjust the
			// X scale.
			if (settingId == SettingId::PRESSURE_PLOT_SCALE)
			{											// $[TI40]
				pressureVolumePlot_.setXLimits(upperLimit, lowerLimit,
																	division);
			}
			// If the volume scale changed then we should adjust the
			// Y scale.
			else if (settingId == SettingId::VOLUME_PLOT_SCALE)
			{											// $[TI41]
				pressureVolumePlot_.setYLimits(upperLimit, lowerLimit,
																	division);
			}											// $[TI62]

			pressureVolumePlot_.erase();
            isPressureVolumeLoopUpdated_ = FALSE;
			GuiApp::RaiseGuiEventPriority(GuiApp::GUI_PLOT1_EVENT,
											GuiApp::GUI_HIGHEST_PRIORITY);
		}												// $[TI44]
	}
	// One or both time plots need adjusting.  Check if the time scale
	// changed.  Need to do special stuff for this scale.
	else if (settingId == SettingId::TIME_PLOT_SCALE)
	{													// $[TI45]
		// Store the length of the time waveform (in milliseconds).
		lengthOfTimeWaveform_ = (upperLimit - lowerLimit) * 1000;

		// Check if we're frozen and that there is valid data
		if( isFrozen_ && (frozenTimePlotIter_.hasData() == TRUE) )
		{												// $[TI46]
			// We're frozen and the user has requested a time rescale.  This
			// means we have to display the last 'lenghtOfTimeWaveform_'
			// milliseconds of data stored in the frozen data buffer.
			// Let's reposition our frozen time plot iterator.
			frozenTimePlotIter_.goLast();
			frozenTimePlotIter_.
				beginIntervalAtOffset(- lengthOfTimeWaveform_);
		}
		else
		{												// $[TI47]
			// A time rescale occurred while drawing waveforms.  We
			// must clear the current waveform, disallow freezing and
			// wait for the next inspiration.

			// Invalidate current waveform
			currentFullPlotIter_.reset();
			isLastSampleExhalation_ = FALSE;
			isStartOfWaveformFound_ = FALSE;

			// Remove freeze option and make sure if we were freezing that
			// we are not anymore
			freezeButton_.setShow(FALSE);
			unFreeze_();
		}

		// Now, pass on the new time scale to the time plots.
		// Only time plot 1 should display scale values below the X-axis,
		// hence the last parameter to setXLimits() is different for
		// timePlot1_ and timePlot2_.
		timePlot1_.setXLimits(upperLimit, lowerLimit, division, TRUE);
		isPlot1Updated_ = FALSE;
		GuiApp::RaiseGuiEventPriority(GuiApp::GUI_PLOT1_EVENT,
										GuiApp::GUI_HIGHEST_PRIORITY);

		// Check to see if a second time plot was chosen by the user
		if (PLOT2_TYPE_VALUE != Plot2TypeValue::NO_PLOT2  &&
			PLOT2_TYPE_VALUE != Plot2TypeValue::WOB_GRAPHIC)
		{												// $[TI48]
			// Yes, rescale and redraw.
			timePlot2_.setXLimits(upperLimit, lowerLimit, division, FALSE);

			isPlot2Updated_ = FALSE;
			GuiApp::RaiseGuiEventPriority(GuiApp::GUI_PLOT2_EVENT,
											GuiApp::GUI_FOURTH_HIGHEST_PRIORITY);
		}												// $[TI49]
	}
	// Check to see if any of the Y scale settings for the time plots
	// changed.
	else if ((settingId == SettingId::PRESSURE_PLOT_SCALE)
			|| (settingId == SettingId::VOLUME_PLOT_SCALE)
			|| (settingId == SettingId::FLOW_PLOT_SCALE)
			 )
	{													// $[TI50]
		// First, we have to match the changed setting to the time plot
		// which it will adjust, either time plot 1 or time plot 2.
		TimePlot *pAdjustedTimePlot = NULL;		// Points to the time plot
												// which will be rescaled

		// Let's match the changed scale setting with the correct time plot
		switch( settingId )
		{
			case SettingId::PRESSURE_PLOT_SCALE:
			{												// $[TI51]
				//	changed to correct DCS #1506
				//	if the peep has changed and the pressure plot is 
				//	not being displayed skip Updating the plot...
				if( PLOT1_TYPE_VALUE == Plot1TypeValue::PRESSURE_VS_TIME ||
					PLOT2_TYPE_VALUE == Plot2TypeValue::PRESSURE_VS_TIME )
				{											// $[TI52]
					pAdjustedTimePlot =
						(PLOT1_TYPE_VALUE == Plot1TypeValue::PRESSURE_VS_TIME) ?
						&timePlot1_ : &timePlot2_;
															// $[TI63], $[TI64]
				}
															// $[TI53]
			}
				break;
			case SettingId::VOLUME_PLOT_SCALE:
			{												// $[TI54]
				pAdjustedTimePlot =
					(PLOT1_TYPE_VALUE == Plot1TypeValue::VOLUME_VS_TIME) ?
					&timePlot1_ : &timePlot2_;
															// $[TI55], $[TI56]
			}
				break;
			case SettingId::FLOW_PLOT_SCALE:
			{												// $[TI57]
				pAdjustedTimePlot =
					(PLOT1_TYPE_VALUE == Plot1TypeValue::FLOW_VS_TIME) ?
					&timePlot1_ : &timePlot2_;
															// $[TI58], $[TI59]
			}
				break;
		}

		if( NULL != pAdjustedTimePlot )
		{													// $[TI60]
			// Rescale the time plot
			// $[01329] Flow axis labels marking the negative portion of the ...
			pAdjustedTimePlot->setYLimits(upperLimit, lowerLimit, division,
									settingId == SettingId::FLOW_PLOT_SCALE);

			// Redraw the time plot
			redrawPlot_(*pAdjustedTimePlot);
		}													// $[TI61]
	}														// $[TI62]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: cancelPrintRequest
//
//@ Interface-Description
//	This method is called to cancel a print request.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If GUI serial task is in the process of printing then blank the
//	print button and the 'Printing' message, otherwise perform printDone()
//	to do the final print events immediately.
//	Inform the GUI serial interface subsystem to cancel the print.
//
// $[GC00402] If the cancel button is pressed prior to ...
// $[GC01003] The print function shall be active when ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::cancelPrintRequest(void)
{
	CALL_TRACE("WaveformsSubScreen::cancelPrintRequest(void)");

	if (SerialInterface::IsPrintInProgress())
	{
	// $[TI1]
		printButton_.setShow(FALSE);
		printingText_.setShow(FALSE);
	}
	else
	{
	// $[TI2]
		printDone();
	}

    SerialInterface::CancelPrint();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: printDone
//
//@ Interface-Description
//	This method is called at the end of the printing process, either
//	when printing has completed (via the CAPTURE_DONE_EVENT from the
//	serial task) or has been cancelled (via the print cancel key or
//	waveform screen dismissal).
//---------------------------------------------------------------------
//@ Implementation-Description
//	Display 'PRINT' on the print button.
//	Display freeze and plot setup buttons, display print button
//	if com1 device is set to Printer.
//	Blank the 'Printing' message.
//
// $[GC00402] If the cancel button is pressed prior to the print ...
// $[GC01002] The Waveforms subscreen shall provide a print button ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::printDone(void)
{
// $[TI1]
	CALL_TRACE("WaveformsSubScreen::printDone(void)");

	DiscreteValue com1DeviceSetting =
		AcceptedContextHandle::GetDiscreteValue(SettingId::COM1_CONFIG);

	printButton_.setTitleText(MiscStrs::PRINT_BUTTON_TITLE);
	printButton_.setTitleColor(Colors::BLACK);
	printButton_.setShow(com1DeviceSetting == ComPortConfigValue::PRINTER_VALUE);
	plotSetupButton_.setShow(TRUE);
	freezeButton_.setShow(TRUE);
	printingText_.setShow(FALSE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePrintButton
//
//@ Interface-Description
//  Show the printButton if the waveforms are frozen.
//  Called when GuiApp receives a PRINTER_ASSIGNMENT_EVENT indicating
//	a printer interface assignment.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// $[GC01002] The Waveforms subscreen shall provide a print button ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
WaveformsSubScreen::updatePrintButton(void)
{
// $[TI1]
  printButton_.setShow(isFrozen_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: transitionCaptureToPrint
//
//@ Interface-Description
//      This method is called to transition the waveforms
//      subscreen from the capturing state to the printing state.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Change the text on the waveforms subscreen from 'CAPTURING' to 
//      'PRINTING'.  Change the text on the PRINT button to CANCEL PRINT
//      so the user can cancel the print/upload in progress.
//
// $[GC01003] The print function shall be active when the button is ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::transitionCaptureToPrint(void)
{
  CALL_TRACE("WaveformsSubScreen::transitionCaptureToPrint(void)");

  if (SerialInterface::IsPrintRequested())
  {												// $[TI1.1]
    // Switch the print button to displaying 'CANCEL'.
    printButton_.setShow(TRUE);
    printButton_.setTitleText(MiscStrs::PRINT_BUTTON_CANCEL_PRINT_TITLE);
    printButton_.setTitleColor(Colors::BLACK);
  } 											// $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isInPavClosedLoop
//
//@ Interface-Description
//  When Ventilator is currently in PAV state of "CLOSED_LOOP" return true;
//  otherwise return false.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//---------------------------------------------------------------------
// NOTE:
//  - PAV3_030415_LJS - Currently, there is no applicable state variable;
//  during runtime, the show state of intrinsicPeepSymbol_ seems 
//  to accurately reflect what we need.
//=====================================================================
//
Boolean  
WaveformsSubScreen::isInPavClosedLoop( void) const
{
    return (intrinsicPeepSymbol_.getShow());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
// Called normally via the BdEventRegistrar when a BD event in which we are
// interested occurs.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition                                              
//  none                                                      
//---------------------------------------------------------------------
//@ PostCondition                                             
//  none                                                      
//@ End-Method                                                
//=====================================================================
                                                              
void                                                          
WaveformsSubScreen::bdEventHappened(EventData::EventId eventId,
									EventData::EventStatus eventStatus,
									EventData::EventPrompt)
{
	Boolean  isStateUpdateNeeded = FALSE;

	// is it appropriate to display the PAV symbols, with or without the
	// values...
	const Boolean  ARE_PAV_DISPLAYS_ALLOWED =
		(BdGuiEvent::GetEventStatus(EventData::VENT_INOP) !=
													EventData::ACTIVE  &&
		 BdGuiEvent::GetEventStatus(EventData::SVO) != EventData::ACTIVE  &&
		 BdGuiEvent::GetEventStatus(EventData::OCCLUSION) !=
													EventData::ACTIVE  &&
		 BdGuiEvent::GetEventStatus(EventData::APNEA_VENT) !=
													EventData::ACTIVE  &&
		 BdGuiEvent::GetEventStatus(EventData::SAFETY_VENT) !=
													EventData::ACTIVE);

	if (arePavSymbolsAllowed_ != ARE_PAV_DISPLAYS_ALLOWED)
	{	// $[TI1]
		arePavSymbolsAllowed_ = ARE_PAV_DISPLAYS_ALLOWED;
		isStateUpdateNeeded   = TRUE;
	}	// $[TI2]

	const Boolean  ARE_PAV_VALUES_ALLOWED =
		 (ARE_PAV_DISPLAYS_ALLOWED  &&
		  BdGuiEvent::GetEventStatus(EventData::PATIENT_CONNECT) ==
													EventData::ACTIVE);

	if (arePavValuesAllowed_ != ARE_PAV_VALUES_ALLOWED)
	{	// $[TI3]
		arePavValuesAllowed_  = ARE_PAV_VALUES_ALLOWED;
		isStateUpdateNeeded   = TRUE;
	}	// $[TI4]

	if (isStateUpdateNeeded  &&  currAuxInfoState_ == PAV_ACTIVE_)
	{	// $[TI5]
		// force an update to ensure proper display of PAV symbols and values...
 		updateAuxInfoState_(PAV_ACTIVE_, TRUE);
	}	// $[TI6]


	// When Breath-Delivery sends a PROX_FAULT message with an active 
    // event id, remove all PROX symbols by setting the isProxInop_ 
    // to TRUE, and reactivate this subscreen. 
    // $[PX06003]
	if (EventData::PROX_FAULT == eventId ||
		EventData::SAFETY_VENT == eventId)
	{

		// When PROX is inoperative, set the isProxInop_ to remove the PROX symbols 
		// and refresh the screen.
		if(eventStatus == EventData::ACTIVE)		  
		{
            isProxInop_ = TRUE;
		}
		else
		{
			isProxInop_ = FALSE;
		}

		if (isVisible())
		{
			deactivate();
			activate();

		}
	}
	else if (EventData::PROX_READY == eventId)
	{

		if(eventStatus == EventData::ACTIVE)		  
		{
            isProxInStartupMode_ = FALSE;
		}
		else
		{
			isProxInStartupMode_ = TRUE;
		}

        if (isVisible())
		{
			deactivate();
			activate();
		}

	}

}                                                             


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when the Plot Setup, Freeze or Continue button is pressed down.
// Passed a pointer to the button as well as 'byOperatorAction', a flag which
// indicates whether the operator pressed the button down or whether the
// setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the Plot Setup button is pressed then we switch to the waveforms
// plot setup screen.  If the Continue button is pressed then we switch
// back to plot setup.  If the Freeze button was pressed then we decide
// whether to freeze or unfreeze the waveforms.
//
// $[01348] The user shall be able to change waveform setup, including ...
// $[GC01003] The print function shall be active when the button is ...
// $[GC00402] If the cancel button is pressed prior to the print function ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::buttonDownHappened(Button *pButton, Boolean)
{
	CALL_TRACE("WaveformsSubScreen::buttonDownHappened(Button *pButton, Boolean)");

	// Was the Plot Setup button pressed?
	if (pButton == &plotSetupButton_)
	{													// $[TI1]
		AdjustPanel::TakePersistentFocus(NULL);		// Release focus
		plotSetupButton_.setToUp();			// Pop it back up
		isOnWaveDisplay_ = FALSE;			// Go to plot setup
		layoutScreen_();				// Layout that screen
	}
	// Maybe the Continue button was pressed?
	else if (pButton == &continueButton_)
	{													// $[TI2]
		AdjustPanel::TakePersistentFocus(NULL);		// Release focus
		continueButton_.setToUp();		       	// Pop button back up

		// Reset current waveform flags so that we start with a clear
		// current waveform.
		isStartOfWaveformFound_ = FALSE;
		isLastSampleExhalation_ = FALSE;
		currentFullPlotIter_.reset();
		currentSegmentIter_.beginInterval();
		currentSegmentIter_.endInterval();

		// do the drawing and presentation only after all setup is completed,
		// to reduce redundant drawing...
		isOnWaveDisplay_ = TRUE;		       	// Go to waveform screen
		layoutScreen_();			       	// Layout that screen
	}
	// Gotta be that Freeze button
	else if (pButton == &freezeButton_)
	{													// $[TI3]
		AdjustPanel::TakePersistentFocus(NULL);		// Release focus
		freezeButton_.setToUp();			// Bounce the button

		// $[01333] A frozen waveform can be unfrozen at any time, regardless ...
		// If the screen is frozen or freezing then unfreeze, otherwise freeze.
		if (isFrozen_ || isFreezing_)
		{												// $[TI4]

            // if the waveform screen is freezing during an RM Maneuver,
            // restore the User-Defined waveform settings.
            if(isNifWaveformFoisting_ || isVcWaveformFoisting_)
            { 
                // Restore User-Defined waveform settings.
                restoreWaveformSettings();
            }

            // Clear the P100 Flag if P100 maneuver was activated.
            if(isP100WaveformFoisting_)
            {
                isP100WaveformFoisting_ = FALSE;
            }

			unFreeze_();
		}
		else
		{												// $[TI5]
			freeze_();
		}
	}
	// or perhaps the print/cancel button
	else if (pButton == &printButton_)
	{													// $[TI6]
        GuiApp::SetPrintTarget(this);
		AdjustPanel::TakePersistentFocus(NULL);		// Release focus
		printButton_.setToUp();

		// If a print request is in progress, cancel it.
		if ( SerialInterface::IsPrintRequested() ) 
		{
		// $[TI7]
			cancelPrintRequest();
		}
		// else start printing unless SerialInterface indicates not to
		else if ( !SerialInterface::StartPrint() )
		{
		// $[TI8]
			Sound::Start(Sound::SoundId::ACCEPT);
			plotSetupButton_.setShow(FALSE);
			freezeButton_.setShow(FALSE);
			printButton_.setShow(FALSE);
			printingText_.setShow(TRUE);
		}
		// else if SerialInterface indicates a print is disallowed then
		// generate the invalid keypress sound
		else
		{
		// $[TI9]
			Sound::Start(Sound::SoundId::INVALID_ENTRY);
		}
	}													
	// $[TI10]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened
//
//@ Interface-Description
// Called when a patient datum value has changed.  It will be one of
// Intrinsic or Total PEEP for expiratory pause.  It will be one of
// Compliance, Resistance, or Plateau pressure for inspiratory pause.
// Update these patient data display with the new value
// (if actually displaying those values).
//---------------------------------------------------------------------
//@ Implementation-Description
// First figure out which drawables (numeric value and unit text) we
// must update, either the ones for Intrinsic, Total PEEP, Compliance,
// Resistance or Plateau pressure.  Then, if the value has timed out,
// remove the numeric value and unit text.  Else display the new value
// and unit text and flash the value if it is out of range.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::patientDataChangeHappened(PatientDataId::PatientItemId
														patientDataId)
{
	CALL_TRACE("patientDataChangeHappened(patientDataId)");

	PauseTypes::ComplianceState staticMechanicsStatusValue = PauseTypes::VALID;
	TextField *pStaticProblemLabel = NULL;

	TextField*     pSymbol;
	NumericField*  pValue;
	TextField*     pUnit;
    
    switch( currAuxInfoState_)
    {
    case EXP_PAUSE_:
    case NIF_PAUSE_:
    case P100_PAUSE_:
    case VC_PAUSE_:
    case INSP_PAUSE_:{
	// Ignore events when not displaying PEEP and inspiratory pause
	// related values
    	if(( patientDataId == PatientDataId::STATIC_LUNG_COMPLIANCE_VALID_ITEM  ||
			 patientDataId == PatientDataId::STATIC_AIRWAY_RESISTANCE_VALID_ITEM  ||
			 patientDataId == PatientDataId::PEEP_INTRINSIC_ITEM  ||
			 patientDataId == PatientDataId::PEEP_TOTAL_ITEM  ||
			 patientDataId == PatientDataId::NIF_PRESSURE_ITEM  ||
			 patientDataId == PatientDataId::P100_PRESSURE_ITEM  ||
			 patientDataId == PatientDataId::VITAL_CAPACITY_ITEM  ||
			 patientDataId == PatientDataId::PLATEAU_PRESSURE_ITEM))
		 {													// $[TI1]
				PatientDataId::PatientItemId pdIdToBeProcessed = patientDataId;
				
				if (patientDataId == PatientDataId::STATIC_LUNG_COMPLIANCE_VALID_ITEM)
				{												// $[TI1.1]
					pdIdToBeProcessed = PatientDataId::STATIC_LUNG_COMPLIANCE_ITEM;
				}
				else if (patientDataId == PatientDataId::STATIC_AIRWAY_RESISTANCE_VALID_ITEM)
				{												// $[TI1.2]
					pdIdToBeProcessed = PatientDataId::STATIC_AIRWAY_RESISTANCE_ITEM;
				}												// $[TI1.3]

				BoundedBreathDatum pdDatum = 
					BreathDatumHandle(pdIdToBeProcessed).getBoundedValue();
													
				// Is it Intrinsic PEEP that changed?		
		        // Don't evaluate the END_EXP_PAUSE_PRESSURE_STATE_ITEM yet
		        // wait until we are signalled.
		        if (pdIdToBeProcessed == PatientDataId::PEEP_INTRINSIC_ITEM)
				{												// $[TI2]
					// Point to the Intrinsic PEEP drawables.
					pSymbol = &intrinsicPeepSymbol_;
					pValue = &intrinsicPeepValue_;
					pUnit = &intrinsicPeepUnit_;

		            isEndofExpPauseManeuver_= FALSE;
;					updateAuxInfoState_(EXP_PAUSE_);
				}
				else if (pdIdToBeProcessed == PatientDataId::PEEP_TOTAL_ITEM)
				{												// $[TI3]
					// Point to the Total PEEP drawables.
					pSymbol = &totalPeepSymbol_;
					pValue = &totalPeepValue_;
					pUnit = &totalPeepUnit_;

		            isEndofExpPauseManeuver_= FALSE;
					updateAuxInfoState_(EXP_PAUSE_);
				}
				else if (pdIdToBeProcessed == PatientDataId::NIF_PRESSURE_ITEM)
				{
					// Point to the NIF pressure drawables
					pSymbol = &nifPressureSymbol_;
					pValue = &nifPressureValue_;
					pUnit = &nifPressureUnit_;

		            isEndofNifPauseManeuver_= FALSE;
					updateAuxInfoState_(NIF_PAUSE_);
				}
				else if (pdIdToBeProcessed == PatientDataId::P100_PRESSURE_ITEM)
				{
					// Point to the P100 pressure drawables
					pSymbol = &p100PressureSymbol_;
					pValue = &p100PressureValue_;
					pUnit = &p100PressureUnit_;

		            isEndofP100PauseManeuver_= FALSE;
					updateAuxInfoState_(P100_PAUSE_);
				}
				else if (pdIdToBeProcessed == PatientDataId::VITAL_CAPACITY_ITEM)
				{
					// Point to the SVC pressure drawables
					pSymbol = &vitalCapacitySymbol_;
					pValue = &vitalCapacityValue_;
					pUnit = &vitalCapacityUnit_;

		            isEndofSvcPauseManeuver_= FALSE;
					updateAuxInfoState_(VC_PAUSE_);
				}
				else if (pdIdToBeProcessed == PatientDataId::STATIC_LUNG_COMPLIANCE_ITEM)
				{												// $[TI7]
					// Point to the lung compliance drawables.
					// $[BL01013] display static compliance
					pSymbol = &lungComplianceSymbol_;
					pValue = &lungComplianceValue_;
					pUnit = &lungComplianceUnit_;

					updateAuxInfoState_(INSP_PAUSE_);

		    		staticMechanicsStatusValue = staticComplianceStatusValue_;
		    		pStaticProblemLabel = &staticComplianceProblemLabel_;
				}
				else if (pdIdToBeProcessed == PatientDataId::STATIC_AIRWAY_RESISTANCE_ITEM)
				{												// $[TI8]
					// Point to the lung resistance drawables.
					// $[BL01013] display static resistance
					pSymbol = &airwayResistanceSymbol_;
					pValue = &airwayResistanceValue_;
					pUnit = &airwayResistanceUnit_;

    				updateAuxInfoState_(INSP_PAUSE_);

		    		staticMechanicsStatusValue = staticResistanceStatusValue_;
		    		pStaticProblemLabel = &staticResistanceProblemLabel_;
				}
				else if (pdIdToBeProcessed == PatientDataId::PLATEAU_PRESSURE_ITEM)
				{												// $[TI9]
					// $[BL01005]
					pSymbol = &plateauPressureSymbol_;
					pValue = &plateauPressureValue_;
					pUnit = &plateauPressureUnit_;
				}
				else
				{
					// unexpected 'pdIdToBeProcessed' value...
					AUX_CLASS_ASSERTION_FAILURE(pdIdToBeProcessed);
				}

				// Make sure value and unit are displayed
				pSymbol->setShow(TRUE);
				pValue->setShow(TRUE);
				pUnit->setShow(TRUE);

				// Set the new value and precision
				pValue->setValue(pdDatum.data.value);
				pValue->setPrecision(pdDatum.data.precision);

				// Flash the datum if out of range else display in white.
				if (pdDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
				{												// $[TI4]
					pValue->setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
				}
				else
				{												// $[TI5]
					pValue->setColor(Colors::WHITE);
				}

				if (pdIdToBeProcessed == PatientDataId::STATIC_LUNG_COMPLIANCE_ITEM ||
					pdIdToBeProcessed == PatientDataId::STATIC_AIRWAY_RESISTANCE_ITEM)
				{												// $[TI11]		
					plateauPressureValue_.setShow(TRUE);
		            plateauPressureMarkerText_.setShow(FALSE);

					// $[BL04064] :p
					switch (staticMechanicsStatusValue)
					{
					case PauseTypes::VALID:
					case PauseTypes::UNAVAILABLE:
					case PauseTypes::NOT_REQUIRED:				// $[TI11.1]		
		                pStaticProblemLabel->setShow(FALSE);
						break;
					case PauseTypes::OUT_OF_RANGE:				// $[TI11.2]	
						pStaticProblemLabel->setShow(TRUE);
						pStaticProblemLabel->setText(MiscStrs::OUT_OF_RANGE_LABEL);
						break;
					case PauseTypes::SUB_THRESHOLD_INPUT:		// $[TI11.3]	
						pStaticProblemLabel->setShow(TRUE);
						pStaticProblemLabel->setText(MiscStrs::SUB_THRESHOLD_INPUT_LABEL);
						break;
					case PauseTypes::CORRUPTED_MEASUREMENT:		// $[TI11.4]	
						pStaticProblemLabel->setShow(TRUE);
						pStaticProblemLabel->setText(MiscStrs::CORRUPTED_MEASUREMENT_LABEL);
						break;
					case PauseTypes::EXH_TOO_SHORT:				// $[TI11.5]	
						pStaticProblemLabel->setShow(TRUE);
						pStaticProblemLabel->setText(MiscStrs::INCOMPLETE_EXHALATION_LABEL);
						break;
					case PauseTypes::NOT_STABLE:				// $[TI11.6]	
						pStaticProblemLabel->setShow(TRUE);
						pStaticProblemLabel->setText(MiscStrs::NO_PLATEAU_LABEL);
						plateauPressureMarkerText_.setShow(TRUE);
						break;
					default:
						break;
					}
				}												// $[TI12]	
			}
			else if (isOnWaveDisplay()  &&  currAuxInfoState_ == EXP_PAUSE_  &&
			 		patientDataId == PatientDataId::END_EXP_PAUSE_PRESSURE_STATE_ITEM)
			{													// $[TI13]	
		        // set flag indicating that it is appropriate to evaluate
		        // data for unstable values.
		        isEndofExpPauseManeuver_= TRUE;
		        		
		        updateAuxInfoState_(EXP_PAUSE_);
			}
    	}break;	// case EXP_PAUSE_ of switch( currAuxInfoState_)
    //----------------------------------------------------------------    
    case PAV_ACTIVE_:{
													// $[TI6]
		BreathDatumHandle  breathDatumHandle(patientDataId);

		if (patientDataId == PatientDataId::PAV_LUNG_COMPLIANCE_ITEM)
		{													// $[TI6.1]
			BoundedBreathDatum  PD_DATUM = breathDatumHandle.getBoundedValue();

			// Set the new value and precision
			lungComplianceValue_.setValue(PD_DATUM.data.value);
			lungComplianceValue_.setPrecision(PD_DATUM.data.precision);

			if (!PD_DATUM.timedOut)
			{											// $[TI6.1.1]
				// $[PA03002] -- Rate...at the completion of each.... 
                lungComplianceValue_.setShow(TRUE);
				lungComplianceUnit_.setShow(TRUE);
			}
			else
			{											// $[TI6.1.2]
				lungComplianceValue_.setShow(FALSE);
				lungComplianceUnit_.setShow(FALSE);
			}

			switch (PD_DATUM.clippingState)
			{
			case BoundedValue::UNCLIPPED :							// $[TI6.1.3]
				lungComplianceValue_.setColor(Colors::WHITE);
				lungComplianceMarkerText_.setShow(FALSE);
				break;
			case BoundedValue::ABSOLUTE_CLIP :						// $[TI6.1.4]
				lungComplianceValue_.setColor(
									  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
											);
				lungComplianceMarkerText_.setShow(TRUE);
				break;
			case BoundedValue::VARIABLE_CLIP :						// $[TI6.1.5]
				lungComplianceValue_.setColor(Colors::WHITE);
				lungComplianceMarkerText_.setShow(TRUE);
				break;
			default :
				// unexpected clipping state...
				AUX_CLASS_ASSERTION_FAILURE(PD_DATUM.clippingState);
				break;
			}
		}
		else if (patientDataId == PatientDataId::TOTAL_AIRWAY_RESISTANCE_ITEM)
		{													// $[TI6.2]
			//-------------------------------------------------------------
			// SPECIAL:  unlike all of the other data items, the Rpav is keyed
			//           keyed off of Rtot's callback only; Rpav's new value
			//           is ready when Rtot's callback is received...
			//-------------------------------------------------------------
			BreathDatumHandle  rpavDatumHandle(
									  PatientDataId::PAV_PATIENT_RESISTANCE_ITEM
											  );

			BoundedBreathDatum  rtotDatum = breathDatumHandle.getBoundedValue();
			BoundedBreathDatum  rpavDatum = rpavDatumHandle.getBoundedValue();

			// Set the new value and precision
			airwayResistanceValue_.setValue(rpavDatum.data.value);
			airwayResistanceValue_.setPrecision(rpavDatum.data.precision);

			if (!rpavDatum.timedOut)
			{											// $[TI6.2.1]
				// $[PA03005] -- Rate...at the completion of each.... 
				airwayResistanceValue_.setShow(TRUE);
				airwayResistanceUnit_.setShow(TRUE);
			}
			else
			{											// $[TI6.2.2]
				airwayResistanceValue_.setShow(FALSE);
				airwayResistanceUnit_.setShow(FALSE);
			}

			switch (rpavDatum.clippingState)
			{
			case BoundedValue::UNCLIPPED :						// $[TI6.2.3]
				airwayResistanceValue_.setColor(Colors::WHITE);
				airwayResistanceMarkerText_.setShow(
										(rtotDatum.clippingState != BoundedValue::UNCLIPPED)
												   );
				break;
			case BoundedValue::ABSOLUTE_CLIP :					// $[TI6.2.4]
				airwayResistanceValue_.setColor(
									  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
											   );
				airwayResistanceMarkerText_.setShow(TRUE);
				break;
			case BoundedValue::VARIABLE_CLIP :					// $[TI6.2.5]
				airwayResistanceValue_.setColor(Colors::WHITE);
				airwayResistanceMarkerText_.setShow(TRUE);
				break;
			default :
				// unexpected clipping state...
				AUX_CLASS_ASSERTION_FAILURE(rpavDatum.clippingState);
				break;
			}
		}
		else if (patientDataId == PatientDataId::PAV_STATE_ITEM)
		{													// $[TI6.3]
			switch (breathDatumHandle.getDiscreteValue().data)
			{
			case PavState::STARTUP :						// $[TI6.3.1]
	            // $[PA01005] -- display PAV Startup messages
				pavStatusMsg_.setShow(TRUE);
				intrinsicPeepSymbol_.setShow(FALSE);
				intrinsicPeepValue_.setShow(FALSE);
				intrinsicPeepUnit_.setShow(FALSE);
                wobGraphic_.activate(); // PAV3_030418_LJS                
				break;
			case PavState::CLOSED_LOOP :					// $[TI6.3.2]
				if(pavStatusMsg_.getShow())
				{	  
				    pavStatusMsg_.setShow(FALSE);
	                // $[PA01010] -- C, R and  PEEPi shall be displayed...
				    intrinsicPeepSymbol_.setShow(TRUE);
				}
                break;
            default:
            // do nothing
            	break;
			};
		}
		else if (patientDataId == PatientDataId::PAV_PEEP_INTRINSIC_ITEM)
		{													// $[TI6.4]
			BoundedBreathDatum  PD_DATUM = breathDatumHandle.getBoundedValue();

			// Make sure symbol is displayed only when the PAV status message
			// is NOT shown...
			intrinsicPeepSymbol_.setShow(!pavStatusMsg_.getShow());

			// Set the new value and precision
			intrinsicPeepValue_.setValue(PD_DATUM.data.value);
			intrinsicPeepValue_.setPrecision(PD_DATUM.data.precision);

			if (!PD_DATUM.timedOut  &&  intrinsicPeepSymbol_.getShow())
			{											// $[TI6.4.1]
	            // $[PA01010] -- C, R and  PEEPi shall be displayed...
				// $[PA03014] -- Rate...at the completion of each.... 
				intrinsicPeepValue_.setShow(TRUE);
				intrinsicPeepUnit_.setShow(TRUE);
			}
			else
			{											// $[TI6.4.2]
				intrinsicPeepValue_.setShow(FALSE);
				intrinsicPeepUnit_.setShow(FALSE);
			}

			// Flash the datum if out of range else display in white.
			if (PD_DATUM.clippingState == BoundedValue::ABSOLUTE_CLIP)
			{												// $[TI6.4.3]
				intrinsicPeepValue_.setColor(
										Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
											);
			}
			else
			{												// $[TI6.4.4]
				intrinsicPeepValue_.setColor(Colors::WHITE);
			}
		}
		else if (patientDataId == PatientDataId::IS_PEEP_RECOVERY_ITEM)
		{													// $[TI6.5]
			if (breathDatumHandle.getDiscreteValue().data)
			{													// $[TI6.5.1]
				// Make sure the patient data field labels and values are hidden
				// when we are in peep recovery state...
				lungComplianceUnit_.setShow(FALSE);
				lungComplianceValue_.setShow(FALSE);
				lungComplianceMarkerText_.setShow(FALSE);

				airwayResistanceUnit_.setShow(FALSE);
				airwayResistanceValue_.setShow(FALSE);
				airwayResistanceMarkerText_.setShow(FALSE);

				intrinsicPeepValue_.setShow(FALSE);
				intrinsicPeepUnit_.setShow(FALSE);
				intrinsicPeepMarkerText_.setShow(FALSE);

				pavStatusMsg_.setShow(FALSE);
			}													// $[TI6.5.2]
		}													// $[TI6.6]
		}break;// case PAV_ACTIVE_ of switch( currAuxInfoState_)
        //------------------------------------------------------------------
    case VCP_ACTIVE_:{
		BreathDatumHandle  breathDatumHandle(patientDataId);													
		if (patientDataId == PatientDataId::VTPCV_STATE_ITEM)
		{
			switch (breathDatumHandle.getDiscreteValue().data)
			{
			case VtpcvState::STARTUP_VTPCV :
					vcpStatusMsg_.setShow(TRUE);									 
				break;															 
			case VtpcvState::NORMAL_VTPCV :										 
					if(vcpStatusMsg_.getShow())
					{
						vcpStatusMsg_.setShow(FALSE);
					}
				break;
			default:
				// do nothing...
				break;
			};
		}													
		else if (patientDataId == PatientDataId::IS_PEEP_RECOVERY_ITEM)
			   {													
				  if(vcpStatusMsg_.getShow())
				    vcpStatusMsg_.setShow(FALSE);
			    }
        }break;// case VCP_ACTIVE_ of switch( currAuxInfoState_)
        //------------------------------------------------------------------
    case VS_ACTIVE_:{
		BreathDatumHandle  breathDatumHandle(patientDataId);													
		if (patientDataId == PatientDataId::VTPCV_STATE_ITEM)
		{
			switch (breathDatumHandle.getDiscreteValue().data)
			{
			case VtpcvState::STARTUP_VTPCV :
					vsStatusMsg_.setShow(TRUE);
				break;															 
			case VtpcvState::NORMAL_VTPCV :										 
				if(vsStatusMsg_.getShow())
				{
					vsStatusMsg_.setShow(FALSE);
				}
				break;
			default:
				// do nothing...
				break;
			};
		}													
		else if (patientDataId == PatientDataId::IS_PEEP_RECOVERY_ITEM)
		{													
			  if(vsStatusMsg_.getShow())
			  {		
			    vsStatusMsg_.setShow(FALSE);
              }
		}
        }break;// case VS_ACTIVE_ of switch( currAuxInfoState_)
     }// end switch( currAuxInfoState_) 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_
//
//@ Interface-Description
// Called whenever the content of the Waveform subscreen might change.
// It lays out the Waveforms subscreen for either the waveform display
// or the plot setup.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The 'isOnWaveDisplay_' data member indicates whether to lay out the
// waveform display screen or the plot setup screen.  We first empty the
// subscreen of drawables.  If we are displaying waveforms then we look
// at the plot setup chosen by the user to determine which waveform plot(s)
// to display.  Displaying the plot setup screen is easier since it has a
// fixed display.
//
// $[01157] The time curve format shall allow independent adjustment ...
// $[01158] The time curve format shall meet the following criteria ...
// $[01163] When fewer than two time curves are selected, the single ...
// $[01164] The loop format design shall be capable of displaying ...
// $[GC01002] The Waveforms subscreen shall provide a print button ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::layoutScreen_(void)
{
	CALL_TRACE("WaveformsSubScreen::layoutScreen_(void)");
	
    // Empty the subscreen of all objects, before updating states.
	removeAllDrawables();

	const ContextSubject *const  P_ACCEPTED_SUBJECT =
							getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

	const DiscreteValue PLOT1_TYPE_VALUE =
				P_ACCEPTED_SUBJECT->getSettingValue(SettingId::PLOT1_TYPE);
	const DiscreteValue PLOT2_TYPE_VALUE =
				P_ACCEPTED_SUBJECT->getSettingValue(SettingId::PLOT2_TYPE);
	const DiscreteValue PROX_ENABLED_VALUE =
				P_ACCEPTED_SUBJECT->getSettingValue(SettingId::PROX_ENABLED);

	isProxEnabled_ = (PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED);


	// If we're laying out the screen that means we should reset the current
	// plot.
	isPlot1Updated_ = TRUE;
	isPlot2Updated_ = TRUE;
    isPressureVolumeLoopUpdated_ = TRUE;
    isFlowVolumeLoopUpdated_ = TRUE;

	isStartOfWaveformFound_ = FALSE;
	currentFullPlotIter_.reset();

	// Send signal to update the WaveformsTabButton so that its color will
	// match the background color of the newly laid out screen.
	UpperScreen::RUpperScreen.getUpperScreenSelectArea()->updateButtonDisplay();

	// Is it the waveform display that needs to be laid out?
	if (isOnWaveDisplay_)
	{													// $[TI1]
		Button *pButton;			// Temporary button pointer

		// need to add, then activate, all of the "display" buttons, due to
		// the removal above...
		for (Uint idx = 0u; arrDisplayBtnPtrs_[idx] != NULL; idx++)
		{
			// all of these buttons attach to this subscreen directly...
			addDrawable(arrDisplayBtnPtrs_[idx]);
			(arrDisplayBtnPtrs_[idx])->activate();
		}

		// Let's display those waveforms

		// Set subscreen background color
		setFillColor(Colors::BLACK);

        // Display the Plot Setup and Freeze buttons.  Make sure the
		// flashing "freezing" message is not shown.
		addDrawable(&plotSetupButton_);
		addDrawable(&freezeButton_);
		addDrawable(&printButton_);
		freezingText_.setShow(FALSE);
		addDrawable(&freezingText_);
		printingText_.setShow(FALSE);
		addDrawable(&printingText_);
		capturingText_.setShow(FALSE);
		addDrawable(&capturingText_);

		// If we're not in the frozen state then the freeze and print buttons
		// should not be shown.
		if (!isFrozen_)
		{												// $[TI2]
			freezeButton_.setShow(FALSE);
			printButton_.setShow(FALSE);
		}
		// If we're in the frozen state display the print button if the
		// com1 device is set to Printer, else blank it.
		else
		{												// $[TI3]
			DiscreteValue com1DeviceSetting =
				AcceptedContextHandle::GetDiscreteValue(SettingId::COM1_CONFIG);
			printButton_.setShow(com1DeviceSetting == ComPortConfigValue::PRINTER_VALUE);
		}

		// fake a change to either mode or spont type, to update state...
		batchSettingUpdate(Notification::ACCEPTED, P_ACCEPTED_SUBJECT,
						   SettingId::MODE);

		// Check first for a Pressure-Volume plot setup
		if (PLOT1_TYPE_VALUE == Plot1TypeValue::PRESSURE_VS_VOLUME)
		{												// $[TI6]
			// since both the 'x' and 'y' pressure scales will be applicable
			// at the same time (they're both monitoring the same setting),
			// we need to explicitly clear one of them...
			removeDrawable(&yPressureScaleButton_);
			removeDrawable(&xVolumeScaleButton_);

            // If the user started to freeze but waveforms was dismissed,
            // we must unfreeze the screeen before redisplaying it.
            if( isFreezing_)
            {	                 

                unFreeze_();
            }

            // Preventing multiple screen refreshing.
            updatePlot_ = FALSE;
			// Fake changes to the settings which adjust the Pressure-Volume
			// plot so that the plot is synched up with the latest setting
			// values.
			nonBatchSettingUpdate(Notification::ACCEPTED,
								  P_ACCEPTED_SUBJECT,
								  SettingId::PRESSURE_PLOT_SCALE);
			nonBatchSettingUpdate(Notification::ACCEPTED,
								  P_ACCEPTED_SUBJECT,
								  SettingId::VOLUME_PLOT_SCALE);
            // Update the screen.
            updatePlot_ = TRUE;            
			nonBatchSettingUpdate(Notification::ACCEPTED,
								  P_ACCEPTED_SUBJECT,
								  SettingId::PRESS_VOL_LOOP_BASE);

			// Activate the plot and the pressure baseline setting button
			pressureVolumePlot_.activate();

			// Display the plot and the buttons which adjust it
			addDrawable(&pressureVolumePlot_);
			volumeScaleButton_.setX(PV_Y_SCALE_BUTTON_X_);
			volumeScaleButton_.setY(PV_Y_SCALE_BUTTON_Y_);
			xPressureScaleButton_.setX(PV_X_SCALE_BUTTON_X_);
			xPressureScaleButton_.setY(PV_X_SCALE_BUTTON_Y_);

		}
		// Check first for a Flow-Volume plot setup
		else if (PLOT1_TYPE_VALUE == Plot1TypeValue::FLOW_VS_VOLUME)
		{											
			// since both the 'x' and 'y' flow scales will be applicable
			// at the same time (they're both monitoring the same setting),
			// we need to explicitly clear one of them...
			removeDrawable(&volumeScaleButton_);
			removeDrawable(&yPressureScaleButton_);
			removeDrawable(&xPressureScaleButton_);

            // If the user started to freeze but waveforms was dismissed,
            // we must unfreeze the screeen before redisplaying it.
            if( isFreezing_)
            {	                 

                unFreeze_();
            }

            // Preventing multiple screen refreshing.
            updatePlot_ = FALSE;

			// Fake changes to the settings which adjust the Flow-Volume
			// plot so that the plot is synched up with the latest setting
			// values.
			nonBatchSettingUpdate(Notification::ACCEPTED,
								  P_ACCEPTED_SUBJECT,
								  SettingId::FLOW_PLOT_SCALE);
            // Update the screen
            updatePlot_ = TRUE;
			nonBatchSettingUpdate(Notification::ACCEPTED,
								  P_ACCEPTED_SUBJECT,
								  SettingId::VOLUME_PLOT_SCALE);

			// Activate the plot and the flow baseline setting button
			flowVolumePlot_.activate();

			// Display the plot and the buttons which adjust it
			addDrawable(&flowVolumePlot_);

            flowScaleButton_.setX(PV_Y_SCALE_BUTTON_X_);
			flowScaleButton_.setY(PV_Y_SCALE_BUTTON_Y_);
			xVolumeScaleButton_.setX(PV_X_SCALE_BUTTON_X_);
			xVolumeScaleButton_.setY(PV_X_SCALE_BUTTON_Y_);


		}
		else
		{												// $[TI9]
			// since both the 'x' and 'y' pressure scales will be applicable
			// at the same time (they're both monitoring the same setting),
			// we need to explicitly clear one of them...
			removeDrawable(&xPressureScaleButton_);
			removeDrawable(&xVolumeScaleButton_);

		    // Plot #1 is NOT Pressure-vs-Volume...
			Boolean onlyOnePlot;		// Are we only displaying a single plot?

			// Check whether only one plot is to be displayed.  This happens
			// when: the Pressure-Volume plot is chosen, or the Plot 2 type is
			// NO_PLOT2 or the Plot 1 Type equals the Plot 2 Type.
			// $[01290] If the user selects the same plot parameter for both ...
			onlyOnePlot = (PLOT2_TYPE_VALUE == Plot2TypeValue::NO_PLOT2)  ||
							(PLOT1_TYPE_VALUE == PLOT2_TYPE_VALUE);
															// $[TI4], $[TI5]

			// OK, we have a time plot setup.  Check to see if only time plot 1 is
			// selected for display and if so then we set the height of plot 1 to
			// take advantage of the full subscreen, otherwise we fit this plot
			// into half the subscreen.  The Y Scale button to scale this plot must
			// move accordingly (it's centered in the height of time plot 1)
			if (onlyOnePlot)
			{												// $[TI10]
				// Only time plot 1 to be displayed, make it big.
				timePlot1_.setGridArea(TIME_PLOT_GRID_X_, TIME_PLOT_GRID_Y_,
							TIME_PLOT_GRID_WIDTH_, BIG_TIME_PLOT_GRID_HEIGHT_);
				timePlot1_.setHeight(BIG_TIME_PLOT_HEIGHT_);
				timePlot1_.setXUnitsString(MiscStrs::TIME_PLOT_UNITS,
										TIME_X_UNITS_X_, TIME_X_UNITS_BIG_PLOT_Y_);

				timeScaleButton_.setY(TIME_X_SCALE_BUTTON_1PLOT_Y_);
			}
			else
			{												// $[TI11]
				// We've two time plots so make time plot 1 small.
				timePlot1_.setGridArea(TIME_PLOT_GRID_X_, TIME_PLOT_GRID_Y_,
										TIME_PLOT_GRID_WIDTH_,
										SMALL_TIME_PLOT_GRID_HEIGHT_);
				timePlot1_.setHeight(SMALL_TIME_PLOT_HEIGHT_);
				timePlot1_.setXUnitsString(MiscStrs::TIME_PLOT_UNITS,
										TIME_X_UNITS_X_,
										TIME_X_UNITS_SMALL_PLOT_Y_);

				timeScaleButton_.setY(TIME_X_SCALE_BUTTON_2PLOTS_Y_);
			}

			// Check which kind of plot 1 we're displaying and attach it to the
			// appropriate waveform data value (pressure, flow or volume) and
			// display the corresponding units label.
			// $[01330] Both cmH2O and hecto-Pascals (hPa) units must be supported ...
			switch (PLOT1_TYPE_VALUE)
			{
			case Plot1TypeValue::PRESSURE_VS_TIME:			// $[TI12]
				timePlot1_.setYUnits(PatientDataId::CIRCUIT_PRESS_ITEM);

			    // $[PX01002] Add the prox symbol when PROX is enabled.
			    if (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_)
				{
					timePlot1_.setYUnitsString(
							GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
								MiscStrs::PROXIMAL_PRESSURE_PLOT_UNITS :
								MiscStrs::PROXIMAL_PRESSURE_PLOT_HPA_UNITS,
							TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);

				}
				else
				{
				
					timePlot1_.setYUnitsString(
							GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
								MiscStrs::PRESSURE_PLOT_UNITS :
								MiscStrs::PRESSURE_PLOT_HPA_UNITS,
																// $[TI13], $[TI14]
							TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);

				}
				break;

			case Plot1TypeValue::FLOW_VS_TIME:				// $[TI15]
				timePlot1_.setYUnits(PatientDataId::NET_FLOW_ITEM);
				if (onlyOnePlot)
				{											// $[TI16]

					// $[PX01002] Add the prox symbol when PROX is enabled.
					if (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_)
					{
						timePlot1_.setYUnitsString(MiscStrs::PROXIMAL_BIG_FLOW_PLOT_UNITS,
													TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
					}
					else
					{
						timePlot1_.setYUnitsString(MiscStrs::BIG_FLOW_PLOT_UNITS,
													TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);

					}
				}
				else
				{											// $[TI17]
					// $[PX01002] Add the prox symbol when PROX is enabled.
					if (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_)
					{
						timePlot1_.setYUnitsString(MiscStrs::PROXIMAL_SMALL_FLOW_PLOT_UNITS,
													TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
					}
					else
					{
						timePlot1_.setYUnitsString(MiscStrs::SMALL_FLOW_PLOT_UNITS,
													TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
					}
				}
				break;

			case Plot1TypeValue::VOLUME_VS_TIME:			// $[TI18]
				timePlot1_.setYUnits(PatientDataId::NET_VOL_ITEM);

			    // $[PX01002] Add the prox symbol when PROX is enabled.
		     	if (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_)
				{
					timePlot1_.setYUnitsString(MiscStrs::PROXIMAL_VOLUME_PLOT_UNITS,
												TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
				}
				else
				{
					timePlot1_.setYUnitsString(MiscStrs::VOLUME_PLOT_UNITS,
												TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
				}
				break;

			case Plot1TypeValue::PRESSURE_VS_VOLUME:
			default:
				// unexpected plot #1 type...
				AUX_CLASS_ASSERTION_FAILURE(PLOT1_TYPE_VALUE);
				break;
			}

			// Check if time plot 2 must be displayed
			if (!onlyOnePlot)
			{												// $[TI19]
				// It sure does so setup the waveform data value and Y unit label
				// for it also.
				switch (PLOT2_TYPE_VALUE)
				{
				case Plot2TypeValue::PRESSURE_VS_TIME:		// $[TI20]
					timePlot2_.setYUnits(PatientDataId::CIRCUIT_PRESS_ITEM);

				    // $[PX01002] Add the prox symbol when PROX is enabled.
				    if (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_)
					{
						timePlot2_.setYUnitsString(
								GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
									MiscStrs::PROXIMAL_PRESSURE_PLOT_UNITS :
									MiscStrs::PROXIMAL_PRESSURE_PLOT_HPA_UNITS,
																	// $[TI21], $[TI22]
								TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
					}
					else
					{
						timePlot2_.setYUnitsString(
								GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
									MiscStrs::PRESSURE_PLOT_UNITS :
									MiscStrs::PRESSURE_PLOT_HPA_UNITS,
																	// $[TI21], $[TI22]
								TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
					}

					break;

				case Plot2TypeValue::FLOW_VS_TIME:			// $[TI23]
					timePlot2_.setYUnits(PatientDataId::NET_FLOW_ITEM);

			    	// $[PX01002] Add the prox symbol when PROX is enabled.
				    if (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_)
					{
						timePlot2_.setYUnitsString(MiscStrs::PROXIMAL_SMALL_FLOW_PLOT_UNITS,
											TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
					}
					else
					{
						timePlot2_.setYUnitsString(MiscStrs::SMALL_FLOW_PLOT_UNITS,
											TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
					}


					break;

				case Plot2TypeValue::VOLUME_VS_TIME:		// $[TI24]
					timePlot2_.setYUnits(PatientDataId::NET_VOL_ITEM);

				    // $[PX01002] Add the prox symbol when PROX is enabled.
				    if (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_)
					{
						timePlot2_.setYUnitsString(MiscStrs::PROXIMAL_VOLUME_PLOT_UNITS,
											TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
					}
					else
					{
						timePlot2_.setYUnitsString(MiscStrs::VOLUME_PLOT_UNITS,
											TIME_Y_UNITS_X_, TIME_Y_UNITS_Y_);
					}
					break;

				case Plot2TypeValue::WOB_GRAPHIC:			// $[TI19.1]
					wobGraphic_.setY(PLOT2_Y_);
					wobGraphic_.setHeight(SMALL_TIME_PLOT_HEIGHT_);
					break;

				default:
					AUX_CLASS_ASSERTION_FAILURE(PLOT2_TYPE_VALUE);	// unexpected plot type
					break;
				}
			}												// $[TI25]

			// Scale the plot(s) by pretending the X and Y scale settings have
			// changed.
			nonBatchSettingUpdate(Notification::ACCEPTED, P_ACCEPTED_SUBJECT,
								  SettingId::TIME_PLOT_SCALE);

			if ((PLOT1_TYPE_VALUE == Plot1TypeValue::PRESSURE_VS_TIME)
				|| (PLOT2_TYPE_VALUE == Plot2TypeValue::PRESSURE_VS_TIME))
			{												// $[TI26]
				nonBatchSettingUpdate(Notification::ACCEPTED,
									  P_ACCEPTED_SUBJECT,
									  SettingId::PRESSURE_PLOT_SCALE);
			}												// $[TI27]
			
			if ((PLOT1_TYPE_VALUE == Plot1TypeValue::VOLUME_VS_TIME)
				|| (PLOT2_TYPE_VALUE == Plot2TypeValue::VOLUME_VS_TIME))
			{												// $[TI28]
				nonBatchSettingUpdate(Notification::ACCEPTED,
									  P_ACCEPTED_SUBJECT,
									  SettingId::VOLUME_PLOT_SCALE);
			}
															// $[TI29]
			if ((PLOT1_TYPE_VALUE == Plot1TypeValue::FLOW_VS_TIME)
				|| (PLOT2_TYPE_VALUE == Plot2TypeValue::FLOW_VS_TIME))
			{												// $[TI30]
				nonBatchSettingUpdate(Notification::ACCEPTED,
									  P_ACCEPTED_SUBJECT,
									  SettingId::FLOW_PLOT_SCALE);
			}												// $[TI31]

			// Find out the correct scale button that should be displayed for
			// time plot 1.
			switch (PLOT1_TYPE_VALUE)
			{
			case Plot1TypeValue::PRESSURE_VS_TIME:			// $[TI32]
				pButton = &yPressureScaleButton_;
				break;

			case Plot1TypeValue::VOLUME_VS_TIME:			// $[TI33]
				pButton = &volumeScaleButton_;
				break;

			case Plot1TypeValue::FLOW_VS_TIME:				// $[TI34]
				pButton = &flowScaleButton_;
				break;

			default:
				AUX_CLASS_ASSERTION_FAILURE(PLOT1_TYPE_VALUE);	// unexpected plot type
				break;
			}

			// Position the scale button according to whether time plot 1 takes
			// up the full subscreen height or only half the height.
			if (onlyOnePlot)
			{												// $[TI35]
				pButton->setX(TIME_Y_SCALE_BUTTON_X_);
				pButton->setY(TIME_Y_SCALE_BUTTON_BIG_PLOT1_Y_);
			}
			else
			{												// $[TI36]
				pButton->setX(TIME_Y_SCALE_BUTTON_X_);
				pButton->setY(TIME_Y_SCALE_BUTTON_SMALL_PLOT1_Y_);
			}

			timePlot1_.activate();				// Activate the plot!

			// If we're in the frozen state and visible then erase and
			// redraw the frozen plot.
			if (isFrozen_ && isVisible())
			{				
				timePlot1_.erase();
				timePlot1_.update(frozenTimePlotIter_);
			}												// $[TI38]

			// add time plot #1...
			addDrawable(&timePlot1_);

			// Do we have to display a second time plot?		
			if (!onlyOnePlot)
			{												// $[TI39]
				// Check which scale button needs to be displayed.
				switch (PLOT2_TYPE_VALUE)
				{
				case Plot2TypeValue::PRESSURE_VS_TIME:		// $[TI40]
					pButton = &yPressureScaleButton_;
					break;

				case Plot2TypeValue::VOLUME_VS_TIME:		// $[TI41]
					pButton = &volumeScaleButton_;
					break;

				case Plot2TypeValue::FLOW_VS_TIME:			// $[TI42]
					pButton = &flowScaleButton_;
					break;

	           // $[PA01011] -- Work-of-breathing plots shall only be selectable 
               //            when PA is the Spontaneous Type and for Plot 2 only
				case Plot2TypeValue::WOB_GRAPHIC:			// $[TI39.1]
					pButton = NULL; // in order to allow WOB graph to show up.
					break;

				default:
					// Uh oh, unexpected plot type...
					AUX_CLASS_ASSERTION_FAILURE(PLOT2_TYPE_VALUE);
					break;
				}

				if (pButton != NULL)
				{	// $[TI98]
					// Position and display button
					pButton->setX(TIME_Y_SCALE_BUTTON_X_);
					pButton->setY(TIME_Y_SCALE_BUTTON_PLOT2_Y_);

					timePlot2_.activate();				// Activate the plot!

					// Draw the frozen plot if needed.
					if (isFrozen_ && isVisible())
					{											// $[TI43]
						timePlot2_.erase();
						timePlot2_.update(frozenTimePlotIter_);
					}											// $[TI44]

					// now that plot 2 is updated, add it, thereby drawing it...
					addDrawable(&timePlot2_);
				}
				else
				{	// $[TI99]
					wobGraphic_.activate();

					// now that WOB graphic is updated, add it, thereby
					// drawing it...
					addDrawable(&wobGraphic_);
				}
			}												// $[TI45]
		}

		updateAuxInfoState_(currAuxInfoState_, TRUE); 

	}
	else
	{													// $[TI46]
		// We must layout the plot setup screen, quite easy.

		// need to add, then activate, all of the "setup" buttons, due to
		// the removal above...
		for (Uint idx = 0u; arrSetupBtnPtrs_[idx] != NULL; idx++)
		{
			// all of these buttons attach to this subscreen directly...
			addDrawable(arrSetupBtnPtrs_[idx]);
			(arrSetupBtnPtrs_[idx])->activate();
		}

		// Set subscreen background color
		setFillColor(Colors::MEDIUM_BLUE);

		// Add the Title Area, and the Continue button to get back to the
		// waveform display screen.
		addDrawable(&titleArea_);
		addDrawable(&continueButton_);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: freeze_				[Private]
//
//@ Interface-Description
// Handles the process of freezing waveforms.  If a full waveform is
// being displayed then we can freeze immediately.  If not then we
// enter the 'freezing' state until the end of the current waveform is
// reached.
//---------------------------------------------------------------------
//@ Implementation-Description
// Switch the Freeze button to displaying the flashing text 'UNFREEZE'.
// Then check whether a full waveform is being displayed.  If so then
// we must exit the 'freezing' state and if we are displaying a time plot
// then we find the last loop plot (see 'findLoopPlot_()').  If we
// are not at the end of a waveform then we must wait for the end so
// we go into the 'freezing' state (see 'setFreezingState_()').
//
// $[GC01002] The Waveforms subscreen shall provide a print button ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::freeze_(void)
{
	CALL_TRACE("WaveformsSubScreen::freeze_(void)");

	// Switch the Freeze button to displaying 'UNFREEZE', flashing.
	// $[01338] The label on the button used to activate the freeze ...
	freezeButton_.setTitleText(MiscStrs::FREEZE_BUTTON_UNFREEZE_TITLE);
	freezeButton_.setTitleColor(Colors::BLACK_ON_LIGHT_GREY_BLINK);

	// Check to see if a full waveform is being displayed
	if(!isStartOfWaveformFound_)
	{														// $[TI1]
		//  gdc - 6-Jul-98
		//  guiEventHappened no longer resets or stops the waveform data or
		//  iterators so the waveform data continues to be received during
		//  an occlusion or disconnect and the iterators remain valid
		if(frozenTimePlotIter_.hasData())
		{													// $[TI2]
			// It is so we enter the frozen state, exit the freezing state and
			// set the frozen time plot iterator to match the currently
			// displayed plot.  Note that the current waveform data has already
			// been stored in the frozen data buffer in the update() method.
			isFrozen_ = TRUE;
			setFreezingState_(FALSE);
			frozenTimePlotIter_.sync(currentFullPlotIter_);

			// If we are displaying time plots then let's search back for the
			// last breath in the frozen data.  This will be the data that the
			// Pressure-Volume plot displays if the user switches to that
			// plot while in the frozen state.
			if (timePlot1_.isVisible())
			{												// $[TI3]
				findLoopPlot_();
			}
			else
			{												// $[TI4]
				// If we are currently displaying the Pressure-Volume plot then
				// just set the frozen loop plot iterator to be the same as
				// the current plot iterator.
				frozenLoopPlotIter_.sync(currentFullPlotIter_);
			}

			// Display the PRINT button if the com1 device is set to Printer.
			DiscreteValue com1DeviceSetting =
				AcceptedContextHandle::GetDiscreteValue(SettingId::COM1_CONFIG);
			printButton_.setShow(com1DeviceSetting == ComPortConfigValue::PRINTER_VALUE);
		}
		else
		{													// $[TI5]
			setFreezingState_(FALSE);
			isFrozen_ = FALSE;
			freezeButton_.setTitleText(MiscStrs::FREEZE_BUTTON_FREEZE_TITLE);
			freezeButton_.setTitleColor(Colors::BLACK);
		}
	}
	else
	{														// $[TI6]
		// We're still displaying waveforms so we can't freeze just yet.
		// Enter the freezing state.
		setFreezingState_(TRUE);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: unFreeze_
//
//@ Interface-Description
// Unfreezes waveforms.  Clears the waveforms, resets the freeze button
// to displaying 'FREEZE' and hides it because freezing waveforms is not
// allowed while waiting for the beginning of the next plot.  If currently
// in the 'freezing' state then exit that state.  Remove Intrinsic and
// Total PEEP values if being displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Do the above.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::unFreeze_(void)
{
	CALL_TRACE("WaveformsSubScreen::unFreeze_(void)");

	// Not in the frozen state anymore
	isFrozen_ = FALSE;

	// Switch the Freeze button back to displaying 'FREEZE'.
	unFreezeButtons_();

	if (SerialInterface::IsPrintRequested())
	{
	// $[TI5]
		cancelPrintRequest();
	}
	// $[TI6]

	// Are we not in the freezing state?
	if (!isFreezing_)
	{													// $[TI1]
		// Not in the freezing state.  Clear all waveforms, and reset
		// the system to the state where we look for trigger to start plotting
		// again.
		timePlot1_.erase();
		timePlot2_.erase();
		pressureVolumePlot_.erase();
        flowVolumePlot_.erase();
		isStartOfWaveformFound_ = FALSE;
		isLastSampleExhalation_ = FALSE;
		currentFullPlotIter_.reset();
		currentSegmentIter_.beginInterval();
		currentSegmentIter_.endInterval();

		// Not allowed to freeze while not plotting so hide the freeze button.
		freezeButton_.setShow(FALSE);
	}
	else
	{													// $[TI2]
		// We were in the freezing state.  Exit it.
		setFreezingState_(FALSE);

		if (GuiApp::GetGuiState() != STATE_ONLINE ||
			GuiApp::IsSettingsLockedOut())
		{   											// $[TI3]
		    timePlot1_.erase();
		    timePlot2_.erase();
		    pressureVolumePlot_.erase();
            flowVolumePlot_.erase();
		    isStartOfWaveformFound_ = FALSE;
		    isLastSampleExhalation_ = FALSE;
		    currentFullPlotIter_.reset();
		    currentSegmentIter_.beginInterval();
		    currentSegmentIter_.endInterval();
		    // Not allowed to freeze while not plotting so hide the freeze button.
		    freezeButton_.setShow(FALSE);
		}												// $[TI4]
	}
	if ( PAV_ACTIVE_ == currAuxInfoState_)
    {
		isInspiratoryPauseState_ = FALSE;
		isExpiratoryPauseState_=   FALSE;
		isNifPauseState_=   FALSE;
		isP100PauseState_=   FALSE;
		isSvcPauseState_=   FALSE;
		isEndofExpPauseManeuver_=  FALSE;
	    removeDrawable(	&pauseCancelByAlarmText_);
	}
    else
    {
	    displayCompliance_(FALSE);
	    displayResistance_(FALSE);
	    displayPlateauPressure_( FALSE) ;
		isInspiratoryPauseState_ = FALSE;  		   
		isExpiratoryPauseState_=   FALSE;  		   
		isNifPauseState_=   FALSE;  		   
		isP100PauseState_=   FALSE;
		isSvcPauseState_=   FALSE;
		isEndofExpPauseManeuver_=  FALSE;  		   
	    removeDrawable(	&pauseCancelByAlarmText_);  
	    removeAuxInfo_(arrExpPausePtrs_);	 	   
    }
    
	if (currAuxInfoState_ == EXP_PAUSE_  
		|| currAuxInfoState_ == NIF_PAUSE_
		|| currAuxInfoState_ == P100_PAUSE_
		|| currAuxInfoState_ == VC_PAUSE_
		|| currAuxInfoState_ == INSP_PAUSE_)
	{
    	// $[TI5]
		// to remove pause data
		updateAuxInfoState_(lastAuxInfoState_);
	}	// $[TI6]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: unFreezeButtons_
//
//@ Interface-Description
// Resets the freeze button to displaying 'FREEZE', displays the plot
// setup button, blanks the print button and resets it to displaying
// 'PRINT' the next time it's displayed, and blanks the 'PRINTING'
// message.
//---------------------------------------------------------------------
//@ Implementation-Description
// Do the above.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
WaveformsSubScreen::unFreezeButtons_(void)
{
  CALL_TRACE("WaveformsSubScreen::unFreezeButtons_(void)");

  // $[01338] The label on the button used to activate the freeze ...
  // Switch the Freeze button back to displaying 'FREEZE'.
  freezeButton_.setTitleText(MiscStrs::FREEZE_BUTTON_FREEZE_TITLE);
  freezeButton_.setTitleColor(Colors::BLACK);
  plotSetupButton_.setShow(TRUE);
  printButton_.setShow(FALSE);
  printButton_.setTitleText(MiscStrs::PRINT_BUTTON_TITLE);
  printButton_.setTitleColor(Colors::BLACK);            
  printingText_.setShow(FALSE);
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFreezingState_			[Private]
//
//@ Interface-Description
// If a request to freeze waveforms occurs before a full waveform has been
// plotted then we enter the 'freezing' state where access to all waveform
// display buttons except the (un)Freeze button is denied and the flashing text
// 'FREEZING' is displayed.  This method is passed a boolean value: if TRUE
// then the freezing state is entered, if FALSE the freezing state is exited
// and access to all buttons is re-enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the boolean value, and set the show flag of the freezing text to
// be that value and the show flag of the buttons to be the inverse of that.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::setFreezingState_(Boolean isFreezing)
{
	CALL_TRACE("WaveformsSubScreen::setFreezingState_(Boolean isFreezing)");

	// Store the new freezing state.
	isFreezing_ = isFreezing;

	// Show/hide the freezing text
	// $[01340] When the user has selected the freeze function, but ...
	freezingText_.setShow(isFreezing_);

	if (isFreezing_)
	{		// $[TI1]
		// freezing; hide all display setting buttons...
		operateOnButtons_(arrDisplayBtnPtrs_, &SettingButton::deactivate);
	}
	else
	{		// $[TI2]
		// not freezing; show appropriate set of display setting buttons...
		operateOnButtons_(arrDisplayBtnPtrs_, &SettingButton::activate);
	}

	// hide this button if freezing...
	plotSetupButton_.setShow(!isFreezing_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: findLoopPlot_				[Private]
//
//@ Interface-Description
// When displaying a time plot that has just been frozen we need to search
// back through the previous 48 seconds of data and look for the last
// breath and store its location.  This function does that and it is needed
// so that if the user switches to displaying the Pressure-Volume plot
// that it can immediately display the appropriate frozen breath.
//---------------------------------------------------------------------
//@ Implementation-Description
// Make a temporary iterator that spans from the latest sample back
// 48 seconds (if that much data is available).  Then search from the end
// of the interval backwards to find the last complete breath.  What to
// look for is a transition from exhalation to inspiration.  The last two
// such transitions mark the beginning and end of the last breath.  We
// store the last breath interval in the member 'frozenLoopPlotIter_'.
//
// Unexpected conditions are handled such as not being able to find one
// exhalation to inspiration transition.  In this case 'frozenLoopPlotIter_'
// is set to be an empty interval such that no loop data will be displayed.
// If only one exhalation to inspiration transition is found then
// the breath interval is set to be all the data from that transition to
// the latest sample collected, so the loop plot will display as much of
// the last partial breath as possible.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::findLoopPlot_(void)
{
	CALL_TRACE("WaveformsSubScreen::findLoopPlot_(void)");
	WaveformIntervalIter maxPlotIter;			// Temporary 48 second interval
	Boolean isLastSampleInspiration = FALSE;	// Flags to help search for
	Boolean isCurrSampleExhalation;				//  e to i transitions
	SigmaStatus status;							// Iterator status


	// Okay, set maxPlotIter to be the latest 48 seconds of data or all of the
	// data if less than 48 seconds of data has been collected.
	maxPlotIter.attachToBuffer(&currentDataBuffer_);
	maxPlotIter.sync(currentFullPlotIter_);
	maxPlotIter.goLast();
	//  This condition was added due to assertion caused by a higher priority 
	//  task gaining control of the CPU and invalidating the waveform data and 
	//  WaveformDataBuffer iterators.  Example... the freeze button is pressed 
	//  just prior to an occlusion.  The Occlusion has the higher priority and
	//  clears the buffer and resets the iterators then, returns control back to 
	//  the freeze function which calls findLoopPlot_ with  invalid data.
	if(!maxPlotIter.hasData())
	{														// $[TI1]
		maxPlotIter.reset();
	}
	else
	{														// $[TI2]
		maxPlotIter.beginIntervalAtOffset(- MAX_TIME_INTERVAL_ * 1000);

		// First, scan backwards for the end of the last full breath.
		// We have to look for a transition between inspiration and
		// exhalation.
		for (status = maxPlotIter.goLast(); SUCCESS == status;
												status = maxPlotIter.goPrev())
		{
			isCurrSampleExhalation =
					maxPlotIter.getDiscreteValue(PatientDataId::BREATH_PHASE_ITEM)
												!= BreathPhaseType::INSPIRATION;

			if (isLastSampleInspiration && isCurrSampleExhalation)
			{												// $[TI3]
				// Found first transition, remember it as the end of the interval
				frozenLoopPlotIter_.syncLastWithCurrent(maxPlotIter);
				break;
			}												// $[TI4]

			isLastSampleInspiration = !isCurrSampleExhalation;
		}

		// Check to see if we found first transition
		if (FAILURE == status)
		{													// $[TI5]
			// Oh no, we searched through all the data and couldn't find one
			// transition between inspiration and expiration.  Reset the frozen
			// loop iterator thereby "emptying" it.
			frozenLoopPlotIter_.reset();
		}
		else
		{													// $[TI6]
			// Now, continue searching backwards for another transition
			// between inspiration and exhalation.  If found this will be
			// the beginning of the last full breath.
			isLastSampleInspiration = FALSE;

			for (status = maxPlotIter.goPrev();
								SUCCESS == status; status = maxPlotIter.goPrev())
			{												// $[TI7]
				isCurrSampleExhalation =
					maxPlotIter.getDiscreteValue(PatientDataId::BREATH_PHASE_ITEM)
												!= BreathPhaseType::INSPIRATION;

				if (isLastSampleInspiration && isCurrSampleExhalation)
				{											// $[TI8]
					// Found second transition.  We've gone one sample beyond
					// the start so go back and then store this point as the
					// start of the loop iterator.  We're done, we found it.
					maxPlotIter.goNext();
					frozenLoopPlotIter_.syncFirstWithCurrent(maxPlotIter);
					break;
				}											// $[TI9]

				isLastSampleInspiration = !isCurrSampleExhalation;
			}												// $[TI10]

			// Check to see if we found the second transition.
			if (FAILURE == status)
			{												// $[TI11]
				// Could only find one exhalation to inspiration transition.
				// The last sample of the 'frozenLoopPlotIter_' is pointing
				// at the exhalation sample at the only exhalation to inspiration
				// transition.  The first sample must be made to be the sample
				// after that one and the last sample to be the latest sample
				// in the current data buffer.  Here's how it's done.
				frozenLoopPlotIter_.goLast();
				frozenLoopPlotIter_.syncFirstWithCurrent(frozenLoopPlotIter_);
				frozenLoopPlotIter_.syncLast(maxPlotIter);
				frozenLoopPlotIter_.goFirst();
				frozenLoopPlotIter_.goNext();
				frozenLoopPlotIter_.syncFirstWithCurrent(frozenLoopPlotIter_);
			}												// $[TI12]
		}
	}  // end of 'else' ([TI2])...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: redrawPlot_
//
//@ Interface-Description
// Takes care of redrawing a plot.  Passed a reference to the plot.
// Called, for example, when a time plot's Y scale has been changed or
// when any frozen plot is rescaled or is displayed for the first time.
//---------------------------------------------------------------------
//@ Implementation-Description
// We first erase the plot.  Then, if the plot is frozen, we redraw
// the plot with the appropriate frozen iterator otherwise we redraw
// it with the current plot iterator.  Then, if we ended up drawing
// a full plot the plot is informed that we've reached the end of the plot.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::redrawPlot_(WaveformPlot &rPlot)
{
	CALL_TRACE("WaveformsSubScreen::redrawPlot_(WaveformPlot &rPlot)");
    // Disable redrawing the plots if needed.
    if(!updatePlot_)
    {
        return;
    }

	// Erase the plot
	rPlot.erase();

	// If in the frozen state use the frozen iterators
	if (isFrozen_)
	{													// $[TI1]
		// If the plot is the Pressure-Volume plot then draw it with the
		// frozen loop plot iterator else use the frozen time plot iterator.
		if ((&rPlot == &pressureVolumePlot_) ||
            (&rPlot == &flowVolumePlot_))
		{												// $[TI2]
			rPlot.update(frozenLoopPlotIter_);
		}
		else
		{												// $[TI3]
			rPlot.update(frozenTimePlotIter_);
		}
	}
	else
	{													// $[TI4]
		// Use the current plot iterator
		rPlot.update(currentFullPlotIter_);
	}

	// Check to see if we were drawing a full waveform
	if (isFrozen_ || !isStartOfWaveformFound_)
	{													// $[TI5]
		// We were so inform the plot so it can do tidy up
		rPlot.endPlot();
	}													// $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateAuxInfoState_()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::updateAuxInfoState_(const AuxInfoState_ newAuxInfoState,
										const Boolean       forceStateChange)
{
	CALL_TRACE("updateAuxInfoState_(newAuxInfoState, forceStateChange)");

	const Boolean  IS_CHANGE_OF_STATE = (forceStateChange  ||
										 newAuxInfoState != currAuxInfoState_);

	switch (newAuxInfoState)
	{
	case STANDARD_ :		// $[TI1]
		lastAuxInfoState_ = STANDARD_;

		switch (currAuxInfoState_)
		{
		case STANDARD_ :
			// do nothing...
			break;
		case EXP_PAUSE_ :		// $[TI1.1]
			removeAuxInfo_(arrExpPausePtrs_);
			break;
		case NIF_PAUSE_ :
			removeAuxInfo_(arrNifPausePtrs_);
			break;
		case P100_PAUSE_ :
			removeAuxInfo_(arrP100PausePtrs_);
			break;
		case VC_PAUSE_ :
			removeAuxInfo_(arrSvcPausePtrs_);
			break;
		case INSP_PAUSE_ :		// $[TI1.2]
			removeAuxInfo_(arrInspPauseResistancePtrs_);
            removeAuxInfo_(arrInspPauseCompliancePtrs_);
            removeAuxInfo_(arrInspPausePlateauPtrs_);            
			break;
		case VCP_ACTIVE_ :		
			removeAuxInfo_(arrVcpPtrs_);
			break;
		case VS_ACTIVE_  :		
			removeAuxInfo_(arrVsPtrs_);
			break;
		case PAV_ACTIVE_ :		// $[TI1.3]
			removeAuxInfo_(arrPavPtrs_);
			break;
		default :
			// unexpected value for 'currAuxInfoState_'...
			AUX_CLASS_ASSERTION_FAILURE(newAuxInfoState);
			break;
		};
		break;

	case EXP_PAUSE_ :		// $[TI2]
		// need this block for the stability data item defined within...
		{   // begin block...
			switch (currAuxInfoState_)
			{
			case STANDARD_ :		// $[TI2.1]
				lastAuxInfoState_ = STANDARD_;
				break;
			case EXP_PAUSE_ :
				// do nothing...
				break;
			case INSP_PAUSE_ :		// $[TI2.2]
				removeAuxInfo_(arrInspPauseResistancePtrs_);
                removeAuxInfo_(arrInspPauseCompliancePtrs_);
                removeAuxInfo_(arrInspPausePlateauPtrs_);
				break;
			case PAV_ACTIVE_ :		// $[TI2.3]
				lastAuxInfoState_ = PAV_ACTIVE_;
				removeAuxInfo_(arrPavPtrs_);
				break;
			case VCP_ACTIVE_ :		
				lastAuxInfoState_ = VCP_ACTIVE_;
				removeAuxInfo_(arrVcpPtrs_);
				break;
			case VS_ACTIVE_ :		
				lastAuxInfoState_ = VS_ACTIVE_;
				removeAuxInfo_(arrVsPtrs_);
				break;
			default :
				// unexpected value for 'currAuxInfoState_'...
				AUX_CLASS_ASSERTION_FAILURE(newAuxInfoState);
				break;
			};

			if (IS_CHANGE_OF_STATE)
			{	// $[TI2.4]
				// add EXP PAUSE drawables...
				addAuxInfo_(arrExpPausePtrs_);

				intrinsicPeepSymbol_.setMessage(
										MiscStrs::TOUCHABLE_INTRINSIC_PEEP_MSG
											   );
				intrinsicPeepSymbol_.setX(::INTRINSIC_PEEP_SYMBOL_X_);
				intrinsicPeepValue_.setX(::INTRINSIC_PEEP_VALUE_X_);
				intrinsicPeepUnit_.setX(::INTRINSIC_PEEP_UNIT_X_);
			
				// show initial artifacts...
				intrinsicPeepSymbol_.setShow(TRUE);
				intrinsicPeepUnit_.setShow(TRUE);
				totalPeepSymbol_.setShow(TRUE);
				totalPeepUnit_.setShow(TRUE);

                // Normally these values are set and shown in
                // patientDataChangeHappened(), so only show
                // here if we are frozen..
                if (isFrozen_)
                {
                   intrinsicPeepValue_.setShow(TRUE);
                   totalPeepValue_.setShow(TRUE);
                }
			}	// $[TI2.5]
            	
            // the flag indicates that we have gotten an END_EXP_PAUSE_PRESSURE_STATE_ITEM
            // so now it is appropriate to check the data for unstable values.
            if (isEndofExpPauseManeuver_)
            {
               const Int32  PRESSURE_STABILITY =
				BreathDatumHandle(PatientDataId::END_EXP_PAUSE_PRESSURE_STATE_ITEM).
									getDiscreteValue().data;

			   if (PRESSURE_STABILITY == PauseTypes::UNSTABLE)
			   {   // $[TI2.6]	
			       // $[01358] A visual indication ...
			       intrinsicPeepMarkerText_.setShow(TRUE);
			       totalPeepMarkerText_.setShow(TRUE);                  
			       peepStabilityProblemLabel_.setShow(TRUE);
			       peepStabilityProblemLabel_.setText(MiscStrs::NO_PLATEAU_LABEL);
			   }   // $[TI2.7]
               else
               {
                   intrinsicPeepMarkerText_.setShow(FALSE);
			       totalPeepMarkerText_.setShow(FALSE);               
			       peepStabilityProblemLabel_.setShow(FALSE);
               }
            }
            else
            {
                intrinsicPeepMarkerText_.setShow(FALSE);
			    totalPeepMarkerText_.setShow(FALSE);               
			    peepStabilityProblemLabel_.setShow(FALSE);
            }
		}   // end block...
		break;


	case NIF_PAUSE_ :
		{
			switch (currAuxInfoState_)
			{
			case STANDARD_ :		// $[TI2.1]
				lastAuxInfoState_ = STANDARD_;
				break;
			case EXP_PAUSE_ :
				removeAuxInfo_(arrExpPausePtrs_);
				break;
			case NIF_PAUSE_ :
				// do nothing...
				break;
			case P100_PAUSE_ :
				removeAuxInfo_(arrP100PausePtrs_);
				break;
			case VC_PAUSE_ :
				removeAuxInfo_(arrSvcPausePtrs_);
				break;
			case INSP_PAUSE_ :
				removeAuxInfo_(arrInspPauseResistancePtrs_);
				removeAuxInfo_(arrInspPauseCompliancePtrs_);
				removeAuxInfo_(arrInspPausePlateauPtrs_);
				break;
			case PAV_ACTIVE_ :
				lastAuxInfoState_ = PAV_ACTIVE_;
				removeAuxInfo_(arrPavPtrs_);
				break;
			case VCP_ACTIVE_ :		
				lastAuxInfoState_ = VCP_ACTIVE_;
				removeAuxInfo_(arrVcpPtrs_);
				break;
			case VS_ACTIVE_ :		
				lastAuxInfoState_ = VS_ACTIVE_;
				removeAuxInfo_(arrVsPtrs_);
				break;
			default :
				// unexpected value for 'currAuxInfoState_'...
				AUX_CLASS_ASSERTION_FAILURE(newAuxInfoState);
				break;
			};

			if (IS_CHANGE_OF_STATE)
			{
				// add NIF PAUSE drawables...
				addAuxInfo_(arrNifPausePtrs_);

				// show initial artifacts...
				nifPressureSymbol_.setShow(TRUE);
				nifPressureUnit_.setShow(TRUE);

				// Normally these values are set and shown in
				// patientDataChangeHappened(), so only show
				// here if we are frozen..
				if (isFrozen_)
				{
				   nifPressureValue_.setShow(TRUE);
				}
			}
		}   // end block...
		break;


	case P100_PAUSE_ :
		{
			switch (currAuxInfoState_)
			{
			case STANDARD_ :
				lastAuxInfoState_ = STANDARD_;
				break;
			case EXP_PAUSE_ :
				removeAuxInfo_(arrExpPausePtrs_);
				break;
			case NIF_PAUSE_ :
				removeAuxInfo_(arrNifPausePtrs_);
				break;
			case P100_PAUSE_ :
				// do nothing...
				break;
			case VC_PAUSE_ :
				removeAuxInfo_(arrSvcPausePtrs_);
				break;
			case INSP_PAUSE_ :
				removeAuxInfo_(arrInspPauseResistancePtrs_);
				removeAuxInfo_(arrInspPauseCompliancePtrs_);
				removeAuxInfo_(arrInspPausePlateauPtrs_);
				break;
			case PAV_ACTIVE_ :
				lastAuxInfoState_ = PAV_ACTIVE_;
				removeAuxInfo_(arrPavPtrs_);
				break;
			case VCP_ACTIVE_ :		
				lastAuxInfoState_ = VCP_ACTIVE_;
				removeAuxInfo_(arrVcpPtrs_);
				break;
			case VS_ACTIVE_ :		
				lastAuxInfoState_ = VS_ACTIVE_;
				removeAuxInfo_(arrVsPtrs_);
				break;
			default :
				// unexpected value for 'currAuxInfoState_'...
				AUX_CLASS_ASSERTION_FAILURE(newAuxInfoState);
				break;
			};

			if (IS_CHANGE_OF_STATE)
			{
				// add P100 PAUSE drawables...
				addAuxInfo_(arrP100PausePtrs_);

				// show initial artifacts...
				p100PressureSymbol_.setShow(TRUE);
				p100PressureUnit_.setShow(TRUE);

				// Normally these values are set and shown in
				// patientDataChangeHappened(), so only show
				// here if we are frozen..
				if (isFrozen_)
				{
				   p100PressureValue_.setShow(TRUE);
				}
			}
		}   // end block...
		break;

	case VC_PAUSE_ :
		{
			switch (currAuxInfoState_)
			{
			case STANDARD_ :
				lastAuxInfoState_ = STANDARD_;
				break;
			case EXP_PAUSE_ :
				removeAuxInfo_(arrExpPausePtrs_);
				break;
			case NIF_PAUSE_ :
				removeAuxInfo_(arrNifPausePtrs_);
				break;
			case P100_PAUSE_ :
				removeAuxInfo_(arrP100PausePtrs_);
				break;
			case VC_PAUSE_ :
				// do nothing
				break;
			case INSP_PAUSE_ :
				removeAuxInfo_(arrInspPauseResistancePtrs_);
				removeAuxInfo_(arrInspPauseCompliancePtrs_);
				removeAuxInfo_(arrInspPausePlateauPtrs_);
				break;
			case PAV_ACTIVE_ :
				lastAuxInfoState_ = PAV_ACTIVE_;
				removeAuxInfo_(arrPavPtrs_);
				break;
			case VCP_ACTIVE_ :		
				lastAuxInfoState_ = VCP_ACTIVE_;
				removeAuxInfo_(arrVcpPtrs_);
				break;
			case VS_ACTIVE_ :		
				lastAuxInfoState_ = VS_ACTIVE_;
				removeAuxInfo_(arrVsPtrs_);
				break;
			default :
				// unexpected value for 'currAuxInfoState_'...
				AUX_CLASS_ASSERTION_FAILURE(newAuxInfoState);
				break;
			};

			if (IS_CHANGE_OF_STATE)
			{
				// add NIF PAUSE drawables...
				addAuxInfo_(arrSvcPausePtrs_);

				// show initial artifacts...
				vitalCapacitySymbol_.setShow(TRUE);
				vitalCapacityUnit_.setShow(TRUE);

				// Normally these values are set and shown in
				// patientDataChangeHappened(), so only show
				// here if we are frozen..
				if (isFrozen_)
				{
				   vitalCapacityValue_.setShow(TRUE);
				}
			}
		}   // end block...
		break;

	case INSP_PAUSE_ :		// $[TI3]
		{   // begin block...
			switch (currAuxInfoState_)
			{
			case STANDARD_ :		// $[TI3.1]
				lastAuxInfoState_ = STANDARD_;
				break;
			case EXP_PAUSE_ :		// $[TI3.2]
				removeAuxInfo_(arrExpPausePtrs_);
				break;
			case NIF_PAUSE_ :
				removeAuxInfo_(arrNifPausePtrs_);
				break;
			case P100_PAUSE_ :
				removeAuxInfo_(arrP100PausePtrs_);
				break;
			case VC_PAUSE_ :
				removeAuxInfo_(arrSvcPausePtrs_);
				break;
			case INSP_PAUSE_ :
				// do nothing...
				break;
			case PAV_ACTIVE_ :		// $[TI3.3]
				lastAuxInfoState_ = PAV_ACTIVE_;
				removeAuxInfo_(arrPavPtrs_);
				break;
			case VCP_ACTIVE_ :		
				lastAuxInfoState_ = VCP_ACTIVE_;
				removeAuxInfo_(arrVcpPtrs_);
				break;
			case VS_ACTIVE_ :		
				lastAuxInfoState_ = VS_ACTIVE_;
				removeAuxInfo_(arrVsPtrs_);
				break;
			default :
				// unexpected value for 'currAuxInfoState_'...
				AUX_CLASS_ASSERTION_FAILURE(newAuxInfoState);
				break;
			};

			// start out hiding error state displays...
			lungComplianceMarkerText_.setShow(FALSE);
			staticComplianceInadequateText_.setShow(FALSE);
			airwayResistanceMarkerText_.setShow(FALSE);
			staticResistanceInadequateText_.setShow(FALSE);
			staticResistanceNotAvailableText_.setShow(FALSE);

			if (IS_CHANGE_OF_STATE)
			{	// $[TI3.4]
				// add INSP PAUSE drawables...
				addAuxInfo_(arrInspPauseResistancePtrs_);
                addAuxInfo_(arrInspPauseCompliancePtrs_);
                addAuxInfo_(arrInspPausePlateauPtrs_);
                
			
				// show initial artifacts...
				plateauPressureSymbol_.setShow(TRUE);
				plateauPressureUnit_.setShow(TRUE);

				lungComplianceSymbol_.setText(MiscStrs::TOUCHABLE_STATIC_COMPLIANCE_LABEL);
				lungComplianceSymbol_.setMessage(MiscStrs::TOUCHABLE_STATIC_COMPLIANCE_MSG);

				airwayResistanceSymbol_.setText(MiscStrs::TOUCHABLE_STATIC_RESISTANCE_LABEL);
				airwayResistanceSymbol_.setMessage(MiscStrs::TOUCHABLE_STATIC_RESISTANCE_MSG);
               
                if(isFrozen_)
                {
                    displayPlateauPressure_(TRUE);
                    displayCompliance_(TRUE);
                    displayResistance_(TRUE);
                }                
			}
            else  // !IS_CHANGE_OF_STATE
            {
                displayPlateauPressure_(isPlateauPressureDisplayed_);
                displayCompliance_(areComplianceDisplayed_);
                displayResistance_(areResistanceDisplayed_);
            }
           
		}   // end block...
		break;

	case PAV_ACTIVE_ :		// $[TI4]
		lastAuxInfoState_ = currAuxInfoState_;

		// remove "cancel" message...
		removeDrawable(&pauseCancelByAlarmText_);

		switch (currAuxInfoState_)
		{
		case STANDARD_ :		// $[TI4.1]
			// do nothing...
			break;
		case EXP_PAUSE_ :		// $[TI4.2]
			removeAuxInfo_(arrExpPausePtrs_);
			break;
		case NIF_PAUSE_ :
			removeAuxInfo_(arrNifPausePtrs_);
			break;
		case P100_PAUSE_ :
			removeAuxInfo_(arrP100PausePtrs_);
			break;
		case VC_PAUSE_ :
			removeAuxInfo_(arrSvcPausePtrs_);
			break;
		case INSP_PAUSE_ :		// $[TI4.3]
			removeAuxInfo_(arrInspPauseResistancePtrs_);
            removeAuxInfo_(arrInspPauseCompliancePtrs_);
            removeAuxInfo_(arrInspPausePlateauPtrs_);
			break;
		case PAV_ACTIVE_ :
			// do nothing...
			break;
		case VCP_ACTIVE_ :		
			removeAuxInfo_(arrVcpPtrs_);
			break;
		case VS_ACTIVE_ :		
			removeAuxInfo_(arrVsPtrs_);
			break;
		default :
			// unexpected value for 'currAuxInfoState_'...
			AUX_CLASS_ASSERTION_FAILURE(newAuxInfoState);
			break;
		};

		if (IS_CHANGE_OF_STATE)
		{												// $[TI4.4]
			if (isOnWaveDisplay_)
			{	// $[TI4.4.1]
				// add PAV drawables...
				addAuxInfo_(arrPavPtrs_);
			}	// $[TI4.4.2]
			
			// show initial artifacts...
			lungComplianceSymbol_.setShow(arePavSymbolsAllowed_);
			lungComplianceSymbol_.setText(MiscStrs::TOUCHABLE_PAV_COMPLIANCE_WF_LABEL);
			lungComplianceSymbol_.setMessage(MiscStrs::TOUCHABLE_PAV_COMPLIANCE_MSG);
			// only show if allowed, and already shown...
			lungComplianceValue_.setShow(arePavValuesAllowed_  &&
										 lungComplianceValue_.getShow());
			lungComplianceUnit_.setShow(arePavValuesAllowed_  &&
										 lungComplianceUnit_.getShow());
			lungComplianceMarkerText_.setShow(arePavValuesAllowed_  &&
										  lungComplianceMarkerText_.getShow());

			airwayResistanceSymbol_.setShow(arePavSymbolsAllowed_);
			airwayResistanceSymbol_.setText(MiscStrs::TOUCHABLE_PAV_RESISTANCE_WF_LABEL);
			airwayResistanceSymbol_.setMessage(MiscStrs::TOUCHABLE_PAV_RESISTANCE_MSG);
			// only show if allowed, and already shown...
			airwayResistanceValue_.setShow(arePavValuesAllowed_  &&
										   airwayResistanceValue_.getShow());
			airwayResistanceUnit_.setShow(arePavValuesAllowed_  &&
										  airwayResistanceUnit_.getShow());
			airwayResistanceMarkerText_.setShow(arePavValuesAllowed_  &&
									  airwayResistanceMarkerText_.getShow());

			// only show if allowed, and already shown...
			intrinsicPeepSymbol_.setShow(arePavSymbolsAllowed_  &&
										 intrinsicPeepSymbol_.getShow());
			intrinsicPeepSymbol_.setMessage(
									MiscStrs::TOUCHABLE_PAV_INTRINSIC_PEEP_MSG
										   );
			intrinsicPeepSymbol_.setX(::COLUMN3_SYMBOL_X_);
			intrinsicPeepValue_.setX(::COLUMN3_VALUE_X_);
			intrinsicPeepUnit_.setX(::COLUMN3_UNIT_X_);
			intrinsicPeepValue_.setShow(arePavValuesAllowed_  &&
										intrinsicPeepValue_.getShow());
			intrinsicPeepUnit_.setShow(arePavValuesAllowed_  &&
									   intrinsicPeepUnit_.getShow());

			pavStatusMsg_.setShow(arePavSymbolsAllowed_  &&
								  pavStatusMsg_.getShow());
		}												// $[TI4.5]
		break;

	case VCP_ACTIVE_ :		
		lastAuxInfoState_ = currAuxInfoState_;

		switch (currAuxInfoState_)
		{
		case STANDARD_ :		
			//  do nothing ...
			break;
		case EXP_PAUSE_ :		
			removeAuxInfo_(arrExpPausePtrs_);
			break;
		case NIF_PAUSE_ :
			removeAuxInfo_(arrNifPausePtrs_);
			break;
		case P100_PAUSE_ :
			removeAuxInfo_(arrP100PausePtrs_);
			break;
		case VC_PAUSE_ :
			removeAuxInfo_(arrSvcPausePtrs_);
			break;
		case INSP_PAUSE_ :		
		    removeAuxInfo_(arrInspPauseResistancePtrs_);
            removeAuxInfo_(arrInspPauseCompliancePtrs_);
            removeAuxInfo_(arrInspPausePlateauPtrs_);
			break;
		case PAV_ACTIVE_ :
			removeAuxInfo_(arrPavPtrs_);
			break;
		case VCP_ACTIVE_ :		
			// do nothing...
			break;
		case VS_ACTIVE_ :		
			removeAuxInfo_(arrVsPtrs_);
			break;
		default :
			// unexpected value for 'currAuxInfoState_'...
			AUX_CLASS_ASSERTION_FAILURE(newAuxInfoState);
			break;
		};

		if (IS_CHANGE_OF_STATE)
		{												
			// add VCP drawables...
			addAuxInfo_(arrVcpPtrs_);
			// initial artifacts will be shown by patientDataChangeHappened
		}
		break;

	case VS_ACTIVE_ :		
	lastAuxInfoState_ = currAuxInfoState_;

		switch (currAuxInfoState_)
		{
		case STANDARD_ :		
			// do nothing...
			break;
		case EXP_PAUSE_ :		
			removeAuxInfo_(arrExpPausePtrs_);
			break;
		case NIF_PAUSE_ :
			removeAuxInfo_(arrNifPausePtrs_);
			break;
		case P100_PAUSE_ :
			removeAuxInfo_(arrP100PausePtrs_);
			break;
		case VC_PAUSE_ :
			removeAuxInfo_(arrSvcPausePtrs_);
			break;
		case INSP_PAUSE_ :		
			removeAuxInfo_(arrInspPauseResistancePtrs_);
            removeAuxInfo_(arrInspPauseCompliancePtrs_);
            removeAuxInfo_(arrInspPausePlateauPtrs_);
			break;
		case PAV_ACTIVE_ :
			removeAuxInfo_(arrPavPtrs_);
			break;
		case VCP_ACTIVE_ :		
			removeAuxInfo_(arrVcpPtrs_);
			break;
		case VS_ACTIVE_ :		
			// do nothing...
			break;
		default :
			// unexpected value for 'currAuxInfoState_'...
			AUX_CLASS_ASSERTION_FAILURE(newAuxInfoState);
			break;
		};

		if (IS_CHANGE_OF_STATE)
		{												
			// add VS drawables...
			addAuxInfo_(arrVsPtrs_);
			// initial artifacts will be shown by patientDataChangeHappened
		}
		break;

	default :
		// unexpected value for 'newAuxInfoState'...
		AUX_CLASS_ASSERTION_FAILURE(newAuxInfoState);
		break;
	};

	// set current to new...
	currAuxInfoState_ = newAuxInfoState;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayCompliance_
//
//@ Interface-Description
// Passed a boolean value which indicates whether to display the patient
// data values of Static compliance.  These values are displayed during 
// an Inspiratory Pause maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the flag in areComplianceDisplayed_ member.  If the flag
// is TRUE then display the various drawables which render the patient data
// values.  
//
// If passed FALSE remove the drawables.
// $[BL03004] questionable accuracy display ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::displayCompliance_(Boolean display )
{
	CALL_TRACE("WaveformsSubScreen::displayCompliance_(Boolean display)");

	// Store flag
	areComplianceDisplayed_ = display;

	// Check whether to display or remove.
	if ( areComplianceDisplayed_ )
	{													// $[TI1]	   
		Int32 complianceFlag = BreathDatumHandle(PatientDataId::STATIC_LUNG_COMPLIANCE_VALID_ITEM).
								getDiscreteValue().data;
		// $[BL04064] :p,q
		switch (complianceFlag)
		{
		case PauseTypes::VALID:							// $[TI1.1]
			// data is reliable
            if ( isFrozen_ )
            {
	            // $[PA01010] -- C, R and  PEEPi shall be displayed...
                lungComplianceSymbol_.setShow(TRUE);
                lungComplianceValue_.setShow(TRUE);           
                lungComplianceUnit_.setShow(TRUE);               
            }
            lungComplianceMarkerText_.setShow(FALSE);      
            staticComplianceInadequateText_.setShow(FALSE);
			break;
		case PauseTypes::OUT_OF_RANGE:
		case PauseTypes::SUB_THRESHOLD_INPUT:
		case PauseTypes::CORRUPTED_MEASUREMENT:
		case PauseTypes::EXH_TOO_SHORT:
		case PauseTypes::NOT_STABLE:					// $[TI1.2]
			// data is questionable -  Number and "()" is displayed
            if ( isFrozen_ )
            {
	            // $[PA01010] -- C, R and  PEEPi shall be displayed...
                lungComplianceSymbol_.setShow(TRUE);
                lungComplianceValue_.setShow(TRUE);           
                lungComplianceUnit_.setShow(TRUE);
                staticComplianceProblemLabel_.setShow(TRUE);        
            }
            if ( lungComplianceSymbol_.getShow() ) 
            {
                lungComplianceMarkerText_.setShow(TRUE);       
            }            
            staticComplianceInadequateText_.setShow(FALSE);
 			break;
	  	case PauseTypes::UNAVAILABLE:					// $[TI1.3]
	  		// data can not be calculataed - "***" and "()" is displayed
            lungComplianceValue_.setShow(FALSE);

            if ( isFrozen_ )
            {
	            // $[PA01010] -- C, R and  PEEPi shall be displayed...
                lungComplianceSymbol_.setShow(TRUE);         
                lungComplianceUnit_.setShow(TRUE);           
                lungComplianceMarkerText_.setShow(TRUE);     
                staticComplianceInadequateText_.setShow(TRUE); 
            }
            else
            if ( lungComplianceSymbol_.getShow()) 
            {
                lungComplianceUnit_.setShow(TRUE);
		        lungComplianceMarkerText_.setShow(TRUE);       
                staticComplianceInadequateText_.setShow(TRUE);  
            }

			break;
	  	case PauseTypes::NOT_REQUIRED:					// $[TI1.4]
	  		// data is not required for this case - display nothing            
            lungComplianceSymbol_.setShow(FALSE);
            lungComplianceValue_.setShow(FALSE);           
            lungComplianceUnit_.setShow(FALSE);               
            lungComplianceMarkerText_.setShow(FALSE);      
            staticComplianceInadequateText_.setShow(FALSE);
            break;
		}
               
        removeAuxInfo_(arrExpPausePtrs_);
		isExpiratoryPauseState_ = FALSE ;
		staticComplianceStatusValue_ = (PauseTypes::ComplianceState) complianceFlag;
	}
    else
    {
        removeAuxInfo_(arrInspPauseCompliancePtrs_);
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayResistance_
//
//@ Interface-Description
// Passed a boolean value which indicates whether to display the patient
// data values of Static resistance.  These values are displayed during
// an Inspiratory Pause maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the flag in areResistanceDisplayed_ member.  If the flag
// is TRUE then display the various drawables which render the patient data
// values.  
//
// If passed FALSE remove the drawables.
// $[BL03009] questionable accuracy display ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::displayResistance_(Boolean display)
{
	CALL_TRACE("WaveformsSubScreen::displayResistance_(Boolean display)");

	// Store flag
	areResistanceDisplayed_ = display;

	// Check whether to display or remove.
	if ( areResistanceDisplayed_ )
	{													// $[TI1]	  
 		Int32 resistanceFlag = BreathDatumHandle(PatientDataId::STATIC_AIRWAY_RESISTANCE_VALID_ITEM).
								getDiscreteValue().data;

		// $[BL04064] :p,q
		switch (resistanceFlag)
		{
		case PauseTypes::VALID:							// $[TI1.1]
			// data is reliable
            if ( isFrozen_ ) 
            {
	            // $[PA01010] -- C, R and  PEEPi shall be displayed...
                airwayResistanceSymbol_.setShow(TRUE);
                airwayResistanceValue_.setShow(TRUE);
                airwayResistanceUnit_.setShow(TRUE);			
            }
            
            airwayResistanceMarkerText_.setShow(FALSE);       
            staticResistanceNotAvailableText_.setShow(FALSE); 
            staticResistanceInadequateText_.setShow(FALSE);   
			break;
		case PauseTypes::OUT_OF_RANGE:
		case PauseTypes::SUB_THRESHOLD_INPUT:
		case PauseTypes::CORRUPTED_MEASUREMENT:
		case PauseTypes::EXH_TOO_SHORT:
		case PauseTypes::NOT_STABLE:					// $[TI1.2]
			// data is questionable -  Number and "()" is displayed
            if ( isFrozen_ ) 
            {
	            // $[PA01010] -- C, R and  PEEPi shall be displayed...
                airwayResistanceSymbol_.setShow(TRUE);
                airwayResistanceValue_.setShow(TRUE);
                airwayResistanceUnit_.setShow(TRUE);
                staticResistanceProblemLabel_.setShow(TRUE);
            }            
            if ( airwayResistanceSymbol_.getShow() ) 
            {
                airwayResistanceMarkerText_.setShow(TRUE);       
            }
            
            staticResistanceNotAvailableText_.setShow(FALSE); 
            staticResistanceInadequateText_.setShow(FALSE);   
			break;
	  	case PauseTypes::UNAVAILABLE:					// $[TI1.3]
	  		// data can not be calculataed - "***" and "()" is displayed
            airwayResistanceValue_.setShow(FALSE);

            if ( isFrozen_ )
            {
	            // $[PA01010] -- C, R and  PEEPi shall be displayed...
                airwayResistanceSymbol_.setShow(TRUE);                         
            }
            if ( airwayResistanceSymbol_.getShow()) 
            {
                airwayResistanceUnit_.setShow(TRUE);
		        airwayResistanceMarkerText_.setShow(TRUE);       
                staticResistanceInadequateText_.setShow(TRUE);  
            }            
			break;
	  	case PauseTypes::NOT_REQUIRED:					// $[TI1.4]
	  		// data is not required for this case - display dash            
            if ( isFrozen_) 
            {
	            // $[PA01010] -- C, R and  PEEPi shall be displayed...
                airwayResistanceSymbol_.setShow(TRUE);
            }
            if ( airwayResistanceSymbol_.getShow() )
            {            	
                staticResistanceNotAvailableText_.setShow(TRUE); 
            }
            
            airwayResistanceMarkerText_.setShow(FALSE);       
            airwayResistanceValue_.setShow(FALSE);
            airwayResistanceUnit_.setShow(FALSE);
            break;
		}

        removeAuxInfo_(arrExpPausePtrs_);
		isExpiratoryPauseState_ = FALSE ;
		staticResistanceStatusValue_ = (PauseTypes::ComplianceState) resistanceFlag;
	}
    else
    {
        removeAuxInfo_(arrInspPauseResistancePtrs_);
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayPlateauPressure_
//
//@ Interface-Description
// 	This method has a boolean that determines whether to display the plateau
//	pressure and returns nothing.  The pressure is displayed
//	only during an inspiratory pause maneauver.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update isPlateauPressureDisplayed_ with the argument passed in.
//  Determine whether to draw or erase the plateau pressure data.  If
//  pressures are displayed, erase expiratory pause data.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::displayPlateauPressure_( Boolean display)
{
	CALL_TRACE("WaveformsSubScreen::displayPlateauPressure_( Boolean display)") ;
	
	isPlateauPressureDisplayed_ = display ;

	if (isPlateauPressureDisplayed_)
	{													// $[TI1]
        plateauPressureSymbol_.setShow(TRUE);
        plateauPressureValue_.setShow(TRUE);
        plateauPressureUnit_.setShow(TRUE);

		if (areComplianceDisplayed_ || areResistanceDisplayed_ || isFrozen_)
		{												// $[TI1.1]
			plateauPressureValue_.setShow(TRUE);
		}
		else
		{												// $[TI1.2]
			plateauPressureValue_.setShow(FALSE);
		}

        removeAuxInfo_(arrExpPausePtrs_);
		isExpiratoryPauseState_ = FALSE ;
	}
    else
    {
        removeAuxInfo_(arrInspPausePlateauPtrs_);
    }
}
   
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, WAVEFORMSSUBSCREEN,
									lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeAuxInfo_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::removeAuxInfo_(Drawable* arrDrawablePtrs[])
{
	CALL_TRACE("removeAuxInfo_(arrDrawablePtrs)");

	for (Uint idx = 0u; arrDrawablePtrs[idx] != NULL; idx++)
	{
		removeDrawable(arrDrawablePtrs[idx]);
		arrDrawablePtrs[idx]->setShow(FALSE);
	}
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  addAuxInfo_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::addAuxInfo_(Drawable* arrDrawablePtrs[])
{
	CALL_TRACE("addAuxInfo_(arrDrawablePtrs)");

	for (Uint idx = 0u; arrDrawablePtrs[idx] != NULL; idx++)
	{
		addDrawable(arrDrawablePtrs[idx]);
	}
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  saveCurrentWaveformSettings
//
//@ Interface-Description
//  Saves the current waveform settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Retrieves the current accepted settings through context manager
//  and stores them temporary.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::saveCurrentWaveformSettings(void)
{
	CALL_TRACE("WaveformsSubScreen::saveCurrentWaveformSettings(void)");

    // Do not save the current waveform settings if
    // either a VC or Nif Maneuver is in progress.
    if(isVcWaveformFoisting_ || isNifWaveformFoisting_)
    {
        return;
    }

    AdjustPanel::TakePersistentFocus(NULL);// Release focus

    // Save Waveform settings.
    AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

    plot1SaveSetting_ = rAccContext.getNonBatchDiscreteValue(SettingId::PLOT1_TYPE);
    plot2SaveSetting_ = rAccContext.getNonBatchDiscreteValue(SettingId::PLOT2_TYPE);

    pressurePlotScaleSaveSetting_ = rAccContext.getNonBatchDiscreteValue(
                                             SettingId::PRESSURE_PLOT_SCALE);

    volumePlotScaleSaveSetting_   = rAccContext.getNonBatchDiscreteValue(
                                             SettingId::VOLUME_PLOT_SCALE);

    flowPlotScaleSaveSetting_   = rAccContext.getNonBatchDiscreteValue(
                                             SettingId::FLOW_PLOT_SCALE);

    timePlotScaleSaveSetting_   = rAccContext.getNonBatchDiscreteValue(
                                             SettingId::TIME_PLOT_SCALE);

    shadowTraceEnableSaveSetting_   = rAccContext.getNonBatchDiscreteValue(
                                             SettingId::SHADOW_TRACE_ENABLE);

    previousSpontType_ = rAccContext.getBatchDiscreteValue(
                                        SettingId::SUPPORT_TYPE); 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setupNifSettings
//
//@ Interface-Description
//  This method sets the NIF Maneuver settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If a NIF maneuver occurs, force the waveform plot to 
//  a single Pressure Vs. Time Screen with the plot scale to 
//  -60 to 20 cmH20 and the time scale from 0 to 12 secs.  
//  $[RM12059] 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::setupNifSettings(void)
{
	CALL_TRACE("WaveformsSubScreen::setupNifSettings(void)");

    // Set the NIF foisting flag to TRUE 
    isNifWaveformFoisting_ = TRUE;

	pTimeScaleSetting_->set30SecondTimeScaleEnable(TRUE);

    // Do not redraw the waveforms.
    disableSettingsUpdate_ = TRUE;

    // Set the nif waveform settings.
    AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

	// Do not save into Novram
	for(Uint32 index = 0;  arrPlotSettingPtr_[index] != NULL; index ++)
	{
	     arrPlotSettingPtr_[index]->setDisableNovRamUpdate(TRUE );
		
	}

    rAccContext.setNonBatchDiscreteValue(SettingId::PRESSURE_PLOT_SCALE, 
                                         PressPlotScaleValue::MINUS_60_TO_20_CMH2O);

    rAccContext.setNonBatchDiscreteValue(SettingId::TIME_PLOT_SCALE, 
                                         TimePlotScaleValue::FROM_0_TO_30_SECONDS);

    rAccContext.setNonBatchDiscreteValue(SettingId::PLOT1_TYPE, 
                                         Plot1TypeValue::PRESSURE_VS_TIME);

    // Allow to redraw the waveform.
    disableSettingsUpdate_ = FALSE;

    rAccContext.setNonBatchDiscreteValue(SettingId::PLOT2_TYPE, 
                                         Plot2TypeValue::NO_PLOT2);
}	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setupVcSettings
//
//@ Interface-Description
//  This method sets the Vital Capacity settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Whenever a Vital Capacity maneuver occurs, force the waveform plot to 
//  a single Volume Vs. Time Screen with the plot scale to 
//  -2000 to 6000 mL and the time scale from 0 to 24 secs.
//  $[RM12201]
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::setupVcSettings(void)
{
	CALL_TRACE("WaveformsSubScreen::setupVcSettings(void)");

    // Do not redraw waveforms.
    disableSettingsUpdate_ = TRUE;

    // Set the VC waveform settings.
    AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

	// Do not save into Novram
	for(Uint32 index = 0;  arrPlotSettingPtr_[index] != NULL; index ++)
	{
	    arrPlotSettingPtr_[index]->setDisableNovRamUpdate(TRUE);
	}

    rAccContext.setNonBatchDiscreteValue(SettingId::VOLUME_PLOT_SCALE, 
                                         VolumePlotScaleValue::MINUS_2000_TO_6000_ML);

    rAccContext.setNonBatchDiscreteValue(SettingId::TIME_PLOT_SCALE, 
                                         TimePlotScaleValue::FROM_0_TO_24_SECONDS);

    rAccContext.setNonBatchDiscreteValue(SettingId::PLOT1_TYPE, 
                                         Plot1TypeValue::VOLUME_VS_TIME);
    // Allow to redraw the waveform.
    disableSettingsUpdate_ = FALSE;

    rAccContext.setNonBatchDiscreteValue(SettingId::PLOT2_TYPE, 
                                         Plot2TypeValue::NO_PLOT2);

	// Set the VC foisting flag to TRUE
    isVcWaveformFoisting_ = TRUE;
}	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setupP100Settings
//
//@ Interface-Description
//  This method sets the P100 settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the isP100WaveformFoisting_ to TRUE which indicates that a 
//  P100 Maneuver is activated.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::setupP100Settings(void)
{
	CALL_TRACE("WaveformsSubScreen::setupP100Settings(void)");
    isP100WaveformFoisting_ = TRUE;

}	


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  restoreWaveformSettings
//
//@ Interface-Description
//  Restores the user-defined waveform settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Restores the waveform settings that was saved by saveCurrentWaveformSettings()
//  function.  Also, clears the isNifWaveformFoisting_ & isVcWaveformFoisting_
//  flag since it is not frozen.
//  $[RM12114] $[RM12230]
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::restoreWaveformSettings()
{
	CALL_TRACE("WaveformsSubScreen::restoreWaveformSettings(void)");
    
    AdjustPanel::TakePersistentFocus(NULL);// Release focus


    isNifWaveformFoisting_ = FALSE;    
	pTimeScaleSetting_->set30SecondTimeScaleEnable(FALSE);

    isVcWaveformFoisting_  = FALSE;

    disableSettingsUpdate_ = TRUE;

    // Restore the previous waveform settings
    AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

    const DiscreteValue CURRENT_SPONT_TYPE = rAccContext.getBatchDiscreteValue(
                                                         SettingId::SUPPORT_TYPE); 

	// Fixing SCR 6295. If the previous support type not PAV and 
    // the current support type is PAV, then the WOB graphic must
    // be displayed.  If the previous support type was PAV and
    // the current support type is not PAV, then set PLOT 1 to
    // pressure vs. time and PLOT 2 to flow vs. time.
	if((CURRENT_SPONT_TYPE == SupportTypeValue::PAV_SUPPORT_TYPE) &&
	   (previousSpontType_  != CURRENT_SPONT_TYPE))
	{
		plot2SaveSetting_  = Plot2TypeValue::WOB_GRAPHIC;
	}
	else if((previousSpontType_ == SupportTypeValue::PAV_SUPPORT_TYPE) &&
			(previousSpontType_  != CURRENT_SPONT_TYPE))
	{
        plot1SaveSetting_  = Plot1TypeValue::PRESSURE_VS_TIME;
        plot2SaveSetting_  = Plot2TypeValue::FLOW_VS_TIME;
	}

	// Allow to save plot settings into Novram.
	for(Uint32 index = 0;  arrPlotSettingPtr_[index] != NULL; index ++)
	{
	    arrPlotSettingPtr_[index]->setDisableNovRamUpdate(FALSE);
	}

    rAccContext.setNonBatchDiscreteValue(SettingId::FLOW_PLOT_SCALE, 
                                         flowPlotScaleSaveSetting_);


    rAccContext.setNonBatchDiscreteValue(SettingId::PRESSURE_PLOT_SCALE, 
                                         pressurePlotScaleSaveSetting_);

    rAccContext.setNonBatchDiscreteValue(SettingId::VOLUME_PLOT_SCALE, 
                                         volumePlotScaleSaveSetting_); 

    rAccContext.setNonBatchDiscreteValue(SettingId::SHADOW_TRACE_ENABLE, 
                                         shadowTraceEnableSaveSetting_);

    rAccContext.setNonBatchDiscreteValue(SettingId::TIME_PLOT_SCALE, 
                                         timePlotScaleSaveSetting_);

    rAccContext.setNonBatchDiscreteValue(SettingId::PLOT1_TYPE, 
                                         plot1SaveSetting_);

    // Allow to redraw waveforms.
    disableSettingsUpdate_ = FALSE;

    rAccContext.setNonBatchDiscreteValue(SettingId::PLOT2_TYPE, 
                                         plot2SaveSetting_);

}  


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  unFreezeWaveform
//
//@ Interface-Description
//  Unfreezes the waveform only due to a NIF, VC or P100 Maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the waveform screen is "Foisted" due to an RM Maneuver,
//  restore the User-Defined waveform settings by calling the 
//  buttonDownHappened method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::unFreezeWaveform()
{
	CALL_TRACE("WaveformsSubScreen::unFreezeWaveform(void)");

    // if the waveform screen is "Foisted" due to an RM Maneuver,
    // restore the User-Defined waveform settings.
    if (isFrozen_ || isFreezing_)
    {   
        if (isNifWaveformFoisting_  || isVcWaveformFoisting_ || isP100WaveformFoisting_)
        {    
            AdjustPanel::TakePersistentFocus(NULL);// Release focus
            buttonDownHappened(&freezeButton_, TRUE);
        }
    }
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSensorDataBuffer
//
//@ Interface-Description
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  return the reference to the only SensorDataBuffer
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

SensorDataBuffer& 
WaveformsSubScreen::GetSensorDataBuffer(void)
{
	static SensorDataBuffer   sensorDataBufferStatic_;
	return( sensorDataBufferStatic_ );
}					

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateSensorBufferViaWaveform_
//
//@ Interface-Description
//  Collect waveform data
//---------------------------------------------------------------------
//@ Implementation-Description
// $[LC01002] ] When Real-time waveform has been selected and activated, 
//              the 840 shall continuously transmit pressure, flow
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
void
WaveformsSubScreen::updateSensorBufferViaWaveform_(WaveformData *waveformData)
{
	SensorDataBuffer & sensorDataBuffer = GetSensorDataBuffer();

	//Setup to start on first cycle of insp
	if (waveformData->bdPhase == BreathPhaseType::EXHALATION)
	{
		startOnInsp_ = TRUE;
	}

	//Write Entry to Buffer for transmittal
	if (startOnInsp_ && waveformData->bdPhase == BreathPhaseType::INSPIRATION)
	{
		startOnInsp_ = FALSE;
		updateCount_ = 0;
		sensorDataBuffer.write((void *)&bufferEntry_, sizeof(SensorDataBuffer::SensorEntry));
	}

	//Update pressure and flow data buffers
	if (updateCount_ < SensorDataBuffer::MAX_SENSOR_DATA_POINTS)
	{
		// Get the value of the current sample (it'll be a bounded value)
		bufferEntry_.flowValues[updateCount_]     = waveformData->netFlow; 
		if (isProxEnabled_ && !isProxInop_ && !isProxInStartupMode_ && isVentNotInStartupOrWaitForPT_)
		{
			bufferEntry_.pressureValues[updateCount_] = waveformData->proxCircuitPressure;
		}
		else
		{
			bufferEntry_.pressureValues[updateCount_] = waveformData->circuitPressure;
		}
		updateCount_++;
		bufferEntry_.count = updateCount_;
	}


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// When the PROX_STATUS_TIMEOUT occurs, we will automatically 
// remove the prox status message.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set isProxDisplayTimerOn_ to FALSE
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the PROX_STATUS_TIMEOUT id.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformsSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("WaveformsSubScreen::timerEventHappened(timerId)");

	switch (timerId)
	{
	
	case  GuiTimerId::PROX_STATUS_TIMEOUT:

		isProxDisplayTimerOn_ = FALSE;
		break;
	default:
		AUX_CLASS_ASSERTION_FAILURE(timerId);
		break;
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isProxInop  
//
//@ Interface-Description
// Returns the current value of isProxInop_ as TRUE or FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply return the value of isProxInop_.	
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 Boolean
WaveformsSubScreen::isProxInop(void)
{
	CALL_TRACE("WaveformsSubScreen::isProxInop(void)");

	// Just return value of isProxInop_ variable.
	return (isProxInop_);								
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isProxInStartupMode  
//
//@ Interface-Description
// Returns the current value of isProxInStartupMode_ as TRUE or FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply return the value of isProxInStartupMode_.	
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 Boolean
WaveformsSubScreen::isProxInStartupMode(void)
{
	CALL_TRACE("WaveformsSubScreen::isProxInStartupMode(void)");

	// Just return value of isProxInStartupMode_ variable.
	return (isProxInStartupMode_);							
}



