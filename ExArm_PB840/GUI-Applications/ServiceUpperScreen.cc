#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ServiceUpperScreen - The base container for all upper screen containers.
//---------------------------------------------------------------------
//@ Interface-Description
// Contains the following high-level containers for the Upper LCD Screen:
// >Von
//	1.	TitleArea.
//	2.	UpperSubScreenArea.
//	3.	ServiceUpperScreenSelectArea.
// >Voff
//
// Provides public access to each of the contained member objects, via inline
// 'get()' methods.
//---------------------------------------------------------------------
//@ Rationale
// Convenient container object for the high-level components of the Upper LCD
// Screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is simply a holding area for the main containers of the Upper Screen.
// The UpperScreen class provides no functionality other than an interface
// which allows "global" access to each of the main containers.
//---------------------------------------------------------------------
//@ Fault-Handling
// none
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceUpperScreen.ccv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 004  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By: yyy      Date: 15-May-1997  DR Number: 2110
//    Project:  Sigma (R8027)
//    Description:
//      Added SRS 07001 mapping.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ServiceUpperScreen.hh"

//@ Usage-Classes
#include "UpperSubScreenArea.hh"
#include "GuiEventRegistrar.hh"
#include "SstResultsSubScreen.hh"
#include "MemoryStructs.hh"

//@ End-Usage

//@ Code...

// Initialize static constants.
const Int32 CONTAINER_X_ = 0;
const Int32 CONTAINER_Y_ = 0;
const Int32 CONTAINER_WIDTH_ = 640;
const Int32 CONTAINER_HEIGHT_ = 480;

ServiceUpperScreen&	ServiceUpperScreen::RServiceUpperScreen = *((ServiceUpperScreen *)
		(((ScreenMemoryUnion *)::ScreenMemory)->serviceModeMemory.upperScreen));

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceUpperScreen()  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Size and position the container and set its background color.  Add
// all member objects to the parent container for display and initialize
// Upper Screen Select Area.
// $[01266] The upper screen shall provide the following ...
// $[07005] The upper screen shall provide the following ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceUpperScreen::ServiceUpperScreen(void) :
		currentScreen_(SERVICE_NO_SCREEN)
{
	CALL_TRACE("ServiceUpperScreen::ServiceUpperScreen(void)");
 
#if defined FORNOW
printf("ServiceUpperScreen::ServiceUpperScreen(void)\n");
#endif	// FORNOW

    // Size and position the area
    setX(CONTAINER_X_);
    setY(CONTAINER_Y_);
    setWidth(CONTAINER_WIDTH_);
    setHeight(CONTAINER_HEIGHT_);
 
    //
	// Default background color is white.
	//
    setFillColor(Colors::FRAME_COLOR);

    //
	// Add the various member objects to the container.
	// $[01003] Upper screen contains four areas ...
	//
    addDrawable(&serviceUpperScreenSelectArea_);
    addDrawable(&serviceUpperSubScreenArea_);
    addDrawable(&serviceTitleArea_);

	// Initialize Service Upper Screen Select Area
 	serviceUpperScreenSelectArea_.initialize();

	// Register for GUI state change callbacks.
	GuiEventRegistrar::RegisterTarget(this);
 											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceUpperScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceUpperScreen::~ServiceUpperScreen(void)
{
	CALL_TRACE("ServiceUpperScreen::~ServiceUpperScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: beginSst
//
//@ Interface-Description
// Sets up the Upper screen for SST mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// We begin SST with blanking the Upper Screen Select Area, and activating
// the SstResultsSubScreen.
// $[01267] The upper screen select area shall remain blank during SST.
// $[01268] The upper LCD shall provice the following functions ...
// $[01286] The upper subscreen area shall display the SST results ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceUpperScreen::beginSst(void)
{
	CALL_TRACE("ServiceUpperScreen::beginSst(void)");

#if defined FORNOW
printf("ServiceUpperScreen::beginSst(void)\n");
#endif	// FORNOW

	UpperSubScreenArea *pUpperSubScreenArea;
	
	// Hide the upper screen select area.
	serviceUpperScreenSelectArea_.updateButtonDisplay(FALSE);

	// Set the current active mode.
	currentScreen_ = BEGIN_SST_MODE;
	
	pUpperSubScreenArea = getUpperSubScreenArea();

	// Activate the SstResultSubScreen
	pUpperSubScreenArea->activateSubScreen(
				UpperSubScreenArea::GetSstResultsSubScreen(), NULL);
				// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: beginServiceMode
//
//@ Interface-Description
// Sets up the Upper screen for service mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// We begin service mode with deactivating any possible subScreens and
// showing the Upper Screen Select Area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceUpperScreen::beginServiceMode(void)
{
	CALL_TRACE("ServiceUpperScreen::beginServiceMode(void)");

#if defined FORNOW
printf("ServiceUpperScreen::beginServiceMode(void)\n");
#endif	// FORNOW

	// Deactivate any subscreen if exists.
	serviceUpperSubScreenArea_.deactivateSubScreen();

	// Show the upper screen select area.
	serviceUpperScreenSelectArea_.updateButtonDisplay(TRUE);

	currentScreen_ = BEGIN_SERVICE_MODE;
		  						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened	[virtual] 
//
//@ Interface-Description
// Called when a GUI event happens.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We display the appropriated title for the GUI event and activate the
//  appropriate subscreen based on the GUI event.
//  If we recieve COMMUNICATION DOWN event, then we will deactivate all
//  screens and buttons.
// $[01268] The upper LCD shall provice the following functions ...
// $[07001] Entering service mode shall cause the ventilator to display ...
// $[07019] The title area shall indicate that Service Mode is active ...
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceUpperScreen::guiEventHappened(GuiApp::GuiAppEvent eventId)
{
	CALL_TRACE("ServiceUpperScreen::guiEventHappened(GuiApp::GuiAppEvent eventId)");

#if defined FORNOW
printf("ServiceUpperScreen::guiEventHappened(void)\n");
#endif	// FORNOW

	switch (eventId)
	{
	case GuiApp::SERVICE_READY:				// $[TI1]
#if defined SERVICE_FORNOW
		getServiceTitleArea()->setDisplayMode(ServiceTitleArea::SERVICE_MODE);
		beginServiceMode();
#elif defined SST_FORNOW
		getServiceTitleArea()->setDisplayMode(ServiceTitleArea::SST_MODE);
		beginSst();
#else	
 		if (GuiApp::GetGuiState() == STATE_SERVICE)
 		{									// $[TI2]	
			// Set the title Area into "SERVICE MODE" mode
			getServiceTitleArea()->setDisplayMode(ServiceTitleArea::SERVICE_MODE);
			beginServiceMode();
	 	}
 		else if (GuiApp::GetGuiState() == STATE_SST)
	 	{									// $[TI3]
			// Set the title Area into "SST MODE" mode
			getServiceTitleArea()->setDisplayMode(ServiceTitleArea::SST_MODE);
 			beginSst();
	 	}
 		else
	 	{
			SAFE_CLASS_ASSERTION(FALSE);
	 	}
#endif
		break;

	case GuiApp::INOP:
	case GuiApp::COMMUNICATIONS_DOWN:	
 		if (GuiApp::GetGuiState() == STATE_SERVICE)
 		{									// $[TI4]		
			getServiceTitleArea()->setDisplayMode(ServiceTitleArea::SERVICE_MODE_COMM_DOWN);
	 	}
 		else if (GuiApp::GetGuiState() == STATE_SST)
	 	{									// $[TI5]
			getServiceTitleArea()->
					setDisplayMode(ServiceTitleArea::SST_MODE_COMM_DOWN);
	 	}
 		else
	 	{
			SAFE_CLASS_ASSERTION(FALSE);
	 	}

		// Deactivate any subscreen if exists.
		serviceUpperSubScreenArea_.deactivateSubScreen();
		break;
	default:
		// Ignore any other BD messages.
		break;	
	}
	
}




#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ServiceUpperScreen::SoftFault(const SoftFaultID  softFaultID,
					   const Uint32       lineNumber,
					   const char*        pFileName,
					   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SERVICEUPPERSCREEN,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
