
#ifndef DiagCodeLog_HH
#define DiagCodeLog_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  DiagCodeLog - Diagnostic Code Log.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/DiagCodeLog.hhv   25.0.4.0   19 Nov 2013 14:18:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah    Date:  08-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//	Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 003  By:  sah    Date:  24-Sep-1997    DCS Number: 2513
//  Project:  Sigma (R8027)
//  Description:
//	Added new method, 'clearOutAllEntries()', for emptying the
//	contents of a diagnostic log, and issuing an update once the
//	log is cleared.
//
//  Revision: 002  By:  sah    Date:  15-Jul-1997    DCS Number: 2175
//  Project:  Sigma (R8027)
//  Description:
//	Added method for removing the last overall EST/SST result diagnostic
//	in the log.  Also, changed name of 'backoutDiagnostics()' to
//	'backoutLastDiags()', to better distinguish its functionality.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "PersistentObjsId.hh"
#include "TemplateMacros.hh"
#include "NovRamStatus.hh"

//@ Usage-Classes
#include "DiagnosticCode.hh"
#include "NovRamUpdateManager.hh"

// forward declarations...
class CodeLogEntry;
class AbstractArray(CodeLogEntry);

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif // defined(SIGMA_DEVELOPMENT)
//@ End-Usage


class DiagCodeLog
{
#if defined(SIGMA_DEVELOPMENT)
    friend Ostream&  operator<<(Ostream&           ostr,
    				const DiagCodeLog& diagCodeLog);
#endif // defined(SIGMA_DEVELOPMENT)

  public:
    ~DiagCodeLog(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    void  clearOutAllEntries(void);

    void  backoutLastDiags  (const Uint startingSeqNumber);
    void  backoutOverallDiag(const Uint overallTestNumber);

#if defined(SIGMA_GUI_CPU)
    void  updateEntries(const AbstractArray(CodeLogEntry)& newBdEntries);
#endif // defined(SIGMA_GUI_CPU)

    static void  MergeCodeLogs(
			    AbstractArray(CodeLogEntry)&       rMergedLog,
			    const AbstractArray(CodeLogEntry)& arrGuiCodes,
			    const AbstractArray(CodeLogEntry)& arrBdCodes,
			    const Uint                         maxEntries
			      );

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  protected:
    DiagCodeLog(CodeLogEntry* pArrEntries, const Uint32 maxEntries,
		const NovRamUpdateManager::UpdateTypeId updateTypeId);

    void  copyCurrEntries_(AbstractArray(CodeLogEntry)& rCurrEntries) const;

    NovRamStatus::InitializationId  initLog_  (const Uint32 maxEntries);
    NovRamStatus::VerificationId    verifyLog_(const Uint32 maxEntries) const;

    void  logDiagCode_(const DiagnosticCode diagnosticCode);

  private:
    DiagCodeLog(const DiagCodeLog&);		// not implemented...
    DiagCodeLog(void);				// not implemented...
    void  operator=(const DiagCodeLog&);	// not implemented...

    inline Uint32  getNextIndex_(const Uint32 fromIndex) const;
    inline Uint32  getPrevIndex_(const Uint32 fromIndex) const;

    //@ Constant:  UPDATE_TYPE_ID_
    // This identifies the update ID of this diagnostic log.
    const NovRamUpdateManager::UpdateTypeId  UPDATE_TYPE_ID_;

    //@ Constant:  MAX_ENTRIES_
    // The maximum number of entries allowed in this fault log.
    const Uint32  MAX_ENTRIES_;

    //@ Data-Member:  headIndex_
    // Index to the newest entry in the log.
    Uint32  headIndex_;

    //@ Data-Member:  tailIndex_
    // Index to the oldest entry in the log.
    Uint32  tailIndex_;

    //@ Data-Member:  P_ARR_ENTRIES_
    // A constant pointer to the derived class's array of (non-constant)
    // log entries.
    CodeLogEntry* const  P_ARR_ENTRIES_;
};


// Inlined methods...
#include "DiagCodeLog.in"


#endif // DiagCodeLog_HH 
