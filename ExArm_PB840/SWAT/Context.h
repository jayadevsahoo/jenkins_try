//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
/// @file Context.h
///
/// @brief
/// Class deffinition for Context.
///
/// A Context defines a coroutines stack.

#ifndef CONTEXT_H
#define CONTEXT_H

#include "Actions.h"

namespace swat{

    /// @class Context
    ///
    /// State data for coroutines.
    class Context{
        public:
            union {
                MarkState ms;
                WaitState ws;
            };

    };

}
#endif
