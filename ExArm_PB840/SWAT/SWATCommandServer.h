////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
Name:		SWATCommandServer.h

Purpose:
	Establish a Command server for receive SWAT commands from SWAT PC.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 30-JUL-2013
PROJECT: PB880 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/
#pragma once

// for SplitString
#include <string>
#include <vector>
#include <map>

using namespace std;

// for WinSock APIs
#include <winsock2.h>
#include <ws2tcpip.h>

// for XML processing
#include "tinyxml.hh"

// for SWAT subsystem types
#include "SWATTypes.h"

// link options
#if defined( SIGMA_GUIPC_CPU )
    #pragma comment(lib, "Ws2_32.lib")
#else
    #pragma comment(lib, "ws2.lib")
    #pragma comment(lib, "xlock.lib")
#endif

////////////////////////////////////////////////////////////////////////////////

typedef struct
{
    INT                 transactionId;
    INT                 seconds;
    INT                 microseconds;
} CommandMsgHeader;


const char MESSAGE_ROOT[]	= "m";
const char HEADER[]			= "h";

class UdpCommunication;
/*
  This class provide function to listen to a network port and receive command
  from a SWAT client. Then SWAT command will be parsed and execute by this class.
    */
class SWATCommandServer
{
private:
	TiXmlDocument       m_doc;
	CommandMsgHeader    header;
	char                m_msg[SWAT_MAX_MSG_LENGTH];
	bool                m_parseErr;

    // command server reference
	static SWATCommandServer&   RSWATCommandServer_;
    static UdpCommunication&    RUdpComm_;

	// parse the message header
	TiXmlElement*   ParseHeader(TiXmlElement* ele, CommandMsgHeader& s);

	// void Execute(const SWATCommand& action, SWATCommandResp& resp);
	SWATStatus      ParseExecCmd(const char * buffer);
	
	// the entry function of the server
	bool            serverMain();

	SWATCommandServer();

public:
	
	static void     Initialize();   // Initialize the command server instance

	static void     Task();         // Entry function of Command Server thread
};