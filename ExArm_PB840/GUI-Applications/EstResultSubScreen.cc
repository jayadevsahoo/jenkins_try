#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: EstResultSubScreen - The subscreen is automatically activated
// during EST Test SubScreen.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains textual fields and containers.  This
// subscreen displays EST test data information while running.
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// Register all test data to Service-Data subsystem and process the data
// by various GuiTestManager methods throught processTestData().
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the Est Result subscreen in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The implementation is quite simple.  The EstResult subscreen is activated.
// When each EST test is running, the test data shall
// be displayed on the upper screen.  Methods are also provided to set
// special data format for different EST test.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/EstResultSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: rhj   Date:  03-Apr-2009    SCR Number: 6495
//  Project:  840S2
//  Description:
//      Modified to hide the leak gauge completely. 
//
//  Revision: 013   By: gdc   Date:  26-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 012   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 011  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 010  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 009  By:  yyy    Date:  08-Oct-97    DR Number: DCS 2203
//    Project:  Sigma (840)
//    Description:
//      Updated data for new BD audio test procedure.
//
//  Revision: 008  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 007  By:  yyy    Date:  01-Oct-97    DR Number: DCS 2204
//    Project:  Sigma (840)
//    Description:
//      Updated data label for battery test.
//
//  Revision: 006  By:  hhd    Date:  20-AUG-97    DR Number: 2321
//    Project:  Sigma (R8027)
//    Description:
//		Added testable items.
//
//  Revision: 005  By:  hhd    Date:  18-AUG-97    DR Number: 2321
//    Project:  Sigma (R8027)
//    Description:
//      Converted decimal point to comma for all European but German language.
//
//  Revision: 004  By:  yyy    Date:  30-JUL-97    DR Number: 2205
//    Project:  Sigma (R8027)
//    Description:
//      Updated exh valve velocity transducer test.
//
//  Revision: 003  By:  yyy    Date:  29-JUL-97    DR Number: 2013
//    Project:  Sigma (R8027)
//    Description:
//      Updated the Nurse Call test.
//
//  Revision: 002  By:  yyy    Date:  10-Jun-97    DR Number: 1988
//    Project:  Sigma (R8027)
//    Description:
//      Added checking for pressure unit based on setting value.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include <string.h>
#include "EstResultSubScreen.hh"

#include "MiscStrs.hh"
#include "EstTestSubScreen.hh"
#include "ServiceLowerScreen.hh"
#include "SettingContextHandle.hh"

//@ Usage-Classes
#	include "SubScreenArea.hh"
#	include "SmDataId.hh"
#	include "SmTestId.hh"
#	include "DiscreteValue.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int16 RESULT_AREA_WIDTH_ = 634;
static const Int16 RESULT_AREA_HEIGHT_ = 273;
static const Int16 LABEL_X_ = 20;
static const Int16 LABEL_Y_ = 35;	
static const Int16 VALUE_X_ = 20;
static const Int16 VALUE_Y_ = 45;	
static const Int16 UNIT_X_  = 45;
static const Int16 UNIT_Y_  = 45;	
static const Int16 X_OFFSET_ = 150;
static const Int16 Y_OFFSET_ = 26;	
static const Int16 DISPLAY_ITEMS_PER_COL_ = 9;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EstResultSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members; Sets the position, size and color of the subscreen
// area. Add the subscreen title object and the log to the 
// parent container for display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EstResultSubScreen::EstResultSubScreen(SubScreenArea *pSubScreenArea) :
		SubScreen(pSubScreenArea),
		titleArea_(NULL, SubScreenTitleArea::SSTA_CENTER),
		testId_(SmTestId::TEST_NOT_START_ID),
		testManager_(this),
		leakGauge_(75.0, 90.0, 
				MiscStrs::EST_LEAK_PASS_MSG, MiscStrs::EST_LEAK_FAIL_MSG,
				80.0),
		maxDataCount_(0),
		maxDisplayableDataCount_(0)
{
	CALL_TRACE("EstResultSubScreen::EstResultSubScreen(SubScreenArea "
													"*pSubScreenArea)");

#if defined FORNOW
printf("EstResultSubScreen::() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Associate the testResults object with the pointer to the test result
	// array.
	testManager_.setupTestResultDataArray();

	// Size and position the text displayable containers
	valueAreaContainer_.setX(0);
	valueAreaContainer_.setY(0);
	valueAreaContainer_.setWidth(RESULT_AREA_WIDTH_);
	valueAreaContainer_.setHeight(RESULT_AREA_HEIGHT_);
	labelAreaContainer_.setX(0);
	labelAreaContainer_.setY(0);
	labelAreaContainer_.setWidth(RESULT_AREA_WIDTH_);
	labelAreaContainer_.setHeight(RESULT_AREA_HEIGHT_);
	unitAreaContainer_.setX(0);
	unitAreaContainer_.setY(0);
	unitAreaContainer_.setWidth(RESULT_AREA_WIDTH_);
	unitAreaContainer_.setHeight(RESULT_AREA_HEIGHT_);
	leakGauge_.setX(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH/2);
	leakGauge_.setY(38);

	// Hide the following drawables
	valueAreaContainer_.setShow(FALSE);
	labelAreaContainer_.setShow(FALSE);
	unitAreaContainer_.setShow(FALSE);
	leakGauge_.setShow(FALSE);

	// Add the following drawables
	addDrawable(&valueAreaContainer_);
	addDrawable(&labelAreaContainer_);
	addDrawable(&unitAreaContainer_);
	addDrawable(&leakGauge_);

	addDrawable(&titleArea_);


	for (Int idx = 0; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		isTestResultDisplayable_[idx] = FALSE;

		// Hide the following drawables
		testResultValueArray_[idx].setShow(FALSE);
		testResultLabelArray_[idx].setShow(FALSE);
		testResultUnitArray_[idx].setShow(FALSE);
		
		// Add the following drawables
		valueAreaContainer_.addDrawable(&testResultValueArray_[idx]);
		labelAreaContainer_.addDrawable(&testResultLabelArray_[idx]);
		unitAreaContainer_.addDrawable(&testResultUnitArray_[idx]);
		
		testResultValueArray_[idx].setColor(Colors::WHITE);
		testResultLabelArray_[idx].setColor(Colors::WHITE);
		testResultUnitArray_[idx].setColor(Colors::WHITE);
	}
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~EstResultSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys EstResultSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EstResultSubScreen::~EstResultSubScreen(void)
{
	CALL_TRACE("EstResultSubScreen::~EstResultSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Called by our subscreen area before this subscreen is to be displayed
//  allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Register to Service-Data subsystem for all possible test data events.
//  Show the value, label and unit drawables for each test.  Initialize
//  the isTestResultDisplayable_[], testResultValueArray_[],
//  testResultLabelArray_[], and testResultUnitArray_[] arrays to null.
//  Activate the leak gauge display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstResultSubScreen::activate(void)
{
	CALL_TRACE("EstResultSubScreen::activate(void)");

	Int idx;

#if defined FORNOW	
printf("EstResultSubScreen::activate()\n");
#endif	// FORNOW

	// Register to Service-Data subsystem for all possible test data events
	testManager_.registerTestResultData();

	// Show the following drawables
	valueAreaContainer_.setShow(TRUE);
	labelAreaContainer_.setShow(TRUE);
	unitAreaContainer_.setShow(TRUE);

	// Initialize all the following items
	for (idx = 0; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		isTestResultDisplayable_[idx] = FALSE;
		testResultValueArray_[idx].setShow(FALSE);
		testResultLabelArray_[idx].setShow(FALSE);
		testResultUnitArray_[idx].setShow(FALSE);
	}

	// Activate the leak gauge.
	leakGauge_.activate();

	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Called by our subscreen area just after this subscreen is removed from
//  the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Inform the EST test subscreen to clear the highlighted test item from
//  the scrollable log.  Hide all test data display related drawables.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstResultSubScreen::deactivate(void)
{
	CALL_TRACE("EstResultSubScreen::deactivate(void)");

#if defined FORNOW
printf("EstResultSubScreen::deactivate() at line  %d\n", __LINE__);
#endif	// FORNOW

	EstTestSubScreen *pEstTestSubScreen;

	// Get the EST setup subscreen address.
	pEstTestSubScreen = LowerSubScreenArea::GetEstTestSubScreen();

	// Inform the EST setup subscreen to clear the highlighted field.
	pEstTestSubScreen->clearHighlightedTestItem();

	// Hide the following drawables
	valueAreaContainer_.setShow(FALSE);
	labelAreaContainer_.setShow(FALSE);
	unitAreaContainer_.setShow(FALSE);
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  This is a virtual method inherited from being an TestResutlTarget.  It is
//  called when the Service Data Manager detectes a new test data as indexed
//  by "dataIndex" and the test result is stored in pResult.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If this piece of data is intended to be displayed on the upper screen
//  then we shall format the test data so the value, unit and label will
//  all be displayed at one time.
// $[07039] When EST is running, the test measurements for the current test...
// $[07051] For the test that is currently being run (if any), the test ...
//---------------------------------------------------------------------
//@ PreCondition
//	dataIndex < TestDataId::LAST_DATA_ITEM && dataIndex >= 0
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstResultSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("EstResultSubScreen::processTestData(Int dataIndex, TestResult *pResult)");

#if defined FORNOW
printf("EstResultSubScreen::processTestData() at line  %d, isTestResultDisplayable_[%d] = %d\n",
					__LINE__, dataIndex, isTestResultDisplayable_[dataIndex]);
#endif	// FORNOW

	SAFE_CLASS_ASSERTION(dataIndex < TestDataId::LAST_DATA_ITEM &&
						dataIndex >= 0);

	// Display the appropriated data
	if (isTestResultDisplayable_[dataIndex])
	{	// Display the data only when the Service mode says so.
											// $[TI1]
		setText_(&testResultValueArray_[dataIndex], pResult->getStringValue());
		testResultValueArray_[dataIndex].setShow(TRUE);
		testResultLabelArray_[dataIndex].setShow(TRUE);
		testResultUnitArray_[dataIndex].setShow(TRUE);

#if defined FORNOW
printf("EstResultSubScreen:::processTestData() dataIndex=%d, value=%s"
		" visible %d, pos %d %d\n",
		dataIndex, pResult->getStringValue(),
		testResultValueArray_[dataIndex].isVisible(),
		testResultValueArray_[dataIndex].getX(),
		testResultValueArray_[dataIndex].getY());
#endif	// FORNOW
	}										// $[TI2]

	// Display the Leak graph
	if (testId_ == SmTestId::SM_LEAK_TEST_ID &&
		dataIndex == SmDataId::SM_LEAK_MONITOR_PRESSURE)
	{										// $[TI3]
		leakGauge_.setValue(pResult->getTestDataValue());
	}										// $[TI4]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setResultTitle
//
//@ Interface-Description
//  Initialize the titleArea_ with the passed parameter "title"
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper titleArea_ to the passed parameter "title"
// $[07024] This subscreen displays the results of a single EST test, ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstResultSubScreen::setResultTitle(StringId title)
{
	CALL_TRACE("EstResultSubScreen::setResultTitle(StringId title)");

#if defined FORNOW
printf("EstResultSubScreen::setResultTitle() at line  %d, isVisible = %d\n",
			__LINE__, (isVisible())?1:0);
#endif	// FORNOW

	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	titleArea_.setShow(TRUE);
	titleArea_.setTitle(title);

#if defined FORNOW
printf("EstResultSubScreen::setResultTitle(), title=%s, title area isVisible= %d\n",
				title, (titleArea_.isVisible()) ? 1:0);
#endif	// FORNOW
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDataFormat
//
//@ Interface-Description
//  Prepare the data format for the current test's displayable result.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize the isTestResultDisplayable_[], testResultValueArray_[],
//  testResultLabelArray_[], and testResultUnitArray_[] for the passed
//  parameter "testId".  
//  Based on the testId, call setupDisplayableTestResult_() to setup the
//  data precision, title string, and unit string for each displayable
//  test data.
// $[07024] This subscreen displays the results of a single EST test, ...
// $[07029] The data displayed on the Upper screen for the current ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstResultSubScreen::setDataFormat(SmTestId::ServiceModeTestId testId)
{
	CALL_TRACE("EstResultSubScreen::setDataFormat(SmTestId::ServiceModeTestId testId)");

	Int16 idx;
	wchar_t labelBuffer[200];  

#if defined FORNOW
printf("EstResultSubScreen::setDataFormat() at line  %d\n", __LINE__);
#endif	// FORNOW

	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	testId_ = testId;
	leakGauge_.setShow(FALSE);

	// Initialize all the following items
	for (idx = 0; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		testManager_.setPrecision((SmDataId::ItemizedTestDataId) idx, 0);
		isTestResultDisplayable_[idx] = FALSE;
		testResultValueArray_[idx].setShow(FALSE);
		testResultLabelArray_[idx].setShow(FALSE);
		testResultUnitArray_[idx].setShow(FALSE);
	}

	maxDataCount_ = 0;
	maxDisplayableDataCount_ = 0;

	idx = 0;
	StringId unitId, labelUnitId;

	const DiscreteValue  UNITS_VALUE =
	  SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
						SettingId::PRESS_UNITS);

	if (UNITS_VALUE == PressUnitsValue::CMH2O_UNIT_VALUE)
	{													// $[TI2.1]
		unitId = MiscStrs::EST_TEST_RESULT_CM_H20_UNIT;
		labelUnitId = MiscStrs::TITLE_CMH20_UNIT;
	}
	else
	{													// $[TI2.2]
		unitId = MiscStrs::EST_TEST_RESULT_HPA_UNIT;
		labelUnitId = MiscStrs::TITLE_HPA_UNIT;
	}
	
	switch (testId_)
	{
	case SmTestId::GAS_SUPPLY_TEST_ID:					// $[TI3]
		setupDisplayableTestResult_(SmDataId::GAS_SUPPLY_LOW_FLOW_CRACKING_PRESSURE, 2, &idx,
									MiscStrs::GAS_SUPPLY_LOW_FLOW_CRACKING_PRESSURE_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::GAS_SUPPLY_HIGH_FLOW_CRACKING_PRESSURE, 2, &idx,
									MiscStrs::GAS_SUPPLY_HIGH_FLOW_CRACKING_PRESSURE_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::GAS_SUPPLY_AIR_PSOL_LEAK_PRESSURE, 2, &idx,
									MiscStrs::GAS_SUPPLY_AIR_PSOL_LEAK_PRESSURE_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::GAS_SUPPLY_AIR_FLOW_SENSOR_0_OFFSET, 2, &idx,
									MiscStrs::GAS_SUPPLY_AIR_FLOW_SENSOR_0_OFFSET_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::GAS_SUPPLY_O2_FLOW_SENSOR_0_OFFSET, 2, &idx,
									MiscStrs::GAS_SUPPLY_O2_FLOW_SENSOR_0_OFFSET_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::GAS_SUPPLY_EXH_FLOW_SENSOR_0_OFFSET, 2, &idx,
									MiscStrs::GAS_SUPPLY_EXH_FLOW_SENSOR_0_OFFSET_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::GAS_SUPPLY_O2_PSOL_LEAK_PRESSURE, 2, &idx,
									MiscStrs::GAS_SUPPLY_O2_PSOL_LEAK_PRESSURE_TITLE,
									unitId);

		maxDataCount_ = SmDataId::GAS_SUPPLY_TEST_END -
						SmDataId::GAS_SUPPLY_TEST_START + 1;
		break;

	case SmTestId::GUI_KEYBOARD_TEST_ID:				// $[TI5]
		break;
	case SmTestId::GUI_KNOB_TEST_ID:					// $[TI6]
		break;
	case SmTestId::GUI_LAMP_TEST_ID:					// $[TI7]
		break;
	case SmTestId::BD_LAMP_TEST_ID:						// $[TI8]
		break;
	case SmTestId::GUI_AUDIO_TEST_ID:					// $[TI9]
		break;
	case SmTestId::BD_AUDIO_TEST_ID:					// $[TI10]
		setupDisplayableTestResult_(SmDataId::BD_AUDIO_ALARM_CABLE_VOLTAGE, 2, &idx,
									MiscStrs::BD_AUDIO_ALARM_CABLE_VOLTAGE_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);

		setupDisplayableTestResult_(SmDataId::BD_AUDIO_POWER_FAIL_CAP_VOLTAGE1, 2, &idx,
									MiscStrs::BD_AUDIO_POWER_FAIL_CAP_VOLTAGE1_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);

		setupDisplayableTestResult_(SmDataId::BD_AUDIO_TIME_CONSTANT, 2, &idx,
									MiscStrs::BD_AUDIO_TIME_CONSTANT_TITLE,
									MiscStrs::EST_TEST_RESULT_SECOND_UNIT);

		maxDataCount_ = SmDataId::BD_AUDIO_TEST_END -
						SmDataId::BD_AUDIO_TEST_START + 1;
		break;

	case SmTestId::FS_CROSS_CHECK_TEST_ID:				// $[TI12]
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_120_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_120_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_120_EXHALATION_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_120_EXHALATION_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_120_PSOL_CURRENT, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_120_PSOL_CURRENT_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_60_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_60_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_60_EXHALATION_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_60_EXHALATION_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_60_PSOL_CURRENT, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_60_PSOL_CURRENT_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);

		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_20_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_20_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_20_EXHALATION_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_20_EXHALATION_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_20_PSOL_CURRENT, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_20_PSOL_CURRENT_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);

		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_5_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_5_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_5_EXHALATION_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_5_EXHALATION_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_5_PSOL_CURRENT, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_5_PSOL_CURRENT_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_1_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_1_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_1_EXHALATION_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_1_EXHALATION_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_1_PSOL_CURRENT, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_1_PSOL_CURRENT_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_0_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_0_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_120_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_120_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_120_EXHALATION_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_120_EXHALATION_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_120_PSOL_CURRENT, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_120_PSOL_CURRENT_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_60_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_60_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_60_EXHALATION_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_60_EXHALATION_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_60_PSOL_CURRENT, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_60_PSOL_CURRENT_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_20_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_20_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_20_EXHALATION_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_20_EXHALATION_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_20_PSOL_CURRENT, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_20_PSOL_CURRENT_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_5_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_5_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_5_EXHALATION_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_5_EXHALATION_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_5_PSOL_CURRENT, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_5_PSOL_CURRENT_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_1_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_1_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_1_EXHALATION_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_1_EXHALATION_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_1_PSOL_CURRENT, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_1_PSOL_CURRENT_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_0_INSPIRATORY_FLOW, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_0_INSPIRATORY_FLOW_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_AIR_LIFTOFF_COMMAND, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_AIR_LIFTOFF_COMMAND_TITLE,
									MiscStrs::EST_TEST_RESULT_COUNT_UNIT);
		setupDisplayableTestResult_(SmDataId::FS_CROSS_CHECK_O2_LIFTOFF_COMMAND, 2, &idx,
									MiscStrs::FS_CROSS_CHECK_O2_LIFTOFF_COMMAND_TITLE,
									MiscStrs::EST_TEST_RESULT_COUNT_UNIT);
		maxDataCount_ = SmDataId::FS_CROSS_CHECK_TEST_END -
						SmDataId::FS_CROSS_CHECK_TEST_START + 1;
		break;

	case SmTestId::CIRCUIT_PRESSURE_TEST_ID:			// $[TI13]
		setupDisplayableTestResult_(SmDataId::CIRCUIT_PRESSURE_INSP_ZERO_COUNTS, 2, &idx,
									MiscStrs::CIRCUIT_PRESSURE_INSP_ZERO_COUNTS_TITLE,
									MiscStrs::EST_TEST_RESULT_COUNT_UNIT);
		setupDisplayableTestResult_(SmDataId::CIRCUIT_PRESSURE_EXH_ZERO_COUNTS, 2, &idx,
									MiscStrs::CIRCUIT_PRESSURE_EXH_ZERO_COUNTS_TITLE,
									MiscStrs::EST_TEST_RESULT_COUNT_UNIT);
		formatLabel_(MiscStrs::CIRCUIT_PRESSURE_INSP_PRESSURE_AT_10_TITLE, labelUnitId, labelBuffer);
		setupDisplayableTestResult_(SmDataId::CIRCUIT_PRESSURE_INSP_PRESSURE_AT_10, 2, &idx,
									labelBuffer, unitId);
		formatLabel_(MiscStrs::CIRCUIT_PRESSURE_EXH_PRESSURE_AT_10_TITLE, labelUnitId, labelBuffer);
		setupDisplayableTestResult_(SmDataId::CIRCUIT_PRESSURE_EXH_PRESSURE_AT_10, 2, &idx,
									labelBuffer, unitId);
		setupDisplayableTestResult_(SmDataId::CIRCUIT_PRESSURE_INSP_AUTOZERO_PRESSURE, 2, &idx,
									MiscStrs::CIRCUIT_PRESSURE_INSP_AUTOZERO_PRESSURE_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::CIRCUIT_PRESSURE_EXH_AUTOZERO_PRESSURE, 2, &idx,
									MiscStrs::CIRCUIT_PRESSURE_EXH_AUTOZERO_PRESSURE_TITLE,
									unitId);
		formatLabel_(MiscStrs::CIRCUIT_PRESSURE_INSP_PRESSURE_AT_50_TITLE, labelUnitId, labelBuffer);
		setupDisplayableTestResult_(SmDataId::CIRCUIT_PRESSURE_INSP_PRESSURE_AT_50, 2, &idx,
									labelBuffer, unitId);
		formatLabel_(MiscStrs::CIRCUIT_PRESSURE_EXH_PRESSURE_AT_50_TITLE, labelUnitId, labelBuffer);
		setupDisplayableTestResult_(SmDataId::CIRCUIT_PRESSURE_EXH_PRESSURE_AT_50, 2, &idx,
									labelBuffer, unitId);
		formatLabel_(MiscStrs::CIRCUIT_PRESSURE_INSP_PRESSURE_AT_100_TITLE, labelUnitId, labelBuffer);
		setupDisplayableTestResult_(SmDataId::CIRCUIT_PRESSURE_INSP_PRESSURE_AT_100, 2, &idx,
									labelBuffer, unitId);
		formatLabel_(MiscStrs::CIRCUIT_PRESSURE_EXH_PRESSURE_AT_100_TITLE, labelUnitId, labelBuffer);
		setupDisplayableTestResult_(SmDataId::CIRCUIT_PRESSURE_EXH_PRESSURE_AT_100, 2, &idx,
									labelBuffer, unitId);

		maxDataCount_ = SmDataId::CIRCUIT_PRESSURE_TEST_END -
						SmDataId::CIRCUIT_PRESSURE_TEST_START + 1;
		break;

	case SmTestId::SAFETY_SYSTEM_TEST_ID:				// $[TI14]
		setupDisplayableTestResult_(SmDataId::SAFETY_SYSTEM_INSPIRATORY_PRESSURE1, 2, &idx,
									MiscStrs::SAFETY_SYSTEM_INSPIRATORY_PRESSURE1_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT1, 2, &idx,
									MiscStrs::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT1_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT2, 2, &idx,
									MiscStrs::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT2_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT3, 2, &idx,
									MiscStrs::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT3_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT4, 2, &idx,
									MiscStrs::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT4_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::SAFETY_SYSTEM_ELAPSED_TIME, 2, &idx,
									MiscStrs::SAFETY_SYSTEM_ELAPSED_TIME_TITLE,
									MiscStrs::EST_TEST_RESULT_MSECOND_UNIT);

		maxDataCount_ = SmDataId::SAFETY_SYSTEM_TEST_END -
						SmDataId::SAFETY_SYSTEM_TEST_START + 1;
		break;

	case SmTestId::EXH_VALVE_SEAL_TEST_ID:				// $[TI16]
		setupDisplayableTestResult_(SmDataId::EXH_VALVE_SEAL_EXH_MOTOR_TEMP, 2, &idx,
									MiscStrs::EXH_VALVE_SEAL_EXH_MOTOR_TEMP_TITLE,
									MiscStrs::EST_TEST_RESULT_DEGREE_C_UNIT);
		setupDisplayableTestResult_(SmDataId::EXH_VALVE_SEAL_DELTA_PRESSURE, 2, &idx,
									MiscStrs::EXH_VALVE_SEAL_DELTA_PRESSURE_TITLE,
									unitId);

		maxDataCount_ = SmDataId::EXH_VALVE_SEAL_TEST_END -
						SmDataId::EXH_VALVE_SEAL_TEST_START + 1;
		break;

	case SmTestId::EXH_VALVE_TEST_ID:					// $[TI17]
		setupDisplayableTestResult_(SmDataId::EXH_VALVE_EXHALATION_PRESSURE_10, 2, &idx,
									MiscStrs::EXH_VALVE_EXHALATION_PRESSURE_10_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::EXH_VALVE_EXHALATION_PRESSURE_45, 2, &idx,
									MiscStrs::EXH_VALVE_EXHALATION_PRESSURE_45_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::EXH_VALVE_EXHALATION_PRESSURE_95, 2, &idx,
									MiscStrs::EXH_VALVE_EXHALATION_PRESSURE_95_TITLE,
									unitId);
		maxDataCount_ = SmDataId::EXH_VALVE_TEST_END -
						SmDataId::EXH_VALVE_TEST_START + 1;
		break;

	case SmTestId::SM_LEAK_TEST_ID:					// $[TI18]

		setupDisplayableTestResult_(SmDataId::SM_LEAK_MONITOR_PRESSURE, 2, &idx,
									MiscStrs::SM_LEAK_MONITOR_PRESSURE_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::SM_LEAK_DELTA_PRESSURE, 2, &idx,
									MiscStrs::SM_LEAK_DELTA_PRESSURE_TITLE,
									unitId);

		maxDataCount_ = SmDataId::SM_LEAK_TEST_END -
						SmDataId::SM_LEAK_TEST_START + 1;

		// $[07054] When the EST leak test is in progress, a real-time ...
		leakGauge_.setShow(TRUE);
		break;

	case SmTestId::EXH_HEATER_TEST_ID:					// $[TI19]
		setupDisplayableTestResult_(SmDataId::EXH_HEATER_EXHALATION_TEMPERATURE1, 2, &idx,
									MiscStrs::EXH_HEATER_EXHALATION_TEMPERATURE1_TITLE,
									MiscStrs::EST_TEST_RESULT_DEGREE_C_UNIT);
		setupDisplayableTestResult_(SmDataId::EXH_HEATER_EXHALATION_TEMPERATURE2, 2, &idx,
									MiscStrs::EXH_HEATER_EXHALATION_TEMPERATURE2_TITLE,
									MiscStrs::EST_TEST_RESULT_DEGREE_C_UNIT);
		setupDisplayableTestResult_(SmDataId::EXH_HEATER_EXHALATION_TEMPERATURE3, 2, &idx,
									MiscStrs::EXH_HEATER_EXHALATION_TEMPERATURE3_TITLE,
									MiscStrs::EST_TEST_RESULT_DEGREE_C_UNIT);

		maxDataCount_ = SmDataId::EXH_HEATER_TEST_END -
						SmDataId::EXH_HEATER_TEST_START + 1;
		break;

	case SmTestId::GENERAL_ELECTRONICS_TEST_ID:
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING0, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING0_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING1, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING1_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING2, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING2_TITLE,
									MiscStrs::EST_TEST_RESULT_PSIA_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING3, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING3_TITLE,
									MiscStrs::EST_TEST_RESULT_DEGREE_C_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING4, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING4_TITLE,
									MiscStrs::EST_TEST_RESULT_DEGREE_C_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING5, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING5_TITLE,
									MiscStrs::EST_TEST_RESULT_DEGREE_C_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING6, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING6_TITLE,
									MiscStrs::EST_TEST_RESULT_DEGREE_C_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING7, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING7_TITLE,
									MiscStrs::EST_TEST_RESULT_DEGREE_C_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING8, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING8_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING9, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING9_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING10, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING10_TITLE,
									MiscStrs::EST_TEST_RESULT_LPM_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING11, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING11_TITLE,
									MiscStrs::EST_TEST_RESULT_COUNT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING12, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING12_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING13, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING13_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING14, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING14_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING15, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING15_TITLE,
									MiscStrs::EST_TEST_RESULT_MAMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING16, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING16_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING17, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING17_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING18, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING18_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING19, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING19_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING20, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING20_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING21, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING21_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING22, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING22_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING23, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING23_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING24, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING24_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING25, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING25_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING26, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING26_TITLE,
									MiscStrs::EST_TEST_RESULT_AMPS_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING27, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING27_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING28, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING28_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING29, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING29_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING30, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING30_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING31, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING31_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::GENERAL_ELECTRONICS_READING32, 2, &idx,
									MiscStrs::GENERAL_ELECTRONICS_READING32_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);

		maxDataCount_ = SmDataId::GENERAL_ELECTRONICS_TEST_END -
						SmDataId::GENERAL_ELECTRONICS_TEST_START + 1;
		break;
	case SmTestId::GUI_TOUCH_TEST_ID:				// $[TI22]
		break;
	case SmTestId::GUI_SERIAL_PORT_TEST_ID:			// $[TI23]
		break;
	case SmTestId::BATTERY_TEST_ID:					// $[TI24]
		setupDisplayableTestResult_(SmDataId::BATTERY_VOLTAGE1, 2, &idx,
									MiscStrs::BATTERY_VOLTAGE1_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::BATTERY_VOLTAGE2, 2, &idx,
									MiscStrs::BATTERY_VOLTAGE2_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);
		setupDisplayableTestResult_(SmDataId::BATTERY_DELTA_VOLTAGE, 2, &idx,
									MiscStrs::BATTERY_DELTA_VOLTAGE_TITLE,
									MiscStrs::EST_TEST_RESULT_VOLT_UNIT);

		maxDataCount_ = SmDataId::BATTERY_TEST_END -
						SmDataId::BATTERY_TEST_START + 1;
		break;
	case SmTestId::GUI_NURSE_CALL_TEST_ID:				// $[TI25]
		break;
	default:
		CLASS_ASSERTION_FAILURE();
		break;		
	}

	// Remember the maximum displayable data count
	maxDisplayableDataCount_ = idx;

#if defined FORNOW
printf("EstResultSubScreen::setDataFormat() testId_=%d,  maxDataCount=%d, maxDisplayableDataCount_ = %d\n",
		testId_, maxDataCount_, maxDisplayableDataCount_);
#endif	// FORNOW

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearScreen
//
//@ Interface-Description
//  Sets the subscreen into empty state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We hide the titleArea_, and all the test data related value, title,
//  and unit drables.  We also hide the leakGauge.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstResultSubScreen::clearScreen(void)
{
	CALL_TRACE("EstResultSubScreen::clearScreen(void)");

	Int idx;

#if defined FORNOW
printf("EstResultSubScreen::clearScreen() at line  %d\n", __LINE__);
#endif	// FORNOW

	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	titleArea_.setShow(FALSE);
	maxDataCount_ = 0;

	// Hide the following drawables
	for (idx = maxDataCount_; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		testResultValueArray_[idx].setShow(FALSE);
		testResultLabelArray_[idx].setShow(FALSE);
		testResultUnitArray_[idx].setShow(FALSE);
	}

	// Hide the leak gauge
	leakGauge_.setShow(FALSE);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setText_
//
//@ Interface-Description
//  Format then store the given resultString into a given text field.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We first format the temporary string with the test result data.
//  Then call the textField method: setText() to set the text value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 
void
EstResultSubScreen::setText_(TextField *testResultField, wchar_t *resultString)
{
	CALL_TRACE("EstResultSubScreen::setText_(TextField &testResultField, char *resultString)");

#if defined FORNOW
printf("EstResultSubScreen::setText_() at line  %d\n", __LINE__);
#endif	// FORNOW

	wchar_t formatString[TextField::MAX_STRING_LENGTH+1];  

	swprintf(formatString, L"{p=8,y=11:%s}", resultString);

	testResultField->setText(formatString);
										// $[TI3]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupDisplayableTestResult_
//
//@ Interface-Description
//  This method is to provide an generic function for formating test data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We first inform the testManager of the precision of this data to be
//  displayed.  Then we set this data to be displayable.  Followed by
//  setting the unit and title for this data.  Calculate the display
//  position for the data value, unit and label.  Finally, we increment
//  the maximun displayable count for this EST test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstResultSubScreen::setupDisplayableTestResult_(SmDataId::ItemizedTestDataId testDataIdx,
												Int16 dataPrecision, Int16 *currentDisplayableCount,
												StringId labelString, StringId unitString)
{

	Int16  rowIncrement;
	Int16  colIncrement;
	Int16  rowPosition;
	Int16  colPosition;
	
#if defined FORNOW
printf("setupDisplayableTestResult() at line  %d, testDataIdx=%d, currentDisplayableCount=%d\n",
				__LINE__, testDataIdx, *currentDisplayableCount);
#endif	// FORNOW

	// Set the precision
	testManager_.setPrecision(testDataIdx, dataPrecision);
	isTestResultDisplayable_[testDataIdx] = TRUE;

	// Set the label and the unit strings
	testResultLabelArray_[testDataIdx].setText(labelString);
	testResultUnitArray_[testDataIdx].setText(unitString);

	// Calculate the row and col for the texts
	rowIncrement = *currentDisplayableCount % DISPLAY_ITEMS_PER_COL_;
	colIncrement = *currentDisplayableCount / DISPLAY_ITEMS_PER_COL_;

#if defined FORNOW
printf("   rowIncrement=%d, colIncrement=%d\n",	rowIncrement, colIncrement);
#endif	// FORNOW

	// Calculate the row, and col pixel location for value string.
	rowPosition = VALUE_Y_ + Y_OFFSET_ * rowIncrement;
	colPosition = VALUE_X_ + X_OFFSET_ * colIncrement;

	testResultValueArray_[testDataIdx].setX(colPosition);
	testResultValueArray_[testDataIdx].setY(rowPosition);

#if defined FORNOW
printf("   testResultValueArray[%d], rowPosition=%d, colPosition=%d\n", testDataIdx, rowPosition, colPosition);
#endif	// FORNOW

	// Calculate the row, and col pixel location for labael string.
	rowPosition = LABEL_Y_ + Y_OFFSET_ * rowIncrement;
	colPosition = LABEL_X_ + X_OFFSET_ * colIncrement;

	testResultLabelArray_[testDataIdx].setX(colPosition);
	testResultLabelArray_[testDataIdx].setY(rowPosition);

#if defined FORNOW
printf("   testResultLabelArray[%d], rowPosition=%d, colPosition=%d\n", testDataIdx, rowPosition, colPosition);
#endif	// FORNOW

	// Calculate the row, and col pixel location for unit string.
	rowPosition = UNIT_Y_ + Y_OFFSET_ * rowIncrement;
	colPosition = UNIT_X_ + X_OFFSET_ * colIncrement;

	testResultUnitArray_[testDataIdx].setX(colPosition);
	testResultUnitArray_[testDataIdx].setY(rowPosition);

#if defined FORNOW
printf("   testResultUnitArray[%d], rowPosition=%d, colPosition=%d\n", testDataIdx, rowPosition, colPosition);
#endif	// FORNOW

	(*currentDisplayableCount)++;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: formatLabel_
//
//@ Interface-Description
//  This method is to provide an generic function for formating the label
//  of the testdata.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We first search for ":" in the label string.  We then copy the
//  result into a temporary text buffer and remove any characters after
//	the "}" character.  Finally, we format the desired label text into
//  "labelBuffer" buffer.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
EstResultSubScreen::formatLabel_(StringId labelString, StringId labelUnitId, wchar_t *labelBuffer)
{

	CALL_TRACE("EstResultSubScreen::formatLabel_(StringId labelString, StringId labelUnitId)");

	wchar_t labelTextString[200];
	StringId labelTextPtr;
	size_t len;
	
	CLASS_ASSERTION((labelTextPtr = wcsstr(labelString, L":")));
	
	wcscpy(labelTextString, labelTextPtr);
	len = wcscspn(labelTextString, L"}");
	CLASS_ASSERTION(len);
	
	labelTextString[len] = NULL;
		
	swprintf(labelBuffer, L"%s%s%s%s", L"{p=8,y=11", labelTextString, labelUnitId, L"}");
										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
EstResultSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
			ESTRESULTSUBSCREEN, lineNumber, pFileName, pPredicate);
}

