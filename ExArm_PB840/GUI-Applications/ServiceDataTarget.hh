#ifndef ServiceDataTarget_HH
#define ServiceDataTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceDataTarget - A target to handle changes to GUI App's service
// data.  Classes can specify this class as an additional parent class
// register to be informed of changed service data.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceDataTarget.hhv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


//@ Usage-Classes
#include "TestDataId.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class ServiceDataTarget
{
public:
	virtual void serviceDataChangeHappened(TestDataId::TestDataItemId testId)=0;

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	ServiceDataTarget(void);
	virtual ~ServiceDataTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	ServiceDataTarget(const ServiceDataTarget&);	// not implemented...
	void operator=(const ServiceDataTarget&);	// not implemented...
};


#endif // ServiceDataTarget_HH 
