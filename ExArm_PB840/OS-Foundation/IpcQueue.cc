#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: IpcQueue - A abstract class for passing Int32's between tasks.
//---------------------------------------------------------------------
//@ Interface-Description
//  IpcQueue is a base class for MsgQueue and MailBox classes.
//  The IpcQueue class provides inter-task communication,
//  synchronization and buffering using Int32's.
//  Some of the methods provided include pendForMsg() or acceptMsg() to 
//  retrieve messages from the queue.  
//---------------------------------------------------------------------
//@ Rationale
//  IpcQueue is a base class for inter-task communication classes.
//  The IpcQueue class was written to hide the details of using the VRTX32  
//  queue mechanism, to isolate problems related to inter-task communication,
//  and to create a safe and simple-to-use interface for passing messages 
//  between tasks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  IpcQueue uses the VRTX32 sc_q* calls to create and communicate to 
//  queues, which live in the VRTX32 Workspace.  
//---------------------------------------------------------------------
//@ Fault-Handling
//  Methods that can produce recoverable errors, such as Ipc::NO_MESSAGE() 
//  from acceptMsg() or Ipc::TIMED_OUT from pendForMsg() will be handled
//  by the calling routine. Return codes are listed in enum Ipc::ReturnCode. 
//  If a message was successfully received or sent the Returncode Ipc::OK 
//  is returned. Assertions are used to catch fault conditions, along with 
//  pre and post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//  There can not be any objects of this abstract class.
//---------------------------------------------------------------------
//@ Invariants
//  (QID_ > 0 && QID_ < IpcId::MAX_QID)
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/IpcQueue.ccv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 005   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004    By: gdc              Date: 24-FEB-1998  DR Number: 2738
//    Project: Sigma (R8027)
//    Description:
//       Added Ipc::Q_FULL as valid return value from sc_qinquiry(). MRI
//       documentation for this function call is incorrect. Added 
//       auxiliary information in assertion calls to provide return
//       values from VRTX calls.
//
//  Revision: 003    By: hct              Date: 24-APR-1997  DR Number: 1994
//    Project: Sigma (R8027)
//    Description:
//       Deleted requirement numbers 00444.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "IpcQueue.hh"
#include "IpcTable.hh"
#include "IpcIds.hh"
#include "OsTimeStamp.hh"

#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT


IPC_Q_HANDLE IpcQueueList::IpcQueueList_[MAX_QID];

// ==============
// IpcQueue class
// ==============
 
//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//@ Method: IpcQueue() 
//
//@ Interface-Description
//    Constructs a IpcQueue object which will be a handle to an
//    already created queue with the given length, and name.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Initializes private data from the IpcTable[] array.  All the
//    actual VRTX32 queues are created at initialization time with
//    IpcQueue::Create_(), and this constructor simply creates a handle
//    to the queue.
//---------------------------------------------------------------------
//@ PreCondition
//  (qId > 0 && qId < MAX_QID)
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
IpcQueue::IpcQueue(const Ipc::IpcType ipcType, const Int32 qId) 
    : ipcType_(ipcType), QID_(qId)
{
    CLASS_PRE_CONDITION(qId > 0 && qId < ::MAX_QID);
    // get the IPC table entry for this qId
    const IpcTableEntry & ipcEntry = IpcTable::GetEntryById(qId);

    pQueueName_     = ipcEntry.name;
    qLength_        = ipcEntry.length;
    error_          = Ipc::OK;
    waitOrder_      = Ipc::PRIORITY_ORDER;
	pQueueHandle_	= IpcQueueList::GetQueueById(qId);
				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~IpcQueue 
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
IpcQueue::~IpcQueue()
{
    // don't delete Queue, somebody else might be using it.
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Create_()
//
//@ Interface-Description
//  This static method creates a queue for the id, length, and waitOrder
//  specified. The wait order is defaulted to PRIORITY_ORDER. This method
//  is a  protected member called by the derived classes MsgQueue and MailBox.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the OS interface that creates an OS queue.
//---------------------------------------------------------------------
//@ PreCondition
//  (waitOrder == Ipc::PRIORITY_ORDER)
//  The VRTX32 sc_qecreate call will return Ipc::BAD_QID if the queue does not
//  exist. This error code will cause an assertion.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void IpcQueueList::Create_(const IpcTableEntry* ipcEntry)
{
	CLASS_PRE_CONDITION(ipcEntry != NULL)
	
#if defined(_WIN32)	|| defined(_WIN32_WCE)
	DWORD error = Ipc::OK;
	const Int32 id = ipcEntry->id;
	WCHAR name[IpcTable::MAX_Q_NAME_STRING_LENGTH];
	mbstowcs(name, ipcEntry->name, IpcTable::MAX_Q_NAME_STRING_LENGTH);
	MSGQUEUEOPTIONS options;
	options.dwSize = sizeof(MSGQUEUEOPTIONS);
	options.dwFlags = 0;//TODO VM test if we need MSGQUEUE_NOPRECOMMIT or MSGQUEUE_ALLOW_BROKEN
	options.dwMaxMessages = ipcEntry->length;   //Note: Windows allows for setting this (0) for unlimited number of messages
	options.cbMaxMessage = IpcQueue::IPC_MSG_SIZE; //pre-defined IPC message size
	options.bReadAccess = TRUE; //creating the Read access handle to use for "pend". It signals when a message is put
	IpcQueueList_[id].rxQhandle = CreateMsgQueue(name, &options);
	if(IpcQueueList_[id].rxQhandle == NULL)
	{
		error = GetLastError();
	}
	else
	{
		SetLastError(0);
	}
	//TODO E600 VM upgrade to __try / except handling
	AUX_CLASS_ASSERTION( (IpcQueueList_[id].rxQhandle != NULL), error | id<<24)

	options.bReadAccess = FALSE; //now creating the Write access handle to use for "put". It signals when Q is empty
	IpcQueueList_[id].txQhandle = CreateMsgQueue(name, &options);
	if(IpcQueueList_[id].txQhandle == NULL)
	{
		error = GetLastError();
	}
	else
	{
		SetLastError(0);
	}
	//TODO E600 VM upgrade to __try / except handling
	AUX_CLASS_ASSERTION( (IpcQueueList_[id].txQhandle != NULL), error | id<<24)	
#endif
    					//$[TI1]
}

const IPC_Q_HANDLE* IpcQueueList::GetQueueById(const Int32 id)
{
	AUX_CLASS_PRE_CONDITION( 0 < id && id <= MAX_QID, id );
	AUX_CLASS_ASSERTION( IpcQueueList_[id].rxQhandle != NULL && IpcQueueList_[id].txQhandle != NULL, id );
	return &IpcQueueList_[id];
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: putMsg()
//
//@ Interface-Description
//  This method is a pure virtual function used by the derived classes of
//  MailBox and MsgQueue.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: mapOsErrorToIpcError()
//
//@ Interface-Description
//  Maps an OS specific error code tothe equivalent returned Ipc enum
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Ipc::ReturnCode IpcQueue::mapOsIpcError(Int32 osError)
{
	Ipc::ReturnCode error;

	switch(osError)
	{
	case ERROR_INSUFFICIENT_BUFFER: //TODO E600: this Windos error does not really mean Q full!
		error = Ipc::Q_FULL; 
		break;
	case ERROR_OUTOFMEMORY:
		error = Ipc::NO_MEMORY;
		break;
	case ERROR_TIMEOUT: 
		error = Ipc::TIMED_OUT; 
		break;
	case ERROR_PIPE_NOT_CONNECTED: 
		//Let this fall through to default. There is really no other MSDN documented error
		//return code for this Queue API
	default:
		error = Ipc::BAD_QID; 
		break;
	}

	return error;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: pendForMsg()
//
//@ Interface-Description
//  Returns the next message , if any, from the queue in the specified
//  time period. Uses a reference to an Int32 which holds the returned message.
//  Also takes a Uint32 timeout value in milliseconds, which 
//  if it expires without a message arriving the error Ipc::TIMED_OUT will 
//  be returned.  The value of NO_TIMEOUT can be passed as the timeout 
//  value, and is the default. This causes the pend to wait until a message
//  is received. Ipc::OK is returned if any pend is successful.
//  To check the queue without the possibility of waiting use acceptMsg().
//  This call can not be made prior to tasking, or from an Interrupt Service 
//  routine.
//---------------------------------------------------------------------
//@ Implementation-Description
//  pendForMsg() calls ReadMsgQueue with the timeout that is passed in
//---------------------------------------------------------------------
//@ PreCondition
//    (IpcTable::IsInitialized() == TRUE)
//---------------------------------------------------------------------
//@ PostCondition
//              none
//@ End-Method
//=====================================================================
Int32 IpcQueue::pendForMsg(Int32& rMsg, const Uint32 waitTime)
{
    CLASS_PRE_CONDITION( IpcTable::IsInitialized() );

#if defined(_WIN32)	|| defined(_WIN32_WCE)
	DWORD read;
	DWORD flags;
	LPVOID msgPtr = &rMsg;
	if(FALSE == ReadMsgQueue(pQueueHandle_->rxQhandle, msgPtr, IPC_MSG_SIZE, &read, waitTime, &flags))
	{
		DWORD ret = GetLastError();
		error_ = mapOsIpcError(ret);
	}
	else
	{
		error_ = Ipc::OK;
	}
#else
	error_ = Ipc::BAD_QID;
#endif
	Uint32 auxErrorCode = error_ | QID_<<24;
    AUX_CLASS_ASSERTION( (error_ == Ipc::OK) || (error_ == Ipc::TIMED_OUT), auxErrorCode);

    return error_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptMsg()
//
//@ Interface-Description
//  To receive/remove a message , if any, from the queue without waiting.
//  Takes a reference to an Int32, which will hold the received 
//  message upon successful return.  If there is no message in the
//  queue then it returns immediately with the return value
//  Ipc::NO_MESSAGE.  Ipc::OK is returned if a message was received.
//  To pend for a message with a timeout, use pendForMsg().
//  This call can be made before tasking or from an interrupt service
//  routine.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls ReadMsgQueue with no wait time to retrieve a message, if one 
//  exists, from the queue.
//---------------------------------------------------------------------
//@ PreCondition
//              none
//---------------------------------------------------------------------
//@ PostCondition
//              none
//@ End-Method
//=====================================================================
Int32 IpcQueue::acceptMsg(Int32& rMsg)
{
    CLASS_PRE_CONDITION( IpcTable::IsInitialized() );

#if defined(_WIN32)	|| defined(_WIN32_WCE)
	DWORD read;
	DWORD flags;
	//if the queue is empty, return the "internally defined" NO_MESSAGE because
	//Windows ReadMsgQueue does not return such a value..
	if(numMsgsAvail() == 0)
	{
		error_ = Ipc::NO_MESSAGE;
	}
	else
	{
		//Q has messages, read one "without waiting"..
		LPVOID msgPtr = &rMsg;
		if(FALSE == ReadMsgQueue(pQueueHandle_->rxQhandle, msgPtr, IPC_MSG_SIZE, &read, Ipc::NO_WAIT, &flags))
		{
			DWORD ret = GetLastError();
			error_ = mapOsIpcError(ret);
		}
		else
		{
			error_ = Ipc::OK;
		}
	}
#else
	error_ = Ipc::BAD_QID;
#endif
	Uint32 auxErrorCode = error_ | QID_<<24;
    AUX_CLASS_ASSERTION( (error_ == Ipc::OK) || (error_ == Ipc::NO_MESSAGE), auxErrorCode);

    return error_;
    				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: numMsgsAvail()
//
//@ Interface-Description
// Takes no arguments and returns the number of messages currently on the
// queue. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls GetMsgQueueInfo() and returns the dwCurrentMessages count.
// It is possible that this call is interrupted after the GetMsgQueueInfo()
// call but before the return, and to have more messages put into the
// IpcQueue.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32   IpcQueue::numMsgsAvail(void)
{
	Int32 msgCount = 0;
#if defined(_WIN32)	|| defined(_WIN32_WCE)
	MSGQUEUEINFO qInfo;
	qInfo.dwSize = sizeof(MSGQUEUEINFO);	//TODO E600 VM do we really need to set this and why?
	if(GetMsgQueueInfo(getHandle()->rxQhandle, &qInfo) == TRUE)
	{
		error_ = Ipc::OK;
		msgCount = qInfo.dwCurrentMessages;
	}
	else
	{
		//Unlike Read/Write APIs, the GetMsgQueue doesnt have any documented 
		//error codes for failure. Just set it to a bad queue id.
		error_ = Ipc::BAD_QID; 
	}

	Uint32 auxErrorCode = error_ | QID_<<24;
	AUX_CLASS_ASSERTION(error_ == Ipc::OK, auxErrorCode);
#else
	error_ = Ipc::BAD_QID;
#endif

	return msgCount;		// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: flush()
//
//@ Interface-Description
//  Takes no arguments, and discards any messages currently on the queue.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls numMsgsAvail() to get the number of messages in the queue,
//  and then calls acceptMsg() that many times.  The reason that it
//  just doesn't call acceptMsg() until no messages are available, is
//  that another task could be interrupting this one, and putting the
//  messages in, and this function could loop forever.
//---------------------------------------------------------------------
//@ PreCondition
//              none
//---------------------------------------------------------------------
//@ PostCondition
//              none
//@ End-Method
//=====================================================================
void   IpcQueue::flush(void)
{
    Int32 msgCount = numMsgsAvail();
    Int32 msgBuff;

    for (Int32 i=0; i < msgCount; i++)
    {
        error_ = acceptMsg(msgBuff);
    }
}

#ifdef	SIGMA_DEVELOPMENT
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PrintError_(const Int32 errno)
//
//@ Interface-Description
//  Takes an Int32 error number, and prints out the error message
//  associated with this number; returns nothing.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  Does a switch on the error number, and if a match is found, calls
//  cout to print the error message.
//---------------------------------------------------------------------
//@ PreCondition
//              none
//---------------------------------------------------------------------
//@ PostCondition
//              none
//@ End-Method
//=====================================================================
void IpcQueue::PrintError_(const Int32 errno)
{
   cout << "Ipc Error:" << (int) errno << ",";
   switch (errno) {
      case Ipc::TIMED_OUT:
         cout << "Timeout" << endl;
         break;
      case Ipc::NO_MESSAGE:
         cout << "No Message Present" << endl;
         break;
      case Ipc::Q_FULL:
         cout << "Queue Full" << endl;
         break;
      case Ipc::BAD_QID:
         cout << "Queue ID Error" << endl;
         break;
      case Ipc::NO_MEMORY:
         cout << "No memory Available" << endl;
         break;
      case Ipc::OK:
         cout << "Successful return" << endl;
         break;
      default:
         cout << "Unkown Error code" << endl;
    }
}
#endif // SIGMA_DEVELOPMENT

#ifdef	SIGMA_DEVELOPMENT
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free-Function: operator <<
//
//@ Interface-Description
//  Overloads the Ostream << operator so that a IpcQueue object can
//  be put into a cout statement, e.g., cout << myqueue << endl;
//---------------------------------------------------------------------
//@ Implementation-Description
//  Puts name, Id, and length into the Ostream and returns it.
//---------------------------------------------------------------------
//@ PreCondition
//              none
//---------------------------------------------------------------------
//@ PostCondition
//              none
//@ End-Method
//=====================================================================
 
Ostream & operator << (Ostream &rOs,const IpcQueue &rQueue)
{
    rOs << rQueue.getName() << "(" << (int) rQueue.getId() << "),"
        << (int) rQueue.getLength();
    return rOs;
}

#endif // SIGMA_DEVELOPMENT



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
IpcQueue::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::IPC_QUEUE,
     lineNumber, pFileName, pPredicate);
}
