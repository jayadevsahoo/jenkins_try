
#ifndef WobGraphic_HH
#define WobGraphic_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: WobGraphic - Displays a sliding-scale representation of PAV's
// patient work-of-breathing (WOBpt), along with indication as to the breakdown
// between resistive and elastive contributions to this total WOB (WOBtot).
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WobGraphic.hhv   25.0.4.0   19 Nov 2013 14:08:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah    Date:  22-Jan-2001    DCS Number: 5835 & 5841
//  Project:  PAV
//  Description:
//      No longer display WOB data during Apnea, Safety-PCV, OSC and
//      Disconnect.
//
//  Revision: 001    By: sah    Date: 20-Nov-2000   DCS Number: 5805
//  Project:  PAV
//  Description:
//      Added for PAV data representation.
//
//====================================================================

#include "Container.hh"
#include "PatientDataTarget.hh"
#include "BdEventTarget.hh"

//@ Usage-Classes
#include "Box.hh"
#include "Line.hh"
#include "Triangle.hh"
#include "TouchableText.hh"
#include "NumericField.hh"
//@ End-Usage


class WobGraphic : public Container, 
					public PatientDataTarget,
					public BdEventTarget
{
public:
	WobGraphic(void);
	~WobGraphic(void);

	// Drawable virtual method
	virtual void activate(void);
	virtual void deactivate(void);

	// PatientDataTarget virtual method
	virtual void patientDataChangeHappened(
								PatientDataId::PatientItemId patientDataId);

	// BdEventTarget virtual method...
	virtual void  bdEventHappened(EventData::EventId     eventId,
								  EventData::EventStatus eventStatus,
								  EventData::EventPrompt eventPrompt =
												  EventData::NULL_EVENT_PROMPT);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
protected:

private:
	WobGraphic(const WobGraphic&);			// not implemented...
	void operator=(const WobGraphic&);		// not implemented...

	Int32  valueToPoint_(const Real32  wobValue,
						 const Boolean isRelativeToGraphic) const;

	Container  graphicContainer_;
	TextField  graphicTitleText_;

	//-----------------------------------------------------------------
	//  Standard look of "WOBtot" display:
	//
	//     |----------|
	//     |  WOBtot  |				WOBtot container and label
	//     |----------|
	//          \/					triangle pointer
	//-----------------------------------------------------------------
	Container      totalWobLabelContainer_;
	TouchableText  totalWobLabel_;
	Triangle       totalWobPointer_;


	//-----------------------------------------------------------------
	//  Extended look of "WOBtot" display:
	//
	//              |----------|
	//              |  WOBtot  |		WOBtot container and label
	//              |----------|
	//                    |				vert line #1
	//           ----------				horiz line
	//           |						vert line #1
	//          \/						triangle pointer
	//-----------------------------------------------------------------
	Line  totalWobVertLine1_;
	Line  totalWobHorizLine_;
	Line  totalWobVertLine2_;


	//-----------------------------------------------------------------
	//
	//     |----------|
	//     |****++++++|				WOBe/WOBr ratio box
	//     |   WOBpt  |				WOBpt label
	//     |----------|
	//
	//-----------------------------------------------------------------
	Container      patientWobInfoContainer_;
	Box            patientWobElastanceBar_;
	Box            patientWobResistanceBar_;
	TouchableText  patientWobLabel_;


	//-----------------------------------------------------------------
	//  "WOBpt" display:
	//
	//		E		 R				E and R labels
	//     |----------|
	//     |****++++++|				WOBe/WOBr ratio box
	//     |   WOBpt  |				WOBpt box and label
	//     |----------|
	//          \/					triangle pointer
	//-----------------------------------------------------------------
	Container      patientWobContainer_;
	TouchableText  elasticWobLabel_;
	TouchableText  resistiveWobLabel_;
	Triangle       patientWobPointer_;


	Container  scaleContainer_;
	Box        minRange_;
	Box        normalRange_;
	Box        highRange_;
	Box        maxRange_;

	TextField  minScaleValue_;
	TextField  lowCutoffValue_;
	TextField  normalCutoffValue_;
	TextField  highCutoffValue_;
	TextField  maxScaleValue_;
	TextField  scaleUnit_;
};


#endif // WobGraphic_HH 
