
#ifndef SortR_Alarm_HH
#define SortR_Alarm_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SortR_Alarm - Abstract Sorted List of 'Alarm' References.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSortR.H_v   25.0.4.0   19 Nov 2013 14:03:44   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "DListR_Alarm.hh"
//@ End-Usage


class SortR_Alarm
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of instances of this class.
    friend inline Ostream&  operator<<(Ostream&                 ostr,
				       const SortR_Alarm& sortList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

    //@ Friend:  SortIterR_Alarm
    // To allow access to 'getFirst_()', 'getCurrent_()', and 'getLast_()'.
    friend class  SortIterR_Alarm;

  public:
    virtual ~SortR_Alarm(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    virtual Boolean  isSorted(void) const = 0;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    SigmaStatus  search(Alarm&                 item,
			const EquivResolution foundWhen = EQUIV_OBJECTS);

    void  addItem(Alarm& newItem);

    SigmaStatus  removeItem(Alarm&                 oldItem,
			    const EquivResolution foundWhen = EQUIV_OBJECTS);
    inline SigmaStatus  removeCurrent(void);
    inline void  clearList(void);

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);
    inline SigmaStatus  goPrev (void);
    inline SigmaStatus  goLast (void);

    inline const Alarm&  currentItem(void) const;

    inline void  operator=(const SortR_Alarm& sortList);

    inline Boolean  isEquivTo(const SortR_Alarm& sortList,
			      const EquivResolution    equivWhen) const;

    inline Boolean  operator==(const SortR_Alarm& sortList) const;
    inline Boolean  operator!=(const SortR_Alarm& sortList) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName = NULL,
			   const char*       pBoolTest = NULL);

  protected:
    inline SortR_Alarm(DListR_Alarm* pList);

    const DNodeC_MemPtr*  findNode_(Alarm&                 item,
					   const EquivResolution foundWhen
					  ) const;

    inline Boolean  isLegalNode_(const DNodeC_MemPtr* pNode) const;

    inline const DNodeC_MemPtr*  getFirst_  (void) const;
    inline const DNodeC_MemPtr*  getCurrent_(void) const;
    inline const DNodeC_MemPtr*  getLast_   (void) const;

    inline DNodeC_MemPtr*  getFirst_  (void);
    inline DNodeC_MemPtr*  getCurrent_(void);
    inline DNodeC_MemPtr*  getLast_   (void);

    inline void  setCurrent_(DNodeC_MemPtr* pNewCurr);

    // get a reference to 'pNode's item...
    inline const Alarm&  getItem_(const DNodeC_MemPtr* pNode) const;
    inline Alarm&        getItem_(DNodeC_MemPtr*       pNode);

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    SortR_Alarm(const SortR_Alarm&);	// not implemented...
    SortR_Alarm(void);				// not implemented...

    //@ Data-Member:  rElemList_
    // A reference to the list of elements of this sorted list.
    DListR_Alarm&  rElemList_;
};


// Inlined methods...
#include "SortR_Alarm.in"


#endif  // SortR_Alarm_HH
