
// PB880SWATUnitTestDlg.cpp : implementation file
//

#include "stdafx.h"

#include "PB880SWATUnitTest.h"
#include "PB880SWATUnitTestDlg.h"

#include "ScriptInterpreter.h"
#include "TouchedPosition.h"
#include "UDPCommunication.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CPB880SWATUnitTestDlg dialog

CPB880SWATUnitTestDlg::CPB880SWATUnitTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPB880SWATUnitTestDlg::IDD, pParent)
	, m_strScriptFile(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPB880SWATUnitTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_SFILE, m_editScriptFile);
	DDX_Text(pDX, IDC_EDIT_SFILE, m_strScriptFile);
	DDX_Control(pDX, IDC_LIST_LOG, m_listLogs);
}

BEGIN_MESSAGE_MAP(CPB880SWATUnitTestDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CPB880SWATUnitTestDlg::OnBnClickedOk)
	ON_MESSAGE( WM_LUASCRIPT, &CPB880SWATUnitTestDlg::OnLuaScript )
	ON_MESSAGE( WM_WRITELOG, &CPB880SWATUnitTestDlg::OnLogMessage )
    ON_MESSAGE( WM_TOUCHED_POS, &CPB880SWATUnitTestDlg::OnTouchedPos )
END_MESSAGE_MAP()


// CPB880SWATUnitTestDlg message handlers

BOOL CPB880SWATUnitTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	InitializeControls(); 
	
    AfxBeginThread( CPB880SWATUnitTestDlg::TouchedPositionMonitorThreadProc, this );

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPB880SWATUnitTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPB880SWATUnitTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPB880SWATUnitTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CPB880SWATUnitTestDlg::InitializeControls()
{
	// set the default test script file to unit_test script, unit test script
	// proposed to call all these swatgui APIs
	m_strScriptFile = ".\\TestScripts\\ut_capture_screen.lua";
	UpdateData( FALSE );

	// initialize the log message list
	CRect rect;
	m_listLogs.GetClientRect(&rect);

	// add two columns on the report view, 'Time' and 'Messages'
	m_listLogs.InsertColumn(0, _T("Time"), LVCFMT_LEFT, 160);
	m_listLogs.InsertColumn(1, _T("Messages"), LVCFMT_LEFT, rect.Width() - 160);


	// allow full row select
	m_listLogs.SetExtendedStyle( LVS_EX_FULLROWSELECT ); 

	return TRUE;
}


void CPB880SWATUnitTestDlg::WriteLog( const TCHAR* pLogMsg )
{
	// make a copy of the message, the message handler of WM_WRITELOG
	// need to free this CString object
	CString* pLogCStr = new CString(pLogMsg);

	// write log will be called from worker thread, so we PostMessage to GUI thread
	PostMessage( WM_WRITELOG, (WPARAM)pLogCStr );
}


UINT CPB880SWATUnitTestDlg::TestScriptExecThreadProc(LPVOID lParam)
{
	CPB880SWATUnitTestDlg* pSwatGuiDlg = static_cast<CPB880SWATUnitTestDlg*>( lParam );

	// tell the GUI thread the thread already started
	pSwatGuiDlg->PostMessage( WM_LUASCRIPT, LUA_BEGIN );

	// create a log function for interpreter write logs
	HostLogFunction logFunc( pSwatGuiDlg, &CPB880SWATUnitTestDlg::WriteLog );

	// create a lua interpreter
	ScriptInterpreter interpreter( logFunc );

	CString strMsg;
	CString* pMsg = NULL;

	// run the lua script file
	if ( !interpreter.RunLuaFile( pSwatGuiDlg->m_strScriptFile, strMsg ) )
	{
		pMsg = new CString( strMsg );
	}
	
	// message handler need to free the pMsg object
	pSwatGuiDlg->PostMessage( WM_LUASCRIPT, LUA_END, (LPARAM)pMsg );
	
	return 0;
}

UINT CPB880SWATUnitTestDlg::TouchedPositionMonitorThreadProc(LPVOID lParam)
{
    
    bool forever = true;

    UdpCommunication udpRecv;

    CPB880SWATUnitTestDlg* pSwatGuiDlg = static_cast<CPB880SWATUnitTestDlg*>( lParam );

    if ( !udpRecv.setup(UdpCommunication::UDP_RECEIVE, TOUCHED_POS_BROADCAST_PORT) )
    {
        return -1;
    }

    Uint8	recvBuffer[2048];
    DWORD   numberOfBytesRead = 0;
    DWORD   maxMsToWait = 1000;

    while ( forever )
    {
        bool received = udpRecv.readData( recvBuffer, 2048, &numberOfBytesRead, maxMsToWait);

        if ( received ) 
        {
            XmitBuf* pBuf = (XmitBuf*) recvBuffer;

#if defined( NETWORK_BYTE_ORDER_FOLLOWED )
            // network byte order to host byte order, note: Data part is not 
            // converted to network byte order, so it is the same as e600 HW
            // little endian
            Uint32* p = (Uint32*) &pBuf->header;
            for ( int i = 0; i < sizeof(NetworkMsgHeader)/sizeof(Uint32); ++ i )
            {
                *p = ntohl( *p );
                p++;
            }
#endif

            TouchedPosition touchedPos;
            if ( pBuf->header.pktSize == sizeof(TouchedPosition)
                && pBuf->header.msgId == TOUCHED_POS_DATA )
            {
                // Byte order of Touched position is not exchanged, use it directly
                touchedPos = *((TouchedPosition*)pBuf->data);

                ASSERT( sizeof(TouchedPosition) == sizeof(WPARAM) );

                WPARAM w;
                memcpy(&w, &touchedPos, sizeof(TouchedPosition));
                // show this latest message to a LABEL
                pSwatGuiDlg->PostMessage( WM_TOUCHED_POS, w, NULL );
            }
        }
        else
        {
            Sleep(20);
        }
    }

    return 0;
}

void CPB880SWATUnitTestDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	AfxBeginThread( CPB880SWATUnitTestDlg::TestScriptExecThreadProc, this );
}


LRESULT CPB880SWATUnitTestDlg::OnLuaScript(WPARAM wParam, LPARAM lParam)
{
	// LUA execution begin
	if ( wParam == LUA_BEGIN )
	{
		// disable the GUI buttons
		GetDlgItem(IDOK)->EnableWindow( FALSE );
		GetDlgItem(IDCANCEL)->EnableWindow( FALSE );
	}
	// LUA execution end
	else if ( wParam == LUA_END )
	{
		// write message to log list if we have a message
		if ( lParam != NULL )
		{
			CString* pMsg = (CString*)lParam;
			WriteLog( *pMsg );
			delete pMsg;
		}
		else
		{
			WriteLog("Test script execute finished.");
		}

		// enable the GUI buttons
		GetDlgItem(IDOK)->EnableWindow( TRUE );
		GetDlgItem(IDCANCEL)->EnableWindow( TRUE );
	}

	return 0;
}

LRESULT CPB880SWATUnitTestDlg::OnLogMessage(WPARAM wParam, LPARAM lParam)
{
	CString* pLogMsg = (CString*)wParam;

	LVITEM lvi;
	CString strItem;
	CTime t = CTime::GetCurrentTime();

	// formate the current date time: MM/DD/YY HH:mm:SS
	strItem.Format("%02d/%02d/%02d %02d:%02d:%02d", t.GetMonth(), t.GetDay(), t.GetYear(),
		t.GetHour(),  t.GetMinute(), t.GetSecond());

	// insert a new item
	lvi.mask = LVIF_TEXT;
	lvi.iItem = m_listLogs.GetItemCount();
	lvi.iSubItem = 0;
	lvi.pszText = (LPTSTR)(LPCTSTR)strItem;
	m_listLogs.InsertItem(&lvi);

	// set message of the item
	lvi.iSubItem = 1;
	lvi.pszText = (LPTSTR)(LPCTSTR)(*pLogMsg);
	m_listLogs.SetItem(&lvi);

	// scroll to bottom of the list
	m_listLogs.EnsureVisible(lvi.iItem, FALSE);

	// free the message object
	delete pLogMsg;

	return 0;
}

LRESULT CPB880SWATUnitTestDlg::OnTouchedPos(WPARAM wParam, LPARAM lParam)
{
    TouchedPosition touchedPos = *((TouchedPosition*)(&wParam));

    char msg[MAX_PATH];

    sprintf_s( msg, "Latest Touched Position is: [%d, %d].", touchedPos.x, touchedPos.y );

    WriteLog( msg );

    GetDlgItem( IDC_STATIC_POS )->SetWindowText( msg );



    return 0;
}