#pragma once

class UdpCommunication;

namespace swat 
{

	class SwatGui
	{
	public:
		static SwatGui*	GetInstance();

		void simulateTouch(int x, int y);
		void simulateBezelKey(int keyId, int onOff);

	private:
		SwatGui(void);
		~SwatGui(void);

		static UdpCommunication&	RUdpComm_;

		static SwatGui*		PInstance_;
	};

}