#ifndef Resistance_HH
#define Resistance_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  Resistance - Calculate and store tubing resistance
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/Resistance.hhv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: gdc    Date: 18-Aug-2009     SCR Number: 6147
//  Project:  XB
//  Description:
//		Changed "get" methods to const to allow for const objects.
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  quf    Date: 03-Nov-1997    DR Number: DCS 2602
//       Project:  Sigma (840)
//       Description:
//			Added data member resistanceValid_ and access methods
//			setResistanceValid() and isResistanceValid(),
//			updated default and copy constructors and operator=().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001   By: quf   Date: 18-Sep-1996   DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//====================================================================

#include "ServiceMode.hh"


extern const Real32 ADULT_RI_SLOPE;
extern const Real32 ADULT_RI_OFFSET;
extern const Real32 CHARACTERIZATION_MIN_FLOW;


class Resistance
{
  public:
	Resistance();
	Resistance( const Resistance& resistance2);  // copy constructor
	~Resistance();

	static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL,
						   const char*       pPredicate = NULL);

	inline void setSlope( Real32 slope);
	inline void setOffset( Real32 intercept);
	inline Real32 getSlope( void) const;
	inline Real32 getOffset( void) const;
	inline Real32 calculateDeltaP( Real32 flow) const;
#ifdef SIGMA_BD_CPU
	inline void setResistanceValid( Boolean valid);
	inline Boolean isResistanceValid( void) const;

	Real32 measureCircuitResistance( void);
#endif  // SIGMA_BD_CPU

	Resistance operator- (const Resistance& resistance2);
	Resistance& operator= (const Resistance& resistance2);
		
  private:
	//@ Data-Member: slope_
	// resistance slope
	Real32 slope_;

	//@ Data-Member: offset_
	// resistance offset
	Real32 offset_;

	//@ Data-Member: resistanceValid_
	// resistance valid flag (TRUE = resistance parameters are valid)
	Boolean resistanceValid_;

};


// Inlined methods...
#include "Resistance.in"

#endif  // Resistance_HH
