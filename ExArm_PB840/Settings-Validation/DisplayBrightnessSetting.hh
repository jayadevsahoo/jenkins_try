 
#ifndef DisplayBrightnessSetting_HH
#define DisplayBrightnessSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  DisplayBrightnessSetting - Display Brightness Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DisplayBrightnessSetting.hhv   25.0.4.0   19 Nov 2013 14:27:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "NonBatchSequentialSetting.hh"
#include "SequentialRange.hh"
//@ End-Usage


class DisplayBrightnessSetting : public NonBatchSequentialSetting 
{
  public:
    DisplayBrightnessSetting(void);
    virtual ~DisplayBrightnessSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getDefaultValue(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    // not implemented...
    DisplayBrightnessSetting(const DisplayBrightnessSetting&);
    void  operator=(const DisplayBrightnessSetting&);

    //@ Data-Member:  sequentialRange_
    // Sequential range manager.
    SequentialRange  sequentialRange_;
};


#endif // DisplayBrightnessSetting_HH 
