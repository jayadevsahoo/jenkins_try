#ifndef TaskAgent_HH
#define TaskAgent_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  TaskAgent - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskAgent.hhv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
#include "Task.hh"
//@ End-Usage

class BdReadyMessage;
class GuiReadyMessage;
class CommDownMessage;
class ChangeStateMessage;
class TaskReadyMessage;

class TaskAgent 
{
 
  public:
 
    TaskAgent();
    ~TaskAgent();
 
	//set TaskInfo object of "this" TaskAgent. The task is being 
	//referenced by its internal ID which is an index in the task 
	//info array (and task table..) and not the OS thread ID.
    void    set(const Int32 taskIdx);
    void    createTask(void);
    void    send(const BdReadyMessage& rMessage) const;
    void    send(const GuiReadyMessage& rMessage) const;
    void    send(const CommDownMessage& rMessage) const;
    void    send(const ChangeStateMessage& rMessage) const;
    void    receive(const TaskReadyMessage& rMessage);
    Boolean isTransitioned(const Uint32 sequence) const;
 
    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);

#ifdef SIGMA_UNIT_TEST
    friend class TaskAgent_UT;
#endif
 
  private:

    TaskAgent(const TaskAgent&);             // declared only
    void operator=(const TaskAgent&);        // declared only
 
    //@ Data-Member:    pTaskInfo_
    //  Pointer to the TaskInfo object for this task
    TaskInfo*           pTaskInfo_;

    //@ Data-Member:    sequenceCompleted_
    //  The last completed transition sequence for this task as
    //  received in the TaskReady message.
    Uint32              sequenceCompleted_;

};
 

#endif // TaskAgent_HH
