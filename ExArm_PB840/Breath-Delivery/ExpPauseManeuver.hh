
#ifndef ExpPauseManeuver_HH
#define ExpPauseManeuver_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ExpPauseManeuver - implements the handling of expiratory pause
// maneuver.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExpPauseManeuver.hhv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc   Date:  29-Nov-2006   SCR Number: 6319
//  Project:  RESPM
//  Description:
//		Removed private data member clearRequested_ to use protected
//      data member with same name in Maneuver base class.
//		
//  Revision: 003   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Refactored code for base class modifications to accomodate RM 
//      maneuvers. Moved pressure stability buffer to its own class
//      that is now a member of this class. New method processUserRequest()
//      implements functionality of old Maneuver::DetermineManeuverStatus()
//      method in each maneuver class.
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  iv    Date:  27-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version - per Breath Delivery formal code review 
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "EventData.hh"
#include "Maneuver.hh"
#include "TimerTarget.hh"
#include "PauseTypes.hh"
#include "PressureStabilityBuffer.hh"

//@ Usage-Classes

//@ End-Usage



class ExpPauseManeuver :  public Maneuver, public TimerTarget
{
  public:

	ExpPauseManeuver(ManeuverId maneuverId, IntervalTimer& rPauseTimer);
    ~ExpPauseManeuver(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	// virtual from Maneuver
	virtual void activate(void);
	virtual Boolean complete(void);
	virtual void clear(void);
	virtual void terminateManeuver(void);
	virtual void newCycle(void);
	virtual Boolean processUserEvent(UiEvent& rUiEvent);

	EventData::EventStatus handleExpPause(const Boolean eventStatus);
	inline PauseTypes::PpiState getPressureStabilityState(void) const;
  
	virtual void timeUpHappened(const IntervalTimer& timer);

  protected:

  private:
    ExpPauseManeuver(const ExpPauseManeuver&);		// not implemented...
    void   operator=(const ExpPauseManeuver&);	// not implemented...
	ExpPauseManeuver(void);

	//@ Type: PauseTimerId
	// This enum represents the state of Expiratory pause.
  	enum PauseTimerId
  	{
  		NULL_TIME,
  		MAX_MANUAL_TIME,
  		MAX_PENDING_TIME,
  		MAX_AUTO_TIME
  	};
  
	//@ Data-Member: &rPauseTimer_
	// a reference to the server IntervalTimer.
	IntervalTimer& rPauseTimer_;

    //@ Data-Member: pauseTime_
    // pause timer count.
    Int32 pauseTime_;
    
    //@ Data-Member: pauseTimeId_
    // currently active pause timer id.
    PauseTimerId pauseTimeId_;
    
	//@ Data-Member: timeOut_
	// Boolean flag to indicate that a timeOut had occurd.
	Boolean timeOut_;
	
	//@ Data-Member: keyReleased_
	// Boolean flag to indicate that a key has been released.
	Boolean keyReleased_;
	
	//@ Data-Member: audibleAnnunciatingNeeded_
	// Boolean flag to indicate the audio alarm is needed to
	// signal the pressure plateau has reached stability.
	Boolean audibleAnnunciatingNeeded_;
	
	//@ Data-Member: maneuverType_
	// Currently active maneuver type (AUTO or MANUAL).
	PauseTypes::ManeuverType maneuverType_;

	//@ Data-Member: pressureState_
	// Flag to indicate if the pressure has reached stability.
	PauseTypes::PpiState pressureState_;

	//@ Data-Member: pressureStabilityBuffer_
	// The buffer used to do stability checking.
	PressureStabilityBuffer pressureStabilityBuffer_;
};



// Inlined methods...
#include "ExpPauseManeuver.in"


#endif // ExpPauseManeuver_HH 
