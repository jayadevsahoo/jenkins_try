#if defined FAKE_SM
// This class is used only for test simulation.

#ifndef FakeSMControlTimer_HH
#define FakeSMControlTimer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: FakeSMControlTimer - Fakes out the interface between the
// Service Data subsystem and GUI.  Generates randomly changing
// pieces of Service Data.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/FakeSMControlTimer.hhv   25.0.4.0   19 Nov 2013 14:21:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================


#include "Sigma.hh"

//@ Usage-Classes
#include "SDTimerTarget.hh"
//@ End-Usage


class FakeSMControlTimer : public SDTimerTarget
{
public:

	FakeSMControlTimer(void);
	~FakeSMControlTimer(void);

	virtual void timerEventHappened(SDTimerId::SDTimerIdType timerId);
	void startTimer(void);
	void stopTimer(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	FakeSMControlTimer(const FakeSMControlTimer&);	// not implemented...
	void operator=(const FakeSMControlTimer&);			// not implemented...


};


#endif // FakeSMControlTimer_HH 


#endif	// FAKE_SM
