
#ifndef SettingSubject_HH
#define SettingSubject_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SettingSubject - Setting Subject Class.
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingSubject.hhv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah   Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added 'isEnabledValue()' to provide support for the new
//         drop-down menus
//
//  Revision: 004   By: sah    Date: 19-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  changed 'get{Max,Min}Limit()' methods to be non-const
//
//  Revision: 003   By: sah  Date:  18-Jun-1999    DR Number:  5440
//  Project:  ATC
//  Description:
//      Changed 'resetForcedChangeFlag()' from an inlined method to a
//      virtual method, to allow derived settings to override and change
//      its behavior.
//
//  Revision: 002   By: sah   Date:  09-Jun-1999    DR Number: 5368 & 5388
//  Project:  ATC
//  Description:
//     Eliminated storage of calculated applicability, to ensure proper
//     state of forced-change flag.
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//====================================================================

#include "SettingId.hh"
#include "Applicability.hh"
#include "SettingBoundId.hh"
#include "Notification.hh"

//@ Usage-Classes
#include "Subject.hh"
#include "SettingValue.hh"

class SettingObserver;  // forward declaration...
class BoundStatus;      // forward declaration...
//@ End-Usage


class SettingSubject : public Subject
{
  public:
    virtual ~SettingSubject(void);

    //-------------------------------------------------------------------
    // Monitoring methods...
    //-------------------------------------------------------------------

    // attach/detach observer for monitoring the indicated type of change
    // for this setting...
    void  attachObserver(SettingObserver*                    pObserver,
			 const Notification::SettingChangeId changeId);
    void  detachObserver(const SettingObserver*              pObserver,
			 const Notification::SettingChangeId changeId);

    // notify all of this setting's observers who monitor this type of change
    // of a change...
    void  notifyAllObservers(
			  const Notification::ChangeQualifier qualifierId,
			  const Notification::SettingChangeId changeId
			    ) const;


    //-------------------------------------------------------------------
    // Query methods...
    //-------------------------------------------------------------------

    inline SettingId::SettingIdType  getId(void) const;

    inline Boolean  isBoundedSetting   (void) const;
    inline Boolean  isDiscreteSetting  (void) const;
    inline Boolean  isSequentialSetting(void) const;

    virtual Boolean  isChanged(void) const = 0;

    inline  void  setForcedChangeFlag  (void);
    virtual void  resetForcedChangeFlag(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const = 0;

    virtual SettingValue  getAcceptedValue(void) const = 0;
    virtual SettingValue  getAdjustedValue(void) const = 0;

    SettingValue   getSafetyPcvValue(void) const;

    virtual Real32  getMaxLimit(void) = 0;
    virtual Real32  getMinLimit(void) = 0;

    virtual Boolean isEnabledValue(const DiscreteValue value) const = 0;


    //-------------------------------------------------------------------
    // Operation methods...
    //-------------------------------------------------------------------

    void  updateApplicability(const Notification::ChangeQualifier qualifierId);

    virtual void  activationHappened(void) = 0;

    virtual const BoundStatus&  calcNewValue(const Int16 knobDelta) = 0;

    virtual void  overrideSoftBound(const SettingBoundId boundId) = 0;

    virtual void  resetToStoredValue(void) = 0; 

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    SettingSubject(const SettingId::SettingIdType settingId);

    virtual void  detachFromAllObservers_(void);

    inline Boolean  getForcedChangeFlag_(void) const;

  private:
    SettingSubject(const SettingSubject&);     // not implemented...
    SettingSubject(void);                      // not implemented...
    void  operator=(const SettingSubject&);    // not implemented...

    void  initObserverTable_(void);

    const SettingId::SettingIdType  SETTING_ID_;

    //@  Data-Member:  forcedChangedFlag_
    // This boolean flag is used to override the determination of if this
    // setting has changed.  When 'TRUE' a change is declared irregardless
    // of the other "change" conditions.  When 'FALSE' only the other
    // conditions are used to determine this setting's "change" status.
    Boolean  forcedChangeFlag_;

    enum { MAX_OBSERVERS_ = 15 };

    SettingObserver*  observerTable_[Notification::NUM_SETTING_CHANGE_TYPES]
				    [SettingSubject::MAX_OBSERVERS_];
};


#include "SettingSubject.in"


#endif // SettingSubject_HH 
