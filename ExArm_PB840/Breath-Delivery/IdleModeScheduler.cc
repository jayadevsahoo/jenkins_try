#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: IdleModeScheduler - A base class for all schedulers that are
// using idle flow to check for patient connection
//---------------------------------------------------------------------
//@ Interface-Description
// This is an abstract class, used as a framework for the derived classes:
// DisconnectScheduler and StandbyScheduler.  This class is responsible for
// maintaining idle flow required during disconnect.  It is responsible for
// relinquishing control to the next valid scheduler and for assuming control
// when disconnect or standby modes are requested.  Standby mode is requested
// during startup sequence while disconnect may be detected anytime during any
// breathing mode.  This scheduler is responsible for enabling valid mode
// triggers for Idle modes. User events, and handle by the derived schedulers.
//---------------------------------------------------------------------
//@ Rationale
// This class implements the common behavior between DisconnectScheduler
// and StandbyScheduler 
//---------------------------------------------------------------------
//@ Implementation-Description
// The class implements methods for setting up the breath phase upon
// initiation of the idle mode, and for mode transitions.
// The call to the pure virtual member functions implements
// a run time binding to the active derived scheduler.
// The Idle mode scheduler is responsible for:
// -- Initiating the activities required for the proper start of a idle
//	breath phase. Set breath triggers, enable breath triggers, and phase
//  out the previous breath phase. 
// -- Relinquishing control when a mode trigger becomes active.
// -- Taking control when the previous scheduler relinquishes control. 
//  Activities while taking control include:
//	register self with the BreathPhaseScheduler object,  and activate
//	the mode trigger list.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/IdleModeScheduler.ccv   25.0.4.0   19 Nov 2013 13:59:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 018   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Use modified Maneuver::CancelManeuver to cancel any active maneuver. 
//
//  Revision: 017  By: syw     Date:  14-Jun-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Call the base class NewBreath() method instead of derived class.
//
//  Revision: 016  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 015  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling of inspiratory pause event.
//
//  Revision: 014  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 013  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 012  By:  iv    Date:  03-Apr-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review for BD Schedulers.
//
//  Revision: 011  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 010  By:  iv    Date:  18-Dec-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 009  By:  iv    Date:  18-Dec-1996    DR Number: DCS 1610
//       Project:  Sigma (R8027)
//       Description:
//             Disable expiratory pause event if active, in takeControl().
//
//  Revision: 008  By:  iv    Date:  04-Dec-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Cleanup for code inspection.
//
//  Revision: 007 By:  iv   Date:   07-Nov-1996    DR Number: DCS 1502 
//       Project:  Sigma (R8027)
//       Description:
//             In determineBreathPhase(): move call for current phase
//             to relinquish control was moved before the call to change
//             the mixture in O2Mixture class.
//
//  Revision: 006 By:  iv   Date:   04-June-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Moved the calls to: takeControl() and UpdateSchedulerId()
//             from the derived classes.  
//
//  Revision: 005 By:  iv   Date:   12-Apr-1996    DR Number: DCS 967 
//       Project:  Sigma (R8027)
//       Description:
//             Deleted rPeep.setActualPeep(0.0) to allow peep recovery to
//             set peep.
//
//  Revision: 004 By:  iv   Date:   12-Apr-1996    DR Number: DCS 672 
//       Project:  Sigma (R8027)
//       Description:
//             Add a condition to check for triggerId == SVC_COMPLETE upon
//             transition to idle mode. This is necessary since svo Scheduler
//             may transition to idle mode.
//
//  Revision: 003 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem
//             and for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "IdleModeScheduler.hh"
#  include "BreathMiscRefs.hh"
#  include "PhaseRefs.hh"
#  include "TriggersRefs.hh"
#  include "ModeTriggerRefs.hh"

//@ Usage-Classes
#  include "BreathTriggerMediator.hh"
#  include "ModeTriggerMediator.hh"
#  include "BreathTrigger.hh"
#  include "ModeTrigger.hh"
#  include "O2Mixture.hh"
#  include "Peep.hh"
#  include "BreathPhase.hh"
#  include "BdSystemStateHandler.hh"
#  include "PhasedInContextHandle.hh"
#  include "InspPauseManeuver.hh"
#  include "ExpPauseManeuver.hh"
#  include "NifManeuver.hh"
#  include "P100Maneuver.hh"
#  include "VitalCapacityManeuver.hh"
#  include "ManeuverRefs.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IdleModeScheduler()
//
//@ Interface-Description
// Construct the scheduler object. The constructor takes a SchedulerId
// type argument and pass it to the base class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The  scheduler id is used to construct the base class.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

IdleModeScheduler::IdleModeScheduler(SchedulerId::SchedulerIdValue schedulerId) :
										BreathPhaseScheduler(schedulerId)
{
	// $[TI1]
	CALL_TRACE("IdleModeScheduler::IdleModeScheduler(\
					SchedulerId::SchedulerIdValue schdulerId)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~IdleModeScheduler()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

IdleModeScheduler::~IdleModeScheduler(void)
{
  CALL_TRACE("~IdleModeScheduler()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathPhase
//
//@ Interface-Description
// This method accepts a breath trigger as an argument. The pure virtual
// method setupBreath_() implements late binding to the run time object.
// Activities performed here include: update of the applied O2 mix,
// change the active breath trigger list, disabling the triggers on the
// current list, instruct the current breath phase to relinquish control,
// and setup the new breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The instance rBreathTriggerMediator is used to reset the
// old phase trigger list, and to set the new phase trigger list,  the old 
// phase relinquishes control, and the new phase registers itself with the 
// BreathPhase object. The instance rO2Mixture is used to update the O2 mix.
// The BreathPhase static method SetCurrentBreathPhase() is used to set the
// current instance of the breath phase: rDisconnectPhase.
// inspiratory pause shall not be active
// $[BL04007]
//---------------------------------------------------------------------
//@ PreCondition
// The breath trigger id is checked to be IMMEDIATE_BREATH or SVC_COMPLETE.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
IdleModeScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)
{
// $[TI1]
	CALL_TRACE("IdleModeScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)");

	// A pointer to the current breath phase
	BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();

	const Trigger::TriggerId triggerId = breathTrigger.getId();
	//Check the validity of the triggers.
	CLASS_PRE_CONDITION( (triggerId == Trigger::IMMEDIATE_BREATH) ||
						 (triggerId == Trigger::SVC_COMPLETE) );

	// phase-out current breath phase
	pBreathPhase->relinquishControl(breathTrigger);
			
	// determine the O2 mix for this scheduler.
	// The disconnect phase is used during both disconnect and standby modes:
	RO2Mixture.determineO2Mix(*this, (BreathPhase&)RDisconnectPhase);

	setupBreath_();

	// $[04215] Reset currently active triggers and setup triggers that are active during
	//disconnect and standby :
	RBreathTriggerMediator.resetTriggerList();
	RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::NON_BREATHING_LIST);
	// disable the immediate trigger on the list:
	((BreathTrigger &)RImmediateBreathTrigger).disable();

	// register new phase:
	BreathPhase::SetCurrentBreathPhase((BreathPhase&)RDisconnectPhase,
												breathTrigger);
	// deliver idle flow
	BreathPhase::NewBreath() ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
// The method takes a reference to a ModeTrigger type argument and has no return
// value. The pure virtual member function determineNextScheduler_() implements a late
// binding, to determine which scheduler should take control next.  Before the
// new mode is instructed to take control, all mode triggers on the current
// trigger list get reset.
//---------------------------------------------------------------------
//@ Implementation-Description
// The object rModeTriggerMediator is used to reset all mode triggers for the
// current active scheduler. The method determineNextScheduler is passed in a
// mode trigger id - the trigger that is active now, and a Boolean specifying
// whether or not a new patient setup has been completed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
IdleModeScheduler::relinquishControl(const ModeTrigger& modeTrigger)
{
// $[TI1]
	CALL_TRACE("IdleModeScheduler:: relinquishControl(const ModeTrigger& modeTrigger)");
	const Boolean isVentSetupComplete = PhasedInContextHandle::AreBdSettingsAvailable();

	const Trigger::TriggerId triggerId = modeTrigger.getId();
	// reset all mode triggers for this scheduler
	RModeTriggerMediator.resetTriggerList();
	determineNextScheduler_(triggerId, isVentSetupComplete);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description
// The method accepts a breath phase scheduler reference as an argument. and
// has no return value. It is invoked by the method relinquishControl() from
// the scheduler instance that relinquishes control.  This method checks the
// validity of the client by calling validateScheduler (which is a pure virtual
// method).  It disables the asap breath trigger, the peep recovery mand insp
// trigger and cancels any pending pause event. the NOV RAM instance of
// BdSystemState is updated with the new scheduler.  The scheduler registers
// with the BreathPhaseScheduler as the current scheduler, and the mode
// triggers relevant to that mode are set to be enabled by calling a second
// pure virtual method - enableTriggers_(). The list of mode triggers is
// changed to the current scheduler list.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler reference passed as an argument is used to get the id of the
// current scheduler. Other activities are: The static method of
// BreathPhaseScheduler is used to register the new scheduler, the instance
// RAsapInspTrigger is disabled, the user events RExpiratoryPauseEvent and
// RInspiratoryPauseEvent are used to cancel pauses, and the new mode trigger
// list is set using the instance RModeTriggerMediator.
// The base class method for takeControl() is invoked to handle activities
// common to all schedulers.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
IdleModeScheduler::takeControl(const BreathPhaseScheduler& scheduler) 
{
	CALL_TRACE("IdleModeScheduler::takeControl(const BreathPhaseScheduler& scheduler)");

    // Invoke the base class takeControl method
    BreathPhaseScheduler::takeControl(scheduler);
    
	validateScheduler_();
	
	// disable the following triggers
	((BreathTrigger&)RAsapInspTrigger).disable();
	((BreathTrigger&)RPeepRecoveryMandInspTrigger).disable();

	// $[BL04070] :b disable pause event request
	// $[BL04072] :b disable pause event request
	// $[BL04008] :b disable pending pause event
	// $[BL04075] :b cancel manual pause request
	// $[RM12077] cancel a NIF maneuver if SVO is detected
	// $[RM12062] cancel a VC maneuver if SVO is detected
	// $[RM12094] cancel a P100 maneuver if SVO is detected
	Maneuver::CancelManeuver();

	// cancel any mode or rate change that may have been initialized during 
	// the previous mode
	BreathPhaseScheduler::ModeChange_ = FALSE;	
	BreathPhaseScheduler::RateChange_ = FALSE;

    // update the BD state, in case of power interruption
   	BdSystemStateHandler::UpdateSchedulerId(getId()); 

	// update the reference to the newly active breath scheduler 
    BreathPhaseScheduler::SetCurrentScheduler_((BreathPhaseScheduler&)(*this));

    //The BreathRecord is not being updated with the new scheduler id, so that
    // the identity of the scheduler that originated this breath is maintained. 
	// until a new breath starts.
	
	// Set the mode triggers for that scheduler.
	enableTriggers_();	
	RModeTriggerMediator.changeModeTriggerList(pModeTriggerList_);
} 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
IdleModeScheduler::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, IDLEMODESCHEDULER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableTriggers_
//
//@ Interface-Description
// The method takes no arguments and returns no value.  A request not to enable
// the disconnect autoreset and the svo triggers is issued when this scheduler
// is taking control.
//---------------------------------------------------------------------
//@ Implementation-Description
// The mode trigger instances are accessed directly with a call to their
// setIsEnableRequested() method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
IdleModeScheduler::enableTriggers_(void) 
{
// $[TI1]
	CALL_TRACE("IdleModeScheduler::enableTriggers_(void)");

	// $[04213] 
	((ModeTrigger&)RDiscAutoResetTrigger).setIsEnableRequested(TRUE);
	((ModeTrigger&)RSvoTrigger).setIsEnableRequested(TRUE);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
