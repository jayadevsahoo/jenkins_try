/*****************************************************************************/
/*
/*   Module Name:     72361.c                                          
/*   Purpose:   840 Ventilator Final Test                               
/*   Requirements:   This module implements requirements detailed in         
/*          <72361-93>    
/*
/*   Modification History:
/*
/*   Description of Change			        By			      Date 
/*   ---------------------					--------		  ------
/*   Fixed a major bug in the Overall       Rhomere Jimenez   05 Oct, 2007
/*   Test Results for Circuit Pressure
/*   test and removed the Metabolic 
/*   Port Test.
/*
/*   Fixed a major bug in the Overall       Rhomere Jimenez   03 Nov, 2005
/*   Test Results for evaluating Air 
/*   and O2 flow Tests.
/*   Removed Ground DC & AC volts from 
/*   Manual Test screens, and reports.
/*
/*   The Error codes in ErrorMessage		Dwarkanath(CGS)   12 Dec, 2000 
/*	 functions are changed.		                
/*
/*	 All the functions dependent on 		Hamsavally(CGS)	  17 Oct, 2000
/*   test executive test data and	        		 
/*	 error structure are removed and 
/*	 equivalent functionality is 
/*	 included.
/*
/*	 Global Test data structure is used		Hamsavally(CGS)	  15 Oct, 2000
/*   for all the Test functions.
/*
/*   Critical Observations during Phase1    Kathirvel(CGS)	  27 Sep, 2000 
/*   is incorporated.			   		 
/*													
/*   Added Serial Loopbak Test              Dave Richardson   18 Nov, 2002
/*   (Manual Tests Other)
/*
/*   $Revision::   1.3         $                                          
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <windows.h>
#include <ansi_c.h>
#include <formatio.h>
#include <stdio.h>
#include <userint.h>
#include <utility.h>

#include "72500.h"
#include "72361.h"
#include "840.h"
#include "common.h"
#include "cpvtmain.h"
#include "helpinit.h"
#include "ldb.h"
#include "limitsh.h"
#include "mainpnls.h"
#include "MsgPopup.h"
#include "panapi.h"
#include "pb_util.h" // UpdateRevisionInfo(Revision)
#include "perf.h"
#include "prmptpnl.h" // typedef PPstates
#include "pts.h"
#include "ptsreset.h"
#include "sysmenu.h"
#include "testing.h"
#include "tt.h"
#include "version.h"
#include "volume.h"

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

// macro to check for the user pressing abort. Also allows UI to
// run. Usefull for while loops, that so can break out early.
#define CHECK_FOR_ABORT \
{\
	ProcessSystemEvents();\
	if(gStoppedInd == 1){\
		TestPrompt(Clear);\
	}\
}

#define CHECK_FOR_ABORT_STOP_AUDIO \
{\
	ProcessSystemEvents();\
	if(gStoppedInd == 1){\
		double timeOut;\
		TestPrompt(Clear);\
		timeOut = Timer() + 1.5;\
		while(Timer() < timeOut){\
			RECV_840()\
		}\
		Prompt_Accept(NO_840);\
		}\
}

#define CHECK_FOR_ABORT_PERFPANEL \
{\
	ProcessSystemEvents();\
	if(gStoppedInd == 1){\
		DiscardPanel(perfPanel1);\
	}\
}

/***********************************************/
/******************** ENUMS ********************/
/***********************************************/

enum{
	ERR_TEST_TIMEOUT = 1,
	ERR_PTS_BURST,
	ERR_VENT_GAS_SEQ,
	ERR_VENT_CP,
	ERR_VENT_TEST_SEQ,
	ERR_UIR_LOAD
};

/***********************************************/
/************ STRUCTURE DEFINITIONS ************/
/***********************************************/

/* Error code number / cross reference table */
struct ERROR_XREF{
    const short err_no; /* Error number                   */
    int err_str;        /* Ldb index to Error description */
};

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

static struct ERROR_XREF const error_xref[] = {
	ERR_TEST_TIMEOUT,  Ldb_ERR_TEST_TIMEOUT,
	ERR_PTS_BURST,     Ldb_ERR_PTS_BURST,
	ERR_VENT_GAS_SEQ,  Ldb_ERR_VENT_GAS_SEQ,
	ERR_VENT_CP,       Ldb_ERR_VENT_CP,
	ERR_VENT_TEST_SEQ, Ldb_ERR_VENT_TEST_SEQ,
	ERR_UIR_LOAD,      Ldb_ERR_UIR_LOAD,
	};

static int ManualEnterWasPressed = 0; // set by ManualPnlCB, 
                                      // used by ManualPerformanceTest
static time_t timeTestStart;          // set by OpenSeq, used by LogError
static BOOL nErrLogStarted = FALSE;   // used by LogError
static char *ErrorLog = "error.log";  // Error log location, used by LogStatus
static char *PN_840 = "4-070000-00";  // Vent P/N, used by OpenSeq
static char *Revision = "$Revision::   1.3    $";  /* PVCS Revision string*/

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     LogStatus                                                       */
/*                                                                           */
/* Purpose:  This function logs a status message to the error log file and   */
/*           returns to the caller.                                          */
/*                                                                           */
/* Example Call:   LogStatus(log_string);                                    */
/*                                                                           */
/* Input:    fmt       Pointer to error message string as in printf          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           ErrorLog, gCurrOperator                                         */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void LogStatus(char *timeStamp, char *fmt, ...)
{
	FILE *ErrorFile; /* Error log file handle */
	va_list arg_ptr;
	time_t ltime ;   /* Local (current) time  */

	time(&ltime) ;   /* Get the current time  */

	va_start(arg_ptr, fmt);

	if( (ErrorFile = fopen(ErrorLog, "a")) != NULL ){
		fprintf( ErrorFile, "\n%sOperator: %s\n", timeStamp, gCurrOperator);
		vfprintf(ErrorFile, fmt, arg_ptr) ;
		fprintf( ErrorFile, "\n") ;
		fclose(  ErrorFile);
	}

	return;
}


/*****************************************************************************/
/* Name:     LogError                                                        */
/*                                                                           */
/* Purpose:  This function is used to process hard errors and performs the   */
/*           following tasks:                                                */
/*                                                                           */
/*            1) logs the error to disk                                      */
/*            2) displays an error message to the operator                   */
/*            3) aborts program                                              */
/*                                                                           */
/* Example Call:   LogError(char*, char*);                                   */
/*                                                                           */
/* Input:    Title     Title to put on Popup box                             */
/*           ErrMsg    Error Message to display                              */
/*                                                                           */
/* Output:   Popup box with error message & OK button                        */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Return:   NOTE: Program is aborted, never returning to caller             */
/*                                                                           */
/*****************************************************************************/
void LogError(char *Title, char *ErrMsg)
{
	time_t ltime ;   /* Local (current) time  */

	if(nErrLogStarted == FALSE){
		LogStatus(ctime(&timeTestStart), 
		          "********** Program Execution Started **********");
		nErrLogStarted = TRUE;
	}

	time(&ltime) ; /* Get the current time  */
	LogStatus(ctime(&ltime), ErrMsg); /* Log error information */

	if(Title != NULL){
 		CPVTMessagePopup(Title, ErrMsg, OK_ONLY_DLG, NO_HELP);
 	}

	return;
}


/*****************************************************************************/
/*                                                                           */
/* Name:     AppErr                                                          */
/*                                                                           */
/* Purpose:  This function decodes error return value form all functions in  */
/*           this file.  LogError is then called with decoded message. Refer */
/*           to ERROR_XREF above.                                            */
/*                                                                           */
/* Example Call:   AppErr(num, 10.0);                                        */
/*                                                                           */
/* Input:    ErrorNo  internal Test Error number                             */
/*           reading  reading value (0.0) if no reading supply               */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           error_xref                                                      */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:   NULL  if no error (err_numb == 0 )                              */
/*                 pointer to brief error message                            */
/*                                                                           */
/*****************************************************************************/
void AppErr(int ErrorNo, double reading)
{
	unsigned int index ;
	char errmsg[120] ; // note: must be large enough for longest message

	if(ErrorNo){
		/* Shut down test stand */
		ShutDown();

		sprintf(errmsg, LDBf(Ldb_error_cant_decode), ErrorNo) ;

		for(index = 0;
		    index < sizeof(error_xref) / sizeof(struct ERROR_XREF);
		    ++index){
			if(error_xref[index].err_no == ErrorNo){
				sprintf(errmsg, "%s", LDBf(error_xref[index].err_str));
				break;
			}
		}

		LogError(LDBf(Ldb_testing_error), errmsg);
	}

	return;
}


/******************************************************************************/
/* Name:     ShutDown                                                         */
/*                                                                            */
/* Purpose:  This function performs the console shutdown.                     */
/*           Shutdown close communcation ports                                */
/*                                                                            */
/*   Note:   No Errors are processed in this function.  This allow the error  */
/*           handlers to use this function and not detect further errors.     */
/*                                                                            */
/* Example Call:   ShutDown();                                                */
/*                                                                            */
/* Input:    <None>                                                           */
/* Output:   <None>                                                           */
/*                                                                            */
/* Globals Used:                                                              */
/*           <None>                                                           */
/*                                                                            */
/* Globals Changed:                                                           */
/*           <None>                                                           */
/*                                                                            */
/* Return:   <None>                                                           */
/******************************************************************************/
void ShutDown(void)
{
	ComBreakPTS(0);     /* default time = 250ms */
	ComBreakPTS(0);     /* default time = 250ms */

	return;
}

//
// Added to avoid that abort error
//
/******************************************************************************/
/* Name:    OpenSeq                                                           */
/*                                                                            */
/* Purpose: This function performs the one time startup test / setup          */
/*          for the test console. These steps include checking console ID     */
/*          and opening measurement devices.                                  */
/*                                                                            */
/* Example Call:  OpenSeq( void )                                             */
/*                                                                            */
/* Input:   <None>                                                            */
/* Output:  <None>                                                            */
/*                                                                            */
/* Globals Used:                                                              */
/*           gStationID, gProgramRev                                          */
/*           Revision                                                         */
/*                                                                            */
/* Globals Changed:                                                           */
/*           gStationID, gProgramRev                                          */
/*                                                                            */
/* Return:  void                                                              */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
void OpenSeq( void )
{
	char ComputerName[MAX_COMPUTERNAME_LENGTH + 1];
	DWORD nSize = MAX_COMPUTERNAME_LENGTH + 1;
	BOOL nStatus;
	int station = 0;

	/* Log Program start Status */
	time(&timeTestStart);   // Get the current time
	nErrLogStarted = FALSE; // Set to TRUE when an error is logged

	/* Clear Globals */
	CommActive    = COM_840_INACTIVE;
	PromptReturn  = NO_PROMPT;
	DataAvailable = NO_840;

	/* Setup global station identification variables */
	strcpy(gUUTPN, PN_840);

	if(nStatus = GetComputerName((LPSTR)ComputerName, &nSize) > 0){
		strcpy(gStationID, ComputerName);
	}
	else{
		#ifndef DEMOMODE
		sprintf(gStationID, "%d", station); /* Store station ID  */
		#else
		sprintf(gStationID, "%s", "DEMO PROGRAM"); /* Store station ID  */
		#endif
	}

	UpdateRevisionInfo(Revision);

	#ifndef DEMOMODE
	ZeroLowPressure( );   /* zero pts 2000 pressure Port before testing */
	#endif

	return ;       
}


/*****************************************************************************/
/* Name:    CloseSeq                                                         */
/*                                                                           */
/* Purpose: This function performs the console shutdown                      */
/*                                                                           */
/* Example Call:   CloseSeq( void )                                          */
/*                                                                           */
/* Input:   <None>                                                           */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  TRUE if NO error(s)/failure(s) detected                          */
/*****************************************************************************/
int CloseSeq( void )
{
	ShutDown();

	return TRUE ;
}


/*****************************************************************************/
/* Name:    StartSeq                                                         */
/*                                                                           */
/* Purpose: This function performs the sequence startup functions            */
/*                                                                           */
/* Example Call:  StartSeq( void )                                           */
/*                                                                           */
/* Input:  	<None>                                                           */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  FALSE if NO error(s)/failure(s) detected                         */
/*****************************************************************************/
int StartSeq( void )
{
	unsigned int x, i;
	char op_name[30];

	op_name[0] = '\0';
	x = strlen(gCurrOperator);

	/* Convert operator name to upper case */
	for(i = 0; i < x; i++){
		op_name[i] = (char)toupper((int)gCurrOperator[i]);
	}

	/* Null terminated string */
	if(i < 29){
		op_name[i] = '\0';
	}

	// force the user to cycle the 840 power. When the user aborts, the 840 sometimes
	// quits accepting commands, so to get the 840 back the user must cycle power
	// and re-enter the establishing remote contol mode.
	if(gMustReset840){
		while(1){
			int connect;
			connect = Establish840(NO_AUTO_DETECT);
			// make new func, Establish840NoAutoDetect()
			if(connect == SUCCESS){
				if(GetLastConnectType840() == CONNECTED_INVALID_MSG){
					int response;

					// user did not reset 840 by cycling power as instructed
					response = CPVTConfirmPopup(LDBf(Ldb_cycle_840), LDBf(Ldb_cycle_before_test), 
					                            OK_CANCEL_DLG, NO_HELP);

					// if user said to quit trying
					if(response == 0){
						// failed to connect to 840
						gErrorFlag = TRUE;
						ErrorMessage(COM_840_ERROR);
						return FALSE;
					}
				}
				else{
					gMustReset840 = FALSE;
					break;
				}
			}
			else{
				// failed to connect to 840
				gErrorFlag = TRUE;
				ErrorMessage(COM_840_ERROR);
				return FALSE;
			} // end if(connect == SUCCESS)
		} // end while(1)
	} // end if(gMustReset840)

	return TRUE;
}


/*****************************************************************************/
/* Name:    StopSeq                                                          */
/*                                                                           */
/* Purpose: This function performs the sequence stop functions               */
/*                                                                           */
/* Example Call: StopSeq(void);                                              */
/*                                                                           */
/* Input:   <None>                                                           */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Globals Changed: gErrorFlag                                               */
/*                                                                           */
/*                                                                           */
/* Return:  void                                                             */
/*                                                                           */
/*****************************************************************************/
void  StopSeq(void )
{
	/* required by Test Exec */
	/* No Sequence clean Required */

	// if error is a PTS or 840 communications error or various test failures,
	// we have already handled the 
	// user popup dialog to inform user. So, clear errorCode and errorFlag to 
	// prevent a subsequent dialog that indicates what file the error occur in.
	if(gErrorFlag){
		gErrorFlag = 0;
	}

	return;
}


/*****************************************************************************/
/* Name:    PostRevision                                                     */
/*                                                                           */
/* Purpose: This function posts the revisions of all software used in the    */
/*          test program.                                                    */
/*                                                                           */
/* Example Call:  void PostRevision(void)                                    */
/*                                                                           */
/* Input:   <None>                                                           */
/*                                                                           */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*           Revision                                                        */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  <None>                                                           */
/*****************************************************************************/
void PostRevision(void)
{
	int  connect;
	char cBuff[60];

	//  per test initialization:
	connect = Establish840(NO_AUTO_DETECT);
	if(connect != SUCCESS){
		gErrorFlag = TRUE;
		ErrorMessage(COM_840_ERROR);
		return;
	}
	SetSysPanelMessage(LDBf(Ldb_Test1_Running));

	// Upload vent information
	GetVentInfo();

	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         Vent.Bd.serial);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), Vent.Bd.serial);
	
	sprintf(cBuff, LDB(Ldb_Vent_Runtimehours, ENGLISH), (int)Vent.runTimeHours);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         cBuff);
	sprintf(cBuff, LDBf(Ldb_Vent_Runtimehours),         (int)Vent.runTimeHours);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), cBuff);

	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         Vent.Bd.version);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), Vent.Bd.version);

	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         Vent.Bd.post);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), Vent.Bd.post);

	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         Vent.Gui.serial);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), Vent.Gui.serial);

	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         Vent.Gui.version);
    PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), Vent.Gui.version);

	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         Vent.Gui.post);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), Vent.Gui.post);

	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         Vent.saas);
    PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), Vent.saas);

	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         Vent.Compressor.serial);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), Vent.Compressor.serial);

	sprintf(cBuff, LDB(Ldb_comp_hours, ENGLISH), (int)Vent.Compressor.runTimeHours);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         cBuff);
	sprintf(cBuff, LDBf(Ldb_comp_hours),         (int)Vent.Compressor.runTimeHours);
 	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), cBuff);

	// Upload PTS-2000 information
	UpdatePTSInfo();
	sprintf(cBuff, LDB(Ldb_pts_sn, ENGLISH), szSerialNumber);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         cBuff);
	sprintf(cBuff, LDBf(Ldb_pts_sn),         szSerialNumber);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), cBuff);

	sprintf(cBuff, LDB(Ldb_pts_vn, ENGLISH), szVersionNumber);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         cBuff);
	sprintf(cBuff, LDBf(Ldb_pts_vn),         szVersionNumber);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), cBuff);

	sprintf(cBuff, LDB(Ldb_pts_cal_date, ENGLISH), szCalDate);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         cBuff);
	sprintf(cBuff, LDBf(Ldb_pts_cal_date),         szCalDate);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), cBuff);

	sprintf(cBuff, LDB(Ldb_pts_next_date, ENGLISH), szNextCalDate);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         cBuff);
	sprintf(cBuff, LDBf(Ldb_pts_next_date),         szNextCalDate);
	PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), cBuff);

	if(IsNextCalOverdue(szNextCalDate)){
		PostStringNoMsg(&(tDataRec[TEST_8].outBuffer),         LDB( Ldb_Pts_cal_expired, ENGLISH));
		PostStringNoMsg(&(tDataRec[TEST_8].outBuff_localized), LDBf(Ldb_Pts_cal_expired));
	}

	gErrorFlag = FALSE;
	tDataRec[TEST_8].result = PASS;

	return;
}


/*****************************************************************************/
/* Name:    AtmospherePressureTest                                           */
/*                                                                           */
/* Purpose: This function check vent atmosphere(comp) with pts 2000          */
/*                                                                           */
/* Example Call:  void AtmospherePressureTest(void)                          */
/*                                                         	                 */
/* Input:   <None>                                                           */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  <None>                                                           */
/*****************************************************************************/
void AtmospherePressureTest(void)
{
	int    connect;
	int    howMany;
	char   cBuff[50];
	float  ventAmt = 0.0;
	double ptsAmt;
	double ventConversion;
	int    actual = 0;
	int    status;
	int    timeOut;
	int    retries = 0;

	//	per test initialization:
	connect = Establish840(NO_AUTO_DETECT);
	if(connect != SUCCESS){
		gErrorFlag = TRUE;
		ErrorMessage(COM_840_ERROR);
		return; 
	}
	//Post the title
	SetSysPanelMessage(LDBf(Ldb_Test2_Running));

BEGINAPT:
	gErrorFlag = FALSE;

	#ifndef DEMOMODE
	SEND_840(AtmosphereTest())

	timeOut = Timer() + (double)TEST_TIMEOUT;
	while(Timer() < timeOut){
		CHECK_FOR_ABORT
		if(gStoppedInd == 1){
			tDataRec[TEST_2].result = ABORT;
			return;
		}	
		RECV_840()

		if(TestActive == NO_840){
			break;
		}
	}

	if(DataAvailable == YES_840){
		howMany = GetDataStorage(&ventAmt);
		if(howMany != 1){
			AppErr(ERR_VENT_TEST_SEQ, 0.0);
			gErrorFlag = TRUE;
			ErrorMessage(ATM_PRESS_TEST_FAILURE_ERROR);
			goto RETRYAPT;
		}

		SEND_PTS(MeasureBarometricPressure(&ptsAmt))
	}
	else{
		AppErr(ERR_VENT_TEST_SEQ, 0.0);
		gErrorFlag = TRUE;
		ErrorMessage(ATM_PRESS_TEST_FAILURE_ERROR);
		goto RETRYAPT;
	}
	#else // DEMOMODE
	ventAmt = 761.9675;
	ptsAmt  = 14.55;
	#endif // #ifndef DEMOMODE

	ventConversion = (double)(ventAmt * 0.019337); /* mmHg to Psi */
	/* +/- 44mmHg = .851 psi */

	// Test limit 0.851 shall not match test specification 0.854 by order of Service Test Manager
//	if(ptsAmt - 0.854 < ventConversion && ventConversion < ptsAmt + 0.854){
	if(ptsAmt - 0.851 < ventConversion && ventConversion < ptsAmt + 0.851){
		tDataRec[TEST_2].result = PASS;
	}
	else{
		tDataRec[TEST_2].result = FAIL;
	}
	if(tDataRec[TEST_2].result == FAIL){
		goto RETRYAPT;
	}

	sprintf(cBuff, LDB(Ldb_target_pts, ENGLISH), ptsAmt);
	PostStringPass(TEST_2, &(tDataRec[TEST_2].outBuffer),         cBuff, "");
	sprintf(cBuff, LDBf(Ldb_target_pts),         ptsAmt);
	PostStringPass(TEST_2, &(tDataRec[TEST_2].outBuff_localized), cBuff, "");

	status = PostReal(TEST_2, &(tDataRec[TEST_2].outBuffer),         LDB( Ldb_vent_atm, ENGLISH), ptsAmt - 0.851, ventConversion, ptsAmt + 0.851);
	status = PostReal(TEST_2, &(tDataRec[TEST_2].outBuff_localized), LDBf(Ldb_vent_atm),          ptsAmt - 0.851, ventConversion, ptsAmt + 0.851);
	if(status){
		tDataRec[TEST_2].result = FAIL;
	}

	return;

RETRYAPT:
	if(retries < MAX_TRIES_ALLOWED &&
	   CPVTConfirmPopup(LDBf(Ldb_Test_Failure), LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP)){
		retries++;
		goto BEGINAPT;
	}
	else{
		sprintf(cBuff, LDB(Ldb_target_pts, ENGLISH), ptsAmt);
		PostStringPass(TEST_2, &(tDataRec[TEST_2].outBuffer),         cBuff, "");
		sprintf(cBuff, LDBf(Ldb_target_pts),         ptsAmt);
		PostStringPass(TEST_2, &(tDataRec[TEST_2].outBuff_localized), cBuff, "");

		status = PostReal(TEST_2, &(tDataRec[TEST_2].outBuffer),         LDB( Ldb_vent_atm, ENGLISH), ptsAmt - 0.851, ventConversion, ptsAmt + 0.851);
		status = PostReal(TEST_2, &(tDataRec[TEST_2].outBuff_localized), LDBf(Ldb_vent_atm),          ptsAmt - 0.851, ventConversion, ptsAmt + 0.851);
		if(status){
			tDataRec[TEST_2].result = FAIL;
		}
	}
	

	return;
}


/*****************************************************************************/
/* Name:     BreathDeliveryAlarmTest                                         */
/*                                                                           */
/* Purpose:  This function will set the to alarm its Bd audio alarm          */
/*                                                                           */
/*                                                                           */
/* Example Call:   void BreathDeliveryAlarmTest(void);                       */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void BreathDeliveryAlarmTest(void)
{
	int connect;
	int alarmPanel; // AlarmPanel ID
	int handle;  // UserEvent commit: ID of panel   where commit
	int control; // UserEvent commit: ID of control where commit
	BOOL doneFlag = FALSE;
	double userTimeOut;
	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	double demoTimeOut;
	#endif
	#endif
	int retry = TRUE;
	int retries = 0;
	int nRetryRequest;

	//  per test initialization:
	connect = Establish840(NO_AUTO_DETECT);
	if(connect != SUCCESS){
		gErrorFlag = TRUE;
		ErrorMessage(COM_840_ERROR);
		return; 
	}

	while(retry){
		doneFlag = FALSE;
		tDataRec[TEST_3].result = FAIL;
		gErrorFlag = FALSE;
		SetSysPanelMessage(LDBf(Ldb_Test3_Running));

		#ifndef DEMOMODE
		SEND_840(BdAudio())

		userTimeOut = Timer() + 1.5;
		while(Timer() < userTimeOut){
			RECV_840()
			CHECK_FOR_ABORT_STOP_AUDIO
			if(gStoppedInd == 1){
				tDataRec[TEST_3].result = ABORT;
				return;
			}	
		}
		#endif 

		TestActive = YES_840;

		/* Retrieve the front panel */
		alarmPanel = TestPrompt(BDUAlarm);

		while(TestActive != NO_840){
			#ifndef DEMOMODE
			CHECK_FOR_ABORT_STOP_AUDIO
			if(gStoppedInd == 1){
				tDataRec[TEST_3].result = ABORT;
				return;
			}	
			RECV_840()
			#else
			PromptReturn = SOUND_PROMPT;
			#endif
			if(PromptReturn == SOUND_PROMPT){
				#ifdef DEMOMODE
				#ifdef USE_DEMO_TIMEOUT
				demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
				#endif
				#endif
				if(doneFlag != TRUE){
					PromptDisplay(alarmPanel);
				}
				while(doneFlag != TRUE){
					CHECK_FOR_ABORT_STOP_AUDIO
					if(gStoppedInd == 1){
						tDataRec[TEST_3].result = ABORT;
						return;
					}
					
					GetUserEvent(0, &handle, &control); // Check for user event
					#ifdef DEMOMODE
					#ifdef USE_DEMO_TIMEOUT
					if(Timer() >= demoTimeOut){
						handle = alarmPanel;
						control = ALARMPANEL_YES;
						Beep();
					}
					#endif
					#endif
					if(handle != alarmPanel){
						continue;
					}

					switch(control){
						case ALARMPANEL_YES:
							// Sending NO_840 turns the alarm off now, sending YES_840 takes
							// about 10 seconds to turn off
							//Prompt_Accept(YES_840);
							#ifndef DEMOMODE
							Prompt_Accept(NO_840);
							#endif
							tDataRec[TEST_3].result = PASS;
							doneFlag = TRUE;
							break;

						case ALARMPANEL_NO:
							#ifndef DEMOMODE
							Prompt_Accept(NO_840);
							#endif
							tDataRec[TEST_3].result = FAIL;
							doneFlag = TRUE;
							break;
					} // end switch(control)
				} // end while(doneFlag)
				TestPrompt(Clear);
				#ifdef DEMOMODE
				TestActive = NO_840;
				#endif
			} // end if(PromptReturn == SOUND_PROMPT)
		} // end while(TestActive != NO_840)

		if(retries >= MAX_TRIES_ALLOWED){
			retry = FALSE;
			continue;
		}

		if(tDataRec[TEST_3].result == PASS){
			retry = FALSE;
		}
		else if(tDataRec[TEST_3].result == FAIL){
			nRetryRequest = CPVTConfirmPopup(LDBf(Ldb_Test_Failure), LDBf(Ldb_Retry_prompt), 
			                                 OK_CANCEL_DLG, NO_HELP);
			if(nRetryRequest){
				retries++;
			}
			else{
				retry = FALSE;
			}
		}
	} // end while(retry)

	if(tDataRec[TEST_3].result == PASS){
		PostStringPass(TEST_3, &(tDataRec[TEST_3].outBuffer),         LDB( Ldb_Alarm_Audible, ENGLISH), "");
		PostStringPass(TEST_3, &(tDataRec[TEST_3].outBuff_localized), LDBf(Ldb_Alarm_Audible),          "");
	}
	else if(tDataRec[TEST_3].result == FAIL){
		PostStringFail(TEST_3, &(tDataRec[TEST_3].outBuffer),         LDB( Ldb_No_Alarm, ENGLISH), "");
		PostStringFail(TEST_3, &(tDataRec[TEST_3].outBuff_localized), LDBf(Ldb_No_Alarm),          "");
	}

	return;
}


/*****************************************************************************/
/* Name:      GuiAlarmTest                                                   */
/*                                                                           */
/* Purpose:  This function will set the to alarm its Gui audio alarm         */
/*                                                                           */
/*                                                                           */
/* Example Call:  void GuiAlarmTest( void );                                 */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void GuiAlarmTest(void)
{
	int connect;
	int alarmPanel; // AlarmPanel ID
	int handle;  // UserEvent commit: ID of panel   where commit
	int control; // UserEvent commit: ID of control where commit
	BOOL doneFlag = FALSE;
	double userTimeOut;
	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	double demoTimeOut;
	#endif
	#endif
	int retry = TRUE;
	int retries = 0;
	int nRetryRequest;

	//  per test initialization:
	connect = Establish840(NO_AUTO_DETECT);
	if(connect != SUCCESS){
		gErrorFlag = TRUE;
		ErrorMessage(COM_840_ERROR);

		return;
	}

	while(retry){
		doneFlag = FALSE;
		tDataRec[TEST_4].result = FAIL;
		gErrorFlag = FALSE;
		SetSysPanelMessage(LDBf(Ldb_Test4_Running));
		#ifndef DEMOMODE
		SEND_840(GuiAudio())
		userTimeOut = Timer() + 1.5;
		while(Timer() < userTimeOut){
			RECV_840()
			CHECK_FOR_ABORT_STOP_AUDIO
			if(gStoppedInd == 1){
				tDataRec[TEST_4].result = ABORT;
				return;
			}
		}
		#endif 

		TestActive = YES_840;

		/* Retrieve the front panel */
		alarmPanel = TestPrompt(GUIAlarm);

		while(TestActive != NO_840){
			#ifndef DEMOMODE
			CHECK_FOR_ABORT_STOP_AUDIO
			if(gStoppedInd == 1){
				tDataRec[TEST_4].result = ABORT;
				return;
			}
			RECV_840()
			#else
			PromptReturn = SOUND_PROMPT;
			#endif
			if(PromptReturn == SOUND_PROMPT){
				#ifdef DEMOMODE
				#ifdef USE_DEMO_TIMEOUT
				demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
				#endif
				#endif
				if(doneFlag != TRUE){
					PromptDisplay(alarmPanel);
				}
				while(doneFlag != TRUE){
					CHECK_FOR_ABORT_STOP_AUDIO
					if(gStoppedInd == 1){
						tDataRec[TEST_4].result = ABORT;
						return;
					}
					GetUserEvent(0, &handle, &control); // Check for user event
					#ifdef DEMOMODE
					#ifdef USE_DEMO_TIMEOUT
					if(Timer() >= demoTimeOut){
						handle = alarmPanel;
						control = ALARMPANEL_YES;
						Beep();
					}
					#endif
					#endif
					if(handle != alarmPanel){
						continue;
					}

					switch(control){
						case ALARMPANEL_YES:
							#ifndef DEMOMODE
							Prompt_Accept(YES_840);
							#endif
							tDataRec[TEST_4].result = PASS;
							doneFlag = TRUE;
							break;

						case ALARMPANEL_NO:
							#ifndef DEMOMODE
							Prompt_Accept(NO_840);
							#endif
							tDataRec[TEST_4].result = FAIL;
							doneFlag = TRUE;
							break;
					} // end switch(control)
				} // end while(doneFlag)
				TestPrompt(Clear);
				#ifdef DEMOMODE
				TestActive = NO_840;
				#endif
			} // end if(PromptReturn == SOUND_PROMPT)
		} // end while(TestActive != NO_840)

		if(retries >= MAX_TRIES_ALLOWED){
			retry = FALSE;
			continue;
		}

		if(tDataRec[TEST_4].result == PASS){
			retry = FALSE;
		}
		else if(tDataRec[TEST_4].result == FAIL){
			nRetryRequest = CPVTConfirmPopup(LDBf(Ldb_Test_Failure), LDBf(Ldb_Retry_prompt), 
			                                 OK_CANCEL_DLG, NO_HELP);
			if(nRetryRequest){
				retries++;
			}
			else{
				retry = FALSE;
			}
		}
	} // end while(retry)

	if(tDataRec[TEST_4].result == PASS){
		PostStringPass(TEST_4, &(tDataRec[TEST_4].outBuffer),         LDB( Ldb_Alarm_Audible, ENGLISH), "");
		PostStringPass(TEST_4, &(tDataRec[TEST_4].outBuff_localized), LDBf(Ldb_Alarm_Audible),          "");
	}
	else if(tDataRec[TEST_4].result == FAIL){
		PostStringFail(TEST_4, &(tDataRec[TEST_4].outBuffer),         LDB( Ldb_No_Alarm, ENGLISH), "");
		PostStringFail(TEST_4, &(tDataRec[TEST_4].outBuff_localized), LDBf(Ldb_No_Alarm),          "");
	}

	return;
}


/*****************************************************************************/
/* Name:      CircuitPressureTest                                            */
/*                                                                           */
/* Purpose:  This function will                                              */
/*            1) instruct operator with use of prompt panels                 */
/*            2) Issue a Circuit Pressure command to Vent                    */
/*            3) read triggered reading from pts 2000                        */
/*                                                                           */
/* Example Call:  void CircuitPressureTest(void);                            */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void CircuitPressureTest(void)
{
	int connect;
	int x;
	int howMany;
	int cpPanel;
	int index;
	int status;
	int handle;
	int points;
	int control = 0;
	int cpDone;
	int cpDone2;
	int cpIndex        = 0;
	int doneFlag       = 0;
	int airO2Flag      = 0;
	int removeInspFlag = 0;
	int readyNextPress = 1;
	int stage          = 1;

	int retryFlag  = 1;
	int retryCount = 0;
	int failed     = 0;

    int isFailure = FALSE;  // Overall Test Result Flag  

	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	double demoTimeOut;
	#endif
	#endif

	#ifndef DEMOMODE
	double reading[CP_TEST_POINTS];
	#else
	double reading[CP_TEST_POINTS] = {9.45, 48.26, 97.61};
	#endif
	double lowLimit, highLimit;

	char cBuff[50];
	#ifndef DEMOMODE
	float cpData[CP_RETURN_POINTS];
	#else
	float cpData[CP_RETURN_POINTS] = {1612, 1610, 9.7121925, 9.5447407, 0, 0, 49.1470451, 48.9795914, 98.7127151, 98.3778152};
	#endif

	//  per test initialization:
	connect = Establish840(NO_AUTO_DETECT);
	if(connect != SUCCESS){
		gErrorFlag = TRUE;
		ErrorMessage(COM_840_ERROR);
		return;
	}

	SetSysPanelMessage(LDBf(Ldb_Test5_Running));

	while(retryFlag && retryCount <= MAX_TRIES_ALLOWED){

		gErrorFlag = FALSE;
		cpIndex        = 0;
		doneFlag       = 0;
		airO2Flag      = 0;
		removeInspFlag = 0;
		readyNextPress = 1;
		stage          = 1;

		TestActive = YES_840;
		#ifndef DEMOMODE
		for(x = 0; x < CP_TEST_POINTS; x++){
			reading[x] = 0.0;
		}
		#endif

		// Retrieve the front panel
		cpPanel = TestPrompt(Clear);
		cpDone  = 0;
		cpDone2 = 0;

		#ifndef DEMOMODE
		SEND_840(CircuitPressure())
		#endif
		TestPrompt(CPT1);

		#ifdef DEMOMODE
		#ifdef USE_DEMO_TIMEOUT
		demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
		#endif
		#endif
		while(!doneFlag){
			CHECK_FOR_ABORT
			if(gStoppedInd == 1){
				tDataRec[TEST_5].result = ABORT;
				return;
			}
			GetUserEvent(0, &handle, &control); // Check for user event
			#ifdef DEMOMODE
			#ifdef USE_DEMO_TIMEOUT
			if(Timer() >= demoTimeOut){
				handle = cpPanel;
				control = ALARMPANEL_DONE;
				Beep();
			}
			#endif
			#endif
			if(handle == cpPanel){
				if(control == ALARMPANEL_DONE){
					if(stage == 1){
						cpDone = 1;
						stage  = 2;
						#ifdef DEMOMODE
						PromptReturn = CONNECT_AIR_AND_O2_PROMPT;
						#endif
					}
					else if(stage == 2){
						cpDone2 = 1;
					}
					TestPrompt(Clear);
				}
			}

			#ifndef DEMOMODE
			RECV_840()
			#endif
			if(PromptReturn == CONNECT_AIR_AND_O2_PROMPT && !airO2Flag){
				if(cpDone){ // send prompt accept
					#ifndef DEMOMODE
					Prompt_Accept(YES_840);
					#else
					PromptReturn =  REMOVE_INSP_FILTER_PROMPT;
					#ifdef USE_DEMO_TIMEOUT
					demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
					#endif
					#endif
					airO2Flag = 1;
				}
			}

			if(PromptReturn ==  REMOVE_INSP_FILTER_PROMPT  && !removeInspFlag){
				// Display instruction to connect circuit
				TestPrompt(CPT2);
				if(cpDone2){ // send prompt accept
					TestPrompt(Clear);
					removeInspFlag = 1;
					#ifndef DEMOMODE
					Prompt_Accept(YES_840);
					#else
					PromptReturn = NO_PROMPT;
					TestActive = NO_840;
					DataAvailable = YES_840;
					#endif
				}
			}
			#ifndef DEMOMODE
			RECV_840()
			#endif

			if(removeInspFlag && (cpIndex < CP_TEST_POINTS) && readyNextPress){
				#ifndef DEMOMODE
				SEND_PTS(SetupPressure(5, 1))
				#endif
				readyNextPress = 0;
			}

			if(CharsInPTSQ() && (!readyNextPress)){
				#ifndef DEMOMODE
				SEND_PTS(RecievePressureReading(&reading[cpIndex], &points, 15))
				#endif
				readyNextPress = 1;
				cpIndex++;
			}

			if(TestActive == NO_840){
				doneFlag = 1;
			}

		} // end while(!doneFlag)

		TestPrompt(Clear);

		// retrieve data element from vent
		if(DataAvailable == YES_840){
			#ifndef DEMOMODE
			howMany = GetDataStorage(cpData);
			#else
			howMany = CP_RETURN_POINTS;
			#endif
		}
		else{
			AppErr(ERR_VENT_CP, 0.0);
			gErrorFlag = TRUE;
			ErrorMessage(PRESSURE_TEST_FAILURE_ERROR);
			return;
		}
		if(howMany != CP_RETURN_POINTS){
			AppErr(ERR_VENT_CP, 0.0);
			gErrorFlag = TRUE;
			ErrorMessage(PRESSURE_TEST_FAILURE_ERROR);
			return;
		}

		cpIndex   = 2;
		failed    = 0;
		retryFlag = FALSE;

		for(index = 0; index < CP_TEST_POINTS ; index ++){

			// Circuit Pressure reported by PTS shall not be qualified in accordance with test specifications 
			// so as to be consistent with manufacturing test software.  By order of Project Manager 
			// with agreement by manufacturing test engineering in peer review 09/12/2000.

			//if(CircuitPressureLimits[index].lowerLimit > reading[index] ||
			//   CircuitPressureLimits[index].upperLimit < reading[index] ){
			//	failed++;
			//}

			lowLimit  = reading[index] * (1 - CircuitPressureAccuracy) - CircuitPressureOffset;
			highLimit = reading[index] * (1 + CircuitPressureAccuracy) + CircuitPressureOffset;

			if(lowLimit > (double)cpData[cpIndex]   || (double)cpData[cpIndex] > highLimit ||
			   lowLimit > (double)cpData[++cpIndex] || (double)cpData[cpIndex] > highLimit )
			{
				failed++;
			} 
			if(cpIndex <= 4 ){
				cpIndex = 6;
			}
			else{
				cpIndex++;
			}
		} // end for(index = 0; index < CP_TEST_POINTS ; index ++)

		if(failed > 0){
			if(!CPVTConfirmPopup(LDBf(Ldb_Test_Failure), LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP)){
				retryFlag = FALSE;
                isFailure = TRUE;
			}
			else{
				retryFlag = TRUE;
				retryCount++;
                if ( (retryCount + 1) > MAX_TRIES_ALLOWED) {
                    isFailure = TRUE;
                }
			}
		}
	} //end while(retryFlag && retryCount <= MAX_TRIES_ALLOWED)

	//Post the final results
	cpIndex = 2;
	for(index = 0; index < CP_TEST_POINTS ; index ++){

		// Circuit Pressure reported by PTS shall not be qualified in accordance with test specifications 
		// so as to be consistent with manufacturing test software.  By order of Project Manager 
		// with agreement by manufacturing test engineering in peer review 09/12/2000.

		//sprintf(cBuff, LDB( Ldb_cp_test_msg, ENGLISH), CpInspTestTarget[index].target, reading[index]);
		//status = PostReal(&(tDataRec[TEST_5]outBuffer),         cBuff, CircuitPressureLimits[index].lowerLimit, reading[index],
		//                  CircuitPressureLimits[index].upperLimit);
		//sprintf(cBuff, LDBf(Ldb_cp_test_msg),          CpInspTestTarget[index].target, reading[index]);
		//status = PostReal(&(tDataRec[TEST_5]outBuff_localized), cBuff, CircuitPressureLimits[index].lowerLimit, reading[index],
		//                  CircuitPressureLimits[index].upperLimit);
		//if(status){
		//	tDataRec[TEST_5]result = FAIL;
		//}

		// Always passes, see comment above
		sprintf(cBuff, LDB( Ldb_cp_test_msg, ENGLISH), CpInspTestTarget[index].target, reading[index]);
		PostStringPass(TEST_5, &(tDataRec[TEST_5].outBuffer),         cBuff, "");
		sprintf(cBuff, LDBf(Ldb_cp_test_msg),          CpInspTestTarget[index].target, reading[index]);
		PostStringPass(TEST_5, &(tDataRec[TEST_5].outBuff_localized), cBuff, "");

		lowLimit  = reading[index] * (1 - CircuitPressureAccuracy) - CircuitPressureOffset;
		highLimit = reading[index] * (1 + CircuitPressureAccuracy) + CircuitPressureOffset;

		sprintf(cBuff, "%s", LDB( CpInspTestTarget[index].name, ENGLISH));
		status = PostReal(TEST_5, &(tDataRec[TEST_5].outBuffer),         cBuff, lowLimit, (double)cpData[cpIndex], highLimit);   //INSP
		sprintf(cBuff, "%s", LDBf(CpInspTestTarget[index].name));
    	status = PostReal(TEST_5, &(tDataRec[TEST_5].outBuff_localized), cBuff, lowLimit, (double)cpData[cpIndex], highLimit);   //INSP
		if(status){
			tDataRec[TEST_5].result = FAIL;
		}

		cpIndex++;
		sprintf(cBuff, "%s", LDB( CpExhTestTarget[index].name, ENGLISH));
		status = PostReal(TEST_5, &(tDataRec[TEST_5].outBuffer),         cBuff, lowLimit, (double)cpData[cpIndex], highLimit);  //EXH
		sprintf(cBuff, "%s", LDBf(CpExhTestTarget[index].name));
		status = PostReal(TEST_5, &(tDataRec[TEST_5].outBuff_localized), cBuff, lowLimit, (double)cpData[cpIndex], highLimit);  //EXH
		if(status){
			tDataRec[TEST_5].result = FAIL;
		}

		if(cpIndex <= 4){
			cpIndex = 6;
		}
		else{
			cpIndex++;
		}
	} // end for(index = 0; index < CP_TEST_POINTS ; index ++)

    // If any of the mini tests above did not pass, the resulting overall test 
    // will be equal to FAIL.
    if(isFailure)
    {    
         tDataRec[TEST_5].result = FAIL;           
    }

	return;
}


/*****************************************************************************/
/* Name:    GasFlowPerformanceTest                                           */
/*                                                                           */
/* Purpose:   This function will                                             */
/*            1) Flush circuit with Gas(Air/O2)                              */
/*            2) Command vent for predetermine flow                          */
/*            3) Arm Pts 2000                                                */
/*            4) Read triggered flow from Pts 2000                           */
/*                                                                           */
/* Example Call:   void GasFlowPerformanceTest(int);                         */
/*                                                                           */
/* Input:    <int>                                                           */
/*                                                                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void GasFlowPerformanceTest(int selection)
{
	int connect;
	int index;
	int status;
	int howMany;
	int promptFlag;
	int pointsReceived;
	int doneFlag = 0;
	int x;
	int maxTests;
	float ans[3];
	float* Allans;
	char cBuff[35];
	unsigned char length;
	double reading;
	double lowTemp;
	double hiTemp;
	double timeOut;
	double* Allreading;

	int retryFlag  = TRUE;
	int retryCount = 0;
	int failed     = 0;

    int isFailure = FALSE;  // Overall Test Result Flag  

	maxTests = sizeof(GasFlowLimits) /  sizeof(struct TargetLimits);
	//set up the retry tables
	Allans     = (float  *)malloc((maxTests * 3) * sizeof(float ));
	Allreading = (double *)malloc( maxTests      * sizeof(double)); 

	//  per test initialization:
	connect = Establish840(NO_AUTO_DETECT);
	if(connect != SUCCESS){
		gErrorFlag = TRUE;
		ErrorMessage(COM_840_ERROR);
		goto GASFLOWQUIT;
	}
	gErrorFlag = FALSE;

	TestActive = YES_840;

	if(selection != 1 && selection != 2){
		tDataRec[TEST_6].result = FAIL;
		tDataRec[TEST_7].result = FAIL;
		goto GASFLOWQUIT;
	}
	
	if(selection == 1){
		SetSysPanelMessage(LDBf(Ldb_Test6_Running));
	}
	else{
		SetSysPanelMessage(LDBf(Ldb_Test7_Running));
	}

	while(retryFlag == TRUE && retryCount <=  MAX_TRIES_ALLOWED){
		gErrorFlag = FALSE;
		TestActive = YES_840;
		// Flush system with Gas
		for(x = 0; x < 5; x++){
			CHECK_FOR_ABORT
			if(gStoppedInd == 1){
				if(selection == 1){
					tDataRec[TEST_6].result = ABORT;
				}
				else{
					tDataRec[TEST_7].result = ABORT;
				}
				return;
			}

			#ifndef DEMOMODE
			if(selection == 1){
				SEND_PTS(FlushGas(AIR_FLUSH)) 
			}
			else{
				SEND_PTS(FlushGas(O2_FLUSH))
			}
			#endif
		}

		Delay(.5);
		// for each element in air limit
		for(index = 0; index < maxTests; index++){
			TestActive = YES_840;
			KeyAllowedReturn = -2;
			doneFlag = 0;
			status = PASS;
			promptFlag = 0;
			#ifndef DEMOMODE
			FlushInQ840();    // fresh start
			FlushOutQ840();
			#endif

			#ifndef DEMOMODE
			// Arm pts2000 to measure flow
			if(selection == 1 ){
				SEND_PTS(SetupFlow(1, 3, 0))   // Air
				SEND_840(SetAirFlow(GasFlowLimits[index].target))
			}
			else{
				SEND_PTS(SetupFlow(1, 3, 1))   // O2
				SEND_840(SetO2Flow(GasFlowLimits[index].target))
			}

			//clear vent message Q
			timeOut = Timer() + (double)GAS_TIMEOUT;
			while(!doneFlag  && (Timer() < timeOut) ){
				CHECK_FOR_ABORT
				if(gStoppedInd == 1){
					if(selection == 1){
						tDataRec[TEST_6].result = ABORT;
					}
					else{
						tDataRec[TEST_7].result = ABORT;
					}
					return;
				}
				RECV_840()
				if(CharsInPTSQ()){
					if(RecieveReading(&reading, &pointsReceived, 20)){
						AppErr(ERR_PTS_BURST, 0.0);        //pts time out
					}
				}
				// SEQ = TEST_START,DATA_RETURNS,ACCPET_KEY_ONLY,PROMPT TO STOP
				if(PromptReturn == STOP_GAS_FLOW  && !promptFlag){
					promptFlag = 1;
					Prompt_Accept(YES_840);
				}

				if(TestActive == NO_840){
					doneFlag = 1;
				}

				//ProcessSystemEvents();
			} // end while(!doneFlag  && (Timer() < timeOut) )

			// collect vent data
			if(DataAvailable == YES_840){
				howMany = GetDataStorage(ans); // Data returned from Vent
				if(howMany != 3){
					status = FAIL;
				}
			}
			else{
				status = FAIL;
			}
			if(TestFail == YES_840){   // If vent reports failure continue test
			                           // user may have bad gas supply
				if(selection == 1){
					PostStringFail(TEST_6, &(tDataRec[TEST_6].outBuffer),         LDB( Ldb_vent_fail_reported, ENGLISH), "");
					PostStringFail(TEST_6, &(tDataRec[TEST_6].outBuff_localized), LDBf(Ldb_vent_fail_reported),          "");
					tDataRec[TEST_6].result = FAIL;
					status = FAIL;
				}
				else{
					PostStringFail(TEST_7, &(tDataRec[TEST_7].outBuffer),         LDB( Ldb_vent_fail_reported, ENGLISH), "");
					PostStringFail(TEST_7, &(tDataRec[TEST_7].outBuff_localized), LDBf(Ldb_vent_fail_reported),          "");
					tDataRec[TEST_7].result = FAIL;
					status = FAIL;
				}
			}

			if(!doneFlag){
				status = FAIL;
			}

			if(status == FAIL){
				AppErr(ERR_VENT_GAS_SEQ, 0.0 );
				gErrorFlag = TRUE;
				ErrorMessage(GAS_FLOW_TEST_FAILURE_ERROR);
				goto GASFLOWQUIT;
			}
			#endif

			// post results of reading and pass/fail
			if(status != FAIL){
				// post pts2000 reading
				#ifdef DEMOMODE
				switch(selection){
					case 1:
						switch(index){
							case 0:
								reading = 0.9300;
								ans[0] = 1.0098;
								ans[1] = 0.9394;
								break;
							case 1:
								reading = 7.5900;
								ans[0] = 7.9685;
								ans[1] = 7.7795;
								break;
							case 2:
								reading = 59.8999;
								ans[0] = 59.6150;
								ans[1] = 59.2086;
								break;
							case 3:
								reading = 150.1067;
								ans[0] = 149.2170;
								ans[1] = 148.2544;
								break;
							case 4:
								reading = 205.8200;
								ans[0] = 198.7946;
								ans[1] = 200.2320;
								break;
						}
						break;
					case 2:
						switch(index){
							case 0:
								reading = 0.9260;
								ans[0] = 0.9963;
								ans[1] = 0.9329;
								break;
							case 1:
								reading = 7.5700;
								ans[0] = 7.9891;
								ans[1] = 7.8052;
								break;
							case 2:
								reading = 57.6730;
								ans[0] = 59.6836;
								ans[1] = 58.5107;
								break;
							case 3:
								reading = 147.9400;
								ans[0] = 149.1727;
								ans[1] = 147.4729;
								break;
							case 4:
								reading = 195.9400;
								ans[0] = 198.7720;
								ans[1] = 194.0451;
								break;
						}
						break;
				}
				#endif
				Allreading[index] = reading;
				Allans[(index*3)+ 0] = ans[0];
				Allans[(index*3)+ 1] = ans[1];
				if(GasFlowLimits[index].lowerLimit > reading ||
				   GasFlowLimits[index].upperLimit < reading ){
					failed++;
				}

				hiTemp  = reading * (1 + InspUpperAccuracy) + InspUpperOffset;
				lowTemp = reading * (1 - InspLowerAccuracy) - InspLowerOffset;

				if(lowTemp > (double)ans[0] || (double)ans[0] > hiTemp){
					failed++;
				}

				hiTemp =  reading * (1 + ExhUpperAccuracy) + ExhUpperOffset;
				lowTemp = reading * (1 - ExhLowerAccuracy) - ExhLowerOffset;

				if(lowTemp > (double)ans[1] || (double)ans[1] > hiTemp){
					failed++;
				}
			} // end if(status != FAIL)
		} // end for(index = 0; index < maxTests; index++)

		if(failed > 0){
			if(!CPVTConfirmPopup(LDBf(Ldb_Test_Failure), LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP)){
				retryFlag = FALSE;
                isFailure = TRUE;
			}
			else{
				retryFlag = TRUE;
				retryCount++;
                if ( (retryCount + 1) > MAX_TRIES_ALLOWED) {
                    isFailure = TRUE;
                }
			}
		}
		else{
			retryFlag = FALSE;
		}
		failed = 0;
	} // end while(retryFlag == TRUE && retryCount <=  MAX_TRIES_ALLOWED)


	for(index = 0; index < maxTests; index++){
		sprintf(cBuff, LDB( Ldb_target, ENGLISH), GasFlowLimits[index].target);
		if(selection == 1){
			status = PostReal(TEST_6, &(tDataRec[TEST_6].outBuffer),         cBuff, GasFlowLimits[index].lowerLimit, Allreading[index],
			                  GasFlowLimits[index].upperLimit);
			sprintf(cBuff, LDBf(Ldb_target),          GasFlowLimits[index].target);
			status = PostReal(TEST_6, &(tDataRec[TEST_6].outBuff_localized), cBuff, GasFlowLimits[index].lowerLimit, Allreading[index],
		    	              GasFlowLimits[index].upperLimit);
			if(status){           
				tDataRec[TEST_6].result = FAIL;
			}
            
		}		
		else if (selection ==2){
			status = PostReal(TEST_7, &(tDataRec[TEST_7].outBuffer),         cBuff, GasFlowLimits[index].lowerLimit, Allreading[index],
			                  GasFlowLimits[index].upperLimit);
			sprintf(cBuff, LDBf(Ldb_target),          GasFlowLimits[index].target);
			status = PostReal(TEST_7, &(tDataRec[TEST_7].outBuff_localized), cBuff, GasFlowLimits[index].lowerLimit, Allreading[index],
			                  GasFlowLimits[index].upperLimit);
			if(status){                  
				tDataRec[TEST_7].result = FAIL;
			}
		}
		// ans[0] = inspSensor reading (q1) or (q2);
		hiTemp  = Allreading[index] * (1 + InspUpperAccuracy) + InspUpperOffset;
		lowTemp = Allreading[index] * (1 - InspLowerAccuracy) - InspLowerOffset;

		sprintf(cBuff, LDB( Ldb_insp, ENGLISH));
		if( selection ==1 ){
		 	status = PostReal(TEST_6, &(tDataRec[TEST_6].outBuffer),         cBuff, lowTemp , (double)Allans[(index * 3) + 0], hiTemp);
            sprintf(cBuff, LDBf(Ldb_insp));
			status = PostReal(TEST_6, &(tDataRec[TEST_6].outBuff_localized), cBuff, lowTemp , (double)Allans[(index * 3) + 0], hiTemp);
			if(status) {            
				tDataRec[TEST_6].result = FAIL;
			}
		}
		else if (selection == 2){
		 	status = PostReal(TEST_7, &(tDataRec[TEST_7].outBuffer),         cBuff, lowTemp , (double)Allans[(index * 3) + 0], hiTemp);
            sprintf(cBuff, LDBf(Ldb_insp));
			status = PostReal(TEST_7, &(tDataRec[TEST_7].outBuff_localized), cBuff, lowTemp , (double)Allans[(index * 3) + 0], hiTemp);
			if(status) {
				tDataRec[TEST_7].result = FAIL;
			}
		}			
		// ans[1] = exhSensor reading (q3);
		hiTemp =  Allreading[index] * (1 + ExhUpperAccuracy) + ExhUpperOffset;
		lowTemp = Allreading[index] * (1 - ExhLowerAccuracy) - ExhLowerOffset;

		sprintf(cBuff, LDB( Ldb_exp, ENGLISH));
		if (selection == 1){
			status = PostReal(TEST_6, &(tDataRec[TEST_6].outBuffer),         cBuff, lowTemp , (double)Allans[(index * 3) + 1], hiTemp);
            sprintf(cBuff, LDBf(Ldb_exp));
			status = PostReal(TEST_6, &(tDataRec[TEST_6].outBuff_localized), cBuff, lowTemp , (double)Allans[(index * 3) + 1], hiTemp);
			if(status){
				tDataRec[TEST_6].result = FAIL;
			}
		}
		else if (selection == 2){
			status = PostReal(TEST_7, &(tDataRec[TEST_7].outBuffer),         cBuff, lowTemp , (double)Allans[(index * 3) + 1], hiTemp);
            sprintf(cBuff, LDBf(Ldb_exp));
			status = PostReal(TEST_7, &(tDataRec[TEST_7].outBuff_localized), cBuff, lowTemp , (double)Allans[(index * 3) + 1], hiTemp);
			if(status) {            
				tDataRec[TEST_7].result = FAIL;
			}
		}
	} // end for(index = 0; index < sizeof(GasFlowLimits) / sizeof(struct TargetLimits);index++)

    // If any of the mini tests above did not pass, the resulting overall test 
    // will be equal to FAIL.
    if(isFailure)
    {    
        if (selection == 1)
        {
            tDataRec[TEST_6].result = FAIL;           
        }
        else
        {
            tDataRec[TEST_7].result = FAIL;
        }
    }

GASFLOWQUIT:
	free(Allans);
	free(Allreading);
	return;
}


/*****************************************************************************/
/* Name:    VolumePerformanceTest                                            */
/*                                                                           */
/* Purpose:  This function during breath delivery perform volume calcuations */
/*           with use of the VerifyVolume function                           */
/*                                                                           */
/* Example Call:   void VolumePerformanceTest(void)                          */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void VolumePerformanceTest(void)
{
	int b;
	int sstErr;
	int control;
	int perfPanel1;
	int doneButton;
	int measureFlag;
	int handle;
	int answer;
	int testResult;
	int spiro;
	int maxTests;
	int currentVolumeTest;
	int tries;
	int panelDone = FALSE;

	double atp;
	double btps;

	char spiroBuff[15];
	char cBuff[50];
    int isFailure = FALSE;  // Overall Test Result Flag 

	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	double demoTimeOut;
	#endif
	#endif

	tDataRec[TEST_9].result = PASS;      /* init input */
	SetSysPanelMessage(LDBf(Ldb_Test8_Running));

	if(InitPerfPanel(&perfPanel1, &doneButton)){
		AppErr(ERR_UIR_LOAD, (double)perfPanel1);
	}

	if(!SST){
		sstErr = DisplaySST();
		if(sstErr){
			PostStringFail(TEST_9, &(tDataRec[TEST_9].outBuffer),         LDB( Ldb_sst_proc_error, ENGLISH), "");
			PostStringFail(TEST_9, &(tDataRec[TEST_9].outBuff_localized), LDBf(Ldb_sst_proc_error),          "");
			tDataRec[TEST_9].result = FAIL;
			return;
		}
		else{
			SST = 1;
		}
	}
	// Start of Modification this statements due to Critical Observation.
	if( sizeof(struct VolumeStruct) > 0){
		maxTests = sizeof(Volume) / sizeof(struct VolumeStruct);
	}
	else{
		maxTests = 0;
	}
	// end of modification.
	currentVolumeTest = 0;
	while(currentVolumeTest < maxTests){
		CHECK_FOR_ABORT_PERFPANEL
		if(gStoppedInd == 1){
			tDataRec[TEST_9].result = ABORT;
			return;
		}
		sprintf(cBuff, LDBf(Ldb_step_x_y), currentVolumeTest + 1, maxTests);
		SetCtrlVal(perfPanel1, PERF_PANEL_TESTCOUNT, cBuff);

		SetPerfPanelVolume(currentVolumeTest + 1);
		DisplayPanel(perfPanel1);
		for(b = 0; b < 3; b++){
			Beep();
		}

		for(tries = 0; tries < MAX_TRIES_ALLOWED; tries++){
			panelDone = FALSE;
			measureFlag = 0;
			atp  = 0.0;
			btps = 0.0;
			spiro = 0;
			//SetCtrlAttribute(perfPanel1, doneButton, ATTR_VISIBLE, 1);
			DisplayPanel(perfPanel1);
			#ifdef DEMOMODE
			#ifdef USE_DEMO_TIMEOUT
			demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
			#endif
			#endif
			while(!panelDone){
				CHECK_FOR_ABORT_PERFPANEL
				if(gStoppedInd == 1){
					tDataRec[TEST_9].result = ABORT;
					return;
				}
				GetUserEvent(0, &handle, &control); /* Check for user event */
				#ifdef DEMOMODE
				#ifdef USE_DEMO_TIMEOUT
				if(Timer() >= demoTimeOut){
					handle = perfPanel1;
					control = doneButton;
					Beep();
				}
				#endif
				#endif
				if(handle != perfPanel1){
					continue;
				}
				if(control == PERF_PANEL_ABORT){
					PostStringFail(TEST_9, &(tDataRec[TEST_9].outBuffer),         LDB( Ldb_test_aborted, ENGLISH), "");
					PostStringFail(TEST_9, &(tDataRec[TEST_9].outBuff_localized), LDBf(Ldb_test_aborted),          "");
					tDataRec[TEST_9].result = ABORT;
					DiscardPanel(perfPanel1);
					return;
				}
				if(control == doneButton){
				//	SetCtrlAttribute(perfPanel1,doneButton, ATTR_VISIBLE, 0);
					HidePanel(perfPanel1);
					ProcessSystemEvents();
					if(Volume[currentVolumeTest].gas == OXYGEN){
						SetO2Gas();
					}
					#ifndef DEMOMODE
					SEND_PTS(SetUpVolumeOn(1, // sampleRate 
					                       Volume[currentVolumeTest].inspTime, 
					                       Volume[currentVolumeTest].inspTime, 
					                       VOLUME_TIMEOUT))
					#endif
					#ifdef DEMOMODE
					switch(currentVolumeTest){
						case 0:
							sprintf(spiroBuff, "%d", 23);
							break;
						case 1:
							sprintf(spiroBuff, "%d", 24);
							break;
						case 2:
							sprintf(spiroBuff, "%d", 22);
							break;
						case 3:
							sprintf(spiroBuff, "%d", 200);
							break;
						case 4:
							sprintf(spiroBuff, "%d", 201);
							break;
						case 5:
							sprintf(spiroBuff, "%d", 611);
							break;
						case 6:
							sprintf(spiroBuff, "%d", 606);
							break;
						case 7:
							sprintf(spiroBuff, "%d", 2493);
							break;
						case 8:
							sprintf(spiroBuff, "%d", 2493);
							break;
					}
					#else
					spiroBuff[0] = '\0';
					#endif
					CPVTPromptPopup(LDBf(Ldb_spirometry), LDBf(Ldb_enter_spiro), OK_CANCEL_DLG, 0, spiroBuff, sizeof(spiroBuff)-1);
					sscanf(spiroBuff, "%d", &spiro);
					#ifndef DEMOMODE
					measureFlag = MeasureVentVolume(&btps, &atp);
					#else
					switch(currentVolumeTest){
						case 0:
							btps = 23.23;
							break;
						case 1:
							btps = 24.39;
							break;
						case 2:
							btps = 22.52;
							break;
						case 3:
							btps = 200.1;
							break;
						case 4:
							btps = 200.1;
							break;
						case 5:
							btps = 611.7;
							break;
						case 6:
							btps = 606.5;
							break;
						case 7:
							btps = 2493;
							break;
						case 8:
							btps = 2493;
							break;
					}
					measureFlag = 0;
					#endif
					panelDone = TRUE;
				}
			} // end while(!panelDone)

			if(measureFlag){
				answer = CPVTConfirmPopup(LDBf(Ldb_pts_timeout), LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP);
				if(!answer){
					PostStringFail(TEST_9, &(tDataRec[TEST_9].outBuffer), LDB( Ldb_pts_timeout, ENGLISH), "");
					PostStringFail(TEST_9, &(tDataRec[TEST_9].outBuffer), LDBf(Ldb_pts_timeout),          "");
					tries = MAX_TRIES_ALLOWED + 1 ;
					tDataRec[TEST_9].result = FAIL;
					currentVolumeTest++;
                    isFailure = TRUE;
					continue;
				}
				else{
                    if ((tries + 1) >= MAX_TRIES_ALLOWED) {
                        PostStringFail(TEST_9, &(tDataRec[TEST_9].outBuffer), LDB( Ldb_pts_timeout, ENGLISH), "");
                        PostStringFail(TEST_9, &(tDataRec[TEST_9].outBuffer), LDBf(Ldb_pts_timeout),          "");
                        isFailure = TRUE;
                    }
					continue;
				}
			}
			if(testResult = VerifyVolume(btps, (double)spiro, currentVolumeTest, 0) == 0){
				VerifyVolume(btps, (double)spiro, currentVolumeTest, 1);			   
				currentVolumeTest++;
				tries = MAX_TRIES_ALLOWED + 1;
			}
			else{
                answer = CPVTConfirmPopup(LDBf(Ldb_volume_incorrect), LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP);
				if(!answer){
					tries = MAX_TRIES_ALLOWED + 1 ;
					VerifyVolume( btps, (double)spiro, currentVolumeTest, 1);
                    tDataRec[TEST_9].result = FAIL;
					currentVolumeTest++;
                    isFailure = TRUE;
				}
                else
                {
                    if ((tries + 1) >= MAX_TRIES_ALLOWED) {
                        isFailure = TRUE;
                    }
                }
			} // end if((testResult = VerifyVolume(data,btps,(double)spiro, currentVolumeTest,0)) == 0 )
		} // end for(tries = 0; tries < MAX_TRIES_ALLOWED; tries++)
	} // end while(currentVolumeTest < maxTests)


    // If any of the mini tests above did not pass, the resulting overall test 
    // will be equal to FAIL.
    if(isFailure)
    {    
         tDataRec[TEST_9].result = FAIL;           
    }

	DiscardPanel(perfPanel1);

	return;
}


/*****************************************************************************/
/* Name:      PressureControlPerformanceTest                                 */
/*                                                                           */
/* Purpose:  Performs the pressure performance test                          */
/*                                                                           */
/* Example Call:   void PressureControlPerformanceTest(void)                 */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void PressureControlPerformanceTest(void)
{
	int control;
	int sstErr;
	int status;
	int perfPanel1;
	int doneButton;
	int pressureIndex;
	int retryIndex;
	int numberOfTries;
	int answer;
	int retry;
	int maxTests;
	int handle;
	int b;
	int panelDone = FALSE;

    int isFailure = FALSE;  // Overall Test Result Flag 
	double reading;
	double lowLimit;
	double highLimit;
	char cBuff[50];
	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	double demoTimeOut;
	#endif
	#endif

	SetSysPanelMessage(LDBf(Ldb_Test9_Running));

	if(InitPerfPanel(&perfPanel1, &doneButton)){
		AppErr(ERR_UIR_LOAD, (double)perfPanel1);
	}

	if(!SST){
		sstErr = DisplaySST();
		if(sstErr){
			PostStringFail(TEST_10, &(tDataRec[TEST_10].outBuffer),         LDB( Ldb_sst_proc_error, ENGLISH), "");
			PostStringFail(TEST_10, &(tDataRec[TEST_10].outBuff_localized), LDBf(Ldb_sst_proc_error),          "");
			tDataRec[TEST_10].result = FAIL;
			return;
		}
		else{
			SST = 1;
		}
	}

	retry         = FALSE;
	retryIndex    = 0;
	numberOfTries = 0;
	// Start of Modification of this statements due to Critical Observation.
	if( sizeof(struct TestTarget) > 0){
		maxTests = sizeof(PressureTestTarget ) / sizeof(struct TestTarget);
	}
	else{
		maxTests = 0;
	} 
	// end of modification.
	
	for(pressureIndex = 0; pressureIndex < maxTests; pressureIndex++){
		CHECK_FOR_ABORT_PERFPANEL
		if(gStoppedInd == 1){
			tDataRec[TEST_10].result = ABORT;
			return;
		}
		
		sprintf(cBuff, LDBf(Ldb_step_x_y), pressureIndex + 1, maxTests);
		SetCtrlVal(perfPanel1, PERF_PANEL_TESTCOUNT, cBuff);

		if(retry != TRUE){
			numberOfTries = 0;
			retryIndex = pressureIndex;
		}
		retry     = FALSE;
		panelDone = FALSE;
		SetPerfPanelPressure(pressureIndex + 1);
		DisplayPanel(perfPanel1);
		for(b = 0; b < 3 ; b++){
			Beep();
		}

		#ifdef DEMOMODE
		#ifdef USE_DEMO_TIMEOUT
		demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
		#endif
		#endif
		while(!panelDone){
			CHECK_FOR_ABORT_PERFPANEL
			if(gStoppedInd == 1){
		   		tDataRec[TEST_10].result = ABORT;
				return;
			}
			
			ProcessSystemEvents();
			GetUserEvent(0, &handle, &control); /* Check for user event */
			#ifdef DEMOMODE
			#ifdef USE_DEMO_TIMEOUT
			if(Timer() >= demoTimeOut){
				handle = perfPanel1;
				control = doneButton;
				Beep();
			}
			#endif
			#endif
			if(handle != perfPanel1){
				continue;
			}

			if(control == PERF_PANEL_ABORT){
				PostStringFail(TEST_10, &(tDataRec[TEST_10].outBuffer),         LDB( Ldb_test_aborted, ENGLISH), "");
				PostStringFail(TEST_10, &(tDataRec[TEST_10].outBuff_localized), LDBf(Ldb_test_aborted),          "");
				tDataRec[TEST_10].result = ABORT;
				DiscardPanel(perfPanel1);
				return;
			}
			if(control == doneButton){
				HidePanel(perfPanel1);
				#ifndef DEMOMODE
				SEND_PTS(MeasurePressureSingleOneSec(&reading, 20))
				#endif
				panelDone = TRUE;
				break;
			}
		} // end while(!panelDone)

		/* check results */
		lowLimit  =  PressureTestTarget[pressureIndex].target * (1 - PressureAccuracy) - PressureOffset;
		highLimit =  PressureTestTarget[pressureIndex].target * (1 + PressureAccuracy) + PressureOffset;
		#ifdef DEMOMODE
		reading = lowLimit + (highLimit - lowLimit) / 2;
		retry = FALSE;
		#endif 
        if (numberOfTries >= MAX_TRIES_ALLOWED)
        {
            retry = FALSE;
            isFailure = TRUE;
            
        }        
		else if(((reading < lowLimit) || (reading > highLimit)) ){
			answer = CPVTConfirmPopup(LDBf(Ldb_Pressure_incorrect), LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP);
			if(answer){   /* yes */
				retry = TRUE;
			}
            else
            {
                isFailure = TRUE;
            }
		}

		if(retry != TRUE){  /* log results */
			sprintf(cBuff, "%s", LDB( PressureTestTarget[pressureIndex].name, ENGLISH));
			status = PostReal(TEST_10, &(tDataRec[TEST_10].outBuffer),         cBuff, lowLimit, reading, highLimit);
			sprintf(cBuff, "%s", LDBf(PressureTestTarget[pressureIndex].name));
			status = PostReal(TEST_10, &(tDataRec[TEST_10].outBuff_localized), cBuff, lowLimit, reading, highLimit);
			if(status){
				tDataRec[TEST_10].result = FAIL;
			}
		}

		if(retry == TRUE){          /* determine retry condition */
			pressureIndex = ( retryIndex - 1);
			numberOfTries++;
		}
	} // end for(pressureIndex = 0; pressureIndex < maxTests; pressureIndex++)

    // If any of the mini tests above did not pass, the resulting overall test 
    // will be equal to FAIL.
    if(isFailure)
    {    
         tDataRec[TEST_10].result = FAIL;           
    }

	DiscardPanel(perfPanel1);

	return;
}


/*****************************************************************************/
/* Name:     PEEPPerformanceTest                                             */
/*                                                                           */
/* Purpose:  performs the peep performance test                              */
/*                                                                           */
/* Example Call:   void PEEPPerformanceTest(void)                            */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void PEEPPerformanceTest(void)
{
	int control;
	int sstErr;
	int answer;
	int maxTests;
	int status;
	int doneButton;
	int perfPanel1;
	int handle;
	int b;
	int panelDone = FALSE;
	int numberOfTries;
	int peepIndex;
	int retryIndex;
	int retry;

	double reading;
	double lowLimit;
	double highLimit;

    int isFailure = FALSE;  // Overall Test Result Flag  

	char cBuff[50];

	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	double demoTimeOut;
	#endif
	#endif

	SetSysPanelMessage(LDBf(Ldb_Test10_Running));

	numberOfTries = 0;
	retryIndex    = 0;

	if(InitPerfPanel(&perfPanel1, &doneButton) ){
		AppErr(ERR_UIR_LOAD, (double)perfPanel1);
	}

	if(!SST){
		sstErr = DisplaySST();
		if(sstErr){
			PostStringFail(TEST_11, &(tDataRec[TEST_11].outBuffer),         LDB( Ldb_sst_proc_error, ENGLISH), "");
			PostStringFail(TEST_11, &(tDataRec[TEST_11].outBuff_localized), LDBf(Ldb_sst_proc_error),          "");
			tDataRec[TEST_11].result = FAIL;
			return;
		}
		else{
			SST = 1;
		}
	}

	retry = FALSE;
	// Start of Modification of this statements due to Critical Observation.
	if(sizeof(struct TestTarget) > 0){
		maxTests = sizeof(PeepTestTarget) / sizeof(struct TestTarget);
	}
	else{
		maxTests = 0;
	} 
	// end of modification.

	//maxTests =  sizeof( PeepTestTarget ) / sizeof(struct TestTarget);
	for(peepIndex = 0; peepIndex < maxTests; peepIndex++){
		CHECK_FOR_ABORT_PERFPANEL
		if(gStoppedInd == 1){
			tDataRec[TEST_11].result = ABORT;
			return;
		}
		sprintf(cBuff, LDBf(Ldb_step_x_y), peepIndex + 1, maxTests);
		SetCtrlVal(perfPanel1, PERF_PANEL_TESTCOUNT, cBuff);
		if(retry != TRUE){
			numberOfTries = 0;
			retryIndex = peepIndex;
		}
		reading   = 0.0;
		retry     = FALSE;
		panelDone = FALSE;
		SetPerfPanelPeep(peepIndex + 1);
		DisplayPanel(perfPanel1);
		for(b = 0; b < 3 ; b++){
			Beep();
		}

		#ifdef DEMOMODE
		#ifdef USE_DEMO_TIMEOUT
		demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
		#endif
		#endif
		while(!panelDone){
			CHECK_FOR_ABORT_PERFPANEL
			if(gStoppedInd == 1){
				tDataRec[TEST_11].result = ABORT;
				return;
			}
			GetUserEvent(0, &handle, &control); /* Check for user event */
			#ifdef DEMOMODE
			#ifdef USE_DEMO_TIMEOUT
			if(Timer() >= demoTimeOut){
				handle = perfPanel1;
				control = doneButton;
				Beep();
			}
			#endif
			#endif
			if(handle != perfPanel1){
				continue;
			}

			if(control == PERF_PANEL_ABORT){
				PostStringFail(TEST_11, &(tDataRec[TEST_11].outBuffer),         LDB( Ldb_test_aborted, ENGLISH), "");
				PostStringFail(TEST_11, &(tDataRec[TEST_11].outBuff_localized), LDBf(Ldb_test_aborted),          "");
				tDataRec[TEST_11].result = ABORT;
				DiscardPanel(perfPanel1);
				return;
			}
			if(control == doneButton){
				HidePanel(perfPanel1);
				#ifndef DEMOMODE
				SEND_PTS(MeasurePressureSingleOneSecAfter(&reading, 20))
				#endif
				panelDone = TRUE;
			}
			ProcessSystemEvents();
		} // end while(!panelDone)

		/* check results */
		lowLimit  =  PeepTestTarget[peepIndex].target * (1 - PeepAccuracy) - PeepOffset;
		highLimit =  PeepTestTarget[peepIndex].target * (1 + PeepAccuracy) + PeepOffset;
		#ifdef DEMOMODE
		reading = lowLimit+(highLimit-lowLimit)/2;
		retry = FALSE;
		#endif 

        if (numberOfTries >= MAX_TRIES_ALLOWED)
        {
            retry = FALSE;
            isFailure = TRUE;
            
        }
		else if(((reading < lowLimit) || (reading > highLimit)) ){
			answer = CPVTConfirmPopup(LDBf(Ldb_peep_incorrect), LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP);
			if(answer){   /* yes */
				retry = TRUE;
			}
            else
            {
                isFailure = TRUE;
            }
		}

		if(retry != TRUE){
			sprintf(cBuff, "%s", LDB( PeepTestTarget[peepIndex].name, ENGLISH));
			status = PostReal(TEST_11, &(tDataRec[TEST_11].outBuffer),         cBuff, lowLimit, reading, highLimit);
			sprintf(cBuff, "%s", LDBf(PeepTestTarget[peepIndex].name));
			status = PostReal(TEST_11, &(tDataRec[TEST_11].outBuff_localized), cBuff, lowLimit, reading, highLimit);
			if(status){
				tDataRec[TEST_11].result = FAIL;
			}
		}

		if(retry == TRUE){          /* determine retry condition */
			peepIndex = (retryIndex - 1);
			numberOfTries++;
		}
	} // end for(peepIndex = 0; peepIndex < maxTests; peepIndex++)

    // If any of the mini tests above did not pass, the resulting overall test 
    // will be equal to FAIL.
    if(isFailure)
    {    
         tDataRec[TEST_11].result = FAIL; 
    }
	DiscardPanel(perfPanel1);

	return;
}


extern int CVICALLBACK O2TimerCB(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2);

/*****************************************************************************/
/* Name:     FIO2PerformanceTest                                             */
/*                                                                           */
/* Purpose:  This function during breath delivery will take triggered        */
/*           oxygen measurements                                             */
/*                                                                           */
/* Example Call:   void FIO2PerformanceTest(void)                            */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void FIO2PerformanceTest(void)
{
	int maxTests;
	int sstErr;
	int oxiIndex;
	int answer;
	int retry;
	int retryFlag;
	int control;
	int perfPanel1;
	int status;
	int doneButton;
	int wlow;
	int whigh;
	int ventOxi;
	int handle;
	int b;
	int panelDone = FALSE;
	double reading;
	double lowLimit;
	double highLimit;
	double tempO2;
	double timeOut;
	char oxiBuff[15];
	char cBuff[50];
    int isFailure = FALSE;  // Overall Test Result Flag  

	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	double demoTimeOut;
	#endif
	#endif

	SetSysPanelMessage(LDBf(Ldb_Test11_Running));

	reading = 0.0;
	if(InitPerfPanel(&perfPanel1, &doneButton)){
		AppErr(ERR_UIR_LOAD, (double)perfPanel1);
	}

	//Set up the O2 reading
	SetCtrlAttribute(perfPanel1, PERF_PANEL_O2PERCENT, ATTR_VISIBLE, TRUE);
	SetCtrlAttribute(perfPanel1, PERF_PANEL_O2READING, ATTR_VISIBLE, TRUE);
	SetCtrlAttribute(perfPanel1, PERF_PANEL_O2TIMER,   ATTR_ENABLED, TRUE);
	O2TimerCB(perfPanel1, PERF_PANEL_O2PERCENT, EVENT_TIMER_TICK, NULL, 0, 0);

	if(!SST){
		sstErr = DisplaySST();
		if(sstErr){
			PostStringFail(TEST_12, &(tDataRec[TEST_12].outBuffer),         LDB( Ldb_sst_proc_error, ENGLISH), "");
			PostStringFail(TEST_12, &(tDataRec[TEST_12].outBuff_localized), LDBf(Ldb_sst_proc_error),          "");
			tDataRec[TEST_12].result = FAIL;
			return;
		}
		else{
			SST = 1;
		}
	}
	retry = 0;
	// Start of Modification of this statements due to Critical Observation.
	if( sizeof(struct TestTarget) > 0){
		maxTests = sizeof(OxiTestTarget) / sizeof(struct TestTarget);
	}
	else{
		maxTests = 0;
	} 
	// end of modification.

	for(oxiIndex = 0; oxiIndex < maxTests; oxiIndex++){
		CHECK_FOR_ABORT_PERFPANEL
		if(gStoppedInd == 1){
			tDataRec[TEST_12].result = ABORT;
			return;
		}
		sprintf(cBuff, LDBf(Ldb_step_x_y), oxiIndex + 1, maxTests);
		SetCtrlVal(perfPanel1, PERF_PANEL_TESTCOUNT, cBuff);

		panelDone = FALSE;
		retryFlag = 0;
		tempO2    = 0.0;
		ventOxi   = 0;

		/* Setup panel */
		SetPerfPanelFIO2(oxiIndex + 1);
		DisplayPanel(perfPanel1);
		for(b = 0; b < 3; b++){
			Beep();
		}

		#ifdef DEMOMODE
		sprintf(cBuff, "%3d", (int)(OxiTestTarget[oxiIndex].target * 100.0));
		SetCtrlVal(perfPanel1, PERF_PANEL_O2READING, cBuff);
		#endif

		/* User response */
		DisplayPanel(perfPanel1);
		#ifdef DEMOMODE
		#ifdef USE_DEMO_TIMEOUT
		demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
		#endif
		#endif
		while(!panelDone){
			CHECK_FOR_ABORT_PERFPANEL
			if(gStoppedInd == 1){
		   		tDataRec[TEST_12].result = ABORT;
				return;
			}
			GetUserEvent(0, &handle, &control); /* Check for user event */
			#ifdef DEMOMODE
			#ifdef USE_DEMO_TIMEOUT
			if(Timer() >= demoTimeOut){
				handle = perfPanel1;
				control = doneButton;
				Beep();
			}
			#endif
			#endif
			ProcessSystemEvents();

			if(handle != perfPanel1){
				continue;
			}

			if(control == PERF_PANEL_ABORT){
				PostStringFail(TEST_12, &(tDataRec[TEST_12].outBuffer),         LDB( Ldb_test_aborted, ENGLISH), "");
				PostStringFail(TEST_12, &(tDataRec[TEST_12].outBuff_localized), LDBf(Ldb_test_aborted),          "");
				tDataRec[TEST_12].result = ABORT;
				DiscardPanel(perfPanel1);
				return;
			}
			if(control == doneButton){
				HidePanel(perfPanel1);
				ProcessSystemEvents();

				oxiBuff[0] = '\0';
				#ifdef DEMOMODE
				if(OxiTestTarget[oxiIndex].target == 0.30){
					sprintf(oxiBuff, "%d", 30);
				}
				else{
					sprintf(oxiBuff, "%d", 90);
				}
				#endif
				CPVTPromptPopup(LDBf(Ldb_oxy_conc), LDBf(Ldb_enter_oxy), OK_CANCEL_DLG, 0, oxiBuff, sizeof(oxiBuff) - 1);

				sscanf(oxiBuff, "%d", &ventOxi);
				timeOut = Timer() + (double)O2_WAIT_TIME;
				#ifndef DEMOMODE
				while(Timer() < timeOut){     /* allow reading to stablize */
					CHECK_FOR_ABORT_PERFPANEL
					if(gStoppedInd == 1){
						tDataRec[TEST_12].result = ABORT;
						return;
					}
				}
				SEND_PTS(MeasureOxygenLocal(&reading))
				#endif
				panelDone = TRUE;
			} // end if(control == doneButton)
		} // end while(!panelDone)

		/* check limits */
		if(OxiTestTarget[oxiIndex].target == 0.30){
			lowLimit  = OxygenAccuracyLow30;
			highLimit = OxygenAccuracyHigh30;
		}
		else{
			lowLimit  = OxygenAccuracyLow90;
			highLimit = OxygenAccuracyHigh90;
		}

		wlow  = lowLimit  * 100;
		whigh = highLimit * 100;

		reading = reading / 100.0;

		if(ventOxi){
			tempO2 = (double)ventOxi / 100.0;    /* change to percent */
		}

		#ifdef DEMOMODE
		reading = lowLimit + (highLimit-lowLimit) / 2;
		retryFlag = FALSE;
		#endif 

		if((reading < lowLimit) || (reading > highLimit) || (ventOxi < wlow) || (ventOxi > whigh) ){
			answer = CPVTConfirmPopup(LDBf(Ldb_oxy_incorrect), LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP);
			if(answer){
				retry++;
				retryFlag = 1;
			
    			if(retry > MAX_TRIES_ALLOWED){
    				retryFlag = 0;  		
                    retry = FALSE;
                    isFailure = TRUE;
                }
            }
            else
            {
                isFailure = TRUE;
            }
		}

		if(!retryFlag){
			retry = 0;
			sprintf(cBuff, "%s", LDB( OxiTestTarget[oxiIndex].name, ENGLISH));
			status = PostReal(TEST_12, &(tDataRec[TEST_12].outBuffer),         cBuff, lowLimit, reading, highLimit);
			sprintf(cBuff, "%s", LDBf(OxiTestTarget[oxiIndex].name));
			status = PostReal(TEST_12, &(tDataRec[TEST_12].outBuff_localized), cBuff, lowLimit, reading, highLimit);
			if(status){
				tDataRec[TEST_12].result = FAIL;
			}

			sprintf(cBuff, LDB( Ldb_vent_oxy_rdg, ENGLISH), OxiTestTarget[oxiIndex].target);
			status = PostReal(TEST_12, &(tDataRec[TEST_12].outBuffer),         cBuff, wlow, ventOxi, whigh);
			sprintf(cBuff, LDBf(Ldb_vent_oxy_rdg), OxiTestTarget[oxiIndex].target);
			status = PostReal(TEST_12, &(tDataRec[TEST_12].outBuff_localized), cBuff, wlow, ventOxi, whigh);
			if(status){
				tDataRec[TEST_12].result = FAIL;
			}
		}
		else{
			oxiIndex--;
		}
	} // end for(oxiIndex = 0; oxiIndex < maxTests; oxiIndex++)

	DiscardPanel(perfPanel1);

    // If any of the mini tests above did not pass, the resulting overall test 
    // will be equal to FAIL.
    if(isFailure)
    {    
         tDataRec[TEST_12].result = FAIL;           
    }


	return;
}


/*****************************************************************************/
/* Name:     ProcessManualTestOptional                                       */
/*                                                                           */
/* Purpose:  Processes the Optional portion of the manual test panel         */
/*                                                                           */
/* Example Call:   int ProcessManualTestOptional(int)                        */
/*                                                                           */
/* Input:    panel - panel handle of manual test panel                       */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   overall pass or fail for this portion of the test               */
/*                                                                           */
/*****************************************************************************/
int ProcessManualTestOptional(int panel)
{
	int overallpass = PASS;
	char cBuff[ 256];
	char cBuff2[256];
	char valbuffer[20];

	// Work Order number
	GetCtrlVal(panel, MANUALPNL_WORKORDER, valbuffer);
	if(strlen(valbuffer) != 0){
		sprintf(cBuff,  LDB( Ldb_mnl_wrkordr_r, ENGLISH), valbuffer);
		sprintf(cBuff2, LDBf(Ldb_mnl_wrkordr_r),          valbuffer);
		sprintf(gWorkOrderNumber, valbuffer);
	}
	else{
		sprintf(cBuff,  LDB( Ldb_mnl_wrkordr_r_nd, ENGLISH));
		sprintf(cBuff2, LDBf(Ldb_mnl_wrkordr_r_nd));
		*gWorkOrderNumber = '\0';
	}
	PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         cBuff);
	PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), cBuff2);

	// Vent Asset number
	GetCtrlVal(panel, MANUALPNL_VENTASSET, valbuffer);
	if(strlen(valbuffer) != 0){
		sprintf(cBuff,  LDB( Ldb_mnl_vent_r, ENGLISH), valbuffer);
		sprintf(cBuff2, LDBf(Ldb_mnl_vent_r),          valbuffer);
		sprintf(gVentAssetNumber, valbuffer);
	}
	else{
		sprintf(cBuff,  LDB( Ldb_mnl_vent_r_nd, ENGLISH));
		sprintf(cBuff2, LDBf(Ldb_mnl_vent_r_nd));
		*gVentAssetNumber = '\0';
	}

	PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         cBuff);
	PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), cBuff2);

	return overallpass;
}


/*****************************************************************************/
/* Name:     ProcessManualTestElectrical                                     */
/*                                                                           */
/* Purpose:  Processes the electrical portion of the manual test panel       */
/*                                                                           */
/* Example Call:   int ProcessManualTestElectrical(int )                     */
/*                                                                           */
/* Input:    panel - panel handle of manual test panel                       */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   overall pass or fail for this portion of the test               */
/*                                                                           */
/*****************************************************************************/
int ProcessManualTestElectrical(int panel)
{
	double resistance;
	double fcurrent;
	double rcurrent;
	char   cBuff[ 256];
	char   cBuff2[256];
	int    voltage;
	int    funits;
	int    runits;
	int    overallpass;

	overallpass = PASS;

	// Ground Resistance Test
	GetCtrlVal(panel, MANUALPNL_GROUNDRES, cBuff);
	if(strlen(cBuff) != 0){
		UnLocalizeNumberInString(cBuff);
		resistance = atof(cBuff);
		sprintf(cBuff,  LDB( Ldb_mnl_gnd_r, ENGLISH), resistance);
		sprintf(cBuff2, LDBf(Ldb_mnl_gnd_r),          resistance);
	}
	else{
		sprintf(cBuff,  LDB( Ldb_mnl_gnd_r_nd, ENGLISH));
		sprintf(cBuff2, LDBf(Ldb_mnl_gnd_r_nd));
	}

	PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         cBuff);
	PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), cBuff2);

	// Get voltage setting
	GetCtrlVal(panel, MANUALPNL_VOLTAGE, &voltage);
	
	// Get Forward current
	GetCtrlVal(panel, MANUALPNL_FRWDLEAK, cBuff);
	if(strlen(cBuff) != 0){
		UnLocalizeNumberInString(cBuff);
		fcurrent = atof(cBuff);
		if(voltage == 0){ // 100-120
			sprintf(cBuff,  LDB( Ldb_mnl_f_leakage1, ENGLISH), fcurrent);
			sprintf(cBuff2, LDBf(Ldb_mnl_f_leakage1),          fcurrent);
		}
		else if(voltage==1){ // 220-240
			sprintf(cBuff,  LDB( Ldb_mnl_f_leakage2, ENGLISH), fcurrent);
			sprintf(cBuff2, LDBf(Ldb_mnl_f_leakage2),          fcurrent);
		}
	}
	else{
		if(voltage == 0){ // 100-120
			sprintf(cBuff,  LDB( Ldb_mnl_f_leakage1_nd, ENGLISH));
			sprintf(cBuff2, LDBf(Ldb_mnl_f_leakage1_nd));
		}
		else if(voltage == 1){ // 220-240
			sprintf(cBuff,  LDB( Ldb_mnl_f_leakage2_nd, ENGLISH));
			sprintf(cBuff2, LDBf(Ldb_mnl_f_leakage2_nd));
		}
	}
	PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         cBuff);
	PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), cBuff2);

	// Get Reverse current
	GetCtrlVal(panel, MANUALPNL_REVLEAK, cBuff);
	if(strlen(cBuff) != 0){
		UnLocalizeNumberInString(cBuff);
		rcurrent = atof(cBuff);
		if(voltage == 0){ // 100-120
			sprintf(cBuff,  LDB( Ldb_mnl_r_leakage1, ENGLISH), rcurrent);
			sprintf(cBuff2, LDBf(Ldb_mnl_r_leakage1),          rcurrent);
		}
		else if(voltage == 1){ // 220-240
			sprintf(cBuff,  LDB( Ldb_mnl_r_leakage2, ENGLISH), rcurrent);
			sprintf(cBuff2, LDBf(Ldb_mnl_r_leakage2),          rcurrent);
		}
	}
	else{
		if(voltage == 0){ // 100-120
			sprintf(cBuff,  LDB( Ldb_mnl_r_leakage1_nd, ENGLISH));
			sprintf(cBuff2, LDBf(Ldb_mnl_r_leakage1_nd));
		}
		else if(voltage == 1){ // 220-240
			sprintf(cBuff,  LDB( Ldb_mnl_r_leakage2_nd, ENGLISH));
			sprintf(cBuff2, LDBf(Ldb_mnl_r_leakage2_nd));
		}
	}
	PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         cBuff);
	PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), cBuff2);
	
	return overallpass;
}


/*****************************************************************************/
/* Name:     ProcessManualTestGround                                         */
/*                                                                           */
/* Purpose:  Processes the ground resistance portion of the manual test panel*/
/*                                                                           */
/* Example Call:   int ProcessManualTestGround(int )                         */
/*                                                                           */
/* Input:    panel - panel handle of manual test panel                       */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   overall pass or fail for this portion of the test               */
/*                                                                           */
/*****************************************************************************/
int ProcessManualTestGround(int panel)
{
	double resistance;
	char   cBuff[ 256];
	char   cBuff2[256];
	int    overallpass;

	overallpass = PASS;

	// Ground Isolation Resistance Test
	GetCtrlVal(panel, MANUALPNL_GRNDISOL, cBuff);
	if(strlen(cBuff) != 0){
		UnLocalizeNumberInString(cBuff);
		resistance = atof(cBuff);
		sprintf(cBuff,  LDB( Ldb_mnl_g_isol_r, ENGLISH), resistance);
		sprintf(cBuff2, LDBf(Ldb_mnl_g_isol_r),          resistance);
	}
	else{
		sprintf(cBuff,  LDB( Ldb_mnl_g_isol_r_nd, ENGLISH));
		sprintf(cBuff2, LDBf(Ldb_mnl_g_isol_r_nd));
	}

	PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         cBuff);
	PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), cBuff2);
	return overallpass;
}


/*****************************************************************************/
/* Name:     ProcessManualTestRegulator                                      */
/*                                                                           */
/* Purpose:  Processes the regulator portion of the manual test panel        */
/*                                                                           */
/* Example Call:   int ProcessManualTestRegulator(int)                       */
/*                                                                           */
/* Input:    panel - panel handle of manual test panel                       */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   overall pass or fail for this portion of the test               */
/*                                                                           */
/*****************************************************************************/
int ProcessManualTestRegulator(int panel)
{
	double AirSetting;
	double O2Setting;
	char cBuff[ 256];
	char cBuff2[256];
	int overallpass;

	overallpass = PASS;

	// Regulator Settings Test
	GetCtrlVal(panel, MANUALPNL_AIRREG, cBuff);
	if(strlen(cBuff) != 0){
		UnLocalizeNumberInString(cBuff);
		AirSetting = atof(cBuff);
		sprintf(cBuff,  LDB( Ldb_mnl_air_reg, ENGLISH),  AirSetting);
		sprintf(cBuff2, LDBf(Ldb_mnl_air_reg),           AirSetting);
	}
	else{
		sprintf(cBuff,  LDB( Ldb_mnl_air_reg_nd, ENGLISH));
		sprintf(cBuff2, LDBf(Ldb_mnl_air_reg_nd));
	}

	PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         cBuff);
	PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), cBuff2);

	GetCtrlVal(panel, MANUALPNL_O2REG, cBuff);
	if(strlen(cBuff) != 0){
		UnLocalizeNumberInString(cBuff);
		O2Setting = atof(cBuff);
		sprintf(cBuff,  LDB( Ldb_mnl_O2_reg, ENGLISH), O2Setting);
		sprintf(cBuff2, LDBf(Ldb_mnl_O2_reg),          O2Setting);
	}
	else{
		sprintf(cBuff,  LDB( Ldb_mnl_O2_reg_nd, ENGLISH));
		sprintf(cBuff2, LDBf(Ldb_mnl_O2_reg_nd));
	}

	PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         cBuff);
	PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), cBuff2);

	return overallpass;
}


/*****************************************************************************/
/* Name:     ProcessManualTestEST_SST                                        */
/*                                                                           */
/* Purpose:  Processes the EST/SST portion of the manual test panel          */
/*                                                                           */
/* Example Call:   int ProcessManualTestEST_SST(int)                         */
/*                                                                           */
/* Input:    panel - panel handle of manual test panel                       */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   overall pass or fail for this portion of the test               */
/*                                                                           */
/*****************************************************************************/
int ProcessManualTestEST_SST(int panel)
{
	int SSTpass;
	int ESTpass;
	int overallpass;

	overallpass = PASS;

	GetCtrlVal(panel, MANUALPNL_SST_PF, &SSTpass);
	if(SSTpass == 2){
		PostStringPass(TEST_1, &(tDataRec[TEST_1].outBuffer),         LDB( Ldb_mnl_sst, ENGLISH), "");
		PostStringPass(TEST_1, &(tDataRec[TEST_1].outBuff_localized), LDBf(Ldb_mnl_sst),          "");
	}
	else if(SSTpass == 1){
		PostStringFail(TEST_1, &(tDataRec[TEST_1].outBuffer),         LDB( Ldb_mnl_sst, ENGLISH), "");
		PostStringFail(TEST_1, &(tDataRec[TEST_1].outBuff_localized), LDBf(Ldb_mnl_sst),          "");
	}
	else if(SSTpass == 0){
		PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         LDB( Ldb_mnl_sst_nd, ENGLISH));
		PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), LDBf(Ldb_mnl_sst_nd));
	}
	
	GetCtrlVal(panel, MANUALPNL_EST_PF, &ESTpass);
	if(ESTpass == 2){
		PostStringPass(TEST_1, &(tDataRec[TEST_1].outBuffer),         LDB( Ldb_mnl_est, ENGLISH), "");
		PostStringPass(TEST_1, &(tDataRec[TEST_1].outBuff_localized), LDBf(Ldb_mnl_est),          "");
	}
	else if(ESTpass == 1){
		PostStringFail(TEST_1, &(tDataRec[TEST_1].outBuffer),         LDB( Ldb_mnl_est, ENGLISH), "");
		PostStringFail(TEST_1, &(tDataRec[TEST_1].outBuff_localized), LDBf(Ldb_mnl_est),          "");
	}
	else if(ESTpass == 0){
		PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         LDB( Ldb_mnl_est_nd, ENGLISH));
		PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), LDBf(Ldb_mnl_est_nd));
	}

	return overallpass;
}


/*****************************************************************************/
/* Name:     ProcessManualTestOther                                          */
/*                                                                           */
/* Purpose:  Processes the Other Tests portion of the manual test panel      */
/*                                                                           */
/* Example Call:   int ProcessManualTestOther(panel)                         */
/*                                                                           */
/* Input:    int panel - panel handle of manual test panel                   */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   overall pass or fail for this portion of the test               */
/*                                                                           */
/*****************************************************************************/
int ProcessManualTestOther(int panel)
{
	int SIO_Loopback_verified;
	int overallpass;

	overallpass = PASS;

	GetCtrlVal(panel, MANUALPNL_SIO_LOOPBACK, &SIO_Loopback_verified);
	if(SIO_Loopback_verified == 1){
		PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         LDB( Ldb_mnl_SIO, ENGLISH));
		PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), LDBf(Ldb_mnl_SIO));
	}
	else if(SIO_Loopback_verified == 0){
		PostStringNoMsg(&(tDataRec[TEST_1].outBuffer),         LDB( Ldb_mnl_SIO_nd, ENGLISH));
		PostStringNoMsg(&(tDataRec[TEST_1].outBuff_localized), LDBf(Ldb_mnl_SIO_nd));
	}

	return overallpass;
}


/*****************************************************************************/
/* Name:     ProcessManualTestPanel                                          */
/*                                                                           */
/* Purpose:  Processes the values entered to the manual test panel           */
/*                                                                           */
/* Example Call:   int ProcessManualTestPanel(int)                           */
/*                                                                           */
/* Input:    panel - panel handle of manual test panel                       */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   overall pass or fail for this test                              */
/*                                                                           */
/*****************************************************************************/
int ProcessManualTestPanel(int panel)
{
	int overallpass = PASS;

	if(ProcessManualTestOptional(  panel) == FAIL){
		overallpass = FAIL;
	}

	if(ProcessManualTestElectrical(panel) == FAIL){
		overallpass = FAIL;
	}

	if(ProcessManualTestGround(    panel) == FAIL){
		overallpass = FAIL;
	}

	if(ProcessManualTestRegulator( panel) == FAIL){
		overallpass = FAIL;
	}

	if(ProcessManualTestEST_SST(   panel) == FAIL){
		overallpass = FAIL;
	}

	if(ProcessManualTestOther(     panel) == FAIL){
		overallpass = FAIL;
	}

	return overallpass;
}


/*****************************************************************************/
/* Name:     ManualPerformanceTest                                           */
/*                                                                           */
/* Purpose:  This function displays a panel to enter manual test information */
/*                                                                           */
/* Example Call:   void ManualPerformanceTest(void);                         */
/*                                                                           */
/* Input:    <None>                                                          */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void ManualPerformanceTest(void)
{
	int ManualPanel;
	int EventPanel;
	int EventControl;
	int retries;

	gPanelHandle[MANUALPNL] = LoadPanel(0, LDBf(Ldb_MainpanelFile), MANUALPNL);
	ManualPanel = gPanelHandle[MANUALPNL];
	PauseTips();
	SwitchPanelsWithConst(ManualPanel, MANUALPNL);

	/* limit input to numbers */
	SetCtrlAttribute(ManualPanel, MANUALPNL_GROUNDRES, ATTR_CALLBACK_FUNCTION_POINTER, NumberFilterCallback);
	SetCtrlAttribute(ManualPanel, MANUALPNL_FRWDLEAK,  ATTR_CALLBACK_FUNCTION_POINTER, NumberFilterCallback);
	SetCtrlAttribute(ManualPanel, MANUALPNL_REVLEAK,   ATTR_CALLBACK_FUNCTION_POINTER, NumberFilterCallback);
	SetCtrlAttribute(ManualPanel, MANUALPNL_GRNDISOL,  ATTR_CALLBACK_FUNCTION_POINTER, NumberFilterCallback);
	SetCtrlAttribute(ManualPanel, MANUALPNL_AIRREG,    ATTR_CALLBACK_FUNCTION_POINTER, NumberFilterCallback);
	SetCtrlAttribute(ManualPanel, MANUALPNL_O2REG,     ATTR_CALLBACK_FUNCTION_POINTER, NumberFilterCallback);

	SetCtrlAttribute(ManualPanel, MANUALPNL_OK,        ATTR_CMD_BUTTON_COLOR,          VAL_TRANSPARENT);
	SetPanelAttribute(ManualPanel, ATTR_CLOSE_CTRL, MANUALPNL_OK);

	InstallPopup(ManualPanel);

	SetSysPanelMessage(LDBf(Ldb_Test12_Running));
	retries = 0;
	while(1){
		ProcessSystemEvents();
		GetUserEvent(0, &EventPanel, &EventControl);
		if(ManualEnterWasPressed == 1){
			ManualEnterWasPressed = 0;
			EventPanel = ManualPanel;
			EventControl = MANUALPNL_OK;
		}
		if(EventPanel == ManualPanel){
			switch(EventControl){
				case (MANUALPNL_OK):
					tDataRec[TEST_1].result = ProcessManualTestPanel(ManualPanel);
					SwitchPanels(gPanelHandle[TESTPANEL]);
					UnPauseTips();
					RemovePopup(ManualPanel);
					DiscardPanel(ManualPanel);
					gPanelHandle[MANUALPNL] = 0;
					return;
			}
		}
	}
}


/*****************************************************************************/
/* Name:     ManualPnlCB                                                     */
/*                                                                           */
/* Purpose:  handles the panel exit as a test abort                          */
/*                                                                           */
/* Example Call:                                                             */
/*           int CVICALLBACK ManualPnlCB (int panel, int event,              */
/*                                        void *callbackData,                */
/*                                        int eventData1, int eventData2);   */
/*                                                                           */
/* Input:    Standard PANEL callback arguments                               */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   always 0                                                        */
/*                                                                           */
/*****************************************************************************/
int CVICALLBACK ManualPnlCB(int panel, int event, void *callbackData, 
                            int eventData1, int eventData2)
{
	switch(event){
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_ENTER_VKEY:
					ManualEnterWasPressed = 1;
					break;
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		}

	return 0;
}


/*****************************************************************************/
/* Name:     VerifyVolume                                                    */
/*                                                                           */
/* Purpose:  This function will test and log volume readings                 */
/*                                                                           */
/*                                                                           */
/* Example Call:   VerifyVolume(double,double,int,int )                      */
/*                                                                           */
/* Input:    <int>                                                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*       Volume                                                              */
/*                                                                           */
/* Return: 0 = pass, 1 = failed limits                                       */
/*                                                                           */
/*****************************************************************************/
int VerifyVolume(double btps, double spiro, int currentVolumeTest, int log)
{
	double low;
	double high;
	double spiroLow;
	double spiroHigh;
	double o2Reading;

	int result = 1;
	int status;
	int o2LevelFault = 0;        /* O2 Concentration Level Flag */
	char cBuff[ 60];
	char cBuff2[60];

	o2Reading = 0.0;
	if(Volume[currentVolumeTest].gas == OXYGEN){
		// TBD - this returns a failure, but the caller will think its out of 
		// spec, not a com failure. Need a new return code to specify com failed
		// to caller.
		#ifndef DEMOMODE
		if(MeasureOxygenLocal(&o2Reading) != 0){
			return 1;
		}

		if(o2Reading < O2_Level_Volume){
			o2LevelFault = 1;
			CPVTMessagePopup(LDBf(Ldb_Test_Failure), LDBf(Ldb_oxy_conc_fail), OK_ONLY_DLG, NO_HELP);
		}
		#else
		o2Reading = 97.5;
		#endif
	}

	if(Volume[currentVolumeTest].target <= 25){
		low  = Volume[currentVolumeTest].target * (1 - Volume25Accuracy) - VolumeOffset;
		high = Volume[currentVolumeTest].target * (1 + Volume25Accuracy) + VolumeOffset;
		spiroLow  = spiro * ( 1 - .30) - 10;
		spiroHigh = spiro * ( 1 + .30) + 10;
	}
	else{
		low  = Volume[currentVolumeTest].target * (1 - VolumeAccuracy ) - VolumeOffset;
		high = Volume[currentVolumeTest].target * (1 + VolumeAccuracy ) + VolumeOffset;
		spiroLow  = spiro * (1 - .10) - 10;
		spiroHigh = spiro * (1 + .10) + 10;
	}

	if((btps >= low) && (btps <= high) && (!o2LevelFault)){   /* if btps or spiro out of spec return 1 */
		result = 0;
	}

	if(!result){
		if( (btps < spiroLow) || (btps > spiroHigh)){
			result = 1;
		}
	}

	if(log){
		sprintf(cBuff,  LDB( Ldb_pts_rdg, ENGLISH), btps);
		sprintf(cBuff2, LDBf(Ldb_pts_rdg),          btps);
		status = PostReal(TEST_9, &(tDataRec[TEST_9].outBuffer),         cBuff,  low, btps, high);
		status = PostReal(TEST_9, &(tDataRec[TEST_9].outBuff_localized), cBuff2, low, btps, high);
		if(status){
			tDataRec[TEST_9].result = FAIL;
		}

		sprintf(cBuff,  LDB( Ldb_sprio_rdg, ENGLISH), Volume[currentVolumeTest].target);
		sprintf(cBuff2, LDBf(Ldb_sprio_rdg),          Volume[currentVolumeTest].target);
		status = PostReal(TEST_9, &(tDataRec[TEST_9].outBuffer),         cBuff,  spiroLow, btps, spiroHigh);
		status = PostReal(TEST_9, &(tDataRec[TEST_9].outBuff_localized), cBuff2, spiroLow, btps, spiroHigh);
		if(status){
			tDataRec[TEST_9].result = FAIL;
		}

		if(Volume[currentVolumeTest].gas == OXYGEN){
			status = PostReal(TEST_9, &(tDataRec[TEST_9].outBuffer),         LDB( Ldb_O2_conc_vol, ENGLISH), O2_Level_Volume, o2Reading, 100.0);
			status = PostReal(TEST_9, &(tDataRec[TEST_9].outBuff_localized), LDBf(Ldb_O2_conc_vol),          O2_Level_Volume, o2Reading, 100.0);
			if(status){
				tDataRec[TEST_9].result = FAIL;
			}
		}
	}

	return(result);
}


/*****************************************************************************/
/* Name:     SetPerfPanelFIO2                                                */
/*                                                                           */
/* Purpose:  This function is display the appropriate instructions to the    */
/*           operator.                                                       */
/*                                                                           */
/* Example Call:   SetPerfPanelFIO2(int )                                    */
/*                                                                           */
/* Input:    <int>                                                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*        <none>                                                             */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void SetPerfPanelFIO2(int whichOne)
{
	char inst[1024]; // note: must be large enough for longest message
	char title[100]; // note: must be large enough for longest message

	sprintf(title, "%s", LDBf(Ldb_fio2_inst));

	SetManTypeDisplay(        D_ON, VC);
	SetBreathPerMinuteDisplay(D_ON, 100.0);
	SetIdealBodyWeightDisplay(D_ON, 20);
	SetTriggerTypeDisplay(    D_ON, PTRIG);
	SetTplDisplay(            D_ON, 0.0);
	SetWaveFormDisplay(       D_ON, SQUARE);
	SetMaxFlowDisplay(        D_ON, 50.0);
	SetPEEPDisplay(           D_ON, 0.0);
	SetVolumeDisplay(         D_ON, 250);
	SetPsensDisplay(          D_ON, 5.0);

	/* Note: ResetFlashOn() will terminate all prior flashing prompts     */
	/*       and add one prompt. FlashOn will add to Flashing Prompt List */

	switch(whichOne){
		case 1:
			SetO2Display(D_ON, 30);
			ResetFlashOn(F_O2);
			FlashOn(F_PSENS);
			FlashOn(F_PEEP);
			FlashOn(F_IBW);
			FlashOn(F_BREATH);
			FlashOn(F_VOLUME);
			FlashOn(F_FLOW);
			sprintf(inst, LDBf(Ldb_9step),
			        LDBf(Ldb_VentOffandOn),
			        LDBf(Ldb_IBW_20kg),
			        LDBf(Ldb_Mode2AC),
			        LDBf(Ldb_ManType2VC),
			        LDBf(Ldb_Trig2PTRIG),
			        LDBf(Ldb_MakeOtherSettings),
			        LDBf(Ldb_DisableAlarms),
			        LDBf(Ldb_EnsurehalfLLung),
			        LDBf(Ldb_Givefiveminutes));
			SetInstructionBox(D_ON, title, inst);
			break;

		case 2:
			ResetFlashOn(F_O2);
			SetO2Display(D_ON, 90);
			sprintf(inst, LDBf(Ldb_2step),
			        LDBf(Ldb_MakeSettings),
			        LDBf(Ldb_Givefiveminutes));
			SetInstructionBox(D_ON, title, inst);
			break;
	}

	return;
}


/*****************************************************************************/
/* Name:     SetPerfPanelPeep                                                */
/*                                                                           */
/* Purpose:  This function is display the appropriate instructions to the    */
/*           operator.                                                       */
/*                                                                           */
/* Example Call:   SetPerfPanelpEEP(int)                                     */
/*                                                                           */
/* Input:    <int>                                                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           None                                                            */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void SetPerfPanelPeep(int whichOne)
{
	char inst[1024]; // note: must be large enough for longest message
	char title[100]; // note: must be large enough for longest message

	sprintf(title, "%s", LDBf(Ldb_peep_inst));

	SetBreathPerMinuteDisplay(D_ON, 10.0);
	SetIdealBodyWeightDisplay(D_ON, 85);
	SetManTypeDisplay(        D_ON, PC);
	SetO2Display(             D_ON, 21);
	SetTiDisplay(             D_ON, 2.0);
	SetPiDisplay(             D_ON, 10.0);
	SetPsensDisplay(          D_ON, 5.0);
	SetTriggerTypeDisplay(    D_ON, PTRIG);

	/* Note: ResetFlashOn() will terminate all prior flashing prompts     */
	/*       and add one prompt. FlashOn will add to Flashing Prompt List */

	switch(whichOne){
		case 1:
			SetPEEPDisplay(D_ON, 5.0);
			ResetFlashOn(F_PI);
			FlashOn(F_PEEP);
			if(gSequenceRunning){
				sprintf(inst, LDBf(Ldb_2step),
				        LDBf(Ldb_MakeSettings),
				        LDBf(Ldb_Givefivebreaths));
			}
			else{
				sprintf(inst, LDBf(Ldb_9step),
				        LDBf(Ldb_VentOffandOn),
				        LDBf(Ldb_IBW_85kg),
				        LDBf(Ldb_Mode2AC),
				        LDBf(Ldb_ManType2PC),
				        LDBf(Ldb_Trig2PTRIG),
				        LDBf(Ldb_MakeOtherSettings),
				        LDBf(Ldb_DisableAlarms),
				        LDBf(Ldb_Ensure4LLung),
				        LDBf(Ldb_Givefivebreaths));
			}
			SetInstructionBox(D_ON, title, inst);
			break;

		case 2:
			SetPEEPDisplay(D_ON, 25.0);
			ResetFlashOn(F_PEEP);
			sprintf(inst, LDBf(Ldb_2step),
			        LDBf(Ldb_MakeSettings),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;

		case 3:
			SetPEEPDisplay(D_ON, 45.0);
			ResetFlashOn(F_PEEP);
			sprintf(inst, LDBf(Ldb_2step),
			        LDBf(Ldb_MakeSettings),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;
	} // end switch(whichOne)

	return;
}


/*****************************************************************************/
/* Name:     SetPerfPanelPressure                                            */
/*                                                                           */
/* Purpose:  This function is display the appropriate instructions to the    */
/*           operator.                                                       */
/*                                                                           */
/* Example Call:   SetPerfPanelPressure(int)                                 */
/*                                                                           */
/* Input:    <int>                                                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           None                                                            */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void SetPerfPanelPressure(int whichOne)
{
	char inst[1024]; // note: must be large enough for longest message
	char title[100]; // note: must be large enough for longest message

	sprintf(title, "%s", LDBf(Ldb_pres_inst));

	SetManTypeDisplay(        D_ON, PC);
	SetIdealBodyWeightDisplay(D_ON, 85);
	SetBreathPerMinuteDisplay(D_ON, 10.0);
	SetTriggerTypeDisplay(    D_ON, PTRIG);
	SetO2Display(             D_ON, 21);
	SetPEEPDisplay(           D_ON, 0.0);
	SetPsensDisplay(          D_ON, 5.0);
	SetTiDisplay(             D_ON, 2.0);
	SetPDisplay(              D_ON, 50);

	/* Note: ResetFlashOn() will terminate all prior flashing prompts     */
	/*       and add one prompt. FlashOn will add to Flashing Prompt List */

	switch(whichOne){
		case 1:
			SetPiDisplay(D_ON, 5.0);
			ResetFlashOn(F_PI);
			FlashOn(F_TI);
			FlashOn(F_MANTYPE);
			if(gSequenceRunning){
				sprintf(inst, LDBf(Ldb_4step),
				        LDBf(Ldb_ManType2PC),
				        LDBf(Ldb_MakeSettings),
				        LDBf(Ldb_Ensure4LLung),
				        LDBf(Ldb_Givefivebreaths));
			}
			else{
				sprintf(inst, LDBf(Ldb_9step),
				        LDBf(Ldb_VentOffandOn),
				        LDBf(Ldb_IBW_85kg),
				        LDBf(Ldb_Mode2AC),
				        LDBf(Ldb_ManType2PC),
				        LDBf(Ldb_Trig2PTRIG),
				        LDBf(Ldb_MakeOtherSettings),
				        LDBf(Ldb_DisableAlarms),
				        LDBf(Ldb_Ensure4LLung),
				        LDBf(Ldb_Givefivebreaths));
			}
			SetInstructionBox(D_ON, title, inst);
			break;

		case 2:
			ResetFlashOn(F_PI);
			SetPiDisplay(D_ON, 90.0);
			sprintf(inst, LDBf(Ldb_2step),
			        LDBf(Ldb_MakeSettings),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;
	} // end switch(whichOne)

	return;
}


/*****************************************************************************/
/* Name:     SetPerfPanelVolume                                              */
/*                                                                           */
/* Purpose:  This function is display the appropriate instructions to the    */
/*           operator.                                                       */
/*                                                                           */
/* Example Call:   SetPerfPanelVolume(int)                                   */
/*                                                                           */
/* Input:    <int>                                                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           None                                                            */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void SetPerfPanelVolume(int whichOne)
{
	char inst[1024]; // note: must be large enough for longest message
	char title[100]; // note: must be large enough for longest message

	sprintf(title, "%s", LDBf(Ldb_vol_inst));

	SetManTypeDisplay(    D_ON, VC);
	SetTriggerTypeDisplay(D_ON, PTRIG);
	SetWaveFormDisplay(   D_ON, SQUARE);
	SetPsensDisplay(      D_ON, 5.0);
	SetTplDisplay(        D_ON, 1.0) ;
	SetPEEPDisplay(       D_ON, 0.0);

	/* Note: ResetFlashOn() will terminate all prior flashing prompts     */
	/*       and add one prompt. FlashOn will add to Flashing Prompt List */

	switch(whichOne){
		case 1:
			SetVolumeDisplay(         D_ON, 25);
			SetIdealBodyWeightDisplay(D_ON, 10);
			SetBreathPerMinuteDisplay(D_ON, 15.0);
			SetMaxFlowDisplay(        D_ON, 7.5);
			// Initial oxygen setting shall not match test specifications 
			// so as to be consistent with manufacturing test software.  By order of Project Manager
			// with agreement by manufacturing test engineering in peer review 09/12/2000.
			//SetO2Display(             D_ON, 21);
			SetO2Display(             D_ON, 100);
			sprintf(inst, LDBf(Ldb_9step),
			        LDBf(Ldb_VentOffandOn),
			        LDBf(Ldb_IBW_10kg),
			        LDBf(Ldb_Mode2AC),
			        LDBf(Ldb_ManType2VC),
			        LDBf(Ldb_Trig2PTRIG),
			        LDBf(Ldb_MakeOtherSettings),
			        LDBf(Ldb_DisableAlarms),
			        LDBf(Ldb_EnsurehalfLLung),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;

		case 2:
			// Step two Trigger Type setting shall not match test specifications 
			// so as to be consistent with manufacturing test software.  By order of Project Manager
			// with agreement by manufacturing test engineering in peer review 09/12/2000.
			//SetTriggerTypeDisplay(D_ON, VTRIG);
			//ResetFlashOn(F_TRIGGER);
			//FlashOn(     F_PSENS);
			//sprintf(inst, LDBf(Ldb_3step),
			//        LDBf(Ldb_Trig2VTRIG),
			//        LDBf(Ldb_MakeOtherSettings),
			//        LDBf(Ldb_Givefivebreaths));
			SetTriggerTypeDisplay(D_ON, PTRIG);
			SetO2Display(         D_ON, 21); // see comment above, in case 1, now change to oxygen 21%
			ResetFlashOn(F_O2);              // see comment above, in case 1
			sprintf(inst, LDBf(Ldb_2step),
			        LDBf(Ldb_MakeSettings),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;

		case 3:
			// Step three Trigger Type and oxygen settings shall not match test specifications 
			// so as to be consistent with manufacturing test software.  By order of Project Manager
			// with agreement by manufacturing test engineering in peer review 09/12/2000.
			//SetTriggerTypeDisplay(D_ON, VTRIG);
			//SetO2Display(         D_ON, 100);
			//ResetFlashOn(F_O2);
			//sprintf(inst, LDBf(Ldb_2step),
			//        LDBf(Ldb_MakeSettings),
			//        LDBf(Ldb_Givefivebreaths));
			//SetInstructionBox(D_ON, title, inst);
			SetTriggerTypeDisplay(D_ON, VTRIG);
			ResetFlashOn(F_TRIGGER); // 
			FlashOn(     F_PSENS);
			sprintf(inst, LDBf(Ldb_3step),
			        LDBf(Ldb_Trig2VTRIG),
			        LDBf(Ldb_MakeSettings),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;

		case 4:
			SetVolumeDisplay(     D_ON, 200);
			//SetO2Display(         D_ON, 21); // Oxygen already at 21, see comment above in case 2
			SetTriggerTypeDisplay(D_ON, VTRIG);
			ResetFlashOn(F_VOLUME);
			//FlashOn(     F_O2);              // Oxygen already at 21, see comment above in case 2
			sprintf(inst, LDBf(Ldb_2step),
			        LDBf(Ldb_MakeSettings),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;

		case 5:
			SetTriggerTypeDisplay(D_ON, PTRIG);
			ResetFlashOn(F_TRIGGER);
			sprintf(inst, LDBf(Ldb_2step),
			        LDBf(Ldb_Trig2PTRIG),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;

		case 6:
			SetVolumeDisplay(         D_ON, 600);
			SetIdealBodyWeightDisplay(D_ON, 85);
			SetBreathPerMinuteDisplay(D_ON, 10.0);
			SetMaxFlowDisplay(        D_ON, 45.0);
			ResetFlashOn(F_VOLUME);
			FlashOn(     F_IBW);
			FlashOn(     F_BREATH);
			FlashOn(     F_FLOW);
			sprintf(inst, LDBf(Ldb_6step),
			        LDBf(Ldb_VentOffandOn),
			        LDBf(Ldb_IBW_85kg),
			        LDBf(Ldb_MakeOtherSettings),
			        LDBf(Ldb_DisableAlarms),
			        LDBf(Ldb_Ensure3LLung),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;

		case 7:
			SetTriggerTypeDisplay(D_ON, VTRIG);
			ResetFlashOn(F_TRIGGER);
			sprintf(inst, LDBf(Ldb_2step),
			        LDBf(Ldb_Trig2VTRIG),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;

		case 8:
			SetVolumeDisplay(     D_ON, 2500);
			SetTriggerTypeDisplay(D_ON, VTRIG);
			SetMaxFlowDisplay(    D_ON, 120.0);
			ResetFlashOn(F_VOLUME);
			FlashOn(     F_FLOW);
			sprintf(inst, LDBf(Ldb_2step),
			        LDBf(Ldb_MakeSettings),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;

		case 9:
			SetTriggerTypeDisplay(D_ON, PTRIG);
			ResetFlashOn(F_TRIGGER);
			sprintf(inst, LDBf(Ldb_2step),
			        LDBf(Ldb_Trig2PTRIG),
			        LDBf(Ldb_Givefivebreaths));
			SetInstructionBox(D_ON, title, inst);
			break;
	} // end switch(whichOne)

	return;
}

