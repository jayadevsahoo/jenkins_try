#pragma once

#include "stdafx.h"
#include <stdio.h>
#include "Windows.h"

#include "Swat.h"
#include "ISettingsCapture.h"
#include "ICConfiguration.h"

class CBDSendSettings
{

public:
    CBDSendSettings();
   ~CBDSendSettings();
	int BDSendSettings(char **pData, char *pHost, char *pPort);

 private:

	HWND m_pWnd;
    IConfiguration   *m_pIConfiguration;
    ISettingsCapture *m_pISettingsCapture;
    HINSTANCE hinstLib;
};
