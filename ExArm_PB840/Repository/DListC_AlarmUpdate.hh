
#ifndef DListC_AlarmUpdate_HH
#define DListC_AlarmUpdate_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: DListC_AlarmUpdate - Abstract Doubly-Linked List of 'AlarmUpdate' Items.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractDListC.H_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class  Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "DNodeC_AlarmUpdate.hh"
#include "Heap.hh"
//@ End-Usage


class DListC_AlarmUpdate
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&                  ostr,
				       const DListC_AlarmUpdate& dList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

    //@ Friend:  DIterC_AlarmUpdate
    // 'DIterC_AlarmUpdate' needs access to the protected members of this
    // class.
    friend class  DIterC_AlarmUpdate;

    //@ Friend:  DListR_AlarmUpdate
    // 'DListR_AlarmUpdate' needs access to the protected members of this
    // class.
    friend class  DListR_AlarmUpdate;

    //@ Friend:  SortC_AlarmUpdate
    // 'SortC_AlarmUpdate' needs access to the protected members of this
    // class.
    friend class  SortC_AlarmUpdate;

  public:
    virtual ~DListC_AlarmUpdate(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);
    inline SigmaStatus  goPrev (void);
    inline SigmaStatus  goLast (void);

    SigmaStatus  search(const AlarmUpdate&           item,
			const SearchFrom      startFrom = FROM_FIRST,
			const SearchDirection direction = FORWARD);

    void  insert (const AlarmUpdate&          newItem,
		  const InsertionPlace insertPlace = AFTER_CURRENT);
    void  append (const AlarmUpdate& newItem);
    void  prepend(const AlarmUpdate& newItem);

    SigmaStatus  removeCurrent(void);
    SigmaStatus  removeItem(const AlarmUpdate& item);
    void         clearList(void);

    inline const AlarmUpdate&  currentItem(void) const;
    inline AlarmUpdate&        currentItem(void);

    void  operator=(const DListC_AlarmUpdate& dList);

    inline Boolean  operator==(const DListC_AlarmUpdate& dList) const;
    inline Boolean  operator!=(const DListC_AlarmUpdate& dList) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
    inline DListC_AlarmUpdate(Heap* pHeap);

    const DNodeC_AlarmUpdate* findNode_(const AlarmUpdate&               item,
					const DNodeC_AlarmUpdate* pStartNode,
					const SearchDirection     direction
				       ) const;

    inline Boolean  isLegalNode_(const DNodeC_AlarmUpdate* pNode) const;

    inline const DNodeC_AlarmUpdate*  getFirst_  (void) const;
    inline const DNodeC_AlarmUpdate*  getCurrent_(void) const;
    inline const DNodeC_AlarmUpdate*  getLast_   (void) const;

    inline DNodeC_AlarmUpdate*  getFirst_  (void);
    inline DNodeC_AlarmUpdate*  getCurrent_(void);
    inline DNodeC_AlarmUpdate*  getLast_   (void);

    inline void  setCurrent_(DNodeC_AlarmUpdate* pNewCurr);

    Boolean  isEquivTo_(const DListC_AlarmUpdate& dList) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    DListC_AlarmUpdate(const DListC_AlarmUpdate&);	// not implemented...
    DListC_AlarmUpdate(void);				// not implemented...

    inline void  deallocNode_(DNodeC_AlarmUpdate*& pNode);

    //@ Data-Member:  rHeapOfNodes
    // A reference to the heap for this list's nodes.
    Heap&  rHeapOfNodes_;

    //@ Data-Member:  pFirst_
    // A pointer to first node of this list.
    DNodeC_AlarmUpdate*  pFirst_;

    //@ Data-Member:  pCurrent_
    // A pointer to current node of this list.
    DNodeC_AlarmUpdate*  pCurrent_;
};
 

// Inlined methods...
#include "DListC_AlarmUpdate.in"


#endif  // DListC_AlarmUpdate_HH
