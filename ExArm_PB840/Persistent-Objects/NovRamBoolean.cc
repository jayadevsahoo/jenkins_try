#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NovRamBoolean - NOVRAM Boolean Value.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a means of storing a boolean value in NOVRAM.
//  What this class provides that the system-wide boolean type does
//  not provide, is a set of boolean values that are represented by
//  bit patterns -- as opposed to '0' and '!0'.
//
//  This use of bit patterns provides protection against "lost" bytes
//  due to power-down and power-up sequences.  If a byte is "lost",
//  it will be detected via the bit patterns.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a safe mechanism for the storing of boolean
//  values in NOVRAM.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamBoolean.ccv   25.0.4.0   19 Nov 2013 14:18:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 002  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Added calling of 'NovRamManager::ReportAuxillaryInfo()' to
//	initialization and verification methods.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "NovRamBoolean.hh"

//@ Usage-Classes
#include "NovRamManager.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  NovRamBoolean()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default instance of this class.  The default value of
//  this boolean class is 'FALSE'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//  (isFalse())
//@ End-Method 
//===================================================================== 

NovRamBoolean::NovRamBoolean(void)
{
  CALL_TRACE("NovRamBoolean()");

  // initialize to 'FALSE'...
  updateValue(FALSE);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~NovRamBoolean()  [Destructor]
//
//@ Interface-Description
//  Destroy this instance.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

NovRamBoolean::~NovRamBoolean(void)
{
  CALL_TRACE("~NovRamBoolean()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize()  [const]
//
//@ Interface-Description
//  A reset sequence occurred, therefore verify the integrity of this
//  instance.  If this value is determined to be valid,
//  'NovRamStatus::ITEM_VERIFIED' is returned.  If this log is not valid
//  the log is fully initialized (from scratch) and
//  'NovRamStatus::ITEM_INITIALIZED' is returned.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method 
//===================================================================== 

NovRamStatus::InitializationId
NovRamBoolean::initialize(void)
{
  CALL_TRACE("initialize()");

  NovRamStatus::InitializationId  initStatusId;

  if (!isValid_())
  {   // $[TI1]
    // initiate a full initialization of this instance...
    new (this) NovRamBoolean();

    initStatusId = NovRamStatus::ITEM_INITIALIZED;

    // report failure location...
    NovRamManager::ReportAuxillaryInfo(this, sizeof(NovRamBoolean),
				       initStatusId);
  }
  else
  {   // $[TI2] -- a full initialization is NOT needed...
    // item is valid, as is...
    initStatusId = NovRamStatus::ITEM_VERIFIED;
  }

  // this should ALWAYS be true...
  SAFE_CLASS_POST_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID),
  			    NovRamBoolean);

  return(initStatusId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifySelf()  [const]
//
//@ Interface-Description
//  Verify that this instance's value is a legal value.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

NovRamStatus::VerificationId
NovRamBoolean::verifySelf(void) const
{
  CALL_TRACE("verifySelf()");

  NovRamStatus::VerificationId  verifStatus;

  if (isValid_())
  {   // $[TI1]
    verifStatus = NovRamStatus::ITEM_VALID;
  }
  else
  {   // $[TI2]
    verifStatus = NovRamStatus::ITEM_INVALID;

    NovRamManager::ReportAuxillaryInfo(this, sizeof(NovRamBoolean));
  }

  return(verifStatus);
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamBoolean::SoftFault(const SoftFaultID  softFaultID,
			 const Uint32       lineNumber,
			 const char*        pFileName,
			 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, NOV_RAM_BOOLEAN,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

