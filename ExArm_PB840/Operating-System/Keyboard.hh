#ifndef Keyboard_HH
#define Keyboard_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor Puritan Bennett Corporation of California.
//
//       Copyright (c) 1996, Nellcor Puritan Bennett Corporation
//=====================================================================

//====================================================================
// Class:  Keyboard - GUI Keyboard Polling Handler
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/Keyboard.hhv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 002   By: sah    Date:  14-Oct-1997    DR Number: 1529
//  Project: Sigma (R8027)
//  Description:
//	As part of the peer review rework for this DCS, I've added
//	a 'SoftFault()' method for handling assertions as "System"
//	assertions.
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//====================================================================

#ifdef SIGMA_GUI_CPU

#include "SigmaTypes.hh"

class Keyboard
{
  public:
    static void Poll();

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);
};

#endif // SIGMA_GUI_CPU
#endif // Keyboard_HH
