#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SerialNumberInitialize - copy serial number from data key to
//	       flash.
//---------------------------------------------------------------------
//@ Interface-Description
//	
//	The BD-IO-Devices class provides the status of the serial number:
//	if the serial number in flash is all X's (default value after download),
//	then serial number is to be copied from data key to flash.
//	The DataKey class (on the BD only) is used to check for data
//	key presence and to copy the serial # from the data key.
//	(On the GUI, the ServiceMode class performs thes functions.)
//	The CalInfoFlashBlock class is used to program the serial #
//	into flash.
//---------------------------------------------------------------------
//@ Rationale
//
//	This class is required to copy serial number from data key to
//	flash if the serial number in flash is all X's (default).
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	copySerialNumber() copies the serial # from the data key to flash.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//	none
//---------------------------------------------------------------------
//@ Invariants
//	none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SerialNumberInitialize.ccv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 25-Feb-2009    SCR Number: 6019
//  Project:  840S
//  Description:
//      Modified to enhance data key reliability. Initialize NOVRAM
//      operational hours from data key when BD serial number is 
//      committed to FLASH.
//
//  Revision: 005   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  gdc    Date: 02-Oct-1997    DR Number: DCS 2513
//       Project:  Sigma (840)
//       Description:
//            Changed to reference GetGuiSerialNumber().
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  syw    Date:  27-Mar-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

#include "SerialNumberInitialize.hh"

//@ Usage-Classes

#include <string.h>
#include "SerialNumber.hh"
#include "DataKey.hh"
#include "VentObjectRefs.hh"
#include "CalInfoRefs.hh"
#include "CalInfoFlashBlock.hh"
#include "SmManager.hh"
#include "BD_IO_Devices.hh"

#include "NovRamManager.hh"

#if defined (SIGMA_GUI_CPU)
#include "GuiApp.hh"
#endif  // defined (SIGMA_GUI_CPU)

#if defined(SIGMA_UNIT_TEST)
# include "SerialNumberInitialize_UT.hh"
#endif  // SIGMA_UNIT_TEST

#ifdef SIGMA_DEVELOPMENT
#include <stdio.h>
#endif // SIGMA_DEVELOPMENT

//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: SerialNumberInitialize
//
//@ Interface - Description
//  	Constructor.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		none
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================

SerialNumberInitialize::SerialNumberInitialize (void)
{
	CALL_TRACE("SerialNumberInitialize::SerialNumberInitialize (void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~SerialNumberInitialize
//
//@ Interface - Description
//		Destructor.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		none
//-----------------------------------------------------------------------
//@ PreCondition
//  	none
//-----------------------------------------------------------------------
//@ PostCondition
//  	none
//@ End - Method
//=======================================================================

SerialNumberInitialize::~SerialNumberInitialize (void)
{
	CALL_TRACE("SerialNumberInitialize::~SerialNumberInitialize (void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: copySerialNumber
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called to copy the serial number to flash.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The serial number is copied over if the serial number in flash is
//		the default serial number or if the data key serial number matches
//		the serial number stored in novram.  The latter condition recovers
//		the serial number when the flash gets corrupted or erased.
//
//  $[LC08000] If the operator chooses to synchronize the serial numbers 
//             (see Serial Number Setup Subscreen), the operational hours 
//             from the data key shall be stored in NOVRAM
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
SerialNumberInitialize::copySerialNumber (void)
{
	CALL_TRACE("SerialNumberInitialize::copySerialNumber (void)");

	SerialNumber novRamSerialNumber ;
	SerialNumber dataKeySerialNumber ;
	
	NovRamManager::GetUnitSerialNumber( novRamSerialNumber) ;

#if defined (SIGMA_BD_CPU)
	dataKeySerialNumber = RDataKey.getSerialNumber( DataKey::BD) ;
	if (BD_IO_Devices::GetSerialNumInitialSignature() ||
			dataKeySerialNumber == novRamSerialNumber)
	{
		if (RDataKey.isInstalled())
		{
            strncpy(  RCalInfoFlashBlock.FlashImage.serialNumber
                    , RDataKey.getSerialNumber(DataKey::BD).getString()
                    , SERIAL_NO_SIZE );

			Uint32 programError = RCalInfoFlashBlock.programFlashBlock() ;

			if (programError)
			{
				// Error occurred while programming serial # into flash
				RSmManager.sendStatusToServiceData( SmStatusId::TEST_FAILURE,
												SmStatusId::CONDITION_1);
			}
			else
			{
				// else: serial # programming into flash was successful

				//  $[LC08000] synchronize NOVRAM with data key hours
				// call sync to set NOVRAM hours to the hours read from the data key
				RDataKey.syncVentOperationalTimes();
			}
		}
		else
		{
			// Data key not installed, so can't get serial #
			RSmManager.sendStatusToServiceData( SmStatusId::TEST_FAILURE,
												SmStatusId::CONDITION_2);
		}
	}
#else
	dataKeySerialNumber = GuiApp::GetGuiDataKeySN() ;
	if (BD_IO_Devices::GetSerialNumInitialSignature() ||
			dataKeySerialNumber == novRamSerialNumber)
	{
 		if (ServiceMode::IsDataKeyInstalled())
		{
            strncpy(  RCalInfoFlashBlock.FlashImage.serialNumber
                    , ServiceMode::GetGuiSerialNumber().getString()
                    , SERIAL_NO_SIZE );

			Uint32 programError = RCalInfoFlashBlock.programFlashBlock() ;

			if (programError)
			{
				// Error occurred while programming serial # into flash
				RSmManager.sendStatusToServiceData( SmStatusId::TEST_FAILURE,
							   					SmStatusId::CONDITION_1);
			}
			// implied else: serial # programming into flash was successful
		}
		else
		{
			// Data key not installed, so can't get serial #
			RSmManager.sendStatusToServiceData( SmStatusId::TEST_FAILURE,
							   					SmStatusId::CONDITION_2);
		}
	}
	// implied else: stored serial number is NOT the initial signature
	// (all X's), so DON'T copy serial number from data key
#endif // SIGMA_BD_CPU
}


#ifdef SIGMA_DEVELOPMENT

void
SerialNumberInitialize::storeXs( void)
{
	CALL_TRACE("SerialNumberInitialize::storeXs( void)") ;

    strncpy(  RCalInfoFlashBlock.FlashImage.serialNumber
   	        , "XXXXXXXXXXXX"
       	    , SERIAL_NO_SIZE );

	Uint32 programError = RCalInfoFlashBlock.programFlashBlock() ;

	if (programError)
	{
		RSmManager.sendStatusToServiceData( SmStatusId::TEST_FAILURE,
					   					SmStatusId::CONDITION_1);

		printf( "programError = %X\n", programError) ;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SerialNumberInitialize::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
	
	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, SERIALNUMBERINITIALIZE,
    	                      lineNumber, pFileName, pPredicate) ;
}

#endif // SIGMA_DEVELOPMENT


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

 
//=====================================================================
//
//  Private Methods...
//
//=====================================================================
