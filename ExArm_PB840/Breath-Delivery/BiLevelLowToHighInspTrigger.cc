#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: BiLevelLowToHighInspTrigger - Mandatory inspiratory time trigger 
//  for BiLevel.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class triggers the start of bilevel mandatory inspiration .  
//  The trigger is contained on a list in the BreathTriggerMediator.
//  The object enabling this trigger must contain
//  the algorithm for determining this trigger's applicability.
//---------------------------------------------------------------------
//@ Rationale
//  This class implements the algorithm for determining when it is time
//  to start a mandatory inspiration at high peep level during BiLevel.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This trigger may be enabled or disabled.  The trigger condition is
//  checked every BD cycle when the trigger is enabled, and on the active
//  trigger list in the BreathTriggerMediator.  When this trigger fires,
//  it notifies the BreathPhaseScheduler that it is time to transition
//  to a new breath phase.
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BiLevelLowToHighInspTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  yyy    Date:  05-Nov-98    DR Number: 5252
//       Project:  Sigma (R8027)
//       Description:
//             Mapping SRS to code.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "BiLevelLowToHighInspTrigger.hh"
#  include "BreathMiscRefs.hh"
#  include "TimersRefs.hh"
#  include "BD_IO_Devices.hh"

//@ Usage-Classes
#  include "IntervalTimer.hh"
#  include "BiLevelScheduler.hh"
#  include "BreathRecord.hh"
#  include "BreathSet.hh"
#  include "Trigger.hh"
#  include "ApneaInterval.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BiLevelLowToHighInspTrigger()  
//
//@ Interface-Description
//	Default Contstructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize TriggerId_ using BreathTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BiLevelLowToHighInspTrigger::BiLevelLowToHighInspTrigger(void)
 : BreathTrigger (Trigger::BL_LOW_TO_HIGH_INSP)
{
  CALL_TRACE("BiLevelLowToHighInspTrigger()");

  timeUp_ = FALSE;
  lastBlBreathType_ = ::CONTROL;
  rateChange_ = FALSE;
  timeUpStartTime_ = 0;
  
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BiLevelLowToHighInspTrigger() 
//
//@ Interface-Description
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BiLevelLowToHighInspTrigger::~BiLevelLowToHighInspTrigger(void)
{
  CALL_TRACE("~BiLevelLowToHighInspTrigger()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  timeUpHappened(void)
//
// Interface-Description
// Setup timeUp_, lastBlBreathType_, timeUpStartTime_, phasedInPeepLowTimeMs_,
// and rateChange_ values for determining when to trigger a
// BiLevelLowToHighInspTrigger.
//---------------------------------------------------------------------
// Implementation-Description
// Set the timeUp_ to timeUp.
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================

void
BiLevelLowToHighInspTrigger::timeUpHappened(Boolean timeUp,
											::BreathType lastBlBreathType,
											Int32 phasedInPeepLowTimeMs,
											Boolean rateChange)
{
	CALL_TRACE("BiLevelLowToHighExpTrigger::timeUpHappened(lastBlBreathType, phasedInPeepLowTimeMs, rateChange)");
	
	timeUp_ = timeUp;
	lastBlBreathType_ = lastBlBreathType;
	timeUpStartTime_ = RBiLevelCycleTimer.getCurrentTime();
	phasedInPeepLowTimeMs_ = phasedInPeepLowTimeMs;
	rateChange_ = rateChange;			// $[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BiLevelLowToHighInspTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BILEVELLOWTOHIGHINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_()
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns 
//    false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    $[04234]
//    With the timer expired, one of the following conditions must be met 
//    if an inspiration is to be triggered:
//
//    1.  last breath at peep high is not SPONT and 
//        a. rate did not change or
//        b. rate changed and timeUpStartTime_ >= phasedInPeepLowTimeMs_ or
//        c. the rate changed and phase is not restricted
//    2.  last breath at peep high is SPONT and 
//        a. phase is not restricted or
//        b. apnea is not possible and breath duration >= apnea interval        
// 
//    The first condition implements the logic of rate change where inspiration 
//    cannot be triggered if breath is restricted unless the previous (phased-in)
//    exhalation time has elapsed.
//       
//    The second condition makes sure that apnea won't start if it is not possible,
//    instead, an BiLevel mandatory breath will be delivered. This condition does not
//    distinguish between SPONT and Mandatory breath types since a spont breath
//    should not be restricted after 10 seconds (minimum apnea interval) and a mandatory
//    breath should have been started more than 60/f seconds ago (since apnea is not
//    possible).
//        
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
BiLevelLowToHighInspTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");
  Boolean returnValue = FALSE;
  Real32 apneaIntervalForAsapTrigger = (Real32)RApneaInterval.getApneaInterval() + 10.0F;
  BreathRecord* pCurrentRecord = RBreathSet.getCurrentBreathRecord();
  Real32 breathDuration =  pCurrentRecord->getBreathDuration();

  if (timeUp_)
  {
	// $[TI5]
    timeUpStartTime_ += CYCLE_TIME_MS;
  }
  // $[TI6]
  
  // $[BL04038] when peep low time expired, spont breath occured and
  // phase is not restricted then transitions to peep high
  // $[BL04039] when peep low time expired, and no spont breath
  // occured then transitions to peep high
  // $[BL04080] Changes in breath timing due to patient effort or user action, ...
  if ( (timeUp_ && lastBlBreathType_ != ::SPONT &&
        ((!rateChange_) ||
         (rateChange_ && timeUpStartTime_ >= phasedInPeepLowTimeMs_) ||
         (rateChange_ && !BreathRecord::IsPhaseRestricted()))) ||
  	   (timeUp_ && lastBlBreathType_ == ::SPONT && !BreathRecord::IsPhaseRestricted()) ||
  	   (!BreathPhaseScheduler::CheckIfApneaPossible() &&
  	    breathDuration >= apneaIntervalForAsapTrigger) )
  {
    // $[TI1]
    returnValue = TRUE;
    timeUp_ = FALSE;
	lastBlBreathType_ = ::CONTROL;
	rateChange_ = FALSE;
	timeUpStartTime_ = 0;
  }
  // $[TI2]

  return (returnValue);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


