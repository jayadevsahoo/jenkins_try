#include "stdafx.h"
//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2008, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Class: SerialQueue
//---------------------------------------------------------------------
//@ Interface-Description
//  This utility class provides a queue that is used for serial port 
//  input. It provides all the necessary methods to create and access
//  a queue implemented as a circular buffer. Data is added to the 
//  queue using the insert() method. It is removed using the remove()
//  method. Data on the queue can be read without removing it using
//  the peek() method.
//---------------------------------------------------------------------
//@ Rationale
//  Utility class to provide simple queue for character data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Initial version to support for VentSet API.
//=====================================================================

#include "SerialQueue.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SerialQueue [default constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SerialQueue::SerialQueue( void )
:
headIndex_(0),
tailIndex_(0)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SerialQueue [destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SerialQueue::~SerialQueue( void )
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: peek
//
//@ Interface-Description
//	Copies the specified number of characters into the buffer provided
//  from the SerialQueue. Returns the number of characters copied.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
int SerialQueue::peek( unsigned char *buf, int num ) const
{
	unsigned int index = tailIndex_;
	int total = 0;

	while ( total < num && index != headIndex_ )
	{
		*buf++ = buffer_[ index++ ];
		if ( index >= sizeof( buffer_ ) )
		{
			index = 0;
		}
		total++;
	}
	return total;
}
