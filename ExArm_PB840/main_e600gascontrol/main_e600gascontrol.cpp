#include "stdafx.h"
// main_e600gascontrol.cpp : Defines the entry point for the console application.
//
#include "Sys_Init.hh"
#include "NV_MemoryStorageThread.hh"
#include "SoftwareRevNum.hh"

static const Int8 BUFFER_SIZE = 100;

int _tmain(int argc, _TCHAR* argv[])
{
	//To print Software Version Number along with Build Date & Time.
	wchar_t string[SoftwareRevNum::REV_NUM_SIZE];
	const char *bufferPtr=NULL;
	char buffer[ BUFFER_SIZE ];

	SoftwareRevNum const &rVersionNumber = SoftwareRevNum::GetInstance();	
	bufferPtr = rVersionNumber.GetAsFullString();

	sprintf (buffer, " %s %s %s",bufferPtr,__DATE__,__TIME__);
	MultiByteToWideChar(CP_ACP, 0,buffer , -1, string, sizeof(string) / sizeof(wchar_t));
	RETAILMSG(1, (L"Software Revison Number with Build Date & Time = %s \r\n",string));

	Sys_Init::InitializeSigma();	//TODO this function runs POST_3 and some board init, it might not be needed

    // Initialize the structures that are expecting to be in NOVRAM or flash.
    // These are here retrieved from the WinCE file system (on flash).
	NV_MemoryStorageThread::Initialize();

	//initialize tasks (threads) objects but do not create them yet..
    Sys_Init::InitializeTasks();
	//initialize subsystems, etc., then start the state-machine
	//Threads will be created in FsmState::Activate_() when it calls activate() for current-state
	Sys_Init::Initialize();

	while(TRUE)
	{
		Sleep(5000);	//sleep for 5 seconds. this is all what main() does.
	}

	//we will never get here
	return 0;
}
