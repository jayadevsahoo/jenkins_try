#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: FaultHandler - Fault-Handler for the System.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is the entry point for all system faults.  When a
//  hardware, POST or communication fault, software exception, or
//  processor exception is detected control is given to this class to:
//  log the diagnostic code that contains the information of the fault,
//  and respond to the exception, accordingly.
//
//  This class has a method for responding to each of the five fault
//  types, taking as arguments, the needed fault information.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a central "hub" for all system diagnostic faults
//  to enter.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each of the five fault-entry methods will log the corresponding
//  fault into the corresponding fault log.
//---------------------------------------------------------------------
//@ Fault-Handling
//  This class does no fault-handling of itself.  This class strictly
//  responds to faults that occur.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/FaultHandler.ccv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision: 009   By: srp   Date: 28-May-2002   DCS Number: 5907
//   Project:  VCP
//   Description:
//      Add traces for 00617 and 00645.
//
//   Revision: 008   By: sah   Date: 08-Sep-1997   DCS Number: 1923
//   Project:  Sigma (R8027)
//   Description:
//	Eliminate use of Communication Diagnostic Log, and replace with
//	System Information Log.  Furthermore, have all system event
//	diagnostics logged into the new System Information Log.
//
//   Revision: 007   By: sah   Date: 18-Mar-1997   DCS Number: 1847
//   Project:  Sigma (R8027)
//   Description:
//	Add missing testable item labels.
//
//   Revision: 006   By: sah   Date: 04-Mar-1997   DCS Number: 1062
//   Project:  Sigma (R8027)
//   Description:
//	Added disabling of NOVRAM semaphore before logging of assertion
//      failure, exception, and NMI diagnostics.
//
//   Revision: 005   By: sah   Date: 30-Aug-1996   DCS Number: 1280
//   Project:  Sigma (R8027)
//   Description:
//	Added "auxillary" assertion macros that take an error code
//	argument.
//
//   Revision: 004   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//
//   Revision: 003   By: sah   Date: 30-Oct-1995   DCS Number: 607
//   Project:  Sigma (R8027)
//   Description:
//	Need mechanism for logging faults during POST.
//
//   Revision: 002   By: sah   Date: 19-Jun-1995   DCS Number: 483
//   Project:  Sigma (R8027)
//   Description:
//	Need to prevent infinite loop of assertions due to assertion
//	failure during processing of an assertion.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "FaultHandler.hh"

#include "Task.hh"
//TODO E600 #include "OsUtil.hh"
#include <stdlib.h>
#include "Sigma.hh"
#include "Utilities.hh"

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif  // defined(SIGMA_DEVELOPMENT)

#include "InitiateReboot.hh"

//@ Usage-Classes...
#include "DiagnosticCode.hh"
#include "NovRamManager.hh"
#include "NovRamSemaphore.hh"
//TODO E600 #include "Watchdog.hh"
#include "Post.hh"
#include "Post_Library.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Members...
//
//=====================================================================

Uint32  FaultHandler::AuxErrorCode_ = 0u;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()
//
//@ Interface-Description
//  Initialize this class's static information.  This is to be called
//  by 'Utilities::Initialize()'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
FaultHandler::Initialize(void)
{
  CALL_TRACE("Initialize()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  StoreAuxErrorCode(auxErrorCode)  [static]
//
//@ Interface-Description
//  Store (temporarily) 'auxErrorCode' for use by this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
FaultHandler::StoreAuxErrorCode(const Uint32 auxErrorCode)
{
  CALL_TRACE("StoreAuxErrorCode(auxErrorCode)");

  FaultHandler::AuxErrorCode_ = auxErrorCode;
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultId, subSystemId, moduleId, lineNumber,
//  	               pFileName, pBoolTest)  [static]
//
//@ Interface-Description
//  Log the occurrence of this software fault, then issue a system
//  reset.
//---------------------------------------------------------------------
//@ Implementation-Description
//  To ensure that no other thread takes control during the processing
//  of this fault, a "hard" lock of the task scheduler is performed.
//
//  $[00613] -- fault information from any software error shall
//              be logged in NOVRAM...
//
//  $[00617] -- Fault/status...including a timestamp, ... 
//              shall persist across a power failure.
//
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
FaultHandler::SoftFault(const SoftFaultID softFaultId,
			const SubSystemID subSystemId,
			const Uint32      moduleId,
			const Uint32      lineNumber,
			const char*       pFileName,
			const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultId, subSystemId, moduleId, lineNumber, pFileName, pBoolTest)");

  if ( Post::IsPostActive() )
  {   // $[TI1]
      MajorPostFailure();    // never returns
  }   // $[TI2]
#  if defined(SIGMA_DEVELOPMENT)
  Uint8  currIplValue;

  // lock all maskable interrupts during the processing of this
  // exception...
  currIplValue =
#  endif // defined(SIGMA_DEVELOPMENT)

#ifdef E600_840_TEMP_REMOVED
  ::SetProcessorIPL(7);
  STROBE_WATCHDOG;
#endif

#if defined(SIGMA_DEVELOPMENT)
  if (Task::IsTaskingOn())
  {
    cout << endl;
    cout << endl;

    switch (softFaultId)
    {
    case INVARIANT_ID :
      cout << "INVARIANT";
      break;
    case PRE_CONDITION_ID :
      cout << "PRE-CONDITION";
      break;
    case POST_CONDITION_ID :
      cout << "POST-CONDITION";
      break;
    case ASSERTION_ID :
      cout << "ASSERTION";
      break;
    case UNDEFINED_SOFT_FAULT :
    case NUM_SOFT_FAULT_IDS :
    default :
      FaultHandler::SoftFault(ASSERTION_ID, UTILITIES, FAULT_HANDLER,
			      __LINE__, __FILE__, "(FALSE)");
      break;
    };

    cout << " FAILURE {" << endl;

    if (pBoolTest != NULL) {
      cout << "   Failed Test:  " << pBoolTest << endl;
    }

    cout << "   SubSystemId:  " << subSystemId << endl;
    cout << "   ModuleId:     " << moduleId << endl;

    if (pFileName != NULL) {
      cout << "   File:         " << pFileName << endl;
    }

    cout << "   Line:         " << lineNumber << endl;

    cout << "   Aux. Code:    " << FaultHandler::AuxErrorCode_ << endl;

    cout << "};" << endl;

    cout.flush();
  }
#endif // defined(SIGMA_DEVELOPMENT)

  // use same behavior as a system call uses...
  FaultHandler::SystemSoftFault(softFaultId, subSystemId, moduleId,
  				lineNumber);

#if defined(SIGMA_DEVELOPMENT)
  // if 'InitiateReboot()' is overridden to allow execution to continue,
  // all maskable interrupts should be reset...
#ifdef E600_840_TEMP_REMOVED
  ::SetProcessorIPL(currIplValue);
#endif
#endif // defined(SIGMA_DEVELOPMENT)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SystemSoftFault(softFaultId, subSystemId, moduleId, lineNumber)
//
//@ Interface-Description
//  Log the occurrence of this software fault that occurred during a system
//  call, then issue a system reset.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this occurred during a system call, no masking of interrupts is
//  needed, in fact, no system calls can be called during this function --
//  this includes standard output calls.
//
//  $[00613] -- fault information from any software error shall
//              be logged in NOVRAM...
//
//  $[00617] -- Fault/status...including a timestamp, ... 
//              shall persist across a power failure.
//
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
FaultHandler::SystemSoftFault(const SoftFaultID softFaultId,
			      const SubSystemID subSystemId,
			      const Uint32      moduleId,
			      const Uint32      lineNumber)
{
  CALL_TRACE("SystemSoftFault(softFaultId, subSystemId, moduleId, lineNumber)");

  static Uint  EntryCounter_ = 0u;

  if (EntryCounter_++ > 0u)
  {   // $[TI1]
    // we've re-entered this routine due to an assertion failure during
    // the processing of a fault, therefore by-pass logging...
    ::InitiateReboot();
  }   // $[TI2]

  DiagnosticCode  diagnosticCode;

  // set the diagnostic code to a software exception with the given
  // parameters...
  diagnosticCode.setSoftwareTestCode(softFaultId, subSystemId,(Uint16) moduleId,
				     (Uint16) lineNumber, FaultHandler::AuxErrorCode_);

  // disable the access semaphore to allow this fatal error to be logged...
  NovRamSemaphore::DisableBlocking();

  // log the diagnostic code...
  FaultHandler::LogDiagnosticCode(diagnosticCode);

  // initiate POST...
  ::InitiateReboot();

#if defined(SIGMA_DEVELOPMENT)
  // just in case someone has overridden 'InitiateReboot()', re-enable
  // semaphore blocking...
  NovRamSemaphore::EnableBlocking();

  EntryCounter_--;
#endif // defined(SIGMA_DEVELOPMENT)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  LogExceptionCode(vectorNumber, instructionAddr,
//			      faultAddress, taskId, pExceptionName)  [static]
//
//@ Interface-Description
//  Log the occurrence of the exception identified by 'vectorNumber',
//  at the instruction given by 'instructionAddr', (possibly) due to the
//  the accessing of the address given by 'faultAddress', by the task
//  (thread) given by 'taskId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[00613] -- fault information from any software error shall
//              be logged in NOVRAM...
//
//  $[00617] -- Fault/status...including a timestamp, ... 
//              shall persist across a power failure.
//
//  $[00645] -- Fault information for each EST and SST Failure and Alert 
//              ... including a timestamp, from EST/SST errors ... shall \
//              persist across a power failure  
//
//  This method CANNOT make any system calls (e.g., printf()).
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
FaultHandler::LogExceptionCode(const Uint16 vectorNumber,
			       const void*  instructionAddr,
			       const void*  faultAddr,
			       const Uint16 taskId,
			       const char*  pExceptionName)
{
  DiagnosticCode  diagnosticCode;

  // set the diagnostic code to a exception with the given parameters...
  diagnosticCode.setExceptionCode(vectorNumber, (Uint32)instructionAddr,
  				  (Uint32)faultAddr, taskId);

  // disable the access semaphore to allow this fatal error to be logged...
  NovRamSemaphore::DisableBlocking();

  // log the diagnostic code...
  FaultHandler::LogDiagnosticCode(diagnosticCode);

  // re-enable the access semaphore...
  NovRamSemaphore::EnableBlocking();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  LogNmiCode(nmiSourceId, errorCode, pNmiName)  [static]
//
//@ Interface-Description
//  Log the occurrence of the non-maskable interrupt identified by
//  'nmiSourceId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[00613] -- fault information from any software error shall
//              be logged in NOVRAM...
//
//  $[00617] -- Fault/status...including a timestamp, ... 
//              shall persist across a power failure.
//
//  $[00645] -- Fault information for each EST and SST Failure and Alert 
//              ... including a timestamp, from EST/SST errors ... shall \
//              persist across a power failure  
//
//  This method CANNOT make any system calls (e.g., printf()).
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
FaultHandler::LogNmiCode(const Uint16 nmiSourceId,
			 const Uint32 errorCode,
			 const char*  pNmiName)
{
  DiagnosticCode  diagnosticCode;

  // set the diagnostic code to a exception with the given parameters...
  diagnosticCode.setNmiCode(nmiSourceId, errorCode);

  // disable the access semaphore to allow this fatal error to be logged...
  NovRamSemaphore::DisableBlocking();

  // log the diagnostic code...
  FaultHandler::LogDiagnosticCode(diagnosticCode);

  // re-enable the access semaphore...
  NovRamSemaphore::EnableBlocking();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  LogDiagnosticCode(diagCode)  [static]
//
//@ Interface-Description
//  Log the occurrence of a diagnostic event that is represented in
//  'diagCode'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[00613] -- fault information from any software error shall
//              be logged in NOVRAM...
//
//  $[00617] -- Fault/status...including a timestamp, ... 
//              shall persist across a power failure.
//
//  $[00645] -- Fault information for each EST and SST Failure and Alert 
//              ... including a timestamp, from EST/SST errors ... shall \
//              persist across a power failure  
//
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================
void
FaultHandler::LogDiagnosticCode(const DiagnosticCode& diagCode)
{

  CALL_TRACE("LogDiagnosticCode(diagCode)");

  const DiagnosticCode::DiagCodeTypeId  DIAG_CODE_TYPE =
					      diagCode.getDiagCodeTypeId();

  switch (DIAG_CODE_TYPE)
  {
  case DiagnosticCode::SOFTWARE_TEST_DIAG :
  case DiagnosticCode::EXCEPTION_DIAG :
  case DiagnosticCode::NMI_DIAG :
  case DiagnosticCode::STARTUP_SELF_TEST_DIAG :
  case DiagnosticCode::BACKGROUND_TEST_DIAG :		// $[TI1]
    // log the diagnostic code into the System Diagnostic Code Log...
    NovRamManager::LogSystemDiagnostic(diagCode);
    break;
  case DiagnosticCode::SYSTEM_EVENT_DIAG :
  case DiagnosticCode::COMMUNICATION_DIAG :		// $[TI2]
    // log the diagnostic code into the System Information Log...
    NovRamManager::LogSystemInformation(diagCode);
    break;
  case DiagnosticCode::SHORT_SELF_TEST_DIAG :
  case DiagnosticCode::EXTENDED_SELF_TEST_DIAG :	// $[TI3]
#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
    // log the diagnostic code into the EST/SST Diagnostic Code Log...
    NovRamManager::LogEstSstDiagnostic(diagCode);
    break;
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
  case DiagnosticCode::UNDEFINED_DIAG_CODE :
  case DiagnosticCode::NUM_FAULT_TYPE_IDS :
  default :
    FaultHandler::SoftFault(ASSERTION_ID, UTILITIES, FAULT_HANDLER,
			    __LINE__, __FILE__, "(FALSE)");
    break;
  };

}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

