
#ifndef SortIterC_AlarmUpdate_HH
#define SortIterC_AlarmUpdate_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SortIterC_AlarmUpdate - Iterator for 'SortC_AlarmUpdate'
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/SortedIterC.H_v   25.0.4.0   19 Nov 2013 14:03:50   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
#include "SortC_AlarmUpdate.hh"
//@ End-Usage


class SortIterC_AlarmUpdate
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    friend Ostream&  operator<<(Ostream&                 ostr,
                                const SortIterC_AlarmUpdate& sortIter);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  public:
    inline SortIterC_AlarmUpdate(const SortIterC_AlarmUpdate& sortIter);
    inline SortIterC_AlarmUpdate(const SortC_AlarmUpdate& sortList);
    inline ~SortIterC_AlarmUpdate(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Boolean  isSorted(void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);
    inline SigmaStatus  goPrev (void);
    inline SigmaStatus  goLast (void);

    SigmaStatus  search(const AlarmUpdate& item);

    inline const AlarmUpdate&  currentItem(void) const;

    inline void  attachTo(const SortIterC_AlarmUpdate& sortIter);
    inline void  attachTo(const SortC_AlarmUpdate& sortList);

    inline Boolean  operator==(const SortIterC_AlarmUpdate& sortIter) const;
    inline Boolean  operator!=(const SortIterC_AlarmUpdate& sortIter) const;

    inline Boolean  operator==(const SortC_AlarmUpdate& sortList) const;
    inline Boolean  operator!=(const SortC_AlarmUpdate& sortList) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
    inline const DNodeC_AlarmUpdate*  getFirst_  (void) const;
    inline const DNodeC_AlarmUpdate*  getCurrent_(void) const;
    inline const DNodeC_AlarmUpdate*  getLast_   (void) const;

    inline Boolean  isLegalNode_(const DNodeC_AlarmUpdate* pNode) const;

    // set the iterator's current pointer to 'pNewCurr'...
    inline void  setCurrent_(DNodeC_AlarmUpdate* pNewCurr);

  private:
    SortIterC_AlarmUpdate(void);			// not implemented...
    void  operator=(const SortIterC_AlarmUpdate&);	// not implemented...

    //@ Data-Member:  pIterList_
    // The pointer to this iterator's constant list.
    const SortC_AlarmUpdate*  pIterList_;

    //@ Data-Member:  pIterCurrent_
    // The pointer to this iterator's constant current node.
    const DNodeC_AlarmUpdate*  pIterCurrent_;
};
 

// Inlined methods...
#include "SortIterC_AlarmUpdate.in"


#endif  // SortIterC_AlarmUpdate_HH
