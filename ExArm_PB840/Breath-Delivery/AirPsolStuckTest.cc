#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AirPsolStuckTest - BD Safety Net Test for the detection of
//                            Air PSOL stuck test
//---------------------------------------------------------------------
//@ Interface-Description
//  The checkCriteria() method of this class is called from the
//  newCycle() method of the SafetyNetTestMediator class.  The
//  checkCriteria() method invokes methods in the Psol class to obtain 
//  the value of the Air PSOL command and the Sensor class to obtain 
//  the Air flow reading.
//  It also interfaces with the GasSupply object to determine if air
//  is available.
//---------------------------------------------------------------------
//@ Rationale
//  Used to determine if the Air PSOL is stuck.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The safety net test is defined in the checkCriteria() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/AirPsolStuckTest.ccv   10.7   08/17/07 09:32:56   pvcs  
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 005  By: iv     Date:  08-Jun-1999    DR Number: 5425
//  Project:  ATC
//  Description:
//      Added an offset to the highPsolCommandLimit.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code when fault is detected.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  05-Sep-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "AirPsolStuckTest.hh"
#include "MainSensorRefs.hh"
#include "MiscSensorRefs.hh"
#include "ValveRefs.hh"
#include "VentObjectRefs.hh"

//@ Usage-Classes
#include "FlowSensor.hh"
#include "LinearSensor.hh"
#include "GasSupply.hh"
#include "Psol.hh"

//@ End-Usage

//@ Code...

#ifdef SIGMA_UNIT_TEST
    extern Int32 UnitTestNum;
#endif // SIGMA_UNIT_TEST
//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AirPsolStuckTest()  
//
//@ Interface-Description
//  Default Constructor.  Passes two arguments to the base class
//  constructor to define the error code and the time criterion.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The backgndEventId_ and maxCyclesCriteriaMet_ data members are
//  set by the base class constructor.  The background event id is
//  a unique identifier that is placed in the diagnostic log if the
//  background check detects a problem.  The maximum Cycles that the
//  criterion is be met before the Background subsystem is notified
//  is MAX_CYCLES_PSOL_STUCK.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

AirPsolStuckTest::AirPsolStuckTest( void ) :
SafetyNetTest( BK_AIR_PSOL_STUCK, MAX_CYCLES_PSOL_STUCK)
{
    // $[TI1]
    CALL_TRACE("AirPsolStuckTest(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AirPsolStuckTest() 
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

AirPsolStuckTest::~AirPsolStuckTest( void )
{
    CALL_TRACE("~AirPsolStuckTest(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCriteria()
//
//@ Interface-Description
//  This method takes no arguments and returns a BkEventName.
//  This method is responsible for detecting the Air PSOL stuck
//  condition.
//  If the background check criteria have been met for the required
//  number of cycles, the backgndEventId for the class is returned;
//  otherwise BK_NO_EVENT is returned to indicate to the caller that
//  the Background subsystem should not be notified.
//
//  If the check has not already failed, the status of the oxygen
//  supply is checked.  If air is available, the value of the
//  Air flow signal and the value of the Air PSOL command level are
//  retrieved from the flow sensor and the Psol objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method first checks the status of the backgndCheckFailed_
//  data member.  If it is TRUE, the test is no longer run since
//  this Background event is not auto-resettable and the Background
//  subsystem only needs to be informed of an event once.  In this
//  case, BK_NO_EVENT is returned to the caller.
//
//  This method invokes GasSupply::isAirPresent to determine if the
//  oxygen supply is available.  This check is not performed if there
//  is no oxygen supply as the loss of the Air supply could look like
//  a PSOL stuck failure when it is not.
//
//  The method also calls the getValue() method for the Air flow Sensor
//  flow signal.
//
//  The following criteria is tested:
//
//      Current Air PSOL Command > High PSOL Command Limit
//      and
//      Current Desired Air Flow <= MAX_PSOL_STUCK_TEST_FLOW_RATE 
//
//  If the condition is met, the numCyclesCriteriaMet_ data item is
//  incremented and compared to the maxCyclesCriteriaMet_ item to
//  determine if the background subsystem needs to be informed.
//
//  $[06183]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
AirPsolStuckTest::checkCriteria( void )
{
    CALL_TRACE("AirPsolStuckTest::checkCriteria( void )") ;

    BkEventName rtnValue = BK_NO_EVENT;

    // Don't do the check if it has already failed    
    if ( ! backgndCheckFailed_ )
    {
    // $[TI1]
        
        // Only do the check if air (wall or compressor) is available
        if ( RGasSupply.isTotalAirPresent() )
        {
        // $[TI1.1]
        
            DacCounts highPsolCommandLimit = (DacCounts)
                ( ( QUICKEST_PSOL_COMMAND_GRADIENT * RAirFlowSensor.getValue()
                    + MAX_HIGH_PSOL_COMMAND ) * PSOL_CURRENT_TO_DAC_SCALE
				    +  PSOL_COMMAND_OFFSET );

            DacCounts currentAirPsolCommand = RAirPsol.getPsolCommand();
            Real32 currentDesiredAirFlow = getCurrentDesiredAirFlow_();

            if ( ( currentAirPsolCommand > highPsolCommandLimit ) &&
                 ( currentDesiredAirFlow <= MAX_PSOL_STUCK_TEST_FLOW_RATE ) )
            {
            // $[TI1.1.1]
                if ( ++numCyclesCriteriaMet_ >= maxCyclesCriteriaMet_ )
                {
                // $[TI1.1.1.1]
                    errorCode_ = currentAirPsolCommand;
                    backgndCheckFailed_ = TRUE;
                    rtnValue = backgndEventId_;
                }
                // $[TI1.1.1.2]
            }
            else  // criteria not met
            {
            // $[TI1.1.2]
                numCyclesCriteriaMet_ = 0;
            }
        }
        else   // no air supply
        {
        // $[TI1.2]
            numCyclesCriteriaMet_ = 0;
        }
    }
    // $[TI2]
    
#ifdef SIGMA_UNIT_TEST
if ( UnitTestNum == 4)
{
    errorCode_ = 1111U;
    backgndCheckFailed_ = TRUE;
    rtnValue = backgndEventId_;
}
#endif // SIGMA_UNIT_TEST

    return( rtnValue );
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
AirPsolStuckTest::SoftFault( const SoftFaultID  softFaultID,
                             const Uint32       lineNumber,
                             const char*        pFileName,
                             const char*        pPredicate )
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

    FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, AIRPSOLSTUCKTEST,
                            lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


