#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BreathTimingDiagram - Displays a timing bar which illustrates
// the relationship between the following settings: Respiratory Rate,
// Inspiratory Time, Expiratory Time and I:E Ratio.
//---------------------------------------------------------------------
//@ Interface-Description
// The breath timing diagram illustrates the relationship between Respiratory
// Rate, Inspiratory Time, Expiratory Time and I:E Ratio.  It does this by
// displaying a horizontal bar, divided in two.  The first section of the bar
// displays the Inspiratory Time, the second section Expiratory Time.  The
// width of each section is set in proportion to the magnitude of the value
// which each displays.  The length of the whole bar is in proportion to
// "T-tot" which is the length of a mandatory breath (equal to 60 / Respiratory
// Rate).  The I:E Ratio is displayed below the timing bar, centered under the
// division between Inspiratory Time and Expiratory Time.
//
// The timing bar scales according to the value of T-tot.  Its display is
// scaled to a maximum of 2, 5, 12 or 60 seconds.  When T-tot goes over
// 2 seconds the scale changes to 5, when it goes over 5 the scale changes
// to 12, etc.
//
// There is a color scheme encoded into the diagram.  The Inspiratory Time,
// Expiratory Time and I:E Ratio values are normally displayed as black on a
// light grey background with T-tot normally being white on a medium grey
// background.  If one of these settings is active (i.e. being adjusted by the
// user via a setting button - note that T-tot is active when Respiratory Rate
// is being adjusted) then the setting is highlighted, i.e.  displayed
// as black on a white background.  Also, whichever one of Inspiratory Time,
// Expiratory Time and I:E Ratio is the "constant-during-rate-change-setting"
// then its value must be displayed as white on black.  Note, however, that the
// "active" coloring takes precedence over the constant during rate change
// coloring.
//
// In Vent Setup, when the Respiratory Rate setting is active and the Mandatory
// Type is PCV, then the timing diagram must display padlocks which allow the
// adjustment of the Constant During Rate Change setting.  The padlocks are
// displayed above the axis, centered around the I:E Ratio pointer.
//
// In SPONT/CPAP mode, only the Inspiratory Time will be displayed.  The legend
// "Manual Insp" is displayed above the diagram indicating that the Inspiratory
// Time is the length of a Manual Inspiration.  The diagram is not scaled
// according to the value of T-tot (which has no meaning in this mode) but to
// three times the value of Inspiratory Time.
//
// Note that the 2, 5, 12 and 60 second scales as well as the length of
// graphical "pointers" have all been chosen so that no collisions will occur
// between graphics on the timing diagram.  Numerous graphics move when
// settings affecting breath timing are adjusted but there is never the
// case where a graphic (such as the I:E Ratio value) will be displayed
// on top of another graphic, thereby causing information to be obscured.
//
// The activate() method must be called every time before the diagram is
// displayed and the setActiveSetting() method is called whenever a timing
// setting is activated/deactivated, i.e. when the button which adjusts a
// Respiratory Rate, Inspiratory Time, Expiratory Time or I:E Ratio is pressed
// down or up then this method should be called.  Changes to any of the timing
// settings or Respiratory Rate are communicated via valueUpdate().
//---------------------------------------------------------------------
//@ Rationale
// Since several screens display a timing diagram it dictated that a
// class should be devoted to it.
//---------------------------------------------------------------------
//@ Implementation-Description
// The class contains quite a number of data members, mostly made up of the
// many graphics (boxes, lines, text, numeric fields) that are needed for the
// display of the diagram.
//
// Any event that might result in a change to the diagram eventually
// ends up in a call to updateDiagram_().  This private method contains
// the brains behind BreathTimingDiagram, it figures out where every
// graphic should go, what size it should be, what color it should be, if
// it should be displayed or not.
//
// We register for setting changes to all the settings which we display,
// i.e. Inspiratory Time, Expiratory Time, I:E Ratio and Respiratory Rate.
// Changes are communicated through the valueUpdate() method which
// simply calls updateDiagram_() in order to register the effect of the
// setting adjustments.
//
// The diagram can display the relationship between the normal ventilation
// timing parameters or the apnea ventilation timing parameters.  Which
// relationship will be displayed is determined by a flag passed to the
// constructor.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BreathTimingDiagram.ccv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 017   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 016 By: gdc    Date: 26-May-2007   SCR Number: 6330
//  Project:  Trend
//	Description:
//		Removed SUN prototype code.
//
//  Revision: 015 By: srp    Date: 28-May-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 014   By: gdc    Date:  20-Feb-2001    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Changed xAxis_ color to light blue so it shows up on screen dump.
//
//  Revision: 013  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  Minute Volume no longer displayed in Apnea Setup and Vent Setup
//         subscreens, therefore removed from this class
//
//  Revision: 012   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//      Optimized graphics library. Implemented Direct Draw mode for drawing
//      outside of BaseContainer refresh cycle.
//
//  Revision: 011  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 010  By:  sah	   Date:  15-Sep-1998    DCS Number: 5133
//  Project:  Color
//  Description:
//		Added mapping for new '[CL01005]' requirement.  Also, added
//		mapping for range/resolution requirements of set minute volume
//		([CL02004-7]).
//
//  Revision: 009  By:  yyy    Date:  01-Sep-1998    DR Number: N/A
//       Project:  BiLevel (R8027)
//       Description:
//             Initial coding.
//
//  Revision: 008  By:  gdc	   Date:  29-Jun-1998    DCS Number: N/A
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 007  By:  yyy    Date:  10-Mar-98    DR Number: 2749 
//    Project:  Sigma (R8027)
//    Description:
//      Fixed assertion due to setting modification in transition of ventsetup
//      and try to change the main setting area.
//
//  Revision: 006  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 005   By: sah    Date:  06-Oct-1997    DR Number:  2544
//  Project: Sigma (R8027)
//  Description:
//      Changed the calculation of the breath period to be based on
//      respiratory rate, instead of inspiratory time plus expiratory time,
//      because of "rounding" inaccuracies affecting the overall breath
//      period value even though respiratory rate remains unchanged.
//      Also, the expiratory time field is calculated from the calculated
//      breath period, and the current inspiratory time.
//      The 'MSECS_PER_SEC_' constant was changed from an 'Int32' to a 'Real32'.
//	Removed unneeded "rounding" of I:E ratio value.
//
//  Revision: 004  By:  hhd    Date:  22-AUG-97    DR Number:  2321
//       Project:  Sigma (R8027)
//       Description:
//				Cleaned up code modification made for DCS 2321.
//
//  Revision: 003  By:  hhd    Date:  20-AUG-97    DR Number:  2321
//       Project:  Sigma (R8027)
//       Description:
//				Added testable items.
//
//  Revision: 002  By:  hhd    Date:  18-AUG-97    DR Number:  2321
//       Project:  Sigma (R8027)
//       Description:
//             Converted decimal point to a comma for all European languages 
//					except for German.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "BreathTimingDiagram.hh"
#include "ConstantParmValue.hh"
#include "MiscStrs.hh"
#include "ModeValue.hh"
#include <math.h>

//@ Usage-Classes
#include "GuiFoundation.hh"
#include "GuiApp.hh"
#include "MathUtilities.hh"
#include "TextUtil.hh"
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 AXIS_X_ = 50;
static const Int32 AXIS_Y_ = 66;
static const Int32 AXIS_LENGTH_ = 300;  // evenly divisible by 2,5,12,60
static const Int32 BAR_HEIGHT_ = 20;
static const Int32 LINE_WIDTH_ = 2;
static const Int32 AXIS_HEIGHT_ = 6;
static const Int32 TICK_MARK_HEIGHT_ = 3;
static const Int32 IE_RATIO_POINTER_HEIGHT_ = 50;
static const Int32 IE_RATIO_Y_ = AXIS_Y_ - BAR_HEIGHT_
				+ IE_RATIO_POINTER_HEIGHT_;
static const Int32 INSP_TIME_POINTER_OFFSET_ = 9;
static const Int32 INSP_TIME_OUTSIDE_X_ = 1;
static const Int32 NUMERIC_FIELD_WIDTH_ = 42;
static const Int32 NUMERIC_FIELD_HEIGHT_ = BAR_HEIGHT_ - 1;
static const Int32 TICK_MARK_OFFSET_ = 1;
static const Int32 UNITS_OFFSET_ = 10;
static const Int32 PADLOCK_Y_ = 0;
static const Int32 PADLOCK_GAP_ = 12;
static const Int32 PADLOCK_POINTER_Y_ = 20;
static const Int32 PADLOCK_POINTER_HEIGHT_ = AXIS_Y_ - BAR_HEIGHT_
				- PADLOCK_POINTER_Y_;
static const Int32 MANUAL_INSP_Y_ = AXIS_Y_ - BAR_HEIGHT_ - 17;
static const Int32 CONTAINER_WIDTH_ = AXIS_X_ + AXIS_LENGTH_ + 80;
static const Int32 CONTAINER_HEIGHT_ = IE_RATIO_Y_ + BAR_HEIGHT_;
static const Int32 NUMERIC_FIELD_DIGITS_ = 3;

static const Real32  MSECS_PER_SEC_   = 1000.0;
static const Real32  SECS_PER_MINUTE_ = 60.0f;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathTimingDiagram()  [Constructor]
//
//@ Interface-Description
// Constructs a BreathTimingDiagram.  It is passed a Boolean, 'isInApneaMode',
// which determines if the diagram should display the Apnea versions of
// Respiratory Rate, Inspiratory Time, Expiratory Time and I:E Ratio or not.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all data members and sets any of the unchangeable features of
// any contained graphics, e.g. their color, position, size or precision.
// The size of the timing diagram is set and any drawables that need to
// be are appended to its container.  All settings which affect the
// diagram are registered for.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathTimingDiagram::BreathTimingDiagram(const Boolean isInApneaMode,
										 const Boolean isVentSetupDiag) :
		IS_IN_APNEA_MODE_(isInApneaMode),
		leftMarker_(AXIS_X_ - LINE_WIDTH_, AXIS_Y_ - BAR_HEIGHT_,
						LINE_WIDTH_,
						BAR_HEIGHT_ + AXIS_HEIGHT_ + TICK_MARK_HEIGHT_),
		rightMarker_(AXIS_X_ + AXIS_LENGTH_, AXIS_Y_,
						LINE_WIDTH_, AXIS_HEIGHT_ + TICK_MARK_HEIGHT_),
		xAxis_(AXIS_X_, AXIS_Y_, AXIS_LENGTH_, AXIS_HEIGHT_),
		inspiratoryTimeXAxis_(AXIS_X_, AXIS_Y_ + 1, 1, AXIS_HEIGHT_ - 2),
		expiratoryTimeXAxis_(AXIS_X_, AXIS_Y_ + 1, 1, AXIS_HEIGHT_ - 2),
		inspiratoryTimeBox_(AXIS_X_, AXIS_Y_ - BAR_HEIGHT_,	1, BAR_HEIGHT_),
		inspiratoryTimeOutsideBox_(INSP_TIME_OUTSIDE_X_, AXIS_Y_ - BAR_HEIGHT_,
						1, BAR_HEIGHT_),
		expiratoryTimeBox_(0, AXIS_Y_ - BAR_HEIGHT_, 1, BAR_HEIGHT_),
		expiratoryTimeOutsideBox_(0, AXIS_Y_ - BAR_HEIGHT_, 1, BAR_HEIGHT_),
		ieRatioBox_(0, IE_RATIO_Y_, 1, BAR_HEIGHT_),
		ieRatioPointer_(0, AXIS_Y_ - BAR_HEIGHT_, LINE_WIDTH_,
						IE_RATIO_POINTER_HEIGHT_),
		tTotOutsideBox_(0, 
						AXIS_Y_ + AXIS_HEIGHT_ 
							+ TICK_MARK_HEIGHT_ + TICK_MARK_OFFSET_, 
						1, BAR_HEIGHT_),
		tTotPointer_(0, AXIS_Y_ + BAR_HEIGHT_, LINE_WIDTH_,
						BAR_HEIGHT_ + AXIS_HEIGHT_ + TICK_MARK_HEIGHT_),
		inspiratoryTimePointer_(AXIS_X_ - INSP_TIME_POINTER_OFFSET_,
						AXIS_Y_ - BAR_HEIGHT_ / 2, 1, LINE_WIDTH_),
		inspiratoryTimePointerEnd_(0,
						AXIS_Y_ - BAR_HEIGHT_ / 2 - LINE_WIDTH_ / 2,
						2 * LINE_WIDTH_, 2 * LINE_WIDTH_),
		expiratoryTimePointer_(0,
						AXIS_Y_ - BAR_HEIGHT_ / 2, 1, LINE_WIDTH_),
		expiratoryTimePointerEnd_(0,
						AXIS_Y_ - BAR_HEIGHT_ / 2 - LINE_WIDTH_ / 2,
						2 * LINE_WIDTH_, 2 * LINE_WIDTH_),
		highlightBox_(0, 0, 1, BAR_HEIGHT_),
		zero_(TextFont::TWELVE, 1, CENTER),
		scale_(TextFont::TWELVE, 2, CENTER),
		unitsText_(MiscStrs::BREATH_TIMING_UNITS_LABEL),
		inspiratoryTime_(TextFont::TWELVE, NUMERIC_FIELD_DIGITS_, CENTER),
		expiratoryTime_(TextFont::TWELVE, NUMERIC_FIELD_DIGITS_, CENTER),
		inspiratoryTimePadlock_(0, PADLOCK_Y_,
						ConstantParmValue::INSP_TIME_CONSTANT),
		ieRatioPadlock_(0, PADLOCK_Y_,
						ConstantParmValue::IE_RATIO_CONSTANT),
		expiratoryTimePadlock_(0, PADLOCK_Y_,
						ConstantParmValue::EXP_TIME_CONSTANT),
		padlockPointer_(0, PADLOCK_POINTER_Y_,
						LINE_WIDTH_, PADLOCK_POINTER_HEIGHT_),
		manualInspText_(MiscStrs::BREATH_TIMING_MANUAL_INSP),
		tTot_(TextFont::TWELVE, NUMERIC_FIELD_DIGITS_, CENTER),
		activeSetting_(SettingId::NULL_SETTING_ID),
		isInManualInspMode_(FALSE),
		isInPadlockMode_(FALSE),
		IS_VENT_SETUP_DIAG_(isVentSetupDiag),
		forcedPadlockState_(BreathTimingDiagram::NONE)
{
	CALL_TRACE("BreathTimingDiagram::BreathTimingDiagram(Boolean apneaMode)");

	// Size the diagram's container
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Color graphics
	leftMarker_.setColor(Colors::BLACK);
	leftMarker_.setFillColor(Colors::BLACK);
	rightMarker_.setColor(Colors::BLACK);
	rightMarker_.setFillColor(Colors::BLACK);
	inspiratoryTimeXAxis_.setColor(Colors::GREEN);
	inspiratoryTimeXAxis_.setFillColor(Colors::GREEN);
	expiratoryTimeXAxis_.setColor(Colors::YELLOW);
	expiratoryTimeXAxis_.setFillColor(Colors::YELLOW);
	xAxis_.setColor(Colors::BLACK);
	xAxis_.setFillColor(Colors::LIGHT_BLUE);
	inspiratoryTimeBox_.setColor(Colors::LIGHT_BLUE);
	inspiratoryTimeBox_.setFillColor(Colors::LIGHT_BLUE);
	inspiratoryTimeOutsideBox_.setColor(Colors::WHITE);
	inspiratoryTimeOutsideBox_.setFillColor(Colors::LIGHT_BLUE);
	expiratoryTimeBox_.setColor(Colors::LIGHT_BLUE);
	expiratoryTimeBox_.setFillColor(Colors::LIGHT_BLUE);
	expiratoryTimeOutsideBox_.setColor(Colors::WHITE);
	expiratoryTimeOutsideBox_.setFillColor(Colors::LIGHT_BLUE);
	ieRatioBox_.setColor(Colors::WHITE);
	ieRatioBox_.setFillColor(Colors::LIGHT_BLUE);
	ieRatioPointer_.setColor(Colors::BLACK);
	tTotOutsideBox_.setColor(Colors::WHITE);
	tTotOutsideBox_.setFillColor(Colors::LIGHT_BLUE);
	ieRatioPointer_.setFillColor(Colors::BLACK);
	tTotPointer_.setColor(Colors::BLACK);
	tTotPointer_.setFillColor(Colors::BLACK);
	inspiratoryTimePointer_.setColor(Colors::BLACK);
	inspiratoryTimePointer_.setFillColor(Colors::BLACK);
	inspiratoryTimePointerEnd_.setColor(Colors::BLACK);
	inspiratoryTimePointerEnd_.setFillColor(Colors::BLACK);
	expiratoryTimePointer_.setColor(Colors::BLACK);
	expiratoryTimePointer_.setFillColor(Colors::BLACK);
	expiratoryTimePointerEnd_.setColor(Colors::BLACK);
	expiratoryTimePointerEnd_.setFillColor(Colors::BLACK);
	highlightBox_.setColor(Colors::WHITE);
	highlightBox_.setFillColor(Colors::GOLDENROD);
	zero_.setColor(Colors::WHITE);
	scale_.setColor(Colors::WHITE);
	unitsText_.setColor(Colors::WHITE);
	inspiratoryTime_.setColor(Colors::BLACK);
	expiratoryTime_.setColor(Colors::BLACK);
	padlockPointer_.setColor(Colors::BLACK);
	padlockPointer_.setFillColor(Colors::BLACK);
	ieRatioText_.setColor(Colors::BLACK);
	manualInspText_.setColor(Colors::WHITE);
	tTot_.setColor(Colors::BLACK);

	// Position and size numeric fields.
	ieRatioText_.setY(IE_RATIO_Y_ + 4);
	inspiratoryTime_.setY(AXIS_Y_ - BAR_HEIGHT_ + 1);
	inspiratoryTime_.setHeight(NUMERIC_FIELD_HEIGHT_);
	expiratoryTime_.setY(AXIS_Y_ - BAR_HEIGHT_ + 1);
	expiratoryTime_.setHeight(NUMERIC_FIELD_HEIGHT_);
	zero_.setX(AXIS_X_ - zero_.getWidth()/2 );
	zero_.setY(AXIS_Y_ + AXIS_HEIGHT_ + TICK_MARK_HEIGHT_ + TICK_MARK_OFFSET_);
	zero_.setHeight(NUMERIC_FIELD_HEIGHT_);
	zero_.setValue(0.0);
	zero_.setPrecision(ONES);
	scale_.setX(AXIS_X_ + AXIS_LENGTH_ - scale_.getWidth()/2);
	scale_.setY(AXIS_Y_ + AXIS_HEIGHT_ + TICK_MARK_HEIGHT_ + TICK_MARK_OFFSET_);
	scale_.setHeight(NUMERIC_FIELD_HEIGHT_);
	unitsText_.setX(rightMarker_.getX() + UNITS_OFFSET_);
	unitsText_.setY(AXIS_Y_ + AXIS_HEIGHT_ + TICK_MARK_HEIGHT_ + TICK_MARK_OFFSET_ + 5);
	manualInspText_.setX(AXIS_X_);
	manualInspText_.setY(MANUAL_INSP_Y_);
	tTot_.setY(AXIS_Y_ + AXIS_HEIGHT_ + TICK_MARK_HEIGHT_ + TICK_MARK_OFFSET_);
	tTot_.setWidth(NUMERIC_FIELD_WIDTH_);
	tTot_.setHeight(NUMERIC_FIELD_HEIGHT_);

	// All numeric settings must be to two places of decimals.  No matter what
	// the actual stored setting precision is, we want to display all numeric
	// values on the diagram with two places of decimals because it looks
	// better that way.
	inspiratoryTime_.setPrecision(HUNDREDTHS);
	expiratoryTime_.setPrecision(HUNDREDTHS);
	tTot_.setPrecision(HUNDREDTHS);

	// Add graphics to container.  Order of appending here is important so
	// that some graphics are displayed on top of others.  All background
	// boxes should be added first.
	addDrawable(&inspiratoryTimeBox_);
	addDrawable(&inspiratoryTimeOutsideBox_);
	inspiratoryTimeOutsideBox_.setShow(FALSE);
	addDrawable(&expiratoryTimeBox_);
	addDrawable(&expiratoryTimeOutsideBox_);
	expiratoryTimeOutsideBox_.setShow(FALSE);
	addDrawable(&tTotOutsideBox_);
	addDrawable(&highlightBox_);
	highlightBox_.setShow(FALSE);
	addDrawable(&ieRatioBox_);
	addDrawable(&leftMarker_);
	addDrawable(&rightMarker_);
	addDrawable(&xAxis_);
	addDrawable(&inspiratoryTimeXAxis_);
	addDrawable(&expiratoryTimeXAxis_);
	addDrawable(&tTotPointer_);
	addDrawable(&ieRatioPointer_);
	addDrawable(&zero_);
	addDrawable(&scale_);
	addDrawable(&unitsText_);
	addDrawable(&inspiratoryTime_);
	addDrawable(&expiratoryTime_);
	addDrawable(&ieRatioText_);
	addDrawable(&tTot_);
	addDrawable(&inspiratoryTimePadlock_);
	addDrawable(&expiratoryTimePadlock_);
	addDrawable(&ieRatioPadlock_);
	addDrawable(&padlockPointer_);

	initSubjectPtrs_();
								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathTimingDiagram  [Destructor]
//
//@ Interface-Description
// Destroys a Breath Timing Diagram.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathTimingDiagram::~BreathTimingDiagram(void)
{
	CALL_TRACE("BreathTimingDiagram::BreathTimingDiagram(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Activates a breath timing diagram.  Should always be called before
// the diagram is displayed allowing us to set the display correctly.
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the breath timing diagram's display.
// Checks whether the diagram should be displayed in Manual Inspiration
// mode (if the Mode is SPONT/CPAP and we're not in Apnea Setup) and, if so,
// displays or hides the appropriate graphics for this mode.
//
// $[01049] Only Insp Time displayed if mode is SPONT/CPAP ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingDiagram::activate(void)
{
	CALL_TRACE("BreathTimingDiagram::activate(void)");

	switch (forcedPadlockState_)
	{  		// $[TI3]
	case BreathTimingDiagram::NONE :				// $[TI4]
		{
			const SettingSubject*  pConstantParm =
											arrSubjectPtrs_[CONSTANT_PARM_];

			// padlock state not being forced, therefore if this diagram is for
			// the Vent Setup subscreen and constant parm is changeable, then
			// we're in padlock mode...
			isInPadlockMode_ = 
				  (IS_VENT_SETUP_DIAG_  &&
				   pConstantParm->getApplicability(Notification::ADJUSTED) ==
													 Applicability::CHANGEABLE);
		}
		break;
	case BreathTimingDiagram::FORCED_ON :			// $[TI5]

		isInPadlockMode_ = TRUE;
		break;
	case BreathTimingDiagram::FORCED_OFF :			// $[TI6]

		isInPadlockMode_ = FALSE;
		break;
	default :
		// unexpected forced padlock state...
		AUX_CLASS_ASSERTION_FAILURE(forcedPadlockState_);
		break;
	}

	inspiratoryTimePadlock_.setShow(isInPadlockMode_);
	expiratoryTimePadlock_.setShow(isInPadlockMode_);
	ieRatioPadlock_.setShow(isInPadlockMode_);
	padlockPointer_.setShow(isInPadlockMode_);

	if (isInPadlockMode_)
	{	  // $[TI7]

		// Activate padlock buttons...
		inspiratoryTimePadlock_.activate();
		expiratoryTimePadlock_.activate();
		ieRatioPadlock_.activate();
	}	 // $[TI8]

	// must activate padlocks BEFORE updating these subject pointers...
	updateSubjectPtrs_();

	const SettingSubject*  pModeSubject = getSubjectPtr_(SettingId::MODE);
	const DiscreteValue MODE = pModeSubject->getAdjustedValue();

	if (!IS_IN_APNEA_MODE_  &&
		(MODE == ModeValue::SPONT_MODE_VALUE ||	MODE == ModeValue::CPAP_MODE_VALUE))
	{													// $[TI1]
		isInManualInspMode_ = TRUE;			// Manual Inspiration mode
		addDrawable(&manualInspText_);		// Display "Manual Insp" legend
	}
	else
	{													// $[TI2]
		isInManualInspMode_ = FALSE;		// Non-Manual Inspiration mode
		removeDrawable(&manualInspText_);	// Remove "Manual Insp" legend
	}

	// Display or hide appropriate graphics according to the value of
	// isInManualInspMode_.
	expiratoryTimeBox_.setShow(!isInManualInspMode_);
	ieRatioBox_.setShow(!isInManualInspMode_);
	ieRatioPointer_.setShow(!isInManualInspMode_);
	tTotPointer_.setShow(!isInManualInspMode_);
	expiratoryTime_.setShow(!isInManualInspMode_);
	expiratoryTimeOutsideBox_.setShow(!isInManualInspMode_);
	expiratoryTimePointer_.setShow(!isInManualInspMode_);
	expiratoryTimePointerEnd_.setShow(!isInManualInspMode_);
	expiratoryTimeXAxis_.setShow(!isInManualInspMode_);
	ieRatioText_.setShow(!isInManualInspMode_);
	tTot_.setShow(!isInManualInspMode_);
	tTotOutsideBox_.setShow(!isInManualInspMode_);

	// No settings are active
	activeSetting_ = SettingId::NULL_SETTING_ID;

	// Update timing diagram
	updateDiagram_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Called when the Breath Timing diagram must be removed from the subscreen
//  it's in.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Deactivate the padlocks if necessary.  Detach itself from Setting's
//  Context Subject.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingDiagram::deactivate(void)
{
	CALL_TRACE("deactivate()");

	// No settings are active
	activeSetting_ = SettingId::NULL_SETTING_ID;

	if (isInPadlockMode_)
	{						// $[TI1]
		// Deactivate padlock buttons...
		inspiratoryTimePadlock_.deactivate();
		expiratoryTimePadlock_.deactivate();
		ieRatioPadlock_.deactivate();
	}						// $[TI2]


	for (Uint idx = 0u; idx < NUM_TIMING_SETTINGS_; idx++)
	{
		const SettingId::SettingIdType  SUBJECT_ID =
								(arrSubjectPtrs_[idx])->getId();

		detachFromSubject_(SUBJECT_ID, Notification::VALUE_CHANGED);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setActiveSetting
//
//@ Interface-Description
// Sets the currrently active setting, i.e. the setting which is currently
// being adjusted by the user.  Passed the appropriate setting id or
// SettingId::NULL_SETTING_ID if a setting was deactivated.  Also passed an
// optional Boolean which should be set to TRUE if we're currently in the Vent
// Setup subscreen (doesn't need to be passed otherwise).  This tells us
// whether we may need to display the padlocks or not.
//---------------------------------------------------------------------
//@ Implementation-Description
// Most of the work is done by updateDiagram_() which is called so that it can
// set any colors that need changing on the diagram.  However,
// activating/deactivating Respiratory Rate while in Vent Setup in PCV
// ventilation causes the Constant During Rate Change padlocks to
// display/undisplay and this feature is handled here.
//
// $[01094] 3 padlocks displayed when respiratory rate is selected.
// $[01315] Out of the three breath timing settings, only the one currently...
//---------------------------------------------------------------------
//@ PreCondition
// Makes sure that the settingId parameter contains an acceptable value.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingDiagram::setActiveSetting(SettingId::SettingIdType settingId)
{
	CALL_TRACE("setActiveSetting(settingId)");
 
	// Store the new active setting
	activeSetting_ = settingId;

	// Update the diagram
	updateDiagram_();
								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
// Called normally when a setting has changed.  We update the display of the
// timing diagram when this happens.  Passed the following parameters:
// >Von
//	settingId		The id of the setting which changed.
//	contextId		The context in which the change happened, either
//					ContextId::ACCEPTED_CONTEXT_ID or
//					ContextId::ADJUSTED_CONTEXT_ID.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// If the diagram is not shown on the screen then this method does
// nothing.  If it is shown then we call updateDiagram_() which takes
// care of displaying the changed setting.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingDiagram::valueUpdate(const Notification::ChangeQualifier,
								 const SettingSubject*)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	// If we're invisible, do nothing
	if (!isVisible())
	{													// $[TI1.1]
		return;
	}													// $[TI1.2]

	// Update the diagram
	updateDiagram_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDiagram_
//
//@ Interface-Description
// Updates the diagram.  Normally called when any change in the system
// might affect the diagram's display.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method is responsible for positioning, sizing, coloring, etc, all
// the graphics that go to make up the breath timing diagram.  We begin
// by deciding which value to scale the diagram to.  We then reset the
// colors of the graphics which have variable coloring.  Each changeable
// numeric value and the graphics that go along with them are now dealt
// with in turn, the values are set, position and sized.
//
// One of Inspiratory Time, Expiratory Time or I:E Ratio is constant
// during a rate change.  The appropriate one is colored white on black.
// Also, the setting (if any) which is currently active is colored
// black on white.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
BreathTimingDiagram::updateDiagram_(void)
{
	const SettingSubject*  pRespRate  = arrSubjectPtrs_[RESP_RATE_];
	const SettingSubject*  pInspTime  = arrSubjectPtrs_[INSP_TIME_];
	const SettingSubject*  pExpTime   = arrSubjectPtrs_[EXP_TIME_];
	const SettingSubject*  pIeRatio   = arrSubjectPtrs_[IE_RATIO_];

	const Boolean  IS_RESP_RATE_CHANGED  = pRespRate->isChanged();
	const Boolean  IS_INSP_TIME_CHANGED  = pInspTime->isChanged();
	const Boolean  IS_EXP_TIME_CHANGED   = pExpTime->isChanged();
	const Boolean  IS_IE_RATIO_CHANGED   = pIeRatio->isChanged();

	const BoundedValue  RESP_RATE_VALUE  = pRespRate->getAdjustedValue();
	const BoundedValue  INSP_TIME_VALUE  = pInspTime->getAdjustedValue();
	const BoundedValue  EXP_TIME_VALUE   = pExpTime->getAdjustedValue();
	const BoundedValue  IE_RATIO_VALUE   = pIeRatio->getAdjustedValue();

	// calculate "raw" timing values, in seconds...
	const Real32  RAW_INSP_TIME_IN_SECS =
							(INSP_TIME_VALUE.value / MSECS_PER_SEC_);
		
	const Real32  RAW_T_TOT_VALUE = (SECS_PER_MINUTE_ / RESP_RATE_VALUE.value);
	
	const Real32  RAW_EXP_TIME_IN_SECS =
								(RAW_T_TOT_VALUE - RAW_INSP_TIME_IN_SECS);

	// get the precision for the timing values, based on the precision of
	// inspiratory time's value (converted from milliseconds to seconds)...
	const Precision  TIMING_PRECISION =
							(Precision)(INSP_TIME_VALUE.precision - 3);

	// "round" all of the "raw" values based on the precision...
	const Real32  INSP_TIME_IN_SECS =
						::PreciseValue(RAW_INSP_TIME_IN_SECS, TIMING_PRECISION);
		
	const Real32  T_TOT_VALUE =
						::PreciseValue(RAW_T_TOT_VALUE, TIMING_PRECISION);
		
	const Real32  EXP_TIME_IN_SECS =
						::PreciseValue(RAW_EXP_TIME_IN_SECS, TIMING_PRECISION);

	Real32  scaleValue = 0.0;
	Real32  scaleToValue = 0.0;
	Int     boxWidth = 0;

	// Choose which value to scale to, either T-tot or the Inspiratory Time
	// value if in Manual Inspiration mode.
	if (isInManualInspMode_)
	{													// $[TI9.1]
		scaleToValue = INSP_TIME_IN_SECS;
	}
	else
	{													// $[TI9.2]
		scaleToValue = T_TOT_VALUE;
	}

	// Decide which scale to use, either a 2, 5, 12 or 60 scale.
	if (scaleToValue <= 2.0f)
	{													// $[TI10.1]
		scaleValue = 2.0f;
	}
	else if (scaleToValue <= 5.0f)
	{													// $[TI10.2]
		scaleValue = 5.0f;
	}
	else if (scaleToValue <= 12.0f)
	{													// $[TI10.3]
		scaleValue = 12.0f;
	}
	else
	{													// $[TI10.4]
		scaleValue = 60.0f;
	}

	// Reset colors to default
	inspiratoryTimeBox_.setColor(Colors::LIGHT_BLUE);
	inspiratoryTimeBox_.setFillColor(Colors::LIGHT_BLUE);
	inspiratoryTimeOutsideBox_.setColor(Colors::WHITE);
	inspiratoryTimeOutsideBox_.setFillColor(Colors::LIGHT_BLUE);
	inspiratoryTime_.setColor(Colors::BLACK);
	expiratoryTimeBox_.setColor(Colors::LIGHT_BLUE);
	expiratoryTimeBox_.setFillColor(Colors::LIGHT_BLUE);
	expiratoryTimeOutsideBox_.setColor(Colors::WHITE);
	expiratoryTimeOutsideBox_.setFillColor(Colors::LIGHT_BLUE);
	tTotOutsideBox_.setColor(Colors::WHITE);
	tTotOutsideBox_.setFillColor(Colors::LIGHT_BLUE);
	expiratoryTime_.setColor(Colors::BLACK);
	ieRatioBox_.setColor(Colors::WHITE);
	ieRatioBox_.setFillColor(Colors::LIGHT_BLUE);
	ieRatioText_.setColor(Colors::BLACK);
	tTot_.setColor(Colors::BLACK);
	highlightBox_.setShow(FALSE);

	// Inspiratory Time
	boxWidth = Int( INSP_TIME_IN_SECS * AXIS_LENGTH_ / scaleValue + 0.5 );
	if (boxWidth <= 0)
	{													// $[TI11.1]
		boxWidth = 1;		// Safeguard, box width of zero is illegal
	}													// $[TI11.2]
	inspiratoryTimeBox_.setWidth(boxWidth);
	inspiratoryTimeXAxis_.setWidth(boxWidth);
	inspiratoryTime_.setX(AXIS_X_ + inspiratoryTimeBox_.getWidth() / 2 -
										inspiratoryTime_.getWidth() / 2);
	inspiratoryTime_.setValue(INSP_TIME_IN_SECS);
	inspiratoryTime_.setItalic(IS_INSP_TIME_CHANGED);
	inspiratoryTime_.setHighlighted(IS_INSP_TIME_CHANGED);

	// Check to see if the Inspiratory Time value fits in its box
	if ((inspiratoryTime_.getX() < inspiratoryTimeBox_.getX())
		|| (inspiratoryTime_.getWidth() > inspiratoryTimeBox_.getWidth()))
	{													// $[TI12.1]
		// It doesn't so move inspiratory time outside the bar to the left and
		// use a pointer to point into the inspiratory time box
		inspiratoryTime_.setX(INSP_TIME_OUTSIDE_X_ + 2);
		inspiratoryTimeOutsideBox_.setShow(TRUE);
		addDrawable(&inspiratoryTimePointer_);
		addDrawable(&inspiratoryTimePointerEnd_);
		inspiratoryTimeOutsideBox_.setWidth(inspiratoryTime_.getWidth() + 4);
		inspiratoryTimePointer_.setWidth(inspiratoryTimeBox_.getX() +
					inspiratoryTimeBox_.getWidth() / 2 -
					inspiratoryTimePointer_.getX());
		inspiratoryTimePointerEnd_.setX(inspiratoryTimeBox_.getX() +
					inspiratoryTimeBox_.getWidth() / 2 -
					inspiratoryTimePointerEnd_.getWidth() / 2);
	}
	else
	{													// $[TI12.2]
		// It does so make sure the inspiratory time pointer is not displayed
		//inspiratoryTimeOutside = FALSE;
		inspiratoryTimeOutsideBox_.setShow(FALSE);
		removeDrawable(&inspiratoryTimePointer_);
		removeDrawable(&inspiratoryTimePointerEnd_);
	}

	// Scale
	scale_.setValue(scaleValue);

	// If we're in Manual Inspiration mode then we just need to color
	// Inspiratory Time if it is active and we've finished updating the
	// diagram as none of the succeeding graphics are displayed in this mode.
	if (isInManualInspMode_)
	{													// $[TI13.1]
		// Check to see if Inspiratory Time is the active setting
		if (activeSetting_ == SettingId::INSP_TIME)
		{												// $[TI14.1]
			// It is so color it white with goldenrod fill
			inspiratoryTimeBox_.setColor(Colors::WHITE);
			inspiratoryTimeBox_.setFillColor(Colors::GOLDENROD);
			inspiratoryTimeOutsideBox_.setColor(Colors::WHITE);
			inspiratoryTimeOutsideBox_.setFillColor(Colors::GOLDENROD);
			inspiratoryTime_.setColor(Colors::BLACK);
			inspiratoryTimePointer_.setColor(Colors::BLACK);
			inspiratoryTimePointer_.setFillColor(Colors::BLACK);
			inspiratoryTimePointerEnd_.setColor(Colors::BLACK);
			inspiratoryTimePointerEnd_.setFillColor(Colors::BLACK);

		}												// $[TI14.2]
		return;
	}													// $[TI13.2]

	// Expiratory Time
	boxWidth = Int( EXP_TIME_IN_SECS * AXIS_LENGTH_ / scaleValue + 0.5 );
	if (boxWidth <= 0)
	{													// $[TI15.1]
		boxWidth = 1;		// Safeguard, box width of zero or less is illegal
	}													// $[TI15.2]
	expiratoryTimeBox_.setWidth(boxWidth);
	expiratoryTimeXAxis_.setWidth(boxWidth);
	expiratoryTimeBox_.setX(AXIS_X_ + inspiratoryTimeBox_.getWidth() + 1);
	expiratoryTimeXAxis_.setX(AXIS_X_ + inspiratoryTimeBox_.getWidth() + 1);
	expiratoryTime_.setX(expiratoryTimeBox_.getX() +
							expiratoryTimeBox_.getWidth() / 2 -
							expiratoryTime_.getWidth() / 2);
	expiratoryTime_.setValue(EXP_TIME_IN_SECS);
	expiratoryTime_.setItalic(IS_EXP_TIME_CHANGED);
	expiratoryTime_.setHighlighted(IS_EXP_TIME_CHANGED);

	// Check to see if the Expiratory Time value fits in its box
	if (   (expiratoryTime_.getX() < expiratoryTimeBox_.getX())
		|| (expiratoryTime_.getWidth() > expiratoryTimeBox_.getWidth()))
	{													// $[TI16.1]
		// It doesn't so move expiratory time outside the bar to the right and
		// use a pointer to point into the expiratory time box
		addDrawable(&expiratoryTimePointer_);
		addDrawable(&expiratoryTimePointerEnd_);
		
		Uint32 boxCenter = 
			expiratoryTimeBox_.getX() + expiratoryTimeBox_.getWidth() / 2;

        expiratoryTimePointerEnd_.setX(boxCenter -
					expiratoryTimePointerEnd_.getWidth() / 2 );

		expiratoryTimePointer_.setX(boxCenter);
		
		expiratoryTimePointer_.setWidth(INSP_TIME_POINTER_OFFSET_ +
					expiratoryTimeBox_.getWidth() / 2);
					
		expiratoryTime_.setX(expiratoryTimePointer_.getX() +
							 expiratoryTimePointer_.getWidth() + 2);

		expiratoryTimeOutsideBox_.setShow(TRUE);
		expiratoryTimeOutsideBox_.setWidth(expiratoryTime_.getWidth() + 4);
		expiratoryTimeOutsideBox_.setX(expiratoryTime_.getX() - 2);
	}
	else
	{													// $[TI16.2]
		// It does so make sure the expiratory time pointer is not displayed
		expiratoryTimeOutsideBox_.setShow(FALSE);
		removeDrawable(&expiratoryTimePointer_);
		removeDrawable(&expiratoryTimePointerEnd_);
	}

	// I:E Ratio
	ieRatioPointer_.setX(expiratoryTimeBox_.getX() - 1);

	// Depending on the value of I:E Ratio we either display it as 1:val or
	// val:1.  Use cheap text to formulate the display.  sprintf will
	// round value to the correct precision for display.
	
	if (IE_RATIO_VALUE.value < 0.0)
	{													// $[TI17.1]
		// Formulate the cheap text for the 1:num display
		swprintf(ieRatioString_, L"{p=12,y=12,%c:1 : %.*f}",
					IS_IE_RATIO_CHANGED ? L'I' : L'N',
					- IE_RATIO_VALUE.precision,
					- IE_RATIO_VALUE.value);
	}
	else
	{													// $[TI17.2]
		// Formulate the cheap text for the num:1 display
		swprintf(ieRatioString_, L"{p=12,y=12,%c:%.*f : 1}",
					IS_IE_RATIO_CHANGED ? L'I' : L'N',
					- IE_RATIO_VALUE.precision,
					IE_RATIO_VALUE.value);
	}


	ieRatioText_.setHighlighted(IS_IE_RATIO_CHANGED);
	ieRatioText_.setText(ieRatioString_);
	ieRatioText_.setX(ieRatioPointer_.getX() - ieRatioText_.getWidth() / 2);

	// Wrap box around I:E Ratio text with 3 pixel gap to the left and right.
	ieRatioBox_.setX(ieRatioText_.getX() - 3);
	ieRatioBox_.setWidth(ieRatioText_.getWidth() + 6);

	// T-tot
	tTotPointer_.setX(AXIS_X_ + inspiratoryTimeBox_.getWidth() +
						expiratoryTimeBox_.getWidth());
	tTot_.setX(tTotPointer_.getX() - NUMERIC_FIELD_WIDTH_ / 2);

	tTot_.setValue(T_TOT_VALUE);
	tTot_.setItalic(IS_RESP_RATE_CHANGED);
	tTot_.setHighlighted(IS_RESP_RATE_CHANGED);

	// First, position the T-tot below timing bar
	tTot_.setY(AXIS_Y_ + AXIS_HEIGHT_ + TICK_MARK_HEIGHT_ + 1);
	tTotPointer_.setY(AXIS_Y_ - BAR_HEIGHT_ );
	tTotPointer_.setHeight(TICK_MARK_HEIGHT_ + AXIS_HEIGHT_ + BAR_HEIGHT_);

	// position it to the right of the I:E pointer
	if (tTot_.getX() <= ieRatioPointer_.getX() + LINE_WIDTH_ + 3)
	{													// $[TI19.1]
		tTot_.setX(ieRatioPointer_.getX() + LINE_WIDTH_ + 3);
	}													// $[TI19.2]

	// Now if T-tot is too near scale, position it above the timing bar
	if ( tTot_.getX() + tTot_.getWidth() > scale_.getX() )
	{													// $[TI18.1]
        // position it above the timing bar
        tTot_.setY(AXIS_Y_ - BAR_HEIGHT_ - TICK_MARK_HEIGHT_
					- NUMERIC_FIELD_HEIGHT_);
        tTotPointer_.setY(AXIS_Y_ - BAR_HEIGHT_ - TICK_MARK_HEIGHT_);
        tTotPointer_.setHeight(BAR_HEIGHT_ + AXIS_HEIGHT_ + TICK_MARK_HEIGHT_);

	}													// $[TI18.2]

	// Wrap box around T-tot text with 2 pixel gap to the left and right.
	tTotOutsideBox_.setX( tTot_.getX() - 2 );
	tTotOutsideBox_.setY( tTot_.getY() - 1 );
	tTotOutsideBox_.setWidth( tTot_.getWidth() + 4 );

	// Check to see if we need to adjust the position of padlocks
	if (isInPadlockMode_)
	{													// $[TI20.1]
		// Padlocks are displayed so let's position them (centered below
		// the I:E Ratio pointer).
		padlockPointer_.setX(ieRatioPointer_.getX());		

		// Center I:E Ratio padlock below the pointer
		ieRatioPadlock_.setX(padlockPointer_.getX() -
									ieRatioPadlock_.getWidth() / 2);

		// Put other two padlocks to left and right
		inspiratoryTimePadlock_.setX(ieRatioPadlock_.getX() -
						inspiratoryTimePadlock_.getWidth() - PADLOCK_GAP_);
		expiratoryTimePadlock_.setX(ieRatioPadlock_.getX() +
							ieRatioPadlock_.getWidth() + PADLOCK_GAP_);
	}													// $[TI20.2]

	// Display the active setting (if any) as a white on goldenrod box
	switch (activeSetting_)
	{
	case SettingId::RESP_RATE:
	case SettingId::APNEA_RESP_RATE:					// $[TI21.1]
		tTotOutsideBox_.setColor(Colors::WHITE);	
		tTotOutsideBox_.setFillColor(Colors::GOLDENROD);	
		break;

	case SettingId::INSP_TIME:
	case SettingId::PEEP_HIGH_TIME:
	case SettingId::APNEA_INSP_TIME:					// $[TI21.2]
		inspiratoryTimeBox_.setColor(Colors::WHITE);
		inspiratoryTimeBox_.setFillColor(Colors::GOLDENROD);
		inspiratoryTimeOutsideBox_.setColor(Colors::WHITE);
		inspiratoryTimeOutsideBox_.setFillColor(Colors::GOLDENROD);
		break;

	case SettingId::PEEP_LOW_TIME:
	case SettingId::EXP_TIME:							// $[TI21.3]
		expiratoryTimeBox_.setColor(Colors::WHITE);
		expiratoryTimeBox_.setFillColor(Colors::GOLDENROD);
		expiratoryTimeOutsideBox_.setColor(Colors::WHITE);
		expiratoryTimeOutsideBox_.setFillColor(Colors::GOLDENROD);
		break;

	case SettingId::HL_RATIO:
	case SettingId::IE_RATIO:							// $[TI21.4]
		ieRatioBox_.setColor(Colors::WHITE);
		ieRatioBox_.setFillColor(Colors::GOLDENROD);
		break;
				
	case SettingId::NULL_SETTING_ID:					// $[TI21.5]
		// No active setting, do nothing
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initSubjectPtrs_
//
//@ Interface-Description
//  Called to initialize array of subject pointers, arrSubjectPtrs_.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Fill the content of array with appropriate pointer values.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
BreathTimingDiagram::initSubjectPtrs_(void)
{
	CALL_TRACE("initSubjectPtrs_()");

	if (IS_IN_APNEA_MODE_)
	{	   // $[TI1]
		arrSubjectPtrs_[CONSTANT_PARM_] =
								getSubjectPtr_(SettingId::APNEA_CONSTANT_PARM);
		arrSubjectPtrs_[RESP_RATE_]     =
								getSubjectPtr_(SettingId::APNEA_RESP_RATE);
		arrSubjectPtrs_[INSP_TIME_]     =
								getSubjectPtr_(SettingId::APNEA_INSP_TIME);
		arrSubjectPtrs_[EXP_TIME_]      =
								getSubjectPtr_(SettingId::APNEA_EXP_TIME);
		arrSubjectPtrs_[IE_RATIO_]      =
								getSubjectPtr_(SettingId::APNEA_IE_RATIO);
	}
	else
	{	  // $[TI2]
		arrSubjectPtrs_[CONSTANT_PARM_] =
									getSubjectPtr_(SettingId::CONSTANT_PARM);
		arrSubjectPtrs_[RESP_RATE_]     =
									getSubjectPtr_(SettingId::RESP_RATE);
		arrSubjectPtrs_[INSP_TIME_]     =
									getSubjectPtr_(SettingId::INSP_TIME);
		arrSubjectPtrs_[EXP_TIME_]      =
									getSubjectPtr_(SettingId::EXP_TIME);
		arrSubjectPtrs_[IE_RATIO_]      =
									getSubjectPtr_(SettingId::IE_RATIO);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateSubjectPtrs_
//
//@ Interface-Description
//	Called to update array of Subject pointers.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Depending on whether we are in Bilevel or not, update the subject
//  pointer array accordingly.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
BreathTimingDiagram::updateSubjectPtrs_(void)
{
	CALL_TRACE("updateSubjectPtrs_()");

	// first, detach from all subjects...
	Uint idx = 0u;
	for (idx = 0u; idx < NUM_TIMING_SETTINGS_; idx++)
	{
		const SettingId::SettingIdType  SUBJECT_ID =
								(arrSubjectPtrs_[idx])->getId();
		detachFromSubject_(SUBJECT_ID, Notification::VALUE_CHANGED);
	}

	if (!IS_IN_APNEA_MODE_)
	{  // $[TI1] -- not in apnea mode; check for applicability change...
		SettingSubject*  pSubject = arrSubjectPtrs_[INSP_TIME_];

		if (pSubject->getApplicability(Notification::ADJUSTED) ==
												Applicability::INAPPLICABLE)
		{  // $[TI1.1] -- the current "INSP_TIME" setting is now inapplicable...
			// the INSP_TIME, EXP_TIME and IE_RATIO can each be one of two
			// settings; based on applicability, determine which set of
			// pointers to use...
			if (pSubject->getId() == SettingId::PEEP_HIGH_TIME)
			{  // $[TI1.1.1] -- PEEP high time is no longer applicable...
				arrSubjectPtrs_[INSP_TIME_] =
										getSubjectPtr_(SettingId::INSP_TIME);
				arrSubjectPtrs_[EXP_TIME_]  =
										getSubjectPtr_(SettingId::EXP_TIME);
				arrSubjectPtrs_[IE_RATIO_]  =
										getSubjectPtr_(SettingId::IE_RATIO);
			}
			else if (pSubject->getId() == SettingId::INSP_TIME)
			{  // $[TI1.1.2] -- insp time is no longer applicable...
				arrSubjectPtrs_[INSP_TIME_] =
									getSubjectPtr_(SettingId::PEEP_HIGH_TIME);
				arrSubjectPtrs_[EXP_TIME_]  =
									getSubjectPtr_(SettingId::PEEP_LOW_TIME);
				arrSubjectPtrs_[IE_RATIO_]  =
									getSubjectPtr_(SettingId::HL_RATIO);
			}
			else
			{
				// unexpected INSP_TIME setting...
				AUX_CLASS_ASSERTION_FAILURE(pSubject->getId());
			}
		}  // $[TI1.2] -- the current "INSP_TIME" setting is still applicable...
	}  // $[TI2] -- in apnea mode, no duplicity to resolve...

	// ...now, attach to all subjects...
	for (idx = 0u; idx < NUM_TIMING_SETTINGS_; idx++)
	{
		const SettingId::SettingIdType  SUBJECT_ID =
								(arrSubjectPtrs_[idx])->getId();
		attachToSubject_(SUBJECT_ID, Notification::VALUE_CHANGED);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
BreathTimingDiagram::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							BREATHTIMINGDIAGRAM, lineNumber,
							pFileName, pPredicate);
}
