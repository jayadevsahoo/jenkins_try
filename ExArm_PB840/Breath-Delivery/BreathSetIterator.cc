#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BreathSetIterator - Iterator class for BreathSet
//---------------------------------------------------------------------
//@ Interface-Description
//   This class provides access methods which enable objects to iterate through the 
//   BreathSet class.  An object may reset the index to point to the current breath
//   or access the previously accessed record.  This class has no effect
//   on the BreathSet.
//---------------------------------------------------------------------
//@ Rationale
//   It is necessary to have access to the records stored in the BreathSet
//   to calculate spiro averages and for use by breath delivery algorithms.
//   This class provides a safe means of accessing this information, with
//   no side effects on the BreathSet indicies, or the BreathSet records.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The iterator has a private index that is used to access the records 
//   stored in BreathSet.  The public methods provide a means of reseting 
//   the index to the beginning of the BreathSet, and for accessing the 
//   next available record.  When all records have been accessed, a NULL 
//   pointer is returned.
//   This class has a frozen copy of the currentRecord_ and the 
//   numRecords_ in the BreathSet when the iterator was last initialized.  
//   This protects the iterator from manipulation of these values in the 
//   BreathSet during the use of the iterator.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathSetIterator.ccv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 006  By:  sp    Date: 10-Apr-1997    DR Number: DCS 1867
//       Project:  Sigma (R8027)
//       Description:
//             Fixed Spont min volume calculation.
//
//  Revision: 004  By:  sp    Date: 25-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 003  By:  sp    Date:  9-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BreathSetIterator.hh"

//@ Usage-Classes

//@ End-Usage
#include "BreathSet.hh"

#include "BreathMiscRefs.hh"

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathSetIterator()  
//
//@ Interface-Description
//	Default Contstructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize data member index_ and numRecords_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathSetIterator::BreathSetIterator(void)
{
    CALL_TRACE("BreathSetIterator()");

    // $[TI1]
    index_ = 0;
    numRecords_ = 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathSetIterator()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathSetIterator::~BreathSetIterator(void)
{
    CALL_TRACE("~BreathSetIterator()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPreviousBreathRecord
//
//@ Interface-Description
//   This method takes no parameters, and returns a pointer to a
//   BreathRecord, or a NULL pointer. This method has to be used before 
//   method getPrevRespRateBreathRecord to exhaust the number of breath 
//   records that can be traced back up to when tidal volume change occured.
//---------------------------------------------------------------------
//@ Implementation-Description
//   If there is a record that can be traced back up to when tidal volume change
//   occured and numRecords_ > 0, a pointer to current index BreathRecord is returned.  
//   Otherwise a NULL pointer is returned.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
BreathRecord*
BreathSetIterator::getPreviousBreathRecord(void)
{
    CALL_TRACE("getPreviousBreathRecord(void)");
 
    BreathRecord* prevRecord = NULL;
 
    //If there are more breath records, return a pointer to the previous breath
    // record.
    if ( (numRecords_ >0) && (numTidalVolumeRecords_ >0) )
    {
        // $[TI1]
        prevRecord = &RBreathSet.breathRecord_[index_];
        index_--;
        if (index_ < 0)
	{
            // $[TI2]
            index_ = BREATH_HISTORY_TABLE_SIZE - 1;
	}
        // $[TI3]

        numRecords_--;
        numRespRateRecords_--;
        numTidalVolumeRecords_--;
    }
    // $[TI4]

    // NULL: $[TI5]
    // NOT NULL: $[TI6]
    return (prevRecord);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPrevRespRateBreathRecord
//
//@ Interface-Description
//   This method takes no parameters, and returns a pointer to a
//   BreathRecord, or a NULL pointer if there is no previous record.
//   This method has to be used after getPreviousBreathRecord has exhausted
//   all the breath records that can be traced back up to when tidal volume
//   change occured.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method is based on the fact that
//   numRespRateRecords_ >= numTidalVolumeRecords_.
//   If there is a record that can be traced back up to when respiratory rate change
//   occured and numRecords_ > 0, a pointer to the current index BreathRecord 
//   is returned.  Otherwise a NULL pointer is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
BreathRecord*
BreathSetIterator::getPrevRespRateBreathRecord(void)
{
    CALL_TRACE("getPrevRespRateBreathRecord(void)");

    BreathRecord* prevRecord = NULL;
 
    //If there are more breath records, return a pointer to the previous breath
    // record.
    if ( (numRecords_ >0) && (numRespRateRecords_>0) )
    {
	// $[TI1]
	prevRecord = &RBreathSet.breathRecord_[index_];
	index_--;
	if (index_ < 0)
	{
	    // $[TI2]
	    index_ = BREATH_HISTORY_TABLE_SIZE - 1;
	}
	// $[TI3]

        numRecords_--;
        numRespRateRecords_--;
    }
    // $[TI4]

    // NULL: $[TI5]
    // NOT NULL: $[TI6]
    return (prevRecord);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reset
//
//@ Interface-Description
//   This method takes a message as an argument and has no return value.
//   The message constains all the indexes that this object require.
//   It is called whenever it is necessary to reset the index to
//   the current breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Extract indexes of BreathSet from the argument msg and 
//   set data members in the following order:
//	(1) numRecords_ is embedded in the first order 8 bits(0x000000FF)
//	(2) index_ is embedded in the second order 8 bits(0x0000FF00)
//	(3) numTidalVolumeRecords_ is embedded in the third order 8 bits(0x00FF0000)
//	(4) numRespRateRecords_ is embedded in the fourth order 8 bits(0x3F000000)
//   numRecords_ is limited  to NUM_PREV_BREATH_RECORDS to prevent
//   overlapping of the first and last breath record when breath set
//   is full.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BreathSetIterator::reset(const Int32 msg)
{
    CALL_TRACE("reset(const Int32 msg)");
 
   numRecords_ = (msg & 0x000000FF);
   index_ = ((msg >> 8) & 0x000000FF);
   numTidalVolumeRecords_ = ((msg >> 16) & 0x000000FF);
   numRespRateRecords_ = ((msg >> 24) & 0x0000003F);

   if ( numRecords_ > NUM_PREV_BREATH_RECORDS)
   {
        // $[TI1]
        numRecords_ = NUM_PREV_BREATH_RECORDS;
   }
   // $[TI2]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BreathSetIterator::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BREATHSETITERATOR,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


