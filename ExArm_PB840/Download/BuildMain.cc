#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename:  BuildMain.cc - Main entry function for the
//   flash image build utility.
//---------------------------------------------------------------------
//@ Interface-Description
//	  The program entry point for the Sigma flash memory image
//	  builder. It parses the program arguments and determines
//	  a target type of the flash image to build, input S record file 
//    names, and flash image output file format.
//    At least one input file must be specified as a command argument.
//	  It also establishes search paths for the input Srecord files from
//    the environment variable, LoadPath. If LoadPath not defined, only
//    the current directory is searched for the input files.
//    When an absolute flash image is successfully built and written
//    out to an output file, the program exits with a zero exit value,
//    otherwise the program exits with a negative exit value.
//---------------------------------------------------------------------
//@ Implementation-Description
//    There is a default output file name for each target type, but
//    a user can override it with the '-o'  option.
//    The default output image file format is binary for the GUI
//    and BD target and Srecord format for the boot prom target type.
//-------------------------------------------------------------------
//@ Rationale
//    Every C or C++ program requires a main function that parses
//    and checks command arguments.
//---------------------------------------------------------------------
//@ Fault-Handling
//   Errors detected by the software and an appropriate error
//   message is generated to stdout.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/BuildMain.ccv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 004  By: gdc  Date: 23-Feb-2005  DR Number: 6148
//       Project:  NIV1
//       Description:
//             DCS 6148 - move to solaris
//             Changed to conform to 840 naming conventions.
//
//  Revision: 003  By: gdc  Date: 22-Feb-2005  DR Number: 6128
//       Project:  NIV1
//       Description:
//             DCS 6128 - move to solaris
//             using static instantiation of BuildFlash object instead of placement new
//             which is not supported by GNU compiler
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  <jhv>    Date:  22-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "BuildFlash.hh"
#include "DownloadTypes.hh"
#include <string.h>
#include <stdio.h>


char *const DefOutFiles[NUM_TYPES] =
{
	"BdLoad",
	"GuiLoad",
	"Bootprom",
};

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free Function:: main()
//
//@ Interface-Description
//	  The program entry point for the Sigma flash memory image
//	  builder. It parses the program arguments and determines
//	  a target type, input S record file names, flash image output
//    file format.
//    At least one input file must be specified as a command argument.
//	  It also establishes search paths for the input Srecord files from
//    the environment variable, LoadPath, if it not defined, only
//    the current directory is searched for the input files.
//    When an absolute flash image is successfully built and written
//    out to an output file, the program exits with a zero exit value,
//    otherwise the program exits with a negative exit value.
//---------------------------------------------------------------------
//@ Implementation-Description
//    There is a default output file names for each target type, but
//    a user can override it with the '-o'  option.
//    The default output image file format is binary for the GUI
//    and BD target and srecord format for the boot prom target type.

//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
main(int argc, char **argv )
{
char *pArg;
char  Usage[200];
TargetType targetType = NO_TARGET;
char *outFile = NULL;
char *inputFiles[MAX_INPUT_FILES + 1];
OutputImageFormat outForm = NO_FORMAT;
char name[MAX_FILE_NAME_LENGTH];
NoChkSumRange exclude;
Boolean performance = TRUE;
int i, nFiles = 0 ;

	BuildFlash::ProgName = argv[0];
	exclude.pStart = NULL;
	sprintf(Usage,
		"Usage: %s -{bd, gui, prom} [-o ofile ] [-ex hexaddr,hexaddr] [-srecord] [-binary] [-n] sfile [sfile ...]\n",
			BuildFlash::ProgName);


	if ( argc < 2 )
	{
		printf(Usage);
		return(-1);
	}

	for (i = 1; i < argc; i++ )
	{
		pArg = argv[i];
		switch(pArg[0])
		{
			case '-':
				switch(pArg[1])
				{
					case 'o':
			 			if ( pArg[2] )	
				  		{
							outFile = &pArg[2];
						}
						else
						{
							if ( ++i >= argc )
							{
								printf(Usage);
								BuildFlash::PrintErr( "must to specify an output file name after -o option\n");
								return(-1);
							}
							outFile = argv[i];
						}
						continue;
					case 's':
						if ( strcmp(&pArg[1], "srecord") == 0 )
						{
							outForm = SRECORD_FORMAT;
							continue;
						}
						printf(Usage);
						BuildFlash::PrintErr("unrecognized option %s,\n", pArg);
						return(-1);

					case 'e' :
						if ( strcmp (&pArg[1], "ex" ) == 0 )
						{
							if ( ++i >= argc )
							{
								printf(Usage);
								BuildFlash::PrintErr( "must to specify address range after -ex option\n");
								return(-1);
							}
							if ( exclude.pStart )
							{
								printf(Usage);
								BuildFlash::PrintErr("Only one -ex option is allowed\n");
								return(-1);
							}
							pArg = argv[i];
							if ( sscanf(pArg, "%X,%X", &exclude.pStart, &exclude.pEnd) != 2 )
							{
								printf(Usage);
								BuildFlash::PrintErr("-ex address range in a wrong format\n");
								return(-1);
							}
							continue;
						}
						printf(Usage);
						BuildFlash::PrintErr("unrecognized option %s,\n", pArg);
						return(-1);

					case 'n':
						performance = FALSE;
						continue;

					case 'b':
						if ( strcmp(&pArg[1], "binary") == 0 )
						{
							outForm = BINARY_FORMAT;
							continue;
						}

					default:
						if ( (targetType = BuildFlash::FindType(&pArg[1])) == NO_TARGET ) 
						{
							printf(Usage);
							BuildFlash::PrintErr("unsupported target type %s specified\n", pArg);
							return(-1);
						}
						continue;
				}
				continue;
			default:
				if ( nFiles >= MAX_INPUT_FILES )
				{
					BuildFlash::PrintErr("too many Srecord files specified, maximum 10 \n");
					return(-1);
				}
				inputFiles[nFiles++] = pArg;
				continue;
		}// switch(pArg[0])
	}

	if ( targetType == NO_TARGET )
	{
		printf(Usage);
		BuildFlash::PrintErr("a target type must be specified.\n");
		return(-1);
	}
	if ( nFiles == 0 )
	{
		printf(Usage);
		BuildFlash::PrintErr("At least one srecord input file must be specified.\n");
		return(-1);
	}
	if ( (targetType != BOOT_PROM)  && exclude.pStart)
	{
		BuildFlash::PrintErr("-ex option not supported for a GUI or BD target, -ex option ignored.\n");
		exclude.pStart = NULL;
		exclude.pEnd = NULL;
	}

	if ( outForm == NO_FORMAT )
	{
		if ( targetType == BOOT_PROM )
		{
			outForm = SRECORD_FORMAT;
		}
		else
		{
			outForm = BINARY_FORMAT;
		}
	}
	if ( outFile == NULL )
	{
		strcpy(name, DefOutFiles[targetType]);
		if ( outForm == BINARY_FORMAT )
		{
			strcat(name, ".abs");
		}
		else
		{
			strcat(name, ".srec");
		}
		outFile = name;
	}

	if ( BootFile::InitLoadPath() == FAILURE )
	{
		printf("%s: too many load paths are defined, max 10 paths.\n");
		return(-1);
	}
	inputFiles[nFiles] = NULL;

	static BuildFlash FlashBuilder_( 
		targetType, inputFiles, outFile, outForm , exclude, performance); 

	if ( FlashBuilder_.task() == FAILURE)

	{
		return(-1);
	}
	else
	{
		printf("%s: output file %s successfully created.\n",
				BuildFlash::ProgName, outFile);
		return(0);
	}
}
