#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  LowMandTidalVolSetting - Low Exhaled Mandatory Tidal Volume
//                                   Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the lower limit of
//  exhaled tidal volume (during a mandatory breath; in milliliters), below
//  which the "low exhaled mandatory tidal volume alarm" shall be activated.
//  This class inherits from 'BoundedSetting' and provides the specific
//  information needed for representation of low mandatory tidal volume.
//  This information includes the interval and range of this setting's
//  values, and this batch setting's new-patient value (see
//  'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, but 'updateConstraints_()' is
//  overridden to update this setting's dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/LowMandTidalVolSetting.ccv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date: 29-Apr-2009    SCR Number: 6478
//  Project:  840S
//  Description:
//      Corrected getNewPatientValue to return unclipped value.
//
//  Revision: 010   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 009  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      NIV mods. Added SRS requirement numbers.  
//
//  Revision: 008  By: gdc    Date: 18-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 007   By: gdc    Date: 15-DEC-2004    DR Number: 6144
//  Project:  NIV1
//  Description:
//	NIV specific changes. Transition setting based on invasive/NIV.
//
//  Revision: 006   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  eliminated 'get{Max,Min}Limit()' method, now defined by base class
//      *  incorporated circuit-specific new-patient values and ranges
//
//  Revision: 005   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.17.1.0) into Rev "BiLevel" (1.17.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "LowMandTidalVolSetting.hh"
#include "SettingConstants.hh"
#include "ModeValue.hh"
#include "PatientCctTypeValue.hh"
#include "VentTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

#define DEFAULT_MIN_VALUE  Real32(1.0f)

//  $[02178] -- The setting's range ...
//  $[02181] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	DEFINED_LOWER_ALARM_LIMIT_OFF,// absolute minimum...
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static const BoundedInterval  NODE3_ =
{
	DEFAULT_MIN_VALUE,	// change-point...
	(DEFAULT_MIN_VALUE - DEFINED_LOWER_ALARM_LIMIT_OFF),// resolution...
	ONES,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE2_ =
{
	100.0f,		// change-point...
	1.0f,		// resolution...
	ONES,		// precision...
	&::NODE3_
};
static const BoundedInterval  NODE1_ =
{
	400.0f,		// change-point...
	5.0f,		// resolution...
	ONES,		// precision...
	&::NODE2_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	2500.0f,		// absolute maximum...
	10.0f,		// resolution...
	TENS,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: LowMandTidalVolSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval for the setting.
//
//  The resolution for the "min" range below, is calculated by taking
//  the difference between the absolute minimum value and the next higher
//  change-point.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LowMandTidalVolSetting::LowMandTidalVolSetting(void)
: BatchBoundedSetting(SettingId::LOW_EXH_MAND_TIDAL_VOL,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  LOW_EXH_MAND_TIDAL_MAX_ID, // maxConstraintId...
					  LOW_EXH_MAND_TIDAL_MIN_ID) // minConstraintId...
{
	CALL_TRACE("LowMandTidalVolSetting()");
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~LowMandTidalVolSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LowMandTidalVolSetting::~LowMandTidalVolSetting(void)
{
	CALL_TRACE("~LowMandTidalVolSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is normally changeable except in CPAP when it is
//  not applicable.
//  $[LC02003]\b\ in CPAP, Vte mand to shall not be viewable
//  $[LC02001]\b\ transition from CPAP, Vte mand shall be viewable
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	LowMandTidalVolSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");
	Applicability::Id applicabilityId;

	const Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const DiscreteValue MODE = (qualifierId == Notification::ADJUSTED) ? 
							   pSetting->getAdjustedValue() : pSetting->getAcceptedValue();

	if ( ModeValue::CPAP_MODE_VALUE == MODE )
	{
		applicabilityId = Applicability::INAPPLICABLE;
	}
	else
	{
		applicabilityId = Applicability::CHANGEABLE;
	}

	return applicabilityId;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[02179] This setting's new-patient value ...
// $[LC02003]\c\ in NIV CPAP, Vte mand alarm shall be disabled
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	LowMandTidalVolSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	const Setting*  pVentType = SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE);

	const DiscreteValue  VENT_TYPE = pVentType->getAdjustedValue();

	BoundedValue  newPatient;

	switch ( VENT_TYPE )
	{
		case VentTypeValue::NIV_VENT_TYPE :					  // $[TI1.1]
			newPatient.value = DEFINED_LOWER_ALARM_LIMIT_OFF;
			newPatient.precision = ONES;
			break;

		case VentTypeValue::INVASIVE_VENT_TYPE :			  // $[TI1.2]
			{
				// The adjusted context contains the new-patient value for IBW ...
				const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);
				const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

				newPatient.value =
					(SettingConstants::NP_LOW_MAND_TIDAL_VOL_IBW_SCALE * IBW_VALUE);

				// warp the calculated value to a "click" boundary... 
				// but not clipped by current constraints (SCR #6478)
				getBoundedRange().warpValue(newPatient, BoundedRange::WARP_NEAREST, FALSE);
				break;
			}
		default:
			AUX_CLASS_ASSERTION_FAILURE(VENT_TYPE);
			break;
	}

	return(newPatient);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	LowMandTidalVolSetting::SoftFault(const SoftFaultID  softFaultID,
									  const Uint32       lineNumber,
									  const char*        pFileName,
									  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							LOW_EXH_MAND_TIDAL_VOL_SETTING, lineNumber,
							pFileName, pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NI02010]\e\ -- transitioning from Invasive to NIV
//  $[NI02011]\a\ -- transitioning from NIV to Invasive
//  $[LC02001]\c\ The VE TOT, VTE MAND and VTE SPONT  alarm limits shall transition to their new patient values.
//---------------------------------------------------------------------
//@ PreCondition
//  settingId == SettingId::VENT_TYPE || settingId == SettingId::MODE
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
	LowMandTidalVolSetting::acceptTransition(
											const SettingId::SettingIdType settingId,
											const DiscreteValue            newValue,
											const DiscreteValue        currValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	AUX_CLASS_PRE_CONDITION((settingId == SettingId::VENT_TYPE || settingId == SettingId::MODE), settingId);

	setAdjustedValue(getNewPatientValue());

	// $[TI1]
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	LowMandTidalVolSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	//-------------------------------------------------------------------
	// Determine maximum constraint...
	//-------------------------------------------------------------------

	const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

	BoundedRange::ConstraintInfo  maxConstraintInfo;

	const BoundedSetting*  pHighExhTidalVol =
		SettingsMgr::GetBoundedSettingPtr(SettingId::HIGH_EXH_TIDAL_VOL);

	// get the current value of high tidal volume alarm setting; this setting
	// must always be lower that the current high tidal volume setting...
	BoundedValue  highTidalVolBasedMax = pHighExhTidalVol->getAdjustedValue();

	// NOTE:  a '<=' comparison is used instead of a '<' comparison because
	//        of the decrement done to the value within the if clause, which
	//        equates an '<=' into an '<'...
	if ( highTidalVolBasedMax.value <= ABSOLUTE_MAX )
	{	// $[TI1]
		BoundStatus  dummyStatus(getId());

		// reset this range's maximum constraint to allow for "warping" of a
		// new maximum value...
		getBoundedRange_().resetMaxConstraint();

		// warp the calculated value to a "click" boundary...
		getBoundedRange_().warpValue(highTidalVolBasedMax);

		// calculate the value one "click" below this current high tidal volume
		// alarm value, by using low tidal volume's bounded range instance...
		getBoundedRange_().decrement(1, highTidalVolBasedMax, dummyStatus);

		// the high exhaled tidal volume-based maximum is more restrictive,
		// therefore save its value and id as the maximum bound value and id...
		maxConstraintInfo.id    = LOW_EXH_MAND_TIDAL_MAX_BASED_HIGH_ID;
		maxConstraintInfo.value = highTidalVolBasedMax.value;
	}
	else
	{	// $[TI2]
		// the absolute maximum is more restrictive, therefore save its value
		// and id as the maximum bound value and id...
		maxConstraintInfo.id    = LOW_EXH_MAND_TIDAL_MAX_ID;
		maxConstraintInfo.value = ABSOLUTE_MAX;
	}

	// this setting has no maximum soft bounds...
	maxConstraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute maximum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic maximum constraint.
//
//  $[02178] -- The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	LowMandTidalVolSetting::getAbsoluteMaxValue_(void) const
{
	CALL_TRACE("getAbsoluteMaxValue_()");

	Real32  absoluteMaxValue;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			absoluteMaxValue = 300.0f;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			absoluteMaxValue = 1000.0f;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			absoluteMaxValue = 2500.0f;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMaxValue);
}
