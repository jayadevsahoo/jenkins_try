#ifndef WaveformDataNum_HH
#define WaveformDataNum_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: WaveformDataNum - A gathering of global numerical constants
// relating to the display of waveform data.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformDataNum.hhv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

enum
{
	//@ Constant: MSECS_PER_WAVEFORM_SAMPLE
	// The number of milliseconds between each sample of waveform data that
	// comes from the BD system.
	MSECS_PER_WAVEFORM_SAMPLE = 20,

	//@ Constant: SECONDS_OF_WAVEFORM_DATA
	// The number of seconds of data that the stream stores.  64 was a
	// good number because it is slightly longer than the maximum gap
	// between valid breaths.
	SECONDS_OF_WAVEFORM_DATA = 64,

	//@ Constant: NUM_WAVEFORM_SAMPLES
	// The number of samples of data needed to store SECONDS_OF_WAVEFORM_DATA
	// worth of data.
	NUM_WAVEFORM_SAMPLES = SECONDS_OF_WAVEFORM_DATA
										* 1000 / MSECS_PER_WAVEFORM_SAMPLE
};

#endif // WaveformDataNum_HH 
