#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SmGasSupply - Gas supply detection and compressor control
//	for Service Mode
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is used to detect gas supply presence and to
//	automatically control the compressor state during Service Mode
//	and SST.  The compressor controller automatically sets the
//	compressor to the RUN state if wall air is disconnected,
//	and sets the compressor to the STANDBY state if wall air is
//	connected.
//---------------------------------------------------------------------
//@ Rationale
//	This class is required for gas supply detection and compressor
//	control for Service Mode and SST.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method newCycle() executes each cycle, updating the gas
//	presence flags and the compressor state.  Inline methods are
//	provided to allow external access to the gas presence flags.
//	In addition, the methods disableCompressorAndControl() and
//	enableCompressorAndControl() are provided to temporarily disable
//	and enable, respectively, the automatic compressor controller,
//	allowing manual compressor control, e.g. for the Compressor Test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmGasSupply.ccv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  syw    Date: 28-Sep-1998    DR Number: DCS 5181
//       Project:  BiLevel
//       Description:
//			Use RCompressor.smxxxx_() methods to control compressor during ac loss
//			directly.
//
//  Revision: 003  By:  quf    Date: 11-Sep-1997    DR Number: DCS 2280
//       Project:  Sigma (840)
//       Description:
//            Eliminated global variable LOW_AC_FACTOR, eliminated
//            use of local calculation of the low AC criteria and
//            now this is done via PowerSource::DetermineLowAcThresh().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date: 09-Feb-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================
#include "SmGasSupply.hh"

//@ Usage-Classes

#include "SmRefs.hh"

#include "VentObjectRefs.hh"
#include "MainSensorRefs.hh"
#include "MiscSensorRefs.hh"
#include "LinearSensor.hh"
#include "Compressor.hh"
#include "FlowSensor.hh"
#include "GasSupply.hh"
#include "NominalLineVoltValue.hh"
#include "PowerSource.hh"
#include "Task.hh"

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeGasSupply.hh"
# include "SmGasSupply_UT.hh"

# define RGasSupply fakeGasSupply
extern FakeGasSupply fakeGasSupply;
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmGasSupply()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.  No arguments.  Initializes private data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize private data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmGasSupply::SmGasSupply(void) :
 	 compressorControlEnabled_(TRUE),
 	 compressorState_(INIT),
	 isWallAirPresent_(FALSE),
	 isCompressorAirPresent_(FALSE),
	 isAnyAirPresent_(FALSE),
	 isO2Present_(FALSE),
	 isAnyGasPresent_(FALSE),
	 nominalVoltage_(120.0)
{
// $[TI1]
	CALL_TRACE("SmGasSupply::SmGasSupply(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SmGasSupply()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmGasSupply::~SmGasSupply(void)
{
	CALL_TRACE("~SmGasSupply()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//	This method periodically updates the states of various state flags, e.g.
//	the gas presence flags, and updates the compressor controller state.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Update the gas presence flags.
//	Update the AC low flag.
//	Update the compressor installed flag.
//	If compressor control is enabled, update the compressor state
//	based on the states of the updated flags.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmGasSupply::newCycle(void)
{
	CALL_TRACE("SmGasSupply::newCycle(void)");

	// check for a status of the wall air availability
	isWallAirPresent_ = RGasSupply.checkAirPresence();

	// Check for the status of the compressed air availability
	isCompressorAirPresent_ = RCompressor.smCheckCompressedAirPresence_();

	// Combine wall and compressor availability
	isAnyAirPresent_ = isWallAirPresent_ || isCompressorAirPresent_;

	// check for a status of the O2 availability:
	isO2Present_ = RGasSupply.checkO2Presence();

	// Combine air and o2 availability
	isAnyGasPresent_ = isAnyAirPresent_ || isO2Present_;


	// TODO E600 MS I don't think this is supported in E600
	/*
	// Check if AC power is low
	Boolean isAcLow;
	if ( RAcLineVoltage.getValue() <
			RPowerSource.DetermineLowAcThresh(nominalVoltage_) )
	{
	// $[TI1]
		isAcLow = TRUE;
	}
	else
	{
	// $[TI2]
		isAcLow = FALSE;
	}

#ifdef SIGMA_UNIT_TEST
	SmGasSupply_UT::IsAcLow = isAcLow;
#endif  // SIGMA_UNIT_TEST

	*/

// TODO E600 MS Take care of the compressor later when applicable

	/*
	// Check if compressor is installed
	Boolean isCompressorInstalled =	RCompressor.smCheckCompressorInstalled_();

	// Automatically set compressor to the proper state if
	// compressor control is enabled
	if (compressorControlEnabled_)
	{
	// $[TI3]
		if (isCompressorInstalled && !isAcLow)
		{
		// $[TI3.1]
			if (compressorState_ == INIT ||
				compressorState_ == DISABLED)
			{
			// $[TI3.1.1]
				RCompressor.smEnableCompressor_();
			}
			// $[TI3.1.2]
			// implied else

			if (isWallAirPresent_)
			{
			// $[TI3.1.3]
				RCompressor.smSetCompressorMotorReady_();
				compressorState_ = STANDBY;
			}
			else
			{
			// $[TI3.1.4]
				RCompressor.smSetCompressorMotorOn_();
				compressorState_ = RUN;
			}
			// end else
		}
		else
		{
		// $[TI3.2]
			RCompressor.smDisableCompressor_();
			compressorState_ = DISABLED;
		}
		// end else
	}
	// $[TI4]
	// implied else

	*/
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableCompressorAndControl()
//
//@ Interface-Description
//	This method enables the compressor controller.  No arguments,
//	no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Set compressorState_ = INIT.
//	Set compressorControlEnabled_ = TRUE to re-enable the compressor
//	controller in newCycle().
//	Wait 1 system cycle to allow newCycle() to set compressor state.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
SmGasSupply::enableCompressorAndControl(void)
{
// $[TI1]
    CALL_TRACE("SmGasSupply::enableCompressorAndControl(void)");

	compressorState_ = INIT;
	compressorControlEnabled_ = TRUE;
	Task::Delay( 0, 10);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableCompressorAndControl()
//
//@ Interface-Description
//	This method disables the compressor controller.  No arguments,
//	no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Set compressorControlEnabled_ = FALSE to shut down the compressor
//	controller in newCycle().
//	Set compressor manually to the disabled mode.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
SmGasSupply::disableCompressorAndControl(void)
{
// $[TI1]
    CALL_TRACE("SmGasSupply::disableCompressorAndControl(void)");

	compressorControlEnabled_ = FALSE;
// E600 BDIO 	RCompressor.smDisableCompressor_();

	// NOTE: compressorState_ is irrelevant when controlEnabled_ is
	// FALSE
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setNominalVoltage()
//
//@ Interface-Description
// This method takes no arguments and has no return value.  It sets the
// nominal AC voltage value.
//---------------------------------------------------------------------
//@ Implementation-Description
// The value for nominal voltage is stored in a setting.  The private
// data member nominalVoltage_ must be updated to the setting value.
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//     none
//@ End-Method
//=====================================================================
void
SmGasSupply::setNominalVoltage (DiscreteValue nominalLineVolt)
{
	CALL_TRACE("SmGasSupply::setNominalVoltage \
		(DiscreteValue nominalLineVolt)");

	switch (nominalLineVolt)
	{
		case NominalLineVoltValue::VAC_100:
		// $[TI1]
			nominalVoltage_ = 100.0;
			break;

		case NominalLineVoltValue::VAC_120:
		// $[TI2]
			nominalVoltage_ = 120.0;
			break;

		case NominalLineVoltValue::VAC_220:
		// $[TI3]
			nominalVoltage_ = 220.0;
			break;

		case NominalLineVoltValue::VAC_230:
		// $[TI4]
			nominalVoltage_ = 230.0;
			break;

		case NominalLineVoltValue::VAC_240:
		// $[TI5]
			nominalVoltage_ = 240.0;
			break;

		default:
		// $[TI6]
			CLASS_ASSERTION_FAILURE();
			break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SmGasSupply::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_MODE, SMGASSUPPLY,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
