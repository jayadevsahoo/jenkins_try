#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhValveTable - Exhalation Valve Table contains calibration
//		information for the valve.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class contains the Exhalation Valve Table and the exhalation
//		valve's current sensor gain and offset values.  Access methods are
//		provided to obtain the data.
//---------------------------------------------------------------------
//@ Rationale
//      Exhalation valve requires a table to store information for
//      commanding the exhalation valve.  This table is built during service
//      mode and is stored in flash memory, thus requiring a separate class.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The ExhValveTable is an array that stores the valve commands.  Data is
//		stored every 1 / STEPS_PER_CMH2O increments from 0 to PRESSURE_RANGE
//		cmH20.  The exhalation valve's current gain and offset are also stored.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      This class is to be referenced only by the ExhalationValve
//      object.  Only one instance of the class shall exist.
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ExhValveTable.ccv   25.0.4.0   19 Nov 2013 13:54:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003  By: syw    Date:  02-May-1996    DR Number: 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added dumpTable() for debugging.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added operator=(), setNormalizedCommand(), and
//			setNormalizedCommand() methods.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "ExhValveTable.hh"

#ifdef SIGMA_DEBUG
#include <stdio.h>
#endif // SIGMA_DEBUG

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhValveTable()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Initialize the table's normalized command, current gain and offset
//		to 0.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

ExhValveTable::ExhValveTable( void)
{
	CALL_TRACE("ExhValveTable::ExhValveTable( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExhValveTable()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

ExhValveTable::~ExhValveTable(void)
{
	CALL_TRACE("ExhValveTable::~ExhValveTable(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=()
//
//@ Interface-Description
//      This method has an ExhValveTable as an argument and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Each data member from the object passed in is copied to the current
//		object.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void
ExhValveTable::operator=( const ExhValveTable& evTable)
{
	CALL_TRACE("ExhValveTable::operator=( const ExhValveTable& evTable)") ;

	if (this != &evTable)
	{
        exhValveTable_ = evTable.exhValveTable_;
	}  	// implied else $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=()
//
//@ Interface-Description
//      This method has an ExhValveCalTable as an argument and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Struct containing the calibration data is copied into local calibration
//		data structure
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
ExhValveTable::ExhValveTable( const ExhValveCalTable& calTable)
{
	exhValveTable_ = calTable;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Copy constructor
//
//@ Interface-Description
//      This method has an ExhValveTable as an argument and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Each data member from the object passed in is copied to the current
//		object.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

ExhValveTable::ExhValveTable( const ExhValveTable& evTable)
{
	if (this != &evTable)
	{
        exhValveTable_ = evTable.exhValveTable_;
	}  	// implied else $[TI1.2]
}

#ifdef SIGMA_DEBUG

void
ExhValveTable::dumpTable( void)
{
	for (Uint16 ii=0; ii < EXH_VALVE_TABLE_SIZE; ii++)
	{
		if (!(ii % 10))
			printf( "\n%4d  ", ii) ;
		printf( "%4d ", getNormalizedCommand( ii)) ;
	}
}
#endif //SIGMA_DEBUG

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initializeToDefault
//
//@ Interface-Description
//      Sets the calibration data to default values
//---------------------------------------------------------------------
//@ Implementation-Description
//		The default calibration structure is copies into the calibration
//		structure.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void ExhValveTable::initializeToDefault()
{


	static const ExhValveCalTable defaultTable = 
	{
		{
			{
				
				// High to low drv values
				1.07, 3.09, 4.10, 5.02, 5.86, 6.65, 7.50, 8.38, 9.29, 10.11, 10.99, 
				11.87, 12.69, 13.52, 14.40, 15.27, 16.06, 17.00, 17.82, 18.63, 19.50, 
				20.39, 21.21, 21.96, 22.76, 23.64, 24.57, 25.36, 26.14, 26.79, 27.60, 
				28.42, 29.23, 30.24, 30.94, 31.77, 32.58, 33.45, 34.28, 35.18, 35.88, 
				36.81, 37.68, 38.62, 39.29, 40.23, 40.88, 41.86, 42.78, 43.53, 44.44, 
				45.19, 46.18, 46.91, 47.81, 48.71, 49.51, 50.39, 51.11, 52.03, 53.03, 
				53.81, 54.59, 55.50, 56.36, 57.10, 58.03, 58.91, 59.77, 60.45, 61.47, 
				62.21, 63.18, 63.98, 64.71, 65.53, 66.35, 67.15, 68.05, 69.03, 69.79, 
				70.70, 71.52, 72.36, 73.30, 74.16, 74.92, 75.97, 76.75, 77.47, 78.32, 
				79.13, 80.12, 80.98, 81.81, 82.50, 83.48, 84.21, 85.04, 86.03, 86.92
			},

	
			{	
				// High to low press values
				0.00, 0.99, 2.00, 2.99, 3.93, 4.95, 5.92, 6.93, 7.97, 8.88, 9.95, 
				10.97, 11.97, 12.89, 13.90, 14.97, 15.88, 16.94, 17.96, 18.95, 19.97, 
				20.98, 21.97, 22.89, 23.84, 24.91, 25.98, 26.88, 27.81, 28.90, 29.83, 
				30.85, 31.79, 32.97, 33.83, 34.81, 35.80, 36.85, 37.90, 38.92, 39.81, 
				40.87, 41.92, 42.96, 43.89, 44.97, 45.85, 46.95, 47.97, 48.85, 49.94, 
				50.87, 51.94, 52.84, 53.95, 54.96, 55.87, 56.96, 57.79, 58.89, 59.98, 
				60.97, 61.87, 62.95, 63.99, 64.82, 65.93, 66.95, 67.98, 68.83, 69.87, 
				70.92, 71.98, 72.99, 73.79, 74.86, 75.77, 76.78, 77.78, 78.89, 79.86, 
				80.92, 81.82, 82.97, 83.96, 85.00, 85.93, 87.00, 87.88, 88.95, 89.93, 
				90.92, 91.94, 92.94, 93.96, 94.92, 95.97, 96.93, 97.86, 98.93, 99.91
				
			},

	
			{ 
				// high to low DAC values
				1426, 1465, 1480, 1492, 1502, 1511, 1520, 1529, 1538, 1546, 1554, 
				1562, 1569, 1576, 1583, 1590, 1596, 1603, 1609, 1615, 1621, 
				1627, 1633, 1638, 1643, 1649, 1655, 1660, 1665, 1669, 1674, 
				1679, 1684, 1690, 1694, 1699, 1704, 1709, 1714, 1719, 1723, 
				1728, 1733, 1738, 1742, 1747, 1751, 1756, 1761, 1765, 1770, 
				1774, 1779, 1783, 1788, 1793, 1797, 1802, 1806, 1811, 1816, 
				1821, 1825, 1830, 1835, 1839, 1844, 1849, 1854, 1858, 1863, 
				1868, 1873, 1878, 1882, 1887, 1892, 1897, 1902, 1908, 1913, 
				1919, 1924, 1930, 1936, 1942, 1948, 1955, 1961, 1968, 1975, 
				1983, 1991, 1999, 2009, 2018, 2028, 2038, 2048, 2061, 2074
			}
		},

		{
			{ 
				// Low to high drv values
				0.00, 4.06, 5.36, 6.47, 7.49, 8.40, 9.32, 10.17, 10.99, 11.90, 12.79, 
				13.57, 14.38, 15.31, 16.18, 17.03, 17.81, 18.62, 19.52, 20.22, 21.04, 
				21.92, 22.78, 23.60, 24.39, 25.15, 25.91, 26.81, 27.61, 28.24, 29.04, 
				29.88, 30.64, 31.45, 32.18, 33.03, 33.83, 34.58, 35.41, 36.17, 37.05, 
				37.94, 38.71, 39.61, 40.34, 41.29, 42.06, 42.94, 43.76, 44.50, 45.46, 
				46.18, 47.07, 47.82, 48.72, 49.50, 50.26, 51.22, 52.24, 52.85, 53.78, 
				54.58, 55.32, 56.07, 57.14, 57.99, 58.77, 59.80, 60.52, 61.32, 62.06, 
				63.01, 63.88, 64.65, 65.60, 66.23, 67.20, 67.92, 68.96, 69.61, 70.53, 
				71.37, 72.23, 73.05, 73.87, 74.67, 75.53, 76.21, 77.24, 77.99, 78.88, 
				79.55, 80.49, 81.53, 82.36, 83.15, 83.94, 84.76, 85.54, 86.48, 87.21
			},

			{ 
				// Low to high press values
				0.58, 1.02, 2.05, 3.06, 4.09, 5.06, 6.10, 7.08, 8.00, 9.10, 10.13, 
				11.07, 12.01, 13.07, 14.05, 15.14, 16.05, 17.05, 18.15, 19.09, 20.01, 
				21.10, 22.08, 23.20, 24.07, 25.05, 26.00, 27.19, 28.16, 29.04, 30.10, 
				31.09, 32.17, 33.13, 34.04, 35.10, 36.18, 37.06, 38.07, 39.00, 40.05, 
				41.10, 42.08, 43.19, 44.08, 45.19, 46.11, 47.19, 48.16, 49.04, 50.20, 
				51.12, 52.13, 53.08, 54.10, 55.12, 56.02, 57.17, 58.21, 59.02, 60.18, 
				61.14, 62.07, 63.01, 64.17, 65.14, 66.09, 67.25, 68.15, 69.06, 70.01, 
				71.24, 72.22, 73.12, 74.13, 75.15, 76.11, 77.06, 78.30, 79.01, 80.24, 
				81.16, 82.11, 83.13, 84.06, 85.03, 86.15, 87.00, 88.14, 89.11, 90.02, 
				91.02, 92.03, 93.19, 94.09, 95.10, 96.18, 97.02, 98.08, 99.09, 100.08
			},

			{ 
				// Low to high DAC values
				999, 1568, 1586, 1600, 1612, 1622, 1632, 1641, 1649, 1658, 1666, 
				1673, 1680, 1688, 1695, 1702, 1708, 1714, 1721, 1726, 1732, 
				1738, 1744, 1750, 1755, 1760, 1765, 1771, 1776, 1780, 1785, 
				1790, 1795, 1800, 1804, 1809, 1814, 1818, 1823, 1827, 1832, 
				1837, 1841, 1846, 1850, 1855, 1859, 1864, 1868, 1872, 1877, 
				1881, 1885, 1889, 1894, 1898, 1902, 1907, 1912, 1915, 1920, 
				1924, 1928, 1932, 1937, 1941, 1945, 1950, 1954, 1958, 1962, 
				1967, 1971, 1975, 1979, 1983, 1987, 1991, 1996, 1999, 2004, 
				2008, 2012, 2016, 2020, 2024, 2029, 2032, 2037, 2041, 2045, 
				2049, 2053, 2058, 2062, 2066, 2070, 2074, 2078, 2082, 2086
			}
		}
	};

	exhValveTable_ = defaultTable;
	
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ExhValveTable::SoftFault( const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;

    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, EXHVALVETABLE,
                             lineNumber, pFileName, pPredicate) ;
}

void ExhValveTable::printHighToLowValues()
{
	DEBUG_MSG( "Printing high to low\n" );
	DEBUG_MSG( "Printing exh drv pressure: " );
	printDataArray ( exhValveTable_.highToLowData.exhPressDrvSamples );
	DEBUG_MSG( "Printing exh pressure: " );
	printDataArray ( exhValveTable_.highToLowData.exhPressSamples );
	DEBUG_MSG( "Printing exh DAC count: " );
	printDataArray ( exhValveTable_.highToLowData.dacSamples, FALSE );
}

void ExhValveTable::printLowToHighValues()
{
	DEBUG_MSG( "Printing low to high\n" );
	DEBUG_MSG( "Printing exh drv pressure: " );
	printDataArray ( exhValveTable_.lowToHighData.exhPressDrvSamples );
	DEBUG_MSG( "Printing exh pressure: " );
	printDataArray ( exhValveTable_.lowToHighData.exhPressSamples );
	DEBUG_MSG( "Printing exh DAC count: " );
	printDataArray ( exhValveTable_.lowToHighData.dacSamples, FALSE );
}
void ExhValveTable::printTableContents()
{
	printLowToHighValues();
	printHighToLowValues();
}


void ExhValveTable::printDataArray( Real32 *arrayPtr, Boolean printAsReal32 )
{
	std::stringstream ss;
	ss << std::setprecision(2);
	ss << std::fixed;
	ss << std::endl;

	for ( Uint32 idx = 0; idx < EXH_VALVE_DATA_ARRAY_SIZE; idx++ )
	{
		ss <<  " " << arrayPtr[idx];

		if ( idx > 0 && idx % 10 == 0 )
		{
			ss << "\n";
		}
	}

	DEBUG_MSG( ss.str().c_str() );
}
//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================




