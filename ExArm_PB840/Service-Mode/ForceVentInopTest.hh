#ifndef ForceVentInopTest_HH
#define ForceVentInopTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ForceVentInopTest - performs the forced vent inop test.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ForceVentInopTest.hhv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  05-Apr-96    DR Number: 1861, 1865, 2525
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

extern const Real32 PSOL_LOOPBACK_OFFSET;
extern const Real32 PSOL_LOOPBACK_AMPS_TO_VOLTS;
extern const Real32 EV_LOOPBACK_AMPS_TO_VOLTS;
extern const Real32 SV_LOOPBACK_OFFSET;
extern const Real32 SV_LOOPBACK_AMPS_TO_VOLTS;
extern const Real32 ADC_OFFSET;

class ForceVentInopTest {
  public:

  	enum PhaseId {
  		START,
		GUI_VENT_INOP = START,
  		VENT_INOP_A,
  		VENT_INOP_B,
  		TEN_SECOND_TIMER_A,
  		TEN_SECOND_TIMER_B,

		END_OF_PHASE_IDS
  	} ;

    ForceVentInopTest( void) ;
    ~ForceVentInopTest( void) ;

#if defined (SIGMA_BD_CPU)
	ServiceMode::TestStatus getStatus( void) const ;
#endif // defined (SIGMA_BD_CPU)

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32 lineNumber,
						   const char *pFileName  = NULL, 
						   const char *pPredicate = NULL) ;
#if defined (SIGMA_BD_CPU)
	void initialize( void) ;
	static void DisablePostTimers(void);
	void reset(void);
#endif // defined (SIGMA_BD_CPU)

	void runTest( void) ;
	  
  protected:

  private:
    ForceVentInopTest( const ForceVentInopTest&) ;	// not implemented...
    void  operator=( const ForceVentInopTest&) ;	// not implemented...

#if defined (SIGMA_BD_CPU)
	// ***** BD private methods *****
	Uint32 checkVentInopStates_( const Boolean userPrompt) ;
	void waitForPowerDown_( void) ;
	void displayCurrentPhase_( void) ;
	void processError_( Uint32 error) ;
	Uint32 getUserResponse_( void) ;

#else
	// ***** GUI private methods *****

#endif  // defined (SIGMA_BD_CPU)
	
	
	//@ Data-Member: phaseId_
	// stores which phase of test we are at
	PhaseId phaseId_ ;

	//@ Data-Member: testPassed_
	// vent inop test passed flag (TRUE = passed)
	Boolean testPassed_ ;
} ;


#endif // ForceVentInopTest_HH 
