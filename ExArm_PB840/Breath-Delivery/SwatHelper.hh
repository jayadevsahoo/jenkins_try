//-------------------------------------------------------------------------------
//                        Copyright (c) 2012 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------

#include"MessageProcessor.h"
#include"BatchJobManager.h"
#include<list>
namespace swat
{
    extern std::list<swat::BatchCommandInfo *> g_List;
    extern MessageProcessor g_clMessageProcessor;
    extern BatchJobManager  g_clBatchJobManager;


    //--------------------------------------------------------------------
    /// getNextBatch
    ///
    /// Return a pointer to the next batch
    ///
    /// @return pointer to BatchCommandInfo
    //--------------------------------------------------------------------
    BatchCommandInfo *getNextBatch();

    //--------------------------------------------------------------------
    /// enqueueMessage
    ///
    /// Puts a message on the list
    ///
    /// @return success
    //--------------------------------------------------------------------
    bool enqueueMessage(BatchCommandInfo* p);

    void update();

}
