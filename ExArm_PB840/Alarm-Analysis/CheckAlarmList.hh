#ifndef CheckAlarmList_HH
#define CheckAlarmList_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CheckAlarmList - derived from template class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/CheckAlarmList.hhv   25.0.4.0   19 Nov 2013 13:51:20   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "MutableR_Alarm_NUM_ALARM.hh"

class Alarm;
//@ End-Usage


class CheckAlarmList : public FixedMutableSortR(Alarm,NUM_ALARM) 
{

  public:
    CheckAlarmList  (void);
    CheckAlarmList  (const CheckAlarmList& rList);
    ~CheckAlarmList (void);

    inline void   operator=(const CheckAlarmList& rList);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
  protected:

  private:


};


// Inlined methods
#include "CheckAlarmList.in"


#endif // CheckAlarmList_HH 
