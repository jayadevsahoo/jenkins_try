 
#ifndef SmSwitchConfirmation_HH
#define SmSwitchConfirmation_HH

//=====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett  of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor Puritan Bennett  of California.
//
//            Copyright (c) 1995, Nellcor Puritan Bennett 
//=====================================================================

//====================================================================
// Class: SmSwitchConfirmation - monitors the Service Mode Switch and
//   determines if the operator has requested that SST be started
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SmSwitchConfirmation.hhv   25.0.4.0   19 Nov 2013 14:00:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp   Date:  25-Feb-1997    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  kam   Date:  06-Nov-1995    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial release.
//
//=====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "TimerTarget.hh"
#  include "UiEvent.hh"

//@ Usage-Classes
class IntervalTimer;
//@ End-Usage


extern const Int32 SM_SWITCH_INTERVAL_MS;
extern const Int32 SM_SWITCH_COUNT_THRESHOLD;

class SmSwitchConfirmation: public TimerTarget
{
    public:

    SmSwitchConfirmation(IntervalTimer& rTimer);
    virtual ~SmSwitchConfirmation(void);

    static void SoftFault(const SoftFaultID softFaultID,
	  const Uint32      lineNumber,
	  const char*       pFileName  = NULL, 
	  const char*       pPredicate = NULL);

    static EventData::EventStatus RequestSmSwitchConfirmation(const EventData::EventId id,
                                                         const Boolean eventStatus);

    static void SetIsOkayToEnterSm (const Boolean flag);
    
    virtual void timeUpHappened(const IntervalTimer& timer);

  protected:

  private:
    SmSwitchConfirmation(const SmSwitchConfirmation&);   // not implemented...
    void operator=(const SmSwitchConfirmation&);    // not implemented...
    SmSwitchConfirmation(void);  // not implemented...

    //@ Data-Member:& rSmSwitchConfirmationTimer_
    // a reference to the interval timer
    IntervalTimer& rSmSwitchConfirmationTimer_;

    //@ Data-Member: IsOkayToEnterSm_
    // indicates if it is okay for the operator to enter SST
    static Boolean IsOkayToEnterSm_;

    //@ Data-Member: OperatorTimeoutCount_
    // Counter used to determine when the operator timeout for pressing the service
    // mode switch has expired
    static Int32 OperatorTimeoutCount_;

    //@ Data-Member: CurrentUserEvent_
    // Variable used to determine which user event is currently associated with the
    // Service Mode switch.  The operator uses similar mechanisms to enter SST.
    static EventData::EventId CurrentUserEvent_;
};


#endif // SmSwitchConfirmation_HH 
