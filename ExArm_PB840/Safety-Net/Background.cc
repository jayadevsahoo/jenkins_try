#include "stdafx.h"
//=============================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.             
//                                            
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=============================================================================

//============================ C L A S S   D E S C R I P T I O N ==============
//@ Class: Background - Background Fault Handling Utilities
//-----------------------------------------------------------------------------
//@ Interface-Description
//  This provides Background event utilities for other subsystems.
//-----------------------------------------------------------------------------
//@ Rationale
//  This class contains utilities for handling and process of Background events.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  When the Safety-Net Subsystem, which resides in Background Task(s),
//  Breath-Delivery Subsystem, or BD-IO-Devices Subsystems, detect a
//  failure, a background event is declared by passing an identifer to
//  the ReportBkEvent method of this class.  BkEventsNames are keyed to
//  alarm identifers.
//
//  The methods of this class are static and require no instantiation.
//  It is important to note that these methods are processed in the thread
//  of the calling task.
//----------------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//----------------------------------------------------------------------------
//@ Restrictions
//  None
//----------------------------------------------------------------------------
//@ Invariants
//  None
//----------------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/Background.ccv   25.0.4.1   20 Nov 2013 16:07:42   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 010  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software.
// 
//  Revision: 009  By:  gdc    Date:  10-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 008 By: srp    Date: 28-May-2002   DR Number: 5901
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 007  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006  By: gdc   Date:  12-Oct-1998    DR Number: DCS 5164
//      Project:  BiLevel (840)
//      Description:
//		  Changed to immediately take the hardware into vent-INOP on a
//		  BTAT 1 and to queue the event to the Background Fault handler
//		  for "normal" processing instead of calling
//		  BackgroundFaultHandler::ProcessBkEvent() directly.  The
//		  latter caused task monitor failures on BdExec.
//
//  Revision: 005  By: iv    Date:  07-Jan-1998    DR Number: DCS 5000
//      Project:  Sigma (840)
//      Description:
//        Added a Uint16 default argument to methods ReportBkEvent() and
//        LogDiagnosticCodeUtil().
//
//  Revision: 004  By: iv    Date:  06-Aug-1997    DR Number: DCS 2032
//      Project:  Sigma (840)
//      Description:
//        Added a constant BK_TEST_10SEC_DELAY.
//
//  Revision: 003   By: iv    Date: 06-May-1997   DR Number: DCS 2040 
//    Project:  Sigma (R8027)
//    Description:
//      Changed BK_TEST_28SEC_DELAY to BK_TEST_60SEC_DELAY.
//
//  Revision: 002   By: iv    Date: 29-Apr-1997   DR Number: None 
//    Project:  Sigma (R8027)
//    Description:
//      Rework per unit test peer review.
//
//  Revision: 001   By: by   Date: 10-DEC-1995   DR Number: None
//    Project:  Sigma (840)
//    Description:
//      Initial version.
//
//=============================================================================

#include "Background.hh"                             
#include "BackgroundCycleApp.hh"
#include "BackgroundFaultHandler.hh" 

#if defined( SIGMA_BD_CPU )
#include "BackgroundMaintApp.hh"
#include "VentStatus.hh"
#endif // defined( SIGMA_BD_CPU ) 

//@ Usage-Classes

#include "AlarmQueueMsg.hh"
#include "MsgQueue.hh"
#include "BackgroundMsg.hh"
#include "MsgTimer.hh"
#include "DiagnosticCode.hh"
#include "TaskControlAgent.hh"
#include "BtatEventState.hh"
#include "Task.hh"
#include "TaskInfo.hh"
#include "NovRamManager.hh"
#include "Post.hh"
//TODO E600 remove or port if needed #include "Watchdog.hh"

#if defined( SIGMA_SAFETY_NET )
#include <stdio.h>
#endif // defined( SIGMA_SAFETY_NET )

///////////////////////////////////////////////////////////////////////////////
//     Declare Global Time Constant
///////////////////////////////////////////////////////////////////////////////
//@ Constant: BACKGROUND_CYCLE_TIME 
// 500 MS the basic cycle time for Background Tests
const Uint16 BACKGROUND_CYCLE_TIME = 500;

//@ Constant: BK_TEST_FREQ_500MS
// scheduling frequency for a 500 MS Background Test
const Int32 BK_TEST_FREQ_500MS = 1;

//@ Constant: BK_TEST_FREQ_1SEC
// scheduling frequency for a 1 Second Background Test
const Int32 BK_TEST_FREQ_1SEC = 2;

//@ Constant: BK_TEST_FREQ_10SEC 
// scheduling frequency for a 10 Second Background Test
const Int32 BK_TEST_FREQ_10SEC = 20;

//@ Constant: BK_TEST_NO_DELAY 
// for no initial delay to a Background Test
const Int32 BK_TEST_NO_DELAY = 0;

//@ Constant: BK_TEST_60SEC_DELAY 
// for a 60 second delay to a Background Test
const Int32 BK_TEST_60SEC_DELAY = -120;

//@ Constant: BK_TEST_10SEC_DELAY 
// for a 10 second delay to a Background Test
const Int32 BK_TEST_10SEC_DELAY = -20;

//@ Constant: NOT_FOUND 
// value returned by BtatEventState class
// when an item is not found by findEntry()
const Int16 NOT_FOUND = -1;

//@ Constant: BK_TOTAL_EVENTS_POSSIBLE 
// maximum possible number of background events
static const Uint32 BK_TOTAL_EVENTS_POSSIBLE = 256u;

//////////////////////////////////////////////////////////////////////////////
// Initialize static data
//////////////////////////////////////////////////////////////////////////////
Boolean Background::TimeHasChangedFlag_ = FALSE;
Boolean Background::PostMinorErrorOccurred_ = FALSE;

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method:  Initialize
//
//@ Interface-Description
//  This method accepts no parameter and returns no value.
//  This method initializes the background classes.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  This is called during system initialization to setup the Background
//  Tasks to a known state.  It calls each initialiation method of the
//  background classes.
//-----------------------------------------------------------------------------
//@ PreCondition
//  None
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

void
Background::Initialize( void )
{
    // $[TI1]
    CLASS_PRE_CONDITION(BK_MAX_EVENT < BK_TOTAL_EVENTS_POSSIBLE);
    // Initialize the tasks in this subsystem
    BackgroundFaultHandler::Initialize();

    BackgroundCycleApp::Initialize();

#if defined( SIGMA_BD_CPU )
    BackgroundMaintApp::Initialize();
#endif // defined( SIGMA_BD_CPU ) 

}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method:  ReportBkEvent
//
//@ Interface-Description
//  This method accepts one parameter of type BkEventName and one of type Uint16
//  and returns no value.
//
//  This utility handles all Background events.  The Background event identifier
//  passed in is used to build a message to be sent to BackgroundFaultHandler.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  Entry point for reporting a background error.  A BkEventName Id
//  is used to identify the event to be processed.  An ID of BK_NO_EVENT
//  does nothing.
//
//  This static method takes a BkEventName and builds a message of type
//  BK_EVENT_MSG and sends it to BackgroundFaultHanlder Queue.
//
//  $[05048] $[05049] $[05050] $[05051] $[05052] $[05053] $[05054] $[05055]
//  $[05056] $[05057] $[05060] $[05062] $[05063] $[05066] $[05068] $[05071]
//  $[05073] $[05074] $[05075]
//-----------------------------------------------------------------------------
//@ PreCondition
//  ( eventName < BK_MAX_EVENT )
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

void
Background::ReportBkEvent(const BkEventName eventName, const Uint16 errorCode)
{
	CALL_TRACE("Background::ReportBkEvent(const BkEventName eventName, const Uint32 errorCode)");

    CLASS_PRE_CONDITION(eventName < BK_MAX_EVENT);
    if ( eventName != BK_NO_EVENT )
    {
    // $[TI1]
#if defined( SIGMA_SAFETY_NET )
printf("\nBackground::ReportBkEvent: %s.\n",
          BackgroundFaultHandler::GetBkEventName( eventName ) );
#endif // defined( SIGMA_SAFETY_NET )

#if defined( SIGMA_BD_CPU )
        if ( BTAT_1_EN == BackgroundFaultHandler::GetAssociatedBtat( eventName )
			&& !Post::IsServiceModeRequested() )
        {
        // $[TI1.1]
           	//  immediately latch the hardware in vent-inop
        	VentStatus::DirectVentStatusService( VentStatus::VENT_INOP_STATE,
                                           		VentStatus::ACTIVATE );
        }
		// $[TI1.2]
#endif
        BackgroundMsg bkEventMsg;

        bkEventMsg.queueInfo.msgId = BackgroundMsg::BK_EVENT_MSG;
        bkEventMsg.queueInfo.eventName =(Uint8) eventName;
        bkEventMsg.queueInfo.errorCode = errorCode;

        MsgQueue::PutMsg( ::BACKGROUND_FAULT_HANDLER_Q,
                          bkEventMsg.queueData );
    }  //  if eventName != BK_NO_EVENT
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: LogDiagnosticCodeUtil 
//
//@ Interface-Description
//  This method accepts an enum BkEventName and a Uint16 and returns no value.
//
//  This utility allows a client to log a minor fault as an entry to the
//  system diagnostics.
//----------------------------------------------------------------------------
//@ Implementation-Description
//  This method logs a minor Background event to the system diagnostic.
//----------------------------------------------------------------------------
//@ PreCondition
//  ( eventId < BK_MAX_EVENT )
//----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================
 
void
Background::LogDiagnosticCodeUtil(const BkEventName eventName, const Uint16 errorCode)
{
	CALL_TRACE("Background::LogDiagnosticCodeUtil(const BkEventName eventName, const Uint16 errorCode)");

    CLASS_PRE_CONDITION(eventName < BK_MAX_EVENT);
    if ( eventName != BK_NO_EVENT )
    {
    // $[TI1]

#if defined( SIGMA_SAFETY_NET )
printf("\nBackground::LogDiagnosticCodeUtil: %s.\n",
          BackgroundFaultHandler::GetBkEventName( eventName ) );
#endif // defined( SIGMA_SAFETY_NET )

        BackgroundMsg logDiagnosticMsg;

        logDiagnosticMsg.queueInfo.msgId = BackgroundMsg::LOG_DIAGNOSTIC_MSG;
        logDiagnosticMsg.queueInfo.eventName = (Uint8)eventName;
        logDiagnosticMsg.queueInfo.errorCode = errorCode;

        MsgQueue::PutMsg( ::BACKGROUND_FAULT_HANDLER_Q,
                          logDiagnosticMsg.queueData );

    }  //  if eventName != BK_NO_EVENT
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: CheckAndProcessPostErrorStatus 
//
//@ Interface-Description
//  This method accepts no parameters and returns a Boolean value.
//
//  This utility allows a client to report a POST minor fault to
//  Alarms-Analysis Subsystem.
//----------------------------------------------------------------------------
//@ Implementation-Description
//  This method check on the POST error status flag.
//  If PostMinorErrorOccurred_ is TRUE, then it sends a message to
//  Alarm-Analysis Subsystem.
//
//  $[05076]
//----------------------------------------------------------------------------
//@ PreCondition
//  None
//----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================
 
void
Background::CheckAndProcessPostErrorStatus( void )
{
    if ( TRUE == PostMinorErrorOccurred_ )
    {
    // $[TI1]
        QueueAlarmEvent( MINOR_POST_FAULT_EN );
        PostMinorErrorOccurred_ = FALSE;
    }
    // $[TI2]
}
//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method:  CheckIfTimeHasChanged
//
//@ Interface-Description
//  This method accepts no parameters and returns a Boolean.
//  This utility allow the Background Time Of Day Test to determine if the
//  system time has been changed by user, or a BD/GUI synchronization has
//  occurred.
//----------------------------------------------------------------------------
//@ Implementation-Description
//  This is used to determine if the RTC has been changed or resynchronized.
//  Returns TRUE if this is the case, FALSE otherwise.
//
//  Examines the data member TimeHasChangedFlag_ for the current
//  status.  This data member is updated whenever either one of the two
//  events occurred.  When the status is TRUE and this method is interrogated,
//  it resets itself to FALSE.
//----------------------------------------------------------------------------
//@ PreCondition
//  None
//----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

Boolean 
Background::CheckIfTimeHasChanged( void )
{
    Boolean rtnStatus = FALSE;

    if ( TimeHasChangedFlag_ == TRUE )
    {
    // $[TI1]
        TimeHasChangedFlag_ = FALSE;
        rtnStatus = TRUE;
    }
    // $[TI2]

    return( rtnStatus );
}  


//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: ResetActiveBtat 
//
//@ Interface-Description
//  This method accepts no parameters and returns no value.
//
//  This utility clears any active Background events, after EST has passed.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  This method clears any active BTAT alarm stored in the NOVRAM and
//  errors detected by POST.
//-----------------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

void
Background::ResetActiveBtat( void )
{
    BtatEventState btatEvents;

    // $[TI1]
    // clean all active BTAT entries in the NOVRAM
    btatEvents.resetActiveBtatEvents();
    NovRamManager::UpdateActiveBtatEvents( btatEvents );

    // clean Background failed flag in NOVRAM
    NovRamManager::UpdateBackgroundTestFailureFlag( FALSE );

    // clean Background failed flag in POST NOVRAM
    Post::SetBackgroundFailed( ::PASSED );
}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: QueueAlarmEvent
//
//@ Interface-Description
//  This method accepts an argument of type OperatingParameterEventName and
//  returns no value.
//
//  This utility is used to notify Alarms-Analysis Subsystem of a Background
//  event.  
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  This method takes the OperatingParameterEventName and builds a message
//  of type OPERATING_PARAMETER_EVENT and sends it to the operating parameter
//  event queue.  When GUI is not in a ready state to receive message, these
//  active BTATs are stored in NOVRAM and will be re-issued when GUI is in
//  online, timeout, or inop state.
//
//-----------------------------------------------------------------------------
//@ PreCondition
//  None
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

void
Background::QueueAlarmEvent( OperatingParameterEventName alarmType )
{
    AlarmQueueMsg alarmMsg; 
    Int32 msgStatus;

    if ( ( TaskControlAgent::GetGuiState() == STATE_ONLINE ) ||
         ( TaskControlAgent::GetGuiState() == STATE_TIMEOUT ) ||
         ( TaskControlAgent::GetGuiState() == STATE_INOP ) )
    { 
    // $[TI1]
#if defined( SIGMA_SAFETY_NET )
printf("Alarm message sent to Alarm Subsystem: %d.\n", alarmType );
#endif // defined( SIGMA_SAFETY_NET )

        alarmMsg.event.eventType = AlarmQueueMsg::OPERATING_PARAMETER_EVENT;
        alarmMsg.event.eventData = alarmType;

        // Send appropriate event to the OperatingParameterEventQueue
        msgStatus = MsgQueue::PutMsg( ::OPERATING_PARAMETER_EVENT_Q, alarmMsg.qWord );

        CLASS_ASSERTION( msgStatus == Ipc::OK );
    }
    // $[TI2]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
Background::SoftFault( const SoftFaultID  softFaultID,
                       const Uint32       lineNumber,
                       const char*        pFileName,
                       const char*        pPredicate )
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

    FaultHandler::SoftFault( softFaultID, SAFETY_NET, BACKGROUND,
                             lineNumber, pFileName, pPredicate );
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
