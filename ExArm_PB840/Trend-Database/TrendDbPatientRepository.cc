#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbPatientRepository
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is the central interface to store and retrieve patient data.
//  On the client side, the storage and retrieval are processed through the 
//  TrendDataMgr. 
//  On the data store side, this class interfaces with the PatientDataMgr 
//  (to sample updated patient data), and the TrendDataMgr. 
//  The TrendDataMgr enables an interface with a series of TrendDbTables 
//  (1 for each of the trended timescales),  the TrendDbEventReceiver (to 
//  sample all user and auto events that occured within the 10-second sample 
//  period) and TrendDbApplicability (to check the validity of each trended
//  patient parameter within the current settings context).
// 
//---------------------------------------------------------------------
//@ Rationale
//  To provide a layer of abstraction for the storage and retrieval of 
//  trended patient data parameters.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The storage scenario executes every 10 seconds when the Task() method
//  in TrendDataMgr calls sample(). Sample() iterates through all of the
//  trended patient data parameters and resamples the parameters that have
//  been modified. If the parameter is valid (determined through the 
//  isApplicable() interface to the TrendDbApplicability class) then it is 
//  appended to the 1 hour table. scheduleTableUpdates_() determines
//  which other tables need to be updated at each 10 second interval.
// 
//  The retrieval scenario executes every 60 seconds, or more often if
//  requested by the GUI, with a call to processRequest(). This function
//  interprets the request by the client to determine parameters requested,
//  starting point, timescale, etc. loadRecordData_() then interfaces with 
//  the compact flash in a loop, retrieving the requested timescale records,
//  and parsing them to extract the relevent paramters, until the request
//  is fulfilled. The TrendDataSet is then returned to the client.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbPatientRepository.ccv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: rpr    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation and Work of Breathing
// 
//  Revision: 002  By:  rhj    Date:  20-Jul-2007   SCR Number: 6389
//       Project:  Trend
//       Description: 
//            Changed Cstat and Rstat results to zero when they are
//            displaying **** or ---.
//
//  Revision: 001  By:  ksg    Date:  4-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
//
//=====================================================================


#include "TimeStamp.hh"             // OS-Foundation
#include "PatientDataMgr.hh"        // Patient-Data
#include "TrendDbPatientRepository.hh"
#include "TrendDbApplicability.hh"
#include "TrendDbEventReceiver.hh"
#include "BreathDatumHandle.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbPatientRepository [constructor]
//
//@ Interface-Description
//  This function constructs the database for the patient data. Each 
//  parameter gets assigned a configuration slot in the database. Each 
//  parameter gets a sample collection object (determines what kind of 
//  sampling to do, avg, last sample, RM, etc.), which will be stored
//  in the TrendDbRecord, and associated to a patientDataId (or event Id)
//  Each Time scale has its own TrendDbTable and schedule update frequency (in 10
//  second intervals)
//---------------------------------------------------------------------
//@ Implementation-Description
//  Allocates the memory. The initialization of the sample data objects
//  and tables is deferred to the initialize() method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbPatientRepository::TrendDbPatientRepository( void ) :   
	// Sample Data objects, hold the incoming values
	userEventsSample_(TrendDataMgr::SAMPLE_USER_EVENTS_ID),
	autoEventsSample_(TrendDataMgr::SAMPLE_AUTO_EVENTS_ID),
	dynComplianceSample_( PatientDataId::DYNAMIC_COMPLIANCE_ITEM,
						  TrendSelectValue::TREND_DYNAMIC_COMPLIANCE ),
	pavComplianceSample_( PatientDataId::PAV_LUNG_COMPLIANCE_ITEM,
						  TrendSelectValue::TREND_PAV_COMPLIANCE ),
	statComplianceSample_( PatientDataId::STATIC_LUNG_COMPLIANCE_ITEM,
						   TrendSelectValue::TREND_STATIC_LUNG_COMPLIANCE ),
	pavElastanceSample_( PatientDataId::PAV_LUNG_ELASTANCE_ITEM,
						 TrendSelectValue::TREND_PAV_ELASTANCE ),
	endExpFlowSample_( PatientDataId::END_EXPIRATORY_FLOW_ITEM,
					   TrendSelectValue::TREND_END_EXPIRATORY_FLOW ),
	peakExpFlowSample_( PatientDataId::PEAK_EXPIRATORY_FLOW_ITEM,
						TrendSelectValue::TREND_PEAK_EXPIRATORY_FLOW_RATE ),
	peakSpontFlowSample_( PatientDataId::PEAK_SPONT_INSP_FLOW_ITEM,
						  TrendSelectValue::TREND_PEAK_SPONTANEOUS_FLOW_RATE ),
	totalRespRateSample_( PatientDataId::TOTAL_RESP_RATE_ITEM,
						  TrendSelectValue::TREND_TOTAL_RESPIRATORY_RATE ),
	ieRatioSample_( PatientDataId::IE_RATIO_ITEM,
					TrendSelectValue::TREND_I_E_RATIO ),
	nifPressSample_( PatientDataId::NIF_PRESSURE_ITEM,
					 TrendSelectValue::TREND_NEGATIVE_INSPIRATORY_FORCE , 
					 TrendEvents::AUTO_NIF_MANEUVER_ACCEPTED ),
	deliveredO2Sample_( PatientDataId::DELIVERED_O2_PERCENTAGE_ITEM,
						TrendSelectValue::TREND_OXYGEN_PERCENTAGE ),
	p100PressSample_( PatientDataId::P100_PRESSURE_ITEM,
					  TrendSelectValue::TREND_P100 ,
					  TrendEvents::AUTO_P100_MANEUVER_ACCEPTED ),
	endExpPressSample_( PatientDataId::END_EXP_PRESS_ITEM,
						TrendSelectValue::TREND_END_EXPIRATORY_PRESSURE ),
	intrinsicPeepSample_( PatientDataId::PEEP_INTRINSIC_ITEM,
						  TrendSelectValue::TREND_INTRINSIC_PEEP ),
	pavIntrinsicPeepSample_( PatientDataId::PAV_PEEP_INTRINSIC_ITEM,
							 TrendSelectValue::TREND_PAV_INTRINSIC_PEEP ),
	totalPeepSample_( PatientDataId::PEEP_TOTAL_ITEM,
					  TrendSelectValue::TREND_TOTAL_PEEP ),
	meanCctPressSample_( PatientDataId::MEAN_CCT_PRESS_ITEM,
						 TrendSelectValue::TREND_MEAN_CIRCUIT_PRESSURE ),
	peakCctPressSample_( PatientDataId::PEAK_CCT_PRESS_ITEM,
						 TrendSelectValue::TREND_PEAK_CIRCUIT_PRESSURE ),
	plateauPressSample_( PatientDataId::PLATEAU_PRESSURE_ITEM,
						 TrendSelectValue::TREND_PLATEAU_PRESSURE ),
	spontRapidIndexSample_( PatientDataId::SPONT_RATE_TO_VOLUME_RATIO_ITEM,
							TrendSelectValue::TREND_SPONTANEOUS_RAPID_SHALLOW_BREATHING_INDEX ),
	dynResistanceSample_( PatientDataId::DYNAMIC_RESISTANCE_ITEM,
						  TrendSelectValue::TREND_DYNAMIC_RESISTANCE ),
	pavResistanceSample_( PatientDataId::PAV_PATIENT_RESISTANCE_ITEM,
						  TrendSelectValue::TREND_PAV_RESISTANCE ),
	statResistanceSample_( PatientDataId::STATIC_AIRWAY_RESISTANCE_ITEM,
						   TrendSelectValue::TREND_STATIC_AIRWAY_RESISTANCE ),
	pavTotalResistanceSample_( PatientDataId::TOTAL_AIRWAY_RESISTANCE_ITEM,
							   TrendSelectValue::TREND_PAV_TOTAL_AIRWAY_RESISTANCE ),
	spontInspiratoryTimeSample_( PatientDataId::SPONT_INSP_TIME_ITEM,
								 TrendSelectValue::TREND_SPONTANEOUS_INSPIRATORY_TIME ),
	spontInspiratoryRatioSample_( PatientDataId::SPONT_PERCENT_TI_ITEM,
								  TrendSelectValue::TREND_SPONTANEOUS_INSPIRATORY_TIME_RATIO ),
	slowVitalCapacitySample_( PatientDataId::VITAL_CAPACITY_ITEM,
							  TrendSelectValue::TREND_VITAL_CAPACITY ,
							  TrendEvents::AUTO_VC_MANEUVER_ACCEPTED ),
	exhMinuteVolSample_( PatientDataId::EXH_MINUTE_VOL_ITEM,
						 TrendSelectValue::TREND_EXHALED_MINUTE_VOLUME ),
	exhSpontMinVolSample_( PatientDataId::EXH_SPONT_MINUTE_VOL_ITEM,
						   TrendSelectValue::TREND_EXHALED_SPONTANEOUS_MINUTE_VOLUME ),
	exhTidalVolSample_( PatientDataId::EXH_TIDAL_VOL_ITEM,
						TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME ),
	exhSpontTidalVolSample_( PatientDataId::EXH_SPONT_TIDAL_VOL_ITEM,
							 TrendSelectValue::TREND_EXHALED_SPONTANEOUS_TIDAL_VOLUME ),
	exhMandTidalVolSample_( PatientDataId::EXH_MAND_TIDAL_VOL_ITEM,
							TrendSelectValue::TREND_EXHALED_MANDATORY_TIDAL_VOLUME ),
	inspTidalVolSample_( PatientDataId::INSP_TIDAL_VOL_ITEM,
						 TrendSelectValue::TREND_INSPIRED_TIDAL_VOLUME ),
	totalWorkOfBreathingSample_( PatientDataId::TOTAL_WORK_OF_BREATHING_ITEM,
								 TrendSelectValue::TREND_TOTAL_WORK_OF_BREATHING ),
	iLeakSample_( PatientDataId::INSP_LEAK_VOL_ITEM,
								 TrendSelectValue::TREND_ILEAK ),
	LeakSample_( PatientDataId::EXH_LEAK_RATE_ITEM,
								 TrendSelectValue::TREND_LEAK ),
	PercentLeakSample_( PatientDataId::PERCENT_LEAK_ITEM,
								 TrendSelectValue::TREND_PERCENT_LEAK ),

	// Construct the configuration object that maps incoming data items 
	// to their slots in the database
	dbConfig_( TrendDataMgr::TREND_PATIENT_MAX_SLOTS ),

	// Object used to read/write data to the Compact Flash
	compactFlashInterface_( CompactFlash::GetCompactFlash() ),

	// The storage tables for each of the timescales
	table1Hr_(  TrendDataMgr::PATIENT_1HR_TABLE_ID, patientRecord_, compactFlashInterface_ ),
	table2Hr_(  TrendDataMgr::PATIENT_2HR_TABLE_ID, patientRecord_, compactFlashInterface_ ),
	table4Hr_(  TrendDataMgr::PATIENT_4HR_TABLE_ID, patientRecord_, compactFlashInterface_ ),
	table8Hr_(  TrendDataMgr::PATIENT_8HR_TABLE_ID, patientRecord_, compactFlashInterface_ ),
	table12Hr_( TrendDataMgr::PATIENT_12HR_TABLE_ID, patientRecord_, compactFlashInterface_ ),
	table24Hr_( TrendDataMgr::PATIENT_24HR_TABLE_ID, patientRecord_, compactFlashInterface_ ),
	table48Hr_( TrendDataMgr::PATIENT_48HR_TABLE_ID, patientRecord_, compactFlashInterface_ ),
	table72Hr_( TrendDataMgr::PATIENT_72HR_TABLE_ID, patientRecord_, compactFlashInterface_ )
{
	TREND_MGR_DBG1( 2, "TrendPatientDataDB constructor" );

	// Configure the slots of the database
	dbConfig_.configSlot( &userEventsSample_,       0 );
	dbConfig_.configSlot( &autoEventsSample_,       1 );
	dbConfig_.configSlot( &dynComplianceSample_,    2 );
	dbConfig_.configSlot( &pavComplianceSample_,    3 );
	dbConfig_.configSlot( &statComplianceSample_,   4 );
	dbConfig_.configSlot( &pavElastanceSample_,     5 );
	dbConfig_.configSlot( &endExpFlowSample_,       6 );
	dbConfig_.configSlot( &peakExpFlowSample_,      7 );
	dbConfig_.configSlot( &peakSpontFlowSample_,    8 );
	dbConfig_.configSlot( &totalRespRateSample_,    9 );
	dbConfig_.configSlot( &ieRatioSample_,          10 );
	dbConfig_.configSlot( &nifPressSample_,         11 );
	dbConfig_.configSlot( &deliveredO2Sample_,      12 );
	dbConfig_.configSlot( &p100PressSample_,        13 );
	dbConfig_.configSlot( &endExpPressSample_,      14 );
	dbConfig_.configSlot( &intrinsicPeepSample_,    15 );
	dbConfig_.configSlot( &pavIntrinsicPeepSample_, 16 );
	dbConfig_.configSlot( &totalPeepSample_,        17);
	dbConfig_.configSlot( &meanCctPressSample_,     18 );
	dbConfig_.configSlot( &peakCctPressSample_,     19 );
	dbConfig_.configSlot( &plateauPressSample_,     20 );
	dbConfig_.configSlot( &spontRapidIndexSample_,  21 );
	dbConfig_.configSlot( &dynResistanceSample_,    22 );
	dbConfig_.configSlot( &pavResistanceSample_,    23 );
	dbConfig_.configSlot( &statResistanceSample_,   24 );
	dbConfig_.configSlot( &pavTotalResistanceSample_, 25 );
	dbConfig_.configSlot( &spontInspiratoryTimeSample_, 26 );
	dbConfig_.configSlot( &spontInspiratoryRatioSample_, 27 );
	dbConfig_.configSlot( &slowVitalCapacitySample_, 28 );
	dbConfig_.configSlot( &exhMinuteVolSample_,      29 );
	dbConfig_.configSlot( &exhSpontMinVolSample_,    30 );
	dbConfig_.configSlot( &exhTidalVolSample_,       31 );
	dbConfig_.configSlot( &exhSpontTidalVolSample_,  32 );
	dbConfig_.configSlot( &exhMandTidalVolSample_,   33 );
	dbConfig_.configSlot( &inspTidalVolSample_,      34 );
	dbConfig_.configSlot( &totalWorkOfBreathingSample_, 35 );
	dbConfig_.configSlot( &iLeakSample_, 36 );
	dbConfig_.configSlot( &LeakSample_, 37 );
	dbConfig_.configSlot( &PercentLeakSample_, 38 );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbPatientRepository [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbPatientRepository::~TrendDbPatientRepository( void )
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
//  Sets initial conditions for data collection. Must be called before 
//  data storage or retrievals begin
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clears all of the TrendDbSampleData objects, initializes the refresh array
//  and the timescale tables 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	Verifies that the database doesn't overrun the expected space
//  on the compact flash
//
//@ End-Method
//=====================================================================
void TrendDbPatientRepository::initialize( void )
{
	TREND_MGR_DBG1( 8, "Initialize the Patient Repository" );

	// Clear each sample data object
	for (Uint8 slotNumber = 0; slotNumber < dbConfig_.getMaxSlotNumber(); slotNumber++)
	{
		TrendDbSampleData * ptrSampleData = dbConfig_.getSampleData( slotNumber );
		if (ptrSampleData)
		{
			ptrSampleData->clear();
		}
	}

	// Initialize the elements of the refresh array to 1. This will force 
	// every parameter to be sampled and tested for validity.
	memset( isRefreshedArray_, 0, sizeof(isRefreshedArray_) );

	// configure the compact flash block location for each table
	Uint32 tableBlock = TrendDataMgr::TREND_PATIENT_START_BLOCK;
	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_1HR].ptrTable = &table1Hr_;
	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_1HR].factor = TABLE_1HR_TIMING;

	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_2HR].ptrTable = &table2Hr_;
	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_2HR].factor = TABLE_2HR_TIMING;

	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_4HR].ptrTable = &table4Hr_;
	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_4HR].factor = TABLE_4HR_TIMING;

	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_8HR].ptrTable = &table8Hr_;
	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_8HR].factor = TABLE_8HR_TIMING;

	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_12HR].ptrTable = &table12Hr_;
	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_12HR].factor = TABLE_12HR_TIMING;

	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_24HR].ptrTable = &table24Hr_;
	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_24HR].factor = TABLE_24HR_TIMING;

	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_48HR].ptrTable = &table48Hr_;
	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_48HR].factor = TABLE_48HR_TIMING;

	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_72HR].ptrTable = &table72Hr_;
	tableSchedule_[TrendTimeScaleValue::TREND_TIME_SCALE_72HR].factor = TABLE_72HR_TIMING;

	// Configure and schedule each table
	for (Int16 tableId = TrendDataMgr::PATIENT_LOW_TABLE_ID;
		tableId <= TrendDataMgr::PATIENT_HIGH_TABLE_ID;
		tableId++)
	{
		TREND_MGR_DBG2( 8, "Trend Patient Repository config table ", tableId );

		// Setup/configure the space on the compact flash
		TrendDbTable * pTable = tableSchedule_[ tableId ].ptrTable;
		Uint32 tableCap = TrendDataMgr::GetTableCapacity((TrendTimeScaleValue::TrendTimeScaleValueId) tableId);

		// Configure the table with the starting LBA and capacity.
		// The current head and size are retreived from the control block inside config()
		pTable->config( tableBlock, tableCap );

		tableBlock += pTable->getCapacityLba();

		TREND_MGR_DBG4( 8, "Patient DB table ", tableId,
						", start LBA ", pTable->getStartLba() );
		TREND_MGR_DBG4( 8, "  end LBA ", (tableBlock - 1),
						" schedule time ", tableSchedule_[ tableId ].factor );
	}

	// Verify that the database will be limited to the defined region of the compact flash
	AUX_CLASS_PRE_CONDITION((tableBlock <= TrendDataMgr::TREND_PATIENT_END_BLOCK),
							((tableBlock << 16) | TrendDataMgr::TREND_PATIENT_END_BLOCK));

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeHappened [static]
//
//@ Interface-Description
//  This is the called from the manager when a PatientItemId has been updated
//  in the Patient Data Manager
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function updates the refeshArray_ at the offset of the 
//  provided PatientItemId.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbPatientRepository::changeHappened( const PatientDataId::PatientItemId dataId )
{
	isRefreshedArray_[dataId] = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sample
//
//@ Interface-Description
//  This function samples the current Patient Data through its interface
//  with the PatientDataMgr.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function loops through each of the trended parameters determining
//  which paramters need to be resampled. The user and auto events are 
//  sampled at every 10 second interval. The isRefreshedArray_ indicates which 
//  of the remaining parameters have been modified and resamples those
//  parameters. Each of the modified parameters is then checked for validity
//  through a call to the isValidContext() method. (Events are not checked - 
//  they are always valid).
//  Before returning, this function calls scheduleTableUpdates_().
// 
//  Satisfies requirement:
//  [[TR01140] [PRD 1493, 1483, 1484, 1485]  The system shall maintain Trend
//  data for the current patient up to 72 hours...
//  [[TR01119] [PRD 1483, 1484, 1485] For the 1 hour timescale, the system
//  shall sample...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbPatientRepository::sample( Uint32 seqPosNumber, const TimeStamp & timeStamp )
{
	Uint8                        slotNumber;
	TrendDbSampleData *          ptrSampleData;
	TrendValue                   newValue;
	const TrendDbEventReceiver & trendEventReceiver = TrendDataMgr::GetEventReceiver();

	TREND_MGR_DBG2( 8, "Trend Patient Repository sample, frame# ", seqPosNumber );

	// For each configured slot, check if value has changed, get updated value
	// Call the compressData based on seqNumber/Position or "10 second pass counts"
	for (slotNumber = 0;
		slotNumber < dbConfig_.getMaxSlotNumber();
		slotNumber++)
	{
		ptrSampleData = dbConfig_.getSampleData( slotNumber );
		if (ptrSampleData)
		{
			// checking for new user events...
			if (ptrSampleData->getTrendId() == TrendDataMgr::SAMPLE_USER_EVENTS_ID)
			{
				// events are always applicable
				newValue.isValid = TRUE;
				// load the user events
				newValue.data.discreteValue = trendEventReceiver.getUserEvents();
				TREND_MGR_DBG4( 8, "Sample User Events ", hex,
								newValue.data.discreteValue, dec );
				// Update storage value
				ptrSampleData->setValue( newValue );
			}
			// checking for new auto events...
			else if (ptrSampleData->getTrendId() == TrendDataMgr::SAMPLE_AUTO_EVENTS_ID)
			{
				// events are always applicable
				newValue.isValid = TRUE;
				// load the auto events
				newValue.data.discreteValue = trendEventReceiver.getAutoEvents();
				TREND_MGR_DBG4( 8, "Sample Auto Events ", hex,
								newValue.data.discreteValue, dec );
				// Update storage value
				ptrSampleData->setValue( newValue );
			}
			else if (ptrSampleData->getTrendId() == TrendSelectValue::TREND_STATIC_LUNG_COMPLIANCE )
			{

				PatientDataId::PatientItemId dataId = ptrSampleData->getId().patient;
				// if patient data item has been updated, sample
				if (isRefreshedArray_[dataId])
				{
					// only bounded patient data is trended
					if (PatientDataMgr::IsBoundedId(dataId))
					{
						Int32 complianceFlag = BreathDatumHandle(PatientDataId::STATIC_LUNG_COMPLIANCE_VALID_ITEM).
												getDiscreteValue().data;

						BreathDatumHandle breathDatumHandle(dataId);
						BoundedBreathDatum boundedDatum;
						// sample current value
						boundedDatum = breathDatumHandle.getBoundedValue();

						// is parameter applicable?
						newValue.isValid = isValidContext( dataId, boundedDatum );

						if (newValue.isValid)
						{

							switch (complianceFlag)
							{
							case PauseTypes::VALID:						
								// data is reliable
							case PauseTypes::OUT_OF_RANGE:
							case PauseTypes::SUB_THRESHOLD_INPUT:
							case PauseTypes::CORRUPTED_MEASUREMENT:
							case PauseTypes::EXH_TOO_SHORT:
							case PauseTypes::NOT_STABLE:				
								// data is questionable 
							    // store data
							    newValue.data.realValue = boundedDatum.data.value;
								break;
							case PauseTypes::UNAVAILABLE:					
								// data can not be calculated 
							case PauseTypes::NOT_REQUIRED:		
								// data is not required for this case 
								// Store zero
								newValue.data.realValue = 0.0F;
								break;
							}
					    }
						else
						{
							// store gap
							newValue.data.realValue = 0.0F;
						}

					}
					else
					{
						TREND_MGR_DBG3( 1, "Patient Id ", dataId, " unknown value type" );
						newValue.isValid = FALSE;
					}

					// Update storage value
					ptrSampleData->setValue( newValue );
					isRefreshedArray_[dataId] = FALSE;
				}
			}
			else if (ptrSampleData->getTrendId() == TrendSelectValue::TREND_STATIC_AIRWAY_RESISTANCE )
			{

				PatientDataId::PatientItemId dataId = ptrSampleData->getId().patient;
				// if patient data item has been updated, sample
				if (isRefreshedArray_[dataId])
				{
					// only bounded patient data is trended
					if (PatientDataMgr::IsBoundedId(dataId))
					{
						Int32 resistanceFlag  = BreathDatumHandle(PatientDataId::STATIC_AIRWAY_RESISTANCE_VALID_ITEM).
												getDiscreteValue().data;
	
						BreathDatumHandle breathDatumHandle(dataId);
						BoundedBreathDatum boundedDatum;
						// sample current value
						boundedDatum = breathDatumHandle.getBoundedValue();

						// is parameter applicable?
						newValue.isValid = isValidContext( dataId, boundedDatum );

						if (newValue.isValid)
						{

							switch (resistanceFlag)
							{
							case PauseTypes::VALID:						
								// data is reliable
							case PauseTypes::OUT_OF_RANGE:
							case PauseTypes::SUB_THRESHOLD_INPUT:
							case PauseTypes::CORRUPTED_MEASUREMENT:
							case PauseTypes::EXH_TOO_SHORT:
							case PauseTypes::NOT_STABLE:				
								// data is questionable 
							    // store data
							    newValue.data.realValue = boundedDatum.data.value;
								break;
							case PauseTypes::UNAVAILABLE:					
								// data can not be calculated 
							case PauseTypes::NOT_REQUIRED:		
								// data is not required for this case 
								// Store zero
								newValue.data.realValue = 0.0F;
								break;
							}
					    }
						else
						{
							// store gap
							newValue.data.realValue = 0.0F;
						}
					}
					else
					{
						TREND_MGR_DBG3( 1, "Patient Id ", dataId, " unknown value type" );
						newValue.isValid = FALSE;
					}

					// Update storage value
					ptrSampleData->setValue( newValue );
					isRefreshedArray_[dataId] = FALSE;
				}
			}
			// checking for updated patient data...
			else
			{
				PatientDataId::PatientItemId dataId = ptrSampleData->getId().patient;
				// if patient data item has been updated, sample
				if (isRefreshedArray_[dataId])
				{
					// only bounded patient data is trended
					if (PatientDataMgr::IsBoundedId(dataId))
					{
						BreathDatumHandle breathDatumHandle(dataId);
						BoundedBreathDatum boundedDatum;
						// sample current value
						boundedDatum = breathDatumHandle.getBoundedValue();
						// is parameter applicable?
						newValue.isValid = isValidContext( dataId, boundedDatum );
						if (newValue.isValid)
						{
							// store data
							newValue.data.realValue = boundedDatum.data.value;
						}
						else
						{
							// store gap
							newValue.data.realValue = 0;
						}
					}
					else
					{
						TREND_MGR_DBG3( 1, "Patient Id ", dataId, " unknown value type" );
						newValue.isValid = FALSE;
					}

					// Update storage value
					ptrSampleData->setValue( newValue );
					isRefreshedArray_[dataId] = FALSE;
				}
			} // patient data
		} // config slot
	} // for each slot

	// Check to see if any of the tables need to be updated
	scheduleTableUpdates_( seqPosNumber, timeStamp );
	TREND_MGR_DBG1( 8, "Patient Repository table updates");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isValidContext
//
//@ Interface-Description
//  Return TRUE is the patientData Item is valid within the current 
//  setting context.
//  Return FALSE otherwise
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function determines the validity of the given parameter through
//  a call to the isApplicable() method in the TrendDbApplicability class.
// 
//  Satisfies requirements: 
//  [[TR01127] Trending shall display Gaps for Trend Parameters that are
//  not applicable...
//  [[TR01203] Trending shall display Gaps for samples acquired during
//  Apnea Ventilation
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendDbPatientRepository::isValidContext( const PatientDataId::PatientItemId dataId, 
												  BoundedBreathDatum boundedDatum)
{
	TREND_MGR_DBG2( 64, "Patient isValidContext ", dataId );
	const TrendDbApplicability & trendApplicability = TrendDataMgr::GetApplicability();
	return( trendApplicability.isApplicable(dataId, boundedDatum) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetDatabase
//
//@ Interface-Description
//  Clears the database and set to default empty
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clears each of the timescale database tables
//  Clears each of the TrendDbSampleData objects
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbPatientRepository::resetDatabase( void )
{
	TREND_MGR_DBG1( 4, "Reset patient repository" );

	// Clear each table
	for (Int16 tableId = 0; tableId <= TrendDataMgr::PATIENT_HIGH_TABLE_ID; tableId++)
	{
		tableSchedule_[tableId].ptrTable->clear();
	}

	// Clear each sample data object
	for (Uint8 slotNumber = 0; slotNumber < dbConfig_.getMaxSlotNumber(); slotNumber++)
	{
		TrendDbSampleData * ptrSampleData = dbConfig_.getSampleData( slotNumber );
		if (ptrSampleData)
		{
			ptrSampleData->clear();
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resyncAll
//
//@ Interface-Description
//  This function is called by the TrendDataMgr to force all of the 
//  trended parameters to be re-sampled and re-checked for validity. This 
//  is called when the system context is modified (user updates one of
//  the context settings).
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets all of the boolean values in the isRefreshedArray_ to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbPatientRepository::resyncAll( void )
{
	// All patient data items must be retrieved from PatientDataMgr and 
	// re-checked for validity.
	memset( isRefreshedArray_, 1, sizeof(isRefreshedArray_) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateTable_
//
//@ Interface-Description
//  Append the sampled data values to the given table
//  Returns SigmaStatus SUCCESS if no write errors
//  Returns SigmaStatus FAILURE otherwise
//---------------------------------------------------------------------
//@ Implementation-Description
// 
//---------------------------------------------------------------------
//@ PreCondition
//	pointer to the table must exist
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus TrendDbPatientRepository::updateTable_( TrendDbTable * pTable,
													TrendTimeScaleValue::TrendTimeScaleValueId timescale,
													Uint32 seqPosNumber, const TimeStamp & trendTimeStamp )
{
	AUX_CLASS_PRE_CONDITION( pTable, (Uint32) pTable );
	SigmaStatus rtn = FAILURE;

	TREND_MGR_DBG2( 32, "Update patient table, timescale ", timescale );

	// Setup the next record in the table
	if (pTable->setNextRecord( seqPosNumber, trendTimeStamp ) == SUCCESS )
	{
		// Update each trended patient data parameter and append it to the table
		for (Uint8 slotNumber = 0;
			slotNumber < dbConfig_.getMaxSlotNumber();
			slotNumber++)
		{
			// get updated patient data value
			TrendDbSampleData * ptrSampleData = dbConfig_.getSampleData( slotNumber );
			if (ptrSampleData)
			{
				// Compress for specified timescale
				ptrSampleData->compressData( timescale ); 

				// Append the data to the output-to-flash records
				pTable->appendData( slotNumber, ptrSampleData->getValue(timescale) );
			}
#ifdef TREND_MGR_DBG
			ptrSampleData->dump();
#endif
		}
		// Close the record, flush to flash if needed
		pTable->closeRecord();
		rtn = SUCCESS;
	}
	else
	{
		// Error in writing trend data to the compact flash
		TREND_MGR_DBG1( 1, "Trend Patient Repository Error in update record" );
		rtn = FAILURE;
	}
	return( rtn );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: scheduleTableUpdates_
//
//@ Interface-Description
//  The table scheduler determines which timescale tables require updates
//  at the current timestamp.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Loops through each of the timescale tables. Modulus of the frequency
//  determines if a table is ready for update. Calls updateTable_() on
//  those tables.
// 
//  Satisfies requirement [[TR01114] [PRD 1512] For the 2, 4, 8, 12, 24,
//  48, and 72 hour timescales, ....
// 
//---------------------------------------------------------------------
//@ PreCondition
//	pointer to the table must exist
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbPatientRepository::scheduleTableUpdates_( Uint32 seqPosNumber,
													  const TimeStamp & trendTimeStamp )
{
	SigmaStatus noErrors = SUCCESS;

	TREND_MGR_DBG2( 32, "Schedule Table updates, seq/position ", seqPosNumber );

	// loop through each of the timescale tables
	for (Int16 tableId = 0;
		tableId < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES && noErrors;
		tableId++)
	{
		if (!(seqPosNumber % tableSchedule_[tableId].factor))
		{
			// Time to update the table
			noErrors = updateTable_( tableSchedule_[ tableId ].ptrTable,
									 (TrendTimeScaleValue::TrendTimeScaleValueId) tableId,
									 seqPosNumber, trendTimeStamp );
			if (noErrors == FAILURE)
			{
				// Error writing to flash. Disable trending.
				TREND_MGR_DBG1( 1, "Patient sample update - compact flash failed" );
				TrendDataMgr::Disable();
			}
		}
	}
	return;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processRequest
//
//@ Interface-Description
//  This function is called by the TrendDataMgr to process a data
//  retrieval request from the GUI.
//  Returns a TrendDataSet::Status :
// 	EMPTY_SET if no data in the database
//  PROCESS_ERROR if problem reading from the database, or 
//  PROCESS_OK if operation completed successfully.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function extracts the request information from the provided
//  TrendDataSet (position, size, timescale, etc), ensures that the requested
//  timescale table contains data, and adjusts the request parameters as 
//  necessary to frame the request as the database interface expects it.
//  
//  The function then iteratively requests and parses the data through a call 
//  to loadRecordData_(). This loop continues until the request is fulfilled.
//  The data is loaded into the provided TrendDataSet.
//  
// 
// Satisfies requirement:
// [[TR01139] [PRD 1493] If a Trend data integrity verification fails the
//  system shall display that Trend value as a Gap...
//---------------------------------------------------------------------
//@ PreCondition
//	pTrendDataSet exists
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDataSet::Status TrendDbPatientRepository::processRequest( TrendDataSet * pTrendDataSet )
{
	AUX_CLASS_PRE_CONDITION( pTrendDataSet, (Uint32) pTrendDataSet );

	TrendDataSet::Status rtnStatus = TrendDataSet::EMPTY_SET;

	// Get the Trend Data Set request information
	TrendRequestInfo & reqInfo = pTrendDataSet->getRequestInfo_();
	Uint16 startPosition = reqInfo.position_;
	Uint16 requestSize = reqInfo.requestSize_;
	TrendTimeScaleValue::TrendTimeScaleValueId tableId = reqInfo.timescale_;

	TREND_MGR_DBG6( 4, "Patient Repository Request, table ", tableId,
					", Index ", startPosition,
					", size ", requestSize );

	// Get the table for the requested timescale
	TrendDbTable * pTable = tableSchedule_[ tableId ].ptrTable;
	AUX_CLASS_PRE_CONDITION( pTable, (Uint32) pTable );

	if (!(pTable->getRecordCount()))
	{
		// No data in the table. Just return.
		TREND_MGR_DBG2( 1, "No data in Table ", tableId );
		return( rtnStatus );
	}

	if (startPosition >= pTable->getRecordCount())
	{
		// Ensure requested start position is within the table.
		TREND_MGR_DBG4( 4, "Invalid start Index ", startPosition,
						", adjust start Index ", (pTable->getRecordCount() - 1) );
		startPosition = pTable->getRecordCount() - 1;
	}

	if (requestSize > (pTable->getRecordCount() - startPosition ))
	{
		// Ensure request size is within the table
		TREND_MGR_DBG6( 4, "Invalid size ", requestSize,
						", table size ", pTable->getRecordCount(),
						", adjust size ", (pTable->getRecordCount() - startPosition) );
		requestSize = pTable->getRecordCount() - startPosition;
	}

	// Get the read buffer from the Trend Data Manager
	TrendDbPatientRecord loadRecord;
	TrendDbTableBuffer loadBuffer( TrendDataMgr::GetReadBuffer(),
								   TrendDataMgr::TREND_READ_BUFFER_SIZE,
								   sizeof( CompactFlash::Block ),
								   loadRecord.size() );

	// Setup the initial values for the transfer
	Boolean isXferDone = FALSE;
	Uint16 loadPosition = startPosition;
	Int16 firstRow = 0;

	// Continue to load chunks of data one buffer at a time from the compact flash
	while (!(isXferDone))
	{
		// Adjust the size to match the buffer (in records)
		Uint16 dataSize = loadBuffer.getRecordCapacity();

		// If request is larger than the data in the table, adjust request size
		if (dataSize > (pTable->getRecordCount() - loadPosition))
		{
			TREND_MGR_DBG6( 16, "Patient Repository load at end, data size ", dataSize,
							", table size ", pTable->getRecordCount(),
							", load position ", loadPosition );
			dataSize = pTable->getRecordCount() - loadPosition;
		}
		TREND_MGR_DBG6( 16, "Patient Repository Xfer Index ", loadPosition,
						", size ", dataSize,
						", sample row ", firstRow );

		if (pTable->loadRecordSet( loadBuffer, loadPosition, dataSize ) == SUCCESS)
		{
			// Parse the data
			// Returns -1 on data error's
			Int16 rows = loadRecordData_( loadBuffer, pTrendDataSet, firstRow );
			// Update the values, if no errors
			if (rows == -1)
			{
				TREND_MGR_DBG1( 1, "Patient Repository load Error" );
				isXferDone = TRUE;
				rtnStatus = TrendDataSet::PROCESS_ERROR;
			}
			else
			{
				// Update the load position for next request
				// Update row offset based on loaded data; may be less than requested
				loadPosition += dataSize;
				firstRow += rows;
				TREND_MGR_DBG4( 16, "  next load Index ", loadPosition,
								", row ", firstRow );

				// request completed?
				if (firstRow >= requestSize || 
					firstRow >= TrendDataMgr::MAX_ROWS ||
					loadPosition >= pTable->getRecordCount())
				{
					isXferDone = TRUE;
					rtnStatus = TrendDataSet::PROCESS_OK;
					TREND_MGR_DBG1( 4, "Patient Repository Load Record Set Done" );
				}
			}
		}
		else
		{
			TREND_MGR_DBG1( 1, "Patient Repository Load record set error" );
			isXferDone = TRUE;
			rtnStatus = TrendDataSet::PROCESS_ERROR;
		}
	}

	TREND_MGR_DBG4( 4, "Patient Load Records done status ", rtnStatus, ", rows ", firstRow );

	// Update data request size
	// The patient repository is the master and sets the data size for settings
	pTrendDataSet->setDataSetSize_( firstRow );

	return( rtnStatus );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loadRecordData_
//
//@ Interface-Description
//  Retrieves the data from the TrendDataMgr's read buffer.
//  Returns the number of rows loaded (>=1), if operation completed without load errors. 
//  Returns -1, otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function extracts data from the TrendDbTable one row-based
//  record at a time. This includes all of the trended patient data parameters.
//  The dbConfig mapping is used to extract the requested parameters from
//  the record. The extracted data is loaded into the provided TrendDataSet
//  to be returned to the client.
//---------------------------------------------------------------------
//@ PreCondition
//	pTrendDataSet exist
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Int16 TrendDbPatientRepository::loadRecordData_( TrendDbTableBuffer & inputBuffer,
												 TrendDataSet * pTrendDataSet,
												 Uint16 firstRow )
{
	AUX_CLASS_PRE_CONDITION( pTrendDataSet, (Uint32) pTrendDataSet );

	TrendDbPatientRecord    loadRecord;
	Uint16                  row = firstRow;
	Boolean                 isMoreToLoad = TRUE;
	Boolean                 isLoadError = FALSE;

	// Make sure that expectedSeq is correct for the given time scale
	TrendRequestInfo & reqInfo = pTrendDataSet->getRequestInfo_();
	Uint16 tableFactor = getTableFrequency( reqInfo.timescale_ );
	Int32 expectedSeq = reqInfo.seqNumber_ - (firstRow * tableFactor);

	// Align the expectedSeq to the timescale
	Int32 ndx = expectedSeq / tableFactor;
	expectedSeq = ndx * tableFactor;

	TREND_MGR_DBG6( 16, "Patient Load record data first row ", row,
					", first seq number ", expectedSeq,
					", table factor ", tableFactor );

	// Load the data from the record into the trend data set in reverse
	// order, such that the newest data is first to go into the data set
	inputBuffer.setLastRecord( loadRecord );
	while (!isLoadError && isMoreToLoad && row < TrendDataMgr::MAX_ROWS)
	{
		// Check to see if record is valid (verify checksums)
		if (loadRecord.isValid())
		{
			// Is frame sequence number what we expected?
			Uint32 seqPosNumber = loadRecord.getSeqNumber();
			if (seqPosNumber == expectedSeq)
			{
				TREND_MGR_DBG2( 32, "Load record with seq/frame ", seqPosNumber );

				// Copy seq/frame number and time stamp
				pTrendDataSet->setSequenceNumber_( row, seqPosNumber);
				TimeStamp timeStamp( loadRecord.getTimeOfDay() );
				pTrendDataSet->setTrendTimeStamp_( row, timeStamp);

				// Copy over the patient data for each column
				for (Uint16 column = 0; column < TrendDataMgr::MAX_COLUMNS; column++)
				{
					TrendDataValues & columnData = pTrendDataSet->getTrendDataValues_( column );
					TrendSelectValue::TrendSelectValueId columnId = reqInfo.requestIds_[ column ];
					Int16 slotNumber = dbConfig_. getSlotNumber( columnId );
					if (slotNumber != - 1)
					{
						// The column Id has found in the load record so
						// copy over the value and Id into the Data Values
						columnData.setValue_( row, loadRecord.getTrendValue( slotNumber ) );
						columnData.setId_( columnId );
					}
				}

				// Copy over the User events
				Int16 userEventSlotNumber = dbConfig_.getSlotNumber( TrendDataMgr::SAMPLE_USER_EVENTS_ID );
				if (userEventSlotNumber == -1)
				{
					TREND_MGR_DBG1( 1, "No User Event's configured in database" );
				}
				else
				{
					TrendValue eventsBits = loadRecord.getTrendValue( userEventSlotNumber );
					TrendEvents & eventsData =  pTrendDataSet->getTrendEvents_( row );
					eventsData.loadUserEvents( eventsBits.data.discreteValue );
				}

				// Copy over the Auto events
				Int16 autoEventSlotNumber = dbConfig_.getSlotNumber( TrendDataMgr::SAMPLE_AUTO_EVENTS_ID );
				if (autoEventSlotNumber == -1)
				{
					TREND_MGR_DBG1( 1, "No Auto Event's configured in database" );
				}
				else
				{
					TrendValue eventsBits = loadRecord.getTrendValue( autoEventSlotNumber );
					TrendEvents & eventsData =  pTrendDataSet->getTrendEvents_( row );
					eventsData.loadAutoEvents( eventsBits.data.discreteValue );
				}

				// Move to the next input record
				isMoreToLoad =
					(inputBuffer.gotoNextRecord( loadRecord, FALSE ) == SUCCESS ? TRUE : FALSE);

				// data transferred into this row, move to the next
				row++;
				expectedSeq -= tableFactor;
			}
			else if (seqPosNumber > expectedSeq && row == firstRow)
			{
				// Invalid sequence number ok for first row. Move to the next input record
				TREND_MGR_DBG4( 4, "Patient Find expected seq number ", expectedSeq,
								", got ", seqPosNumber );
				isMoreToLoad = 
					(inputBuffer.gotoNextRecord( loadRecord, FALSE) == SUCCESS ? TRUE : FALSE);
			}
			else
			{
				// Invalid sequence number. Throw Load Error 
				isLoadError = TRUE;
				TREND_MGR_DBG6( 1, "Error expected seq number ", expectedSeq,
								", got ", seqPosNumber,
								", on row ", row );
			}
		}
		else
		{
			// Invalid record. Throw Load Error
			isLoadError = TRUE;
			TREND_MGR_DBG2( 1, "Load Record invalid seq/frame ", expectedSeq  );
		}
	}

	TREND_MGR_DBG4( 16, "Patient Repository Loaded rows ", (row - firstRow),
					", load error ", isLoadError );

	// If operation completed without load errors, return the number of rows loaded. (>=1)
	// Otherwise, return -1. 
	return( (!isLoadError) ? (row - firstRow) : -1 );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTableFrequency
//
//@ Interface-Description
//  Return the frequency of the table's updates
//  In the number of 10sec slices
//---------------------------------------------------------------------
//@ Implementation-Description
//  Frequency of 1 is once every 10 seconds
//               2 is once every 20 seconds
//  etc ...
//---------------------------------------------------------------------
//@ PreCondition
//	
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint16 TrendDbPatientRepository::getTableFrequency( TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION( (timescale < TrendSelectValue::TOTAL_TREND_SELECT_VALUES),
							 timescale );   
	return( tableSchedule_[timescale].factor );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sampleSimulatedData
//
//@ Interface-Description
//  Fill the database with simulated data
//---------------------------------------------------------------------
//@ Implementation-Description
//  Loop through each of the configured slots for patient data. For each of these
//  slots, load the data from the associated offset in the input array.
//  Follow with a call to ScheduleTableUpdates to load data into all timescales.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
// 
//@ End-Method
//=====================================================================
void TrendDbPatientRepository::sampleSimulatedData( Uint32 seqPosNumber,
													const TimeStamp & trendTimeStamp,
													const TrendValue * pTrendValues )
{
	// Load simulated data into each configured slot
	for (Int16 slotNumber = 0; slotNumber < dbConfig_.getMaxSlotNumber(); slotNumber++)
	{
		TrendDbSampleData * pSampleData = dbConfig_.getSampleData( slotNumber );
		if (pSampleData)
		{
			pSampleData->setValue( pTrendValues[slotNumber] );
			TREND_MGR_DBG6( 64, "Patient simulated sample ", slotNumber,
							", value ", pSampleData->getValue().data.realValue,
							", flag ", pSampleData->getValue().isValid );
		}
	}
	// update timescale tables
	scheduleTableUpdates_( seqPosNumber, trendTimeStamp );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbPatientRepository::SoftFault(const SoftFaultID  softFaultID,
										 const Uint32       lineNumber,
										 const char*        pFileName,
										 const char*        pPredicate)  
{

	CALL_TRACE("TrendDbPatientRepository::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_PATIENT_REPOSITORY,
							lineNumber, pFileName, pPredicate);
}
