#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendEvents - An encapsulation of events
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the bit logic to handle reading and 
//  writing events in the 64-bit vector that stores the user and auto
//  events.
//---------------------------------------------------------------------
//@ Rationale
//  To provide a layer of abstraction to access the event bit vectors
//---------------------------------------------------------------------
//@ Implementation-Description
//  The 32-bit user and auto bit-vectors are implemented and accessed
//  as a single 64-bit array.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendEvents.ccv   25.0.4.0   19 Nov 2013 14:34:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjf    Date:  05-MAR-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version.
// 
//=====================================================================

//@ Usage-Classes
//@ End-Usage

#include "BitUtilities.hh"	// Utilities 
#include "TrendEvents.hh"


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendEvents [constructor]
//
//@ Interface-Description
//  Clears the auto and user events
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendEvents::TrendEvents( void )
{
	eventData_[USER_EVENT] = 0;
	eventData_[AUTO_EVENT] = 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendEvents [copy constructor]
//
//@ Interface-Description
//  copys the events from the source
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendEvents::TrendEvents( const TrendEvents & source )
{
	eventData_[USER_EVENT] = source.eventData_[USER_EVENT];
	eventData_[AUTO_EVENT] = source.eventData_[AUTO_EVENT];
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendEvents [Assignment]
//
//@ Interface-Description
//  copys the events from the source
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendEvents& TrendEvents::operator=( const TrendEvents & source )
{
	if (this != &source)
	{
		eventData_[USER_EVENT] = source.eventData_[USER_EVENT];
		eventData_[AUTO_EVENT] = source.eventData_[AUTO_EVENT];
	}
	return( *this );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendEvents [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendEvents::~TrendEvents() 
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isEventSet		
//
//@ Interface-Description
//  Return TRUE if the event is set
//  Return FALSE otherwise
//---------------------------------------------------------------------
//@ Implementation-Description
//  validates the Event Id
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendEvents::isEventSet( TrendEvents::EventId event ) const
{
	AUX_CLASS_PRE_CONDITION( (event < TrendEvents::TOTAL_EVENT_IDS), event );
	return( IsBitSet( &eventData_[USER_EVENT], event ) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loadUserEvents
//
//@ Interface-Description
//  Load the event bits from the supplied 32-bit vector
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendEvents::loadUserEvents( Uint32 data )
{
	eventData_[USER_EVENT] = data;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loadAutoEvents
//
//@ Interface-Description
//  Load the event bits into the supplied 32-bit vector
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendEvents::loadAutoEvents( Uint32 data )
{
	eventData_[AUTO_EVENT] = data;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendEvents::SoftFault(const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate)  
{
	CALL_TRACE("TrendEvents::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_EVENTS,
							lineNumber, pFileName, pPredicate);
}

