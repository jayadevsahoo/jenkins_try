//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Swat.h
//----------------------------------------------------------------------------


#pragma once


#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif


#include "BDSendSettings.h"

#include "resource.h"       // main symbols
#include "TestSessionSet.h"
#include "TestResultSet.h"
#include "END_OF_BREATH_DATA_set.h"

#include "PD_EventDataSet.h"

#include "SwatDoc.h"

#include "ShowMsgContinueDlg.h"

#include "SettingsCapture.h"



#include "TestGraphset.h"

#include "GuiIOServer.h"



#include "DownloadSWDlg.h"
#include "VentStatusDlg.h"

#include "SwatView.h"
#include "ILogger.h"
#include "CLogger.h"


#include "BatchBuilder.h"



class BatchBuilder;

#define WM_NEW_LOG_RCVD					(WM_USER + 100)
#define NEW_SETTINGS_RCVD				(WM_USER + 101)
#define WM_CRITICAL_ERROR_RCVD			(WM_USER + 102)
#define WM_SHOW_MSG						(WM_USER + 103)
#define WM_HIDE_MSG						(WM_USER + 104)
#define WM_BREATH_PHASE_RCVD			(WM_USER + 105)
#define WM_REPORT_LOG_RCVD				(WM_USER + 106)
#define WM_TURN_ON_OFF_VENT				(WM_USER + 107)



#define	GUI_IO_SLEEP 30

#define SHOW_STATUS_DONT_LOG	-1
#define REPORT_TEST_OBJECTIVE	1
#define REPORT_TEST_PASS		2
#define REPORT_TEST_FAIL		3
#define REPORT_TEST_SKIP		4
#define REPORT_PASS_FAIL_INFO	5

#define REPORT_RESULT_HEADER	10
#define REPORT_RESULT_FOOTER	11

#define REPORT_RED				20
#define REPORT_GREEN			21

#define REPORT_MESSAGE			50
#define REPORT_TITLE			51
#define REPORT_OBJ_SEPERATOR	"_^_^_"
#define REPORT_WARNING			52
#define REPORT_ERROR			53


#define REPORT_IMAGE			70
#define REPORT_IMAGE_LANGUAGE   71





#define TEMP_SESSION_INIT "C:\\SWAT\\temp\\session_init.lua"
#define TEMP_SESSION_EXIT "C:\\SWAT\\temp\\session_exit.lua"

#define TEMP_LAST_BDELF_PATH "C:\\SWAT\\temp\\lastbdfilename.txt"
#define TEMP_LAST_GUIELF_PATH "C:\\SWAT\\temp\\lastguifilename.txt"

#define TEMP_LUNG_SETUP_DETAILS "C:\\SWAT\\temp\\lungDetails.txt"


#define TEMP_SCRIPT_INIT "C:\\SWAT\\temp\\script_init.lua"
#define TEMP_SCRIPT_EXIT "C:\\SWAT\\temp\\script_exit.lua"

//#define SCRIPT_ROOT_FOLDER "C:\\SWAT\\TestScripts" ----> replaced with CString m_strTestScriptPath;
#define TEMP_FILE_REPORT "C:\\SWAT\\temp\\testReport.txt"

#define SWAT_SETTING_FILE "C:\\SWAT\\Lua\\SWATinit.lua"

#define TEMP_FILE_BD_DWNLD_Log "C:\\SWAT\\temp\\BD_downnload_log.txt"
#define TEMP_FILE_GUI_DWNLD_Log "C:\\SWAT\\temp\\GUI_downnload_log.txt"

#define TEMP_FILE_BD_BATCH_DWNLD "C:\\SWAT\\temp\\BD_SW_batch.txt"
#define TEMP_FILE_GUI_BATCH_DWNLD "C:\\SWAT\\temp\\GUI_SW_batch.txt"

#define TEMP_BD_ELF_FILE_PATH_NAME "C:\\SWAT\\temp\\BDLoad.elf"
#define TEMP_GUI_ELF_FILE_PATH_NAME "C:\\SWAT\\temp\\GUILoad.elf"

#define TEMP_EXPAND_LOG_FILE "C:\\SWAT\\temp\\logfile.csv"
#define TEMP_GET_FILE_VERSION "C:\\SWAT\\temp\\getFileversion.txt"

#define TEMP_GET_FILE_VERSION_BAT	"c:\\SWAT\\temp\\getFileVer.bat"
#define TEMP_GET_FILE_VERSION_1		"c:\\SWAT\\temp\\fileVer1.txt"
#define TEMP_GET_FILE_VERSION_2		"c:\\SWAT\\temp\\fileVer2.txt"
#define TEMP_DEBUG_LOG				"c:\\SWAT\\batchSend.log"

#define TEMP_FILE_POWER_SWITCH_MISSING_ERROR  "c:\\SWAT\\temp\\pwrSwtMissing.htm"


#define SERVICE_MODE_BATCH	"C:\\gotoservicemode.bat"


#define SWAT_INTERNAL_LOG_FILE "C:\\SWAT\\temp\\swat_internal_log.txt"
#define SWAT_TEST_LOG_FILE "C:\\SWAT\\temp\\swat_test_log.htm"

#define SWAT_EMAIL_BAT	"C:\\SWAT\\sendEmail.bat"
#define SWAT_EMAIL_PL   "C:\\SWAT\\sendEmail.pl"
#define SWAT_SOUND   "C:\\SWAT\\swat.wav"


#define SWAT_USB_ROOT		"/usbfs/SWAT" //to give to vent to capture there

#define SWAT_SUCCESS      1
#define SWAT_FAIL	     -1

//Registry Keys
#define SWAT_REG_KEY				"SWAT\\User Settings"
#define SWAT_REG_PARAM_TREE_PATH	"Script_Tree_Path"
#define SWAT_REG_PARAM_LAST_FILE	"Last_Opened_Script_List_swt"
#define SWAT_REG_PARAM_LAST_BDELF	"Last BD elf path"
#define SWAT_REG_PARAM_LAST_GUIELF	"Last GUI elf path"
#define SWAT_REG_PARAM_LUA_EDITOR	"Lua editor"
#define SWAT_REG_PARAM_RECORD_SW_VER	"Record vent SW Version "
#define SWAT_REG_X_POS				"X co-ordinate"
#define SWAT_REG_PARAM_APPEND_TIME	"Append time in status logs "
#define SWAT_SEND_EMAIL				"email the logs"
#define SWAT_SET_REPORT_READ_ONLY   "Set report read-only if all pass"




#define SWAT_APPLICATION_TITLE		"SWAT - SoftWare Automation Tester   "







#define BUFLEN 8192
#define NPACK 10

//#define SEND_BATCH_PORT 9930 //old
#define SEND_BATCH_PORT 7020 //new


#define ssize_t signed int


#define lua_c

extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}


struct ScriptResults
{
	CString m_strFileName;
	int m_nObjective, m_nTotal, m_nPass, m_nFail, m_Info, m_nWarnings, m_nErrors;

	CTime m_timeStart, m_timeStop;

	ScriptResults()
	{
		m_strFileName.Empty();
		m_nObjective = m_nTotal = m_nPass = m_nFail = m_Info = m_nWarnings = m_nErrors = 0;
	}
	void ScriptResults::reset()
	{
		m_strFileName.Empty();
		m_nObjective = m_nTotal = m_nPass = m_nFail = m_Info = m_nWarnings = m_nErrors = 0;
	}

};


struct ImagesCaptured
{
	CString m_strDestinationFilePath;
	CString m_FileName;
	BOOL m_bCopied2PC;
};



struct SessionResults
{

	CString m_strSessionID;
	int m_nTotal, m_nPass, m_nFail, m_Info, m_nWarning, m_nError;

	SessionResults()
	{
		m_strSessionID.Empty();
		m_nTotal= m_nPass = m_nFail = m_Info = m_nWarning = m_nError = 0;
	}
	void SessionResults::reset()
	{
		m_strSessionID.Empty();
		m_nTotal= m_nPass = m_nFail = m_Info = m_nWarning = m_nError = 0;
	}

};
struct BreathData
{
	char m_chBreathType[300];
	char m_chBreathPhase[300];
	float m_fPEEP;
	float m_fBreathDuration;
	float m_fIERatio;
};

struct WaveformData
{
	int n_isAutozeroActive;

};

struct CapturedSettings{
	CString strSettingName;
	CString strSettingValue;
};

enum power_switch_t {NOT_CHECKED, NOT_CONNECTED, CONNECTED};

// CSwatApp:
//
struct VikingConsoleDriver; 

class CSwatApp : public CWinApp
{

private:
	CString m_strBDComPort;
	VikingConsoleDriver *m_pVikingConsoleDriver;
public:
	CSwatApp();
	void InitLua();
	void CloseLua();
	void LogStatus(CString str, int n = 0);
	
	CString m_strFile4Lua;
	CStringArray m_strArrayFiles;

	lua_State* m_L;
	/* the Lua interpreter */


	//logged in user name
	CString m_strUserName;
	
	//Handle to the datacature dll module
	HINSTANCE m_hinstLib; 


	CGUISettingsCapture* m_pGUISettingsCapture;

	bool m_bScriptRunning;
	//batch script running or completed

	bool m_bRecording; //current status
	bool m_bRecord; //flag to turn off recording

	bool m_bRun;
	//batch script running or completed

	bool m_bUserStopped;
	//batch script running or completed

	//Pause/unpause the script
	bool m_bPause;


	bool m_bScriptResult;
	//last script result

	int m_nTotalScripts;
	//Total scripts in the list to run

	int m_nCurentScriptNumber;
	//Number of current script running in the list box

	bool m_bReset;

	CTime m_timeSessionStart;
	CTime m_timeSessionEnd;


	//IP and port numbers for BD/GUI
	CString m_strBD_IPAddr;
	CString m_strGUI_IPAddr;


	//IPs for the JTAGs for SW download
	CString m_strBD_JTAG_IPAddr;
	CString m_strGUI_JTAG_IPAddr;

	CString m_strPowerSwitch_IPAddr;

	CString m_strPortPower;
	CString m_strPortServiceMode;
	CString m_strPortVentSwitch;

	CString m_strVentType;




	CString m_strMyPC_IPAddr;


	CString strSWATVersion;
	//Vent name and other info
//	CString m_strVentName; //replaced by m_strVentSerialNumber

	CString GetLanguageInString(ALT_LANGUAGE lang);

	//Lung name and other info
	CString m_strLungName;

	ScriptResults m_ScriptResults; //stores current file and updtaes results in here

	SessionResults m_SessionResults;

	//status log to show in the status window
	CString m_strLog;
	
	
	//To control the status String "m_strLog" in multi threads
	CRITICAL_SECTION m_CriticalSection;


	//Test session recordset in the database
	CTestSessionSet m_SessionSet;


	CString m_strCurrentSessionID;
	//Session ID for the batch of scripts run in one go


	CTestResultSet m_TestResultSet;
	//Table test result recordset


	CTestGraphset m_TestGraphset;



	CShowMsgContinueDlg *m_ShowMsgContinueDlg;

	int m_nBD_SW_DownloadStatus;
	int m_nGUI_SW_DownloadStatus;

	CString m_str_BD_SW_Dwnld_log;
	CString m_str_GUI_SW_Dwnld_log;
	int m_nDownloading;


	bool m_bNewSettingsRcvd;

	CString m_strSWATLogStatus; //SWAT logs

	
	CString m_strSettings0, m_strSettings1, m_strSettings2, m_strSettings3, m_strSettings4; //captured settings, 0 is latest

	class CBDSendSettings* m_pBDSendSettings;


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnFileNew();
	virtual int ExitInstance();

	bool OpenDB();
	bool CloseDB();

public:
	void CreateSessionID();
	void GetScriptResult(CString strFile);
	void Create_Lua_Init_Exit_Files(CString strScriptFile);

	void ShowContinueMessage(CString strMessage);
	void ClearMessage();

	void GetNewSettings(CString str);
	void GenerateReport(CString strScriptName, CString strSessionID);
	void CaptureSettings(void);

	void EnableBDSendSettings(); //load dll for sending settings

	void CreateRegistryKey();
	void SaveRegistrySetting(CString strParam, CString strVal);
	CString ReadRegistrySetting(CString strParam);
	void ReadAllRegistrySettings();

	void SaveString2LocalTempFile(CString strTempFilename, CString str);
	CString ReadLocalTempFile(CString strTempFilename);

	afx_msg void OnSwatSwDwnload();
	afx_msg void OnSwatCaptureSettings();
	afx_msg void OnSwatPatientSettings();
	void ReadLuaInitFile();//reads the luaInitfile to set the member variable value
	bool GetSetLungSetup(); //reads and asks user to verify lung details, saves if changed
	void MapDrive(); //map the shared drive per SWATinit parameter

	void RefreshSWATLogs(CString &str);


	CSwatView *m_pView;



	
	int GUI_IODisconnect( );

	int GUI_IOConnect( );

	CDownloadSWDlg* m_pDownloadSWDlg;
	CVentStatusDlg* m_pVentStatusDlg;


	int BDPushSettingsToHost(CString strSettings, CString strHost, CString strPort);

	afx_msg void OnSwatCleanDatabase();

	CString m_strTestScriptPath;//Default path for the test scripts
	
	BOOL m_bPopErrorMsg;


	CString GUI_IOGetXYWH(CString strID);
	int GUI_IOClickXY(int X, int Y, float nVariable);
	int GUI_IOClickXYDirect(int X, int Y);

	int GUI_IOClickXY_DBL(int X, int Y);
	CString GUI_IOGetValue(CString strID);
	CString GUI_IOGetText(CString strID);
	CString GUI_IOGetRowText(CString strID, int nRow);
	CString GUI_IOGetCellText(CString strResID, int nRow, int nCol);

	int GUI_IsActivated(CString strID);
	int GUI_IsSelected(CString strID);
	int GUI_IsBlinking(CString strResID);
	int GUI_IsAdornment(CString strResID);

	int GUI_CaptureScreen(CString strFileName);
	int GUI_CaptureScreenStatus( );

	CWinThread* m_pThreadCopyFTP;//thread to copy files over ftp
	//int m_nImagesYetToCopyOverFTP;
	CList<ImagesCaptured,ImagesCaptured&> m_listCapturedImages;
	void CopyAllCapturedImages();

	//To control ftp connection over multiple threads
	//CRITICAL_SECTION m_CriticalSectionFTP;
	CString FormatImageName(CString strFileName);
	int m_nImageCounter;
	void ResetImageCounter( );
	BOOL m_bTwoImagesForComparison;


	CString m_strSWAT_DECODA_PATH; //"C:\\Program Files\\Decoda\\Decoda.exe"
	CString m_strSWAT_SCITE_PATH; //"C:\\Program Files\\Lua\\5.1\\SciTE\\scite.exe"
	CString m_strTTP_MACRO_EXE;		//"C:\\Program Files\\TTERMPRO\\ttpmacro.exe"


	int GUI_SimulateBDAlarm(CString strAlarmId, CString strCondAugOptions, CString strDepenAugOptions);


	int PlayWaveFile(CString strPath);

	void ReadErrorPopupValue();

	int GUI_IOKnob(int n);

	afx_msg void OnGuiConnectGUI();
	
	afx_msg void OnGuiDisconnectGUI();

	bool Enable_DB_Logging();
	bool Disable_DB_Logging();
	afx_msg void OnUpdateGuiConnectGUI(CCmdUI *pCmdUI);


	bool m_bGUIConnected;
	afx_msg void OnUpdateGuiDisconnectGUI(CCmdUI *pCmdUI);

	//check if GUI is connected or not, if not connected give error and stop script
	void CheckGUIConnection( );


	//Thread to monitor breath phase info and display on the GUI
	static UINT Thread_BreathDisplay(LPVOID lp);

	//Thread to capture waveform data from BD
	static UINT Thread_WaveformData(LPVOID lp);

	//Thread to copy images over ftp in background
	static UINT Thread_CopyImageOverFTP(LPVOID lp);

	//copy image over ftp
	static UINT CopyImageFromVent(CString strFile, CString strHDrivepath);

	//Thread to download SW to the vent
	static UINT Thread_SWDownloader(LPVOID lp);


	//SWAT internal log file
	CStdioFile m_SWAT_log_file;
	//SWAT internal test script log file
	CStdioFile m_SWAT_test_log_file;

	//SWAT Report, created fresh for each test script report
	CStdioFile	m_ReportFile;
	BOOL		m_bSetCurrentReportReadOnly;
	CString		m_strLockPassReports;//global setting to lock reports if all pass


	//session file, created for each session
	CStdioFile m_SessionFile;

	//current breath data
	BreathData m_CurrentBreathData;

	//current waveform data
	WaveformData m_CurrentWaveformData;

	//[ch] gives buttonID of button which has focus
	int GetFocusButtonID( char *ch);

	CString GUI_IOGetError(int nEnum);

	int GUI_Bezel_Key_Down(CString &strBKey);
	int GUI_Bezel_Key_Up(CString &strBKey);

	int GUI_Bezel_Key_Click(CString strBKey, int nTime);

	void CreateReportFile(CString& strSessionID);

	void ConvertSpecialChar(CString &str);

	int Over_Write_PD_ValueEx(CString strParam, CString strValue);
	CString Get_Raw_PD_ValueEx(CString strParam);
	int Reset_PD_ValuesEx( );

	CString VerifyManually_Ex( CString strParam, CString strExpected);

	//logs the given string to report based on value of n 
	//(n can be REPORT_TEST_PASS -> It's a psaa, color it green in the HTML
	void LogReportMessage(CString& str, int n);

	//Reports header in HTML report
	int LogReportHeader(CString& strScriptName);

	//Reports footer in HTML report, if (bFailed = true) mentions the procedure failed
	void LogReportFooter(CString& strScriptName, bool bFailed);

	//Parse out this opbjective and display on the SWAT UI while running a test
	CString m_strTestObjective;

	//check if given sensorID is valid
	bool CheckValidSensorID( unsigned int sensorID);

	bool m_bVent_Batch_enabled;

	//Takes the script file name/path and returns the test report file name to create/open
	CString GetReportFileName(CString strScriptFileName);

	afx_msg void OnHelpTest();

	CString m_strCurrentOpenScriptListFile;

	CString m_strEditorPath;
	bool m_bAppendTimeInStatusWindow;

	CString m_strRecordSWinReport;

	bool m_bSendMail;
	void EmailUser(CString strStringOrFilename, CString strSubject, BOOL bIsFile = 0);//email any data to the user


	afx_msg void OnSwatSettings();
	BOOL Power_on_off_vent(CString strCaller, int nPortNum, bool bPowerOnOff);

	CString Get_Sent_Setting(CString str);

	CList<CapturedSettings, CapturedSettings&> m_capturedSettingsList;

	//functions to store all file names and getting version info
	CStringArray m_strArrFileNames;
	int ExecuteBlocking(const char *appName, const char *cmdLine);
	CString GetFileVersions( );
	CString AddFileToRecordVersion(CString strFilePathName);
	void RemoveFileToRecordVersion();


	int nSWAT_XOpen_Position;
	int nSWAT_XClose_Position;
	void VentStatus();

	afx_msg void OnSwatStopBdsimulations();
	afx_msg void OnUpdateSwatStopBdsimulations(CCmdUI *pCmdUI);

	void AddSessionTestResultsToFile();
	void ReadSessionTestResultsFromFile(CStdioFile *file);

	CString GetScriptNameToRun();

	CString m_strVentBDSWVersion;
	CString m_strVentGUISWVersion;
	CString m_strVentLanguage;
	CString m_strVentSerialNumber;


	CString GetUserInput(CString strTitle);


	int TestLua(CString str1, CString str2);

	
	int GUI_IOGetVentSWVersion(GuiIOVentInfoResp *out);

	BOOL PingVent(BOOL bPrintError = 0);
	int m_nGuiIOServerVerexpected;

	bool m_bVentDownloaderInProgress;
	bool m_bVentDownloaderRun;


	CString GetBDComPort();//returns BD com port
	BOOL TurnOnHitS(bool bScriptCall, bool bTurnOn = true);


	afx_msg void OnSoftwaredownloadStartdownload();
	afx_msg void OnSoftwaredownloadCanceldoo();
	afx_msg void OnSwatTakeventscreenshot();
	afx_msg void OnUpdateSwatTakeventscreenshot(CCmdUI *pCmdUI);


	void WaterMarkOnOff(int n);
	void MouseOnOff(int n);
	void SetWidgetProperties(CString str, int n) ;

	void SetWindowsPath();
	CString GetVentLanguage();
};

extern CSwatApp theApp;













 enum SettingIdType
    {
      NULL_SETTING_ID      = -1,		// undefined setting id...
      LOW_SETTING_ID_VALUE =  0,		// lowest setting value...

      //==================================================================
      // Batch Setting Ids...
      //==================================================================

      LOW_BATCH_ID_VALUE = LOW_SETTING_ID_VALUE,  // must start at 0!!

      // Batch Settings that are sent to Breath-Delivery...
      LOW_BATCH_BD_ID_VALUE = LOW_BATCH_ID_VALUE,
      LOW_BATCH_BD_BOUNDED_ID_VALUE = LOW_BATCH_BD_ID_VALUE, 

      APNEA_FLOW_ACCEL_PERCENT = LOW_BATCH_BD_BOUNDED_ID_VALUE,
      APNEA_INSP_PRESS,		// Apnea Insp. Press.
      APNEA_INSP_TIME,		// Apnea Inspiratory Time...
      APNEA_INTERVAL,		// Apnea Interval Time...
      APNEA_MIN_INSP_FLOW,	// Apnea Minimum Inspiratory Flow...
      APNEA_O2_PERCENT,		// Apnea Oxygen Percentage...
      APNEA_PEAK_INSP_FLOW,	// Apnea Peak Inspiratory Flow...
      APNEA_PLATEAU_TIME,	// Apnea Plateau Time...
      APNEA_RESP_RATE,		// Apnea Respiratory Rate...
      APNEA_TIDAL_VOLUME,	// Apnea Tidal Volume...
      DISCO_SENS,		// Disconnection Sensitivity...
      EXP_SENS,			// Expiratory Sensitivity...
      EXP_PAV_SENS,			// Expiratory PAV Sensitivity...
      FLOW_ACCEL_PERCENT,	// Flow Acceleration Percent...
      FLOW_SENS,		// Flow Sensitivity...
      HIGH_CCT_PRESS,		// High Circuit Pressure...
      HIGH_INSP_TIDAL_VOL,	// High Inspired Tidal Volume (ATC/PA/VS/VC+)...
      HUMID_VOLUME,		// Humidifier Volume (ATC)...
      IBW,			// Ideal Body Weight...
      INSP_PRESS,		// Inspiratory Pressure...
      INSP_TIME,		// Inspiratory Time...
      MIN_INSP_FLOW,		// Minimum Inspiratory Flow...
      OXYGEN_PERCENT,		// Oxygen Percentage...
      INCREASE_O2_ELEVATE_PERCENTAGE, // increased o2 user percentage
      //increased o2 actual percentage (calculated based on normal o2 and elevated o2)
      INCREASE_O2_DELIVER_PERCENTAGE,
      //increase o2 actual percentage (calculated based on apnea o2 and elevated o2)
      INCREASE_O2_APNEA_DELIVER_PERCENTAGE,
      PEAK_INSP_FLOW,		// Peak Inspiratory Flow...
      PERCENT_SUPPORT,		// Percent (Work) Support (ATC)...
      PLATEAU_TIME,		// Plateau Time...
      PEEP, 			// Positive End-Expiratory Press...
      PEEP_HIGH, 		// High PEEP (BiLevel)...
      PEEP_HIGH_TIME, 		// High PEEP time (BiLevel)...
      PEEP_LOW, 		// Low PEEP (BiLevel)...
      PRESS_SENS,		// Pressure Sensitivity...
      PRESS_SUPP_LEVEL,		// Pressure Support Level...
      RESP_RATE,		// Respiratory Rate...
      TIDAL_VOLUME,		// Tidal Volume...
      TUBE_ID,			// Tube Id (ATC)...
      VOLUME_SUPPORT,		// Volume Support (VTPC)...
	  HIGH_SPONT_INSP_TIME, // High spontaneous inspiratory time (NIV)...
			IESYNC_OMEGA,
			IESYNC_OMEGA_PI,
			IESYNC_BETA,
			IESYNC_TRIGGER_SENS,
			IESYNC_CYCLE_SENS,

			HIGH_BATCH_BD_BOUNDED_ID_VALUE = IESYNC_CYCLE_SENS,
      LOW_BATCH_BD_DISCRETE_ID_VALUE, 

      APNEA_FLOW_PATTERN = LOW_BATCH_BD_DISCRETE_ID_VALUE,// Apnea Flow Pattern
      APNEA_MAND_TYPE,		// Apnea Mandatory Type...
      FIO2_ENABLED,		// FIO2 Option Enabled...
      FLOW_PATTERN,		// Flow Pattern...
      HUMID_TYPE,		// Humidification Type...
      NOMINAL_VOLT,		// Nominal Line Voltage...

			// Maintain the order of the main vent setup settings that follow
			// as they are required to be in order of dependency for proper 
			// processing by their valueUpdate callbacks.
			// BEGIN -- main vent setup settings
      PATIENT_CCT_TYPE,		// Patient Circuit Type...
      VENT_TYPE,		// Vent Type (NIV)...
			MODE,					// Mode...
			MAND_TYPE,				// Mandatory Type..
			SUPPORT_TYPE,			// Support Type...
			TRIGGER_TYPE,			// Trigger Type...
			// END -- main vent setup settings

			TUBE_TYPE,		  		// Tube Type (ATC)...
			LEAK_COMP_ENABLED,		// Leak Compensation enabled...
			IESYNC_MODE,		// IESync Mode

			HIGH_BATCH_BD_DISCRETE_ID_VALUE = IESYNC_MODE,
      HIGH_BATCH_BD_ID_VALUE          = HIGH_BATCH_BD_DISCRETE_ID_VALUE,

      // Batch Settings that are NOT sent to Breath-Delivery...
      LOW_BATCH_NONBD_ID_VALUE,
      LOW_BATCH_NONBD_BOUNDED_ID_VALUE = LOW_BATCH_NONBD_ID_VALUE,

      APNEA_EXP_TIME = LOW_BATCH_NONBD_BOUNDED_ID_VALUE,// Apnea Exp. Time...
      APNEA_IE_RATIO,		// Apnea Inspiratory-to-Expiratory Time Ratio
      ATM_PRESSURE,		// Atmospheric Pressure...
      EXP_TIME,			// Expiratory Time...
      HEIGHT_METRIC,                // Patient's Height (used to set IBW/PBW) in cm.
      HEIGHT_ENGLISH,               // Patient's Height (used to set IBW/PBW) in inchees.
      HIGH_EXH_MINUTE_VOL,	// High Exhaled Minute Volume...
      HIGH_EXH_TIDAL_VOL,	// High Exhaled Tidal Volume...
      HIGH_RESP_RATE,		// High Respiratory Rate...
      HL_RATIO,			// Ratio of Th to Tl (BiLevel)...
      IE_RATIO,			// Inspiratory-to-Expiratory Time Ratio...
      LOW_CCT_PRESS,		// Low Circuit Pressure...
      LOW_EXH_MAND_TIDAL_VOL,	// Low Exhale Mandatory Tidal Volume...
      LOW_EXH_MINUTE_VOL,	// Low Exhale Minute Volume...
      LOW_EXH_SPONT_TIDAL_VOL,	// Low Exhale Spontaneous Tidal Volume...
      MINUTE_VOLUME,		// Minute Volume...
      PEEP_LOW_TIME, 		// Low PEEP time (BiLevel)...
      VSUPP_IBW_RATIO,		// Vsupp-to-IBW Ratio...
      VT_IBW_RATIO,		// Vt-to-IBW Ratio...

      HIGH_BATCH_NONBD_BOUNDED_ID_VALUE = VT_IBW_RATIO,
      LOW_BATCH_NONBD_DISCRETE_ID_VALUE,

      APNEA_CONSTANT_PARM = LOW_BATCH_NONBD_DISCRETE_ID_VALUE,// Apnea Constant
      CONSTANT_PARM,			// Constant-During-Rate-Change...
      DCI_BAUD_RATE,			// DCI's Baud Rate...
      DCI_DATA_BITS,			// DCI's Data Bits...
      DCI_PARITY_MODE,			// DCI's Parity Mode...
      COM1_BAUD_RATE=DCI_BAUD_RATE, // COM1's Baud Rate...
      COM1_DATA_BITS,           // COM1's Data Bits...
      COM1_PARITY_MODE,         // COM1's Parity Mode...
      GENDER,                   // Patient's Gender (used for Height setting)
      MONTH,				// Month of the Year...
      PRESS_UNITS,			// Units to use for pressures...
      SERVICE_BAUD_RATE,		// Service Mode's Baud Rate...
      COM2_BAUD_RATE,           // COM2's Baud Rate...
      COM2_DATA_BITS,           // COM2's Data Bits...
      COM2_PARITY_MODE,         // COM2's Parity Mode...
      COM3_BAUD_RATE,           // COM3's Baud Rate...
      COM3_DATA_BITS,           // COM3's Data Bits...
      COM3_PARITY_MODE,         // COM3's Parity Mode...
      COM1_CONFIG,              // COM1's configuration
      COM2_CONFIG,              // COM2's configuration
      COM3_CONFIG,              // COM3's configuration
      DATE_FORMAT,              // Date format
                     

      HIGH_BATCH_NONBD_DISCRETE_ID_VALUE = DATE_FORMAT,
       LOW_BATCH_NONBD_SEQUENTIAL_ID_VALUE,

      DAY = LOW_BATCH_NONBD_SEQUENTIAL_ID_VALUE,// Day of the Week...
      DISPLAY_CONTRAST_DELTA,		// Display Contrast Delta...
      HOUR,				// Hour of the Day...
      MINUTE,				// Minute of the Hour...
      YEAR,				// Year...

      HIGH_BATCH_NONBD_SEQUENTIAL_ID_VALUE = YEAR,

      HIGH_BATCH_NONBD_ID_VALUE = HIGH_BATCH_NONBD_SEQUENTIAL_ID_VALUE,
      HIGH_BATCH_ID_VALUE       = HIGH_BATCH_NONBD_ID_VALUE,

      NUM_BATCH_IDS = (HIGH_BATCH_ID_VALUE - LOW_BATCH_ID_VALUE + 1),

      //==================================================================
      // Non-Batch Setting Ids...
      //==================================================================

      LOW_NON_BATCH_ID_VALUE = HIGH_BATCH_ID_VALUE + 1,
      LOW_NON_BATCH_BOUNDED_ID_VALUE = LOW_NON_BATCH_ID_VALUE,

      PRESS_VOL_LOOP_BASE = LOW_NON_BATCH_BOUNDED_ID_VALUE,//Baseline for P-V Loop Plot...
      TREND_CURSOR_POSITION,                               //Trend cursor position
      HIGH_NON_BATCH_BOUNDED_ID_VALUE = TREND_CURSOR_POSITION,
      LOW_NON_BATCH_DISCRETE_ID_VALUE,

      FLOW_PLOT_SCALE = LOW_NON_BATCH_DISCRETE_ID_VALUE,// Flow Axis Scale...
      PLOT1_TYPE,			// Type of Waveform Plot #1...
      PLOT2_TYPE,			// Type of Waveform Plot #2...
      TREND_SELECT_1,            // Trend display 1
      TREND_SELECT_2,            // Trend display 2
      TREND_SELECT_3,            // Trend display 3
      TREND_SELECT_4,            // Trend display 4
      TREND_SELECT_5,            // Trend display 5
      TREND_SELECT_6,            // Trend display 6
      TREND_TIME_SCALE,          // Trend time scale to display
      TREND_PRESET,              // Trend preset                   
      SHADOW_TRACE_ENABLE,		// Enable/Disable Shadow Trace...
      PRESSURE_PLOT_SCALE,		// Scale of the Pressure Axis...
      TIME_PLOT_SCALE,			// Scale of the Time Axis...
      VOLUME_PLOT_SCALE,		// Scale of the Volume Axis...
			IESYNC_PLOT_SCALE1,
			IESYNC_PLOT_SCALE2,
      
			HIGH_NON_BATCH_DISCRETE_ID_VALUE  = IESYNC_PLOT_SCALE2,
      LOW_NON_BATCH_SEQUENTIAL_ID_VALUE,

      ALARM_VOLUME = LOW_NON_BATCH_SEQUENTIAL_ID_VALUE,// Alarm Volume...
      DISPLAY_BRIGHTNESS,		// Display Brightness...
      DISPLAY_CONTRAST_SCALE,		// Display Contrast Scale...

      HIGH_NON_BATCH_SEQUENTIAL_ID_VALUE = DISPLAY_CONTRAST_SCALE,
      HIGH_NON_BATCH_ID_VALUE = HIGH_NON_BATCH_SEQUENTIAL_ID_VALUE,

      NUM_NON_BATCH_IDS = (HIGH_NON_BATCH_ID_VALUE -
      			   LOW_NON_BATCH_ID_VALUE + 1),



      //==================================================================
      // Totals...
      //==================================================================

      HIGH_SETTING_ID_VALUE = HIGH_NON_BATCH_ID_VALUE,

      TOTAL_SETTING_IDS = (HIGH_SETTING_ID_VALUE  + 1)
    };


enum UnsignedDataIds
	{
        // KEEP THIS FIRST ENTRY AT 0
	    INVALID,

	    // Insp
	    INSP_I_PREVCMD_24V_PSOL_PWR_ON,
	    INSP_I_PREVCMD_TST_PAT_OVR_PRES_DECT,
	    INSP_I_PREVCMD_OVR_VT_INSP_LATCH_CLEAR,
	    INSP_I_PREVCMD_INSPIRATORY_AUTO_ZERO_SOLENOID,
	    INSP_I_PREVCMD_SAFETY_VALVE_SOLENOID,
	    INSP_I_PREVCMD_DAC1_WRAP_HAMMING,
	    INSP_I_PREVCMD_DELIVERY_PSOL_DRIVER_HAMMING,
	    INSP_I_PREVCMD_BUV_EXH_VALVE_DRVR_HAMMING,
	    INSP_I_PREVCMD_MESSAGE_CHECKSUM_ERROR,
	    INSP_I_PREVCMD_DELIVERY_PSOL_DRIVER_STATUS,
	    INSP_I_PREVCMD_BUV_EXH_VALVE_DRIVER_PWM,

	    INSP_I_OVER_VT_INSP_LATCHED_N,
	    INSP_I_SWITCH_IN_EXH_BUV_DRVR,
	    INSP_I_OVER_115CMH2O,
	    INSP_I_OPM_DEACTIVATE_SV_CMD,
	    INSP_I_OVR_VT_DETECT_INSP_N,
	    INSP_I_EXH_MTR_DRV_BUV,

        INSP_I_SIDE_PRESSURE_SENSOR,
        INSP_I_FLOW_SENSOR,
        INSP_I_TEMPERATURE_SENSOR,
        INSP_I_PSOL_CURRENT,
        INSP_I_ATMOSPHERIC_PRESSURE_SENSOR,
        INSP_I_EXH_MTR_BUV_CURRENT,
        INSP_I_DAC_WRAP_ADC1,
        INSP_I_DAC_WRAP_ADC2,
        INSP_I_DAC_WRAP_ADC3,
        INSP_I_DAC_WRAP_ADC4,

        INSP_I_PLUS_3_3VREF_INSP_SCALED,
        INSP_I_PRES_PATIENT_INSP_BUF,
        INSP_I_PLUS_3_3VREF_INSP_OPM_SCALED,
        INSP_I_TEST_ADC,
        INSP_I_EXH_VALVE_I_BUF,
        INSP_I_VENTURI_I_SIG,
        INSP_I_SV_I_SIG,
        INSP_I_O2_CONC_PATIENT_INSP_BUF,
        INSP_I_PATIENT_OVR_PRES_SNSR_BUF,
        INSP_I_PLUS_3_3VREF_PI_EXH_SCALED,
        INSP_I_PLUS_24V_SCALED,
        INSP_I_MIX_ACCUM_PRES_INSP_BUF,
        INSP_I_QINSP_HELIOX_FLW_BUF,

        // Exh
        EXH_I_PREVCMD_DIGITAL_POT_SETTING,
        EXH_I_PREVCMD_OVR_VT_EXH_LATCH_CLEAR,
        EXH_I_PREVCMD_SAFETY_VALVE_BUV_SOLENOID,
        EXH_I_PREVCMD_EXHALATION_HEATER,
        EXH_I_PREVCMD_EXHALATION_AUTO_ZERO_SOLENOID,
        EXH_I_PREVCMD_DAC2_WRAP_HAMMING,
        EXH_I_PREVCMD_DAMPENING_HAMMING,
        EXH_I_PREVCMD_EXH_VLV_POS_I_CMD_HAMMING,
        EXH_I_PREVCMD_EXH_VLV_NEG_I_CMD_HAMMING,
        EXH_I_PREVCMD_VENTURI_DRIVER_HAMMING,
        EXH_I_PREVCMD_MESSAGE_CHECKSUM_ERROR,
        EXH_I_PREVCMD_VENTURI_DRIVER,

        EXH_I_DIG_POT_AUTO_ZERO_STATUS,
        EXH_I_DIG_POT_AUTO_ZERO_STATE,
        EXH_I_DIG_POT_WIPER_POSITION,
        EXH_I_DIG_POT_INIT_VALUE,
        EXH_I_OVR_VT_EXH_LATCHED_N,
        EXH_I_PSOL_BYPASS_BUV,
        EXH_I_ENABLE_MANUAL_ADJUST_DIGITAL_POT,
        EXH_I_OVT_VT_DETECT_EXH_N,

        EXH_I_FLOW_SENSOR,
        EXH_I_GAS_TEMPERATURE_SENSOR,
        EXH_I_SIDE_PRESSURE_SENSOR,
        EXH_I_SAFETY_VALVE_CURRENT,
        EXH_I_HEATER_TEMPERATURE,
        EXH_I_MOTOR_CURRENT,
        EXH_I_POPPET_POSITION,
        EXH_I_POPPET_VELOCITY,
        EXH_I_POPPET_ACCELERATION,
        EXH_I_DAC_WRAP_ADC6,
        EXH_I_DAC_WRAP_ADC7,

        EXH_I_VENTURI_I_SIG,
        EXH_I_EXH_PRES_BUF2,
        EXH_I_PLUS_3_3VREF_PI_INSP_SCALED,
        EXH_I_TMP_LVDT_BUF,
        EXH_I_PLUS_3_3VREF_EXH_SCALED,
        EXH_I_EXH_VALVE_CURRENT_AUTO_ZERO,
        EXH_I_FILTER_OPTO_BUF,
        EXH_I_QEXH_HELIOX_FLW_BUF,
        EXH_I_EXH_VALVE_POSITION,

        // Power Control Module (PCM),
        PCM_I_PREVCMD_POWER_MODE,
        PCM_I_PREVCMD_SECONDARY_ALARM,
        PCM_I_PREVCMD_POWER_OFF_COMMAND,
        PCM_I_PREVCMD_POWER_FAIL_CAP_CHARGE,
        PCM_I_PREVCMD_BATTERY_LAMPS,
        PCM_I_PREVCMD_BATTERY_CHANNEL_SELECT,
        PCM_I_PREVCMD_POWER_INTERFACE,

        PCM_I_PRIMARY_BATTERY_TEMP_BUFFER,
        PCM_I_EXTENDED_BATTERY_TEMP_BUFFER,
        PCM_I_BOARD_TEMPERATURE,
        PCM_I_VCC_STANDBY,
        PCM_I_POWER_24_VOLT_BUS,
        PCM_I_EXTENDED_BATTERY_SENSE,
        PCM_I_PRIMARY_BATTERY_SENSE,
        PCM_I_ALARM_CABLE_VOLTAGE,
        PCM_I_POWER_FAIL_CAP_VOLTAGE,
        PCM_I_CURRENT_CHARGE_BUFFER,
        PCM_I_PRIMARY_BATTERY_CURRENT_BUFFER,
        PCM_I_POWER_MINUS_5_VOLT_MONITOR,
        PCM_I_POWER_3_3_VOLT_MONITOR,
        PCM_I_POWER_3_3_VOLT_REFERENCE,
        PCM_I_VOSENSE,
        PCM_I_V_SUP_SENSE,
        PCM_I_PRIMARY_BATTERY_VOLTAGE_BUFFER,
        PCM_I_EXTENDED_BATTERY_VOLTAGE_BUFFER,

        PCM_I_SELECTED_BUF,

        PCM_I_PRIMARY_BATTERY_CHARGE_INHIBIT_HIGH_TEMP,
        PCM_I_PRIMARY_BATTERY_PRESENT,
        PCM_I_PRIMARY_BATTERY_STATE,
        PCM_I_PRIMARY_BATTERY_COMMS_STATUS,
        PCM_I_EXTENDED_BATTERY_CHARGE_INHIBIT_HIGH_TEMP,
        PCM_I_EXTENDED_BATTERY_PRESENT,
        PCM_I_EXTENDED_BATTERY_STATE,
        PCM_I_EXTENDED_BATTERY_COMMS_STATUS,
        PCM_I_CHARGER_MODE,
        PCM_I_CHARGER_CHANNEL_SELECT,
        PCM_I_POWER_SUPPLY_FAILURE_DETECTION,
        PCM_I_VENTILATOR_POWER_SWITCH_SWON,
        PCM_I_CC_FAN_FAILURE,
        PCM_I_CHARGER_FAULT_EXCEEDED_CHARGE_TIME,
        PCM_I_CHARGER_FAULT_OVER_TEMPERATURE,
        PCM_I_COMPRESSOR_PRESENT,

        PCM_I_PRIMARY_BATTERY_STATUS,
        PCM_I_PRIMARY_BATTERY_TEMP,
        PCM_I_PRIMARY_BATTERY_SERIAL_NUMBER,
        PCM_I_PRIMARY_BATTERY_RELATIVE_STATE_OF_CHARGE,
        PCM_I_PRIMARY_BATTERY_ABSOLUTE_STATE_OF_CHARGE,
        PCM_I_PRIMARY_BATTERY_REMAINING_CAPACITY,
        PCM_I_PRIMARY_BATTERY_FULL_CHARGE_CAPACITY,
        PCM_I_PRIMARY_BATTERY_RUN_TIME_TO_EMPTY,
        PCM_I_PRIMARY_BATTERY_MANUFACTURE_DATE,
        PCM_I_PRIMARY_BATTERY_CYCLE_COUNT,
		PCM_I_PRIMARY_BATTERY_VOLTAGE,

        PCM_I_EXTENDED_BATTERY_STATUS,
        PCM_I_EXTENDED_BATTERY_TEMP,
        PCM_I_EXTENDED_BATTERY_SERIAL_NUMBER,
        PCM_I_EXTENDED_BATTERY_RELATIVE_STATE_OF_CHARGE,
        PCM_I_EXTENDED_BATTERY_ABSOLUTE_STATE_OF_CHARGE,
        PCM_I_EXTENDED_BATTERY_REMAINING_CAPACITY,
        PCM_I_EXTENDED_BATTERY_FULL_CHARGE_CAPACITY,
        PCM_I_EXTENDED_BATTERY_RUN_TIME_TO_EMPTY,
        PCM_I_EXTENDED_BATTERY_MANUFACTURE_DATE,
        PCM_I_EXTENDED_BATTERY_CYCLE_COUNT,
    	PCM_I_EXTENDED_BATTERY_VOLTAGE,

        PCM_I_FAN1_TACH,

        // Compressor Module (CMPRSR)
        CMPRSR_I_PREVCMD_PRESSURE_LOW_ERROR,
        CMPRSR_I_PREVCMD_PRESSURE_HIGH_ERROR,
        CMPRSR_I_PREVCMD_MOTOR_LOW_SPEED_ERROR,
        CMPRSR_I_PREVCMD_MOTOR_HIGH_SPEED_ERROR,
        CMPRSR_I_PREVCMD_PRESSURIZATION_TIMEOUT_ERROR,
        CMPRSR_I_PREVCMD_TEST_SPEED_ERROR,

        CMPRSR_I_PREVCMD_CLEAR_PRESSURIZATION_TIMEOUT_ERROR,
        CMPRSR_I_PREVCMD_CLEAR_MOTOR_CONTROLLER_FAULT,
        CMPRSR_I_PREVCMD_COMPRESSOR_MODE,
        CMPRSR_I_PREVCMD_POWER_MODE,

        CMPRSR_I_PREVCMD_COMPRESSOR_CONTROL,

        CMPRSR_I_PREVCMD_COMPRESSOR_UNLOAD_SOLENOID,
        CMPRSR_I_PREVCMD_BARE_FAN_ASSEMBLY,
        CMPRSR_I_PREVCMD_PDC_INT,
        CMPRSR_I_PREVCMD_POWER_FAIL_COMPRESSOR,
        CMPRSR_I_PREVCMD_COMPRESSOR_MOTOR,

        CMPRSR_I_PREVCMD_BATTERY_LAMPS,
        CMPRSR_I_PREVCMD_BATTERY_CHANNEL_SELECT,
        CMPRSR_I_PREVCMD_POWER_INTERFACE,

        CMPRSR_I_PREVCMD_PRESSURE_LOW_HAMMING,
        CMPRSR_I_PREVCMD_PRESSURE_HIGH_HAMMING,
        CMPRSR_I_PREVCMD_MOTOR_LOW_SPEED_HAMMING,
        CMPRSR_I_PREVCMD_MOTOR_HIGH_SPEED_HAMMING,
        CMPRSR_I_PREVCMD_PRESSURIZATION_TIMEOUT_HAMMING,
        CMPRSR_I_PREVCMD_TEST_SPEED_HAMMING,
        CMPRSR_I_PREVCMD_MESSAGE_CHECKSUM_ERROR,

        CMPRSR_I_PREVCMD_PRESSURE_LOW,
        CMPRSR_I_PREVCMD_PRESSURE_HIGH,
        CMPRSR_I_PREVCMD_MOTOR_LOW_SPEED,
        CMPRSR_I_PREVCMD_MOTOR_HIGH_SPEED,
        CMPRSR_I_PREVCMD_PRESSURIZATION_TIMEOUT,
        CMPRSR_I_PREVCMD_TEST_SPEED,

        CMPRSR_I_EXTENDED_BATTERY_PRESENT,
        CMPRSR_I_EXTENDED_BATTERY_STATE,
        CMPRSR_I_EXTENDED_BATTERY_COMMS_STATUS,
        CMPRSR_I_PRIMARY_BATTERY_PRESENT,
        CMPRSR_I_PRIMARY_BATTERY_STATE,
        CMPRSR_I_PRIMARY_BATTERY_COMMS_STATUS,
        CMPRSR_I_MOTOR_CONTROLLER_STATUS,
        CMPRSR_I_CHARGER_MODE,
        CMPRSR_I_CHARGER_CHANNEL_SELECT,
        CMPRSR_I_PRESSURIZATION_TIMEOUT_EXPIRED,
        CMPRSR_I_CHARGER_FAULT_EXCEEDED_CHARGE_TIME,
        CMPRSR_I_CHARGER_FAULT_OVER_TEMPERATURE,
        CMPRSR_I_PRIMARY_BATTERY_CHARGE_INHIBIT_HIGH_TEMP,
        CMPRSR_I_EXTENDED_BATTERY_CHARGE_INHIBIT_HIGH_TEMP,
        CMPRSR_I_SWON,
        CMPRSR_I_VENT_INOP,
        CMPRSR_I_SVO,
        CMPRSR_I_MOTOR_CONTROLLER_FAULT,

        CMPRSR_I_EXTENDED_BATTERY_TEMP_BUFFER,
        CMPRSR_I_PRIMARY_BATTERY_TEMP_BUFFER,
        CMPRSR_I_VCC_STANDBY,
        CMPRSR_I_POWER_24_VOLT_BUS,
        CMPRSR_I_PRIMARY_BATTERY_VOLTAGE_BUFFER,
        CMPRSR_I_EXTENDED_BATTERY_VOLTAGE_BUFFER,
        CMPRSR_I_CURRENT_CHARGE_BUFFER,
        CMPRSR_I_ACCUMULATOR_TEMP,
        CMPRSR_I_COMPRESSOR_TEMP,
        CMPRSR_I_ACCUMULATOR_PRESSURE,
        CMPRSR_I_BATTERY_CURRENT,
        CMPRSR_I_POWER_MINUS_5_VOLT_MONITOR,
        CMPRSR_I_POWER_3_3_VOLT_MONITOR,
        CMPRSR_I_POWER_3_3_VOLT_REFERENCE,
        CMPRSR_I_SELECTED_BUF_VOLTAGE,
        CMPRSR_I_VOSENSE,
        CMPRSR_I_V_SUP_SENSE,

        CMPRSR_I_PRIMARY_BATTERY_STATUS,
        CMPRSR_I_PRIMARY_BATTERY_TEMP,
        CMPRSR_I_PRIMARY_BATTERY_SERIAL_NUMBER,
        CMPRSR_I_PRIMARY_BATTERY_RELATIVE_STATE_OF_CHARGE,
        CMPRSR_I_PRIMARY_BATTERY_ABSOLUTE_STATE_OF_CHARGE,
        CMPRSR_I_PRIMARY_BATTERY_REMAINING_CAPACITY,
        CMPRSR_I_PRIMARY_BATTERY_FULL_CHARGE_CAPACITY,
        CMPRSR_I_PRIMARY_BATTERY_RUN_TIME_TO_EMPTY,
        CMPRSR_I_PRIMARY_BATTERY_MANUFACTURE_DATE,
        CMPRSR_I_PRIMARY_BATTERY_CYCLE_COUNT,
		CMPRSR_I_PRIMARY_BATTERY_VOLTAGE,

        CMPRSR_I_EXTENDED_BATTERY_STATUS,
		CMPRSR_I_EXTENDED_BATTERY_TEMP,
		CMPRSR_I_EXTENDED_BATTERY_SERIAL_NUMBER,
		CMPRSR_I_EXTENDED_BATTERY_RELATIVE_STATE_OF_CHARGE,
		CMPRSR_I_EXTENDED_BATTERY_ABSOLUTE_STATE_OF_CHARGE,
		CMPRSR_I_EXTENDED_BATTERY_REMAINING_CAPACITY,
		CMPRSR_I_EXTENDED_BATTERY_FULL_CHARGE_CAPACITY,
		CMPRSR_I_EXTENDED_BATTERY_RUN_TIME_TO_EMPTY,
		CMPRSR_I_EXTENDED_BATTERY_MANUFACTURE_DATE,
		CMPRSR_I_EXTENDED_BATTERY_CYCLE_COUNT,
    	CMPRSR_I_EXTENDED_BATTERY_VOLTAGE,

        CMPRSR_I_MOTOR_START_EVENTS,
        CMPRSR_I_MOTOR_REVOLUTIONS,
        CMPRSR_I_MOTOR_RUNTIME_HOURS,

        CMPRSR_I_FANS_BARE_ASSEMBLY_FAN_STATUS,
        CMPRSR_I_FANS_BARE_ASSEMBLY_FAN_TACH,

        CMPRSR_I_MISC_MOTOR_STATUS,
        CMPRSR_I_MISC_MOTOR_RPM,

        // Mix Module
        MIX_I_PREVCMD_MIX_SET_POINT_ERROR,
        MIX_I_PREVCMD_MIX_PRESSURE_ERROR,
        MIX_I_PREVCMD_EST_TEST,
        MIX_I_PREVCMD_BYPASS_OVER_PRESSURE_SWITCH,
        MIX_I_PREVCMD_OPTION_GAS_SUPPLY_VALVE,
        MIX_I_PREVCMD_OVER_VOLTAGE_MIX_LATCH_CLEAR,
        MIX_I_PREVCMD_MIX_PURGE_VALVE,
        MIX_I_PREVCMD_MIXER_MODE,
        MIX_I_PREVCMD_BUV_CONTROL,
        MIX_I_PREVCMD_MIX_CONTROL,
        MIX_I_PREVCMD_MIX_OPTIONAL_GAS,
        MIX_I_PREVCMD_INT_MIX,

        MIX_I_SET_AIR_PSOL_PWM_HAMMING,
        MIX_I_SET_OXYGEN_PSOL_PWM_HAMMING,
        MIX_I_SET_HELIOX_PSOL_PWM_HAMMING,
        MIX_I_MIX_SET_POINT_HAMMING,
        MIX_I_MIX_PRESSURE_HAMMING,
        MIX_I_DESIRED_INSPIRATORY_FLOW_HAMMING,
        MIX_I_AIR_LIFTOFF_SETTING_HAMMING,
        MIX_I_OXYGEN_LIFTOFF_SETTING_HAMMING,
        MIX_I_HELIOX_LIFTOFF_SETTING_HAMMING,
        MIX_I_DAC_WRAP_HAMMING,
        MIX_I_MESSAGE_CHECKSUM_ERROR,

        MIX_I_MIX_SET_POINT,
        MIX_I_MIX_PRESSURE,
        MIX_I_DESIRED_INSPIRATORY_FLOW,
        MIX_I_AIR_LIFTOFF_SETTING,
        MIX_I_OXYGEN_LIFTOFF_SETTING,
        MIX_I_HELIOX_LIFTOFF_SETTING,
        MIX_I_DAC_WRAP,
        MIX_I_SET_AIR_PSOL_PWM,
        MIX_I_SET_OXYGEN_PSOL_PWM,
        MIX_I_SET_HELIOX_PSOL_PWM,

        MIX_I_OVER_VOLTAGE_DETECT_MIX_N,
        MIX_I_ACCUMULATOR_PRESSURE_FAULT,
        MIX_I_VOLTAGE_FAULT,
        MIX_I_IE_STATE,
        MIX_I_BUV_SET_AIR_DISCRETE_INPUT,
        MIX_I_BUV_MIX_DISABLE_DISCRETE_INPUT,
        MIX_I_VENT_INOP_DISCRETE_INPUT,
        MIX_I_SAFETY_VALVE_STATE,
        MIX_I_DWELL_MIX_TRANSITION,
        MIX_I_OA_MODULE_STATUS,
        MIX_I_AIR_FLOW_MODULE_STATUS,
        MIX_I_O2_FLOW_MODULE_STATUS,
        MIX_I_OPTIONAL_GAS_FLOW_MODULE_STATUS,
        MIX_I_GAS_TYPE,

        MIX_I_DAC_WRAP_ADC1,
        MIX_I_VREF_MONITOR_MIX,
        MIX_I_AIR_INLET_PRESSURE,
        MIX_I_AIR_FLOW,
        MIX_I_AIR_TEMP,
        MIX_I_O2_INLET_PRESSURE,
        MIX_I_O2_FLOW,
        MIX_I_O2_TEMP,
        MIX_I_DAC_WRAP_ADC2,
        MIX_I_OPT_FLOW_HELIOX_BUF,
        MIX_I_HELIOX_INLET_PRESSURE,
        MIX_I_HELIOX_FLOW,
        MIX_I_HELIOX_TEMP,
        MIX_I_SOL_ACC_DUMP,
        MIX_I_TEST_ADC,
        MIX_I_TEMP_DC_DC_PCBA,
        MIX_I_PLUS_12V_DISPLAYED_SCALED,
        MIX_I_DAC_WRAP_ADC3,
        MIX_I_MINUS_12V_SCALED,
        MIX_I_MINUS_5V_SCALED,
        MIX_I_PLUS_5V_SCALED,
        MIX_I_PLUS_12V_ANA_SCALED,
        MIX_I_PLUS_12V_SCALED,
        MIX_I_PLUS_24V_SCALED,
        MIX_I_ACC_PRESSURE,
        MIX_I_DAC_WRAP_ADC4,
        MIX_I_AIR_PSOL_CURRENT,
        MIX_I_O2_PSOL_CURRENT,
        MIX_I_HELIOX_PSOL_CURRENT,
        MIX_I_DAC_WRAP_ADC5,
        MIX_I_QINSP_AIR_O2_FLW_BUF,
        MIX_I_INSP_TEMP_BUF,

        MIX_I_ACC_PRESSURE_FAULT,
        MIX_I_BUV_ENABLED,

		MIX_I_IR_FLOW_MODULE_CFG_STATUS,
        MIX_I_O2_FLOW_MODULE_CFG_STATUS,
        MIX_I_HELIOX_FLOW_MODULE_CFG_STATUS,

        MIX_I_CALC_AIR_PWM_CMD,
        MIX_I_CALC_O2_PWM_CMD,
        MIX_I_CALC_HEO2_PWM_CMD,

        // Options
        OPTIONS_I_PROX_PRESENT,
        OPTIONS_I_PROX_CAP_BUSY,
        OPTIONS_I_CAP_PRESENT,
        OPTIONS_I_SUPPLY_VALVE_SHUTOFF_REQUESTED,
        OPTIONS_I_PROX_OVER_PRESSURE_PRESENT,
        OPTIONS_I_PROX_PURGE_CHARGE_TIMEOUT,

    	MODULE_DATA_IDS_GETTERS,

    	// Insp
        INSP_O_PSOL_PORT,
        INSP_O_SV_PRESS_SETTING,
        INSP_O_SAFETY_VALVE_SOLENOID,
        INSP_O_AUTO_ZERO_SOLENOID,
        INSP_O_EXH_BUV_PWM_CMD,
        INSP_O_DAC1_WRAP,

        // Exh
        EXH_O_VALVE_PORT,
        EXH_O_VALVE_NEGATIVE_PORT,
        EXH_O_VALVE_DAMPING_GAIN_PORT,
        EXH_O_VALVE_DAC2_WRAP,
        EXH_O_AUTO_ZERO_SOLENOID,
        EXH_O_HEATER,
        EXH_O_SV_BUV_SOLENOID,

        // Mix
        MIX_O_SET_POINT,
        MIX_O_PRESSURE,
        MIX_O_AIR_PSOL_LIFTOFF,
        MIX_O_O2_PSOL_LIFTOFF,
        MIX_O_HELIOX_PSOL_LIFTOFF,
        MIX_O_AIR_PSOL_PWM,
        MIX_O_O2_PSOL_PWM,
        MIX_O_HELIOX_PSOL_PWM,

        MIX_O_PURGE_VALVE,
        MIX_O_OPTIONS_VALVE,
        MIX_O_MODE,
        MIX_O_CONTROL,
        MIX_O_OPTIONAL_GAS,
        MIX_O_DAC_WRAP,
        MIX_O_BUV_CONTROL,
        MIX_O_BYPASS_OVER_PRESSURE_SWITCH,

        // Power Control Module (PCM)
        PCM_O_POWER_OFF_COMMAND,
        PCM_O_FAN_1,
        PCM_O_POWER_MODE,
        PCM_O_SECONDARY_ALARM,
        PCM_O_POWER_FAIL_CAP_CHARGE,
        PCM_O_BATTERY_CHANNEL_SELECT,

        // Compressor Module (CMPRSR)
        CMPRSR_O_CLEAR_PRESSURIZATION_TIMEOUT_ERROR,
        CMPRSR_O_CLEAR_MOTOR_CONTROLLER_FAULT,
        CMPRSR_O_COMPRESSOR_MODE,
        CMPRSR_O_POWER_MODE,

        CMPRSR_O_COMPRESSOR_CONTROL,

        CMPRSR_O_TEST_UNLOAD_SOLENOID,
        CMPRSR_O_TEST_BARE_ASSEMBLY_FAN,
        CMPRSR_O_TEST_PDC_INT,
        CMPRSR_O_TEST_POWER_FAIL_COMPRESSOR,
        CMPRSR_O_TEST_COMPRESSOR_MOTOR,
        CMPRSR_O_TEST_BATTERY_LAMPS,
        CMPRSR_O_TEST_BATTERY_CHANNEL_SELECT,
        CMPRSR_O_TEST_POWER_INTERFACE,

        CMPRSR_O_PRESSURE_LOW,
        CMPRSR_O_PRESSURE_HIGH,
        CMPRSR_O_MOTOR_LOW_SPEED,
        CMPRSR_O_MOTOR_HIGH_SPEED,
        CMPRSR_O_PRESSURIZATION_TIMEOUT,

        CMPRSR_O_TEST_SPEED,


        // Options
        OPTIONS_O_PROX_POWER_COMMAND,
        OPTIONS_O_VSRC_POWER_COMMAND,

    	// KEEP THIS LAST
    	MODULE_DATA_IDS_UNSIGNED_MAX
	};