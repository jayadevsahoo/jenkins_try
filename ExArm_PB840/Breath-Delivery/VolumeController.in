#ifndef VolumeController_IN
#define VolumeController_IN
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: VolumeController - Implements an inspiratory flow controller
//		for volume based breaths.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VolumeController.inv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  26-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetControllerType()
//
//@ Interface-Description
//		This method has the controller type as an argument and has no
//		return value.  This method is called to update the controller type
//		to the value passed in.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		The data member, ControllerType_, is set to the	value passed in.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

inline void
VolumeController::SetControllerType( const ControllerTypes controllerType)
{
	CALL_TRACE("VolumeController::SetControllerType( const ControllerTypes controllerType)") ;
	
	// $[TI1]
   	ControllerType_ = controllerType ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetControllerType()
//
//@ Interface-Description
//		This method has no arguments and returns the controller type.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		The value of the data member, ControllerType_, is returned.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

inline VolumeController::ControllerTypes
VolumeController::GetControllerType( void)
{
	CALL_TRACE("VolumeController::GetControllerType( void)") ;

	// $[TI1]
   	return( ControllerType_) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetBreathLikenessStatus()
//
//@ Interface-Description
//      This method has a Boolean as an argument and has no	return value.
//		This method is called to update the breath likeness status flag to
//		the value passed in.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		The data member, BreathLikenessStatus_, is set to the value passed in.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

inline void
VolumeController::SetBreathLikenessStatus( const Boolean likenessStatus)
{
	CALL_TRACE("VolumeController::SetBreathLikenessStatus( const Boolean likenessStatus)") ;
	
	// $[TI1]
   	BreathLikenessStatus_ = likenessStatus ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetBreathLikenessStatus()
//
//@ Interface-Description
//		This method has no arguments and returns the breath likeness status.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		Returns the data member BreathLikenessStatus_.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

inline Boolean
VolumeController::GetBreathLikenessStatus( void)
{
	CALL_TRACE("VolumeController::GetBreathLikenessStatus( void)") ;
	
	// $[TI1]
   	return( BreathLikenessStatus_) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPeakDesiredFlow()
//
//@ Interface-Description
//		This method has desired peak flow as an argument and has no return
//		value.  This method is called to update the peak desired flow
//		value.  This method must be called at the beginning of each
//		inspiration by the VcvPhase::newBreath() before
//		VolumeController::newBreath() is called.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set peakDesiredFlow_ to the value passed in.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
inline void
VolumeController::setPeakDesiredFlow( const Real32 peakDesiredFlow)
{
	CALL_TRACE("VolumeController::setPeakDesiredFlow( const Real32 peakDesiredFlow)") ;

	// $[TI1]
	peakDesiredFlow_ = peakDesiredFlow ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

#endif // VolumeController_IN 





