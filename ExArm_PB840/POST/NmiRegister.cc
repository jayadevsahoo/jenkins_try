#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission f
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: NmiRegister  - NMI Source Register Power On Self Test
//---------------------------------------------------------------------
//@ Interface-Description
//    This class contains the power on self test of the NMI 
//    source register.  The test verifies that all bits in the source
//    register that are not supposed to be set during POST are not set.
//---------------------------------------------------------------------
//@ Rationale
//    This test satisfy the NMI test functions of POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description.
//---------------------------------------------------------------------
//@ Fault-Handling
//    The test returns a MAJOR error upon failing the test.
//---------------------------------------------------------------------
//@ Restrictions
//    None
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/NmiRegister.ccv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "NmiRegister.hh"   
#include "CpuDevice.hh"


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: Test
//
//@ Interface-Description
//  Verifies the NMI source register is in its "reset" state with all
//  NMI source bits not set masking out the board revision bits which
//  are also located in the NMI source register.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

#define  NMI_MASK          (0x3f)  // mask off non-NMI source bits

Criticality 
NmiRegister::Test(void)
{
#ifdef E600_840_TEMP_REMOVED
   Uint8 value = *NMI_REGISTER & NMI_MASK;
#endif
   Uint8 value = 0;

UNIT_TEST_POINT(POSTUT_22)

   // $[TI1.1]  $[TI1.2]
   return value ? MAJOR : PASSED;  // if any bits are set => failure
}
