#ifndef OperatingCondition_HH
#define OperatingCondition_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: OperatingCondition - Alarm-Analysis class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/OperatingCondition.hhv   25.0.4.0   19 Nov 2013 13:51:22   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   gbs      Date: 05/16/96         DR Number:   981
//      Project:   Sigma   (R8027)
//      Description:
//         Fixed syntax warnings generated by new MRI compiler.  These changes
//         do not affect Unit Test.
//  
//   Revision 003   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

//@ Usage-Classes
#include "AlarmOperand.hh"

//@ End-Usage

class OperatingCondition: public AlarmOperand
{
  public:
    typedef Boolean (*EvaluationFunctionPtrType)(void);

    OperatingCondition(
        OperatingCondition::EvaluationFunctionPtrType pEvalFunc, 
        OperandName n);
 
    OperatingCondition(const OperatingCondition& rOpCon);
    virtual ~OperatingCondition();

    virtual void setUpdate(void);
    virtual void setReset (void);

 
    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

#ifdef SIGMA_UNIT_TEST
    OperatingCondition::EvaluationFunctionPtrType
        getEvaluationFunction(void) const;
#endif // SIGMA_UNIT_TEST

  protected:
    virtual Boolean evaluateReset_         (void);
    virtual Boolean evaluateUpdate_        (void);
    virtual void    setCauseUpdate_        (void);
    virtual void    setAugmentUpdate_      (void);
    virtual void    updateViolationHistory_(void);
    virtual void    resetViolationHistory_ (void);

  private:
    OperatingCondition(); // Declared only
    void operator=(const OperatingCondition&); // Declared only

    void initTopLevel_     (void);
    void initMaxViolations_(void);

    //@ Data-member
    //  Pointer to the method which evaluates the actual operating parameter
    //  modeled by this object.
    OperatingCondition::EvaluationFunctionPtrType pEvaluationFunction_;


};

// Inlined methods
#include "OperatingCondition.in"

#endif // OperatingCondition_HH 
