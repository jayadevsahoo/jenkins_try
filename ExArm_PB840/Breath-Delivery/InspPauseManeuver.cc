#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: InspPauseManeuver - implements the handling of inspiratory pause
//  meneuver.
//---------------------------------------------------------------------
//@ Interface-Description
// Handles the inspiratory pause events that are originated by the UI.  At
// construction time, a maneuver object with the appropriated maneuver ID,
// and a stability data buffer are created.  Access methods are implemented
// to get the maneuver's state, resistance state and value and the compliance
// state and value.  A method to set the clear request to cancel the
// maneuver is implemented as well.  When a inspiratory pause is requested,
// the method handleInspPause shall determine the maneuver state, setup
// the maximum time allowed for pause to be initiated and starts the state
// machine.  The activate method setups the timer requirement for the
// maximum auto mode pause duration.  Access methods which meets the
// requirement that allows the inspiratory phase to become active when
// we have collected a complete set of inspiratory flow data is also implemented.
// The virtual function timeUpHappened also handles various timeout requirement
// to either cancel the manual or setup the maximum manual activated pause time.
//---------------------------------------------------------------------
//@ Rationale
// This class defines how inspiratory pause works.
//---------------------------------------------------------------------
//@ Implementation-Description
// The method handleInspPause() is invoked when a inspiratory pause is started/
// stopped.  Once the inspiratory pause key is released, only when the inspiratory
// pause maneuver is active and we are in manual mode, we shall enable the
// PauseCompletionTrigger to tell the scheduler to complete the pause.  When
// starting a pause, either an IDLE maneuver shall change to PENDING
// state and ready for pause to start or an IDLE_PENDING maneuver shall
// change to ACTIVE_PENDING state and ready for pause to start.
// When a pending pause becomes activated, the maneuver state is set
// accordingly, the pause time is set to allow 2 seconds of auto pause
// time.  The method timeUpHappened() is invoked when the timer is time up.
// If the MAX_PENDING_TIME expired, then we simply cancel this maneuver.
// If the MAX_PLATEAU_TIME expired, then if the user is still pressing
// the inspiratory pause key then the pause transitions to the manual mode,
// otherwise, we simply set the timeOut_ to TRUE.  The method activate() is
// invoked either by the scheduler or VCV phase when the maneuver is in
// pending mode.  If there is enough time during the pause pending mode to
// collect full buffer of inspiratory flow data then we can transitions to
// active mode.  A message is also sent to GUI of the activation of the
// pause.  Else, we have to reset all the pressure and flow data buffer and
// ignore the activate request.  The method newCycle() only functions
// during either pending or active maneuver modes.  When in pending mode,
// we need to decide if the pause request should be granted based on
// the flag pEdState_.  When VCV phase is active, plateau is not active,
// and square flow pattern is selected, then we started to collect the
// inspiration data for both flow and pressure.  Once we have collected
// the full set of data, then we allows the inspiratory phase to become
// active and we know that a valid resistance value is possible.  When
// VCV phase is active, plateau is not active, and ramp flow pattern is
// selected, or PCV phase is active, or we are in BiLevel scheduler, then
// we allows the inspiratory phase to become active without collecting
// any inspiratory data and we know that the resistance value is not required.
// If the breath is transitioned to exhalation and we still have not collected
// enough inspiratory data, then we simply reset all inspiratory data for
// pressure and flow and wait for next inspiration phase to reinitiate the
// pause activation.  While the pause is in active mode, if the user has
// requested to cancel the pause, then the maneuver shall be terminated
// accordingly, else it shall collect the airway pressure and calculate
// the pressure stability.  Once the pressure reaches stability, the pause
// completion trigger shall be enabled and the maneuver shall transition
// to idle pending state.  If in any cases, the timeout happened, then the
// pause completion trigger shall be enabled, the maneuver shall transitions
// to idle pending state.  The method clear() shall send a cancel command
// when the maneuver state is in pending or active state.  A queue message
// shall be send to GUI when the maneuver state is idle pending.  The method
// complete() shall set the maneuver to idle mode, stop the timer the maneuver
// is in idle pending mode, it shall set the maneuver to pending mode when
// the maneuver state is in active state.  The method
// calculateComplianceAndResistance_() shall calculate the compliance and
// resistance value and set up the appropriated compliance and resistance
// state.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/InspPauseManeuver.ccv   25.0.4.0   19 Nov 2013 13:59:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc   Date:  29-Nov-2006   SCR Number: 6319
//  Project:  RESPM
//  Description:
//		Removed private data member clearRequested_ to use protected
//      data member with same name in Maneuver base class.
//
//  Revision: 007   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Refactored code for base class modifications to accomodate RM
//      maneuvers. Moved pressure stability buffer to its own class
//      that is now a member of this class. New method processUserRequest()
//      implements functionality of old Maneuver::DetermineManeuverStatus()
//      method in each maneuver class.
//
//  Revision: 006  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Handle VtpcvPhase same as PcvPhase; that is the resistanceState_ is
//			NOT_REQUIRED.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      ATC initial release.
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  yyy    Date:  05-Nov-98    DR Number: 5252
//       Project:  Sigma (R8027)
//       Description:
//             Corrected mapping SRS to code.
//
//  Revision: 003  By:  syw    Date:  20-Oct-1998    DR Number: 5217, 5219, 5220, 5222, 5223,
//       Project:  BiLevel
//       Description:
//			Eliminate FORNOW statements.  Added SIGMA_INTEG_TEST code.  Only update
//			complianceState_ and resistanceState_ if the new state is higher rank.
//			Requirement tracing.
//
//  Revision: 002  By:  syw    Date:  14-Oct-1998    DR Number: 5185
//       Project:  BiLevel
//       Description:
//      	Added handle in timeUpHappened() for the case when keyReleased_
//			is TRUE.  Terminate pause with RPauseCompletionTrigger and
//			cleanup.
//
//  Revision: 001  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel baseline.
//
//=====================================================================

#include "InspPauseManeuver.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes
#include "IntervalTimer.hh"
#include "Trigger.hh"
#include "TriggersRefs.hh"
#include "TimerBreathTrigger.hh"
#include "MsgQueue.hh"
#include "BdQueuesMsg.hh"
#include "PhaseRefs.hh"
#include "InspiratoryPausePhase.hh"
#include "StaticMechanicsBuffer.hh"
#include "SummationBuffer.hh"
#include "BreathRecord.hh"
#include "PhasedInContextHandle.hh"
#include "SettingId.hh"
#include "FlowPatternValue.hh"
#include "MainSensorRefs.hh"
#include "FlowSensor.hh"
#include "PressureSensor.hh"
#include "Btps.hh"
#include "CircuitCompliance.hh"
#include "BreathSet.hh"
#include "VcvPhase.hh"
#include "BreathPhaseScheduler.hh"
#include "PendingContextHandle.hh"
#include "MathUtilities.hh"
#include "UiEvent.hh"
#include "ManeuverTimes.hh"

#if defined GUIA_UT
#include "PrintQueue.hh"
#include "Ostream.hh"
#endif	// GUIA_UT

#ifdef SIGMA_INTEG_TEST
Boolean TestA = FALSE ;
Boolean TestB = FALSE ;
Boolean TestC = FALSE ;
Boolean TestD = FALSE ;
Boolean TestE = FALSE ;
Boolean TestF = FALSE ;
Boolean TestG = FALSE ;
Boolean TestH = FALSE ;
Boolean TestI = FALSE ;
Boolean TestJ = FALSE ;
Boolean TestM = FALSE ;
Boolean TestN = FALSE ;
#endif // SIGMA_INTEG_TEST

//@ End-Usage

//@ Code...
//=====================================================================
//
//  //@ Data-Member: Methods..
//
//
//=====================================================================

//@ Constant MAX_TIME_ALLOWED_TO_INITIATE_PAUSE_PHASE
static const Int32 MAX_TIME_ALLOWED_TO_INITIATE_PAUSE_PHASE = 72000;

//@ Constant TIME_INTERVAL_TO_CHECK_STABLE_PL
// $[04237] The maximum mandatory interval allowed
static const Int32 TIME_INTERVAL_TO_CHECK_STABLE_PL = 2000;

//@ Constant MAX_INSP_PAUSE_INTERVAL
#ifdef E600_840_TEMP_REMOVED
extern Int32 MAX_INSP_PAUSE_INTERVAL;
#endif

//@ Constant PLATEAU_TIME_OFFSET
//
static const Int32 PLATEAU_TIME_OFFSET = 100;

//@ Constant
static const Real32 MAX_UNAVALIABLE_RANGE = 0.1f;
static const Real32 CSTAT_MIN_RANGE = 1.0f;
static const Real32 CSTAT_MAX_RANGE = 100.0f;
static const Real32 CSTAT_MAX_DISPLAY_RANGE = 500.0f;
static const Real32 CSTAT_MAX_FLASHING_RANGE = 500.01f;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InspPauseManeuver
//
//@ Interface-Description
// The method takes a maneuverId and a reference to a IntervalTimer as
// arguments. It calls the base class constructors and initializes some
// data members.
//---------------------------------------------------------------------
//@ Implementation-Description
// It invokes the base class constructor with the inspiratory pause
// maneuver id argument.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

InspPauseManeuver::InspPauseManeuver(ManeuverId maneuverId, IntervalTimer& rPauseTimer) :
	Maneuver(maneuverId),
    TimerTarget(),
    rPauseTimer_(rPauseTimer),
    pauseTime_(0),
    pauseTimeId_(NULL_TIME),
    timeOut_(FALSE),
    keyReleased_(TRUE),
    audibleAnnunciatingNeeded_(TRUE),
	maneuverType_(PauseTypes::AUTO),
	pEdState_(PauseTypes::PED_IDLE),
	pPlState_(PauseTypes::UNSTABLE),
	pEd_(0.0f),
	qPat_(0.0f),
	pPl_(0.0f),
	cStat_(0.0f),
	rAw_(0.0f),
	pEdLR_(StaticMechanicsBuffer::GetTimeBuffer(),
		  StaticMechanicsBuffer::GetTimePressureBuffer(),
		  StaticMechanicsBuffer::GetPressureBuffer(),
		  StaticMechanicsBuffer::GetTime2Buffer(),
		  StaticMechanicsBuffer::GetLinearRegressionBufferSize()),
	inspFlowBuffer_(StaticMechanicsBuffer::GetInspFlowBuffer(),
					StaticMechanicsBuffer::GetInspFlowBufferSize()),
	pressureStabilityBuffer_(StaticMechanicsBuffer::GetPlateauPressureBuffer(),
							 StaticMechanicsBuffer::GetPlateauPressBufferSize())
{
// $[TI1]
	CALL_TRACE("InspPauseManeuver::InspPauseManeuver(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~InspPauseManeuver()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

InspPauseManeuver::~InspPauseManeuver(void)
{
  CALL_TRACE("~InspPauseManeuver()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
// This method takes a reference to an IntervalTimer as an argument and has no
// return value. The inspiratory pause maneuver is a client of the timer server
// that is responsible to notify (by invoking this function) the end of the
// current inspiratory pause interval.
//---------------------------------------------------------------------
//@ Implementation-Description
// The IntervalTimer reference passed in, rTimer, is that of the cycle timer
// server for the inspiratory pause maneuver.  The private data member pauseTimeId_
// is used to identify which timer period the maneuver is using.  If the
// MAX_PENDING_TIME (72 seconds passes without initiating a pause) has elapsed,
// then we shall cancel this request.  If the MAX_AUTO_TIME (2 seconds passed
// without reaching pressure stability) then if the user has not released the
// inspiratory pause key we shall transition to manual mode.  The MAX_MANUAL_TIME
// id and the corresponding time shall be set for the pause timer.  If the
// MAX_MANUAL_TIME (7 seconds passed for the activated maneuver) then we
// just set the timeOut_ flag to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
// &rTimer == &rPauseTimer_
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
InspPauseManeuver::timeUpHappened(const IntervalTimer& rTimer)
{
    CALL_TRACE("BiLevelScheduler::timeUpHappened(const IntervalTimer& rTimer)");

    // make sure the caller is the proper server
    CLASS_PRE_CONDITION(&rTimer == &rPauseTimer_);

	// Stop the timer, and reset the currentTime_,
    rPauseTimer_.stop();
    rPauseTimer_.setCurrentTime(0);

	if (pauseTimeId_ == MAX_PENDING_TIME)
	{
											// $[TI1]
		// $[BL04008] :k
		// The time elapsed has exceeded 72 seconds without initiating
		// the pause phase, so we have to cancel the insp. pause request.
		// CANCEL pause.
		clear();
	}
	else if (pauseTimeId_ == MAX_PLATEAU_TIME)
	{
											// $[TI2]
		timeOut_ = TRUE;

		if (!keyReleased_)
		{									// $[TI2.1]
			// When key is not released, then we shall drop to manual
			// mode.  then we should set the max time that allows the
			// inspiratory pause to proceed (7-2 = 5 seconds).
			maneuverType_ = PauseTypes::MANUAL;

			// adjust the target time
			pauseTime_ = MAX_INSP_PAUSE_INTERVAL - pauseTime_;
			pauseTimeId_ = MAX_MANUAL_TIME;
   			rPauseTimer_.setTargetTime(pauseTime_);

		    // start the timer to check the duration when the pause was
		    // requested
		   	rPauseTimer_.restart();

			// Reset the timeOut_ flag, so the newCycle() will not proceed
			// with this TIME_INTERVAL_TO_CHECK_STABLE_PL timeout.
			timeOut_ = FALSE;

			// The status shall be sent to GUI to communicate to
			// the user through UiEvent.cc so the appropriated prompt
			// can be displayed.
			RInspiratoryPauseEvent.setEventStatus(EventData::ACTIVE, EventData::MANUAL_PROMPT);
		}
		else
		{
			// $[TI2.2]
			((Trigger&)RPauseCompletionTrigger).enable();
			maneuverState_ = PauseTypes::IDLE_PENDING;
			pressureStabilityBuffer_.reset();
		}
	}
	else if (pauseTimeId_ == MAX_MANUAL_TIME)
	{										// $[TI3]
		timeOut_ = TRUE;
	}
	else
	{
        AUX_CLASS_ASSERTION_FAILURE(pauseTimeId_);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: handleInspPause
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// for user event status.
//---------------------------------------------------------------------
//@ Implementation-Description
// The eventStatus argument is set to TRUE by the client, to start the pause.
// The eventStatus argument is set to FALSE by the client, to stop the pause.
//
// When required to start the pause, the following happens:
//		1. idle maneuverState_ transitions to pending.
//			setActiveManeuver()
//			set rPauseTimer_ with MAX_TIME_ALLOWED_TO_INITIATE_PAUSE_PHASE
// 		2. pending, active pending, or active maneuverState_, simply ignore
//		   this request - do nothing
//		3. idle pending maneuverState_ transitions to active pending.
//			setActiveManeuver()
//			set rPauseTimer_ with MAX_TIME_ALLOWED_TO_INITIATE_PAUSE_PHASE
//
// When required to stop the pause, the following happened:
// 		1. pending or active pending  maneuverState_, simply ignore this
//		   request - do nothing
//		2. active maneuverState_ and in manual mode, then
//			RPauseCompletionTrigger) gets enabled so the pause can be
//			cancelled at next BD cycle.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
InspPauseManeuver::handleInspPause(const Boolean eventStatus)
{
    CALL_TRACE("InspPauseManeuver::handleInspPause(const Boolean eventStatus)");

	EventData::EventStatus rtnStatus = EventData::IDLE;
	
	if (eventStatus)
	{									// $[TI1]
		keyReleased_ = FALSE;
		if (maneuverState_ == PauseTypes::PENDING ||
			maneuverState_ == PauseTypes::ACTIVE_PENDING ||
			maneuverState_ == PauseTypes::ACTIVE)
		{								// $[TI1.1]
			// An insp pause was requested before.
			// Simply ignore this request - do nothing
			// an EventData::NO_ACTION_ACK is sending back to GUI.
			rtnStatus = EventData::NO_ACTION_ACK;
		}
		else if (maneuverState_ == PauseTypes::IDLE)
		{								// $[TI1.2]
			// maneuverState_ is IDLE so we allow the insp pause to
			// happen.
		    // adjust the target time
		    pauseTime_ = MAX_TIME_ALLOWED_TO_INITIATE_PAUSE_PHASE;
		    pauseTimeId_ = MAX_PENDING_TIME;
		   	rPauseTimer_.setTargetTime(pauseTime_);

		    // start the timer to check the duration when the pause was
		    // requested
		   	rPauseTimer_.restart();

			setActiveManeuver();
			clearRequested_ = FALSE;

			// User pressed the insp pause key,
			// so set the the maneuverType_ to AUTO (default)
			maneuverType_ = PauseTypes::AUTO;
			maneuverState_ = PauseTypes::PENDING;

			// This return status shall update the InspiratoryPauseEvent's
			// eventStatus_ through UiEvent.cc (UiEvent::setEventStatus)
			// This return status shall be sent to GUI to communicate to
			// the user through UiEvent.cc (UiEvent::setEventStatus) too.
			rtnStatus = EventData::PENDING;

			// Reset the circular buffer for pressure and flow data.
			pEdLR_.reset();
			inspFlowBuffer_.reset();
			pressureStabilityBuffer_.reset();
			pPlState_ = PauseTypes::UNSTABLE;
			qPat_ = 0;
			pEdTimer_ = 0;
			pEdState_ = PauseTypes::PED_IDLE;
			complianceState_ = PauseTypes::VALID;
			resistanceState_ = PauseTypes::VALID;
			squareFlowPattenInVcMode_ = FALSE;
		}
		else if (maneuverState_ == PauseTypes::IDLE_PENDING)
		{								// $[TI1.3]
			// maneuverState_ is IDLE_PENDING so we allow the insp pause to
			// happen.
		    // adjust the target time
		    pauseTime_ = MAX_TIME_ALLOWED_TO_INITIATE_PAUSE_PHASE;
		    pauseTimeId_ = MAX_PENDING_TIME;
		   	rPauseTimer_.setTargetTime(pauseTime_);

		    // start the timer to check the duration when the pause was
		    // requested
		   	rPauseTimer_.restart();

			setActiveManeuver();
			clearRequested_ = FALSE;

			// User pressed the insp pause key,
			// so set the the maneuverType_ to AUTO (default)
			maneuverType_ = PauseTypes::AUTO;
			maneuverState_ = PauseTypes::ACTIVE_PENDING;
			rtnStatus = EventData::ACTIVE_PENDING;
		}
		else
		{
	        AUX_CLASS_ASSERTION_FAILURE(maneuverState_);
		}
	}
	else
	{									// $[TI2]
		keyReleased_ = TRUE;
		if (maneuverState_ == PauseTypes::PENDING ||
			maneuverState_ == PauseTypes::ACTIVE_PENDING)
		{								// $[TI2.1]
			// User released the insp pause key,
			rtnStatus = EventData::NO_ACTION_ACK;
		}
		else if (maneuverState_ == PauseTypes::ACTIVE)
		{								// $[TI2.2]
			if (maneuverType_ == PauseTypes::MANUAL)
			{							// $[TI2.2.1]
				// In manual state, and user release the INSP
				// PAUSE key so, it time to terminate the
				// pause.
				rtnStatus = EventData::ACTIVE;

				// Stop the timer, and reset the currentTime_,
				// $[BL04077] :a complete manual pause request
			    rPauseTimer_.stop();
			    rPauseTimer_.setCurrentTime(0);

				((Trigger&)RPauseCompletionTrigger).enable();

				// User release the insp pause key,
				// so set the the maneuverType_ to AUTO (default)
				maneuverState_ = PauseTypes::IDLE_PENDING;
				pressureStabilityBuffer_.reset();
			}
			else
			{							// $[TI2.2.2]
				// For AUTO mode, we simply set the keyReleased_ flag
				// and let the 2 second time out to take care of the rest.
				rtnStatus = EventData::NO_ACTION_ACK;
    			AUX_CLASS_ASSERTION(maneuverType_ == PauseTypes::AUTO,
    														maneuverType_);
			}
		}								// $[TI2.3]
	}
	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method is invoked by the currently running scheduler when the
//  maneuver is in auto pending mode.  Once the maneuver is activated,
//  we shall setup the maximum plateau time for checking the pressure
//  stability at the end of inspiration.  If there is enough data being
//  accumulated during maneuver pending period, then we can calculate the
//  following:
//  Averaged pressure at end of inspiration - pEd_ - the End Inspiratory
//  pressure projected from linear regression of the last p pressure
//  samples of inspiration.
//  Breathing circuit compliance - cT_ - calculated on a breath by breath
//  basis using the proposed formula as specified in the control specs.
//  Estimated patient flow - qPat_ - the estimated flow into the patient
//  during the last segment of flow delivery.  Also a message shall be
//  sent to GUI to indicate the start of the active pause.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
InspPauseManeuver::activate(void)
{
    CALL_TRACE("InspPauseManeuver::activate(void)");

	if (maneuverState_ == PauseTypes::PENDING)
	{
										// $[TI1]
		timeOut_ = FALSE;
		pPlState_ = PauseTypes::UNSTABLE;
		audibleAnnunciatingNeeded_ = TRUE;

		if (maneuverType_ == PauseTypes::AUTO)
		{
										// $[TI1.1]
		   	BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
		   	Int32 plateauTime;
			if (pBreathPhase == (BreathPhase *) &RVcvPhase)
			{										// $[TI1.1.1]
			   	plateauTime = (Int32)(PhasedInContextHandle::GetBoundedValue(SettingId::PLATEAU_TIME).value +
		   						PLATEAU_TIME_OFFSET);
			}										// $[TI1.1.2]
			else
			{
		   		plateauTime = 0;
			}
			// adjust the pause time to make sure that the vcv plateau ends before
			// the pause timer expires.
			pauseTime_ = MAX_VALUE(plateauTime, TIME_INTERVAL_TO_CHECK_STABLE_PL);
			pauseTimeId_ = MAX_PLATEAU_TIME;
			rPauseTimer_.setTargetTime(pauseTime_);
		}
		else
		{
			AUX_CLASS_ASSERTION_FAILURE(maneuverType_);
		}

		// restart the BiLevel sync timer with appropriate target time.
		rPauseTimer_.restart();

		if (pEdState_ == PauseTypes::PED_COMPLETE)
		{									// $[TI1.2]
			maneuverState_ = PauseTypes::ACTIVE;

            // get the circuit compliance volume when 1 is used for first argument
            // cT_ : ml/cm

			cT_ = RCircuitCompliance.getComplianceVolume(
					PRESSURE_TO_OBTAIN_COMPLIANCE, (Uint32) BreathRecord::GetInspiratoryTime());

			// $[BL04054] pEd, qPat, and rAw calculations are only applicable
			// during volume based ventilation with square flow waveform.
			if (squareFlowPattenInVcMode_)
			{								// $[TI1.2.1]
				// Do calculation for pEd_, qPat_
				// $[BL04055] calculate pEd_
				pEd_ = pEdLR_.getEstimateYValue((Real32)pEdTimer_);

			    DiscreteValue humidType =
					PhasedInContextHandle::GetDiscreteValue(SettingId::HUMID_TYPE);

				// $[BL04056] calculate qPat_
				// $[BL04054] pEd, qPat, and rAw calculations are only applicable
				// during volume based ventilation with square flow waveform.
				Real32 btpsFactor = Btps::GetInspBtpsCf() ;

#ifdef BTREE_UT
    // $[TI1.2.1.2]
	btpsFactor = 0.0 ;
#endif

				// avoid divide by zero.  Set factor to one if factor is near zero.
				if (btpsFactor > 0.001)
				{							// $[TI1.2.1.1]
					btpsFactor = 1.0F / btpsFactor ;
				}
				else
				{							// $[TI1.2.1.2]
					btpsFactor = 1.0 ;
				}

				if (humidType == HumidTypeValue::HME_HUMIDIFIER)
			    {							// $[TI1.2.1.3]
					qPat_ = btpsFactor * (inspFlowBuffer_.getAverage() - 60.f * cT_ * pEdLR_.getSlope()); // lpm
			    }
			    else
			    {							// $[TI1.2.1.4]
					qPat_ = inspFlowBuffer_.getAverage() * btpsFactor - 60.f * cT_ * pEdLR_.getSlope();
				}
			}
			else
			{								// $[TI1.2.2]
				pEd_ = 0;
				qPat_ = 0;
			}

			// Inform Gui about the starting of the Insp pause
		    MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_BEGIN_INSP_PAUSE_DATA);
		}
		else if (pEdState_ == PauseTypes::PED_ACTIVE ||
				 pEdState_ == PauseTypes::PED_IDLE)
		{								// $[TI1.3]
			// We do not have enough time to collect samples when maneuver is
			// activated, so ignore the activate and reset
			// the buffers.
			// Reset the circular buffer for pressure and flow data.
			pEdLR_.reset();
			inspFlowBuffer_.reset();
			pressureStabilityBuffer_.reset();
			pEdTimer_ = 0;
			qPat_ = 0;
			pEdState_ = PauseTypes::PED_IDLE;
			resistanceState_ = updateState_( resistanceState_, PauseTypes::NOT_REQUIRED) ;
		}
		else
		{
	        AUX_CLASS_ASSERTION_FAILURE(pEdState_);
		}
	}
	else
	{									// $[TI2]
		AUX_CLASS_ASSERTION(maneuverState_ == PauseTypes::ACTIVE, maneuverState_);
		// If platue is active, then we shall start the
		// maneuver activate first, then transition to
		// insp pause active which shall call active
		// again so we simply ignore it.
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//    This method takes no arguments and returns nothing.  It is
//    invoked every BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method does all its function when the maneuver is in either in
// PENDING or ACTIVE mode.
// When in pending mode, it does the following:
// Setup the pEdState_ to PED_COMPLETE which shall allow the scheduler
// to start the inspiratory pause phase when
// 1. VCV phase is active, plateau is not active, and square flow pattern
// is selected, then we start to collect the inspiration data for both
// flow and pressure.  Once we have collected the full set of data, then
// we allows the inspiratory phase to become active and we know that a
// valid resistance value is possible.
// The following are the definition for the data being collected.
// 		pEdLR -	data buffer to collect the inspiratory pressure for the last 100
//              msec of inspiration.
// 		inspFlowBuffer_ - data buffer to collect the inspired flow measurement
// 2. VCV phase is active, plateau is not active, and ramp flow pattern is
// selected, or PCV phase is active, or we are in BiLevel scheduler, then
// we allow the inspiratory phase to become active without collecting
// any inspiratory data and we know that the resistance value is not required.
// 3. If the breath transitions to exhalation and we still have not collected
// enough inspiratory data, then we simply reset all inspiratory data for
// pressure and flow and wait for next inspiration phase to reinitiate the
// pause activation.
// While the pause is in active mode, it does the following:
// 1. if the user has requested to cancel the pause, then the maneuver shall
// be terminated accordingly, else it shall collect the airway pressure
// and calculate the pressure stability.
// 2. Once the pressure reached stability, the pause completion trigger
// shall be enabled and the maneuver shall transition to idle pending state.
// pressureStabilityBuffer_	- data buffer to collect the pressure during
//							  the pause.
// 3. If in any cases, the timeout happened, then the pause completion
// trigger shall be enabled, the maneuver shall transitions to idle pending
// state.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
InspPauseManeuver::newCycle(void)
{
	CALL_TRACE("InspPauseManeuver::newCycle(void)");
	// A pointer to the current breath phase
	BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
	// determine the current phase type (e.g. EXHALATION, INSPIRATION)
	const  BreathPhaseType::PhaseType currentPhase = pBreathPhase->getPhaseType();

	// if starting inspiration and the maneuver is
	// idle-pending or active-pending then complete the maneuver
	if ((currentPhase == BreathPhaseType::INSPIRATION &&
		 PreviousBreathPhaseType_ == BreathPhaseType::EXHALATION) &&
		(maneuverState_ == PauseTypes::IDLE_PENDING ||
		 maneuverState_ == PauseTypes::ACTIVE_PENDING))
	{
		// $[TI1.1]
		// $[BL03002] update compliance at the start of next inspiration after pause
		// $[BL03007] update resistance at the start of next inspiration after pause
		// $[BL04060] once inspiratory pause terminates, exhalation begins.
		Maneuver::Complete();
	}
	else
	{
		if (maneuverState_ == PauseTypes::PENDING)
		{				  // $[TI1]
			// Collecting the data only when we are in inspiration phase and
			// the flow pattern is square flow pattern.
			// When VcvPhase in active, it is guaranteed that the we are in inspiration phase
			// $[BL04054] pEd, qPat, and rAw calculations are only applicable
			// during volume based ventilation with square flow waveform.
			if ( (pBreathPhase == (BreathPhase *) &RVcvPhase && !RVcvPhase.IsPlateauActive()) &&
				 PhasedInContextHandle::GetDiscreteValue(SettingId::FLOW_PATTERN) ==
				 FlowPatternValue::SQUARE_FLOW_PATTERN )
			{				// $[TI1.1]
				squareFlowPattenInVcMode_ = TRUE;
				pEdLR_.updateValues((Real32)pEdTimer_, RExhPressureSensor.getValue());
				inspFlowBuffer_.updateBuffer(RO2FlowSensor.getValue() + RAirFlowSensor.getValue());
				pEdTimer_ += CYCLE_TIME_MS;

				// We are in the state of collecting inspiratory data.
				if (inspFlowBuffer_.isBufferFull())
				{			  // $[TI1.1.1]
					// $[BL04069] :a During volume based ventilation with square
					// waveform, the inspiratory pause request must be detected
					// at least 100 msec before the end of inspiration for the
					// pause to be honored.
					pEdState_ = PauseTypes::PED_COMPLETE;
					resistanceState_ = PauseTypes::VALID;
				}
				else
				{			  // $[TI1.1.2]
					pEdState_ = PauseTypes::PED_ACTIVE;
				}
			}
			else if ((pBreathPhase == (BreathPhase *) &RVcvPhase  && !RVcvPhase.IsPlateauActive()) ||
					 pBreathPhase == (BreathPhase *) &RPcvPhase ||
					 pBreathPhase == (BreathPhase *) &RVtpcvPhase ||
					 (BreathPhaseScheduler::GetCurrentScheduler()).getId() ==  SchedulerId::BILEVEL)
			{				// $[TI1.2]
				// When VcvPhase and the flow pattern is ramp pattern, PcvPhase, VtpcvPhase or Bilevel
				// scheduler is active, it is guaranteed that the we are in mandatory
				// inspiration phase and we are not in square flow pattern.
				// $[BL04069] :b For any other type of a mandatory breath, the request
				// will be honored for the current phase type as long as it is
				// detected before the end of the inspiratory phase.
				resistanceState_ = updateState_( resistanceState_, PauseTypes::NOT_REQUIRED) ;
				pEdState_ = PauseTypes::PED_COMPLETE;
			}
			else if (currentPhase == BreathPhaseType::EXHALATION && pEdState_ == PauseTypes::PED_ACTIVE)
			{				// $[TI1.3]
				// We do not have enough samples and we are in exhalation phase
				// so reset the buffers.
				// Reset the circular buffer for pressure and flow data.
				pEdLR_.reset();
				inspFlowBuffer_.reset();
				pressureStabilityBuffer_.reset();
				pEdTimer_ = 0;
				qPat_ = 0;
				pEdState_ = PauseTypes::PED_IDLE;
				resistanceState_ = updateState_( resistanceState_, PauseTypes::NOT_REQUIRED) ;
			}				// $[TI1.4]
			// At this point, an inspiratory pause has been requested by the
			// user (PENDING).  However, the current breath phase is at exhalation,
			// so no pressure data should be collected until the inspiration
			// occurs.
		}
		else if (maneuverState_ == PauseTypes::ACTIVE)
		{				  // $[TI2]
			if (clearRequested_)
			{				// $[TI2.1]
				if (RInspiratoryPauseEvent.getEventStatus() == EventData::ACTIVE)
				{			  // $[TI2.1.1]
					// Trigger an immediated breath to terminate the active
					// pause request.
					((Trigger&)RImmediateBreathTrigger).enable();
					maneuverState_ = PauseTypes::IDLE_PENDING;
					pressureStabilityBuffer_.reset();
				}
				else if (RInspiratoryPauseEvent.getEventStatus() == EventData::PENDING)
				{			  // $[TI2.1.2]
					clear();
				}
				return;
			}				// $[TI2.2]

			pressureStabilityBuffer_.updateBuffer(RExhPressureSensor.getValue());
			PauseTypes::PpiState pPlState = PauseTypes::STABLE;

			// Calculate pressure stability only when it never reaches
			// stability.
			if (pPlState_ != PauseTypes::STABLE)
			{				  // $[TI2.4.1]
				pPlState = pressureStabilityBuffer_.calculatePressureStability();
			}				  // $[TI2.4.2]


			if (pPlState == PauseTypes::STABLE)
			{				// $[TI2.5]
				if (audibleAnnunciatingNeeded_ && rPauseTimer_.getCurrentTime() >= 500)
				{			  // $[TI2.5.1]
					// Pressure reached plateau, declare stability to GUI
					// $[BL04060] pressure is stabilized then pPl_ is
					// recorded for the interval that stability was achieved
					audibleAnnunciatingNeeded_ = FALSE;
					RInspiratoryPauseEvent.setEventStatus(EventData::AUDIO_ACK);
					pPlState_ = PauseTypes::STABLE;

					// Update the pPl_ through the maneuverState_ ACTIVE cycle
					// $[BL04057] calculate pPl_
					pPl_ = pressureStabilityBuffer_.getAverage();
				}			  // $[TI2.5.2]

				if (maneuverType_ == PauseTypes::AUTO)
				{			  // $[TI2.5.3]
					// $[BL04012] :a
					if (rPauseTimer_.getCurrentTime() >= 500 &&
						RInspiratoryPauseEvent.getEventStatus() == EventData::ACTIVE)
					{			// $[TI2.5.3.1]
						if (keyReleased_)
						{		  // $[TI2.5.3.1.1]
							// $[BL04076] :a complete an active inspirtory pause
							// If pressure stability is detected and more than
							// 0.5 seconds have elapsed since the start of the
							// pause, then terminate the pause
							// The scheduler should trigger the following to occure:
							// This call shall update the InspiratoryPauseEvent's
							// eventStatus_ through UiEvent.cc (UiEvent::setEventStatus)
							// The status shall be sent to GUI to communicate to
							// the user through UiEvent.cc (UiEvent::setEventStatus) too.
							// $[TI2.5.3.1]
							((Trigger&)RPauseCompletionTrigger).enable();
							rPauseTimer_.stop();
							rPauseTimer_.setCurrentTime(0);
							maneuverState_ = PauseTypes::IDLE_PENDING;
							pressureStabilityBuffer_.reset();
						}
						else
						{		  // $[TI2.5.3.1.2]
							CLASS_ASSERTION(pauseTimeId_ == MAX_PLATEAU_TIME);

							if (pBreathPhase == (BreathPhase *) &RInspiratoryPausePhase ||
								pBreathPhase == (BreathPhase *) &RBiLevelSpontPausePhase)
							{		// $[TI2.5.3.1.2.1]
								// Remember how much time had passed during stability checking
								pauseTime_ = rPauseTimer_.getCurrentTime();

								// Force a timeout to happen, so we will transition to
								// manual mode.
								rPauseTimer_.setTargetTime(0);
							}		// $[TI2.5.3.1.2.2]
						}
					}			// $[TI2.5.3.2]
				}
				else if (maneuverType_ == PauseTypes::MANUAL &&
						 pauseTimeId_ == MAX_MANUAL_TIME && timeOut_)
				{			  // $[TI2.5.4]
					((Trigger&)RPauseCompletionTrigger).enable();
					maneuverState_ = PauseTypes::IDLE_PENDING;
					pressureStabilityBuffer_.reset();
					CLASS_ASSERTION(RInspiratoryPauseEvent.getEventStatus() == EventData::ACTIVE);
				}			  // $[TI2.5.5]
			}
			else
			{				// $[TI2.6]
				if (timeOut_)
				{			  // $[TI2.6.1]
					// the maneuverType_ == AUTO and
					// 	   timer = TIME_INTERVAL_TO_CHECK_STABLE_PL
					// 	   -> Pressure has not reached plateau yet
					// 	   The maneuver should terminate if the pressure does not
					// 	   stabilize within 2 seconds from the start of the pause.
					// the maneuverType_ == MANUAL and
					// 	   timer == MAX_INSP_PAUSE_INTERVAL
					//	   -> Exceed 7 seconds from the start of the pause phase.
					// 	   The maneuver should terminate if the time exceeded 10
					// 	   seconds from the start of the pause.
					// $[BL04059] :a pressure does not stabilize then terminates
					// maneuver and pPl_ is recorded for the last pause interval.
					// $[BL04060] pressure does not stabilize then pPl_ is
					// recorded for the last pause interval.
					// $[BL04076] :b complete an active inspirtory pause
					// $[BL04075] :k cancel manual pause request
					// $[BL04077] :b complete manual pause request
					((Trigger&)RPauseCompletionTrigger).enable();
					maneuverState_ = PauseTypes::IDLE_PENDING;
					pPl_ = pressureStabilityBuffer_.getAverage();
					pressureStabilityBuffer_.reset();

					// $[BL04064] :a if pressure stabilization was not achieved,
					// let the user knows about it
					complianceState_ = updateState_( complianceState_, PauseTypes::NOT_STABLE) ;

					AUX_CLASS_ASSERTION(RInspiratoryPauseEvent.getEventStatus() == EventData::ACTIVE,
										RInspiratoryPauseEvent.getEventStatus());
				}			  // $[TI2.6.2]
			}
		}
		// $[TI3]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
//    This method takes no arguments and returns nothing.  It is
//    invoked every time the inspiratory pause is terminated.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If the current pause is in PENDING, ACTIVE, IDLE_PENDING, or
//    ACTIVE_PENDING state, then we send the message to GUI telling it
//	  that the pause has been cancelled.  Finally, we initialize the
//	  data members to initial state.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
InspPauseManeuver::clear(void)
{
    CALL_TRACE("InspPauseManeuver::clear(void)");

	if (maneuverState_ == PauseTypes::PENDING ||
		maneuverState_ == PauseTypes::ACTIVE ||
		maneuverState_ == PauseTypes::IDLE_PENDING ||
		maneuverState_ == PauseTypes::ACTIVE_PENDING)
	{	// This call shall update the InspiratoryPauseEvent's
		// eventStatus_ through UiEvent.cc (UiEvent::setEventStatus)
		// The status shall be sent to GUI to communicate to
		// the user through UiEvent.cc (UiEvent::setEventStatus) too.
										// $[TI1]
		RInspiratoryPauseEvent.setEventStatus(EventData::CANCEL);
	}
										// $[TI2]

	maneuverState_ = PauseTypes::IDLE;
	maneuverType_ = PauseTypes::AUTO;

	pauseTimeId_ = NULL_TIME;
	clearRequested_ = FALSE;

	// Stop the timer, and reset the currentTime_,
   	rPauseTimer_.stop();
    rPauseTimer_.setCurrentTime(0);
    ((Trigger&)RPauseCompletionTrigger).disable();

	pressureStabilityBuffer_.reset();

   	resetActiveManeuver();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: complete
//
//@ Interface-Description
//    This method takes no arguments and returns Boolean.  It is
//    invoked every time the inspiratory pause is completed.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If the current pause is in IDLE_PENDING mode, then we initialize
//    maneuverState_, and pauseTimeId_ data members.  If the current
//    pause is in ACTIVE_PENDING mode, then we set the maneuver state
//    to PENDING.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
InspPauseManeuver::complete(void)
{
    CALL_TRACE("InspPauseManeuver::complete(void)");

	// $[BL04060] once inspiratory pause terminates, exhalation
	// begins, conditions for calculation are detected at the
	// end of exhalation
	calculateComplianceAndResistance_();

	if (maneuverState_ == PauseTypes::IDLE_PENDING)
	{
										// $[TI1]
		maneuverState_ = PauseTypes::IDLE;
		pauseTimeId_ = NULL_TIME;

		// Stop the timer, and reset the currentTime_,
   		rPauseTimer_.stop();
	    rPauseTimer_.setCurrentTime(0);
	}
	else if (maneuverState_ == PauseTypes::ACTIVE_PENDING)
	{
										// $[TI2]
		maneuverState_ = PauseTypes::PENDING;
		resistanceState_ = PauseTypes::VALID;
	}
	else
	{
        AUX_CLASS_ASSERTION_FAILURE(maneuverState_);
	}

	// returns TRUE if there is no pending inspiratory pause
	return (maneuverState_ != PauseTypes::PENDING);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: terminateManeuver
//
//@ Interface-Description
//    This method takes no arguments and returns nothing.  Issues
//	  cancel commands to deactivate the active maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Set clearRequested_ TRUE so the maneuver is cancelled
//    and the associated phase deactivated by the next BD cycle.
//    When the active maneuver is in pending state, invoke
//	  clear() method to cancel this maneuver immediately
//    since no pause phase has been involved.
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
InspPauseManeuver::terminateManeuver()
{
	if (maneuverState_ == PauseTypes::ACTIVE)
	{				 // $[TI1.2.1]
		// $[BL04012] :a terminate active pause request
		// $[BL04071] :a terminate active auto expiratory pause
		clearRequested_ = TRUE;
	}
	else if (maneuverState_ == PauseTypes::PENDING ||
			 maneuverState_ == PauseTypes::IDLE_PENDING ||
			 maneuverState_ == PauseTypes::ACTIVE_PENDING)
	{				 // $[TI1.2.2]
		// $[BL04008] :a disable pending pause event
		// $[BL04070] :a disable pending pause event
		clear();
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineManeuverStatus
//
//@ Interface-Description
//    This method takes a UiEvent reference as an argument and returns
//    a Boolean.  This method is invoked whenever the new UiEvent
//    occured and exp pause is the active maneuver.  A TRUE value is
//    returned if the new GUI event should proceed.  A FALSE value is
//    returned when the new GUI event should be rejected.
//---------------------------------------------------------------------
//@ Implementation-Description
//    For an expiratory pause requested while the insp pause is
//    pending then call CancelManeuver() to terminate the insp pause and
//    return FALSE to discontinue processing of the exp pause
//    maneuver. If insp pause is active or idle pending then reject the
//    expiratory pause event.  For an insp pause or manual insp
//    request, return TRUE to allow continued processing of the
//    request.  For a respiratory mechanics maneuver request, reject
//    the request.
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
InspPauseManeuver::processUserEvent(UiEvent &rUserEvent)
{
	Boolean continueNewEvent = TRUE;

	switch (rUserEvent.getId())
	{
	case EventData::EXPIRATORY_PAUSE:
		switch (maneuverState_)
		{
		case PauseTypes::PENDING:
		case PauseTypes::ACTIVE_PENDING:
			// Cancel Insp Pause when Insp pause status is pending
			// Set the Insp pause event status and communicate status to GUI
			// $[BL04008] :h disable pending pause event
			Maneuver::CancelManeuver();
			continueNewEvent = FALSE;
			break;
		case PauseTypes::ACTIVE:
		case PauseTypes::IDLE_PENDING:
			// $[BL04053] reject expiratory pause if inspiratory pause
			// is already taken place.
			// Complete (continueNewEvent) Insp pause when Insp pause status is active
			// reject the Exp pause.
			// Set the event status and communicate status to GUI
			rUserEvent.setEventStatus(EventData::REJECTED);
			continueNewEvent = FALSE;
			break;
		default:
			AUX_CLASS_ASSERTION_FAILURE(maneuverState_);
			break;
		}
		break;

	case EventData::INSPIRATORY_PAUSE:
	case EventData::MANUAL_INSPIRATION:
		continueNewEvent = TRUE;
		// Do nothing
		break;

	case EventData::NIF_MANEUVER:
	case EventData::P100_MANEUVER:
	case EventData::VITAL_CAPACITY_MANEUVER:
		// $[RM12033] All RM maneuver requests shall be rejected if any other pause or RM maneuver has already taken place during the same breath.
		rUserEvent.setEventStatus(EventData::REJECTED);
		continueNewEvent = FALSE;
		break;

	default:
		AUX_CLASS_ASSERTION_FAILURE(rUserEvent.getId());
		break;
	}

	return(continueNewEvent);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
InspPauseManeuver::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, INSPPAUSEMANEUVER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateComplianceAndResistance_
//
//@ Interface-Description
// This method takes no arguments and returns nothing.
// This method shall calculate the compliance and resistance values and
// set up the appropriate compliance and resistance states.
//---------------------------------------------------------------------
//@ Implementation-Description
// The following are the definition for the data being calculated.
// Static respiratory compliance - cStat_ - a simple linear estimate of
// compliance based on the difference in pressure before and after
// exhalation and the exhaled volume.
// Airway resistance - rAw_ - A simple linear estimate of airway resistance
// based on the ratio of pressure change to flow over the last segment
// of the inspiratory interval.  Valid only for mandatory square flow
// waveform.
//
// NOTE: the resistanceState_ is set to NOT_REQUIRED,
// and displays "----" for rAw_ under the following conditions.
// 1. when do not have enough time to collect samples,
// 2. when we do not have enough samples and exhalation phase
//    starts while maneuver is activated
// 3. when in VcvPhase and the flow pattern is ramp pattern
// 4. in PcvPhase
// 5. the Bilevel scheduler is active.
// 6. in VtpcvPhase
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
InspPauseManeuver::calculateComplianceAndResistance_(void)
{
    CALL_TRACE("InspPauseManeuver::calculateComplianceAndResistance_(void)");

	Real32 pEdSlope;

	if (maneuverState_ == PauseTypes::IDLE_PENDING ||
		maneuverState_ == PauseTypes::ACTIVE_PENDING)
	{										// $[TI1]
		// Either plateau pressure not stable or
		// exhalation not complete, we need to calculate
		// the cStat_.
		// $[BL04064] :b if condition for end of exhalation was not
		// detected by Q_exh, let the user knows about it
		if (!BreathRecord::IsExhFlowFinished())
		{								// $[TI1.1.1]
			complianceState_ = updateState_( complianceState_, PauseTypes::EXH_TOO_SHORT) ;
		}								// $[TI1.1.2]

		if (pPlState_ == PauseTypes::UNSTABLE)
		{
			// $[TI3]
			complianceState_ = updateState_( complianceState_, PauseTypes::NOT_STABLE) ;
		}	// implied else $[TI4]

		if (complianceState_ == PauseTypes::NOT_STABLE ||
			complianceState_ == PauseTypes::EXH_TOO_SHORT)
		{								// $[TI1.3]
			// we have compliance problem, but still need to
			// find out resistance status and the compliance and
			// resistance values.
			calculateCstat_();

			// resistance shares the same status with
			// compliance even though the compliance is
			// not valid
			resistanceState_ = updateState_( resistanceState_, complianceState_) ;
		}
		else
		{								// $[TI1.4]
			complianceState_ = updateState_( complianceState_, calculateCstat_()) ;

			if (complianceState_ == PauseTypes::UNAVAILABLE)
			{						// $[TI1.4.1.1]
				// due to (pPl_ - BreathRecord::GetEndExpPressureComp() <= 0.1f
				// resistance shares the same status with
				// compliance
				// $[BL04064] :f condition n
				resistanceState_ = updateState_( resistanceState_, complianceState_) ;
			}						// $[TI1.4.1.2]
		}

#ifdef BTREE_UT
    // unit test TI1.5-TI1.11.3
	calculateComplianceAndResistanceUT_(TRUE, pEdSlope);
#endif

#ifdef SIGMA_INTEG_TEST
	if (TestM)
	{
		cStat_ = 0.05 ;
	}
	else if (TestA)
	{
		cT_ = 6.0 ;
		cStat_ = cT_ / 4.0 ;
	}
#endif // SIGMA_INTEG_TEST

		const Real32 MIN_FLOW =  0.1 ;

		// calculate rAw_
		if (squareFlowPattenInVcMode_)
		{								// $[TI1.5]
			if (cStat_ >= MAX_UNAVALIABLE_RANGE && qPat_ >= MIN_FLOW)
			{							// $[TI1.5.1]
				// $[BL04054] pEd, qPat, and rAw calculations are only applicable
				// during volume based ventilation with square flow waveform.
				// $[BL04063]
				rAw_ = (60 * (1 + cT_ / cStat_) * (pEd_ - pPl_)) / qPat_;

#ifdef SIGMA_INTEG_TEST
				if (TestG)
				{
					rAw_ = 151.0 ;
				}
				else if (TestH)
				{
					rAw_ = 0.4 ;
				}
#endif // SIGMA_INTEG_TEST

			}
			else
			{							// $[TI1.5.2]
				// $[BL04064] :e condition m
				complianceState_ = updateState_( complianceState_, PauseTypes::UNAVAILABLE) ;
				resistanceState_ = updateState_( resistanceState_, PauseTypes::UNAVAILABLE) ;
			}
		}								// $[TI1.6]

		// calculate pEdSlope
		pEdSlope = pEdLR_.getSlope() * 1000.0f;

#ifdef SIGMA_INTEG_TEST
		if (TestI)
		{
			pEdSlope = 201.0 ;
		}
		else if (TestB)
		{
			pEd_ = pPl_ ;
		}
		else if (TestD)
		{
			cStat_ = 3.9 ;
			cT_ = cStat_ ;
		}
		else if (TestC)
		{
			pPl_ = BreathRecord::GetEndExpPressureComp() ;
		}
		else if (TestJ)
		{
			pEdSlope = 0.1 ;
		}
#endif // SIGMA_INTEG_TEST

		// $[BL04064] :c condition a,i
		if (cT_ / cStat_ > 3)
		{								// $[TI1.7]
			resistanceState_ = updateState_( resistanceState_, PauseTypes::SUB_THRESHOLD_INPUT) ;
		}								// $[TI1.8]

		if (pEdSlope > 200 )
		{
			// $[TI5]
			resistanceState_ = updateState_( resistanceState_, PauseTypes::CORRUPTED_MEASUREMENT) ;
		}	// $[TI6]


		// $[BL04064] :c condition b,d
		if (squareFlowPattenInVcMode_)
		{								// $[TI1.9]
			if ( (pEd_ - pPl_) < 0.5 ||
				 (PendingContextHandle::GetBoundedValue(SettingId::PEAK_INSP_FLOW).value < 20 &&
				  cStat_ < 4) )
			{							// $[TI1.9.1]
				resistanceState_ = updateState_( resistanceState_, PauseTypes::SUB_THRESHOLD_INPUT) ;
			}							// $[TI1.9.2]
		}								// $[TI1.10]

		// Continuing parsing compliance status
		// $[BL04064] :c condition c
		if ( (pPl_ - BreathRecord::GetEndExpPressureComp()) < 0.5 )
		{								// $[TI1.11]
			complianceState_ = updateState_( complianceState_, PauseTypes::SUB_THRESHOLD_INPUT) ;

			resistanceState_ = updateState_( resistanceState_, PauseTypes::CORRUPTED_MEASUREMENT) ;
		}								// $[TI1.12]

#ifdef BTREE_UT
    // unit test TI1.13-TI1.18
    calculateComplianceAndResistanceUT_(FALSE, pEdSlope);

#endif

		// Continuing parsing compliance status
		// $[BL04064] :c condition j
		if (squareFlowPattenInVcMode_)
		{								// $[TI1.13]
			if ( pEdSlope < 0.25)
			{							// $[TI1.13.1]
				complianceState_ = updateState_( complianceState_, PauseTypes::CORRUPTED_MEASUREMENT) ;
				resistanceState_ = updateState_( resistanceState_, PauseTypes::CORRUPTED_MEASUREMENT) ;
			}							// $[TI1.13.2]
		}								// $[TI1.14]

		// $[BL04064] :d condition e,f
		if ( cStat_ < CSTAT_MIN_RANGE ||
			 cStat_ > CSTAT_MAX_RANGE )
		{								// $[TI1.15]
			complianceState_ = updateState_( complianceState_, PauseTypes::OUT_OF_RANGE) ;
			resistanceState_ = updateState_( resistanceState_, PauseTypes::CORRUPTED_MEASUREMENT) ;
		}								// $[TI1.16]

		if (squareFlowPattenInVcMode_)
		{								// $[TI1.17]
			// $[BL04064] :d condition g
			if ( rAw_ > 150 )
			{							// $[TI1.17.1]
				complianceState_ = updateState_( complianceState_, PauseTypes::CORRUPTED_MEASUREMENT) ;
				resistanceState_ = updateState_( resistanceState_, PauseTypes::OUT_OF_RANGE) ;
			}							// $[TI1.17.2]

			// $[BL04064] :d condition h
			if (rAw_ < 0.5 )
			{							// $[TI1.17.3]
				resistanceState_ = updateState_( resistanceState_, PauseTypes::OUT_OF_RANGE) ;
			}							// $[TI1.17.4]
		}								// $[TI1.18]

#if defined GUIA_UT
	static Int32 count=0;
	resistanceState_ = (PauseTypes::ComplianceState) count;
	complianceState_ = (PauseTypes::ComplianceState) count;
	count = (count+1) % ((Int32) PauseTypes::HIGHEST_STATIC_STATE);
#endif // GUIA_UT

		// $[BL03002] update compliance at the start of next inspiration after pause
		// $[BL03007] update resistance at the start of next inspiration after pause
		MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_END_INSP_PAUSE_DATA);

		// Reset the circular buffer for pressure and flow data.
		pEdLR_.reset();
		inspFlowBuffer_.reset();
		pressureStabilityBuffer_.reset();
		pEdTimer_ = 0;
		qPat_ = 0;
		pEdState_ = PauseTypes::PED_IDLE;
	}

									// $[TI2]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateCstat_
//
//@ Interface-Description
// This method takes no arguments and returns nothing.
// This method shall calculate the compliance and resistance values and
// set up the appropriate compliance and resistance states.
//---------------------------------------------------------------------
//@ Implementation-Description
// The following are the definition for the data being calculated.
// Static respiratory compliance - cStat_ - a simple linear estimate of
// compliance based on the difference in pressure before and after
// exhalation and the exhaled volume.
// Airway resistance - rAw_ - A simple linear estimate of airway resistance
// based on the ratio of pressure change to flow over the last segment
// of the inspiratory interval.  Valid only for mandatory square flow
// waveform.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
PauseTypes::ComplianceState
InspPauseManeuver::calculateCstat_(void)
{
    CALL_TRACE("InspPauseManeuver::calculateCstat_(void)");

	PauseTypes::ComplianceState complianceState = PauseTypes::VALID;

#ifdef BTREE_UT
	calculateCstatUT_(TRUE);
#endif

	// At this time, the following events had happened.
	// complianceState_ is initialized to VALID when the maneuver
	// is set to PENDING state.
	// When maneuver is ACTIVE state and pPlState_ does not reach
	// stable state when pause ends, set complianceState_ to UNAVAILABLE.

#ifdef SIGMA_INTEG_TEST
	if (TestN)
	{
		pPl_ = BreathRecord::GetEndExpPressureComp() ;
	}
#endif // SIGMA_INTEG_TEST

	if (pPl_ - BreathRecord::GetEndExpPressureComp() < 0.1f)
	{									// $[TI1.1]
		// $[BL04064] :f condition n
		cStat_ = 0;
	}
	else
	{									// $[TI1.2]
		// $[BL04062] Vexp is the BTPS compensated net exhaled volume.
		// EEP is the value of end exhalation pressure
		// Adjust the compliance exhalation volume with the fraction
		// of the mandatory volume.  The MandatoryVolumeFraction is
		// 1 for CONTROL and ASSIST breath type.  For spont breath
		// the value should be less than one, and we need to adjust
		// the ComplianceExhVolume based on the MandatoryVolumeFraction.
		cStat_ = (RBreathSet.getLastBreathRecord()->getComplianceExhVolume() *
				  RBreathSet.getLastBreathRecord()->getMandatoryVolumeFraction()) /
				 (pPl_ - BreathRecord::GetEndExpPressureComp());

#ifdef BTREE_UT
		calculateCstatUT_(FALSE);
#endif
	}

#ifdef SIGMA_INTEG_TEST
	if (TestE)
	{
		cStat_ = 0.5 ;
		cT_ = cStat_ ;
	}
	else if (TestF)
	{
		cStat_ = 600.0 ;
	}
#endif // SIGMA_INTEG_TEST

	// $[BL04062] cStat_ shall be limited between 0 and 500
	// $[BL04064] :d condition e
	if (cStat_ < MAX_UNAVALIABLE_RANGE)
	{								// $[TI2.1]
		cStat_ = 0.01;
		complianceState = updateState_( complianceState, PauseTypes::UNAVAILABLE) ;
	}
	else if (cStat_ < CSTAT_MIN_RANGE)
	{								// $[TI1.3]
		complianceState = updateState_( complianceState, PauseTypes::OUT_OF_RANGE) ;
	}
	else if (cStat_ > CSTAT_MAX_RANGE)
	{								// $[TI1.4]
		if (cStat_ > CSTAT_MAX_DISPLAY_RANGE)
		{							// $[TI1.4.1]
			// to guarantee flashing ...
			cStat_ = CSTAT_MAX_FLASHING_RANGE;
		}							// $[TI1.4.2]

		complianceState = updateState_( complianceState, PauseTypes::OUT_OF_RANGE) ;
	}								// $[TI1.5]

	return(complianceState);
}


#ifdef BTREE_UT
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateComplianceAndResistanceUT_
//
//@ Interface-Description
//    This method accepts a constant boolean flag and pedSlope
//    as the arguments.  If the flag testTopSection is  TRUE, then
//    TI1.5-TI1.11.3 will be tested, FALSE, TI1.13 - TI1.18 will be
//    tested.  For testing TI1.13 - TI1.18, local variable pedSlope
//    will be modified in order to unit test those branches.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// This method shall be used for unit testing calculateComplianceAndResistance_
// method.  This method shall set class members to certain values to force
// software through certain paths.  This method shall sequenciallly test
// TI1.5 - TI1.18.
//
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
InspPauseManeuver::calculateComplianceAndResistanceUT_(const Boolean testTopSection, Real32 &pEdSlope)
{

	static Int32 unitTestCounter = 0;

	// reset count if exceeds total number of test
	if (unitTestCounter > 15)
	{
		unitTestCounter = 0;
	}

	if ((testTopSection && unitTestCounter <= 7) ||
		(!testTopSection && unitTestCounter > 7))
	{
		switch (unitTestCounter)
		{
			case 0:
				// $[TI1.5.2]
				// $[TI1.5.2.1]
				// $[TI1.5.2.3]
				squareFlowPattenInVcMode_ = TRUE;
				cStat_ = 0.001;
				complianceState_ = PauseTypes::VALID;
				resistanceState_ = PauseTypes::VALID;
				break;

			case 1:
				// $[TI1.5.2.2]
				squareFlowPattenInVcMode_ = TRUE;
				cStat_ = 0.001;
				complianceState_ = PauseTypes::UNAVAILABLE;
				resistanceState_ = PauseTypes::VALID;
				break;

			case 2:
				// $[TI1.5.2.4]
				squareFlowPattenInVcMode_ = TRUE;
				cStat_ = 0.001;
				complianceState_ = PauseTypes::VALID;
				resistanceState_ = PauseTypes::UNAVAILABLE;
				break;

			case 3:
				// $[TI1.6]
				// $[TI1.7]
				// $[TI1.7.1]
				// $[TI1.10]
				// $[TI1.18]
				squareFlowPattenInVcMode_ = FALSE;
				cT_ = 10;
				cStat_ = 1;
				resistanceState_ = PauseTypes::VALID;
				break;

			case 4:
				// $[TI1.7.2]
				// $[TI1.9.2]
				squareFlowPattenInVcMode_ = TRUE;
				cT_ = 10;
				cStat_ = 1;
				resistanceState_ = PauseTypes::UNAVAILABLE;
				break;

			case 5:
				// $[TI1.9]
				// $[TI1.9.1]
				// $[TI1.9.1.1]
				squareFlowPattenInVcMode_ = TRUE;
				resistanceState_ = PauseTypes::VALID;
				pEd_ = 0;
				pPl_ = 0;
				break;

			case 6:
				// $[TI1.9.1.2]
				// $[TI1.11]
				// $[TI1.11.1]
				// $[TI1.11.4]
				resistanceState_ = PauseTypes::UNAVAILABLE;
				complianceState_ = PauseTypes::VALID;
				squareFlowPattenInVcMode_ = TRUE;
				pEd_ = 0;
				pPl_ = 0;
				break;

			case 7:
				// $[TI1.11.2]
				// $[TI1.11.3]
				resistanceState_ = PauseTypes::VALID;
				complianceState_ = PauseTypes::UNAVAILABLE;
				pPl_ = 0;
				break;


			case 8:
				// $[TI1.13]
				// $[TI1.13.1]
				// $[TI1.13.4]
				pEdSlope = 0;
				complianceState_ = PauseTypes::VALID;
				resistanceState_ = PauseTypes::UNAVAILABLE;
				break;

			case 9:
				// $[TI1.13.2]
				// $[TI1.13.3]
				pEdSlope = 0;
				complianceState_ = PauseTypes::UNAVAILABLE;
				resistanceState_ = PauseTypes::VALID;
				break;

			case 10:
				// $[TI1.14]
				// $[TI1.15]
				// $[TI1.15.1]
				// $[TI1.15.4]
				pEdSlope = 1;
				cStat_ = 0;
				complianceState_ = PauseTypes::VALID;
				resistanceState_ = PauseTypes::UNAVAILABLE;
				break;

			case 11:
				// $[TI1.14]
				// $[TI1.15.2]
				// $[TI1.15.3]
				cStat_ = 0;
				complianceState_ = PauseTypes::UNAVAILABLE;
				resistanceState_ = PauseTypes::VALID;
				break;

			case 12:
				// $[TI1.17]
				// $[TI1.17.1]
				// $[TI1.17.1.1]
				// $[TI1.17.1.4]
				squareFlowPattenInVcMode_ = TRUE;
				rAw_ = 200;
				complianceState_ = PauseTypes::VALID;
				resistanceState_ = PauseTypes::UNAVAILABLE;
				break;

			case 13:
				// $[TI1.17.1.2]
				// $[TI1.17.1.3]
				squareFlowPattenInVcMode_ = TRUE;
				rAw_ = 200;
				complianceState_ = PauseTypes::UNAVAILABLE;
				resistanceState_ = PauseTypes::VALID;
				break;

			case 14:
				// $[TI1.17.3]
				// $[TI1.17.4]
				// $[TI1.17.3.1]
				squareFlowPattenInVcMode_ = TRUE;
				rAw_ = 0;
				resistanceState_ = PauseTypes::VALID;
				break;

			case 15:
				// $[TI1.17.3]
				// $[TI1.17.3.2]
				// $[TI1.17.2]
				squareFlowPattenInVcMode_ = TRUE;
				rAw_ = 0;
				resistanceState_ = PauseTypes::UNAVAILABLE;
				break;


			default:
				break;
		}

		unitTestCounter++;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateCstatUT_
//
//@ Interface-Description
//    This method takes a boolean flag and returns nothing.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// This method shall be used for unit testing calculateCstat_
// method.  This method shall set class members to certain values to force
// software through certain paths.  This method shall sequenciallly test
// TI1.1 - TI1.5 in calculateCstat_.  If boolean flag testTopSection is
// true, then TI1.1 will be tested.  If boolean flag is false, then TI1.2-
// TI1.5 will be tested.
//
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
InspPauseManeuver::calculateCstatUT_(const Boolean testTopSection)
{

	static Int32 counter = 0;

	// reset unit test counter after all test are completed
	if (counter >= 3)
	{
		counter = 0;
	}

	if ((testTopSection && counter < 1) ||
		(!testTopSection && counter >= 1))
	{
		switch (counter)
		{
			case 0:
				// $[TI1.1]
				pPl_ = 0.0f;
				break;

			case 1:
				// $[TI1.3]
				cStat_ = 0.0f;
				break;

			case 2:
				// $[TI1.4]
				// $[TI1.4.1]
				cStat_ = 600.0f;
				break;

			default:
				counter = 0;
				break;
		}

		counter++;
	}
}

#endif // BTREE_UT
