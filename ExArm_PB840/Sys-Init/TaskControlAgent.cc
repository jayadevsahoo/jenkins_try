#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TaskControlAgent - Interface Agent to the Task-Control Task.
//---------------------------------------------------------------------
//@ Interface-Description
//  The TaskControlAgent class provides the interface to Task-Control
//  task.  It contains static methods (event interfaces) to create
//  and queue TaskControlMessages to the Task-Control task.  The
//  TaskControlAgent also maintains a local cache for the current
//  state of the GUI and BD processors.  Applications access these
//  states through this class instead of directly accessing the
//  Task-Control finite state machine.  
//---------------------------------------------------------------------
//@ Rationale
//  The TaskControlAgent class encapsulates the external interface to 
//  the Task-Control task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The TaskControlAgent class is a collection of static functions.
//  It is never instantiated.  
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskControlAgent.ccv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 004   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 003   By:   gdc      Date: 04-FEB-1998 DR Number: 2736
//      Project:   Sigma   (R8027)
//      Description:
//	   Modified ChangeState() to change state immediately only for
//	   transitions to INOP. Other transitions proceed normally
//	   through the finite state machine. Modified Update() to not
//	   change state when the processor is already in the INOP
//	   state. Changing the cached state to SST in ChangeState()
//	   caused an anomaly when the state was then changed back to
//	   ONLINE by FsmState::startTransition().  The state "glitch"
//	   caused the BD main cycle task to switch off the ADC
//	   sequencer which the BD background task reported as an error
//	   when the state returned to ONLINE.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Modified to send TaskReadyMessages to a different queue
//         so TaskControl cann process them in a different context 
//         than other message so one transition completes before 
//         starting another.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TaskControlAgent.hh"
#include "IpcIds.hh"

//@ Usage-Classes
#include "ChangeStateMessage.hh"
#include "CommTaskAgent.hh"
#include "FsmState.hh"
#include "InitConditions.hh"
#include "MsgQueue.hh"
#include "NetworkApp.hh"
#include "BdReadyMessage.hh"
#include "BdFailedMessage.hh"
#include "GuiReadyMessage.hh"
#include "Post.hh"
#include "SyncClockMessage.hh"
#include "TaskControlMessage.hh"
#include "TaskControlQueueMsg.hh"
#include "TaskReadyMessage.hh"
#include "MemoryHandle.hh"

#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif

//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

SigmaState  TaskControlAgent::BdState_     = STATE_START;
SigmaState  TaskControlAgent::GuiState_    = STATE_START;
SigmaState  TaskControlAgent::TargetState_ = STATE_UNKNOWN;

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AddEvent
//
//@ Interface-Description
//  Convenience function.  
//  Constructs a TaskControlQueueMsg of type TASK_CONTROL_MSG
//  from the TaskControlMessage at pMessage.  Queues the event to the 
//  Task Control Queue (default) or queue number specified.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskControlAgent::AddEvent(TaskControlMessage* pMessage, Int32 queueNumber)
{
    CALL_TRACE("AddEvent(TaskControlMessage*)");

    TaskControlQueueMsg  msg( TaskControlQueueMsg::TASK_CONTROL_MSG, 
                               MemoryHandle::CreateHandle(pMessage));
#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_TASKCONTROLAGENT) >= 1)
        cout << "--- " << pMessage->nameOf() << " ---> TASK_CONTROL (" 
             << queueNumber << ")" << endl;
#endif
	
    Int32 status = MsgQueue::PutMsg(queueNumber,msg.getQueueData());
    AUX_CLASS_ASSERTION( status == Ipc::OK, status );
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReportTaskReady
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskControlAgent::ReportTaskReady(const ChangeStateMessage& rMessage)
{
    CALL_TRACE("ReportTaskReady(ChangeStateMessage&)");

    //  Queue the TaskReadyMessage to Task-Control that will
    //  act on the task-ready using the finite state machine

    TaskReadyMessage* pTaskReady = new TaskReadyMessage(rMessage);
    TaskControlAgent::AddEvent(pTaskReady, TASK_CONTROL_TRANS_Q);
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeState
//
//@ Interface-Description
//  This function provides the event interface for the Breath-Delivery
//  subsystem to direct a state transition to the SST or INOP states.
//  The Safety-Net subsystem also uses this method to transition the
//  processor to the INOP state.  Changes the processor state 
//  immediately when called.  Constructs a ChangeStateMessage with 
//  the specified SigmaState and queues it to the Task-Control task
//  that will then process the transition through the finite state
//  machine.  An invalid state transition will cause an assertion in 
//  the Task-Control task when it processes the ChangeStateMessage.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskControlAgent::ChangeState(SigmaState state)
{
    CALL_TRACE("ChangeState(SigmaState)");

	if ( state == STATE_INOP )
	{
		// $[TI1.1]
    	//  For INOP only, set the processor state to INOP immediately
		//  without going through normal state transition mechanism
#ifdef SIGMA_GUI_CPU
    	GuiState_ = state;
#endif
#ifdef SIGMA_BD_CPU
    	BdState_ = state;
#endif
	}
	// $[TI1.2]
    
    //  Queue the ChangeStateMessage to Task-Control that will
    //  act on the state change using the finite state machine

    ChangeStateMessage* pChangeState = new ChangeStateMessage(state);
    TaskControlAgent::AddEvent(pChangeState);
    // $[TI1]
}

#ifdef SIGMA_BD_CPU
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReportNodeReady
//
//@ Interface-Description
//  Sends a ReadyMessage to Task-Control task on the remote
//  CPU.  The ReadyMessage conveys the "service required" state
//  for the processor combining its POST completion status with the 
//  status of uncorrected major faults. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControlAgent::ReportNodeReady(void)
{
    CALL_TRACE("ReportNodeReady(void)");

    BdReadyMessage  nodeReady( TaskControlAgent::GetBdState(),
                               InitConditions::IsServiceRequired() );
 
    // queue BdReadyMessage to GUI Task-Control
#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_TASKCONTROLAGENT) >= 1)
        cout << "--- " << nodeReady.nameOf() << " ---> GUI" << endl;
#endif
	//This is an internal network xmit between BD and GUI. If the receiving
	//procesor has a diferent endianness than the sender, endianness compatibility
	//should be provided by converting nodeReady endianness to network. and the
	//receiver needs to convert it back to host
    (void)NetworkApp::XmitMsg( BD_NODE_READY_MSG_ID,
                               &nodeReady,
                               sizeof(nodeReady) );
    // $[TI1]
}
#endif  // SIGMA_BD_CPU
 

#ifdef SIGMA_GUI_CPU
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReportNodeReady
//
//@ Interface-Description
//  Sends a GuiReadyMessage to Task-Control task on the remote
//  CPU.  The GuiReadyMessage conveys the "service required" state
//  for the processor combining its POST completion status with the 
//  status of uncorrected major faults. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControlAgent::ReportNodeReady(void)
{
    CALL_TRACE("ReportNodeReady(void)");
 
    //  don't report ready to a TUV declared failed BD - this will
    //  prevent the LOUI light from coming on when GUI transitions
    //  to the INOP state due to a TUV BD failure
    if ( FsmState::GetBdState() != STATE_FAILED )
    {
        // $[TI2]
        GuiReadyMessage nodeReady( TaskControlAgent::GetGuiState(),
                                   InitConditions::IsServiceRequired());
 
        // queue GuiReady message to BD Task Control Task
#ifdef SIGMA_DEVELOPMENT
        if (Sys_Init::GetDebugLevel(Sys_Init::ID_TASKCONTROLAGENT) >= 1)
            cout << "--- " << nodeReady.nameOf() << " ---> BD" << endl;
#endif
        (void)NetworkApp::XmitMsg( GUI_NODE_READY_MSG_ID,
                                   &nodeReady,
                                   sizeof(nodeReady) );
    }
    // $[TI3]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SyncClocks
//
//@ Interface-Description
//  Sends a SyncClockMessage to Task Control on the BD to synchronize
//  the BD clock with the current GUI time.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
TaskControlAgent::SyncClocks(Boolean userInitiated)
{
    CALL_TRACE("SyncClocks(Boolean)");
 
    SyncClockMessage syncClock(userInitiated);
 
    // queue SyncClocks message to BD 
#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_TASKCONTROLAGENT) >= 1)
        cout << "--- " << syncClock.nameOf() << " ---> BD" << endl;
#endif
    (void)NetworkApp::XmitMsg( SYNC_CLOCK_MSG_ID,
                               &syncClock,
                               sizeof(syncClock) );
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FailBd
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
TaskControlAgent::FailBd(void)
{
    CALL_TRACE("FailBd(void)");
    BdState_ = STATE_FAILED;
    Post::SetBdFailed( TRUE );
 
    BdFailedMessage* pMessage = new BdFailedMessage;
    TaskControlAgent::AddEvent(pMessage);
    // $[TI1]
}

#endif // SIGMA_GUI_CPU

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Update
//
//@ Interface-Description
//  Updates the GUI and BD states that are cached in this class for
//  performance purposes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Gets the finite state machine internal states for the GUI and BD.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
TaskControlAgent::Update(void)
{
    CALL_TRACE("Update(void)");
 
	if ( GuiState_ != STATE_INOP )
	{
		// $[TI1.1]
    	GuiState_    = FsmState::GetGuiState();
	}
	// $[TI1.2]

	if ( BdState_ != STATE_INOP )
	{
		// $[TI2.1]
    	BdState_     = FsmState::GetBdState();
	}
	// $[TI2.2]

    TargetState_ = FsmState::GetTargetState().getSigmaState();

    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
TaskControlAgent::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_TASKCONTROLAGENT, 
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================
