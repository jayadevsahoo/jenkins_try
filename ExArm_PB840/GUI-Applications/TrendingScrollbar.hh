#ifndef TrendingScrollbar_HH
#define TrendingScrollbar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendingScrollbar -  Scrollbar for the TrendingLog
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendingScrollbar.hhv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc	   Date:  09-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Initial version.
//====================================================================

#include "Scrollbar.hh"

//@ Usage-Classes
//@ End-Usage

class TrendingScrollbar : public Scrollbar
{
public:
	TrendingScrollbar(Uint16 height, Uint16 numDisplayRows);
	~TrendingScrollbar(void);

	// Scrollbar virtual
	virtual void scroll(Int32 numRowsToScroll);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

	private:
	// these methods are purposely declared, but not implemented...
	TrendingScrollbar(void);					 // not implemented...
	TrendingScrollbar(const TrendingScrollbar&); // not implemented...
	void   operator=(const TrendingScrollbar&);	 // not implemented...

	//@ Data-Member: selectedEntry_
	// The index of the entry that is highlighted in the scrolling window.
	Int32 selectedEntry_;
};

#endif // TrendingScrollbar_HH
