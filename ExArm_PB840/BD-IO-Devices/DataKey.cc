#include "stdafx.h"
/**
// This is a proprietary work to which Puritan-Bennett Corporation
// claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or,
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or, otherwise with the prior written permission of
// Puritan-Bennett Corporation.
//
//        Copyright (c) 1996, Puritan-Bennett Corporation
//=======================================================================*/



// Include files
#include "DataKey.hh"

#include "Background.hh"
#include "NovRamManager.hh"
#include "SoftwareOptions.hh"

//@ Usage - Classes
#include <fstream>

//
#define SYSLOG(x)
#define TIMEWARP (1)


//@ End - Usage
//@ Code...

// See Requirement Specification
// Analog Interface PCBA No. 70550-45

static const SerialNumber InvalidSerialNumber;
static const Uint32 MS_IN_ONE_SECOND = 1000;
static const Uint32 MS_IN_ONE_MINUTE = 60 * MS_IN_ONE_SECOND;

static const Uint32 MINUTES_IN_ONE_HOUR = 60;
static const Uint32 DATAKEY_UPDATE_CYCLE_MINUTES = 10 * MINUTES_IN_ONE_HOUR;

//Define the folder structure for the data key folder
//TODO: BVP - E600 - Set the folder to the right one for target
static const char* DATA_KEY_FOLDER = "c:\\temp\\datakey";
static const char* DATA_KEY_OPTION_KEY_FILENAME = "ecc.txt";
static const char* DATA_KEY_OPERATIONAL_HOURS_FILENAME 	= "oh.txt";
static const char* DATA_KEY_BD_SERIAL_NUMBER_FILENAME   = "bd.txt";
static const char* DATA_KEY_GUI_SERIAL_NUMBER_FILENAME  = "gui.txt";



//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method:  DataKey
//
//@ Interface-Description
//  Constructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//  The constructor initializes its private data members to a known state.
//  Initialize the register to a neutral image. It checks for a data key.
//  If a data key is detected, it determines its size. It then verifies
//  the vent head operational time and reads the vent head and GUI
//  serial numbers and stores it in RAM.
//-----------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=======================================================================

DataKey::DataKey() :
	bdSerialNumber_(),
	dataKeyType_(PRODUCTION),
	guiSerialNumber_(),
	intervalDriftMs_(0),
	isFirstUpdate_(TRUE),
	minutesSincePowerUp_(0),
	minutesToDataKeyUpdate_(0),
	rSoftwareOptions_(SoftwareOptions::GetSoftwareOptions()),
	updateTimer_(),
	ventMinutesAtPowerup_(0)
{

	//TODO: BVP - E600 Read the Encrypted configuration code from flash
	//and initialize the SoftwareOptions. Also verify against this vent's
	//serial number
	rSoftwareOptions_.setSerialNumber( bdSerialNumber_) ;
	rSoftwareOptions_.setDataKeyType( dataKeyType_) ;
	rSoftwareOptions_.validateData() ;

	//TODO: BVP - E600 Override the options set for SALES/DEMO type keys

} 

//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method:  ~DataKey(void)
//
//@ Interface-Description
//  Destructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//  None.
//-----------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=======================================================================

DataKey::~DataKey(void)
{
	// destructor
} 

//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method: setOptionKey
//
//@ Interface-Description
// this method takes encryptedkey from client and updates to ecc.txt file by creating new file .
// whenever this is called then a new file is created and updates optionkey .
// returns status depending on file operation. If successfully updated then returns status as SUCCESS otherwise FAILURE.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=======================================================================

SigmaStatus DataKey::setOptionKey( const Char *optionkey )const
{
	CALL_TRACE("setOptionKey( const Char *optionkey )");
	
	//file pointer for ecc.txt
	std::fstream optionkeyFileStream;			
	SigmaStatus status = FAILURE;

	//open newfile or existed file to write and verifying it in if block is open or not.
	optionkeyFileStream.open( DATA_KEY_OPTION_KEY_FILENAME, std::fstream::trunc |std::fstream::out );	
	if( optionkeyFileStream.is_open())													  			
	{
		optionkeyFileStream.write( optionkey,SoftwareOptions::SERIAL_KEY_LENGTH );
		optionkeyFileStream.close();
		status = SUCCESS;
	}
	return status;
}

//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method: getOptionKey
//
//@ Interface-Description
// this method takes void. It returns encryptedkey by reading from ecc.txt file on success.
// if it fails to read optionkey then returns a constant string "file not opened"..
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=======================================================================

const char* DataKey::getOptionKey( void )
{
	CALL_TRACE("getOptionKey( void )");

	static char readBuffer[SoftwareOptions::SERIAL_KEY_LENGTH+1];
	
	//file pointer for ecc.txt
	std::fstream optionkeyFileStream;

	//open existed file to read and verifying is open or not in if block  
	optionkeyFileStream.open( DATA_KEY_OPTION_KEY_FILENAME , std::fstream::in );	

	if( optionkeyFileStream.is_open())										
	{
		optionkeyFileStream.read( readBuffer , SoftwareOptions::SERIAL_KEY_LENGTH+1 );
		optionkeyFileStream.close();
	}
	
	return readBuffer;
}


//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method: setSerialNumber
//
//@ Interface-Description
//  This method accepts a SerialNumberId and a reference to	a SerialNumber.
//
//  Based on the SerialNumberId, it stores the contents of the SerialNumber 
//  into the designated location on the DataKey.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=======================================================================

SigmaStatus
DataKey::setSerialNumber( SerialNumberId serialNumberId,
						  const SerialNumber& rSerialNumber )
{
	CALL_TRACE("setSerialNumber( SerialNumberId serialNumberId, const SerialNumber & rSerialNumber )");
	
	SigmaStatus status = FAILURE;
	const char* destFilename = NULL;

	switch(serialNumberId)
	{
	case GUI:

		destFilename = DATA_KEY_GUI_SERIAL_NUMBER_FILENAME;
		break;

	case BD:
		
		destFilename = DATA_KEY_BD_SERIAL_NUMBER_FILENAME;
		break;
	}

	if(destFilename != NULL)
	{
		//file pointers for gui.txt and bd.txt files
		std::fstream bdguiNumberFileStream; 

		//open newfile or existed file to write and verifying it in if block is open or not.
		bdguiNumberFileStream.open( destFilename, 
											std::fstream::out |std::fstream::trunc | std::fstream::binary); 

		if( bdguiNumberFileStream.is_open() )
		{
			bdguiNumberFileStream.write((char *)&rSerialNumber,sizeof(rSerialNumber));
			bdguiNumberFileStream.close();
			status = SUCCESS;
		}
	}

	return status;

}


//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method: getSerialNumber
//
//@ Interface-Description
//  This method accepts a SerialNumberId and returns a SerialNumber.
//-----------------------------------------------------------------------
//@ Implementation-Description
//  Based on the SerialNumberId, it returns the contents of the 
//  designated serial number read at power-up or during the last update.
//-----------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=======================================================================

SerialNumber 
DataKey::getSerialNumber( SerialNumberId serialNumberId ) const
{
	CALL_TRACE("getSerialNumber( SerialNumberId serialNumberId )const");

	SerialNumber rtnValue;
	const char* destFilename = NULL;

	switch(serialNumberId)
	{
	case GUI:

		destFilename = DATA_KEY_GUI_SERIAL_NUMBER_FILENAME;
		break;

	case BD:
		
		destFilename = DATA_KEY_BD_SERIAL_NUMBER_FILENAME;
		break;
	}


	if(destFilename != NULL)
	{

		//file pointers for gui.txt and bd.txt files
		std::fstream bdguiNumberFileStream; 

		//open newfile or existed file to write and verifying it in if block is open or not.
		bdguiNumberFileStream.open( destFilename, 
											std::fstream::out |std::fstream::trunc | std::fstream::binary); 

		if( bdguiNumberFileStream.is_open() )
		{
			bdguiNumberFileStream.read((char *)&bdSerialNumber_,sizeof(bdSerialNumber_));
			bdguiNumberFileStream.close();
			rtnValue = bdSerialNumber_;	
		}
	}

	return rtnValue;

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateVentHeadOperationalTime 
//
//@ Interface-Description
//  This method accepts no argument and return SigmaStatus.
//
//  This method maintains vent head operational time. The background
//  maintenance task calls this method at power-up and every minute 
//  thereafter to update the operational minutes store in non-volatile
//  memory and to update operational hours in the data key every 10
//  hours. It writes to the data key once every ten hours due to the 
//  limited number of write operations for the data key, a FLASH based
//  memory device.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method maintains the number of operational minutes in non-
//  volatile memory using the NovRamManager. Every 10 hours, it updates
//  the number of operational hours to the data key device. This 
//  method accurately maintains the operational time using the system
//  countup timer OsTimeStamp to track the number of milliseconds
//  elapsed since the last time this method was called. It truncates
//  the elapsed millseconds value to a number of minutes and updates
//  non-volatile memory with the sum of this value and the number of 
//  vent-minutes read from non-volatile memory at power up. The 
//  milliseconds remaining (driftMs) after the truncation operation is 
//  added to the elapsed milliseconds on the next update cycle. This
//  accurately maintains the elapsed vent time even if the update cycle
//  is delayed.
//
//  This method also maintains the number of minutes to when the data
//  key should be updated. It bases this countdown time on the same
//  elapsed operational minutes calculated for the non-volatile memory
//  update. When the minutes to update countdown timer goes to zero, 
//  this method calls the syncDataKeyHours_ method to write the number
//  of operational hours to the data key based on the number of 
//  operational minutes maintained in non-volatile memory.
//  
//  The client must handle a failure status returned by this method.
// 
// $[LC08001] The system shall still accumulate operational time in 
//            non-volatile memory in the case of a "not functional" 
//            data key type -- see (BL00400), but these hours are not 
//            written to the data key until the system detects a 
//            "normal functionality" data key with a serial number 
//            matching the vent's serial number stored in non-volatile 
//            memory.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

SigmaStatus
DataKey::updateVentHeadOperationalTime( void )
{
	SigmaStatus errorStatus = SUCCESS;

	if (isFirstUpdate_)
	{
		updateTimer_.now();
		intervalDriftMs_ = 0;
		ventMinutesAtPowerup_ = NovRamManager::GetSystemOperationalMinutes();
		isFirstUpdate_ = FALSE;
	}
	else
	{
		const Uint32 intervalMs = updateTimer_.getPassedTime() * TIMEWARP;
	
		updateTimer_.now();
	
		Int32 intervalMinutes = (intervalMs + intervalDriftMs_) / MS_IN_ONE_MINUTE;
		intervalDriftMs_ = (intervalMs + intervalDriftMs_) % MS_IN_ONE_MINUTE;
	
		minutesSincePowerUp_ += intervalMinutes;
		minutesToDataKeyUpdate_ -= intervalMinutes;
	
		Uint32 currentVentMinutes = ventMinutesAtPowerup_ + minutesSincePowerUp_;
		NovRamManager::UpdateSystemOperationalMinutes(currentVentMinutes);
	
		SYSLOG((LOG_DEBUG, "updated novram vent minutes = %d", currentVentMinutes ));
	
		if (minutesToDataKeyUpdate_ <= 0)
		{
			errorStatus = syncDataKeyHours_();
		}
	}

	return( errorStatus );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: readVentHeadOperationalHours
// 
//@ Interface-Description
//  This method accepts no argument and returns a Uint32 containing the
//  number of operational hours read from the data key.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Reads the vent head operational hours from the data key device and
//  returns it to the client.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

Uint32
DataKey::readVentHeadOperationalHours( void )
{
	CALL_TRACE("readVentHeadOperationalHours( void )");
	
	//file pointer for oh.txt
	std::fstream OperationalHourFileStream;	 
	Uint32 tempHours =0 ;

	//open existed file to read and verifying is it open or not in if block 
	OperationalHourFileStream.open( DATA_KEY_OPERATIONAL_HOURS_FILENAME , std::fstream::in );
	if( OperationalHourFileStream.is_open())
	{
		OperationalHourFileStream >> tempHours;
		OperationalHourFileStream.close();
	}

	return tempHours;	
}

//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method: writeVentHeadOperationalHours
//
//@ Interface-Description
//  This method accepts a Uint32 argument and returns a SigmaStatus.
//  This method writes the specified operational time to the data key
//  device. Returns a failure status if the write operation fails.
//-----------------------------------------------------------------------
//@ Implementation-Description
//  Write the input value to the vent head operational time location.
//  Since the operational time is a 32-bit number, it is broken up into
//  two separate writes of 16-bits.
//-----------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=======================================================================

SigmaStatus
DataKey::writeVentHeadOperationalHours( const Uint32 operationalTime )
{
	CALL_TRACE("writeVentHeadOperationalHours( const Uint32 operationalTime )");

	//file pointer for oh.txt
	std::fstream operationalHourFileStream;	 
	SigmaStatus errorStatus = FAILURE;

	//open newfile or existed file to write and verifying it in if block is open or not.
	operationalHourFileStream.open( DATA_KEY_OPERATIONAL_HOURS_FILENAME, std::fstream::trunc |std::fstream::out );	
	if( operationalHourFileStream.is_open())
	{
		operationalHourFileStream<< operationalTime;
		errorStatus = SUCCESS;			
		operationalHourFileStream.close();
	}

	return errorStatus;
}

//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method: setDataKeyType 
//
//@ Interface-Description
//  This method accepts a DataKeyType and returns a	SigmaStatus.
//  This method takes the input data key type and writes it to the data
//  key device.
//-----------------------------------------------------------------------
//@ Implementation-Description
//  Write the input DataKeyType to the data key type location.
//  Since the DataKeyType is a 32-bit enumeration, it is broken up into
//  two separate writes of 16-bits.
//-----------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=======================================================================

SigmaStatus
DataKey::setDataKeyType( const DataKey::DataKeyType dataKeyType )
{
	SigmaStatus errorStatus = FAILURE;
	UNUSED_SYMBOL(dataKeyType);	

	//TODO: BVP - E600 This API probably does not make sense with flash-based
	//virtual data key. Need to investigate where it is used and see if it needs
	//to stay. In PB980, we dont change data key type on the vent (it is only 'read')
	//the 'set' only happens in the key generation tool.
	return( errorStatus );
}




//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method:  SoftFault
//
//@ Interface-Description
//  This method takes four arguments: softFaultID, lineNumber, pFileName,
//  and pPredicate and is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//-----------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//-----------------------------------------------------------------------
//@ PreCondition
//  None
//-----------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=======================================================================
void
DataKey::SoftFault( const SoftFaultID softFaultID,
                    const Uint32      lineNumber,
                    const char*       pFileName,
                    const char*       pPredicate)
{
  FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, DATAKEY, 
                           lineNumber, pFileName, pPredicate) ;

}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
 
//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method: syncDataKeyHours_
//
//@ Interface-Description
//  This method accepts no arguments and returns SigmaStatus.
//  This method writes the vent operational time stored in non-volatile
//  memory to the data key. Updates hours to the data key only if 
//  software options allow it. A return status of FAILURE indicates 
//  the data key write operation failed.
//-----------------------------------------------------------------------
//@ Implementation-Description
//  Uses SoftwareOptions::isDataKeyHoursUpdateEnabled() to determine if 
//  write to data key is allowed.
//
//  $[08002] At power-up and every 10 hours afterwards the BD CPU shall 
//           write the number of operational hours maintained in NOVRAM 
//           to the data key if the data key's "operational hours 
//           functionality" is specified as normal functionality per 
//           (BL00400).  
//-----------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=======================================================================
SigmaStatus DataKey::syncDataKeyHours_(void)
{
	SigmaStatus errorStatus = SUCCESS;
	minutesToDataKeyUpdate_ = DATAKEY_UPDATE_CYCLE_MINUTES;

	if ( rSoftwareOptions_.isDataKeyHoursUpdateEnabled() )
	{
		errorStatus = FAILURE;
		Uint32 currentVentMinutes = NovRamManager::GetSystemOperationalMinutes();
		Uint32 novramHours = currentVentMinutes / MINUTES_IN_ONE_HOUR;

		// write the number of hours stored in NOVRAM to the data key
		errorStatus = writeVentHeadOperationalHours( novramHours );

		if (FAILURE == errorStatus)
		{
			SYSLOG((LOG_DEBUG,"data key sync: failed - device write failure"));
			SYSLOG((LOG_DEBUG,"data key sync: failed - retry in five minutes"));
			minutesToDataKeyUpdate_ = 5;
		}
		SYSLOG((LOG_DEBUG,"data key sync: novramHours=%d, datakeyHours=%d", novramHours, readVentHeadOperationalHours()));
	}
	else
	{
		SYSLOG((LOG_DEBUG,"data key sync: bypassed - not enabled for this data key type"));
	}

	return errorStatus;
}

//============================== M E T H O D  D E S C R I P T I O N =====
//@ Method: syncVentOperationalTimes
//
//@ Interface-Description
//  This method accepts no arguments and returns no value.
//  This method synchronizes the vent head operational time stored in 
//  the data key with the time stored in NOVRAM.
//-----------------------------------------------------------------------
//@ Implementation-Description
//  This method is called on power-up to compare the vent head operational
//  time in NOVRAM and Data Key.  If the NOVRAM is not initialized (ie.
//  is zero) then the operational time stored in Data Key is used.
//  This method is also called in service mode when setting the vent 
//  head operational hours to a new value through the service laptop.
//
//  $[LC08000] If the operator chooses to synchronize the serial numbers 
//             (see Serial Number Setup Subscreen), the operational hours 
//             from the data key shall be stored in NOVRAM
//-----------------------------------------------------------------------
//@ PreCondition
//  None.
//-----------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=======================================================================
 
void
DataKey::syncVentOperationalTimes( void )
{
	if (rSoftwareOptions_.isDataKeyHoursUpdateEnabled())
	{
		// update NOVRAM from datakey hours
		const Uint32 DATAKEY_HOURS = readVentHeadOperationalHours();
		ventMinutesAtPowerup_ = DATAKEY_HOURS * MINUTES_IN_ONE_HOUR;
		NovRamManager::UpdateSystemOperationalMinutes( ventMinutesAtPowerup_ );

		updateTimer_.now();
		intervalDriftMs_ = 0;
		minutesSincePowerUp_ = 0;

		// write NOVRAM value to data key in a minute 
		minutesToDataKeyUpdate_ = 1 * TIMEWARP;

		SYSLOG((LOG_DEBUG,"novram sync: novramHours=%d, datakeyHours=%d", 
				ventMinutesAtPowerup_ / MINUTES_IN_ONE_HOUR, readVentHeadOperationalHours()));
	}
	else
	{
		SYSLOG((LOG_DEBUG,"novram sync: bypassed - not enabled for this data key type"));
	}
}






 
