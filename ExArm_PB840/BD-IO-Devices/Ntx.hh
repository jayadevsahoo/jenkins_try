#ifndef NTX_HH
#define NTX_HH

#include "SigmaTypes.hh"

class Ntx
{
public:
	static const char*	PSignature;
	static const Uint8	HeaderSize;
	static const char*	PInstrumentType;
	static const char*	PRevision;
	static const char*	PMinBootcodeRev;
	static const Uint8	Flags;
	static const Uint32	StartAddress;
	static const Uint32	DataChecksum;
	static const Uint16 BlockSize;
	static const Uint16 StartOfDataOffset;
	static const Uint32	ImageSize;
	static const char*  PFileImage;
};

#endif
