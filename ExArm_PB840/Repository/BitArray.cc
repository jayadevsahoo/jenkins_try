#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class:  BitArray - Abstract Array of Bits.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is an abstract base class of an array of bits.  Array
//  operations are provided, and each accessible element in the
//  array is a bit.  Safety is ensured by checking all bit accesses
//  for out-of-range access attempts.  There are operations to get the
//  maximum number of bits, along with the number that are either
//  set (turned "on") or clear (turned "off").  There are also
//  operations to test, and alter single bits, or all of the bits.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a safe and consistent way of using a collection of
//  boolean values.  It is very compact -- each boolean value
//  is represented by a single bit as opposed to an entire byte -- and
//  there are both single-bit, and multiple-bit operations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An array of integers is used to store all of the bits.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/BitArray.C_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "BitArray.hh"

//@ Usage-Classes...
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Initialization...
//
//=====================================================================

const Uint  BitArray::ALL_BITS_OFF_ = 0u;
const Uint  BitArray::ALL_BITS_ON_ = ~0u;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~BitArray()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this bit array.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

BitArray::~BitArray(void)
{
  CALL_TRACE("~BitArray()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getNumSet()  [const]
//
//@ Interface-Description
//  Return the number of bits that are set ("on") within this bit
//  array.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function iterates through every integer the internal
//  array of integers, and checks to see if there are any bits within
//  that integer that are set.  If the integer contains a set bit, then
//  bit-0 is tested, and the integer's value is shifted one bit right,
//  while there is still a bit that is set within the integer value.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  ((getNumSet() + getNumClear()) == getMaxBits())
//@ End-Method
//=====================================================================

Uint
BitArray::getNumSet(void) const
{
  CALL_TRACE("getNumSet()");

  const Uint  MAX_INTEGERS = getMaxElems_();

  Uint  numSet = 0u;
  Uint  elemValue;

  for (Uint idx = 0u; idx < MAX_INTEGERS; idx++)
  {   // $[TI1] -- ALWAYS executed...
    if (pBits_[idx] != BitArray::ALL_BITS_ON_)
    {   // $[TI1.1]
      // if this is the last integer of this bit array, only count the
      // "used" SET bits within this integer, otherwise count all of the SET
      // bits...
      elemValue = (idx == (MAX_INTEGERS-1))
		   ? (pBits_[idx] & ::UsedBitsMask(MAX_BITS_))	// $[TI1.1.1]
		   : pBits_[idx];				// $[TI1.1.2]

      // for each integer check to see if there are any bits that are set...
      for (; elemValue != 0u; elemValue >>= 1)
      {   // $[TI1.1.3] -- at least one iteration...
	// 'elemValue' contains at least one set bit, test bit-0...
	if ((elemValue & 1u) != 0u)
	{   // $[TI1.1.3.1] -- zero-bit set...
	  numSet++;
	}   // $[TI1.1.3.2] -- zero-bit NOT set...
      }   // $[TI1.1.4] -- no set bits in current integer...
    }
    else
    {   // $[TI1.2]
      // all of the bits within the current integer are set, therefore,
      // if the current element is the last integer of the array of integers,
      // add only the valid bits within the last integer, otherwise,
      // increment by the total number of bits in an integer...
      numSet += (idx == (MAX_INTEGERS-1))
		 ? (MAX_BITS_ % ::BITS_PER_LWORD)	// $[TI1.2.1]
		 : ::BITS_PER_LWORD;			// $[TI1.2.2]
    }
  }

  return(numSet);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                 [static]
//
//@ Interface-Description
//  Report the software fault that occured within the source code
//  of this class.  The fault, indicated by 'softFaultID', occured
//  at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
BitArray::SoftFault(const SoftFaultID softFaultID,
			    const Uint32      lineNumber,
			    const char*       pFileName,
			    const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, ABSTRACT_BIT_ARRAY,
			  lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEquivTo_(bitArray)  [const]
//
//@ Interface-Description
//  Is this bit array equivalent to 'bitArray'?  Two bit arrays are
//  equivalent when they both have the same number of bits, and each
//  bit in one bit array is equivalent to the corresponding bit in
//  the other bit array.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method first tests to see if the bit arrays are of the same
//  number of bits, and returns 'FALSE' if they are not.  If they
//  have the same number of bits, then each of the integers from the
//  bit arrays are tested for equivalence.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
BitArray::isEquivTo_(const BitArray& bitArray) const
{
  CALL_TRACE("isEquivTo_(bitArray)");

  // first, test to see if they are both the same size...
  Boolean  isEquiv = (getMaxBits() == bitArray.getMaxBits());

  if (isEquiv)
  {   // $[TI1]
    // they both have the same number of bits, now see if all of the
    // bits are equivalent by comparing each of the integers...

    // get the total number of integers in this bit array...
    const Uint  MAX_INTEGERS = getMaxElems_();

    // iterate while there are more integers and each of the integers are
    // equal to each other...
	Uint idx;
    for (idx = 0u;
	 idx < MAX_INTEGERS  &&  pBits_[idx] == bitArray.pBits_[idx]; idx++)
    {   // $[TI1.1] -- at least one iteration...
      // do nothing...
    }   // $[TI1.2] -- no iterations...

    isEquiv = (idx == MAX_INTEGERS);  // $[TI1.3] (T) $[TI1.4] (F)

    if (!isEquiv  &&  idx == (MAX_INTEGERS - 1))
    {   // $[TI1.5]
      // all of the COMPLETE integers are equivalent, now see if the USED
      // portion of the last integer of this instance is equivalent to the
      // corresponding portion of 'bitArray'...

      // get a mask with all of the bits SET that are "used" in the last
      // integer...
      const Uint  ALL_USED_BITS_ON = ::UsedBitsMask(MAX_BITS_);

      // XOR the two values together and mask out all of the "unused" bits to
      // see if there is a difference...$[TI1.5.1] (TRUE)  $[TI1.5.2] (FALSE)
      isEquiv = (((pBits_[idx] ^ bitArray.pBits_[idx]) & ALL_USED_BITS_ON) == 0);
    }   // $[TI1.6] -- all of the integers ARE equivalent, OR NOT at 2nd to
	//             last integer...
  }   // $[TI2] -- 'this' is of a different size than 'bitArray'...

  return(isEquiv);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  copyAllElems_(pNewBits)
//
//@ Interface-Description
//  Copy all of the integers from the array given by 'pNewBits', to all of
//  the integers in this instance's array.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
BitArray::copyAllElems_(const Uint* pNewBits)
{
  CALL_TRACE("copyAllElems_(pNewBits)");

  // for each integer of this bit array initialize its value by copying
  // each integer from 'pNewBits' to this array of integers...
  for (Uint idx = 0u; idx < getMaxElems_(); idx++)
  {
    pBits_[idx] = pNewBits[idx];
  }
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setAllElems_(newValue)
//
//@ Interface-Description
//  Set all of the integers in this instance's array to the value given
//  by 'newValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
BitArray::setAllElems_(const Uint newValue)
{
  CALL_TRACE("setAllElems_(newValue)");

  // for each integer of this bit array initialize its value by
  // initializing each integer to 'newValue'...
  for (Uint idx = 0u; idx < getMaxElems_(); idx++)
  {
    pBits_[idx] = newValue;
  }
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  print_(ostr)  [const, pure virtual]
//
// Interface-Description
//  This is a pure virtual method that provides the derived classes
//  a means of "dumping" the contents of an instance to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

// Not-Implemented


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  areAllEqualTo_(elemValue)  [const]
//
//@ Interface-Description
//  Are all of the elements (integers) of this bit array equal to the
//  value indicated by 'elemValue'?
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses the '::UsedBitsMask()' function, which is a
//  'BitUtilities' utility.
//
//  It is critical for this method to be fast.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
BitArray::areAllEqualTo_(const Uint elemValue) const
{
  CALL_TRACE("areAllEqualTo_(elemValue)");

  // get the total number of integers in the array of integers...
  const Uint  MAX_INTEGERS = getMaxElems_();

  // iterate while there are more integers and each of the integers are
  // equal to 'elemValue'...
  Uint idx;
  for (idx = 0u; idx < MAX_INTEGERS  &&  pBits_[idx] == elemValue; idx++)
  {   // $[TI1] -- at least one iteration...
    // do nothing...
  }   // $[TI2] -- no iterations...

  Boolean  isEqualTo = (idx == MAX_INTEGERS);  // $[TI3] (T) $[TI4] (F)

  if (!isEqualTo  &&  idx == (MAX_INTEGERS - 1))
  {   // $[TI5]
    // all of the COMPLETE integers are equal to 'elemValue', now see
    // if the USED portion of the last integer is equal to the
    // corresponding portion of 'elemValue'...

    // get a mask with all of the bits SET that are "used" in the last
    // integer...
    const Uint  ALL_USED_BITS_ON = ::UsedBitsMask(MAX_BITS_);

    // XOR the two values together and mask out all of the "unused" bits to
    // see if there is a difference...$[TI5.1] (TRUE)  $[TI5.2] (FALSE)
    isEqualTo = (((elemValue ^ pBits_[idx]) & ALL_USED_BITS_ON) == 0);
  }   // $[TI6] -- all of the integers ARE equivalent to 'elemValue', OR
      //           NOT at 2nd to last integer...

  return(isEqualTo);
}
