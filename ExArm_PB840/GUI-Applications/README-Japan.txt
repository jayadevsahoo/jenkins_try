                       Build Procedure and Background Info
                 for the Japanese Implementation of the Model 840


Originally written: 4 June 1998
Original Author: Charlie Wallace

Revision History:

Update: 11 Feb 1999
Update Author: Charlie Wallace

Update: 05-Nov-1999
Update Author: Daniel Osman

Update: 01-Dec-1999
Update Author: Charlie Wallace
Change Descrip: clarifications only.

Current version: $Header:   /840/Baseline/GUI-Applications/vcssrc/README-Japan.txv   25.0.4.0   19 Nov 2013 14:08:18   pvcs  $
IMPORTANT - if this file is updated, it must be checked into the PVCS archive under
GUI-Applications!


This document is for anybody who needs to make changes to the Japanese messages for
the model 840, and then (of course) needs to rebuild the code.

(This doc assumes that the build is being done under the Baseline project..)

This README covers the following subjects:

  OVERVIEW OF BUILD PROCEDURE
  DETAILED BUILD PROCEDURE
  EXPLANATION OF HOW WE ADAPTED OUR FONT SYSTEM TO HANDLE JAPANESE
  Notes on adapting WORD 97 for Japanese
  Notes on PropHead Development's Toolset
  Notes on current weaknesses and possible improvements


==========================================================================  
====== OVERVIEW OF BUILD PROCEDURE =======

First, here is an overview of the whole process at a high level.  A detailed
version is also provided further on.  Skip to the detailed section for guidance during a run.

* Check out the 3 Japanese doc files in the GUI-Applications subsys on the SUN, and ftp
  them to the NT PC.  These are Word 97 files.
 
* Edit them as needed on the PC using Word 97, with the Far East extensions and KanjiKit.

* Run the "JAPEXTRACT" program on the PC. This analyzes unicode versions of the 3
  doc files and updates the set of font files (.BFN) (if needed) and outputs the set of 3
  message files (.IN)**.

  ** Todo: JAPEXTRACT program needs to be updated to produce ".LA" files instead of ".IN" files.

* If any fonts were updated above, use the "Create .cc and .hh files for all Japanese BFNs"
  command in the "FontEdit" program on the PC.  This converts the Jpnse font files (.BFN)  
  into compilable font source (.cc and .hh) files.

* FTP the changed font source (.cc and .hh) files and the changed message files (.IN)** to the
  SUN.  Also FTP  the updated .doc files.  Note: usually, only the third ("c") .cc file for 
  each Japanese point size is affected.

  ** Todo: JAPEXTRACT program needs to be updated to produce ".LA" files instead of ".IN" files.

* Archive the overall set of BFN files, they will be needed for future updates.  Also
  archive the .DOC files.

* If the font source files were changed, copy the new ones to the VGA-Graphics-Driver 
  subsystem. 

* Rebuild the VGA-Graphics-Driver subsystem, thus producing a new library file.

* Copy the changed .IN** files to the GUI-Applications subsystem.  These must be manually
  inserted into the corresponging strings file: JapaneseAlarmStrs.IN,
  JapanesePromptStrs.IN, and JapaneseMiscStrs.IN.  Note that the non-translated
  misc strings reside in the JapaneseMiscStrs.IN file.

  ** Todo: JAPEXTRACT program needs to be updated to produce ".LA" files instead of ".IN" files.

* Rebuild the GUI-Applications subsystem, thus producing a new library file.

* Rebuild the Build subsystem, thus producing the new absoulte file GuiLoad.abs. This
  is now ready to download to a ventilator.

* All changed files must be checked in to the appropriate PVCS archive.


==========================================================================  
====== DETAILED BUILD PROCEDURE =======


* The first step is to check out the doc files on the SUN.  These are Word 97
  files containing all of the Japanese strings.  They are archived under the
  GUI-Applications/groupsrc directory, under the Baseline project.  The 3 files are 
       JapaneseMiscstrs.doc ("japanese misc msgs"), 
       JapanesePrmtstrs.doc ("japanese prompt msgs"), and
       JapaneseAlrmstrs.doc ("japanese alarm msgs"). 
 
 Any of these that need changing should be checked out using a line like

    get -l JapaneseMiscstrs.doc

NOTE: to avoid corrupting these files while checking them out, make sure the PVCS
NOEXPANDKEYWORDS and NOTRANSLATE options are set.  This is done using the vcs command.
You can use the VLOG command to check these settings.

* Once these are checked out, you should ftp them over to the PC side. Be sure
  to use the binary mode when doing the ftp.  Note that you must get all three,
  even if only one or two needs to be changed.  However - only the ones being
  changed must be locked.

* Assuming that you will be doing an "update" run with JapExtract (Sigma-C button), 
  rather than the full font regen run (Sigma button) (not recommended, char edits will be
  lost!), you will also need to get all of the Japanese .BFN font files on the SUN, and
  ftp them to the PC.  These are archived in the VGA-Graphics-Driver subsystem.  Like the
  doc files, you must use the binary mode when doing the ftp.  You should lock the "_3" files,
  (named like JAP_XX_3.BFN where XX is the size) because they will be changed if
  the new Japanese messages contain any new characters. You need sizes 8, 10, 12, 14, 16, 18, and 24.  
  However, the JapExtract application also needs at least the "a" and "b" files for size 10: JAP_10_1.BFN
  and JAP_10_2.BFN.  You can check these out without a lock, they will not be changed.

* Go the to VGA-Graphics-Driver/groupsrc directory (under the japanese
  project), and check out all of the "c" japanese font source files, using a line like
  this:
  
    get -l VgaJapanese{8,10,12,14,16,18,24}c.cc
    
  This assumes that the JAPEXTRACT run will be an "update" run, so only the
  third file for each point size might be changed.  You don't need to ftp these to the PC.

  ============================================================================
  =  Table 1: Ftping between Sun and Windows
  =  ---------------------------------------
  =  File Type                         Ftp Mode
  =  ---------                            --------
  =  Japanese*.doc                BINARY
  =  JAP*.BFN                       BINARY
  =  VgaJapanese*.cc            ASCII  --> you only ftp these in the to-SUN direction
  =  Japanese*.LA                 ASCII  --> you only ftp these in the to-SUN direction
  ============================================================================

The following steps are done on the PC side - they require Windows NT and Word
97.


* Edit the doc files using Word 97 to make any needed changes to Japanese
  strings.  See below for notes on how to configure Word to handle Japanese.

  If your change only affects non-translated messages, you don't need to
  go thru all this trouble - just edit English{Misc,Alarm,Prompt,Help}Strs.LA, 
  on the Unix side, as needed.

  Usually, any changes you need to make to Japanese strings can be handled by
  copy-and-pasting from a document provided by our SME ("Subject Matter
  Expert") in Japan, thus avoiding the need to directly enter Japanese text.
  (Currently our SME is Misao Takeuchi, who works in our Japanese office.  He
  is reachable via CCMail.)  This requires him to send a Word document
  (currently he sends Word 6.0 documents) as an email attachment.

  If you don't have text to copy-paste, you can use the "romanji" feature of
  KanjiKit to enter each desired Japanese word using the Roman characters, and it will
  display several alternatives of how the Japanese text would look - you must
  be able to choose the correct one.  So you will need the roman spelling, plus
  a picture of how the Japanese should look - this could be sent by fax.

  The format of the doc files is important - the JapExtract program is hard-wired
  to expect the format currently used.  Refer to the section below that explains
  the Japextract and Fontedit tools for format info.

  The doc files may contain comments like "##corrected" or "$corrected".  You can add
  comments like this for personal use, but they are not needed.  There is no current
  convention for such comments.

  The font size of the actual characters in the cheap text string in the doc files
  does not matter.  These files will be converted to unicode so the size of those characters
  in the doc file will be lost. The value that we give to the cheap text's point size
  (eg. p=value) matters.  

* Make sure there are no un-accepted revisions in the .doc files!  If there are,
  the unicode files (see below) will not be usable.  To accept revisions, see the
  "Tools/Track Changes/Accept or Reject Changes" menu selection in Word 97. Be careful,
  because there may be revisions that don't show if the "Highlight Changes" selection
  is set up with "Highlight changes on screen" turned off.

* Save the new files as Word97 doc files for archiving! (don't forget!)  Make sure that word doesn't 
  use it's company default, which is Word95/6.0.

* Then, save the new files in unicode format under the names alrmstrs.uni, prmtstrs.uni,
  and miscstrs.uni.  These are temp files used as inputs to JapExtract.  This is done using the 
  usual Word "Save As" command, but setting the output file type to "unicode".  You'll need to scroll down 
  a bit thru all the available output types to find "unicode".

* Run the "JAPEXTRACT" program.  But read ahead first to see how! You'll need to
  set up a shortcut to the app, customized with your directory as the "working" directory.  
  See below for details.

  Currently this is at version 2.0, with modifications that allow edits in previous
  versions of the BFN files to be preserved.  Only the "update" command 
  (shown as Sigma-C on the tool bar) should be used, otherwise all the BFN
  files will be regenerated from scratch, and all improvements to the characters will
  be lost!

  For an example of how to set up the working directory, you can refer to the following:
      S:\sigma\software\messages\japanref\tools\japextract\fonts
  Here's where you find the most recent version of the JapExtract.exe application:
      S:\sigma\software\messages\japanref\tools\japextract\Debug\JapExtract.exe

  You can do your run in this directory, or you can set up your own.  The main thing is to
  make sure you provide the needed input files.  These include CJKXREF.JIS, all three 
  .UNI files, and a set of Japanese .BFN files.  These must be the most current
  versions.  Actually, only the JAP_*_3.BFN files are needed, with the exception of 
  point size 10, where all three (JAP_10_1.BFN, JAP_10_2.BFN, and JAP_10_3.BFN) are needed.
  Also, I usually define a fontSrc subdirectory to hold the automatically generated
  .cc and .hh files produced later when you run FontEdit.

  NOTE: here is how you tell the program where to find the input files: you create
  a shortcut to JapExtract.exe, then edit the properties of the shortcut and set the "working
  directory" to the desired location.  This location is shown when the program is started.

  Once this is launched, you click on the button labeled with a "sigma C"
  to start processing. (Don't click the "Sigma" button - it will regen all BFNs)

  This analyzes the 3 unicode files, determines all the unique Japanese
  characters present, identifies any new characters not already present in the
  initial set of BFN files.  These are added to the end of the last (third) font 
  file for each point size.  (It also identifies characters that are present but
  no longer used - however, currently we leave these characters alone.  In the
  future we might use their slots for new characters.)  If no new characters
  are present, the BFN files are not changed.  In any case, the first and second
  BFN font files for each size are not changed.

  NOTE: if no new characters are found, you can skip all of the steps relating to fonts
  below.  You will not need to run FontEdit, and it will not be necessary to rebuild the 
  VGA-Graphics-Driver subsystem at all!

  A window on the PC displays a listing of all the characters -- any new chars will be 
  at the end of the list.  The top of this list will say how many new chars and
  how many obsolete chars there were.  The listing shows these in a pre-determined size (I
  think it's 14-point) but acutally, all characters are generated in all font sizes.  

  Once this is done, JAPEXTRACT creates "*.IN" (**) files for each kind of messages:
  ALARMS.IN, PROMPTS.IN, and MISC.IN.  These are incorrectly named - the names needed
  to do the GUI-Applications build are JapaneseAlarmStrs.LA, JapanesePromptStrs.LA,
  and JapaneseMiscStrs.LA.  You will need to rename them after they have been FTP'd
  over to the SUN. These are in the correct format to be built on the SUN, no editing is needed.

  To creat these three .IN files, the Japanese messages are
  extracted from the unicode files, and each unicode character is mapped into a
  corresponding code based on its location in the font files. See explanation
  below on how the mapping is done.

  Note that as part of the build process, additional non-translated messages are added to these
  .LA files (mainly to the Misc file), thus creating files with the extension .IN; and in a further build step,
  bracketed symbol names are converted to the raw format, creating files with extension .in.

  (**) Todo: JAPEXTRACT program needs to be updated to produce ".LA" files instead of ".IN" files.

  Although all three .IN (**) files are created for each run, these will only differ from
  the old ones if the corresponding .doc file has been changed.

* If any of the BFN files were changed in the JAPEXTRACT run, we need to 
  run the "FontEdit" program supplied by PropHead.  This is currently at version
  2.0. It converts all the .bfn files into a complete set of compilable .cc and .hh files.
  (It can also be used to edit individual characters.)

* You may find, while looking at the individual new characters in FontEdit, that some of
  the characters may look very different from the original "reference" characters 
  in the Word document you received from the translators.  Unfortunately, the technique used
  by JapExtract to grab bitmaps from a truetype font is less than ideal, and the resulting Japanese
  characters are often of poor quality.

  ******** HINT: how to fix the ugly characters *******
  You can use FontEdit to display the characters and to edit them.  It is not really necessary to be
  a Japanese expert.  The trick is to display the same character in Word  (which usually looks much
  better), then do a screen-capture to grab the entire screen into the clipboard (press shift-PrintScreen).
  Note that this works best if the "smooth fonts" feature in the windows control panel
  (display properties) is turned off.  Launch Paint, insert the clipboard, then clip out the characters of
  intrest - these will be the "reference" versions.  This will result in a .BMP file.  You can adjust Paint
  to display this at very high magnification so that individual pixels are visible.  This can displayed on-
  screen side-by-side with FontEdit (just un-maximize them), to guide you while you fix the ugly versions
  of the new Japanese characters grabbed by Japextract.

  *** Review of edited characters *****
  FontEdit provides a way to display all of the charcters of a font (part a, b, or c) on-screen at the
  same time.  You can then use the "Print Scrn" key on the PC to capture the screen.
  Then you can access this screen capture in Paint, cut and paste the characters
  of interest into a clean .bmp file.  You could also cut and paste into the .bmp file (in Paint)
  the "reference" characters from the Word document you received from the translator, and then
  send this information to the translators to determine if the characters are acceptable.

  *** How to run FontEdit ****
  Here's where you find the most recent version of the FontEdit.exe application (where S: is \\CB4\Vol2\)
      S:\sigma\software\fonts\FontEdit\Debug\FontEdit.exe

  Once this is launched, if you want to generate a complete set of cc and hh files,
  you click on the button labeled with "sigma with an arrow thru it" to start processing.
  A dialog box appears asking where to find the incoming BFN files; this is a bit 
  confusing because it seems to be asking you to enter a file name, but really only looks
  at the directory selected.  Once this is done, a similar dialog box appears asking 
  where to put the .cc and .hh files - it also is only looking for a directory.
 
  If you don't need to generate the complete set, you can export cc and hh files for one 
  .bfn at a time using the export command (toolbar icon: floppy with arrow thru it).  

* FTP any changed japanese font files (.cc and .hh) and the changed message files (PROMPTS.IN,
  MISC.IN, and ALARMS.IN) (**) to the SUN. Be sure to use the ASCII transfer mode for all of these!!
  Also be sure that your ftp program supports long file names   These should go the the 
  VGA-Graphics-Driver/groupsrc directory.  

  You will need to rename them after they have been FTP'd over to the SUN. - the names needed
  are JapaneseAlarmStrs.LA, JapanesePromptStrs.LA, and JapaneseMiscStrs.LA. 
  Normally the .hh files will not change, so you don't need to FTP them..

  (**) Todo: JAPEXTRACT program needs to be updated to produce ".LA" files instead of ".IN" files.

  Also FTP the changed .BFN files (if any) to the same place, but this time use binary mode.

  Also FTP the updated .doc files, JapaneseAlrmstrs.doc, JapanesePrmtstrs.doc, and 
  JapaneseMiscstrs.doc, to the GUI-Applications/groupsrc directory.  Be sure to use
  BINARY mode for these transfers.
  

The following steps are all performed on the UNIX side:


==================================================
VGA REBUILD: 

Note: this is only needed if the change to the Japanese doc files resulted in
a change to the font files:

* Copy the changed japanese font files (.BFN) to the VGA-Graphics-Driver/groupsrc,
  directory, overwriting the files you ckecked out earlier.

* Copy the changed japanese font source files (.cc) to the VGA-Graphics-Driver/groupsrc,
  directory, overwriting the files you ckecked out earlier.  These are ready to compile.

* cd to the prod_gui_target directory, and remove the old VgaJapanese*c.o files.

* Rebuild the VGA-Graphics-Driver subsystem, thus producing a new library file.
  To do this, go back the to VGA-Graphics-Driver/groupsrc directory and do this:

    build LANGUAGE=JAPANESE PLATFORM=TARGET CPU_TYPE=GUI_CPU MODE=PRODUCTION

  Some of these settings might be specified in your makedefs file.
  Note: if this library is built for English, it will not include the Japanese fonts
  to save memory.  


====================================
GUI-Applications Rebuild:

* Copy any changed .LA files (JapaneseAlarmStrs.LA, JapanesePromptStrs.LA, 
  and JapaneseMiscStrs.LA) to the GUI-Applications/groupsrc directory.  
  You must have previously checked out each of these strings files
  that needs changes.

  These auto-generated files do not require any editing, and there are no modification
  logs to update - we rely on the PVCS checkin log for mod notes.

  To make sure the symbol substitution processing is redone during the build, you should delete the 
  Japanese*Strs.{IN,in} file for any Japanese*Strs.LA file that you changed.

* Rebuild the GUI-Applications subsystem, thus producing a new library file.
  To do this, go to the GUI-Applications/groupsrc directory and do this:

    build LANGUAGE=JAPANESE PLATFORM=TARGET CPU_TYPE=GUI_CPU MODE=PRODUCTION 

  Some of these settings might be specified in your makedefs file.

  Note: make sure that all of the string files were recompiled.  If they are not recompiled,
  you can force them to be compiled by removing their object files.  These are located
  under the GUI-Applications/groupsrc/prod_gui_target/JAPANESE directory. The files
  to delete are MiscStrs.o, PromptStrs.o, and AlarmStrs.o.

=====================================
Build subsystem rebuild:

* Rebuild the Build subsystem.  Before doing this you may need to rebuild the
  links to your libraries.  To do this, go the the Build/groupsrc directory
  under the Japanese project and do this:

    build LANGUAGE=JAPANESE PLATFORM=TARGET CPU_TYPE=GUI_CPU MODE=PRODUCTION updateLinks

  Then build the absolute file like this:

    build LANGUAGE=JAPANESE PLATFORM=TARGET CPU_TYPE=GUI_CPU MODE=PRODUCTION abs

  thus producing the new absoulte file GuiLoad.abs. This is now ready to
  download to a ventilator.

================================
Testing and check-in:

* Test the downloaded code to verify that the changes work as planned.  Perform
  label review, and fill out the PEER REVIEW form to indicate that the changes are OK.
  This step may require the assistance of the SME (Misao Takeuchi).  If he is not on-site,
  you may need to make a screen capture and send it to him via email.  This is done using
  a BTREE unit.

NOTE: it may be necessary to do some screen captures from a BTREE unit in order to keep an
up-to-date set of Japanese screen prints available for verification purposes - this has not yet
been established as a required step as of 13Jan99.

Once the SME checkout is complete, the PEER REVIEW COMPLETED label is added.

* Check in the changed JapaneseAlarmStrs.LA, JapaneseMiscStrs.LA, and
  JapanesePromptStrs.LA files. For each of these, the PEER REVIEW COMPLETED
  label is needed, along with a version printout like the one in the previous
  step.

* Check in the changed font source files (if any).  For each of these, the PEER REVIEW COMPLETED
  label is needed, along with a version printout like the one in the previous
  step.  To check them in all together, use this:

     put -y VgaJapanese{8,10,12,14,16,18,24}c.cc

  This prompts only once for a change description - enter a note indicating the date of
  the JapExtract run where the file mapping was determined.  Then, to generate the
  version printout, do this (may be obsolete):

    logDoneP VgaJapanese{8,10,12,14,16,18,24}c.cc | enscript -Gr1

* Check in the changed .BFN font files (if any).  For each of these, the PEER REVIEW COMPLETED
  label is needed, along with a version printout like the one in the previous
  step.  To check them in all together, use this:

     put -y JAP_{8,10,12,14,16,18,24}_3.BFN

* IF ALARM STRINGS HAVE CHANGED: if any strings have become longer than they
  were, you should rerun the alarm fit test program.  This is archived in the
  GUI-Applications/vcstest directory.  It uses files TestAlarmStrings.cc and
  AlarmStrings.hh, and is built in development mode.  To build it, go to your
  GUI-Applications/testsrc directory and check out these files, plus the README
  file for more detailed instructions.


==================================
Checkin of WORD doc files:

* The updated .doc files should be checked into the PVCS archive in the
  GUI-Applications subsystem.  Add the LABEL REVIEW COMPLETED label.

  Print out the new version numbers using

    vlog -bv"LABEL REVIEW COMPLETED" JapaneseAlrmstrs.doc JapaneseMiscstrs.doc 
              JapanesePrmtstrs.doc | enscript -Gr1

  The printout is needed with the label review package.

NOTE: In the past, we had problems with corruption of the doc files resulting from
either bad FTP programs or PVCS processing.  It is a good practice after checkin to
reverse the FTP process and move a few files back to the PC with a new name, to make 
sure it is still identical to the original on the PC side.  Use vdiff or the DOS COMP
command for the comparison.


==========================================================================  
====== EXPLANATION OF HOW WE ADAPTED OUR FONT SYSTEM TO HANDLE JAPANESE ========

Our pre-existing font system was extended to support Japanese.  Since one font
file can handle a max of 256 characters, which is all that is required for
ASCII characters, it is obvious that more than one font file is needed for
Japanese.  The VGA-Graphics-Driver subsystem was extended to support the use of
up to 4 font files for each point size of japanese text.  These are identified
by letter: "a", "b", "c", and "d" (currently "d" is not needed).  Thus, these 3
give a max of 768 Japanese characters (or 1024 if 4 files are used).  In practice, 
some of the ASCII locations are reserved, so the total is a bit less.

When the font files were originally
generated by the JapExtract program (see below), the
Japanese characters were assigned succesively to slots in the "a", "b", "c", or
"d" fonts according to how often they occurred in the message files. The most
common characters were thus mapped to locations in the first "a" font, less-used
characters are in the "b" font, and so on.  A set (a, b, c) of fonts was
produced for each point size.  Each Japanese character is provided in all point
sizes.

Now, when the font files are updated with new characters, we do not alter the 
initial mapping (as of version 2.0).  This makes it possible to edit the BFN files; 
previously, any edits would be lost when all BFNs were regenerated in each run.
Now, only the third (c) font is changed when Japextract runs, and only if new
characters were found; and any edits in the C font file are preserved.

To render a character identified by an 8-bit (0-255) character value, we need a
way to determine which of the 4 fonts (for a given point size) is to be used; 
this is done by checking
for a preceding "escape" character.  A different reserved escape character is
defined for the "b" (^) "c" (tilde) and "d" (`) fonts; the absence of an escape
character indicates the "a" font.  The 0-255 character codes (possibly preceded
by an escape character) appear in the cheap text strings as a backslash
followed by a 3-digit octal value, thus allowing them to be treated as ascii
characters.

At a higher level, some changes were made to the GUI-Foundations subsystem to
support japanese.  In particular, a new "style" was added to the "cheap text"
string-formatting syntax for japanese.  So in addition to the T, N, and I
styles (thin, normal[really bold], and italic), we now have J.  Classes
TextFont and TextField were affected.  Refer to the explanation of the
JapExtract tool below for more info on how this is used.

And at the GUI-Applications level, some changes were made to the AlarmRender
class to support rendering of Japanese alarm messages.  The tokenizing
process is done differently for Japanese - basically, each character is a
separate token, except that characters joined by "+" characters all form
a single token.  This was needed because certain combinations of Japanese
characters should not be broken apart.


==========================================================================  
====== Notes on adapting WORD 97 for Japanese ===============

To get the japanenese characters to appear correctly, it was necessary to
install the "Far East" support package.  This should be available on the
microsoft web site, or possibly on the original install CD for word.  Mine was
installed from a CD provided by Prophead development.  In addition, we used a
program called KanjiKit to enable japanese strings to be entered and edited.
This was purchased for us by PropHead.


==========================================================================  
====== Notes on PropHead Development's Toolset =========

The tools used on the NT system were developed by contractors at PropHead
Development in Carlsbad.  The primary developer was Rob Hyatt, who incidentally
used to work for NPB.  The president of PropHead, Brian Bender, also worked
here, and was the original developer of our font system.  The phone number
there is (760)757-0900.  Rob's extension is 115, his email is rob@propeller.com.


The JapExtract and FontEdit programs were developed using MS Visual C++ with
MFC.  A separate user's guide for these programs is being written at this time.

Both tools were updated in Jan 99 by Charlie Wallace, bringing both to
rev 2.0.

The format of the doc files is important.  For each string, JapExtract expects
a string name first, then the actual string.

The string name is identified as follows: it is preceded by a number
followed by a period.  The name is all caps with possible embedded underscores,
and the name must be followed by a comma.

The string must be in "cheap text" format.  It must start with an open brace
character ("{"), which must be immediately followed by "p=".


=== JapExtract Application ==============================================

Note: Instructions for the use of this program are provided by PropHead - but
the short version is: start the program, and click the "sigma-C" button.

The "JapExtract" program analyzes all 3 doc files, including the alarms,
prompts, and misc messages.  These must have been saved in unicode format,
under the names alrmstrs.uni, prmtstrs.uni, and miscstrs.uni.

There are 2 modes of operation. One (the Sigma button) generates a full set
of BFN files from scratch each time it runs.  The other (Sigma-C) only updates
the existing BFN files without altering any existing characters - actually, only
the third font file of each set of three is changed, and only to add new 
characters at the end.  Obsolete characters are counted but not removed.

---- Complete Regen description ----
DON'T USE THIS OPTION unless you really intend to discard all the 
editing of the BFN files that has been done to improve the look of the
Japanese characters!

JapExtract first determines all the unique Japanese characters present, and
builds font files that include all these characters.  This program outputs a
set of .bfn and .bdf files.  The .bdf files are not used for production builds
- these are SUN fonts.  The .bfn files are the font files.

The original XXX.bfn files that were used as input are first stored (saved)
as AXXX.bfn files, so the previous .bfn files are recoverable.

Sometimes the .bfn files that result are ugly.  In such cases you can 
create a document containing the "reference" characters from the translator's 
Word document and the "new" characters generated by JapExtract.
For example, you can start FontEdit, load one of the font files, and hit 
the [AB] button-menu item, which will display all the characters.
You can then hit the "Print Scrn" key on the PC, which will perform
a screen capture.  Then, you can start Paint program and paste the screen capture
into Paint.  You can then cut out the "new" characters and paste them
onto another blank Paint document that can serve as the document 
to give to the SME.  You can follow this procedure for each "new" character
for each font size.  You can then use the Word document from the SME,
change the font size of the character in Word to create multiple instances
of the same character, one at each font size, and use the "Print Scrn"
screen capture ability to store these characters into the same Paint document.
Eventually you will have all the "new" and "reference" characters
at all different font sizes, and you can send this document to
the SME for review and suggestions for correction.

It then makes a second pass thru the three unicode files, converting each
japanese character (represented in unicode) to the corresponding ascii code
indexing that character in the japanese font file, possibly preceded by a
"shift" character to indicate which japanese font file (out of the set of 3
provided for each point size) to use.  This pass results in a set of .IN ** files.

** Todo: JAPEXTRACT program needs to be updated to produce ".LA" files instead of ".IN" files.

For prompts and "misc" strings, each contiguous set of converted japanese
characters is surrounded by a set of braces, and "J:" is inserted after the
open brace to indicate that the japanese style is in effect.  Since japanese is
modeled as a style, the original "thin" or "italic" or "normal"(really bold)
syles no longer exist.  JapExtract preserves these only by mapping thin and
bold japanese characters to different locations within the font file, where
thin and bold versions are stored.  Italic characters are treated as if they
were "normal".  Note that after this conversion is done, an open brace is never
encountered within a set of japanese characters, so we never enounter the
situation where non-japanese characters are embedded within japanese text - and
thus it is never neccessary to "remember" the original style replaced by
japanese.

For alarm strings, the processing is a bit different.  We do not insert the
"J:" before the japanese in these strings, because it is assumed - the
AlarmRender.cc code automatically does this at run time. However, we do
sometimes have non-japanese text and symbol references (in square brackets)
embedded within the japanese strings.  To avoid the "remembering the original
style" issue mentioned above, we simply require that each contiguous set of
non-japanese text and each symbol in the alarm text must be surrounded by
braces, and preceded by the appropriate style (N,T, or I).

---- Update-Only Description (Sigma-C) ----
Sigma-C only updates the existing BFN files without altering any existing 
characters - actually, only the third font file of each set of three is changed,
and only to add new characters at the end.  Obsolete characters are counted
but not removed.


========== FontEdit Application =================================================

The "FontEdit" program provides editing capabilities and is also used to
automatically generate compilable versions of the font files. It is also able
to output Sun-compatible font files (.bdf files) - but this feature is disabled
after version 2.0 because it was not being used..  It is able to load any
.bfn file, including both english and japanese.  It displays one character at a
time for editing/viewing. It provides a button that allows all of the japanese
fonts to be processed as a batch to produce .cc and .hh output files.

Note: Instructions for the use of this program are provided by PropHead - but
if all you want to do is to generate the .cc and .hh files, all that you have
to do is to start the program, and click the "sigma + arrow" button.  This converts all
the japanese .bfn files into compilable .cc and .hh files. These files are in
the correct format to be compiled on the SUN.


==========================================================================  
===  Notes on current weaknesses and possible improvements ===============

English 16-point font is currently faked.  In VgaFontData.cc, there is a hack that
redirects accesses to 16-point English fonts to the corresponding 14-point
characters.

Many strings in the EnglishMiscStrs.IN file are "bare" strings - that is, there
is no "cheap text" syntax.  Any such strings that are translated into Japanese
(most are not) have been converted to non-bare by adding "{p=10:" in front, and
adding "}" in back (a few use p=12 instead of p=10).  This is done because the
JapExtract program requires the braces for correct extraction.  It would be possible
to simple remove the extra syntax following extraction - but this is not currently
done.

The initial quality level of the current Japanese font is marginal. Attempts to use MS-Mincho
were not successful due to software problems - extracting bitmaps from the font did
not produce characters that look like the ones displayed on-screen in Word.  
 ***13Jan99 update ***: Since it is now possible to edit the BFNs and not lose the edits on
 the next run (vers 2.0), we have corrected many of the worst characters with the help
 of Misao Takeuchi.

In AlarmRender.cc - the GetTok_ method does not allow a "GLUE CHAR" (+) character between
Japanese and non-Japanese characters (which are always surrounded by braces). This
could be corrected with a logic change.

In AlarmRender.cc - the "WithoutWordwrap" routine should be used on the
analysis/remedy messages as well as the base messages.  It was originally
intended to be used on both sides.  Not doing this results in unneccessary
breaking of remedy messages, and significantly reduces readability.  It will
need rework to allow this, because it currently assumes the max # of alarm
phrases is 3, when in reality it can be 6 or more.  Currently it is only used
on base messages, where the number of alarm phrases is always one.  Also - it
should be modified to attempt to pack phrases into a line (not tokens, as in
WithWordwrap).

Also in AlarmRender.cc - there is some code that references the
TextFont::GetDescent method, and attempts to avoid uneven vertical spacing that
occurs when a line happens to have no descenders.  As currently written, this
code assumes that if the style is not Normal, is must be Thin - when in reality
it might be Japanese.  This does not currently cause a problem, but is not very
clean.

Also - The code mentioned above has been removed in the Color Project, and this
will lead to some strange looking alarm messages, especially when 3 lines are
needed (3 lines does not happen in English).

If we run low on memory - some memory could be saved by removing japanese character
data for characters that are not used.  Since all characters are provided at all
point sizes, there are many that are never used.  This is tricky because in some
cases the point size is changed at run time - but a lot of space could be saved.

A change was made to the TextField::renderText_ method to automatically adjust the
position of a message to avoid having the text extend upwards above the
specified y-position of the field.  Therefore, some Japanese messaes may have
incorrect y positions at this time.  This could be corrected by turning this feature
off temporarily and debugging.  Note that a similar automatic adjustment has been
done all along in the horizontal axis.. thus some x positions may be incorrect...


==========================================================================  
===  Lessons from the road travelled ===============

We tried to run JapExtract.exe on another PC, but it asserted (JapExtractDoc.cpp, line 3426,
where the problem was something about nBufSize==1).  We narrowed down the cause of the problem
somewhat: it occurs because of a new Japanese character that we added to a string.  If we take
out that particular character, the program works fine.  We checked that the following font sets
are resident on the PC (MS Mincho, UWJMG3).  However, we have not figured out the underlying
cause of the problem....
 
