#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SensorMediator - Moderates the updating of the sensor values.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class is used to moderate the sampling and the updating of the
//		sensors.  Methods are implemented to sample and update all sensors
//		pointed by the list pointers to their engineering units.
//---------------------------------------------------------------------
//@ Rationale
//      Used to update all the sensors.
//---------------------------------------------------------------------
//@ Implementation-Description
//      When constructed, a list of pointers to all the sensors is defined.
//		The list is defining the order that the sensors will be sampled and
//		updated to engineering units each time newCycle or newSecondaryCycle
//		is invoked.  There is a list for sensors to be sampled every BD cycle
//		and a list for the remaining sensors intended to be sampled periodically
//		at another rate.  The submux sensors are updated every 20 msecs
//		or 4th BD cycle.
//
// $[00401]
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/SensorMediator.ccv   25.0.4.0   19 Nov 2013 13:54:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 010  By: syw     Date:  16-Sep-1997    DR Number: 2427
//  	Project:  Sigma (840)
//		Description:
//			Added requirement traceability.
//
//  Revision: 009 By: syw    Date: 07-Jul-1997   DR Number: 2283
//  	Project:  Sigma (R8027)
//			Sample all flow temperature sensors during 5 ms loop.
//
//  Revision: 008  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 007 By: syw    Date: 22-May-1997   DR Number: DCS 2165
//  	Project:  Sigma (R8027)
//		Description:
//			Removed TI label.
//
//  Revision: 006  By: syw    Date: 28-Apr-1997    DR Number: DCS 1996
//      Project:  Sigma (840)
//        Description:
//          Added requirement tracing.
//
//  Revision: 005 By: syw    Date: 25-Feb-1997   DR Number: DCS 1795
//  	Project:  Sigma (R8027)
//			Eliminate rExhDryFlowSensor from list.
//
//  Revision: 005 By: syw    Date: 25-Feb-1997   DR Number: DCS 1795
//  	Project:  Sigma (R8027)
//			Moved exh flow temperature sensors to 5 ms primary.
//
//  Revision: 004 By: by    Date: 06-Jun-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added new method newSecondaryCycle(). Unload sensors from
//			newCycle to new method.
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Renamed rO2Sensor to rFio2Monitor.  Moved rDacWrap and
//			rSystemBatteryModel to pSubMuxSensors_[].  Update pSubMuxSensors_
//			sensor during newCycle().
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================
#include "SensorMediator.hh"

#include "AdcChannels.hh"
#include "Sensor.hh"
#include "MainSensorRefs.hh"
#include "MiscSensorRefs.hh"
#include "VentObjectRefs.hh"

//@ Usage-Classes

#include "FlowSensor.hh"
#include "ExhFlowSensor.h"
#include "TemperatureSensor.hh"
#include "Barometer.hh"
#include "Fio2Monitor.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SensorMediator()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and returns nothing.
//		It sets up a list of pointers to the sensors that are sampled by
//		this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Initialize the pointers to the sensors.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

SensorMediator::SensorMediator( void)
{
	CALL_TRACE("SensorMediator::SensorMediator( void)") ;


	///////////////////////////////////////////////////////////////////////////
	// Initialize the Primary Sensor Group
	///////////////////////////////////////////////////////////////////////////
	// $[TI1]
   	pPrimarySensorsGroup_[ INSP_PRESSURE_SENSOR ] = (Sensor*)&RInspPressureSensor ;
   	pPrimarySensorsGroup_[ EXH_PRESSURE_SENSOR ] = (Sensor*)&RExhPressureSensor ;
	pPrimarySensorsGroup_[ EXH_DRV_PRESSURE_SENSOR ] = (Sensor*)&RExhDrvPressureSensor ;
  
   	pPrimarySensorsGroup_[ O2_FLOW_SENSOR ] = (Sensor*)&RO2FlowSensor ;
   	pPrimarySensorsGroup_[ AIR_FLOW_SENSOR ] = (Sensor*)&RAirFlowSensor ;
   	pPrimarySensorsGroup_[ EXH_FLOW_SENSOR ] = (Sensor*)&RExhFlowSensor ;
   	pPrimarySensorsGroup_[ O2_TEMP_SENSOR ] = (Sensor*)&RO2TemperatureSensor ;
   	pPrimarySensorsGroup_[ AIR_TEMP_SENSOR ] = (Sensor*)&RAirTemperatureSensor ;
   	
	// TODO E600_LL: to be added when new exh sensor is ready
	//pPrimarySensorsGroup_[ EXH_GAS_TEMP_SENSOR ] = (Sensor*)&RExhGasTemperatureSensor ;
	pPrimarySensorsGroup_[ AIR_PRESS_SENSOR ] = (Sensor*)&RAirInletPressureSensor ;
	pPrimarySensorsGroup_[ O2_PRESS_SENSOR ] = (Sensor*)&RO2InletPressureSensor ;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SensorMediator()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

SensorMediator::~SensorMediator( void)
{
	CALL_TRACE("SensorMediator::~SensorMediator( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//      This method takes no arguments, and updates the raw count and
//      engineering values for each sensor pointed to in the list.  This
//		method returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The ADC channels are sampled.  The raw counts are updated.
//      The engineering values are updated.  AdcChannels::NewCycle() is
//		called to ensure that the acquisition of the raw samples are done
//		in the shortest time possible and to ensure that all the data
//		sampled are from the same batch in time.
// 		$[04331]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SensorMediator::newCycle( void ) const
{
	CALL_TRACE("SensorMediator::newCycle( void )") ;

    // Sample all the channels
    AdcChannels::NewCycle() ;

    // ADC updates must be done before the values are updated.  This
    // ensures that related data (flow/temperature) is used from the
    // same (or adjacent) sample block.
    for ( Uint8 sensorNdx = 0; sensorNdx < MAX_PRIMARY_SENSORS; ++sensorNdx )
    {
        pPrimarySensorsGroup_[ sensorNdx ]->updateValue() ;
    }

	Real32 airFlow = RAirFlowSensor.getValue() ;
	Real32 o2Flow = RO2FlowSensor.getValue() ;
	// TODO E600_LL: to be reviewed when new exh sensor is ready
	//Real32 exhFlow = RExhFlowSensor.getDryValue() ;
	Real32 exhFlow = RExhFlowSensor.getValue() ;

	RAirTemperatureSensor.calculateTau( airFlow) ;
	RO2TemperatureSensor.calculateTau( o2Flow) ;

	// TODO E600_LL: to be reviewed when new exh sensor is ready
	//RExhGasTemperatureSensor.calculateTau( exhFlow) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newSecondaryCycle
//
//@ Interface-Description
//      This method takes no arguments, and updates the raw count and
//      engineering values for each sensor pointed to in the list.  This
//		method returns nothing.  The newCycle() method must be invoked prior
//		to calling this method to ensure that AdcChannels::NewCycle() is
//		called.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The raw counts for each sensor on the secondary list is converted
// 		to engineering value via the updateValue method in Sensor class.  
//		$[00476]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
void
SensorMediator::newSecondaryCycle( void ) const
{
	CALL_TRACE("SensorMediator::secondaryNewCycle( void )") ;

	RAtmosphericPressureSensor.updateValue();
	RFio2Monitor.updateValue();
}

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SensorMediator::SoftFault( const SoftFaultID  softFaultID,
		                   const Uint32       lineNumber,
        		           const char*        pFileName,
                		   const char*        pPredicate)
{
  	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	
 	FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, SENSORMEDIATOR,
					         lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================





