//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file gettimeofday.h
//----------------------------------------------------------------------------
#ifndef GETTIMEOFDAY_H
#define GETTIMEOFDAY_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include < winsock2.h >
#include < time.h     >

struct timespec 
{

    time_t tv_sec;  /* Seconds since 00:00:00 GMT, */
                    /* 1 January 1970 */

    long tv_nsec;   /* Additional nanoseconds since */
                    /* tv_sec */

}; 


int gettimeofday(struct timeval *tv, struct timezone *tz);


#endif
