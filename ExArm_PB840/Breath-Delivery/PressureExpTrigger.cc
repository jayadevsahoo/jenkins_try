#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PressureExpTrigger - Triggers exhalation based on maximum allowed
//    pressure defined in the breath phase.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers exhalation when the measured filtered expiratory
//    pressure exceeds the maximum inspiratory pressure. The maximum 
//    inspiratory pressure is based on the target pressure plus 
//    additional pressure margin to allow for pressure controller 
//    overshoot.  This trigger can be enabled or disabled, 
//    depending upon the current breath phase and the applicability 
//    of the trigger.  When the pressure condition calculated
//    by this trigger is true, the trigger is considered to have fired.
//    This trigger is kept on a list contained in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    This class is necessary to implement the algorithm for pressure
//    triggering an exhalation.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of 
//    whether this trigger is enabled or not.  If the trigger is not 
//    enabled, it will always return a state of false.  The private data 
//    member maxInspiratoryPressure_ stores the computed pressure limit.
//    It is set by the currently active breath phase. The condition 
//    detected by this trigger depends on the circuit pressure 
//    and the computed maxInspiratoryPressure_. 
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    When the active BreathPhase is pressure based, it must set the
//    pressure limit value every cycle.  This limit also needs to be
//    set before activating the trigger, so an old limit is not
//    inadvertently used. Only one instance of this trigger and its 
//    derived class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureExpTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      ATC initial release
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 008  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 007  By:  iv    Date:  23-Dec-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 006  By:  sp    Date:  31-Oct-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 005  By:  sp    Date:  26-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Update unit test. 
//  
//  Revision: 004  By:  sp    Date:  26-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Method getMaxInspiratoryPressure has been move from unit test
//             to public.
//  
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp   Date:  8-Jan-1995    DR Number: DCS 621
//       Project:  Sigma (R8027)
//       Description:
//             Change const PRESSURE_SENSITIVITY to 1.5.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "PressureExpTrigger.hh"

//@ Usage-Classes
#  include "PressureSensor.hh"
 
#  include "FlowController.hh"

#include "PhasedInContextHandle.hh"

#  include "MainSensorRefs.hh"
 
//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureExpTrigger()  
//
//@ Interface-Description
//	Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ using BreathTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureExpTrigger::PressureExpTrigger(const Trigger::TriggerId id) 
: BreathTrigger(id)
{
  CALL_TRACE("PressureExpTrigger()");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureExpTrigger()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureExpTrigger::~PressureExpTrigger(void)
{
  CALL_TRACE("~PressureExpTrigger()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable()
//
//@ Interface-Description
//	This method takes no argument and has no return value. This method
//      enables the trigger and is responsible for the trigger's
//      initialization
//---------------------------------------------------------------------
//@ Implementation-Description
//	The state data member isEnabled_ is set to true, activating this trigger.
//	The data members maxInspiratoryPressure_  and intervalNumber_ are 
//	initialized. maxInspiratoryPressure_ is initialized to 200 such
//	that the trigger conditiion can not trigger when maxInspiratoryPressure_
//	is not set.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
PressureExpTrigger::enable(void)
{
  CALL_TRACE("enable(void)");
 
   // $[TI1]
  isEnabled_ = TRUE;
  maxInspiratoryPressure_ = 200;
  intervalNumber_ = 0;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateTargetPressure
//
//@ Interface-Description
//     The argument targetPressure includes the effective pressure
//     and peep. The argument is then used to calculate
//     the max inpiratory pressure. This method has no return value.
//     This method is used by the current pressure based phase every 
//     BD cycle to set the maximum inspiratory pressure.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Data member maxInspiratoryPressure_ is set to exhPressLimit +
//     targetPressure. In the first 200 msec exhPressLimit is set to 
//     pressMax and after 200 msec exhPressLimit is set to
//     ((pressMax - 3.0) / 80.0) * (intervalNumber - 40).
//     exhPressLimit has minimum value of PRESSURE_SENSITIVITY.
//     $[04017]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
const Real32 PRESSURE_SENSITIVITY = 1.5; //cmH2O
void
PressureExpTrigger::updateTargetPressure(
        const Real32 targetPressure
)
{
  CALL_TRACE("updateTargetPressure(Real32 targetPressure)");
 
  Real32 exhPressLimit;
  Real32 pressMax = MAX_VALUE(8.0F, 0.1F * targetPressure );
  
  if( intervalNumber_ <= 40)
  {
    // $[TI1]
    exhPressLimit = pressMax;
  }
  else
  {
    // $[TI2]
    exhPressLimit = pressMax - ((pressMax - 3.0F) / 80.0F) * (intervalNumber_ - 40);
    exhPressLimit = MAX_VALUE(exhPressLimit, PRESSURE_SENSITIVITY);
  }
 
 
  maxInspiratoryPressure_ = targetPressure + exhPressLimit;

  intervalNumber_++;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PressureExpTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PRESSUREEXPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//     This method takes no parameters.  It returns true if the trigger 
//     condition has occured.  Otherwise, it returns false.  If the
//     pressure measured by the expiratory pressure sensor is greater
//     than or equal to the calculated pressure limit, the trigger
//     condition is met.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The circuit pressure, as measured by the expiratory pressure sensor,
//     compares circuitPressure to maxInspiratoryPressure_ and if it is >= ,
//     the method returns true.
// $[04014] $[04016]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
PressureExpTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

  Boolean rtnValue = FALSE;
  Real32 circuitPressure =  RExhPressureSensor.getFilteredValue();
 
  if ( circuitPressure >= maxInspiratoryPressure_ )
  {
    // $[TI1]
    rtnValue = TRUE;
  }
  // $[TI2]
 
  // TRUE: $[TI3]
  // FALSE: $[TI4]
  return (rtnValue);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

