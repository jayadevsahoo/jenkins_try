
#ifndef BreathItemDataInfo_HH
#define BreathItemDataInfo_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993,1995 Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  BreathItemDataInfo - Breath Data Item processing agent class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/BreathItemDataInfo.hhv   10.7   08/17/07 10:27:24   pvcs  
//
//@ Modification-Log
//
//  Revision: 003  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//  
//  Revision: 002  By:  jhv    Date:  27-Jan-98    DR Number:
//	Project:  BiLevel (R8027)
//	Description:
//		Initial version for Bilevel.
//  
//  Revision: 001  By:  jhv    Date:  26-Jan-94    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include	"DataValue.hh"
#include	"BufferIndex.hh"
#include	"BreathDatum.hh"
#include    "PatientDataId.hh"
#include	"PatientDataClassIds.hh"

class BreathItemDataInfo
{
  //@ Friend: PatientDataMgr.
  // Allows PatientDataMgr class to call the 'modifyData_' private method
  // when it receives new data from the network.
  friend class PatientDataMgr;

  //@ Friend: PEEPDataInfo.
  // Allows PEEPDataInfo class to call the 'modifyData_' private method on
  // other data agent objects.
  friend class PEEPDataInfo;

  //@ Friend: InspPauseDataInfo.
  // Allows InspPauseDataInfo class to call the 'modifyData_' private method on
  // other data agent objects.
  friend class InspPauseDataInfo;

  public:
    BreathItemDataInfo(const PatientDataId::PatientItemId id);
    virtual ~BreathItemDataInfo(void);

    inline const BoundedValue &getBoundedValue(void);
    inline const BoundedBreathDatum  &getBreathDatum(void);
    inline DiscreteBreathDatum  &getDiscreteDatum(void);

    // MRI compiler generates annoying warning messages when this inline method
    // code is defined separately from the actual method declaration.
    inline DiscreteValue getDiscreteValue(void);
    inline PatientDataId::PatientItemId getDataId(void) const; 

    virtual void setDataTimeout(void) = 0;
    inline  Boolean isActive(void) const;
    inline Boolean  isBounded(void) const;
    inline Boolean  isDiscrete(void) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
						   const Uint32      lineNumber, 
						   const char*       pFileName = NULL, 
						   const char*       pBoolTest = NULL); 

  protected:
    virtual	void modifyData_( void *)= 0;

    //@ data-member: data_[2]
    // Two-dimensional breath data buffer to hold breath data.
    BreathDatum data_[2];

    //@ Data-Member:   index_
    //  index_.network indexes into one of the dual data buffer that
    //  new data from the network is put in.
    //  index_.active indexes into the dual data buffer that holds
    //  the most current data.
    BufferIndex	index_;


    //@ Data-Member:   DATA_ID_
    // A constant, a unigue patient item id that identifies a breath
    // data item type that this object is managing.
    const PatientDataId::PatientItemId DATA_ID_;

  private:
    BreathItemDataInfo(void); 		// not implemented.
    BreathItemDataInfo(const BreathItemDataInfo& range); // not implemented ...
    void  operator=(const BreathItemDataInfo&); // not implemented...

};

// Inlined methods...
#include "BreathItemDataInfo.in"

#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
#endif  // BreathItemDataInfo_HH

