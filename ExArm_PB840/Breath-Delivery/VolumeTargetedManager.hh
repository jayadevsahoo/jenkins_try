#ifndef VolumeTargetedManager_HH
#define VolumeTargetedManager_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VolumeTargetedManager - Central location to manage volume targeted
//					related data required for Vtpcv or Vsv phases.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/VolumeTargetedManager.hhv   10.7   08/17/07 09:45:42   pvcs  
//
//@ Modification-Log
//
//  Revision: 004  By:  gfu    Date:  06-May-2002    DR Number: 6006
//  Project:  VCP
//  Description:
//	Created resetAlorithm as calling class' reset method when resetting the algorithm based upon 
//	Hips, Vti alarms, etc. caused too many variables to reset resulting in two test breaths as the algorithm
//	was actualy reset twice.
//		
//  Revision: 003  By:  yakovb Date:  22-Mar-2002    DR Number: 5790
//  Project:  VCP
//  Description:
//		Added reset algorithm by VTi alarm during volume targeted breaths.
//
//  Revision: 002  By:  quf    Date:  24-Jan-2001    DR Number: 5790
//  Project:  VTPC
//  Description:
//		Added methods and data members to support blocked wye
//		handling during volume targeted breaths.
//		Fixed comment for isVolumeTargetedBreath_.
//
//  Revision: 001  By:  syw    Date:  03-Sep-2000    DR Number: 5755
//       Project:  VTPC
//       Description:
//             Initial version
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "BreathPhase.hh"
#include "Trigger.hh"
#include "SettingId.hh"

//@ End-Usage

const Real32 MIN_DELTA_P = 1.5;
const Real32 MIN_LUNG_VOLUME = 0.0;


class VolumeTargetedManager {
  public:
	enum TestBreathType {VC_BASED, PC_BASED, VT_BASED, PS_BASED, VS_BASED} ;
	
    VolumeTargetedManager( void) ;
    ~VolumeTargetedManager( void) ;

	inline void setBeginPlateauPress( const Real32 pressure) ;
	inline void setEndPlateauPress( const Real32 pressure) ;
	inline Real32 getDeltaPLimited( void) const;
	inline Real32 getEndInspPress( void) const ;
	inline Real32 getEndExhPress( void) const ;
	inline Real32 getInspLungVolumeLimited( void) const ;
	inline Real32 getExhLungVolume( void) const ;
	inline Real32 getMinPatientCompliance( void) const;
	inline Real32 getMaxPatientCompliance( void) const;
	inline VolumeTargetedManager::TestBreathType getTestBreathState( void) const ;	
	inline Boolean getVtiLastBreath( void) const;
	void reset( void) ;
	void updateData( const DiscreteValue mandType,
					 const DiscreteValue supportType) ;
	
	void determineBreathType( BreathPhase* &pBreathPhase, DiscreteValue &mandatoryType) ;
	void setExhalationTrigger( const Trigger::TriggerId triggerId, const BreathPhase* pBreathPhase) ;
	
    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;

  protected:

  private:
    VolumeTargetedManager( const VolumeTargetedManager&) ;  	// not implemented...
    void   operator=( const VolumeTargetedManager&) ; 			// not implemented...

	Boolean neoComplianceIncreaseExcessive_( Real32 patientCompliance) const;
	void calcMinAndMaxPatientCompliance_( Real32 ibw);

	void resetAlgorithm (void) ;

	//@ Data-Member: testBreathState_
	// breath type to deliver
	TestBreathType testBreathState_ ;

	//@ Data-Member: volumeTestBreathCounter_
	// counts number of volume test breaths
	Uint32 volumeTestBreathCounter_ ;

	//@ Data-Member:::TriggerId exhTriggerId_
	// trigger id that caused exhalation to occur
	Trigger::TriggerId exhTriggerId_ ;

	//@ Data-Member: beginPlateauPress_
	// pressure at the beginning of plateau
	Real32 beginPlateauPress_ ;

	//@ Data-Member: endPlateauPress_
	// pressure at the end of plateau
	Real32 endPlateauPress_ ;

	//@ Data-Member: hcpCounter_
	// number of high circuit pressure events that have occurred
	Uint32 hcpCounter_ ;

	//@ Data-Member: hvtiCounter_
	// number of high VTi alarm events that have occurred
	Uint32 hvtiCounter_ ;

	//@ Data-Member: supportType_
	// support type setting value
	DiscreteValue supportType_ ;

	//@ Data-Member: mandType_
	// mandatory type setting value
	DiscreteValue mandType_ ;

	//@ Data-Member: isVolumeTargetedBreath_
	// flag to indicate if breath is Vtpcv or Vsv or test breath
	Boolean isVolumeTargetedBreath_ ;

	//@ Data-Member: endInspPress_
	// pressure at the end of inspiration
	Real32 endInspPress_ ;

	//@ Data-Member: endExhPress_
	// pressure at the end of exhalation
	Real32 endExhPress_ ;

	//@ Data-Member: inspLungVolume_
	// inspired lung volume
	Real32 inspLungVolume_ ;

	//@ Data-Member: exhLungVolume_
	// expired lung volume
	Real32 exhLungVolume_ ;

	//@ Data-Member: minPatientCompliance_
	// minimum patient compliance
	Real32 minPatientCompliance_ ;

	//@ Data-Member: maxPatientCompliance_
	// maximum patient compliance
	Real32 maxPatientCompliance_ ;

	//@ Data-Member: vtiLastBreath_
	// flag indicating Vti alarm last breath
	Boolean vtiLastBreath_ ;
} ;

// Inlined methods
#include "VolumeTargetedManager.in"

#endif // VolumeTargetedManager_HH 

