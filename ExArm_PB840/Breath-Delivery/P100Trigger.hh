#ifndef P100Trigger_HH
#define P100Trigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: P100Trigger - Trigger to enter P100 breath phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/P100Trigger.hhv   25.0.4.0   19 Nov 2013 13:59:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//=====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"

//@ Usage-Classes

#  include "PatientTrigger.hh"

//@ End-Usage


class P100Trigger : public PatientTrigger 
{
public:
	P100Trigger(void);
	virtual ~P100Trigger(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	virtual void enable(void);

protected:
	virtual Boolean triggerCondition_(void);

private:
	P100Trigger(const P100Trigger&);	// not implemented...
	void   operator=(const P100Trigger&); // not implemented...

	//@ Data-Member: prevNetFlow_
	// Net flow from the previous execution cycle
	Real32 prevNetFlow_;

	//@ Data-Member: flowThreshold_
	// The flow threshold trigger value for triggering the P100 occlusion
	// negative values represent exhalation flow
	Real32 flowThreshold_;

	//@ Data-Member: elapsedTime_
	// Time elapsed since this trigger was enabled (ms)
	Int32  elapsedTime_; 

};

#endif // P100Trigger_HH 
