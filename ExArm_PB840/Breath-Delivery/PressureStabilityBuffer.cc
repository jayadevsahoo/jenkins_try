#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PressureStabilityBuffer - class to check the stability of 
//  pressure data during insp/exp pause maneuvers.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for checking the stablity of the data buffer.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates stability checking within it.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class is derived from AveragedStabilityBuffer. It implements
// additionaly functionality with calculatePressureStability().
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// n/a
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureStabilityBuffer.ccv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial implementation. Moved pressure stability functions out
//      of Maneuver class into this new class so RM maneuvers are
//		burdened with this unrelated functionality.
//
//=====================================================================

#include "PressureStabilityBuffer.hh"
#include "BreathRecord.hh"

//@ Usage-Classes
#include <math.h>

//@ End-Usage

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureStabilityBuffer()  [Default Contstructor]
//
//@ Interface-Description
// The constructor takes a pointer to a pre-allocated data buffer, and
// the size of this buffer.  The base class constructor is called with 
// these arguments.
//---------------------------------------------------------------------
//@ Im-Description
//        none
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
PressureStabilityBuffer::PressureStabilityBuffer(Real32 *pBuffer, Uint32 bufferSize) :
	AveragedStabilityBuffer(pBuffer, bufferSize),
	sCurrent_(0.0),
	rCurrent_(0.0),
	rCurrent_1_(0.0),
	rCurrent_2_(0.0)
{
    CALL_TRACE("PressureStabilityBuffer::PressureStabilityBuffer(void)");

    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureStabilityBuffer()  [Destructor]
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
PressureStabilityBuffer::~PressureStabilityBuffer(void)
{
	CALL_TRACE("PressureStabilityBuffer::~PressureStabilityBuffer(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculatePressureStability
//
//@ Interface-Description
// The method takes no arguments and returns stability status for the
// pressureStabilityBuffer_.
//---------------------------------------------------------------------
//@ Implementation-Description
// Based on the control algorithm to calculate sCurrent_ and rCurrent_
// values.  When the value of rCurrent_ is less than 1.0 and the
// pressureStabilityBuffer_ is stabilized then we return PauseTypes::STABLE
// else return PauseTypes::UNSTABLE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PauseTypes::PpiState
PressureStabilityBuffer::calculatePressureStability(void)
{
	CALL_TRACE("Maneuver::calculatePressureStability(void)");

	PauseTypes::PpiState isPressureStable = PauseTypes::UNSTABLE;
	
	sCurrent_ = -3.4064F * (BreathRecord::GetAirwayPressure() - pCurrent_1_) +
				1.739F * rCurrent_1_ -
				0.7561F * rCurrent_2_;

	Real32 temp = MAX_VALUE(sCurrent_, 0);
	rCurrent_ = MIN_VALUE(temp, 1000);

	// Criteria to determine the pressure stability
	if (rCurrent_ < 1.0)
	{
		// $[BL04058] :a criteria for pressure stability
									// $[TI1]
		// if AveragedStabilityBuffer::isStable
		if (isStable())
		{
									// $[TI1.1]
			isPressureStable = PauseTypes::STABLE;
		}
									// $[TI1.2]
	}
									// $[TI2]
	// Reset the variables with current readings ready for next
	// set calculations.
	rCurrent_2_ = rCurrent_1_;
	rCurrent_1_ = rCurrent_;
	pCurrent_1_ = BreathRecord::GetAirwayPressure();

	return(isPressureStable);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reset
//
//@ Interface-Description
// The method takes no arguments and returns nothing. It resets this
// object to its initial state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureStabilityBuffer::reset(void)
{
	SummationBuffer::reset();
	sCurrent_ = 0;
	rCurrent_ = 0;
	rCurrent_1_ = 0;
	rCurrent_2_ = 0;
	pCurrent_1_ = 0;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
PressureStabilityBuffer::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PRESSURESTABILITYBUFFER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
