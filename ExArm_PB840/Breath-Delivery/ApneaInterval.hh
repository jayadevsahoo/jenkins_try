
#ifndef ApneaInterval_HH
#define ApneaInterval_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ApneaInterval - manages changes to the actual apnea interval.
//---------------------------------------------------------------------
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ApneaInterval.hhv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By:  yyy    Date: 06-Jan-1998     DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//          Initial release for Bilevel.
//
//  Revision: 004  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal Breath Delivery code review.
//
//  Revision: 003 By:  iv   Date:   09-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             The Boolean noInspiration argument was eliminated from
//             newExhalation_().
//             Eliminated unused methods: setElapsedInterval(),
//             checkForApneaCondition().
//
//  Revision: 002 By:  iv   Date:   15-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "TimersRefs.hh"

//@ Usage-Classes
#  include "IntervalTimer.hh"
#  include "BreathPhaseType.hh"
//@ End-Usage


class ApneaInterval
{
  public:
    ApneaInterval(void);
    ~ApneaInterval(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

    void newPhase(const BreathPhaseType::PhaseType phase);
	void newCycle(void);
	inline void extendInterval(const Int32 intervalDuration);
	void extendIntervalForModeChange(void);
	void extendIntervalForPeepReduction(void);
	inline Int32 getApneaInterval(void) const;

  protected:

  private:
    ApneaInterval(const ApneaInterval&);		// not implemented...
    void   operator=(const ApneaInterval&);	// not implemented...

    void newInspiration_(void);
    void newExhalation_(void);

	//@ Data-Member:  DELTA_
	// The fixed incremental value added to the set apnea interval
	static const Int32 DELTA_;

    //@ Data-Member:  actualApneaInterval_
	// The apnea interval in effect.
    Int32  actualApneaInterval_;

	//@ Data-Member:  isModeChangeRequest_
	// flags a mode change that happened during the preceding inspiration
	Boolean isModeChangeRequest_;

	//@ Data-Member:  isApneaDetectionOff_
	// TRUE when the apnea interval is turned OFF (during CPAP)
	Boolean isApneaDetectionOff_;
};


// Inlined methods...
#include "ApneaInterval.in"


#endif // ApneaInterval_HH 
