#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SettingContextHandle -  Handle to the Setting Contexts.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides an external interface to the Settings-Validation
//  Subsystem, for the functionality relating to the Accepted, Adjusted,
//  Recoverable and Pending Contexts.  This handle class provides the
//  interface needed to conduct the adjustment and acceptance of all of
//  the settings, and the recovery of the previous Vent-Setup  batch
//  settings.
//
//  The purpose of this class is to provide an interface for all of the
//  other subsystems, that need to interact with the setting contexts.
//  This class provides a clear demarcation line between the functionality
//  of this subsystem's internal classes, and the functionality needed by
//  other subsystems.  This class also activates the context callbacks,
//  based on the modifications that may have occured with each of its
//  methods.
//
//  This class is only available on the GUI CPU.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a clear border between the Settings-Validation
//  Subsystem, and the subsystems that need functionality relating to the
//  setting contexts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All of the interface methods are set up as static, non-inline methods.
//  The fact that the methods are static, allows the other subsystems to
//  have access to the needed functionality, without needing access to an
//  instance of the class; direct calls to these methods could be made.
//  By making all of the methods non-inline, the client's dependency on
//  the classes and types needed for the implementation only is eliminated.
//  In other words, the non-inline methods allow for the header files needed
//  for the implementation to be included in the source file and NOT the
//  header file.
//
//  None of these methods are set-up for multiple thread capability.
//  All of these methods are for the GUI task only (which Settings-Validation
//  is a part of), and therefore all interaction with this handle is through
//  a single thread of control.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingContextHandle.ccv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 010   By: sah  Date:  2-Feb-2000    DR Number:  5327
//  Project:  NeoMode
//  Description:
//  Updated for NeoMode
//
//  Revision: 009   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'GetSettingValue()' and 'GetSettingApplicability()'
//	   methods
//	*  removed all obsoleted callback manager implementation
//
//  Revision: 008   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 007   By: sah   Date:  16-Dec-1998    DR Number: 5292
//  Project:  BILEVEL-2
//  Description:
//     Added new 'IsSettingApplicable()' method.
//
//  Revision: 006   By: sah  Date:  23-Nov-1998    DR Number: 5238
//  Project:  BILEVEL
//  Description:
//	Added the clearing of all registered changes (for Accepted
//	Context, only), before processing the acceptance of any batch
//	changes.  This resets any flags from non-batch settings whose
//	callbacks were not activated because of being limited by a
//	bound.
//
//  Revision: 005   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 004   By: sah    Date: 19-Dec-1997    DR Number: 2700
//  Project: Sigma (R8027)
//  Description:
//	Fixed problem where GUI-Apps wasn't being notified of FiO2
//	setting changes due to a Same-Patient Setup.
//  
//  Revision: 003   By: sah    Date: 29-Sep-1997    DR Number: 2410
//  Project: Sigma (R8027)
//  Description:
//	Added new interface methods for the adjustment/acceptance of
//	the new Atmospheric Pressure Setting.
//  
//  Revision: 002   By: sah    Date: 07 Feb 1997    DR Number: 1730
//  Project: Sigma (R8027)
//  Description:
//	Provided method for Sys-Init to query whether a Vent-Startup
//	(i.e., New- or Same-Patient Setup) has completed.  The return
//	value of this new method will be used to help determine whether
//	the state of the BD settings is out-of-sync with the GUI settings.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SettingContextHandle.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "AdjustedContext.hh"
#include "RecoverableContext.hh"
#include "SettingsMgr.hh"
#include "Setting.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSettingValue(contextId, settingId)  [static]
//
//@ Interface-Description
//  Return the value of the setting given by 'settingId', from the context
//  indicated by 'contextId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the indicated setting.
//---------------------------------------------------------------------
//@ PreCondition
//  ((contextId == ContextId::ADJUSTED_CONTEXT_ID  ||
//    contextId == ContextId::ACCEPTED_CONTEXT_ID)  &&
//   SettingId::IsId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
SettingContextHandle::GetSettingValue(const ContextId::ContextIdType contextId,
				      const SettingId::SettingIdType settingId)
{
  CALL_TRACE("GetSettingValue(contextId, settingId)");

  const Setting*  pSetting = SettingsMgr::GetSettingPtr(settingId);

  SettingValue  settingValue;

  switch (contextId)
  {
  case ContextId::ACCEPTED_CONTEXT_ID :			// $[TI1]
    settingValue = pSetting->getAcceptedValue();
    break;
  case ContextId::ADJUSTED_CONTEXT_ID :			// $[TI2]
    settingValue = pSetting->getAdjustedValue();
    break;
  default :
    // unexpected context id...
    AUX_CLASS_ASSERTION_FAILURE(contextId);
    break;
  }

  return(settingValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSettingApplicability(contextId, settingId)  [static]
//
//@ Interface-Description
//  Return the applicability state of the setting indicated by 'settingId',
//  with respect to the context indicated by 'contextId'.  For example,
//  inspiratory pressure is NOT applicable while mandatory type is VC.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request on to the indicated setting.
//---------------------------------------------------------------------
//@ PreCondition
//  ((contextId == ContextId::ADJUSTED_CONTEXT_ID  ||
//    contextId == ContextId::ACCEPTED_CONTEXT_ID)  &&
//   SettingId::IsId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
SettingContextHandle::GetSettingApplicability(
				const ContextId::ContextIdType contextId,
				const SettingId::SettingIdType settingId
					     )
{
  CALL_TRACE("GetSettingApplicability(contextId, settingId)");

  const Setting*  pSetting = SettingsMgr::GetSettingPtr(settingId);

  Notification::ChangeQualifier  qualifierId;

  switch (contextId)
  {
  case ContextId::ACCEPTED_CONTEXT_ID :		// $[TI1]
    qualifierId = Notification::ACCEPTED;
    break;
  case ContextId::ADJUSTED_CONTEXT_ID :		// $[TI2]
    qualifierId = Notification::ADJUSTED;
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(contextId);
    break;
  }

  return(pSetting->getApplicability(qualifierId));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsSamePatientValid()  [static]
//
//@ Interface-Description
//  Is there a "previous" patient available for use for startup?  This
//  indicates whether a Same-Patient Setup can be offered to the operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SettingContextHandle::IsSamePatientValid(void)
{
  CALL_TRACE("IsSamePatientValid()");

  return(ContextMgr::GetAcceptedContext().arePatientSettingsAvailable());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  HasVentStartupCompleted()  [static]
//
//@ Interface-Description
//  This method indicates whether a (required) Vent-Startup Sequence (i.e.,
//  New- or Same-Patient Setup) has completed since the last power cycle.
//  This is used to determine some Task-Control states during the startup
//  sequence.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If, according to the Accepted Context, the BD is NOT in a Safety-PCV
//  state, then a required Vent-Startup Sequence has already completed.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SettingContextHandle::HasVentStartupCompleted(void)
{
  CALL_TRACE("HasVentStartupCompleted()");

  return(!ContextMgr::GetAcceptedContext().isBdInSafetyPcvState());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdjustNewPatientBatch(phase)  [static]
//
//@ Interface-Description
//  Prepare the Adjusted Context for phase number 'phase' of the
//  New-Patient Setup.  This method allows the Adjusted Context to
//  initialize its settings based on the phase of the New-Patient
//  Setup that is being entered.  When 'phase' is 'IBW_ADJUST_PHASE',
//  only IBW is initialized and allowed to be adjusted.  When 'phase' is
//  'MAIN_CONTROL_ADJUST_PHASE', only the Main Control Settings (i.e.,
//  Mode, Mandatory Type, Trigger Type and Support Type) are initialized and
//  allowed to be adjusted.  Finally, when 'phase' is set to
//  'BREATH_SETTINGS_ADJUST_PHASE', all other batch settings are initialized
//  (often, based on IBW and the Main Control Settings) and all non-apnea
//  batch settings are allowed to be adjusted.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  (phase == IBW_ADJUST_PHASE           ||
//   phase == MAIN_CONTROL_ADJUST_PHASE  ||
//   phase == BREATH_SETTINGS_ADJUST_PHASE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AdjustNewPatientBatch(const BatchAdjustPhase phase)
{
  CALL_TRACE("AdjustNewPatientBatch(phase)");
  SAFE_CLASS_PRE_CONDITION((phase == IBW_ADJUST_PHASE           ||
			    phase == MAIN_CONTROL_ADJUST_PHASE  ||
			    phase == BREATH_SETTINGS_ADJUST_PHASE));

  // forward request to Adjusted Context...
  ContextMgr::GetAdjustedContext().adjustNewPatientBatch(phase);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptNewPatientBatch()  [static]
//
//@ Interface-Description
//  Accept the new-patient setting adjustments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AcceptNewPatientBatch(void)
{
  CALL_TRACE("AcceptNewPatientBatch()");

  // get an alias to the Accepted Context...
  AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

  // forward the request to the Accepted Context...
  rAccContext.acceptNewPatientBatch();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptSamePatientBatch()  [static]
//
//@ Interface-Description
//  Accept the same-patient setting adjustments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AcceptSamePatientBatch(void)
{
  CALL_TRACE("AcceptSamePatientBatch()");

  // forward the request to the Accepted Context...
  ContextMgr::GetAcceptedContext().acceptSamePatientBatch();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdjustVentSetupBatch(phase)  [static]
//
//@ Interface-Description
//  Prepare the Adjusted Context for phase number 'phase' of Vent Setup.
//  When 'phase' is 'MAIN_CONTROL_ADJUST_PHASE', only the Main Control
//  Settings (i.e., Mode, Mandatory Type, Trigger Type and Support Type)
//  are allowed to be adjusted.  When 'phase' is set to
//  'BREATH_SETTINGS_ADJUST_PHASE', all other non-apnea, batch settings
//  allowed to be adjusted.  Given changes of the Main Control Settings, the
//  specified Transition Rules will run upon entry into the
//  'BREATH_SETTINGS_ADJUST_PHASE' phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  (phase == MAIN_CONTROL_ADJUST_PHASE  ||
//   phase == BREATH_SETTINGS_ADJUST_PHASE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AdjustVentSetupBatch(const BatchAdjustPhase phase)
{
  CALL_TRACE("AdjustVentSetupBatch(phase)");
  SAFE_CLASS_PRE_CONDITION((phase == MAIN_CONTROL_ADJUST_PHASE  ||
			    phase == BREATH_SETTINGS_ADJUST_PHASE));

  // forward request to Adjusted Context...
  ContextMgr::GetAdjustedContext().adjustVentSetupBatch(phase);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptVentSetupBatch()  [static]
//
//@ Interface-Description
//  Accept the Vent-Setup setting adjustments.  This will correct any
//  invalid apnea settings, when apnea is possible.   A status, indicating
//  which apnea settings were corrected, is returned.  The currently accepted
//  batch settings are copied away for possible "recovery" at a later time.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (SettingContextHandle::IsRecoverableBatchValid())
//@ End-Method
//=====================================================================

ApneaCorrectionStatus
SettingContextHandle::AcceptVentSetupBatch(void)
{
  CALL_TRACE("AcceptVentSetupBatch()");

  // get an alias to the Accepted Context...
  AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

  ApneaCorrectionStatus  correctionStatus;

  // forward the request to the Accepted Context...
  correctionStatus = rAccContext.acceptVentSetupBatch();

  return(correctionStatus);   // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdjustApneaSetupBatch(phase, isReEntry)  [static]
//
//@ Interface-Description
//  Prepare the Adjusted Context for phase number 'phase' of Apnea Setup.
//  This will correct any invalid apnea settings, whether apnea is possible,
//  or not.  A status, indicating which apnea settings were corrected, is
//  returned.  When 'phase' is 'MAIN_CONTROL_ADJUST_PHASE', only the Apnea
//  Main Control Setting (i.e., Apnea Mandatory Type) is allowed to be
//  adjusted.  When 'phase' is set to 'BREATH_SETTINGS_ADJUST_PHASE', all
//  other apnea, batch settings allowed to be adjusted.  Given changes of
//  the Apnea Main Control Setting, the specified Transition Rule will run
//  upon entry into the 'BREATH_SETTINGS_ADJUST_PHASE' phase.
//
//  If 'phase' indicates an entry into a "breath settings" phase, 'isReEntry'
//  must indicate whether is an initial entry or re-entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  (phase == ::MAIN_CONTROL_ADJUST_PHASE  ||
//   phase == ::BREATH_SETTINGS_ADJUST_PHASE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaCorrectionStatus
SettingContextHandle::AdjustApneaSetupBatch(const BatchAdjustPhase phase,
					    const Boolean          isReEntry)
{
  CALL_TRACE("AdjustApneaSetupBatch(phase, isReEntry)");
  SAFE_CLASS_PRE_CONDITION((phase == ::MAIN_CONTROL_ADJUST_PHASE  ||
			    phase == ::BREATH_SETTINGS_ADJUST_PHASE));

  // get an alias to the Adjusted Context...
  AdjustedContext&  rAdjContext = ContextMgr::GetAdjustedContext();

  ApneaCorrectionStatus  correctionStatus;

  // forward request to Adjusted Context...
  correctionStatus = rAdjContext.adjustApneaSetupBatch(phase, isReEntry);

  return(correctionStatus);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptApneaSetupBatch()  [static]
//
//@ Interface-Description
//  Accept the Apnea-Setup setting adjustments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AcceptApneaSetupBatch(void)
{
  CALL_TRACE("AcceptApneaSetupBatch()");

  // forward the request to the Accepted Context...
  ContextMgr::GetAcceptedContext().acceptApneaSetupBatch();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdjustBatch()  [static]
//
//@ Interface-Description
//  Generic adjustment of the main settings.  This method initiates the
//  initialization of the Adjusted Context, based on the current state
//  of the Accepted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AdjustBatch(void)
{
  CALL_TRACE("AdjustBatch()");

  // forward request to Adjusted Context...
  ContextMgr::GetAdjustedContext().adjustGenericBatch();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptBatch()  [static]
//
//@ Interface-Description
//  Accept the setting adjustments.  This will correct any invalid apnea
//  settings, when apnea is possible.  A status, indicating which apnea
//  settings were corrected, is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaCorrectionStatus
SettingContextHandle::AcceptBatch(void)
{
  CALL_TRACE("AcceptBatch()");

  // get an alias to the Accepted Context...
  AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

  ApneaCorrectionStatus  correctionStatus;

  // forward the request to the Accepted Context...
  correctionStatus = rAccContext.acceptGenericBatch();

  return(correctionStatus);   // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdjustSstBatch()  [static]
//
//@ Interface-Description
//  Prepare the Adjusted Context for adjustment of an SST Setup batch.
//  This method initiates the initialization of the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AdjustSstBatch(void)
{
  CALL_TRACE("AdjustSstBatch()");

  // forward request to Adjusted Context...
  ContextMgr::GetAdjustedContext().adjustSstBatch();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptSstBatch()  [static]
//
//@ Interface-Description
//  Accept the SST Setup setting adjustments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AcceptSstBatch(void)
{
  CALL_TRACE("AcceptSstBatch()");

  // forward the request to the Accepted Context...
  ContextMgr::GetAcceptedContext().acceptSstBatch();
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdjustTimeDateBatch()  [static]
//
//@ Interface-Description
//  Prepare the Adjusted Context for adjustment of a Time/Date Setup batch.
//  This method initiates the initialization of the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AdjustTimeDateBatch(void)
{
  CALL_TRACE("AdjustTimeDateBatch()");

  // forward request to Adjusted Context...
  ContextMgr::GetAdjustedContext().adjustTimeDateBatch();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptTimeDateBatch()  [static]
//
//@ Interface-Description
//  Accept the Time/Date Setup setting adjustments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AcceptTimeDateBatch(void)
{
  CALL_TRACE("AcceptTimeDateBatch()");

  // forward the request to the Accepted Context...
  ContextMgr::GetAcceptedContext().acceptTimeDateBatch();
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdjustDciSetupBatch()  [static]
//
//@ Interface-Description
//  Prepare the Adjusted Context for adjustment of the DCI Setup batch.
//  This method initiates the initialization of the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AdjustDciSetupBatch(void)
{
  CALL_TRACE("AdjustDciSetupBatch()");

  // forward request to Adjusted Context...
  ContextMgr::GetAdjustedContext().adjustDciSetupBatch();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptDciSetupBatch()  [static]
//
//@ Interface-Description
//  Accept the DCI Setup setting adjustments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AcceptDciSetupBatch(void)
{
  CALL_TRACE("AcceptDciSetupBatch()");

  // forward the request to the Accepted Context...
  ContextMgr::GetAcceptedContext().acceptDciSetupBatch();
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdjustServiceSetupBatch()  [static]
//
//@ Interface-Description
//  Prepare the Adjusted Context for adjustment of the Service-Mode Setup
//  batch.  This method initiates the initialization of the Adjusted
//  Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AdjustServiceSetupBatch(void)
{
  CALL_TRACE("AdjustServiceSetupBatch()");

  // forward request to Adjusted Context...
  ContextMgr::GetAdjustedContext().adjustServiceSetupBatch();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptServiceSetupBatch()  [static]
//
//@ Interface-Description
//  Accept the Service-Mode Setup setting adjustments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AcceptServiceSetupBatch(void)
{
  CALL_TRACE("AcceptServiceSetupBatch()");

  // forward the request to the Accepted Context...
  ContextMgr::GetAcceptedContext().acceptServiceSetupBatch();
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdjustAtmPressureCal(atmPressureValue)  [static]
//
//@ Interface-Description
//  Prepare the Adjusted Context for adjustment of the Atmospheric
//  Pressure Setting Calibration.  This method initiates the initialization
//  of the Adjusted Context.  The passed parameter, 'atmPressureValue', is
//  used to initialize the Atm. Pressure Setting to the current sensor
//  reading.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AdjustAtmPressureCal(const Real32 atmPressureValue)
{
  CALL_TRACE("AdjustAtmPressureCal(atmPressureValue)");

  // forward request to Adjusted Context...
  ContextMgr::GetAdjustedContext().adjustAtmPressureCal(atmPressureValue);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptAtmPressureCal()  [static]
//
//@ Interface-Description
//  Accept the Service-Mode Setup setting adjustments.
//  Accept the Atmospheric Pressure Setting Calibration adjustments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Accepted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::AcceptAtmPressureCal(void)
{
  CALL_TRACE("AcceptAtmPressureCal()");

  // forward the request to the Accepted Context...
  ContextMgr::GetAcceptedContext().acceptAtmPressureCal();
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsRecoverableBatchValid()  [static]
//
//@ Interface-Description
//  Are the stored settings in a valid state?  In other words, has there
//  been a storage, without a recover?
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request on to the Recoverable Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SettingContextHandle::IsRecoverableBatchValid(void)
{
  CALL_TRACE("IsRecoverableBatchValid()");

  return(ContextMgr::GetRecoverableContext().isValid());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RecoverStoredBatch()  [static]
//
//@ Interface-Description
//  Recover the accepted settings that have been stored from the previous
//  Vent-Setup.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingContextHandle::IsRecoverableSettingsValid())
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::RecoverStoredBatch(void)
{
  CALL_TRACE("RecoverStoredBatch()");
  CLASS_PRE_CONDITION((SettingContextHandle::IsRecoverableBatchValid()));

  ContextMgr::GetAdjustedContext().recoverStoredBatch();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  InvalidateRecoverableBatch()  [static]
//
//@ Interface-Description
//  Invalidate the recoverable context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request on to the Recoverable Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (!SettingContextHandle::IsRecoverableBatchValid())
//@ End-Method
//=====================================================================

void
SettingContextHandle::InvalidateRecoverableBatch(void)
{
  CALL_TRACE("InvalidateRecoverableBatch()");

  ContextMgr::GetRecoverableContext().invalidate();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AreAnyBatchSettingsChanged()  [static]
//
//@ Interface-Description
//  Have any of the proposed batch settings changed, with respect to their
//  accepted values?
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this request on to the Adjusted Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SettingContextHandle::AreAnyBatchSettingsChanged(void)
{
  CALL_TRACE("AreAnyBatchSettingsChanged()");

  return(ContextMgr::GetAdjustedContext().areAnyBatchSettingsChanged());
}  // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingContextHandle::SoftFault(const SoftFaultID  softFaultID,
			        const Uint32       lineNumber,
			        const char*        pFileName,
			        const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  SETTING_CONTEXT_HANDLE, lineNumber, pFileName,
			  pPredicate);
}
