#ifndef	BdSerialInterface_HH
#define BdSerialInterface_HH

//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2008, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Class:  BdSerialInterface - Serial Data Interface for the BD board
//---------------------------------------------------------------------
//@ Version-Information
//@(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BdSerialInterface.hhv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $  
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  25-APR-2008    SCR Number: XXXX
//  Project:  PROX
//  Description:
//	Initial version.
//=====================================================================

#include "BD_IO_Devices.hh"
#include "SerialPort.hh"


//@ Usage-Classes
//@ End-Usage

class SerialPort;
class BdSerialPort;
class MsgQueue;

class BdSerialInterface 
{
  public:

	//@ Type: SerialCommandCodes
	//  application originated commands to BdSerialInterface::Task
	enum SerialCommandCodes
	{
		SERIAL_OUTPUT_AVAILABLE
	};

	//@ Type: PortNum
	//  Enumerators for the three serial ports.
	enum PortNum
	{
		SERIAL_PORT_NULL = -1,
		SERIAL_PORT_1 = 1,
		SERIAL_PORT_2 = 2,
		MAX_NUM_SERIAL_PORTS
	};

	static  BdSerialInterface* PBdSerialInterface;

	BdSerialInterface( SerialPort & rSerialPort, 
            BdSerialInterface::PortNum portNum );

	virtual ~BdSerialInterface();

	virtual Uint write(const void * pBuffer, Uint count, Uint milliseconds = 0);
	virtual Uint read( void * pBuffer, Uint count, Uint milliseconds = 0);
	virtual Uint32 bytesInReceiveBuffer(void);
	virtual Boolean SetCommunicationBaudRate(Uint32 BaudRate);
	virtual void enableLoopback(void);
	virtual void disableLoopback(void);
	virtual void enableExternalLoopback(void);
	virtual void disableExternalLoopback(void);
	virtual void start(void);
    virtual void purgeline(void);
	virtual void newCycle(void) = 0;



	static  void  Initialize(void);
	static  BdSerialPort& GetAuxSerialPort(BdSerialInterface::PortNum portNum);
	static  BdSerialPort& GetSerialPort(BdSerialInterface::PortNum portNum);
	static  void  Task(BdSerialInterface::PortNum portNum);
	static  void  Task1(void);
	static  void  Task2(void); 
	static	SigmaStatus SetupSerialPort(void);
	static  Uint  Write( const void * pBuffer, Uint count, 
            Uint milliseconds = 0, 
			BdSerialInterface::PortNum portNum = BdSerialInterface::SERIAL_PORT_1);
	static  Uint  Read( void * pBuffer, Uint count, Uint milliseconds = 0,
            BdSerialInterface::PortNum portNum = BdSerialInterface::SERIAL_PORT_1);
	static  void  Start( void );

	static  void  EnableLoopback( 
			BdSerialInterface::PortNum portNum = BdSerialInterface::SERIAL_PORT_1);
	static  void  DisableLoopback(
			BdSerialInterface::PortNum portNum = BdSerialInterface::SERIAL_PORT_1);
	static  void  EnableExternalLoopback(
			BdSerialInterface::PortNum portNum = BdSerialInterface::SERIAL_PORT_1);
	static  void  DisableExternalLoopback(
			BdSerialInterface::PortNum portNum = BdSerialInterface::SERIAL_PORT_1);

    static void   SoftFault(const SoftFaultID  softFaultID,
            const Uint32       lineNumber,
            const char*        pFileName = NULL,
            const char*        pPredicate = NULL);

  protected:

	virtual void driver_(void);
	virtual void processRequests_(void) = 0;
	void setSerialParameters_(Uint baudRate, Uint dataBits,
		                      SerialPort::Parity parity, Uint stopBits);
	Boolean isSerialPortActivated_() const;

	//@ Data-Member: rSerialPort_
	//  Reference to the SerialPort that this BdSerialInterface controls
	SerialPort  & rSerialPort_;
 
	//@ Data-Member: portNum_
	//  Holds the port number associated with this instance
	BdSerialInterface::PortNum  portNum_;
 
  private:

	BdSerialInterface& operator=(const BdSerialInterface& rhs);	//not implemented
	//@ Data-Member: serialPortActivated_
	//  Set TRUE by the Start method indicating the application wants 
    //  the serial interface activated
	Boolean     serialPortActivated_;

};

#endif	// ! BdSerialInterface_HH
