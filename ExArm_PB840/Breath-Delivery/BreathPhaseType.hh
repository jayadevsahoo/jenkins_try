
#ifndef BreathPhaseType_HH
#define BreathPhaseType_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Struct:  BreathPhaseType - Values of Phase Type.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/BreathPhaseType.hhv   10.7   08/17/07 09:34:54   pvcs  
//
//@ Modification-Log
//
//  Revision: 005   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added NIF/P100/VCM phase types.
//
//  Revision: 004   By: syw   Date:  15-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added PAV_INSPIRATORY_PAUSE.
//
//  Revision: 003  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.  Added INSPIRATORY_PAUSE,
//          NULL_INSPIRATION.
//
//  Revision: 002  By:  iv    Date:  23-Jan-1997    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 001   By: iv    Date: 03 May 1995    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct BreathPhaseType
{
  //@ Type:  PhaseType
  // All of the possible values of Phase Type.
	enum PhaseType
	{
		EXHALATION= 0,
		LOWEST_BREATH_PHASE_VALUE = EXHALATION,
		INSPIRATION,
		NULL_INSPIRATION,
		EXPIRATORY_PAUSE,
		INSPIRATORY_PAUSE,
		BILEVEL_PAUSE_PHASE,
		PAV_INSPIRATORY_PAUSE,
		NIF_PAUSE,
		P100_PAUSE,
		VITAL_CAPACITY_INSP,
		NON_BREATHING,
		HIGHEST_BREATH_PHASE_VALUE = NON_BREATHING
	};
};

#endif // BreathPhaseType_HH 
