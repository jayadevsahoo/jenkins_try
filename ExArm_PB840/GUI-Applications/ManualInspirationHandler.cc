#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ManualInspirationHandler - Handles Manual Inspiration requests
// (initiated by the Manual Inspiration off-screen key).
//---------------------------------------------------------------------
//@ Interface-Description
// The only methods in ManualInspirationHandler called by another
// class are the key panel callback methods , keyPanelPressHappened()
// and keyPanelReleaseHappened() which are automatically called when a
// Manual Inspiration key press/release event occurs (by virtue of the
// fact that ManualInspirationHandler registers for these events).
//---------------------------------------------------------------------
//@ Rationale
// Something needs to handle the Manual Inspiration key and this is it.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply register for Manual Inspiration Key event callbacks and inform
// the Breath Delivery system via an ambassador object of any key press
// event.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ManualInspirationHandler.ccv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ManualInspirationHandler.hh"

//@ Usage-Classes
#include "KeyPanel.hh"
#include "BdGuiEvent.hh"
#include "Sound.hh"
#include "BdEventRegistrar.hh"
#include "MiscStrs.hh"
#include "MessageArea.hh"
#include "GuiApp.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ManualInspirationHandler()  [Default Constructor]
//
//@ Interface-Description
// Creates the Manual Inspiration Handler object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Register for Manual Inspiration Key event callbacks and Breath-Delivery's
// Manual Inspiration's status event.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ManualInspirationHandler::ManualInspirationHandler(void)
{
	CALL_TRACE("ManualInspirationHandler::ManualInspirationHandler(void)");

	// Register for Manual Inspiration key presses
	KeyPanel::SetCallback(KeyPanel::MAN_INSP_KEY, this);

	// Register for Breath-Delivery Manual Inspiration status events
	BdEventRegistrar::RegisterTarget(EventData::MANUAL_INSPIRATION, this); 
																// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ManualInspirationHandler  [Destructor]
//
//@ Interface-Description
// Destroys the Manual Inspiration Handler object.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ManualInspirationHandler::~ManualInspirationHandler(void)
{
	CALL_TRACE("ManualInspirationHandler::~ManualInspirationHandler(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelPressHappened
//
//@ Interface-Description
// Called when the Manual Inspiration key is pressed.  Inform BD.
//---------------------------------------------------------------------
//@ Implementation-Description
// Use the ambassador object to inform BD that Manual Inspiration is
// active.
//
// Note: pressing of this key is invalid during service mode so just make
// the invalid entry sound in this case.
// $[01289] During SST, Alarm silence, alarm reset, manual inspiration ...
// $[07034] During service mide, Alarm silence, alarm reset, help, ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ManualInspirationHandler::keyPanelPressHappened(KeyPanel::KeyId)
{
	CALL_TRACE("ManualInspirationHandler::keyPanelPressHappened(KeyPanel::KeyId)");

	// Display the offscreen key help message 
	GuiApp::PMessageArea->setMessage(
						MiscStrs::MANUAL_INSPIRATION_HELP_MESSAGE,
								MessageArea::MA_HIGH);

	// Only react to keys during normal ventilation mode.
	if (GuiApp::GetGuiState() == STATE_ONLINE)
	{													// $[TI1]
		// Notify Breath-Delivery subsystem
		BdGuiEvent::RequestUserEvent(EventData::MANUAL_INSPIRATION);
	}
	else
	{													// $[TI2]
		// Illegal key press during service mode
		Sound::Start(Sound::INVALID_ENTRY);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelReleaseHappened
//
//@ Interface-Description
// Called when the Manual Inspiration key is released.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply turn off the offscreen help message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ManualInspirationHandler::keyPanelReleaseHappened(KeyPanel::KeyId)
{
	CALL_TRACE("ManualInspirationHandler::keyPanelReleaseHappened(KeyPanel::KeyId)");

	//								 $[TI1]
	// Clear the off screen key message
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened 
//
//@ Interface-Description
// Called when Manual Inspiration event happens and its status is notified
// by the Breath-Delivery subsystems.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the event status is BDGE_REJECTED, beep the invalid sound.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
ManualInspirationHandler::bdEventHappened(EventData::EventId,
										EventData::EventStatus eventStatus,
										EventData::EventPrompt eventPrompt)
{
	CALL_TRACE("ManualInspirationHandler::bdEventHappened("
				"EventData::EventId, "
				"EventData::EventStatus eventStatus)");

	switch (eventStatus)
	{
	case EventData::REJECTED:							// $[TI1]	
		// Prompt users of the rejected state
		Sound::Start(Sound::INVALID_ENTRY);
		break;

	default:											// $[TI2]
		break;
	}
}






#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ManualInspirationHandler::SoftFault(const SoftFaultID  softFaultID,
								    const Uint32       lineNumber,
								    const char*        pFileName,
								    const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, MANUALINSPIRATIONHANDLER,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
