; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
ClassCount=8
Class1=CDaocpvtdbApp
LastClass=CSeqSNDateSet
NewFileInclude2=#include "daocpvtdb.h"
ResourceCount=0
NewFileInclude1=#include "stdafx.h"
Class2=CInfoSet
LastTemplate=CDaoRecordset
Class3=CLogHeaderSet
Class4=CLogInfoSet
Class5=COATestSet
Class6=CSequenceSet
Class7=CTestrunsSet
Class8=CSeqSNDateSet

[CLS:CDaocpvtdbApp]
Type=0
HeaderFile=daocpvtdb.h
ImplementationFile=daocpvtdb.cpp
Filter=N
LastObject=CDaocpvtdbApp

[CLS:CInfoSet]
Type=0
HeaderFile=InfoSet.h
ImplementationFile=InfoSet.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x
LastObject=CInfoSet

[DB:CInfoSet]
DB=1
DBType=DAO
ColumnCount=7
Column1=[ID], 4, 4
Column2=[Info1], 12, 50
Column3=[Info2], 12, 50
Column4=[Info3], 12, 50
Column5=[Info4], 12, 50
Column6=[Info5], 12, 50
Column7=[LastSeq], 4, 4

[CLS:CLogHeaderSet]
Type=0
HeaderFile=LogHeaderSet.h
ImplementationFile=LogHeaderSet.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x
LastObject=CLogHeaderSet

[DB:CLogHeaderSet]
DB=1
DBType=DAO
ColumnCount=4
Column1=[ID], 4, 4
Column2=[FileName], 12, 50
Column3=[BDSN], 12, 50
Column4=[GUISN], 12, 50

[CLS:CLogInfoSet]
Type=0
HeaderFile=LogInfoSet.h
ImplementationFile=LogInfoSet.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x
LastObject=CLogInfoSet

[DB:CLogInfoSet]
DB=1
DBType=DAO
ColumnCount=3
Column1=[ID], 4, 4
Column2=[Line], 12, 255
Column3=[Header], 4, 4

[CLS:COATestSet]
Type=0
HeaderFile=OATestSet.h
ImplementationFile=OATestSet.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x
LastObject=COATestSet

[DB:COATestSet]
DB=1
DBType=DAO
ColumnCount=4
Column1=[TID], 4, 4
Column2=[TName], 4, 4
Column3=[TPFInd], 4, 4
Column4=[Sequence], 4, 4

[CLS:CSequenceSet]
Type=0
HeaderFile=SequenceSet.h
ImplementationFile=SequenceSet.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x
LastObject=CSequenceSet

[DB:CSequenceSet]
DB=1
DBType=DAO
ColumnCount=18
Column1=[ID], 4, 4
Column2=[time], 11, 8
Column3=[User], 12, 50
Column4=[P/F], 4, 4
Column5=[UUTPN], 12, 50
Column6=[UUTBDSN], 12, 50
Column7=[UUTRev], 12, 50
Column8=[BDPost], 12, 50
Column9=[GUISN], 12, 50
Column10=[GUIPost], 12, 50
Column11=[Saas], 12, 50
Column12=[CompSN], 12, 50
Column13=[CompRunTime], 12, 50
Column14=[PTSSN], 12, 50
Column15=[PTSVer], 12, 50
Column16=[PTSLastCal], 12, 50
Column17=[PTSNextCal], 12, 50
Column18=[PTSOutofCal], -7, 1

[CLS:CTestrunsSet]
Type=0
HeaderFile=TestrunsSet.h
ImplementationFile=TestrunsSet.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x
LastObject=CTestrunsSet

[DB:CTestrunsSet]
DB=1
DBType=DAO
ColumnCount=8
Column1=[ID], 4, 4
Column2=[pf_flag], 4, 4
Column3=[Fact], 12, 50
Column4=[MinRdg], 8, 8
Column5=[Rdg], 8, 8
Column6=[MaxRdg], 8, 8
Column7=[TEST_ID], 5, 2
Column8=[Type], 4, 4

[CLS:CSeqSNDateSet]
Type=0
HeaderFile=SeqSNDateSet.h
ImplementationFile=SeqSNDateSet.cpp
BaseClass=CDaoRecordset
Filter=N
VirtualFilter=x
LastObject=CSeqSNDateSet

[DB:CSeqSNDateSet]
DB=1
DBType=DAO
ColumnCount=21
Column1=[ID], 4, 4
Column2=[time], 11, 8
Column3=[User], 12, 255
Column4=[P/F], 4, 4
Column5=[UUTPN], 12, 255
Column6=[UUTBDSN], 12, 255
Column7=[UUTBDRunTime], 12, 255
Column8=[UUTRev], 12, 255
Column9=[BDPost], 12, 255
Column10=[GUISN], 12, 255
Column11=[GUIPost], 12, 255
Column12=[Saas], 12, 255
Column13=[CompSN], 12, 255
Column14=[CompRunTime], 12, 255
Column15=[PTSSN], 12, 255
Column16=[PTSVer], 12, 255
Column17=[PTSLastCal], 12, 255
Column18=[PTSNextCal], 12, 255
Column19=[PTSOutofCal], -7, 1
Column20=[VentAsset], 12, 255
Column21=[WorkOrder], 12, 255

