//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#ifndef MESSAGEPROCESSOR_H
#define MESSAGEPROCESSOR_H


#include "BatchCommandInfo.h"
#include "BatchMemoryManager.h"

#include"SwatBatchUser.h"

#ifndef NMEMORY_SLOTS
#define NMEMORY_SLOTS 20
#endif

namespace swat
{

    class MessageProcessor
    {
        private:
            static BatchMemoryManager m_clBatchMemoryManager;
            static bool (*m_fcall)(BatchCommandInfo *);

        public:
            MessageProcessor(bool (*fcall)(BatchCommandInfo *) );
            static void ProcessMessage(char *pIn, int  iMsgLen );
            static bool ProcessPacket(char *buf, int iMsgLen  );


    };

}
#endif
