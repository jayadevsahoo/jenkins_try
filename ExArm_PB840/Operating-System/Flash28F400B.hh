#ifndef Flash28F400B_HH
#define Flash28F400B_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Filename: Flash28F400B.hh - FLASH Memory Programmer for (i.e) Intel
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/Operating-System/vcssrc/Flash28F400B.hhv   25.0.4.0   19 Nov 2013 14:16:18   pvcs  $
//
//@ Modification-Log
//
//  Revision 001  By: Rhomere Jimenez  Date: 07-March-2006  DR Number: 6192
//   Project:   Xena
//   Description:
//      Initial version.
//=====================================================================

#include "Sigma.hh"
#include "OsClassId.hh"

#define Flash28F400B_WSM_BUSY 1

Int Flash28F400B_EraseBlock(volatile Uint32 * address);
Int Flash28F400B_WriteWord(volatile Uint32 * address, const Uint data);
Int Flash28F400B_EraseBlockNonBlocking(volatile Uint32 * address);
Int Flash28F400B_Check(volatile Uint32 * address);
Int Flash28F400B_GetDeviceId( volatile Uint32 * address );
Uint32 * Flash28F400B_GetFlashBlockAddress(Uint8 index);
Int      Flash28F400B_GetFlashBlockSize(Uint8 index);
Uint32 * Flash28F400B_GetFlashBlock1Size(void);
Uint32   Flash28F400B_GetNumSubBlocks(void);
Uint32   Flash28F400B_GetTotalSubBlocks(void);

#endif // Flash28F400B_HH
