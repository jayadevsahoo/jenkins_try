#ifndef VrtxHooks_HH
#define VrtxHooks_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  VrtxHooks - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/VrtxHooks.hhv   25.0.4.0   19 Nov 2013 14:34:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 

#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
//@ End-Usage

//#include "vrtxvisi.h"

class VrtxHooks
{
//  public:
//    static void Initialize(void);
//
//    static void     SoftFault(const SoftFaultID softFaultID,
//			      const Uint32      lineNumber,
//			      const char*       pFileName  = NULL,
//			      const char*       pPredicate = NULL);
//    
//#ifdef SIGMA_UNIT_TEST
//    friend  class VrtxHooks_UT;
//#endif
//
//  private:
//    VrtxHooks();                             // declared only
//    ~VrtxHooks();                            // declared only
//
//    VrtxHooks(const VrtxHooks&);             // declared only
//    void operator=(const VrtxHooks&);        // declared only
//
//    static void   TCreateHook_(TCB * pCreateeTcb, TCB * pCreatorTcb);
//    static void   TDeleteHook_(TCB * pDeleteeTcb, TCB * pDeletorTcb);
//    static void   TSwitchHook_(TCB * pOldTcb    , TCB * pNewTcb    );

};


#endif // VrtxHooks_HH
