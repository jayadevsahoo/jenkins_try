/*****************************************************************************/
/*   Module Name:    volume.h                                           
/*   Purpose:      This module contains the code which performs          
/*                   volume testing using PTS 2000.                                                   
/*                                                                           
/*   Requirements:   This module implements requirements detailed in a       
/*                   72361.h                                                         
/*                                                                           
/*   $Revision::   1.1      $                                                   
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _VOLUME_H
#define _VOLUME_H

/*************/
/** ENUMS ****/
/*************/
          
enum {
	ERR_COMPORT,
	ERR_VIPER_RESPONSE,
	ERR_SAMPLE_RATE,
	ERR_TIMEOUT,
	ERR_BAD_VIPER_TRANSMIT,
	ERR_NO_VIPER,
	ERR_BAD_END_CUT
};

int  SetUpVolumeOn(int sampleRate,int numberOfSamples, int inspTime, int TimeOut);
int  MeasureVentVolume(double *btps, double *atp);
int  Init(void); 
void SetO2Gas(void); 

#endif // #ifndef _VOLUME_H
