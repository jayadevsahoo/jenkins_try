#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: BdService - BD State: Service.
//---------------------------------------------------------------------
//@ Interface-Description
//  The BdService class is a FsmState used by the BdFsm finite state
//  machine.  The class represents the wait state that BD Task Control
//  enters upon completing a BD transition to the Service state.  It
//  contains methods for processing events while in this state.  
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the BD Service state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  BdService is derived from the FsmState class.  It overrides the
//  virtual method receiveGuiReady to provide state specific behavior
//  when it receives a GuiReadyMessage.  It overrides the virtual
//  method receiveCommUp to provide state specific behavior when it
//  receives a CommUpMessage.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/BdService.ccv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Changed to ignore ChangeState requests while in SERVICE mode.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "BdService.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "TaskControlAgent.hh"
#include "GuiReadyMessage.hh"
#include "TaskAgentManager.hh"
//TODO E600 port Reset #include "Reset.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdService [default constructor]
//
//@ Interface-Description
//  BdService class default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdService::BdService()
{
    CALL_TRACE("BdService()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdService [destructor]
//
//@ Interface-Description
//  BdService class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdService::~BdService()
{
    CALL_TRACE("~BdService()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

const char*
BdService::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "BdService";
    // $[TI1]
}
 
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with 
//  this BdService state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the SERVICE state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
SigmaState
BdService::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_SERVICE;
    // $[TI1]
}
 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveCommUp
//
//@ Interface-Description
//  The receiveCommUp method processes a CommUpMessage received by BD
//  task control in the BdService state.  It sends a BdReady-Service
//  message to GUI task control.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method invokes TaskControlAgent::ReportNodeReady to send
//  the BdReady to the GUI.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdService::receiveCommUp(const CommUpMessage& ) const
{
    CALL_TRACE("receiveCommUp(const CommUpMessage&)");
    TaskControlAgent::ReportNodeReady();
    // $[TI1]
}
 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveGuiReady
//
//@ Interface-Description
//  The receiveGuiReady method processes a GuiReadyMessage received 
//  from GUI task control by BD task control in the BdService state.
//  It updates the local state of the GUI from the message.  For GUI
//  reporting ready in the SERVICE state, it forwards the 
//  GuiReadyMessage to the application tasks.  Upon receiving GuiReady
//  for any other GUI state, this method causes the BD to reboot.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method invokes FsmState::SetGuiState to set the local state of
//  the GUI to the state contained in the GuiReadyMessage.  It uses
//  TaskAgentManager::Send to forward the GuiReadyMessage to each 
//  managed task.  It will assert to cause the BD to reboot.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdService::receiveGuiReady(const GuiReadyMessage& message) const
{
    CALL_TRACE("receiveGuiReady(const GuiReadyMessage&)");
 
    FsmState::SetGuiState(message);

    switch (message.getState())
    {
        case STATE_SERVICE:  // $[TI1]
        case STATE_START:
        case STATE_INIT:
            TaskAgentManager::Send(message);
            break;
        case STATE_INOP:     // $[TI2]
        case STATE_ONLINE:
        case STATE_SST:
        case STATE_TIMEOUT:
        default:        
        	//TODO E600 port Reset            Reset::Initiate(INTENTIONAL, Reset::BDSERVICE);
            break;
    }
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BdService::SoftFault(const SoftFaultID  softFaultID,
                 const Uint32       lineNumber,
                 const char*        pFileName,
                 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_BDSERVICE, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveChangeState
//
//@ Interface-Description
//  The receiveChangeState method processes a ChangeStateMessage while
//  in the SERVICE state.  All requested state transitions are ignored
//  while in the SERVICE state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void 
BdService::receiveChangeState(const ChangeStateMessage& message) const
{
    CALL_TRACE("receiveChangeState(const ChangeStateMessage&)");
    // $[TI1]
}
    
//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

