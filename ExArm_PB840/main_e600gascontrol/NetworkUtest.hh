#ifndef NetworkUtest_HH
#define NetworkUtest_HH

//ONLY FOR UNIT TESTING, ON BD SIDE..
#if defined (SIGMA_BD_CPU)
#undef RUN_NETWORK_UNIT_TEST	//#define for running this unit-test

#include "Sigma.hh"


class NetworkUtest
{
public:

	static void runEndiannessUtest(void);

private:
	
	static Int testRTBreathData(void);
    static Int testAveragedBreathData(void);
    static Int testEndBreathData(void);
    static Int testWaveformData(void);
    static Int testBiLevelMandData(void);
    static Int testBiLevelMandExhData(void);
    static Int testComputedInspPauseData(void);
    static Int testPavBreathData(void);
    static Int testPavPlateauData(void);
    static Int testPavStartupData(void);
    static Int testEndExpPauseData(void);

};

#endif // (SIGMA_BD_CPU)

#endif //NetworkUtest_HH