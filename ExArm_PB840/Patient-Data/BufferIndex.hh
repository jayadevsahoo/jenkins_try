
#ifndef BufferIndex_HH
#define BufferIndex_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  BufferIndex -  data structure definition for an index 
//                           to Double Breath Data buffer.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/BufferIndex.hhv   10.7   08/17/07 10:27:28   pvcs  
//
//@ Modification-Log
//
//  Revision: 000  By:  jhv    Date:  29-Jan-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//            Initial version 
//  
//====================================================================


struct BufferIndex {
  Uint   network;
  Uint   active;
};



#endif // BufferIndex_HH 
