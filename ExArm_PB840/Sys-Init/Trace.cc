#include "stdafx.h"
#ifndef SIGMA_OFFICIAL
#ifdef TRACE_ON
#ifndef TRACE_OFF

//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
// Class: Trace - Call tracing class -- TRACE_ON ONLY
//---------------------------------------------------------------------
// Interface-Description
//  A macro is provided for the entry/exit tracing so that all uses of
//  this facility can be "compiled away" when NOT in debug mode.  This
//  module is compiled only for non-official releases only.  The 
//  compiler flag TRACE_ON must be defined and the TRACE_OFF flag must
//  be "undefined" as well.
//---------------------------------------------------------------------
// Rationale
//---------------------------------------------------------------------
// Implementation-Description
//  This mechanism depends on the C++'s issuance of a constructor (at
//  definition time) and destructor (at scope exit).
//---------------------------------------------------------------------
// Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
// Restrictions
//  none
//---------------------------------------------------------------------
// Invariants
//  none
//---------------------------------------------------------------------
// End-Preamble
//
// Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/Trace.ccv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
// Modification-Log
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "Trace.hh"

// Usage-Classes
#include "Task.hh"
#include "Ostream.hh"
// End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

static int      TraceLevel_[Task::MAX_NUM_TASKS+1];
static Boolean  TraceOn_[Task::MAX_NUM_TASKS+1];

// Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//=====================================================================
//
//  Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  Trace(funcName, fileName, lineNumber)  [Constructor]
//
// Interface-Description
//	Construct an instance of this class, and -- if call-tracing is
//	activated -- use the input parameters as the information of an
//	entry into a routine.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//      none
//---------------------------------------------------------------------
// PostCondition
//      none
// End-Method
//=====================================================================

Trace::Trace(const char*  funcName,
             const char*  fileName,
             const int    lineNumber) 

           : pFuncName_(funcName), pFileName_(fileName), lineNumber_(lineNumber)
{
    Uint32   taskid = Task::GetId();
    if (TraceOn_[taskid]) 
    {
        for (int i=0; i<TraceLevel_[taskid]; i++) cout << "  ";
        cout << ">>" << pFuncName_ << " in file " << fileName << endl;
        ++TraceLevel_[taskid];
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  ~Trace()  [Destructor]
//
// Interface-Description
//	Destroy this instance of this class, and -- if call-tracing is
//	activated -- process the exit of a routine.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//      none
//---------------------------------------------------------------------
// PostCondition
//      none
// End-Method
//=====================================================================

Trace::~Trace(void)
{
    Uint32   taskid = Task::GetId();
    if (TraceOn_[taskid]) 
    {
        --TraceLevel_[taskid];
        for (int i=0; i<TraceLevel_[taskid]; i++) cout << "  ";
        cout << "<<" << pFuncName_ << " in file " << pFileName_ << endl;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: SetTraceOn
//
// Interface-Description
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
Trace::SetTraceOn()
{
    TraceOn_[Task::GetId()] = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method: SetTraceOn(int)
//
// Interface-Description
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
Trace::SetTraceOn(int thread)
{
    if (0 <= thread && thread <= Task::MAX_NUM_TASKS)
        TraceOn_[thread] = TRUE;
    else
    {
        if (thread == 99)
            for (int i = 0; i <= Task::MAX_NUM_TASKS; i++) 
                TraceOn_[i] = TRUE;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: SetTraceOff
//
// Interface-Description
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
Trace::SetTraceOff()
{
    TraceOn_[Task::GetId()] = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method: SetTraceOff(int)
//
// Interface-Description
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
Trace::SetTraceOff(int thread)
{
    if (0 <= thread && thread <= Task::MAX_NUM_TASKS)
        TraceOn_[thread] = FALSE;
    else
    {
        if (thread == 99)
            for (int i = 0; i <= Task::MAX_NUM_TASKS; i++) 
                TraceOn_[i] = FALSE;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: IsTraceOn
//
// Interface-Description
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
Trace::IsTraceOn()
{
    return TraceOn_[Task::GetId()];
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: IsTraceOn(int)
//
// Interface-Description
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
Trace::IsTraceOn(int thread)
{
    if (0 <= thread && thread <= Task::MAX_NUM_TASKS)
        return TraceOn_[thread];
    else
        return FALSE;
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================



#endif  // !TRACE_OFF
#endif  // TRACE_ON
#endif  // !SIGMA_OFFICIAL
