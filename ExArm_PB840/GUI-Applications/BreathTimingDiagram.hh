#ifndef BreathTimingDiagram_HH
#define BreathTimingDiagram_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BreathTimingDiagram - Displays a timing bar which illustrates
// the relationship between the following settings: Respiratory Rate,
// Inspiratory Time, Expiratory Time and I:E Ratio.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BreathTimingDiagram.hhv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  Minute Volume no longer displayed in Apnea Setup and Vent Setup
//         subscreens, therefore removed from this class
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002   By: sah    Date:  06-Oct-1997    DR Number: 2544
//  Project: Sigma (R8027)
//  Description:
//      Added new static constant for storing the seconds-to-minutes
//      conversion factor.  Also, changed 'MSECS_PER_SEC_' constant
//      from an 'Int32' to a 'Real32'.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "Box.hh"
#include "Line.hh"
#include "NumericField.hh"
#include "Padlock.hh"
#include "TextField.hh"
#include "TouchableText.hh"
#include "SettingObserver.hh"

class  SettingSubject; // forward declaration...
//@ End-Usage


class BreathTimingDiagram : public Container, public SettingObserver
{
public:
	enum ForcedPadlockState
	{
		NONE,
		FORCED_ON,
		FORCED_OFF
	};

	BreathTimingDiagram(const Boolean isInApneaMode   = FALSE,
						const Boolean isVentSetupDiag = FALSE);
	~BreathTimingDiagram(void);

	void  setActiveSetting(SettingId::SettingIdType settingId);

	inline void  setForcedPadlockState(const ForcedPadlockState forcedState);

	// Drawable virtual method
	virtual void activate  (void);
	virtual void deactivate(void);

	// SettingObserver virtual method
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject);

	static void SoftFault(const SoftFaultID softFaultID,
			  			  const Uint32      lineNumber,
			  			  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	BreathTimingDiagram(const BreathTimingDiagram&);	// not implemented...
	void operator=(const BreathTimingDiagram&);			// not implemented...

	enum
	{
		//@ Constant: BTD_MAX_IE_RATIO_STRING_LENGTH_
		// The maximum size of the buffer which contains the cheap text for
		// the I:E Ratio value string (see below for more info).
		BTD_MAX_IE_RATIO_STRING_LENGTH_ = 128
	};

	enum TimingSettingId_
	{
		CONSTANT_PARM_,
		RESP_RATE_,
		INSP_TIME_,			// this represents both Ti and Th (BiLevel)...
		EXP_TIME_,			// this represents both Te and Tl (BiLevel)...
		IE_RATIO_,			// this represents both I:E and H:L (BiLevel)...

		NUM_TIMING_SETTINGS_
	};

	void  initSubjectPtrs_  (void);
	void  updateSubjectPtrs_(void);

	void  updateDiagram_(void);

	SettingSubject*  arrSubjectPtrs_[NUM_TIMING_SETTINGS_];

	//@ Data-Member:  IS_IN_APNEA_MODE_
	// Tells us whether this diagram should display the Apnea timing settings
	// rather than the normal ventilation timing settings.
	const Boolean  IS_IN_APNEA_MODE_;

	//@ Data-Member: leftMarker_
	// The black upright line which marks the beginning of the timing bar.
	Box leftMarker_;

	//@ Data-Member: rightMarker_
	// The blank line which marks the end of the timing bar.
	Box rightMarker_;

	//@ Data-Member: xAxis_
	// The horizontal line which forms the "X" axis of the bar.
	Box xAxis_;

	//@ Data-Member: inspiratoryXAxis_
	// The horizontal line which forms the inspiratory part of the "X" axis 
	// bar.
	Box inspiratoryTimeXAxis_;

	//@ Data-Member: expiratoryXAxis_
	// The horizontal line which forms the expiratory part of the "X" axis 
	// bar.
	Box expiratoryTimeXAxis_;

	//@ Data-Member: inspiratoryTimeBox_
	// The colored box in which the inspiratory time value is normally displayed
	// and whose size width gives an indication of the relative size of the
	// inspiratory time.
	Box inspiratoryTimeBox_;

	//@ Data-Member: inspiratoryTimeOutsideBox_
	// The colored box in which the inspiratory time value is displayed outside
	// the breath bar when the inspiratoryTimeBox_ is too small to contain the value.
	Box inspiratoryTimeOutsideBox_;

	//@ Data-Member: expiratoryTimeBox_
	// The colored box in which the expiratory time value is displayed
	// and whose size width gives an indication of the relative size of the
	// expiratory time.
	Box expiratoryTimeBox_;

	//@ Data-Member: expiratoryTimeOutsideBox_
	// The box in which the expiratory time value is displayed outside
	// the breath bar when the expiratoryTimeBox_ is too small to contain
	// the expiratory time value.
	Box expiratoryTimeOutsideBox_;

	//@ Data-Member: ieRatioBox_
	// The colored box with a white border in which the I:E ratio value is
	// displayed and which is centered around the border between the
	// inspiratory and expiratory time boxes.
	Box ieRatioBox_;

	//@ Data-Member: ieRatioPointer_
	// The line which points from the I:E ratio box down to the X axis of the
	// bar.
	Box ieRatioPointer_;

	//@ Data-Member: tTotOutsideBox_
	// The box outside the breath bar containing the T-tot value.
	Box tTotOutsideBox_;

	//@ Data-Member: tTotPointer_
	// The small tick mark which points from the T-tot value to the X axis.
	Box tTotPointer_;

	//@ Data-Member: inspiratoryTimePointer_
	// The line which points from the inspiratory time value into the middle
	// of the inspiratory time box when the inspiratory time value is too
	// big to fit in the box and must be displayed to the left of same.
	Box inspiratoryTimePointer_;

	//@ Data-Member: inspiratoryTimePointerEnd_
	// The small box which forms a square dot and the end of the inspiratory
	// time pointer.
	Box inspiratoryTimePointerEnd_;

	//@ Data-Member: expiratoryTimePointer_
	// The line which points from the expiratory time value into the middle
	// of the expiratory time box when the expiratory time value is too
	// big to fit in the box and must be displayed to the right of same.
	Box expiratoryTimePointer_;

	//@ Data-Member: expiratoryTimePointerEnd_
	// The small box which forms a square dot and the end of the expiratory
	// time pointer.
	Box expiratoryTimePointerEnd_;

	//@ Data-Member: highlightBox_
	// A box which is used to highlight either the T-tot value or the
	// inspiratory time value when it is displayed outside of its box.
	// These settings are highlighted when they are the active setting and
	// the highlight is accomplished by displaying the value on a white box.
	Box highlightBox_;

	//@ Data-Member: zero_
	// The numeric field which displays a zero for denoting the start of the
	// timing bar.
	NumericField zero_;

	//@ Data-Member: scale_
	// The numeric field at the end of the timing diagram which displays the
	// current value to which the diagram is being scaled, either 2, 5, 12
	// or 60.
	NumericField scale_;

	//@ Data-Member: unitsText_
	// The text field at the end of the timing diagram which displays
	// the units (seconds) for the timing graph.
	TextField unitsText_;

	//@ Data-Member: inspiratoryTime_
	// The numeric field which displays the inspiratory time value, normally
	// displayed in the inspiratory time box.
	NumericField inspiratoryTime_;

	//@ Data-Member: expiratoryTime_
	// The numeric field which displays the expiratory time value,  displayed
	// in the expiratory time box.
	NumericField expiratoryTime_;

	//@ Data-Member: ieRatioText_
	// The text field which displays the I:E ratio value,  displayed
	// in the I:E Ratio box.
	TextField ieRatioText_;

	//@ Data-Member: ieRatioString_
	// The buffer which contains the cheap text for the I:E Ratio value
	// displayed in ieRatioText_.
	wchar_t ieRatioString_[BTD_MAX_IE_RATIO_STRING_LENGTH_];   

	//@ Data-Member: inspiratoryTimePadlock_
	// The padlock used for making the Inspiratory Time setting constant
	// during a rate change.
	Padlock inspiratoryTimePadlock_;

	//@ Data-Member: expiratoryTimePadlock_
	// The padlock used for making the Expiratory Time setting constant
	// during a rate change.
	Padlock expiratoryTimePadlock_;

	//@ Data-Member: ieRatioPadlock_
	// The padlock used for making the I:E Ratio setting constant during a
	// rate change.
	Padlock ieRatioPadlock_;

	//@ Data-Member: padlockPointer_
	// The line pointing from the middle padlock (I:E Ratio) up to the I:E Ratio
	// diving line.
	Box padlockPointer_;

	//@ Data-Member: manualInspText_
	// The text field which displays "Manual Insp" in Manual Inspiration mode
	// which is active during Vent Setup when the Mode is SPONT.
	TextField manualInspText_;

	//@ Data-Member: tTot_
	// The numeric field which displays T-tot (60 / Respiratory Rate).
	NumericField tTot_;

	//@ Data-Member: activeSetting_
	// The currently active setting.  If one of the timing setting buttons
	// is active (i.e. currently being adjusted by the user) then the
	// corresponding value on the timing bar needs to be highlighted (also,
	// if respiratory rate is active then the padlock buttons must be
	// displayed).  Users of this class should set this member via the
	// setActiveSetting() method.
	SettingId::SettingIdType activeSetting_;

	//@ Data-Member: isInManualInspMode_
	// Indicates that Manual Inspiration mode is active.  In this mode,
	// the Expiratory Time and I:E Ratio are not shown.
	Boolean isInManualInspMode_;

	//@ Data-Member: isInPadlockMode_
	// Indicates that padlocks should be displayed.  Padlocks should only
	// be displayed if the Respiratory Rate setting is active while the
	// Mandatory Type is PCV and only while in Vent Setup.
	Boolean isInPadlockMode_;

	const Boolean  IS_VENT_SETUP_DIAG_;

	BreathTimingDiagram::ForcedPadlockState  forcedPadlockState_;
};

#include "BreathTimingDiagram.in"


#endif // BreathTimingDiagram_HH 
