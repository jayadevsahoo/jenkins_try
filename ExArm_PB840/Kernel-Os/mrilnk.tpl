@dnl -------------------------------------------------------------------------
@dnl
@dnl		Copyright (c) 1994 MICROTEC RESEARCH INCORPORATED.
@dnl
@dnl	All rights reserved. MICROTEC RESEARCH's source code is an unpublished
@dnl	work and the use of a copyright notice does not imply otherwise.
@dnl	This source code contains confidential, trade secret material of
@dnl	MICROTEC RESEARCH. Any attempt or participation in deciphering,
@dnl	decoding, reverse engineering or in any way altering the source code
@dnl	is strictly prohibited, unless the prior written consent of
@dnl	MICROTEC RESEARCH is obtained.
@dnl
@dnl
@dnl	Module Name:		mrilnk.tpl
@dnl
@dnl	Identification:		@(#) 6.18 mrilnk.tpl
@dnl
@dnl	Date:			12/9/94  12:24:39
@dnl
@dnl -------------------------------------------------------------------------

LISTMAP CROSSREF,PUBLICS/ADDR
CHIP	@(board.proc)
ALIAS   9,vectors
ALIAS	9,code
ALIAS	13,const
ALIAS	13,strings
ALIAS	13,literals
ALIAS	13,vars
@ifelse(@(tool.cxx),,,`
ALIAS   13,ehsftbl
ALIAS   13,ehscfn
ALIAS   13,0
')@dnl
ALIAS	14,zerovars
ALIAS	14,tags
*ABSOLUTE	9,13
@ifelse(@tplargv(4),.,`ORDER 9,13,14,initfini
SECT 9  = $@tplargv(3)
',`ORDER	9,13,initfini
SECT 14	= $@tplargv(4)
SECT 9	= $@tplargv(3)
SECT post_vectors = $00004000
SECT scratchpad   = $00004400
')
