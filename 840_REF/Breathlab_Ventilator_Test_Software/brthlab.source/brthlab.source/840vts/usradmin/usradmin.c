/*****************************************************************************/
/*   Module Name:      usradmin.c                                               
/*   Purpose:       Manipulation and support of user administration panels.                                                     
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   Description of Change			             By			      Date 
/*   ---------------------				    	-----   		  ------
/*   Set command button color to transparent    Dave Richardson   25 Nov, 2002
/*   to make square outline of rectangular 
/*   region around round buttons disappear
/*
/*   $Revision::   1.2      $                                                
/*                                                                          
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/usradmin/usradmin.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:37:18   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   25 Nov 2002 11:56:02   Richardson
 * Updated per SCR # 24

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <pwctrl.h>
#include <userint.h>
#include <utility.h>

#include "globals.h"

#include "cpvtmain.h"
#include "ldb.h"
#include "logonfun.h"
#include "mainpnls.h"
#include "registry.h"
#include "regpwd.h"
#include "usradmin.h"


/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

UserOptions crntOpts;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     ProcessOptions                                                     
/*                                                                           
/* Purpose:  Sets user options
/*                                          
/*                                                                           
/* Example Call:   void ProcessOptions(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void ProcessOptions(void)
{
	CurrLanguage = crntOpts.Language;
	nLanguageID = GetLanguageID(CurrLanguage);

	return;
}


/*****************************************************************************/
/* Name:     Adduser_Callback                                                    
/*                                                                           
/* Purpose:  New user panel callback function
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK Adduser_Callback(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK Adduser_Callback(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	char PassWord[       MAX_PWD_LEN + 1];
	char ConfirmPassWord[MAX_PWD_LEN + 1];

	char UserName[MAX_USERNAME_LEN + 1];

	switch(control){
		case ADDUSERPNL_USERNAME:
			switch(event){
				case EVENT_VAL_CHANGED:
				case EVENT_KEYPRESS:
				case EVENT_GOT_FOCUS:
				case EVENT_LOST_FOCUS:
					ProcessSystemEvents();
					ProcessDrawEvents();

					GetCtrlVal(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME, UserName);
					if(strcmp(UserName, "") != 0){
						SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_OK_BUTTON, ATTR_DIMMED, FALSE);
					}
					else{
						SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_OK_BUTTON, ATTR_DIMMED, TRUE);
					}
					break;
			} // end switch(event)
			break;
	} // end switch(control)

	return 0;
}

/*****************************************************************************/
/* Name:     UIAddUser                                                    
/*                                                                           
/* Purpose:  Display the adduser panel and handle it's events.
/*                                          
/*                                                                           
/* Example Call:   void UIAddUser();                                    
/*                                                                           
/* Input:    <None>        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void UIAddUser(void)
{
	int tempPanelHandle, tempCntrlHandle;
	char PassWord[       MAX_PWD_LEN      + 1];
	char ConfirmPassWord[MAX_PWD_LEN      + 1];
	char UserName[       MAX_USERNAME_LEN + 1];

	gPanelHandle[ADDUSERPNL] = LoadPanel(0, LDBf(Ldb_MainpanelFile), ADDUSERPNL);
	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_ERROR,     ATTR_VISIBLE, FALSE);
	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_OK_BUTTON, ATTR_DIMMED,  TRUE);

	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_OK_BUTTON,     ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_CANCEL_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

	PasswordCtrl_ConvertFromString(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD);
	PasswordCtrl_ConvertFromString(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2);
	PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD,   ATTR_PASSWORD_MAX_LENGTH, MAX_PWD_LEN);
	PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2, ATTR_PASSWORD_MAX_LENGTH, MAX_PWD_LEN);
	SetActiveCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME);

	InstallPopup(gPanelHandle[ADDUSERPNL]);
	while(GetUserEvent(1, &tempPanelHandle, &tempCntrlHandle)){
		if(tempCntrlHandle == ADDUSERPNL_OK_BUTTON){
			GetCtrlVal(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME, UserName);
			PasswordCtrl_GetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD,   ATTR_PASSWORD_VAL, PassWord);
			PasswordCtrl_GetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2, ATTR_PASSWORD_VAL, ConfirmPassWord);
			if(strcmp(PassWord, ConfirmPassWord) != 0){
				SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_ERROR, ATTR_CTRL_VAL, LDBf(Ldb_Password_Mismatch));
				SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_ERROR, ATTR_VISIBLE, TRUE);
				DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD);
				DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2);
				SetActiveCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD);
			}
			else{
				if(AddUser(UserName, PassWord) == PWD_USER_EXISTS){
					SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_ERROR, ATTR_CTRL_VAL, LDBf(Ldb_Dup_Username));
					SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_ERROR, ATTR_VISIBLE, TRUE);
					DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME);
					DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD);
					DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2);
					SetActiveCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME);
				}
				else{
					DiscardPanel(gPanelHandle[ADDUSERPNL]);
					gPanelHandle[ADDUSERPNL] = NULL;
					break;
				}
			}
			PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD,   ATTR_PASSWORD_VAL, "");
			PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2, ATTR_PASSWORD_VAL, "");
		}
		if(tempCntrlHandle == ADDUSERPNL_CANCEL_BUTTON){
			DiscardPanel(gPanelHandle[ADDUSERPNL]);
			gPanelHandle[ADDUSERPNL] = NULL;
			if(gPanelHandle[LOGONPANEL] != NULL){
				SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS, ATTR_CTRL_VAL, "");
			}
			break;
		}
	}

	return;
}

