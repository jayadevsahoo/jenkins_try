#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SafetyPcvScheduler - This scheduler is responsible for handling
// breathing during safety pcv
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for scheduling breath phases during safety pcv
// mode. The class is also responsible for relinquishing control to the next
// valid scheduler and for assuming control when safety pcv is requested.
// Safety pcv may be requested upon emergency mode recovery and power-up
// sequence.  This scheduler is responsible for enabling valid mode triggers
// and for enabling breath triggers that are not under the breath phase direct
// control. The scheduler is responsible for handling user events like 100% O2,
// manual inspiration request, etc.  No setting changes are allowed during this
// mode.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates all the breathing rules specified for safety pcv. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The class implements methods for breath and mode transitions and for
// breath data updates. The scheduler is responsible for:
// - Determining the next breath when a breath trigger becomes active.
// - Initiating the activities required for the proper start of a new breath:
//	  update breath record, set breath triggers, 
//    enable breath triggers, and phase out the previous breath phase, 
// - Relinquishing control when a mode trigger becomes active.
//    Activities while control is relinquished include: 
//    disable out of date breath triggers, determine which is the next
//    valid scheduler, enable breath triggers for the next scheduler,
//    and instruct the next scheduler to take control.
// - Taking control when the previous scheduler relinquishes control. 
//    Activities while taking control include:
//    register self with the BreathPhaseScheduler object,  and activate
//    the mode trigger list.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SafetyPcvScheduler.ccv   25.0.4.0   19 Nov 2013 14:00:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 029   By: rhj    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Added net flow backup inspiratory trigger.
//
//  Revision: 025   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 024   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added case to reject RM maneuvers during Safety PCV.
//
//  Revision: 023  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      ATC initial release.
//      Changed 'SoftFault()' method to a non-inlined method.
//      Added new triggers to class precondition.
//
//  Revision: 022  By:  syw    Date:  17-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (R8027)
//       Description:
//			Call RPeep.setPeepChange if breath is to be Peep Recovery.
//
//  Revision: 021  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling of inspiratory pause event.
//
//  Revision: 020  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 019  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 018  By:  iv    Date:  03-Apr-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per unit test peer review
//             Deleted duplicate testable item.
//             Rework per formal code review for BD Schedulers.
//
//  Revision: 017  By:  iv    Date:  24-Mar-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery unit test peer review.
//
//  Revision: 016  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 015  By:  iv    Date:  25-Jan-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 014  By:  iv    Date:  03-Dec-1996    DR Number: DCS 1608, 1541
//       Project:  Sigma (R8027)
//       Description:
//             determineBreathPhase(), case = NON_BREATHING : eliminate assertion
//             and add initialization for peep recovery state.
//             Prepare code for inspections.
//
//  Revision: 013  By:  iv    Date:  15-Nov-1996    DR Number: DCS 1559
//       Project:  Sigma (R8027)
//       Description:
//             Replaced safe class assertion with class assertion in constructor.
//
//  Revision: 012  By:  iv    Date:  06-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Deleted class assertion in determineBreathPhase() for NON_BREATHING
//             case.
//
//  Revision: 011  By:  sp    Date:  26-Sep-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed PeepRecoveryTrigger to PeepRecoveryInspTrigger.
//
//  Revision: 010 By:  iv   Date:   09-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//               Added a check for peep recovery trigger in determineBreathPhase(),
//             and disable the trigger on relinquishControl().
//
//  Revision: 009 By:  iv   Date:   02-Aug-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Incorporated PEEP recovery after OSC and Powerup.
//
//  Revision: 008 By:  iv   Date:   21-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Added a Boolean isPeepRecovery to newBreathRecord(...) call.
//
//  Revision: 007 By:  iv   Date:   04-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Fixed call to base class takeControl() from takeControl().
//             Added a call BdSystemStateHandler::UpdateSafetyPcvActiveStatus(TRUE).
//
//  Revision: 006 By:  iv   Date:   07-May-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Changed takeControl(), determineBreathPhase(), and
//             startInspiration_() methods - to allow for a PEEP recovery
//             breath phase to take place on transitions from disconnect,
//             standby, and SVO.
//
//  Revision: 005 By:  iv   Date:   15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated O2_MONITOR_CALIBRATE case in reportEventStatus_().
//
//  Revision: 004 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 003 By:  kam   Date:  01-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Modified interface for reporting ventilator status.  Vent
//             and user event status are posted to the BD Status task
//             via the method VentAndUserEventStatus::PostEventStatus().
//             The BD status task is then responsible for transmitting
//             the status information to the GUI through NetworkApps.
//
//             Also, updated SST_CONFIRMATION case of reportEventStatus_() and
//             added O2_MONITOR_CALIBRATE.  Added call to base class
//             takeControl() from takeControl().
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem and 
//             for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "SafetyPcvScheduler.hh"
#include "BreathMiscRefs.hh"
#include "PhaseRefs.hh"
#include "TriggersRefs.hh"
#include "SchedulerRefs.hh"
#include "ModeTriggerRefs.hh"
#include "BdDiscreteValues.hh"

//@ Usage-Classes
#include "BreathSet.hh"
#include "BreathTriggerMediator.hh"
#include "UiEvent.hh"
#include "BdSystemStateHandler.hh"
#include "BreathTrigger.hh"
#include "ModeTrigger.hh"
#include "ModeTriggerMediator.hh"
#include "TimerBreathTrigger.hh"
#include "TimerModeTrigger.hh"
#include "O2Mixture.hh"
#include "Peep.hh"
#include "ApneaInterval.hh"
#include "PendingContextHandle.hh"
#include "PhasedInContextHandle.hh"
#include "PhaseInEvent.hh"
#include "BreathPhase.hh"
#include "BdAlarms.hh"
#include "VentAndUserEventStatus.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SafetyPcvScheduler()  [Default Contstructor]
//
//@ Interface-Description
//        Constructor.
// $[04125] $[04130] $[04141]
// The constructor takes no arguments. It constructs the class instance with
// the proper id. The list of valid mode triggers, used by the mode trigger
// mediator is initialized as well. The triggers are ordered based on priority
// - the most urgent is first. The list also guarantees that when a trigger
// fires - the triggers that follow it do not have to be processed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor invokes the base class constructor with the SAFETY_PCV id
// argument. It initializes the private data member pModeTriggerList_.
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
// The number of elements on the trigger list is asserted
// to be MAX_NUM_SPCV_TRIGGERS and MAX_NUM_SPCV_TRIGGERS to be less than
// MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static const Int32 MAX_NUM_SPCV_TRIGGERS = 5;
SafetyPcvScheduler::SafetyPcvScheduler(void) :
                    BreathPhaseScheduler(SchedulerId::SAFETY_PCV)
{
    // $[TI1]
    CALL_TRACE("SafetyPcvScheduler::SafetyPcvScheduler(void)");

    Int32 ii = 0;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RDisconnectTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&ROcclusionTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RVentSetupCompleteTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RApneaTrigger;
    pModeTriggerList_[ii] = (ModeTrigger*)NULL;

    CLASS_ASSERTION(ii == MAX_NUM_SPCV_TRIGGERS &&
                        MAX_NUM_SPCV_TRIGGERS < MAX_MODE_TRIGGERS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SafetyPcvScheduler()  [Destructor]
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
SafetyPcvScheduler::~SafetyPcvScheduler(void)
{
  CALL_TRACE("SafetyPcvScheduler(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathPhase
//
//@ Interface-Description
//  $[04254] $[04255] $[04217]
// Safety pcv mode schedules VIM breaths and responds to patient effort
// the same as AC mode. However, expiratory pause and inspiratory pause are
// not allowed during this mode.
// The method accepts a breath trigger as an argument. It determines
// which breath phase to start, based on the trigger id, the current phase,
// and the vent settings.
// Once the next breath phase is determined, the following activities
// take place:
//    - Update the O2%. 
//    - Update the current breath record with new data.
//    - Change the active breath trigger list, disabling the breath triggers
//      on the current list.
//    - Instruct the current breath phase to relinquish control and setup
//      the new breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The activities handled by this method are dependent on the current breath
// phase. The current breath phase is retrieved from the BreathPhase object.
// Before any phase starts, the BreathTriggerMediator is used to reset the
// old phase trigger list, and to set the new phase trigger list,  the old 
// phase relinquishes control, and the new phase registers itself with the 
// BreathPhase object. The applied O2 percent and the breath record get
// updated.
// A switch statement is implemented for the various breath phases which are:
//  NON_BREATHING, EXHALATION, and INSPIRATION.
// Inspiration begins by calling the method startInspiration_(). Before that
// method is called, the breath type is set (e.g. CONTROL, ASSIST, etc.)
// When current phase is EXHALATION, the breath type  is determined 
// and inspiration is delivered. When current phase is NON_BREATHING, the
// breathType is set to SPONT to start peep recovery inspiration.
//---------------------------------------------------------------------
//@ PreCondition
// For each case in the switch statement, the expected range of valid
// triggers and/or previous schedulers is checked. The valid range for
// phase type is also checked.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
SafetyPcvScheduler::determineBreathPhase(const BreathTrigger& breathTrigger) 
{
    CALL_TRACE("SafetyPcvScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)");

    // the type of breath to communicate to the user
    ::BreathType breathType = NON_MEASURED;

    const Trigger::TriggerId triggerId = breathTrigger.getId();

    // Get the scheduler id from the current breath record. Note that the current
    // breath record stores the id of the scheduler that was active at the time the
    // record was created.  
    const SchedulerId::SchedulerIdValue previousSchedulerId =
            (RBreathSet.getCurrentBreathRecord())->getSchedulerId();

    // A pointer to the current breath phase
    BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
    // determine the current phase type (e.g. EXHALATION, INSPIRATION)
    const BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();

    switch (phase)
    {
        case BreathPhaseType::NON_BREATHING:
        // $[TI1]
            // $[04321] Deliver a peep recovery inspiration:
            BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
            startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
            
            break;
            
        case BreathPhaseType::EXHALATION:
        // $[TI2]
            if( (triggerId == Trigger::TIME_INSP) &&
                            (previousSchedulerId != SchedulerId::SAFETY_PCV) )
            {
            // $[TI2.1]
                //previous scheduler is not SAFETY_PCV, therefore the time trigger
                // should have been disabled:
                CLASS_ASSERTION_FAILURE();
            }
            else // inspiration is to be delivered
            {
            // $[TI2.2]
                // the peep recovery trigger is enable when OSC scheduler relinquishes
                // control, to ensure fast transition to the next inspiration.
                if(triggerId == Trigger::PEEP_RECOVERY)
                {
                // $[TI2.2.1]
                    breathType = ::SPONT;
                    CLASS_ASSERTION( BreathPhaseScheduler::PeepRecovery_ ==
                                      BreathPhaseScheduler::START &&
                                     previousSchedulerId == SchedulerId::OCCLUSION );
                }
                // $[03008] $[04010] set the breath type:
                // Peep recovery mand insp trigger is checked here since this trigger
                // terminates exhalation after peep recovery inspiration.
                else if( (triggerId == Trigger::OPERATOR_INSP) ||
                        (triggerId == Trigger::PEEP_RECOVERY_MAND_INSP) ||
                        (triggerId == Trigger::TIME_INSP) )
                {
                // $[TI2.2.2]
                    //This is a VIM breath
                    breathType = ::CONTROL;
                    CLASS_PRE_CONDITION(previousSchedulerId == SchedulerId::SAFETY_PCV);
                }
                // $[02258]
                else if( (triggerId == Trigger::PRESSURE_INSP) ||
					     (triggerId == Trigger::NET_FLOW_BACKUP_INSP)
						) 
                {
                // $[TI2.2.3]
                    // this is a patient triggered breath -- PIM
                    breathType     = ::ASSIST;
                }
                else
                {
                // $[TI2.2.4]
                    //The trigger should be one of the following legal triggers:
                    // immediate trigger is not valid since it trigggers inspiration only
                    // from the NON_BREATHING phase.
                    AUX_CLASS_PRE_CONDITION(
                            (triggerId == Trigger::PRESSURE_INSP) || 
							(triggerId == Trigger::NET_FLOW_BACKUP_INSP) ||
                            (triggerId == Trigger::TIME_INSP) || 
                            (triggerId == Trigger::PEEP_RECOVERY_MAND_INSP) ||
                            (triggerId == Trigger::PEEP_RECOVERY) ||
                            (triggerId == Trigger::OPERATOR_INSP), triggerId )
                }
                startInspiration_(breathTrigger, pBreathPhase, breathType);
            }
            break;

        case BreathPhaseType::INSPIRATION:
        
        // $[TI3]
            // inspiratory pause shall not be active
            //Check the validity of the triggers. Once triggers are checked, settings for
            //new exhalation can be phased in.

            //The trigger should be one of the following legal triggers:
            CLASS_PRE_CONDITION( (triggerId == Trigger::DELIVERED_FLOW_EXP) || 
			                (triggerId == Trigger::HIGH_PRESS_COMP_EXP) || 
			                (triggerId == Trigger::LUNG_FLOW_EXP) || 
            			    (triggerId == Trigger::LUNG_VOLUME_EXP) || 
                            (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) || 
                            (triggerId == Trigger::PRESSURE_EXP) || 
                            (triggerId == Trigger::BACKUP_TIME_EXP) || 
                            (triggerId == Trigger::IMMEDIATE_EXP) );

            //Note that no setting phase-in is done during safety pcv

#ifdef INTEGRATION_TEST_ENABLE
            // Signal Event for swat
            swat::EventManager::signalEvent(swat::EventManager::START_OF_EXH);
#endif // INTEGRATION_TEST_ENABLE
    
            //Reset currently active triggers and setup triggers that are active during
            // inspiration:
            RBreathTriggerMediator.resetTriggerList();
            RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);

            if(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::ACTIVE)
            {
            // $[TI3.1]
            // $[04323]
                BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::OFF;
                ((BreathTrigger&)RPeepRecoveryMandInspTrigger).enable();
            }    // $[TI3.2]
            // phase-out current breath phase
            pBreathPhase->relinquishControl(breathTrigger);
            
            // Determine the O2 mix, apnea interval and the peep value.
            // (there is a need to update the apnea interval to be ready for a transition
            // out of safety pcv)
            RO2Mixture.determineO2Mix(*this, (BreathPhase&)RExhalationPhase);
            RApneaInterval.newPhase(BreathPhaseType::EXHALATION);
            RPeep.updatePeep(BreathPhaseType::EXHALATION);

            // register new phase:
            BreathPhase::SetCurrentBreathPhase((BreathPhase&)RExhalationPhase,
                                                                        breathTrigger);
            //Update the breath record
            (RBreathSet.getCurrentBreathRecord())->newExhalation();
            // start delivering exhalation
			BreathPhase::NewBreath() ;
            
            break;
        
        // $[TI5] DELETED

        default:
        // $[TI4]
            AUX_CLASS_ASSERTION_FAILURE(phase);
    }                                                        
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
// The method accepts a mode trigger reference as an argument and has no return
// value.  Before the new mode is instructed to take control, the time insp
// trigger is disabled, the asap trigger is enabled (on transition to SIMV, and
// AC modes), and all mode triggers on the current trigger list get reset.
// Since SafetyPcv mode may follow SVO scheduler, a NON_BREATHING breath phase
// is possible. In that case, any breath delivery is avoided until the
// completion of the transitional NON_BREATHING phase that guarantees a
// complete closure of the safety valve. If this is the case, state of the
// breath triggers do not change when a transition to a breathing mode is
// detected.  The immediate breath trigger is enabled on transition to
// emergency modes. No breath triggers are enabled on transition to Spont mode.
// When an immediate breath trigger is enabled, the rest of the triggers on the
// active list are disabled to ensure proper transition to the emergency mode.
// The argument passed, provides the method with the information required to
// determine the next scheduler.  The BD Status task is notified of the Safety PCV status.
//---------------------------------------------------------------------
//@ Implementation-Description
// Individual breath triggers are enabled and disabled by directly
// accessing the trigger instance.
// Disabling all triggers on the current list is accomplished by 
// using the only one instance of the breath trigger mediator,
// calling the method resetTriggerList(). 
// The mode trigger id passed as an argument is checked to
// determine the requested scheduler which then instructed to
// take control. 
// VentAndUserEventStatus::PostEventStatus() is also
// invoked to notify the BD Status task of exit from Safety PCV.
//---------------------------------------------------------------------
//@ PreCondition
// When the mode trigger is vent setup complete, the non breathing
// phase is checked to be SafetyValveClosePhase.
// The mode trigger id is checked for valid range of mode trigger values.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
SafetyPcvScheduler::relinquishControl(const ModeTrigger& modeTrigger)
{

    CALL_TRACE("SafetyPcvScheduler::relinquishControl(const ModeTrigger& modeTrigger)");

    const Trigger::TriggerId modeTriggerId = modeTrigger.getId();

    // determine the current phase type (e.g. EXHALATION, INSPIRATION)
    const BreathPhaseType::PhaseType phase = BreathRecord::GetPhaseType();

    // Note: any unexpected trigger will cause a soft fault!

    // disable the inspiratory time trigger, to avoid inspiration trigger
    // during the new mode
    RTimeInspTrigger.disable();

    // disable the peep recovery trigger, to avoid its interference with
    // the next scheduler.
    ((BreathTrigger&)RPeepRecoveryInspTrigger).disable();

    // reset all mode triggers for this scheduler
    RModeTriggerMediator.resetTriggerList();
    
    // notify the BD Status task of end of Safety PCV
    VentAndUserEventStatus::PostEventStatus (EventData::SAFETY_VENT, EventData::CANCEL);
        
    if(modeTriggerId == Trigger::VENT_SETUP_COMPLETE)
    {
    // $[TI1]
        // check what mode to switch to
        const DiscreteValue mode = PendingContextHandle::GetDiscreteValue(SettingId::MODE);
        if (phase != BreathPhaseType::NON_BREATHING)
        {
        // $[TI1.1]
               // set the asap trigger only if the mode is not spont. 
            if ((mode != ModeValue::SPONT_MODE_VALUE) && (mode != ModeValue::CPAP_MODE_VALUE))
            {
               // $[TI1.1.1]
                   ((BreathTrigger&)RAsapInspTrigger).enable();
            }
               // $[TI1.1.2]
        }
        else // phase == BreathPhaseType::NON_BREATHING
            // If the breath phase is a non breathing phase, it is the
            // safety valve closed phase that is in progress, and since a time trigger
            // is already set to terminate that phase, there is no need to set any
            // breath trigger.
        {
           // $[TI1.2]
            CLASS_PRE_CONDITION( BreathPhase::GetCurrentBreathPhase() ==
                        (BreathPhase*)&RSafetyValveClosePhase );
        }

        BreathPhaseScheduler& rSetScheduler =
                                BreathPhaseScheduler::EvaluateSetScheduler(mode);
        rSetScheduler.takeControl(*this);
    }
    else // vent setup not complete
    {
    // $[TI2]
        //Reset currently active triggers and then enable the only valid breath
        // trigger for that transition.
        RBreathTriggerMediator.resetTriggerList();
        ((BreathTrigger&)RImmediateBreathTrigger).enable();
        if(modeTriggerId == Trigger::OCCLUSION)
        {
        // $[TI2.1]
            ((BreathPhaseScheduler&)ROscScheduler).takeControl(*this);
        }
        else if(modeTriggerId == Trigger::DISCONNECT)
        {
        // $[TI2.2]
            ((BreathPhaseScheduler&)RDisconnectScheduler).takeControl(*this);
        }
        else if(modeTriggerId == Trigger::SVO)
        {
        // $[TI2.3]
            ((BreathPhaseScheduler&)RSvoScheduler).takeControl(*this);
        }
        else
        {
        // $[TI2.4]
            // make sure trigger is valid:
            CLASS_PRE_CONDITION( 
                (modeTriggerId == Trigger::OCCLUSION) ||
                (modeTriggerId == Trigger::DISCONNECT) ||
                (modeTriggerId == Trigger::SVO) );
        }
    }
}      

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description
// This method is invoked by the method relinquishControl() from the scheduler
// instance that relinquishes control.  It accepts a breath phase scheduler
// reference as an argument and has no return value. The base class method for
// takeControl() is invoked to handle activities common to all schedulers.
// Peep recovery flag is set on transition from a non breathing mode. Safety
// PCV settings are phased in first then the NOV RAM instance rBdSystemState is
// updated with the new scheduler to enable BD system to properly recover from
// power interruption.  The scheduler registers with the BreathPhaseScheduler
// as the current scheduler, and the mode triggers relevant to that mode are
// set to be enabled.  This method also interfaces with BdAlarms to post the
// Procedure error condition, which is then passed to the Alarm-Analysis
// subsystem.  The BD Status task is also notified of the Safety PCV status.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler reference passed as an argument is used to get the id of the
// current scheduler.  The static method of BreathPhaseScheduler is used to
// register the new scheduler. The BdSystemStateHandler is used to update the
// NOV RAM object for BdSystemState with the new scheduler id.  The private
// data member pModeTriggerList_ is passed as an argument to the method in the
// mode trigger mediator object that is responsible for changing the current
// mode trigger list.  The method rBdAlarms.postBdAlarm() is invoked to log the
// procedure error.  and VentAndUserEventStatus::PostEventStatus() is invoked
// to notify the BD Status task of the start of Safety PCV.
//---------------------------------------------------------------------
//@ PreCondition
// The CLASS_PRE_CONDITION method is used to validate the IDs of the
// valid breath phase schedulers.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
SafetyPcvScheduler::takeControl(const BreathPhaseScheduler& scheduler) 
{
// $[TI1]
    CALL_TRACE("SafetyPcvScheduler::const takeControl(BreathPhaseScheduler& scheduler)");

    // Invoke the base class takeControl method
    BreathPhaseScheduler::takeControl(scheduler);
    
    SchedulerId::SchedulerIdValue schedulerId = scheduler.getId();

    // set the safety pcv active flag in nov ram
    BdSystemStateHandler::UpdateSafetyPcvActiveStatus(TRUE);

    // $[05172] 
    // notify alarms of procedure error
    RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_PROCEDURE_ERROR);

    // notify the BD Status task of start of Safety PCV
    VentAndUserEventStatus::PostEventStatus (EventData::SAFETY_VENT, EventData::ACTIVE);
        
    // phase-in safety pcv settings:
    PhasedInContextHandle::PhaseInSafetyPcvSettings();

    if( (schedulerId == SchedulerId::DISCONNECT) || 
        (schedulerId == SchedulerId::STANDBY) ||
        (schedulerId == SchedulerId::SVO) ||
        (schedulerId == SchedulerId::OCCLUSION) ||
        (schedulerId == SchedulerId::POWER_UP) )
    {
    // $[TI1.1]
        BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
    }
    else
    {
    // $[TI1.2]
        CLASS_ASSERTION(
            (schedulerId != SchedulerId::AC) &&
            (schedulerId != SchedulerId::APNEA) &&
            (schedulerId != SchedulerId::SPONT) &&
            (schedulerId != SchedulerId::SIMV) &&
            (schedulerId != SchedulerId::BILEVEL) &&
            (schedulerId != SchedulerId::SAFETY_PCV) );
    }

    // update the reference to the newly active breath scheduler 
    BreathPhaseScheduler::SetCurrentScheduler_((BreathPhaseScheduler&)(*this));

    //The BreathRecord is not being updated with the new scheduler id, so that
    // the identity of the scheduler that originated this breath is maintained. 
    // until a new inspiration starts.
    
    // Initialize the cycle time to an arbitrary value. This value does not effect
    // ventilation.
    breathCycleTime_ = 10000;    

    // update the BD state, in case of power interruption
       BdSystemStateHandler::UpdateSchedulerId(getId()); 

    // Set the mode triggers for that scheduler.
    enableTriggers_();    
    RModeTriggerMediator.changeModeTriggerList(pModeTriggerList_);
} 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SafetyPcvScheduler::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, SAFETYPCVSCHEDULER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableTriggers_
//
//@ Interface-Description
// $[04125] $[04130] $[04141]
// The method takes no arguments and returns no value.  A request to enable
// SafetyPcv scheduler valid mode triggers is issued whenever this scheduler
// takes control. Immediate triggers are not set here since by design
// they are only set once the trigger they instanciate becomes active.
//---------------------------------------------------------------------
//@ Implementation-Description
// The mode trigger instances are accessed directly with a call to their
// setIsEnableRequested() method.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
SafetyPcvScheduler::enableTriggers_(void) 
{
// $[TI1]
    CALL_TRACE("SafetyPcvScheduler::enableTriggers_(void)");

    ((ModeTrigger&)ROcclusionTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RDisconnectTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RSvoTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RVentSetupCompleteTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RApneaTrigger).setIsEnableRequested(TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts a EventId and a Boolean for the event status as
// arguments.  It returns a EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247] $[01255]
// The argument of type EventId indicates which user event is active.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is accepted by
// invoking a call to the static method:
// BreathPhaseScheduler::AcceptManualInspiration_(eventStatus).
// The alarm reset event is ignored, while the eventStatus argument value is checked for
// TRUE.
// The expiratory and inspiratory pause events are rejected by invoking a call to
// the static methods: BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus) and
// BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus) .
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

EventData::EventStatus
SafetyPcvScheduler::reportEventStatus_(const EventData::EventId id,
                                                     const Boolean eventStatus)
{
    CALL_TRACE("SafetPcvScheduler::reportEventStatus_(const EventData::EventId id,\
                                                     const Boolean eventStatus)");

    EventData::EventStatus rtnStatus = EventData::IDLE;

    switch (id)
    {
        case EventData::MANUAL_INSPIRATION:
        // $[TI1]
            rtnStatus =
                    BreathPhaseScheduler::AcceptManualInspiration_(eventStatus);
            break;

        case EventData::ALARM_RESET:
        // $[TI2]
            // ignore alarm reset
            CLASS_PRE_CONDITION(eventStatus);
            break;

        case EventData::EXPIRATORY_PAUSE:
        // $[TI3] $[04255] $[04217]
            rtnStatus =
                    BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus);
            break;

        case EventData::INSPIRATORY_PAUSE:
        // $[TI4]
        // inspiratory pause shall not be active
	    // $[BL04007]
            rtnStatus =
                    BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus);
            break;

		case EventData::NIF_MANEUVER:
		case EventData::P100_MANEUVER:
		case EventData::VITAL_CAPACITY_MANEUVER:
			// $[RM12032] Reject RM maneuvers in Safety PCV
			rtnStatus = EventData::REJECTED;
			break;

        default:
        // $[TI6]
            AUX_CLASS_ASSERTION_FAILURE(id);
    }    
    return (rtnStatus);
}


//=====================================================================
//
// private methods
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startInspiration_ 
//
//@ Interface-Description
// The method takes a breath trigger reference, a breath phase pointer, and a
// breath type as arguments. It does not return any value.  When the next phase
// is determined to be inspiration, the method is called to perform all the
// standard procedures required to start inspiration: A new breath record is
// created, the oxygen mix, the apnea interval, and the peep get updated, the
// breath trigger list is changed for inspiration list, and the previous breath
// relinquishes control. For control or assist breath type, the time insp
// trigger is set and enabled to determine the time of the next inspiration,
// and the breath phase is set to reference the Pcv breath phase. For
// spont breath type, the peep recovery state is set to active and the breath
// phase is set to reference the PeepRecovery breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// A new breath record is created using the object rBreathSet.  The actual
// oxygen mix, apnea interval, and peep are updated using the objects
// rO2Mixture, rApneainterval, and rPeep.  The breath trigger mediator instance
// rBreathTriggerMediator is used to set the inspiratory breath trigger list.
// The breath trigger instance rTimeInspTriger is restarted with the new cycle
// time. The breath phase instance pointed to by argument pBreathPhase is
// instructed to phase out.  The static method BreathPhase::SetCurrentBreathPhase
// is used to set up the new mandatory breath type.
//---------------------------------------------------------------------
//@ PreCondition
// Check ranges for mandatory type and breath type. Check peep recovery flag for
// true in case of a spont breath.
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
SafetyPcvScheduler::startInspiration_(const BreathTrigger& breathTrigger,
                                        BreathPhase* pBreathPhase,
                                         BreathType breathType)
{
    CALL_TRACE("SafetyPcvScheduler::startInspiration_(const BreathTrigger& breathTrigger, \
                                BreathPhase* pBreathPhase, BreathType breathType)");

    // pointer to a breath phase
    BreathPhase* pPhase = NULL;
    // variable to store setting values:
    DiscreteValue mandatoryType;
    // flags peep recovery in process
    Boolean isPeepRecovery = TRUE;

    // get the mandatory type
    mandatoryType = PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);
    CLASS_PRE_CONDITION( mandatoryType == MandTypeValue::PCV_MAND_TYPE );

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

    //Reset currently active triggers and setup triggers that are active during inspiration:
    RBreathTriggerMediator.resetTriggerList();
    RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::INSP_LIST);

    if( breathType == ::CONTROL || breathType == ::ASSIST)
    {
    // $[TI1]
        isPeepRecovery = FALSE;
        pPhase = (BreathPhase*)&RPcvPhase;
        // $[04009] $[04254]
        //Set the time trigger for next inspiration, start its timer and enable the
        //trigger. The trigger is actually re-enabled here after being disabled
        //previously by BreathTriggerMediator method resetTriggerList().
        // respiratory rate setting
        const BoundedValue& rRespiratoryRate =
            PhasedInContextHandle::GetBoundedValue(SettingId::RESP_RATE);    
        breathCycleTime_ = (Int32)(60.0F * 1000.0F / rRespiratoryRate.value);
        RTimeInspTrigger.restartTimer(breathCycleTime_);
        RTimeInspTrigger.enable();
    }
    else if (breathType == ::SPONT)
    {
    // $[TI2]
        isPeepRecovery = TRUE;
        // deliver a peep recovery phase
        // Set the PeepRecovery_ variable to ACTIVE to flag the activation of the peep recovery
        // mand insp trigger on the start of the next exhalation. 
        CLASS_ASSERTION(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::START);
        BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::ACTIVE;
        mandatoryType = ::NULL_MANDATORY_TYPE;
        pPhase = (BreathPhase*)&RPeepRecoveryPhase;
    }
    else
    {
    // $[TI3]
        CLASS_PRE_CONDITION( (breathType == ::CONTROL) ||
                             (breathType == ::ASSIST) ||
                             (breathType == ::SPONT) );
    }

    // phase-out current breath phase
    pBreathPhase->relinquishControl(breathTrigger);

    // $[04118] $[04122]
    // determine the O2 mix, apnea, and peep for this phase. Note that
    // although apnea is not possible during safety pcv, the apnea timer is actively
    // maintained. This is done so that apnea can be detected on transition from
    // safety pcv to the next mode. 
    RO2Mixture.determineO2Mix(*this, *pPhase);
    RApneaInterval.newPhase(BreathPhaseType::INSPIRATION);
    RPeep.updatePeep(BreathPhaseType::INSPIRATION);

	if (pPhase == (BreathPhase*)&RPeepRecoveryPhase)
	{	// $[TI4]
	    RPeep.setPeepChange( TRUE) ;
	}	// $[TI5]

    //  register new phase:
    BreathPhase::SetCurrentBreathPhase(*pPhase, breathTrigger);

    //Update the breath record
    RBreathSet.newBreathRecord(SchedulerId::SAFETY_PCV, mandatoryType,
                breathType, BreathPhaseType::INSPIRATION, isPeepRecovery);
    // set the breath record with the breath cycle time:
    BreathRecord::SetBreathPeriod((Real32)breathCycleTime_);
    // start inspiration
	BreathPhase::NewBreath() ;
}
