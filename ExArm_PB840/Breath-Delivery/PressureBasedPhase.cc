#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PressureBasedPhase - Base class for all pressure based inspiration
//		phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This is an abstract base class with both virtual and pure virtual methods
//		to be overwritten by each derived class as required.  The pure virtual
//		functions enforce the implementation of the following methods: to initialize
// 	 	the pressure based phase, activities each BD cycle during a pressure
//		based phase and to wrap up the pressure based phase.
//---------------------------------------------------------------------
//@ Rationale
// 		There are several pressure based phases - PCV, PSV, OSC, PeepRecovery
//		- that share common functionality.  Each derived class defines the
//		specific rules that make each derived phase unique.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The PressureBasedPhase class main responsibilities are to determine the
// 		desired pressures to feed to the pressure controller and the
// 		exhalation valve controller.
//
// 		Pure virtual methods are used to obtain the specifics of each
// 		derived class.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureBasedPhase.ccv   25.0.4.1   20 Nov 2013 17:34:46   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 027  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software.
// 
//  Revision: 026   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 025   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Modified initialization of flow trajectory calculations after P100 
//		maneuver to account for pressure drop during P100 occlusion.
//
//  Revision: 024  By: erm     Date:  05-Mar-2002    DR Number: 5790
// Project: VCP
// Description:
//       Enable volume trigger during Vtpc and Vsv
//
//  Revision: 023  By: syw     Date:  09-Feb-2000    DR Number: 5632
//  Project:  NeoMode
//  Description:
//		For neonatal circuit types:
//		* set mirrorGain = 0
//		* ExhValveIController is ran open loop until trajectory reaches 0.5 of
//			final presssure
//		* avoid calling determineWf_()
//		* new kp and ki gains
//
//  Revision: 022  By: sah     Date:  22-Sep-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  added use of exhalation resistance values and desired relief flow
//         value for the neonatal trajectory calculations
//      *  eliminated unnecessary code used to eliminate compiler warning
//      *  added neonatal-specific trajectory calculations
//      *  eliminated unnecessary run-time calculations of tau's constant
//         factors
//      *  added neonatal-specific 'kp_' and 'ki_' values
//		*  added control of running ExhValveIController open loop.
//		*  alter fap for neo.
//		*  prevent setting kp=0.0 during Neo.
//
//  Revision: 021  By: yyy     Date:  11-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Make PRESSURE_THRESHOLD an extern constant.
//      Initial ATC revision
//
//  Revision: 021  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 020  By:  iv    Date:  23-Oct-1998    DR Number: 5229
//       Project:  BiLevel
//       Description:
//			Added call to RDeliveredFlowExpTrigger.setPressureTrajectory().
//
//  Revision: 019  By:  syw   Date:  16-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.
//			If there was a peep change, use pressure sensor as initial trajectory.
//
//  Revision: 018  By: syw   Date:  15-Sep-1997    DR Number: 2430
//  	Project:  Sigma (840)
//		Description:
//			Added requirement traceability.
//
//  Revision: 017  By: syw   Date:  12-Aug-1997    DR Number: 2356
//  	Project:  Sigma (840)
//		Description:
//			Set AdiabaticFactor_ data member.
//
//  Revision: 016  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 015  By:  iv    Date:  01-Feb-1997    DR Number: NONE
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 014  By:  sp    Date:  08-Jan-1997    DR Number: DCS 1656
//      Project:  Sigma (R8027)
//      Description:
//             Changed rBdMonitoredData to RBdMonitoredData.
//
//  Revision: 013	 By: syw   Date: 30-Sep-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			desiredReliefFlow is now constant 5.0.  exhValveDesPressure is now
//			limitted to zero instead of peep when trajectory < 0.
//
//  Revision: 012	 By: syw   Date: 27-Aug-1996   DR Number: DCS 1101
//  	Project:  Sigma (R8027)
//		Description:
//			Changed value of PRESSURE_THRESHOLD to 97.
//
//  Revision: 011 By: syw   Date: 16-Aug-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added call to updateFlowControllerFlags_().
//
//  Revision: 010 By: sp    Date: 28-May-1996   DR Number: DCS 993
//  	Project:  Sigma (R8027)
//		Description:
//			Moved rDeliveredFlowExpTrigger.setEffectivePressure
//			from newBreath to newcycle.
//
//  Revision: 009 By: hct    Date: 14-May-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added call to 
//                      rBdMonitoredData.setTargetPressure(exhValveDesPressure)
//
//  Revision: 008 By: syw    Date: 06-May-1996   DR Number: DCS 967
//  	Project:  Sigma (R8027)
//		Description:
//			Added call to rDisconnectTrigger.setDesiredFlow()
//
//  Revision: 007 By: syw    Date: 06-May-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Moved determineMirrorGain_() to protected region.
//
//  Revision: 006 By: syw    Date: 26-Feb-1996   DR Number: DCS 669
//  	Project:  Sigma (R8027)
//		Description:
//			Added call to rDeliveredFlowExpTrigger.setReliefFlowTarget().
//			Removed RELIEF_FLOW_TARGET... no longer used.
//
//  Revision: 005 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes.
//
//  Revision: 004 By: syw    Date: 27-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added determineTargetPressure_() method since Settings will not
//			update the setting to the proper value as initially assumed.
//
//  Revision: 003 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By:  kam   Date:  29-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "PressureBasedPhase.hh"

#include "MainSensorRefs.hh"
#include "BreathMiscRefs.hh"
#include "ControllersRefs.hh"
#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "PhaseRefs.hh"

//@ Usage-Classes

#include <math.h>	// for use of exp() 
#include "BD_IO_Devices.hh"
#include "MathUtilities.hh"
#include "BdDiscreteValues.hh"
#include "BreathTrigger.hh"
#include "PressureSensor.hh"
#include "PressureController.hh"
#include "FlowController.hh"
#include "ExhValveIController.hh"
#include "UiEvent.hh"
#include "HighCircuitPressureExpTrigger.hh"
#include "DeliveredFlowExpTrigger.hh"
#include "PhasedInContextHandle.hh"
#include "Peep.hh"
#include "DisconnectTrigger.hh"
#include "LungFlowExpTrigger.hh"
#include "NovRamManager.hh"
#include "P100Maneuver.hh"
#include "ManeuverRefs.hh"
#include "LeakCompMgr.hh"

//@ End-Usage

//@ Code...

Resistance PressureBasedPhase::ExhResistance_ ;

//@ Constant: BACKUP_PRESSURE_SENS_VALUE
// backup pressure sensitivity for triggering
extern const Real32 BACKUP_PRESSURE_SENS_VALUE ;

//@ Constant: MIN_ALPHA
// initial reduction factor value
static const Real32 MIN_ALPHA = 0.7F ;

//@ Constant: PRESSURE_THRESHOLD
// pressure threshold where safety valve crack prevention algorithm kicks in.
const Real32 PRESSURE_THRESHOLD = 97.0 ;

static const Real32 DesiredReliefFlow_ = 5.0F;

//@ Constant: DEFAULT_ADULT_KP_GAIN
// Default kp gain for adult circuit
static const Real32 DEFAULT_ADULT_KP_GAIN     = 2.0f;

//@ Constant: DEFAULT_ADULT_KI_GAIN
// Default ki gain for adult circuit
static const Real32 DEFAULT_ADULT_KI_GAIN     = 0.125f;

//@ Constant: DEFAULT_PEDIATRIC_KP_GAIN
// Default kp gain for pediatric circuit
static const Real32 DEFAULT_PEDIATRIC_KP_GAIN = 1.0f;

//@ Constant: DEFAULT_PEDIATRIC_KI_GAIN
// Default ki gain for pediatric circuit
static const Real32 DEFAULT_PEDIATRIC_KI_GAIN = 0.075f;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureBased()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called with the phase type argument.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PressureBasedPhase::PressureBasedPhase(void)
 : BreathPhase(BreathPhaseType::INSPIRATION),  	// $[TI1]
   pediatricKp_(DEFAULT_PEDIATRIC_KP_GAIN),
   adultKp_(DEFAULT_ADULT_KP_GAIN),
   pediatricKi_(DEFAULT_PEDIATRIC_KI_GAIN),
   adultKi_(DEFAULT_ADULT_KI_GAIN)
{
	CALL_TRACE("PressureBasedPhase::PressureBasedPhase(void)") ;

	// $[TI2]
	
	NovRamManager::GetExhResistance( ExhResistance_) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureBasedPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PressureBasedPhase::~PressureBasedPhase(void)
{
	CALL_TRACE("PressureBasedPhase::~PressureBasedPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
//      This method takes a BreathTrigger& as an argument and has no return
//      value.  This method is called at the beginning of exhalation to wrap
//      up inspiration phase in preparation for the next inspiration phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//      If the trigger to this breath was a manual insp trigger, inform GUI.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PressureBasedPhase::relinquishControl( const BreathTrigger&)
{
	CALL_TRACE("PressureBasedPhase::relinquishControl( const BreathTrigger& trigger)") ;
	
	Trigger::TriggerId id = PBreathTrigger_->getId() ;
	
	// if the trigger to this breath was a manual insp trigger, indicate completion
	// of phase delivery to GUI

	if (id == Trigger::OPERATOR_INSP)
	{
	  	// $[TI1.1]
		((UiEvent&)RManualInspEvent).setEventStatus( EventData::COMPLETE) ;
	}	// implied else $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
// 		This method has no arguments and has no return value.  This method is
// 		called every BD cycle during a pressure based phase to determine the
// 		desired pressure to be used as input into the pressure controller and
// 		the exhalation valve controller for pressure based inspirations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	The desired pressure fed to the pressure controller, refTarget,  is the
//		mirror image of the filtered patient pressure about the exponential
//  	trajectory.  The exponential trajectory is the output of the
//  	alpha filter.  The exponential trajecory is fed to the exhalation
//  	valve controller as the desired pressure and a desired relief flow
//		is also computed and passed in.
// 		$[04170] $[04174] $[04175]	$[04179] $[04183] $[04186] $[04187]
//		$[04019]
//---------------------------------------------------------------------
//@ PreCondition
//		wf == 1.0 || 0.0
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PressureBasedPhase::newCycle( void)
{
	CALL_TRACE("PressureBasedPhase::newCycle( void)") ;
	
	Real32 patPressExh = RExhPressureSensor.getValue() ;

	enableExhalationTriggers_() ;
	determineDesPressFilter_( patPressExh) ;

	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	Real32 finalPressure = effectivePressure_ ;
	Real32 mirrorGain = 0.0 ;
	
	switch (PATIENT_CCT_TYPE)
	{
	case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
	case PatientCctTypeValue::ADULT_CIRCUIT :
		// $[TI3]
		trajectory_ = (1.0F - trajectoryAlpha_) * (peep_ + effectivePressure_)
						+ trajectoryAlpha_ * trajectory_ ;
		RPressureExpTrigger.updateTargetPressure( effectivePressure_ + peep_) ;
		RDeliveredFlowExpTrigger.setEffectivePressure( effectivePressure_) ;
		mirrorGain = determineMirrorGain_() ;					
		break;

	case PatientCctTypeValue::NEONATAL_CIRCUIT :
		// $[TI4]
		finalPressure = effectivePressure_ - deltaPe_ + peep_ ;
		
		Real32 rampPressure ;
		
		if (elapsedTimeMs_ < transitionTimeMs_)
		{
			// $[TI5]
			rampPressure =
				 (((finalPressure - initialRampPress_) * elapsedTimeMs_) /
							transitionTimeMs_) + initialRampPress_;
		}
		else
		{
			// $[TI6]
			rampPressure = finalPressure ;
		}

		trajectory_ = (1.0F - neoTrajectoryAlpha_) * rampPressure +
					   neoTrajectoryAlpha_ * trajectory_ ;

		if (trajectory_ < finalPressure - 0.5)
		{
			// $[TI9]
			RExhValveIController.setIsOpenLoopControl( TRUE) ;
		}
		else
		{
			// $[TI10]
			RExhValveIController.setIsOpenLoopControl( FALSE) ;
		}

		RPressureExpTrigger.updateTargetPressure( finalPressure) ;
		RDeliveredFlowExpTrigger.setEffectivePressure( effectivePressure_ - deltaPe_) ;
		mirrorGain = 0.0 ;
		break;

	default :
		// unexpected circuit type value...
		AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE);
		break;
	}
	
	RHighCircuitPressureExpTrigger.updateTargetPressure( effectivePressure_ + peep_) ;
	
	Real32 refTarget = trajectory_ + (trajectory_ - desPressFilter_) * mirrorGain ;

	if (PATIENT_CCT_TYPE == PatientCctTypeValue::PEDIATRIC_CIRCUIT ||
			PATIENT_CCT_TYPE == PatientCctTypeValue::ADULT_CIRCUIT)
	{
		// $[TI11]
		determineWf_( refTarget) ;
	}
	// $[TI12]
	
	RPressureController.determineErrorGain( refTarget, targetPressure_) ;
	
	updateKpGain_( refTarget, targetPressure_) ;		
	
	Real32 wf = RPressureController.getWf() ;
	Real32 desiredPressure = 0 ;
	
	if (IsEquivalent( wf, 0.0, HUNDREDTHS))
	{
	  	// $[TI1.1]
		desiredPressure = refTarget ;
	}
	else if (IsEquivalent( wf, 1.0, HUNDREDTHS))
	{
	  	// $[TI1.2]
		desiredPressure = targetPressure_ ;
	}
	else
	{
		CLASS_PRE_CONDITION( IsEquivalent( wf, 0.0, HUNDREDTHS)
							 || IsEquivalent( wf, 1.0, HUNDREDTHS)) ;
	}

	updateFlowControllerFlags_() ;
    RPressureController.updatePsol( desiredPressure, effectivePressure_ + peep_, elapsedTimeMs_) ;
	RDeliveredFlowExpTrigger.setDesiredFlow( RAirFlowController.getDesiredFlow()
											 + RO2FlowController.getDesiredFlow()) ;
	RDeliveredFlowExpTrigger.setPressureTrajectory( trajectory_) ;
	RDisconnectTrigger.setDesiredFlow( RAirFlowController.getDesiredFlow()
											 + RO2FlowController.getDesiredFlow()) ;
	RLungFlowExpTrigger.setDesiredFlow( RAirFlowController.getDesiredFlow()
											 + RO2FlowController.getDesiredFlow()) ;
	Real32 exhValveDesPressure = trajectory_ ;

    if (trajectory_ < 0.0F)
    {
	  	// $[TI2.1]
	    exhValveDesPressure = 0.0F ;
	} 	// implied else $[TI2.2]

	if (PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
	{
		// $[TI7]
	    exhValveDesPressure = finalPressure ;
	} 	// $[TI8]

	RDeliveredFlowExpTrigger.setReliefFlowTarget( DesiredReliefFlow_) ;

	RExhValveIController.updateExhalationValve( exhValveDesPressure, DesiredReliefFlow_) ;

    elapsedTimeMs_ += CYCLE_TIME_MS ;

	RLeakCompMgr.setEffectivePressure(effectivePressure_);
	RLeakCompMgr.setPressureTrajectory(trajectory_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called at the beginning of each pressure based inspiration to update
//      parameters needed for inspiration.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Obtain the values of the data members.  rPressureController and
//		rExhValveController newBreath() are called to initialize the controllers.
// 		$[04031] $[04035] $[04174] $[04175] $[04176] $[04177] $[04180] $[04182]
//		$[04183] $[04211] $[04258] $[04258] $[04330]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PressureBasedPhase::newBreath( void)
{
	CALL_TRACE("PressureBasedPhase::newBreath( void)") ;

	determineEffectivePressureAndBiasOffset_() ;
	determineTimeToTarget_() ;
	determineFlowAccelerationPercent_() ;
	determineTargetPressure_() ;

	overshootOccured_ = FALSE ;
		
	desPressAlpha_ = 0.98F ;
	
	desPressFilter_ = RExhPressureSensor.getValue() ;


    const DiscreteValue  CIRCUIT_TYPE =
        PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	if (fap_ > 90.0)
	{
		fap_ = 90.0 + 0.5 * (fap_ - 90.0);
	}

	if (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
	{
	  	// $[TI6]
		static const Real32 A = 0.9489 ;
		static const Real32 B = 0.0905 ;

		Real32 temp = (A + B * fap_) ;

		temp *= temp ;
		
		if (temp < 1.0)
		{
		  	// $[TI7]
			temp = 1.0 ;
		}
		else if (temp > 100.0)
		{
		  	// $[TI8]
			temp = 100.0 ;
		}	// $[TI9]
		
		fap_ = temp ;
	}
  	// $[TI10]
	
	// to reduce the number of run-time divides...
	static const Real32  INVERSE_FACTOR1_ = (1.0F / 99.0F);
	static const Real32  INVERSE_FACTOR2_ = (1.0F / 3.0F);

	const Real32  TAU = (50.0F - ((timeToTarget_ - 50.0F) * INVERSE_FACTOR1_) *
							(fap_ - 100.0F)) * INVERSE_FACTOR2_ ;

	trajectoryAlpha_ = (Real32) (exp( -CYCLE_TIME_MS / TAU)) ;

	peep_ = RPeep.getActualPeep() ;

	Trigger::TriggerId triggerId = ((const Trigger*)PBreathTrigger_)->getId() ;
	
	if (triggerId == Trigger::PRESSURE_BACKUP_INSP
			|| triggerId == Trigger::PRESSURE_INSP
		    || triggerId == Trigger::P100_COMPLETE)
	{
	  	// $[TI1.1]
		Real32 sensitivity = BASE_FLOW_MIN ;
		
		if (triggerId == Trigger::PRESSURE_BACKUP_INSP)
		{
		  	// $[TI2.1]
			sensitivity = BACKUP_PRESSURE_SENS_VALUE ;
		}
		else if (triggerId == Trigger::PRESSURE_INSP)
		{			
		  	// $[TI2.2]
			sensitivity = PhasedInContextHandle::GetBoundedValue( SettingId::PRESS_SENS).value ;
		}
		else if (triggerId == Trigger::P100_COMPLETE)
		{
		    // $[RM24009] After P100 maneuver, initialize sensitivity to... 
			sensitivity = MAX_VALUE(1.0F, -RP100Maneuver.getP100Pressure());
		}
			 
		trajectory_ = peep_ - sensitivity ;
		mirrorGainSensitivity_ = sensitivity ;
	}
	else
	{
	  	// $[TI1.2]
		trajectory_ = peep_ ;
		mirrorGainSensitivity_ = 1.0F ;
	}

	if (RPeep.isPeepChange())
	{
	  	// $[TI4]
		trajectory_ = RExhPressureSensor.getValue() ;
		BreathRecord::SetIsPeepTransition( TRUE) ;
	}
	else
	{
	  	// $[TI5]
		BreathRecord::SetIsPeepTransition( FALSE) ;
	}

    RPeep.setPeepChange( FALSE) ;

	determineKp_() ;
	determineKi_() ;
	
	isDecrementAlpha_ = FALSE ;	
	isPatPressLessThanTraj_ = FALSE ;
	isFreezeGains_ = FALSE ;
    elapsedTimeMs_ = 0 ;

	if (this != (PressureBasedPhase*)&ROscPcvPhase)
	{
	  	// $[TI3.1]
		((Trigger&)RHighCircuitPressureExpTrigger).enable() ;
	}	// implied else $[TI3.2]

	Real32 	alpha = exp( -CYCLE_TIME_MS / (1.0*TAU)) ;
    RPressureController.newBreath( kp_, ki_, alpha ) ;
    
 
	if ( (this == (PressureBasedPhase*)&RVtpcvPhase) ||
		 (this == (PressureBasedPhase*)&RVsvPhase) )
         {
             // $[TI3.3]
             ((BreathTrigger&)RLungVolumeExpTrigger).enable() ;
         }    // implied else $[TI3.4]



    // updatePressureControllerWf_() must be called AFTER
    // rPressureController.newBreath() to avoid newBreath()
    // overwriting the data member.

	updatePressureControllerWf_() ;
       
    RExhValveIController.newBreath() ;
	AdiabaticFactor_ = 1.0 ;

	deltaPe_ = ExhResistance_.calculateDeltaP( DesiredReliefFlow_) ;

	const Real32  PRESSURE_FACTOR =	MAX_VALUE (0.1F, (0.01F * (fap_ - 1.0F))) ;
	initialRampPress_ =	peep_ + (effectivePressure_ - deltaPe_) * PRESSURE_FACTOR ;

	transitionTimeMs_ =	(3.0F * trajectoryAlpha_ * CYCLE_TIME_MS) /
								(1 - trajectoryAlpha_) ;

	neoTrajectoryAlpha_ = (0.97f * trajectoryAlpha_) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PressureBasedPhase::SoftFault( const SoftFaultID  softFaultID,
                   			   const Uint32       lineNumber,
		   					   const char*        pFileName,
		   					   const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PRESSUREBASEDPHASE,
                             lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineTargetPressure_()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to determine the target pressure.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The targetPressure_ is set to PRESSURE_THRESHOLD.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PressureBasedPhase::determineTargetPressure_( void)
{
	CALL_TRACE("PressureBasedPhase::determineTargetPressure_( void)") ;

  	// $[TI1]
	targetPressure_ = PRESSURE_THRESHOLD ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineKp_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called to update the proportional gain.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The proportional gain value is dependent upon patient circuit type.
// 		$[04183]	
//---------------------------------------------------------------------
//@ PreCondition
//		patientCircuitType == PatientCctTypeValue::ADULT_CIRCUIT
//			|| patientCircuitType == PatientCctTypeValue::PEDIATRIC_CIRCUIT
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PressureBasedPhase::determineKp_( void)
{
	CALL_TRACE("PressureBasedPhase::determineKp_( void)") ;

    const DiscreteValue  CIRCUIT_TYPE =
        PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

    switch (CIRCUIT_TYPE)
    {
    case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
	  	// $[TI2]
		kp_ = pediatricKp_ ;
        break;
    case PatientCctTypeValue::ADULT_CIRCUIT :
	  	// $[TI1]
		kp_ = adultKp_ ;
        break;
    case PatientCctTypeValue::NEONATAL_CIRCUIT :
	  	// $[TI3]
		kp_ = 2.0F ;
        break;
    default :
        AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
        break;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePressureControllerWf_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This base class
//		method has no code since it is a provided to meet the derived classes
//		requirements.  The derived classes may overload this method.    
//---------------------------------------------------------------------
//@ Implementation-Description
//		None.  
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PressureBasedPhase::updatePressureControllerWf_( void)
{
	CALL_TRACE("PressureBasedPhase::updatePressureControllerWf_( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineMirrorGain_( void)
//
//@ Interface-Description
//      This method has no arguments and returns the mirror gain.  This method
//      is called to obtain the mirrorGain.  This method must be called AFTER
//      the desired pressure filter and trajectory are determined.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The mirror gain is dependent on conditions with respect to
//		desPressFilter_ and trajectory_	values.
// 		$[04176] $[04177] $[04178]	
//---------------------------------------------------------------------
//@ PreCondition
//		mirrorGainSensitivity_	+ effectivePressure_ - biasOffset_ > 0 
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
PressureBasedPhase::determineMirrorGain_( void)
{
	CALL_TRACE("PressureBasedPhase::determineMirrorGain_( void)") ;
	
	Real32 mirrorGain ;
	
	if (desPressFilter_ <= trajectory_)
	{
	  	// $[TI1.1]
		CLASS_PRE_CONDITION( mirrorGainSensitivity_	+ effectivePressure_ - biasOffset_ > 0) ;

		mirrorGain = 1.0F
			 / (mirrorGainSensitivity_ + effectivePressure_ - biasOffset_) ;
	}
	else
	{
	  	// $[TI1.2]
		mirrorGain = 0.0F ;
	}

	if (mirrorGain > 1.0F)
	{
	  	// $[TI2.1]
		mirrorGain = 1.0F ;
	}	// implied else $[TI2.2]

	return ( mirrorGain) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateKpGain_()
//
//@ Interface-Description
//      This method has a reference target and a target pressure as arguments and
//		has no return value.  This method is called to update the proportional
//		gain in the pressure controller object.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The proportional gain is set to zero when certain conditions are
//		satisfied.  The overshootOccured_ flag is set if conditions are
//		met.
//		$ [04307]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PressureBasedPhase::updateKpGain_( const Real32 refTarget,
								   const Real32 targetPressure)
{
	CALL_TRACE("PressureBasedPhase::updateKpGain_( const Real32 refTarget, \
  			    const Real32 targetPressure)") ;

	Real32 patPressExh = RExhPressureSensor.getValue() ;
	Real32 patPressInsp = RInspPressureSensor.getValue() ;

	if (patPressInsp > targetPressure)
	{
	  	// $[TI1.1]
		overshootOccured_ = TRUE ;
	} 	// implied else $[TI1.2]

	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	if ( (PATIENT_CCT_TYPE == PatientCctTypeValue::PEDIATRIC_CIRCUIT ||
				PATIENT_CCT_TYPE == PatientCctTypeValue::ADULT_CIRCUIT) &&
		(patPressExh > refTarget
			|| (overshootOccured_ && patPressInsp < targetPressure)))
	{
	  	// $[TI2.1]
		RPressureController.setKp( 0.0) ;
 	} 	// $[TI2.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineKi_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called to update the integral gain.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The integral gain value is dependent upon patient circuit type.
// 		$[04183]	
//---------------------------------------------------------------------
//@ PreCondition
//		patientCircuitType == PatientCctTypeValue::ADULT_CIRCUIT
//			|| patientCircuitType == PatientCctTypeValue::PEDIATRIC_CIRCUIT
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PressureBasedPhase::determineKi_( void)
{
	CALL_TRACE("PressureBasedPhase::determineKi_( void)") ;

    const DiscreteValue  CIRCUIT_TYPE =
        PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

    switch (CIRCUIT_TYPE)
    {
    case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
	  	// $[TI2]
		ki_ = pediatricKi_ ;
        break;
    case PatientCctTypeValue::ADULT_CIRCUIT :
	  	// $[TI1]
		ki_ = adultKi_ ;
        break;
    case PatientCctTypeValue::NEONATAL_CIRCUIT :
	  	// $[TI3]
		ki_ = 0.05F ;
        break;
    default :
        AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
        break;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineWf_()
//
//@ Interface-Description
//      This method has reference target as an argument and has no return value.
//		This method is called to update the weighting factor of the pressure
//		controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//		rPressureController.setWf() is called to set either 1.0 or 0.0
//		depending on the conditions met, else left unchanged.
//		$[04305]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PressureBasedPhase::determineWf_( const Real32 refTarget)
{
	CALL_TRACE("PressureBasedPhase::determineWf_( const Real32 refTarget)") ;
	
	Real32 patPressExh = RExhPressureSensor.getValue() ;
	Real32 patPressInsp = RInspPressureSensor.getValue() ;

	Real32 errorGain = RPressureController.getErrorGain() ;
	Real32 result1 = (patPressInsp - targetPressure_) * errorGain ;
	Real32 result2 = (patPressExh - refTarget) * 2.0F ;

	if (patPressInsp > targetPressure_ && result1 > result2)
	{
	  	// $[TI1.1]
		RPressureController.setWf( 1.0F) ;
	}
	else if (patPressExh > refTarget &&	result1 <= result2)
	{
	  	// $[TI1.2]
		RPressureController.setWf( 0.0F) ;
	}	// implied else $[TI1.3]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineDesPressFilter_()
//
//@ Interface-Description
//		This method has exhalation pressure as an argument and has no return
//		value.  This method is called to determine the desired pressure
//		filter value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The	desPressFilter_ is computed.  The desPressAlpha_ and isDecrementAlpha_
//		are modified when conditions are met.  
//		$ [04174]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PressureBasedPhase::determineDesPressFilter_( Real32 patPressExh)
{
	CALL_TRACE("PressureBasedPhase::determineDesPressFilter_( Real32 patPressExh)") ;
	
	if (!isDecrementAlpha_)
	{
	  	// $[TI1.1]
		Real32 filteredPatPress = RExhPressureSensor.getFilteredValue() ;

		if (filteredPatPress > trajectory_ && elapsedTimeMs_ >= 10)
		{
		  	// $[TI2.1]
			isDecrementAlpha_ = TRUE ;
		}	// implied else $[TI2.2]
	}
	else
	{
	  	// $[TI1.2]
		desPressAlpha_ -= 0.01F ;
		
		if (desPressAlpha_ < MIN_ALPHA)
		{
		  	// $[TI3.1]
			desPressAlpha_ = MIN_ALPHA ;
		} 	// implied else $[TI3.2]
	}

	desPressAlpha_ = MIN_ALPHA ;
	desPressFilter_ = desPressAlpha_ * desPressFilter_
							+ (1.0F - desPressAlpha_) * patPressExh ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPediatricKp()
//
//@ Interface-Description
//      Takes in a floating point gain, returns nothing
//---------------------------------------------------------------------
//@ Implementation-Description
//      Sets the kp gain used for pediatric patient circuits
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
PressureBasedPhase::setPediatricKp( Real32 gain )
{
    CALL_TRACE("PressureBasedPhase::setPediatricKp( Real32 gain )") ;
    pediatricKp_ = gain;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAdultKp()
//
//@ Interface-Description
//      Takes in a floating point gain, returns nothing
//---------------------------------------------------------------------
//@ Implementation-Description
//      Sets the kp gain used for adult patient circuits
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
PressureBasedPhase::setAdultKp( Real32 gain )
{
    CALL_TRACE("PressureBasedPhase::setAdultKp( Real32 gain )") ;
    adultKp_ = gain;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPediatricKi()
//
//@ Interface-Description
//      Takes in a floating point gain, returns nothing
//---------------------------------------------------------------------
//@ Implementation-Description
//      Sets the ki gain used for pediatric patient circuits
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
PressureBasedPhase::setPediatricKi( Real32 gain )
{
    CALL_TRACE("PressureBasedPhase::setPediatricKi( Real32 gain )") ;
    pediatricKi_ = gain;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAdultKi()
//
//@ Interface-Description
//      Takes in a floating point gain, returns nothing
//---------------------------------------------------------------------
//@ Implementation-Description
//      Sets the ki gain used for adult patient circuits
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
PressureBasedPhase::setAdultKi( Real32 gain )
{
    CALL_TRACE("PressureBasedPhase::setAdultKi( Real32 gain )") ;
    adultKi_ = gain;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPediatricKp()
//
//@ Interface-Description
//      Takes in nothing, returns floating point gain
//---------------------------------------------------------------------
//@ Implementation-Description
//      Returns kp gain used for pediatric patient circuits
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
PressureBasedPhase::getPediatricKp()
{
    CALL_TRACE("PressureBasedPhase::getPediatricKp()") ;
    return (pediatricKp_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAdultKp()
//
//@ Interface-Description
//      Takes in nothing, returns floating point gain
//---------------------------------------------------------------------
//@ Implementation-Description
//      Returns kp gain used for adult patient circuits
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
PressureBasedPhase::getAdultKp()
{
    CALL_TRACE("PressureBasedPhase::getAdultKp()") ;
    return (adultKp_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPediatricKi()
//
//@ Interface-Description
//      Takes in nothing, returns floating point gain
//---------------------------------------------------------------------
//@ Implementation-Description
//      Returns the kp gain used for pediatric patient circuits
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
PressureBasedPhase::getPediatricKi()
{
    CALL_TRACE("PressureBasedPhase::getPediatricKi()") ;
    return (pediatricKi_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAdultKi()
//
//@ Interface-Description
//      Takes in nothing, returns floating point gain
//---------------------------------------------------------------------
//@ Implementation-Description
//      Returns the ki gain used for adult patient circuits
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
PressureBasedPhase::getAdultKi()
{
    CALL_TRACE("PressureBasedPhase::getAdultKi()") ;
    return (adultKi_);
}
