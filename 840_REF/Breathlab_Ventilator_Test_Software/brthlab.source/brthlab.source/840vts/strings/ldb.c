/*****************************************************************************/
/*   Module Name:    ldb.c                                             
/*   Purpose:       Language Database (ldb) support functions                                  
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Changed the Alarm840.Log               RHJ               29 Feb,2008    
/*   to Alrm840.LOG.                                                         
/*
/*   Removed MetabolicPortTest strings      Rhomere Jimenez   27 Nov, 2007 
/*   and added Alarm Log strings.     
/*
/*   Removed Ground DC & AC volts from      Rhomere Jimenez   03 Nov, 2005
/*   Manual Test screens, and reports. 
/*   
/*	 Selected Reports to preview or delete
/* 	 title and confirm message strings are	Lakshmi(CGS)      5 Dec.2000
/*   added. 
/*
/*   Changed language file identifiers      Dave Richardson   20 nov, 2002
/*   from ENG (English, Great Britain)
/*   to ENU (English, US) and
/*   from SPA (made-up term)
/*   to ESP (Spanish, Common) so as to 
/*   comply with ISO standards
/*
/*   Commented-out, but not yet deleted
/*   message strings from National Instruments
/*   Test Executive that were left behind
/*   in rev 'D' program.
/*
/*   Revised text for units-of-measure
/*   to match international standards
/*
/*   Added message text for ventilator
/*   software download feature
/*
/*   Requirements:                                     
/*                                                                           
/*   $Revision::                                              
/*                                                                          
/*   $Log:   //taplea-ww01/swqa/Data/arc/840_CPVT_VTS/Program/strings/ldb.c-arc  $
 * 
 *    Rev 1.3   Aug 11 2006 12:44:52   Jimenez
 * Updated for Rev F.
 * 
 *    Rev 1.2   04 Dec 2002 15:36:16   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   20 Nov 2002 10:48:44   Richardson
 * Updated per SCRs # 26, 29, 37, 41
/*
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <windows.h>
#include <ansi_c.h>
#include <userint.h>

#include "ldb.h"

/***********************************************/
/*************** GLOBAL VARIABLES **************/
/***********************************************/

//int CurrLanguage=FIRST_LANG;
int CurrLanguage = ENGLISH;
int nLanguageID  = 0x09;

static char cb_E_string[] = "English";
static char cb_S_string[] = "Spanish";
static char cb_F_string[] = "French";
static char cb_G_string[] = "German";
static char cb_I_string[] = "Italian";
static char cb_P_string[] = "Portuguese";

// TitleBar Program Name
static char cb_E_Ldb_ProgramName[] = "BreathLab PTS 840 Ventilator Test Software";
static char cb_S_Ldb_ProgramName[] = "Software de prueba del ventilador BreathLab PTS 840";
static char cb_F_Ldb_ProgramName[] = "Logiciel de test BreathLab PTS pour ventilateur 840";
static char cb_G_Ldb_ProgramName[] = "Beatmungsger�te-Testsoftware BreathLab PTS 840";
static char cb_I_Ldb_ProgramName[] = "Software di prova del ventilatore BreathLab PTS 840";
static char cb_P_Ldb_ProgramName[] = "Software de Teste do Ventilador BreathLab PTS 840";

// Splash Program Name
static char cb_E_Ldb_Splash_ProgramName[] = "840� Ventilator Test Software";
static char cb_S_Ldb_Splash_ProgramName[] = "Software de prueba del ventilador 840�";
static char cb_F_Ldb_Splash_ProgramName[] = "Logiciel de test du ventilateur 840�";
static char cb_G_Ldb_Splash_ProgramName[] = "Beatmungsger�t 840� Testsoftware";
static char cb_I_Ldb_Splash_ProgramName[] = "Software di prova del ventilatore 840�";
static char cb_P_Ldb_Splash_ProgramName[] = "Software de Teste do Ventilador 840�";

// Copyright
static char cb_E_Ldb_Copyright_Str[] = "Copyright � 1999-2008 Covidien";
static char cb_S_Ldb_Copyright_Str[] = "Copyright � 1999-2008 Covidien";
static char cb_F_Ldb_Copyright_Str[] = "Copyright � 1999-2008 Covidien";
static char cb_G_Ldb_Copyright_Str[] = "Copyright � 1999-2008 Covidien";
static char cb_I_Ldb_Copyright_Str[] = "Copyright � 1999-2008 Covidien";
static char cb_P_Ldb_Copyright_Str[] = "Copyright � 1999-2008 Covidien";

// Rights Reserved
static char cb_E_Ldb_Copyright_Rights[] = "All Rights Reserved.";
static char cb_S_Ldb_Copyright_Rights[] = "Reservados todos los derechos.";
static char cb_F_Ldb_Copyright_Rights[] = "Tous droits r�serv�s.";
static char cb_G_Ldb_Copyright_Rights[] = "Alle Rechte vorbehalten.";
static char cb_I_Ldb_Copyright_Rights[] = "Tutti i diritti riservati.";
static char cb_P_Ldb_Copyright_Rights[] = "Todos os direitos reservados.";

// Splash Eval Msg
static char cb_E_Ldb_Splash_Eval_Msg[] = "BETA TEST VERSION, FOR EVALUATION ONLY";
static char cb_S_Ldb_Splash_Eval_Msg[] = "VERSI�N BETA, S�LO PARA EVALUACI�N";
static char cb_F_Ldb_Splash_Eval_Msg[] = "VERSION B�TA, SEULEMENT POUR �VALUATION";
static char cb_G_Ldb_Splash_Eval_Msg[] = "BETATESTVERSION, NUR F�R TESTZWECKE";
static char cb_I_Ldb_Splash_Eval_Msg[] = "VERSIONE BETA, SOLO PER VALUTAZIONE";
static char cb_P_Ldb_Splash_Eval_Msg[] = "VERS�O BETA, APENAS PARA AVALIA��O";

// SPLASH_COMSTATUS
static char cb_E_Ldb_Cncling_Pls_Wait[] = "Canceling... Please wait.";
static char cb_S_Ldb_Cncling_Pls_Wait[] = "Cancelando... Espere.";
static char cb_F_Ldb_Cncling_Pls_Wait[] = "Annulation en cours... Veuillez patienter.";
static char cb_G_Ldb_Cncling_Pls_Wait[] = "Abbruch... Bitte warten.";
static char cb_I_Ldb_Cncling_Pls_Wait[] = "Annullamento in corso... Attendere.";
static char cb_P_Ldb_Cncling_Pls_Wait[] = "Cancelando... Aguarde.";

// Language names
static char cb_E_Ldb_English[] = "English";
static char cb_S_Ldb_English[] = "English"; // "Ingl�s";
static char cb_F_Ldb_English[] = "English"; // "Anglais";
static char cb_G_Ldb_English[] = "English"; // "Englisch"; 
static char cb_I_Ldb_English[] = "English"; // "Inglese"; 
static char cb_P_Ldb_English[] = "English"; // "Ingl�s";

static char cb_E_Ldb_Spanish[] = "Spanish";
static char cb_S_Ldb_Spanish[] = "Spanish"; // "Espa�ol";
static char cb_F_Ldb_Spanish[] = "Spanish"; // "Espagnol";
static char cb_G_Ldb_Spanish[] = "Spanish"; // "Spanisch"; 
static char cb_I_Ldb_Spanish[] = "Spanish"; // "Spagnolo";
static char cb_P_Ldb_Spanish[] = "Spanish"; // "Espanhol";

static char cb_E_Ldb_French[] = "French";
static char cb_S_Ldb_French[] = "French"; // "Franc�s";
static char cb_F_Ldb_French[] = "French"; // "Fran�ais";
static char cb_G_Ldb_French[] = "French"; // "Franz�sisch"; 
static char cb_I_Ldb_French[] = "French"; // "Francese"; 
static char cb_P_Ldb_French[] = "French"; // "Franc�s";

static char cb_E_Ldb_Italian[] = "Italian";
static char cb_S_Ldb_Italian[] = "Italian"; // "Italiano";
static char cb_F_Ldb_Italian[] = "Italian"; // "Italien";
static char cb_G_Ldb_Italian[] = "Italian"; // "Italienisch"; 
static char cb_I_Ldb_Italian[] = "Italian"; // "Italiano"; 
static char cb_P_Ldb_Italian[] = "Italian"; // "Italiano";

static char cb_E_Ldb_German[] = "German";
static char cb_S_Ldb_German[] = "German"; // "Alem�n";
static char cb_F_Ldb_German[] = "German"; // "Allemand";
static char cb_G_Ldb_German[] = "German"; // "Deutsch"; 
static char cb_I_Ldb_German[] = "German"; // "Tedesco";
static char cb_P_Ldb_German[] = "German"; // "Alem�o";

static char cb_E_Ldb_Portuguese[] = "Portuguese";
static char cb_S_Ldb_Portuguese[] = "Portuguese"; // "Portugu�s";
static char cb_F_Ldb_Portuguese[] = "Portuguese"; // "Portugais";
static char cb_G_Ldb_Portuguese[] = "Portuguese"; // "Portugiesisch";
static char cb_I_Ldb_Portuguese[] = "Portuguese"; // "Portoghese";
static char cb_P_Ldb_Portuguese[] = "Portuguese"; // "Portugu�s";

static char cb_E_Ldb_RSE_Dir[] = "c:\\brthlab\\840vts\\logs\\72362.log"; // DO NOT TRANSLATE
static char cb_S_Ldb_RSE_Dir[] = "c:\\brthlab\\840vts\\logs\\72362.log";
static char cb_F_Ldb_RSE_Dir[] = "c:\\brthlab\\840vts\\logs\\72362.log";
static char cb_G_Ldb_RSE_Dir[] = "c:\\brthlab\\840vts\\logs\\72362.log";
static char cb_I_Ldb_RSE_Dir[] = "c:\\brthlab\\840vts\\logs\\72362.log";
static char cb_P_Ldb_RSE_Dir[] = "c:\\brthlab\\840vts\\logs\\72362.log";


static char cb_E_Ldb_CSE_Dir[] = "c:\\brthlab\\840vts\\logs"; // DO NOT TRANSLATE
static char cb_S_Ldb_CSE_Dir[] = "c:\\brthlab\\840vts\\logs";
static char cb_F_Ldb_CSE_Dir[] = "c:\\brthlab\\840vts\\logs";
static char cb_G_Ldb_CSE_Dir[] = "c:\\brthlab\\840vts\\logs";
static char cb_I_Ldb_CSE_Dir[] = "c:\\brthlab\\840vts\\logs";
static char cb_P_Ldb_CSE_Dir[] = "c:\\brthlab\\840vts\\logs";

static char cb_E_Ldb_CSE_File[] = "c:\\sv01\\sv01.exe"; // DO NOT TRANSLATE
static char cb_S_Ldb_CSE_File[] = "c:\\sv01\\sv01.exe";
static char cb_F_Ldb_CSE_File[] = "c:\\sv01\\sv01.exe";
static char cb_G_Ldb_CSE_File[] = "c:\\sv01\\sv01.exe";
static char cb_I_Ldb_CSE_File[] = "c:\\sv01\\sv01.exe";
static char cb_P_Ldb_CSE_File[] = "c:\\sv01\\sv01.exe";

// report.c
// CheckForNoNameLog
static char cb_E_Ldb_DiagLog_Def_Path[] = "C:\\BRTHLAB\\840VTS"; // DO NOT TRANSLATE
static char cb_S_Ldb_DiagLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_F_Ldb_DiagLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_G_Ldb_DiagLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_I_Ldb_DiagLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_P_Ldb_DiagLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";

static char cb_E_Ldb_DiagLog_Def_File[] = "DIAGLOG.LOG"; // DO NOT TRANSLATE
static char cb_S_Ldb_DiagLog_Def_File[] = "DIAGLOG.LOG";
static char cb_F_Ldb_DiagLog_Def_File[] = "DIAGLOG.LOG";
static char cb_G_Ldb_DiagLog_Def_File[] = "DIAGLOG.LOG";
static char cb_I_Ldb_DiagLog_Def_File[] = "DIAGLOG.LOG";
static char cb_P_Ldb_DiagLog_Def_File[] = "DIAGLOG.LOG";

// HandleLogPrint()
static char cb_E_Ldb_CSE_DiagLog[] = "\\Sys840.LOG"; // DO NOT TRANSLATE
static char cb_S_Ldb_CSE_DiagLog[] = "\\Sys840.LOG";
static char cb_F_Ldb_CSE_DiagLog[] = "\\Sys840.LOG";
static char cb_G_Ldb_CSE_DiagLog[] = "\\Sys840.LOG";
static char cb_I_Ldb_CSE_DiagLog[] = "\\Sys840.LOG";
static char cb_P_Ldb_CSE_DiagLog[] = "\\Sys840.LOG";

static char cb_E_Ldb_TestLog_Def_Path[] = "C:\\BRTHLAB\\840VTS"; // DO NOT TRANSLATE
static char cb_S_Ldb_TestLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_F_Ldb_TestLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_G_Ldb_TestLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_I_Ldb_TestLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_P_Ldb_TestLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";

static char cb_E_Ldb_TestLog_Def_File[] = "TESTLOG.LOG"; // DO NOT TRANSLATE
static char cb_S_Ldb_TestLog_Def_File[] = "TESTLOG.LOG";
static char cb_F_Ldb_TestLog_Def_File[] = "TESTLOG.LOG";
static char cb_G_Ldb_TestLog_Def_File[] = "TESTLOG.LOG";
static char cb_I_Ldb_TestLog_Def_File[] = "TESTLOG.LOG";
static char cb_P_Ldb_TestLog_Def_File[] = "TESTLOG.LOG";


static char cb_E_Ldb_AlarmLog_Def_Path[] = "C:\\BRTHLAB\\840VTS"; // DO NOT TRANSLATE
static char cb_S_Ldb_AlarmLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_F_Ldb_AlarmLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_G_Ldb_AlarmLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_I_Ldb_AlarmLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_P_Ldb_AlarmLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";

static char cb_E_Ldb_AlarmLog_Def_File[] = "ALRMLOG.LOG"; // DO NOT TRANSLATE
static char cb_S_Ldb_AlarmLog_Def_File[] = "ALRMLOG.LOG";
static char cb_F_Ldb_AlarmLog_Def_File[] = "ALRMLOG.LOG";
static char cb_G_Ldb_AlarmLog_Def_File[] = "ALRMLOG.LOG";
static char cb_I_Ldb_AlarmLog_Def_File[] = "ALRMLOG.LOG";
static char cb_P_Ldb_AlarmLog_Def_File[] = "ALRMLOG.LOG";

// HandleLogPrint()
static char cb_E_Ldb_CSE_TestLog[] = "\\St840.LOG"; // DO NOT TRANSLATE
static char cb_S_Ldb_CSE_TestLog[] = "\\St840.LOG";
static char cb_F_Ldb_CSE_TestLog[] = "\\St840.LOG";
static char cb_G_Ldb_CSE_TestLog[] = "\\St840.LOG";
static char cb_I_Ldb_CSE_TestLog[] = "\\St840.LOG";
static char cb_P_Ldb_CSE_TestLog[] = "\\St840.LOG";

static char cb_E_Ldb_InfoLog_Def_Path[] = "C:\\BRTHLAB\\840VTS"; // DO NOT TRANSLATE
static char cb_S_Ldb_InfoLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_F_Ldb_InfoLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_G_Ldb_InfoLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_I_Ldb_InfoLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";
static char cb_P_Ldb_InfoLog_Def_Path[] = "C:\\BRTHLAB\\840VTS";

static char cb_E_Ldb_InfoLog_Def_File[] = "INFOLOG.LOG"; // DO NOT TRANSLATE
static char cb_S_Ldb_InfoLog_Def_File[] = "INFOLOG.LOG";
static char cb_F_Ldb_InfoLog_Def_File[] = "INFOLOG.LOG";
static char cb_G_Ldb_InfoLog_Def_File[] = "INFOLOG.LOG";
static char cb_I_Ldb_InfoLog_Def_File[] = "INFOLOG.LOG";
static char cb_P_Ldb_InfoLog_Def_File[] = "INFOLOG.LOG";

// HandleLogPrint()
static char cb_E_Ldb_CSE_InfoLog[] = "\\Info840.LOG"; // DO NOT TRANSLATE
static char cb_S_Ldb_CSE_InfoLog[] = "\\Info840.LOG";
static char cb_F_Ldb_CSE_InfoLog[] = "\\Info840.LOG";
static char cb_G_Ldb_CSE_InfoLog[] = "\\Info840.LOG";
static char cb_I_Ldb_CSE_InfoLog[] = "\\Info840.LOG";
static char cb_P_Ldb_CSE_InfoLog[] = "\\Info840.LOG";

static char cb_E_Ldb_CSE_AlarmLog[] = "\\Alrm840.LOG"; // DO NOT TRANSLATE
static char cb_S_Ldb_CSE_AlarmLog[] = "\\Alrm840.LOG";
static char cb_F_Ldb_CSE_AlarmLog[] = "\\Alrm840.LOG";
static char cb_G_Ldb_CSE_AlarmLog[] = "\\Alrm840.LOG";
static char cb_I_Ldb_CSE_AlarmLog[] = "\\Alrm840.LOG";
static char cb_P_Ldb_CSE_AlarmLog[] = "\\Alrm840.LOG";

static char cb_E_Ldb_Select_Diag_File[] = "Select Diagnostic Log File";
static char cb_S_Ldb_Select_Diag_File[] = "�Borrar registros del ventilador 840?";
static char cb_F_Ldb_Select_Diag_File[] = "S�lectionner fichier Journl diagnost";
static char cb_G_Ldb_Select_Diag_File[] = "Diagnose-Protokolldatei ausw�hlen";
static char cb_I_Ldb_Select_Diag_File[] = "Selezionare un file registro di diagnostica";
static char cb_P_Ldb_Select_Diag_File[] = "Selecionar arquivo de registro de diagn�stico";

static char cb_E_Ldb_Select_SysInfo_file[] = "Select System Information Log File";
static char cb_S_Ldb_Select_SysInfo_file[] = "Seleccionar archivo de registro de informaci�n del sistema";
static char cb_F_Ldb_Select_SysInfo_file[] = "S�lectionner fichier Journl diagnost communic";
static char cb_G_Ldb_Select_SysInfo_file[] = "Systeminformations-Protokolldatei ausw�hlen";
static char cb_I_Ldb_Select_SysInfo_file[] = "Selezionare un registro dati di sistema";
static char cb_P_Ldb_Select_SysInfo_file[] = "Selecionar arquivo de registro de informa��es do sistema";

static char cb_E_Ldb_Select_testlog_file[] = "Select Test Log File";
static char cb_S_Ldb_Select_testlog_file[] = "Seleccionar archivo de registro de prueba";
static char cb_F_Ldb_Select_testlog_file[] = "S�lectionner fichier Journl test";
static char cb_G_Ldb_Select_testlog_file[] = "Test-Protokolldatei ausw�hlen";
static char cb_I_Ldb_Select_testlog_file[] = "Selezionare un registro de test";
static char cb_P_Ldb_Select_testlog_file[] = "Selecionar arquivo de registro de teste";


static char cb_E_Ldb_Select_alarmlog_file[] = "Select Alarm Log File";
static char cb_S_Ldb_Select_alarmlog_file[] = "Seleccionar archivo de registro de Alarma";
static char cb_F_Ldb_Select_alarmlog_file[] = "S�lectionner fichier Journl Alarme";
static char cb_G_Ldb_Select_alarmlog_file[] = "Alarm-Protokolldatei ausw�hlen";
static char cb_I_Ldb_Select_alarmlog_file[] = "Selezionare un registro de Allarme";
static char cb_P_Ldb_Select_alarmlog_file[] = "Selecionar arquivo de registro de Alarme";


static char cb_E_Ldb_rpt_FileName[] = "c:\\brthlab\\840vts\\report\\840VTS_ENU.rpt"; // DO NOT TRANSLATE
static char cb_S_Ldb_rpt_FileName[] = "c:\\brthlab\\840vts\\report\\840VTS_ESP.rpt";
static char cb_F_Ldb_rpt_FileName[] = "c:\\brthlab\\840vts\\report\\840VTS_FRA.rpt";
static char cb_G_Ldb_rpt_FileName[] = "c:\\brthlab\\840vts\\report\\840VTS_DEU.rpt";
static char cb_I_Ldb_rpt_FileName[] = "c:\\brthlab\\840vts\\report\\840VTS_ITA.rpt";
static char cb_P_Ldb_rpt_FileName[] = "c:\\brthlab\\840vts\\report\\840VTS_PTB.rpt";

//static char cb_E_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\cpvtdb.mdb"; // DO NOT TRANSLATE
//static char cb_S_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\cpvtdb.mdb";
//static char cb_F_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\cpvtdb.mdb";
//static char cb_G_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\cpvtdb.mdb";
//static char cb_I_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\cpvtdb.mdb";
//static char cb_P_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\cpvtdb.mdb";

static char cb_E_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\840VTS_ENU.mdb"; // DO NOT TRANSLATE
static char cb_S_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\840VTS_ESP.mdb";
static char cb_F_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\840VTS_FRA.mdb";
static char cb_G_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\840VTS_DEU.mdb";
static char cb_I_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\840VTS_ITA.mdb";
static char cb_P_Ldb_DB_Name[] = "c:\\brthlab\\840vts\\database\\840VTS_PTB.mdb";

static char cb_E_Ldb_Cant_read_registry[] = "Unable to read information from the registry!";
static char cb_S_Ldb_Cant_read_registry[] = "No se puede leer informaci�n del registro.";
static char cb_F_Ldb_Cant_read_registry[] = "Impossible de lire les informations du registre!";
static char cb_G_Ldb_Cant_read_registry[] = "Kann Daten der Registrierung nicht lesen!";
static char cb_I_Ldb_Cant_read_registry[] = "Impossibile leggere i dati del registro!";
static char cb_P_Ldb_Cant_read_registry[] = "N�o � poss�vel ler informa��es do registro!";

// DB_Conf_CB
static char cb_E_Ldb_DB_Delete_title[] = "Erase test results database";
static char cb_S_Ldb_DB_Delete_title[] = "Borrar la base de datos de resultados de prueba";
static char cb_F_Ldb_DB_Delete_title[] = "Effac base des r�sultats de tests";
static char cb_G_Ldb_DB_Delete_title[] = "Testergebnis-Datenbank l�schen";
static char cb_I_Ldb_DB_Delete_title[] = "Cancella risultati test dal database";
static char cb_P_Ldb_DB_Delete_title[] = "Apagar banco de dados de resultados de teste";

static char cb_E_Ldb_DB_Sure_Delete[] = "Are you sure you want to erase\nthe test results database?";
static char cb_S_Ldb_DB_Sure_Delete[] = "Confirme que desea borrar la base\nde datos de resultados de prueba ";
static char cb_F_Ldb_DB_Sure_Delete[] = "�tes-vous s�r de vouloir effacer \nla base des r�sultats de tests?";
static char cb_G_Ldb_DB_Sure_Delete[] = "Testergebnis-Datenbank\nwirklich l�schen?";
static char cb_I_Ldb_DB_Sure_Delete[] = "Eliminare i risultati\ndei test dal database?";
static char cb_P_Ldb_DB_Sure_Delete[] = "Deseja apagar o banco de dados\nde resultados de teste?";

//Delete records title
static char cb_E_Ldb_Reports_Delete_title[] = "Delete selected report(s) from the database";
static char cb_S_Ldb_Reports_Delete_title[] = "Borrar de la base de datos el(los) informe(s) seleccionado(s)";
static char cb_F_Ldb_Reports_Delete_title[] = "Supprimer le(s) rapport(s) de la base de donn�es";
static char cb_G_Ldb_Reports_Delete_title[] = "Gew�hlte(n) Bericht(e) aus der Datenbank l�schen";
static char cb_I_Ldb_Reports_Delete_title[] = "Eliminazione rapporti selezionati dal database";
static char cb_P_Ldb_Reports_Delete_title[] = "Apagar o(s) relat�rio(s) selecionado(s) da base de dados";

//delete records confirmation
static char cb_E_Ldb_Reports_Sure_Delete[] = "Are you sure you want to delete the selected \nreport(s) from the database?";
static char cb_S_Ldb_Reports_Sure_Delete[] = "�Est� seguro de que quiere borrar de la base \nde datos el(los) informe(s) seleccionado(s)?";
static char cb_F_Ldb_Reports_Sure_Delete[] = "�tes-vous s�r de vouloir supprimer le(s) rapport(s) \ns�lectionn�(s) de la base de donn�es ?";
static char cb_G_Ldb_Reports_Sure_Delete[] = "Gew�hlte(n) Bericht(e) aus der Datenbank l�schen?";
static char cb_I_Ldb_Reports_Sure_Delete[] = "Eliminare i rapporti selezionati?";
static char cb_P_Ldb_Reports_Sure_Delete[] = "Tem certeza que quer apagar o(s) relat�rio(s) \nselecionados da base de dados?";

static char cb_E_Ldb_ClearingDB[] = "Test results database being cleared";
static char cb_S_Ldb_ClearingDB[] = "Borrando base de datos de resultados de prueba";
static char cb_F_Ldb_ClearingDB[] = "Base des r�sultats de tests effac�e";
static char cb_G_Ldb_ClearingDB[] = "Testergebnis-Datenbank wird gel�scht";
static char cb_I_Ldb_ClearingDB[] = "Eliminazione dei risultati dei test dal database";
static char cb_P_Ldb_ClearingDB[] = "Apagando banco de dados de teste";

static char cb_E_Ldb_MainpanelFile[] = ".\\panels\\840VTS_ENU.uir";
static char cb_S_Ldb_MainpanelFile[] = ".\\panels\\840VTS_ESP.uir";
static char cb_F_Ldb_MainpanelFile[] = ".\\panels\\840VTS_FRA.uir";
static char cb_G_Ldb_MainpanelFile[] = ".\\panels\\840VTS_DEU.uir";
static char cb_I_Ldb_MainpanelFile[] = ".\\panels\\840VTS_ITA.uir";
static char cb_P_Ldb_MainpanelFile[] = ".\\panels\\840VTS_PTB.uir";

static char cb_E_Ldb_AdminpanelFile[] = ".\\panels\\840VTS_ENU.uir";
static char cb_S_Ldb_AdminpanelFile[] = ".\\panels\\840VTS_ESP.uir";
static char cb_F_Ldb_AdminpanelFile[] = ".\\panels\\840VTS_FRA.uir";
static char cb_G_Ldb_AdminpanelFile[] = ".\\panels\\840VTS_DEU.uir";
static char cb_I_Ldb_AdminpanelFile[] = ".\\panels\\840VTS_ITA.uir";
static char cb_P_Ldb_AdminpanelFile[] = ".\\panels\\840VTS_PTB.uir";

static char cb_E_Ldb_HelpCommand[] = " .\\cpvt.chm"; // DO NOT TRANSLATE (No longer used)
static char cb_S_Ldb_HelpCommand[] = " .\\cpvt.chm";
static char cb_F_Ldb_HelpCommand[] = " .\\cpvt.chm";
static char cb_G_Ldb_HelpCommand[] = " .\\cpvt.chm";
static char cb_I_Ldb_HelpCommand[] = " .\\cpvt.chm";
static char cb_P_Ldb_HelpCommand[] = " .\\cpvt.chm";

static char cb_E_Ldb_Helpfile_Name[] = "\\840VTS_ENU.hlp";
static char cb_S_Ldb_Helpfile_Name[] = "\\840VTS_ESP.hlp";
static char cb_F_Ldb_Helpfile_Name[] = "\\840VTS_FRA.hlp";
static char cb_G_Ldb_Helpfile_Name[] = "\\840VTS_DEU.hlp";
static char cb_I_Ldb_Helpfile_Name[] = "\\840VTS_ITA.hlp";
static char cb_P_Ldb_Helpfile_Name[] = "\\840VTS_PTB.hlp";

//static char cb_E_Ldb_InfFile[] = "tooltips.inf"; // DO NOT TRANSLATE (No longer used)
//static char cb_S_Ldb_InfFile[] = "tooltips.inf";
//static char cb_F_Ldb_InfFile[] = "tooltips.inf";
//static char cb_G_Ldb_InfFile[] = "tooltips.inf";
//static char cb_I_Ldb_InfFile[] = "tooltips.inf";
//static char cb_P_Ldb_InfFile[] = "tooltips.inf";

// Demo Title
static char cb_E_Ldb_DemoTitle[] = " - FOR DEMONSTRATION ONLY";
static char cb_S_Ldb_DemoTitle[] = " - PARA LA DEMOSTRACI�N SOLAMENTE";
static char cb_F_Ldb_DemoTitle[] = " - POUR LA D�MONSTRATION SEULEMENT";
static char cb_G_Ldb_DemoTitle[] = " - F�R NUR DEMONSTRATION";
static char cb_I_Ldb_DemoTitle[] = " - PER LA DIMOSTRAZIONE SOLTANTO";
static char cb_P_Ldb_DemoTitle[] = " - PARA A DEMONSTRA��O SOMENTE";					

// SetSplashVersionNum
static char cb_E_Ldb_Version_Str[] = "Version ";
static char cb_S_Ldb_Version_Str[] = "Versi�n ";
static char cb_F_Ldb_Version_Str[] = "Version ";
static char cb_G_Ldb_Version_Str[] = "Version ";
static char cb_I_Ldb_Version_Str[] = "Versione ";
static char cb_P_Ldb_Version_Str[] = "Vers�o ";

// Month names
static char cb_E_Ldb_Jan_Abrv[] = "JAN";
static char cb_E_Ldb_Feb_Abrv[] = "FEB";
static char cb_E_Ldb_Mar_Abrv[] = "MAR";
static char cb_E_Ldb_Apr_Abrv[] = "APR";
static char cb_E_Ldb_May_Abrv[] = "MAY";
static char cb_E_Ldb_Jun_Abrv[] = "JUN";
static char cb_E_Ldb_Jul_Abrv[] = "JUL";
static char cb_E_Ldb_Aug_Abrv[] = "AUG";
static char cb_E_Ldb_Sep_Abrv[] = "SEP";
static char cb_E_Ldb_Oct_Abrv[] = "OCT";
static char cb_E_Ldb_Nov_Abrv[] = "NOV";
static char cb_E_Ldb_Dec_Abrv[] = "DEC";

static char cb_S_Ldb_Jan_Abrv[] = "ENE";
static char cb_S_Ldb_Feb_Abrv[] = "FEB";
static char cb_S_Ldb_Mar_Abrv[] = "MAR";
static char cb_S_Ldb_Apr_Abrv[] = "ABR";
static char cb_S_Ldb_May_Abrv[] = "MAY";
static char cb_S_Ldb_Jun_Abrv[] = "JUN";
static char cb_S_Ldb_Jul_Abrv[] = "JUL";
static char cb_S_Ldb_Aug_Abrv[] = "AGO";
static char cb_S_Ldb_Sep_Abrv[] = "SEP";
static char cb_S_Ldb_Oct_Abrv[] = "OCT";
static char cb_S_Ldb_Nov_Abrv[] = "NOV";
static char cb_S_Ldb_Dec_Abrv[] = "DIC";

static char cb_F_Ldb_Jan_Abrv[] = "JAN";
static char cb_F_Ldb_Feb_Abrv[] = "F�V";
static char cb_F_Ldb_Mar_Abrv[] = "MAR";
static char cb_F_Ldb_Apr_Abrv[] = "AVR";
static char cb_F_Ldb_May_Abrv[] = "MAI";
static char cb_F_Ldb_Jun_Abrv[] = "JUN";
static char cb_F_Ldb_Jul_Abrv[] = "JUL";
static char cb_F_Ldb_Aug_Abrv[] = "AO�";
static char cb_F_Ldb_Sep_Abrv[] = "SEP";
static char cb_F_Ldb_Oct_Abrv[] = "OCT";
static char cb_F_Ldb_Nov_Abrv[] = "NOV";
static char cb_F_Ldb_Dec_Abrv[] = "D�C";

static char cb_G_Ldb_Jan_Abrv[] = "JAN";
static char cb_G_Ldb_Feb_Abrv[] = "FEB";
static char cb_G_Ldb_Mar_Abrv[] = "M�R";
static char cb_G_Ldb_Apr_Abrv[] = "APR";
static char cb_G_Ldb_May_Abrv[] = "MAI";
static char cb_G_Ldb_Jun_Abrv[] = "JUN";
static char cb_G_Ldb_Jul_Abrv[] = "JUL";
static char cb_G_Ldb_Aug_Abrv[] = "AUG";
static char cb_G_Ldb_Sep_Abrv[] = "SEP";
static char cb_G_Ldb_Oct_Abrv[] = "OKT";
static char cb_G_Ldb_Nov_Abrv[] = "NOV";
static char cb_G_Ldb_Dec_Abrv[] = "DEZ";

static char cb_I_Ldb_Jan_Abrv[] = "GEN";
static char cb_I_Ldb_Feb_Abrv[] = "FEB";
static char cb_I_Ldb_Mar_Abrv[] = "MAR";
static char cb_I_Ldb_Apr_Abrv[] = "APR";
static char cb_I_Ldb_May_Abrv[] = "MAG";
static char cb_I_Ldb_Jun_Abrv[] = "GIU";
static char cb_I_Ldb_Jul_Abrv[] = "LUG";
static char cb_I_Ldb_Aug_Abrv[] = "AGO";
static char cb_I_Ldb_Sep_Abrv[] = "SET";
static char cb_I_Ldb_Oct_Abrv[] = "OTT";
static char cb_I_Ldb_Nov_Abrv[] = "NOV";
static char cb_I_Ldb_Dec_Abrv[] = "DIC";

static char cb_P_Ldb_Jan_Abrv[] = "JAN";
static char cb_P_Ldb_Feb_Abrv[] = "FEV";
static char cb_P_Ldb_Mar_Abrv[] = "MAR";
static char cb_P_Ldb_Apr_Abrv[] = "APR";
static char cb_P_Ldb_May_Abrv[] = "MAI";
static char cb_P_Ldb_Jun_Abrv[] = "JUN";
static char cb_P_Ldb_Jul_Abrv[] = "JUL";
static char cb_P_Ldb_Aug_Abrv[] = "AUG";
static char cb_P_Ldb_Sep_Abrv[] = "SEP";
static char cb_P_Ldb_Oct_Abrv[] = "OCT";
static char cb_P_Ldb_Nov_Abrv[] = "NOV";
static char cb_P_Ldb_Dec_Abrv[] = "DEC";

// MainPanelCallback
static char cb_E_Ldb_test_aborted[] = "Test aborted";
static char cb_S_Ldb_test_aborted[] = "Prueba anulada";
static char cb_F_Ldb_test_aborted[] = "Test annul�";
static char cb_G_Ldb_test_aborted[] = "Test abgebrochen";
static char cb_I_Ldb_test_aborted[] = "Test interrotto";
static char cb_P_Ldb_test_aborted[] = "Teste anulado";

static char cb_E_Ldb_cycle_before_test1[] = "Cycle the 840 Ventilator power before starting another test.";
static char cb_S_Ldb_cycle_before_test1[] = "Ejecute un ciclo de funcionamiento del ventilador 840 antes de iniciar otra prueba.";
static char cb_F_Ldb_cycle_before_test1[] = "�teindre et allumer ventilateur 840 avant de commencer un autre test.";
static char cb_G_Ldb_cycle_before_test1[] = "Beatmungsger�t 840 aus- und wieder einschalten, bevor ein neuer Test gestartet wird.";
static char cb_I_Ldb_cycle_before_test1[] = "Accendere ventilatore 840 prima di avviare un altro test.";
static char cb_P_Ldb_cycle_before_test1[] = "Ligar e desligar o Ventilador 840 antes de iniciar outro teste.";

// detfun.c
static char cb_E_Ldb_S_Starting[] = "Searching for COM ports";
static char cb_S_Ldb_S_Starting[] = "Buscando puertos COM";
static char cb_F_Ldb_S_Starting[] = "Recherche des ports COM ";
static char cb_G_Ldb_S_Starting[] = "Suche COM-Ports";
static char cb_I_Ldb_S_Starting[] = "Ricerca porte COM";
static char cb_P_Ldb_S_Starting[] = "Procurando portas COM";

// DetFun_840
static char cb_E_Ldb_S_840_Establishing[] = "Searching for 840 Ventilator on port";
static char cb_S_Ldb_S_840_Establishing[] = "Buscando ventilador 840 en el puerto";
static char cb_F_Ldb_S_840_Establishing[] = "Recherche du ventilateur 840 sur port";
static char cb_G_Ldb_S_840_Establishing[] = "Suche Beatmungsger�t 840 an Port";
static char cb_I_Ldb_S_840_Establishing[] = "Ricerca ventilatore 840 sulla porta";
static char cb_P_Ldb_S_840_Establishing[] = "Procurando Ventilador 840 na porta";

static char cb_E_Ldb_S_840_Success[]      = "Found 840 Ventilator on port";
static char cb_S_Ldb_S_840_Success[]      = "Ventilador 840 encontrado en el puerto";
static char cb_F_Ldb_S_840_Success[]      = "Ventilateur 840 trouv� sur port";
static char cb_G_Ldb_S_840_Success[]      = "Beatmungsger�t 840 gefunden an Port";
static char cb_I_Ldb_S_840_Success[]      = "Ventilatore 840 rilevato sulla porta";
static char cb_P_Ldb_S_840_Success[]      = "Ventilador 840 encontrado na porta";

static char cb_E_Ldb_S_840_Failure[]      = "Ventilator not found on port";
static char cb_S_Ldb_S_840_Failure[]      = "No se encuentra ventilador en el puerto";
static char cb_F_Ldb_S_840_Failure[]      = "Ventilateur non trouv� sur port";
static char cb_G_Ldb_S_840_Failure[]      = "Beatmungsger�t an Port X nicht gefunden";
static char cb_I_Ldb_S_840_Failure[]      = "Ventilatore non rilevato sulla porta";
static char cb_P_Ldb_S_840_Failure[]      = "Ventilador n�o encontrado na porta";

// DetFun_PTS
static char cb_E_Ldb_S_PTS_Establishing[] = "Searching for PTS 2000 on port";
static char cb_S_Ldb_S_PTS_Establishing[] = "Buscando PTS 2000 en el puerto";
static char cb_F_Ldb_S_PTS_Establishing[] = "Recherche du PTS 2000 sur port";
static char cb_G_Ldb_S_PTS_Establishing[] = "Suche PTS 2000 an Port";
static char cb_I_Ldb_S_PTS_Establishing[] = "Ricerca PTS 2000 sulla porta";
static char cb_P_Ldb_S_PTS_Establishing[] = "Procurando PTS 2000 na porta";

static char cb_E_Ldb_S_PTS_Success[]      = "Found PTS 2000 on port";
static char cb_S_Ldb_S_PTS_Success[]      = "PTS 2000 encontrado en el puerto";
static char cb_F_Ldb_S_PTS_Success[]      = "Recherche du PTS 2000 sur port";
static char cb_G_Ldb_S_PTS_Success[]      = "PTS 2000 gefunden an Port";
static char cb_I_Ldb_S_PTS_Success[]      = "PTS 2000 rilevato sulla porta";
static char cb_P_Ldb_S_PTS_Success[]      = "PTS 2000 encontrado na porta";

static char cb_E_Ldb_S_PTS_Failure[]      = "PTS 2000 not found on port";
static char cb_S_Ldb_S_PTS_Failure[]      = "No se encuentra PTS 2000 en el puerto";
static char cb_F_Ldb_S_PTS_Failure[]      = "PTS 2000 non trouv� sur port";
static char cb_G_Ldb_S_PTS_Failure[]      = "PTS 2000 nicht gefunden an Port";
static char cb_I_Ldb_S_PTS_Failure[]      = "PTS 2000 rilevato sulla porta";
static char cb_P_Ldb_S_PTS_Failure[]      = "PTS 2000 n�o encontrado na porta";

// AppErr
static char cb_E_Ldb_error_cant_decode[] = "Error: cannot decode error # %d";
static char cb_S_Ldb_error_cant_decode[] = "Error: no se puede descodificar el error n� %d";
static char cb_F_Ldb_error_cant_decode[] = "Erreur: d�codage erreur # %d impossible";
static char cb_G_Ldb_error_cant_decode[] = "Fehler: Kann Fehler Nr. %d nicht dekodieren";
static char cb_I_Ldb_error_cant_decode[] = "Errore: impossibile decodificare l'errore # %d";
static char cb_P_Ldb_error_cant_decode[] = "Erro: imposs�vel decodificar erro n� %d";

static char cb_E_Ldb_ERR_TEST_TIMEOUT[] = "Test time limit expired";
static char cb_S_Ldb_ERR_TEST_TIMEOUT[] = "Ha transcurrido el tiempo l�mite de la prueba";
static char cb_F_Ldb_ERR_TEST_TIMEOUT[] = "D�lai dur�e test d�pass�";
static char cb_G_Ldb_ERR_TEST_TIMEOUT[] = "Test Zeitgrenze erreicht";
static char cb_I_Ldb_ERR_TEST_TIMEOUT[] = "Limite tempo test scaduto";
static char cb_P_Ldb_ERR_TEST_TIMEOUT[] = "Expirado limite de tempo do teste";

static char cb_E_Ldb_ERR_PTS_BURST[] = "PTS 2000 external trigger timeout.";
static char cb_S_Ldb_ERR_PTS_BURST[] = "Tiempo de espera activador externo PTS 2000.";
static char cb_F_Ldb_ERR_PTS_BURST[] = "Temps d�clencht externe PTS 2000 d�pass�.";
static char cb_G_Ldb_ERR_PTS_BURST[] = "PTS 2000 Externer Trigger Zeit�berschreitung.";
static char cb_I_Ldb_ERR_PTS_BURST[] = "Timeout trigger PTS 2000.";
static char cb_P_Ldb_ERR_PTS_BURST[] = "Tempo de espera do disparo externo do PTS 2000.";

static char cb_E_Ldb_ERR_VENT_GAS_SEQ[] = "Gas flows not established. Check gas supplies.";
static char cb_S_Ldb_ERR_VENT_GAS_SEQ[] = "No se han establecido los flujos de gas. Compruebe el suministro de gas.";
static char cb_F_Ldb_ERR_VENT_GAS_SEQ[] = "D�bits gazeux non �tablis. Contr�ler alimentations en gaz.";
static char cb_G_Ldb_ERR_VENT_GAS_SEQ[] = "Gasflows nicht vorhanden. Gasversorgungen �berpr�fen.";
static char cb_I_Ldb_ERR_VENT_GAS_SEQ[] = "Flussi gas non stabiliti. Verifica alimentazioni gas.";
static char cb_P_Ldb_ERR_VENT_GAS_SEQ[] = "Fluxos de g�s n�o estabelecidos. Verificar fontes de g�s.";

static char cb_E_Ldb_ERR_VENT_CP[] = "Circuit could not be pressurized. Check gas supplies.";
static char cb_S_Ldb_ERR_VENT_CP[] = "No se ha podido presurizar el circuito. Compruebe el suministro de gas.";
static char cb_F_Ldb_ERR_VENT_CP[] = "Pressurisation du circuit impossible. Contr�ler alimentations gaz.";
static char cb_G_Ldb_ERR_VENT_CP[] = "Im Schlauchsystem konnte kein Druck aufgebaut werden. Gasversorgungen �berpr�fen.";
static char cb_I_Ldb_ERR_VENT_CP[] = "Impossibile pressurizzare il circuito. Verificare alimentazioni gas.";
static char cb_P_Ldb_ERR_VENT_CP[] = "Imposs�vel pressurizar circuito. Verificar fontes de g�s.";

static char cb_E_Ldb_ERR_VENT_TEST_SEQ[] = "Error acquiring atmospheric pressure from the 840 Ventilator.";
static char cb_S_Ldb_ERR_VENT_TEST_SEQ[] = "Error al obtener la presi�n atmosf�rica del ventilador 840.";
static char cb_F_Ldb_ERR_VENT_TEST_SEQ[] = "Acquisition erron�e de pression atmosph du ventilateur 840.";
static char cb_G_Ldb_ERR_VENT_TEST_SEQ[] = "Fehler bei Erfassung des Luftdrucks vom Beatmungsger�t 840.";
static char cb_I_Ldb_ERR_VENT_TEST_SEQ[] = "Errore acquisizione pressione atmosferica dal ventilatore 840.";
static char cb_P_Ldb_ERR_VENT_TEST_SEQ[] = "Erro ao adquirir press�o atmosf�rica do Ventilador 840.";

static char cb_E_Ldb_ERR_UIR_LOAD[] = "Unable to load .UIR file";
static char cb_S_Ldb_ERR_UIR_LOAD[] = "No se puede cargar el archivo .UIR";
static char cb_F_Ldb_ERR_UIR_LOAD[] = "Chargt fichier .UIR impossible";
static char cb_G_Ldb_ERR_UIR_LOAD[] = "Kann .UIR Datei nicht laden";
static char cb_I_Ldb_ERR_UIR_LOAD[] = "Impossibile caricare il file .UIR";
static char cb_P_Ldb_ERR_UIR_LOAD[] = "N�o � poss�vel carregar arquivo .UIR";

static char cb_E_Ldb_testing_error[] = "Testing error";
static char cb_S_Ldb_testing_error[] = "Error al realizar la prueba";
static char cb_F_Ldb_testing_error[] = "Erreur de test";
static char cb_G_Ldb_testing_error[] = "Testfehler";
static char cb_I_Ldb_testing_error[] = "Errore test";
static char cb_P_Ldb_testing_error[] = "Erro de teste";

// StartSeq
static char cb_E_Ldb_cycle_840[] = "Cycle 840 power";
static char cb_S_Ldb_cycle_840[] = "Ejecutar ciclo del 840";
static char cb_F_Ldb_cycle_840[] = "�teindre et allumer 840";
static char cb_G_Ldb_cycle_840[] = "Beatmungsger�t 840 aus- und wieder einschalten";
static char cb_I_Ldb_cycle_840[] = "Accendere 840";
static char cb_P_Ldb_cycle_840[] = "Ligar e desligar o Ventilador 840";

static char cb_E_Ldb_cycle_before_test[] = "Cycle the 840 Ventilator power before testing. Try again?";
static char cb_S_Ldb_cycle_before_test[] = "Ejecute un ciclo de funcionamiento del ventilador 840 antes de realizar pruebas. �Volver a intentarlo?";
static char cb_F_Ldb_cycle_before_test[] = "�teindre et allumer ventilateur 840 avant de tester. Nouvel essai?";
static char cb_G_Ldb_cycle_before_test[] = "Beatmungsger�t 840 aus- und wieder einschalten, bevor ein Test gestartet wird. Nochmals versuchen?";
static char cb_I_Ldb_cycle_before_test[] = "Accendere ventilatore 840 prima di eseguire i test. Riprovare?";
static char cb_P_Ldb_cycle_before_test[] = "Ligar e desligar o Ventilador 840 antes testar. Repetir?";

// manual test panel
static char cb_E_Ldb_Test12_Running[] = "Manual Tests running";
static char cb_S_Ldb_Test12_Running[] = "Ejecutando pruebas manuales";
static char cb_F_Ldb_Test12_Running[] = "Tests manuels en cours";
static char cb_G_Ldb_Test12_Running[] = "Manuelle Tests laufen";
static char cb_I_Ldb_Test12_Running[] = "Test manuali in corso";
static char cb_P_Ldb_Test12_Running[] = "Testes manuais em execu��o";

// Optional portion of the manual test panel
// Work Order number
static char cb_E_Ldb_mnl_wrkordr_r[]    = "Work Order Number: %s";
static char cb_E_Ldb_mnl_wrkordr_r_nd[] = "Work Order Number: No data";
static char cb_S_Ldb_mnl_wrkordr_r[]    = "N�mero de la orden de trabajo: %s";
static char cb_S_Ldb_mnl_wrkordr_r_nd[] = "N�mero de la orden de trabajo: sin datos";
static char cb_F_Ldb_mnl_wrkordr_r[]    = "Num�ro d'ordre des t�ches: %s";
static char cb_F_Ldb_mnl_wrkordr_r_nd[] = "Num�ro d'ordre des t�ches: pas de donn�es";
static char cb_G_Ldb_mnl_wrkordr_r[]    = "Auftragsnummer: %s";
static char cb_G_Ldb_mnl_wrkordr_r_nd[] = "Auftragsnummer: Keine Daten";
static char cb_I_Ldb_mnl_wrkordr_r[]    = "Numero ordine lavoro: %s";
static char cb_I_Ldb_mnl_wrkordr_r_nd[] = "Numero ordine lavoro: nessun dato";
static char cb_P_Ldb_mnl_wrkordr_r[]    = "N�mero da ordem de trabalho: %s";
static char cb_P_Ldb_mnl_wrkordr_r_nd[] = "N�mero da ordem de trabalho: sem dados";

// Vent Asset number
static char cb_E_Ldb_mnl_vent_r[]    = "Ventilator Asset Number: %s";
static char cb_E_Ldb_mnl_vent_r_nd[] = "Ventilator Asset Number: No data";
static char cb_S_Ldb_mnl_vent_r[]    = "N�mero de recurso del ventilador: %s";
static char cb_S_Ldb_mnl_vent_r_nd[] = "N�mero de recurso del ventilador: sin datos";
static char cb_F_Ldb_mnl_vent_r[]    = "Num�ro de contr�le ventilateur: %s";
static char cb_F_Ldb_mnl_vent_r_nd[] = "Num�ro de contr�le ventilateur: pas de donn�es";
static char cb_G_Ldb_mnl_vent_r[]    = "Beatmungsger�t Testnummer: %s";
static char cb_G_Ldb_mnl_vent_r_nd[] = "Beatmungsger�t Testnummer: Keine Daten";
static char cb_I_Ldb_mnl_vent_r[]    = "Numero risorsa ventilatore: %s";
static char cb_I_Ldb_mnl_vent_r_nd[] = "Numero risorsa ventilatore: nessun dato";
static char cb_P_Ldb_mnl_vent_r[]    = "N�mero do ventilador: %s";
static char cb_P_Ldb_mnl_vent_r_nd[] = "N�mero do ventilador: sem dados";

// electrical portion of the manual test panel
// Ground Resistance Test
static char cb_E_Ldb_mnl_gnd_r[]    = "Ground Resistance: %5.3f Ohm";
static char cb_E_Ldb_mnl_gnd_r_nd[] = "Ground Resistance: No data";
static char cb_S_Ldb_mnl_gnd_r[]    = "Resistencia de tierra: %5.3f Ohm";
static char cb_S_Ldb_mnl_gnd_r_nd[] = "Resistencia de tierra: sin datos";
static char cb_F_Ldb_mnl_gnd_r[]    = "R�sistance de terre: %5.3f Ohm";
static char cb_F_Ldb_mnl_gnd_r_nd[] = "R�sistance de terre: pas de donn�es";
static char cb_G_Ldb_mnl_gnd_r[]    = "Erdungswiderstand: %5.3f Ohm";
static char cb_G_Ldb_mnl_gnd_r_nd[] = "Erdungswiderstand: Keine Daten";
static char cb_I_Ldb_mnl_gnd_r[]    = "Resistenza terra: %5.3f Ohm";
static char cb_I_Ldb_mnl_gnd_r_nd[] = "Resistenza terra: nessun dato";
static char cb_P_Ldb_mnl_gnd_r[]    = "Resist�ncia de terra: %5.3f Ohm";
static char cb_P_Ldb_mnl_gnd_r_nd[] = "Resist�ncia de terra: sem dados";

// Get Forward current
//100-120
//220-240
static char cb_E_Ldb_mnl_f_leakage1[]    = "Forward current leakage (100-120V ac): %5.3f �A";
static char cb_E_Ldb_mnl_f_leakage1_nd[] = "Forward current leakage (100-120V ac): No data";
static char cb_E_Ldb_mnl_f_leakage2[]    = "Forward current leakage (220-240V ac): %5.3f �A";
static char cb_E_Ldb_mnl_f_leakage2_nd[] = "Forward current leakage (220-240V ac): No data";
static char cb_S_Ldb_mnl_f_leakage1[]    = "Fuga de corriente directa (100-120 V CA): %5.3f �A";
static char cb_S_Ldb_mnl_f_leakage1_nd[] = "Fuga de corriente directa (100-120 V CA): sin datos";
static char cb_S_Ldb_mnl_f_leakage2[]    = "Fuga de corriente directa (220-240 V CA): %5.3f �A";
static char cb_S_Ldb_mnl_f_leakage2_nd[] = "Fuga de corriente directa (220-240 V CA): sin datos";
static char cb_F_Ldb_mnl_f_leakage1[]    = "Courant de fuite direct (100-120V ca): %5.3f �A";
static char cb_F_Ldb_mnl_f_leakage1_nd[] = "Courant de fuite direct (100-120V ca): pas de donn�es";
static char cb_F_Ldb_mnl_f_leakage2[]    = "Courant de fuite direct (220-240V ca): %5.3f �A";
static char cb_F_Ldb_mnl_f_leakage2_nd[] = "Courant de fuite direct (220-240V ca): pas de donn�es";
static char cb_G_Ldb_mnl_f_leakage1[]    = "Forward-Leckstrom (100-120V AC): %5.3f �A";
static char cb_G_Ldb_mnl_f_leakage1_nd[] = "Forward-Leckstrom (100-120V AC): Keine Daten";
static char cb_G_Ldb_mnl_f_leakage2[]    = "Forward-Leckstrom (220-240V AC): %5.3f �A";
static char cb_G_Ldb_mnl_f_leakage2_nd[] = "Forward-Leckstrom (220-240V AC): Keine Daten";
static char cb_I_Ldb_mnl_f_leakage1[]    = "Corrente di dispersione diretta (100-120V CA): %5.3f �A";
static char cb_I_Ldb_mnl_f_leakage1_nd[] = "Corrente di dispersione diretta (100-120V CA): nessun dato";
static char cb_I_Ldb_mnl_f_leakage2[]    = "Corrente di dispersione diretta (220-240V CA): %5.3f �A";
static char cb_I_Ldb_mnl_f_leakage2_nd[] = "Corrente di dispersione diretta (220-240V CA): nessun dato";
static char cb_P_Ldb_mnl_f_leakage1[]    = "Fuga de corrente avan�ada (100-120V ca): %5.3f �A";
static char cb_P_Ldb_mnl_f_leakage1_nd[] = "Fuga de corrente avan�ada (100-120V ca): sem dados";
static char cb_P_Ldb_mnl_f_leakage2[]    = "Fuga de corrente avan�ada (220-240V ca): %5.3f �A";
static char cb_P_Ldb_mnl_f_leakage2_nd[] = "Fuga de corrente avan�ada (220-240V ca): sem dados";

// Get Reverse current
//100-120
//220-240
static char cb_E_Ldb_mnl_r_leakage1[]    = "Reverse current leakage (100-120V ac): %5.3f �A";
static char cb_E_Ldb_mnl_r_leakage1_nd[] = "Reverse current leakage (100-120V ac): No data";
static char cb_E_Ldb_mnl_r_leakage2[]    = "Reverse current leakage (220-240V ac): %5.3f �A";
static char cb_E_Ldb_mnl_r_leakage2_nd[] = "Reverse current leakage (220-240V ac): No data";
static char cb_S_Ldb_mnl_r_leakage1[]    = "Fuga de corriente inversa (100-120 V CA): %5.3f �A";
static char cb_S_Ldb_mnl_r_leakage1_nd[] = "Fuga de corriente inversa (100-120 V CA): sin datos";
static char cb_S_Ldb_mnl_r_leakage2[]    = "Fuga de corriente inversa (220-240 V CA): %5.3f �A";
static char cb_S_Ldb_mnl_r_leakage2_nd[] = "Fuga de corriente inversa (220-240 V CA): sin datos";
static char cb_F_Ldb_mnl_r_leakage1[]    = "Courant de fuite inverse (100-120V ca): %5.3f �A";
static char cb_F_Ldb_mnl_r_leakage1_nd[] = "Courant de fuite inverse (100-120V ca): pas de donn�es";
static char cb_F_Ldb_mnl_r_leakage2[]    = "Courant de fuite inverse (220-240V ca): %5.3f �A";
static char cb_F_Ldb_mnl_r_leakage2_nd[] = "Courant de fuite inverse (220-240V ca): pas de donn�es";
static char cb_G_Ldb_mnl_r_leakage1[]    = "Reverse-Leckstrom (100-120V AC): %5.3f �A";
static char cb_G_Ldb_mnl_r_leakage1_nd[] = "Reverse-Leckstrom (100-120V AC): Keine Daten";
static char cb_G_Ldb_mnl_r_leakage2[]    = "Reverse-Leckstrom (220-240V AC): %5.3f �A";
static char cb_G_Ldb_mnl_r_leakage2_nd[] = "Reverse-Leckstrom (220-240V AC): Keine Daten";
static char cb_I_Ldb_mnl_r_leakage1[]    = "Corrente di dispersione inversa (100-120V CA): %5.3f �A";
static char cb_I_Ldb_mnl_r_leakage1_nd[] = "Corrente di dispersione inversa (100-120V CA): nessun dato";
static char cb_I_Ldb_mnl_r_leakage2[]    = "Corrente di dispersione inversa (220-240V CA): %5.3f �A";
static char cb_I_Ldb_mnl_r_leakage2_nd[] = "Corrente di dispersione inversa (220-240V CA): nessun dato";
static char cb_P_Ldb_mnl_r_leakage1[]    = "Fuga de corrente reversa (100-120V ca): %5.3f �A";
static char cb_P_Ldb_mnl_r_leakage1_nd[] = "Fuga de corrente reversa (100-120V ca): sem dados";
static char cb_P_Ldb_mnl_r_leakage2[]    = "Fuga de corrente reversa (220-240V ca): %5.3f �A";
static char cb_P_Ldb_mnl_r_leakage2_nd[] = "Fuga de corrente reversa (220-240V ca): sem dados";

// ground resistance portion of the manual test panel
// Ground Isolation Resistance Test
static char cb_E_Ldb_mnl_g_isol_r[]    = "Ground Isolation Resistance: %5.3f Ohm";
static char cb_E_Ldb_mnl_g_isol_r_nd[] = "Ground Isolation Resistance: No data";
static char cb_S_Ldb_mnl_g_isol_r[]    = "Resistencia del aislamiento de tierra: %5.3f Ohm";
static char cb_S_Ldb_mnl_g_isol_r_nd[] = "Resistencia del aislamiento de tierra: sin datos";
static char cb_F_Ldb_mnl_g_isol_r[]    = "R�sistance isolation /terre: %5.3f Ohm";
static char cb_F_Ldb_mnl_g_isol_r_nd[] = "R�sistance isolation /terre: pas de donn�es";
static char cb_G_Ldb_mnl_g_isol_r[]    = "Erdungsisolationswiderstand: %5.3f Ohm";
static char cb_G_Ldb_mnl_g_isol_r_nd[] = "Erdungsisolationswiderstand: Keine Daten";
static char cb_I_Ldb_mnl_g_isol_r[]    = "Resistenza isolamento terra: %5.3f Ohm";
static char cb_I_Ldb_mnl_g_isol_r_nd[] = "Resistenza isolamento terra: nessun dato";
static char cb_P_Ldb_mnl_g_isol_r[]    = "Resist�ncia de isolamento de terra: %5.3f Ohm";
static char cb_P_Ldb_mnl_g_isol_r_nd[] = "Resist�ncia de isolamento de terra: sem dados";



// regulator portion of the manual test panel
// Regulator Settings Test
static char cb_E_Ldb_mnl_air_reg[]    = "Air regulator reading: %5.3f psi";
static char cb_E_Ldb_mnl_air_reg_nd[] = "Air regulator reading: No data";
static char cb_S_Ldb_mnl_air_reg[]    = "Lectura regulador de aire: %5.3f psi";
static char cb_S_Ldb_mnl_air_reg_nd[] = "Lectura regulador de aire: sin datos";
static char cb_F_Ldb_mnl_air_reg[]    = "Lecture r�gulateur air: %5.3f psi";
static char cb_F_Ldb_mnl_air_reg_nd[] = "Lecture r�gulateur air: pas de donn�es";
static char cb_G_Ldb_mnl_air_reg[]    = "Air-Regulator-Me�wert: %5.3f psi";
static char cb_G_Ldb_mnl_air_reg_nd[] = "Air-Regulator-Me�wert: Keine Daten";
static char cb_I_Ldb_mnl_air_reg[]    = "Regolatore aria: %5.3f psi";
static char cb_I_Ldb_mnl_air_reg_nd[] = "Regolatore aria: nessun dato";
static char cb_P_Ldb_mnl_air_reg[]    = "Leitura do regulador de ar: %5.3f psi";
static char cb_P_Ldb_mnl_air_reg_nd[] = "Leitura do regulador de ar: sem dados";

static char cb_E_Ldb_mnl_O2_reg[]    = "O2 regulator reading: %5.3f psi";
static char cb_E_Ldb_mnl_O2_reg_nd[] = "O2 regulator reading: No data";
static char cb_S_Ldb_mnl_O2_reg[]    = "Lectura del regulador de O2: %5.3f psi";
static char cb_S_Ldb_mnl_O2_reg_nd[] = "Lectura del regulador de O2: sin datos";
static char cb_F_Ldb_mnl_O2_reg[]    = "Lecture r�gulateur O2: %5.3f psi";
static char cb_F_Ldb_mnl_O2_reg_nd[] = "Lecture r�gulateur O2: pas de donn�es";
static char cb_G_Ldb_mnl_O2_reg[]    = "O2-Regulator-Me�wert: %5.3f psi";
static char cb_G_Ldb_mnl_O2_reg_nd[] = "O2-Regulator-Me�wert: Keine Daten";
static char cb_I_Ldb_mnl_O2_reg[]    = "Dati regolatore O2: %5.3f psi";
static char cb_I_Ldb_mnl_O2_reg_nd[] = "Dati regolatore O2: nessun dato";
static char cb_P_Ldb_mnl_O2_reg[]    = "Leitura do regulador de O2: %5.3f psi";
static char cb_P_Ldb_mnl_O2_reg_nd[] = "Leitura do regulador de O2: sem dados";

// EST/SST portion of the manual test panel
static char cb_E_Ldb_mnl_est[]    = "EST test result";
static char cb_E_Ldb_mnl_est_nd[] = "EST test result: No data";
static char cb_E_Ldb_mnl_sst[]    = "SST test result";
static char cb_E_Ldb_mnl_sst_nd[] = "SST test result: No data";
static char cb_S_Ldb_mnl_est[]    = "Resultados de la prueba ATG";
static char cb_S_Ldb_mnl_est_nd[] = "Resultados de la prueba ATG: sin datos";
static char cb_S_Ldb_mnl_sst[]    = "Resultado de la prueba ATC";
static char cb_S_Ldb_mnl_sst_nd[] = "Resultado de la prueba ATC: sin datos";
static char cb_F_Ldb_mnl_est[]    = "R�sultat du test ATG";
static char cb_F_Ldb_mnl_est_nd[] = "R�sultat du test ATG: pas de donn�es";
static char cb_F_Ldb_mnl_sst[]    = "R�sultat du test ATR";
static char cb_F_Ldb_mnl_sst_nd[] = "R�sultat du test ATR: pas de donn�es";
static char cb_G_Ldb_mnl_est[]    = "EST-Testergebnis";
static char cb_G_Ldb_mnl_est_nd[] = "EST-Testergebnis: Keine Daten";
static char cb_G_Ldb_mnl_sst[]    = "SST Testergebnis";
static char cb_G_Ldb_mnl_sst_nd[] = "SST Testergebnis: Keine Daten";
static char cb_I_Ldb_mnl_est[]    = "Risultati test EST";
static char cb_I_Ldb_mnl_est_nd[] = "Risultati test EST: nessun dato";
static char cb_I_Ldb_mnl_sst[]    = "Risultati test SST";
static char cb_I_Ldb_mnl_sst_nd[] = "Risultati test SST: nessun dato";
static char cb_P_Ldb_mnl_est[]    = "Resultado do teste de EST";
static char cb_P_Ldb_mnl_est_nd[] = "Resultado do teste de EST: sem dados";
static char cb_P_Ldb_mnl_sst[]    = "Resultado de teste do SST";
static char cb_P_Ldb_mnl_sst_nd[] = "Resultado de teste do SST: sem dados";

// Other Tests portion of the manual test panel
// Serial Loopback Test
static char cb_E_Ldb_mnl_SIO[]    = "Serial Loopback Test: Verified";
static char cb_E_Ldb_mnl_SIO_nd[] = "Serial Loopback Test: No data";
static char cb_S_Ldb_mnl_SIO[]    = "Prueba Puertos Serie: Verificados";
static char cb_S_Ldb_mnl_SIO_nd[] = "Prueba Puertos Serie: sin datos";
static char cb_F_Ldb_mnl_SIO[]    = "Test Ports S�rie: V�rifi�";
static char cb_F_Ldb_mnl_SIO_nd[] = "Test Ports S�rie: pas de donn�es";
static char cb_G_Ldb_mnl_SIO[]    = "Test Serielle Ports: Nachgepr�ft";
static char cb_G_Ldb_mnl_SIO_nd[] = "Test Serielle Ports: Keine Daten";
static char cb_I_Ldb_mnl_SIO[]    = "Test Porte Seriali: Verificato";
static char cb_I_Ldb_mnl_SIO_nd[] = "Test Porte Seriali: nessun dato";
static char cb_P_Ldb_mnl_SIO[]    = "Teste Portas Seriais: Verificado";
static char cb_P_Ldb_mnl_SIO_nd[] = "Teste Portas Seriais: sem dados";


// AtmospherePressureTest
static char cb_E_Ldb_Test2_Running[] = "Atmospheric Pressure Test running";
static char cb_S_Ldb_Test2_Running[] = "Ejecutando prueba de presi�n atmosf�rica";
static char cb_F_Ldb_Test2_Running[] = "Test Pression atmosph�rique en cours";
static char cb_G_Ldb_Test2_Running[] = "Luftdruck-Test l�uft";
static char cb_I_Ldb_Test2_Running[] = "Test pressione atmosferica in corso";
static char cb_P_Ldb_Test2_Running[] = "Teste de press�o atmosf�rica em execu��o";

// check vent atmosphere(comp) with pts 2000
static char cb_E_Ldb_target_pts[] = "Target On PTS 2000: %3.3f";
static char cb_S_Ldb_target_pts[] = "Objetivo en PTS 2000: %3.3f";
static char cb_F_Ldb_target_pts[] = "Cible sur PTS 2000: %3.3f";
static char cb_G_Ldb_target_pts[] = "Zielwert am PTS 2000: %3.3f";
static char cb_I_Ldb_target_pts[] = "Target su PTS 2000: %3.3f";
static char cb_P_Ldb_target_pts[] = "Destino no PTS 2000: %3.3f";

static char cb_E_Ldb_vent_atm[] = "840 Ventilator atmospheric reading:";
static char cb_S_Ldb_vent_atm[] = "Lectura del valor atmosf�rico del ventilador 840:";
static char cb_F_Ldb_vent_atm[] = "Lecture atmosph�rique du ventilateur 840:";
static char cb_G_Ldb_vent_atm[] = "Beatmungsger�t 840 Luftdruckme�wert:";
static char cb_I_Ldb_vent_atm[] = "Lettura atmosferica Ventilatore 840:";
static char cb_P_Ldb_vent_atm[] = "Leitura atmosf�rica do Ventilador 840:";

// BreathDeliveryAlarmTest
static char cb_E_Ldb_Test3_Running[] = "Breath Delivery Alarm Test running";
static char cb_S_Ldb_Test3_Running[] = "Ejecutando prueba de la alarma de administraci�n de respiraci�n";
static char cb_F_Ldb_Test3_Running[] = "Test Alarme insufflation d'air en cours";
static char cb_G_Ldb_Test3_Running[] = "Beatmungsalarm-Test l�uft";
static char cb_I_Ldb_Test3_Running[] = "Test allarme erogazione BDV in corso";
static char cb_P_Ldb_Test3_Running[] = "Teste de alarme de fornecimento de respira��o em execu��o";

static char cb_E_Ldb_Alarm_Audible[] = "Alarm audible";
static char cb_S_Ldb_Alarm_Audible[] = "Alarma audible";
static char cb_F_Ldb_Alarm_Audible[] = "Alarme audible";
static char cb_G_Ldb_Alarm_Audible[] = "Alarm h�rbar";
static char cb_I_Ldb_Alarm_Audible[] = "Allarme sonoro";
static char cb_P_Ldb_Alarm_Audible[] = "Alarme aud�vel";

static char cb_E_Ldb_No_Alarm[] = "No alarm";
static char cb_S_Ldb_No_Alarm[] = "Ninguna alarma";
static char cb_F_Ldb_No_Alarm[] = "Pas d'alarme";
static char cb_G_Ldb_No_Alarm[] = "Kein Alarm";
static char cb_I_Ldb_No_Alarm[] = "Nessun allarme";
static char cb_P_Ldb_No_Alarm[] = "Sem alarme";

// GuiAlarmTest
static char cb_E_Ldb_Test4_Running[] = "GUI Alarm Test running";
static char cb_S_Ldb_Test4_Running[] = "Ejecutando prueba de alarma IGU";
static char cb_F_Ldb_Test4_Running[] = "Test Alarme IGU en cours";
static char cb_G_Ldb_Test4_Running[] = "GUI-Alarm-Test l�uft";
static char cb_I_Ldb_Test4_Running[] = "Test allarme GUI in corso ";
static char cb_P_Ldb_Test4_Running[] = "Teste de alarme de GUI em execu��o";

// GuiAlarmTest messages same as BreathDeliveryAlarmTest, above

// CircuitPressureTest
static char cb_E_Ldb_Test5_Running[] = "Circuit Pressure Test running";
static char cb_S_Ldb_Test5_Running[] = "Ejecutando prueba de presi�n del circuito";
static char cb_F_Ldb_Test5_Running[] = "Test Pression circuit en cours";
static char cb_G_Ldb_Test5_Running[] = "Schlauchsystemdruck-Test l�uft";
static char cb_I_Ldb_Test5_Running[] = "Test pressione circuito in corso";
static char cb_P_Ldb_Test5_Running[] = "Teste de press�o de circuito em execu��o";

static char cb_E_Ldb_cp_test_msg[] = "Target = %3.2f , PTS 2000 reports %3.2f";
static char cb_S_Ldb_cp_test_msg[] = "Objetivo = %3.2f , PTS 2000 notifica %3.2f";
static char cb_F_Ldb_cp_test_msg[] = "Cible = %3.2f , Rapports PTS 2000 %3.2f";
static char cb_G_Ldb_cp_test_msg[] = "Zielwert = %3.2f , PTS 2000 berichtet %3.2f";
static char cb_I_Ldb_cp_test_msg[] = "Target = %3.2f , PTS 2000 rileva %3.2f";
static char cb_P_Ldb_cp_test_msg[] = "Destino = %3.2f , PTS 2000 informa %3.2f";

static char cb_E_Ldb_cpe_10[]  = "Circuit Pressure Exp 10";
static char cb_E_Ldb_cpe_50[]  = "Circuit Pressure Exp 50";
static char cb_E_Ldb_cpe_100[] = "Circuit Pressure Exp 100";
static char cb_E_Ldb_cpi_10[]  = "Circuit Pressure Insp 10";
static char cb_E_Ldb_cpi_50[]  = "Circuit Pressure Insp 50";
static char cb_E_Ldb_cpi_100[] = "Circuit Pressure Insp 100";

static char cb_S_Ldb_cpe_10[]  = "Presi�n esp del circuito 10";
static char cb_S_Ldb_cpe_50[]  = "Presi�n esp del circuito 50";
static char cb_S_Ldb_cpe_100[] = "Presi�n esp del circuito 100";
static char cb_S_Ldb_cpi_10[]  = "Presi�n insp del circuito 10";
static char cb_S_Ldb_cpi_50[]  = "Presi�n insp del circuito 50";
static char cb_S_Ldb_cpi_100[] = "Presi�n insp del circuito 100";

static char cb_F_Ldb_cpe_10[]  = "Pression circuit exp 10";
static char cb_F_Ldb_cpe_50[]  = "Pression circuit exp 50";
static char cb_F_Ldb_cpe_100[] = "Pression circuit exp 100";
static char cb_F_Ldb_cpi_10[]  = "Pression circuit insp 10";
static char cb_F_Ldb_cpi_50[]  = "Pression circuit insp 50";
static char cb_F_Ldb_cpi_100[] = "Pression circuit insp 100";

static char cb_G_Ldb_cpe_10[]  = "Schlauchsystemdruck Exsp. 10";
static char cb_G_Ldb_cpe_50[]  = "Schlauchsystemdruck Exsp. 50";
static char cb_G_Ldb_cpe_100[] = "Schlauchsystemdruck Exsp. 100";
static char cb_G_Ldb_cpi_10[]  = "Schlauchsystemdruck Insp. 10";
static char cb_G_Ldb_cpi_50[]  = "Schlauchsystemdruck Insp. 50";
static char cb_G_Ldb_cpi_100[] = "Schlauchsystemdruck Insp. 100";

static char cb_I_Ldb_cpe_10[]  = "Pressione circuito Esp 10";
static char cb_I_Ldb_cpe_50[]  = "Pressione circuito Esp 50";
static char cb_I_Ldb_cpe_100[] = "Pressione circuito Esp 100";
static char cb_I_Ldb_cpi_10[]  = "Pressione circuito Insp 10";
static char cb_I_Ldb_cpi_50[]  = "Pressione circuito Insp 50";
static char cb_I_Ldb_cpi_100[] = "Pressione circuito Insp 100";

static char cb_P_Ldb_cpe_10[]  = "Press�o Exp de Circuito 10";
static char cb_P_Ldb_cpe_50[]  = "Press�o Exp de Circuito 50";
static char cb_P_Ldb_cpe_100[] = "Press�o Exp de Circuito 100";
static char cb_P_Ldb_cpi_10[]  = "Press�o Insp de Circuito 10";
static char cb_P_Ldb_cpi_50[]  = "Press�o Insp de Circuito 50";
static char cb_P_Ldb_cpi_100[] = "Press�o Insp de Circuito 100";


// GasFlowPerformanceTest
static char cb_E_Ldb_Test6_Running[] = "Air Flow Test running";
static char cb_E_Ldb_Test7_Running[] = "O2 Flow Test running";

static char cb_S_Ldb_Test6_Running[] = "Ejecutando prueba de flujo de aire";
static char cb_S_Ldb_Test7_Running[] = "Ejecutando prueba de flujo de O2";

static char cb_F_Ldb_Test6_Running[] = "Test D�bit d'air en cours";
static char cb_F_Ldb_Test7_Running[] = "Test D�bit O2 en cours";

static char cb_G_Ldb_Test6_Running[] = "Air-Flow-Test l�uft";
static char cb_G_Ldb_Test7_Running[] = "O2-Flow-Test l�uft";

static char cb_I_Ldb_Test6_Running[] = "Test flusso aria in corso";
static char cb_I_Ldb_Test7_Running[] = "Test flusso O2 in corso";

static char cb_P_Ldb_Test6_Running[] = "Teste de fluxo de ar em execu��o";
static char cb_P_Ldb_Test7_Running[] = "Teste de fluxo de O2 em execu��o";

static char cb_E_Ldb_vent_fail_reported[] = "840 Ventilator failure reported";
static char cb_S_Ldb_vent_fail_reported[] = "Fallo en el ventilador 840";
static char cb_F_Ldb_vent_fail_reported[] = "D�fail. ventilateur 840 rapport�e";
static char cb_G_Ldb_vent_fail_reported[] = "Beatmungsger�t 840 - Fehler berichtet";
static char cb_I_Ldb_vent_fail_reported[] = "Errore ventilatore 840";
static char cb_P_Ldb_vent_fail_reported[] = "Falha no Ventilador 840";

static char cb_E_Ldb_target[] = "Target %3.2f ";
static char cb_S_Ldb_target[] = "Objetivo %3.2f ";
static char cb_F_Ldb_target[] = "Cible %3.2f ";
static char cb_G_Ldb_target[] = "Zielwert %3.2f ";
static char cb_I_Ldb_target[] = "Target %3.2f ";
static char cb_P_Ldb_target[] = "Destino %3.2f ";

static char cb_E_Ldb_insp[] = "Insp:";
static char cb_S_Ldb_insp[] = "Insp:";
static char cb_F_Ldb_insp[] = "Insp:";
static char cb_G_Ldb_insp[] = "Insp:";
static char cb_I_Ldb_insp[] = "Insp:";
static char cb_P_Ldb_insp[] = "Insp:";

static char cb_E_Ldb_exp[] = "Exp:";
static char cb_S_Ldb_exp[] = "Esp:";
static char cb_F_Ldb_exp[] = "Exp:";
static char cb_G_Ldb_exp[] = "Exp:";
static char cb_I_Ldb_exp[] = "Esp:";
static char cb_P_Ldb_exp[] = "Exp:";

// PostRevision
static char cb_E_Ldb_Test1_Running[] = "Firmware Revision Test running";
static char cb_S_Ldb_Test1_Running[] = "Ejecutando prueba de versi�n de firmware";
static char cb_F_Ldb_Test1_Running[] = "Test R�vision microprogramme en cours";
static char cb_G_Ldb_Test1_Running[] = "Ger�tedaten-Revisionstest l�uft";
static char cb_I_Ldb_Test1_Running[] = "Test revisione firmware in corso";
static char cb_P_Ldb_Test1_Running[] = "Teste de revis�o de firmware em execu��o";

static char cb_E_Ldb_Vent_Runtimehours[] = "Ventilator Run Time: %d";
static char cb_S_Ldb_Vent_Runtimehours[] = "Horas del ventilador: %d";
static char cb_F_Ldb_Vent_Runtimehours[] = "Heures du ventilateur: %d";
static char cb_G_Ldb_Vent_Runtimehours[] = "Beatmungsger�tstunden: %d";
static char cb_I_Ldb_Vent_Runtimehours[] = "Ore ventilatore: %d";
static char cb_P_Ldb_Vent_Runtimehours[] = "Horas do ventilador: %d";

static char cb_E_Ldb_comp_hours[] = "Compressor hours: %d";
static char cb_S_Ldb_comp_hours[] = "Horas del compresor: %d";
static char cb_F_Ldb_comp_hours[] = "Heures du compresseur: %d";
static char cb_G_Ldb_comp_hours[] = "Kompressorstunden: %d";
static char cb_I_Ldb_comp_hours[] = "Ore compressore: %d";
static char cb_P_Ldb_comp_hours[] = "Horas do compressor: %d";

static char cb_E_Ldb_pts_sn[] = "PTS 2000 serial number: %s";
static char cb_S_Ldb_pts_sn[] = "N�mero de serie PTS 2000: %s";
static char cb_F_Ldb_pts_sn[] = "Num�ro de s�rie du PTS 2000: %s";
static char cb_G_Ldb_pts_sn[] = "PTS 2000 Seriennummer: %s";
static char cb_I_Ldb_pts_sn[] = "Numero di serie PTS 2000: %s";
static char cb_P_Ldb_pts_sn[] = "N�mero de s�rie do PTS 2000: %s";

static char cb_E_Ldb_pts_vn[] = "PTS 2000 version number: %s";
static char cb_S_Ldb_pts_vn[] = "N�mero de versi�n PTS 2000: %s";
static char cb_F_Ldb_pts_vn[] = "Num�ro de version du PTS 2000: %s";
static char cb_G_Ldb_pts_vn[] = "PTS 2000 Versionsnummer: %s";
static char cb_I_Ldb_pts_vn[] = "Numero di versione PTS 2000: %s";
static char cb_P_Ldb_pts_vn[] = "N�mero da vers�o do PTS 2000: %s";

static char cb_E_Ldb_pts_cal_date[]    = "PTS 2000 calibration date: %s";
static char cb_E_Ldb_pts_next_date[]   = "PTS 2000 next calibration date: %s";
static char cb_E_Ldb_Pts_cal_expired[] = "The PTS 2000 calibration has expired.";

static char cb_S_Ldb_pts_cal_date[]    = "Fecha de calibraci�n PTS 2000: %s";
static char cb_S_Ldb_pts_next_date[]   = "Fecha de la pr�xima calibraci�n del PTS 2000: %s";
static char cb_S_Ldb_Pts_cal_expired[] = "La calibraci�n de PTS 2000 ha caducado.";

static char cb_F_Ldb_pts_cal_date[]    = "Date �talonnage du PTS 2000: %s";
static char cb_F_Ldb_pts_next_date[]   = "Prochaine date �talonnage du PTS 2000: %s";
static char cb_F_Ldb_Pts_cal_expired[] = "�talonnage du PTS 2000 expir�.";

static char cb_G_Ldb_pts_cal_date[]    = "PTS 2000 Kalibrierungsdatum: %s";
static char cb_G_Ldb_pts_next_date[]   = "PTS 2000 Datum n�chste Kalibrierung: %s";
static char cb_G_Ldb_Pts_cal_expired[] = "Die PTS 2000 Kalibration ist abgelaufen.";

static char cb_I_Ldb_pts_cal_date[]    = "Data calibrazione PTS 2000: %s";
static char cb_I_Ldb_pts_next_date[]   = "Data prossima calibrazione PTS 2000: %s";
static char cb_I_Ldb_Pts_cal_expired[] = "Calibrazione PTS 2000 scaduta.";

static char cb_P_Ldb_pts_cal_date[]    = "Data de calibra��o do PTS 2000: %s";
static char cb_P_Ldb_pts_next_date[]   = "Data da pr�xima calibra��o do PTS 2000: %s";
static char cb_P_Ldb_Pts_cal_expired[] = "A calibra��o do PTS 2000 expirou.";

// VolumePerformanceTest
static char cb_E_Ldb_Test8_Running[] = "Volume Performance Test running";
static char cb_S_Ldb_Test8_Running[] = "Ejecutando prueba de funcionamiento del volumen";
static char cb_F_Ldb_Test8_Running[] = "Test Performances volume en cours";
static char cb_G_Ldb_Test8_Running[] = "Volumen-Funktionstest l�uft";
static char cb_I_Ldb_Test8_Running[] = "Test volume in corso";
static char cb_P_Ldb_Test8_Running[] = "Teste de desempenho de volume em execu��o";

static char cb_E_Ldb_spirometry[] = "Spirometry";
static char cb_S_Ldb_spirometry[] = "Espirometr�a";
static char cb_F_Ldb_spirometry[] = "Spirom�trie";
static char cb_G_Ldb_spirometry[] = "Spirometrie";
static char cb_I_Ldb_spirometry[] = "Spirometria";
static char cb_P_Ldb_spirometry[] = "Espirometria";

static char cb_E_Ldb_enter_spiro[] = "Enter spirometry reading (from the upper right corner of the 840 Ventilator GUI)";
static char cb_S_Ldb_enter_spiro[] = "Introducir lectura de espirometr�a (esquina superior derecha de la IGU del ventilador 840)";
static char cb_F_Ldb_enter_spiro[] = "Entrer lecture spirom�trie (voir coin sup�rieur droit de l'IGU du ventilateur 840)";
static char cb_G_Ldb_enter_spiro[] = "Spirometrie-Me�ergebnis eingeben (steht in der rechten oberen Ecke der GUI des Beatmungsger�tes 840)";
static char cb_I_Ldb_enter_spiro[] = "Immettere valore spirometrico (angolo superiore destro della GUI del ventilatore 840)";
static char cb_P_Ldb_enter_spiro[] = "Inserir leitura de espirometria (do canto direito superior da GUI do Ventilador 840)";

static char cb_E_Ldb_pts_timeout[] = "PTS 2000 timeout error";
static char cb_S_Ldb_pts_timeout[] = "Error de tiempo de espera PTS 2000";
static char cb_F_Ldb_pts_timeout[] = "Erreur d�lai PTS 2000 �coul�";
static char cb_G_Ldb_pts_timeout[] = "PTS 2000 Zeit�berschreitungsfehler";
static char cb_I_Ldb_pts_timeout[] = "Errore di timeout PTS 2000";
static char cb_P_Ldb_pts_timeout[] = "Erro de tempo de espera do PTS 2000";

static char cb_E_Ldb_volume_incorrect[] = "Volume measurement incorrect!";
static char cb_S_Ldb_volume_incorrect[] = "Medici�n de volumen incorrecta.";
static char cb_F_Ldb_volume_incorrect[] = "Mesure volume incorrecte!";
static char cb_G_Ldb_volume_incorrect[] = "Volumen-Me�wert falsch!";
static char cb_I_Ldb_volume_incorrect[] = "Misurazione volume errata!";
static char cb_P_Ldb_volume_incorrect[] = "Medi��o de volume incorreta!";

// VerifyVolume
static char cb_E_Ldb_oxy_conc_fail[] = "This test fails due to bad O2 concentration.";
static char cb_S_Ldb_oxy_conc_fail[] = "Esta prueba falla debido a una concentraci�n de O2 incorrecta.";
static char cb_F_Ldb_oxy_conc_fail[] = "�chec du test pour concentration O2 incorrecte.";
static char cb_G_Ldb_oxy_conc_fail[] = "Dieser Test ist aufgrund fehlerhafter O2-Konzentration mi�lungen.";
static char cb_I_Ldb_oxy_conc_fail[] = "Errore del test causato da una concentrazione errata di O2.";
static char cb_P_Ldb_oxy_conc_fail[] = "Falha no teste devido � baixa concentra��o de O2.";

static char cb_E_Ldb_pts_rdg[] = "PTS 2000 reading: %3.2f";
static char cb_S_Ldb_pts_rdg[] = "Lectura PTS 2000: %3.2f";
static char cb_F_Ldb_pts_rdg[] = "Lecture du PTS 2000: %3.2f";
static char cb_G_Ldb_pts_rdg[] = "PTS 2000 Me�wert: %3.2f";
static char cb_I_Ldb_pts_rdg[] = "Lettura PTS 2000: %3.2f";
static char cb_P_Ldb_pts_rdg[] = "Leitura do PTS 2000: %3.2f";

static char cb_E_Ldb_sprio_rdg[] = "Spirometry: %d";
static char cb_S_Ldb_sprio_rdg[] = "Espirometr�a: %d";
static char cb_F_Ldb_sprio_rdg[] = "Spirom�trie: %d";
static char cb_G_Ldb_sprio_rdg[] = "Spirometrie: %d";
static char cb_I_Ldb_sprio_rdg[] = "Spirometria: %d";
static char cb_P_Ldb_sprio_rdg[] = "Espirometria: %d";

static char cb_E_Ldb_O2_conc_vol[] = "O2 concentration volume";
static char cb_S_Ldb_O2_conc_vol[] = "Volumen de concentraci�n de O2";
static char cb_F_Ldb_O2_conc_vol[] = "volume concentration O2";
static char cb_G_Ldb_O2_conc_vol[] = "O2-Konzentration Volumen";
static char cb_I_Ldb_O2_conc_vol[] = "Volume concentrazione O2";
static char cb_P_Ldb_O2_conc_vol[] = "Valor de concentra��o de O2";

// PressureControlPerformanceTest
static char cb_E_Ldb_Test9_Running[] = "Pressure Performance Test running";
static char cb_S_Ldb_Test9_Running[] = "Ejecutando Prueba de funcionamiento de la presi�n";
static char cb_F_Ldb_Test9_Running[] = "Test Performances pression en cours";
static char cb_G_Ldb_Test9_Running[] = "Druck-Funktionstest l�uft";
static char cb_I_Ldb_Test9_Running[] = "Test pressione in corso";
static char cb_P_Ldb_Test9_Running[] = "Teste de desempenho de press�o em execu��o";

static char cb_E_Ldb_Pressure_incorrect[] = "Pressure measurement incorrect!";
static char cb_S_Ldb_Pressure_incorrect[] = "Medici�n de presi�n incorrecta!";
static char cb_F_Ldb_Pressure_incorrect[] = "Mesure de pression incorrecte!";
static char cb_G_Ldb_Pressure_incorrect[] = "Druck-Me�wert falsch!";
static char cb_I_Ldb_Pressure_incorrect[] = "Misurazione pressione errata!";
static char cb_P_Ldb_Pressure_incorrect[] = "Medi��o de press�o incorreta!";

static char cb_E_Ldb_pres_5[]  = "Pressure 5.0";
static char cb_E_Ldb_pres_90[] = "Pressure 90.0";

static char cb_S_Ldb_pres_5[]  = "Presi�n 5,0";
static char cb_S_Ldb_pres_90[] = "Presi�n 90,0";

static char cb_F_Ldb_pres_5[]  = "Pression 5,0";
static char cb_F_Ldb_pres_90[] = "Pression 90,0";

static char cb_G_Ldb_pres_5[]  = "Druck 5,0";
static char cb_G_Ldb_pres_90[] = "Druck 90,0";

static char cb_I_Ldb_pres_5[]  = "Pressione 5,0";
static char cb_I_Ldb_pres_90[] = "Pressione 90,0";

static char cb_P_Ldb_pres_5[]  = "Press�o 5,0";
static char cb_P_Ldb_pres_90[] = "Press�o 90,0";

// PEEPPerformanceTest
static char cb_E_Ldb_Test10_Running[] = "PEEP Performance Test running";
static char cb_S_Ldb_Test10_Running[] = "Ejecutando prueba de funcionamiento PEEP";
static char cb_F_Ldb_Test10_Running[] = "Test Performances PEP en cours";
static char cb_G_Ldb_Test10_Running[] = "PEEP-Funktionstest l�uft";
static char cb_I_Ldb_Test10_Running[] = "Test prestazioni PEEP in corso";
static char cb_P_Ldb_Test10_Running[] = "Teste de desempenho de PEEP em execu��o";

static char cb_E_Ldb_peep_incorrect[] = "PEEP measurement incorrect!";
static char cb_S_Ldb_peep_incorrect[] = "Medici�n de PEEP incorrecta!";
static char cb_F_Ldb_peep_incorrect[] = "Mesure PEP incorrecte!";
static char cb_G_Ldb_peep_incorrect[] = "PEEP-Me�wert falsch!";
static char cb_I_Ldb_peep_incorrect[] = "Misurazione PEEP errata!";
static char cb_P_Ldb_peep_incorrect[] = "Medi��o de PEEP incorreta!";

static char cb_E_Ldb_peep_5[]  = "PEEP 5.0";
static char cb_E_Ldb_peep_25[] = "PEEP 25.0";
static char cb_E_Ldb_peep_45[] = "PEEP 45.0";

static char cb_S_Ldb_peep_5[]  = "PEEP 5,0";
static char cb_S_Ldb_peep_25[] = "PEEP 25,0";
static char cb_S_Ldb_peep_45[] = "PEEP 45,0";

static char cb_F_Ldb_peep_5[]  = "PEP 5,0";
static char cb_F_Ldb_peep_25[] = "PEP 25,0";
static char cb_F_Ldb_peep_45[] = "PEP 45,0";

static char cb_G_Ldb_peep_5[]  = "PEEP 5,0";
static char cb_G_Ldb_peep_25[] = "PEEP 25,0";
static char cb_G_Ldb_peep_45[] = "PEEP 45,0";

static char cb_I_Ldb_peep_5[]  = "PEEP 5,0";
static char cb_I_Ldb_peep_25[] = "PEEP 25,0";
static char cb_I_Ldb_peep_45[] = "PEEP 45,0";

static char cb_P_Ldb_peep_5[]  = "PEEP 5,0";
static char cb_P_Ldb_peep_25[] = "PEEP 25,0";
static char cb_P_Ldb_peep_45[] = "PEEP 45,0";

// FIO2PerformanceTest
static char cb_E_Ldb_Test11_Running[] = "O2 Performance Test running";
static char cb_S_Ldb_Test11_Running[] = "Ejecutando prueba de funcionamiento de O2";
static char cb_F_Ldb_Test11_Running[] = "Test Performances O2 en cours";
static char cb_G_Ldb_Test11_Running[] = "O2-Funktionstest l�uft";
static char cb_I_Ldb_Test11_Running[] = "Test O2 in corso";
static char cb_P_Ldb_Test11_Running[] = "Teste de desempenho de O2 em execu��o";

static char cb_E_Ldb_oxy_conc[] = "O2 concentration";
static char cb_S_Ldb_oxy_conc[] = "Concentraci�n de O2";
static char cb_F_Ldb_oxy_conc[] = "Concentration O2";
static char cb_G_Ldb_oxy_conc[] = "O2-Konzentration";
static char cb_I_Ldb_oxy_conc[] = "Concentrazione O2";
static char cb_P_Ldb_oxy_conc[] = "Concentra��o de O2";

static char cb_E_Ldb_enter_oxy[] = "Enter O2 reading from the 840 Ventilator.";
static char cb_S_Ldb_enter_oxy[] = "Introducir la lectura de O2 del ventilador 840.";
static char cb_F_Ldb_enter_oxy[] = "Entrer lecture 02 du ventilateur 840.";
static char cb_G_Ldb_enter_oxy[] = "O2-Me�wert vom Beatmungsger�t 840 eingeben.";
static char cb_I_Ldb_enter_oxy[] = "Immettere lettura O2 ventilatore 840.";
static char cb_P_Ldb_enter_oxy[] = "Inserir leitura de O2 do Ventilador 840.";

static char cb_E_Ldb_oxy_incorrect[] = "O2 measurement incorrect!";
static char cb_S_Ldb_oxy_incorrect[] = "Medici�n de O2 incorrecta!";
static char cb_F_Ldb_oxy_incorrect[] = "Mesure O2 incorrecte!";
static char cb_G_Ldb_oxy_incorrect[] = "O2-Me�wert falsch!";
static char cb_I_Ldb_oxy_incorrect[] = "Misurazione O2 non corretta!";
static char cb_P_Ldb_oxy_incorrect[] = "Medi��o de O2 incorreta!";

static char cb_E_Ldb_oxygen_30[] = "O2 %30";
static char cb_E_Ldb_oxygen_90[] = "O2 %90";

static char cb_S_Ldb_oxygen_30[] = "O2 %30";
static char cb_S_Ldb_oxygen_90[] = "O2 %90";

static char cb_F_Ldb_oxygen_30[] = "O2 %30";
static char cb_F_Ldb_oxygen_90[] = "O2 %90";

static char cb_G_Ldb_oxygen_30[] = "O2 %30";
static char cb_G_Ldb_oxygen_90[] = "O2 %90";

static char cb_I_Ldb_oxygen_30[] = "O2 %30";
static char cb_I_Ldb_oxygen_90[] = "O2 %90";

static char cb_P_Ldb_oxygen_30[] = "O2 %30";
static char cb_P_Ldb_oxygen_90[] = "O2 %90";

static char cb_E_Ldb_vent_oxy_rdg[] = "840 Ventilator reading at %1.2f";
static char cb_S_Ldb_vent_oxy_rdg[] = "Ventilador 840 leyendo a %1.2f";
static char cb_F_Ldb_vent_oxy_rdg[] = "Lecture du ventilateur 840 � %1.2f";
static char cb_G_Ldb_vent_oxy_rdg[] = "Beatmungsger�t 840 Me�wert bei %1.2f";
static char cb_I_Ldb_vent_oxy_rdg[] = "Lettura ventilatore 840 a %1.2f";
static char cb_P_Ldb_vent_oxy_rdg[] = "Ventilador 840 lendo em %1.2f";

// SetPerfPanelFIO2
static char cb_E_Ldb_fio2_inst[] = "O2 Performance Test instruction box";
static char cb_S_Ldb_fio2_inst[] = "Cuadro de instrucciones prueba de funcionamiento de O2";
static char cb_F_Ldb_fio2_inst[] = "Case instruct. Test Performances O2";
static char cb_G_Ldb_fio2_inst[] = "O2-Funktionstest Anweisungsfenster";
static char cb_I_Ldb_fio2_inst[] = "Confezione istruzioni Test prestazioni O2";
static char cb_P_Ldb_fio2_inst[] = "Caixa de instru��o do teste de desempenho de O2";

// 9 steps
static char cb_E_Ldb_9step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s\n7. %s\n8. %s\n9. %s";
static char cb_S_Ldb_9step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s\n7. %s\n8. %s\n9. %s";
static char cb_F_Ldb_9step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s\n7. %s\n8. %s\n9. %s";
static char cb_G_Ldb_9step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s\n7. %s\n8. %s\n9. %s";
static char cb_I_Ldb_9step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s\n7. %s\n8. %s\n9. %s";
static char cb_P_Ldb_9step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s\n7. %s\n8. %s\n9. %s";

static char cb_E_Ldb_VentOffandOn[] = "Remove test lung, turn ventilator off, then back on.";
static char cb_S_Ldb_VentOffandOn[] = "Retire el pulm�n de prueba, apague el ventilador y, a continuaci�n, vuelva a encenderlo.";
static char cb_F_Ldb_VentOffandOn[] = "Retirez le poumon test, �teignez puis rallumez le ventilateur.";
static char cb_G_Ldb_VentOffandOn[] = "Testlunge entfernen, Beatmungsger�t aus- und wieder einschalten.";
static char cb_I_Ldb_VentOffandOn[] = "Rimuovere il simulatore polmonare, spegneree riaccendere il ventilatore.";
static char cb_P_Ldb_VentOffandOn[] = "Remova o pulm�o de teste, desligue e ligue o ventilador.";

static char cb_E_Ldb_IBW_20kg[] = "Set patient IBW to 20 kg.";
static char cb_S_Ldb_IBW_20kg[] = "Establecer PCI del paciente en 20 kg.";
static char cb_F_Ldb_IBW_20kg[] = "R�gler PIDP sur 20 kg.";
static char cb_G_Ldb_IBW_20kg[] = "IBW Patient auf 20 kg stellen.";
static char cb_I_Ldb_IBW_20kg[] = "Imposta PCI su 20 kg.";
static char cb_P_Ldb_IBW_20kg[] = "Definir IBW do paciente para 20 kg.";

static char cb_E_Ldb_Mode2AC[] = "Set mode to Assist/Control (A/C).";
static char cb_S_Ldb_Mode2AC[] = "Establecer el modo en Asistencia/Control (A/C).";
static char cb_F_Ldb_Mode2AC[] = "R�gler mode sur Assist�e/Contr�l�e (A/C).";
static char cb_G_Ldb_Mode2AC[] = "Modus Assist/Control (A/C) ausw�hlen.";
static char cb_I_Ldb_Mode2AC[] = "Imposta modalit� su Assistita/Controllata (A/C).";
static char cb_P_Ldb_Mode2AC[] = "Definir modo para A/C (assistido/controlado).";

static char cb_E_Ldb_ManType2VC[] = "Set 840 Ventilator mandatory type to Volume Control (VC).";
static char cb_S_Ldb_ManType2VC[] = "Establecer el tipo obligatorio de ventilador en Control de volumen (CV).";
static char cb_F_Ldb_ManType2VC[] = "R�gler type de ventilation sur Volume contr�l� (VC).";
static char cb_G_Ldb_ManType2VC[] = "F�r kontrollierte Atemh�be VC (Volume Control) w�hlen.";
static char cb_I_Ldb_ManType2VC[] = "Imposta su Ventilazione a volume controllato (VCV).";
static char cb_P_Ldb_ManType2VC[] = "Definir tipo obrigat�rio de ventilador para VC (controle de volume).";

static char cb_E_Ldb_Trig2PTRIG[] = "Set trigger to P-TRIG.";
static char cb_S_Ldb_Trig2PTRIG[] = "Establecer el disparo en DISP-P.";
static char cb_F_Ldb_Trig2PTRIG[] = "R�gler d�clenchement sur P-D�CL.";
static char cb_G_Ldb_Trig2PTRIG[] = "Trigger auf P-TRIG stellen.";
static char cb_I_Ldb_Trig2PTRIG[] = "Imposta trigger su P-TRIG.";
static char cb_P_Ldb_Trig2PTRIG[] = "Definir disparo para P-TRIG.";

static char cb_E_Ldb_MakeOtherSettings[] = "Make other setting changes as specified above.";
static char cb_S_Ldb_MakeOtherSettings[] = "Realizar otros cambios en los par�metros como se especifica anteriormente.";
static char cb_F_Ldb_MakeOtherSettings[] = "Modifier autres r�glages comme indiqu� ci-dessus.";
static char cb_G_Ldb_MakeOtherSettings[] = "Andere Einstellungen wie oben angegeben �ndern.";
static char cb_I_Ldb_MakeOtherSettings[] = "Apporta le altre modifiche specificate alle impostazioni.";
static char cb_P_Ldb_MakeOtherSettings[] = "Fazer outras altera��es de configura��o conforme especificado acima.";

static char cb_E_Ldb_DisableAlarms[] = "Set alarm min and max settings to their respective limits to disable alarms.";
static char cb_S_Ldb_DisableAlarms[] = "Establecer los par�metros m�n y m�x de alarma a sus l�mites respectivos para desactivar las alarmas.";
static char cb_F_Ldb_DisableAlarms[] = "R�gler alarmes min et max sur limites respectives pour d�sactiver alarmes.";
static char cb_G_Ldb_DisableAlarms[] = "Min. und max. Alarmeinstellungen auf ihre jeweiligen Grenzwerte einstellen, um die Alarme abzuschalten.";
static char cb_I_Ldb_DisableAlarms[] = "Per disattivare gli allarmi, impostare come valore minimo e massimo i relativi limiti.";
static char cb_P_Ldb_DisableAlarms[] = "Definir configura��es m�n. e m�x. de alarme para seus respectivos limites a fim de desativar alarmes.";

static char cb_E_Ldb_EnsurehalfLLung[] = "Attach 0.5 liter lung simulator.";
static char cb_S_Ldb_EnsurehalfLLung[] = "Conectar simulador de pulm�n de 0,5 litros.";
static char cb_F_Ldb_EnsurehalfLLung[] = "Brancher le poumon test de 0,5 litre.";
static char cb_G_Ldb_EnsurehalfLLung[] = "0,5-Liter-Testlunge anschlie�en.";
static char cb_I_Ldb_EnsurehalfLLung[] = "Collegare il simulatore polmonare da 0,5 litri.";
static char cb_P_Ldb_EnsurehalfLLung[] = "Conectar simulador de pulm�o de 0,5 litro.";

static char cb_E_Ldb_Givefiveminutes[] = "Wait a minimum of 5 min. before continuing.";
static char cb_S_Ldb_Givefiveminutes[] = "Espere por lo menos 5 minutos antes de continuar.";
static char cb_F_Ldb_Givefiveminutes[] = "Attendre au moins 5 min. avant de continuer.";
static char cb_G_Ldb_Givefiveminutes[] = "Vor dem Fortsetzen mindestens 5 Minuten warten.";
static char cb_I_Ldb_Givefiveminutes[] = "Attendere almeno 5 minuti prima di proseguire.";
static char cb_P_Ldb_Givefiveminutes[] = "Aguardar um m�nimo de 5 minutos antes de continuar.";

static char cb_E_Ldb_MakeSettings[] = "Make setting changes as specified above.";
static char cb_S_Ldb_MakeSettings[] = "Realizar otros cambios en los par�metros como se especifica anteriormente.";
static char cb_F_Ldb_MakeSettings[] = "Modifier r�glages comme indiqu� ci-dessus.";
static char cb_G_Ldb_MakeSettings[] = "Einstellungen wie oben angegeben �ndern.";
static char cb_I_Ldb_MakeSettings[] = "Apporta le modifiche specificate alle impostazioni.";
static char cb_P_Ldb_MakeSettings[] = "Fazer altera��es de configura��o conforme especificado acima.";

// SetPerfPanelPeep
static char cb_E_Ldb_peep_inst[] = "PEEP Performance Test instruction box";
static char cb_S_Ldb_peep_inst[] = "Cuadro de instrucciones prueba de funcionamiento de PEEP";
static char cb_F_Ldb_peep_inst[] = "Case instruct. Test Performances PEP";
static char cb_G_Ldb_peep_inst[] = "PEEP-Funktionstest Anweisungsfenster";
static char cb_I_Ldb_peep_inst[] = "Confezione istruzioni Test prestazioni PEEP";
static char cb_P_Ldb_peep_inst[] = "Caixa de instru��o do teste de desempenho de PEEP";

static char cb_E_Ldb_2step[] = "1. %s\n2. %s";
static char cb_S_Ldb_2step[] = "1. %s\n2. %s";
static char cb_F_Ldb_2step[] = "1. %s\n2. %s";
static char cb_G_Ldb_2step[] = "1. %s\n2. %s";
static char cb_I_Ldb_2step[] = "1. %s\n2. %s";
static char cb_P_Ldb_2step[] = "1. %s\n2. %s";

static char cb_E_Ldb_IBW_85kg[] = "Set patient IBW to 85 kg.";
static char cb_S_Ldb_IBW_85kg[] = "Establecer PCI del paciente en 85 kg.";
static char cb_F_Ldb_IBW_85kg[] = "R�gler PIDP sur 85 kg.";
static char cb_G_Ldb_IBW_85kg[] = "IBW Patient auf 85 kg stellen.";
static char cb_I_Ldb_IBW_85kg[] = "Imposta PCI su 85 kg.";
static char cb_P_Ldb_IBW_85kg[] = "Definir IBW do paciente para 85 kg.";

static char cb_E_Ldb_ManType2PC[] = "Set 840 Ventilator mandatory type to Pressure Control (PC).";
static char cb_S_Ldb_ManType2PC[] = "Establecer el tipo mandatorio de ventilador en Control de presi�n (CP)";
static char cb_F_Ldb_ManType2PC[] = "R�gler type de ventilation sur Pression contr�l�e (PC).";
static char cb_G_Ldb_ManType2PC[] = "F�r kontrollierte Atemh�be PC (Pressure Control) w�hlen.";
static char cb_I_Ldb_ManType2PC[] = "Imposta su Ventilazione a pressione controllata (PCV).";
static char cb_P_Ldb_ManType2PC[] = "Definir tipo obrigat�rio de ventilador para PC (controle de press�o).";

static char cb_E_Ldb_Ensure4LLung[] = "Attach 4 liter lung simulator.";
static char cb_S_Ldb_Ensure4LLung[] = "Conectar simulador de pulm�n de 4 litros.";
static char cb_F_Ldb_Ensure4LLung[] = "Brancher le poumon test de 4 litres.";
static char cb_G_Ldb_Ensure4LLung[] = "4-Liter-Testlunge anschlie�en.";
static char cb_I_Ldb_Ensure4LLung[] = "Collegare il simulatore polmonare da 4 litri.";
static char cb_P_Ldb_Ensure4LLung[] = "Conectar simulador de pulm�o de 4 litros.";

static char cb_E_Ldb_Givefivebreaths[] = "Wait a minimum of 5 breaths before continuing.";
static char cb_S_Ldb_Givefivebreaths[] = "Espere por lo menos 5 respiraciones antes de continuar.";
static char cb_F_Ldb_Givefivebreaths[] = "Attendre au moins 5 cycles avant de continuer.";
static char cb_G_Ldb_Givefivebreaths[] = "Vor dem Fortsetzen mindestens 5 Atemh�be warten.";
static char cb_I_Ldb_Givefivebreaths[] = "Attendere almeno 5 atti respiratoriprima di proseguire.";
static char cb_P_Ldb_Givefivebreaths[] = "Aguardar um m�nimo de 5 respira��es antes de continuar.";

// SetPerfPanelPressure
static char cb_E_Ldb_pres_inst[] = "Pressure Performance Test instruction box";
static char cb_S_Ldb_pres_inst[] = "Cuadro de instrucciones Prueba de funcionamiento de la presi�n";
static char cb_F_Ldb_pres_inst[] = "Case instruct. Test Performances pression";
static char cb_G_Ldb_pres_inst[] = "Druck-Funktionstest Anweisungsfenster";
static char cb_I_Ldb_pres_inst[] = "Casella istruzioni Test prestazioni pressione";
static char cb_P_Ldb_pres_inst[] = "Caixa de instru��o de teste de desempenho de press�o";

static char cb_E_Ldb_4step[] = "1. %s\n2. %s\n3. %s\n4. %s";
static char cb_S_Ldb_4step[] = "1. %s\n2. %s\n3. %s\n4. %s";
static char cb_F_Ldb_4step[] = "1. %s\n2. %s\n3. %s\n4. %s";
static char cb_G_Ldb_4step[] = "1. %s\n2. %s\n3. %s\n4. %s";
static char cb_I_Ldb_4step[] = "1. %s\n2. %s\n3. %s\n4. %s";
static char cb_P_Ldb_4step[] = "1. %s\n2. %s\n3. %s\n4. %s";

// SetPerfPanelVolume
static char cb_E_Ldb_vol_inst[] = "Volume Performance Test instruction box";
static char cb_S_Ldb_vol_inst[] = "Cuadro de instrucciones prueba de funcionamiento del volumen";
static char cb_F_Ldb_vol_inst[] = "Case instruct. Test Performances volume";
static char cb_G_Ldb_vol_inst[] = "Volumen-Funktionstest Anweisungsfenster";
static char cb_I_Ldb_vol_inst[] = "Casella istruzioni Test volume";
static char cb_P_Ldb_vol_inst[] = "Caixa de instru��o do teste de desempenho de volume";

static char cb_E_Ldb_IBW_10kg[] = "Set patient IBW to 10 kg.";
static char cb_S_Ldb_IBW_10kg[] = "Establecer PCI del paciente en 10 kg.";
static char cb_F_Ldb_IBW_10kg[] = "R�gler PIDP sur 10 kg.";
static char cb_G_Ldb_IBW_10kg[] = "IBW Patient auf 10 kg stellen.";
static char cb_I_Ldb_IBW_10kg[] = "Imposta PCI su 10 kg.";
static char cb_P_Ldb_IBW_10kg[] = "Definir IBW do paciente para 10 kg.";

static char cb_E_Ldb_3step[] = "1. %s\n2. %s\n3. %s";
static char cb_S_Ldb_3step[] = "1. %s\n2. %s\n3. %s";
static char cb_F_Ldb_3step[] = "1. %s\n2. %s\n3. %s";
static char cb_G_Ldb_3step[] = "1. %s\n2. %s\n3. %s";
static char cb_I_Ldb_3step[] = "1. %s\n2. %s\n3. %s";
static char cb_P_Ldb_3step[] = "1. %s\n2. %s\n3. %s";

static char cb_E_Ldb_6step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s";
static char cb_S_Ldb_6step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s";
static char cb_F_Ldb_6step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s";
static char cb_G_Ldb_6step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s";
static char cb_I_Ldb_6step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s";
static char cb_P_Ldb_6step[] = "1. %s\n2. %s\n3. %s\n4. %s\n5. %s\n6. %s";

static char cb_E_Ldb_Ensure3LLung[] = "Attach 3 liter lung simulator.";
static char cb_S_Ldb_Ensure3LLung[] = "Conectar simulador de pulm�n de 3 litros.";
static char cb_F_Ldb_Ensure3LLung[] = "Brancher le poumon test de 3 litres.";
static char cb_G_Ldb_Ensure3LLung[] = "3-Liter-Testlunge anschlie�en.";
static char cb_I_Ldb_Ensure3LLung[] = "Collegare il simulatore polmonare da 3 litri.";
static char cb_P_Ldb_Ensure3LLung[] = "Conectar simulador de pulm�o de 3 litros.";

static char cb_E_Ldb_Trig2VTRIG[] = "Set trigger to V-TRIG.";
static char cb_S_Ldb_Trig2VTRIG[] = "Establecer el disparo en DISP-V.";
static char cb_F_Ldb_Trig2VTRIG[] = "R�gler d�clenchement sur V-D�CL.";
static char cb_G_Ldb_Trig2VTRIG[] = "Trigger auf V-TRIG stellen.";
static char cb_I_Ldb_Trig2VTRIG[] = "Imposta trigger su V-TRIG.";
static char cb_P_Ldb_Trig2VTRIG[] = "Definir disparo para V-TRIG.";

// TestPrompt
// BDUAlarm
static char cb_E_Ldb_BDU_Title[] = "Breath Delivery Alarm Test";
static char cb_S_Ldb_BDU_Title[] = "Prueba de la alarma de administraci�n de respiraci�n";
static char cb_F_Ldb_BDU_Title[] = "Test Alarme insufflation d'air";
static char cb_G_Ldb_BDU_Title[] = "Beatmungsalarm-Test";
static char cb_I_Ldb_BDU_Title[] = "Test allarme erogazione ventilazione";
static char cb_P_Ldb_BDU_Title[] = "Teste de alarme de fornecimento de respira��o ";

static char cb_E_Ldb_BDU_Message[] = "Do you hear the breath delivery alarm?";
static char cb_S_Ldb_BDU_Message[] = "�Puede o�r la alarma de administraci�n de respiraci�n?";
static char cb_F_Ldb_BDU_Message[] = "Entendez-vous l'alarme d'insufflation d'air?";
static char cb_G_Ldb_BDU_Message[] = "Ist der Beatmungsalarm zu h�ren?";
static char cb_I_Ldb_BDU_Message[] = "Allarme erogazione ventilazione attivo?";
static char cb_P_Ldb_BDU_Message[] = "Voc� ouve o alarme de fornecimento de respira��o?";

// GUIAlarm
static char cb_E_Ldb_GUI_Title[] = "GUI Alarm Test";
static char cb_S_Ldb_GUI_Title[] = "Prueba de alarma IGU";
static char cb_F_Ldb_GUI_Title[] = "Test Alarme IGU";
static char cb_G_Ldb_GUI_Title[] = "GUI-Alarm-Test";
static char cb_I_Ldb_GUI_Title[] = "Test allarme GUI";
static char cb_P_Ldb_GUI_Title[] = "Teste de alarme de GUI";

static char cb_E_Ldb_GUI_Message[] = "Do you hear the GUI alarm?";
static char cb_S_Ldb_GUI_Message[] = "�Puede o�r la alarma IGU?";
static char cb_F_Ldb_GUI_Message[] = "Entendez-vous l'alarme IGU?";
static char cb_G_Ldb_GUI_Message[] = "Ist der GUI-Alarm zu h�ren?";
static char cb_I_Ldb_GUI_Message[] = "Allarme GUI attivo?";
static char cb_P_Ldb_GUI_Message[] = "Voc� ouve o alarme da GUI?";

static char cb_E_Ldb_Circ_Pres[] = "Circuit Pressure Test";
static char cb_S_Ldb_Circ_Pres[] = "Prueba de presi�n del circuito";
static char cb_F_Ldb_Circ_Pres[] = "Test Pression circuit";
static char cb_G_Ldb_Circ_Pres[] = "Schlauchsystemdruck-Test";
static char cb_I_Ldb_Circ_Pres[] = "Test pressione circuito";
static char cb_P_Ldb_Circ_Pres[] = "Teste de press�o de circuito";

// CPT1
static char cb_E_Ldb_CP_Air_Msg[] = "Ensure that air and O2 are properly supplied to the 840 Ventilator.";
static char cb_S_Ldb_CP_Air_Msg[] = "Compruebe que el ventilador 840 recibe un suministro adecuado de aire y O2.";
static char cb_F_Ldb_CP_Air_Msg[] = "V�rifier alimentation air et O2 du ventilateur 840.";
static char cb_G_Ldb_CP_Air_Msg[] = "Sicherstellen, da� das Beatmungsger�t 840 korrekt mit Air und O2 versorgt wird.";
static char cb_I_Ldb_CP_Air_Msg[] = "Verificare che l'aria e l'ossigeno siano collegati correttamente al ventilatore 840.";
static char cb_P_Ldb_CP_Air_Msg[] = "Garantir que ar e O2 estejam sendo fornecidos corretamente ao Ventilador 840.";

// CPT2
static char cb_E_Ldb_CP_CIR_Msg[] = "Place PTS 2000 in series with the test circuit.";
static char cb_S_Ldb_CP_CIR_Msg[] = "Conecte el PTS 2000 en serie con el circuito de prueba.";
static char cb_F_Ldb_CP_CIR_Msg[] = "Int�grer PTS 2000 au circuit de test.";
static char cb_G_Ldb_CP_CIR_Msg[] = "PTS 2000 in Serie mit Testschlauchsystem schalten.";
static char cb_I_Ldb_CP_CIR_Msg[] = "Collegare il PTS 2000 in serie al circuito di prova.";
static char cb_P_Ldb_CP_CIR_Msg[] = "Colocar PTS 2000 em s�rie com o circuito de teste.";

// SST
static char cb_E_Ldb_SST_Msg_1[] = "Perform SST by connecting the Patient Circuit to the PTS 2000. When prompted to block Wye, block the rear port of the PTS 2000. Perform SST using an adult circuit and HME setting.";
static char cb_E_Ldb_SST_Msg_2[] = "Patient circuit type: Adult";
static char cb_E_Ldb_SST_Msg_3[] = "Humidification type : HME";
static char cb_E_Ldb_SST_Title[] = "Short Self Test (SST)";

static char cb_S_Ldb_SST_Msg_1[] = 
						"Realice el autotest corto (ATC) conectando el circuito del paciente al PTS 2000. Cuando se le indique que bloquee la ""Y"", bloquee el puerto posterior del PTS 2000. Realice este autotest utilizando un circuito de adulto y el ajuste HME.";
static char cb_S_Ldb_SST_Msg_2[] = "Tipo de circuito de paciente: adulto";
static char cb_S_Ldb_SST_Msg_3[] = "Tipo de humidificaci�n: HME";
static char cb_S_Ldb_SST_Title[] = "Autotest corto (ATC)";

static char cb_F_Ldb_SST_Msg_1[] = "Effectuer ATR en connectant le circuit patient au PTS 2000. A la demande de blocage du raccord en Y, bouchez le port arri�re du PTS 2000. Effectuez l'ATR en utilisant un circuit adulte et les r�glages HME.";
static char cb_F_Ldb_SST_Msg_2[] = "Type de circuit patient: Adulte";
static char cb_F_Ldb_SST_Msg_3[] = "Type d'humidification: HME";
static char cb_F_Ldb_SST_Title[] = "Autotest rapide (ATR)";

static char cb_G_Ldb_SST_Msg_1[] = "F�r SST Y-St�ck an PTS 2000 anschlie�en. Bei Aufforderung, das Y-St�ck zu blockieren, r�ckseitigen Anschlu� des PTS 2000 blockieren. SST mit Einstellungen Erwachsenen-Schlauchsystem und HME durchf�hren.";
static char cb_G_Ldb_SST_Msg_2[] = "Schlauchsystemtyp: Erwachsene";
static char cb_G_Ldb_SST_Msg_3[] = "Befeuchtertyp: HME";
static char cb_G_Ldb_SST_Title[] = "Kurzer Selbsttest (SST)";

static char cb_I_Ldb_SST_Msg_1[] = "Per eseguire SST collegare il circuito paziente al PTS 2000. Alla richiesta di bloccare la Y, blocare la porta posteriore del PTS 2000. Per eseguire SST utilizzare un'impostazione circuito adulto e HME.";
static char cb_I_Ldb_SST_Msg_2[] = "Circuito paziente: Adulto";
static char cb_I_Ldb_SST_Msg_3[] = "Tipo umidificazione: HME";
static char cb_I_Ldb_SST_Title[] = "Test autodiagnostico breve (SST)";

static char cb_P_Ldb_SST_Msg_1[] = 
						"Execute o SST conectando o Circuito do Paciente ao PTS 2000. Quando for solicitado a bloquear o tubo em forma de V, bloqueie a porta traseira do PTS 2000. Execute o SST usando um circuito de adulto e a defini��o de HME.";
static char cb_P_Ldb_SST_Msg_2[] = "Tipo de circuito do paciente: Adulto";
static char cb_P_Ldb_SST_Msg_3[] = "Tipo de umidifica��o: HME";
static char cb_P_Ldb_SST_Title[] = "SST (Autoteste curto)";

static char cb_E_Ldb_sst_proc_error[] = "SST procedural error";
static char cb_S_Ldb_sst_proc_error[] = "Error de procedimiento ATC";
static char cb_F_Ldb_sst_proc_error[] = "Erreur de proc�dure ATR";
static char cb_G_Ldb_sst_proc_error[] = "SST Prozedurfehler";
static char cb_I_Ldb_sst_proc_error[] = "Errore procedurale SST";
static char cb_P_Ldb_sst_proc_error[] = "Erro de procedimento do SST";

// Test step identification
static char cb_E_Ldb_step_x_y[] = "Step %d of %d";
static char cb_S_Ldb_step_x_y[] = "Paso %d de %d";
static char cb_F_Ldb_step_x_y[] = "�tape %d sur %d";
static char cb_G_Ldb_step_x_y[] = "Schritt %d von %d";
static char cb_I_Ldb_step_x_y[] = "Passaggio %d di %d";
static char cb_P_Ldb_step_x_y[] = "Etapa %d de %d";

// SetManTypeDisplay
// PERF_PANEL_MANTYPE
static char cb_E_Ldb_PC[] = "PC";
static char cb_S_Ldb_PC[] = "CP";
static char cb_F_Ldb_PC[] = "PC";
static char cb_G_Ldb_PC[] = "PC";
static char cb_I_Ldb_PC[] = "PCV";
static char cb_P_Ldb_PC[] = "PC";

static char cb_E_Ldb_VC[] = "VC";
static char cb_S_Ldb_VC[] = "CV";
static char cb_F_Ldb_VC[] = "VC";
static char cb_G_Ldb_VC[] = "VC";
static char cb_I_Ldb_VC[] = "VC";
static char cb_P_Ldb_VC[] = "VC";

// SetTriggerTypeDisplay
// PERF_PANEL_TRIGGER
static char cb_E_Ldb_PTRIG[] = "P-TRIG";
static char cb_S_Ldb_PTRIG[] = "P-DISP";
static char cb_F_Ldb_PTRIG[] = "P-D�CL";
static char cb_G_Ldb_PTRIG[] = "P-TRIG";
static char cb_I_Ldb_PTRIG[] = "P-TRIG";
static char cb_P_Ldb_PTRIG[] = "P-TRIG";

// PERF_PANEL_PSENS_TITLE
static char cb_E_Ldb_Psens[] = "Psens";
static char cb_S_Ldb_Psens[] = "Psens";
static char cb_F_Ldb_Psens[] = "Psens";
static char cb_G_Ldb_Psens[] = "Psens";
static char cb_I_Ldb_Psens[] = "Psens";
static char cb_P_Ldb_Psens[] = "Psens";

static char cb_E_Ldb_VTRIG[] = "V-TRIG";
static char cb_S_Ldb_VTRIG[] = "DISP-V";
static char cb_F_Ldb_VTRIG[] = "V-D�CL";
static char cb_G_Ldb_VTRIG[] = "V-TRIG";
static char cb_I_Ldb_VTRIG[] = "V-TRIG";
static char cb_P_Ldb_VTRIG[] = "V-TRIG";

static char cb_E_Ldb_Vsens[] = "Vsens";
static char cb_S_Ldb_Vsens[] = "Vsens";
static char cb_F_Ldb_Vsens[] = "Vsens";
static char cb_G_Ldb_Vsens[] = "Vsens";
static char cb_I_Ldb_Vsens[] = "Vsens";
static char cb_P_Ldb_Vsens[] = "Vsens";

// SetWaveFormDisplay
// PERF_PANEL_WAVEFORM
static char cb_E_Ldb_SQUARE[] = "SQUARE";
static char cb_S_Ldb_SQUARE[] = "CUADRADA";
static char cb_F_Ldb_SQUARE[] = "RECTANGUL";
static char cb_G_Ldb_SQUARE[] = "RECHTECK";
static char cb_I_Ldb_SQUARE[] = "QUADRA";
static char cb_P_Ldb_SQUARE[] = "QUADRADO";

static char cb_E_Ldb_TRIANGLE[] = "TRIANGLE";
static char cb_S_Ldb_TRIANGLE[] = "TRI�NGULO";
static char cb_F_Ldb_TRIANGLE[] = "TRIANGLE";
static char cb_G_Ldb_TRIANGLE[] = "DREIECK";
static char cb_I_Ldb_TRIANGLE[] = "TRIANGOLO";
static char cb_P_Ldb_TRIANGLE[] = "TRI�NGULO";

// MakeAutozeroPanel
static char cb_E_Ldb_Calibrate[] = "Calibrate";
static char cb_S_Ldb_Calibrate[] = "Calibrar";
static char cb_F_Ldb_Calibrate[] = "�talonner";
static char cb_G_Ldb_Calibrate[] = "Kalibrieren";
static char cb_I_Ldb_Calibrate[] = "Calibra";
static char cb_P_Ldb_Calibrate[] = "Calibrar";

static char cb_E_Ldb_Selection_Panel[] = "";
static char cb_S_Ldb_Selection_Panel[] = "";
static char cb_F_Ldb_Selection_Panel[] = "";
static char cb_G_Ldb_Selection_Panel[] = "";
static char cb_I_Ldb_Selection_Panel[] = "";
static char cb_P_Ldb_Selection_Panel[] = "";

static char cb_E_Ldb_AutoZeroHiPres[] = "Autozero high pressure";
static char cb_S_Ldb_AutoZeroHiPres[] = "Presi�n alta a cero";
static char cb_F_Ldb_AutoZeroHiPres[] = "Autoz�ro pression maxi";
static char cb_G_Ldb_AutoZeroHiPres[] = "Nullabgleich hoher Druck";
static char cb_I_Ldb_AutoZeroHiPres[] = "Azzeramento automatico alta pressione";
static char cb_P_Ldb_AutoZeroHiPres[] = "Press�o alta de autozero";

static char cb_E_Ldb_AutoZeroLoPres[] = "Autozero low pressure";
static char cb_S_Ldb_AutoZeroLoPres[] = "Presi�n baja a cero";
static char cb_F_Ldb_AutoZeroLoPres[] = "Autoz�ro pression mini";
static char cb_G_Ldb_AutoZeroLoPres[] = "Nullabgleich niedriger Druck";
static char cb_I_Ldb_AutoZeroLoPres[] = "Azzeramento automatico bassa pressione";
static char cb_P_Ldb_AutoZeroLoPres[] = "Press�o baixa de autozero";

static char cb_E_Ldb_CaliOxygen[] = "Calibrate O2";
static char cb_S_Ldb_CaliOxygen[] = "Calibrar O2";
static char cb_F_Ldb_CaliOxygen[] = "�talonner O2";
static char cb_G_Ldb_CaliOxygen[] = "Kalibrieren O2";
static char cb_I_Ldb_CaliOxygen[] = "Calibra O2";
static char cb_P_Ldb_CaliOxygen[] = "Calibrar O2";

// MakeAutozeroHPPanel
static char cb_E_Ldb_CaliHiPres[] = "Calibrate high pressure";
static char cb_S_Ldb_CaliHiPres[] = "Calibrar presi�n alta";
static char cb_F_Ldb_CaliHiPres[] = "�talonner pression maxi";
static char cb_G_Ldb_CaliHiPres[] = "Kalibrieren hoher Druck";
static char cb_I_Ldb_CaliHiPres[] = "Calibra alta pressione";
static char cb_P_Ldb_CaliHiPres[] = "Calibrar press�o alta";

static char cb_E_Ldb_Discon_Hi_Instr[] = "Disconnect hose from the HIGH PRESSURE port.";
static char cb_S_Ldb_Discon_Hi_Instr[] = "Desconecte la manguera del puerto de PRESI�N ALTA.";
static char cb_F_Ldb_Discon_Hi_Instr[] = "D�connecter tuyau du port PRESSION MAXI.";
static char cb_G_Ldb_Discon_Hi_Instr[] = "Schlauch vom Anschlu� HIGH PRESSURE entfernen.";
static char cb_I_Ldb_Discon_Hi_Instr[] = "Scollegare tubo da porta di ALTA PRESSIONE.";
static char cb_P_Ldb_Discon_Hi_Instr[] = "Desconectar tubo da porta PRESS�O ALTA.";

static char cb_E_Ldb_OK_When_Ready[] = "Press OK when ready.";
static char cb_S_Ldb_OK_When_Ready[] = "Presione Aceptar cuando est� listo.";
static char cb_F_Ldb_OK_When_Ready[] = "Press OK qd vous �tes pr�t.";
static char cb_G_Ldb_OK_When_Ready[] = "OK anklicken, wenn fertig.";
static char cb_I_Ldb_OK_When_Ready[] = "Al termine premere OK.";
static char cb_P_Ldb_OK_When_Ready[] = "Pressione OK quando estiver pronto.";

// MakeAutozeroLPPanel
static char cb_E_Ldb_CaliLoPres[] = "Calibrate low pressure";
static char cb_S_Ldb_CaliLoPres[] = "Calibrar presi�n baja";
static char cb_F_Ldb_CaliLoPres[] = "�talonner pression mini";
static char cb_G_Ldb_CaliLoPres[] = "Kalibrieren niedriger Druck";
static char cb_I_Ldb_CaliLoPres[] = "Calibra bassa pressione";
static char cb_P_Ldb_CaliLoPres[] = "Calibrar press�o baixa";

static char cb_E_Ldb_Discon_Lo_Instr[] = "Disconnect hose from the LOW PRESSURE port.";
static char cb_S_Ldb_Discon_Lo_Instr[] = "Desconecte la manguera del puerto de PRESI�N BAJA.";
static char cb_F_Ldb_Discon_Lo_Instr[] = "D�connecter tuyau du port PRESSION MINI.";
static char cb_G_Ldb_Discon_Lo_Instr[] = "Schlauch vom Anschlu� LOW PRESSURE entfernen.";
static char cb_I_Ldb_Discon_Lo_Instr[] = "Scollegare tubo da porta di BASSA PRESSIONE.";
static char cb_P_Ldb_Discon_Lo_Instr[] = "Desconectar tubo da porta PRESS�O BAIXA.";

// MakeAutozeroO2Panel
static char cb_E_Ldb_AutoZero_Cbuf1[] = "From 2 to 10 slpm of room air (for 21% O2) or 100% oxygen should be applied";
static char cb_E_Ldb_AutoZero_Cbuf2[] = " to the High Flow inlet for at least one minute before selecting an adjustment function.\n";
static char cb_E_Ldb_AutoZero_Cbuf3[] = "21% O2 must be done first, and both must be completed to adjust the Oxygen sensor.";

static char cb_S_Ldb_AutoZero_Cbuf1[] = "Debe aplicarse 2 a 10 slpm de aire ambiente (para un 21% de O2) o 100 % de O2";
//static char cb_S_Ldb_AutoZero_Cbuf1[] = "Deber�an aplicarse entre 2 y 10 slpm de aire ambiente (para un 21 % de O2) o 100 % de O2";
static char cb_S_Ldb_AutoZero_Cbuf2[] = " a la entrada de flujo alto durante al menos un minuto antes de seleccionar una funci�n de ajuste.\n";
static char cb_S_Ldb_AutoZero_Cbuf3[] = "21 % de O2 debe realizarse primero, y ambos deben completarse para ajustar el sensor de ox�geno.";
//static char cb_S_Ldb_AutoZero_Cbuf3[] = "L'�talonnage � 21% doit �tre ex�cut� en premier. Les 2 �talonnages doivent �tre ex�cut�s pour un �talonnage correct de la cellule.";

static char cb_F_Ldb_AutoZero_Cbuf1[] = "Un d�bit de 2 � 10 slpm d'air (21%) ou d'oxyg�ne (100%) doit �tre appliqu�";
static char cb_F_Ldb_AutoZero_Cbuf2[] = " vers l'entr�e Haut d�bit pendant une min au moins avant s�lection d'une fonction de r�glage.\n";
static char cb_F_Ldb_AutoZero_Cbuf3[] = "Ex�cuter d'abord 21% O2 et terminer les deux avant r�glage du capteur d'oxyg�ne.";

static char cb_G_Ldb_AutoZero_Cbuf1[] = "Von 2 bis 10 slpm Raumluft (bei 21% O2), oder es sollte 100% Sauerstoff angewendet werden";
static char cb_G_Ldb_AutoZero_Cbuf2[] = " an den High Flow Einla� f�r mindestens eine Minute, bevor eine Einstellfunktion gew�hlt wird.\n";
static char cb_G_Ldb_AutoZero_Cbuf3[] = "21% O2 mu� zuerst durchgef�hrt werden, und zum Einstellen des Sauerstoffsensors m�ssen beide beendet werden.";

static char cb_I_Ldb_AutoZero_Cbuf1[] = "Applicare 2 -10 slpm di aria ambiente (per 21% O2) o 100% ossigeno";
static char cb_I_Ldb_AutoZero_Cbuf2[] = " all'ingresso di alto flusso per almeno un minuto prima di selezionare una funzione di regolazione.\n";
static char cb_I_Ldb_AutoZero_Cbuf3[] = "Eseguire prima 21% O2. Eseguire entrambi prima di regolare il sensore dell'ossigeno.";

static char cb_P_Ldb_AutoZero_Cbuf1[] = "Devem ser aplicados de 2 a 10 slpm de ar ambiente (para O2 a 21%) ou oxig�nio a 100%";
static char cb_P_Ldb_AutoZero_Cbuf2[] = " � entrada Fluxo Alto por pelo menos um minuto antes de selecionar uma fun��o de ajuste.\n";
static char cb_P_Ldb_AutoZero_Cbuf3[] = "O2 a 21% deve ser conclu�do primeiro e ambos devem ser completados para ajustar o sensor de Oxig�nio.";

static char cb_E_Ldb_Adj_21_O2[]  = "Adjust gain for  21% O2.";
static char cb_E_Ldb_Adj_100_O2[] = "Adjust gain for 100% O2.";

static char cb_S_Ldb_Adj_21_O2[]  = "Ajustar ganancia para  21% de O2.";
static char cb_S_Ldb_Adj_100_O2[] = "Ajustar ganancia para 100% de O2.";

static char cb_F_Ldb_Adj_21_O2[]  = "R�gler augm. pour  21% O2.";
static char cb_F_Ldb_Adj_100_O2[] = "R�gler augm. pour 100% O2.";

static char cb_G_Ldb_Adj_21_O2[]  = "Verst�rkung einstellen f�r  21% O2.";
static char cb_G_Ldb_Adj_100_O2[] = "Verst�rkung einstellen f�r 100% O2.";

static char cb_I_Ldb_Adj_21_O2[]  = "Regola  21% O2.";
static char cb_I_Ldb_Adj_100_O2[] = "Regola 100% O2.";

static char cb_P_Ldb_Adj_21_O2[]  = "Ajuste de ganho para O2 a  21%.";
static char cb_P_Ldb_Adj_100_O2[] = "Ajuste de ganho para O2 a 100%.";

static char cb_E_Ldb_O2_prcnt[] = "O2% ";
static char cb_S_Ldb_O2_prcnt[] = "O2% ";
static char cb_F_Ldb_O2_prcnt[] = "%O2 ";
static char cb_G_Ldb_O2_prcnt[] = "O2% ";
static char cb_I_Ldb_O2_prcnt[] = "O2% ";
static char cb_P_Ldb_O2_prcnt[] = "O2% ";

// CmdAutoZero
static char cb_E_Ldb_EstabCom_PTS[] = "Establishing communication with PTS 2000";
static char cb_S_Ldb_EstabCom_PTS[] = "Establecimiento de comunicaci�n con PTS 2000";
static char cb_F_Ldb_EstabCom_PTS[] = "Communication en cours avec le PTS 2000";
static char cb_G_Ldb_EstabCom_PTS[] = "Kommunikationsaufbau mit dem PTS 2000";
static char cb_I_Ldb_EstabCom_PTS[] = "Comunicazione con PTS 2000 in corso";
static char cb_P_Ldb_EstabCom_PTS[] = "Estabelecendo comunica��o com o PTS 2000";

// AutoZeroCMDHP
static char cb_E_Ldb_HiPres_AutoZero_Succ[] = "High pressure autozero successful.";
static char cb_E_Ldb_HiPres_AutoZero_Fail[] = "High pressure autozero failed.";

static char cb_S_Ldb_HiPres_AutoZero_Succ[] = "Presi�n alta a cero con �xito.";
static char cb_S_Ldb_HiPres_AutoZero_Fail[] = "Fallo al poner a cero la presi�n alta.";

static char cb_F_Ldb_HiPres_AutoZero_Succ[] = "Autoz�ro pression maxi r�ussi.";
static char cb_F_Ldb_HiPres_AutoZero_Fail[] = "Autoz�ro pression maxi �chou�.";

static char cb_G_Ldb_HiPres_AutoZero_Succ[] = "Hoher Druck - Nullabgleich erfolgreich.";
static char cb_G_Ldb_HiPres_AutoZero_Fail[] = "Hoher Druck - Nullabgleich mi�lungen.";

static char cb_I_Ldb_HiPres_AutoZero_Succ[] = "Azzeramento automatico alta pressione eseguito.";
static char cb_I_Ldb_HiPres_AutoZero_Fail[] = "Errore azzeramento automatico alta pressione.";

static char cb_P_Ldb_HiPres_AutoZero_Succ[] = "Autozero de press�o alta bem sucedido.";
static char cb_P_Ldb_HiPres_AutoZero_Fail[] = "Autozero de press�o alta falhou.";

// AutoZeroCMDLP
static char cb_E_Ldb_LoPres_AutoZero_Succ[] = "Low pressure autozero successful.";
static char cb_E_Ldb_LoPres_AutoZero_Fail[] = "Low pressure autozero failed.";

static char cb_S_Ldb_LoPres_AutoZero_Succ[] = "Puesta a cero presi�n baja satisfactoria.";
static char cb_S_Ldb_LoPres_AutoZero_Fail[] = "Fallo en la puesta a cero presi�n baja.";

static char cb_F_Ldb_LoPres_AutoZero_Succ[] = "Autoz�ro pression mini r�ussi.";
static char cb_F_Ldb_LoPres_AutoZero_Fail[] = "Autoz�ro pression mini �chou�.";

static char cb_G_Ldb_LoPres_AutoZero_Succ[] = "Niedriger Druck - Nullabgleich erfolgreich.";
static char cb_G_Ldb_LoPres_AutoZero_Fail[] = "Niedriger Druck - Nullabgleich mi�lungen.";

static char cb_I_Ldb_LoPres_AutoZero_Succ[] = "Azzeramento automatico bassa pressione eseguito.";
static char cb_I_Ldb_LoPres_AutoZero_Fail[] = "Errore azzeramento automatico bassa pressione.";

static char cb_P_Ldb_LoPres_AutoZero_Succ[] = "Autozero de press�o baixa bem sucedido.";
static char cb_P_Ldb_LoPres_AutoZero_Fail[] = "Falha de autozero de press�o baixa.";

// cmd_Cal_O2_21
static char cb_E_Ldb_Adj_21_O2_Succ[] = "Adjust for 21% O2 successful.";
static char cb_E_Ldb_Adj_21_O2_Fail[] = "Adjust for 21% O2 failed.";

static char cb_S_Ldb_Adj_21_O2_Succ[] = "21 % de O2 ajustado con �xito.";
static char cb_S_Ldb_Adj_21_O2_Fail[] = "Fallo al ajustar 21 % de O2.";

static char cb_F_Ldb_Adj_21_O2_Succ[] = "�talonnage � 21% r�ussi.";
static char cb_F_Ldb_Adj_21_O2_Fail[] = "�talonnage � 21% �chou�.";

static char cb_G_Ldb_Adj_21_O2_Succ[] = "Einstellen f�r 21% O2 erfolgreich.";
static char cb_G_Ldb_Adj_21_O2_Fail[] = "Einstellen f�r 21% O2 mi�lungen.";

static char cb_I_Ldb_Adj_21_O2_Succ[] = "Regolazione 21% O2 eseguita.";
static char cb_I_Ldb_Adj_21_O2_Fail[] = "Errore regolazione 21% O2.";

static char cb_P_Ldb_Adj_21_O2_Succ[] = "Ajuste para O2 a 21% bem sucedido.";
static char cb_P_Ldb_Adj_21_O2_Fail[] = "Ajuste para O2 a 21% falhou.";

// cmd_Cal_O2_100
static char cb_E_Ldb_Adj_100_O2_Succ[] = "Adjust for 100% O2 successful.";
static char cb_E_Ldb_Adj_100_O2_Fail[] = "Adjust for 100% O2 failed.";

static char cb_S_Ldb_Adj_100_O2_Succ[] = "100% de O2 ajustado con �xito.";
static char cb_S_Ldb_Adj_100_O2_Fail[] = "Fallo al ajustar 100% de O2.";

static char cb_F_Ldb_Adj_100_O2_Succ[] = "�talonnage � 100% r�ussi.";
static char cb_F_Ldb_Adj_100_O2_Fail[] = "�talonnage � 100% �chou�.";

static char cb_G_Ldb_Adj_100_O2_Succ[] = "Einstellen f�r 100% O2 erfolgreich.";
static char cb_G_Ldb_Adj_100_O2_Fail[] = "Einstellen f�r 100% O2 mi�lungen.";

static char cb_I_Ldb_Adj_100_O2_Succ[] = "Regolazione 100% O2 eseguita.";
static char cb_I_Ldb_Adj_100_O2_Fail[] = "Errore regolazione 100% O2.";

static char cb_P_Ldb_Adj_100_O2_Succ[] = "Ajuste para O2 a 100% bem sucedido.";
static char cb_P_Ldb_Adj_100_O2_Fail[] = "Ajuste para O2 a 100% falhou.";

// UIAddUser
static char cb_E_Ldb_Dup_Username[] = "Duplicate username!";
static char cb_S_Ldb_Dup_Username[] = "Nombre de usuario duplicado";
static char cb_F_Ldb_Dup_Username[] = "Nom d'utilisateur en double!";
static char cb_G_Ldb_Dup_Username[] = "Schon vorhandener Benutzername!";
static char cb_I_Ldb_Dup_Username[] = "Duplica nome utente!";
static char cb_P_Ldb_Dup_Username[] = "Nome de usu�rio duplicado!";

// UIDeleteUser
static char cb_E_Ldb_SureDeleteUser[] = "Are you sure you want to delete user: ";
static char cb_S_Ldb_SureDeleteUser[] = "Confirme que desea eliminar el usuario: ";
static char cb_F_Ldb_SureDeleteUser[] = "�tes-vous s�r de vouloir supprimer l'utilisateur: ";
static char cb_G_Ldb_SureDeleteUser[] = "Soll folgender Benutzer wirklich gel�scht werden: ";
static char cb_I_Ldb_SureDeleteUser[] = "Si desidera eliminare l'utente";
static char cb_P_Ldb_SureDeleteUser[] = "Deseja excluir o usu�rio";

static char cb_E_Ldb_Delete_User[] = "Delete user";
static char cb_S_Ldb_Delete_User[] = "Eliminar usuario";
static char cb_F_Ldb_Delete_User[] = "Effacer utilisateur";
static char cb_G_Ldb_Delete_User[] = "Benutzer l�schen";
static char cb_I_Ldb_Delete_User[] = "Elimina utente";
static char cb_P_Ldb_Delete_User[] = "Excluir usu�rio";

// UIChangeUserPassword
static char cb_E_Ldb_ChangePwdTitle[] = "Change password";
static char cb_S_Ldb_ChangePwdTitle[] = "Cambiar contrase�a";
static char cb_F_Ldb_ChangePwdTitle[] = "Changer mot de passe";
static char cb_G_Ldb_ChangePwdTitle[] = "Kennwort �ndern";
static char cb_I_Ldb_ChangePwdTitle[] = "Modifica password";
static char cb_P_Ldb_ChangePwdTitle[] = "Alterar senha";

static char cb_E_Ldb_Old_Password[] = "Old password:";
static char cb_S_Ldb_Old_Password[] = "Contrase�a anterior:";
static char cb_F_Ldb_Old_Password[] = "Ancien mot de passe:";
static char cb_G_Ldb_Old_Password[] = "Altes Kennwort:";
static char cb_I_Ldb_Old_Password[] = "Password precedente:";
static char cb_P_Ldb_Old_Password[] = "Senha antiga:";

static char cb_E_Ldb_Old_Password_wrng[] = "Old password is incorrect";
static char cb_S_Ldb_Old_Password_wrng[] = "La contrase�a anterior es incorrecta";
static char cb_F_Ldb_Old_Password_wrng[] = "Ancien mot de passe incorrect";
static char cb_G_Ldb_Old_Password_wrng[] = "Altes Kennwort ist falsch";
static char cb_I_Ldb_Old_Password_wrng[] = "Password precedente errata";
static char cb_P_Ldb_Old_Password_wrng[] = "A senha antiga est� incorreta";

static char cb_E_Ldb_Password_Mismatch[] = "Passwords do not match!";
static char cb_S_Ldb_Password_Mismatch[] = "Las contrase�as no coinciden";
static char cb_F_Ldb_Password_Mismatch[] = "Mots de passe diff�rents!";
static char cb_G_Ldb_Password_Mismatch[] = "Kennw�rter stimmen nicht �berein!";
static char cb_I_Ldb_Password_Mismatch[] = "Password non corrispondenti!";
static char cb_P_Ldb_Password_Mismatch[] = "As senhas n�o se correspondem!";

// UserConfig
static char cb_E_Ldb_Max_Users_Reg[] = "Maximum users registered";
static char cb_S_Ldb_Max_Users_Reg[] = "N� m�ximo de usuarios registrados";
static char cb_F_Ldb_Max_Users_Reg[] = "Nbre d'utilisateurs maxi enregistr�s";
static char cb_G_Ldb_Max_Users_Reg[] = "Maximale Benutzerzahl registriert";
static char cb_I_Ldb_Max_Users_Reg[] = "Massimo utenti registrati";
static char cb_P_Ldb_Max_Users_Reg[] = "M�ximo de usu�rios registrados";

// SetUpLanguagePanel
static char cb_E_Ldb_Select_Lang_for[] = "Select default language for user ";
static char cb_S_Ldb_Select_Lang_for[] = "Seleccione el idioma predeterminado para el usuario ";
static char cb_F_Ldb_Select_Lang_for[] = "S�lectionner langue par d�faut util ";
static char cb_G_Ldb_Select_Lang_for[] = "Standardsprache ausw�hlen f�r Benutzer ";
static char cb_I_Ldb_Select_Lang_for[] = "Selezionare la lingua predefinita per l'utente ";
static char cb_P_Ldb_Select_Lang_for[] = "Selecionar idioma padr�o para usu�rio";

// AutoDetectPTS
static char cb_E_Ldb_Pts_cal_err[] = "PTS 2000 calibration error";
static char cb_S_Ldb_Pts_cal_err[] = "Error de calibraci�n PTS 2000";
static char cb_F_Ldb_Pts_cal_err[] = "Erreur �talonnage du PTS 2000";
static char cb_G_Ldb_Pts_cal_err[] = "PTS 2000 Kalibrierungsfehler";
static char cb_I_Ldb_Pts_cal_err[] = "Errore calibrazione PTS 2000";
static char cb_P_Ldb_Pts_cal_err[] = "Erro de calibra��o do PTS 2000";

// SetTestButtonMode
static char cb_E_Ldb_PauseMsg2[] = "Testing paused, press 'Run all tests' to continue";
static char cb_S_Ldb_PauseMsg2[] = "Prueba detenida, haga clic en 'Ejecutar todas las pruebas' para continuar";
static char cb_F_Ldb_PauseMsg2[] = "Test suspendu, press 'Ex�cuter tous tests' pour continuer";
static char cb_G_Ldb_PauseMsg2[] = "Test Pause, weiter mit 'Alle Tests durchf�hren' ";
static char cb_I_Ldb_PauseMsg2[] = "Pausa. Per proseguire premere 'Esegui tutti i test' ";
static char cb_P_Ldb_PauseMsg2[] = "Teste pausado, pressione 'Executar todos os testes' para continuar";

static char cb_E_Ldb_Stopped_Fnsh_tst[] = "ABORTED. Completing current test.";
static char cb_S_Ldb_Stopped_Fnsh_tst[] = "ANULADA. Completando la prueba actual.";
static char cb_F_Ldb_Stopped_Fnsh_tst[] = "ANNUL�. Fin test courant.";
static char cb_G_Ldb_Stopped_Fnsh_tst[] = "ABGEBROCHEN. Beende aktuellen Test.";
static char cb_I_Ldb_Stopped_Fnsh_tst[] = "INTERROTTO. Completamento test in corso.";
static char cb_P_Ldb_Stopped_Fnsh_tst[] = "ANULADO. Concluindo teste atual.";

// LoopTest // RunSequence // MainPanelCallback
static char cb_E_Ldb_PauseMsg1[] = "Pause function requested";
static char cb_S_Ldb_PauseMsg1[] = "Funci�n de pausa solicitada";
static char cb_F_Ldb_PauseMsg1[] = "Fonction pause demand�e";
static char cb_G_Ldb_PauseMsg1[] = "Pause der Funktion angefordert";
static char cb_I_Ldb_PauseMsg1[] = "Funzione pausa richiesta";
static char cb_P_Ldb_PauseMsg1[] = "Solicitada a pausa da fun��o";

// LoopTest
static char cb_E_Ldb_MSG_LOOP_ABORT[] = "Loop test aborted.\n\n";
static char cb_S_Ldb_MSG_LOOP_ABORT[] = "Prueba de ciclos anulada.\n\n";
static char cb_F_Ldb_MSG_LOOP_ABORT[] = "It�ration test annul�e.\n\n";
static char cb_G_Ldb_MSG_LOOP_ABORT[] = "Testschleife abgebrochen.\n\n";
static char cb_I_Ldb_MSG_LOOP_ABORT[] = "Loop test interrotto.\n\n";
static char cb_P_Ldb_MSG_LOOP_ABORT[] = "Teste de ciclo anulado.\n\n";

// RunTestUUT
static char cb_E_Ldb_Err_in_db_2[] = "Error in writing to database.\nTesting cannot continue";
static char cb_S_Ldb_Err_in_db_2[] = "Error al escribir en la base de datos.\nNo se puede continuar la prueba";
static char cb_F_Ldb_Err_in_db_2[] = "Erreur d'�criture ds base donn�es.\nImpossible continuer test";
static char cb_G_Ldb_Err_in_db_2[] = "Fehler beim Schreiben in die Datenbank.\nDer Test kann nicht fortgesetzt werden";
static char cb_I_Ldb_Err_in_db_2[] = "Errore durante la scrittura su database.\nImpossibile eseguire i test";
static char cb_P_Ldb_Err_in_db_2[] = "Erro ao gravar para banco de dados.\nO teste n�o pode continuar";

// RunSequence
static char cb_E_Ldb_MSG_UUT_ABORT[] = "Test UUT aborted.\n\n";
static char cb_S_Ldb_MSG_UUT_ABORT[] = "Prueba UUT anulada.\n\n";
static char cb_F_Ldb_MSG_UUT_ABORT[] = "Test UUT annul�.\n\n";
static char cb_G_Ldb_MSG_UUT_ABORT[] = "Test UUT abgebrochen.\n\n";
static char cb_I_Ldb_MSG_UUT_ABORT[] = "Test UUT interrotto.\n\n";
static char cb_P_Ldb_MSG_UUT_ABORT[] = "UUT de teste anulado.\n\n";

static char cb_E_Ldb_MSG_UUT_RTE[] = "Test UUT stopped by run-time error.\n\n";
static char cb_S_Ldb_MSG_UUT_RTE[] = "Prueba UUT detenida por error en tiempo de ejecuci�n.\n\n";
static char cb_F_Ldb_MSG_UUT_RTE[] = "Test UUT arr�t� par erreur d'ex�cution.\n\n";
static char cb_G_Ldb_MSG_UUT_RTE[] = "Test UUT durch Laufzeitfehler beendet.\n\n";
static char cb_I_Ldb_MSG_UUT_RTE[] = "Test UUT interrotto da un errore di run time.\n\n";
static char cb_P_Ldb_MSG_UUT_RTE[] = "UUT de teste interrompido por erro de execu��o.\n\n";

static char cb_E_Ldb_MSG_UUT_FAIL[] = "Test UUT stopped by test failure.\n\n";
static char cb_S_Ldb_MSG_UUT_FAIL[] = "Prueba UUT detenida por fallo de la prueba.\n\n";
static char cb_F_Ldb_MSG_UUT_FAIL[] = "Test UUT arr�t� par �chec.\n\n";
static char cb_G_Ldb_MSG_UUT_FAIL[] = "Test UUT durch mi�lungenen Test beendet.\n\n";
static char cb_I_Ldb_MSG_UUT_FAIL[] = "Test UUT interrotto da errore del test.\n\n";
static char cb_P_Ldb_MSG_UUT_FAIL[] = "UUT de teste interrompido por falha no teste.\n\n";

// RunSingleTest
static char cb_E_Ldb_MSG_ABORT[] = "Aborted";
static char cb_S_Ldb_MSG_ABORT[] = "Anulada";
static char cb_F_Ldb_MSG_ABORT[] = "Annul�";
static char cb_G_Ldb_MSG_ABORT[] = "Abgebrochen";
static char cb_I_Ldb_MSG_ABORT[] = "Interrotto";
static char cb_P_Ldb_MSG_ABORT[] = "Anulado";

// CreateNewDBFiles
static char cb_E_Ldb_DB_files_missing[] = "One or more database files were missing.\n %d database files were renamed.";
static char cb_S_Ldb_DB_files_missing[] = "Faltan uno o m�s archivos de base de datos.\n Se ha cambiado el nombre a %d archivos de base de datos.";
static char cb_F_Ldb_DB_files_missing[] = "Un ou plusieurs fichiers bd manquant(s).\n %d fichiers bd ont �t� renomm�s.";
static char cb_G_Ldb_DB_files_missing[] = "Mind. eine Datenbankdatei fehlte.\n %d Datenbankdateien wurde umbenannt.";
static char cb_I_Ldb_DB_files_missing[] = "Impossibile trovare uno o pi� file di database.\n Rinominati %d file database.";
static char cb_P_Ldb_DB_files_missing[] = "Um ou mais arquivos de banco de dados est�o faltando.\n %d arquivos de banco de dados foram renomeados.";

static char cb_E_Ldb_Database_error[] = "Database error";
static char cb_S_Ldb_Database_error[] = "Error de base de datos";
static char cb_F_Ldb_Database_error[] = "Erreur base donn�es";
static char cb_G_Ldb_Database_error[] = "Datenbankfehler";
static char cb_I_Ldb_Database_error[] = "Errore database";
static char cb_P_Ldb_Database_error[] = "Erro de banco de dados";

// ShowJetError (dbapi)
static char cb_E_Ldb_Database_error_msg[] = "There was an error writing to the database.\nResults can not be saved.";
static char cb_S_Ldb_Database_error_msg[] = "Error al escribir en la base de datos.\nNo se pueden guardar los resultados.";
static char cb_F_Ldb_Database_error_msg[] = "Erreur d'�criture ds la bd.\nEnregistrement des r�sultats impossible.";
static char cb_G_Ldb_Database_error_msg[] = "Fehler beim Schreiben in die Datenbank.\nErgebnisse k�nnen nicht gespeichert werden.";
static char cb_I_Ldb_Database_error_msg[] = "Errore durante la scrittura sul database.\nImpossibile salvare i risultati.";
static char cb_P_Ldb_Database_error_msg[] = "Erro ao gravar para o banco de dados.\nImposs�vel salvar resultados.";

// CheckDBSize (dbapi)
static char cb_E_Ldb_DB_Warning[] = "Database Warning";
static char cb_S_Ldb_DB_Warning[] = "Advertencia de base de datos";
static char cb_F_Ldb_DB_Warning[] = "Avertissement base de donn�es";
static char cb_G_Ldb_DB_Warning[] = "Datenbank Warnung";
static char cb_I_Ldb_DB_Warning[] = "Avvertenza database";
static char cb_P_Ldb_DB_Warning[] = "Aviso de banco de dados";

static char cb_E_Ldb_DB_getting_big[] = "The Database is over 2 megabytes.";
static char cb_S_Ldb_DB_getting_big[] = "La base de datos tiene un tama�o de 2 megabytes aproximadamente.";
static char cb_F_Ldb_DB_getting_big[] = "La base de donn�es fait plus de 2 m�gabytes.";
static char cb_G_Ldb_DB_getting_big[] = "Die Datenbank enth�lt �ber 2 MB.";
static char cb_I_Ldb_DB_getting_big[] = "Il database supera 2 megabyte.";
static char cb_P_Ldb_DB_getting_big[] = "O banco de dados tem mais de 2 megabytes.";

// GetReportFileName
static char cb_E_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY[] = "No report file name specified\n.Choose report file name now?";
static char cb_S_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY[] = "No se ha especificado ning�n nombre de archivo de informe\n.�Elegir uno ahora?";
static char cb_F_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY[] = "Nom de fichier de rapport non sp�cifi�\n.Choisir nom fichier maintenant?";
static char cb_G_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY[] = "Kein Dateiname f�r Bericht angegeben\n.Name f�r Berichtsdatei jetzt w�hlen?";
static char cb_I_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY[] = "Nessun report specificato\n.Scegliere un nome?";
static char cb_P_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY[] = "Nenhum nome de arquivo de relat�rio especificado\nEscolher nome de arquivo de relat�rio agora?";

static char cb_E_Ldb_MSG_CHANGE_DFLT_RPT[] = "Default report file name is %s.\nChange report file name?";
static char cb_S_Ldb_MSG_CHANGE_DFLT_RPT[] = "El nombre de archivo de informe predeterminado es %s.\n�Cambiar nombre de archivo de informe?";
static char cb_F_Ldb_MSG_CHANGE_DFLT_RPT[] = "Nom fichier du rapport par d�faut est %s.\nChanger nom fichier du rapport?";
static char cb_G_Ldb_MSG_CHANGE_DFLT_RPT[] = "Standard-Dateiname f�r Bericht ist %s.\nDateiname f�r Bericht �ndern?";
static char cb_I_Ldb_MSG_CHANGE_DFLT_RPT[] = "Il nome del file del report � %s.\nModificare il nome?";
static char cb_P_Ldb_MSG_CHANGE_DFLT_RPT[] = "O nome de arquivo de relat�rio-padr�o � %s.\nAlterar nome de arquivo de relat�rio?";

static char cb_E_Ldb_MSG_CHANGE_DFLT_RPT_TITLE[] = "Report file name";
static char cb_S_Ldb_MSG_CHANGE_DFLT_RPT_TITLE[] = "Nombre del archivo de informe";
static char cb_F_Ldb_MSG_CHANGE_DFLT_RPT_TITLE[] = "Nom fichier du rapport";
static char cb_G_Ldb_MSG_CHANGE_DFLT_RPT_TITLE[] = "Bericht Dateiname";
static char cb_I_Ldb_MSG_CHANGE_DFLT_RPT_TITLE[] = "Nome file";
static char cb_P_Ldb_MSG_CHANGE_DFLT_RPT_TITLE[] = "Nome de arquivo do relat�rio";

static char cb_E_Ldb_MSG_SAVE_RPT_TITLE[] = "Save test report";
static char cb_S_Ldb_MSG_SAVE_RPT_TITLE[] = "Guardar informe de prueba";
static char cb_F_Ldb_MSG_SAVE_RPT_TITLE[] = "Enregistrer rapport de test";
static char cb_G_Ldb_MSG_SAVE_RPT_TITLE[] = "Testbericht speichern";
static char cb_I_Ldb_MSG_SAVE_RPT_TITLE[] = "Salva report test";
static char cb_P_Ldb_MSG_SAVE_RPT_TITLE[] = "Salvar relat�rio de teste";

// UpdateTestListing
static char cb_E_Ldb_MSG_RUNNING[] = "Running";
static char cb_S_Ldb_MSG_RUNNING[] = "Ejecutando";
static char cb_F_Ldb_MSG_RUNNING[] = "Ex�cution";
static char cb_G_Ldb_MSG_RUNNING[] = "L�uft";
static char cb_I_Ldb_MSG_RUNNING[] = "In corso";
static char cb_P_Ldb_MSG_RUNNING[] = "Em execu��o";

static char cb_E_Ldb_MSG_NONE[] = "None";
static char cb_S_Ldb_MSG_NONE[] = "Ninguno";
static char cb_F_Ldb_MSG_NONE[] = "Aucun";
static char cb_G_Ldb_MSG_NONE[] = "Keine";
static char cb_I_Ldb_MSG_NONE[] = "Nessuno";
static char cb_P_Ldb_MSG_NONE[] = "Nenhum";

static char cb_E_Ldb_MSG_PASS[] = "Pass";
static char cb_S_Ldb_MSG_PASS[] = "Paso";
static char cb_F_Ldb_MSG_PASS[] = "R�ussi";
static char cb_G_Ldb_MSG_PASS[] = "OK";
static char cb_I_Ldb_MSG_PASS[] = "Superato";
static char cb_P_Ldb_MSG_PASS[] = "Passa";

static char cb_E_Ldb_MSG_FAIL[] = "Fail";
static char cb_S_Ldb_MSG_FAIL[] = "Fallo";
static char cb_F_Ldb_MSG_FAIL[] = "�chec";
static char cb_G_Ldb_MSG_FAIL[] = "Mi�lungen";
static char cb_I_Ldb_MSG_FAIL[] = "Errore";
static char cb_P_Ldb_MSG_FAIL[] = "Falha";

static char cb_E_Ldb_MSG_SKIP[] = "Skipped";
static char cb_S_Ldb_MSG_SKIP[] = "Omitido";
static char cb_F_Ldb_MSG_SKIP[] = "Ignor�";
static char cb_G_Ldb_MSG_SKIP[] = "�bersprungen";
static char cb_I_Ldb_MSG_SKIP[] = "Ignorato";
static char cb_P_Ldb_MSG_SKIP[] = "Ignorado";

static char cb_E_Ldb_MSG_LOOPING[] = "Looping";
static char cb_S_Ldb_MSG_LOOPING[] = "Ejecutando ciclo";
static char cb_F_Ldb_MSG_LOOPING[] = "It�ration";
static char cb_G_Ldb_MSG_LOOPING[] = "Schleife";
static char cb_I_Ldb_MSG_LOOPING[] = "Loop";
static char cb_P_Ldb_MSG_LOOPING[] = "Ciclando";

static char cb_E_Ldb_MSG_ERROR[] = "Error";
static char cb_S_Ldb_MSG_ERROR[] = "Error";
static char cb_F_Ldb_MSG_ERROR[] = "Erreur";
static char cb_G_Ldb_MSG_ERROR[] = "Fehler";
static char cb_I_Ldb_MSG_ERROR[] = "Errore";
static char cb_P_Ldb_MSG_ERROR[] = "Erro";

// SysStatusPanel
static char cb_E_Ldb_PASSED[] = "PASSED";
static char cb_S_Ldb_PASSED[] = "PASO";
static char cb_F_Ldb_PASSED[] = "R�USSI";
static char cb_G_Ldb_PASSED[] = "OK";
static char cb_I_Ldb_PASSED[] = "SUPERATO";
static char cb_P_Ldb_PASSED[] = "PASSOU";

static char cb_E_Ldb_FAILED[] = "FAILED";
static char cb_S_Ldb_FAILED[] = "FALLO";
static char cb_F_Ldb_FAILED[] = "�CHOU�";
static char cb_G_Ldb_FAILED[] = "MISSLUNGEN";
static char cb_I_Ldb_FAILED[] = "ERRORE";
static char cb_P_Ldb_FAILED[] = "FALHOU";

static char cb_E_Ldb_ABORT[] = "ABORT";
static char cb_S_Ldb_ABORT[] = "ANULAR";
static char cb_F_Ldb_ABORT[] = "ANNUL";
static char cb_G_Ldb_ABORT[] = "ABBRECHEN";
static char cb_I_Ldb_ABORT[] = "INTERROMPI";
static char cb_P_Ldb_ABORT[] = "ANULAR";

static char cb_E_Ldb_ABORTED[] = "ABORTED";
static char cb_S_Ldb_ABORTED[] = "ANULADA";
static char cb_F_Ldb_ABORTED[] = "ANNUL�";
static char cb_G_Ldb_ABORTED[] = "ABGEBROCHEN";
static char cb_I_Ldb_ABORTED[] = "INTERROTTO";
static char cb_P_Ldb_ABORTED[] = "ANULADO";

// ResultMenuClear
static char cb_E_Ldb_SureClearResults[] = "Are you sure you want to clear results?";
static char cb_S_Ldb_SureClearResults[] = "�Confirme que desea borrar los resultados?";
static char cb_F_Ldb_SureClearResults[] = "Voulez-vous effacer les r�sultats?";
static char cb_G_Ldb_SureClearResults[] = "Sollen die Ergebnisse wirklich gel�scht werden?";
static char cb_I_Ldb_SureClearResults[] = "Cancellare i risultati?";
static char cb_P_Ldb_SureClearResults[] = "Tem certeza que deseja apagar os resultados?";

// ResultMenuPrint
static char cb_E_Ldb_Error[] = "Error";
static char cb_S_Ldb_Error[] = "Error";
static char cb_F_Ldb_Error[] = "Erreur";
static char cb_G_Ldb_Error[] = "Fehler";
static char cb_I_Ldb_Error[] = "Errore";
static char cb_P_Ldb_Error[] = "Erro";

static char cb_E_Ldb_Out_of_Memory[] = "Out of memory";
static char cb_S_Ldb_Out_of_Memory[] = "Memoria insuficiente";
static char cb_F_Ldb_Out_of_Memory[] = "M�moire insuf";
static char cb_G_Ldb_Out_of_Memory[] = "Zu wenig Speicher";
static char cb_I_Ldb_Out_of_Memory[] = "Memoria insufficiente";
static char cb_P_Ldb_Out_of_Memory[] = "Mem�ria insuficiente";

// ResultMenuSave
static char cb_E_Ldb_SaveAs[] = "Save as";
static char cb_S_Ldb_SaveAs[] = "Guardar como";
static char cb_F_Ldb_SaveAs[] = "Enregistrer sous";
static char cb_G_Ldb_SaveAs[] = "Speichern als";
static char cb_I_Ldb_SaveAs[] = "Salva con nome";
static char cb_P_Ldb_SaveAs[] = "Salvar como";

// DrawResultsOptMenu
static char cb_E_Ldb_Clear[] = "Clear";
static char cb_S_Ldb_Clear[] = "Borrar";
static char cb_F_Ldb_Clear[] = "Effac";
static char cb_G_Ldb_Clear[] = "L�schen";
static char cb_I_Ldb_Clear[] = "Cancella";
static char cb_P_Ldb_Clear[] = "Apagar";

static char cb_E_Ldb_Print[] = "Print";
static char cb_S_Ldb_Print[] = "Imprimir";
static char cb_F_Ldb_Print[] = "Imprimer";
static char cb_G_Ldb_Print[] = "Drucken";
static char cb_I_Ldb_Print[] = "Stampa";
static char cb_P_Ldb_Print[] = "Imprimir";

// UserMenu
static char cb_E_Ldb_Add_User[] = "Add user";
static char cb_S_Ldb_Add_User[] = "A�adir usuario";
static char cb_F_Ldb_Add_User[] = "Ajout utilisateur";
static char cb_G_Ldb_Add_User[] = "Benutzer hinzuf�gen";
static char cb_I_Ldb_Add_User[] = "Aggiungi utente";
static char cb_P_Ldb_Add_User[] = "Adicionar usu�rio";

// SetCompressorSerial
static char cb_E_Ldb_Warning[] = "Warning";
static char cb_S_Ldb_Warning[] = "Advertencia";
static char cb_F_Ldb_Warning[] = "Avertissement";
static char cb_G_Ldb_Warning[] = "Warnung";
static char cb_I_Ldb_Warning[] = "Avvertenza";
static char cb_P_Ldb_Warning[] = "Aviso";

static char cb_E_Ldb_Set_Compressor[] = "Do you wish to continue setting the compressor?";
static char cb_S_Ldb_Set_Compressor[] = "�Desea continuar configurando el compresor?";
static char cb_F_Ldb_Set_Compressor[] = "Voulez-vous continuer � r�gler le compresseur?";
static char cb_G_Ldb_Set_Compressor[] = "Soll mit der Einstellung des Kompressors fortgefahren werden?";
static char cb_I_Ldb_Set_Compressor[] = "Proseguire la configurazione del compressore?";
static char cb_P_Ldb_Set_Compressor[] = "Deseja continuar configurando o compressor?";

static char cb_E_Ldb_Set_Comp_SN_status[] = "Setting compressor serial number";
static char cb_S_Ldb_Set_Comp_SN_status[] = "Estableciendo n�mero de serie del compresor";
static char cb_F_Ldb_Set_Comp_SN_status[] = "R�glage num�ro de s�rie du compresseur";
static char cb_G_Ldb_Set_Comp_SN_status[] = "Einstellen der Kompressor-Seriennummer";
static char cb_I_Ldb_Set_Comp_SN_status[] = "Impostazione numero di serie compressore";
static char cb_P_Ldb_Set_Comp_SN_status[] = "Definindo n�mero de s�rie do compressor";

static char cb_E_Ldb_status[] = "Status";
static char cb_S_Ldb_status[] = "Estado";
static char cb_F_Ldb_status[] = "�tat";
static char cb_G_Ldb_status[] = "Status";
static char cb_I_Ldb_status[] = "Stato";
static char cb_P_Ldb_status[] = "Status";

static char cb_E_Ldb_Comp_not_installed[] = "Compressor not installed";
static char cb_S_Ldb_Comp_not_installed[] = "El compresor no est� instalado";
static char cb_F_Ldb_Comp_not_installed[] = "Compresseur non install�";
static char cb_G_Ldb_Comp_not_installed[] = "Kompressor nicht installiert";
static char cb_I_Ldb_Comp_not_installed[] = "Compressore non installato ";
static char cb_P_Ldb_Comp_not_installed[] = "Compressor n�o instalado";

static char cb_E_Ldb_Comp_set_fail[] = "Setting failed";
static char cb_S_Ldb_Comp_set_fail[] = "Fallo al establecer";
static char cb_F_Ldb_Comp_set_fail[] = "�chec r�glage";
static char cb_G_Ldb_Comp_set_fail[] = "Einstellung mi�lungen";
static char cb_I_Ldb_Comp_set_fail[] = "Errore impostazione";
static char cb_P_Ldb_Comp_set_fail[] = "Falha de defini��o";

static char cb_E_Ldb_Retry_prompt[] = "Do you wish to retry?";
static char cb_S_Ldb_Retry_prompt[] = "�Desea volver a intentarlo?";
static char cb_F_Ldb_Retry_prompt[] = "Voulez-vous r�essayer?";
static char cb_G_Ldb_Retry_prompt[] = "Erneut versuchen?";
static char cb_I_Ldb_Retry_prompt[] = "Riprovare?";
static char cb_P_Ldb_Retry_prompt[] = "Deseja repetir?";

// SetCompressorHours
static char cb_E_Ldb_comp_hours_f[] = "Compressor Hours: %7.0f";
static char cb_S_Ldb_comp_hours_f[] = "Horas del compresor: %7.0f";
static char cb_F_Ldb_comp_hours_f[] = "Heures du compresseur: %7.0f";
static char cb_G_Ldb_comp_hours_f[] = "Kompressorstunden: %7.0f";
static char cb_I_Ldb_comp_hours_f[] = "Ore compressore: %7.0f";
static char cb_P_Ldb_comp_hours_f[] = "Horas do compressor: %7.0f";

static char cb_E_Ldb_Set_Comp_hours_status[] = "Setting compressor hours";
static char cb_S_Ldb_Set_Comp_hours_status[] = "Estableciendo horas del compresor";
static char cb_F_Ldb_Set_Comp_hours_status[] = "R�glage heures du compresseur";
static char cb_G_Ldb_Set_Comp_hours_status[] = "Einstellen der Kompressorstunden";
static char cb_I_Ldb_Set_Comp_hours_status[] = "Impostazione ore compressore";
static char cb_P_Ldb_Set_Comp_hours_status[] = "Definindo horas do compressor";

// Set840Callback
static char cb_E_Ldb_EstabCom_840[] = "Establishing communication with 840 Ventilator";
static char cb_S_Ldb_EstabCom_840[] = "Establecimiento de comunicaci�n con el ventilador 840";
static char cb_F_Ldb_EstabCom_840[] = "Communication en cours avec le ventilateur 840";
static char cb_G_Ldb_EstabCom_840[] = "Kommunikationsaufbau mit dem Beatmungsger�t 840";
static char cb_I_Ldb_EstabCom_840[] = "Comunicazione con il Ventilatore 840 in corso";
static char cb_P_Ldb_EstabCom_840[] = "Estabelecendo comunica��o com o Ventilador 840";

static char cb_E_Ldb_Com_Error[] = "Communications error";
static char cb_S_Ldb_Com_Error[] = "Error de comunicaci�n";
static char cb_F_Ldb_Com_Error[] = "Erreur communication";
static char cb_G_Ldb_Com_Error[] = "Kommunikationsfehler";
static char cb_I_Ldb_Com_Error[] = "Errore di comunicazione";
static char cb_P_Ldb_Com_Error[] = "Erro de comunica��es";

static char cb_E_Ldb_840_Com_Error[] = "Cannot communicate with the 840 Ventilator";
static char cb_S_Ldb_840_Com_Error[] = "No se puede establecer comunicaci�n con el ventilador 840";
static char cb_F_Ldb_840_Com_Error[] = "Impossible de communiquer avec ventilateur 840";
static char cb_G_Ldb_840_Com_Error[] = "Kann nicht mit Beatmungsger�t 840 kommunizieren";
static char cb_I_Ldb_840_Com_Error[] = "Impossibile comunicare con il ventilatore 840";
static char cb_P_Ldb_840_Com_Error[] = "Imposs�vel a comunica��o com o Ventilador 840";

static char cb_E_Ldb_Retrieving_logs[] = "Capturing 840 Ventilator Logs";
static char cb_S_Ldb_Retrieving_logs[] = "Capturando registros del ventilador 840";
static char cb_F_Ldb_Retrieving_logs[] = "Capture journaux ventilateur 840 en cours";
static char cb_G_Ldb_Retrieving_logs[] = "Erfassen der Protokolle des Beatmungsger�tes 840";
static char cb_I_Ldb_Retrieving_logs[] = "Rilevamento registrazioni ventilatore 840 in corso";
static char cb_P_Ldb_Retrieving_logs[] = "Capturando registros do Ventilador 840";

static char cb_E_Ldb_Clear_sys_logs[] = "Clearing 840 Ventilator Logs";
static char cb_S_Ldb_Clear_sys_logs[] = "Borrando registros del ventilador 840";
static char cb_F_Ldb_Clear_sys_logs[] = "Effacement journaux ventilateur 840 en cours";
static char cb_G_Ldb_Clear_sys_logs[] = "Protokolle des Beatmungsger�tes 840 werden gel�scht";
static char cb_I_Ldb_Clear_sys_logs[] = "Cancellazione registrazioni ventilatore 840";
static char cb_P_Ldb_Clear_sys_logs[] = "Apagando registros do Ventilador 840";

static char cb_E_Ldb_Confirm_clear[] = "Confirm clear";
static char cb_S_Ldb_Confirm_clear[] = "Confirmar borrado";
static char cb_F_Ldb_Confirm_clear[] = "Confirmer effac";
static char cb_G_Ldb_Confirm_clear[] = "L�schen best�tigen";
static char cb_I_Ldb_Confirm_clear[] = "Conferma cancellazione";
static char cb_P_Ldb_Confirm_clear[] = "Confirmar apagamento";

static char cb_E_Ldb_Clear_Sys_logs[] = "Clear 840 Ventilator Logs?";
static char cb_S_Ldb_Clear_Sys_logs[] = "�Borrar registros del ventilador 840?";
static char cb_F_Ldb_Clear_Sys_logs[] = "Effac journaux ventilateur 840?";
static char cb_G_Ldb_Clear_Sys_logs[] = "Protokolle des Beatmungsger�tes 840 l�schen?";
static char cb_I_Ldb_Clear_Sys_logs[] = "Cancella registrazioni ventilatore 840?";
static char cb_P_Ldb_Clear_Sys_logs[] = "Apagar registros do Ventilador 840?";

// logonfun.c
// login
// Logon Select User
static char cb_E_Ldb_SelectUser[] = "Select username:";
static char cb_S_Ldb_SelectUser[] = "Seleccionar nombre de usuario:";
static char cb_F_Ldb_SelectUser[] = "S�lectionner nom utilisateur:";
static char cb_G_Ldb_SelectUser[] = "Benutzernamen ausw�hlen:";
static char cb_I_Ldb_SelectUser[] = "Selezionare nome utente:";
static char cb_P_Ldb_SelectUser[] = "Selecionar nome de usu�rio:";

// Logon PTS 2000 Info Group
static char cb_E_Ldb_PTS2000_Name[] = "PTS 2000";
static char cb_S_Ldb_PTS2000_Name[] = "PTS 2000";
static char cb_F_Ldb_PTS2000_Name[] = "PTS 2000";
static char cb_G_Ldb_PTS2000_Name[] = "PTS 2000";
static char cb_I_Ldb_PTS2000_Name[] = "PTS 2000";
static char cb_P_Ldb_PTS2000_Name[] = "PTS 2000";

// Logon PTS Serial Label
static char cb_E_Ldb_PTS_Serial_Lbl[] = "Serial number:";
static char cb_S_Ldb_PTS_Serial_Lbl[] = "N�mero de serie:";
static char cb_F_Ldb_PTS_Serial_Lbl[] = "Num�ro de s�rie:";
static char cb_G_Ldb_PTS_Serial_Lbl[] = "Seriennummer:";
static char cb_I_Ldb_PTS_Serial_Lbl[] = "Numero di serie:";
static char cb_P_Ldb_PTS_Serial_Lbl[] = "N�mero de s�rie:";

// Logon PTS SW Version Label
static char cb_E_Ldb_PTS_SWversion_Lbl[] = "Software version:";
static char cb_S_Ldb_PTS_SWversion_Lbl[] = "Versi�n del software:";
static char cb_F_Ldb_PTS_SWversion_Lbl[] = "Version du logiciel:";
static char cb_G_Ldb_PTS_SWversion_Lbl[] = "Softwareversion:";
static char cb_I_Ldb_PTS_SWversion_Lbl[] = "Versione software:";
static char cb_P_Ldb_PTS_SWversion_Lbl[] = "Vers�o do software:";

// Logon PTS Cal Last
static char cb_E_Ldb_PTS_CalLast_Lbl[] = "Calibration:    Last:";
static char cb_S_Ldb_PTS_CalLast_Lbl[] = "Calibraci�n:    �ltima:";
static char cb_F_Ldb_PTS_CalLast_Lbl[] = "�talonnage:    Dernier:";
static char cb_G_Ldb_PTS_CalLast_Lbl[] = "Kalibrierung:    Letzte:";
static char cb_I_Ldb_PTS_CalLast_Lbl[] = "Calibrazione:    Ultima:";
static char cb_P_Ldb_PTS_CalLast_Lbl[] = "Calibra��o:    �ltima:";

// Logon PTS Cal Next
static char cb_E_Ldb_PTS_CalNext_Lbl[] = "Next:";
static char cb_S_Ldb_PTS_CalNext_Lbl[] = "Siguiente:";
static char cb_F_Ldb_PTS_CalNext_Lbl[] = "Suivant:";
static char cb_G_Ldb_PTS_CalNext_Lbl[] = "N�chster:";
static char cb_I_Ldb_PTS_CalNext_Lbl[] = "Successivo:";
static char cb_P_Ldb_PTS_CalNext_Lbl[] = "Pr�xima:";

// Logon 840 Vent Info Group
static char cb_E_Ldb_840Vent_Name[] = "840 Ventilator";
static char cb_S_Ldb_840Vent_Name[] = "Ventilador 840";
static char cb_F_Ldb_840Vent_Name[] = "Ventilateur 840";
static char cb_G_Ldb_840Vent_Name[] = "Beatmungsger�t 840";
static char cb_I_Ldb_840Vent_Name[] = "Ventilatore 840";
static char cb_P_Ldb_840Vent_Name[] = "Ventilador 840";

// Logon 840 GUIserial Label
static char cb_E_Ldb_840_GUIserial_Lbl[] = "GUI serial number:";
static char cb_S_Ldb_840_GUIserial_Lbl[] = "N�mero de serie IGU:";
static char cb_F_Ldb_840_GUIserial_Lbl[] = "Num�ro de s�rie IGU:";
static char cb_G_Ldb_840_GUIserial_Lbl[] = "GUI-Seriennummer:";
static char cb_I_Ldb_840_GUIserial_Lbl[] = "Numero di serie GUI:";
static char cb_P_Ldb_840_GUIserial_Lbl[] = "N�mero de s�rie da GUI:";

// Logon 840 BDUserial Label
static char cb_E_Ldb_840_BDUserial_Lbl[] = "BDU serial number:";
static char cb_S_Ldb_840_BDUserial_Lbl[] = "N�mero de serie BDU:";
static char cb_F_Ldb_840_BDUserial_Lbl[] = "Num�ro de s�rie de BDU:"; //l'unit� insuf
static char cb_G_Ldb_840_BDUserial_Lbl[] = "BDU Seriennummer:";
static char cb_I_Ldb_840_BDUserial_Lbl[] = "Numero di serie BDU:";
static char cb_P_Ldb_840_BDUserial_Lbl[] = "N�mero de s�rie de BDU:";

// SetPTSLabels
static char cb_E_Ldb_PTS_NoDetect[] = "PTS 2000 not detected.";
static char cb_S_Ldb_PTS_NoDetect[] = "PTS 2000 no detectado.";
static char cb_F_Ldb_PTS_NoDetect[] = "PTS 2000 non d�tect�.";
static char cb_G_Ldb_PTS_NoDetect[] = "PTS 2000 nicht erkannt.";
static char cb_I_Ldb_PTS_NoDetect[] = "PTS 2000 non rilevato.";
static char cb_P_Ldb_PTS_NoDetect[] = "PTS 2000 n�o detectado.";

// Set840Labels
static char cb_E_Ldb_840_NoDetect[] = "840 Ventilator not detected";
static char cb_S_Ldb_840_NoDetect[] = "Ventilador 840 no detectado";
static char cb_F_Ldb_840_NoDetect[] = "Ventilateur 840 non d�tect�";
static char cb_G_Ldb_840_NoDetect[] = "Beatmungsger�t 840 nicht erkannt";
static char cb_I_Ldb_840_NoDetect[] = "Ventilatore 840 non rilevato";
static char cb_P_Ldb_840_NoDetect[] = "Ventilador 840 n�o detectado";

static char cb_E_Ldb_Not_Found[] = "not found.";
static char cb_S_Ldb_Not_Found[] = "no se encuentra.";
static char cb_F_Ldb_Not_Found[] = "introuvable.";
static char cb_G_Ldb_Not_Found[] = "nicht gefunden.";
static char cb_I_Ldb_Not_Found[] = "non trovato.";
static char cb_P_Ldb_Not_Found[] = "n�o encontrado(a).";

// UpdatePTSInfo (ptsreset)
static char cb_E_Ldb_PTS_Comm_Rcv_Error[] = "PTS 2000 Communications error";
static char cb_S_Ldb_PTS_Comm_Rcv_Error[] = "Error de comunicaci�n PTS 2000";
static char cb_F_Ldb_PTS_Comm_Rcv_Error[] = "Erreur communication du PTS 2000";
static char cb_G_Ldb_PTS_Comm_Rcv_Error[] = "PTS 2000 Kommunikationsfehler";
static char cb_I_Ldb_PTS_Comm_Rcv_Error[] = "Errore comunicazione PTS 2000";
static char cb_P_Ldb_PTS_Comm_Rcv_Error[] = "Erro de comunica��es do PTS 2000";

static char cb_E_Ldb_PTS_Ver_Error[] = "An invalid version number was received from the PTS 2000.";
static char cb_S_Ldb_PTS_Ver_Error[] = "Se ha recibido un n�mero de versi�n no v�lido de PTS 2000.";
static char cb_F_Ldb_PTS_Ver_Error[] = "Le num�ro de version re�u du PTS 2000 est incorrect.";
static char cb_G_Ldb_PTS_Ver_Error[] = "Ung�ltige Versionsnummer empfangen vom PTS 2000.";
static char cb_I_Ldb_PTS_Ver_Error[] = "Numero di versione non valido ricevuto da PTS 2000.";
static char cb_P_Ldb_PTS_Ver_Error[] = "N�mero de vers�o inv�lido recebido do PTS 2000.";

static char cb_E_Ldb_PTS_SN_Error[] = "An invalid serial number was received from the PTS 2000.";
static char cb_S_Ldb_PTS_SN_Error[] = "Se ha recibido un n�mero de serie no v�lido de PTS 2000.";
static char cb_F_Ldb_PTS_SN_Error[] = "Le num�ro de s�rie re�u du PTS 2000 est incorrect.";
static char cb_G_Ldb_PTS_SN_Error[] = "Ung�ltige Seriennummer empfangen vom PTS 2000.";
static char cb_I_Ldb_PTS_SN_Error[] = "Numero di serie non valido fornito da PTS 2000.";
static char cb_P_Ldb_PTS_SN_Error[] = "N�mero de s�rie inv�lido recebido do PTS 2000.";

static char cb_E_Ldb_PTS_Date_Error[] = "An invalid calibration date was received from the PTS 2000.";
static char cb_S_Ldb_PTS_Date_Error[] = "Se ha recibido una fecha de calibraci�n no v�lida de PTS 2000.";
static char cb_F_Ldb_PTS_Date_Error[] = "La date d'�talonnage re�ue du PTS 2000 est incorrecte.";
static char cb_G_Ldb_PTS_Date_Error[] = "Ung�ltiges Kalibrierungsdatum empfangen vom PTS 2000.";
static char cb_I_Ldb_PTS_Date_Error[] = "Data calibrazione non valida fornita da PTS 2000.";
static char cb_P_Ldb_PTS_Date_Error[] = "Data de calibra��o inv�lida recebida do PTS 2000.";

static char cb_E_Ldb_invalid[] = "Invalid";
static char cb_S_Ldb_invalid[] = "No v�lido";
static char cb_F_Ldb_invalid[] = "Invalide";
static char cb_G_Ldb_invalid[] = "Ung�ltig";
static char cb_I_Ldb_invalid[] = "Non valido";
static char cb_P_Ldb_invalid[] = "Inv�lido";

// Reset_PTS_Device (ptsreset)
static char cb_E_Ldb_Reseting_PTS[] = "Resetting PTS 2000 ... %d";
static char cb_S_Ldb_Reseting_PTS[] = "Reponiendo PTS 2000 ... %d";
static char cb_F_Ldb_Reseting_PTS[] = "R�initialisation PTS 2000 ... %d";
static char cb_G_Ldb_Reseting_PTS[] = "PTS 2000 wird zur�ckgesetzt ... %d";
static char cb_I_Ldb_Reseting_PTS[] = "Reimpostazione PTS 2000 ... %d";
static char cb_P_Ldb_Reseting_PTS[] = "Redefinindo PTS 2000 ... %d";

// Software Download
// softdlic.c
// MakeAgreePanel
// Panel Title
static char cb_E_Ldb_SoftdLicPanelTitle[] = "ELECTRONIC END USER LICENSE AGREEMENT";
static char cb_S_Ldb_SoftdLicPanelTitle[] = "ACUERDO DE LICENCIA ELECTR�NICO DEL USUARIO FINAL";
static char cb_F_Ldb_SoftdLicPanelTitle[] = "CONTRAT DE LICENCE ELECTRONIQUE DE L'UTILISATEUR";
static char cb_G_Ldb_SoftdLicPanelTitle[] = "ELEKTRONISCHE ENDNUTZER LIZENZVEREINBARUNG";
static char cb_I_Ldb_SoftdLicPanelTitle[] = "CONTRATTO DI LICENZA ELETTRONICA CON L'UTENTE FINALE";
static char cb_P_Ldb_SoftdLicPanelTitle[] = "CONTRATO DE LINCEN�A ELETR�NICO PARA USU�RIO FINAL";

// AGREE_TXT_LINE_1
static char cb_E_Ldb_SoftdLicAgreeTxtLine01[] = "Notice to user: By utilizing this CD-ROM and the software it contains, you accept all the terms and conditions of the BreathLab 840 VTS Software agreement.";
static char cb_S_Ldb_SoftdLicAgreeTxtLine01[] = "Aviso a los Usuarios:  Al utilizar este CD-ROM y el software que contiene, Ud. acepta todos los t�rminos y condiciones del Acuerdo para el Software BreathLab 840 VTS.";
static char cb_F_Ldb_SoftdLicAgreeTxtLine01[] = "Notice d'utilisation: En utilisant ce CD-ROM et le logiciel qu'il contient, vous acceptez tous les termes et les conditions du logiciel BreathLab 840 VTS.";
static char cb_G_Ldb_SoftdLicAgreeTxtLine01[] = "Nutzerhinweis: Mit dem Gebrauch dieser CD-ROM und der darauf enthaltenen Software, akzeptieren Sie alle Bestimmungen und Bedingungen des BreathLab 840VTS Lizenzvertrages.";
static char cb_I_Ldb_SoftdLicAgreeTxtLine01[] = "Informazione per l'utilizzatore: Utilizzando questo CD-ROM e il software che esso contiene, lei accetta tutti i termini e le condizioni in accordo con il software del BreathLab 840 VTS.";
static char cb_P_Ldb_SoftdLicAgreeTxtLine01[] = "Aviso ao usu�rio: Ao utilizar este CD-ROM e o software que ele cont�m, voc� aceitar� todos os termos e condi��es do contrato do Software BreathLab 840 VTS.";

// AGREE_LICENSE_TEXT
static char cb_E_Ldb_SoftdAgreeLicText[] = "By utilizing this software you agree that:";
static char cb_S_Ldb_SoftdAgreeLicText[] = "Al utilizar este software, Ud. est� de acuerdo con que:";
static char cb_F_Ldb_SoftdAgreeLicText[] = "En utilisant ce logiciel, vous acceptez que:";
static char cb_G_Ldb_SoftdAgreeLicText[] = "Mit dem Gebrauch dieser Software best�tigen Sie, dass:";
static char cb_I_Ldb_SoftdAgreeLicText[] = "Utilizzando questo software , accetta quanto segue:";
static char cb_P_Ldb_SoftdAgreeLicText[] = "Utilizando este software, voc� concordar� que:";

// License_Para_1
static char cb_E_Ldb_SoftdLicPara_1[] = "1) You have legally obtained the 840 software CD-ROM through Puritan-Bennett.";
static char cb_S_Ldb_SoftdLicPara_1[] = "1) Ud. ha obtenido legalmente el software CD-ROM a trav�s de  Puritan-Bennett.";
static char cb_F_Ldb_SoftdLicPara_1[] = "1) Vous avez l�galement obtenu le logiciel CDROM par le Putitan-Bennet.";
static char cb_G_Ldb_SoftdLicPara_1[] = "1) Sie die Software CD-ROM legal von Puritan Bennett erworben haben.";
static char cb_I_Ldb_SoftdLicPara_1[] = "1) Di aver ottenuto legalmente il CD ROM software attraverso la Puritan-Bennett.";
static char cb_P_Ldb_SoftdLicPara_1[] = "1) Voc� obteve legalmente o CDROM atrav�s da Puritan-Bennett.";

// License_Para_2
static char cb_E_Ldb_SoftdLicPara_2[] = "2) You received training in operating the 840 Ventilator and performing ventilator software updates.";
static char cb_S_Ldb_SoftdLicPara_2[] = "2) Ud. se ha adiestrado en el funcionamiento del Ventilador 840 y en el funcionamiento actualizado de este software.";
static char cb_F_Ldb_SoftdLicPara_2[] = "2) Vous �tes form�s � la manipulation du Ventilateur 840 et � la mise � jour du logiciel.";
static char cb_G_Ldb_SoftdLicPara_2[] = "2) Sie f�r die Bedienung des 840 Ventilators und die Durchf�hrung dieser Softwareaktualisierung geschult wurden.";
static char cb_I_Ldb_SoftdLicPara_2[] = "2) Di aver frequentato un corso sulla operativit� del Ventilatore 840 e sulle modalit� per effettuare l'aggiornamento software.";
static char cb_P_Ldb_SoftdLicPara_2[] = "2) Voc� foi treinado na opera��o do Ventilador 840 e na execu��o da atualiza��o deste software.";

// License_Para_3
static char cb_E_Ldb_SoftdLicPara_3_1[] = "3) You will not modify, adapt, translate, reverse engineer, decompile, disassemble, \n";
static char cb_S_Ldb_SoftdLicPara_3_1[] = "3) Ud. se compromete a no modificar, adaptar, traducir, hacer ingenier�a revertida, desordenar, desarmar, \n";
static char cb_F_Ldb_SoftdLicPara_3_1[] = "3) Vous consentez � ne pas modifier, adapter, traduire, d�compiler, d�monter, \n";
static char cb_G_Ldb_SoftdLicPara_3_1[] = "3) Sie erkl�ren sich damit einverstanden, dass Sie diese Software nicht modifizieren, anpassen, �bersetzen, r�ckentwickeln, decompilieren, disassemblieren, \n";
static char cb_I_Ldb_SoftdLicPara_3_1[] = "3) Di accettare di non modificare, adattare, trasmettere, scomporre, \n";
static char cb_P_Ldb_SoftdLicPara_3_1[] = "3) Voc� concorda em n�o modificar, adaptar, traduzir, inverter a instala��o, decompor, desagrupar, \n";

static char cb_E_Ldb_SoftdLicPara_3_2[] = "   or otherwise attempt to discover the source code of the 840 Software.";
static char cb_S_Ldb_SoftdLicPara_3_2[] = "   o intentar descubrir en modo alguno el c�digo del software.";
static char cb_F_Ldb_SoftdLicPara_3_2[] = "   ou autrement essayer de d�couvrir le code source du Logiciel.";
static char cb_G_Ldb_SoftdLicPara_3_2[] = "   oder in anderer Weise behandeln d�rfen, um den Quellcode auszulesen.";
static char cb_I_Ldb_SoftdLicPara_3_2[] = "   o tentare di scoprire la fonte del codice del Software.";
static char cb_P_Ldb_SoftdLicPara_3_2[] = "   ou, por outro lado, tentar descobrir o c�digo fonte do Software.";

// AGREE_YES
static char cb_E_Ldb_SoftdLicAgreeYes[] = "I AGREE";
static char cb_S_Ldb_SoftdLicAgreeYes[] = "ESTOY DE ACUERDO";
static char cb_F_Ldb_SoftdLicAgreeYes[] = "J'ACCEPTE";
static char cb_G_Ldb_SoftdLicAgreeYes[] = "Ich stimme zu";
static char cb_I_Ldb_SoftdLicAgreeYes[] = "ACCETTO";
static char cb_P_Ldb_SoftdLicAgreeYes[] = "EU CONCORDO";

// AGREE_NO
static char cb_E_Ldb_SoftdLicAgreeNo[] = "I DO NOT AGREE";
static char cb_S_Ldb_SoftdLicAgreeNo[] = "NO ESTOY DE ACUERDO";
static char cb_F_Ldb_SoftdLicAgreeNo[] = "JE REFUSE";
static char cb_G_Ldb_SoftdLicAgreeNo[] = "Ich stimme nicht zu";
static char cb_I_Ldb_SoftdLicAgreeNo[] = "NON ACCETTO";
static char cb_P_Ldb_SoftdLicAgreeNo[] = "EU N�O CONCORDO";

// softdload.c
// BOOT_PANEL
// Panel Title
static char cb_E_Ldb_BootPanelTitle[] = "BootP Server Status";
static char cb_S_Ldb_BootPanelTitle[] = "Estado del BootP";
static char cb_F_Ldb_BootPanelTitle[] = "�tat du BootP";
static char cb_G_Ldb_BootPanelTitle[] = "BootP Status";
static char cb_I_Ldb_BootPanelTitle[] = "Stato BootP";
static char cb_P_Ldb_BootPanelTitle[] = "Estado do BootP";

// BOOTP Error
static char cb_E_Ldb_BootP_Error[] = " BOOTP ERROR ";
static char cb_S_Ldb_BootP_Error[] = " Error BOOTP ";
static char cb_F_Ldb_BootP_Error[] = " BOOTP erreur ";
static char cb_G_Ldb_BootP_Error[] = " BOOTP FEHLER ";
static char cb_I_Ldb_BootP_Error[] = " ERRORE BOOTP ";
static char cb_P_Ldb_BootP_Error[] = " ERRO DO BOOTP ";

// BOOTP Processing
static char cb_E_Ldb_BootP_Processing[] = " BOOTP PROCESSING ";
static char cb_S_Ldb_BootP_Processing[] = " Procesando BOOTP ";
static char cb_F_Ldb_BootP_Processing[] = " BOOTP en cours ";
static char cb_G_Ldb_BootP_Processing[] = " BOOTP WIRD DURCHGEF�HRT ";
static char cb_I_Ldb_BootP_Processing[] = " BOOTP IN PROCESSO ";
static char cb_P_Ldb_BootP_Processing[] = " BOOTP PROCESSANDO ";

// BootP Ready
static char cb_E_Ldb_BootP_Ready[] = " BOOTP READY ";
static char cb_S_Ldb_BootP_Ready[] = " Listo BOOTP ";
static char cb_F_Ldb_BootP_Ready[] = " BOOTP pr�t ";
static char cb_G_Ldb_BootP_Ready[] = " BOOTP FERTIG ";
static char cb_I_Ldb_BootP_Ready[] = " BOOTP PRONTO ";
static char cb_P_Ldb_BootP_Ready[] = " BOOTP PREPARADO ";

// BOOTP Stopped
static char cb_E_Ldb_BootP_Stopped[] = " BOOTP STOPPED ";
static char cb_S_Ldb_BootP_Stopped[] = " Detenido BOOTP ";
static char cb_F_Ldb_BootP_Stopped[] = " BOOTP arr�t� ";
static char cb_G_Ldb_BootP_Stopped[] = " BOOTP ANGEHALTEN ";
static char cb_I_Ldb_BootP_Stopped[] = " BOOTP BLOCCATO ";
static char cb_P_Ldb_BootP_Stopped[] = " BOOTP PARADO ";

// IMAG_PANEL
// IMAG_PANEL_TITLE
static char cb_E_Ldb_ImagPanelTitle[] = "Select Firmware Revision";
static char cb_S_Ldb_ImagPanelTitle[] = "Seleccione la Revisi�n de Firmware";
static char cb_F_Ldb_ImagPanelTitle[] = "Choisir le programme usine";
static char cb_G_Ldb_ImagPanelTitle[] = "Firmware Revision ausw�hlen";
static char cb_I_Ldb_ImagPanelTitle[] = "Seleziona Revisione Firmware";
static char cb_P_Ldb_ImagPanelTitle[] = "Selecionar a Revis�o do Firmware";

// 840 GUIsoftware Label
static char cb_E_Ldb_840_GUIsoftware_Lbl[] = "GUI software version:";
static char cb_S_Ldb_840_GUIsoftware_Lbl[] = "Versi�n del software de IGU:";
static char cb_F_Ldb_840_GUIsoftware_Lbl[] = "Version du logiciel de IGU:";
static char cb_G_Ldb_840_GUIsoftware_Lbl[] = "GUI-Softwareversion:";
static char cb_I_Ldb_840_GUIsoftware_Lbl[] = "Versione software di GUI:";
static char cb_P_Ldb_840_GUIsoftware_Lbl[] = "Vers�o do software da GUI:";

// 840 BDUsoftware Label
static char cb_E_Ldb_840_BDUsoftware_Lbl[] = "BDU software version:";
static char cb_S_Ldb_840_BDUsoftware_Lbl[] = "Versi�n del software de BDU:";
static char cb_F_Ldb_840_BDUsoftware_Lbl[] = "Version du logiciel de BDU:"; //l'unit� insuf
static char cb_G_Ldb_840_BDUsoftware_Lbl[] = "BDU Softwareversion:";
static char cb_I_Ldb_840_BDUsoftware_Lbl[] = "Versione software di BDU:";
static char cb_P_Ldb_840_BDUsoftware_Lbl[] = "Vers�o do software de BDU:";

// IMAG_PANEL_ADAPTER_LIST
static char cb_E_Ldb_ImagPnlIPlist_lbl[] = "Select Network Port From List";
static char cb_S_Ldb_ImagPnlIPlist_lbl[] = "Seleccione el puerto Ethernet de la Lista";
static char cb_F_Ldb_ImagPnlIPlist_lbl[] = "Choisir le port Ethernet dans la liste";
static char cb_G_Ldb_ImagPnlIPlist_lbl[] = "Ethernet port in der Liste ausw�hlen";
static char cb_I_Ldb_ImagPnlIPlist_lbl[] = "Seleziona Dalla Lista il port del Ethernet";
static char cb_P_Ldb_ImagPnlIPlist_lbl[] = "Selecionar o porta do Ethernet da Lista";

// IMAG_PANEL_IMAG_LIST
static char cb_E_Ldb_ImagPnlImaglist_lbl[] = "Select Software File From List";
static char cb_S_Ldb_ImagPnlImaglist_lbl[] = "Seleccione el Archivo de Software de la Lista";
static char cb_F_Ldb_ImagPnlImaglist_lbl[] = "Choisir le logiciel dans la liste";
static char cb_G_Ldb_ImagPnlImaglist_lbl[] = "Software Datei in der Liste ausw�hlen";
static char cb_I_Ldb_ImagPnlImaglist_lbl[] = "Seleziona Dalla Lista il File del Software";
static char cb_P_Ldb_ImagPnlImaglist_lbl[] = "Selecionar o Arquivo do Software da Lista";

// INS_PANEL
// INS_PANEL_TITLE (also used for "Cycle Power" prior to dload)
static char cb_E_Ldb_InsPanelTitle[] = "840 Instruction";
static char cb_S_Ldb_InsPanelTitle[] = "Instrucci�n 840";
static char cb_F_Ldb_InsPanelTitle[] = "Instruction 840";
static char cb_G_Ldb_InsPanelTitle[] = "840 Anleitung";
static char cb_I_Ldb_InsPanelTitle[] = "Istruzioni 840";
static char cb_P_Ldb_InsPanelTitle[] = "Instru��o do 840";

// INS_PANEL_INSTRUCTIONS
static char cb_E_Ldb_InsPnlInstr_Line01[] = " Install serial cable between computer and ventilator GUI assembly.\n";
static char cb_E_Ldb_InsPnlInstr_Line02[] = " Install ethernet breakout box between ventilator GUI and BDU.\n";
static char cb_E_Ldb_InsPnlInstr_Line03[] = " Install ethernet cable between breakout box and computer.\n";
static char cb_E_Ldb_InsPnlInstr_Line04[] = " Power-up ventilator in Service Mode for External Test Control.";

static char cb_S_Ldb_InsPnlInstr_Line01[] = " Instale el cable de serie entre el ensamblaje IGU de la computadora y el ventilador.\n";
static char cb_S_Ldb_InsPnlInstr_Line02[] = " Instale la salida de la caja ethernet entre el ventilador IGU y BDU.\n";
static char cb_S_Ldb_InsPnlInstr_Line03[] = " Instale el cable de ethernet entre la caja de salida y la computadora.\n";
static char cb_S_Ldb_InsPnlInstr_Line04[] = " Active el ventilador en la modalidad de Servicio para Prueba de Control Externo.";

static char cb_F_Ldb_InsPnlInstr_Line01[] = " Installer le c�ble s�rie entre l'ordinateur et l'�cran du respirateur.\n";
static char cb_F_Ldb_InsPnlInstr_Line02[] = " Installer le bo�tier �thernet entre le c�ble �cran et l'unit� BDU.\n";
static char cb_F_Ldb_InsPnlInstr_Line03[] = " Installer le c�ble �thernet entre le bo�tier et l'ordinateur.\n";
static char cb_F_Ldb_InsPnlInstr_Line04[] = " D�marrer le respirateur en mode service et en contr�le externe.";

static char cb_G_Ldb_InsPnlInstr_Line01[] = " Serielles Kabel zwischen Computer und GUI-Aufsatz vom Beatmungsger�t anschliessen.\n";
static char cb_G_Ldb_InsPnlInstr_Line02[] = " Ethernet Breakout Box zwischen GUI und BDU vom Beatmungsger�t anschliessen.\n";
static char cb_G_Ldb_InsPnlInstr_Line03[] = " Ethernet Kabel zwischen Breakout Box und Computer anschliessen.\n";
static char cb_G_Ldb_InsPnlInstr_Line04[] = " Beatmungsger�t einschalten und den Modus Externe Verbindung im Service Mode anw�hlen.";

static char cb_I_Ldb_InsPnlInstr_Line01[] = " Installare il cavo seriale tra il computer e il modulo GUI del ventilatore.\n";
static char cb_I_Ldb_InsPnlInstr_Line02[] = " Installare ethernet breakout box tra la GUI e la BDU del ventilatore.\n";
static char cb_I_Ldb_InsPnlInstr_Line03[] = " Installare il cavo ethernet tra il breakout box e il computer.\n";
static char cb_I_Ldb_InsPnlInstr_Line04[] = " Accendere il ventilatore in Service Mode per accedere al Controllo Test Esterni.";

static char cb_P_Ldb_InsPnlInstr_Line01[] = " Instale o cabo seriado entre o computador e o conjunto do GUI do ventilador.\n";
static char cb_P_Ldb_InsPnlInstr_Line02[] = " Instale a caixa do breakout do Ethernet entre o GUI e o BDU do ventilador.\n";
static char cb_P_Ldb_InsPnlInstr_Line03[] = " Instale o cabo do Ethernet entre a caixa do breakout e o computador.\n";
static char cb_P_Ldb_InsPnlInstr_Line04[] = " Come�a o ventilador no modo do servi�o para o controle externo do teste.";

// INS_PANEL_TEXTMSG
static char cb_E_Ldb_InsPnlTextMsg[] = "Prepare Ventilator for Software Download";
static char cb_S_Ldb_InsPnlTextMsg[] = "Prepare  el ventilador para la instalaci�n del Software";
static char cb_F_Ldb_InsPnlTextMsg[] = "Pr�paration du respirateur pour un chargement logiciel";
static char cb_G_Ldb_InsPnlTextMsg[] = "Beatmungsger�t f�r Software Download vorbereiten";
static char cb_I_Ldb_InsPnlTextMsg[] = "Preparazione del Ventilatore per il Software Download";
static char cb_P_Ldb_InsPnlTextMsg[] = "Prepara-se o ventilador para o download do software";

// STATEPANEL
static char cb_E_Ldb_StatePnlTitle[] = "Ventilator State";
static char cb_S_Ldb_StatePnlTitle[] = "Estado del Ventilador";
static char cb_F_Ldb_StatePnlTitle[] = "�tat du respirateur";
static char cb_G_Ldb_StatePnlTitle[] = "Ventilator Status";
static char cb_I_Ldb_StatePnlTitle[] = "Stato Ventilatore";
static char cb_P_Ldb_StatePnlTitle[] = "Estado do Ventilador";

static char cb_E_Ldb_StatePnlInit[] = "Initial Download - BDU and/or GUI Software";
static char cb_S_Ldb_StatePnlInit[] = "Instalaci�n Inicial - BDU y o IGU Software";
static char cb_F_Ldb_StatePnlInit[] = "Chargement initial - BDU et ou GUI logiciel";
static char cb_G_Ldb_StatePnlInit[] = "Initial Download - BDU und oder GUI Software";
static char cb_I_Ldb_StatePnlInit[] = "Inizio Download";
static char cb_P_Ldb_StatePnlInit[] = "Download Inicial - BDU e ou GUI Software";

static char cb_E_Ldb_StatePnlReplace[] = "Replace BDU and GUI Software";
static char cb_S_Ldb_StatePnlReplace[] = "Reemplace el Software BDU & IGU";
static char cb_F_Ldb_StatePnlReplace[] = "Chargement du logiciel BD & GUI";
static char cb_G_Ldb_StatePnlReplace[] = "BDU und GUI Software ersetzen";
static char cb_I_Ldb_StatePnlReplace[] = "Rimpiazzare Software BDU & GUI";
static char cb_P_Ldb_StatePnlReplace[] = "Substitua o Software de BDU & do GUI";

// TFTP_PANEL
// TFTP_PANEL_TITLE
static char cb_E_Ldb_TFTP_PnlTitle[] = "File Transfer Progress";
static char cb_S_Ldb_TFTP_PnlTitle[] = "Progreso del Traspaso de Archivo";
static char cb_F_Ldb_TFTP_PnlTitle[] = "Transfert de fichier en cours";
static char cb_G_Ldb_TFTP_PnlTitle[] = "Fortschritt der Daten�bertragung";
static char cb_I_Ldb_TFTP_PnlTitle[] = "Trasferimento File in corso";
static char cb_P_Ldb_TFTP_PnlTitle[] = "Progresso da Transfer�ncia do Arquivo";

static char cb_E_Ldb_TFTP_BDUfile_lbl[] = "BDU File:";
static char cb_S_Ldb_TFTP_BDUfile_lbl[] = "Archivo BDU:";
static char cb_F_Ldb_TFTP_BDUfile_lbl[] = "Fichier BDU:";
static char cb_G_Ldb_TFTP_BDUfile_lbl[] = "BDU Datei:";
static char cb_I_Ldb_TFTP_BDUfile_lbl[] = "File BDU:";
static char cb_P_Ldb_TFTP_BDUfile_lbl[] = "BDU File:";

static char cb_E_Ldb_TFTP_GUIfile_lbl[] = "GUI File:";
static char cb_S_Ldb_TFTP_GUIfile_lbl[] = "Archivo IGU:";
static char cb_F_Ldb_TFTP_GUIfile_lbl[] = "Fichier GUI:";
static char cb_G_Ldb_TFTP_GUIfile_lbl[] = "GUI Datei:";
static char cb_I_Ldb_TFTP_GUIfile_lbl[] = "File GUI:";
static char cb_P_Ldb_TFTP_GUIfile_lbl[] = "GUI File:";

static char cb_E_Ldb_TFTP_BDUpercent_lbl[] = "% Completed";
static char cb_S_Ldb_TFTP_BDUpercent_lbl[] = "% Completado";
static char cb_F_Ldb_TFTP_BDUpercent_lbl[] = "% transf�r�s";
static char cb_G_Ldb_TFTP_BDUpercent_lbl[] = "% abgeschlossen";
static char cb_I_Ldb_TFTP_BDUpercent_lbl[] = "Completato %";
static char cb_P_Ldb_TFTP_BDUpercent_lbl[] = "% Completa";

static char cb_E_Ldb_TFTP_GUIpercent_lbl[] = "% Completed";
static char cb_S_Ldb_TFTP_GUIpercent_lbl[] = "% Completado";
static char cb_F_Ldb_TFTP_GUIpercent_lbl[] = "% transf�r�s";
static char cb_G_Ldb_TFTP_GUIpercent_lbl[] = "% abgeschlossen";
static char cb_I_Ldb_TFTP_GUIpercent_lbl[] = "Completato %";
static char cb_P_Ldb_TFTP_GUIpercent_lbl[] = "% Completa";

static char cb_E_Ldb_TFTP_BDUblocks_lbl[] = "Blocks Sent:";
static char cb_S_Ldb_TFTP_BDUblocks_lbl[] = "Bloques Enviados:";
static char cb_F_Ldb_TFTP_BDUblocks_lbl[] = "Blocs envoy�s:";
static char cb_G_Ldb_TFTP_BDUblocks_lbl[] = "Gesendete Blocks:";
static char cb_I_Ldb_TFTP_BDUblocks_lbl[] = "Invio Dati:";
static char cb_P_Ldb_TFTP_BDUblocks_lbl[] = "Os Blocos Emitiram:";

static char cb_E_Ldb_TFTP_BDUtotal_lbl[] = "Total Blocks:";
static char cb_S_Ldb_TFTP_BDUtotal_lbl[] = "Bloques Totales:";
static char cb_F_Ldb_TFTP_BDUtotal_lbl[] = "Total des blocs:";
static char cb_G_Ldb_TFTP_BDUtotal_lbl[] = "Gesamt Blocks:";
static char cb_I_Ldb_TFTP_BDUtotal_lbl[] = "Dati Totali:";
static char cb_P_Ldb_TFTP_BDUtotal_lbl[] = "Blocos Total:";

static char cb_E_Ldb_TFTP_GUIblocks_lbl[] = "Blocks Sent:";
static char cb_S_Ldb_TFTP_GUIblocks_lbl[] = "Bloques Enviados:";
static char cb_F_Ldb_TFTP_GUIblocks_lbl[] = "Blocs envoy�s:";
static char cb_G_Ldb_TFTP_GUIblocks_lbl[] = "Gesendete Blocks:";
static char cb_I_Ldb_TFTP_GUIblocks_lbl[] = "Invio Dati:";
static char cb_P_Ldb_TFTP_GUIblocks_lbl[] = "Os Blocos Emitiram:";

static char cb_E_Ldb_TFTP_GUItotal_lbl[] = "Total Blocks:";
static char cb_S_Ldb_TFTP_GUItotal_lbl[] = "Bloques Totales:";
static char cb_F_Ldb_TFTP_GUItotal_lbl[] = "Total des blocs:";
static char cb_G_Ldb_TFTP_GUItotal_lbl[] = "Gesamt Blocks:";
static char cb_I_Ldb_TFTP_GUItotal_lbl[] = "Dati Totali:";
static char cb_P_Ldb_TFTP_GUItotal_lbl[] = "Blocos Total:";

static char cb_E_Ldb_TFTP_BDUmsg[] = "Updating BDU Flash Rom";
static char cb_S_Ldb_TFTP_BDUmsg[] = "Actualizando el Flash Rom BDU";
static char cb_F_Ldb_TFTP_BDUmsg[] = "Mise � jour de la flash rom BDU";
static char cb_G_Ldb_TFTP_BDUmsg[] = "Update des BDU Flash ROM l�uft";
static char cb_I_Ldb_TFTP_BDUmsg[] = "Aggiornamento Flash Rom BDU";
static char cb_P_Ldb_TFTP_BDUmsg[] = "Atualizando o Flash Rom do BDU";

static char cb_E_Ldb_TFTP_BDUcomplete[] = "BDU PROCESS COMPLETE";
static char cb_S_Ldb_TFTP_BDUcomplete[] = "Completado el Proceso de BDU";
static char cb_F_Ldb_TFTP_BDUcomplete[] = "Proc�dure sur le BDU achev�e";
static char cb_G_Ldb_TFTP_BDUcomplete[] = "BDU Vorgang abgeschlossen";
static char cb_I_Ldb_TFTP_BDUcomplete[] = "PROCESSO BDU COMPLETO";
static char cb_P_Ldb_TFTP_BDUcomplete[] = "PROCESSO COMPLETO DO BDU";

static char cb_E_Ldb_TFTP_GUImsg[] = "Updating GUI Flash Rom";
static char cb_S_Ldb_TFTP_GUImsg[] = "Actualizando el Flash Rom IGU";
static char cb_F_Ldb_TFTP_GUImsg[] = "Mise � jour de la flash rom GUI";
static char cb_G_Ldb_TFTP_GUImsg[] = "Update des GUI Flash ROM l�uft";
static char cb_I_Ldb_TFTP_GUImsg[] = "Aggiornamento Flash Rom GUI";
static char cb_P_Ldb_TFTP_GUImsg[] = "Atualizando o Flash Rom do GUI";

static char cb_E_Ldb_TFTP_GUIcomplete[] = "GUI PROCESS COMPLETE";
static char cb_S_Ldb_TFTP_GUIcomplete[] = "Completado el Proceso de IGU";
static char cb_F_Ldb_TFTP_GUIcomplete[] = "Proc�dure sur le GUI achev�e";
static char cb_G_Ldb_TFTP_GUIcomplete[] = "GUI Vorgang abgeschlossen";
static char cb_I_Ldb_TFTP_GUIcomplete[] = "PROCESSO GUI COMPLETO";
static char cb_P_Ldb_TFTP_GUIcomplete[] = "PROCESSO COMPLETO DO GUI";

// 75677.c
// StartDload
static char cb_E_Ldb_Dload_CannotEraseBDU[] = "Can Not Erase BDU Image File in Ventilator";
static char cb_S_Ldb_Dload_CannotEraseBDU[] = "No puede Borrar el Archivo de Imagen BDU en el Ventilador";
static char cb_F_Ldb_Dload_CannotEraseBDU[] = "Impossible d'effacer le fichier image BDU du respirateur";
static char cb_G_Ldb_Dload_CannotEraseBDU[] = "BDU Image Datei des Beatmungsger�tes kann nicht gel�scht werden";
static char cb_I_Ldb_Dload_CannotEraseBDU[] = "Cancellazione non possibile del File contenente il Linguaggio BDU del ventilatore";
static char cb_P_Ldb_Dload_CannotEraseBDU[] = "Nao pode apagar o arquivo da imagem do BDU no ventilador";

static char cb_E_Ldb_Dload_CannotEraseGUI[] = "Can Not Erase GUI Image File in Ventilator";
static char cb_S_Ldb_Dload_CannotEraseGUI[] = "No puede Borrar el Archivo de Imagen IGU en el Ventilador";
static char cb_F_Ldb_Dload_CannotEraseGUI[] = "Impossible d'effacer le fichier image GUI du respirateur";
static char cb_G_Ldb_Dload_CannotEraseGUI[] = "GUI Image Datei des Beatmungsger�tes kann nicht gel�scht werden";
static char cb_I_Ldb_Dload_CannotEraseGUI[] = "Cancellazione non possibile del File contenente il Linguaggio GUI del ventilatore";
static char cb_P_Ldb_Dload_CannotEraseGUI[] = "Nao pode apagar o arquivo da imagem do GUI no ventilador";

// 840 Cycle Power
static char cb_E_Ldb_840_cycle_power[] = "Cycle the 840 Ventilator.";
static char cb_S_Ldb_840_cycle_power[] = "Ejecute un ciclo de funcionamiento del ventilador 840.";
static char cb_F_Ldb_840_cycle_power[] = "�teindre et allumer ventilateur 840.";
static char cb_G_Ldb_840_cycle_power[] = "Beatmungsger�t 840 aus- und wieder einschalten.";
static char cb_I_Ldb_840_cycle_power[] = "Accendere ventilatore 840.";
static char cb_P_Ldb_840_cycle_power[] = "Ligar e desligar o Ventilador 840.";

// StartServer
static char cb_E_Ldb_Dload_UserAbortTitle[] = "User Abort";
static char cb_S_Ldb_Dload_UserAbortTitle[] = "Aborto de Usuario";
static char cb_F_Ldb_Dload_UserAbortTitle[] = "Abandon utilisateur";
static char cb_G_Ldb_Dload_UserAbortTitle[] = "Abbruch durch Benutzer";
static char cb_I_Ldb_Dload_UserAbortTitle[] = "Interruzione da Operatore";
static char cb_P_Ldb_Dload_UserAbortTitle[] = "Aborto do Usu�rio";

static char cb_E_Ldb_Dload_UserAbortMsg[] = "Cancel";
static char cb_S_Ldb_Dload_UserAbortMsg[] = "Cancelar";
static char cb_F_Ldb_Dload_UserAbortMsg[] = "Abandon";
static char cb_G_Ldb_Dload_UserAbortMsg[] = "Abbrechen";
static char cb_I_Ldb_Dload_UserAbortMsg[] = "Cancellare";
static char cb_P_Ldb_Dload_UserAbortMsg[] = "Cancela";

static char cb_E_Ldb_Dload_TimeoutAbortTitle[] = "Timeout Abort";
static char cb_S_Ldb_Dload_TimeoutAbortTitle[] = "Aborto del tiempo expirado";
static char cb_F_Ldb_Dload_TimeoutAbortTitle[] = "Temps d�pass�";
static char cb_G_Ldb_Dload_TimeoutAbortTitle[] = "Abbruch durch Zeit�berschreitung";
static char cb_I_Ldb_Dload_TimeoutAbortTitle[] = "Interruzione da Timeout";
static char cb_P_Ldb_Dload_TimeoutAbortTitle[] = "Intervalo para Aborto";

static char cb_E_Ldb_Dload_BDUtimeoutMsg[] = "BDU Transmittal Timeout";
static char cb_S_Ldb_Dload_BDUtimeoutMsg[] = "Tiempo expirado para Transmisi�n de BDU";
static char cb_F_Ldb_Dload_BDUtimeoutMsg[] = "Temps d�pass� sur transmission BDU";
static char cb_G_Ldb_Dload_BDUtimeoutMsg[] = "Zeit�berschreitung bei �bertragung zur BDU";
static char cb_I_Ldb_Dload_BDUtimeoutMsg[] = "Timeout Trasmissione BDU";
static char cb_P_Ldb_Dload_BDUtimeoutMsg[] = "Intervalo da Transmiss�o do BDU";

static char cb_E_Ldb_Dload_GUItimeoutMsg[] = "GUI Transmittal Timeout";
static char cb_S_Ldb_Dload_GUItimeoutMsg[] = "Tiempo expirado para Transmisi�n de IGU";
static char cb_F_Ldb_Dload_GUItimeoutMsg[] = "Temps d�pass� sur transmission GUI";
static char cb_G_Ldb_Dload_GUItimeoutMsg[] = "Zeit�berschreitung bei �bertragung zur GUI";
static char cb_I_Ldb_Dload_GUItimeoutMsg[] = "Timeout Trasmissione GUI";
static char cb_P_Ldb_Dload_GUItimeoutMsg[] = "Intervalo da Transmiss�o do GUI";

static char cb_E_Ldb_Dload_CompleteTitle[] = "Done";
static char cb_S_Ldb_Dload_CompleteTitle[] = "Terminado";
static char cb_F_Ldb_Dload_CompleteTitle[] = "R�alis�";
static char cb_G_Ldb_Dload_CompleteTitle[] = "Fertig";
static char cb_I_Ldb_Dload_CompleteTitle[] = "Fatto";
static char cb_P_Ldb_Dload_CompleteTitle[] = "Pronto";

static char cb_E_Ldb_Dload_BDUcomplete[] = "BDU COMPLETE";
static char cb_S_Ldb_Dload_BDUcomplete[] = "Completado el BDU";
static char cb_F_Ldb_Dload_BDUcomplete[] = "BDU achev�e";
static char cb_G_Ldb_Dload_BDUcomplete[] = "BDU abgeschlossen";
static char cb_I_Ldb_Dload_BDUcomplete[] = "BDU COMPLETO";
static char cb_P_Ldb_Dload_BDUcomplete[] = "BDU COMPLETA";

static char cb_E_Ldb_Dload_GUIcomplete[] = "GUI COMPLETE";
static char cb_S_Ldb_Dload_GUIcomplete[] = "Completado el IGU";
static char cb_F_Ldb_Dload_GUIcomplete[] = "GUI achev�";
static char cb_G_Ldb_Dload_GUIcomplete[] = "GUI abgeschlossen";
static char cb_I_Ldb_Dload_GUIcomplete[] = "GUI COMPLETO";
static char cb_P_Ldb_Dload_GUIcomplete[] = "GUI COMPLETA";

// GetImageFile
static char cb_E_Ldb_ImagFileErrTitle[] = "Software File Error";
static char cb_S_Ldb_ImagFileErrTitle[] = "Error del Archivo de Software";
static char cb_F_Ldb_ImagFileErrTitle[] = "Erreur sur logiciel";
static char cb_G_Ldb_ImagFileErrTitle[] = "Software Datenfehler";
static char cb_I_Ldb_ImagFileErrTitle[] = "Errore File Software";
static char cb_P_Ldb_ImagFileErrTitle[] = "Erro do Arquivo do Software";

static char cb_E_Ldb_ImagFileErrNofind[] = "Can Not Locate Image File";
static char cb_S_Ldb_ImagFileErrNofind[] = "No puede localizar el Archivo de Im�genes";
static char cb_F_Ldb_ImagFileErrNofind[] = "Impossible de trouver le fichier image";
static char cb_G_Ldb_ImagFileErrNofind[] = "Image Datei nicht gefunden";
static char cb_I_Ldb_ImagFileErrNofind[] = "Impossibile Individuare il File del Linguaggio";
static char cb_P_Ldb_ImagFileErrNofind[] = "Nao Pode Encontrar o Arquivo da Imagem";

// SelfTest
static char cb_E_Ldb_NoDloadCDerrTitle[] = "Can not find CD with ventilator files";
static char cb_S_Ldb_NoDloadCDerrTitle[] = "No puede localizar el CD del software del ventilador 840";
static char cb_F_Ldb_NoDloadCDerrTitle[] = "Impossible de trouver le CD Logiciel de ventilateur 840";
static char cb_G_Ldb_NoDloadCDerrTitle[] = "Die CD mit der Beatmungsger�te-840 nicht gefunden";
static char cb_I_Ldb_NoDloadCDerrTitle[] = "Impossibile Individuare il CD contenente '840 Ventilator Software'";
static char cb_P_Ldb_NoDloadCDerrTitle[] = "Nao Pode Encontrar o CD do Software de Ventilador 840";

static char cb_E_Ldb_NoDloadCDerrMsg[] = "Please insert CD with ventilator software files.";
static char cb_S_Ldb_NoDloadCDerrMsg[] = "Insertar el CD del software del ventilador 840 en la unidad de CD-ROM.";
static char cb_F_Ldb_NoDloadCDerrMsg[] = "Placez le CD Logiciel de ventilateur 840 dans le lecteur de CD-ROM.";
static char cb_G_Ldb_NoDloadCDerrMsg[] = "Die CD mit der Beatmungsger�te-840 in das CD-ROM-Laufwerkeinlegen.";
static char cb_I_Ldb_NoDloadCDerrMsg[] = "Inserire il CD contenente '840 Ventilator Software' nell'unit� CD-ROM.";
static char cb_P_Ldb_NoDloadCDerrMsg[] = "Coloque o CD do Software de Ventilador 840 na unidade de CD-ROM.";


// ValidateLogin
static char cb_E_Ldb_Enter_Pwd_For[] = "__Enter password for ";
static char cb_S_Ldb_Enter_Pwd_For[] = "__Escribir la contrase�a para ";
static char cb_F_Ldb_Enter_Pwd_For[] = "__Entrer mot de passe pour ";
static char cb_G_Ldb_Enter_Pwd_For[] = "__Kennwort eingeben f�r ";
static char cb_I_Ldb_Enter_Pwd_For[] = "__Immettere password per ";
static char cb_P_Ldb_Enter_Pwd_For[] = "__Inserir senha de ";

// CPVTMessagePopup
static char cb_E_Ldb_Test_Failure[] = "Test failure";
static char cb_S_Ldb_Test_Failure[] = "Fallo de la prueba";
static char cb_F_Ldb_Test_Failure[] = "�chec du test";
static char cb_G_Ldb_Test_Failure[] = "Test mi�lungen";
static char cb_I_Ldb_Test_Failure[] = "Errore test";
static char cb_P_Ldb_Test_Failure[] = "Falha no teste";

static char cb_E_Ldb_programming_err[] = "Programming error";
static char cb_S_Ldb_programming_err[] = "Error de programaci�n";
static char cb_F_Ldb_programming_err[] = "Erreur programmation";
static char cb_G_Ldb_programming_err[] = "Programmierfehler";
static char cb_I_Ldb_programming_err[] = "Errore di programmazione";
static char cb_P_Ldb_programming_err[] = "Erro de programa��o";

static char cb_E_Ldb_sw_fault_1[] = "Invalid call to CPVTMessagePopup";
static char cb_E_Ldb_sw_fault_2[] = "Invalid call to CPVTConfirmPopup";
static char cb_E_Ldb_sw_fault_3[] = "Invalid call to CPVTPromptPopup";

static char cb_S_Ldb_sw_fault_1[] = "Llamada no v�lida a CPVTMessagePopup";
static char cb_S_Ldb_sw_fault_2[] = "Llamada no v�lida a CPVTConfirmPopup";
static char cb_S_Ldb_sw_fault_3[] = "Llamada no v�lida a CPVTPromptPopup";

static char cb_F_Ldb_sw_fault_1[] = "Appel � CPVTMessagePopup incorrect";
static char cb_F_Ldb_sw_fault_2[] = "Appel � CPVTConfirmPopup incorrect";
static char cb_F_Ldb_sw_fault_3[] = "Appel � CPVTPromptPopup incorrect";

static char cb_G_Ldb_sw_fault_1[] = "Ung�ltiger Aufruf von CPVTMessagePopup";
static char cb_G_Ldb_sw_fault_2[] = "Ung�ltiger Aufruf von CPVTConfirmPopup";
static char cb_G_Ldb_sw_fault_3[] = "Ung�ltiger Aufruf von CPVTPromptPopup";

static char cb_I_Ldb_sw_fault_1[] = "Chiamata non valida a CPVTMessagePopup";
static char cb_I_Ldb_sw_fault_2[] = "Chiamata non valida a CPVTConfirmPopup";
static char cb_I_Ldb_sw_fault_3[] = "Chiamata non valida a CPVTPromptPopup";

static char cb_P_Ldb_sw_fault_1[] = "Chamada inv�lida ao CPVTMessagePopup";
static char cb_P_Ldb_sw_fault_2[] = "Chamada inv�lida ao CPVTConfirmPopup";
static char cb_P_Ldb_sw_fault_3[] = "Chamada inv�lida ao CPVTPromptPopup";

// txreport.c
// WriteRptHeader
static char cb_E_Ldb_rpt_seq_name[] = "Sequence Name:      ";
static char cb_S_Ldb_rpt_seq_name[] = "Nombre de secuencia:      ";
static char cb_F_Ldb_rpt_seq_name[] = "Nom de s�quence:      ";
static char cb_G_Ldb_rpt_seq_name[] = "Name der Sequenz:      ";
static char cb_I_Ldb_rpt_seq_name[] = "Nome sequenza:      ";
static char cb_P_Ldb_rpt_seq_name[] = "Nome da seq��ncia:      ";

static char cb_E_Ldb_rpt_desc[] = "Description:";
static char cb_S_Ldb_rpt_desc[] = "Descripci�n:";
static char cb_F_Ldb_rpt_desc[] = "Description:";
static char cb_G_Ldb_rpt_desc[] = "Beschreibung:";
static char cb_I_Ldb_rpt_desc[] = "Descrizione:";
static char cb_P_Ldb_rpt_desc[] = "Descri��o:";

static char cb_E_Ldb_rpt_cr_operator[] = "\nOperator:           ";
static char cb_S_Ldb_rpt_cr_operator[] = "\nOperador:           ";
static char cb_F_Ldb_rpt_cr_operator[] = "\nOp�rateur:           ";
static char cb_G_Ldb_rpt_cr_operator[] = "\nBediener:           ";
static char cb_I_Ldb_rpt_cr_operator[] = "\nOperatore:           ";
static char cb_P_Ldb_rpt_cr_operator[] = "\nOperador:           ";

// WriteSeqHeader
static char cb_E_Ldb_rpt_title[] = "\n                              T E S T   R E P O R T \n\n";
static char cb_S_Ldb_rpt_title[] = "\n                        I N F O R M E   D E   P R U E B A \n\n";
static char cb_F_Ldb_rpt_title[] = "\n                            R A P P O R T   T E S T \n\n";
static char cb_G_Ldb_rpt_title[] = "\n                              T E S T B E R I C H T \n\n";
static char cb_I_Ldb_rpt_title[] = "\n                              T E S T   R E P O R T \n\n";
static char cb_P_Ldb_rpt_title[] = "\n                      R E L A T � R I O   D E   T E S T E \n\n";

static char cb_E_Ldb_rpt_test_station[] = "Test Station: ";
static char cb_S_Ldb_rpt_test_station[] = "Estaci�n de prueba: ";
static char cb_F_Ldb_rpt_test_station[] = "Cent. de test: ";
static char cb_G_Ldb_rpt_test_station[] = "Test Station: ";
static char cb_I_Ldb_rpt_test_station[] = "Stazione di verifica: ";
static char cb_P_Ldb_rpt_test_station[] = "Esta��o do teste: ";

static char cb_E_Ldb_station_desc[] = "BreathLab PTS 840 Ventilator Test Software";
static char cb_S_Ldb_station_desc[] = "Software de prueba del ventilador BreathLab PTS 840";
static char cb_F_Ldb_station_desc[] = "Logiciel de test BreathLab PTS pour ventilateur 840";
static char cb_G_Ldb_station_desc[] = "Beatmungsger�te-Testsoftware BreathLab PTS 840";
static char cb_I_Ldb_station_desc[] = "Software di prova del ventilatore BreathLab PTS 840";
static char cb_P_Ldb_station_desc[] = "Software de Teste do Ventilador BreathLab PTS 840";

static char cb_E_Ldb_rpt_stn_id[] = "Station ID: ";
static char cb_S_Ldb_rpt_stn_id[] = "ID estaci�n: ";
static char cb_F_Ldb_rpt_stn_id[] = "ID centre: ";
static char cb_G_Ldb_rpt_stn_id[] = "Stations-ID: ";
static char cb_I_Ldb_rpt_stn_id[] = "ID stazione: ";
static char cb_P_Ldb_rpt_stn_id[] = "ID de esta��o: ";

static char cb_E_Ldb_rpt_pgm_rev[] = "Program Rev: ";
static char cb_S_Ldb_rpt_pgm_rev[] = "Revisi�n programa: ";
static char cb_F_Ldb_rpt_pgm_rev[] = "R�v. programme: ";
static char cb_G_Ldb_rpt_pgm_rev[] = "Programmrevision: ";
static char cb_I_Ldb_rpt_pgm_rev[] = "Rev. programma: ";
static char cb_P_Ldb_rpt_pgm_rev[] = "Rev. do programa: ";

static char cb_E_Ldb_rpt_operator[] = "Operator: ";
static char cb_S_Ldb_rpt_operator[] = "Operador: ";
static char cb_F_Ldb_rpt_operator[] = "Op�rateur: ";
static char cb_G_Ldb_rpt_operator[] = "Bediener: ";
static char cb_I_Ldb_rpt_operator[] = "Operatore: ";
static char cb_P_Ldb_rpt_operator[] = "Operador: ";

static char cb_E_Ldb_rpt_uut_pn[] = "UUT P/N: ";
static char cb_S_Ldb_rpt_uut_pn[] = "N� de pieza UUT: ";
static char cb_F_Ldb_rpt_uut_pn[] = "R�f. UUT: ";
static char cb_G_Ldb_rpt_uut_pn[] = "UUT P/N: ";
static char cb_I_Ldb_rpt_uut_pn[] = "Codice UUT: ";
static char cb_P_Ldb_rpt_uut_pn[] = "P/N do UUT: ";

static char cb_E_Ldb_rpt_uut_sn[] = "UUT S/N: ";
static char cb_S_Ldb_rpt_uut_sn[] = "N� de serie UUT: ";
static char cb_F_Ldb_rpt_uut_sn[] = "N/S UUT: ";
static char cb_G_Ldb_rpt_uut_sn[] = "UUT S/N: ";
static char cb_I_Ldb_rpt_uut_sn[] = "S/N UUT: ";
static char cb_P_Ldb_rpt_uut_sn[] = "N/S do UUT: ";

static char cb_E_Ldb_rpt_uut_rev[] = "UUT Rev: ";
static char cb_S_Ldb_rpt_uut_rev[] = "Revisi�n UUT: ";
static char cb_F_Ldb_rpt_uut_rev[] = "R�v. UUT: ";
static char cb_G_Ldb_rpt_uut_rev[] = "UUT Rev: ";
static char cb_I_Ldb_rpt_uut_rev[] = "Rev UUT: ";
static char cb_P_Ldb_rpt_uut_rev[] = "Rev do UUT: ";

static char cb_E_Ldb_rpt_date[] = "Date: ";
static char cb_S_Ldb_rpt_date[] = "Fecha: ";
static char cb_F_Ldb_rpt_date[] = "Date: ";
static char cb_G_Ldb_rpt_date[] = "Datum: ";
static char cb_I_Ldb_rpt_date[] = "Data: ";
static char cb_P_Ldb_rpt_date[] = "Data: ";

static char cb_E_Ldb_rpt_time[] = "\nTime: ";
static char cb_S_Ldb_rpt_time[] = "\nHora: ";
static char cb_F_Ldb_rpt_time[] = "\nTemps: ";
static char cb_G_Ldb_rpt_time[] = "\nZeit: ";
static char cb_I_Ldb_rpt_time[] = "\nOra: ";
static char cb_P_Ldb_rpt_time[] = "\nHora: ";

static char cb_E_Ldb_rpt_test_results[] = "\n\nTest Results: ~~~~~~~~~~~           ";
static char cb_S_Ldb_rpt_test_results[] = "\n\nResultados de prueba: ~~~~~~~~~~~           ";
static char cb_F_Ldb_rpt_test_results[] = "\n\nR�sultats du test: ~~~~~~~~~~~           ";
static char cb_G_Ldb_rpt_test_results[] = "\n\nTestergebnisse: ~~~~~~~~~~~           ";
static char cb_I_Ldb_rpt_test_results[] = "\n\nRisultati test: ~~~~~~~~~~~           ";
static char cb_P_Ldb_rpt_test_results[] = "\n\nResultados do teste: ~~~~~~~~~~~           ";

// WriteStopReason
static char cb_E_Ldb_rpt_tp_aborted[] = "\n\n**** Test Program Aborted ****\n";
static char cb_S_Ldb_rpt_tp_aborted[] = "\n\n**** Programa de prueba anulado ****\n";
static char cb_F_Ldb_rpt_tp_aborted[] = "\n\n**** Programme de test annul� ****\n";
static char cb_G_Ldb_rpt_tp_aborted[] = "\n\n**** Testprogramm abgebrochen ****\n";
static char cb_I_Ldb_rpt_tp_aborted[] = "\n\n**** Programma interrotto ****\n";
static char cb_P_Ldb_rpt_tp_aborted[] = "\n\n**** Programa de teste anulado ****\n";

static char cb_E_Ldb_rpt_rte_stopped[] = "\n\n**** Stopped by run time error or test failure ****\n";
static char cb_S_Ldb_rpt_rte_stopped[] = "\n\n**** Se ha detenido debido a un error en tiempo de ejecuci�n o fallo de prueba ****\n";
static char cb_F_Ldb_rpt_rte_stopped[] = "\n\n**** Arr�t pr erreur d'ex�cution ou �chec du test ****\n";
static char cb_G_Ldb_rpt_rte_stopped[] = "\n\n**** Stop durch Laufzeitfehler oder Testfehler ****\n";
static char cb_I_Ldb_rpt_rte_stopped[] = "\n\n**** Interruzione causata da un errore run time ****\n";
static char cb_P_Ldb_rpt_rte_stopped[] = "\n\n**** Interrompido por erro de execu��o ou falha de teste ****\n";

// WriteTestResult
static char cb_E_Ldb_Error_Caps[] = "ERROR";
static char cb_S_Ldb_Error_Caps[] = "ERROR";
static char cb_F_Ldb_Error_Caps[] = "ERREUR";
static char cb_G_Ldb_Error_Caps[] = "FEHLER";
static char cb_I_Ldb_Error_Caps[] = "ERRORE";
static char cb_P_Ldb_Error_Caps[] = "ERRO";

static char cb_E_Ldb_Skip_Caps[] = "SKIP";
static char cb_S_Ldb_Skip_Caps[] = "OMITIR";
static char cb_F_Ldb_Skip_Caps[] = "SAUT";
static char cb_G_Ldb_Skip_Caps[] = "�BERSPRINGEN";
static char cb_I_Ldb_Skip_Caps[] = "IGNORA";
static char cb_P_Ldb_Skip_Caps[] = "IGNORAR";

static char cb_E_Ldb_None_caps[] = "NONE";
static char cb_S_Ldb_None_caps[] = "NINGUNO";
static char cb_F_Ldb_None_caps[] = "AUCUN";
static char cb_G_Ldb_None_caps[] = "KEINE";
static char cb_I_Ldb_None_caps[] = "NESSUNO";
static char cb_P_Ldb_None_caps[] = "NENHUM";

static char cb_E_Ldb_rpt_measurement[] = "                    Measurement: ";
static char cb_S_Ldb_rpt_measurement[] = "                    Medici�n: ";
static char cb_F_Ldb_rpt_measurement[] = "                    Mesure: ";
static char cb_G_Ldb_rpt_measurement[] = "                    Me�wert: ";
static char cb_I_Ldb_rpt_measurement[] = "                    Misurazione: ";
static char cb_P_Ldb_rpt_measurement[] = "                    Medi��o: ";

static char cb_E_Ldb_Pass_caps[] = "PASS";
static char cb_S_Ldb_Pass_caps[] = "PASO";
static char cb_F_Ldb_Pass_caps[] = "R�USSI";
static char cb_G_Ldb_Pass_caps[] = "OK";
static char cb_I_Ldb_Pass_caps[] = "SUPERATO";
static char cb_P_Ldb_Pass_caps[] = "PASSA";

static char cb_E_Ldb_Fail_caps[] = "FAIL";
static char cb_S_Ldb_Fail_caps[] = "FALLO";
static char cb_F_Ldb_Fail_caps[] = "�CHEC";
static char cb_G_Ldb_Fail_caps[] = "MISSLUNGEN";
static char cb_I_Ldb_Fail_caps[] = "ERRORE";
static char cb_P_Ldb_Fail_caps[] = "FALHA";

static char cb_E_Ldb_rpt_Limit[] = "                    Limit:       ";
static char cb_S_Ldb_rpt_Limit[] = "                    L�mite:       ";
static char cb_F_Ldb_rpt_Limit[] = "                    Limite:       ";
static char cb_G_Ldb_rpt_Limit[] = "                    Grenze:       ";
static char cb_I_Ldb_rpt_Limit[] = "                    Limite:       ";
static char cb_P_Ldb_rpt_Limit[] = "                    Limite:       ";

//static char cb_E_Ldb_rte_unknown[] = "%s[a]<Runtime Error location unknown. Error Code:%i\n";
//static char cb_S_Ldb_rte_unknown[] = "%s[a]<Error en tiempo de ejecuci�n: ubicaci�n desconocida. C�digo de error:%i\n";
//static char cb_F_Ldb_rte_unknown[] = "%s[a]<Emplacement erreur ex�cution inconnu. Code erreur:%i\n";
//static char cb_G_Ldb_rte_unknown[] = "%s[a]<Laufzeitfehler, Stelle unbekannt. Fehlercode:%i\n";
//static char cb_I_Ldb_rte_unknown[] = "%s[a]<Errore di run time percorso sconosciuto. Codice di errore:%i\n";
//static char cb_P_Ldb_rte_unknown[] = "%s[a]<Localiza��o desconhecida do erro de tempo de execu��o. C�digo de erro:%i\n";

//static char cb_E_Ldb_rte_load[] = "%s[a]<Runtime Error in load sequence setup routine. Error Code:%i\n";
//static char cb_S_Ldb_rte_load[] = "%s[a]<Error en tiempo de ejecuci�n en la rutina de configuraci�n de la secuencia de carga. C�digo de error:%i\n";
//static char cb_F_Ldb_rte_load[] = "%s[a]<Erreur ex�cution dans routine de config s�rie chargement. Code erreur:%i\n";
//static char cb_G_Ldb_rte_load[] = "%s[a]<Lauftzeitfehler in Ladesequenz Setup Routine. Fehlercode:%i\n";
//static char cb_I_Ldb_rte_load[] = "%s[a]<Errore di run time nella routine di configurazione caricamento sequenza. Codice di errore:%i\n";
//static char cb_P_Ldb_rte_load[] = "%s[a]<Erro de tempo de execu��o na rotina de configura��o da seq��ncia de carregamento. C�digo de erro:%i\n";

//static char cb_E_Ldb_rte_seq_setup[] = "%s[a]<Runtime Error in sequence setup routine. Error Code:%i\n";
//static char cb_S_Ldb_rte_seq_setup[] = "%s[a]<Error en tiempo de ejecuci�n en la rutina de configuraci�n de la secuencia. C�digo de error:%i\n";
//static char cb_F_Ldb_rte_seq_setup[] = "%s[a]<Erreur ex�cution dans routine de config s�rie. Code erreur:%i\n";
//static char cb_G_Ldb_rte_seq_setup[] = "%s[a]<Lauftzeitfehler in Sequenz Setup Routine. Fehlercode:%i\n";
//static char cb_I_Ldb_rte_seq_setup[] = "%s[a]<Errore di run time nella routine di configurazione sequenza. Codice di errore:%i\n";
//static char cb_P_Ldb_rte_seq_setup[] = "%s[a]<Erro de tempo de execu��o na rotina de configura��o de seq��ncia. C�digo de erro:%i\n";

//static char cb_E_Ldb_rte_setup[] = "%s[a]<Runtime Error in setup routine. Error Code:%i\n";
//static char cb_S_Ldb_rte_setup[] = "%s[a]<Error en tiempo de ejecuci�n en la rutina de configuraci�n. C�digo de error:%i\n";
//static char cb_F_Ldb_rte_setup[] = "%s[a]<Erreur ex�cution dans routine de config. Code erreur:%i\n";
//static char cb_G_Ldb_rte_setup[] = "%s[a]<Lauftzeitfehler in Setup Routine. Fehlercode:%i\n";
//static char cb_I_Ldb_rte_setup[] = "%s[a]<Errore di run time nella routine di configurazione. Codice di errore:%i\n";
//static char cb_P_Ldb_rte_setup[] = "%s[a]<Erro de tempo de execu��o na rotina de configura��o. C�digo de erro:%i\n";

//static char cb_E_Ldb_rte_test_err[] = "%s[a]<Runtime Error in test. Error Code:%i\n";
//static char cb_S_Ldb_rte_test_err[] = "%s[a]<Error en tiempo de ejecuci�n en la prueba. C�digo de error:%i\n";
//static char cb_F_Ldb_rte_test_err[] = "%s[a]<Erreur ex�cution dans test. Code erreur:%i\n";
//static char cb_G_Ldb_rte_test_err[] = "%s[a]<Laufzeitfehler bei Test. Fehlercode:%i\n";
//static char cb_I_Ldb_rte_test_err[] = "%s[a]<Errore di run time nel test. Codice di errore:%i\n";
//static char cb_P_Ldb_rte_test_err[] = "%s[a]<Erro de tempo de execu��o no teste. C�digo de erro:%i\n";

//static char cb_E_Ldb_rte_cleanup[] = "%s[a]<Runtime Error in cleanup routine. Error Code:%i\n";
//static char cb_S_Ldb_rte_cleanup[] = "%s[a]<Error en tiempo de ejecuci�n en la rutina de limpieza. C�digo de error:%i\n";
//static char cb_F_Ldb_rte_cleanup[] = "%s[a]<Erreur ex�cution dans routine nettoyage. Code erreur:%i\n";
//static char cb_G_Ldb_rte_cleanup[] = "%s[a]<Lauftzeitfehler in Cleanup Routine. Fehlercode:%i\n";
//static char cb_I_Ldb_rte_cleanup[] = "%s[a]<Errore di run time nella routine di pulitura. Codice di errore:%i\n";
//static char cb_P_Ldb_rte_cleanup[] = "%s[a]<Erro de tempo de execu��o na rotina de limpeza. C�digo de erro:%i\n";

//static char cb_E_Ldb_rte_seq_cleanup[] = "%s[a]<Runtime Error in sequence cleanup routine. Error Code:%i\n";
//static char cb_S_Ldb_rte_seq_cleanup[] = "%s[a]<Error en tiempo de ejecuci�n en la rutina de limpieza de la secuencia. C�digo de error:%i\n";
//static char cb_F_Ldb_rte_seq_cleanup[] = "%s[a]<Erreur ex�cution dans routine nettoyage s�rie. Code erreur:%i\n";
//static char cb_G_Ldb_rte_seq_cleanup[] = "%s[a]<Lauftzeitfehler in Sequenz Cleanup Routine. Fehlercode:%i\n";
//static char cb_I_Ldb_rte_seq_cleanup[] = "%s[a]<Errore di run time nella routine di pulitura sequenza. Codice di errore:%i\n";
//static char cb_P_Ldb_rte_seq_cleanup[] = "%s[a]<Erro de tempo de execu��o na rotina de limpeza de seq��ncia. C�digo de erro:%i\n";

//static char cb_E_Ldb_rte_unload_seq[] = "%s[a]<Runtime Error in unload sequence cleanup routine. Error Code:%i\n";
//static char cb_S_Ldb_rte_unload_seq[] = "%s[a]<Error en tiempo de ejecuci�n en la rutina de limpieza de la secuencia de descarga. C�digo de error:%i\n";
//static char cb_F_Ldb_rte_unload_seq[] = "%s[a]<Erreur ex�cution dans routine nettoyage s�rie d�chargement. Code erreur:%i\n";
//static char cb_G_Ldb_rte_unload_seq[] = "%s[a]<Lauftzeitfehler in Entladesequenz Cleanup Routine. Fehlercode:%i\n";
//static char cb_I_Ldb_rte_unload_seq[] = "%s[a]<Errore di run time nella routine di pulitura sequenza di scaricamento. Codice di errore:%i\n";
//static char cb_P_Ldb_rte_unload_seq[] = "%s[a]<Erro de tempo de execu��o na rotina de limpeza da seq��ncia de descarregamento. C�digo de erro:%i\n";

//static char cb_E_Ldb_rte_error_msg[] = "Error Message:";
//static char cb_S_Ldb_rte_error_msg[] = "Mensaje de error:";
//static char cb_F_Ldb_rte_error_msg[] = "Message d'erreur:";
//static char cb_G_Ldb_rte_error_msg[] = "Fehlermeldung:";
//static char cb_I_Ldb_rte_error_msg[] = "Messaggio di errore:";
//static char cb_P_Ldb_rte_error_msg[] = "Mensagem de erro:";

// BeginReportString
static char cb_E_Ldb_MSG_OUT_OF_MEMORY[] = "Out of memory";
static char cb_S_Ldb_MSG_OUT_OF_MEMORY[] = "Memoria insuficiente";
static char cb_F_Ldb_MSG_OUT_OF_MEMORY[] = "M�moire insuf";
static char cb_G_Ldb_MSG_OUT_OF_MEMORY[] = "Zu wenig Speicher";
static char cb_I_Ldb_MSG_OUT_OF_MEMORY[] = "Memoria insufficiente";
static char cb_P_Ldb_MSG_OUT_OF_MEMORY[] = "Mem�ria insuficiente ";

static char cb_E_Ldb_MSG_RPT_NO_MEM[] = "Could not allocate memory for test report";
static char cb_S_Ldb_MSG_RPT_NO_MEM[] = "No se puede asignar memoria para el informe de prueba";
static char cb_F_Ldb_MSG_RPT_NO_MEM[] = "Impossible d'allouer m�moire pour rapport de test";
static char cb_G_Ldb_MSG_RPT_NO_MEM[] = "Kein Speicher f�r Testbericht";
static char cb_I_Ldb_MSG_RPT_NO_MEM[] = "Impossibile allocare memoria per il report dei test";
static char cb_P_Ldb_MSG_RPT_NO_MEM[] = "N�o foi poss�vel alocar mem�ria para relat�rio de teste";

// EndReportString

// PrintTestReport
static char cb_E_Ldb_rpt_print_to_file[] = "Print test report to file";
static char cb_S_Ldb_rpt_print_to_file[] = "Imprimir informe de prueba en el archivo";
static char cb_F_Ldb_rpt_print_to_file[] = "Impr. rapport de test ds fichier";
static char cb_G_Ldb_rpt_print_to_file[] = "Testbericht in Datei drucken";
static char cb_I_Ldb_rpt_print_to_file[] = "Stampa su file report test";
static char cb_P_Ldb_rpt_print_to_file[] = "Imprimir relat�rio de teste para arquivo";

static char cb_E_Ldb_rpt_enter_fname[] = "Enter filename";
static char cb_S_Ldb_rpt_enter_fname[] = "Escribir nombre de archivo";
static char cb_F_Ldb_rpt_enter_fname[] = "Entrer nom du fichier";
static char cb_G_Ldb_rpt_enter_fname[] = "Dateinamen eingeben";
static char cb_I_Ldb_rpt_enter_fname[] = "Immettere nome file";
static char cb_P_Ldb_rpt_enter_fname[] = "Inserir nome de arquivo";

static char cb_E_Ldb_rpt_invalid_fname[] = "Invalid filename";
static char cb_S_Ldb_rpt_invalid_fname[] = "Nombre de archivo no v�lido";
static char cb_F_Ldb_rpt_invalid_fname[] = "Nom de fichier incorrect";
static char cb_G_Ldb_rpt_invalid_fname[] = "Ung�ltiger Dateiname";
static char cb_I_Ldb_rpt_invalid_fname[] = "Nome file non valido";
static char cb_P_Ldb_rpt_invalid_fname[] = "Nome de arquivo inv�lido";

static char cb_E_Ldb_rpt_why_fname_inv[] = "Filename must have no extension";
static char cb_S_Ldb_rpt_why_fname_inv[] = "El nombre de archivo no debe tener ninguna extensi�n";
static char cb_F_Ldb_rpt_why_fname_inv[] = "Nom du fichier doit �tre sans extension";
static char cb_G_Ldb_rpt_why_fname_inv[] = "Dateiname darf keine Erweiterung haben";
static char cb_I_Ldb_rpt_why_fname_inv[] = "I nomi di file non devono avere estensioni";
static char cb_P_Ldb_rpt_why_fname_inv[] = "Nome de arquivo n�o deve ter extens�o";

// IncreaseTestReport (testreport)
static char cb_E_Ldb_MSG_RPT_NO_MORE_MEM[] = "Could not allocate more memory for test report";
static char cb_S_Ldb_MSG_RPT_NO_MORE_MEM[] = "No se puede asignar m�s memoria para el informe de prueba";
static char cb_F_Ldb_MSG_RPT_NO_MORE_MEM[] = "Impossible d'allouer plus de m�moire pour rapport de test";
static char cb_G_Ldb_MSG_RPT_NO_MORE_MEM[] = "Nicht genug Speicher f�r Testbericht";
static char cb_I_Ldb_MSG_RPT_NO_MORE_MEM[] = "Impossibile allocare ulteriore memoria per il report dei test";
static char cb_P_Ldb_MSG_RPT_NO_MORE_MEM[] = "N�o foi poss�vel alocar mais mem�ria para relat�rio de teste";

// tooltips
static char cb_E_Ldb_mantest_tt1[] = "< 0.1 Ohm";
static char cb_E_Ldb_mantest_tt2[] = "<=300 �A (100-120 V ac), <=500 �A (220-240 V ac)";
static char cb_E_Ldb_mantest_tt3[] = "<=300 �A (100-120 V ac), <=500 �A (220-240 V ac)";
static char cb_E_Ldb_mantest_tt4[] = "> 1 Ohm";
static char cb_E_Ldb_mantest_tt5[] = "< 100 mV";
static char cb_E_Ldb_mantest_tt6[] = "< 100 mV";
static char cb_E_Ldb_mantest_tt7[] = "9 to 12 psi";
static char cb_E_Ldb_mantest_tt8[] = "9 to 12 psi";

static char cb_E_Ldb_tt1[] = "840 Ventilator Functions screen [F6]";
static char cb_E_Ldb_tt2[] = "Report [F4]";
static char cb_E_Ldb_tt3[] = "Log out [F10]";
static char cb_E_Ldb_tt4[] = "Test screen [F3]";
static char cb_E_Ldb_tt5[] = "Help [F1]";
static char cb_E_Ldb_tt6[] = "PTS 2000 Functions screen [F5]";
static char cb_E_Ldb_tt7[] = "Abort testing. [Ctrl-F5]";
static char cb_E_Ldb_tt8[] = "Setup Functions screen [F2]";
static char cb_E_Ldb_tt9[] = "Click here and choose a username";
static char cb_E_Ldb_tt10[] = "Quit";
static char cb_E_Ldb_tt11[] = "Run all tests [Ctrl-F1]";
static char cb_E_Ldb_tt12[] = "Run only the selected test [Ctrl-F2]";
static char cb_E_Ldb_tt13[] = "Run selected test multiple times [Ctrl-F3]";
static char cb_E_Ldb_tt14[] = "Pause testing [Ctrl-F4]";
static char cb_E_Ldb_tt15[] = "Log in";
static char cb_E_Ldb_tt16[] = "Connect to PTS 2000.";
static char cb_E_Ldb_tt17[] = "Connect to 840 Ventilator.";

static char cb_S_Ldb_mantest_tt1[] = "< 0,1 Ohm";
static char cb_S_Ldb_mantest_tt2[] = "<=300 �A (100-120 V CA), <=500 �A (220-240 V CA)";
static char cb_S_Ldb_mantest_tt3[] = "<=300 �A (100-120 V CA), <=500 �A (220-240 V CA)";
static char cb_S_Ldb_mantest_tt4[] = "> 1 Ohm";
static char cb_S_Ldb_mantest_tt5[] = "< 100 mV";
static char cb_S_Ldb_mantest_tt6[] = "< 100 mV";
static char cb_S_Ldb_mantest_tt7[] = "9 a 12 psi";
static char cb_S_Ldb_mantest_tt8[] = "9 a 12 psi";

static char cb_S_Ldb_tt1[] = "Pantalla Funciones del ventilador 840 [F6]";
static char cb_S_Ldb_tt2[] = "Informe [F4]";
static char cb_S_Ldb_tt3[] = "Cerrar sesi�n [F10]";
static char cb_S_Ldb_tt4[] = "Pantalla Prueba [F3]";
static char cb_S_Ldb_tt5[] = "Ayuda [F1]";
static char cb_S_Ldb_tt6[] = "Pantalla Funciones PTS 2000 [F5]";
static char cb_S_Ldb_tt7[] = "Anular prueba. [Ctrl-F5]";
static char cb_S_Ldb_tt8[] = "Pantalla Configurar funciones [F2]";
static char cb_S_Ldb_tt9[] = "Haga clic aqu� y elija un nombre de usuario";
static char cb_S_Ldb_tt10[] = "Salir";
static char cb_S_Ldb_tt11[] = "Ejecutar todas las pruebas [Ctrl-F1]";
static char cb_S_Ldb_tt12[] = "Ejecutar s�lo la prueba seleccionada [Ctrl-F2]";
static char cb_S_Ldb_tt13[] = "Ejecutar la prueba seleccionada varias veces [Ctrl-F3]";
static char cb_S_Ldb_tt14[] = "Detener prueba [Ctrl-F4]";
static char cb_S_Ldb_tt15[] = "Iniciar sesi�n";
static char cb_S_Ldb_tt16[] = "Conectar a PTS 2000.";
static char cb_S_Ldb_tt17[] = "Conectar al ventilador 840.";

static char cb_F_Ldb_mantest_tt1[] = "< 0,1 Ohm";
static char cb_F_Ldb_mantest_tt2[] = "<=300 �A (100-120 V ca), <=500 �A (220-240 V ca)";
static char cb_F_Ldb_mantest_tt3[] = "<=300 �A (100-120 V ca), <=500 �A (220-240 V ca)";
static char cb_F_Ldb_mantest_tt4[] = "> 1 Ohm";
static char cb_F_Ldb_mantest_tt5[] = "< 100 mV";
static char cb_F_Ldb_mantest_tt6[] = "< 100 mV";
static char cb_F_Ldb_mantest_tt7[] = "9 � 12 psi";
static char cb_F_Ldb_mantest_tt8[] = "9 � 12 psi";

static char cb_F_Ldb_tt1[] = "�cran Fonctions du ventilateur 840 [F6]";
static char cb_F_Ldb_tt2[] = "Rapport [F4]";
static char cb_F_Ldb_tt3[] = "Fin session [F10]";
static char cb_F_Ldb_tt4[] = "�cran Tests [F3]";
static char cb_F_Ldb_tt5[] = "Aide [F1]";
static char cb_F_Ldb_tt6[] = "�cran Fonctions du PTS 2000 [F5]";
static char cb_F_Ldb_tt7[] = "Annuler test. [Ctrl-F5]";
static char cb_F_Ldb_tt8[] = "�cran Fonctions de configuration [F2]";
static char cb_F_Ldb_tt9[] = "Cliquer ici et choisir nom utilisateur";
static char cb_F_Ldb_tt10[] = "Quitt";
static char cb_F_Ldb_tt11[] = "Ex�cuter tous tests [Ctrl-F1]";
static char cb_F_Ldb_tt12[] = "Ex�cuter seult test s�lectionn� [Ctrl-F2]";
static char cb_F_Ldb_tt13[] = "Ex�cuter plusieurs fois le test s�lectionn� [Ctrl-F3]";
static char cb_F_Ldb_tt14[] = "Suspendre test [Ctrl-F4]";
static char cb_F_Ldb_tt15[] = "Ouv session";
static char cb_F_Ldb_tt16[] = "Connecter au PTS 2000.";
static char cb_F_Ldb_tt17[] = "Connecter au ventilateur 840.";

static char cb_G_Ldb_mantest_tt1[] = "< 0,1 Ohm";
static char cb_G_Ldb_mantest_tt2[] = "<=300 �A (100-120 V AC), <=500 �A (220-240 V AC)";
static char cb_G_Ldb_mantest_tt3[] = "<=300 �A (100-120 V AC), <=500 �A (220-240 V AC)";
static char cb_G_Ldb_mantest_tt4[] = "> 1 Ohm";
static char cb_G_Ldb_mantest_tt5[] = "< 100 mV";
static char cb_G_Ldb_mantest_tt6[] = "< 100 mV";
static char cb_G_Ldb_mantest_tt7[] = "9 bis 12 psi";
static char cb_G_Ldb_mantest_tt8[] = "9 bis 12 psi";

static char cb_G_Ldb_tt1[] = "Beatmungsger�t 840 Funktionsbildschirm [F6]";
static char cb_G_Ldb_tt2[] = "Bericht [F4]";
static char cb_G_Ldb_tt3[] = "Abmelden [F10]";
static char cb_G_Ldb_tt4[] = "Testbildschirm [F3]";
static char cb_G_Ldb_tt5[] = "Hilfe [F1]";
static char cb_G_Ldb_tt6[] = "PTS 2000 Funktionsbildschirm [F5]";
static char cb_G_Ldb_tt7[] = "Test abbrechen. [Strg-F5]";
static char cb_G_Ldb_tt8[] = "Bildschirm Funktionen konfigurieren [F2]";
static char cb_G_Ldb_tt9[] = "Hier klicken und Benutzernamen ausw�hlen";
static char cb_G_Ldb_tt10[] = "Beenden";
static char cb_G_Ldb_tt11[] = "Alle Tests durchf�hren [Strg-F1]";
static char cb_G_Ldb_tt12[] = "Nur ausgew�hlten Test durchf�hren [Strg-F2]";
static char cb_G_Ldb_tt13[] = "Ausgew�hlten Test mehrmals durchf�hren [Strg-F3]";
static char cb_G_Ldb_tt14[] = "Test Pause [Strg-F4]";
static char cb_G_Ldb_tt15[] = "Anmelden";
static char cb_G_Ldb_tt16[] = "PTS 2000 anschlie�en.";
static char cb_G_Ldb_tt17[] = "Beatmungsger�t 840 anschlie�en.";

static char cb_I_Ldb_mantest_tt1[] = "< 0,1 Ohm";
static char cb_I_Ldb_mantest_tt2[] = "<=300 �A (100-120 V CA), <=500 �A (220-240 V CA)";
static char cb_I_Ldb_mantest_tt3[] = "<=300 �A (100-120 V CA), <=500 �A (220-240 V CA)";
static char cb_I_Ldb_mantest_tt4[] = "> 1 Ohm";
static char cb_I_Ldb_mantest_tt5[] = "< 100 mV";
static char cb_I_Ldb_mantest_tt6[] = "< 100 mV";
static char cb_I_Ldb_mantest_tt7[] = "9-12 psi";
static char cb_I_Ldb_mantest_tt8[] = "9-12 psi";

static char cb_I_Ldb_tt1[] = "Funzioni del ventilatore 840 [F6]";
static char cb_I_Ldb_tt2[] = "Report [F4]";
static char cb_I_Ldb_tt3[] = "Scollega [F10]";
static char cb_I_Ldb_tt4[] = "Schermata dei test [F3]";
static char cb_I_Ldb_tt5[] = "Guida [F1]";
static char cb_I_Ldb_tt6[] = "Funzioni del PTS 2000 [F5]";
static char cb_I_Ldb_tt7[] = "Interrompere test. [Ctrl-F5]";
static char cb_I_Ldb_tt8[] = "Funzioni di impostazione [F2]";
static char cb_I_Ldb_tt9[] = "Fare clic qui e scegliere un nome utente";
static char cb_I_Ldb_tt10[] = "Esci";
static char cb_I_Ldb_tt11[] = "Esegui tutti i test [Ctrl-F1]";
static char cb_I_Ldb_tt12[] = "Esegui solo test selezionato [Ctrl-F2]";
static char cb_I_Ldb_tt13[] = "Esegui il test selezionato pi� volte [Ctrl-F3]";
static char cb_I_Ldb_tt14[] = "Pausa test [Ctrl-F4]";
static char cb_I_Ldb_tt15[] = "Accesso";
static char cb_I_Ldb_tt16[] = "Collegare al PTS 2000.";
static char cb_I_Ldb_tt17[] = "Collegare al ventilatore 840.";

static char cb_P_Ldb_mantest_tt1[] = "< 0.1 Ohm";
static char cb_P_Ldb_mantest_tt2[] = "<=300 �A (100-120 V ca), <=500 �A (220-240 V ca)";
static char cb_P_Ldb_mantest_tt3[] = "<=300 �A (100-120 V ca), <=500 �A (220-240 V ca)";
static char cb_P_Ldb_mantest_tt4[] = "> 1 Ohm";
static char cb_P_Ldb_mantest_tt5[] = "< 100 mV";
static char cb_P_Ldb_mantest_tt6[] = "< 100 mV";
static char cb_P_Ldb_mantest_tt7[] = "9 a 12 psi";
static char cb_P_Ldb_mantest_tt8[] = "9 a 12 psi";

static char cb_P_Ldb_tt1[] = "Tela de fun��es do Ventilador 840 [F6]";
static char cb_P_Ldb_tt2[] = "Relat�rio [F4]";
static char cb_P_Ldb_tt3[] = "Logout [F10]";
static char cb_P_Ldb_tt4[] = "Tela de teste [F3]";
static char cb_P_Ldb_tt5[] = "Ajuda [F1]";
static char cb_P_Ldb_tt6[] = "Tela de fun��es do PTS 2000 [F5]";
static char cb_P_Ldb_tt7[] = "Anular teste. [Ctrl-F5]";
static char cb_P_Ldb_tt8[] = "Tela de fun��es de configura��o [F2]";
static char cb_P_Ldb_tt9[] = "Clique aqui e escolha um nome de usu�rio";
static char cb_P_Ldb_tt10[] = "Sair";
static char cb_P_Ldb_tt11[] = "Executar todos os testes [Ctrl-F1]";
static char cb_P_Ldb_tt12[] = "Executar apenas teste selecionado [Ctrl-F2]";
static char cb_P_Ldb_tt13[] = "Executar teste selecionado v�rias vezes [Ctrl-F3]";
static char cb_P_Ldb_tt14[] = "Pausar teste [Ctrl-F4]";
static char cb_P_Ldb_tt15[] = "Login";
static char cb_P_Ldb_tt16[] = "Conectar ao PTS 2000.";
static char cb_P_Ldb_tt17[] = "Conectar ao Ventilador 840.";

// Vtest.c
static char cb_E_Ldb_Engine_Error[] = "Engine error";
static char cb_S_Ldb_Engine_Error[] = "Error mec�nico";
static char cb_F_Ldb_Engine_Error[] = "Erreur moteur";
static char cb_G_Ldb_Engine_Error[] = "Ger�tefehler";
static char cb_I_Ldb_Engine_Error[] = "Errore motore";
static char cb_P_Ldb_Engine_Error[] = "Erro de mecanismo";

static char cb_E_Ldb_No_Tests[] = "No tests";
static char cb_S_Ldb_No_Tests[] = "Ninguna prueba";
static char cb_F_Ldb_No_Tests[] = "Aucun test";
static char cb_G_Ldb_No_Tests[] = "Keine Tests";
static char cb_I_Ldb_No_Tests[] = "Nessun test";
static char cb_P_Ldb_No_Tests[] = "Sem testes";

//static char cb_E_Ldb_List_not_Empty[] = "List not empty";
//static char cb_S_Ldb_List_not_Empty[] = "La lista no est� vac�a";
//static char cb_F_Ldb_List_not_Empty[] = "Liste non vide";
//static char cb_G_Ldb_List_not_Empty[] = "Liste nicht leer";
//static char cb_I_Ldb_List_not_Empty[] = "Elenco non vuoto";
//static char cb_P_Ldb_List_not_Empty[] = "A lista n�o est� vazia";

//static char cb_E_Ldb_No_More_Tests[] = "No more tests";
//static char cb_S_Ldb_No_More_Tests[] = "Ninguna prueba m�s";
//static char cb_F_Ldb_No_More_Tests[] = "Plus de test";
//static char cb_G_Ldb_No_More_Tests[] = "Keine weiteren Tests";
//static char cb_I_Ldb_No_More_Tests[] = "Nessun altro test";
//static char cb_P_Ldb_No_More_Tests[] = "Sem mais testes";

//static char cb_E_Ldb_PP_Test_Missing[] = "Pre or post test missing, name unknown.";
//static char cb_S_Ldb_PP_Test_Missing[] = "Falta la prueba previa o posterior, nombre desconocido.";
//static char cb_F_Ldb_PP_Test_Missing[] = "Pr�test ou posttest manquant, nom inconnu.";
//static char cb_G_Ldb_PP_Test_Missing[] = "Vor- oder Nachtest fehlt, Name unbekannt.";
//static char cb_I_Ldb_PP_Test_Missing[] = "Impossibile trovare pre o post test. Nome sconosciuto.";
//static char cb_P_Ldb_PP_Test_Missing[] = "Pr�-teste ou p�s faltando, nome desconhecido.";

//static char cb_E_Ldb_PP_Test_Exists[] = "Pre or post test already exists";
//static char cb_S_Ldb_PP_Test_Exists[] = "Ya existe una prueba previa o posterior";
//static char cb_F_Ldb_PP_Test_Exists[] = "Pr�test ou posttest existe d�j�";
//static char cb_G_Ldb_PP_Test_Exists[] = "Vor- oder Nachtest schon vorhanden";
//static char cb_I_Ldb_PP_Test_Exists[] = "Pre o post test gi� esistente";
//static char cb_P_Ldb_PP_Test_Exists[] = "Pr�-teste ou j� existe";

//static char cb_E_Ldb_Test_Not_Verif[] = "Test %s (from %s) not verified";
//static char cb_S_Ldb_Test_Not_Verif[] = "Prueba %s (de %s) sin verificar";
//static char cb_F_Ldb_Test_Not_Verif[] = "Test %s (de %s) non v�rifi�";
//static char cb_G_Ldb_Test_Not_Verif[] = "Test %s (von %s) nicht �berpr�ft";
//static char cb_I_Ldb_Test_Not_Verif[] = "Test %s (da %s) non verificato";
//static char cb_P_Ldb_Test_Not_Verif[] = "Teste %s (de %s) n�o verificado";

//static char cb_E_Ldb_preTest_Not_Verif[] = "Pre test %s (from %s) not verified";
//static char cb_S_Ldb_preTest_Not_Verif[] = "Prueba previa %s (de %s) sin verificar";
//static char cb_F_Ldb_preTest_Not_Verif[] = "Pr�test %s (de %s) pas v�rifi�";
//static char cb_G_Ldb_preTest_Not_Verif[] = "Vortest %s (von %s) nicht �berpr�ft";
//static char cb_I_Ldb_preTest_Not_Verif[] = "Pre test %s (da %s) non verificato";
//static char cb_P_Ldb_preTest_Not_Verif[] = "Teste pr� %s (de %s) n�o verificado";

//static char cb_E_Ldb_postTest_Not_Verif[] = "Post test %s (from %s) not verified";
//static char cb_S_Ldb_postTest_Not_Verif[] = "Prueba posterior %s (de %s) sin verificar";
//static char cb_F_Ldb_postTest_Not_Verif[] = "Posttest %s (de %s) pas v�rifi�";
//static char cb_G_Ldb_postTest_Not_Verif[] = "Nachtest %s (von %s) nicht �berpr�ft";
//static char cb_I_Ldb_postTest_Not_Verif[] = "Post test %s (da %s) non verificato";
//static char cb_P_Ldb_postTest_Not_Verif[] = "P�s-teste %s (de %s) n�o verificado";

//static char cb_E_Ldb_Runtime_Err_Test[] = "Run time error in test %s (in file %s)";
//static char cb_S_Ldb_Runtime_Err_Test[] = "Error en tiempo de ejecuci�n en la prueba %s (en el archivo %s)";
//static char cb_F_Ldb_Runtime_Err_Test[] = "Erreur d'ex�cution du test %s (ds fichier %s)";
//static char cb_G_Ldb_Runtime_Err_Test[] = "Laufzeitfehler in Test %s (in Datei %s)";
//static char cb_I_Ldb_Runtime_Err_Test[] = "Errore di run time nel test %s (nel file %s)";
//static char cb_P_Ldb_Runtime_Err_Test[] = "Erro de execu��o no teste %s (no arquivo %s)";

//static char cb_E_Ldb_Runtime_Err_preTest[] = "Run time error in pre test %s (in file %s)";
//static char cb_S_Ldb_Runtime_Err_preTest[] = "Error en tiempo de ejecuci�n en la prueba previa %s (en el archivo %s)";
//static char cb_F_Ldb_Runtime_Err_preTest[] = "Erreur d'ex�cution du pr�test %s (ds fichier %s)";
//static char cb_G_Ldb_Runtime_Err_preTest[] = "Laufzeitfehler in Vortest %s (in Datei %s)";
//static char cb_I_Ldb_Runtime_Err_preTest[] = "Errore di run time nel pre test %s (nel file %s)";
//static char cb_P_Ldb_Runtime_Err_preTest[] = "Erro de execu��o no pr�-teste %s (no arquivo %s)";

//static char cb_E_Ldb_Runtime_Err_postTest[] = "Run time error in post test %s (in file %s)";
//static char cb_S_Ldb_Runtime_Err_postTest[] = "Error en tiempo de ejecuci�n en la prueba posterior %s (en el archivo %s)";
//static char cb_F_Ldb_Runtime_Err_postTest[] = "Erreur d'ex�cution du posttest %s (ds fichier %s)";
//static char cb_G_Ldb_Runtime_Err_postTest[] = "Laufzeitfehler in Nachtest %s (in Datei %s)";
//static char cb_I_Ldb_Runtime_Err_postTest[] = "Errore di run time nel post test %s (nel file %s)";
//static char cb_P_Ldb_Runtime_Err_postTest[] = "Erro de execu��o no p�s-teste %s (no arquivo %s)";

//static char cb_E_Ldb_Runtime_Err_seq_pre_test[] = "Run time error in sequence pre test %s (in file %s)";
//static char cb_S_Ldb_Runtime_Err_seq_pre_test[] = "Error en tiempo de ejecuci�n en la prueba previa de la secuencia %s (en el archivo %s)";
//static char cb_F_Ldb_Runtime_Err_seq_pre_test[] = "Erreur d'ex�cution du pr�test s�quence %s (ds fichier %s)";
//static char cb_G_Ldb_Runtime_Err_seq_pre_test[] = "Laufzeitfehler in Sequenz Vortest %s (in Datei %s)";
//static char cb_I_Ldb_Runtime_Err_seq_pre_test[] = "Errore di run time nel pre test %s (nel file %s)";
//static char cb_P_Ldb_Runtime_Err_seq_pre_test[] = "Erro de execu��o no pr�-teste da seq��ncia %s (no arquivo %s)";

//static char cb_E_Ldb_Runtime_Err_seq_post_test[] = "Run time error in sequence post test %s (in file %s)";
//static char cb_S_Ldb_Runtime_Err_seq_post_test[] = "Error en tiempo de ejecuci�n en la prueba posterior de la secuencia %s (en el archivo %s)";
//static char cb_F_Ldb_Runtime_Err_seq_post_test[] = "Erreur d'ex�cution du posttest s�quence %s (ds fichier %s)";
//static char cb_G_Ldb_Runtime_Err_seq_post_test[] = "Laufzeitfehler in Sequenz Nachtest %s (in Datei %s)";
//static char cb_I_Ldb_Runtime_Err_seq_post_test[] = "Errore di run time nel post test %s (nel file %s)";
//static char cb_P_Ldb_Runtime_Err_seq_post_test[] = "Erro de execu��o no p�s-teste da seq��ncia %s (no arquivo %s)";

//static char cb_E_Ldb_Runtime_Err_loadseq_pre_test[] = "Run time error in load-sequence pre test %s (in file %s)";
//static char cb_S_Ldb_Runtime_Err_loadseq_pre_test[] = "Error en tiempo de ejecuci�n en la prueba previa %s a la secuencia de carga (en el archivo %s)";
//static char cb_F_Ldb_Runtime_Err_loadseq_pre_test[] = "Erreur d'ex�cution du pr�test chargt  s�quence %s (ds fichier %s)";
//static char cb_G_Ldb_Runtime_Err_loadseq_pre_test[] = "Laufzeitfehler in Ladesequenz Vortest %s (in Datei %s)";
//static char cb_I_Ldb_Runtime_Err_loadseq_pre_test[] = "Errore di run time nel pre test %s (nel file %s)";
//static char cb_P_Ldb_Runtime_Err_loadseq_pre_test[] = "Erro de execu��o no carregamento-seq��ncia do pr�-teste %s (no arquivo %s)";

//static char cb_E_Ldb_Runtime_Err_unlaodseq_post_test[] = "Run time error in unload-sequence post test %s (in file %s)";
//static char cb_S_Ldb_Runtime_Err_unlaodseq_post_test[] = "Error en tiempo de ejecuci�n en la prueba posterior de la secuencia de descarga %s (en el archivo %s)";
//static char cb_F_Ldb_Runtime_Err_unlaodseq_post_test[] = "Erreur d'ex�cution du posttest d�chargt s�quence %s (ds fichier %s)";
//static char cb_G_Ldb_Runtime_Err_unlaodseq_post_test[] = "Laufzeitfehler in Entladesequenz Nachtest %s (in Datei %s)";
//static char cb_I_Ldb_Runtime_Err_unlaodseq_post_test[] = "Errore di run time nel post test %s (nel file %s)";
//static char cb_P_Ldb_Runtime_Err_unlaodseq_post_test[] = "Erro de execu��o no p�s-teste de descarregamento-seq��ncia %s (no arquivo %s)";

//static char cb_E_Ldb_Unable_to_Verif_test[] = "Unable to verify test %s in file %s\n(LoadExternalModule error: %s)";
//static char cb_S_Ldb_Unable_to_Verif_test[] = "No se puede verificar la prueba %s en el archivo %s\n(error LoadExternalModule: %s)";
//static char cb_F_Ldb_Unable_to_Verif_test[] = "Impossible de v�rifier test %s ds fichier %s\n(erreur LoadExternalModule: %s)";
//static char cb_G_Ldb_Unable_to_Verif_test[] = "Kann Test %s in Datei %s nicht �berpr�fen.\n(LoadExternalModule Fehler: %s)";
//static char cb_I_Ldb_Unable_to_Verif_test[] = "Impossibile verificare il test %s nel file %s\n(Errore LoadExternalModule: %s)";
//static char cb_P_Ldb_Unable_to_Verif_test[] = "N�o � poss�vel verificar teste %s no arquivo %s\n(Erro de LoadExternalModule: %s)";

//static char cb_E_Ldb_Unable_to_Verif_pretest[] = "Unable to verify pre test %s in file %s\n(LoadExternalModule error: %s)";
//static char cb_S_Ldb_Unable_to_Verif_pretest[] = "No se puede verificar la prueba previa %s en el archivo %s\n(error LoadExternalModule: %s)";
//static char cb_F_Ldb_Unable_to_Verif_pretest[] = "Impossible de v�rifier pr�test %s ds fichier %s\n(erreur LoadExternalModule: %s)";
//static char cb_G_Ldb_Unable_to_Verif_pretest[] = "Kann Vortest %s in Datei %s nicht �berpr�fen.\n(LoadExternalModule Fehler: %s)";
//static char cb_I_Ldb_Unable_to_Verif_pretest[] = "Impossibile verificare pre test %s nel file %s\n(Errore LoadExternalModule: %s)";
//static char cb_P_Ldb_Unable_to_Verif_pretest[] = "N�o � poss�vel verificar pr�-teste %s no arquivo %s\n(Erro de LoadExternalModule: %s)";

//static char cb_E_Ldb_Unable_to_Verif_posttest[] = "Unable to verify post test %s in file %s\n(LoadExternalModule error: %s)";
//static char cb_S_Ldb_Unable_to_Verif_posttest[] = "No se puede verificar la prueba posterior %s en el archivo %s\n(error LoadExternalModule: %s)";
//static char cb_F_Ldb_Unable_to_Verif_posttest[] = "Impossible de v�rifier posttest %s ds fichier %s\n(erreur LoadExternalModule: %s)";
//static char cb_G_Ldb_Unable_to_Verif_posttest[] = "Kann Nachtest %s in Datei %s nicht �berpr�fen.\n(LoadExternalModule Fehler: %s)";
//static char cb_I_Ldb_Unable_to_Verif_posttest[] = "Impossibile verificare post test %s nel file %s\n(Errore LoadExternalModule: %s)";
//static char cb_P_Ldb_Unable_to_Verif_posttest[] = "N�o � poss�vel verificar p�s-teste %s no arquivo %s\n(Erro de LoadExternalModule: %s)";

//static char cb_E_Ldb_Unable_to_Verif_test_2[] = "Unable to verify test %s in file %s\n(GetExternalModuleAddr error: %s)";
//static char cb_S_Ldb_Unable_to_Verif_test_2[] = "No se puede verificar la prueba %s en el archivo %s\n(error GetExternalModuleAddr: %s)";
//static char cb_F_Ldb_Unable_to_Verif_test_2[] = "Impossible de v�rifier test %s ds fichier %s\n(erreur GetExternalModuleAddr: %s)";
//static char cb_G_Ldb_Unable_to_Verif_test_2[] = "Kann Test %s in Datei %s nicht �berpr�fen.\n(GetExternalModuleAddr Fehler: %s)";
//static char cb_I_Ldb_Unable_to_Verif_test_2[] = "Impossibile verificare il test %s nel file %s\n(Errore GetExternalModuleAddr: %s)";
//static char cb_P_Ldb_Unable_to_Verif_test_2[] = "N�o � poss�vel verificar teste %s no arquivo %s\n(Erro de GetExternalModuleAddr: %s)";

//static char cb_E_Ldb_Unable_to_Verif_pretest_2[] = "Unable to verify pre test %s in file %s\n(GetExternalModuleAddr error: %s)";
//static char cb_S_Ldb_Unable_to_Verif_pretest_2[] = "No se puede verificar la prueba previa %s en el archivo %s\n(error GetExternalModuleAddr: %s)";
//static char cb_F_Ldb_Unable_to_Verif_pretest_2[] = "Impossible de v�rifier pr�test %s ds fichier %s\n(erreur GetExternalModuleAddr: %s)";
//static char cb_G_Ldb_Unable_to_Verif_pretest_2[] = "Kann Vortest %s in Datei %s nicht �berpr�fen.\n(GetExternalModuleAddr Fehler: %s)";
//static char cb_I_Ldb_Unable_to_Verif_pretest_2[] = "Impossibile verificare pre test %s nel file %s\n(Errore GetExternalModuleAddr: %s)";
//static char cb_P_Ldb_Unable_to_Verif_pretest_2[] = "N�o � poss�vel verificar pr�-teste %s no arquivo %s\n(Erro de GetExternalModuleAddr: %s)";

//static char cb_E_Ldb_Unable_to_Verif_posttest_2[] = "Unable to verify post test %s in file %s\n(GetExternalModuleAddr error: %s)";
//static char cb_S_Ldb_Unable_to_Verif_posttest_2[] = "No se puede verificar la prueba posterior %s en el archivo %s\n(error GetExternalModuleAddr: %s)";
//static char cb_F_Ldb_Unable_to_Verif_posttest_2[] = "Impossible de v�rifier posttest %s ds fichier %s\n(erreur GetExternalModuleAddr: %s)";
//static char cb_G_Ldb_Unable_to_Verif_posttest_2[] = "Kann Nachtest %s in Datei %s nicht �berpr�fen.\n(GetExternalModuleAddr Fehler: %s)";
//static char cb_I_Ldb_Unable_to_Verif_posttest_2[] = "Impossibile verificare post test %s nel file %s\n(Errore GetExternalModuleAddr: %s)";
//static char cb_P_Ldb_Unable_to_Verif_posttest_2[] = "N�o � poss�vel verificar p�s-teste %s no arquivo %s\n(Erro de GetExternalModuleAddr: %s)";

//static char cb_E_Ldb_Invalid_Limit_Spec[] = "Invalid limit specification for test %s";
//static char cb_S_Ldb_Invalid_Limit_Spec[] = "Especificaci�n l�mite de la prueba %s no v�lida";
//static char cb_F_Ldb_Invalid_Limit_Spec[] = "Sp�cification des limites test %s invalide";
//static char cb_G_Ldb_Invalid_Limit_Spec[] = "Ung�ltiger Grenzwert f�r Test %s";
//static char cb_I_Ldb_Invalid_Limit_Spec[] = "Limite specificato per test %s non valido";
//static char cb_P_Ldb_Invalid_Limit_Spec[] = "Especifica��o de limite inv�lida para teste %s";

//static char cb_E_Ldb_Invalid_Module[] = "Invalid test module";
//static char cb_S_Ldb_Invalid_Module[] = "M�dulo de prueba no v�lido";
//static char cb_F_Ldb_Invalid_Module[] = "Module de test invalide";
//static char cb_G_Ldb_Invalid_Module[] = "Ung�ltiges Testmodul";
//static char cb_I_Ldb_Invalid_Module[] = "Modulo test non valido";
//static char cb_P_Ldb_Invalid_Module[] = "M�dulo de teste inv�lido";

static char cb_E_Ldb_Cant_Open_file[] = "Unable to open file %s";
static char cb_S_Ldb_Cant_Open_file[] = "No se puede abrir el archivo %s";
static char cb_F_Ldb_Cant_Open_file[] = "Ouverture fichier %s impossible";
static char cb_G_Ldb_Cant_Open_file[] = "Kann Datei %s nicht �ffnen";
static char cb_I_Ldb_Cant_Open_file[] = "Impossibile aprire il file %s";
static char cb_P_Ldb_Cant_Open_file[] = "N�o � poss�vel abrir arquivo %s";

//static char cb_E_Ldb_Err_Reading_File[] = "Error reading file";
//static char cb_S_Ldb_Err_Reading_File[] = "Error al leer el archivo";
//static char cb_F_Ldb_Err_Reading_File[] = "Erreur lecture du fichier";
//static char cb_G_Ldb_Err_Reading_File[] = "Fehler beim Lesen der Datei";
//static char cb_I_Ldb_Err_Reading_File[] = "Errore durante la lettura del file";
//static char cb_P_Ldb_Err_Reading_File[] = "Erro ao ler arquivo";

//static char cb_E_Ldb_Not_a_TX_file[] = "%s is not a test executive file";
//static char cb_S_Ldb_Not_a_TX_file[] = "%s no es un archivo de prueba ejecutivo";
//static char cb_F_Ldb_Not_a_TX_file[] = "%s n'est pas un fichier d'ex�cution de test";
//static char cb_G_Ldb_Not_a_TX_file[] = "%s ist keine Test ausf�hrende Datei";
//static char cb_I_Ldb_Not_a_TX_file[] = "%s non � un file esecutivo del test";
//static char cb_P_Ldb_Not_a_TX_file[] = "%s n�o � um arquivo de execu��o de teste";

static char cb_E_Ldb_Cant_Write_File[] = "Error writing to file %s";
static char cb_S_Ldb_Cant_Write_File[] = "Error al escribir en el archivo %s";
static char cb_F_Ldb_Cant_Write_File[] = "Erreur �criture ds fichier %s";
static char cb_G_Ldb_Cant_Write_File[] = "Fehler beim Schreiben in die Datei %s";
static char cb_I_Ldb_Cant_Write_File[] = "Errore durante la scrittura sul file %s";
static char cb_P_Ldb_Cant_Write_File[] = "Erro ao gravar para arquivo %s";

//static char cb_E_Ldb_Seq_pretest_Fail[] = "Sequence pre-test failed";
//static char cb_S_Ldb_Seq_pretest_Fail[] = "Fallo de la prueba previa a la secuencia";
//static char cb_F_Ldb_Seq_pretest_Fail[] = "Pr�test s�quence �chou�";
//static char cb_G_Ldb_Seq_pretest_Fail[] = "Sequenz Vortest mi�lungen";
//static char cb_I_Ldb_Seq_pretest_Fail[] = "Errore pre test sequenza";
//static char cb_P_Ldb_Seq_pretest_Fail[] = "Falha no pr�-teste da seq��ncia";

//static char cb_E_Ldb_Seq_posttest_Fail[] = "Sequence post-test failed";
//static char cb_S_Ldb_Seq_posttest_Fail[] = "Fallo de la prueba posterior a la secuencia";
//static char cb_F_Ldb_Seq_posttest_Fail[] = "Posttest s�quence �chou�";
//static char cb_G_Ldb_Seq_posttest_Fail[] = "Sequenz Nachtest mi�lungen";
//static char cb_I_Ldb_Seq_posttest_Fail[] = "Errore post test sequenza";
//static char cb_P_Ldb_Seq_posttest_Fail[] = "Falha no p�s-teste da seq��ncia";

//static char cb_E_Ldb_SeqLoad_test_Fail[] = "Sequence-load pre-test failed";
//static char cb_S_Ldb_SeqLoad_test_Fail[] = "Fallo de la prueba previa a la carga de la secuencia";
//static char cb_F_Ldb_SeqLoad_test_Fail[] = "Pr�test chargt s�quence �chou�";
//static char cb_G_Ldb_SeqLoad_test_Fail[] = "Sequenz-Laden Vortest mi�lungen";
//static char cb_I_Ldb_SeqLoad_test_Fail[] = "Errore pre test caricamento sequenza";
//static char cb_P_Ldb_SeqLoad_test_Fail[] = "Falha no pr�-teste de seq��ncia-carregamento";

//static char cb_E_Ldb_SeqUnload_test_Fail[] = "Sequence-unload post-test failed";
//static char cb_S_Ldb_SeqUnload_test_Fail[] = "Fallo de la prueba posterior a la descarga de la secuencia";
//static char cb_F_Ldb_SeqUnload_test_Fail[] = "Posttest d�chargt s�quence �chou�";
//static char cb_G_Ldb_SeqUnload_test_Fail[] = "Sequenz-Entladen Nachtest mi�lungen";
//static char cb_I_Ldb_SeqUnload_test_Fail[] = "Errore post test scaricamento sequenza";
//static char cb_P_Ldb_SeqUnload_test_Fail[] = "Falha no p�s-teste de seq��ncia-descarregamento";

//static char cb_E_Ldb_Test_out_of_range[] = "Test number out of range %d (first test %d - last test %d)";
//static char cb_S_Ldb_Test_out_of_range[] = "N�mero de prueba fuera de intervalo %d (primera prueba %d - �ltima prueba %d)";
//static char cb_F_Ldb_Test_out_of_range[] = "Num�ro du test hors tol�rance %d (premier test %d - dernier test %d)";
//static char cb_G_Ldb_Test_out_of_range[] = "Testanzahl nicht im zul�ssigen Bereich %d (erster Test %d - letzter Test %d)";
//static char cb_I_Ldb_Test_out_of_range[] = "Numero test fuori intervallo %d (primo test %d - ultimo test %d)";
//static char cb_P_Ldb_Test_out_of_range[] = "N�mero do teste for-a da faixa %d (primeiro teste %d - �ltimo teste %d)";

//static char cb_E_Ldb_Invalid_Engine_Info[] = "Invalid engine info field or value";
//static char cb_S_Ldb_Invalid_Engine_Info[] = "Campo de informaci�n o valor mec�nicos no v�lido";
//static char cb_F_Ldb_Invalid_Engine_Info[] = "Champ ou valeur info du moteur incorrect";
//static char cb_G_Ldb_Invalid_Engine_Info[] = "Ung�lt. Engine Infofeld/Wert";
//static char cb_I_Ldb_Invalid_Engine_Info[] = "Valore o campo dati del motore non valido";
//static char cb_P_Ldb_Invalid_Engine_Info[] = "Campo ou valor de info de mecanismo inv�lido";

//static char cb_E_Ldb_Newer_Version_Err[] = "File version (%d) is greater than current version (%d)";
//static char cb_S_Ldb_Newer_Version_Err[] = "La versi�n del archivo (%d) es superior a la versi�n actual (%d)";
//static char cb_F_Ldb_Newer_Version_Err[] = "Version (%d) du fichier est sup�rieure � la version actuelle (%d)";
//static char cb_G_Ldb_Newer_Version_Err[] = "Dateiversion (%d) ist gr��er als aktuelle Version (%d)";
//static char cb_I_Ldb_Newer_Version_Err[] = "Le dimensioni della versione (%d) sono maggiori rispetto alla versione corrente (%d)";
//static char cb_P_Ldb_Newer_Version_Err[] = "A vers�o do arquivo (%d) � superior � vers�o atual (%d)";

static char cb_E_Ldb_PTS_Com_Error[] = "Cannot communicate with the PTS 2000";
static char cb_S_Ldb_PTS_Com_Error[] = "No se puede establecer comunicaci�n con PTS 2000";
static char cb_F_Ldb_PTS_Com_Error[] = "Impossible de communiquer avec PTS 2000";
static char cb_G_Ldb_PTS_Com_Error[] = "Kann nicht mit PTS 2000 kommunizieren";
static char cb_I_Ldb_PTS_Com_Error[] = "Impossibile comunicare con PTS 2000";
static char cb_P_Ldb_PTS_Com_Error[] = "N�o � poss�vel a comunica��o com o PTS 2000";

static char cb_E_Ldb_Unknown_Error[] = "Unknown error %d";
static char cb_S_Ldb_Unknown_Error[] = "Error desconocido %d";
static char cb_F_Ldb_Unknown_Error[] = "Erreur inconnue %d";
static char cb_G_Ldb_Unknown_Error[] = "Unbekannter Fehler %d";
static char cb_I_Ldb_Unknown_Error[] = "Errore sconosciuto %d";
static char cb_P_Ldb_Unknown_Error[] = "Erro desconhecido %d";

// LoadExternalModuleErrorString
//static char cb_E_Ldb_File_No_Found[] = "File not found";
//static char cb_S_Ldb_File_No_Found[] = "Archivo no encontrado";
//static char cb_F_Ldb_File_No_Found[] = "Fichier introuvable";
//static char cb_G_Ldb_File_No_Found[] = "Datei nicht gefunden";
//static char cb_I_Ldb_File_No_Found[] = "Impossibile trovare il file";
//static char cb_P_Ldb_File_No_Found[] = "Arquivo n�o encontrado";

//static char cb_E_Ldb_Invalid_file_format[] = "Invalid file format";
//static char cb_S_Ldb_Invalid_file_format[] = "Formato de archivo no v�lido";
//static char cb_F_Ldb_Invalid_file_format[] = "Format de fichier incorrect";
//static char cb_G_Ldb_Invalid_file_format[] = "Ung�ltiges Dateiformat";
//static char cb_I_Ldb_Invalid_file_format[] = "Formato file non valido";
//static char cb_P_Ldb_Invalid_file_format[] = "Formato de arquivo inv�lido";

//static char cb_E_Ldb_Invalid_Path[] = "Invalid path name";
//static char cb_S_Ldb_Invalid_Path[] = "INombre de ruta no v�lido";
//static char cb_F_Ldb_Invalid_Path[] = "Nom de chemin incorrect";
//static char cb_G_Ldb_Invalid_Path[] = "Ung�ltiger Pfadname";
//static char cb_I_Ldb_Invalid_Path[] = "Nome percorso non valido";
//static char cb_P_Ldb_Invalid_Path[] = "Nome de caminho inv�lido";

//static char cb_E_Ldb_Unknown_Ext[] = "Unknown file extension";
//static char cb_S_Ldb_Unknown_Ext[] = "Extensi�n de archivo desconocida";
//static char cb_F_Ldb_Unknown_Ext[] = "Extension fichier inconnue";
//static char cb_G_Ldb_Unknown_Ext[] = "Unbekannte Dateierweiterung";
//static char cb_I_Ldb_Unknown_Ext[] = "Estensione del file sconosciuta";
//static char cb_P_Ldb_Unknown_Ext[] = "Extens�o de arquivo desconhecida";

//static char cb_E_Ldb_Cannot_open_file[] = "Cannot open file";
//static char cb_S_Ldb_Cannot_open_file[] = "No se puede abrir el archivo";
//static char cb_F_Ldb_Cannot_open_file[] = "Ouverture fichier impossible";
//static char cb_G_Ldb_Cannot_open_file[] = "Kann Datei nicht �ffnen";
//static char cb_I_Ldb_Cannot_open_file[] = "Impossibile aprire il file";
//static char cb_P_Ldb_Cannot_open_file[] = "N�o � poss�vel abrir arquivo";

//static char cb_E_Ldb_PTH_file_open_err[] = ".PTH file open error";
//static char cb_S_Ldb_PTH_file_open_err[] = "Error al abrir el archivo .PTH";
//static char cb_F_Ldb_PTH_file_open_err[] = "erreur ouverture fichier .PTH";
//static char cb_G_Ldb_PTH_file_open_err[] = ".PTH-Datei - Fehler beim �ffnen";
//static char cb_I_Ldb_PTH_file_open_err[] = "Errore di apertura del file .PTH";
//static char cb_P_Ldb_PTH_file_open_err[] = "Erro de abertura de arquivo .PTH";

//static char cb_E_Ldb_PTH_file_read_err[] = ".PTH file read error";
//static char cb_S_Ldb_PTH_file_read_err[] = "Error al leer el archivo .PTH";
//static char cb_F_Ldb_PTH_file_read_err[] = "erreur lecture fichier .PTH";
//static char cb_G_Ldb_PTH_file_read_err[] = ".PTH-Datei - Fehler beim Lesen";
//static char cb_I_Ldb_PTH_file_read_err[] = "Errore di lettura del file .PTH";
//static char cb_P_Ldb_PTH_file_read_err[] = "Erro de leitura de arquivo .PTH";

//static char cb_E_Ldb_PTH_invalid_contents[] = ".PTH file invalid contents";
//static char cb_S_Ldb_PTH_invalid_contents[] = "Contenido no v�lido del archivo .PTH";
//static char cb_F_Ldb_PTH_invalid_contents[] = "contenu fichier .PTH invalide";
//static char cb_G_Ldb_PTH_invalid_contents[] = ".PTH-Datei - Ung�ltiger Inhalt";
//static char cb_I_Ldb_PTH_invalid_contents[] = "Contenuto del file .PTH non valido";
//static char cb_P_Ldb_PTH_invalid_contents[] = "Conte�do inv�lido de arquivo .PTH";

//static char cb_E_Ldb_DLL_sfp[] = "DLL header file contains a static function prototype.";
//static char cb_S_Ldb_DLL_sfp[] = "El archivo de encabezado contiene un prototipo de funci�n est�tica.";
//static char cb_F_Ldb_DLL_sfp[] = "Le fichier d'en-t�te DLL contient un prototype de fonction statique.";
//static char cb_G_Ldb_DLL_sfp[] = "DLL Header Datei enth�lt den Prototyp einer statischen Funktion.";
//static char cb_I_Ldb_DLL_sfp[] = "File dell'intestazione DLL con prototipo di funzione statica.";
//static char cb_P_Ldb_DLL_sfp[] = "Arquivo de cabe�alho de DLL cont�m prot�tipo de fun��o est�tica.";

//static char cb_E_Ldb_DLL_uat[] = "DLL function has an unsupported argument type.";
//static char cb_S_Ldb_DLL_uat[] = "La funci�n DLL tiene un tipo de argumento no admitido.";
//static char cb_F_Ldb_DLL_uat[] = "Le type d'argument de la fonction DLL n'est pas pris en charge.";
//static char cb_G_Ldb_DLL_uat[] = "DLL Funktion hat einen Programmfehler.";
//static char cb_I_Ldb_DLL_uat[] = "Argomento della funzione DLL non supportato.";
//static char cb_P_Ldb_DLL_uat[] = "Fun��o de DLL possui um tipo de argumento n�o suportado.";

//static char cb_E_Ldb_DLL_vaf[] = "DLL has a variable argument function.";
//static char cb_S_Ldb_DLL_vaf[] = "La DLL tiene una funci�n de argumento variable.";
//static char cb_F_Ldb_DLL_vaf[] = "DLL a une fonction � argument variable.";
//static char cb_G_Ldb_DLL_vaf[] = "DLL hat eine Funktion mit variablem Argument.";
//static char cb_I_Ldb_DLL_vaf[] = "Funzione dell'argomento DLL variabile.";
//static char cb_P_Ldb_DLL_vaf[] = "DLL possui fun��o de argumento vari�vel.";

//static char cb_E_Ldb_DLL_wo_p_proto[] = "DLL header contains a function without a proper prototype.";
//static char cb_S_Ldb_DLL_wo_p_proto[] = "El encabezado de la DLL contiene una funci�n sin un prototipo adecuado.";
//static char cb_F_Ldb_DLL_wo_p_proto[] = "L'en-t�te DLL contient une fonction sans prototype correct.";
//static char cb_G_Ldb_DLL_wo_p_proto[] = "DLL Header enth�lt eine Funktion ohne einen korrekten Prototyp.";
//static char cb_I_Ldb_DLL_wo_p_proto[] = "Funzione dell'intestazione DLL senza prototipo corretto.";
//static char cb_P_Ldb_DLL_wo_p_proto[] = "Cabe�alho de DLL cont�m fun��o sem prot�tipo correto.";

//static char cb_E_Ldb_DLL_urt[] = "DLL function has an unsupported return type.";
//static char cb_S_Ldb_DLL_urt[] = "La funci�n DLL tiene un tipo de retorno no admitido.";
//static char cb_F_Ldb_DLL_urt[] = "Le type de retour de la fonction DLL n'est pas pris en charge.";
//static char cb_G_Ldb_DLL_urt[] = "DLL Funktion hat einen Programmfehler.";
//static char cb_I_Ldb_DLL_urt[] = "Ritorno della funzione DLL non supportato.";
//static char cb_P_Ldb_DLL_urt[] = "Fun��o de DLL possui um tipo de retorno n�o suportado.";

//static char cb_E_Ldb_DLL_artfp[] = "A DLL function's argument or return type is a function pointer.";
//static char cb_S_Ldb_DLL_artfp[] = "Un tipo de retorno o argumento de la funci�n DLL es un indicador de funci�n.";
//static char cb_F_Ldb_DLL_artfp[] = "Le type de retour ou d'argument d'une fonction DLL est un pointeur.";
//static char cb_G_Ldb_DLL_artfp[] = "Argument oder R�ckgabetyp einer DLL ist ein Funktionszeiger.";
//static char cb_I_Ldb_DLL_artfp[] = "L'argomento di una funzione DLL o un tipo di ritorno � un puntatore di funzione.";
//static char cb_P_Ldb_DLL_artfp[] = "Tipo de retorno ou argumento de fun��o de DLL � um ponteiro de fun��o.";

//static char cb_E_Ldb_DLL_func_not_found[] = "A function in the DLL header file was not found in the DLL.";
//static char cb_S_Ldb_DLL_func_not_found[] = "No se encontr� ninguna funci�n en el archivo de encabezado de DLL en la DLL.";
//static char cb_F_Ldb_DLL_func_not_found[] = "Une fonction du fichier d'en-t�te DLL est introuvable dans DLL.";
//static char cb_G_Ldb_DLL_func_not_found[] = "Eine Funktion der DLL Header Datei wurde nicht in der DLL gefunden.";
//static char cb_I_Ldb_DLL_func_not_found[] = "Funzione del file di intestazione DLL non trovata nella DLL.";
//static char cb_P_Ldb_DLL_func_not_found[] = "Uma fun��o do arquivo de cabe�alho da DLL n�o foi encontrada na DLL.";

//static char cb_E_Ldb_DLL_cant_load[] = "Could not load the DLL.";
//static char cb_S_Ldb_DLL_cant_load[] = "No se puede cargar la DLL.";
//static char cb_F_Ldb_DLL_cant_load[] = "Chargement DLL impossible.";
//static char cb_G_Ldb_DLL_cant_load[] = "DLL konnte nicht geladen werden.";
//static char cb_I_Ldb_DLL_cant_load[] = "Impossibile caricare la DLL.";
//static char cb_P_Ldb_DLL_cant_load[] = "N�o foi poss�vel carregar a DLL.";

//static char cb_E_Ldb_DLL_cant_find_hdr[] = "Could not find the DLL header file.";
//static char cb_S_Ldb_DLL_cant_find_hdr[] = "No se puede encontrar el archivo de encabezado de DLL.";
//static char cb_F_Ldb_DLL_cant_find_hdr[] = "Fichier d'en-t�te DLL introuvable.";
//static char cb_G_Ldb_DLL_cant_find_hdr[] = "Konnte DLL Header Datei nicht finden.";
//static char cb_I_Ldb_DLL_cant_find_hdr[] = "Impossibile trovare il file di intestazione DLL.";
//static char cb_P_Ldb_DLL_cant_find_hdr[] = "N�o foi poss�vel encontrar o arquivo de cabe�alho da DLL.";

//static char cb_E_Ldb_DLL_cant_ld_hdr[] = "Could not load the DLL header file (out of memory or the file is corrupted).";
//static char cb_S_Ldb_DLL_cant_ld_hdr[] = "No se puede cargar el archivo de encabezado de DLL (memoria insuficiente o archivo da�ado).";
//static char cb_F_Ldb_DLL_cant_ld_hdr[] = "Impossible de charger fichier d'en-t�te DLL (m�moire insuf ou fichier alt�r�).";
//static char cb_G_Ldb_DLL_cant_ld_hdr[] = "Konnte DLL Header Datei nicht laden (zu wenig Speicher oder Datei ist besch�digt).";
//static char cb_I_Ldb_DLL_cant_ld_hdr[] = "Impossibile caricare il file di intestazione DLL (memoria insufficiente o file danneggiato).";
//static char cb_P_Ldb_DLL_cant_ld_hdr[] = "N�o foi poss�vel carregar o arquivo de cabe�alho da DLL (mem�ria insuficiente ou arquivo danificado).";

//static char cb_E_Ldb_DLL_hdr_syntax_err[] = "Syntax error in the DLL header file.";
//static char cb_S_Ldb_DLL_hdr_syntax_err[] = "Error de sintaxis en el archivo de encabezado de DLL.";
//static char cb_F_Ldb_DLL_hdr_syntax_err[] = "Erreur de syntaxe ds fichier d'en-t�te DLL.";
//static char cb_G_Ldb_DLL_hdr_syntax_err[] = "Syntaxfehler in DLL Header Datei.";
//static char cb_I_Ldb_DLL_hdr_syntax_err[] = "Errore di sintassi nel file di intestazione DLL.";
//static char cb_P_Ldb_DLL_hdr_syntax_err[] = "Erro de sintaxe no arquivo de cabe�alho da DLL.";

//static char cb_E_Ldb_Err_num_ofr[] = "Error code out of range.";
//static char cb_S_Ldb_Err_num_ofr[] = "C�digo de error fuera de intervalo.";
//static char cb_F_Ldb_Err_num_ofr[] = "Code d'erreur hors tol�rance.";
//static char cb_G_Ldb_Err_num_ofr[] = "Fehlercode au�erhalb des zul�ssigen Bereichs.";
//static char cb_I_Ldb_Err_num_ofr[] = "Codice errore fuori intervallo.";
//static char cb_P_Ldb_Err_num_ofr[] = "C�digo de erro for-a da faixa.";



//static char cb_E_Ldb_AC[] = "A/C";
//static char cb_E_Ldb_Cancel[] = "Cancel";
//static char cb_E_Ldb_Connect[] = "Connect";
//static char cb_E_Ldb_Exit[] = "Exit";
//static char cb_E_Ldb_Invalid_Mod_ID[] = "Invalid module ID";
//static char cb_E_Ldb_Looping[] = "Looping";
//static char cb_E_Ldb_MSG_LOADSEQ_SETUP_ERR[] = "Could not execute sequence-load setup function.";
//static char cb_E_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE[] = "Sequence-load setup error";
//static char cb_E_Ldb_MSG_SEQ_MOVED[] = "Warning: The sequence has moved since it was last saved.\nDo you wish to update the file paths?\n";
//static char cb_E_Ldb_MSG_SEQ_MOVED_TITLE[] = "Sequence moved";
//static char cb_E_Ldb_MSG_SINGLE_ABORT[] = "Single pass aborted.\n\n";
//static char cb_E_Ldb_MSG_SINGLE_FAIL[] = "Single pass stopped by test failure.\n\n";
//static char cb_E_Ldb_MSG_SINGLE_RTE[] = "Single pass stopped by run-time error.\n\n";
//static char cb_E_Ldb_Met_Port[] = "Exhalation port";
//static char cb_E_Ldb_Not_Glbl_ID[] = "Identifier not defined globally in module";
//static char cb_E_Ldb_OK[] = "OK";
//static char cb_E_Ldb_Paused[] = "PAUSED";
//static char cb_E_Ldb_Port[] = "Port";
//static char cb_E_Ldb_SIMV[] = "SIMV";
//static char cb_E_Ldb_SPONT[] = "SPONT";
//static char cb_E_Ldb_Select_Filename[] = "Select filename";
//static char cb_E_Ldb_TX_error[] = "Test executive error";
//static char cb_E_Ldb_Undef_Refs[] = "Undefined references";
//static char cb_E_Ldb_cant_decode[] = "Error: cannot decode error # %d";

static char * E_langTable[] = { // English
	 0,	// 0 -- unused
	 cb_E_Ldb_ProgramName
	,cb_E_Ldb_Splash_ProgramName
	,cb_E_Ldb_Copyright_Str
	,cb_E_Ldb_Copyright_Rights
	,cb_E_Ldb_Splash_Eval_Msg
	,cb_E_Ldb_Version_Str
	,cb_E_Ldb_SelectUser
	,cb_E_Ldb_PTS2000_Name
	,cb_E_Ldb_PTS_Serial_Lbl
	,cb_E_Ldb_PTS_SWversion_Lbl
	,cb_E_Ldb_PTS_CalLast_Lbl
	,cb_E_Ldb_PTS_CalNext_Lbl
	,cb_E_Ldb_840Vent_Name
	,cb_E_Ldb_840_GUIserial_Lbl
	,cb_E_Ldb_840_BDUserial_Lbl
	,cb_E_Ldb_840_GUIsoftware_Lbl
	,cb_E_Ldb_840_BDUsoftware_Lbl
	,cb_E_Ldb_840_cycle_power
	,cb_E_Ldb_2step
	,cb_E_Ldb_3step
	,cb_E_Ldb_4step
	,cb_E_Ldb_6step
	,cb_E_Ldb_840_Com_Error
	,cb_E_Ldb_840_NoDetect
	,cb_E_Ldb_9step
	,cb_E_Ldb_ABORT
	,cb_E_Ldb_ABORTED
//	,cb_E_Ldb_AC
	,cb_E_Ldb_Add_User
	,cb_E_Ldb_Adj_100_O2_Fail
	,cb_E_Ldb_Adj_100_O2_Succ
	,cb_E_Ldb_Adj_100_O2
	,cb_E_Ldb_Adj_21_O2_Fail
	,cb_E_Ldb_Adj_21_O2_Succ
	,cb_E_Ldb_Adj_21_O2
	,cb_E_Ldb_AdminpanelFile
	,cb_E_Ldb_Alarm_Audible
	,cb_E_Ldb_Apr_Abrv
	,cb_E_Ldb_Aug_Abrv
	,cb_E_Ldb_AutoZeroHiPres
	,cb_E_Ldb_AutoZeroLoPres
	,cb_E_Ldb_AutoZero_Cbuf1
	,cb_E_Ldb_AutoZero_Cbuf2
	,cb_E_Ldb_AutoZero_Cbuf3
	,cb_E_Ldb_BDU_Message
	,cb_E_Ldb_BDU_Title
	,cb_E_Ldb_BootPanelTitle
	,cb_E_Ldb_BootP_Error
	,cb_E_Ldb_BootP_Processing
	,cb_E_Ldb_BootP_Ready
	,cb_E_Ldb_BootP_Stopped
	,cb_E_Ldb_CP_Air_Msg
	,cb_E_Ldb_CP_CIR_Msg
	,cb_E_Ldb_CSE_DiagLog
	,cb_E_Ldb_RSE_Dir
	,cb_E_Ldb_CSE_Dir
	,cb_E_Ldb_CSE_File
	,cb_E_Ldb_CSE_InfoLog
	,cb_E_Ldb_CSE_TestLog
	,cb_E_Ldb_CaliHiPres
	,cb_E_Ldb_CaliLoPres
	,cb_E_Ldb_CaliOxygen
	,cb_E_Ldb_Calibrate
//	,cb_E_Ldb_Cancel
//	,cb_E_Ldb_Cannot_open_file
	,cb_E_Ldb_Cant_Open_file
	,cb_E_Ldb_Cant_Write_File
	,cb_E_Ldb_Cant_read_registry
	,cb_E_Ldb_ChangePwdTitle
	,cb_E_Ldb_Circ_Pres
	,cb_E_Ldb_Clear
	,cb_E_Ldb_Clear_Sys_logs
	,cb_E_Ldb_Clear_sys_logs
	,cb_E_Ldb_ClearingDB
	,cb_E_Ldb_Cncling_Pls_Wait
	,cb_E_Ldb_Com_Error
	,cb_E_Ldb_Comp_not_installed
	,cb_E_Ldb_Comp_set_fail
	,cb_E_Ldb_Confirm_clear
//	,cb_E_Ldb_Connect
	,cb_E_Ldb_DB_Delete_title
	,cb_E_Ldb_Reports_Delete_title
	,cb_E_Ldb_DB_Name
	,cb_E_Ldb_DB_Sure_Delete
	,cb_E_Ldb_Reports_Sure_Delete
	,cb_E_Ldb_DB_Warning
	,cb_E_Ldb_DB_files_missing
	,cb_E_Ldb_DB_getting_big
//	,cb_E_Ldb_DLL_artfp
//	,cb_E_Ldb_DLL_cant_find_hdr
//	,cb_E_Ldb_DLL_cant_ld_hdr
//	,cb_E_Ldb_DLL_cant_load
//	,cb_E_Ldb_DLL_func_not_found
//	,cb_E_Ldb_DLL_hdr_syntax_err
//	,cb_E_Ldb_DLL_sfp
//	,cb_E_Ldb_DLL_uat
//	,cb_E_Ldb_DLL_urt
//	,cb_E_Ldb_DLL_vaf
//	,cb_E_Ldb_DLL_wo_p_proto
	,cb_E_Ldb_Database_error
	,cb_E_Ldb_Database_error_msg
	,cb_E_Ldb_Dec_Abrv
	,cb_E_Ldb_Delete_User
	,cb_E_Ldb_DemoTitle
	,cb_E_Ldb_DiagLog_Def_File
	,cb_E_Ldb_DiagLog_Def_Path
	,cb_E_Ldb_DisableAlarms
	,cb_E_Ldb_Discon_Hi_Instr
	,cb_E_Ldb_Discon_Lo_Instr
	,cb_E_Ldb_Dup_Username
	,cb_E_Ldb_ERR_PTS_BURST
	,cb_E_Ldb_ERR_TEST_TIMEOUT
	,cb_E_Ldb_ERR_UIR_LOAD
	,cb_E_Ldb_ERR_VENT_CP
	,cb_E_Ldb_ERR_VENT_GAS_SEQ
	,cb_E_Ldb_ERR_VENT_TEST_SEQ
	,cb_E_Ldb_Engine_Error
	,cb_E_Ldb_English
	,cb_E_Ldb_Ensure3LLung
	,cb_E_Ldb_Ensure4LLung
	,cb_E_Ldb_EnsurehalfLLung
	,cb_E_Ldb_Enter_Pwd_For
//	,cb_E_Ldb_Err_Reading_File
	,cb_E_Ldb_Err_in_db_2
//	,cb_E_Ldb_Err_num_ofr
	,cb_E_Ldb_Error
	,cb_E_Ldb_Error_Caps
	,cb_E_Ldb_EstabCom_840
	,cb_E_Ldb_EstabCom_PTS
//	,cb_E_Ldb_Exit
	,cb_E_Ldb_FAILED
	,cb_E_Ldb_Fail_caps
	,cb_E_Ldb_Feb_Abrv
//	,cb_E_Ldb_File_No_Found
	,cb_E_Ldb_French
	,cb_E_Ldb_GUI_Message
	,cb_E_Ldb_GUI_Title
	,cb_E_Ldb_German
	,cb_E_Ldb_Givefivebreaths
	,cb_E_Ldb_Givefiveminutes
	,cb_E_Ldb_HelpCommand
	,cb_E_Ldb_Helpfile_Name
	,cb_E_Ldb_HiPres_AutoZero_Fail
	,cb_E_Ldb_HiPres_AutoZero_Succ
	,cb_E_Ldb_IBW_10kg
	,cb_E_Ldb_IBW_20kg
	,cb_E_Ldb_IBW_85kg
	,cb_E_Ldb_ImagPanelTitle
	,cb_E_Ldb_ImagPnlImaglist_lbl
	,cb_E_Ldb_ImagPnlIPlist_lbl
//	,cb_E_Ldb_InfFile
	,cb_E_Ldb_InfoLog_Def_File
	,cb_E_Ldb_InfoLog_Def_Path
	,cb_E_Ldb_InsPanelTitle
	,cb_E_Ldb_InsPnlInstr_Line01
	,cb_E_Ldb_InsPnlInstr_Line02
	,cb_E_Ldb_InsPnlInstr_Line03
	,cb_E_Ldb_InsPnlInstr_Line04
	,cb_E_Ldb_InsPnlTextMsg
//	,cb_E_Ldb_Invalid_Engine_Info
//	,cb_E_Ldb_Invalid_Limit_Spec
//	,cb_E_Ldb_Invalid_Mod_ID
//	,cb_E_Ldb_Invalid_Module
//	,cb_E_Ldb_Invalid_Path
//	,cb_E_Ldb_Invalid_file_format
	,cb_E_Ldb_Italian
	,cb_E_Ldb_Jan_Abrv
	,cb_E_Ldb_Jul_Abrv
	,cb_E_Ldb_Jun_Abrv
//	,cb_E_Ldb_List_not_Empty
	,cb_E_Ldb_LoPres_AutoZero_Fail
	,cb_E_Ldb_LoPres_AutoZero_Succ
//	,cb_E_Ldb_Looping
	,cb_E_Ldb_MSG_ABORT
	,cb_E_Ldb_MSG_CHANGE_DFLT_RPT
	,cb_E_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY
	,cb_E_Ldb_MSG_CHANGE_DFLT_RPT_TITLE
	,cb_E_Ldb_MSG_ERROR
	,cb_E_Ldb_MSG_FAIL
//	,cb_E_Ldb_MSG_LOADSEQ_SETUP_ERR
//	,cb_E_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE
	,cb_E_Ldb_MSG_LOOPING
	,cb_E_Ldb_MSG_LOOP_ABORT
	,cb_E_Ldb_MSG_NONE
	,cb_E_Ldb_MSG_OUT_OF_MEMORY
	,cb_E_Ldb_MSG_PASS
	,cb_E_Ldb_MSG_RPT_NO_MEM
	,cb_E_Ldb_MSG_RPT_NO_MORE_MEM
	,cb_E_Ldb_MSG_RUNNING
	,cb_E_Ldb_MSG_SAVE_RPT_TITLE
//	,cb_E_Ldb_MSG_SEQ_MOVED
//	,cb_E_Ldb_MSG_SEQ_MOVED_TITLE
//	,cb_E_Ldb_MSG_SINGLE_ABORT
//	,cb_E_Ldb_MSG_SINGLE_FAIL
//	,cb_E_Ldb_MSG_SINGLE_RTE
	,cb_E_Ldb_MSG_SKIP
	,cb_E_Ldb_MSG_UUT_ABORT
	,cb_E_Ldb_MSG_UUT_FAIL
	,cb_E_Ldb_MSG_UUT_RTE
	,cb_E_Ldb_MainpanelFile
	,cb_E_Ldb_MakeOtherSettings
	,cb_E_Ldb_MakeSettings
	,cb_E_Ldb_ManType2PC
	,cb_E_Ldb_ManType2VC
	,cb_E_Ldb_Mar_Abrv
	,cb_E_Ldb_Max_Users_Reg
	,cb_E_Ldb_May_Abrv
//	,cb_E_Ldb_Met_Port
	,cb_E_Ldb_Mode2AC
//	,cb_E_Ldb_Newer_Version_Err
	,cb_E_Ldb_No_Alarm
//	,cb_E_Ldb_No_More_Tests
	,cb_E_Ldb_No_Tests
	,cb_E_Ldb_None_caps
	,cb_E_Ldb_Not_Found
//	,cb_E_Ldb_Not_Glbl_ID
//	,cb_E_Ldb_Not_a_TX_file
	,cb_E_Ldb_Nov_Abrv
	,cb_E_Ldb_O2_prcnt
//	,cb_E_Ldb_OK
	,cb_E_Ldb_OK_When_Ready
	,cb_E_Ldb_Oct_Abrv
	,cb_E_Ldb_Old_Password
	,cb_E_Ldb_Old_Password_wrng
	,cb_E_Ldb_Out_of_Memory
	,cb_E_Ldb_PASSED
	,cb_E_Ldb_PC
//	,cb_E_Ldb_PP_Test_Exists
//	,cb_E_Ldb_PP_Test_Missing
//	,cb_E_Ldb_PTH_file_open_err
//	,cb_E_Ldb_PTH_file_read_err
//	,cb_E_Ldb_PTH_invalid_contents
	,cb_E_Ldb_PTRIG
	,cb_E_Ldb_PTS_Com_Error
	,cb_E_Ldb_PTS_Comm_Rcv_Error
	,cb_E_Ldb_PTS_Date_Error
	,cb_E_Ldb_PTS_NoDetect
	,cb_E_Ldb_PTS_SN_Error
	,cb_E_Ldb_PTS_Ver_Error
	,cb_E_Ldb_Pass_caps
	,cb_E_Ldb_Password_Mismatch
	,cb_E_Ldb_PauseMsg1
	,cb_E_Ldb_PauseMsg2
//	,cb_E_Ldb_Paused
//	,cb_E_Ldb_Port
	,cb_E_Ldb_Portuguese
	,cb_E_Ldb_Pressure_incorrect
	,cb_E_Ldb_Print
	,cb_E_Ldb_Psens
	,cb_E_Ldb_Pts_cal_err
	,cb_E_Ldb_Pts_cal_expired
	,cb_E_Ldb_Reseting_PTS
	,cb_E_Ldb_Retrieving_logs
	,cb_E_Ldb_Retry_prompt
//	,cb_E_Ldb_Runtime_Err_Test
//	,cb_E_Ldb_Runtime_Err_loadseq_pre_test
//	,cb_E_Ldb_Runtime_Err_postTest
//	,cb_E_Ldb_Runtime_Err_preTest
//	,cb_E_Ldb_Runtime_Err_seq_post_test
//	,cb_E_Ldb_Runtime_Err_seq_pre_test
//	,cb_E_Ldb_Runtime_Err_unlaodseq_post_test
//	,cb_E_Ldb_SIMV
//	,cb_E_Ldb_SPONT
	,cb_E_Ldb_SQUARE
	,cb_E_Ldb_SST_Msg_1
	,cb_E_Ldb_SST_Msg_2
	,cb_E_Ldb_SST_Msg_3
	,cb_E_Ldb_SST_Title
	,cb_E_Ldb_S_840_Establishing
	,cb_E_Ldb_S_840_Failure
	,cb_E_Ldb_S_840_Success
	,cb_E_Ldb_S_PTS_Establishing
	,cb_E_Ldb_S_PTS_Failure
	,cb_E_Ldb_S_PTS_Success
	,cb_E_Ldb_S_Starting
	,cb_E_Ldb_SaveAs
	,cb_E_Ldb_Select_Diag_File
//	,cb_E_Ldb_Select_Filename
	,cb_E_Ldb_Select_Lang_for
	,cb_E_Ldb_Select_SysInfo_file
	,cb_E_Ldb_Select_testlog_file
	,cb_E_Ldb_Selection_Panel
	,cb_E_Ldb_Sep_Abrv
//	,cb_E_Ldb_SeqLoad_test_Fail
//	,cb_E_Ldb_SeqUnload_test_Fail
//	,cb_E_Ldb_Seq_posttest_Fail
//	,cb_E_Ldb_Seq_pretest_Fail
	,cb_E_Ldb_Set_Comp_SN_status
	,cb_E_Ldb_Set_Comp_hours_status
	,cb_E_Ldb_Set_Compressor
	,cb_E_Ldb_Skip_Caps
	,cb_E_Ldb_Spanish
	,cb_E_Ldb_StatePnlInit
	,cb_E_Ldb_StatePnlReplace
	,cb_E_Ldb_StatePnlTitle
	,cb_E_Ldb_Stopped_Fnsh_tst
	,cb_E_Ldb_SureClearResults
	,cb_E_Ldb_SureDeleteUser
	,cb_E_Ldb_TRIANGLE
//	,cb_E_Ldb_TX_error
	,cb_E_Ldb_Test10_Running
	,cb_E_Ldb_Test11_Running
	,cb_E_Ldb_Test12_Running
	,cb_E_Ldb_Test1_Running
	,cb_E_Ldb_Test2_Running
	,cb_E_Ldb_Test3_Running
	,cb_E_Ldb_Test4_Running
	,cb_E_Ldb_Test5_Running
	,cb_E_Ldb_Test6_Running
	,cb_E_Ldb_Test7_Running
	,cb_E_Ldb_Test8_Running
	,cb_E_Ldb_Test9_Running
	,cb_E_Ldb_TestLog_Def_File
	,cb_E_Ldb_TestLog_Def_Path
	,cb_E_Ldb_Test_Failure
//	,cb_E_Ldb_Test_Not_Verif
//	,cb_E_Ldb_Test_out_of_range
	,cb_E_Ldb_SoftdLicPanelTitle
	,cb_E_Ldb_SoftdLicAgreeTxtLine01
	,cb_E_Ldb_SoftdAgreeLicText
	,cb_E_Ldb_SoftdLicPara_1
	,cb_E_Ldb_SoftdLicPara_2
	,cb_E_Ldb_SoftdLicPara_3_1
	,cb_E_Ldb_SoftdLicPara_3_2
	,cb_E_Ldb_SoftdLicAgreeYes
	,cb_E_Ldb_SoftdLicAgreeNo
	,cb_E_Ldb_ImagFileErrTitle
	,cb_E_Ldb_ImagFileErrNofind
	,cb_E_Ldb_NoDloadCDerrTitle
	,cb_E_Ldb_NoDloadCDerrMsg
	,cb_E_Ldb_Dload_CannotEraseBDU
	,cb_E_Ldb_Dload_CannotEraseGUI
	,cb_E_Ldb_Dload_UserAbortTitle
	,cb_E_Ldb_Dload_UserAbortMsg
	,cb_E_Ldb_Dload_TimeoutAbortTitle
	,cb_E_Ldb_Dload_BDUtimeoutMsg
	,cb_E_Ldb_Dload_GUItimeoutMsg
	,cb_E_Ldb_Dload_CompleteTitle
	,cb_E_Ldb_Dload_BDUcomplete
	,cb_E_Ldb_Dload_GUIcomplete
	,cb_E_Ldb_TFTP_PnlTitle
	,cb_E_Ldb_TFTP_BDUfile_lbl
	,cb_E_Ldb_TFTP_GUIfile_lbl
	,cb_E_Ldb_TFTP_BDUpercent_lbl
	,cb_E_Ldb_TFTP_GUIpercent_lbl
	,cb_E_Ldb_TFTP_BDUblocks_lbl
	,cb_E_Ldb_TFTP_BDUtotal_lbl
	,cb_E_Ldb_TFTP_GUIblocks_lbl
	,cb_E_Ldb_TFTP_GUItotal_lbl
	,cb_E_Ldb_TFTP_BDUmsg
	,cb_E_Ldb_TFTP_BDUcomplete
	,cb_E_Ldb_TFTP_GUImsg
	,cb_E_Ldb_TFTP_GUIcomplete
	,cb_E_Ldb_Trig2PTRIG
	,cb_E_Ldb_Trig2VTRIG
//	,cb_E_Ldb_Unable_to_Verif_posttest
//	,cb_E_Ldb_Unable_to_Verif_posttest_2
//	,cb_E_Ldb_Unable_to_Verif_pretest
//	,cb_E_Ldb_Unable_to_Verif_pretest_2
//	,cb_E_Ldb_Unable_to_Verif_test
//	,cb_E_Ldb_Unable_to_Verif_test_2
//	,cb_E_Ldb_Undef_Refs
	,cb_E_Ldb_Unknown_Error
//	,cb_E_Ldb_Unknown_Ext
	,cb_E_Ldb_VC
	,cb_E_Ldb_VTRIG
	,cb_E_Ldb_VentOffandOn
	,cb_E_Ldb_Vent_Runtimehours
	,cb_E_Ldb_Vsens
	,cb_E_Ldb_Warning
//	,cb_E_Ldb_cant_decode
	,cb_E_Ldb_comp_hours
	,cb_E_Ldb_comp_hours_f
	,cb_E_Ldb_cp_test_msg
	,cb_E_Ldb_cpe_100
	,cb_E_Ldb_cpe_10
	,cb_E_Ldb_cpe_50
	,cb_E_Ldb_cpi_10
	,cb_E_Ldb_cpi_100
	,cb_E_Ldb_cpi_50
	,cb_E_Ldb_cycle_840
	,cb_E_Ldb_cycle_before_test
	,cb_E_Ldb_cycle_before_test1
	,cb_E_Ldb_enter_oxy
	,cb_E_Ldb_enter_spiro
	,cb_E_Ldb_error_cant_decode
	,cb_E_Ldb_exp
	,cb_E_Ldb_fio2_inst
	,cb_E_Ldb_insp
	,cb_E_Ldb_invalid
	,cb_E_Ldb_mantest_tt1
	,cb_E_Ldb_mantest_tt2
	,cb_E_Ldb_mantest_tt3
	,cb_E_Ldb_mantest_tt4
	,cb_E_Ldb_mantest_tt5
	,cb_E_Ldb_mantest_tt6
	,cb_E_Ldb_mantest_tt7
	,cb_E_Ldb_mantest_tt8
	,cb_E_Ldb_mnl_SIO
	,cb_E_Ldb_mnl_SIO_nd
	,cb_E_Ldb_mnl_air_reg
	,cb_E_Ldb_mnl_air_reg_nd
	,cb_E_Ldb_mnl_est
	,cb_E_Ldb_mnl_est_nd
	,cb_E_Ldb_mnl_f_leakage1
	,cb_E_Ldb_mnl_f_leakage1_nd
	,cb_E_Ldb_mnl_f_leakage2
	,cb_E_Ldb_mnl_f_leakage2_nd
	,cb_E_Ldb_mnl_g_isol_r
	,cb_E_Ldb_mnl_g_isol_r_nd
	,cb_E_Ldb_mnl_gnd_r
	,cb_E_Ldb_mnl_gnd_r_nd
	,cb_E_Ldb_mnl_O2_reg
	,cb_E_Ldb_mnl_O2_reg_nd
	,cb_E_Ldb_mnl_r_leakage1
	,cb_E_Ldb_mnl_r_leakage1_nd
	,cb_E_Ldb_mnl_r_leakage2
	,cb_E_Ldb_mnl_r_leakage2_nd
	,cb_E_Ldb_mnl_sst
	,cb_E_Ldb_mnl_sst_nd
	,cb_E_Ldb_mnl_vent_r
	,cb_E_Ldb_mnl_vent_r_nd
	,cb_E_Ldb_mnl_wrkordr_r
	,cb_E_Ldb_mnl_wrkordr_r_nd
	,cb_E_Ldb_O2_conc_vol
	,cb_E_Ldb_oxy_conc
	,cb_E_Ldb_oxy_conc_fail
	,cb_E_Ldb_oxy_incorrect
	,cb_E_Ldb_oxygen_30
	,cb_E_Ldb_oxygen_90
	,cb_E_Ldb_peep_25
	,cb_E_Ldb_peep_45
	,cb_E_Ldb_peep_5
	,cb_E_Ldb_peep_incorrect
	,cb_E_Ldb_peep_inst
//	,cb_E_Ldb_postTest_Not_Verif
//	,cb_E_Ldb_preTest_Not_Verif
	,cb_E_Ldb_pres_5
	,cb_E_Ldb_pres_90
	,cb_E_Ldb_pres_inst
	,cb_E_Ldb_programming_err
	,cb_E_Ldb_pts_cal_date
	,cb_E_Ldb_pts_next_date
	,cb_E_Ldb_pts_rdg
	,cb_E_Ldb_pts_sn
	,cb_E_Ldb_pts_timeout
	,cb_E_Ldb_pts_vn
	,cb_E_Ldb_rpt_FileName
	,cb_E_Ldb_rpt_Limit
	,cb_E_Ldb_rpt_cr_operator
	,cb_E_Ldb_rpt_date
	,cb_E_Ldb_rpt_desc
	,cb_E_Ldb_rpt_enter_fname
	,cb_E_Ldb_rpt_invalid_fname
	,cb_E_Ldb_rpt_measurement
	,cb_E_Ldb_rpt_operator
	,cb_E_Ldb_rpt_pgm_rev
	,cb_E_Ldb_rpt_print_to_file
	,cb_E_Ldb_rpt_rte_stopped
	,cb_E_Ldb_rpt_seq_name
	,cb_E_Ldb_rpt_stn_id
	,cb_E_Ldb_rpt_test_results
	,cb_E_Ldb_rpt_test_station
	,cb_E_Ldb_rpt_time
	,cb_E_Ldb_rpt_title
	,cb_E_Ldb_rpt_tp_aborted
	,cb_E_Ldb_rpt_uut_pn
	,cb_E_Ldb_rpt_uut_rev
	,cb_E_Ldb_rpt_uut_sn
	,cb_E_Ldb_rpt_why_fname_inv
//	,cb_E_Ldb_rte_cleanup
//	,cb_E_Ldb_rte_error_msg
//	,cb_E_Ldb_rte_load
//	,cb_E_Ldb_rte_seq_cleanup
//	,cb_E_Ldb_rte_seq_setup
//	,cb_E_Ldb_rte_setup
//	,cb_E_Ldb_rte_test_err
//	,cb_E_Ldb_rte_unknown
//	,cb_E_Ldb_rte_unload_seq
	,cb_E_Ldb_spirometry
	,cb_E_Ldb_sprio_rdg
	,cb_E_Ldb_sst_proc_error
	,cb_E_Ldb_station_desc
	,cb_E_Ldb_status
	,cb_E_Ldb_step_x_y
	,cb_E_Ldb_sw_fault_1
	,cb_E_Ldb_sw_fault_2
	,cb_E_Ldb_sw_fault_3
	,cb_E_Ldb_target
	,cb_E_Ldb_target_pts
	,cb_E_Ldb_test_aborted
	,cb_E_Ldb_testing_error
	,cb_E_Ldb_tt1
	,cb_E_Ldb_tt10
	,cb_E_Ldb_tt11
	,cb_E_Ldb_tt12
	,cb_E_Ldb_tt13
	,cb_E_Ldb_tt14
	,cb_E_Ldb_tt15
	,cb_E_Ldb_tt16
	,cb_E_Ldb_tt17
	,cb_E_Ldb_tt2
	,cb_E_Ldb_tt3
	,cb_E_Ldb_tt4
	,cb_E_Ldb_tt5
	,cb_E_Ldb_tt6
	,cb_E_Ldb_tt7
	,cb_E_Ldb_tt8
	,cb_E_Ldb_tt9
	,cb_E_Ldb_vent_atm
	,cb_E_Ldb_vent_fail_reported
	,cb_E_Ldb_vent_oxy_rdg
	,cb_E_Ldb_vol_inst
	,cb_E_Ldb_volume_incorrect
    ,cb_E_Ldb_CSE_AlarmLog
    ,cb_E_Ldb_Select_alarmlog_file
	,cb_E_Ldb_AlarmLog_Def_File
	,cb_E_Ldb_AlarmLog_Def_Path
	,0 
};

//static char cb_S_Ldb_AC[] = "A/C";
//static char cb_S_Ldb_Cancel[] = "Cancelar";
//static char cb_S_Ldb_Connect[] = "Conectar";
//static char cb_S_Ldb_Exit[] = "Salir";
//static char cb_S_Ldb_Invalid_Mod_ID[] = "ID de m�dulo no v�lido";
//static char cb_S_Ldb_Looping[] = "Ejecutando ciclo";
//static char cb_S_Ldb_MSG_LOADSEQ_SETUP_ERR[] = "No se puede ejecutar la funci�n de configuraci�n de carga de secuencias.";
//static char cb_S_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE[] = "Error de configuraci�n de la carga de secuencia";
//static char cb_S_Ldb_MSG_SEQ_MOVED[] = "Advertencia: Se ha desplazado la secuencia desde la �ltima vez que se guard�.\n�Desea actualizar las rutas de archivo?\n";
//static char cb_S_Ldb_MSG_SEQ_MOVED_TITLE[] = "Secuencia desplazada";
//static char cb_S_Ldb_MSG_SINGLE_ABORT[] = "Anulado paso individual.\n\n";
//static char cb_S_Ldb_MSG_SINGLE_FAIL[] = "Paso individual detenido por fallo de la prueba.\n\n";
//static char cb_S_Ldb_MSG_SINGLE_RTE[] = "Paso individual detenido por error de tiempo de ejecuci�n.\n\n";
//static char cb_S_Ldb_Met_Port[] = "Puerto de espiraci�n";
//static char cb_S_Ldb_Not_Glbl_ID[] = "El identificador no est� definido globalmente en el m�dulo";
//static char cb_S_Ldb_OK[] = "Aceptar";
//static char cb_S_Ldb_Paused[] = "DETENIDA";
//static char cb_S_Ldb_Port[] = "Puerto";
//static char cb_S_Ldb_SIMV[] = "SIMV";
//static char cb_S_Ldb_SPONT[] = "ESPONT";
//static char cb_S_Ldb_Select_Filename[] = "Seleccionar nombre de archivo";
//static char cb_S_Ldb_TX_error[] = "Error ejecutivo de prueba";
//static char cb_S_Ldb_Undef_Refs[] = "Referencias no definidas";
//static char cb_S_Ldb_cant_decode[] = "Error: no se puede descodificar el error n� %d";

static char * S_langTable[] = { // Spanish
	 0,	// 0 -- unused
	 cb_S_Ldb_ProgramName
	,cb_S_Ldb_Splash_ProgramName
	,cb_S_Ldb_Copyright_Str
	,cb_S_Ldb_Copyright_Rights
	,cb_S_Ldb_Splash_Eval_Msg
	,cb_S_Ldb_Version_Str
	,cb_S_Ldb_SelectUser
	,cb_S_Ldb_PTS2000_Name
	,cb_S_Ldb_PTS_Serial_Lbl
	,cb_S_Ldb_PTS_SWversion_Lbl
	,cb_S_Ldb_PTS_CalLast_Lbl
	,cb_S_Ldb_PTS_CalNext_Lbl
	,cb_S_Ldb_840Vent_Name
	,cb_S_Ldb_840_GUIserial_Lbl
	,cb_S_Ldb_840_BDUserial_Lbl
	,cb_S_Ldb_840_GUIsoftware_Lbl
	,cb_S_Ldb_840_BDUsoftware_Lbl
	,cb_S_Ldb_840_cycle_power
	,cb_S_Ldb_2step
	,cb_S_Ldb_3step
	,cb_S_Ldb_4step
	,cb_S_Ldb_6step
	,cb_S_Ldb_840_Com_Error
	,cb_S_Ldb_840_NoDetect
	,cb_S_Ldb_9step
	,cb_S_Ldb_ABORT
	,cb_S_Ldb_ABORTED
//	,cb_S_Ldb_AC
	,cb_S_Ldb_Add_User
	,cb_S_Ldb_Adj_100_O2_Fail
	,cb_S_Ldb_Adj_100_O2_Succ
	,cb_S_Ldb_Adj_100_O2
	,cb_S_Ldb_Adj_21_O2_Fail
	,cb_S_Ldb_Adj_21_O2_Succ
	,cb_S_Ldb_Adj_21_O2
	,cb_S_Ldb_AdminpanelFile
	,cb_S_Ldb_Alarm_Audible
	,cb_S_Ldb_Apr_Abrv
	,cb_S_Ldb_Aug_Abrv
	,cb_S_Ldb_AutoZeroHiPres
	,cb_S_Ldb_AutoZeroLoPres
	,cb_S_Ldb_AutoZero_Cbuf1
	,cb_S_Ldb_AutoZero_Cbuf2
	,cb_S_Ldb_AutoZero_Cbuf3
	,cb_S_Ldb_BDU_Message
	,cb_S_Ldb_BDU_Title
	,cb_S_Ldb_BootPanelTitle
	,cb_S_Ldb_BootP_Error
	,cb_S_Ldb_BootP_Processing
	,cb_S_Ldb_BootP_Ready
	,cb_S_Ldb_BootP_Stopped
	,cb_S_Ldb_CP_Air_Msg
	,cb_S_Ldb_CP_CIR_Msg
	,cb_S_Ldb_CSE_DiagLog
	,cb_S_Ldb_RSE_Dir 
	,cb_S_Ldb_CSE_Dir
	,cb_S_Ldb_CSE_File
	,cb_S_Ldb_CSE_InfoLog
	,cb_S_Ldb_CSE_TestLog
	,cb_S_Ldb_CaliHiPres
	,cb_S_Ldb_CaliLoPres
	,cb_S_Ldb_CaliOxygen
	,cb_S_Ldb_Calibrate
//	,cb_S_Ldb_Cancel
//	,cb_S_Ldb_Cannot_open_file
	,cb_S_Ldb_Cant_Open_file
	,cb_S_Ldb_Cant_Write_File
	,cb_S_Ldb_Cant_read_registry
	,cb_S_Ldb_ChangePwdTitle
	,cb_S_Ldb_Circ_Pres
	,cb_S_Ldb_Clear
	,cb_S_Ldb_Clear_Sys_logs
	,cb_S_Ldb_Clear_sys_logs
	,cb_S_Ldb_ClearingDB
	,cb_S_Ldb_Cncling_Pls_Wait
	,cb_S_Ldb_Com_Error
	,cb_S_Ldb_Comp_not_installed
	,cb_S_Ldb_Comp_set_fail
	,cb_S_Ldb_Confirm_clear
//	,cb_S_Ldb_Connect
	,cb_S_Ldb_DB_Delete_title
	,cb_S_Ldb_Reports_Delete_title
	,cb_S_Ldb_DB_Name
	,cb_S_Ldb_DB_Sure_Delete
	,cb_S_Ldb_Reports_Sure_Delete
	,cb_S_Ldb_DB_Warning
	,cb_S_Ldb_DB_files_missing
	,cb_S_Ldb_DB_getting_big
//	,cb_S_Ldb_DLL_artfp
//	,cb_S_Ldb_DLL_cant_find_hdr
//	,cb_S_Ldb_DLL_cant_ld_hdr
//	,cb_S_Ldb_DLL_cant_load
//	,cb_S_Ldb_DLL_func_not_found
//	,cb_S_Ldb_DLL_hdr_syntax_err
//	,cb_S_Ldb_DLL_sfp
//	,cb_S_Ldb_DLL_uat
//	,cb_S_Ldb_DLL_urt
//	,cb_S_Ldb_DLL_vaf
//	,cb_S_Ldb_DLL_wo_p_proto
	,cb_S_Ldb_Database_error
	,cb_S_Ldb_Database_error_msg
	,cb_S_Ldb_Dec_Abrv
	,cb_S_Ldb_Delete_User
	,cb_S_Ldb_DemoTitle
	,cb_S_Ldb_DiagLog_Def_File
	,cb_S_Ldb_DiagLog_Def_Path
	,cb_S_Ldb_DisableAlarms
	,cb_S_Ldb_Discon_Hi_Instr
	,cb_S_Ldb_Discon_Lo_Instr
	,cb_S_Ldb_Dup_Username
	,cb_S_Ldb_ERR_PTS_BURST
	,cb_S_Ldb_ERR_TEST_TIMEOUT
	,cb_S_Ldb_ERR_UIR_LOAD
	,cb_S_Ldb_ERR_VENT_CP
	,cb_S_Ldb_ERR_VENT_GAS_SEQ
	,cb_S_Ldb_ERR_VENT_TEST_SEQ
	,cb_S_Ldb_Engine_Error
	,cb_S_Ldb_English
	,cb_S_Ldb_Ensure3LLung
	,cb_S_Ldb_Ensure4LLung
	,cb_S_Ldb_EnsurehalfLLung
	,cb_S_Ldb_Enter_Pwd_For
//	,cb_S_Ldb_Err_Reading_File
	,cb_S_Ldb_Err_in_db_2
//	,cb_S_Ldb_Err_num_ofr
	,cb_S_Ldb_Error
	,cb_S_Ldb_Error_Caps
	,cb_S_Ldb_EstabCom_840
	,cb_S_Ldb_EstabCom_PTS
//	,cb_S_Ldb_Exit
	,cb_S_Ldb_FAILED
	,cb_S_Ldb_Fail_caps
	,cb_S_Ldb_Feb_Abrv
//	,cb_S_Ldb_File_No_Found
	,cb_S_Ldb_French
	,cb_S_Ldb_GUI_Message
	,cb_S_Ldb_GUI_Title
	,cb_S_Ldb_German
	,cb_S_Ldb_Givefivebreaths
	,cb_S_Ldb_Givefiveminutes
	,cb_S_Ldb_HelpCommand
	,cb_S_Ldb_Helpfile_Name
	,cb_S_Ldb_HiPres_AutoZero_Fail
	,cb_S_Ldb_HiPres_AutoZero_Succ
	,cb_S_Ldb_IBW_10kg
	,cb_S_Ldb_IBW_20kg
	,cb_S_Ldb_IBW_85kg
	,cb_S_Ldb_ImagPanelTitle
	,cb_S_Ldb_ImagPnlImaglist_lbl
	,cb_S_Ldb_ImagPnlIPlist_lbl
//	,cb_S_Ldb_InfFile
	,cb_S_Ldb_InfoLog_Def_File
	,cb_S_Ldb_InfoLog_Def_Path
	,cb_S_Ldb_InsPanelTitle
	,cb_S_Ldb_InsPnlInstr_Line01
	,cb_S_Ldb_InsPnlInstr_Line02
	,cb_S_Ldb_InsPnlInstr_Line03
	,cb_S_Ldb_InsPnlInstr_Line04
	,cb_S_Ldb_InsPnlTextMsg
//	,cb_S_Ldb_Invalid_Engine_Info
//	,cb_S_Ldb_Invalid_Limit_Spec
//	,cb_S_Ldb_Invalid_Mod_ID
//	,cb_S_Ldb_Invalid_Module
//	,cb_S_Ldb_Invalid_Path
//	,cb_S_Ldb_Invalid_file_format
	,cb_S_Ldb_Italian
	,cb_S_Ldb_Jan_Abrv
	,cb_S_Ldb_Jul_Abrv
	,cb_S_Ldb_Jun_Abrv
//	,cb_S_Ldb_List_not_Empty
	,cb_S_Ldb_LoPres_AutoZero_Fail
	,cb_S_Ldb_LoPres_AutoZero_Succ
//	,cb_S_Ldb_Looping
	,cb_S_Ldb_MSG_ABORT
	,cb_S_Ldb_MSG_CHANGE_DFLT_RPT
	,cb_S_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY
	,cb_S_Ldb_MSG_CHANGE_DFLT_RPT_TITLE
	,cb_S_Ldb_MSG_ERROR
	,cb_S_Ldb_MSG_FAIL
//	,cb_S_Ldb_MSG_LOADSEQ_SETUP_ERR
//	,cb_S_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE
	,cb_S_Ldb_MSG_LOOPING
	,cb_S_Ldb_MSG_LOOP_ABORT
	,cb_S_Ldb_MSG_NONE
	,cb_S_Ldb_MSG_OUT_OF_MEMORY
	,cb_S_Ldb_MSG_PASS
	,cb_S_Ldb_MSG_RPT_NO_MEM
	,cb_S_Ldb_MSG_RPT_NO_MORE_MEM
	,cb_S_Ldb_MSG_RUNNING
	,cb_S_Ldb_MSG_SAVE_RPT_TITLE
//	,cb_S_Ldb_MSG_SEQ_MOVED
//	,cb_S_Ldb_MSG_SEQ_MOVED_TITLE
//	,cb_S_Ldb_MSG_SINGLE_ABORT
//	,cb_S_Ldb_MSG_SINGLE_FAIL
//	,cb_S_Ldb_MSG_SINGLE_RTE
	,cb_S_Ldb_MSG_SKIP
	,cb_S_Ldb_MSG_UUT_ABORT
	,cb_S_Ldb_MSG_UUT_FAIL
	,cb_S_Ldb_MSG_UUT_RTE
	,cb_S_Ldb_MainpanelFile
	,cb_S_Ldb_MakeOtherSettings
	,cb_S_Ldb_MakeSettings
	,cb_S_Ldb_ManType2PC
	,cb_S_Ldb_ManType2VC
	,cb_S_Ldb_Mar_Abrv
	,cb_S_Ldb_Max_Users_Reg
	,cb_S_Ldb_May_Abrv
//	,cb_S_Ldb_Met_Port
	,cb_S_Ldb_Mode2AC
//	,cb_S_Ldb_Newer_Version_Err
	,cb_S_Ldb_No_Alarm
//	,cb_S_Ldb_No_More_Tests
	,cb_S_Ldb_No_Tests
	,cb_S_Ldb_None_caps
	,cb_S_Ldb_Not_Found
//	,cb_S_Ldb_Not_Glbl_ID
//	,cb_S_Ldb_Not_a_TX_file
	,cb_S_Ldb_Nov_Abrv
	,cb_S_Ldb_O2_prcnt
//	,cb_S_Ldb_OK
	,cb_S_Ldb_OK_When_Ready
	,cb_S_Ldb_Oct_Abrv
	,cb_S_Ldb_Old_Password
	,cb_S_Ldb_Old_Password_wrng
	,cb_S_Ldb_Out_of_Memory
	,cb_S_Ldb_PASSED
	,cb_S_Ldb_PC
//	,cb_S_Ldb_PP_Test_Exists
//	,cb_S_Ldb_PP_Test_Missing
//	,cb_S_Ldb_PTH_file_open_err
//	,cb_S_Ldb_PTH_file_read_err
//	,cb_S_Ldb_PTH_invalid_contents
	,cb_S_Ldb_PTRIG
	,cb_S_Ldb_PTS_Com_Error
	,cb_S_Ldb_PTS_Comm_Rcv_Error
	,cb_S_Ldb_PTS_Date_Error
	,cb_S_Ldb_PTS_NoDetect
	,cb_S_Ldb_PTS_SN_Error
	,cb_S_Ldb_PTS_Ver_Error
	,cb_S_Ldb_Pass_caps
	,cb_S_Ldb_Password_Mismatch
	,cb_S_Ldb_PauseMsg1
	,cb_S_Ldb_PauseMsg2
//	,cb_S_Ldb_Paused
//	,cb_S_Ldb_Port
	,cb_S_Ldb_Portuguese
	,cb_S_Ldb_Pressure_incorrect
	,cb_S_Ldb_Print
	,cb_S_Ldb_Psens
	,cb_S_Ldb_Pts_cal_err
	,cb_S_Ldb_Pts_cal_expired
	,cb_S_Ldb_Reseting_PTS
	,cb_S_Ldb_Retrieving_logs
	,cb_S_Ldb_Retry_prompt
//	,cb_S_Ldb_Runtime_Err_Test
//	,cb_S_Ldb_Runtime_Err_loadseq_pre_test
//	,cb_S_Ldb_Runtime_Err_postTest
//	,cb_S_Ldb_Runtime_Err_preTest
//	,cb_S_Ldb_Runtime_Err_seq_post_test
//	,cb_S_Ldb_Runtime_Err_seq_pre_test
//	,cb_S_Ldb_Runtime_Err_unlaodseq_post_test
//	,cb_S_Ldb_SIMV
//	,cb_S_Ldb_SPONT
	,cb_S_Ldb_SQUARE
	,cb_S_Ldb_SST_Msg_1
	,cb_S_Ldb_SST_Msg_2
	,cb_S_Ldb_SST_Msg_3
	,cb_S_Ldb_SST_Title
	,cb_S_Ldb_S_840_Establishing
	,cb_S_Ldb_S_840_Failure
	,cb_S_Ldb_S_840_Success
	,cb_S_Ldb_S_PTS_Establishing
	,cb_S_Ldb_S_PTS_Failure
	,cb_S_Ldb_S_PTS_Success
	,cb_S_Ldb_S_Starting
	,cb_S_Ldb_SaveAs
	,cb_S_Ldb_Select_Diag_File
//	,cb_S_Ldb_Select_Filename
	,cb_S_Ldb_Select_Lang_for
	,cb_S_Ldb_Select_SysInfo_file
	,cb_S_Ldb_Select_testlog_file
	,cb_S_Ldb_Selection_Panel
	,cb_S_Ldb_Sep_Abrv
//	,cb_S_Ldb_SeqLoad_test_Fail
//	,cb_S_Ldb_SeqUnload_test_Fail
//	,cb_S_Ldb_Seq_posttest_Fail
//	,cb_S_Ldb_Seq_pretest_Fail
	,cb_S_Ldb_Set_Comp_SN_status
	,cb_S_Ldb_Set_Comp_hours_status
	,cb_S_Ldb_Set_Compressor
	,cb_S_Ldb_Skip_Caps
	,cb_S_Ldb_Spanish
	,cb_S_Ldb_StatePnlInit
	,cb_S_Ldb_StatePnlReplace
	,cb_S_Ldb_StatePnlTitle
	,cb_S_Ldb_Stopped_Fnsh_tst
	,cb_S_Ldb_SureClearResults
	,cb_S_Ldb_SureDeleteUser
	,cb_S_Ldb_TRIANGLE
//	,cb_S_Ldb_TX_error
	,cb_S_Ldb_Test10_Running
	,cb_S_Ldb_Test11_Running
	,cb_S_Ldb_Test12_Running
	,cb_S_Ldb_Test1_Running
	,cb_S_Ldb_Test2_Running
	,cb_S_Ldb_Test3_Running
	,cb_S_Ldb_Test4_Running
	,cb_S_Ldb_Test5_Running
	,cb_S_Ldb_Test6_Running
	,cb_S_Ldb_Test7_Running
	,cb_S_Ldb_Test8_Running
	,cb_S_Ldb_Test9_Running
	,cb_S_Ldb_TestLog_Def_File
	,cb_S_Ldb_TestLog_Def_Path
	,cb_S_Ldb_Test_Failure
//	,cb_S_Ldb_Test_Not_Verif
//	,cb_S_Ldb_Test_out_of_range
	,cb_S_Ldb_SoftdLicPanelTitle
	,cb_S_Ldb_SoftdLicAgreeTxtLine01
	,cb_S_Ldb_SoftdAgreeLicText
	,cb_S_Ldb_SoftdLicPara_1
	,cb_S_Ldb_SoftdLicPara_2
	,cb_S_Ldb_SoftdLicPara_3_1
	,cb_S_Ldb_SoftdLicPara_3_2
	,cb_S_Ldb_SoftdLicAgreeYes
	,cb_S_Ldb_SoftdLicAgreeNo
	,cb_S_Ldb_ImagFileErrTitle
	,cb_S_Ldb_ImagFileErrNofind
	,cb_S_Ldb_NoDloadCDerrTitle
	,cb_S_Ldb_NoDloadCDerrMsg
	,cb_S_Ldb_Dload_CannotEraseBDU
	,cb_S_Ldb_Dload_CannotEraseGUI
	,cb_S_Ldb_Dload_UserAbortTitle
	,cb_S_Ldb_Dload_UserAbortMsg
	,cb_S_Ldb_Dload_TimeoutAbortTitle
	,cb_S_Ldb_Dload_BDUtimeoutMsg
	,cb_S_Ldb_Dload_GUItimeoutMsg
	,cb_S_Ldb_Dload_CompleteTitle
	,cb_S_Ldb_Dload_BDUcomplete
	,cb_S_Ldb_Dload_GUIcomplete
	,cb_S_Ldb_TFTP_PnlTitle
	,cb_S_Ldb_TFTP_BDUfile_lbl
	,cb_S_Ldb_TFTP_GUIfile_lbl
	,cb_S_Ldb_TFTP_BDUpercent_lbl
	,cb_S_Ldb_TFTP_GUIpercent_lbl
	,cb_S_Ldb_TFTP_BDUblocks_lbl
	,cb_S_Ldb_TFTP_BDUtotal_lbl
	,cb_S_Ldb_TFTP_GUIblocks_lbl
	,cb_S_Ldb_TFTP_GUItotal_lbl
	,cb_S_Ldb_TFTP_BDUmsg
	,cb_S_Ldb_TFTP_BDUcomplete
	,cb_S_Ldb_TFTP_GUImsg
	,cb_S_Ldb_TFTP_GUIcomplete
	,cb_S_Ldb_Trig2PTRIG
	,cb_S_Ldb_Trig2VTRIG
//	,cb_S_Ldb_Unable_to_Verif_posttest
//	,cb_S_Ldb_Unable_to_Verif_posttest_2
//	,cb_S_Ldb_Unable_to_Verif_pretest
//	,cb_S_Ldb_Unable_to_Verif_pretest_2
//	,cb_S_Ldb_Unable_to_Verif_test
//	,cb_S_Ldb_Unable_to_Verif_test_2
//	,cb_S_Ldb_Undef_Refs
	,cb_S_Ldb_Unknown_Error
//	,cb_S_Ldb_Unknown_Ext
	,cb_S_Ldb_VC
	,cb_S_Ldb_VTRIG
	,cb_S_Ldb_VentOffandOn
	,cb_S_Ldb_Vent_Runtimehours
	,cb_S_Ldb_Vsens
	,cb_S_Ldb_Warning
//	,cb_S_Ldb_cant_decode
	,cb_S_Ldb_comp_hours
	,cb_S_Ldb_comp_hours_f
	,cb_S_Ldb_cp_test_msg
	,cb_S_Ldb_cpe_100
	,cb_S_Ldb_cpe_10
	,cb_S_Ldb_cpe_50
	,cb_S_Ldb_cpi_10
	,cb_S_Ldb_cpi_100
	,cb_S_Ldb_cpi_50
	,cb_S_Ldb_cycle_840
	,cb_S_Ldb_cycle_before_test
	,cb_S_Ldb_cycle_before_test1
	,cb_S_Ldb_enter_oxy
	,cb_S_Ldb_enter_spiro
	,cb_S_Ldb_error_cant_decode
	,cb_S_Ldb_exp
	,cb_S_Ldb_fio2_inst
	,cb_S_Ldb_insp
	,cb_S_Ldb_invalid
	,cb_S_Ldb_mantest_tt1
	,cb_S_Ldb_mantest_tt2
	,cb_S_Ldb_mantest_tt3
	,cb_S_Ldb_mantest_tt4
	,cb_S_Ldb_mantest_tt5
	,cb_S_Ldb_mantest_tt6
	,cb_S_Ldb_mantest_tt7
	,cb_S_Ldb_mantest_tt8
	,cb_S_Ldb_mnl_SIO
	,cb_S_Ldb_mnl_SIO_nd
	,cb_S_Ldb_mnl_air_reg
	,cb_S_Ldb_mnl_air_reg_nd
	,cb_S_Ldb_mnl_est
	,cb_S_Ldb_mnl_est_nd
	,cb_S_Ldb_mnl_f_leakage1
	,cb_S_Ldb_mnl_f_leakage1_nd
	,cb_S_Ldb_mnl_f_leakage2
	,cb_S_Ldb_mnl_f_leakage2_nd
	,cb_S_Ldb_mnl_g_isol_r
	,cb_S_Ldb_mnl_g_isol_r_nd
	,cb_S_Ldb_mnl_gnd_r
	,cb_S_Ldb_mnl_gnd_r_nd
	,cb_S_Ldb_mnl_O2_reg
	,cb_S_Ldb_mnl_O2_reg_nd
	,cb_S_Ldb_mnl_r_leakage1
	,cb_S_Ldb_mnl_r_leakage1_nd
	,cb_S_Ldb_mnl_r_leakage2
	,cb_S_Ldb_mnl_r_leakage2_nd
	,cb_S_Ldb_mnl_sst
	,cb_S_Ldb_mnl_sst_nd
	,cb_S_Ldb_mnl_vent_r
	,cb_S_Ldb_mnl_vent_r_nd
	,cb_S_Ldb_mnl_wrkordr_r
	,cb_S_Ldb_mnl_wrkordr_r_nd
	,cb_S_Ldb_O2_conc_vol
	,cb_S_Ldb_oxy_conc
	,cb_S_Ldb_oxy_conc_fail
	,cb_S_Ldb_oxy_incorrect
	,cb_S_Ldb_oxygen_30
	,cb_S_Ldb_oxygen_90
	,cb_S_Ldb_peep_25
	,cb_S_Ldb_peep_45
	,cb_S_Ldb_peep_5
	,cb_S_Ldb_peep_incorrect
	,cb_S_Ldb_peep_inst
//	,cb_S_Ldb_postTest_Not_Verif
//	,cb_S_Ldb_preTest_Not_Verif
	,cb_S_Ldb_pres_5
	,cb_S_Ldb_pres_90
	,cb_S_Ldb_pres_inst
	,cb_S_Ldb_programming_err
	,cb_S_Ldb_pts_cal_date
	,cb_S_Ldb_pts_next_date
	,cb_S_Ldb_pts_rdg
	,cb_S_Ldb_pts_sn
	,cb_S_Ldb_pts_timeout
	,cb_S_Ldb_pts_vn
	,cb_S_Ldb_rpt_FileName
	,cb_S_Ldb_rpt_Limit
	,cb_S_Ldb_rpt_cr_operator
	,cb_S_Ldb_rpt_date
	,cb_S_Ldb_rpt_desc
	,cb_S_Ldb_rpt_enter_fname
	,cb_S_Ldb_rpt_invalid_fname
	,cb_S_Ldb_rpt_measurement
	,cb_S_Ldb_rpt_operator
	,cb_S_Ldb_rpt_pgm_rev
	,cb_S_Ldb_rpt_print_to_file
	,cb_S_Ldb_rpt_rte_stopped
	,cb_S_Ldb_rpt_seq_name
	,cb_S_Ldb_rpt_stn_id
	,cb_S_Ldb_rpt_test_results
	,cb_S_Ldb_rpt_test_station
	,cb_S_Ldb_rpt_time
	,cb_S_Ldb_rpt_title
	,cb_S_Ldb_rpt_tp_aborted
	,cb_S_Ldb_rpt_uut_pn
	,cb_S_Ldb_rpt_uut_rev
	,cb_S_Ldb_rpt_uut_sn
	,cb_S_Ldb_rpt_why_fname_inv
//	,cb_S_Ldb_rte_cleanup
//	,cb_S_Ldb_rte_error_msg
//	,cb_S_Ldb_rte_load
//	,cb_S_Ldb_rte_seq_cleanup
//	,cb_S_Ldb_rte_seq_setup
//	,cb_S_Ldb_rte_setup
//	,cb_S_Ldb_rte_test_err
//	,cb_S_Ldb_rte_unknown
//	,cb_S_Ldb_rte_unload_seq
	,cb_S_Ldb_spirometry
	,cb_S_Ldb_sprio_rdg
	,cb_S_Ldb_sst_proc_error
	,cb_S_Ldb_station_desc
	,cb_S_Ldb_status
	,cb_S_Ldb_step_x_y
	,cb_S_Ldb_sw_fault_1
	,cb_S_Ldb_sw_fault_2
	,cb_S_Ldb_sw_fault_3
	,cb_S_Ldb_target
	,cb_S_Ldb_target_pts
	,cb_S_Ldb_test_aborted
	,cb_S_Ldb_testing_error
	,cb_S_Ldb_tt1
	,cb_S_Ldb_tt10
	,cb_S_Ldb_tt11
	,cb_S_Ldb_tt12
	,cb_S_Ldb_tt13
	,cb_S_Ldb_tt14
	,cb_S_Ldb_tt15
	,cb_S_Ldb_tt16
	,cb_S_Ldb_tt17
	,cb_S_Ldb_tt2
	,cb_S_Ldb_tt3
	,cb_S_Ldb_tt4
	,cb_S_Ldb_tt5
	,cb_S_Ldb_tt6
	,cb_S_Ldb_tt7
	,cb_S_Ldb_tt8
	,cb_S_Ldb_tt9
	,cb_S_Ldb_vent_atm
	,cb_S_Ldb_vent_fail_reported
	,cb_S_Ldb_vent_oxy_rdg
	,cb_S_Ldb_vol_inst
	,cb_S_Ldb_volume_incorrect
    ,cb_S_Ldb_CSE_AlarmLog
    ,cb_S_Ldb_Select_alarmlog_file
	,cb_S_Ldb_AlarmLog_Def_File
	,cb_S_Ldb_AlarmLog_Def_Path
	,0 
};

//static char cb_F_Ldb_AC[] = "A/C";
//static char cb_F_Ldb_Cancel[] = "Annuler";
//static char cb_F_Ldb_Connect[] = "Connecter";
//static char cb_F_Ldb_Exit[] = "Quitt";
//static char cb_F_Ldb_Invalid_Mod_ID[] = "ID module invalide";
//static char cb_F_Ldb_Looping[] = "It�ration";
//static char cb_F_Ldb_MSG_LOADSEQ_SETUP_ERR[] = "Impossible d'ex�cuter fonction config chargt s�quence.";
//static char cb_F_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE[] = "Erreur config chargt s�quence";
//static char cb_F_Ldb_MSG_SEQ_MOVED[] = "Avertissement: La s�rie a �t� d�plac�e depuis derni�re sauvgarde.\nVoulez-vous m�j chemins du fichier?\n";
//static char cb_F_Ldb_MSG_SEQ_MOVED_TITLE[] = "S�quence d�plac�e";
//static char cb_F_Ldb_MSG_SINGLE_ABORT[] = "Passe unique annul�e.\n\n";
//static char cb_F_Ldb_MSG_SINGLE_FAIL[] = "Passe unique arr�t�e par �chec du test.\n\n";
//static char cb_F_Ldb_MSG_SINGLE_RTE[] = "Passe unique arr�t�e par erreur d'ex�cution.\n\n";
//static char cb_F_Ldb_Met_Port[] = "Port expiratoire";
//static char cb_F_Ldb_Not_Glbl_ID[] = "Identificateur pas d�fini globalement ds module";
//static char cb_F_Ldb_OK[] = "OK";
//static char cb_F_Ldb_Paused[] = "PAUSE";
//static char cb_F_Ldb_Port[] = "Port";
//static char cb_F_Ldb_SIMV[] = "VACI";
//static char cb_F_Ldb_SPONT[] = "SPONT";
//static char cb_F_Ldb_Select_Filename[] = "S�lectionner nom de fichier";
//static char cb_F_Ldb_TX_error[] = "Erreur d'ex�cution du test";
//static char cb_F_Ldb_Undef_Refs[] = "R�f�rences ind�finies";
//static char cb_F_Ldb_cant_decode[] = "Erreur: d�codage erreur # %d impossible";

static char * F_langTable[] = { // French
	 0,	// 0 -- unused
	 cb_F_Ldb_ProgramName
	,cb_F_Ldb_Splash_ProgramName
	,cb_F_Ldb_Copyright_Str
	,cb_F_Ldb_Copyright_Rights
	,cb_F_Ldb_Splash_Eval_Msg
	,cb_F_Ldb_Version_Str
	,cb_F_Ldb_SelectUser
	,cb_F_Ldb_PTS2000_Name
	,cb_F_Ldb_PTS_Serial_Lbl
	,cb_F_Ldb_PTS_SWversion_Lbl
	,cb_F_Ldb_PTS_CalLast_Lbl
	,cb_F_Ldb_PTS_CalNext_Lbl
	,cb_F_Ldb_840Vent_Name
	,cb_F_Ldb_840_GUIserial_Lbl
	,cb_F_Ldb_840_BDUserial_Lbl
	,cb_F_Ldb_840_GUIsoftware_Lbl
	,cb_F_Ldb_840_BDUsoftware_Lbl
	,cb_F_Ldb_840_cycle_power
	,cb_F_Ldb_2step
	,cb_F_Ldb_3step
	,cb_F_Ldb_4step
	,cb_F_Ldb_6step
	,cb_F_Ldb_840_Com_Error
	,cb_F_Ldb_840_NoDetect
	,cb_F_Ldb_9step
	,cb_F_Ldb_ABORT
	,cb_F_Ldb_ABORTED
//	,cb_F_Ldb_AC
	,cb_F_Ldb_Add_User
	,cb_F_Ldb_Adj_100_O2_Fail
	,cb_F_Ldb_Adj_100_O2_Succ
	,cb_F_Ldb_Adj_100_O2
	,cb_F_Ldb_Adj_21_O2_Fail
	,cb_F_Ldb_Adj_21_O2_Succ
	,cb_F_Ldb_Adj_21_O2
	,cb_F_Ldb_AdminpanelFile
	,cb_F_Ldb_Alarm_Audible
	,cb_F_Ldb_Apr_Abrv
	,cb_F_Ldb_Aug_Abrv
	,cb_F_Ldb_AutoZeroHiPres
	,cb_F_Ldb_AutoZeroLoPres
	,cb_F_Ldb_AutoZero_Cbuf1
	,cb_F_Ldb_AutoZero_Cbuf2
	,cb_F_Ldb_AutoZero_Cbuf3
	,cb_F_Ldb_BDU_Message
	,cb_F_Ldb_BDU_Title
	,cb_F_Ldb_BootPanelTitle
	,cb_F_Ldb_BootP_Error
	,cb_F_Ldb_BootP_Processing
	,cb_F_Ldb_BootP_Ready
	,cb_F_Ldb_BootP_Stopped
	,cb_F_Ldb_CP_Air_Msg
	,cb_F_Ldb_CP_CIR_Msg
	,cb_F_Ldb_CSE_DiagLog
	,cb_F_Ldb_RSE_Dir 
	,cb_F_Ldb_CSE_Dir
	,cb_F_Ldb_CSE_File
	,cb_F_Ldb_CSE_InfoLog
	,cb_F_Ldb_CSE_TestLog
	,cb_F_Ldb_CaliHiPres
	,cb_F_Ldb_CaliLoPres
	,cb_F_Ldb_CaliOxygen
	,cb_F_Ldb_Calibrate
//	,cb_F_Ldb_Cancel
//	,cb_F_Ldb_Cannot_open_file
	,cb_F_Ldb_Cant_Open_file
	,cb_F_Ldb_Cant_Write_File
	,cb_F_Ldb_Cant_read_registry
	,cb_F_Ldb_ChangePwdTitle
	,cb_F_Ldb_Circ_Pres
	,cb_F_Ldb_Clear
	,cb_F_Ldb_Clear_Sys_logs
	,cb_F_Ldb_Clear_sys_logs
	,cb_F_Ldb_ClearingDB
	,cb_F_Ldb_Cncling_Pls_Wait
	,cb_F_Ldb_Com_Error
	,cb_F_Ldb_Comp_not_installed
	,cb_F_Ldb_Comp_set_fail
	,cb_F_Ldb_Confirm_clear
//	,cb_F_Ldb_Connect
	,cb_F_Ldb_DB_Delete_title
	,cb_F_Ldb_Reports_Delete_title
	,cb_F_Ldb_DB_Name
	,cb_F_Ldb_DB_Sure_Delete
	,cb_F_Ldb_Reports_Sure_Delete
	,cb_F_Ldb_DB_Warning
	,cb_F_Ldb_DB_files_missing
	,cb_F_Ldb_DB_getting_big
//	,cb_F_Ldb_DLL_artfp
//	,cb_F_Ldb_DLL_cant_find_hdr
//	,cb_F_Ldb_DLL_cant_ld_hdr
//	,cb_F_Ldb_DLL_cant_load
//	,cb_F_Ldb_DLL_func_not_found
//	,cb_F_Ldb_DLL_hdr_syntax_err
//	,cb_F_Ldb_DLL_sfp
//	,cb_F_Ldb_DLL_uat
//	,cb_F_Ldb_DLL_urt
//	,cb_F_Ldb_DLL_vaf
//	,cb_F_Ldb_DLL_wo_p_proto
	,cb_F_Ldb_Database_error
	,cb_F_Ldb_Database_error_msg
	,cb_F_Ldb_Dec_Abrv
	,cb_F_Ldb_Delete_User
	,cb_F_Ldb_DemoTitle
	,cb_F_Ldb_DiagLog_Def_File
	,cb_F_Ldb_DiagLog_Def_Path
	,cb_F_Ldb_DisableAlarms
	,cb_F_Ldb_Discon_Hi_Instr
	,cb_F_Ldb_Discon_Lo_Instr
	,cb_F_Ldb_Dup_Username
	,cb_F_Ldb_ERR_PTS_BURST
	,cb_F_Ldb_ERR_TEST_TIMEOUT
	,cb_F_Ldb_ERR_UIR_LOAD
	,cb_F_Ldb_ERR_VENT_CP
	,cb_F_Ldb_ERR_VENT_GAS_SEQ
	,cb_F_Ldb_ERR_VENT_TEST_SEQ
	,cb_F_Ldb_Engine_Error
	,cb_F_Ldb_English
	,cb_F_Ldb_Ensure3LLung
	,cb_F_Ldb_Ensure4LLung
	,cb_F_Ldb_EnsurehalfLLung
	,cb_F_Ldb_Enter_Pwd_For
//	,cb_F_Ldb_Err_Reading_File
	,cb_F_Ldb_Err_in_db_2
//	,cb_F_Ldb_Err_num_ofr
	,cb_F_Ldb_Error
	,cb_F_Ldb_Error_Caps
	,cb_F_Ldb_EstabCom_840
	,cb_F_Ldb_EstabCom_PTS
//	,cb_F_Ldb_Exit
	,cb_F_Ldb_FAILED
	,cb_F_Ldb_Fail_caps
	,cb_F_Ldb_Feb_Abrv
//	,cb_F_Ldb_File_No_Found
	,cb_F_Ldb_French
	,cb_F_Ldb_GUI_Message
	,cb_F_Ldb_GUI_Title
	,cb_F_Ldb_German
	,cb_F_Ldb_Givefivebreaths
	,cb_F_Ldb_Givefiveminutes
	,cb_F_Ldb_HelpCommand
	,cb_F_Ldb_Helpfile_Name
	,cb_F_Ldb_HiPres_AutoZero_Fail
	,cb_F_Ldb_HiPres_AutoZero_Succ
	,cb_F_Ldb_IBW_10kg
	,cb_F_Ldb_IBW_20kg
	,cb_F_Ldb_IBW_85kg
	,cb_F_Ldb_ImagPanelTitle
	,cb_F_Ldb_ImagPnlImaglist_lbl
	,cb_F_Ldb_ImagPnlIPlist_lbl
//	,cb_F_Ldb_InfFile
	,cb_F_Ldb_InfoLog_Def_File
	,cb_F_Ldb_InfoLog_Def_Path
	,cb_F_Ldb_InsPanelTitle
	,cb_F_Ldb_InsPnlInstr_Line01
	,cb_F_Ldb_InsPnlInstr_Line02
	,cb_F_Ldb_InsPnlInstr_Line03
	,cb_F_Ldb_InsPnlInstr_Line04
	,cb_F_Ldb_InsPnlTextMsg
//	,cb_F_Ldb_Invalid_Engine_Info
//	,cb_F_Ldb_Invalid_Limit_Spec
//	,cb_F_Ldb_Invalid_Mod_ID
//	,cb_F_Ldb_Invalid_Module
//	,cb_F_Ldb_Invalid_Path
//	,cb_F_Ldb_Invalid_file_format
	,cb_F_Ldb_Italian
	,cb_F_Ldb_Jan_Abrv
	,cb_F_Ldb_Jul_Abrv
	,cb_F_Ldb_Jun_Abrv
//	,cb_F_Ldb_List_not_Empty
	,cb_F_Ldb_LoPres_AutoZero_Fail
	,cb_F_Ldb_LoPres_AutoZero_Succ
//	,cb_F_Ldb_Looping
	,cb_F_Ldb_MSG_ABORT
	,cb_F_Ldb_MSG_CHANGE_DFLT_RPT
	,cb_F_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY
	,cb_F_Ldb_MSG_CHANGE_DFLT_RPT_TITLE
	,cb_F_Ldb_MSG_ERROR
	,cb_F_Ldb_MSG_FAIL
//	,cb_F_Ldb_MSG_LOADSEQ_SETUP_ERR
//	,cb_F_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE
	,cb_F_Ldb_MSG_LOOPING
	,cb_F_Ldb_MSG_LOOP_ABORT
	,cb_F_Ldb_MSG_NONE
	,cb_F_Ldb_MSG_OUT_OF_MEMORY
	,cb_F_Ldb_MSG_PASS
	,cb_F_Ldb_MSG_RPT_NO_MEM
	,cb_F_Ldb_MSG_RPT_NO_MORE_MEM
	,cb_F_Ldb_MSG_RUNNING
	,cb_F_Ldb_MSG_SAVE_RPT_TITLE
//	,cb_F_Ldb_MSG_SEQ_MOVED
//	,cb_F_Ldb_MSG_SEQ_MOVED_TITLE
//	,cb_F_Ldb_MSG_SINGLE_ABORT
//	,cb_F_Ldb_MSG_SINGLE_FAIL
//	,cb_F_Ldb_MSG_SINGLE_RTE
	,cb_F_Ldb_MSG_SKIP
	,cb_F_Ldb_MSG_UUT_ABORT
	,cb_F_Ldb_MSG_UUT_FAIL
	,cb_F_Ldb_MSG_UUT_RTE
	,cb_F_Ldb_MainpanelFile
	,cb_F_Ldb_MakeOtherSettings
	,cb_F_Ldb_MakeSettings
	,cb_F_Ldb_ManType2PC
	,cb_F_Ldb_ManType2VC
	,cb_F_Ldb_Mar_Abrv
	,cb_F_Ldb_Max_Users_Reg
	,cb_F_Ldb_May_Abrv
//	,cb_F_Ldb_Met_Port
	,cb_F_Ldb_Mode2AC
//	,cb_F_Ldb_Newer_Version_Err
	,cb_F_Ldb_No_Alarm
//	,cb_F_Ldb_No_More_Tests
	,cb_F_Ldb_No_Tests
	,cb_F_Ldb_None_caps
	,cb_F_Ldb_Not_Found
//	,cb_F_Ldb_Not_Glbl_ID
//	,cb_F_Ldb_Not_a_TX_file
	,cb_F_Ldb_Nov_Abrv
	,cb_F_Ldb_O2_prcnt
//	,cb_F_Ldb_OK
	,cb_F_Ldb_OK_When_Ready
	,cb_F_Ldb_Oct_Abrv
	,cb_F_Ldb_Old_Password
	,cb_F_Ldb_Old_Password_wrng
	,cb_F_Ldb_Out_of_Memory
	,cb_F_Ldb_PASSED
	,cb_F_Ldb_PC
//	,cb_F_Ldb_PP_Test_Exists
//	,cb_F_Ldb_PP_Test_Missing
//	,cb_F_Ldb_PTH_file_open_err
//	,cb_F_Ldb_PTH_file_read_err
//	,cb_F_Ldb_PTH_invalid_contents
	,cb_F_Ldb_PTRIG
	,cb_F_Ldb_PTS_Com_Error
	,cb_F_Ldb_PTS_Comm_Rcv_Error
	,cb_F_Ldb_PTS_Date_Error
	,cb_F_Ldb_PTS_NoDetect
	,cb_F_Ldb_PTS_SN_Error
	,cb_F_Ldb_PTS_Ver_Error
	,cb_F_Ldb_Pass_caps
	,cb_F_Ldb_Password_Mismatch
	,cb_F_Ldb_PauseMsg1
	,cb_F_Ldb_PauseMsg2
//	,cb_F_Ldb_Paused
//	,cb_F_Ldb_Port
	,cb_F_Ldb_Portuguese
	,cb_F_Ldb_Pressure_incorrect
	,cb_F_Ldb_Print
	,cb_F_Ldb_Psens
	,cb_F_Ldb_Pts_cal_err
	,cb_F_Ldb_Pts_cal_expired
	,cb_F_Ldb_Reseting_PTS
	,cb_F_Ldb_Retrieving_logs
	,cb_F_Ldb_Retry_prompt
//	,cb_F_Ldb_Runtime_Err_Test
//	,cb_F_Ldb_Runtime_Err_loadseq_pre_test
//	,cb_F_Ldb_Runtime_Err_postTest
//	,cb_F_Ldb_Runtime_Err_preTest
//	,cb_F_Ldb_Runtime_Err_seq_post_test
//	,cb_F_Ldb_Runtime_Err_seq_pre_test
//	,cb_F_Ldb_Runtime_Err_unlaodseq_post_test
//	,cb_F_Ldb_SIMV
//	,cb_F_Ldb_SPONT
	,cb_F_Ldb_SQUARE
	,cb_F_Ldb_SST_Msg_1
	,cb_F_Ldb_SST_Msg_2
	,cb_F_Ldb_SST_Msg_3
	,cb_F_Ldb_SST_Title
	,cb_F_Ldb_S_840_Establishing
	,cb_F_Ldb_S_840_Failure
	,cb_F_Ldb_S_840_Success
	,cb_F_Ldb_S_PTS_Establishing
	,cb_F_Ldb_S_PTS_Failure
	,cb_F_Ldb_S_PTS_Success
	,cb_F_Ldb_S_Starting
	,cb_F_Ldb_SaveAs
	,cb_F_Ldb_Select_Diag_File
//	,cb_F_Ldb_Select_Filename
	,cb_F_Ldb_Select_Lang_for
	,cb_F_Ldb_Select_SysInfo_file
	,cb_F_Ldb_Select_testlog_file
	,cb_F_Ldb_Selection_Panel
	,cb_F_Ldb_Sep_Abrv
//	,cb_F_Ldb_SeqLoad_test_Fail
//	,cb_F_Ldb_SeqUnload_test_Fail
//	,cb_F_Ldb_Seq_posttest_Fail
//	,cb_F_Ldb_Seq_pretest_Fail
	,cb_F_Ldb_Set_Comp_SN_status
	,cb_F_Ldb_Set_Comp_hours_status
	,cb_F_Ldb_Set_Compressor
	,cb_F_Ldb_Skip_Caps
	,cb_F_Ldb_Spanish
	,cb_F_Ldb_StatePnlInit
	,cb_F_Ldb_StatePnlReplace
	,cb_F_Ldb_StatePnlTitle
	,cb_F_Ldb_Stopped_Fnsh_tst
	,cb_F_Ldb_SureClearResults
	,cb_F_Ldb_SureDeleteUser
	,cb_F_Ldb_TRIANGLE
//	,cb_F_Ldb_TX_error
	,cb_F_Ldb_Test10_Running
	,cb_F_Ldb_Test11_Running
	,cb_F_Ldb_Test12_Running
	,cb_F_Ldb_Test1_Running
	,cb_F_Ldb_Test2_Running
	,cb_F_Ldb_Test3_Running
	,cb_F_Ldb_Test4_Running
	,cb_F_Ldb_Test5_Running
	,cb_F_Ldb_Test6_Running
	,cb_F_Ldb_Test7_Running
	,cb_F_Ldb_Test8_Running
	,cb_F_Ldb_Test9_Running
	,cb_F_Ldb_TestLog_Def_File
	,cb_F_Ldb_TestLog_Def_Path
	,cb_F_Ldb_Test_Failure
//	,cb_F_Ldb_Test_Not_Verif
//	,cb_F_Ldb_Test_out_of_range
	,cb_F_Ldb_SoftdLicPanelTitle
	,cb_F_Ldb_SoftdLicAgreeTxtLine01
	,cb_F_Ldb_SoftdAgreeLicText
	,cb_F_Ldb_SoftdLicPara_1
	,cb_F_Ldb_SoftdLicPara_2
	,cb_F_Ldb_SoftdLicPara_3_1
	,cb_F_Ldb_SoftdLicPara_3_2
	,cb_F_Ldb_SoftdLicAgreeYes
	,cb_F_Ldb_SoftdLicAgreeNo
	,cb_F_Ldb_ImagFileErrTitle
	,cb_F_Ldb_ImagFileErrNofind
	,cb_F_Ldb_NoDloadCDerrTitle
	,cb_F_Ldb_NoDloadCDerrMsg
	,cb_F_Ldb_Dload_CannotEraseBDU
	,cb_F_Ldb_Dload_CannotEraseGUI
	,cb_F_Ldb_Dload_UserAbortTitle
	,cb_F_Ldb_Dload_UserAbortMsg
	,cb_F_Ldb_Dload_TimeoutAbortTitle
	,cb_F_Ldb_Dload_BDUtimeoutMsg
	,cb_F_Ldb_Dload_GUItimeoutMsg
	,cb_F_Ldb_Dload_CompleteTitle
	,cb_F_Ldb_Dload_BDUcomplete
	,cb_F_Ldb_Dload_GUIcomplete
	,cb_F_Ldb_TFTP_PnlTitle
	,cb_F_Ldb_TFTP_BDUfile_lbl
	,cb_F_Ldb_TFTP_GUIfile_lbl
	,cb_F_Ldb_TFTP_BDUpercent_lbl
	,cb_F_Ldb_TFTP_GUIpercent_lbl
	,cb_F_Ldb_TFTP_BDUblocks_lbl
	,cb_F_Ldb_TFTP_BDUtotal_lbl
	,cb_F_Ldb_TFTP_GUIblocks_lbl
	,cb_F_Ldb_TFTP_GUItotal_lbl
	,cb_F_Ldb_TFTP_BDUmsg
	,cb_F_Ldb_TFTP_BDUcomplete
	,cb_F_Ldb_TFTP_GUImsg
	,cb_F_Ldb_TFTP_GUIcomplete
	,cb_F_Ldb_Trig2PTRIG
	,cb_F_Ldb_Trig2VTRIG
//	,cb_F_Ldb_Unable_to_Verif_posttest
//	,cb_F_Ldb_Unable_to_Verif_posttest_2
//	,cb_F_Ldb_Unable_to_Verif_pretest
//	,cb_F_Ldb_Unable_to_Verif_pretest_2
//	,cb_F_Ldb_Unable_to_Verif_test
//	,cb_F_Ldb_Unable_to_Verif_test_2
//	,cb_F_Ldb_Undef_Refs
	,cb_F_Ldb_Unknown_Error
//	,cb_F_Ldb_Unknown_Ext
	,cb_F_Ldb_VC
	,cb_F_Ldb_VTRIG
	,cb_F_Ldb_VentOffandOn
	,cb_F_Ldb_Vent_Runtimehours
	,cb_F_Ldb_Vsens
	,cb_F_Ldb_Warning
//	,cb_F_Ldb_cant_decode
	,cb_F_Ldb_comp_hours
	,cb_F_Ldb_comp_hours_f
	,cb_F_Ldb_cp_test_msg
	,cb_F_Ldb_cpe_100
	,cb_F_Ldb_cpe_10
	,cb_F_Ldb_cpe_50
	,cb_F_Ldb_cpi_10
	,cb_F_Ldb_cpi_100
	,cb_F_Ldb_cpi_50
	,cb_F_Ldb_cycle_840
	,cb_F_Ldb_cycle_before_test
	,cb_F_Ldb_cycle_before_test1
	,cb_F_Ldb_enter_oxy
	,cb_F_Ldb_enter_spiro
	,cb_F_Ldb_error_cant_decode
	,cb_F_Ldb_exp
	,cb_F_Ldb_fio2_inst
	,cb_F_Ldb_insp
	,cb_F_Ldb_invalid
	,cb_F_Ldb_mantest_tt1
	,cb_F_Ldb_mantest_tt2
	,cb_F_Ldb_mantest_tt3
	,cb_F_Ldb_mantest_tt4
	,cb_F_Ldb_mantest_tt5
	,cb_F_Ldb_mantest_tt6
	,cb_F_Ldb_mantest_tt7
	,cb_F_Ldb_mantest_tt8
	,cb_F_Ldb_mnl_SIO
	,cb_F_Ldb_mnl_SIO_nd
	,cb_F_Ldb_mnl_air_reg
	,cb_F_Ldb_mnl_air_reg_nd
	,cb_F_Ldb_mnl_est
	,cb_F_Ldb_mnl_est_nd
	,cb_F_Ldb_mnl_f_leakage1
	,cb_F_Ldb_mnl_f_leakage1_nd
	,cb_F_Ldb_mnl_f_leakage2
	,cb_F_Ldb_mnl_f_leakage2_nd
	,cb_F_Ldb_mnl_g_isol_r
	,cb_F_Ldb_mnl_g_isol_r_nd
	,cb_F_Ldb_mnl_gnd_r
	,cb_F_Ldb_mnl_gnd_r_nd
	,cb_F_Ldb_mnl_O2_reg
	,cb_F_Ldb_mnl_O2_reg_nd
	,cb_F_Ldb_mnl_r_leakage1
	,cb_F_Ldb_mnl_r_leakage1_nd
	,cb_F_Ldb_mnl_r_leakage2
	,cb_F_Ldb_mnl_r_leakage2_nd
	,cb_F_Ldb_mnl_sst
	,cb_F_Ldb_mnl_sst_nd
	,cb_F_Ldb_mnl_vent_r
	,cb_F_Ldb_mnl_vent_r_nd
	,cb_F_Ldb_mnl_wrkordr_r
	,cb_F_Ldb_mnl_wrkordr_r_nd
	,cb_F_Ldb_O2_conc_vol
	,cb_F_Ldb_oxy_conc
	,cb_F_Ldb_oxy_conc_fail
	,cb_F_Ldb_oxy_incorrect
	,cb_F_Ldb_oxygen_30
	,cb_F_Ldb_oxygen_90
	,cb_F_Ldb_peep_25
	,cb_F_Ldb_peep_45
	,cb_F_Ldb_peep_5
	,cb_F_Ldb_peep_incorrect
	,cb_F_Ldb_peep_inst
//	,cb_F_Ldb_postTest_Not_Verif
//	,cb_F_Ldb_preTest_Not_Verif
	,cb_F_Ldb_pres_5
	,cb_F_Ldb_pres_90
	,cb_F_Ldb_pres_inst
	,cb_F_Ldb_programming_err
	,cb_F_Ldb_pts_cal_date
	,cb_F_Ldb_pts_next_date
	,cb_F_Ldb_pts_rdg
	,cb_F_Ldb_pts_sn
	,cb_F_Ldb_pts_timeout
	,cb_F_Ldb_pts_vn
	,cb_F_Ldb_rpt_FileName
	,cb_F_Ldb_rpt_Limit
	,cb_F_Ldb_rpt_cr_operator
	,cb_F_Ldb_rpt_date
	,cb_F_Ldb_rpt_desc
	,cb_F_Ldb_rpt_enter_fname
	,cb_F_Ldb_rpt_invalid_fname
	,cb_F_Ldb_rpt_measurement
	,cb_F_Ldb_rpt_operator
	,cb_F_Ldb_rpt_pgm_rev
	,cb_F_Ldb_rpt_print_to_file
	,cb_F_Ldb_rpt_rte_stopped
	,cb_F_Ldb_rpt_seq_name
	,cb_F_Ldb_rpt_stn_id
	,cb_F_Ldb_rpt_test_results
	,cb_F_Ldb_rpt_test_station
	,cb_F_Ldb_rpt_time
	,cb_F_Ldb_rpt_title
	,cb_F_Ldb_rpt_tp_aborted
	,cb_F_Ldb_rpt_uut_pn
	,cb_F_Ldb_rpt_uut_rev
	,cb_F_Ldb_rpt_uut_sn
	,cb_F_Ldb_rpt_why_fname_inv
//	,cb_F_Ldb_rte_cleanup
//	,cb_F_Ldb_rte_error_msg
//	,cb_F_Ldb_rte_load
//	,cb_F_Ldb_rte_seq_cleanup
//	,cb_F_Ldb_rte_seq_setup
//	,cb_F_Ldb_rte_setup
//	,cb_F_Ldb_rte_test_err
//	,cb_F_Ldb_rte_unknown
//	,cb_F_Ldb_rte_unload_seq
	,cb_F_Ldb_spirometry
	,cb_F_Ldb_sprio_rdg
	,cb_F_Ldb_sst_proc_error
	,cb_F_Ldb_station_desc
	,cb_F_Ldb_status
	,cb_F_Ldb_step_x_y
	,cb_F_Ldb_sw_fault_1
	,cb_F_Ldb_sw_fault_2
	,cb_F_Ldb_sw_fault_3
	,cb_F_Ldb_target
	,cb_F_Ldb_target_pts
	,cb_F_Ldb_test_aborted
	,cb_F_Ldb_testing_error
	,cb_F_Ldb_tt1
	,cb_F_Ldb_tt10
	,cb_F_Ldb_tt11
	,cb_F_Ldb_tt12
	,cb_F_Ldb_tt13
	,cb_F_Ldb_tt14
	,cb_F_Ldb_tt15
	,cb_F_Ldb_tt16
	,cb_F_Ldb_tt17
	,cb_F_Ldb_tt2
	,cb_F_Ldb_tt3
	,cb_F_Ldb_tt4
	,cb_F_Ldb_tt5
	,cb_F_Ldb_tt6
	,cb_F_Ldb_tt7
	,cb_F_Ldb_tt8
	,cb_F_Ldb_tt9
	,cb_F_Ldb_vent_atm
	,cb_F_Ldb_vent_fail_reported
	,cb_F_Ldb_vent_oxy_rdg
	,cb_F_Ldb_vol_inst
	,cb_F_Ldb_volume_incorrect
    ,cb_F_Ldb_CSE_AlarmLog
    ,cb_F_Ldb_Select_alarmlog_file
	,cb_F_Ldb_AlarmLog_Def_File
	,cb_F_Ldb_AlarmLog_Def_Path
	,0 
};

//static char cb_G_Ldb_AC[] = "A/C";
//static char cb_G_Ldb_Cancel[] = "Abbrechen";
//static char cb_G_Ldb_Connect[] = "Verbinden";
//static char cb_G_Ldb_Exit[] = "Beenden";
//static char cb_G_Ldb_Invalid_Mod_ID[] = "Ung�ltige Modul-ID";
//static char cb_G_Ldb_Looping[] = "Schleife";
//static char cb_G_Ldb_MSG_LOADSEQ_SETUP_ERR[] = "Konnte Setup-Funktion f�r Sequenz-Laden nicht ausf�hren.";
//static char cb_G_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE[] = "Sequenz-Laden Setup-Fehler";
//static char cb_G_Ldb_MSG_SEQ_MOVED[] = "WARNUNG: Die Sequenz wurde seit dem letzen Speichern verschoben.\nWollen Sie die Dateipfade aktualisieren?\n";
//static char cb_G_Ldb_MSG_SEQ_MOVED_TITLE[] = "Sequenz verschoben";
//static char cb_G_Ldb_MSG_SINGLE_ABORT[] = "Einzeldurchlauf abgebrochen.\n\n";
//static char cb_G_Ldb_MSG_SINGLE_FAIL[] = "Einzeldurchlauf durch mi�lungenen Test beendet.\n\n";
//static char cb_G_Ldb_MSG_SINGLE_RTE[] = "Einzeldurchlauf durch Laufzeitfehler beendet.\n\n";
//static char cb_G_Ldb_Met_Port[] = "Exspirationsanschlu�";
//static char cb_G_Ldb_Not_Glbl_ID[] = "Bezeichner im Modul nicht global definiert";
//static char cb_G_Ldb_OK[] = "OK";
//static char cb_G_Ldb_Paused[] = "PAUSE";
//static char cb_G_Ldb_Port[] = "Port";
//static char cb_G_Ldb_SIMV[] = "SIMV";
//static char cb_G_Ldb_SPONT[] = "SPONT";
//static char cb_G_Ldb_Select_Filename[] = "Dateinamen ausw�hlen";
//static char cb_G_Ldb_TX_error[] = "Executive-Error (Test)";
//static char cb_G_Ldb_Undef_Refs[] = "Undefinierte Referenzen";
//static char cb_G_Ldb_cant_decode[] = "Fehler: Kann Fehler Nr. %d nicht dekodieren";

static char * G_langTable[] = { // German
	 0,	// 0 -- unused
	 cb_G_Ldb_ProgramName
	,cb_G_Ldb_Splash_ProgramName
	,cb_G_Ldb_Copyright_Str
	,cb_G_Ldb_Copyright_Rights
	,cb_G_Ldb_Splash_Eval_Msg
	,cb_G_Ldb_Version_Str
	,cb_G_Ldb_SelectUser
	,cb_G_Ldb_PTS2000_Name
	,cb_G_Ldb_PTS_Serial_Lbl
	,cb_G_Ldb_PTS_SWversion_Lbl
	,cb_G_Ldb_PTS_CalLast_Lbl
	,cb_G_Ldb_PTS_CalNext_Lbl
	,cb_G_Ldb_840Vent_Name
	,cb_G_Ldb_840_GUIserial_Lbl
	,cb_G_Ldb_840_BDUserial_Lbl
	,cb_G_Ldb_840_GUIsoftware_Lbl
	,cb_G_Ldb_840_BDUsoftware_Lbl
	,cb_G_Ldb_840_cycle_power
	,cb_G_Ldb_2step
	,cb_G_Ldb_3step
	,cb_G_Ldb_4step
	,cb_G_Ldb_6step
	,cb_G_Ldb_840_Com_Error
	,cb_G_Ldb_840_NoDetect
	,cb_G_Ldb_9step
	,cb_G_Ldb_ABORT
	,cb_G_Ldb_ABORTED
//	,cb_G_Ldb_AC
	,cb_G_Ldb_Add_User
	,cb_G_Ldb_Adj_100_O2_Fail
	,cb_G_Ldb_Adj_100_O2_Succ
	,cb_G_Ldb_Adj_100_O2
	,cb_G_Ldb_Adj_21_O2_Fail
	,cb_G_Ldb_Adj_21_O2_Succ
	,cb_G_Ldb_Adj_21_O2
	,cb_G_Ldb_AdminpanelFile
	,cb_G_Ldb_Alarm_Audible
	,cb_G_Ldb_Apr_Abrv
	,cb_G_Ldb_Aug_Abrv
	,cb_G_Ldb_AutoZeroHiPres
	,cb_G_Ldb_AutoZeroLoPres
	,cb_G_Ldb_AutoZero_Cbuf1
	,cb_G_Ldb_AutoZero_Cbuf2
	,cb_G_Ldb_AutoZero_Cbuf3
	,cb_G_Ldb_BDU_Message
	,cb_G_Ldb_BDU_Title
	,cb_G_Ldb_BootPanelTitle
	,cb_G_Ldb_BootP_Error
	,cb_G_Ldb_BootP_Processing
	,cb_G_Ldb_BootP_Ready
	,cb_G_Ldb_BootP_Stopped
	,cb_G_Ldb_CP_Air_Msg
	,cb_G_Ldb_CP_CIR_Msg
	,cb_G_Ldb_CSE_DiagLog
	,cb_G_Ldb_RSE_Dir 
	,cb_G_Ldb_CSE_Dir
	,cb_G_Ldb_CSE_File
	,cb_G_Ldb_CSE_InfoLog
	,cb_G_Ldb_CSE_TestLog
	,cb_G_Ldb_CaliHiPres
	,cb_G_Ldb_CaliLoPres
	,cb_G_Ldb_CaliOxygen
	,cb_G_Ldb_Calibrate
//	,cb_G_Ldb_Cancel
//	,cb_G_Ldb_Cannot_open_file
	,cb_G_Ldb_Cant_Open_file
	,cb_G_Ldb_Cant_Write_File
	,cb_G_Ldb_Cant_read_registry
	,cb_G_Ldb_ChangePwdTitle
	,cb_G_Ldb_Circ_Pres
	,cb_G_Ldb_Clear
	,cb_G_Ldb_Clear_Sys_logs
	,cb_G_Ldb_Clear_sys_logs
	,cb_G_Ldb_ClearingDB
	,cb_G_Ldb_Cncling_Pls_Wait
	,cb_G_Ldb_Com_Error
	,cb_G_Ldb_Comp_not_installed
	,cb_G_Ldb_Comp_set_fail
	,cb_G_Ldb_Confirm_clear
//	,cb_G_Ldb_Connect
	,cb_G_Ldb_DB_Delete_title
	,cb_G_Ldb_Reports_Delete_title
	,cb_G_Ldb_DB_Name
	,cb_G_Ldb_DB_Sure_Delete
	,cb_G_Ldb_Reports_Sure_Delete
	,cb_G_Ldb_DB_Warning
	,cb_G_Ldb_DB_files_missing
	,cb_G_Ldb_DB_getting_big
//	,cb_G_Ldb_DLL_artfp
//	,cb_G_Ldb_DLL_cant_find_hdr
//	,cb_G_Ldb_DLL_cant_ld_hdr
//	,cb_G_Ldb_DLL_cant_load
//	,cb_G_Ldb_DLL_func_not_found
//	,cb_G_Ldb_DLL_hdr_syntax_err
//	,cb_G_Ldb_DLL_sfp
//	,cb_G_Ldb_DLL_uat
//	,cb_G_Ldb_DLL_urt
//	,cb_G_Ldb_DLL_vaf
//	,cb_G_Ldb_DLL_wo_p_proto
	,cb_G_Ldb_Database_error
	,cb_G_Ldb_Database_error_msg
	,cb_G_Ldb_Dec_Abrv
	,cb_G_Ldb_Delete_User
	,cb_G_Ldb_DemoTitle
	,cb_G_Ldb_DiagLog_Def_File
	,cb_G_Ldb_DiagLog_Def_Path
	,cb_G_Ldb_DisableAlarms
	,cb_G_Ldb_Discon_Hi_Instr
	,cb_G_Ldb_Discon_Lo_Instr
	,cb_G_Ldb_Dup_Username
	,cb_G_Ldb_ERR_PTS_BURST
	,cb_G_Ldb_ERR_TEST_TIMEOUT
	,cb_G_Ldb_ERR_UIR_LOAD
	,cb_G_Ldb_ERR_VENT_CP
	,cb_G_Ldb_ERR_VENT_GAS_SEQ
	,cb_G_Ldb_ERR_VENT_TEST_SEQ
	,cb_G_Ldb_Engine_Error
	,cb_G_Ldb_English
	,cb_G_Ldb_Ensure3LLung
	,cb_G_Ldb_Ensure4LLung
	,cb_G_Ldb_EnsurehalfLLung
	,cb_G_Ldb_Enter_Pwd_For
//	,cb_G_Ldb_Err_Reading_File
	,cb_G_Ldb_Err_in_db_2
//	,cb_G_Ldb_Err_num_ofr
	,cb_G_Ldb_Error
	,cb_G_Ldb_Error_Caps
	,cb_G_Ldb_EstabCom_840
	,cb_G_Ldb_EstabCom_PTS
//	,cb_G_Ldb_Exit
	,cb_G_Ldb_FAILED
	,cb_G_Ldb_Fail_caps
	,cb_G_Ldb_Feb_Abrv
//	,cb_G_Ldb_File_No_Found
	,cb_G_Ldb_French
	,cb_G_Ldb_GUI_Message
	,cb_G_Ldb_GUI_Title
	,cb_G_Ldb_German
	,cb_G_Ldb_Givefivebreaths
	,cb_G_Ldb_Givefiveminutes
	,cb_G_Ldb_HelpCommand
	,cb_G_Ldb_Helpfile_Name
	,cb_G_Ldb_HiPres_AutoZero_Fail
	,cb_G_Ldb_HiPres_AutoZero_Succ
	,cb_G_Ldb_IBW_10kg
	,cb_G_Ldb_IBW_20kg
	,cb_G_Ldb_IBW_85kg
	,cb_G_Ldb_ImagPanelTitle
	,cb_G_Ldb_ImagPnlImaglist_lbl
	,cb_G_Ldb_ImagPnlIPlist_lbl
//	,cb_G_Ldb_InfFile
	,cb_G_Ldb_InfoLog_Def_File
	,cb_G_Ldb_InfoLog_Def_Path
	,cb_G_Ldb_InsPanelTitle
	,cb_G_Ldb_InsPnlInstr_Line01
	,cb_G_Ldb_InsPnlInstr_Line02
	,cb_G_Ldb_InsPnlInstr_Line03
	,cb_G_Ldb_InsPnlInstr_Line04
	,cb_G_Ldb_InsPnlTextMsg
//	,cb_G_Ldb_Invalid_Engine_Info
//	,cb_G_Ldb_Invalid_Limit_Spec
//	,cb_G_Ldb_Invalid_Mod_ID
//	,cb_G_Ldb_Invalid_Module
//	,cb_G_Ldb_Invalid_Path
//	,cb_G_Ldb_Invalid_file_format
	,cb_G_Ldb_Italian
	,cb_G_Ldb_Jan_Abrv
	,cb_G_Ldb_Jul_Abrv
	,cb_G_Ldb_Jun_Abrv
//	,cb_G_Ldb_List_not_Empty
	,cb_G_Ldb_LoPres_AutoZero_Fail
	,cb_G_Ldb_LoPres_AutoZero_Succ
//	,cb_G_Ldb_Looping
	,cb_G_Ldb_MSG_ABORT
	,cb_G_Ldb_MSG_CHANGE_DFLT_RPT
	,cb_G_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY
	,cb_G_Ldb_MSG_CHANGE_DFLT_RPT_TITLE
	,cb_G_Ldb_MSG_ERROR
	,cb_G_Ldb_MSG_FAIL
//	,cb_G_Ldb_MSG_LOADSEQ_SETUP_ERR
//	,cb_G_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE
	,cb_G_Ldb_MSG_LOOPING
	,cb_G_Ldb_MSG_LOOP_ABORT
	,cb_G_Ldb_MSG_NONE
	,cb_G_Ldb_MSG_OUT_OF_MEMORY
	,cb_G_Ldb_MSG_PASS
	,cb_G_Ldb_MSG_RPT_NO_MEM
	,cb_G_Ldb_MSG_RPT_NO_MORE_MEM
	,cb_G_Ldb_MSG_RUNNING
	,cb_G_Ldb_MSG_SAVE_RPT_TITLE
//	,cb_G_Ldb_MSG_SEQ_MOVED
//	,cb_G_Ldb_MSG_SEQ_MOVED_TITLE
//	,cb_G_Ldb_MSG_SINGLE_ABORT
//	,cb_G_Ldb_MSG_SINGLE_FAIL
//	,cb_G_Ldb_MSG_SINGLE_RTE
	,cb_G_Ldb_MSG_SKIP
	,cb_G_Ldb_MSG_UUT_ABORT
	,cb_G_Ldb_MSG_UUT_FAIL
	,cb_G_Ldb_MSG_UUT_RTE
	,cb_G_Ldb_MainpanelFile
	,cb_G_Ldb_MakeOtherSettings
	,cb_G_Ldb_MakeSettings
	,cb_G_Ldb_ManType2PC
	,cb_G_Ldb_ManType2VC
	,cb_G_Ldb_Mar_Abrv
	,cb_G_Ldb_Max_Users_Reg
	,cb_G_Ldb_May_Abrv
//	,cb_G_Ldb_Met_Port
	,cb_G_Ldb_Mode2AC
//	,cb_G_Ldb_Newer_Version_Err
	,cb_G_Ldb_No_Alarm
//	,cb_G_Ldb_No_More_Tests
	,cb_G_Ldb_No_Tests
	,cb_G_Ldb_None_caps
	,cb_G_Ldb_Not_Found
//	,cb_G_Ldb_Not_Glbl_ID
//	,cb_G_Ldb_Not_a_TX_file
	,cb_G_Ldb_Nov_Abrv
	,cb_G_Ldb_O2_prcnt
//	,cb_G_Ldb_OK
	,cb_G_Ldb_OK_When_Ready
	,cb_G_Ldb_Oct_Abrv
	,cb_G_Ldb_Old_Password
	,cb_G_Ldb_Old_Password_wrng
	,cb_G_Ldb_Out_of_Memory
	,cb_G_Ldb_PASSED
	,cb_G_Ldb_PC
//	,cb_G_Ldb_PP_Test_Exists
//	,cb_G_Ldb_PP_Test_Missing
//	,cb_G_Ldb_PTH_file_open_err
//	,cb_G_Ldb_PTH_file_read_err
//	,cb_G_Ldb_PTH_invalid_contents
	,cb_G_Ldb_PTRIG
	,cb_G_Ldb_PTS_Com_Error
	,cb_G_Ldb_PTS_Comm_Rcv_Error
	,cb_G_Ldb_PTS_Date_Error
	,cb_G_Ldb_PTS_NoDetect
	,cb_G_Ldb_PTS_SN_Error
	,cb_G_Ldb_PTS_Ver_Error
	,cb_G_Ldb_Pass_caps
	,cb_G_Ldb_Password_Mismatch
	,cb_G_Ldb_PauseMsg1
	,cb_G_Ldb_PauseMsg2
//	,cb_G_Ldb_Paused
//	,cb_G_Ldb_Port
	,cb_G_Ldb_Portuguese
	,cb_G_Ldb_Pressure_incorrect
	,cb_G_Ldb_Print
	,cb_G_Ldb_Psens
	,cb_G_Ldb_Pts_cal_err
	,cb_G_Ldb_Pts_cal_expired
	,cb_G_Ldb_Reseting_PTS
	,cb_G_Ldb_Retrieving_logs
	,cb_G_Ldb_Retry_prompt
//	,cb_G_Ldb_Runtime_Err_Test
//	,cb_G_Ldb_Runtime_Err_loadseq_pre_test
//	,cb_G_Ldb_Runtime_Err_postTest
//	,cb_G_Ldb_Runtime_Err_preTest
//	,cb_G_Ldb_Runtime_Err_seq_post_test
//	,cb_G_Ldb_Runtime_Err_seq_pre_test
//	,cb_G_Ldb_Runtime_Err_unlaodseq_post_test
//	,cb_G_Ldb_SIMV
//	,cb_G_Ldb_SPONT
	,cb_G_Ldb_SQUARE
	,cb_G_Ldb_SST_Msg_1
	,cb_G_Ldb_SST_Msg_2
	,cb_G_Ldb_SST_Msg_3
	,cb_G_Ldb_SST_Title
	,cb_G_Ldb_S_840_Establishing
	,cb_G_Ldb_S_840_Failure
	,cb_G_Ldb_S_840_Success
	,cb_G_Ldb_S_PTS_Establishing
	,cb_G_Ldb_S_PTS_Failure
	,cb_G_Ldb_S_PTS_Success
	,cb_G_Ldb_S_Starting
	,cb_G_Ldb_SaveAs
	,cb_G_Ldb_Select_Diag_File
//	,cb_G_Ldb_Select_Filename
	,cb_G_Ldb_Select_Lang_for
	,cb_G_Ldb_Select_SysInfo_file
	,cb_G_Ldb_Select_testlog_file
	,cb_G_Ldb_Selection_Panel
	,cb_G_Ldb_Sep_Abrv
//	,cb_G_Ldb_SeqLoad_test_Fail
//	,cb_G_Ldb_SeqUnload_test_Fail
//	,cb_G_Ldb_Seq_posttest_Fail
//	,cb_G_Ldb_Seq_pretest_Fail
	,cb_G_Ldb_Set_Comp_SN_status
	,cb_G_Ldb_Set_Comp_hours_status
	,cb_G_Ldb_Set_Compressor
	,cb_G_Ldb_Skip_Caps
	,cb_G_Ldb_Spanish
	,cb_G_Ldb_StatePnlInit
	,cb_G_Ldb_StatePnlReplace
	,cb_G_Ldb_StatePnlTitle
	,cb_G_Ldb_Stopped_Fnsh_tst
	,cb_G_Ldb_SureClearResults
	,cb_G_Ldb_SureDeleteUser
	,cb_G_Ldb_TRIANGLE
//	,cb_G_Ldb_TX_error
	,cb_G_Ldb_Test10_Running
	,cb_G_Ldb_Test11_Running
	,cb_G_Ldb_Test12_Running
	,cb_G_Ldb_Test1_Running
	,cb_G_Ldb_Test2_Running
	,cb_G_Ldb_Test3_Running
	,cb_G_Ldb_Test4_Running
	,cb_G_Ldb_Test5_Running
	,cb_G_Ldb_Test6_Running
	,cb_G_Ldb_Test7_Running
	,cb_G_Ldb_Test8_Running
	,cb_G_Ldb_Test9_Running
	,cb_G_Ldb_TestLog_Def_File
	,cb_G_Ldb_TestLog_Def_Path
	,cb_G_Ldb_Test_Failure
//	,cb_G_Ldb_Test_Not_Verif
//	,cb_G_Ldb_Test_out_of_range
	,cb_G_Ldb_SoftdLicPanelTitle
	,cb_G_Ldb_SoftdLicAgreeTxtLine01
	,cb_G_Ldb_SoftdAgreeLicText
	,cb_G_Ldb_SoftdLicPara_1
	,cb_G_Ldb_SoftdLicPara_2
	,cb_G_Ldb_SoftdLicPara_3_1
	,cb_G_Ldb_SoftdLicPara_3_2
	,cb_G_Ldb_SoftdLicAgreeYes
	,cb_G_Ldb_SoftdLicAgreeNo
	,cb_G_Ldb_ImagFileErrTitle
	,cb_G_Ldb_ImagFileErrNofind
	,cb_G_Ldb_NoDloadCDerrTitle
	,cb_G_Ldb_NoDloadCDerrMsg
	,cb_G_Ldb_Dload_CannotEraseBDU
	,cb_G_Ldb_Dload_CannotEraseGUI
	,cb_G_Ldb_Dload_UserAbortTitle
	,cb_G_Ldb_Dload_UserAbortMsg
	,cb_G_Ldb_Dload_TimeoutAbortTitle
	,cb_G_Ldb_Dload_BDUtimeoutMsg
	,cb_G_Ldb_Dload_GUItimeoutMsg
	,cb_G_Ldb_Dload_CompleteTitle
	,cb_G_Ldb_Dload_BDUcomplete
	,cb_G_Ldb_Dload_GUIcomplete
	,cb_G_Ldb_TFTP_PnlTitle
	,cb_G_Ldb_TFTP_BDUfile_lbl
	,cb_G_Ldb_TFTP_GUIfile_lbl
	,cb_G_Ldb_TFTP_BDUpercent_lbl
	,cb_G_Ldb_TFTP_GUIpercent_lbl
	,cb_G_Ldb_TFTP_BDUblocks_lbl
	,cb_G_Ldb_TFTP_BDUtotal_lbl
	,cb_G_Ldb_TFTP_GUIblocks_lbl
	,cb_G_Ldb_TFTP_GUItotal_lbl
	,cb_G_Ldb_TFTP_BDUmsg
	,cb_G_Ldb_TFTP_BDUcomplete
	,cb_G_Ldb_TFTP_GUImsg
	,cb_G_Ldb_TFTP_GUIcomplete
	,cb_G_Ldb_Trig2PTRIG
	,cb_G_Ldb_Trig2VTRIG
//	,cb_G_Ldb_Unable_to_Verif_posttest
//	,cb_G_Ldb_Unable_to_Verif_posttest_2
//	,cb_G_Ldb_Unable_to_Verif_pretest
//	,cb_G_Ldb_Unable_to_Verif_pretest_2
//	,cb_G_Ldb_Unable_to_Verif_test
//	,cb_G_Ldb_Unable_to_Verif_test_2
//	,cb_G_Ldb_Undef_Refs
	,cb_G_Ldb_Unknown_Error
//	,cb_G_Ldb_Unknown_Ext
	,cb_G_Ldb_VC
	,cb_G_Ldb_VTRIG
	,cb_G_Ldb_VentOffandOn
	,cb_G_Ldb_Vent_Runtimehours
	,cb_G_Ldb_Vsens
	,cb_G_Ldb_Warning
//	,cb_G_Ldb_cant_decode
	,cb_G_Ldb_comp_hours
	,cb_G_Ldb_comp_hours_f
	,cb_G_Ldb_cp_test_msg
	,cb_G_Ldb_cpe_100
	,cb_G_Ldb_cpe_10
	,cb_G_Ldb_cpe_50
	,cb_G_Ldb_cpi_10
	,cb_G_Ldb_cpi_100
	,cb_G_Ldb_cpi_50
	,cb_G_Ldb_cycle_840
	,cb_G_Ldb_cycle_before_test
	,cb_G_Ldb_cycle_before_test1
	,cb_G_Ldb_enter_oxy
	,cb_G_Ldb_enter_spiro
	,cb_G_Ldb_error_cant_decode
	,cb_G_Ldb_exp
	,cb_G_Ldb_fio2_inst
	,cb_G_Ldb_insp
	,cb_G_Ldb_invalid
	,cb_G_Ldb_mantest_tt1
	,cb_G_Ldb_mantest_tt2
	,cb_G_Ldb_mantest_tt3
	,cb_G_Ldb_mantest_tt4
	,cb_G_Ldb_mantest_tt5
	,cb_G_Ldb_mantest_tt6
	,cb_G_Ldb_mantest_tt7
	,cb_G_Ldb_mantest_tt8
	,cb_G_Ldb_mnl_SIO
	,cb_G_Ldb_mnl_SIO_nd
	,cb_G_Ldb_mnl_air_reg
	,cb_G_Ldb_mnl_air_reg_nd
	,cb_G_Ldb_mnl_est
	,cb_G_Ldb_mnl_est_nd
	,cb_G_Ldb_mnl_f_leakage1
	,cb_G_Ldb_mnl_f_leakage1_nd
	,cb_G_Ldb_mnl_f_leakage2
	,cb_G_Ldb_mnl_f_leakage2_nd
	,cb_G_Ldb_mnl_g_isol_r
	,cb_G_Ldb_mnl_g_isol_r_nd
	,cb_G_Ldb_mnl_gnd_r
	,cb_G_Ldb_mnl_gnd_r_nd
	,cb_G_Ldb_mnl_O2_reg
	,cb_G_Ldb_mnl_O2_reg_nd
	,cb_G_Ldb_mnl_r_leakage1
	,cb_G_Ldb_mnl_r_leakage1_nd
	,cb_G_Ldb_mnl_r_leakage2
	,cb_G_Ldb_mnl_r_leakage2_nd
	,cb_G_Ldb_mnl_sst
	,cb_G_Ldb_mnl_sst_nd
	,cb_G_Ldb_mnl_vent_r
	,cb_G_Ldb_mnl_vent_r_nd
	,cb_G_Ldb_mnl_wrkordr_r
	,cb_G_Ldb_mnl_wrkordr_r_nd
	,cb_G_Ldb_O2_conc_vol
	,cb_G_Ldb_oxy_conc
	,cb_G_Ldb_oxy_conc_fail
	,cb_G_Ldb_oxy_incorrect
	,cb_G_Ldb_oxygen_30
	,cb_G_Ldb_oxygen_90
	,cb_G_Ldb_peep_25
	,cb_G_Ldb_peep_45
	,cb_G_Ldb_peep_5
	,cb_G_Ldb_peep_incorrect
	,cb_G_Ldb_peep_inst
//	,cb_G_Ldb_postTest_Not_Verif
//	,cb_G_Ldb_preTest_Not_Verif
	,cb_G_Ldb_pres_5
	,cb_G_Ldb_pres_90
	,cb_G_Ldb_pres_inst
	,cb_G_Ldb_programming_err
	,cb_G_Ldb_pts_cal_date
	,cb_G_Ldb_pts_next_date
	,cb_G_Ldb_pts_rdg
	,cb_G_Ldb_pts_sn
	,cb_G_Ldb_pts_timeout
	,cb_G_Ldb_pts_vn
	,cb_G_Ldb_rpt_FileName
	,cb_G_Ldb_rpt_Limit
	,cb_G_Ldb_rpt_cr_operator
	,cb_G_Ldb_rpt_date
	,cb_G_Ldb_rpt_desc
	,cb_G_Ldb_rpt_enter_fname
	,cb_G_Ldb_rpt_invalid_fname
	,cb_G_Ldb_rpt_measurement
	,cb_G_Ldb_rpt_operator
	,cb_G_Ldb_rpt_pgm_rev
	,cb_G_Ldb_rpt_print_to_file
	,cb_G_Ldb_rpt_rte_stopped
	,cb_G_Ldb_rpt_seq_name
	,cb_G_Ldb_rpt_stn_id
	,cb_G_Ldb_rpt_test_results
	,cb_G_Ldb_rpt_test_station
	,cb_G_Ldb_rpt_time
	,cb_G_Ldb_rpt_title
	,cb_G_Ldb_rpt_tp_aborted
	,cb_G_Ldb_rpt_uut_pn
	,cb_G_Ldb_rpt_uut_rev
	,cb_G_Ldb_rpt_uut_sn
	,cb_G_Ldb_rpt_why_fname_inv
//	,cb_G_Ldb_rte_cleanup
//	,cb_G_Ldb_rte_error_msg
//	,cb_G_Ldb_rte_load
//	,cb_G_Ldb_rte_seq_cleanup
//	,cb_G_Ldb_rte_seq_setup
//	,cb_G_Ldb_rte_setup
//	,cb_G_Ldb_rte_test_err
//	,cb_G_Ldb_rte_unknown
//	,cb_G_Ldb_rte_unload_seq
	,cb_G_Ldb_spirometry
	,cb_G_Ldb_sprio_rdg
	,cb_G_Ldb_sst_proc_error
	,cb_G_Ldb_station_desc
	,cb_G_Ldb_status
	,cb_G_Ldb_step_x_y
	,cb_G_Ldb_sw_fault_1
	,cb_G_Ldb_sw_fault_2
	,cb_G_Ldb_sw_fault_3
	,cb_G_Ldb_target
	,cb_G_Ldb_target_pts
	,cb_G_Ldb_test_aborted
	,cb_G_Ldb_testing_error
	,cb_G_Ldb_tt1
	,cb_G_Ldb_tt10
	,cb_G_Ldb_tt11
	,cb_G_Ldb_tt12
	,cb_G_Ldb_tt13
	,cb_G_Ldb_tt14
	,cb_G_Ldb_tt15
	,cb_G_Ldb_tt16
	,cb_G_Ldb_tt17
	,cb_G_Ldb_tt2
	,cb_G_Ldb_tt3
	,cb_G_Ldb_tt4
	,cb_G_Ldb_tt5
	,cb_G_Ldb_tt6
	,cb_G_Ldb_tt7
	,cb_G_Ldb_tt8
	,cb_G_Ldb_tt9
	,cb_G_Ldb_vent_atm
	,cb_G_Ldb_vent_fail_reported
	,cb_G_Ldb_vent_oxy_rdg
	,cb_G_Ldb_vol_inst
	,cb_G_Ldb_volume_incorrect
    ,cb_G_Ldb_CSE_AlarmLog
    ,cb_G_Ldb_Select_alarmlog_file
	,cb_G_Ldb_AlarmLog_Def_File
	,cb_G_Ldb_AlarmLog_Def_Path
	,0 
};

//static char cb_I_Ldb_AC[] = "A/C";
//static char cb_I_Ldb_Cancel[] = "Annulla";
//static char cb_I_Ldb_Connect[] = "Collega";
//static char cb_I_Ldb_Exit[] = "Esci";
//static char cb_I_Ldb_Invalid_Mod_ID[] = "ID modulo non valido";
//static char cb_I_Ldb_Looping[] = "Loop";
//static char cb_I_Ldb_MSG_LOADSEQ_SETUP_ERR[] = "Impossibile eseguire la funzione di configurazione caricamento sequenza.";
//static char cb_I_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE[] = "Errore configurazione caricamento sequenza";
//static char cb_I_Ldb_MSG_SEQ_MOVED[] = "Avvertenza: la sequenza � stata spostata in seguito all'ultimo salvataggio.\nAggiornare i percorsi dei file?\n";
//static char cb_I_Ldb_MSG_SEQ_MOVED_TITLE[] = "Sequenza spostata";
//static char cb_I_Ldb_MSG_SINGLE_ABORT[] = "Esecuzione singola interrotta.\n\n";
//static char cb_I_Ldb_MSG_SINGLE_FAIL[] = "Esecuzione singola interrotta da errore di test.\n\n";
//static char cb_I_Ldb_MSG_SINGLE_RTE[] = "Esecuzione singola interrotta da un errore di run time.\n\n";
//static char cb_I_Ldb_Met_Port[] = "Porta esalazione";
//static char cb_I_Ldb_Not_Glbl_ID[] = "Identificatore non definito globalmente nel modulo";
//static char cb_I_Ldb_OK[] = "OK";
//static char cb_I_Ldb_Paused[] = "PAUSA";
//static char cb_I_Ldb_Port[] = "Porta";
//static char cb_I_Ldb_SIMV[] = "SIMV";
//static char cb_I_Ldb_SPONT[] = "SPONT";
//static char cb_I_Ldb_Select_Filename[] = "Selezionare nome file";
//static char cb_I_Ldb_TX_error[] = "Errore esecutivo test";
//static char cb_I_Ldb_Undef_Refs[] = "Riferimenti non definiti";
//static char cb_I_Ldb_cant_decode[] = "Errore: impossibile decodificare l'errore # %d";

static char * I_langTable[] = { // Italian
	 0,	// 0 -- unused
	 cb_I_Ldb_ProgramName
	,cb_I_Ldb_Splash_ProgramName
	,cb_I_Ldb_Copyright_Str
	,cb_I_Ldb_Copyright_Rights
	,cb_I_Ldb_Splash_Eval_Msg
	,cb_I_Ldb_Version_Str
	,cb_I_Ldb_SelectUser
	,cb_I_Ldb_PTS2000_Name
	,cb_I_Ldb_PTS_Serial_Lbl
	,cb_I_Ldb_PTS_SWversion_Lbl
	,cb_I_Ldb_PTS_CalLast_Lbl
	,cb_I_Ldb_PTS_CalNext_Lbl
	,cb_I_Ldb_840Vent_Name
	,cb_I_Ldb_840_GUIserial_Lbl
	,cb_I_Ldb_840_BDUserial_Lbl
	,cb_I_Ldb_840_GUIsoftware_Lbl
	,cb_I_Ldb_840_BDUsoftware_Lbl
	,cb_I_Ldb_840_cycle_power
	,cb_I_Ldb_2step
	,cb_I_Ldb_3step
	,cb_I_Ldb_4step
	,cb_I_Ldb_6step
	,cb_I_Ldb_840_Com_Error
	,cb_I_Ldb_840_NoDetect
	,cb_I_Ldb_9step
	,cb_I_Ldb_ABORT
	,cb_I_Ldb_ABORTED
//	,cb_I_Ldb_AC
	,cb_I_Ldb_Add_User
	,cb_I_Ldb_Adj_100_O2_Fail
	,cb_I_Ldb_Adj_100_O2_Succ
	,cb_I_Ldb_Adj_100_O2
	,cb_I_Ldb_Adj_21_O2_Fail
	,cb_I_Ldb_Adj_21_O2_Succ
	,cb_I_Ldb_Adj_21_O2
	,cb_I_Ldb_AdminpanelFile
	,cb_I_Ldb_Alarm_Audible
	,cb_I_Ldb_Apr_Abrv
	,cb_I_Ldb_Aug_Abrv
	,cb_I_Ldb_AutoZeroHiPres
	,cb_I_Ldb_AutoZeroLoPres
	,cb_I_Ldb_AutoZero_Cbuf1
	,cb_I_Ldb_AutoZero_Cbuf2
	,cb_I_Ldb_AutoZero_Cbuf3
	,cb_I_Ldb_BDU_Message
	,cb_I_Ldb_BDU_Title
	,cb_I_Ldb_BootPanelTitle
	,cb_I_Ldb_BootP_Error
	,cb_I_Ldb_BootP_Processing
	,cb_I_Ldb_BootP_Ready
	,cb_I_Ldb_BootP_Stopped
	,cb_I_Ldb_CP_Air_Msg
	,cb_I_Ldb_CP_CIR_Msg
	,cb_I_Ldb_CSE_DiagLog
	,cb_I_Ldb_RSE_Dir 
	,cb_I_Ldb_CSE_Dir
	,cb_I_Ldb_CSE_File
	,cb_I_Ldb_CSE_InfoLog
	,cb_I_Ldb_CSE_TestLog
	,cb_I_Ldb_CaliHiPres
	,cb_I_Ldb_CaliLoPres
	,cb_I_Ldb_CaliOxygen
	,cb_I_Ldb_Calibrate
//	,cb_I_Ldb_Cancel
//	,cb_I_Ldb_Cannot_open_file
	,cb_I_Ldb_Cant_Open_file
	,cb_I_Ldb_Cant_Write_File
	,cb_I_Ldb_Cant_read_registry
	,cb_I_Ldb_ChangePwdTitle
	,cb_I_Ldb_Circ_Pres
	,cb_I_Ldb_Clear
	,cb_I_Ldb_Clear_Sys_logs
	,cb_I_Ldb_Clear_sys_logs
	,cb_I_Ldb_ClearingDB
	,cb_I_Ldb_Cncling_Pls_Wait
	,cb_I_Ldb_Com_Error
	,cb_I_Ldb_Comp_not_installed
	,cb_I_Ldb_Comp_set_fail
	,cb_I_Ldb_Confirm_clear
//	,cb_I_Ldb_Connect
	,cb_I_Ldb_DB_Delete_title
	,cb_I_Ldb_Reports_Delete_title
	,cb_I_Ldb_DB_Name
	,cb_I_Ldb_DB_Sure_Delete
	,cb_I_Ldb_Reports_Sure_Delete
	,cb_I_Ldb_DB_Warning
	,cb_I_Ldb_DB_files_missing
	,cb_I_Ldb_DB_getting_big
//	,cb_I_Ldb_DLL_artfp
//	,cb_I_Ldb_DLL_cant_find_hdr
//	,cb_I_Ldb_DLL_cant_ld_hdr
//	,cb_I_Ldb_DLL_cant_load
//	,cb_I_Ldb_DLL_func_not_found
//	,cb_I_Ldb_DLL_hdr_syntax_err
//	,cb_I_Ldb_DLL_sfp
//	,cb_I_Ldb_DLL_uat
//	,cb_I_Ldb_DLL_urt
//	,cb_I_Ldb_DLL_vaf
//	,cb_I_Ldb_DLL_wo_p_proto
	,cb_I_Ldb_Database_error
	,cb_I_Ldb_Database_error_msg
	,cb_I_Ldb_Dec_Abrv
	,cb_I_Ldb_Delete_User
	,cb_I_Ldb_DemoTitle
	,cb_I_Ldb_DiagLog_Def_File
	,cb_I_Ldb_DiagLog_Def_Path
	,cb_I_Ldb_DisableAlarms
	,cb_I_Ldb_Discon_Hi_Instr
	,cb_I_Ldb_Discon_Lo_Instr
	,cb_I_Ldb_Dup_Username
	,cb_I_Ldb_ERR_PTS_BURST
	,cb_I_Ldb_ERR_TEST_TIMEOUT
	,cb_I_Ldb_ERR_UIR_LOAD
	,cb_I_Ldb_ERR_VENT_CP
	,cb_I_Ldb_ERR_VENT_GAS_SEQ
	,cb_I_Ldb_ERR_VENT_TEST_SEQ
	,cb_I_Ldb_Engine_Error
	,cb_I_Ldb_English
	,cb_I_Ldb_Ensure3LLung
	,cb_I_Ldb_Ensure4LLung
	,cb_I_Ldb_EnsurehalfLLung
	,cb_I_Ldb_Enter_Pwd_For
//	,cb_I_Ldb_Err_Reading_File
	,cb_I_Ldb_Err_in_db_2
//	,cb_I_Ldb_Err_num_ofr
	,cb_I_Ldb_Error
	,cb_I_Ldb_Error_Caps
	,cb_I_Ldb_EstabCom_840
	,cb_I_Ldb_EstabCom_PTS
//	,cb_I_Ldb_Exit
	,cb_I_Ldb_FAILED
	,cb_I_Ldb_Fail_caps
	,cb_I_Ldb_Feb_Abrv
//	,cb_I_Ldb_File_No_Found
	,cb_I_Ldb_French
	,cb_I_Ldb_GUI_Message
	,cb_I_Ldb_GUI_Title
	,cb_I_Ldb_German
	,cb_I_Ldb_Givefivebreaths
	,cb_I_Ldb_Givefiveminutes
	,cb_I_Ldb_HelpCommand
	,cb_I_Ldb_Helpfile_Name
	,cb_I_Ldb_HiPres_AutoZero_Fail
	,cb_I_Ldb_HiPres_AutoZero_Succ
	,cb_I_Ldb_IBW_10kg
	,cb_I_Ldb_IBW_20kg
	,cb_I_Ldb_IBW_85kg
	,cb_I_Ldb_ImagPanelTitle
	,cb_I_Ldb_ImagPnlImaglist_lbl
	,cb_I_Ldb_ImagPnlIPlist_lbl
//	,cb_I_Ldb_InfFile
	,cb_I_Ldb_InfoLog_Def_File
	,cb_I_Ldb_InfoLog_Def_Path
	,cb_I_Ldb_InsPanelTitle
	,cb_I_Ldb_InsPnlInstr_Line01
	,cb_I_Ldb_InsPnlInstr_Line02
	,cb_I_Ldb_InsPnlInstr_Line03
	,cb_I_Ldb_InsPnlInstr_Line04
	,cb_I_Ldb_InsPnlTextMsg
//	,cb_I_Ldb_Invalid_Engine_Info
//	,cb_I_Ldb_Invalid_Limit_Spec
//	,cb_I_Ldb_Invalid_Mod_ID
//	,cb_I_Ldb_Invalid_Module
//	,cb_I_Ldb_Invalid_Path
//	,cb_I_Ldb_Invalid_file_format
	,cb_I_Ldb_Italian
	,cb_I_Ldb_Jan_Abrv
	,cb_I_Ldb_Jul_Abrv
	,cb_I_Ldb_Jun_Abrv
//	,cb_I_Ldb_List_not_Empty
	,cb_I_Ldb_LoPres_AutoZero_Fail
	,cb_I_Ldb_LoPres_AutoZero_Succ
//	,cb_I_Ldb_Looping
	,cb_I_Ldb_MSG_ABORT
	,cb_I_Ldb_MSG_CHANGE_DFLT_RPT
	,cb_I_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY
	,cb_I_Ldb_MSG_CHANGE_DFLT_RPT_TITLE
	,cb_I_Ldb_MSG_ERROR
	,cb_I_Ldb_MSG_FAIL
//	,cb_I_Ldb_MSG_LOADSEQ_SETUP_ERR
//	,cb_I_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE
	,cb_I_Ldb_MSG_LOOPING
	,cb_I_Ldb_MSG_LOOP_ABORT
	,cb_I_Ldb_MSG_NONE
	,cb_I_Ldb_MSG_OUT_OF_MEMORY
	,cb_I_Ldb_MSG_PASS
	,cb_I_Ldb_MSG_RPT_NO_MEM
	,cb_I_Ldb_MSG_RPT_NO_MORE_MEM
	,cb_I_Ldb_MSG_RUNNING
	,cb_I_Ldb_MSG_SAVE_RPT_TITLE
//	,cb_I_Ldb_MSG_SEQ_MOVED
//	,cb_I_Ldb_MSG_SEQ_MOVED_TITLE
//	,cb_I_Ldb_MSG_SINGLE_ABORT
//	,cb_I_Ldb_MSG_SINGLE_FAIL
//	,cb_I_Ldb_MSG_SINGLE_RTE
	,cb_I_Ldb_MSG_SKIP
	,cb_I_Ldb_MSG_UUT_ABORT
	,cb_I_Ldb_MSG_UUT_FAIL
	,cb_I_Ldb_MSG_UUT_RTE
	,cb_I_Ldb_MainpanelFile
	,cb_I_Ldb_MakeOtherSettings
	,cb_I_Ldb_MakeSettings
	,cb_I_Ldb_ManType2PC
	,cb_I_Ldb_ManType2VC
	,cb_I_Ldb_Mar_Abrv
	,cb_I_Ldb_Max_Users_Reg
	,cb_I_Ldb_May_Abrv
//	,cb_I_Ldb_Met_Port
	,cb_I_Ldb_Mode2AC
//	,cb_I_Ldb_Newer_Version_Err
	,cb_I_Ldb_No_Alarm
//	,cb_I_Ldb_No_More_Tests
	,cb_I_Ldb_No_Tests
	,cb_I_Ldb_None_caps
	,cb_I_Ldb_Not_Found
//	,cb_I_Ldb_Not_Glbl_ID
//	,cb_I_Ldb_Not_a_TX_file
	,cb_I_Ldb_Nov_Abrv
	,cb_I_Ldb_O2_prcnt
//	,cb_I_Ldb_OK
	,cb_I_Ldb_OK_When_Ready
	,cb_I_Ldb_Oct_Abrv
	,cb_I_Ldb_Old_Password
	,cb_I_Ldb_Old_Password_wrng
	,cb_I_Ldb_Out_of_Memory
	,cb_I_Ldb_PASSED
	,cb_I_Ldb_PC
//	,cb_I_Ldb_PP_Test_Exists
//	,cb_I_Ldb_PP_Test_Missing
//	,cb_I_Ldb_PTH_file_open_err
//	,cb_I_Ldb_PTH_file_read_err
//	,cb_I_Ldb_PTH_invalid_contents
	,cb_I_Ldb_PTRIG
	,cb_I_Ldb_PTS_Com_Error
	,cb_I_Ldb_PTS_Comm_Rcv_Error
	,cb_I_Ldb_PTS_Date_Error
	,cb_I_Ldb_PTS_NoDetect
	,cb_I_Ldb_PTS_SN_Error
	,cb_I_Ldb_PTS_Ver_Error
	,cb_I_Ldb_Pass_caps
	,cb_I_Ldb_Password_Mismatch
	,cb_I_Ldb_PauseMsg1
	,cb_I_Ldb_PauseMsg2
//	,cb_I_Ldb_Paused
//	,cb_I_Ldb_Port
	,cb_I_Ldb_Portuguese
	,cb_I_Ldb_Pressure_incorrect
	,cb_I_Ldb_Print
	,cb_I_Ldb_Psens
	,cb_I_Ldb_Pts_cal_err
	,cb_I_Ldb_Pts_cal_expired
	,cb_I_Ldb_Reseting_PTS
	,cb_I_Ldb_Retrieving_logs
	,cb_I_Ldb_Retry_prompt
//	,cb_I_Ldb_Runtime_Err_Test
//	,cb_I_Ldb_Runtime_Err_loadseq_pre_test
//	,cb_I_Ldb_Runtime_Err_postTest
//	,cb_I_Ldb_Runtime_Err_preTest
//	,cb_I_Ldb_Runtime_Err_seq_post_test
//	,cb_I_Ldb_Runtime_Err_seq_pre_test
//	,cb_I_Ldb_Runtime_Err_unlaodseq_post_test
//	,cb_I_Ldb_SIMV
//	,cb_I_Ldb_SPONT
	,cb_I_Ldb_SQUARE
	,cb_I_Ldb_SST_Msg_1
	,cb_I_Ldb_SST_Msg_2
	,cb_I_Ldb_SST_Msg_3
	,cb_I_Ldb_SST_Title
	,cb_I_Ldb_S_840_Establishing
	,cb_I_Ldb_S_840_Failure
	,cb_I_Ldb_S_840_Success
	,cb_I_Ldb_S_PTS_Establishing
	,cb_I_Ldb_S_PTS_Failure
	,cb_I_Ldb_S_PTS_Success
	,cb_I_Ldb_S_Starting
	,cb_I_Ldb_SaveAs
	,cb_I_Ldb_Select_Diag_File
//	,cb_I_Ldb_Select_Filename
	,cb_I_Ldb_Select_Lang_for
	,cb_I_Ldb_Select_SysInfo_file
	,cb_I_Ldb_Select_testlog_file
	,cb_I_Ldb_Selection_Panel
	,cb_I_Ldb_Sep_Abrv
//	,cb_I_Ldb_SeqLoad_test_Fail
//	,cb_I_Ldb_SeqUnload_test_Fail
//	,cb_I_Ldb_Seq_posttest_Fail
//	,cb_I_Ldb_Seq_pretest_Fail
	,cb_I_Ldb_Set_Comp_SN_status
	,cb_I_Ldb_Set_Comp_hours_status
	,cb_I_Ldb_Set_Compressor
	,cb_I_Ldb_Skip_Caps
	,cb_I_Ldb_Spanish
	,cb_I_Ldb_StatePnlInit
	,cb_I_Ldb_StatePnlReplace
	,cb_I_Ldb_StatePnlTitle
	,cb_I_Ldb_Stopped_Fnsh_tst
	,cb_I_Ldb_SureClearResults
	,cb_I_Ldb_SureDeleteUser
	,cb_I_Ldb_TRIANGLE
//	,cb_I_Ldb_TX_error
	,cb_I_Ldb_Test10_Running
	,cb_I_Ldb_Test11_Running
	,cb_I_Ldb_Test12_Running
	,cb_I_Ldb_Test1_Running
	,cb_I_Ldb_Test2_Running
	,cb_I_Ldb_Test3_Running
	,cb_I_Ldb_Test4_Running
	,cb_I_Ldb_Test5_Running
	,cb_I_Ldb_Test6_Running
	,cb_I_Ldb_Test7_Running
	,cb_I_Ldb_Test8_Running
	,cb_I_Ldb_Test9_Running
	,cb_I_Ldb_TestLog_Def_File
	,cb_I_Ldb_TestLog_Def_Path
	,cb_I_Ldb_Test_Failure
//	,cb_I_Ldb_Test_Not_Verif
//	,cb_I_Ldb_Test_out_of_range
	,cb_I_Ldb_SoftdLicPanelTitle
	,cb_I_Ldb_SoftdLicAgreeTxtLine01
	,cb_I_Ldb_SoftdAgreeLicText
	,cb_I_Ldb_SoftdLicPara_1
	,cb_I_Ldb_SoftdLicPara_2
	,cb_I_Ldb_SoftdLicPara_3_1
	,cb_I_Ldb_SoftdLicPara_3_2
	,cb_I_Ldb_SoftdLicAgreeYes
	,cb_I_Ldb_SoftdLicAgreeNo
	,cb_I_Ldb_ImagFileErrTitle
	,cb_I_Ldb_ImagFileErrNofind
	,cb_I_Ldb_NoDloadCDerrTitle
	,cb_I_Ldb_NoDloadCDerrMsg
	,cb_I_Ldb_Dload_CannotEraseBDU
	,cb_I_Ldb_Dload_CannotEraseGUI
	,cb_I_Ldb_Dload_UserAbortTitle
	,cb_I_Ldb_Dload_UserAbortMsg
	,cb_I_Ldb_Dload_TimeoutAbortTitle
	,cb_I_Ldb_Dload_BDUtimeoutMsg
	,cb_I_Ldb_Dload_GUItimeoutMsg
	,cb_I_Ldb_Dload_CompleteTitle
	,cb_I_Ldb_Dload_BDUcomplete
	,cb_I_Ldb_Dload_GUIcomplete
	,cb_I_Ldb_TFTP_PnlTitle
	,cb_I_Ldb_TFTP_BDUfile_lbl
	,cb_I_Ldb_TFTP_GUIfile_lbl
	,cb_I_Ldb_TFTP_BDUpercent_lbl
	,cb_I_Ldb_TFTP_GUIpercent_lbl
	,cb_I_Ldb_TFTP_BDUblocks_lbl
	,cb_I_Ldb_TFTP_BDUtotal_lbl
	,cb_I_Ldb_TFTP_GUIblocks_lbl
	,cb_I_Ldb_TFTP_GUItotal_lbl
	,cb_I_Ldb_TFTP_BDUmsg
	,cb_I_Ldb_TFTP_BDUcomplete
	,cb_I_Ldb_TFTP_GUImsg
	,cb_I_Ldb_TFTP_GUIcomplete
	,cb_I_Ldb_Trig2PTRIG
	,cb_I_Ldb_Trig2VTRIG
//	,cb_I_Ldb_Unable_to_Verif_posttest
//	,cb_I_Ldb_Unable_to_Verif_posttest_2
//	,cb_I_Ldb_Unable_to_Verif_pretest
//	,cb_I_Ldb_Unable_to_Verif_pretest_2
//	,cb_I_Ldb_Unable_to_Verif_test
//	,cb_I_Ldb_Unable_to_Verif_test_2
//	,cb_I_Ldb_Undef_Refs
	,cb_I_Ldb_Unknown_Error
//	,cb_I_Ldb_Unknown_Ext
	,cb_I_Ldb_VC
	,cb_I_Ldb_VTRIG
	,cb_I_Ldb_VentOffandOn
	,cb_I_Ldb_Vent_Runtimehours
	,cb_I_Ldb_Vsens
	,cb_I_Ldb_Warning
//	,cb_I_Ldb_cant_decode
	,cb_I_Ldb_comp_hours
	,cb_I_Ldb_comp_hours_f
	,cb_I_Ldb_cp_test_msg
	,cb_I_Ldb_cpe_100
	,cb_I_Ldb_cpe_10
	,cb_I_Ldb_cpe_50
	,cb_I_Ldb_cpi_10
	,cb_I_Ldb_cpi_100
	,cb_I_Ldb_cpi_50
	,cb_I_Ldb_cycle_840
	,cb_I_Ldb_cycle_before_test
	,cb_I_Ldb_cycle_before_test1
	,cb_I_Ldb_enter_oxy
	,cb_I_Ldb_enter_spiro
	,cb_I_Ldb_error_cant_decode
	,cb_I_Ldb_exp
	,cb_I_Ldb_fio2_inst
	,cb_I_Ldb_insp
	,cb_I_Ldb_invalid
	,cb_I_Ldb_mantest_tt1
	,cb_I_Ldb_mantest_tt2
	,cb_I_Ldb_mantest_tt3
	,cb_I_Ldb_mantest_tt4
	,cb_I_Ldb_mantest_tt5
	,cb_I_Ldb_mantest_tt6
	,cb_I_Ldb_mantest_tt7
	,cb_I_Ldb_mantest_tt8
	,cb_I_Ldb_mnl_SIO
	,cb_I_Ldb_mnl_SIO_nd
	,cb_I_Ldb_mnl_air_reg
	,cb_I_Ldb_mnl_air_reg_nd
	,cb_I_Ldb_mnl_est
	,cb_I_Ldb_mnl_est_nd
	,cb_I_Ldb_mnl_f_leakage1
	,cb_I_Ldb_mnl_f_leakage1_nd
	,cb_I_Ldb_mnl_f_leakage2
	,cb_I_Ldb_mnl_f_leakage2_nd
	,cb_I_Ldb_mnl_g_isol_r
	,cb_I_Ldb_mnl_g_isol_r_nd
	,cb_I_Ldb_mnl_gnd_r
	,cb_I_Ldb_mnl_gnd_r_nd
	,cb_I_Ldb_mnl_O2_reg
	,cb_I_Ldb_mnl_O2_reg_nd
	,cb_I_Ldb_mnl_r_leakage1
	,cb_I_Ldb_mnl_r_leakage1_nd
	,cb_I_Ldb_mnl_r_leakage2
	,cb_I_Ldb_mnl_r_leakage2_nd
	,cb_I_Ldb_mnl_sst
	,cb_I_Ldb_mnl_sst_nd
	,cb_I_Ldb_mnl_vent_r
	,cb_I_Ldb_mnl_vent_r_nd
	,cb_I_Ldb_mnl_wrkordr_r
	,cb_I_Ldb_mnl_wrkordr_r_nd
	,cb_I_Ldb_O2_conc_vol
	,cb_I_Ldb_oxy_conc
	,cb_I_Ldb_oxy_conc_fail
	,cb_I_Ldb_oxy_incorrect
	,cb_I_Ldb_oxygen_30
	,cb_I_Ldb_oxygen_90
	,cb_I_Ldb_peep_25
	,cb_I_Ldb_peep_45
	,cb_I_Ldb_peep_5
	,cb_I_Ldb_peep_incorrect
	,cb_I_Ldb_peep_inst
//	,cb_I_Ldb_postTest_Not_Verif
//	,cb_I_Ldb_preTest_Not_Verif
	,cb_I_Ldb_pres_5
	,cb_I_Ldb_pres_90
	,cb_I_Ldb_pres_inst
	,cb_I_Ldb_programming_err
	,cb_I_Ldb_pts_cal_date
	,cb_I_Ldb_pts_next_date
	,cb_I_Ldb_pts_rdg
	,cb_I_Ldb_pts_sn
	,cb_I_Ldb_pts_timeout
	,cb_I_Ldb_pts_vn
	,cb_I_Ldb_rpt_FileName
	,cb_I_Ldb_rpt_Limit
	,cb_I_Ldb_rpt_cr_operator
	,cb_I_Ldb_rpt_date
	,cb_I_Ldb_rpt_desc
	,cb_I_Ldb_rpt_enter_fname
	,cb_I_Ldb_rpt_invalid_fname
	,cb_I_Ldb_rpt_measurement
	,cb_I_Ldb_rpt_operator
	,cb_I_Ldb_rpt_pgm_rev
	,cb_I_Ldb_rpt_print_to_file
	,cb_I_Ldb_rpt_rte_stopped
	,cb_I_Ldb_rpt_seq_name
	,cb_I_Ldb_rpt_stn_id
	,cb_I_Ldb_rpt_test_results
	,cb_I_Ldb_rpt_test_station
	,cb_I_Ldb_rpt_time
	,cb_I_Ldb_rpt_title
	,cb_I_Ldb_rpt_tp_aborted
	,cb_I_Ldb_rpt_uut_pn
	,cb_I_Ldb_rpt_uut_rev
	,cb_I_Ldb_rpt_uut_sn
	,cb_I_Ldb_rpt_why_fname_inv
//	,cb_I_Ldb_rte_cleanup
//	,cb_I_Ldb_rte_error_msg
//	,cb_I_Ldb_rte_load
//	,cb_I_Ldb_rte_seq_cleanup
//	,cb_I_Ldb_rte_seq_setup
//	,cb_I_Ldb_rte_setup
//	,cb_I_Ldb_rte_test_err
//	,cb_I_Ldb_rte_unknown
//	,cb_I_Ldb_rte_unload_seq
	,cb_I_Ldb_spirometry
	,cb_I_Ldb_sprio_rdg
	,cb_I_Ldb_sst_proc_error
	,cb_I_Ldb_station_desc
	,cb_I_Ldb_status
	,cb_I_Ldb_step_x_y
	,cb_I_Ldb_sw_fault_1
	,cb_I_Ldb_sw_fault_2
	,cb_I_Ldb_sw_fault_3
	,cb_I_Ldb_target
	,cb_I_Ldb_target_pts
	,cb_I_Ldb_test_aborted
	,cb_I_Ldb_testing_error
	,cb_I_Ldb_tt1
	,cb_I_Ldb_tt10
	,cb_I_Ldb_tt11
	,cb_I_Ldb_tt12
	,cb_I_Ldb_tt13
	,cb_I_Ldb_tt14
	,cb_I_Ldb_tt15
	,cb_I_Ldb_tt16
	,cb_I_Ldb_tt17
	,cb_I_Ldb_tt2
	,cb_I_Ldb_tt3
	,cb_I_Ldb_tt4
	,cb_I_Ldb_tt5
	,cb_I_Ldb_tt6
	,cb_I_Ldb_tt7
	,cb_I_Ldb_tt8
	,cb_I_Ldb_tt9
	,cb_I_Ldb_vent_atm
	,cb_I_Ldb_vent_fail_reported
	,cb_I_Ldb_vent_oxy_rdg
	,cb_I_Ldb_vol_inst
	,cb_I_Ldb_volume_incorrect
    ,cb_I_Ldb_CSE_AlarmLog
    ,cb_I_Ldb_Select_alarmlog_file
	,cb_I_Ldb_AlarmLog_Def_File
	,cb_I_Ldb_AlarmLog_Def_Path
	,0 
};

//static char cb_P_Ldb_AC[] = "C/A";
//static char cb_P_Ldb_Cancel[] = "Cancelar";
//static char cb_P_Ldb_Connect[] = "Conectar";
//static char cb_P_Ldb_Exit[] = "Sair";
//static char cb_P_Ldb_Invalid_Mod_ID[] = "ID de m�dulo inv�lida";
//static char cb_P_Ldb_Looping[] = "Ciclando";
//static char cb_P_Ldb_MSG_LOADSEQ_SETUP_ERR[] = "N�o foi poss�vel executar fun��o de configura��o de seq��ncia-carregamento.";
//static char cb_P_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE[] = "Erro de configura��o de seq��ncia-carregamento";
//static char cb_P_Ldb_MSG_SEQ_MOVED[] = "Aviso: a seq��ncia foi movida deste o seu �ltimo salvamento.\nDeseja atualizar os caminhos do arquivo?\n";
//static char cb_P_Ldb_MSG_SEQ_MOVED_TITLE[] = "Seq��ncia movida";
//static char cb_P_Ldb_MSG_SINGLE_ABORT[] = "Passe �nico anulado.\n\n";
//static char cb_P_Ldb_MSG_SINGLE_FAIL[] = "Passe �nico interrompido por falha de teste.\n\n";
//static char cb_P_Ldb_MSG_SINGLE_RTE[] = "Passe �nico interrompido por erro de execu��o.\n\n";
//static char cb_P_Ldb_Met_Port[] = "Porta de exala��o";
//static char cb_P_Ldb_Not_Glbl_ID[] = "Identificador n�o definido globalmente no m�dulo";
//static char cb_P_Ldb_OK[] = "OK";
//static char cb_P_Ldb_Paused[] = "PAUSADO";
//static char cb_P_Ldb_Port[] = "Porta";
//static char cb_P_Ldb_SIMV[] = "SIMV";
//static char cb_P_Ldb_SPONT[] = "ESPONT";
//static char cb_P_Ldb_Select_Filename[] = "Selecionar nome de arquivo";
//static char cb_P_Ldb_TX_error[] = "Erro de execu��o de teste";
//static char cb_P_Ldb_Undef_Refs[] = "Refer�ncias indefinidas";
//static char cb_P_Ldb_cant_decode[] = "Erro: imposs�vel decodificar erro n� %d";

static char * P_langTable[] = { // Portuguese
	 0,	// 0 -- unused
	 cb_P_Ldb_ProgramName
	,cb_P_Ldb_Splash_ProgramName
	,cb_P_Ldb_Copyright_Str
	,cb_P_Ldb_Copyright_Rights
	,cb_P_Ldb_Splash_Eval_Msg
	,cb_P_Ldb_Version_Str
	,cb_P_Ldb_SelectUser
	,cb_P_Ldb_PTS2000_Name
	,cb_P_Ldb_PTS_Serial_Lbl
	,cb_P_Ldb_PTS_SWversion_Lbl
	,cb_P_Ldb_PTS_CalLast_Lbl
	,cb_P_Ldb_PTS_CalNext_Lbl
	,cb_P_Ldb_840Vent_Name
	,cb_P_Ldb_840_GUIserial_Lbl
	,cb_P_Ldb_840_BDUserial_Lbl
	,cb_P_Ldb_840_GUIsoftware_Lbl
	,cb_P_Ldb_840_BDUsoftware_Lbl
	,cb_P_Ldb_840_cycle_power
	,cb_P_Ldb_2step
	,cb_P_Ldb_3step
	,cb_P_Ldb_4step
	,cb_P_Ldb_6step
	,cb_P_Ldb_840_Com_Error
	,cb_P_Ldb_840_NoDetect
	,cb_P_Ldb_9step
	,cb_P_Ldb_ABORT
	,cb_P_Ldb_ABORTED
//	,cb_P_Ldb_AC
	,cb_P_Ldb_Add_User
	,cb_P_Ldb_Adj_100_O2_Fail
	,cb_P_Ldb_Adj_100_O2_Succ
	,cb_P_Ldb_Adj_100_O2
	,cb_P_Ldb_Adj_21_O2_Fail
	,cb_P_Ldb_Adj_21_O2_Succ
	,cb_P_Ldb_Adj_21_O2
	,cb_P_Ldb_AdminpanelFile
	,cb_P_Ldb_Alarm_Audible
	,cb_P_Ldb_Apr_Abrv
	,cb_P_Ldb_Aug_Abrv
	,cb_P_Ldb_AutoZeroHiPres
	,cb_P_Ldb_AutoZeroLoPres
	,cb_P_Ldb_AutoZero_Cbuf1
	,cb_P_Ldb_AutoZero_Cbuf2
	,cb_P_Ldb_AutoZero_Cbuf3
	,cb_P_Ldb_BDU_Message
	,cb_P_Ldb_BDU_Title
	,cb_P_Ldb_BootPanelTitle
	,cb_P_Ldb_BootP_Error
	,cb_P_Ldb_BootP_Processing
	,cb_P_Ldb_BootP_Ready
	,cb_P_Ldb_BootP_Stopped
	,cb_P_Ldb_CP_Air_Msg
	,cb_P_Ldb_CP_CIR_Msg
	,cb_P_Ldb_CSE_DiagLog
	,cb_P_Ldb_RSE_Dir
	,cb_P_Ldb_CSE_Dir
	,cb_P_Ldb_CSE_File
	,cb_P_Ldb_CSE_InfoLog
	,cb_P_Ldb_CSE_TestLog
	,cb_P_Ldb_CaliHiPres
	,cb_P_Ldb_CaliLoPres
	,cb_P_Ldb_CaliOxygen
	,cb_P_Ldb_Calibrate
//	,cb_P_Ldb_Cancel
//	,cb_P_Ldb_Cannot_open_file
	,cb_P_Ldb_Cant_Open_file
	,cb_P_Ldb_Cant_Write_File
	,cb_P_Ldb_Cant_read_registry
	,cb_P_Ldb_ChangePwdTitle
	,cb_P_Ldb_Circ_Pres
	,cb_P_Ldb_Clear
	,cb_P_Ldb_Clear_Sys_logs
	,cb_P_Ldb_Clear_sys_logs
	,cb_P_Ldb_ClearingDB
	,cb_P_Ldb_Cncling_Pls_Wait
	,cb_P_Ldb_Com_Error
	,cb_P_Ldb_Comp_not_installed
	,cb_P_Ldb_Comp_set_fail
	,cb_P_Ldb_Confirm_clear
//	,cb_P_Ldb_Connect
	,cb_P_Ldb_DB_Delete_title
	,cb_P_Ldb_Reports_Delete_title
	,cb_P_Ldb_DB_Name
	,cb_P_Ldb_DB_Sure_Delete
	,cb_P_Ldb_Reports_Sure_Delete
	,cb_P_Ldb_DB_Warning
	,cb_P_Ldb_DB_files_missing
	,cb_P_Ldb_DB_getting_big
//	,cb_P_Ldb_DLL_artfp
//	,cb_P_Ldb_DLL_cant_find_hdr
//	,cb_P_Ldb_DLL_cant_ld_hdr
//	,cb_P_Ldb_DLL_cant_load
//	,cb_P_Ldb_DLL_func_not_found
//	,cb_P_Ldb_DLL_hdr_syntax_err
//	,cb_P_Ldb_DLL_sfp
//	,cb_P_Ldb_DLL_uat
//	,cb_P_Ldb_DLL_urt
//	,cb_P_Ldb_DLL_vaf
//	,cb_P_Ldb_DLL_wo_p_proto
	,cb_P_Ldb_Database_error
	,cb_P_Ldb_Database_error_msg
	,cb_P_Ldb_Dec_Abrv
	,cb_P_Ldb_Delete_User
	,cb_P_Ldb_DemoTitle
	,cb_P_Ldb_DiagLog_Def_File
	,cb_P_Ldb_DiagLog_Def_Path
	,cb_P_Ldb_DisableAlarms
	,cb_P_Ldb_Discon_Hi_Instr
	,cb_P_Ldb_Discon_Lo_Instr
	,cb_P_Ldb_Dup_Username
	,cb_P_Ldb_ERR_PTS_BURST
	,cb_P_Ldb_ERR_TEST_TIMEOUT
	,cb_P_Ldb_ERR_UIR_LOAD
	,cb_P_Ldb_ERR_VENT_CP
	,cb_P_Ldb_ERR_VENT_GAS_SEQ
	,cb_P_Ldb_ERR_VENT_TEST_SEQ
	,cb_P_Ldb_Engine_Error
	,cb_P_Ldb_English
	,cb_P_Ldb_Ensure3LLung
	,cb_P_Ldb_Ensure4LLung
	,cb_P_Ldb_EnsurehalfLLung
	,cb_P_Ldb_Enter_Pwd_For
//	,cb_P_Ldb_Err_Reading_File
	,cb_P_Ldb_Err_in_db_2
//	,cb_P_Ldb_Err_num_ofr
	,cb_P_Ldb_Error
	,cb_P_Ldb_Error_Caps
	,cb_P_Ldb_EstabCom_840
	,cb_P_Ldb_EstabCom_PTS
//	,cb_P_Ldb_Exit
	,cb_P_Ldb_FAILED
	,cb_P_Ldb_Fail_caps
	,cb_P_Ldb_Feb_Abrv
//	,cb_P_Ldb_File_No_Found
	,cb_P_Ldb_French
	,cb_P_Ldb_GUI_Message
	,cb_P_Ldb_GUI_Title
	,cb_P_Ldb_German
	,cb_P_Ldb_Givefivebreaths
	,cb_P_Ldb_Givefiveminutes
	,cb_P_Ldb_HelpCommand
	,cb_P_Ldb_Helpfile_Name
	,cb_P_Ldb_HiPres_AutoZero_Fail
	,cb_P_Ldb_HiPres_AutoZero_Succ
	,cb_P_Ldb_IBW_10kg
	,cb_P_Ldb_IBW_20kg
	,cb_P_Ldb_IBW_85kg
	,cb_P_Ldb_ImagPanelTitle
	,cb_P_Ldb_ImagPnlImaglist_lbl
	,cb_P_Ldb_ImagPnlIPlist_lbl
//	,cb_P_Ldb_InfFile
	,cb_P_Ldb_InfoLog_Def_File
	,cb_P_Ldb_InfoLog_Def_Path
	,cb_P_Ldb_InsPanelTitle
	,cb_P_Ldb_InsPnlInstr_Line01
	,cb_P_Ldb_InsPnlInstr_Line02
	,cb_P_Ldb_InsPnlInstr_Line03
	,cb_P_Ldb_InsPnlInstr_Line04
	,cb_P_Ldb_InsPnlTextMsg
//	,cb_P_Ldb_Invalid_Engine_Info
//	,cb_P_Ldb_Invalid_Limit_Spec
//	,cb_P_Ldb_Invalid_Mod_ID
//	,cb_P_Ldb_Invalid_Module
//	,cb_P_Ldb_Invalid_Path
//	,cb_P_Ldb_Invalid_file_format
	,cb_P_Ldb_Italian
	,cb_P_Ldb_Jan_Abrv
	,cb_P_Ldb_Jul_Abrv
	,cb_P_Ldb_Jun_Abrv
//	,cb_P_Ldb_List_not_Empty
	,cb_P_Ldb_LoPres_AutoZero_Fail
	,cb_P_Ldb_LoPres_AutoZero_Succ
//	,cb_P_Ldb_Looping
	,cb_P_Ldb_MSG_ABORT
	,cb_P_Ldb_MSG_CHANGE_DFLT_RPT
	,cb_P_Ldb_MSG_CHANGE_DFLT_RPT_EMPTY
	,cb_P_Ldb_MSG_CHANGE_DFLT_RPT_TITLE
	,cb_P_Ldb_MSG_ERROR
	,cb_P_Ldb_MSG_FAIL
//	,cb_P_Ldb_MSG_LOADSEQ_SETUP_ERR
//	,cb_P_Ldb_MSG_LOADSEQ_SETUP_ERR_TITLE
	,cb_P_Ldb_MSG_LOOPING
	,cb_P_Ldb_MSG_LOOP_ABORT
	,cb_P_Ldb_MSG_NONE
	,cb_P_Ldb_MSG_OUT_OF_MEMORY
	,cb_P_Ldb_MSG_PASS
	,cb_P_Ldb_MSG_RPT_NO_MEM
	,cb_P_Ldb_MSG_RPT_NO_MORE_MEM
	,cb_P_Ldb_MSG_RUNNING
	,cb_P_Ldb_MSG_SAVE_RPT_TITLE
//	,cb_P_Ldb_MSG_SEQ_MOVED
//	,cb_P_Ldb_MSG_SEQ_MOVED_TITLE
//	,cb_P_Ldb_MSG_SINGLE_ABORT
//	,cb_P_Ldb_MSG_SINGLE_FAIL
//	,cb_P_Ldb_MSG_SINGLE_RTE
	,cb_P_Ldb_MSG_SKIP
	,cb_P_Ldb_MSG_UUT_ABORT
	,cb_P_Ldb_MSG_UUT_FAIL
	,cb_P_Ldb_MSG_UUT_RTE
	,cb_P_Ldb_MainpanelFile
	,cb_P_Ldb_MakeOtherSettings
	,cb_P_Ldb_MakeSettings
	,cb_P_Ldb_ManType2PC
	,cb_P_Ldb_ManType2VC
	,cb_P_Ldb_Mar_Abrv
	,cb_P_Ldb_Max_Users_Reg
	,cb_P_Ldb_May_Abrv
//	,cb_P_Ldb_Met_Port
	,cb_P_Ldb_Mode2AC
//	,cb_P_Ldb_Newer_Version_Err
	,cb_P_Ldb_No_Alarm
//	,cb_P_Ldb_No_More_Tests
	,cb_P_Ldb_No_Tests
	,cb_P_Ldb_None_caps
	,cb_P_Ldb_Not_Found
//	,cb_P_Ldb_Not_Glbl_ID
//	,cb_P_Ldb_Not_a_TX_file
	,cb_P_Ldb_Nov_Abrv
	,cb_P_Ldb_O2_prcnt
//	,cb_P_Ldb_OK
	,cb_P_Ldb_OK_When_Ready
	,cb_P_Ldb_Oct_Abrv
	,cb_P_Ldb_Old_Password
	,cb_P_Ldb_Old_Password_wrng
	,cb_P_Ldb_Out_of_Memory
	,cb_P_Ldb_PASSED
	,cb_P_Ldb_PC
//	,cb_P_Ldb_PP_Test_Exists
//	,cb_P_Ldb_PP_Test_Missing
//	,cb_P_Ldb_PTH_file_open_err
//	,cb_P_Ldb_PTH_file_read_err
//	,cb_P_Ldb_PTH_invalid_contents
	,cb_P_Ldb_PTRIG
	,cb_P_Ldb_PTS_Com_Error
	,cb_P_Ldb_PTS_Comm_Rcv_Error
	,cb_P_Ldb_PTS_Date_Error
	,cb_P_Ldb_PTS_NoDetect
	,cb_P_Ldb_PTS_SN_Error
	,cb_P_Ldb_PTS_Ver_Error
	,cb_P_Ldb_Pass_caps
	,cb_P_Ldb_Password_Mismatch
	,cb_P_Ldb_PauseMsg1
	,cb_P_Ldb_PauseMsg2
//	,cb_P_Ldb_Paused
//	,cb_P_Ldb_Port
	,cb_P_Ldb_Portuguese
	,cb_P_Ldb_Pressure_incorrect
	,cb_P_Ldb_Print
	,cb_P_Ldb_Psens
	,cb_P_Ldb_Pts_cal_err
	,cb_P_Ldb_Pts_cal_expired
	,cb_P_Ldb_Reseting_PTS
	,cb_P_Ldb_Retrieving_logs
	,cb_P_Ldb_Retry_prompt
//	,cb_P_Ldb_Runtime_Err_Test
//	,cb_P_Ldb_Runtime_Err_loadseq_pre_test
//	,cb_P_Ldb_Runtime_Err_postTest
//	,cb_P_Ldb_Runtime_Err_preTest
//	,cb_P_Ldb_Runtime_Err_seq_post_test
//	,cb_P_Ldb_Runtime_Err_seq_pre_test
//	,cb_P_Ldb_Runtime_Err_unlaodseq_post_test
//	,cb_P_Ldb_SIMV
//	,cb_P_Ldb_SPONT
	,cb_P_Ldb_SQUARE
	,cb_P_Ldb_SST_Msg_1
	,cb_P_Ldb_SST_Msg_2
	,cb_P_Ldb_SST_Msg_3
	,cb_P_Ldb_SST_Title
	,cb_P_Ldb_S_840_Establishing
	,cb_P_Ldb_S_840_Failure
	,cb_P_Ldb_S_840_Success
	,cb_P_Ldb_S_PTS_Establishing
	,cb_P_Ldb_S_PTS_Failure
	,cb_P_Ldb_S_PTS_Success
	,cb_P_Ldb_S_Starting
	,cb_P_Ldb_SaveAs
	,cb_P_Ldb_Select_Diag_File
//	,cb_P_Ldb_Select_Filename
	,cb_P_Ldb_Select_Lang_for
	,cb_P_Ldb_Select_SysInfo_file
	,cb_P_Ldb_Select_testlog_file
	,cb_P_Ldb_Selection_Panel
	,cb_P_Ldb_Sep_Abrv
//	,cb_P_Ldb_SeqLoad_test_Fail
//	,cb_P_Ldb_SeqUnload_test_Fail
//	,cb_P_Ldb_Seq_posttest_Fail
//	,cb_P_Ldb_Seq_pretest_Fail
	,cb_P_Ldb_Set_Comp_SN_status
	,cb_P_Ldb_Set_Comp_hours_status
	,cb_P_Ldb_Set_Compressor
	,cb_P_Ldb_Skip_Caps
	,cb_P_Ldb_Spanish
	,cb_P_Ldb_StatePnlInit
	,cb_P_Ldb_StatePnlReplace
	,cb_P_Ldb_StatePnlTitle
	,cb_P_Ldb_Stopped_Fnsh_tst
	,cb_P_Ldb_SureClearResults
	,cb_P_Ldb_SureDeleteUser
	,cb_P_Ldb_TRIANGLE
//	,cb_P_Ldb_TX_error
	,cb_P_Ldb_Test10_Running
	,cb_P_Ldb_Test11_Running
	,cb_P_Ldb_Test12_Running
	,cb_P_Ldb_Test1_Running
	,cb_P_Ldb_Test2_Running
	,cb_P_Ldb_Test3_Running
	,cb_P_Ldb_Test4_Running
	,cb_P_Ldb_Test5_Running
	,cb_P_Ldb_Test6_Running
	,cb_P_Ldb_Test7_Running
	,cb_P_Ldb_Test8_Running
	,cb_P_Ldb_Test9_Running
	,cb_P_Ldb_TestLog_Def_File
	,cb_P_Ldb_TestLog_Def_Path
	,cb_P_Ldb_Test_Failure
//	,cb_P_Ldb_Test_Not_Verif
//	,cb_P_Ldb_Test_out_of_range
	,cb_P_Ldb_SoftdLicPanelTitle
	,cb_P_Ldb_SoftdLicAgreeTxtLine01
	,cb_P_Ldb_SoftdAgreeLicText
	,cb_P_Ldb_SoftdLicPara_1
	,cb_P_Ldb_SoftdLicPara_2
	,cb_P_Ldb_SoftdLicPara_3_1
	,cb_P_Ldb_SoftdLicPara_3_2
	,cb_P_Ldb_SoftdLicAgreeYes
	,cb_P_Ldb_SoftdLicAgreeNo
	,cb_P_Ldb_ImagFileErrTitle
	,cb_P_Ldb_ImagFileErrNofind
	,cb_P_Ldb_NoDloadCDerrTitle
	,cb_P_Ldb_NoDloadCDerrMsg
	,cb_P_Ldb_Dload_CannotEraseBDU
	,cb_P_Ldb_Dload_CannotEraseGUI
	,cb_P_Ldb_Dload_UserAbortTitle
	,cb_P_Ldb_Dload_UserAbortMsg
	,cb_P_Ldb_Dload_TimeoutAbortTitle
	,cb_P_Ldb_Dload_BDUtimeoutMsg
	,cb_P_Ldb_Dload_GUItimeoutMsg
	,cb_P_Ldb_Dload_CompleteTitle
	,cb_P_Ldb_Dload_BDUcomplete
	,cb_P_Ldb_Dload_GUIcomplete
	,cb_P_Ldb_TFTP_PnlTitle
	,cb_P_Ldb_TFTP_BDUfile_lbl
	,cb_P_Ldb_TFTP_GUIfile_lbl
	,cb_P_Ldb_TFTP_BDUpercent_lbl
	,cb_P_Ldb_TFTP_GUIpercent_lbl
	,cb_P_Ldb_TFTP_BDUblocks_lbl
	,cb_P_Ldb_TFTP_BDUtotal_lbl
	,cb_P_Ldb_TFTP_GUIblocks_lbl
	,cb_P_Ldb_TFTP_GUItotal_lbl
	,cb_P_Ldb_TFTP_BDUmsg
	,cb_P_Ldb_TFTP_BDUcomplete
	,cb_P_Ldb_TFTP_GUImsg
	,cb_P_Ldb_TFTP_GUIcomplete
	,cb_P_Ldb_Trig2PTRIG
	,cb_P_Ldb_Trig2VTRIG
//	,cb_P_Ldb_Unable_to_Verif_posttest
//	,cb_P_Ldb_Unable_to_Verif_posttest_2
//	,cb_P_Ldb_Unable_to_Verif_pretest
//	,cb_P_Ldb_Unable_to_Verif_pretest_2
//	,cb_P_Ldb_Unable_to_Verif_test
//	,cb_P_Ldb_Unable_to_Verif_test_2
//	,cb_P_Ldb_Undef_Refs
	,cb_P_Ldb_Unknown_Error
//	,cb_P_Ldb_Unknown_Ext
	,cb_P_Ldb_VC
	,cb_P_Ldb_VTRIG
	,cb_P_Ldb_VentOffandOn
	,cb_P_Ldb_Vent_Runtimehours
	,cb_P_Ldb_Vsens
	,cb_P_Ldb_Warning
//	,cb_P_Ldb_cant_decode
	,cb_P_Ldb_comp_hours
	,cb_P_Ldb_comp_hours_f
	,cb_P_Ldb_cp_test_msg
	,cb_P_Ldb_cpe_100
	,cb_P_Ldb_cpe_10
	,cb_P_Ldb_cpe_50
	,cb_P_Ldb_cpi_10
	,cb_P_Ldb_cpi_100
	,cb_P_Ldb_cpi_50
	,cb_P_Ldb_cycle_840
	,cb_P_Ldb_cycle_before_test
	,cb_P_Ldb_cycle_before_test1
	,cb_P_Ldb_enter_oxy
	,cb_P_Ldb_enter_spiro
	,cb_P_Ldb_error_cant_decode
	,cb_P_Ldb_exp
	,cb_P_Ldb_fio2_inst
	,cb_P_Ldb_insp
	,cb_P_Ldb_invalid
	,cb_P_Ldb_mantest_tt1
	,cb_P_Ldb_mantest_tt2
	,cb_P_Ldb_mantest_tt3
	,cb_P_Ldb_mantest_tt4
	,cb_P_Ldb_mantest_tt5
	,cb_P_Ldb_mantest_tt6
	,cb_P_Ldb_mantest_tt7
	,cb_P_Ldb_mantest_tt8
	,cb_P_Ldb_mnl_SIO
	,cb_P_Ldb_mnl_SIO_nd
	,cb_P_Ldb_mnl_air_reg
	,cb_P_Ldb_mnl_air_reg_nd
	,cb_P_Ldb_mnl_est
	,cb_P_Ldb_mnl_est_nd
	,cb_P_Ldb_mnl_f_leakage1
	,cb_P_Ldb_mnl_f_leakage1_nd
	,cb_P_Ldb_mnl_f_leakage2
	,cb_P_Ldb_mnl_f_leakage2_nd
	,cb_P_Ldb_mnl_g_isol_r
	,cb_P_Ldb_mnl_g_isol_r_nd
	,cb_P_Ldb_mnl_gnd_r
	,cb_P_Ldb_mnl_gnd_r_nd
	,cb_P_Ldb_mnl_O2_reg
	,cb_P_Ldb_mnl_O2_reg_nd
	,cb_P_Ldb_mnl_r_leakage1
	,cb_P_Ldb_mnl_r_leakage1_nd
	,cb_P_Ldb_mnl_r_leakage2
	,cb_P_Ldb_mnl_r_leakage2_nd
	,cb_P_Ldb_mnl_sst
	,cb_P_Ldb_mnl_sst_nd
	,cb_P_Ldb_mnl_vent_r
	,cb_P_Ldb_mnl_vent_r_nd
	,cb_P_Ldb_mnl_wrkordr_r
	,cb_P_Ldb_mnl_wrkordr_r_nd
	,cb_P_Ldb_O2_conc_vol
	,cb_P_Ldb_oxy_conc
	,cb_P_Ldb_oxy_conc_fail
	,cb_P_Ldb_oxy_incorrect
	,cb_P_Ldb_oxygen_30
	,cb_P_Ldb_oxygen_90
	,cb_P_Ldb_peep_25
	,cb_P_Ldb_peep_45
	,cb_P_Ldb_peep_5
	,cb_P_Ldb_peep_incorrect
	,cb_P_Ldb_peep_inst
//	,cb_P_Ldb_postTest_Not_Verif
//	,cb_P_Ldb_preTest_Not_Verif
	,cb_P_Ldb_pres_5
	,cb_P_Ldb_pres_90
	,cb_P_Ldb_pres_inst
	,cb_P_Ldb_programming_err
	,cb_P_Ldb_pts_cal_date
	,cb_P_Ldb_pts_next_date
	,cb_P_Ldb_pts_rdg
	,cb_P_Ldb_pts_sn
	,cb_P_Ldb_pts_timeout
	,cb_P_Ldb_pts_vn
	,cb_P_Ldb_rpt_FileName
	,cb_P_Ldb_rpt_Limit
	,cb_P_Ldb_rpt_cr_operator
	,cb_P_Ldb_rpt_date
	,cb_P_Ldb_rpt_desc
	,cb_P_Ldb_rpt_enter_fname
	,cb_P_Ldb_rpt_invalid_fname
	,cb_P_Ldb_rpt_measurement
	,cb_P_Ldb_rpt_operator
	,cb_P_Ldb_rpt_pgm_rev
	,cb_P_Ldb_rpt_print_to_file
	,cb_P_Ldb_rpt_rte_stopped
	,cb_P_Ldb_rpt_seq_name
	,cb_P_Ldb_rpt_stn_id
	,cb_P_Ldb_rpt_test_results
	,cb_P_Ldb_rpt_test_station
	,cb_P_Ldb_rpt_time
	,cb_P_Ldb_rpt_title
	,cb_P_Ldb_rpt_tp_aborted
	,cb_P_Ldb_rpt_uut_pn
	,cb_P_Ldb_rpt_uut_rev
	,cb_P_Ldb_rpt_uut_sn
	,cb_P_Ldb_rpt_why_fname_inv
//	,cb_P_Ldb_rte_cleanup
//	,cb_P_Ldb_rte_error_msg
//	,cb_P_Ldb_rte_load
//	,cb_P_Ldb_rte_seq_cleanup
//	,cb_P_Ldb_rte_seq_setup
//	,cb_P_Ldb_rte_setup
//	,cb_P_Ldb_rte_test_err
//	,cb_P_Ldb_rte_unknown
//	,cb_P_Ldb_rte_unload_seq
	,cb_P_Ldb_spirometry
	,cb_P_Ldb_sprio_rdg
	,cb_P_Ldb_sst_proc_error
	,cb_P_Ldb_station_desc
	,cb_P_Ldb_status
	,cb_P_Ldb_step_x_y
	,cb_P_Ldb_sw_fault_1
	,cb_P_Ldb_sw_fault_2
	,cb_P_Ldb_sw_fault_3
	,cb_P_Ldb_target
	,cb_P_Ldb_target_pts
	,cb_P_Ldb_test_aborted
	,cb_P_Ldb_testing_error
	,cb_P_Ldb_tt1
	,cb_P_Ldb_tt10
	,cb_P_Ldb_tt11
	,cb_P_Ldb_tt12
	,cb_P_Ldb_tt13
	,cb_P_Ldb_tt14
	,cb_P_Ldb_tt15
	,cb_P_Ldb_tt16
	,cb_P_Ldb_tt17
	,cb_P_Ldb_tt2
	,cb_P_Ldb_tt3
	,cb_P_Ldb_tt4
	,cb_P_Ldb_tt5
	,cb_P_Ldb_tt6
	,cb_P_Ldb_tt7
	,cb_P_Ldb_tt8
	,cb_P_Ldb_tt9
	,cb_P_Ldb_vent_atm
	,cb_P_Ldb_vent_fail_reported
	,cb_P_Ldb_vent_oxy_rdg
	,cb_P_Ldb_vol_inst
	,cb_P_Ldb_volume_incorrect
    ,cb_P_Ldb_CSE_AlarmLog
    ,cb_P_Ldb_Select_alarmlog_file
	,cb_P_Ldb_AlarmLog_Def_File
	,cb_P_Ldb_AlarmLog_Def_Path
	,0 
};

static t_langTables langTables[] = {
	 {cb_E_string, E_langTable, sizeof(E_langTable)/sizeof(char *)}
	,{cb_S_string, S_langTable, sizeof(S_langTable)/sizeof(char *)}
	,{cb_F_string, F_langTable, sizeof(F_langTable)/sizeof(char *)}
	,{cb_G_string, G_langTable, sizeof(G_langTable)/sizeof(char *)}
	,{cb_I_string, I_langTable, sizeof(I_langTable)/sizeof(char *)}
	,{cb_P_string, P_langTable, sizeof(P_langTable)/sizeof(char *)}
};
int nNumLangTables = sizeof(langTables)/sizeof(t_langTables);


/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

int PREFGET_DEFLANG(void);
int GetLanguageID(int nLanguage);

char *LDB(unsigned long token,  // String index
          unsigned int flag     // CURRENT_LANG, or specific language
         );
int  GetLangfromString(char* langstr);
void LocalizeNumberInString(char *szNumberStr);
void UnLocalizeNumberInString(char *szNumberStr);
int CVICALLBACK NumberFilterCallback(int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2);

/*****************************************************************************/
/* Name:     PREFGET_DEFLANG                                               
/*                                                                           
/* Purpose:  gets to current language
/*                                          
/*                                                                           
/* Example Call:   int PREFGET_DEFLANG(void);                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   The current language                                                         
/*                                                                           
/*****************************************************************************/
int PREFGET_DEFLANG(void)
{
	return CurrLanguage;
}


int GetLanguageID(int nLanguage){

	int nLangID;

	switch(nLanguage){
		case ENGLISH:
			nLangID = 0x09;
			break;
		case SPANISH:
			nLangID = 0x0a;
			break;
		case FRENCH:
			nLangID = 0x40c;
			break;
		case GERMAN:
			nLangID = 0x07;
			break;
		case ITALIAN:
			nLangID = 0x10;
			break;
		case PORTUGUESE:
			nLangID = 0x416;
			break;
		default:
			nLangID = 0x09; // default ENGLISH
			break;
	} // end switch nLanguage

	return nLangID;
}


/*****************************************************************************/
/* Name:     LDB                                                 
/*                                                                           
/* Purpose:  gets a string from the database.
/*                                          
/*                                                                           
/* Example Call:   char* LDB(
/*	unsigned long token,	// String index
/*	unsigned int flag		// CURRENT_LANG, or specific language;                                    
/*                                                                           
/* Input:    token - string indicator
/*			 flag - CURRENT_LANG, or specific language; 
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   the requested string                                                       
/*                                                                           
/*****************************************************************************/
char* LDB(
	unsigned long token,	// String index
	unsigned int flag		// CURRENT_LANG, or specific language
	)
{
	char *tf;
	int language;

	// If not already a char*. ie. an 'enum'
	if(token && !( token & 0xFFFF0000 )){
		if(flag == CURRENT_LANG){
			// Force default language into range
			language = PREFGET_DEFLANG();
		}
		else{
			// For this string, force to a specific language
			language = flag;
		}
		// If token not in domain of selected language array
		//   use 'base' language
		if(token > langTables[language].tableSize){
			language = 0;
			if(token > langTables[language].tableSize){
				// Error
				return 0;
			}
		}

		if(token <= langTables[language].tableSize){
			// Look up string using index
			tf = langTables[language].table[token];
		}
		else{
			// Error
			tf = 0;
		}
		return tf;
	}
	else{
		// Already a char*
		return (char*)token;
	}
}

/*****************************************************************************/
/* Name:     GetLangfromString                                                     
/*                                                                           
/* Purpose:  converts a string representation of a language to and integer
/*                                          
/*                                                                           
/* Example Call:   int GetLangfromString(char* langstr);                                    
/*                                                                           
/* Input:    input1 -	description          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int GetLangfromString(char* langstr)
{
	if(strcmp(LDBf(Ldb_English),    langstr) == 0){
		return ENGLISH;
	}
	if(strcmp(LDBf(Ldb_Spanish),    langstr) == 0){
		return SPANISH;
	}
	if(strcmp(LDBf(Ldb_French),     langstr) == 0){
		return FRENCH;
	}
	if(strcmp(LDBf(Ldb_German),     langstr) == 0){
		return GERMAN;
	}
	if(strcmp(LDBf(Ldb_Italian),    langstr) == 0){
		return ITALIAN;
	}
	if(strcmp(LDBf(Ldb_Portuguese), langstr) == 0){
		return PORTUGUESE;
	}


	return 0;
}

/*****************************************************************************/
/* Name:     LocalizeNumberInString                                          */
/*                                                                           */
/* Purpose:  Localizes a string representation of a number                   */
/*                                                                           */
/* Example Call:   LocalizeNumberInString(szNumberStr);                      */
/*                                                                           */
/* Input:    char *szNumberStr - null terminated string                      */
/*                                                                           */
/* Output:   decimal points in string replaced with comma                    */
/*           if language is not English                                      */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Return:   void                                                            */
/*                                                                           */
/*****************************************************************************/
#define DECIMAL '.'
#define COMMA ','

void LocalizeNumberInString(char *szNumberStr)
{
	char *ptr;

	if(CurrLanguage != ENGLISH){
		while((ptr = strchr(szNumberStr, DECIMAL)) != NULL){
			*ptr = COMMA;
		}
	}

	return;
}

/*****************************************************************************/
/* Name:     UnLocalizeNumberInString                                        */
/*                                                                           */
/* Purpose:  Unlocalizes a string representation of a number                 */
/*                                                                           */
/* Example Call:   UnLocalizeNumberInString(szNumberStr);                    */
/*                                                                           */
/* Input:    char *szNumberStr - null terminated string                      */
/*                                                                           */
/* Output:   comma in string replaced with decimal point                     */
/*           if language is not English                                      */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Return:   void                                                            */
/*                                                                           */
/*****************************************************************************/
#define DECIMAL '.'
#define COMMA ','

void UnLocalizeNumberInString(char *szNumberStr)
{
	char *ptr;

	if(CurrLanguage != ENGLISH){
		while((ptr = strchr(szNumberStr, COMMA)) != NULL){
			*ptr = DECIMAL;
		}
	}

	return;
}

int CVICALLBACK NumberFilterCallback(int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
	int asciiCode;
	int virtualKey;         /* cursor keys, function keys, esc, enter, etc.. are virtual keys */
	int swallowEvent = 0;   /* 0 means DO NOT swallow (ignore) the event, only mouse and key event can be swallowed */

	switch(event){
        case EVENT_KEYPRESS:
			/*  eventData1 has the keypress value represented by a 4-byte integer consisting 
			    of 3 fields, 0x00MMVVAA: MM = the modifier key, VV = the virtual key, -+*
			    AA = the ASCII key.  eventData2 is a pointer to an integer which hold
			    the actual key value.  This pointer can be used to change the value of
			    the key before the control processes it. */

			asciiCode = eventData1 & VAL_ASCII_KEY_MASK;    /* key masks are defined in userint.h */
			virtualKey = eventData1 & VAL_VKEY_MASK;

			/*  if the key was an ascii key and the menukey (ctrl on the PC) was not pressed */
			if(asciiCode != 0 && (eventData1 & VAL_MENUKEY_MODIFIER) == 0){
                if(!isdigit(asciiCode)){
					if(*(int *)eventData2 != '.' && *(int *)eventData2 != ','){
						swallowEvent = 1;
					}
				}
			}
			break;
	}

	return swallowEvent;
}

int GetLanguageCode(int nLocale)
{
	int nNumBytes;
	char szLangCode[128] = {'\0'};

	nNumBytes = GetLocaleInfo((LCID)nLocale, LOCALE_SABBREVLANGNAME, szLangCode, 128);

	return nNumBytes;
}

