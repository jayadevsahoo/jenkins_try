#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MiscBdEventHandler - Handles control of miscellaneous BD events
//	like SAFETY_VENT, APNEA_VENT, COMPRESSOR_READY, COMPRESSOR_OPERATING,
//	BATTERY_BACKUP_READY, and ON_BATTERY_POWER.
//---------------------------------------------------------------------
//@ Interface-Description
// This is a class dedicated to the control of miscellaneous BD event maneuver.
// Miscellaneous BD events include: SAFETY_VENT, APNEA_VENT, COMPRESSOR_READY,
// COMPRESSOR_OPERATING, BATTERY_BACKUP_READY, ON_BATTERY_POWER.
// Because MiscBdEventHandler class is a BdEventTarget, the virtual method 
// bdEventHappened() is redefined here and is invoked when the Breath-Delivery
// subsystem notifies us that a misc. BD event occurs.
//---------------------------------------------------------------------
//@ Rationale
// This class is to handle the miscellaneous BD event maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply register for the events' callbacks.  This object receives messages
// from the BD subsystem on status of MiscBdEvent events through method
// bdEventHappened().  
//---------------------------------------------------------------------
//@ Fault-Handling
// None
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MiscBdEventHandler.ccv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd		Date:  19-JUL-95	DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//             Integration baseline.
//=====================================================================

#include "MiscBdEventHandler.hh"

//@ Usage-Classes
#include "Led.hh"
#include "BdEventRegistrar.hh"
#include "UpperScreen.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MiscBdEventHandler()  [Default Constructor]
//
//@ Interface-Description
// Creates the MiscBdEventHandler object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Register for miscellaneous BD event callbacks. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MiscBdEventHandler::MiscBdEventHandler(void) 
{
	CALL_TRACE("MiscBdEventHandler::MiscBdEventHandler(void)");

	// Register for miscellaneous Breath-Delivery event callback
	BdEventRegistrar::RegisterTarget(EventData::SAFETY_VENT, this);	
	BdEventRegistrar::RegisterTarget(EventData::APNEA_VENT, this);	
	BdEventRegistrar::RegisterTarget(EventData::COMPRESSOR_READY, this);	
	BdEventRegistrar::RegisterTarget(EventData::COMPRESSOR_OPERATING, this);
	BdEventRegistrar::RegisterTarget(EventData::BATTERY_BACKUP_READY, this);
	BdEventRegistrar::RegisterTarget(EventData::ON_BATTERY_POWER, this);	
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MiscBdEventHandler  [Destructor]
//
//@ Interface-Description
// Destroys the MiscBdEventHandler object.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MiscBdEventHandler::~MiscBdEventHandler(void)
{
	CALL_TRACE("MiscBdEventHandler::~MiscBdEventHandler(void)");

	// Do nothing
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
//	Called to notify this object when a miscellaneous Breath-Delivery subsystem 
//	event occurs.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method handles an event differently depending on the status of the 
//  event:  cancel or active. 
//	On receiving SAFETY_VENT is ACTIVE, activate SafetyVentilationSubScreen.
//	On receiving SAFETY_VENT is CANCEL, deactivate SafetyVentilationSubscreen.
//	On receiving APNEA_VENT is ACTIVE, activate ApneaVentilationSubScreen.
//	On receiving APNEA_VENT is CANCEL, deactivate ApneaVentilationSubScreen.
//	On receiving one of COMPRESSOR_READY, COMPRESSOR_OPERATING,
//	BATTERY_BACKUP_READY or ON_BATTERY_POWER events:  if the event status is
//  ACTIVE, we light the corresponding led; if the event status is CANCEL, we
//  switch off that led.
// $[01222] The compressor operation LED shall indicate that the air supply ...
// $[01223] The compressor ready  LED shall indicate that the compressor ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
MiscBdEventHandler::bdEventHappened(EventData::EventId eventId,
                EventData::EventStatus eventStatus,
				EventData::EventPrompt eventPrompt)
{
    CALL_TRACE("MiscBdEventHandler::bdEventHappened("
				"EventData::EventId eventId,"
                "EventData::EventStatus eventStatus,"
				"EventData::EventPrompt eventPrompt)");
 
    switch (eventId)
    {
	case EventData::SAFETY_VENT:						// $[TI1]
		if (eventStatus == EventData::ACTIVE)
		{												// $[TI1.1]
			UpperScreen::RUpperScreen.getUpperSubScreenArea()->
										activateSafetyVentilationSubScreen();
		}
		else if (eventStatus == EventData::CANCEL)
		{												// $[TI1.2]
			UpperScreen::RUpperScreen.getUpperSubScreenArea()->
										deactivateSafetyVentilationSubScreen();
		}												// $[TI1.3]
		break;

	case EventData::APNEA_VENT:							// $[TI2]
		// $[01355] When the ventilator transitions to apnea ventilation mode ...
		// $[01356] When the ventilator transitions out of apnea ventilation mode ...
		if (eventStatus == EventData::ACTIVE)
		{												// $[TI2.1]
			UpperScreen::RUpperScreen.getUpperSubScreenArea()->
										activateApneaVentilationSubScreen();
		} 
		else if (eventStatus == EventData::CANCEL)
		{												// $[TI2.2]
			UpperScreen::RUpperScreen.getUpperSubScreenArea()->
									deactivateApneaVentilationSubScreen();
		}												// $[TI2.3]
		break;

	case EventData::COMPRESSOR_READY:					// $[TI3] 
		if (eventStatus == EventData::ACTIVE)
		{												// $[TI3.1]
			Led::SetState(Led::COMPRESSOR_READY, Led::ON_STATE);
		}
		else if (eventStatus == EventData::CANCEL)
		{												// $[TI3.2]
			Led::SetState(Led::COMPRESSOR_READY, Led::OFF_STATE);
		}												// $[TI3.3]
		break;

	case EventData::COMPRESSOR_OPERATING:				// $[TI4]
		if (eventStatus == EventData::ACTIVE) 
		{												// $[TI4.1]
			Led::SetState(Led::COMPRESSOR_OPERATING, Led::ON_STATE);
		}
		else if (eventStatus == EventData::CANCEL)
		{												// $[TI4.2]
			Led::SetState(Led::COMPRESSOR_OPERATING, Led::OFF_STATE);
		}												// $[TI4.3]
		break;

	case EventData::BATTERY_BACKUP_READY:				// $[TI5]
		// $[01352] The battery backup ready LED shall indicate ...
		if (eventStatus == EventData::ACTIVE)
		{												// $[TI5.1]
			Led::SetState(Led::BATTERY_BACKUP_READY, Led::ON_STATE);
		}
		else if (eventStatus == EventData::CANCEL)
		{												// $[TI5.2]
			Led::SetState(Led::BATTERY_BACKUP_READY, Led::OFF_STATE);
		}												// $[TI5.3]
		break;

	case EventData::ON_BATTERY_POWER:					// $[TI6]
		// $[01353] The on battery power LED shall indicate ...
		if (eventStatus == EventData::ACTIVE)
		{												// $[TI6.1]
			Led::SetState(Led::ON_BATTERY_POWER, Led::ON_STATE);
			AioDioDriver_t::GetAioDioDriverHandle().SetBackLight(BRIGHT_70_PER);

		}
		else if (eventStatus == EventData::CANCEL)
		{												// $[TI6.2]
			Led::SetState(Led::ON_BATTERY_POWER, Led::OFF_STATE);
			AioDioDriver_t::GetAioDioDriverHandle().SetBackLight(BRIGHT_100_PER);
		}												// $[TI6.3]
		break;

	default:
		CLASS_ASSERTION(FALSE);
        break;
    }        
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
MiscBdEventHandler::SoftFault(const SoftFaultID  softFaultID,
								    const Uint32       lineNumber,
								    const char*        pFileName,
								    const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, 
							MISCBDEVENTHANDLER,
							lineNumber, pFileName, pPredicate);
}
