
#ifndef HighSpontInspTimeSetting_HH
#define HighSpontInspTimeSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2005, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  HighSpontInspTimeSetting - High spontaneous inspiratory
//                                    time setting 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HighSpontInspTimeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date: 01-Dec-2008    SCR Number: 6438
//  Project:  840S
//  Description:
//      Lower bound changed to 0.2 sec for Neonatal patients.
//
//  Revision: 001  By: gdc    Date: 27-May-2005    SCR Number: 6170
//  Project:  NIV2
//  Description:
//      Initial version.
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
//@ End-Usage


class HighSpontInspTimeSetting : public BatchBoundedSetting
{
  public:
    HighSpontInspTimeSetting(void); 
    virtual ~HighSpontInspTimeSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual void updateConstraints_(void);
    virtual Real32  getAbsoluteMinValue_(void) const;
    virtual Real32  getAbsoluteMaxValue_(void) const;

  private:
    HighSpontInspTimeSetting(const HighSpontInspTimeSetting&);	// not implemented...
    void  operator=(const HighSpontInspTimeSetting&);	// not implemented...
};


#endif // HighSpontInspTimeSetting_HH 
