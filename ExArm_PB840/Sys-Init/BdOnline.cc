#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: BdOnline - BD State: Online.
//---------------------------------------------------------------------
//@ Interface-Description
//  The BdOnline class is a FsmState used by the BdFsm finite state
//  machine.  The class represents the wait state that BD Task Control
//  enters upon completing a BD transition to the Online state.  It
//  contains methods for processing events while in this state.
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the BD Online state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  BdOnline is derived from the FsmState class.  It overrides the
//  virtual method receiveGuiReady to provide state specific behavior
//  when it receives a GuiReadyMessage.  It overrides the virtual
//  method receiveCommUp to provide state specific behavior when it
//  receives a CommUpMessage.  It overrides the virtual method 
//  receiveChangeState to provide state specific behavior when it
//  receives a ChangeStateMessage.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/BdOnline.ccv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "BdOnline.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "BdFsm.hh"
#include "BdInop.hh"
#include "BdSst.hh"
#include "ChangeStateMessage.hh"
#include "GuiReadyMessage.hh"
#include "TaskControlAgent.hh"
#include "TaskAgentManager.hh"
//@ End-Usage
#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdOnline [default constructor]
//
//@ Interface-Description
//  BdOnline class default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdOnline::BdOnline()
{
    CALL_TRACE("BdOnline()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdOnline [destructor]
//
//@ Interface-Description
//  BdOnline class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

 
BdOnline::~BdOnline()
{
    CALL_TRACE("~BdOnline()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
BdOnline::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "BdOnline";
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with 
//  this BdOnline state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the ONLINE state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
SigmaState
BdOnline::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_ONLINE;
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveCommUp
//
//@ Interface-Description
//  The receiveCommUp method processes a CommUpMessage received by BD
//  task control in the BdOnline state.  It sends a BdReady-Online
//  message to GUI task control.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method invokes TaskControlAgent::ReportNodeReady to report
//  to the GUI that the BD is ready in the ONLINE state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdOnline::receiveCommUp(const CommUpMessage& ) const
{
    CALL_TRACE("receiveCommUp(const CommUpMessage&)");
    TaskControlAgent::ReportNodeReady();
    // $[TI1]
}
 
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveGuiReady
//
//@ Interface-Description
//  The receiveGuiReady method processes a GuiReadyMessage received 
//  from GUI task control by BD task control in the BdOnline state.
//  It updates the local state of the GUI from the message.  For GUI-
//  Ready in the INOP, ONLINE or SERVICE states, it forwards the 
//  GuiReadyMessage to the applications. Any other reported state is 
//  ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method invokes FsmState::SetGuiState to set the local state of
//  the GUI to the state contained in the GuiReadyMessage.  If the
//  GUI state is INOP, ONLINE or SERVICE, this method invokes 
//  TaskAgentManager::Send to forward the GuiReadyMessage to each 
//  managed task.  A GuiReadyMessage with any other state is ignored
//  and BD processing continues.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdOnline::receiveGuiReady(const GuiReadyMessage& message) const
{
    CALL_TRACE("receiveGuiReady(const GuiReadyMessage&)");
 
    FsmState::SetGuiState(message);

    switch (message.getState())
    {
        case STATE_INOP:    // $[TI1]
        case STATE_ONLINE: 
        case STATE_SERVICE: 
        case STATE_START:
        case STATE_INIT:
            TaskAgentManager::Send(message);
            break;
        case STATE_SST:     // $[TI2]
        case STATE_TIMEOUT:
        default:
#ifdef SIGMA_DEVELOPMENT
            if (Sys_Init::GetDebugLevel(Sys_Init::ID_BDONLINE) >= 1)
                cout << "INVALID " << message.nameOf() 
                     << " RECEIVED/IGNORED" << endl;
#endif
            break;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveChangeState
//
//@ Interface-Description
//  The receiveChangeState method processes a ChangeStateMessage 
//  received from the Breath Delivery subsystem by BD task control 
//  in the BdOnline state.  It changes the task control state from 
//  ONLINE to either INOP or SST based on the ChangeStateMessage.
//  Upon changing states, this method informs the application tasks
//  of the change and sends a BdReadyMessage to GUI task control.
//  The Breath Delivery subsystem queues the ChangeStateMessage
//  using the TaskControlAgent.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method changes the BD task control state to the state 
//  specified in the ChangeStateMessage for INOP and SST.  Other 
//  state change requests cause an assertion.  Upon changing state, 
//  this method invokes TaskAgentManager::ChangeState to inform managed
//  tasks of the transition.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdOnline::receiveChangeState(const ChangeStateMessage& message) const
{
    CALL_TRACE("receiveChangeState(const ChangeStateMessage&)");
 
    switch (message.getState())
    {
        case STATE_INOP: // $[TI1]
            BdFsm::GetBdInopState().startTransition();
            break;
        case STATE_SST:  // $[TI2]
            BdFsm::GetBdSstState().startTransition();
            break;
        default:  // $[TI3]
#ifdef SIGMA_DEVELOPMENT
            if (Sys_Init::GetDebugLevel(Sys_Init::ID_BDONLINE) >= 1)
                cout << "INVALID " << message.nameOf() 
                     << " RECEIVED" << endl;
#endif
            AUX_CLASS_ASSERTION_FAILURE(message.getState());
            break;
    }
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
BdOnline::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_BDONLINE, 
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================


