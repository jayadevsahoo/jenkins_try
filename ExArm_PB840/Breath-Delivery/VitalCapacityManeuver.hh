
#ifndef VitalCapacityManeuver_HH
#define VitalCapacityManeuver_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VitalCapacityManeuver - Vital Capacity Maneuver handler
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VitalCapacityManeuver.hhv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "EventData.hh"
#include "Maneuver.hh"
#include "TimerTarget.hh"
#include "PauseTypes.hh"

//@ Usage-Classes

//@ End-Usage



class VitalCapacityManeuver :  public Maneuver, public TimerTarget
{
  public:

	VitalCapacityManeuver(ManeuverId maneuverId, IntervalTimer& rPauseTimer);
    ~VitalCapacityManeuver(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	// virtual from Maneuver
	virtual void activate(void);
	virtual Boolean complete(void);
	virtual void clear(void);
	virtual void terminateManeuver(void);
	virtual void newCycle(void);
	virtual Boolean processUserEvent(UiEvent& rUiEvent);

	EventData::EventStatus handleManeuver(const Boolean eventStatus);

	virtual void timeUpHappened(const IntervalTimer& timer);

  protected:

  private:
    VitalCapacityManeuver(const VitalCapacityManeuver&);		// not implemented...
    void   operator=(const VitalCapacityManeuver&);	// not implemented...
	VitalCapacityManeuver(void);

	//@ Data-Member: &rPauseTimer_
	// a reference to the server IntervalTimer.
	IntervalTimer& rPauseTimer_;

    //@ Data-Member: pauseTime_
    // maneuver timer count.
    Int32 pauseTime_;
    
	//@ Data-Member: isTimedOut_
	// TRUE when the interval timer expires
	Boolean isTimedOut_;
	
	//@ Data-Member: isVcmExhComplete_
	// TRUE when valid exhalation (vital capacity) data is available to send to the GUI
	Boolean isVcmExhComplete_;
	
};


#endif // VitalCapacityManeuver_HH 
