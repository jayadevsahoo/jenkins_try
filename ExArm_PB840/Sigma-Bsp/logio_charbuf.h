/******************************************************************
*
*		Copyright (c) 1992 READY SYSTEMS CORPORATION.
*
* All rights reserved. READY SYSTEMS' source code is an unpublished
* work and the use of a copyright notice does not imply otherwise.
* This source code contains confidential, trade secret material of
* READY SYSTEMS. Any attempt or participation in deciphering, 
* decoding, reverse engineering or in any way altering the source 
* code is strictly prohibited, unless the prior written consent of
* READY SYSTEMS is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
*  Revision: 001   By: gdc    Date: 23-Aug-2010     SCR Number: 6663
*  Project: API/MAT
*  Description:
*  Increased buffer size from 40 bytes to 200 bytes.
* 
*******************************************************************
*/

/*
 *
 * File:
 *
 *    logio_charbuf.c
 * 
 * Author: 
 *
 *    Eugene Walden
 *
 * Date:
 *
 *    6-02-92
 * 
 * Contents:
 * 
 *    This file contains macros that store characters in and read 
 *    characters from the character input buffers for SERIAL_1 
 *    through SERIAL_4.
 *
 *    These macros are used for the serial devices when they are 
 *    operating in interrupt mode.
 *
 * Revision History:
 *
 *    Name |   Date   | Description of modification
 *    -----+----------+--------------------------------------------
 *     MWM | 09-08-92 | Added multiple inclusion protection
 *
 */

/*
 *
 *     The data structure used for the buffer is a circular buffer.
 *     The structure looks like:
 *
 *             +----------------------------+
 *         +---+-o         start            |
 *         |   +----------------------------+
 *         |   |            end           o-+---+
 *         |   +----------------------------+   |
 *         |   |            cnt             |   |
 *         |   +----------------------------+   |
 *         +-->| data[0]                    |   |
 *             +----------------------------+   |
 *             | data[1]                    |   |
 *             +----------------------------+   |
 *             | data[2]                    |   |
 *             +----------------------------+   |
 *             |             ...            |<--+
 *             +----------------------------+    
 *             | data[LOGIO_CHARBUF_SIZE-1] |    
 *             +----------------------------+    
 * 
 *     An empty buffer is indicated by the condition where the 
 *     start pointer and the end pointer point to the same array 
 *     position. In order to prevent ambiguity in distinguishing 
 *     between a completely full buffer and a completely empty  
 *     buffer, the buffer is only allowed to grow to a length of 
 *     LOGIO_CHARBUF_SIZE-1 characters. 
 *
 *     In addition, we keep a "cnt" of the number of characters in 
 *     the buffer, to make it easier to check to see if the buffer 
 *     is full. Without the "cnt" field you must increment the end 
 *     pointer (possibly wrapping around the end of the array), and 
 *     see if it is the same as the start pointer. Since increment 
 *     and decrement are cheap operations, this implementation 
 *     makes it easier to check boundary conditions. 
 */

#ifndef _LOGIO_CHARBUF_H_
#define _LOGIO_CHARBUF_H_

#define LOGIO_CHARBUF_SIZE 200

struct logio_charbuf {
   char *cbuf_start;	       	/* start pointer             */
   char *cbuf_end;	       	/* end pointer               */
   int   cbuf_cnt;	       	/* number of chars in buffer */
   char  cbuf_data[LOGIO_CHARBUF_SIZE];
   };

typedef struct logio_charbuf logio_charbuf_t;


/*
 *
 * Macro name:
 *
 *     logio_charbuf_init
 *
 * Description:
 * 
 *     This macro initializes a circular character buffer used for 
 *     the logical I/O serial devices when they are operating in 
 *     interrupt mode. The buffer is set to be empty initially; 
 *     that is, the start and end pointers point to the first array 
 *     element, and there are 0 characters in the buffer.
 *
 * Inputs:
 *
 *     buf:	pointer to the buffer structure
 *
 * Outputs:
 *
 *     NONE 
 *
 * Return codes:
 *
 *     NONE
 *
 * Method:
 *
 *
 * Side-effects:
 * Global variables:
 *
 */

#define logio_charbuf_init(buf)                               \
{                                                             \
(buf)->cbuf_start = (buf)->cbuf_data;                         \
(buf)->cbuf_end = (buf)->cbuf_data;                           \
(buf)->cbuf_cnt = 0;                                          \
}


/*
 *
 * Macro name:
 *
 *     logio_charbuf_read
 *
 * Description:
 *
 *     This macro gets a character from the start of a circular 
 *     character buffer.
 *
 *     This macro does not check to see if the buffer is empty. 
 *     That check must be made before logio_charbuf_read is called.
 *
 * Inputs:
 *
 *     buf:	pointer to the buffer structure
 *
 * Outputs:
 *
 *     NONE
 *
 * Return codes:
 *
 *     The next character in the buffer.
 *
 * Side-effects:
 * Global variables:
 *
 */

#define logio_charbuf_read(buf)                                   \
   (*(buf)->cbuf_start)



/*
 *
 * Macro name:
 *
 *     logio_charbuf_write
 *
 * Description:
 *
 *     This macro writes a character to the end of the circular 
 *     buffer pointed to by "buf".
 *
 *     This macro does not check to see if the buffer is full. 
 *     That check must be made before logio_charbuf_write is called.
 *
 * Inputs:
 *
 *     buf:	pointer to the buffer structure
 *     chr:     the character to be written to the buffer
 *
 * Outputs:
 *
 *     NONE
 *
 * Return codes:
 *
 *     NONE
 *
 * Side-effects:
 * Global variables:
 *
 */

#define logio_charbuf_write(buf, chr)                              \
{                                                                  \
*(buf)->cbuf_end = (chr);                                          \
}


/*
 *
 * Macro name:
 *
 *     logio_charbuf_inc
 *
 * Description:
 *
 *     This macro increments either the start or end pointer of the
 *     character buffer. The increment wraps around at the end of 
 *     the array, in a circular fashion.
 *
 * Inputs:
 *
 *     buf:	pointer to the buffer structure
 *     p:       pointer to an array element
 *
 * Outputs:
 *
 *     NONE
 *
 * Return codes:
 *
 *     NONE
 *
 * Side-effects:
 * Global variables:
 *
 */

#define logio_charbuf_inc(buf, p)                             \
   (((p) == ((buf)->cbuf_data+LOGIO_CHARBUF_SIZE-1)) ?        \
                                &((buf)->cbuf_data[0]) :      \
		                (p)+1)


/*
 *
 * Macro name:
 * 
 *     logio_charbuf_notfull
 *
 * Description:
 *
 *     This macro returns TRUE if the buffer is not full, FALSE if 
 *     the buffer is full.
 *
 * Inputs:
 *
 *     buf:	pointer to the buffer structure
 *
 * Outputs:
 *
 *     NONE
 *
 * Return codes:
 *
 *     1: The buffer is not full
 *     0: The buffer is full
 *
 * Side-effects:
 * Global variables:
 *
 */

#define logio_charbuf_notfull(buf) \
   !( ( ((buf)->cbuf_end+1) == (buf)->cbuf_start ) \
   || ( (buf)->cbuf_end == ((buf)->cbuf_start+LOGIO_CHARBUF_SIZE-1) ) )



/*
 *
 * Macro name:
 *
 *     logio_charbuf_isempty
 *
 * Description:
 *
 *     This macro returns TRUE if the buffer is empty, FALSE if the
 *     buffer is empty.
 *
 * Inputs:
 *
 *     buf:	pointer to the buffer structure
 *
 * Outputs:
 *
 *     NONE
 *
 * Return codes:
 *
 *     1: The buffer is empty 
 *     0: The buffer is not empty
 *
 * Side-effects:
 * Global variables:
 *
 */

#define logio_charbuf_isempty(buf) ((buf)->cbuf_cnt == 0)


#endif	/* !_LOGIO_CHARBUF_H_ */
