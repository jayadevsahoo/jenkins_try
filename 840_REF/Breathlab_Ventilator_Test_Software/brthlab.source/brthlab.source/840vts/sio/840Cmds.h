/*****************************************************************************/
/*   Module Name:    840Cmds.h                                               */
/*   Purpose:      Purpose                                                   */
/*                                                                           */
/*                                                                           */
/*   Requirements:   This module implements requirements detailed in         */
/*          <75359-93>                                                       */
/*                                                                           */
/*   Description of Change                  By                Date           */
/*   ---------------------                  -----             ------         */
/*   Changed the Alarm840.Log               RHJ               29 Feb,2008    */
/*   to Alrm840.LOG                                                          */
/*                                                                           */
/*   Added Alarm Log definition             RHJ               27 Nov,2007    */
/*                                                                           */
/*   Changed function KillLog to            Dave Richardson   27 Nov, 2002   */
/*   VentDoFunction and expanded function                                    */
/*   to include erasing BDU or GUI                                           */
/*                                                                           */
/*   $Revision::   1.2      $                                                */
/*                                                                           */
/*   $Log:  //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/sio/840Cmds.h-arc  $
/*                                                                           */
/*    Rev 1.2   04 Dec 2002 15:35:24   Buchman                               */
/* 840 VTS build done                                                        */
/*                                                                           */
/*    Rev 1.1   27 Nov 2002 08:15:56   Richardson                            */
/* Updated per SCR # 26                                                      */
/*                                                                           */
/*****************************************************************************/
/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

#ifndef _PACKET_H
#define _PACKET_H

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include "common.h"
#include "840Msgs.h"
#include "ldb.h"

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#define MAX_RETURN_VALUE  64
#define POST_MAX                256
#define VENT_SERIAL_NUMBER_MAX  256
#define VENT_VERSION_NUMBER_MAX 256
#define SAAS_MAX                256
#define STOP_GAS_FLOW 64
#define MAX_PACKET_SIZE         256
#define NO_PROMPT_ALLOWED -2
#define NO_KEYS_ALLOWED   -5
#define MAX_NO_ALARMLOG_VENTS   200

#define SYS_LOG_FILE  "C:\\VLOG\\Sys840.LOG"
#define INFO_LOG_FILE "C:\\VLOG\\Info840.LOG"
#define ST_LOG_FILE   "C:\\VLOG\\St840.LOG"
#define ALARM_LOG_FILE  "C:\\VLOG\\Alrm840.LOG"
#define VENT_DATA_TIMEOUT    20  // units = seconds
#define CLEAR_IMAGE_TIMEOUT  30  // units = seconds
#define TEST_TIMEOUT        180  // units = seconds
#define COM_TIMEOUT           3  // units = seconds


/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

int CommActive;       /* if vent comport avalible           */
int PromptReturn;     /* instructional message from vent    */
int KeyAllowedReturn; /* keys the vent will allow in a test */
int DataAvailable;    /* are there data elements from vent  */
int VentAckCommand;   /* Message Ack recived from vent      */
int TestActive;       /* Vent is currently working on test  */
int ConditionCode;    /* code from vent not pass/fail       */
int TestFail;         /* Vent reported test failure         */
int Test_id;          /* Current Test if used               */
int PartInstalled;    /* if option installed on vent        */
int DecodeFlag;       /* decode error                       */

/* PACKET FORMAT */
/* 2 BYTES packet[0], packet[1]  for Packet size (length)      */
/* packet [2] Sequence Number, Number of packet sent to vent   */
/* packet [3] Event Family, Enum LTEventId                     */
/* packet [4] outgoing subEvent, Incoming MessageType          */
/* packet [5] REMOTE TEST ID or ACTION ID                      */
/* packet [6] if no param = 0 else stream across end with null */
// TBD typedef unsigned char  Packet[MAX_PACKET_SIZE];
typedef char Packet[MAX_PACKET_SIZE];

/***********************************************/
/************ STRUCTURE DEFINITIONS ************/
/***********************************************/

typedef struct tag_ventData {

	float runTimeHours;
	char saas[SAAS_MAX];

	struct tag_compressor {
		char serial[VENT_SERIAL_NUMBER_MAX];
		float runTimeHours;
	} Compressor;
	struct tag_bd {
		char version[VENT_VERSION_NUMBER_MAX];
		char serial[VENT_SERIAL_NUMBER_MAX];
		char post[POST_MAX];
	} Bd;
	struct tag_gui {
		char version[VENT_VERSION_NUMBER_MAX];
		char serial[VENT_SERIAL_NUMBER_MAX];
		char post[POST_MAX];
	} Gui;
} VENTDATA;

extern VENTDATA Vent;

/**************/
/** FUNCTION **/
/**************/
void UploadProcessing(Packet inPacket);
int ProcessIncomingPacket(void);
void ClearPacket(Packet target);
int Prompt_Accept(int);
int  GetDataStorage( float *out);
int CircuitPressure(void);
int  GetVentLog(void);
int  GetVentInfo(void);
int  VentDoFunction(int type);
int  SetCompressorData( int type, char *str);
int  BdAudio(void);
int  GuiAudio(void);
int AtmosphereTest(void);
int SetAirFlow(int flow);
int FlushGas(int type);
int SetO2Flow(int flow);

static int GetVentDataVersion(void);
static int GetVentDataRunTime(void);
static int GetVentDataSerial(void) ;
int GetSystemLog(void);
int GetInfoLog(void);
int GetStLog(void);
int GetAlarmLog(void);
static Bool Decode(Packet inPacket);
Bool IsAlarmLogDownloadable(void);

#endif // #ifndef _PACKET_H
