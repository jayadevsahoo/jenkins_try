#ifndef KeyPanel_IN
#define KeyPanel_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: KeyPanel - Routes events from off-screen keys to application objects.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/KeyPanel.inv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 003  By: clw      Date: 20-Mar-1996   DCR Number: 1950
//    Project:  Sigma (R8027)
//    Description:
//      Fixed EnableAllKeys() to call EnableKey() for each key.
//      Fixed EnableKey() to enable key sounds for the specified key.
//      Fixed DisableAllKeys() to call DisableKey() for each key.
//      Fixed DisableKey() to disable key sounds for the specified key.
//
//  Revision: 002  By: yyy      Date: 06-Jul-1995   DCR Number: 1952
//    Project:  Sigma (R8027)
//    Description:
//      Modified due to the addition of KeyEnabled_[] static variable, which controls
//      the enabling/disabling of a specific key functions.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EnableAllKeys [static]
//
//@ Interface-Description
// This function enables All the OffScreen keys.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method sets the enable flag to TRUE for the whole KeyEnabled_[] array.
// It also enables all key sounds.  This is done by calling the 
// EnableKey() method for each key.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
inline void
KeyPanel::EnableAllKeys(void)
{
	CALL_TRACE("KeyPanel::EnableAllKeys(void)");

	for (GuiKeys i = 0; i < NUMBER_OF_KEYS; i++)
	{	//														$[TI1]
		EnableKey( DecodeGuiIoKey_(i) );	// convert type of i to enum KeyId
	}
	
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EnableKey [static]
//
//@ Interface-Description
// This function enables the specified OffScreen key.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method sets the private KeyEnabled_ cell Boolean flag as indexed
// by the keyCode. It also enables the key sound by calling the Sound
// EnableKeySound() method.
//---------------------------------------------------------------------
//@ PreCondition
// The given "keyCode" must be valid.
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
inline void
KeyPanel::EnableKey(KeyId keyCode)
{
	CALL_TRACE("KeyPanel::EnableKey(KeyId keyCode)");
	CLASS_PRE_CONDITION((keyCode >= 0)
							&& (keyCode < NUMBER_OF_KEYS));

	//														$[TI1]
	KeyEnabled_[keyCode] = TRUE;
	Sound::EnableKeySound(EncodeGuifKey_(keyCode));
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisableAllKeys [static]
//
//@ Interface-Description
// This function disables All the OffScreen keys.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method sets the enable flag to FALSE for the whole KeyEnabled_[] array.
// It also disables the key sounds. This is done by calling the DisableKey()
// method for each key.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
inline void
KeyPanel::DisableAllKeys(void)
{
	CALL_TRACE("KeyPanel::DisableAllKeys(void)");

	for (Int i = 0; i < NUMBER_OF_KEYS; i++)
	{	//														$[TI1]
		DisableKey( KeyId(i) );		// Convert type of i to enum KeyId.
	}
	
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisableKey [static]
//
//@ Interface-Description
// This function disables the specified OffScreen key.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method sets the private KeyEnabled_ cell Boolean flag as indexed
// by the keyCode. It also disables the key sound by calling the 
// Sound DisableKeySound() method.
//---------------------------------------------------------------------
//@ PreCondition
// The given "keyCode" must be valid.
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
inline void
KeyPanel::DisableKey(KeyId keyCode)
{
	CALL_TRACE("KeyPanel::DisableKey(KeyId keyCode)");
	CLASS_PRE_CONDITION((keyCode >= 0)
							&& (keyCode < NUMBER_OF_KEYS));

	//														$[TI1]
	KeyEnabled_[keyCode] = FALSE;
	Sound::DisableKeySound(EncodeGuifKey_(keyCode)); 
}



#endif // KeyPanel_IN 

