#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LowerSubScreenArea - This subscreen area is the area on the Lower screen
// in which Lower subscreens are displayed (normally selected by Main Settings
// buttons or buttons in the Lower Screen Select Area).  Part of the GUI-App
// Framework cluster.
//---------------------------------------------------------------------
//@ Interface-Description
// This is a Container class, derived from SubScreenArea, which manages the
// display of the Lower Subscreen Area.  Most of the basic subscreen switching
// functionality is provided by the base class.
//
// This class is the repository for all subscreen objects (all derived from the
// SubScreen class) that are displayed in the Lower Subscreen Area, and this
// class provides a public access method for each subscreen member.
//---------------------------------------------------------------------
//@ Rationale
// The main purpose of this class is to create and display the Lower Screen
// subscreens.
//---------------------------------------------------------------------
//@ Implementation-Description
// All subscreen members are created automatically.  The
// lockoutChangeHappened() simply passes on the event to interested parties.
// All other functionality is inherited from the SubScreenArea class.
//---------------------------------------------------------------------
//@ Fault-Handling
// none
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created (by LowerScreen).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LowerSubScreenArea.ccv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014  By:  rhj   Date:  19-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	Added ProxSetupSubScreen.
//
//  Revision: 013  By:  gdc	  Date:  22-AUG-2008    SCR Number: 6435
//  Project:  840S
//  Description:     
//      Provided for changing IBW while ventilating. Eliminated the
//      IBW setup sub-screen (aka New Patient Sub-Screen).
//
//  Revision: 012  By:  gdc	  Date:  26-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:     
//      Added compact flash service mode test sub-screen
//
//  Revision: 011   By: rhj   Date:  02-Oct-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Added NifSubScreen, P100SubScreen, VitalCapacitySubScreen to LSAContents.
//   
//  Revision: 010  By:  quf	   Date:  23-Jan-2002    DCS Number: 5984
//  Project:  GUIComms
//  Description:
//	Added SerialLoopbackTestSubScreen to ServiceLSAContents.
//
//  Revision: 009  By:  hlg	   Date:  12-SEP-2001    DCS Number: 5933
//  Project:  GUIComms
//  Description:
//	Set PContentsMemory_ to the larger of LSAContents or
//      ServiceLSAContents by use of SIZEOF_CONTENTS.
//
//  Revision: 008  By:  hct	   Date:  07-MAR-2000    DCS Number: 5493
//  Project:  GUIComms
//  Description:
//	Set PContentsMemory_ to LSAContents since it is bigger than 
//      ServiceLSAContents now.
//
//  Revision: 007  By: sah	Date: 18-Apr-2000   DCS Number:  5705
//  Project:  NeoMode
//  Description:
//      Due to changes to upper-level classes, the size advantage changed
//      from 'ServiceLSAContents' to 'LSAContents', therefore the memory
//      is now allocated based on 'LSAContents'.
//
//  Revision: 006  By:  yyy	   Date:  09-Nov-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		(hhd 16-Nov-1999) - Reorganize this class to reduce header file dependency. 
//		(yyy) Initial version.
//
//  Revision: 005  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//	Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 004  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/LowerSubScreenArea.ccv   1.12.1.0   07/30/98 10:15:04   gdc
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy   Date:  04-Aug-1997    DCS Number:  1846
//  Project:  Sigma (R8027)
//  Description:
//      04-Aug-1997 Reactivate the DateTime subScreen if the VitalPatientDataArea is changes
//      mode.
//		11-Sep-1997 Fixed the other screen tab button so it is displayed
//		correctly.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "LowerSubScreenArea.hh"


//@ Usage-Classes
#include "LowerScreen.hh"
#include "AlarmSetupSubScreen.hh"
#include "ApneaSetupSubScreen.hh"
#include "CommSetupSubScreen.hh"
#include "BreathTimingSubScreen.hh"
#include "LowerOtherScreensSubScreen.hh"
#include "DateTimeSettingsSubScreen.hh"
#include "MoreSettingsSubScreen.hh"
#include "VentSetupSubScreen.hh"
#include "VentStartSubScreen.hh"
#include "OffKeysSubScreen.hh"
#include "NifSubScreen.hh"
#include "P100SubScreen.hh"
#include "VitalCapacitySubScreen.hh"

#include "CalibrationSetupSubScreen.hh"
#include "EstTestSubScreen.hh"
#include "ExitServiceModeSubScreen.hh"
#include "ExternalControlSubScreen.hh"
#include "ServiceLowerOtherScreensSubScreen.hh"
#include "ServiceModeSetupSubScreen.hh"
#include "SstSetupSubScreen.hh"
#include "SstTestSubScreen.hh"
#include "ServiceInitializationSubScreen.hh"
#include "SerialNumSetupSubScreen.hh"
#include "FlowSensorCalibrationSubScreen.hh"
#include "PressureXducerCalibrationSubScreen.hh"
#include "OperationHourCopySubScreen.hh"
#include "SerialLoopbackTestSubScreen.hh"
#include "CompactFlashTestSubScreen.hh"
#include "ProxSetupSubScreen.hh"
//@ End-Usage

//@ Code...


// Initialize static constants.
static  Int32 CONTAINER_X_=3;
static  Int32 NORMAL_CONTAINER_Y_ = 162;
static  Int32 SERVICE_CONTAINER_Y_=3;


// The following structs are established here holding data once member
// of class LowerSubScreenArea for one reason:  To reduce header file dependency.
// By moving them here, the class header file doesn't have to include
// other class header files needed for it to compile.
// Consequently, less memory is used for the final executable image.
//
// The following struct contains members of the Lower SubScreen Area
struct LSAContents
{
	LSAContents(SubScreenArea *pSubScreenArea);

	AlarmSetupSubScreen alarmSetupSubScreen;
	ApneaSetupSubScreen apneaSetupSubScreen;
	BreathTimingSubScreen breathTimingSubScreen;
	CommSetupSubScreen commSetupSubScreen;
	LowerOtherScreensSubScreen lowerOtherScreensSubScreen;
	DateTimeSettingsSubScreen dateTimeSettingsSubScreen;
	MoreSettingsSubScreen moreSettingsSubScreen;
	VentSetupSubScreen ventSetupSubScreen;
	VentStartSubScreen ventStartSubScreen;
	OffKeysSubScreen offKeysSubScreen;
	NifSubScreen nifSubScreen;
	P100SubScreen p100SubScreen;
	VitalCapacitySubScreen vitalCapacitySubScreen;
    ProxSetupSubScreen  proxSetupSubScreen;
};

// The following struct contains members of the Service Lower SubScreen Area
struct ServiceLSAContents
{
	ServiceLSAContents(SubScreenArea *pSubScreenArea);

	SstSetupSubScreen sstSetupSubScreen;
	SstTestSubScreen sstTestSubScreen;
	EstTestSubScreen estTestSubScreen;
	DateTimeSettingsSubScreen dateTimeSettingsSubScreen;
	ExitServiceModeSubScreen exitServiceModeSubScreen;
	ServiceLowerOtherScreensSubScreen serviceLowerOtherScreensSubScreen;
	CalibrationSetupSubScreen calibrationSetupSubScreen;
	ServiceModeSetupSubScreen serviceModeSetupSubScreen;
	ExternalControlSubScreen externalControlSubScreen;
	ServiceInitializationSubScreen serviceInitializationSubScreen;
	SerialNumSetupSubScreen serialNumSetupSubScreen;
	FlowSensorCalibrationSubScreen flowSensorCalibrationSubScreen;
	PressureXducerCalibrationSubScreen pressureXducerCalibrationSubScreen;
	OperationHourCopySubScreen operationHourCopySubScreen;
	SerialLoopbackTestSubScreen serialLoopbackTestSubScreen;
	CompactFlashTestSubScreen compactFlashTestSubScreen;
};

// Constructor of LSAContents.
LSAContents::LSAContents(SubScreenArea *pSubScreenArea):
			alarmSetupSubScreen(pSubScreenArea),
			apneaSetupSubScreen(pSubScreenArea),
			breathTimingSubScreen(pSubScreenArea),
			commSetupSubScreen(pSubScreenArea),
			lowerOtherScreensSubScreen(pSubScreenArea),
			moreSettingsSubScreen(pSubScreenArea),
			ventSetupSubScreen(pSubScreenArea),
			ventStartSubScreen(pSubScreenArea),
			offKeysSubScreen(pSubScreenArea),
			nifSubScreen(pSubScreenArea),
			p100SubScreen(pSubScreenArea),
			vitalCapacitySubScreen(pSubScreenArea),
			dateTimeSettingsSubScreen(pSubScreenArea),
            proxSetupSubScreen(pSubScreenArea)

{
}

// Constructor of ServiceLSAContents.
ServiceLSAContents::ServiceLSAContents(SubScreenArea *pSubScreenArea) :
			sstSetupSubScreen(pSubScreenArea),
			sstTestSubScreen(pSubScreenArea),
			estTestSubScreen(pSubScreenArea),
			dateTimeSettingsSubScreen(pSubScreenArea),
			exitServiceModeSubScreen(pSubScreenArea),
			serviceLowerOtherScreensSubScreen(pSubScreenArea),
			calibrationSetupSubScreen(pSubScreenArea),
			serviceModeSetupSubScreen(pSubScreenArea),
			externalControlSubScreen(pSubScreenArea),
			serviceInitializationSubScreen(pSubScreenArea),
			serialNumSetupSubScreen(pSubScreenArea),
			flowSensorCalibrationSubScreen(pSubScreenArea),
			pressureXducerCalibrationSubScreen(pSubScreenArea),
			operationHourCopySubScreen(pSubScreenArea),
			serialLoopbackTestSubScreen(pSubScreenArea),
			compactFlashTestSubScreen(pSubScreenArea)
{}

#define SIZEOF_CONTENTS ((sizeof(LSAContents) > sizeof(ServiceLSAContents)) \
			  ? sizeof(LSAContents) : sizeof(ServiceLSAContents))

// Allocate a memory block for (union of) Lower SubScreen Area and Service Lower SubScreen Area 
static Uint PContentsMemory_[(SIZEOF_CONTENTS + 
									sizeof(Uint) - 1) / sizeof(Uint)];

static LSAContents *const PLSAContents_ = (LSAContents *)PContentsMemory_;
static ServiceLSAContents *const PServiceLSAContents_ = (ServiceLSAContents *)PContentsMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LowerSubScreenArea()  [Default Constructor]
//
//@ Interface-Description
// No construction parameters needed.  Constructs all subscreens that
// will be displayed in the Lower Subscreen Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// All subscreen members are initialized with a pointer to this
// subscreen area.  We then size, position and color this area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LowerSubScreenArea::LowerSubScreenArea(void)
{
	CALL_TRACE("LowerSubScreenArea::LowerSubScreenArea(void)");

	const SigmaState  GUI_STATE = GuiApp::GetGuiState();

	switch (GUI_STATE)
	{// $[TI1]
	case ::STATE_INOP :
	case ::STATE_TIMEOUT :
	case ::STATE_ONLINE :	// $[TI1.1]
		// Initialize static constants.
		setX(CONTAINER_X_);
		setY(NORMAL_CONTAINER_Y_);
		setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);

		// ensure '::PContentsMemory_' is big enough for this instance...
		AUX_CLASS_ASSERTION((sizeof(PContentsMemory_) >= sizeof(LSAContents)),
							(sizeof(LSAContents) - sizeof(PContentsMemory_)));

		// Construct the "Normal" lower subscreen area contents...
		new (::PLSAContents_) LSAContents(this);
		break;

	case ::STATE_SST : 	// $[TI1.2]
	case ::STATE_SERVICE :
		setX(CONTAINER_X_);
		setY(SERVICE_CONTAINER_Y_);
		setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

		// ensure '::PContentsMemory_' is big enough for this instance...
		AUX_CLASS_ASSERTION((sizeof(PContentsMemory_) >= sizeof(ServiceLSAContents)),
							(sizeof(ServiceLSAContents) - sizeof(PContentsMemory_)));

		// Construct the "Service" lower subscreen area contents...
		new (::PServiceLSAContents_) ServiceLSAContents(this);
		break;

	case ::STATE_START : 
	case ::STATE_INIT :
	case ::STATE_UNKNOWN :
	case ::STATE_FAILED :
	default : 	// $[TI1.3]
		// unexpected state...
		AUX_CLASS_ASSERTION_FAILURE(GUI_STATE);
		break;
	}

	// Size and position the area
	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);

	// Default background color is Medium Grey
	setFillColor(Colors::MEDIUM_BLUE);					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LowerSubScreenArea()  [Destructor]
//
//@ Interface-Description
// Destroys the Lower Subscreen Area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LowerSubScreenArea::~LowerSubScreenArea(void)
{
	CALL_TRACE("LowerSubScreenArea::~LowerSubScreenArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDateTimeSettingScreen
//
//@ Interface-Description
//  Disable the access to DateTimeSettingsSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the currently displayed subScreen is either DateTimeSettingsSubScreen
//  or LowerOtherScreensSubScreen then will need to refresh the display
//  accordingly.
// $[01363] Access to the Date/Timec change subscreen...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LowerSubScreenArea::updateDateTimeSettingScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::updateDateTimeSettingScreen(void)");

	if (isCurrentSubScreen(&(PLSAContents_->dateTimeSettingsSubScreen)))
	{													// $[TI1]
		TabButton *pTabButton;

		pTabButton = LowerScreen::RLowerScreen.getLowerScreenSelectArea()->
											getLowerOtherScreensTabButton();
		
		// Deactivate the date/time screen.
		deactivateSubScreen();
		// Activate the date/time subscreen and force the tab button
		// down with it. 
		activateSubScreen(&(PLSAContents_->dateTimeSettingsSubScreen), pTabButton);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateOffKeysSubScreen
//
//@ Interface-Description
// Displays the OffKeysSubScreen as the "default" subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the 'offKeysSubScreen' as the default subscreen for
// the subscreen area, then display it.  The subscreen area
// automatically displays the "default" subscreen when no other
// user-selected subscreen is being displayed.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
LowerSubScreenArea::activateOffKeysSubScreen(Boolean delayedDisplay)
{
	CALL_TRACE("activateOffKeysSubScreen(void)");

	setDefaultSubScreen(&(PLSAContents_->offKeysSubScreen), NULL);

	if (!delayedDisplay)
	{// $[TI1.1]
		activateDefaultSubScreen();
	}// $[TI1.2]
									
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateOffKeysSubScreen
//
//@ Interface-Description
// Hide the OffKeysSubScreen, if it is currently displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Nullify the default subscreen for the subscreen area.  If the
// OffKeysSubScreen (the only default subscreen) is currently
// displayed, then dismiss it.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
LowerSubScreenArea::deactivateOffKeysSubScreen(void)
{
    CALL_TRACE("LowerSubScreenArea::deactivateOffKeysSubScreen(void)");
 
	setDefaultSubScreen(NULL, NULL);

	// deactivate the sub-screen if it's currently displayed
	if (isCurrentSubScreen(&(PLSAContents_->offKeysSubScreen)))
	{												// $[TI1.1]
		deactivateSubScreen();
	}
	else
	{												// $[TI1.2]
		pDefaultSubScreen_ = NULL;
		pDefaultButton_ = NULL;
	}
}                                                                         

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetAlarmSetupSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's Alarm Setup subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of alarmSetupSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmSetupSubScreen *
LowerSubScreenArea::GetAlarmSetupSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetAlarmSetupSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->alarmSetupSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetApneaSetupSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's Apnea Setup subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of apneaSetupSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 ApneaSetupSubScreen *
LowerSubScreenArea::GetApneaSetupSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetApneaSetupSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->apneaSetupSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBreathTimingSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's Breath Timing
// subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of breathTimingSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 BreathTimingSubScreen *
LowerSubScreenArea::GetBreathTimingSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetBreathTimingSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->breathTimingSubScreen));					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetCommSetupSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's Communication Setup
// subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of commSetupSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 CommSetupSubScreen *
LowerSubScreenArea::GetCommSetupSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetCommSetupSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->commSetupSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetLowerOtherScreensSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's Other Screens
// subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of lowerOtherScreensSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 LowerOtherScreensSubScreen *
LowerSubScreenArea::GetLowerOtherScreensSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetLowerOtherScreensSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->lowerOtherScreensSubScreen));				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDateTimeSettingsSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's Date/Time Settings
// subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of dateTimeSettingsSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 DateTimeSettingsSubScreen *
LowerSubScreenArea::GetDateTimeSettingsSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetDateTimeSettingsSubScreen(void)");

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{		  // $[TI1]
	 	return (&(PServiceLSAContents_->dateTimeSettingsSubScreen));	
	}
	else
	{		   // $[TI2]
		return (&(PLSAContents_->dateTimeSettingsSubScreen));
	}
}
									 	
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetMoreSettingsSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's More Settings
// subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of moreSettingsSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 MoreSettingsSubScreen *
LowerSubScreenArea::GetMoreSettingsSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetMoreSettingsSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->moreSettingsSubScreen));					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetVentSetupSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's Vent Setup subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of ventSetupSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 VentSetupSubScreen *
LowerSubScreenArea::GetVentSetupSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetVentSetupSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->ventSetupSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetVentStartSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's Vent Start subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of ventStartSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 VentStartSubScreen *
LowerSubScreenArea::GetVentStartSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetVentStartSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->ventStartSubScreen));						// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetOffKeysSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's Off-Screen Keys subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of ventStartSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 OffKeysSubScreen *
LowerSubScreenArea::GetOffKeysSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetOffKeysSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->offKeysSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetNifSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's NIF Manueuver
// sub-screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of nifSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NifSubScreen *
LowerSubScreenArea::GetNifSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetNifSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->nifSubScreen));						// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetP100SubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's P100 Manueuver
// sub-screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of p100SubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

P100SubScreen *
LowerSubScreenArea::GetP100SubScreen(void)
{
	CALL_TRACE("LowerScreen::GetP100SubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->p100SubScreen));						// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetVitalCapacitySubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's Vital Capacity Manueuver
// sub-screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of vitalCapacitySubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VitalCapacitySubScreen *
LowerSubScreenArea::GetVitalCapacitySubScreen(void)
{
	CALL_TRACE("LowerScreen::GetVitalCapacitySubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->vitalCapacitySubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSstSetupSubScreen
//
//@ Interface-Description
// Returns a pointer to the SstSetupSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of SstSetupSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 SstSetupSubScreen  *
LowerSubScreenArea::GetSstSetupSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetSstSetupSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SST);

	return (&(PServiceLSAContents_->sstSetupSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSstTestSubScreen
//
//@ Interface-Description
// Returns a pointer to the SstTestSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of SstTestSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 SstTestSubScreen  *
LowerSubScreenArea::GetSstTestSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetSstTestSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SST);

	return (&(PServiceLSAContents_->sstTestSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEstTestSubScreen
//
//@ Interface-Description
// Returns a pointer to the EstTestSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of EstTestSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 EstTestSubScreen  *
LowerSubScreenArea::GetEstTestSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetEstTestSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SERVICE);

	return (&(PServiceLSAContents_->estTestSubScreen));						// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetExitServiceModeSubScreen
//
//@ Interface-Description
// Returns a pointer to the ExitServiceModeSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of ExitServiceModeSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 ExitServiceModeSubScreen  *
LowerSubScreenArea::GetExitServiceModeSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetExitServiceModeSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SERVICE || GuiApp::GetGuiState() == STATE_SST);

	return (&(PServiceLSAContents_->exitServiceModeSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetLowerOtherScreensSubScreen
//
//@ Interface-Description
// Returns a pointer to the LowerOtherScreensSubScreen.
// subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of LowerOtherScreensSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 ServiceLowerOtherScreensSubScreen *
LowerSubScreenArea::GetServiceLowerOtherScreensSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetServiceLowerOtherScreensSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SERVICE);

	return (&(PServiceLSAContents_->serviceLowerOtherScreensSubScreen));				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetCalibrationSetupSubScreen
//
//@ Interface-Description
// Returns a pointer to the CalibrationSetupSubScreen
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of CalibrationSetupSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 CalibrationSetupSubScreen*
LowerSubScreenArea::GetCalibrationSetupSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetCalibrationSetupSubScreen(void)");

	return (&(PServiceLSAContents_->calibrationSetupSubScreen));				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetServiceModeSetupSubScreen
//
//@ Interface-Description
// Returns a pointer to the GetServiceModeSetupSubScreen
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of serviceModeSetupSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 ServiceModeSetupSubScreen*
LowerSubScreenArea::GetServiceModeSetupSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetServiceModeSetupSubScreen(void)");

	return (&(PServiceLSAContents_->serviceModeSetupSubScreen));				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetExternalControlSubScreen
//
//@ Interface-Description
// Returns a pointer to the ExternalControlSubScreen
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of ExternalControlSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 ExternalControlSubScreen*
LowerSubScreenArea::GetExternalControlSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetExternalControlSubScreen(void)");

	return (&(PServiceLSAContents_->externalControlSubScreen));				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetServiceInitializationSubScreen
//
//@ Interface-Description
// Returns a pointer to the ServiceInitializationSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of serviceInitializationSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 ServiceInitializationSubScreen*
LowerSubScreenArea::GetServiceInitializationSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetServiceInitializationSubScreen(void)");

	return (&(PServiceLSAContents_->serviceInitializationSubScreen));						// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSerialNumSetupSubScreen
//
//@ Interface-Description
// Returns a pointer to the SerialNumSetupSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of SerialNumSetupSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 SerialNumSetupSubScreen*
LowerSubScreenArea::GetSerialNumSetupSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetSerialNumSetupSubScreen(void)");

	return (&(PServiceLSAContents_->serialNumSetupSubScreen));						// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetFlowSensorCalibrationSubScreen
//
//@ Interface-Description
// Returns a pointer to the FlowSensorCalibrationSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of flowSensorCalibrationSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 FlowSensorCalibrationSubScreen*
LowerSubScreenArea::GetFlowSensorCalibrationSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetFlowSensorCalibrationSubScreen(void)");

	return (&(PServiceLSAContents_->flowSensorCalibrationSubScreen));						// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPressureXducerCalibrationSubScreen
//
//@ Interface-Description
// Returns a pointer to the PressureXducerCalibrationSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of pressureXducerCalibrationSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 PressureXducerCalibrationSubScreen*
LowerSubScreenArea::GetPressureXducerCalibrationSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetPressureXducerCalibrationSubScreen(void)");

	return (&(PServiceLSAContents_->pressureXducerCalibrationSubScreen));					// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetOperationHourCopySubScreen
//
//@ Interface-Description
// Returns a pointer to the OperationHourCopySubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of OperationHourCopySubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 OperationHourCopySubScreen*
LowerSubScreenArea::GetOperationHourCopySubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetOperationHourCopySubScreen(void)");

	return (&(PServiceLSAContents_->operationHourCopySubScreen));					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSerialLoopbackTestSubScreen
//
//@ Interface-Description
// Returns a pointer to the SerialLoopbackTestSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of SerialLoopbackTestSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SerialLoopbackTestSubScreen*
LowerSubScreenArea::GetSerialLoopbackTestSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetSerialLoopbackTestSubScreen(void)");
	return (&(PServiceLSAContents_->serialLoopbackTestSubScreen));					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetCompactFlashTestSubScreen
//
//@ Interface-Description
// Returns a pointer to the CompactFlashTestSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of CompactFlashTestSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CompactFlashTestSubScreen*
LowerSubScreenArea::GetCompactFlashTestSubScreen(void)
{
	CALL_TRACE("LowerSubScreenArea::GetCompactFlashTestSubScreen(void)");
	return (&(PServiceLSAContents_->compactFlashTestSubScreen));					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LowerSubScreenArea::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LOWERSUBSCREENAREA,
									lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetProxSetupSubScreen
//
//@ Interface-Description
// Returns a pointer to the Prox Setup SubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of Prox Setup SubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ProxSetupSubScreen *
LowerSubScreenArea::GetProxSetupSubScreen(void)
{
	CALL_TRACE("LowerScreen::GetProxSetupSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PLSAContents_->proxSetupSubScreen));						// $[TI1]
}

