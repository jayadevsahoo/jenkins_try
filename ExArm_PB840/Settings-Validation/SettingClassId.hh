
#ifndef SettingClassId_HH
#define SettingClassId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  SettingClassId - Defines ids for all of the Settings' modules.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingClassId.hhv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 023   By:   rhj   Date: 23-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//       PROX project-related changes.
// 
//  Revision: 022   By: rhj   Date: 07-July-2008    SCR Number: 6435 
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 021   By: rhj   Date:  23-May-2007    SCR Number: 6237 
//  Project:  Trend
//  Description:
//      Added Date International feature.
//
//  Revision: 020   By: gdc   Date:  27-May-2005    SCR Number: 6170 
//  Project:  NIV2
//  Description:
//      Added High Ti spont limit setting for NIV SPONT/SIMV.
//
//  Revision: 013   By: gdc   Date:  18-Feb-2005    SCR Number: 6144 
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 012   By: gfu   Date:  08-Aug-2004    SCR Number: 6132 
//  Project:  PAV3
//  Description: Modified code per SDCR #6132.  Also added keyword 
//               "Log" to file header as requested during code review.
//
//  Revision: 011   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//	Added 'SHADOW_TRACE_ENABLE_SETTING' id for supporting the
//      enable/disable of shadow traces.
//
//  Revision: 010  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added new ids for volume support level, Vt-to-IBW ratio and
//         Vsupp-to-IBW ratio settings
//
//  Revision: 009   By: hct    Date: 14-FEB-2000    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 008   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 007   By: sah    Date: 20-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode project-related changes:
//      *  obsoleted patient type setting
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added ids for new observer/subject base classes
//	*  added ids for new BiLevel-specific classes
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added ids for new ATC-specific classes
//	*  deleted obsolete 'DEFAULT_BATCH_SETTING_VALUES' id
//
//  Revision: 004   By: dosman Date:  24-Aug-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	added SEPTUPLE_INTERVAL_RANGE
//	because IeRatio now has two more ranges.
//
//  Revision: 003   By: dosman Date:  05-Feb-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//	added peep high setting
//
//  Revision: 002   By: sah    Date:  22-Sep-1997    DR Number: 2410
//  Project: Sigma (R8027)
//  Description:
//	Added new Atmospheric Pressure Setting.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version 
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage

//@ Type:  SettingClassId
// Ids for all of this sub-system's classes.
enum SettingClassId
{
  ACCEPTED_CONTEXT_HANDLE		=   0,
  PENDING_CONTEXT_HANDLE		=   1,
  PHASED_IN_CONTEXT_HANDLE		=   2,
  SETTING_CONTEXT_HANDLE		=   3,
  SETTING_CONTROL_HANDLE		=   4,

  SETTINGS_VALIDATION_CLASS		=   5,
  SETTINGS_MGR				=   6,
  CONTEXT_MGR				=   7,

  ACCEPTED_CONTEXT			=   8,
  ADJUSTED_CONTEXT			=   9,
  RECOVERABLE_CONTEXT			=  10,
  PENDING_CONTEXT			=  11,
  PHASED_IN_CONTEXT			=  12,

  SETTINGS_XACTION			=  13,

  SETTING_VALUE_MGR			=  14,
  BATCH_SETTING_VALUES			=  15,
  BD_SETTING_VALUES			=  16,
  NON_BATCH_SETTING_VALUES		=  17,
  SAFETY_PCV_SETTING_VALUES		=  18,

  BATCH_VALUE				=  19,
  NON_BATCH_VALUE			=  20,

  SUBJECT				= 138,
  SETTING_SUBJECT			= 136,
  SETTING_OBSERVER			= 137,
  CONTEXT_SUBJECT			= 118,
  CONTEXT_OBSERVER			=  80,

  SETTING				=  21,

  BOUNDED_SETTING			=  22,
  BATCH_BOUNDED_SETTING			=  23,
  NON_BATCH_BOUNDED_SETTING		=  24,

  DISCRETE_SETTING			=  25,
  BATCH_DISCRETE_SETTING		=  26,
  NON_BATCH_DISCRETE_SETTING		=  27,

  SEQUENTIAL_SETTING			=  28,
  BATCH_SEQUENTIAL_SETTING		=  29,
  NON_BATCH_SEQUENTIAL_SETTING		=  30,

  //==================================================================
  // Batch Setting Class Ids...
  //==================================================================

  APNEA_INSP_PRESS_SETTING		=  31,
  APNEA_INSP_TIME_SETTING		=  32,
  APNEA_INTERVAL_SETTING		=  33,
  APNEA_MIN_INSP_FLOW_SETTING		=  34,
  APNEA_PEAK_INSP_FLOW_SETTING		=  35,
  APNEA_PLATEAU_TIME_SETTING		=  36,
  APNEA_RESP_RATE_SETTING		=  37,
  APNEA_TIDAL_VOLUME_SETTING		=  38,
  DISCO_SENS_SETTING			= 120,
  EXP_SENS_SETTING			=  39,
  FLOW_ACCEL_PERCENT_SETTING		=  40,
  FLOW_SENS_SETTING			=  41,
  HIGH_CIRCUIT_PRESS_SETTING		=  42,
  LOW_CIRCUIT_PRESS_SETTING		= 141,
  HIGH_INSP_TIDAL_VOL_SETTING		= 128,
  HIGH_EXH_TIDAL_VOL_SETTING		=  43,
  HL_RATIO_SETTING			= 133,
  HUMID_VOLUME_SETTING			= 124,
  IBW_SETTING				=  44,
  INSP_PRESS_SETTING			=  45,
  INSP_TIME_SETTING			=  46,
  MIN_INSP_FLOW_SETTING			=  47,
  OXYGEN_PERCENT_SETTING		=  48,
  PEAK_INSP_FLOW_SETTING		=  49,
  PERCENT_SUPPORT_SETTING		= 125,
  PLATEAU_TIME_SETTING			=  50,
  PEEP_SETTING				=  51,
  PEEP_HIGH_SETTING			= 122,
  PEEP_HIGH_TIME_SETTING		= 130,
  PEEP_LOW_SETTING			= 131,
  PEEP_LOW_TIME_SETTING			= 132,
  PRESSURE_SENS_SETTING			=  52,
  PRESSURE_SUPP_LEVEL_SETTING		=  53,
  RESP_RATE_SETTING			=  54,
  TIDAL_VOLUME_SETTING			=  55,
  TUBE_ID_SETTING			= 126,
  VOLUME_SUPPORT_SETTING		= 129,
  HIGH_SPONT_INSP_TIME_SETTING  = 143,

  APNEA_FLOW_PATTERN_SETTING		=  56,
  APNEA_MAND_TYPE_SETTING		=  57,
  APNEA_O2_PERCENT_SETTING		=  58,
  FLOW_PATTERN_SETTING			=  59,
  HUMIDIFICATION_TYPE_SETTING		=  60,
  MAND_TYPE_SETTING			=  61,
  MODE_SETTING				=  62,
  NOMINAL_VOLTAGE_SETTING		=  63,
  PATIENT_CIRCUIT_TYPE_SETTING		=  64,
  SUPPORT_TYPE_SETTING			=  66,
  TRIGGER_TYPE_SETTING			=  67,
  TUBE_TYPE_SETTING			= 127,
  VENT_TYPE_SETTING			= 142,

  APNEA_EXP_TIME_SETTING		=  68,
  APNEA_FLOW_ACCEL_PERCENT_SETTING	=  69,
  APNEA_IE_RATIO_SETTING		=  70,
  ATM_PRESSURE_SETTING			= 121,
  EXP_TIME_SETTING			=  71,
  HIGH_DEL_TIDAL_VOL_SETTING		=  72,
  HIGH_EXH_MINUTE_VOL_SETTING		=  73,
  HIGH_INSP_TIME_SETTING		=  74,
  HIGH_RESP_RATE_SETTING		=  75,
  IE_RATIO_SETTING			=  76,
  LOW_EXH_MAND_TIDAL_VOL_SETTING	=  77,
  LOW_EXH_MINUTE_VOL_SETTING		=  78,
  LOW_EXH_SPONT_TIDAL_VOL_SETTING	=  79,
  MINUTE_VOLUME_SETTING			= 135,
  VSUPP_IBW_RATIO_SETTING		=  65,
  VT_IBW_RATIO_SETTING			= 134,

  APNEA_CONSTANT_PARM_SETTING		=  81,
  CONSTANT_PARM_SETTING			=  82,
  DCI_BAUD_RATE_SETTING			=  83,
  DCI_DATA_BITS_SETTING			=  84,
  DCI_PARITY_MODE_SETTING		=  85,
  FIO2_ENABLED_SETTING			=  86,
  MONTH_SETTING				=  87,
  PRESS_UNITS_SETTING			=  88,
  SERVICE_BAUD_RATE_SETTING		=  89,

  DAY_SETTING				=  90,
  DISPLAY_CONTRAST_DELTA_SETTING	=  91,
  HOUR_SETTING				=  92,
  MINUTE_SETTING			=  93,
  YEAR_SETTING				=  94,

  //==================================================================
  // Non-Batch Setting Class Ids...
  //==================================================================

  PRESS_VOL_LOOP_BASE_SETTING		=  95,

  FLOW_PLOT_SCALE_SETTING		=  96,
  PLOT_1_TYPE_SETTING			=  97,
  PLOT_2_TYPE_SETTING			=  98,
  SHADOW_TRACE_ENABLE_SETTING		= 139,

  PRESS_PLOT_SCALE_SETTING		=  99,
  TIME_PLOT_SCALE_SETTING		= 100,
  VOLUME_PLOT_SCALE_SETTING		= 101,

  ALARM_VOLUME_SETTING			= 102,
  DISPLAY_BRIGHTNESS_SETTING		= 103,
  DISPLAY_CONTRAST_SCALE_SETTING	= 104,

  //==================================================================
  // Miscellaneous Ids...
  //==================================================================

  BOUNDED_RANGE				= 105,
  DOUBLE_INTERVAL_RANGE			= 106,
  SINGLE_INTERVAL_RANGE			= 107,
  TRIPLE_INTERVAL_RANGE			= 108,
  QUADRUPLE_INTERVAL_RANGE		= 109,
  QUINTUPLE_INTERVAL_RANGE		= 110,
  SEPTUPLE_INTERVAL_RANGE		= 123,

  SEQUENTIAL_RANGE			= 111,

  SETTING_CALLBACK_MGR			= 112,

  BOUNDED_SETTING_BOUND			= 113,
  HARD_SETTING_BOUND			= 114,

  BOUND_STATUS				= 115,

  SETTING_ID_CLASS			= 116,
  SETTING_XMIT_ID_CLASS			= 119,
  CONTEXT_ID_CLASS			= 117,
  COM_PORT_CONFIG_SETTING		= 140,
  TREND_SELECT_SETTING		= 144,
  TREND_TIME_SCALE_SETTING		= 145,
  TREND_CURSOR_POSITION_SETTING = 146,
  TREND_PRESET_TYPE_SETTING     = 147,
  DATE_FORMAT_SETTING           = 148,
   LEAK_COMP_ENABLED_SETTING    = 149,  
   PROX_ENABLED_SETTING         = 150,
   NUM_SETTING_CLASSES			= 151
};


#endif // SettingClassId_HH 
