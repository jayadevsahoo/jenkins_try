#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  FlowPlotScaleSetting - Y Axis Scale for the Flow Waveform.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a non-batch, discrete setting that indicates the Y-Axis Scale
//  for flow-based waveforms.  This class inherits from
//  'NonBatchDiscreteSetting' and provides the specific information needed
//  for the representation of Y-Axis Flow Scale.  This information includes
//  the set of values that this setting can have (see
//  'FlowPlotScaleValue.hh'), and this non-batch setting's default value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/FlowPlotScaleSetting.ccv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 004   By: dosman    Date:  07-Apr-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getDefaultValue()
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5092
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.4.1.0) into Rev "BiLevel" (1.4.2.0)
//	Changed this discrete setting to allow its values to be bounded,
//	rather than cycled through.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//  Inital version.
//
//=====================================================================

#include "FlowPlotScaleSetting.hh"
#include "FlowPlotScaleValue.hh"
#include "Plot1TypeValue.hh"
#include "Plot2TypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  FlowPlotScaleSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01159] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FlowPlotScaleSetting::FlowPlotScaleSetting(void)
    : NonBatchDiscreteSetting(SettingId::FLOW_PLOT_SCALE,
			      Setting::NULL_DEPENDENT_ARRAY_,
			      FlowPlotScaleValue::TOTAL_FLOW_SCALE_VALUES,
			      TRUE, ::FLOW_PLOT_SCALE_MAX_ID,
			      ::FLOW_PLOT_SCALE_MIN_ID)
{
  CALL_TRACE("FlowPlotScaleSetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~FlowPlotScaleSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FlowPlotScaleSetting::~FlowPlotScaleSetting(void)
{
  CALL_TRACE("~FlowPlotScaleSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
FlowPlotScaleSetting::getApplicability(
			  const Notification::ChangeQualifier
				      ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pPlot1Type =
			  SettingsMgr::GetSettingPtr(SettingId::PLOT1_TYPE);

  const DiscreteValue  PLOT1_TYPE_VALUE = pPlot1Type->getAcceptedValue();

  Applicability::Id  applicability;

  switch (PLOT1_TYPE_VALUE)
  {
  case Plot1TypeValue::FLOW_VS_VOLUME :		
  case Plot1TypeValue::FLOW_VS_TIME :			// $[TI1]
    applicability = Applicability::CHANGEABLE;
    break;

  case Plot1TypeValue::PRESSURE_VS_TIME :
  case Plot1TypeValue::VOLUME_VS_TIME :			// $[TI2]
    {
      const Setting*  pPlot2Type =
			    SettingsMgr::GetSettingPtr(SettingId::PLOT2_TYPE);

      const DiscreteValue  PLOT2_TYPE_VALUE = pPlot2Type->getAcceptedValue();

      switch (PLOT2_TYPE_VALUE)
      {
      case Plot2TypeValue::FLOW_VS_TIME :	// $[TI2.1]
	applicability = Applicability::CHANGEABLE;
	break;
      case Plot2TypeValue::PRESSURE_VS_TIME :
      case Plot2TypeValue::VOLUME_VS_TIME :
      case Plot2TypeValue::WOB_GRAPHIC :
      case Plot2TypeValue::NO_PLOT2 :		// $[TI2.2]
	applicability = Applicability::INAPPLICABLE;
	break;
      case Plot2TypeValue::TOTAL_PLOT2_TYPES :
      default :
	// unexpected plot type value...
	AUX_CLASS_ASSERTION_FAILURE(PLOT2_TYPE_VALUE);
	break;
      }
    }
    break;

  case Plot1TypeValue::PRESSURE_VS_VOLUME :		// $[TI3]
    applicability = Applicability::INAPPLICABLE;
    break;

  case Plot1TypeValue::TOTAL_PLOT1_TYPES :
  default :
    // unexpected plot type value...
    AUX_CLASS_ASSERTION_FAILURE(PLOT1_TYPE_VALUE);
    break;
  }

  return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
FlowPlotScaleSetting::getDefaultValue(void) const
{
  CALL_TRACE("getDefaultValue()");

  // $[01162] The new-patient value for the Y-axis scale of Flow-Plot...
  return(DiscreteValue(FlowPlotScaleValue::MINUS_120_TO_120_LPM));   // $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FlowPlotScaleSetting::SoftFault(const SoftFaultID  softFaultID,
				const Uint32       lineNumber,
				const char*        pFileName,
				const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          FLOW_PLOT_SCALE_SETTING, lineNumber, pFileName,
                          pPredicate);
}
