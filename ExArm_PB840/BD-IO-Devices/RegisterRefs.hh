
#ifndef RegisterRefs_HH
#define RegisterRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: RegisterRefs - All the external references of various Register
//		   object references.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/RegisterRefs.hhv   25.0.4.0   19 Nov 2013 13:54:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003  By: syw    Date:  02-Jan-1997    DR Number: DCS 1651
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate rSoftResetPort.
//
//  Revision: 002  By: syw    Date:  19-Dec-1996    DR Number: DCS 1641
//  	Project:  Sigma (R8027)
//		Description:
//			Changed rCompressorReadWritePort to rCompressorReadPort or
//			rCompressorWritePort.  Changed some Register to BitAccessRegister.
//			Added BitAccessRegisterMediator.  Added rSerialReadPort and
//			rSerialWritePort.  Deleted rSerialReadWritePort.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================


class BitAccessGpio;
extern BitAccessGpio &RBitAccessGpio;

class BitAccessGpioMediator ;
extern BitAccessGpioMediator& RBitAccessGpioMediator ;

#endif // RegisterRefs_HH



