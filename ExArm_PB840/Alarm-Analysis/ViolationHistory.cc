#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ViolationHistory - Used to track violations and alarm events.
//---------------------------------------------------------------------
//@ Interface-Description
// ViolationHistory contains an ordinalValue_ field and methods for reading and
// incrementing it.
//
// ViolationHistory also contains the Boolean field eventDetected_ and methods 
// to set and get it.
//---------------------------------------------------------------------
//@ Rationale
// The ViolationHistory class is used to track breaths which violate normal
// operating parameters and other events which cause an alarm's urgency to
// escalate.  The class includes data and methods to be useful to all classes
// inheriting from ViolationHistoryManager.
//---------------------------------------------------------------------
//@ Implementation-Description
// The classes derived from ViolationHistoryManager that track violations for
// CountConstraint objects (VHMCurrBreathCountDur, VHMEventCountDur,
// VHMCurrBreathCountCardinal and VHMPrevBreathCountCardinal) track a finite
// number of breaths.  As that maximum number is reached, the oldest
// ViolationHistory object is discarded and the remaining ViolationHistory
// objects have their ordinalValue_ field incremented.
//
// Violation History Manager objects which model alarm events detected during
// a breath use the setEventDetected/clearEventDetected to set/clear the 
// eventDetected_ field.
//
// ViolationHistory contains a pointer to a timer.  Once a timer is started, if
// there is an auto-reset or the user presses the Alarm Reset key, the timer
// must be reset.  This pointer and the reset method provide that functionality.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//    none
//---------------------------------------------------------------------
//@ Invariants
//    none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/ViolationHistory.ccv   25.0.4.0   19 Nov 2013 13:51:26   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 01/18/96         DR Number:   659
//      Project:   Sigma   (R8027)
//      Description:
//         Interval timer OsTimeStamp utilizes milliseconds so the 
//         TIME_CONVERSION_FACTOR is no longer necessary.
//  
//   Revision 003   By:   hct      Date: 03/22/96         DR Number:   795
//      Project:   Sigma   (R8027)
//      Description:
//         Integrate with TaskMonitor.
//  
//   Revision 004   By:   gbs      Date: 05/17/96         DR Number:   995
//      Project:   Sigma   (R8027)
//      Description:
//         Fix CALL_TRACE and add passed parameter comments.
//         These changes do not affect Unit Test.
//  
//   Revision 005   By:   gbs      Date: 05/17/96         DR Number:   996
//      Project:   Sigma   (R8027)
//      Description:
//         Modify [TI] numbers.
//  
//   Revision 006   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 007   By:   hct    Date: 08/27/99         DR Number:  5513
//      Project:   NewAlarms
//      Description:
//         Added new methods to model escalation changes in patient data alarms.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "AlarmQueueMsg.hh"
#include "ViolationHistory.hh"

//@ Usage-Classes
#include "AlarmConstants.hh"
#include "IpcIds.hh"
#include "Heap_MsgTimer_NUM_TIMER.hh"
#include "MsgTimer.hh"
#include "MemoryHandle.hh"
//@ End-Usage

//@ Code...

static Uint pValidVHListMemory_[(sizeof(ValidVHList) + sizeof(Uint) - 1) / 
    sizeof(Uint)];
ValidVHList& ViolationHistory::RValidReferences_ =
     *((ValidVHList*)::pValidVHListMemory_);

// MsgTimer
static Uint pFHMT_
    [(sizeof(FixedHeap(MsgTimer,NUM_TIMER)) + sizeof(Uint)
    - 1) / sizeof(Uint)];
FixedHeap(MsgTimer,NUM_TIMER)& rFHMT_ =
    *((FixedHeap(MsgTimer,NUM_TIMER)*)::pFHMT_);

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ViolationHistory
//
//@ Interface-Description
//    Constructor.
//
// >Von
//    pGroup        Attribute pGroup_ points to the OperatingGroup representing 
//                  the violation criteria tracked by this object.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    (pGroup != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

ViolationHistory::ViolationHistory(OperatingGroup*    pGroup):
                                   pGroup_(pGroup)
{
  CALL_TRACE("ViolationHistory(pGroup)");

  CLASS_PRE_CONDITION(pGroup != NULL);
 
  eventDetected_ = FALSE;
  ordinalValue_ = 0;

  RValidReferences_.append(*this);
  pTimer_ = NULL;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ViolationHistory
//
//@ Interface-Description
//    Copy constructor.
//
// >Von
//    rHist  Reference to the object to be copied from.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

ViolationHistory::ViolationHistory(const ViolationHistory& rHist):
                        ordinalValue_(rHist.ordinalValue_),
                        eventDetected_(rHist.eventDetected_),
                        pGroup_(rHist.pGroup_),
                        pTimer_(rHist.pTimer_)
{
  CALL_TRACE("ViolationHistory(rHist)");

  RValidReferences_.append(*this);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ViolationHistory
//
//@ Interface-Description
//    Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

ViolationHistory::~ViolationHistory(void)
{
  CALL_TRACE("~ViolationHistory(void)");

  SigmaStatus status = RValidReferences_.search(*this,FROM_FIRST,EQUIV_REFERENCES);
  CLASS_ASSERTION(status == SUCCESS);
  RValidReferences_.removeCurrent();

  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//    Assignment operator.
//
// >Von
//    rHist  Reference to the object to be assigned from.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
ViolationHistory::operator=(const ViolationHistory& rHist)
{
  CALL_TRACE("operator=(rHist)");

  ordinalValue_  = rHist.ordinalValue_;
  eventDetected_ = rHist.eventDetected_;
  pGroup_ = rHist.pGroup_;
  pTimer_ = rHist.pTimer_;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//      Method called during system initialization to instantiate objects for
//      the Alarm subsystem to model the Alarm matrices.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
ViolationHistory::Initialize(void)
{
  CALL_TRACE("Initialize(void)");

  // MsgTimer
  CLASS_ASSERTION(sizeof(::pFHMT_) >= sizeof(FixedHeap(MsgTimer,NUM_TIMER)));
  new (::pFHMT_) FixedHeap(MsgTimer,NUM_TIMER);

  // ValidVHList
  CLASS_ASSERTION(sizeof(::pValidVHListMemory_) >= sizeof(ValidVHList));
  new (::pValidVHListMemory_) ValidVHList;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: createTimer
//
//@ Interface-Description
//      Method to initialize the timer for Alarm Silence.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
ViolationHistory::createTimer(void)
{
  CALL_TRACE("createTimer(void)");
  CLASS_ASSERTION( ((Int32)this & 0xFF000000) == 0 );

  AlarmQueueMsg    msg;
  msg.event.eventType = AlarmQueueMsg::VIOLATION_HISTORY_EVENT;
  msg.event.eventData = MemoryHandle::CreateHandle(static_cast<MemPtr>(this));

  pTimer_ = new (::rFHMT_) 
      MsgTimer((Int32)OPERATING_PARAMETER_EVENT_Q, msg.qWord,
      MsgTimer::MIN_MSEC_REQUEST);
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setActive
//
//@ Interface-Description
//    Set this object to active by setting attribute ordinalValue_ to 
//    1.  Set attribute eventDetected_ to the value of parameter
//    eventDetected.
//
// >Von
//    eventDetected   Value to set eventDetected_ attribute to.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
ViolationHistory::setActive(Boolean eventDetected)
{
  CALL_TRACE("setActive(eventDetected)");

  ordinalValue_ = 1;
  eventDetected_ = eventDetected;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getIsActive
//
//@ Interface-Description
//    If attribute ordinalValue_ > 0 return TRUE, else return FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
ViolationHistory::getIsActive(void) const
{
  CALL_TRACE("getIsActive(void)");

  return(ordinalValue_ > 0);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getEventDetected
//
//@ Interface-Description
//    Return the Boolean value of attribute eventDetected_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
ViolationHistory::getEventDetected(void) const
{
  CALL_TRACE("getEventDetected(void)");

  return(eventDetected_);
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearEventDetected
//
//@ Interface-Description
//    Set attribute eventDetected_ to FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
ViolationHistory::clearEventDetected(void)
{
  CALL_TRACE("clearEventDetected(void)");

  eventDetected_ = FALSE;
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getGroup
//
//@ Interface-Description
//    Return the value of pGroup_, a pointer to an OperatingGroup.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

OperatingGroup*
ViolationHistory::getGroup(void) const
{
  CALL_TRACE("getGroup(void)");

  return(pGroup_);
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reset
//
//@ Interface-Description
//    Reset the timer cell.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
ViolationHistory::reset(void)
{
  CALL_TRACE("reset(void)");

  // Cancel the timer
  if (pTimer_) // $[TI1]
  {
    pTimer_->cancel();
  } // $[TI2]

  // Reset ordinalValue_.
  ordinalValue_ = 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTimerMsec
//
//@ Interface-Description
//    Message attribute pTimer_'s set method.
//
// >Von
//    time   Time to set timer to.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
ViolationHistory::setTimerMsec(Int32 time)
{
  CALL_TRACE("setTimerMsec(time)");

  pTimer_->set(time + 1000);
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CheckIsValid
//
//@ Interface-Description
// To verify that a pointer to an instance of ViolationHistory is a valid
// pointer, method CheckIsValid() is messaged.  It compares the passed pointer
// to a list of valid references.
//
// >Von
//    pViolation   Pointer to the ViolationHistory object.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
ViolationHistory::CheckIsValid(ViolationHistory* pViolation)
{
  CALL_TRACE("CheckIsValid(pViolation)");

  CLASS_ASSERTION(pViolation != NULL);

  Boolean found = FALSE;

  SigmaStatus status = RValidReferences_.goFirst();
 
  while ((status == SUCCESS) && !found)
  {
    ViolationHistory* tempPtr = &(RValidReferences_.currentItem());
    if (pViolation == tempPtr)  // $[TI1]
    {
      found = TRUE;
    } // else $[TI2]
    status = RValidReferences_.goNext();
  } // don't check for no while because of following class assertion

  CLASS_ASSERTION(found == TRUE); 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//    This method is called when a software fault is detected by the
//    fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//    'lineNumber' are essential pieces of information.  The 'pFileName'
//    and 'pPredicate' strings may be defaulted in the macro to reduce
//    code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method receives the call for the SoftFault, adds it sub-system
//    and class name ID and sends the message to the non-member function
//    SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition    
//    none
//@ End-Method
//=====================================================================

void
ViolationHistory::SoftFault(const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::VIOLATIONHISTORY, lineNumber,
                          pFileName, pPredicate);
}
