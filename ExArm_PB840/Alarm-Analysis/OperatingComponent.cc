#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OperatingComponent - Connects parent and child AlarmOperand objects.
//---------------------------------------------------------------------
//@ Interface-Description
// OperatingComponent connects parent and child AlarmOperand objects and
// contains data relevant to the evaluation of the children.  The parent
// OperatingGroup (an OperatingCondition object can never be a parent) contains
// an ordered ChildList (OperatingGroup::children_) of OperatingComponent
// objects.  An OperatingComponent object contains a reference to the child
// AlarmOperand, along with a field negate_ (indicates whether or not to invert
// the state of a child when it is merged into the evaluation), and a field
// logOp_ (a logical operator indicating whether the child is to be ANDed or
// ORed into the evaluation).
//---------------------------------------------------------------------
//@ Rationale
// The alarm handling strategy is organized around the goal of detecting and
// communicating root causes of alarms.  A single root problem can cause a
// number of secondary alarms to be detected.  Therefore, instead of
// annunciating all of these as separate alarms, as has commonly been done in
// the past, the Sigma Alarm Analysis subsystem does the detective work for the
// operator and annunciates the primary alarm and adds to the primary
// annunciation the annunciation of secondary alarms.
//
// To accomplish this, and also to make the Alarm Analysis subsystem easily
// extensible, a hierarchy of operating parameters was developed.  This
// hierarchy is an inverted forest of interconnected trees.  The lowermost
// level of leaves is modeled through the OperatingCondition class.  It
// sometimes requires a number of simple conditions to detect an alarm or
// augment an alarm.  The OperatingGroup class collects these conditions (and
// other OperatingGroups) through the use of the OperatingComponent class.  The
// OperatingGroup class includes an ordered list of OperatingComponents, each
// of which in turn points to a single OperatingCondition or OperatingGroup.
// The OperatingComponent includes two fields to determine how the states of
// the child AlarmOperand objects should be combined; the negate_ field acts as
// a NOT operator on the state of the child AlarmOperand and the logOp_ field
// indicates whether to AND or OR the state of the child AlarmOperand with the
// other children.
//
// To extend or change alarms, the initialization of the alarm hierarchy is all
// that needs to be changed.  The framework which processes the hierarchy
// doesn't change.
//---------------------------------------------------------------------
//@ Implementation-Description
// When being evaluated, the parent OperatingGroup initializes a temporary
// Boolean to FALSE and passes it to each OperatingComponent in the ChildList.
// Each OperatingComponent ANDs or ORs the state (inverted if specified negate_
// = TRUE) of the child AlarmOperand with the temporary Boolean and returns the
// result.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/OperatingComponent.ccv   25.0.4.0   19 Nov 2013 13:51:22   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "OperatingComponent.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OperatingComponent
//
//@ Interface-Description
//      Constructor.
//
// >Von
//      rChild   Reference to the child AlarmOperand.
//      lo       Enumerated value representing the logical operator used to 
//               combine the state of the child AlarmOperand (referred to by
//               rChild_) with the states of the other child AlarmOperand 
//               objects.
//      neg      Boolean value indicating whether or not the state of the 
//               AlarmOperand referred to by attribute rChild_ should be 
//               inverted before being combined with the states of the other 
//               child AlarmOperand objects.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      ((lo == OperatingComponent::AND) || (lo == OperatingComponent::OR))
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
OperatingComponent::OperatingComponent(
                        AlarmOperand&                             rChild,
                        const OperatingComponent::LogicalOperator lo,
                        const Boolean                             neg):

                        rChild_(rChild),
                        logOp_(lo),
                        negate_(neg)
{
  CALL_TRACE("OperatingComponent (rChild, lo, neg)");
  CLASS_PRE_CONDITION((lo == OperatingComponent::AND) ||
                      (lo == OperatingComponent::OR));
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OperatingComponent
//
//@ Interface-Description
//      Copy Constructor.
//
// >Von
//      rOpComp  Reference to the object to be copied from.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
OperatingComponent::OperatingComponent(const OperatingComponent& rOpComp): 

                                       rChild_(rOpComp.rChild_),
                                       logOp_ (rOpComp.logOp_),
                                       negate_(rOpComp.negate_)
{
  CALL_TRACE("OperatingComponent(rOpComp)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OperatingComponent
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
OperatingComponent::~OperatingComponent(void)
{
  CALL_TRACE("~OperatingComponent(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluate
//
//@ Interface-Description
//      During the evaluation of the Operating Parameter Hierarchy when marked
//      OperatingGroup objects are messaged to evaluate themselves, they in
//      turn message their children via OperatingComponent objects to evaluate
//      themselves.  The parent OperatingGroup passes a temporary Boolean to
//      each of its child OperatingComponent objects.  The OperatingComponent
//      messages the child AlarmOperand to evaluate itself and negates the
//      returned value if attribute negate_ is TRUE.  It then combines the
//      state of the child with the passed parameter, state, following the
//      Boolean logic contained in attribute logOp_ and returns the result.
// >Von
//      state  Represents the evaluated states of the other children
//             AlarmOperand objects.  The state of the AlarmOperand referred to
//             by attribute rChild_ is combined with this Boolean value
//             according to attributes logOp_ and negate_.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
OperatingComponent::evaluate(Boolean state) 
{
  CALL_TRACE("evaluate(state) ");

  Boolean conditionState = FALSE;
  
  conditionState = rChild_.evaluate();

  if (negate_) // $[TI1]
    conditionState = !conditionState;
  // else $[TI2]

  if (logOp_ == OperatingComponent::AND) // $[TI3]
    state = state && conditionState; // $[TI3.1] $[TI3.2]
  else // $[TI4]
    state = state || conditionState; // $[TI4.1] $[TI4.2]

  return(state);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
OperatingComponent::SoftFault(const SoftFaultID  softFaultID,
                              const Uint32       lineNumber,
                              const char*        pFileName,
                              const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::OPERATINGCOMPONENT, 
                          lineNumber, pFileName, pPredicate);
}
