#ifndef BdIoUtil_HH
#define BdIoUtil_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename: BdIoUtil.hh - BD IO utilities and memory map.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/BdIoUtil.hhv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "PostDefs.hh"

// When reading the ADC, only 12 bits are valid
enum AdcMask
{
    ADC_MASK                = 0xfff
};

//commands to ADC and safety valve
enum AdcCommands
{
     ADC_FREEZE_CMD          = 0
    ,ADC_SOFT_RESET_CMD      = 0
    ,ADC_REPEAT_CMD          = 0
    ,ADC_RESYNCH_CMD         = 0
    ,SAFE_STATE_CMD          = 0
    ,SAFETY_VALVE_OPEN_CMD   = 0x0000
    ,SAFETY_VALVE_CLOSE_CMD  = 0x0100
    ,BD_AUDIO_ALARM_OFF      = 0x2000
    ,SELECT_SUBMUX1_CMD      = 0x0000    //DACWRAP
    ,SELECT_SUBMUX2_CMD      = 0x1000    //AI_PCBA_REVISION
    ,SELECT_SUBMUX3_CMD      = 0x8000    //SYSTEM_BATTERY_MODEL
    ,SELECT_SUBMUX4_CMD      = 0x9000    //SPARE
    ,ADC_CYCLE_MS            = 1         //time constant
    ,POST_SERIAL_RW_IMAGE    = 0x5700
 
};

enum VentControlStatusCmd
{
     LOUI_LED1_ON            = 0x01
    ,VENT_INOP_LED_ON        = 0x04
    ,SVO_LED_ON              = 0x08
};

enum SafetyNetReadStatus
{
     VENT_INOP_INACTIVE      = 0x10
    ,SAFE_STATE_INACTIVE     = 0x08
    ,TEST_MODE_LATCH_SET     = 0x04
    ,TEST_MODE_SWITCH_PRESSED= 0x04
};

enum SafetyNetControlACmd
{
     SAFE_STATE_A            = 0x00
	,SAFE_OFF_A              = 0x08
    ,FORCE_VENTINOP_A        = 0x04
    ,TEST_ENABLE             = 0x02
    ,DISABLE_TEN_SEC_A       = 0x01
};

enum SafetyNetControlBCmd
{
     SAFE_STATE_B            = 0x00
    ,SAFE_OFF_B              = 0x08
    ,FORCE_VENTINOP_B        = 0x04
    ,DISABLE_TEN_SEC_B       = 0x01
};

enum PowerSupplyCmd
{
     BPS_TESTMODE_OFF        = 0x01
    ,LOUI_LED2_ON            = 0x02
};

enum AdcSystemStatusPortMask
{
     POWER_SWITCH_ON_MASK    = 0x0100
};

enum AnalogIfError
{
     AIF_ADC_TIMING_FAULT             = 0x0001
    ,AIF_ADC_CHANNEL_SEQUENCER_FAULT  = 0x0002
    ,AIF_HAMMING_DECODE_FAULT         = 0x0004
};
 
// AI I/O  port addresses
// NOTE: These addresses are based upon addresses defined
//       in "IODevicesMemoryMap.hh" in BD-IO subsystem.
//       These could be static const but the MRI compiler generates 50% 
//       less code and no extra storage using #define instead
#define AI_BASE_ADDR              (0xFFBEA300)
#define SERIAL_RW_EEPROMS         ((volatile Uint16 *)(AI_BASE_ADDR + 0x4))
#define WRITE_IO_PORT             ((volatile Uint16 *)(AI_BASE_ADDR + 0xE))
#define ADC_SYSTEM_STATUS_PORT    ((volatile Uint16 *)(AI_BASE_ADDR + 0x6))
#define ADC_MONITOR_COUNT_PORT    ((volatile Uint16 *)(AI_BASE_ADDR + 0x8))
#define ADC_RESYNCH_PORT          ((volatile Uint16 *)(AI_BASE_ADDR + 0x10))
#define ADC_ERROR_STATUS_PORT     ((volatile Uint16 *)(AI_BASE_ADDR + 0x12))
#define ADC_FREEZE_PORT           ((volatile Uint16 *)(AI_BASE_ADDR + 0x14))
#define ADC_REPEAT_PORT           ((volatile Uint16 *)(AI_BASE_ADDR + 0x16))
#define ADC_SOFT_RESET_PORT       ((volatile Uint16 *)(AI_BASE_ADDR + 0x1A))
#define AIR_PSOL_DAC_PORT         ((volatile Uint16 *)(AI_BASE_ADDR + 0x20))
#define O2_PSOL_DAC_PORT          ((volatile Uint16 *)(AI_BASE_ADDR + 0x22))
#define EXH_VALVE_DAC_PORT        ((volatile Uint16 *)(AI_BASE_ADDR + 0x24))
#define EV_DAMPING_GAIN_DAC_PORT  ((volatile Uint16 *)(AI_BASE_ADDR + 0x26))
#define DACWRAP_WRITE_PORT        ((volatile Uint16 *)(AI_BASE_ADDR + 0x28))
#define ADC_READ_PORT             ((volatile Uint16 *)(AI_BASE_ADDR + 0x40))

//CPU I/O addresses
// CPU IO
#define CPU_IO_BASE_ADDR          (0xFFBEB000)
#define POWER_SUPPLY_WRITE_PORT   ((volatile Uint8 *) (CPU_IO_BASE_ADDR + 0x4))
#define SAFETY_NET_WRITE_REGA     ((volatile Uint8 *) (CPU_IO_BASE_ADDR + 0x8))
#define SAFETY_NET_WRITE_REGB     ((volatile Uint8 *) (CPU_IO_BASE_ADDR + 0xA))
#define SAFETY_NET_READ_REG       ((volatile Uint8 *) (CPU_IO_BASE_ADDR + 0xC))
#define VENT_CONTROL_STATUS       ((volatile Uint8 *) (CPU_IO_BASE_ADDR + 0x10))

Uint16      ReadWord(volatile Uint16 * const address);
Uint8       ReadByte(volatile Uint16 * const address);
void		WriteWord(volatile Uint16 * const address, const Uint16 data);
void		WriteByte(volatile Uint16 * const address, const Uint8 data);
void		ActivateVentInop(void);
void		ActivateSafeState(void);
void        SwitchBdAudioAlarm(Boolean state);
void        SwitchBdLeds(Boolean state);

#endif // BdIoUtil_HH
