#ifndef FlashProgrammer_HH
#define FlashProgrammer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: FlashProgrammer - Provides an interface to determine which 
//                          flash chip programming algorithms to use based on 
//                          the initialize function.  
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/FlashProgrammer.hhv   25.0.4.0   19 Nov 2013 14:16:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision 001  By: Rhomere Jimenez  Date: 07-March-2006  DR Number: 6192
//   Project:   Xena
//   Description:
//      Initial version.
//====================================================================

#if defined( SIGMA_COMMON )
    #include "DownloadFault.hh"
#endif

#include "Flash28F400B.hh"
#include "FlashS29AL008D.hh"
#include "FlashMemCons.hh"
#include "OsClassId.hh"

#include "SoftFaultId.hh"


#define  MAX_NUM_SUB_BLOCKS 19
#define  MAX_TOTAL_NUM_SUB_BLOCKS  (MAX_NUM_SUB_BLOCKS * 2)

//@ Usage-Classes
//@ End-Usage

struct FlashDevice
{
    //@ Type:  FlashDeviceId
    // All of the possible Flash Chip Type.
    enum FlashDeviceId
    {
        NoFlashChipDetected = 0,
        Flash28F400B  = 0x44714471,   // (i.e) Intel, Micron
        FlashS29AL008D = 0x225b225b    // (i.e) Spansion       
    };
};


class FlashProgrammer 
{
  public:

    static void Initialize(void);

#if defined( SIGMA_COMMON ) || defined( SIGMA_UNIT_TEST )

    static Int FlashEraseBlockNonBlocking(volatile Uint32 * address);
 
    static Int FlashCheck(volatile Uint32 * address);

    static Int FlashEntireChipErase(void);

    static void SoftFault( const FaultType  softFaultID,
                           const Uint32     lineNumber,
                           const char       *pFileName  = NULL, 
                           const char       *pPredicate = NULL) ;

#endif // SIGMA_COMMON || SIGMA_UNIT_TEST

    static Uint32 GetSubBlockSize(Uint8 index);
    static Uint32 GetWsmBusy(void);
    static Int FlashEraseBlock(volatile Uint32 * address);
    static Int FlashWriteWord(volatile Uint32 * address, const Uint data);
    static Uint32 GetNumOfSubBlocks(void);
    static Int FlashGetDeviceId( volatile Uint32 * address );
    static Uint32 * GetFlashBlockAddress(Uint8 index);
    static Uint32  GetTotalSubBlocks(void);
    static void CopyCheckSumTable(void);
    static Int WriteCheckSumTable(void);


  protected:

  private:
    FlashProgrammer( void);                 // not implemented...
    ~FlashProgrammer( void);                 // not implemented...

    FlashProgrammer( const FlashProgrammer&) ;      // not implemented...
    void operator=( const FlashProgrammer&) ;   // not implemented...


    //@ Data-Member:  flashChipType_
    //  Type of Flash Chip
    static Uint32 flashChipType_;

    //@ Data-Member:  checkSumTable_
    //  Stores the checksum table
    static Uint32  checkSumTable_[256] ;


} ;

#endif // FlashProgrammer_HH 
