#ifndef Image_HH
#define Image_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Image - Represents monochrome and color images displayed by Bitmap objects.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Image.hhv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 010   By: rhj    Date:  21-May-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Added TrendIcon.
// 
//  Revision: 009   By: btray    Date:  20-Jul-1999    DCS Number: 5424
//  Project: 840 Neonatal
//  Description:
//	Added new CheckMarkIcon for Development Options subscreen.
//
//  Revision: 008   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 007   By: hhd    Date: 08-Jun-1999   DR Number: 5423
//  Project:  ATC
//  Description:
//	Removed CHECKMARK_ICON & CheckmarkIconBits_.
//	Added ARROW_ICON & ArrowIconBits_.
//
//  Revision: 006   By: sah    Date: 29-Apr-1999   DR Number: 5365
//  Project:  ATC
//  Description:
//      Added new 'CHECKMARK_ICON' bitmap, to be used for the new
//      flashing, verification-needed mechanism.
//
//  Revision: 005   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 004  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 003  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 002  By: mpm      Date: 22-Jan-1996  DCR Number: 654
//    Project:  Sigma (R8027)
//    Description:
//      Added COLOR_BLINKING_ALARM icon
//  
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"

//@ Usage-Classes
#include "DibInfo.hh"  // defined in VGA-Graphics-Devices
//@ End-Usage

class Image 
{
  public:

	static void Initialize(void);

	static void SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName  = NULL,
						  const char*        pPredicate = NULL);

	//@ Data-Member: RMonoAlarmIcon
	//  reference to the MonoAlarmIcon DibInfo struct
	static DibInfo & RMonoAlarmIcon;

	//@ Data-Member: RWaveformIcon
	//  reference to the WaveformIcon DibInfo struct
	static DibInfo & RWaveformIcon;

	//@ Data-Member: RBlinkingAlarmIcon
	//  reference to the BlinkingAlarmIcon DibInfo struct
	static DibInfo & RBlinkingAlarmIcon;

	//@ Data-Member: RAlarmLogIcon
	//  reference to the AlarmLogIcon DibInfo struct
	static DibInfo & RAlarmLogIcon;

	//@ Data-Member: RMoreDataIcon
	//  reference to the MoreDataIcon DibInfo struct
	static DibInfo & RMoreDataIcon;

	//@ Data-Member: ROtherScreensIcon
	//  reference to the OtherScreensIcon DibInfo struct
	static DibInfo & ROtherScreensIcon;

	//@ Data-Member: RAlertIcon
	//  reference to the AlertIcon DibInfo struct
	static DibInfo & RAlertIcon;

	//@ Data-Member: RLockedPadlockIcon
	//  reference to the LockedPadlock DibInfo struct
	static DibInfo & RLockedPadlockIcon;

	//@ Data-Member: RUnlockedPadlockIcon
	//  reference to the RUnlockedPadlock DibInfo struct
	static DibInfo & RUnlockedPadlockIcon;

	//@ Data-Member: RXAxisIcon
	//  reference to the XAxisIcon DibInfo struct
	static DibInfo & RXAxisIcon;

	//@ Data-Member: RYAxisIcon
	//  reference to the YAxisIcon DibInfo struct
	static DibInfo & RYAxisIcon;

	//@ Data-Member: RLeftArrowIcon
	//  reference to the LeftArrowIcon DibInfo struct
	static DibInfo & RLeftArrowIcon;

	//@ Data-Member: RRightArrowIcon
	//  reference to the RightArrowIcon DibInfo struct
	static DibInfo & RRightArrowIcon;

	//@ Data-Member: RCheckArrowIcon
	// Arrow used for new flashing, verification-needed mechanism.
	static DibInfo & RCheckArrowIcon;

	//@ Data-Member: RCheckMarkIcon
	// Check mark used to indicate if a software option is enabled or not. 
	static DibInfo & RCheckMarkIcon;

	//@ Data-Member: RTrendIcon
	//  reference to the RTrendIcon DibInfo struct
	static DibInfo & RTrendIcon;

protected:

private:
	Image(void);            		// Declared but not implemented
	Image(const Image&);            // Declared but not implemented
	~Image(void);            		// Declared but not implemented
	void operator=(const Image&);   // Declared but not implemented

	//@ Data-Member: DibInfos_
	//  Array containing pointers to DibInfo for each image
	static DibInfo * const PDibInfos_[];


	//@ Data-Member: ArrowIconBits_
	// Color VGA bit pattern for ARROW_ICON.
	static Uint8 ArrowIconBits_[];

};

#endif // Image_HH 
