#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LowToHighPeepPhase - Implements transition from low to high
//		peep during BiLevel.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from PsvPhase class.  No public
//		methods are defined by this class since base class methods are used.
//		Protected virtual methods are implemented to support the base class.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms for transitioning from a low
//		peep to a high peep during BiLevel.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The data members values from the base class are defined	in the pure
//		virtual methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/LowToHighPeepPhase.ccv   25.0.4.0   19 Nov 2013 13:59:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: cep     Date:  17-Apr-2002    DR Number: 5899
//  Project:  VCP
//  Description:
//		Added comment: timeToTarget_ gets set in PsvPhase::determineTimeToTarget().
//
//  Revision: 006  By: syw     Date:  10-Feb-2000    DR Number: 5617, 5638
//  Project:  NeoMode
//  Description:
//		Use the same algorithm for time to target as PsvPhase for neonatal.
//
//  Revision: 005  By: sah     Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//      Changed 'PEEP_HI' to 'PEEP_HIGH', also, added in new 'PEEP_HIGH_TIME'
//      setting.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv    Date: 08-Sep-1998     DR Number:DCS 5140
//       Project:  BiLevel
//       Description:
//             Added method relinquishControl() to disable disconnect detection
//             when the phase trigger is not time backup exp trigger and not
//             immediate exp trigger.
//
//  Revision: 002  By:  iv     Date:  01-Jul-1998    DR Number: DCS 5112
//       Project:  Sigma (840)
//       Description:
//		 	Changed method enableExhalationTriggers_() not to include the backup time
//          trigger.
//
//  Revision: 001  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.
//
//=====================================================================

#include "LowToHighPeepPhase.hh"

#include "BreathMiscRefs.hh"
#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "ControllersRefs.hh"

//@ Usage-Classes

#include "O2Mixture.hh"
#include "PhasedInContextHandle.hh"
#include "BdDiscreteValues.hh"
#include "BD_IO_Devices.hh"
#include "BreathTrigger.hh"
#include "Peep.hh"
#include "DisconnectTrigger.hh"
#include "PressureController.hh"
#include "TimerBreathTrigger.hh"

//@ End-Usage

//@ Code...

extern const Real32 PEEP_THRESHOLD; 
const Int32 MAX_TIME_LIMIT = 5000;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LowToHighPeepPhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

LowToHighPeepPhase::LowToHighPeepPhase( void)
 : PsvPhase() 			// $[TI1]
{
	CALL_TRACE("LowToHighPeepPhase::LowToHighPeepPhase( void)") ;
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LowToHighPeepPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

LowToHighPeepPhase::~LowToHighPeepPhase(void)
{
	CALL_TRACE("LowToHighPeepPhase::~LowToHighPeepPhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
//      This method takes a BreathTrigger& as an argument and has no return
//      value.  This method is called at the beginning of exhalation to wrap
//      up inspiration phase in preparation for the next inspiration phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//      If the trigger is not immediate exp,
//      reset the volume criteria for the disconnect trigger.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
LowToHighPeepPhase::relinquishControl( const BreathTrigger& trigger)
{

	PressureBasedPhase::relinquishControl(trigger) ;
	if (trigger.getId() != Trigger::IMMEDIATE_EXP)
	{
	  	// $[TI1]
		RDisconnectTrigger.disableVolumeCriteria() ;
	}	// implied else $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
LowToHighPeepPhase::SoftFault( const SoftFaultID  softFaultID,
                   	 const Uint32       lineNumber,
					 const char*        pFileName,
		   			 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, LOWTOHIGHPEEPPHASE,
                             lineNumber, pFileName, pPredicate) ;
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineEffectivePressureAndBiasOffset_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called by the base class, PressureBasePhase, to determine the
//		effective pressure and the bias offset. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set effectivePressure_ to the PEEP_HI - peep + biasOffset_.
// 		$[BL04021] $[BL04038] $[BL04039]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
LowToHighPeepPhase::determineEffectivePressureAndBiasOffset_( void)
{
	CALL_TRACE("LowToHighPeepPhase::determineEffectivePressureAndBiasOffset_( void)") ;
	
	// $[TI1]
	
	biasOffset_ = 1.5F ;
	effectivePressure_ = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_HIGH).value
							- RPeep.getActualPeep() + biasOffset_ ;
}
		

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineTimeToTarget_()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called by the base class, PressureBasePhase, to determine the
//		time to target.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The time to target is the same as the base class call but is limitted
//		to BILEVEL_HIGH_TIME * 2.0 / 3.0
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
LowToHighPeepPhase::determineTimeToTarget_( void)
{
	CALL_TRACE("LowToHighPeepPhase::determineTimeToTarget_( void)") ;

	const BoundedValue& BILEVEL_HIGH_TIME =
				PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_HIGH_TIME) ;

	PsvPhase::determineTimeToTarget_() ;

	static const Real32  FACTOR_ = 2.0 / 3.0 ;

// 	timeToTarget_ has been set in PsvPhase::determineTimeToTarget_( void) function.
	timeToTarget_ = MIN_VALUE( BILEVEL_HIGH_TIME * FACTOR_, timeToTarget_) ;
	
	// $[TI1]	BILEVEL_HIGH_TIME * FACTOR_ is minimum
	// $[TI2]	timeToTarget_ is minimum
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableExhalationTriggers_()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called by the base class, PressureBasePhase, to enable the
//		triggers that will terminate inspiration.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The EnablePatientTriggers is called on the very first cycle of the
//      breath to enable all patient related triggers.
//		This method must be called BEFORE elapsedTimeMs_ is updated in newCycle().
// 		$[BL04023]
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
LowToHighPeepPhase::enableExhalationTriggers_( void)
{
	CALL_TRACE("PsvPhase::enableExhalationTriggers_( void)") ;

	if (elapsedTimeMs_ == 0)
	{
		// $[TI3]
   		BreathPhase::EnablePatientTriggers( BreathPhaseType::INSPIRATION) ;
		((BreathTrigger&)RBackupTimeExpTrigger).enable();

        // the member maxInspTimeMs_ is set in the newBreath() method of the
        // base class when the method determineTimeToTarget_() is called.
        Int32 maxInspTime = maxInspTimeMs_;

        Real32 highPeep = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_HIGH).value;
        if(highPeep > PEEP_THRESHOLD)
        {
            // $[TI3.1]
            maxInspTime = MAX_TIME_LIMIT;
        }
            // $[TI3.2]
        
		RBackupTimeExpTrigger.restartTimer(maxInspTime) ;
		RDisconnectTrigger.setTimeLimit(maxInspTime) ;

		RDisconnectTrigger.setFlowCmdLimit( RPressureController.getFlowCommandLimit()) ;
		
	}	// implied else $[TI4]

 	Real32 highTimeMs = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_HIGH_TIME).value ;
	
	if (elapsedTimeMs_ >= highTimeMs - CYCLE_TIME_MS)
	{
	  	// $[TI1]
   		((BreathTrigger&)RImmediateExpTrigger).enable() ;
   	} 	// implied else $[TI2]
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================











