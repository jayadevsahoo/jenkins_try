
#ifndef PatientDataId_HH
#define PatientDataId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//  PatientDataId - enum ids for all the patient data items.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/PatientDataId.hhv   10.7   08/17/07 10:28:06   pvcs  
//
//@ Modification-Log
//
//  Revision: 014   By:   rhj    Date: 05-March-2009    SCR Number: 6454
//  Project:  840S
//  Description:
//       Added INSP_TIDAL_VOL_ALARM_ITEM.
//  
//  Revision: 013   By:   rhj    Date: 07-July-2008    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 012   By: gdc      Date:  05-Jun-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project related changes.
//
//  Revision: 011   By: gdc      Date:  10-Jun-2005    SCR Number: 6170
//  Project:  NIV2
//  Description:
//      Added processing to detect NIV high inspiratory time and generate 
//      patient-data callback for displaying alert.
//
//  Revision: 010   By: gdc      Date:  16-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      No change.
//
//  Revision: 009   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  PAV
//  Description:
//      Added 'IS_APNEA_ACTIVE_ITEM' and 'IS_ERROR_STATE_ITEM', to allow
//      shadow traces to determine whether to be present, or not.
//
//  Revision: 008   By: jja      Date:  22-Mar-2002    DR Number: 5790
//  Project:  VCP
//  Description:
//      Revise names for consistency now that Vti applies to mand breaths also.
//
//  Revision: 007   By: yakovb   Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Handle VC+/VS Startup messages.
//
//  Revision: 006   By: syw   Date:  15-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      * added mode and support type setting values fields for managing
//        waveform data display
//      * added three new spontaneous data items:  insp time, percent
//        Ti, and f-to-Vt.
//      * added lung flow and lung volume shadow traces
//
//  Revision: 005   By: syw   Date:  01-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      * added PAV state field
//      * added PAV-based R, C, E and WOB items.
//      * added lung pressure for shadow trace
//
//  Revision: 004  By: yyy    Date: 24-Aug-99   DR Number: 5481
//  Project:  ATC
//  Description:
//      Added BREATH_TYPE_DISPLAY_ITEM GUI breath bar display.
//
//  Revision: 003  By:  hct   Date:  19-Jan-1999    DR Number: 5322
//       Project:  ATC (R8027)
//       Description:
//             ATC initial version.
//
//  Revision: 002  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling for Inspiratory Pause data.
//
//  Revision: 001  By:  jhv    Date:  10-Feb-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


class PatientDataId
{
public:
	enum PatientItemId
	{
		NULL_PATIENT_DATA_ITEM   = -1,
		LOW_PATIENT_ITEM =  0,

		//==============================================================
		// Discrete-valued patient data items...
		//==============================================================

		LOW_DISCRETE_ITEM= LOW_PATIENT_ITEM,
		BREATH_PHASE_ITEM = LOW_DISCRETE_ITEM,
		BREATH_TYPE_ITEM,
		BREATH_TYPE_DISPLAY_ITEM,
		IS_PEEP_RECOVERY_ITEM,
		STATIC_AIRWAY_RESISTANCE_VALID_ITEM,
		STATIC_LUNG_COMPLIANCE_VALID_ITEM,
		END_EXP_PAUSE_PRESSURE_STATE_ITEM,
		PAV_STATE_ITEM,
		VTPCV_STATE_ITEM,
		PREVIOUS_BREATH_TYPE_ITEM,
		IS_NIV_INSP_TOO_LONG_ITEM,
		IS_LEAK_DETECTED_ITEM,
		HIGH_DISCRETE_ITEM = IS_LEAK_DETECTED_ITEM,

		//==============================================================
		// Bounded-valued patient data items...
		//==============================================================

		LOW_BOUNDED_ITEM,
		END_EXP_PRESS_ITEM = LOW_BOUNDED_ITEM,
		EXH_TIDAL_VOL_ITEM,
		PEAK_CCT_PRESS_ITEM,
		MEAN_CCT_PRESS_ITEM,
		IE_RATIO_ITEM,

		// Deduced Breath Data Item
		EXH_MAND_TIDAL_VOL_ITEM,
		EXH_SPONT_TIDAL_VOL_ITEM,
		INSP_TIDAL_VOL_ITEM,
		INSP_TIDAL_VOL_ALARM_ITEM,

		// Average Breath Data
		EXH_MINUTE_VOL_ITEM,
		EXH_SPONT_MINUTE_VOL_ITEM,
		TOTAL_RESP_RATE_ITEM,

		// Positive End Expiratory Pressure Data
		PEEP_INTRINSIC_ITEM,
		PEEP_TOTAL_ITEM,

		// Inspiratory Pause Data
		STATIC_LUNG_COMPLIANCE_ITEM,
		STATIC_AIRWAY_RESISTANCE_ITEM,
		PLATEAU_PRESSURE_ITEM,

		// Leak Compensation Data
		PERCENT_LEAK_ITEM,
		EXH_LEAK_RATE_ITEM,
		INSP_LEAK_VOL_ITEM,

		// Real Time Breath Data
		END_INSP_PRESS_ITEM,


		// New patient data item
		DELIVERED_O2_PERCENTAGE_ITEM,
		SPONT_RATE_TO_VOLUME_RATIO_ITEM,
		SPONT_PERCENT_TI_ITEM,
		SPONT_INSP_TIME_ITEM,
		PAV_LUNG_COMPLIANCE_ITEM,
		PAV_PATIENT_RESISTANCE_ITEM,
		TOTAL_AIRWAY_RESISTANCE_ITEM,
		PAV_LUNG_ELASTANCE_ITEM,
		PAV_PEEP_INTRINSIC_ITEM,
		ELASTANCE_WORK_OF_BREATHING_ITEM,
		RESISTIVE_WORK_OF_BREATHING_ITEM,
		PATIENT_WORK_OF_BREATHING_ITEM,
		TOTAL_WORK_OF_BREATHING_ITEM,

		// respiratory mechanics items
		DYNAMIC_COMPLIANCE_ITEM,
		DYNAMIC_RESISTANCE_ITEM,
		PEAK_SPONT_INSP_FLOW_ITEM,
		C20_TO_C_RATIO_ITEM,
		PEAK_EXPIRATORY_FLOW_ITEM,
		END_EXPIRATORY_FLOW_ITEM,
		NIF_PRESSURE_ITEM,
		P100_PRESSURE_ITEM,
		VITAL_CAPACITY_ITEM,


		HIGH_BOUNDED_ITEM = VITAL_CAPACITY_ITEM,
		NUM_TOTAL_PATIENT_DATA,

		// Waveform Data
		LOWEST_WAVEFORM_ITEM = NUM_TOTAL_PATIENT_DATA,
		NET_FLOW_ITEM= LOWEST_WAVEFORM_ITEM,
		NET_VOL_ITEM,
		CIRCUIT_PRESS_ITEM,
		LUNG_PRESS_ITEM,
		LUNG_FLOW_ITEM,
		LUNG_VOLUME_ITEM,
		IS_APNEA_ACTIVE_ITEM,
		IS_ERROR_STATE_ITEM,
		MODE_SETTING_ITEM,
		SUPPORT_TYPE_SETTING_ITEM,
		AUTOZERO_ACTIVE_ITEM,
		HIGHEST_WAVEFORM_ITEM = AUTOZERO_ACTIVE_ITEM
	};
};

#endif // PatientDataId_HH

