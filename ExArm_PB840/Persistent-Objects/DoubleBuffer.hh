
#ifndef DoubleBuffer_HH
#define DoubleBuffer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  DoubleBuffer - Double-Buffered Memory Framework.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/DoubleBuffer.hhv   25.0.4.0   19 Nov 2013 14:18:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By:  sah    Date:  25-Jun-1997    DCS Number: 1908
//  Project:  Sigma (R8027)
//  Description:
//     Since the derived class now manages its own data area separate
//     from what this base class manages (i.e., the "semaphore" area),
//     a method is needed by the derived class, whereby it can put this
//     instance into an "ILLEGAL" state during its verification.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//  
//====================================================================

#include "Sigma.hh"
#include "PersistentObjsId.hh"
#include "NovRamStatus.hh"

//@ Usage-Classes
#include "CrcUtilities.hh"
//@ End-Usage


class DoubleBuffer
{
  public:
    //@ Type:  DoubleBufferState
    // Represents the current state of this double buffer.
    enum DoubleBufferState
    {
      BOTH_BUFFERS_OK = 0x75a5a5a5,
      WRITE_BUFFER_OK = 0x79696969,
      READ_BUFFER_OK  = 0x76969696,
      ILLEGAL_STATE   = 0x7a5a5a5a
    };

    ~DoubleBuffer(void);

    inline DoubleBuffer::DoubleBufferState  getState(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  protected:
#if defined(SIGMA_UNIT_TEST)
  public:  // make everything public for unit testing...
#endif // defined(SIGMA_UNIT_TEST)
    //@ Type:  BufferId
    // Ids for the two buffers.
    enum BufferId
    {
      READ_BUFFER_ID,
      WRITE_BUFFER_ID
    };

    DoubleBuffer(Byte* pBuffer0, Byte* pBuffer1, const size_t bufferSize);

    NovRamStatus::InitializationId initBuffer_  (const size_t sizeofDataItem);
    NovRamStatus::VerificationId   verifyBuffer_(const size_t sizeofDataItem);

    Uint  accessReadBuffer_ (void) const;
    Uint  accessWriteBuffer_(void);

    void  updateUsingReadBuff_ (void);
    void  updateUsingWriteBuff_(void);

    inline void  setToIllegalState_(void);

  private:
#if defined(SIGMA_UNIT_TEST)
  public:  // make everything public for unit testing...
#endif // defined(SIGMA_UNIT_TEST)
    DoubleBuffer(const DoubleBuffer&);		// not implemented...
    DoubleBuffer(void);				// not implemented...
    void  operator=(const DoubleBuffer&);	// not implemented...

    inline Boolean  doBuffersMatch_(void) const;

    inline size_t  infoCrcOffset_(void) const;

    inline Boolean  isInfoValid_  (const BufferId bufferId,
				   const size_t   sizeofDataItem) const;
    inline Boolean  isBufferValid_(const BufferId bufferId) const;

    inline CrcValue  calcInfoCrc_  (const BufferId bufferId) const;
    inline CrcValue  calcBufferCrc_(const BufferId bufferId) const;

    //@ Type:  InfoRecord_
    // This stores the information about a single buffer.  The ordering of
    // the fields matters -- 'infoCrc' MUST be the last field.
    struct InfoRecord_
    {
      Byte*     pBuffer;	// pointer to the buffer...
      size_t    bufferSize;	// size of the buffer...
      CrcValue  bufferCrc;	// CRC value for the buffer...
      CrcValue  infoCrc;	// CRC value for these info fields...
    };

    //@ Data-Member:  currState_
    // The current state of the buffers.
    DoubleBufferState  currState_;

    //@ Data-Member:  arrInfoRecord_
    // An array of two buffer information records, one for each buffer.
    InfoRecord_  arrInfoRecord_[2];
};


// Inlined Methods...
#include "DoubleBuffer.in"


#endif // DoubleBuffer_HH 
