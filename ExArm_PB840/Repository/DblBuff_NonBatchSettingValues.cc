#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class:  DblBuff_NonBatchSettingValues - Double-Buffered Data Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides its base class ('DoubleBuffer') with the
//  two data buffers (items).  Most of the double-buffering functionality
//  is contained in the base class.  This class is responsible for
//  providing two data items -- as the two buffers -- to the base class,
//  and using the base class's access-permission methods to control
//  access to the two data items.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a generic placeholder of data items that are
//  to be double-buffered.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An array of two of the specified data items are contained internally,
//  while all access to these two data items is conducted through
//  the base class's access methods.
//
//  This class protects against multiple threads by using a multi-reader,
//  single-writer semaphore.  This has the effect of a mutual-exclusion
//  semaphore for "write" access, while allowing multiple "read" accesses
//  through when no "write" access is grabbeed.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/DoubleBufferData.C_v   25.0.4.0   19 Nov 2013 14:18:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//	Also, as part of making all 'SoftFault()' methods non-inlined,
//	I'm also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//	Added call to 'NovRamManager::ReportAuxillaryInfo()' when an
//	invalid state is detected.
//
//  Revision: 002  By:  sah    Date:  25-Jun-1997    DCS Number: 1908
//  Project:  Sigma (R8027)
//  Description:
//     Add mechanism to allow different semaphores to be used with different
//     instances of this class.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "DblBuff_NonBatchSettingValues.hh"

//@ Usage-Classes
#include "NovRamManager.hh"
//@ End-Usage

//@ Code

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DblBuff_NonBatchSettingValues(semaphore)  [Constructor]
//
//@ Interface-Description
//  Construct a default double-buffered data manager.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The base class's constructor will complete BEFORE the constructors
//  of the two data items are run, therefore this constructor must
//  update itself within the body of this constructor.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (getState() == DoubleBuffer::BOTH_BUFFERS_OK)
//  (verifySelf(getSemaphoreId_()) == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

DblBuff_NonBatchSettingValues::DblBuff_NonBatchSettingValues(
			  const NovRamSemaphore::AccessIdType semaphoreId
				      )
       : DoubleBuffer((Byte*)(arrDataItems_ + DoubleBuffer::READ_BUFFER_ID),
		      (Byte*)(arrDataItems_ + DoubleBuffer::WRITE_BUFFER_ID),
		      sizeof(NonBatchSettingValues))
{
  CALL_TRACE("DblBuff_NonBatchSettingValues(semaphoreId)");

  // store 'semaphoreId'...
  updateSemaphoreId_(semaphoreId);

  //===================================================================
  //  at this point the current state is 'ILLEGAL_STATE'...
  //===================================================================

  SAFE_CLASS_ASSERTION((getState() == DoubleBuffer::ILLEGAL_STATE));

  // the constructors of the two data items in 'arrDataItems_[]' were run,
  // therefore use the "write" buffer to update the double buffer...
  updateUsingWriteBuff_();
  SAFE_CLASS_POST_CONDITION((getState() == DoubleBuffer::BOTH_BUFFERS_OK),
			    DoubleBuffer);
  SAFE_CLASS_POST_CONDITION((verifySelf(getSemaphoreId_()) == NovRamStatus::ITEM_VALID),
  			    DoubleBuffer);

  //===================================================================
  //  at this point the current state is 'BOTH_BUFFERS_OK'...
  //===================================================================
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DblBuff_NonBatchSettingValues()  [Destructor]
//
//@ Interface-Description
//  Destroy this buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

DblBuff_NonBatchSettingValues::~DblBuff_NonBatchSettingValues(void)
{
  CALL_TRACE("~DblBuff_NonBatchSettingValues()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize(semaphoreId)
//
//@ Interface-Description
//  This is called when this instance has already been initialized but
//  a reset occurred, therefore this instance needs to be verified
//  and initialized.  There are three types of "initialization" that can
//  occur:  verification only, "correction" of invalid buffer, or a full
//  initialization.  Each of these three types will leave this double
//  buffer instance in a "BOTH OK" state.
//
//  If both buffers are valid (verification only) OR only one of the buffers
//  is OK ("correction"), a status of 'NovRamStatus::ITEM_VERIFIED' is
//  returned.  This status indicates that the values of the two buffers
//  were NOT reverted to their initialization state.
//
//  If a full initialization occurs, a status of
//  'NovRamStatus::ITEM_INITIALIZED' is returned.  This status indicates
//  that the values of the two buffers WERE reverted to their initialization
//  state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passing the size of the buffered type, use the base class's
//  reinitialization method.
//
//  Though the return type has three possible values, the "RECOVERED" state
//  is never returned from this method, because it can't be determined
//  whether a power loss occurred in the middle of a valid buffer operation,
//  or 
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (getState() == DoubleBuffer::BOTH_BUFFERS_OK  &&
//   verifySelf(semaphoreId) == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
DblBuff_NonBatchSettingValues::initialize(const NovRamSemaphore::AccessIdType semaphoreId)
{
  CALL_TRACE("initialize(semaphoreId)");

  // store 'semaphoreId'...
  updateSemaphoreId_(semaphoreId);

  NovRamStatus::InitializationId  initStatusId;

  // conduct the initialization...
  initStatusId = initBuffer_(sizeof(NonBatchSettingValues));

  if (getState() == DoubleBuffer::ILLEGAL_STATE)
  {   // $[TI1] -- initiate a full initialization...
    new (this) DblBuff_NonBatchSettingValues(semaphoreId);
  }   // $[TI2] -- no full initialization needed...

  if (initStatusId != NovRamStatus::ITEM_VERIFIED)
  {   // $[TI3]
    NovRamManager::ReportAuxillaryInfo(this, sizeof(DblBuff_NonBatchSettingValues),
				       initStatusId);
  }   // $[TI4]

  return(initStatusId);
}

 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifySelf(semaphoreId)
//
//@ Interface-Description
//  This verifies that this instance is in a valid state.  A valid
//  state is defined as containing a valid buffer state value
//  (i.e. 'BOTH_BUFFERS_OK', 'READ_BUFFER_OK', or 'WRITE_BUFFER_OK'),
//  that matches the actual state of the buffer and buffer information.
//  For example, if the current state is 'READ_BUFFER_OK', the READ
//  buffer must match its CRC AND its buffer information block must
//  match its buffer info CRC.  There are no requirements of the state
//  of a buffer that is not explicitly identified in the state variable
//  (if the current state is 'READ_BUFFER_OK' there is NO requirement on
//  the state of the WRITE buffer).  If either the current state value
//  is not a valid buffer state value (as listed above), or the buffers
//  do not pass the CRC verifications, the buffer manager is considered
//  in an invalid state.
//
//  If this instance is determined to be in a VALID state, 'ITEM_VALID' is
//  returned and the current state is retained, otherwise, 'ITEM_INVALID' is
//  returned and this instance is left with its state set to 'ILLEGAL_STATE'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passing the size of the buffered type, use the base class's
//  verification method.
//
//  This method must grab WRITE access, because if a corruption is detected
//  this instance's state will be modified.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  ((verifySelf(semaphoreId) == NovRamStatus::ITEM_VALID  &&
//    (getState() == DoubleBuffer::BOTH_BUFFERS_OK  ||
//     getState() == DoubleBuffer::READ_BUFFER_OK   ||
//     getState() == DoubleBuffer::WRITE_BUFFER_OK))  ||
//   (verifySelf(semaphoreId) == NovRamStatus::ITEM_VALID  &&
//    getState() == DoubleBuffer::ILLEGAL_STATE))
//@ End-Method
//=====================================================================

NovRamStatus::VerificationId
DblBuff_NonBatchSettingValues::verifySelf(const NovRamSemaphore::AccessIdType semaphoreId)
{
  CALL_TRACE("verifySelf(semaphoreId)");

  NovRamStatus::VerificationId  status;

  if (getSemaphoreId_() == semaphoreId  &&  isSemaphoreIdValid_())
  {   // $[TI1] -- this instance's semaphore ID is valid...
    NovRamSemaphore::RequestWriteAccess(getSemaphoreId_());

    status = verifyBuffer_(sizeof(NonBatchSettingValues));

    NovRamSemaphore::ReleaseWriteAccess(getSemaphoreId_());
  }
  else
  {   // $[TI2] -- this instance's semaphore ID is NOT valid...
    // put this instance into the "ILLEGAL" state...
    setToIllegalState_();

    // return "INVALID" status...
    status = NovRamStatus::ITEM_INVALID;
  }

  if (status != NovRamStatus::ITEM_VALID)
  {   // $[TI3]
    NovRamManager::ReportAuxillaryInfo(this, sizeof(DblBuff_NonBatchSettingValues));
  }   // $[TI4]

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getData(rData)  [const]
//
//@ Interface-Description
//  Return a copy of the read-only data item, in 'rData'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Request access to the "read" data item, then return a copy of it.
//  The state of this buffer manager is not changed by this method.
//---------------------------------------------------------------------
//@ PreConditions
//  (getState() == DoubleBuffer::BOTH_BUFFERS_OK  ||
//   getState() == DoubleBuffer::READ_BUFFER_OK   ||
//   getState() == DoubleBuffer::WRITE_BUFFER_OK)
//---------------------------------------------------------------------
//@ PostConditions
//  (getState() == OLD{getState()})
//  (verifySelf(semaphoreId) == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

void
DblBuff_NonBatchSettingValues::getData(NonBatchSettingValues& rData) const
{
  CALL_TRACE("getData(rData)");

  NovRamSemaphore::RequestReadAccess(getSemaphoreId_());

  SAFE_AUX_CLASS_PRE_CONDITION((getState() == DoubleBuffer::BOTH_BUFFERS_OK  ||
			        getState() == DoubleBuffer::READ_BUFFER_OK   ||
			        getState() == DoubleBuffer::WRITE_BUFFER_OK),
			       getState());

  // get the index to the current "read" buffer by requesting access
  // to the "read" buffer...
  const Uint32  READ_ID = accessReadBuffer_();

  // return a copy of the "read" data item...
  rData = arrDataItems_[READ_ID];

  NovRamSemaphore::ReleaseReadAccess(getSemaphoreId_());
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateData(newData)  [const]
//
//@ Interface-Description
//  Update this buffer's data to 'newData'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Request access to the "write" data item, copy 'newData' into it,
//  then update this buffer manager using the "write" data item.
//---------------------------------------------------------------------
//@ PreConditions
//  (getState() == DoubleBuffer::BOTH_BUFFERS_OK)
//---------------------------------------------------------------------
//@ PostConditions
//  (getState() == DoubleBuffer::BOTH_BUFFERS_OK)
//  (verifySelf(semaphoreId) == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

void
DblBuff_NonBatchSettingValues::updateData(const NonBatchSettingValues& newData)
{
  CALL_TRACE("updateData(newData)");

  NovRamSemaphore::RequestWriteAccess(getSemaphoreId_());

  SAFE_AUX_CLASS_PRE_CONDITION((getState() == DoubleBuffer::BOTH_BUFFERS_OK),
			       getState());

  //===================================================================
  //  at this point the current state is 'BOTH_BUFFERS_OK'...
  //===================================================================

  // get the index to the current "write" buffer by requesting access
  // to the "write" buffer...
  const Uint32  WRITE_ID = accessWriteBuffer_();
  SAFE_AUX_CLASS_POST_CONDITION((getState() == DoubleBuffer::READ_BUFFER_OK),
  			        getState(), DoubleBuffer);

  //===================================================================
  //  at this point the current state is 'READ_BUFFER_OK'...
  //===================================================================

  // copy the new data into the current writable buffer...
  arrDataItems_[WRITE_ID] = newData;

  // accept the update of the write buffer...
  updateUsingWriteBuff_();
  SAFE_AUX_CLASS_POST_CONDITION((getState() == DoubleBuffer::BOTH_BUFFERS_OK),
  			        getState(), DoubleBuffer);

  //===================================================================
  //  at this point the current state is 'BOTH_BUFFERS_OK'...
  //===================================================================

  NovRamSemaphore::ReleaseWriteAccess(getSemaphoreId_());
}   // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
DblBuff_NonBatchSettingValues::SoftFault(const SoftFaultID  softFaultID,
			      const Uint32       lineNumber,
			      const char*        pFileName,
			      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, DOUBLE_BUFFER_DATA,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
