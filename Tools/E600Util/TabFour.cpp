// TabThree.cpp : implementation file
//

#include "stdafx.h"
#include "E600Util.h"
#include "TabFour.h"
#include "GlobalObjects.h"
#include "UserMessages.h"

// TabFour dialog

IMPLEMENT_DYNAMIC(CTabFour, CDialog)

static LPCTSTR CalMsgInProgress[NUM_CALS] = { L"Calibration not in progress...",
										L"Air Psol calibration in progress...",
										L"O2 Psol calibration in progress...",
										L"Exh. valve calibration in progress...",
										L"Exh. flow sensor calibration in progress...",
										L"Flow Sensor Offset calibration in progress...",
										L"Psol Liftoff calibration in progress...",
										L"All calibrations in progress...",
										L"Circuit check in progress..."
};

static LPCTSTR CalMsgPassed[NUM_CALS] = { L"Calibration not in progress...",
										L"Air Psol calibration passed",
										L"O2 Psol calibration passed",
										L"Exh. valve calibration passed",
										L"Exh. flow sensor calibration passed",
										L"Flow Sensor Offset calibration passed",
										L"Psol Liftoff calibration passed",
										L"All calibrations passed",
										L"Circuit check passed"
};

static LPCTSTR CalMsgFailed[NUM_CALS] = { L"Calibration not in progress...",
										L"Air Psol calibration failed",
										L"O2 Psol calibration failed",
										L"Exh. valve calibration failed",
										L"Exh. flow sensor calibration failed",
										L"Flow Sensor Offset calibration failed",
										L"Psol Liftoff calibration failed",
										L"All calibrations failed",
										L"Circuit check failed"
};

static LPCTSTR CalMsgCanceled[NUM_CALS] = { L"Calibration not in progress...",
										L"Air Psol calibration canceled",
										L"O2 Psol calibration canceled",
										L"Exh. valve calibration canceled",
										L"Exh. flow sensor calibration canceled",
										L"Flow Sensor Offset calibration canceled",
										L"Psol Liftoff calibration canceled",
										L"All calibrations canceled",
										L"Circuit check canceled"
};

static CButton *pCalButton[NUM_CALS];

CTabFour::CTabFour(CWnd* pParent /*=NULL*/)
	: CDialog(CTabFour::IDD, pParent)
{
	CalType = CAL_NONE;
	bCalInProgress = FALSE;
}

CTabFour::~CTabFour()
{
}

void CTabFour::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTabFour, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_AIR_PSOL_CAL, &CTabFour::OnBnClickedButtonAirPsolCal)
	ON_BN_CLICKED(IDC_BUTTON_O2_PSOL_CAL, &CTabFour::OnBnClickedButtonO2PsolCal)
	ON_BN_CLICKED(IDC_BUTTON_EXH_DRIVE_CAL, &CTabFour::OnBnClickedButtonExhDriveCal)
	ON_BN_CLICKED(IDC_BUTTON_EXH_FLOW_SENSOR_CAL, &CTabFour::OnBnClickedButtonExhFlowSensorCal)
	ON_BN_CLICKED(IDC_BUTTON_ALL_CALS, &CTabFour::OnBnClickedButtonAllCals)
	ON_BN_CLICKED(IDC_BUTTON_CIRCUIT_CHECK, &CTabFour::OnBnClickedButtonCircuitCheck)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL_CAL, &CTabFour::OnBnClickedButtonCancelCal)

	ON_MESSAGE(WM_CAL_STATUS, &CTabFour::ProcessCalStatus)
	ON_BN_CLICKED(IDC_BUTTON_FLOW_SENSOR_OFFSET_CAL, &CTabFour::OnBnClickedButtonFlowSensorOffsetCal)
	ON_BN_CLICKED(IDC_BUTTON_PSOL_LIFTOFF_CAL, &CTabFour::OnBnClickedButtonPsolLiftoffCal)
	ON_BN_CLICKED(IDC_BUTTON_EXH_SENSOR_AZ, &CTabFour::OnBnClickedButtonExhSensorAz)
	ON_BN_CLICKED(IDC_BUTTON_AIR_FLOW, &CTabFour::OnBnClickedButtonAirFlow)
	ON_BN_CLICKED(IDC_BUTTON_O2_FLOW, &CTabFour::OnBnClickedButtonO2Flow)
END_MESSAGE_MAP()


// TabFour message handlers
BOOL CTabFour::OnInitDialog()
{
	CDialog::OnInitDialog();

	return FALSE;
}

void CTabFour::GetButtonPointers()
{
	pCalButton[CAL_NONE] = NULL;
	pCalButton[CAL_AIR_PSOL] = (CButton *)GetDlgItem(IDC_BUTTON_AIR_PSOL_CAL);
	pCalButton[CAL_O2_PSOL] = (CButton *)GetDlgItem(IDC_BUTTON_O2_PSOL_CAL);
	pCalButton[CAL_EXH_VALVE] = (CButton *)GetDlgItem(IDC_BUTTON_EXH_DRIVE_CAL);
	pCalButton[CAL_EXH_FLOW_SENSOR] = (CButton *)GetDlgItem(IDC_BUTTON_EXH_FLOW_SENSOR_CAL);
	pCalButton[CAL_FLOW_SENSOR_OFFSET] = (CButton *)GetDlgItem(IDC_BUTTON_FLOW_SENSOR_OFFSET_CAL);
	pCalButton[CAL_PSOL_LIFTOFF] = (CButton *)GetDlgItem(IDC_BUTTON_PSOL_LIFTOFF_CAL);
	pCalButton[CAL_ALL] = (CButton *)GetDlgItem(IDC_BUTTON_ALL_CALS);
	pCalButton[CIRCUIT_CHECK] = (CButton *)GetDlgItem(IDC_BUTTON_CIRCUIT_CHECK);
	return;
}

void CTabFour::ProcessCalRequest(CalType_t RequestCalType)
{
	UINT16 MsgBuff[2];

	if(!bCalInProgress)
	{
		bCalInProgress = TRUE;
		CalType = RequestCalType;
		MsgBuff[0] = PC_CALIBRATION_MSG_ID;
		MsgBuff[1] = CalType;
		pTabOne->SendMessageW(WM_CAL_CMD, sizeof(UINT), (LPARAM)MsgBuff);
		pStatusBar->SetPaneText(0, CalMsgInProgress[CalType]);

		GetButtonPointers();
		for(int i=1; i<NUM_CALS; i++)
		{
			pCalButton[i]->EnableWindow(FALSE);
		}
	}
}

void CTabFour::ProcessTestRequest(TestType_t TestType)
{
	UINT16 MsgBuff[2];

	MsgBuff[0] = PC_TEST_MSG_ID;
	MsgBuff[1] = TestType;
	pTabOne->SendMessageW(WM_CAL_CMD, sizeof(UINT), (LPARAM)MsgBuff);
}

void CTabFour::ProcessCalCompletion(CalStatus_t Status)
{
	if(bCalInProgress)
	{
		if(Status == CAL_PASSED)
			pStatusBar->SetPaneText(0, CalMsgPassed[CalType]);
		else
			pStatusBar->SetPaneText(0, CalMsgFailed[CalType]);
		bCalInProgress = FALSE;
		CalType = CAL_NONE;

		GetButtonPointers();
		for(int i=1; i<NUM_CALS; i++)
		{
			pCalButton[i]->EnableWindow(TRUE);
		}
	}
}

LRESULT CTabFour::ProcessCalStatus(WPARAM x, LPARAM y)
{
	CalStatus_t Status = (CalStatus_t)x;
	if((Status == CAL_FAILED) || (Status == CAL_PASSED))
		ProcessCalCompletion(Status);
	return 0;
}

void CTabFour::OnBnClickedButtonAirPsolCal()
{
	ProcessCalRequest(CAL_AIR_PSOL);	
}

void CTabFour::OnBnClickedButtonO2PsolCal()
{
	ProcessCalRequest(CAL_O2_PSOL);
}

void CTabFour::OnBnClickedButtonExhDriveCal()
{
	ProcessCalRequest(CAL_EXH_VALVE);
}

void CTabFour::OnBnClickedButtonExhFlowSensorCal()
{
	ProcessCalRequest(CAL_EXH_FLOW_SENSOR);
}

void CTabFour::OnBnClickedButtonAllCals()
{
	ProcessCalRequest(CAL_ALL);
}

void CTabFour::OnBnClickedButtonCircuitCheck()
{
	ProcessCalRequest(CIRCUIT_CHECK);
}



void CTabFour::OnBnClickedButtonCancelCal()
{
	UINT16 MsgBuff[2];

	if(bCalInProgress)
	{
		bCalInProgress = FALSE;
		MsgBuff[0] = PC_CAL_CANCEL_MSG_ID;
		MsgBuff[1] = CalType;
		pTabOne->SendMessageW(WM_CAL_CMD, sizeof(UINT), (LPARAM)MsgBuff);
		pStatusBar->SetPaneText(0, CalMsgCanceled[CalType]);
		CalType = CAL_NONE;

		GetButtonPointers();
		for(int i=1; i<NUM_CALS; i++)
		{
			pCalButton[i]->EnableWindow(TRUE);
		}
	}
}

void CTabFour::OnBnClickedButtonFlowSensorOffsetCal()
{
	ProcessCalRequest(CAL_FLOW_SENSOR_OFFSET);
}

void CTabFour::OnBnClickedButtonPsolLiftoffCal()
{
	ProcessCalRequest(CAL_PSOL_LIFTOFF);
}


void CTabFour::OnBnClickedButtonExhSensorAz()
{
	ProcessTestRequest(TEST_EXH_SENSOR_AZ);
}

void CTabFour::OnBnClickedButtonAirFlow()
{
	ProcessTestRequest(TEST_AIR_FLOW);
}

void CTabFour::OnBnClickedButtonO2Flow()
{
	ProcessTestRequest(TEST_O2_FLOW);
}