// e600_Benchmark.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#ifdef STANDARDSHELL_UI_MODEL
#include "resource.h"
#endif

// Ce600_BenchmarkApp:
// See e600_Benchmark.cpp for the implementation of this class
//

class Ce600_BenchmarkApp : public CWinApp
{
public:
	Ce600_BenchmarkApp();
	
// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern Ce600_BenchmarkApp theApp;
