#include "stdafx.h"
//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#include"SwatBuffer.h"
#include<string.h>

namespace swat{

    float SwatBuffer::m_data[SWATBUFFER_BUFFER_SIZE];


    void SwatBuffer::copyToBuffer(char *pData, unsigned int iLength)
    {
        if(iLength < SWATBUFFER_BUFFER_SIZE )
        {
            memcpy(m_data,pData,iLength);
        }

    }

    void SwatBuffer::copyToBuffer(char *pData, unsigned int iLength, unsigned int iOffset)
    {
        if((iLength+iOffset) < SWATBUFFER_BUFFER_SIZE )
        {
            memcpy(&m_data[iOffset],pData,iLength);
        }
    }


}
