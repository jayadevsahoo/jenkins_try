
#ifndef NonBatchSettingValues_HH
#define NonBatchSettingValues_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: NonBatchSettingValues - Manager of the Non-Batch Setting Values.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/NonBatchSettingValues.hhv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "SettingId.hh"

//@ Usage-Classes
#include "SettingValue.hh"
#include "SettingValueMgr.hh"
//@ End-Usage


class NonBatchSettingValues : public SettingValueMgr
{
  public:
    NonBatchSettingValues(const NonBatchSettingValues& nonBatchValues);
    NonBatchSettingValues(const SettingValueMgr::ChangeCallbackPtr pInitCallback);
    NonBatchSettingValues(void);
    ~NonBatchSettingValues(void);

    const SettingValue&  getSettingValue(
				  const SettingId::SettingIdType settingId
				        ) const;
    void                 setSettingValue(
				  const SettingId::SettingIdType settingId,
				  const SettingValue&            newValue
				        );

    BoundedValue  getBoundedValue(
			      const SettingId::SettingIdType boundedId
				 ) const;
    void          setBoundedValue(
			      const SettingId::SettingIdType boundedId,
			      const BoundedValue&            newValue
				 );

    DiscreteValue  getDiscreteValue(
				const SettingId::SettingIdType discreteId
				   ) const;
    void           setDiscreteValue(
				const SettingId::SettingIdType discreteId,
				const DiscreteValue            newValue
				   );

    SequentialValue  getSequentialValue(
				const SettingId::SettingIdType sequentialId
				       ) const;
    void             setSequentialValue(
				const SettingId::SettingIdType sequentialId,
				const SequentialValue          newValue
				       );

    void  operator=(const NonBatchSettingValues& nonBatchValues);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    //@ Data-Member:  arrNonBatchValues_
    // Fixed array containing the values for each of the non-batch
    // settings.
    SettingValue  arrNonBatchValues_[SettingId::NUM_NON_BATCH_IDS];
};


#endif // NonBatchSettingValues_HH 
