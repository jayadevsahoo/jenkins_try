#ifndef BackgroundGuiTests_HH
#define BackgroundGuiTests_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================ H E A D E R   D E S C R I P T I O N ====
//  Class: BackgroundGuiTests - Individual background tests for the GUI CPU.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundGuiTests.hhv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: gdc    Date:  18-Nov-1999    DR Number: 5585
//  Project:  ATC
//  Description:
//      Added TouchScreenFailCounter_ used for counting sequential
//		touch screen failure reports. Two sequential failures result
//		in a device alert.
//
//  Revision: 003  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//
//  Revision: 002 By: iv     Date: 11-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//		   Added error code argument to all tests.
//
//  Revision: 001    By: by  Date: 12-Dec-1995  DR Number: None
//
//    Project: Sigma (840)
//    Description:
//       Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "SafetyNet.hh"
#include "Background.hh"
#include "Keys.hh"
#include "TimeStamp.hh"

class BackgroundGuiTests
{
    public:

        static BkEventName KeyboardTest( Uint16 &errorCode );
        static BkEventName NovRamChecksumTest( Uint16 &errorCode );
        static BkEventName TodTest( Uint16 &errorCode );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL, 
			       const char*       pPredicate = NULL );

    private:

        BackgroundGuiTests( const BackgroundGuiTests& ); // Not implemented
        void operator= ( const BackgroundGuiTests& );    // Not implemented

        BackgroundGuiTests( void );   // Not implemented
        ~BackgroundGuiTests( void );  // Not implemented

        //@ Data-Member: TouchScreenCounter_ 
        //  time counter for a reply from GUI-IO-Devices
        //  after a request for touch error status is made
        static Uint16 TouchScreenCounter_;

        //@ Data-Member: TouchScreenBlockedCounter_ 
        //  indicates the number of times the a touch blocked
        //  failure was reported from GUI-IO-Devices
        static Uint32 TouchScreenBlockedCounter_;

        //@ Data-Member: TouchScreenFailed_ 
        //  indicates that the touch failure has been detected 
        //  either by a severe failure or 3 beam blocked errors
        static Boolean TouchScreenFailed_;

        //@ Data-Member: KeyboardTestFailed_
        //  indicates that this Background test has failed
        static Boolean KeyboardTestFailed_;

        //@ Data-Member: SaasAliveTestFailed_ 
        //  indicates that this Background test has failed
        static Boolean SaasAliveTestFailed_;

        //@ Data-Member: NovRamChecksumTestFailed_ 
        //  indicates that this Background test has failed
        static Boolean NovRamChecksumTestFailed_;

        //@ Data-Member: TodTestFailed_ 
        //  indicates that this Background test has failed
        static Boolean TodTestFailed_;

        //@ Data-Member: TodTestInitialized_
        //  indicate whether the TOD test is initialized
        static Boolean TodTestInitialized_;
                         
        //@ Data-Member: PreviousTime_
        //  stores the previous time for the TOD Test
        static TimeStamp PreviousTime_;
                                                  
        //@ Data-Member: CurrentTime_
        //  stores the current time for the TOD Test
        static TimeStamp CurrentTime_;

        //@ Data-Member: GuiKeyTimer_
        //  GUI Key status counter
        static Uint16 GuiKeyTimer_[ NUMBER_OF_GUI_KEYS ];

        //@ Data-Member: TouchScreenFailCounter_ 
        //  indicates the number of times a touch screen failure 
		//  (other than screen blocked )was reported from GUI-IO-Devices
        static Uint32 TouchScreenFailCounter_;

#if defined (SIGMA_UNIT_TEST)
static Uint16 KeyBoardImage_;
static Int    SaasStatus_;
#endif // defined (SIGMA_UNIT_TEST)

};


#endif    // #ifndef BackgroundGuiTests_HH
