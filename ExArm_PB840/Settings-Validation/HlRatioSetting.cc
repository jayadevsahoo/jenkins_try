#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  HlRatioSetting - Th-to-Tl Ratio Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the ratio between the
//  PEEP high and PEEP low times.  This class inherits from
//  'BatchBoundedSetting' and provides the specific information needed for
//  representation of H:L ratio.  This information includes the interval
//  and range of this setting's values, this setting's response to a change
//  of its primary setting (see 'acceptPrimaryChange()'), this batch
//  setting's and range of this setting's values, this batch setting's
//  new-patient value (see 'getNewPatientValue()'), and the dependent
//  settings of this setting.
//
//  The value stored by this setting is based on one of the numbers of the
//  ratio, rather than the ratio itself.  That is, the number is stored
//  according to:
//>Von
//     xxx:1;  where xxx = (Th/Tl) when Th > Tl,
//     1.00:1; when Th=Tl,
//     1:xxx;  where xxx = (Tl/Th) when Th < Tl,
//>Voff
//  and xxx:1 > 1.00:1 > 1:xxx
//
//  This setting also dynamically monitors the constant parm setting,
//  and updates its applicability accordingly.
//
//  For "xxx:1" ratios, the internal value of this setting is a positive
//  number, while for "1:xxx" the internal value is negative.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HlRatioSetting.ccv   25.0.4.0   19 Nov 2013 14:27:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: sah     Date:  25-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  removed code that would re-activate soft limit with primary
//         setting changes (this caused the I:E ratio assertion listed in
//         VTPC-DCS #40)
//
//  Revision: 005   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 004   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//      *  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  added new 1.00:1 soft limit
//      *  soft-limit framework was re-designed such that settings with
//         soft limits need only define new 'findSoftMinMaxValues_()'
//         method, therefore all other "soft" methods were removed
//
//  Revision: 003   By: sah   Date:  10-Jun-1999    DR Number: 5377
//  Project:  ATC
//  Description:
//      Fixed problem where soft-bound wasn't being reactivated following
//      a transition.
//
//  Revision: 002   By: sah   Date:  12-Mar-1999    DR Number: 5310
//  Project:  ATC
//  Description:
//     Added missing '$' to requirement mapping.
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//=====================================================================

#include "HlRatioSetting.hh"
#include "SettingConstants.hh"
#include "ModeValue.hh"
#include "ConstantParmValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "RespRateSetting.hh"
#include "ConstantParmSetting.hh"
#include "SoftwareOptions.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[TC02016] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
  {
    SettingId::PEEP_LOW_TIME,
    SettingId::PEEP_HIGH_TIME,

    SettingId::NULL_SETTING_ID
  };


// $[BL02013] -- soft upper bound for H:L...
static const Real32  INVERSE_SOFT_BOUND_VALUE_ = 1.00f;
static const Real32  MAX_SOFT_BOUND_VALUE_     = 4.00f;


//  $[TC02012] -- The setting's range ...
//  $[TC02015] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    -10000.0f,		// absolute minimum...
    0.0f,		// unused...
    ONES,		// unused...
    NULL
  };
static const BoundedInterval  NODE6_ =
  {
    -100.0f,		// change-point...
    1.0f,		// resolution...
    ONES,		// precision...
    &::LAST_NODE_
  };
static const BoundedInterval  NODE5_ =
  {
    -10.0f,		// change-point...
    0.1f,		// resolution...
    TENTHS,		// precision...
    &::NODE6_
  };
static const BoundedInterval  NODE4_ =
  {
    -1.01f,		// change-point...
    0.01f,		// resolution...
    HUNDREDTHS,		// precision...
    &::NODE5_
  };
static const BoundedInterval  NODE3_ =
  {
    1.00f,		// change-point...
    (1.00f - (-1.01f)),// resolution...
    HUNDREDTHS,		// precision...
    &::NODE4_
  };
static const BoundedInterval  NODE2_ =
  {
    10.0f,		// change-point...
    0.01f,		// resolution...
    HUNDREDTHS,		// precision...
    &::NODE3_
  };
static const BoundedInterval  NODE1_ =
  {
    100.0f,		// change-point...
    0.1f,		// resolution...
    TENTHS,		// precision...
    &::NODE2_
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    149.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::NODE1_
  };



//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: HlRatioSetting()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default H:L ratio setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HlRatioSetting::HlRatioSetting(void)
       : BatchBoundedSetting(SettingId::HL_RATIO,
			     ::ARR_DEPENDENT_SETTING_IDS_,
			     &::INTERVAL_LIST_,
			     HL_RATIO_MAX_ID,	   // maxConstraintId...
			     NULL_SETTING_BOUND_ID,// minConstraintId...
			     FALSE)		   // no epsilon factor...
{
  CALL_TRACE("HlRatioSetting()");

  // register this setting's soft bounds...
  registerSoftBound_(::HL_RATIO_SOFT_MAX_ID, Setting::UPPER);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~HlRatioSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HlRatioSetting::~HlRatioSetting(void)
{
  CALL_TRACE("~HlRatioSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability(qualifierId)  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
HlRatioSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  Applicability::Id  applicability;

  if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL))
  {  // $[TI1] -- BILEVEL is an active option...
    const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
    const Setting*  pConstantParm =
			  SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM);

    DiscreteValue  modeValue;
    DiscreteValue  constantParmValue;

    switch (qualifierId)
    {
    case Notification::ACCEPTED :		// $[TI1.1]
      modeValue         = pMode->getAcceptedValue();
      constantParmValue = pConstantParm->getAcceptedValue();
      break;
    case Notification::ADJUSTED :		// $[TI1.2]
      modeValue         = pMode->getAdjustedValue();
      constantParmValue = pConstantParm->getAdjustedValue();
      break;
    default :
      AUX_CLASS_ASSERTION_FAILURE(qualifierId);
      break;
    }

    if (modeValue == ModeValue::BILEVEL_MODE_VALUE)
    {  // $[TI1.3]
      applicability =
	    (constantParmValue == ConstantParmValue::HL_RATIO_CONSTANT)
		       ? Applicability::CHANGEABLE	// $[TI1.3.1]
		       : Applicability::VIEWABLE;	// $[TI1.3.2]
    }
    else
    {  // $[TI1.4]
      applicability = Applicability::INAPPLICABLE;
    }
  }
  else
  {  // $[TI2] -- BILEVEL is not an active option...
    applicability = Applicability::INAPPLICABLE;
  }

  return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TC02014] -- new patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
HlRatioSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;

  // the H:L ratio new patient value is based on the new-patient values of
  // PEEP high time and respiratory rate...
  newPatient = calcBasedOnSetting_(SettingId::PEEP_HIGH_TIME,
  				   BoundedRange::WARP_NEAREST,
				   BASED_ON_NEW_PATIENT_VALUES);

  return(newPatient);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned, and this setting's value is "clipped"
//  to that bound.  Otherwise, a value of 'NULL' is returned to indicate no
//  no dependent bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02002] -- new dependent settings based on proposed primary settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
HlRatioSetting::acceptPrimaryChange(const SettingId::SettingIdType primaryId)
{
  CALL_TRACE("acceptPrimaryChange(primaryId)");

  BoundStatus&  rBoundStatus = getBoundStatus_();

  // initialize to holding this setting's id...
  rBoundStatus.setViolationId(getId());

  BoundedValue  newHlRatio;

  newHlRatio = calcBasedOnSetting_(primaryId, BoundedRange::WARP_NEAREST,
				   BASED_ON_ADJUSTED_VALUES);

  // update dynamic constraints...
  updateConstraints_();

  // test 'newHlRatio' against this setting's bounded range; the bound
  // violation state, if any, is returned in 'rBoundStatus', while
  // 'newHlRatio' is "clipped" if a bound is violated...
  getBoundedRange_().testValue(newHlRatio, rBoundStatus);

  // store the value...
  setAdjustedValue(newHlRatio);

  const BoundStatus*  pBoundStatus;

  // if there is a bound violation return a pointer to this setting's
  // bound status, otherwise return 'NULL'...
  pBoundStatus = (rBoundStatus.isInViolation()) ? &rBoundStatus	// $[TI1]
						: NULL;		// $[TI2]

  return(pBoundStatus);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02025]\f\   -- transitioning from A/C to BiLevel
//  $[TC02000]\c\ -- transitioning from SIMV to BiLevel
//  $[02026]\e\   -- transitioning from SPONT to BiLevel
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HlRatioSetting::acceptTransition(const SettingId::SettingIdType,
				 const DiscreteValue,
				 const DiscreteValue)
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

  // temporarily disable all soft bounds so they don't limit the calculated
  // value (this also updates the constraints)...
  overrideAllSoftBoundStates_();

  BoundedValue  newHlRatio;

  newHlRatio = calcBasedOnSetting_(SettingId::PEEP_HIGH_TIME,
				   BoundedRange::WARP_NEAREST,
				   BASED_ON_ADJUSTED_VALUES);

  // store the transition value into the adjusted context...
  setAdjustedValue(newHlRatio);

  // now that the calculated value has been stored, re-enable all applicable
  // soft bounds...
  updateAllSoftBoundStates_(Setting::NON_CHANGING);

  setForcedChangeFlag();
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to subject's value changes, by, possibly, updating this setting's
//  applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::CONSTANT_PARM)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HlRatioSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
			    const SettingSubject*               pSubject)
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");
  SAFE_AUX_CLASS_PRE_CONDITION((pSubject->getId() == SettingId::CONSTANT_PARM),
			       pSubject->getId());

  if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL))
  {  // $[TI1] -- BiLevel option is active...
    // update this instance's applicability...
    updateApplicability(qualifierId);
  }  // $[TI2] -- BiLevel option is NOT active...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
HlRatioSetting::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Constant Parm setting,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HlRatioSetting::settingObserverInit(void)
{
  CALL_TRACE("settingObserverInit()");

  // monitor changes in constant parm's value...
  attachToSubject_(SettingId::CONSTANT_PARM, Notification::VALUE_CHANGED);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
HlRatioSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) !=
					    Applicability::INAPPLICABLE)
    {
      const Real32  HL_RATIO_VALUE = BoundedValue(getAcceptedValue()).value;

      const DiscreteValue  CONSTANT_PARM_VALUE =
		    ConstantParmSetting::GetValue(BASED_ON_ACCEPTED_VALUES);

      SettingId::SettingIdType  basedOnSettingId;

      switch (CONSTANT_PARM_VALUE)
      {
      case ConstantParmValue::PEEP_HIGH_TIME_CONSTANT :
      case ConstantParmValue::HL_RATIO_CONSTANT :
	basedOnSettingId = SettingId::PEEP_HIGH_TIME;
        break;
      case ConstantParmValue::PEEP_LOW_TIME_CONSTANT :
	basedOnSettingId = SettingId::PEEP_LOW_TIME;
        break;
      default : 
	AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
        break;
      };

      const BoundedValue  CALC_HL_RATIO  =
			       calcBasedOnSetting_(basedOnSettingId,
						   BoundedRange::WARP_NEAREST,
						   BASED_ON_ACCEPTED_VALUES);

      isValid = ::IsEquivalent(HL_RATIO_VALUE,
			       CALC_HL_RATIO.value,
			       CALC_HL_RATIO.precision);

      if (!isValid)
      {
	// a 4.0% error is allowed between the PEEP high time-based
	// calculation and the current H:L ratio values...
	const Real32  CALCULATION_ERROR =
			      ABS_VALUE((CALC_HL_RATIO.value * 0.04));

	isValid = (HL_RATIO_VALUE >=
			    (CALC_HL_RATIO.value - CALCULATION_ERROR)  &&
		   HL_RATIO_VALUE <=
			    (CALC_HL_RATIO.value + CALCULATION_ERROR));
      }
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  IsAdjustedValid()  [static]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?  Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
HlRatioSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  // forward to base class...
  Boolean isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) !=
					    Applicability::INAPPLICABLE)
    {
      const Real32  HL_RATIO_VALUE = BoundedValue(getAdjustedValue()).value;

      const DiscreteValue  CONSTANT_PARM_VALUE =
		    ConstantParmSetting::GetValue(BASED_ON_ADJUSTED_VALUES);

      SettingId::SettingIdType  basedOnSettingId;

      switch (CONSTANT_PARM_VALUE)
      {
      case ConstantParmValue::HL_RATIO_CONSTANT :
      case ConstantParmValue::PEEP_HIGH_TIME_CONSTANT :
	basedOnSettingId = SettingId::PEEP_HIGH_TIME;
        break;
      case ConstantParmValue::PEEP_LOW_TIME_CONSTANT :
	basedOnSettingId = SettingId::PEEP_LOW_TIME;
        break;
      default : 
	AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
        break;
      };

      const BoundedValue  CALC_HL_RATIO  =
			       calcBasedOnSetting_(basedOnSettingId,
						   BoundedRange::WARP_NEAREST,
						   BASED_ON_ADJUSTED_VALUES);

      isValid = ::IsEquivalent(HL_RATIO_VALUE,
			       CALC_HL_RATIO.value,
			       CALC_HL_RATIO.precision);

      if (!isValid)
      {
	// since the calculation of an H:L ratio is inherently inaccurate,
	// a 5.0% error is allowed between the PEEP high time-based
	// calculation and the current H:L ratio values...
	const Real32  CALCULATION_ERROR =
			      ABS_VALUE((CALC_HL_RATIO.value * 0.04));

	isValid = (HL_RATIO_VALUE >=
			    (CALC_HL_RATIO.value - CALCULATION_ERROR)  &&
		   HL_RATIO_VALUE <=
			    (CALC_HL_RATIO.value + CALCULATION_ERROR));
      }

      if (!isValid)
      {
	const Setting*  pPeepHighTime =
		     SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH_TIME);
	const Setting*  pRespRate =
		     SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);

	cout << "INVALID H:L Ratio:\n";
	cout << "CALC = " << CALC_HL_RATIO << endl;
	cout << *this << *pPeepHighTime << *pRespRate << endl;
      }
    }
  }

  return(isValid);
}

#endif // defined (SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HlRatioSetting::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  HL_RATIO_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//  [TC02012] -- max value of TH:TL (H:L)
//  [BL02013] -- soft inverse and upper bounds for Th:Tl
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HlRatioSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  //===================================================================
  // determine H:L ratio's maximum constraint...
  //===================================================================

  BoundedRange::ConstraintInfo  maxConstraintInfo;

  if (isSoftBoundActive_(HL_RATIO_SOFT_MAX_ID))
  {   // $[TI1] -- 'HL_RATIO_SOFT_MAX_ID' soft bound is currently active...
    // the soft-bound is ALWAYS more restrictive than the hard bound...
    Real32  dummySoftMinValue;

    // get the soft max value...
    findSoftMinMaxValues_(dummySoftMinValue, maxConstraintInfo.value);

    maxConstraintInfo.id     = HL_RATIO_SOFT_MAX_ID;
    maxConstraintInfo.isSoft = TRUE;
  }
  else
  {   // $[TI2] -- 'HL_RATIO_SOFT_MAX_ID' soft bound is NOT active...
    maxConstraintInfo.id     = HL_RATIO_MAX_ID;
    maxConstraintInfo.value  = ::INTERVAL_LIST_.value;
    maxConstraintInfo.isSoft = FALSE;
  }

  getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  H:L ratio has only a soft upper limit, no soft lower limit.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HlRatioSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
				      Real32& rSoftMaxValue) const
{
  CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

  rSoftMinValue = getAbsoluteMinValue_();

  const BoundedValue  HL_RATIO = getAdjustedValue();

  if (HL_RATIO.value <= ::INVERSE_SOFT_BOUND_VALUE_)
  {  // $[TI1] -- use the inverse soft bound value...
    rSoftMaxValue = ::INVERSE_SOFT_BOUND_VALUE_;
  }
  else
  {  // $[TI2] -- use the max soft bound value...
    rSoftMaxValue = ::MAX_SOFT_BOUND_VALUE_;
  }
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnSettingId == SettingId::PEEP_HIGH_TIME  &&
//   (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//    basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//			||
//  ((basedOnSettingId == SettingId::RESP_RATE  ||
//    basedOnSettingId == SettingId::PEEP_LOW_TIME  ||
//    basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
HlRatioSetting::calcBasedOnSetting_(
			    const SettingId::SettingIdType basedOnSettingId,
			    const BoundedRange::WarpDir    warpDirection,
			    const BasedOnCategory          basedOnCategory
				    ) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

  SettingId::SettingIdType  finalBasedOnSettingId;

  if (basedOnSettingId == SettingId::RESP_RATE)
  {   // $[TI1] -- convert 'RESP_RATE' to "based-on" id...
    const DiscreteValue  CONSTANT_PARM_VALUE =
			       ConstantParmSetting::GetValue(basedOnCategory);

    switch (CONSTANT_PARM_VALUE)
    {
    case ConstantParmValue::PEEP_HIGH_TIME_CONSTANT :    // $[TI1.1]
      // respiratory rate is being changed while holding PEEP high time
      // constant, therefore calculate the new H:L ratio from the
      // current PEEP high time...
      finalBasedOnSettingId = SettingId::PEEP_HIGH_TIME;
      break;
    case ConstantParmValue::PEEP_LOW_TIME_CONSTANT :    // $[TI1.2]
      // respiratory rate is being changed while holding PEEP low time
      // constant, therefore calculate the new H:L ratio from the
      // current PEEP low time...
      finalBasedOnSettingId = SettingId::PEEP_LOW_TIME;
      break;
    case ConstantParmValue::HL_RATIO_CONSTANT :
    case ConstantParmValue::TOTAL_CONSTANT_PARMS :
    default :
      // invalid value for 'CONSTANT_PARM_VALUE'...
      AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
      break;
    };
  }
  else
  {   // $[TI2]
    // use passed in parameter...
    finalBasedOnSettingId = basedOnSettingId;
  }

  // get the breath period from the respiratory rate...
  const Real32  BREATH_PERIOD_VALUE =
			    RespRateSetting::GetBreathPeriod(basedOnCategory);

  const BoundedSetting*  pPeepHighTime =
		  SettingsMgr::GetBoundedSettingPtr(SettingId::PEEP_HIGH_TIME);
  const BoundedSetting*  pPeepLowTime =
		  SettingsMgr::GetBoundedSettingPtr(SettingId::PEEP_LOW_TIME);

  BoundedValue  hlRatio;

  Real32  peepHighTimeValue;
  Real32  peepLowTimeValue;

  switch (finalBasedOnSettingId)
  {
  //-------------------------------------------------------------------
  // $[02014](1) -- new H:L ratio while changing PEEP high time...
  // $[02018](1) -- new H:L ratio while keeping PEEP high time constant...
  //-------------------------------------------------------------------
  case SettingId::PEEP_HIGH_TIME :
    {   // $[TI3] -- base H:L ratio's calculation on PEEP high time...
      switch (basedOnCategory)
      {
      case BASED_ON_NEW_PATIENT_VALUES :	// $[TI3.1]
	// get the new-patient PEEP high time value...
	peepHighTimeValue =
		    BoundedValue(pPeepHighTime->getNewPatientValue()).value;
	break;
      case BASED_ON_ADJUSTED_VALUES :		// $[TI3.2]
	// get the "adjusted" PEEP high time value...
	peepHighTimeValue =
		    BoundedValue(pPeepHighTime->getAdjustedValue()).value;
	break;
      case BASED_ON_ACCEPTED_VALUES :
	// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
	// get the "accepted" PEEP high time value...
	peepHighTimeValue =
		    BoundedValue(pPeepHighTime->getAcceptedValue()).value;
	break;
#endif // defined(SIGMA_DEVELOPMENT)
      default :
	// unexpected 'basedOnCategory' value..
	AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
	break;
      };

      // calculate an PEEP low time, based on the "adjusted" PEEP high
      // time...
      peepLowTimeValue = (BREATH_PERIOD_VALUE - peepHighTimeValue);
    }
    break;

  //-------------------------------------------------------------------
  // $[02012](1) -- new H:L ratio while changing PEEP low time...
  // $[02016](1) -- new H:L ratio while keeping PEEP low time constant...
  //-------------------------------------------------------------------
  case SettingId::PEEP_LOW_TIME :
    {   // $[TI4] -- base H:L ratio's calculation on PEEP low time...
      switch (basedOnCategory)
      {
      case BASED_ON_ADJUSTED_VALUES :
	// get the "adjusted" PEEP low time value...
	peepLowTimeValue = BoundedValue(pPeepLowTime->getAdjustedValue()).value;
	break;
      case BASED_ON_ACCEPTED_VALUES :
	// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
	// get the "accepted" PEEP low time value...
	peepLowTimeValue = BoundedValue(pPeepLowTime->getAcceptedValue()).value;
	break;
#endif // defined(SIGMA_DEVELOPMENT)
      case BASED_ON_NEW_PATIENT_VALUES :
      default :
	// unexpected 'basedOnCategory' value..
	AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
	break;
      };

      // calculate an PEEP high time, based on the PEEP low time...
      peepHighTimeValue = BREATH_PERIOD_VALUE - peepLowTimeValue;
    }
    break;

  default :
    // unexpected dependent setting id...
    AUX_CLASS_ASSERTION_FAILURE(finalBasedOnSettingId);
    break;
  };

  //====================================================================
  // a LARGE knob delta can cause Th or Tl to "jump" passed a bound
  // constraint, causing a Th or Tl value less than its minimum; clip at
  // minimum values...
  //====================================================================
  
  peepHighTimeValue = MAX_VALUE(peepHighTimeValue,
			        pPeepHighTime->getBoundedRange().getMinValue());
  peepLowTimeValue  = MAX_VALUE(peepLowTimeValue,
			        pPeepLowTime->getBoundedRange().getMinValue());

  if (peepHighTimeValue == peepLowTimeValue)
  {   // $[TI5]
    // when the PEEP high time is equivalent to the PEEP low time
    // (to be shown as "1.00:1"), we store a value of one...
    hlRatio.value = 1.00f;
  }
  else if (peepHighTimeValue > peepLowTimeValue)
  {   // $[TI6]
    // when the PEEP high time is greater than the PEEP low time
    // (to be shown as "xx:1"), we store the positive Th-to-Tl ratio...
    hlRatio.value = (peepHighTimeValue / peepLowTimeValue);
  }
  else
  {   // $[TI7]
    // when the PEEP low time is greater than the PEEP high time
    // (to be shown as "1:xx"), we store the negative Tl-to-Th ratio...
    hlRatio.value = -(peepLowTimeValue / peepHighTimeValue);
  }

  getBoundedRange().warpValue(hlRatio, warpDirection, FALSE);

  if (hlRatio.value == 1.00f  &&  peepHighTimeValue > peepLowTimeValue)
  {  // $[TI8] -- SPECIAL CASE...
    // the rounding of the Th/Tl calculation resulted in '1.00' -- which
    // may be an active soft-bound -- but since Th > Tl, we want to ensure
    // a bound violation, therefore force up...
    hlRatio.value += 0.005f;  // modify value...

    // warp modified value "up" to the next resolution...
    getBoundedRange().warpValue(hlRatio, BoundedRange::WARP_UP, FALSE);
  }  // $[TI9] -- no special processing needed...

  return(hlRatio);
}
