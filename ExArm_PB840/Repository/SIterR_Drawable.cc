#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SIterR_Drawable - Iterator for 'SListR_Drawable'
//---------------------------------------------------------------------
//@ Interface-Description
//  This provides an iterator for a singly-linked list of data
//  items where each item is a reference to a type 'Drawable'.  Iterators
//  are used to "step" through the elements within a linked list.  The
//  iterator is strictly a constant reference to its list -- no
//  changes to the list can be made with an iterator.  Because of
//  the referencing to a list, it is dangerous to change/destroy the
//  list while this iterator still exists, therefore iterators should
//  be used in very limited scopes.  Many iterators may be attached
//  to the same list; iterators may attach to many different lists
//  during its lifetime.
//
//  This iterator can attach to abstract, singly-linked lists containing
//  references to data items of type 'Drawable'.  Any fixed, singly-linked
//  list of references to data items of type 'Drawable' can be used with
//  this iterator, no matter what 'SIZE' parameter is specified.
//---------------------------------------------------------------------
//@ Rationale
//  Iteration through a constant list is often needed; this provides
//  a means of viewing the items in a constant list without changing
//  the list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A constant pointer to the list, and to a particular node in
//  the list are kept within this iterator.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The list that this iterator references should NOT be changed
//  while the iterator refers to that list.  If a change occurs to an
//  iterator's list, the iterator should be re-attached to the list before
//  using the iterator again.  (See 'SListR_Drawable' for restrictions
//  on its use.)
//---------------------------------------------------------------------
//@ Invariants
//  (pIterList_ != NULL)
//  ((pIterCurrent_ == NULL  &&  isEmpty())  ||
//   (isLegalNode_(pIterCurrent_)  &&  !isEmpty()))
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/SIteratorR.C_v   25.0.4.0   19 Nov 2013 14:03:50   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SIterR_Drawable.hh"
#include "Drawable.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  search(item, startFrom, foundWhen)
//
//@ Interface-Description
//  Search the list for a node that matches 'item'.  'startFrom'
//  indicates whether to start the search from the first node
//  ('FROM_FIRST'), or the current node ('FROM_CURRENT').  If a
//  match is found, then 'SUCCESS' is returned, and the current node
//  is set to the matching node.  If no match is found, then 'FAILURE'
//  is returned and the list is left unchanged.  If 'foundWhen' is
//  'EQUIV_OBJECTS', then a match occurs when an equivalent item is
//  found in the list.  If 'foundWhen' is 'EQUIV_REFERENCES', then a
//  match occurs when a reference to 'item' is found in the list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Near the end of this method a pointer to a constant node,
//  'pFoundNode', has its const-ness cast away.  This is due to the
//  fact that 'findNode_()' is a constant method (to allow its use
//  in constant public methods) that returns a pointer.  To ensure
//  consistent const-ness of 'findNode_()', the pointer that is
//  returned is a pointer to a CONSTANT node.  The const-ness of
//  the returned node is not necessary in the design of 'findNode_()',
//  but rather a consistency of design of that method.  Therefore,
//  when this method casts the const-ness of 'pFoundNode' away, it
//  is an acceptable case of working properly with a proper design.
//---------------------------------------------------------------------
//@ PreConditions
//  (startFrom == FROM_FIRST  ||  startFrom == FROM_CURRENT)
//  (foundWhen == EQUIV_OBJECTS  ||  foundWhen == EQUIV_REFERENCES)
//---------------------------------------------------------------------
//@ PostConditions
//  (status == SUCCESS) ==> (currentItem() == item)
//  (status == FAILURE) ==> (*this == OLD{*this})
//@ End-Method
//=====================================================================

SigmaStatus
SIterR_Drawable::search(Drawable&                 item,
			 const SearchFrom      startFrom,
			 const EquivResolution foundWhen)
{
  CALL_TRACE("search(item, startFrom, foundWhen)");

  SigmaStatus  status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this iterator's list is not empty, therefore use this iterator's
    // list to find a node that contains an item that is equivalent to
    // 'item'...
    const SNodeC_MemPtr*  pFoundNode = NULL;

    // set 'pFoundNode' to the node that this search is to begin from...
    switch (startFrom)
    {
    case FROM_FIRST:		// $[TI1.1]
      pFoundNode = getFirst_();
      break;
    case FROM_CURRENT:		// $[TI1.2]
      pFoundNode = getCurrent_();
      break;
    case FROM_LAST:
    default:
      AUX_CLASS_ASSERTION_FAILURE(((startFrom << 16) | sizeof(Drawable)));
      break;
    }

    SAFE_CLASS_PRE_CONDITION((foundWhen == EQUIV_OBJECTS  ||
			      foundWhen == EQUIV_REFERENCES));

    // return a pointer to the link node that is equivalent to 'item',
    // starting from the link node pointed to by 'pFoundNode'...
    pFoundNode = pIterList_->findNode_(item, pFoundNode, foundWhen);

    if (pFoundNode != NULL)
    {   // $[TI1.3]
      // the matching node is found, therefore set this iterator's current
      // node to the found node...
      setCurrent_((SNodeC_MemPtr*)pFoundNode);  // "const cast away:"
      status = SUCCESS;
    }   // $[TI1.4] -- no match was found...
  }   // $[TI2] -- this iterator's list is currently empty..

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//	                [static]
//
//@ Interface-Description
//	Report the software fault that occured within the source code
//	of this class.  The fault, indicated by 'softFaultID', occured
//	at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

void
SIterR_Drawable::SoftFault(const SoftFaultID softFaultID,
			    const Uint32      lineNumber,
			    const char*       pFileName,
			    const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, S_ITERATOR_R,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Friend Functions...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  operator<<(ostr, sIter)  [Friend Function]
//
// Interface-Description
//  Dump out an ASCII representation of the current state of this
//  list to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const SIterR_Drawable& sIter)
{
  CALL_TRACE("operator<<(ostr, sIter)");

  ostr << "SIterR_Drawable {\n";

  if (!sIter.isEmpty())
  {
    const SNodeC_MemPtr*  pNode = sIter.getFirst_();

    do
    {
      ostr << ((pNode == sIter.getCurrent_()) ? "->" : "  ")
	   << *((const Drawable*)(pNode->getItem())) << '\n';
    } while ((pNode = pNode->getNextPtr()) != NULL);
  }

  ostr << "};\n";
  ostr.flush();

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
