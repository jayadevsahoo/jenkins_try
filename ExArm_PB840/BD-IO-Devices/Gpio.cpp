#include "stdafx.h"
//=======================================================================
// This is a proprietary work to which Puritan-Bennett Corporation
// claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or,
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or, otherwise with the prior written permission of
// Puritan-Bennett Corporation.
//
//		Copyright (c) 1995, Puritan-Bennett Corporation
//
//=======================================================================


// ============================= C L A S S  D E S C R I P T I O N =======
//@ Class:  Gpio_t - Interface to GPIO hardware.
//-----------------------------------------------------------------------
//@ Interface-Description
//		This class provides methods to control and read GPIO pins.
//-----------------------------------------------------------------------
//@ Rationale
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//-----------------------------------------------------------------------
//@ Restrictions
//-----------------------------------------------------------------------
//@ Invariants
//-----------------------------------------------------------------------
//@ End-Preamble

#include "Gpio.h"
#include "BDIORefs.hh"
#include "AioDioDriver.h"


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  FlowSerialEeprom()
//
//@ Interface-Description
//  	Constructor.  
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Initialize class variables and set the passed GPIO pin to the
//		specified state.
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
Gpio_t::Gpio_t(const ApplicationDigitalOutputs_t bit, Uint8 initState)
{
    bit_ =  bit;
    currentState_ = initState;
	pendingState_ = initState;
    GpioDirection = GPIO_OUTPUT;

    if (initState)
	{
		Set();
	}
	else
	{
		Clear();
	}
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  Gpio_t()
//
//@ Interface-Description
//  	Constructor.  
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Initialize class variables.
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
Gpio_t::Gpio_t(const ApplicationDigitalInputs_t bit)
{
    bit_ =  bit;
	currentState_ = 0;
	pendingState_ = 0;
    GpioDirection = GPIO_INPUT;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  ~Gpio_t()
//
//@ Interface-Description
//  	Desstructor.  
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
Gpio_t::~Gpio_t()
{
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  Set()
//
//@ Interface-Description
//  	Set GPIO hardware to high state.  
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
void Gpio_t::Set()
{
	AioDioDriver.Write((ApplicationDigitalOutputs_t)bit_, DOUT_HIGH);
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  Clear()
//
//@ Interface-Description
//  	Set GPIO hardware to low state.  
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
void Gpio_t::Clear()
{
	AioDioDriver.Write((ApplicationDigitalOutputs_t)bit_, DOUT_LOW);
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  Read()
//
//@ Interface-Description
//  	Get the current state of GPIO hardware.  
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
Uint8 Gpio_t::Read()
{
	currentState_ = AioDioDriver.Read((ApplicationDigitalInputs_t)bit_);
	return currentState_;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  SetPendingState()
//
//@ Interface-Description
//  	Input:	newState	- the new state to set for hardware 
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Save the input state to be set by newCycle().
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
void Gpio_t::SetPendingState(Uint8 newState)
{
	pendingState_ = newState;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  ReadPendingState()
//
//@ Interface-Description
//  	Output:	the state last read from hardware
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Save the input state to be used by newCycle().
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
Uint8 Gpio_t::ReadPendingState()
{
	return pendingState_;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  newCycle()
//
//@ Interface-Description
//  	 
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Save the input state to be used by newCycle().
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
void Gpio_t::newCycle()
{
    // If this is an input, read the signal, save it and return it
    if (GpioDirection == GPIO_INPUT)
    {
        currentState_ = AioDioDriver.Read((ApplicationDigitalInputs_t)bit_);
		pendingState_ = currentState_;				// Not needed, but this keeps them consistent
    }
    else
    {
        // Has a state change request occurred?
		if (currentState_ != pendingState_)
        {
            currentState_ = pendingState_;
            if (currentState_)
            {
                Set();
            }
            else
            {
                Clear();
            }
        }
    }
}
