#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmLogSubScreen - Displays a scrollable history of alarm events.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container class.
// This class contains a ScrollableLog object which provides scrolling
// features, leaving this class to the task of specifying what text is
// displayed in each column of the log.
//
// This class depends directly on the Alarms subsystem for providing the 
// alarm log entries.
//
// When alarm events occur the method updateAlarmLogDisplay() should be called
// so that we can update the log accordingly.  The activate()/deactivate()
// methods are called automatically on display/removal of this subscreen by our
// subscreen area owner.  The getLogEntryColumn() is called by ScrollableLog
// whenever it needs to know the contents of a log "cell".  The
// novRamUpdateEventHappened() is called whenever the NovRamManger issures
// an REAL_TIME_CLOCK_UPDATE event to update the date/time change to the log.
//---------------------------------------------------------------------
//@ Rationale
// This class is dedicated to managing the display of the
// AlarmLogSubScreen on the UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// The overall responsibility of this subscreen is to provide a
// scrollable display of the alarm log history maintained by the Alarms
// subsystem.  This alarm log display provides the following features:
// >Von
//		1.	The ability to simultaneously view up to six consecutive
//			alarm events.
//		2.	The ability to scroll through the log if more than six events
//			are contained in the log.
//		3.	The ability to automatically update the display when a new
//			entry is added to the alarm log (updateAlarmLogDisplay()).
//		4.	The ability to automatically update the display when the
//			viewed entries no longer exist in the alarm log
//			(updateAlarmLogDisplay()).
// >Voff
// The alarm log is presented in table format with 6 rows and 5 columns using a
// ScrollableLog object.  Whenever the ScrollableLog needs to know the
// contents of a log cell it calls getLogEntryColumn().  This method
// retrieves the appropriate text for the specified entry, possibly using
// the AlarmRender class to do formatting/word-wrapping, and returns it to
// ScrollableLog for display.
//
// The updateAlarmLogDisplay() should be called whenever an update to
// the alarm log stored in NovRam is detected.  It looks after how the
// ScrollableLog should be updated.
//---------------------------------------------------------------------
//@ Fault-Handling
// Standard assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmLogSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 014   By: rhj   Date:  27-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 013   By: rhj   Date:  27-May-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added Date Localization. Text formatting corrected (gdc).
//
//  Revision: 013   By: rhj   Date:  25-Sep-2006    SCR Number: 6169
//  Project:  RESPM
//  Description:
//      Implement the ability to download alarm logs.
//
//  Revision: 012   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 011  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 010  By:  clw	   Date:  13-May-98    DR Number: 
//    Project:  Sigma (R8027)
//    Description:
//      Added japanese-only code to implement different date format in the alarm log.
//
//  Revision: 009  By:  hhd	   Date:  14-Oct-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      Code cleanup.
//
//  Revision: 008  By:  hhd	   Date:  15-Sep-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      Fixed Analysis column width which used to block the scroll bar.
//
//  Revision: 007  By:  yyy    Date:  10-Sep-97    DR Number: 2169
//    Project:  Sigma (R8027)
//    Description:
//      Removed obsolete SRS references.
//
//  Revision: 006  By:  yyy    Date:  08-Sep-1997    DCS Number: 2269
//  Project:  Sigma (R8027)
//  Description:
//      Used REAL_TIME_CLOCK_UPDATE the newly added callbacks registered for real-time
//		clock changes to update the new date/time.
//
//  Revision: 005  By:  sah    Date:  20-AUG-1997    DCS Number: 2379
//  Project:  Sigma (R8027)
//  Description:
//      Now calling 'GuiApp::SetAlarmUpdateNeededFlag()' method, instead of
//      directly issueing a re-draw of the Alarm History Log.
//
//  Revision: 004  By:  hhd    Date:  18-AUG-1997    DCS Number: 2265 
//  Project:  Sigma (R8027)
//  Description:
//		  Changed LEFT_TEXT_MARGIN value.
//       
//  Revision: 003  By:  yyy    Date:  04-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Registered for DATE/TIME settings to capture the setting change
//		events and update the date/time accordingly.
//
//  Revision:  002  By: yyy   Date:  22-May-1997      DR Number: DCS 1917
//    Project:    Sigma (R8027)
//    Description:
//      Update the alarm log counts and last alarm timer each time the
//		updateAlarmLogDisplay() method is called.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================
#include "AlarmLogSubScreen.hh"

#include "Sigma.hh"
#include "GuiApp.hh"
#include "TextUtil.hh"

//@ Usage-Classes
#include "AlarmEvent.hh"
#include "AlarmRender.hh"
#include "MiscStrs.hh"
#include "NovRamManager.hh"
#include "ScrollableLog.hh"
#include "UpperScreen.hh"
#include "UpperSubScreenArea.hh"
#include "ServiceUpperScreen.hh"
#include "NovRamEventRegistrar.hh"
#include "AlarmLogEntry.hh"
#include "TaskMonitor.hh"
#include "LanguageValue.hh"
#include "StringConverter.hh"
//@ End-Usage

//@ Code...

// Initialize static constants
static const Int32 LOG_X_ = -1;
static const Int32 LOG_Y_ = 5;
static const Int32 NUM_ROWS_ = 6;
static const Int32 NUM_COLUMNS_ = 5;
static const Int32 ROW_HEIGHT_ = 42;
static const Int32 TIME_COL_WIDTH_ = 56;
static const Int32 EVENT_COL_WIDTH_ = 70;
static const Int32 ALARM_COL_WIDTH_ = 123;
static const Int32 ANALYSIS_COL_WIDTH_ = 295; 

static const Int32 CENTERED_TEXT_MARGIN_ = 0; // Changed from 4
static const Int32 LEFT_TEXT_MARGIN_ = 1;  // Changed from 5

static const Int32 NUM_COLUMNS5_ = 5;
const Int32 MAX_TEMP_BUFF = 180;
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmLogSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and sets up the subscreen information.
//---------------------------------------------------------------------
//@ PreCondition
// The given 'pSubScreenArea' pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================

AlarmLogSubScreen::AlarmLogSubScreen(SubScreenArea* pSubScreenArea) :
	SubScreen(pSubScreenArea),
	pLog_(NULL),
	numLogEntries_(-1)
{
	CALL_TRACE("AlarmLogSubScreen::AlarmLogSubScreen(pSubScreenArea)");
	SAFE_CLASS_ASSERTION(pSubScreenArea != NULL);

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	// Register all logs with NovRam for receiving event updates
	// for changes to the date/time setting changes
	NovRamEventRegistrar::RegisterTarget(NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE,
										this);

	lastTimeStamp_.invalidate();
					// $[TI1]
    if (GuiApp::GetGuiState() == STATE_SERVICE)
    {
        //
        // Register for laptop event callbacks.
        //
        LaptopEventRegistrar::RegisterTarget(LaptopEventId::UPLOAD_ALARM_LOG, this);
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmLogSubScreen  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmLogSubScreen::~AlarmLogSubScreen(void)
{
	CALL_TRACE("AlarmLogSubScreen::AlarmLogSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()
//
//@ Interface-Description
// Prepares this subscreen for display.  Displays the alarm log starting
// with the latest entry.
//---------------------------------------------------------------------
//@ Implementation-Description
// First create and setup the ScrollableLog.  The set the view to the "top" of
// the alarm log.  That is, set the display to show the six most recent entries
// in the alarm log.  This is done by setting the 'alarmViewIndex_' index to
// the top of the log and then calling the normal screen refresh routine.
//
// $[01181] Each displayed alarm history log entry shall contain ...
// $[01185] On each activation the display should be at the top.
// $[01183] If there are more than six events in the alarm history log ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmLogSubScreen::activate(void)
{
	CALL_TRACE("AlarmLogSubScreen::activate(void)");

	// Create and setup the scrollable log and display it
	pLog_ = ScrollableLog::New(this, NUM_ROWS_, NUM_COLUMNS_, ROW_HEIGHT_);
	pLog_->setX(LOG_X_);
	pLog_->setY(LOG_Y_);
	pLog_->setColumnInfo(TIME_COLUMN, MiscStrs::ALARM_LOG_TIME, TIME_COL_WIDTH_, CENTER,
							0, TextFont::NORMAL, TextFont::TEN, TextFont::EIGHT);
	pLog_->setColumnInfo(EVENT_COLUMN, MiscStrs::ALARM_LOG_EVENT, EVENT_COL_WIDTH_, CENTER,
							0, TextFont::NORMAL, TextFont::TEN);
	pLog_->setColumnInfo(URGENCY_COLUMN, MiscStrs::ALARM_LOG_URGENCY, EVENT_COL_WIDTH_, 
							CENTER, 0, TextFont::NORMAL, TextFont::TEN);
	pLog_->setColumnInfo(ALARM_COLUMN, MiscStrs::ALARM_LOG_ALARM, ALARM_COL_WIDTH_,
							CENTER, 0, TextFont::NORMAL, TextFont::TEN);
	pLog_->setColumnInfo(ANALYSIS_COLUMN, MiscStrs::ALARM_LOG_ANALYSIS, ANALYSIS_COL_WIDTH_,
							CENTER, 0, TextFont::NORMAL, TextFont::TEN);
	addDrawable(pLog_);

	// We start by displaying the top of the alarm history log.
	pLog_->setEntryInfo(0, 0);		// Reset the scrollable log

	// Store the latest alarm log entries, retrieving them from NovRam.
	NovRamManager::GetAlarmLogEntries(latestLogEntries_ );

	// Set up the log display
	updateAlarmLogDisplay();

	// Activate the log also
	pLog_->activate();									// $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()
//
//@ Interface-Description
// Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
// We must remove the scrollable log from the subscreen because it is
// a shared object, used by other screens.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmLogSubScreen::deactivate(void)
{
	CALL_TRACE("AlarmLogSubScreen::deactivate(void)");

	pLog_->deactivate();

	// Remove the scrollable log from the display
	removeDrawable(pLog_);								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateAlarmLogDisplay
//
//@ Interface-Description
// Redraw the alarm log display as necessary.  This processing only
// takes place if the subscreen is displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Mostly this function is called when a new entry has been added to the
// alarm log.  If we are currently displaying the top of the log then
// we should display the new entry.  If however we are displaying another
// part of the log we try to keep the same log entries displayed.  We
// use the rFirstDisplayedAlarmEvent_ to help us find where to set our
// position in the newest alarm log so as to keep the display unchanged. 
//
// $[01180] Alarm Log subscreen displays up to 6 alarm history entries.
// $[01184] If the view is at the top and more entries arrive then ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmLogSubScreen::updateAlarmLogDisplay(void)
{
	CALL_TRACE("AlarmLogSubScreen::updateAlarmLogDisplay(void)");

	Int32 firstDisplayedEntry = 0;			// Index of first displayed entry
	Int32 numLogEntries;					// Number of entries in the log
	AlarmLogEntry firstDisplayedAlarmInfo;	// The structure of alarm info
											// displayed at the top of the log

	// If this subscreen is the currently active subscreen then we should
	// update our display.
	SubScreenArea* pSubScreenArea;

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{													// $[TI1]
		pSubScreenArea = (SubScreenArea *) ServiceUpperScreen::RServiceUpperScreen.getUpperSubScreenArea();
	}
	else
	{													// $[TI2]
		pSubScreenArea = (SubScreenArea *) UpperScreen::RUpperScreen.getUpperSubScreenArea();
	}

	if (pSubScreenArea->isCurrentSubScreen(this))
	{													// $[TI3]
		// Store the alarm that's currently the first displayed entry (the row
		// that's displayed in the first row on the screen, as long as the
		// log is not empty).
		firstDisplayedEntry = pLog_->getFirstDisplayedEntry();
		if (!latestLogEntries_[firstDisplayedEntry].isClear())
		{												// $[TI4]
			firstDisplayedAlarmInfo = latestLogEntries_[firstDisplayedEntry];
		}												// $[TI5]

		// Copy the latest alarm log from NovRam
		NovRamManager::GetAlarmLogEntries(latestLogEntries_ );

		// Determine the number of entries in the new alarm log.
		for (numLogEntries = 0; numLogEntries < MAX_ALARM_ENTRIES;
														numLogEntries++)
		{												// $[TI15.1]
			if (latestLogEntries_[numLogEntries].isClear())
			{											// $[TI6]
				break;
			}											// $[TI7]
		}												// $[TI15.2]

		CLASS_ASSERTION(numLogEntries >= 0 &&
										numLogEntries <= MAX_ALARM_ENTRIES);
		CLASS_ASSERTION(!numLogEntries ||
				(firstDisplayedEntry >= 0 &&
										firstDisplayedEntry < numLogEntries));

		numLogEntries_ = numLogEntries;
		lastTimeStamp_ = 
					AlarmEvent(latestLogEntries_[0].getAlarmAction()).getTimeStamp();

		// If there are entries in the log and we're not displaying the top
		// of the log then find out where rFirstDisplayedAlarmEvent_ is in
		// the new log and try to keep it as the first displayed entry in
		// the log (typically rFirstDisplayedAlarmEvent_ will have moved one
		// entry down in the log).
		if (numLogEntries && firstDisplayedEntry)
		{												// $[TI8]
			for (; firstDisplayedEntry < numLogEntries; firstDisplayedEntry++)
			{											// $[TI16.1]
				if (AlarmEvent(firstDisplayedAlarmInfo.getAlarmAction()) ==
						AlarmEvent(latestLogEntries_[firstDisplayedEntry].
															getAlarmAction()))
				{										// $[TI9]
					break;
				}										// $[TI10]
			}											// $[TI16.2]

			// If the saved entry has moved down in the log so far that we
			// are unable to make it the first displayed entry in the log
			// then we must pick a new first displayed entry, one that
			// will cause the very last page in the log to be displayed.
			if (firstDisplayedEntry > numLogEntries - NUM_ROWS_)
			{											// $[TI11]
				firstDisplayedEntry = numLogEntries - NUM_ROWS_;
			}											

			SAFE_CLASS_ASSERTION(firstDisplayedEntry >= 0 &&
										firstDisplayedEntry < numLogEntries);
		}												// $[TI13]

		// Inform the scrollable log of the change in log entries.
		pLog_->setEntryInfo(firstDisplayedEntry, numLogEntries);
	}													// $[TI14]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of an alarm log entry.  Passed the following
// parameters:
// >Von
//	entryNumber			The index of the log entry
//	columnNumber		The column number of the log entry
//	rIsCheapText		Output parameter.  A reference to a flag which
//						determines whether the text content of the log
//						entry is specified as cheap text or not.
//	rString1			String id of the first string. Can be
//						set to NULL_STRING_ID to indicate an empty entry
//						column.
//	rString2			String id of the second string.
//	rString3			String id of the third string.  Not used if
//						rIsCheapText is FALSE.
//	rString1Message		A help message that will be displayed in the
//						Message Area when rString1 is touched.  This only
//						works when rIsCheapText is TRUE.
// >Voff
// We retrieve information for the appropriate column, make sure it's
// formatted correctly and return it to ScrollableLog.
//---------------------------------------------------------------------
//@ Implementation-Description
// Depending on which column is requested, we do appropriate alarm information
// retrieval from latestLogEntries_[], format the text possibly using
// the AlarmRender class to help here and then return the values.
//
// $[01182] The time stamp portion of the alarm history log entry shall ...
//---------------------------------------------------------------------
//@ PreCondition
//	The 'entry' must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmLogSubScreen::getLogEntryColumn(Uint16 entry, Uint16 column,
				Boolean &rIsCheapText, StringId &rString1, StringId &rString2,
				StringId &rString3, StringId &rString1Message)
{
	CALL_TRACE("AlarmLogSubScreen::getLogEntryColumn(Uint16 entry, Uint16 column, "
				"Boolean &rIsCheapText, StringId &rString1, "
				"StringId &rString2, StringId &rString3, StringId &rString1Message)");

	CLASS_PRE_CONDITION(entry < MAX_ALARM_ENTRIES);
	 
	static wchar_t tmpBuffer1[400];			// Buffers for storing text of
	static wchar_t tmpBuffer2[400];			//   column entries
	static wchar_t tmpBuffer3[400];
	 
	// Default the two string output parameters to point to the static
	// character buffers.
	rString1 = &tmpBuffer1[0];
	rString2 = &tmpBuffer2[0];
	rString3 = &tmpBuffer3[0];
	rString1Message = NULL_STRING_ID;
	tmpBuffer1[0] = L'\0';
	tmpBuffer2[0] = L'\0';
	tmpBuffer3[0] = L'\0';

	// Strobing TaskMonitor to keep alive
	TaskMonitor::Report();

	const AlarmAction &rAlarmAction = latestLogEntries_[entry].getAlarmAction();
	AlarmEvent alarmEvent(rAlarmAction);
											// Holds the information for all
											// columns of the requested entry.

	if (!latestLogEntries_[entry].isClear())
	{																// $[TI1]
		if (!latestLogEntries_[entry].isCorrected())
		{															// $[TI2]
			//@ Constant: MAX_MSG_IDS
			// Maximum number of message id's provided by the Alarms software.
			const Int32	MAX_MSG_IDS = 20;

			Int32		messageIds[MAX_MSG_IDS];	// null-terminated array of alarm
											// message ids, needed for
											// AlarmRender routines

			// Check which column was requested.
			switch (column)
			{
			case TIME_COLUMN:										// $[TI3]
				// Column 0, the Time column.

				// First line of text should be the time, second line should be the
				// date.
				{
					const TimeStamp & timeStamp = alarmEvent.getTimeStamp();

					TextUtil::FormatTime(tmpBuffer1, L"{p=10,y=10:%H:%M:%S}", timeStamp);
					wchar_t format[30];
					swprintf(format, L"{p=8,y=10:%s}", MiscStrs::LOG_DATE_FORMAT);
					TextUtil::FormatTime(tmpBuffer2, format, timeStamp);
					rString3 = NULL_STRING_ID;
					// rIsCheapText set false so LogCell centers the text
					rIsCheapText = FALSE;
				}
				break;

			case EVENT_COLUMN:										// $[TI4]
			{
				// Column 1, the Event column.
				// Display event message.  
				
				rIsCheapText = TRUE;
				rString3 = NULL_STRING_ID;

				// Check if it's a clock update event.  Display event text if so.
				if (rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_PENDING)
				{													// $[TI5]
					swprintf(tmpBuffer1, L"%s", MiscStrs::ALOG_DATE_TIME_BEFORE_CHANGE);
				}
				else if (rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_DONE)
				{													// $[TI6]	
					swprintf(tmpBuffer1, L"%s", MiscStrs::ALOG_DATE_TIME_CHANGED);
				}
				else
				{													// $[TI7]
					// First, try rendering on a single line.  If it
					// doesn't fit, then go to a triple-line display with word wrapping.
					messageIds[0] = alarmEvent.getUpdateTypeMessage();
					messageIds[1] = MESSAGE_NAME_NULL;

					TextFont::Size fontSize;
					Int16 maxLines;
					if (GuiApp::GetLanguage() == LanguageValue::CHINESE)
					{
						fontSize = TextFont::TEN;
						maxLines = 2;
					}
					else
					{
						fontSize = TextFont::EIGHT;
						maxLines = 3;
					}
					if (!AlarmRender::WithoutWordWrap(tmpBuffer1, tmpBuffer2, tmpBuffer3,
							 messageIds, fontSize, EVENT_COL_WIDTH_,
							 ROW_HEIGHT_, CENTER, CENTERED_TEXT_MARGIN_))
					{												// $[TI8]
						CLASS_ASSERTION(maxLines >=
							AlarmRender::WithWordWrap(tmpBuffer1, tmpBuffer2, tmpBuffer3,
							messageIds, fontSize, EVENT_COL_WIDTH_,
							ROW_HEIGHT_, CENTER, CENTERED_TEXT_MARGIN_)
						);
					}												// $[TI9]
				}
				break;
			}
			case URGENCY_COLUMN:									// $[TI10]
			{
				// Column 2, the Urgency column.
				if (rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_PENDING
				   || rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_DONE
				   || latestLogEntries_[entry].isTimeDateEntry())
				{													// $[TI11]
					rString1 = rString2 = rString3 = NULL_STRING_ID;
				}
				else
				{													// $[TI12]
					switch (alarmEvent.getUrgencyMessage())
					{
					case LOW_URGENCY_MSG:							// $[TI13]
						wcscpy(tmpBuffer1, MiscStrs::ALARM_LOG_URGENCY_LOW);
						break;
		
					case MEDIUM_URGENCY_MSG:						// $[TI14]
						wcscpy(tmpBuffer1, MiscStrs::ALARM_LOG_URGENCY_MEDIUM);
						break;
	
					case HIGH_URGENCY_MSG:							// $[TI15]
						wcscpy(tmpBuffer1, MiscStrs::ALARM_LOG_URGENCY_HIGH);
						break;
	
					case NORMAL_URGENCY_MSG:						// $[TI16]
						wcscpy(tmpBuffer1, MiscStrs::ALARM_LOG_URGENCY_NORMAL);
						break;
	
					default:
						CLASS_ASSERTION(FALSE);
						break;
					}	// switch
					
					rIsCheapText = FALSE;
					rString2 = NULL_STRING_ID;
					rString3 = NULL_STRING_ID;
				}
				break;
			}
			case ALARM_COLUMN:									// $[TI17]
			{
				// Column 3, the Alarm column.
				if (rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_PENDING
					|| rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_DONE)
				{												// $[TI18]
					rIsCheapText = FALSE;
					rString1 = rString2 = rString3 = NULL_STRING_ID;
				}
				else
				{												// $[TI19]
					rIsCheapText = TRUE;

				    // Display the base message.  First, try rendering on a single line.
					// If it doesn't fit, then go to a dual-line display with word wrap.
					messageIds[0] = alarmEvent.getBaseMessage();
					messageIds[1] = MESSAGE_NAME_NULL;
					if (AlarmRender::WithoutWordWrap(tmpBuffer1, tmpBuffer2, tmpBuffer3,
										 messageIds, TextFont::TEN, ALARM_COL_WIDTH_,
										 ROW_HEIGHT_, CENTER, CENTERED_TEXT_MARGIN_))
					{												// $[TI20]
						// Successfully rendered base message

						// If this is an alarm symbol, upgrade the base message text to
					    // be touchable and attach a translation message to be displayed
					// in the lower screen Message Area.
						rString1Message = AlarmRender::GetSymbolHelpMessage(messageIds);
					}
					else
					{												// $[TI21]
						// Single line rendering failed, so attempt rendering on two lines
						// with word wrapping.
						CLASS_ASSERTION(0 !=
							AlarmRender::WithWordWrap(tmpBuffer1, tmpBuffer2, tmpBuffer3,
									 messageIds, TextFont::TEN, ALARM_COL_WIDTH_,
									 ROW_HEIGHT_, CENTER, CENTERED_TEXT_MARGIN_));
					}
				}
				break;
			}
			case ANALYSIS_COLUMN:									// $[TI22]
			{
				// Column 4, the Analysis column.
				if (rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_PENDING
					|| rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_DONE)
				{													// $[TI23]
					rIsCheapText = FALSE;
					rString1 = rString2 = rString3 = NULL_STRING_ID;
				}
				else
				{		
					rIsCheapText = TRUE;

					// Display the analysis message.  The first step is to copy the
					// message id's into the null-terminated messageIds[] array.
					const MessageNameArray& rAnalysisMsgs =
											alarmEvent.getAnalysisMessage();
					Int32 numMsgIds = 0;

					for (Int32 i = 0; i < alarmEvent.getAnalysisIndex(); i++)
					{											// $[TI24]
						CLASS_ASSERTION(numMsgIds < MAX_MSG_IDS);
						CLASS_ASSERTION(rAnalysisMsgs[i] != MESSAGE_NAME_NULL);
						messageIds[numMsgIds++] = rAnalysisMsgs[i];
					}											// $[TI25]

					CLASS_ASSERTION(numMsgIds < MAX_MSG_IDS);
					messageIds[numMsgIds] = MESSAGE_NAME_NULL;

					// Check if there's no analysis message
					if (0 < numMsgIds)
					{											// $[TI26]
						// Always word wrap	analysis messages
						CLASS_ASSERTION(0 !=
								AlarmRender::WithWordWrap(tmpBuffer1, tmpBuffer2,
								tmpBuffer3, messageIds, TextFont::TEN,
								ANALYSIS_COL_WIDTH_ - LEFT_TEXT_MARGIN_,
								ROW_HEIGHT_, LEFT, LEFT_TEXT_MARGIN_));
					}											// $[TI27]
				}
				break;
			}

			default:
				CLASS_ASSERTION(FALSE);			// Cannot be reached.
				break;
			}
		}
		else
		{														// $[TI28]
			switch (column)
			{
			case TIME_COLUMN:
				{												// $[TI29]
				// Column 0, the Time column.
				// First line of text should be the time, second line should 
				// be the date.
				const TimeStamp & timeStamp = alarmEvent.getTimeStamp();

				rIsCheapText = FALSE;
				TextUtil::FormatTime(tmpBuffer1, L"%t", timeStamp);
				TextUtil::FormatTime(tmpBuffer2, L"%P", timeStamp);
				rString3 = NULL_STRING_ID;
				break;
				}
			case EVENT_COLUMN:									// $[TI30]
				{
				// Column 1, the Event column.
				wcscpy(tmpBuffer1, MiscStrs::ALOG_UNKNOWN_ALARM_EVENT);
				rIsCheapText = FALSE;
				rString2 = rString3 = NULL_STRING_ID;	
				break;
				}
			case URGENCY_COLUMN:
				{												// $[TI31]
				// Column 2, the Urgency data column
				rString1 = rString2 = rString3 = NULL_STRING_ID;
				break;
				}
			case ALARM_COLUMN:
				{												// $[TI32]
				// Column 3, the Alarm column.
				rIsCheapText = FALSE;
				wcscpy(tmpBuffer1, MiscStrs::ALOG_ALARM_DATA_LOST);
				rString2 = rString3 = NULL_STRING_ID;	
				break;
				}
			case ANALYSIS_COLUMN:								// $[TI33]
				{
				// Column 4, the Analysis column.
				rIsCheapText = FALSE;
				wcscpy(tmpBuffer1, MiscStrs::ALOG_ALARM_DATA_CORRUPT);
				rString2 = rString3 = NULL_STRING_ID;	
				break;
				}
			default:// Urgency data
				CLASS_ASSERTION(FALSE);			// Cannot be reached.
				break;
			}	/* Switch */
		}														// $[TI34]
	}															// $[TI35]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: novRamUpdateEventHappened
//
//@ Interface-Description
// Called by NovRam manager, this function updates the display with
// the latest, real-time data in the log.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call updateAlarmLogDisplay to process it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
AlarmLogSubScreen::novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId updateTypeId)
{
	CALL_TRACE("AlarmLogSubScreen::novRamUpdateEventHappened(updateTypeId)");

	if (isVisible())
	{ 								// $[TI1]
		if (updateTypeId == NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE)
		{ 							// $[TI1.1]
			// If this subscreen is the currently active subscreen then we should
			// update our display.
			GuiApp::SetAlarmUpdateNeededFlag();
		} 							// $[TI1.2]
	}								// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: laptopRequestHappened
//
//@ Interface-Description
// This is a virtual method inherited from being a LaptopEventTarget.  It is
// called when the GuiApp task queue detected an request/response from the
// Serial I/O interface.   Each registered target shall process this
// request/response and an appropriated action shall be conducted.
//
// This method also fetches and store data in message then sends.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Make sure that the request is for an Alarm log.
//  The message buffer is formatted according to the request protocol.
//  The data in the Diagnostic Log then fills the buffer.
// 
//  $[08054] The entries from each diagnostic log shall be returned to
//  the PC upon request through the serial interface.
//---------------------------------------------------------------------
//@ PreCondition
//  Verify that the Serial Communication is connected to the laptop and 
//  the upload alarm log event was the request from the laptop.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AlarmLogSubScreen::laptopRequestHappened(LaptopEventId::LTEventId eventId,
                                            Uint8 *pMsgData)
{
    CALL_TRACE("AlarmLogSubScreen::laptopRequestHappened("
               "LaptopEventId::LTEventId eventId, Uint8 *pMsgData)");

    CLASS_PRE_CONDITION(GuiApp::IsSerialCommunicationUp());
    CLASS_ASSERTION( (eventId == LaptopEventId::UPLOAD_ALARM_LOG) );

     

    // Set serial system info flag
#ifdef FORNOW
    printf("In AlarmLogSubScreen::laptopRequestHappened(), line %d\n", __LINE__);   
#endif

    // Strobing TaskMonitor to keep alive
    TaskMonitor::Report();
	 
    wchar_t tmpBuf[MAX_TEMP_BUFF];    // Buffer used to transmit the data to the serial port
    wchar_t alarmStr[MAX_TEMP_BUFF];   // Temp storage for strings.
	 

    // Send the message for START_UPLOAD_COMMAND to the SerialOutput queue. 
    LaptopMessageHandler::ClearBuffer();
    LaptopMessageHandler::SetMessageNo();
    LaptopMessageHandler::SetLogCommand(LaptopEventId::START_UPLOAD_COMMAND);

    // Ask GuiApp to post the message to the Serial I/O queue.
    LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
                                                    LaptopMessageHandler::GetBuffer(),
                                                    LaptopMessageHandler::GetBufferIndex());

    // Set the temporary Buffers to terminating zeros.
    for(Int ii = 0; ii < MAX_TEMP_BUFF; ii++)
    {
         tmpBuf[ii]   = L'\0';
         alarmStr[ii] = L'\0';
    }

    // Clear the Alarm Log Entry buffer.
    for (Int index = 0; index < MAX_ALARM_ENTRIES; index++)
    {
        latestLogEntries_[index].clear();
    } 

    // Get the latest alarm log entries.
    NovRamManager::GetAlarmLogEntries(latestLogEntries_);
    
    // Find log size
    int logSize = 0;
    for ( logSize = 0; logSize < latestLogEntries_.getNumElems(); logSize++)
    {                                                       // $[TI5]
        if (latestLogEntries_[logSize].isClear())                 // $[TI6]   
        {
            break;
        }                                                   // $[TI7]
    }                                                       // $[TI8]


#ifdef FORNOW
    cout << "logSize is " << logSize << endl;
#endif
    //@ Constant: MAX_MSG_IDS
    // Maximum number of message id's provided by the Alarms software.
    const Int32 MAX_MSG_IDS = 20;

    Int32  messageIds[MAX_MSG_IDS]; // null-terminated array of alarm
                                    // message ids


    if (logSize > 0)
    { // $[TI10]
        // While not end log, fetch for data
        for (Uint16 currentRow=0; currentRow < logSize; currentRow++)
        { // $[TI11]

            // Strobing TaskMonitor to keep alive
            TaskMonitor::Report();

            // Begin storing a Null character
            tmpBuf[0] = L'\0';
            for (Uint16 currentColumn = 0; currentColumn < NUM_COLUMNS5_; currentColumn++)
            { // $[TI12]
                // Get data to be displayed
                alarmStr[0] = L'\0';
                const AlarmAction &rAlarmAction = latestLogEntries_[currentRow].getAlarmAction();
                AlarmEvent alarmEvent(rAlarmAction);  // Holds the information for all
                                                      // columns of the requested entry.
                if (!latestLogEntries_[currentRow].isClear())
                {  // $[TI1]
                    if (!latestLogEntries_[currentRow].isCorrected())
                    {
                        // $[TI2]

                        // Check which column was requested.
                        switch (currentColumn)
                        {
                            case TIME_COLUMN:  // $[TI3]
                                // Column 0, the Time column.
                                {
                                    wchar_t temp[MAX_TEMP_BUFF];
                                    const TimeStamp & timeStamp = alarmEvent.getTimeStamp();
                                    TextUtil::FormatTime(temp, L"%H:%M:%S %d %n %y", timeStamp);
                                    wcscpy(alarmStr, temp);

                                }
                                break;

                            case EVENT_COLUMN:  // $[TI4]
                                {
                                    // Column 1, the Event column.
                                    // Display event message.  

                                    // Check if it's a clock update event.  Display event text if so.
                                    if (rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_PENDING)
                                    {   // $[TI5]
                                        convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF, MiscStrs::ALOG_DATE_TIME_BEFORE_CHANGE );

                                    }
                                    else if (rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_DONE)
                                    {   // $[TI6]
                                        convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF, MiscStrs::ALOG_DATE_TIME_CHANGED );

                                    }
                                    else
                                    {   // $[TI7]

                                        // Retrieve the event message
                                        messageIds[0] = alarmEvent.getUpdateTypeMessage();
                                        if(messageIds[0] != MESSAGE_NAME_NULL)
                                        {     
                                           
                                            // Get the Alarm string from Alarm-Analysis and
                                            // translate that message to ASCII text.
                                            convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF, AlarmStrs::GetMessage(MessageName(messageIds[0])));
                                            
                                        }
                                        else
                                        {
                                            wcscpy(alarmStr, L" ");
                                        }
                                        
                                    }
                                    break;
                                }
                            case URGENCY_COLUMN:   // $[TI10]
                                {
                                    // Column 2, the Urgency column.
                                    if (rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_PENDING
                                        || rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_DONE
                                        || latestLogEntries_[currentRow].isTimeDateEntry())
                                    {   // $[TI11]

                                        wcscpy(alarmStr, L" ");
                                    }
                                    else
                                    {   // $[TI12]
                                        // Retrieve the Urgency Message and translate it to ASCII text.
                                        switch (alarmEvent.getUrgencyMessage())
                                        {
                                            case LOW_URGENCY_MSG:                           // $[TI13]
                                                convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF ,  MiscStrs::ALARM_LOG_URGENCY_LOW);                     // $[TI16]
                                                break;

                                            case MEDIUM_URGENCY_MSG:                        // $[TI14]
                                                convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF ,  MiscStrs::ALARM_LOG_URGENCY_MEDIUM);                     // $[TI16]
                                                break;

                                            case HIGH_URGENCY_MSG:  
                                                convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF ,  MiscStrs::ALARM_LOG_URGENCY_HIGH);                     // $[TI16]
                                                break;

                                            case NORMAL_URGENCY_MSG:   
                                                convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF ,  MiscStrs::ALARM_LOG_URGENCY_NORMAL);                     // $[TI16]
                                                break;

                                            default:
                                                // Not a valid Urgency Alarm Message.
                                                AUX_CLASS_ASSERTION(FALSE, alarmEvent.getUrgencyMessage() );
                                                break;
                                        }   // switch

                                    }
                                    break;
                                }
                            case ALARM_COLUMN:                                  // $[TI17]
                                {
                                    // Column 3, the Alarm column.
                                    if (rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_PENDING
                                        || rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_DONE)
                                    {

                                        wcscpy(alarmStr, L" ");
                                    }
                                    else
                                    {                                               // $[TI19]
                                        // Retrieve the Message id for Alarm column
                                        messageIds[0] = alarmEvent.getBaseMessage();                                      
                                        if(messageIds[0] != MESSAGE_NAME_NULL)
                                        {   
                                            // Get the Alarm string from Alarm-Analysis and
                                            // translate that message to ASCII text.
                                            convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF, AlarmStrs::GetMessage(MessageName(messageIds[0])));

                                        }
                                        else
                                        {
                                            wcscpy(alarmStr, L" ");

                                        }
                                        wcscat(alarmStr,L"\t");
                                        
                                    }
                                    break;
                                }
                            case ANALYSIS_COLUMN:                                   // $[TI22]
                                {
                                    // Column 4, the Analysis column.
                                    if (rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_PENDING
                                        || rAlarmAction.systemLevelId == (AlarmAction::SystemLevelId) AlarmAction::CLOCK_UPDATE_DONE)
                                    {
                                        wcscpy(alarmStr, L" ");
                                    }
                                    else
                                    {

                                        Int32 numMsgIds = 0;
                                        // Retreive the analysis messages.  The first step is to copy the
                                        // message id's into the null-terminated messageIds[] array.
                                        const MessageNameArray& rAnalysisMsgs =  alarmEvent.getAnalysisMessage();

                                        for (Int32 i = 0; i < alarmEvent.getAnalysisIndex(); i++)
                                        {   // $[TI24]

                                            CLASS_ASSERTION(numMsgIds < MAX_MSG_IDS);
                                            CLASS_ASSERTION(rAnalysisMsgs[i] != MESSAGE_NAME_NULL);
                                            messageIds[numMsgIds++] = rAnalysisMsgs[i];
                                        }   // $[TI25]

                                        CLASS_ASSERTION(numMsgIds < MAX_MSG_IDS);
                                        messageIds[numMsgIds] = MESSAGE_NAME_NULL;
                                                                                
                                        
                                        // Check if there's no analysis message
                                        if (numMsgIds > 0)
                                        {   // $[TI26]

                                            // Translate each Message Id to ASCII text.
                                            wchar_t temp[MAX_TEMP_BUFF];
                                            for (Int32 phraseIdx = 0; messageIds[phraseIdx] != MESSAGE_NAME_NULL; phraseIdx++)
                                            {
                                                temp[0] = L'\0';                                       
                                                // Get the Alarm string from Alarm-Analysis and
                                                // translate that message to ASCII text.
                                                convertAlarmMsgToAscii_(temp, MAX_TEMP_BUFF, AlarmStrs::GetMessage(MessageName(messageIds[phraseIdx])));

                                                // Prevent adding characters into the buffer if it is
                                                // full.
                                                if(MAX_TEMP_BUFF <= (wcslen(temp) + wcslen(alarmStr)) )
                                                {
                                                     alarmStr[ (wcslen(alarmStr) - 1) ] = L'\0';
                                                     break;
                                                }
                                                else
                                                {
                                                    // Only copy the first message into the
                                                    // buffer, else concatenate the rest into
                                                    // the buffer.
                                                    if(phraseIdx == 0)
                                                    {                                                    
                                                        wcscpy(alarmStr,temp);
                                                    }
                                                    else
                                                    {                                                      
                                                        wcscat(alarmStr,temp);
                                                    }
                                                }
                                                
                                            }

                                        } 
                                        else
                                        {
                                            wcscpy(alarmStr, L" ");
                                        }
                                          // $[TI27]                                                                           
                                        // $[TI25]                                        
                                    }
                                    break;
                                }

                            default:
                                // Not a valid Alarm Column.
                                AUX_CLASS_ASSERTION(FALSE, currentColumn);
                                break;
                        }  // end of switch
                    }
                    else
                    {

                        switch (currentColumn)
                        {
                            case TIME_COLUMN:
                                {                                               // $[TI29]
                                    // Column 0, the Time column.                                  
                                    const TimeStamp & timeStamp = alarmEvent.getTimeStamp();
                                    wchar_t temp[MAX_TEMP_BUFF];
                                    TextUtil::FormatTime(temp, L"%H:%M:%S %d %n %y", timeStamp);
                                    wcscpy(alarmStr, temp);
                                    break;
                                }
                            case EVENT_COLUMN:                                  // $[TI30]
                                {
                                    // Column 1, the Event column.
                                    convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF ,  MiscStrs::ALOG_UNKNOWN_ALARM_EVENT);                     // $[TI16]
                                    break;
                                }
                            case URGENCY_COLUMN:
                                {                                               // $[TI31]
                                    // Column 2, the Urgency data column
                                    wcscpy(alarmStr, L"     ");
                                    break;
                                }
                            case ALARM_COLUMN:
                                {                                               // $[TI32]
                                    // Column 3, the Alarm column.
                                    convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF ,  MiscStrs::ALOG_ALARM_DATA_LOST);                     // $[TI16]
                                    break;
                                }
                            case ANALYSIS_COLUMN:                               // $[TI33]
                                {
                                    // Column 4, the Analysis column.
                                    convertAlarmMsgToAscii_(alarmStr, MAX_TEMP_BUFF ,  MiscStrs::ALOG_ALARM_DATA_CORRUPT);                     // $[TI16]
                                    break;
                                }
                            default:
                                // Not a valid Alarm Column.
                                AUX_CLASS_ASSERTION(FALSE, currentColumn);  
                                break;
                        }   /* Switch */
                    }  // if !latestLogEntries_[i].isCorrected()
                }  // $[TI17]

                // Prevent adding characters into the buffer if it is
                // full.  Must add "..." if the string is longer than
                // the buffer size.
                if(MAX_TEMP_BUFF <= (wcslen(tmpBuf) + wcslen(alarmStr)) )
                {
                     Uint8 alarmIndex = 0;
                     for(Uint8 tmpIndex = wcslen(tmpBuf); 
                         tmpIndex < MAX_TEMP_BUFF && alarmIndex < wcslen(alarmStr); 
                         tmpIndex++, alarmIndex++)
                     {
                         tmpBuf[tmpIndex ] = alarmStr[alarmIndex];
                     }

                     tmpBuf[ (MAX_TEMP_BUFF - 6) ] = L'.';
                     tmpBuf[ (MAX_TEMP_BUFF - 5) ] = L'.';
                     tmpBuf[ (MAX_TEMP_BUFF - 4) ] = L'.';
                     tmpBuf[ (MAX_TEMP_BUFF - 3) ] = L'\0';
                     
                }
                else
                {
                     wcscat(tmpBuf, alarmStr);
                     wcscat(tmpBuf, L"\t");   
                }

            }    // currentColumn < numColumns 

            // Send the message for DATA_UPLOAD_COMMAND to the SerialOutput queue. 
#ifdef FORNOW
            printf("   tmpBuf = %s\n", tmpBuf);
#endif
            LaptopMessageHandler::ClearBuffer();
            LaptopMessageHandler::SetMessageNo();
			// TODO E600 Unicode Text 
			// This is an external communication interface class and not sure that other side is ready to .
			// accept wide char hence converting to char
			char cTempBuff[MAX_TEMP_BUFF]; 
			StringConverter::ToString( cTempBuff, MAX_TEMP_BUFF, tmpBuf );
            LaptopMessageHandler::SetLogCommand(LaptopEventId::DATA_UPLOAD_COMMAND, cTempBuff);

#ifdef SIGMA_UNIT_TEST
            printf("        Log Data = %s\n", &(LaptopMessageHandler::GetBuffer()[LaptopEventId::OUTGOING_LOG_DATA_IDX]));              
#endif
            // Ask GuiApp to post the message to the Serial I/O queue.
            LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
                                                            LaptopMessageHandler::GetBuffer(),
                                                            LaptopMessageHandler::GetBufferIndex());
        } // currentRow < logSize               // $[TI18]

    } // logSize > 0                            // $[TI19]

    // Send the message for END_UPLOAD_COMMAND to the SerialOutput queue. 
    LaptopMessageHandler::ClearBuffer();
    LaptopMessageHandler::SetMessageNo();
    LaptopMessageHandler::SetLogCommand(LaptopEventId::END_UPLOAD_COMMAND);

    // Ask GuiApp to post the message to the Serial I/O queue.
    LaptopMessageHandler::SerialOutputEventHappened(
                                                   SerialInterface::SERIAL_OUTPUT_AVAILABLE,
                                                   LaptopMessageHandler::GetBuffer(),
                                                   LaptopMessageHandler::GetBufferIndex());
    // $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmLogSubScreen::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("AlarmLogSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMLOGSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  convertAlarmMsgToAscii_(char * destStr, Int32 stringLen, const char * srcStr)
//
//@ Interface-Description
//  This method filters out unnecessary characters, and convert symbols to ASCII  
//  characters. 
//  Parameters are as follows:
// 
//      destStr        Pointer to the return Alarm text which contains 
//                     only ASCII characters.
//      stringLen      Length of the return Alarm text which contains 
//                     only ASCII characters.
//      srcStr        Pointer to the original Alarm Message.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method loops through each character to remove subscripts, 
//  italics, brackets, and thin fonts and to convert symbols to ASCII characters.
//  It returns the resulted Alarm text to the 'destStr' parameter.
//---------------------------------------------------------------------
//@ PreCondition
// The 'destStr', 'srcStr' arguments must not be NULL
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void 
 
AlarmLogSubScreen::convertAlarmMsgToAscii_(wchar_t * destStr, Int32 stringLen, const wchar_t * srcStr)
 
{
    CALL_TRACE("AlarmLogSubScreen::convertAlarmMsgToAscii_(char * destStr, Int32 stringLen, const char * srcStr)");

    // Check for illegal null parameters
    CLASS_ASSERTION(destStr != NULL);
    CLASS_ASSERTION(srcStr != NULL);
    CLASS_ASSERTION( (stringLen > wcslen(srcStr)) );

    // Start the string position at zero.
    stringPos_ = 0;
    
    // Loop through the string to filter out unnecessary characters
    // and to translate symbols to ASCII characters.
    for(int index = 0; index < wcslen(srcStr); index++)
    {
            switch(srcStr[index])
            {
                case L'}':    // Remove Brackets
                    break;
                case L'{':
                    if(index != 0) // Add a space when there are 
                    {              // nested brackets
                        if((index + 2) < wcslen(srcStr))
                        {
                            if(srcStr[(index + 1)] == L'A'
                               && srcStr[(index + 2)] == L':')                            
                            {
                                index = index + 2;
                            }
                            else
                            {
                                updateAsciiString_(destStr, stringLen, L' ');
                            }
                        }
                        else
                        {                       
                           updateAsciiString_(destStr, stringLen, L' ');
                        }
                    }
                    break;
                case L'$':   // Get rid of the auto-hyphen found in French Alarm strings
                    break;

                // List of Symbols found in Polish
                case 0xA3:   // Symbol: L
                    updateAsciiString_(destStr, stringLen, L'L');
                    break;
                case 0xEA:   // Symbol: e
                    updateAsciiString_(destStr, stringLen, L'e');
                    break;
                case 0xB6:   // Symbol: s'
                    updateAsciiString_(destStr, stringLen, L's');                                   
                    break;
                case 0xB3:   // Symbol: l
                    updateAsciiString_(destStr, stringLen, L'l');                                   
                    break;
                // end list of Symbols found in Polish

                case 0x0E:   // Symbol: <=
                    updateAsciiString_(destStr, stringLen, L'<');
                    updateAsciiString_(destStr, stringLen, L'=');                                       
                    break;
                case 0x0D:   // Symbol: >=
                    updateAsciiString_(destStr, stringLen, L'>');
                    updateAsciiString_(destStr, stringLen, L'=');                                       
                    break;
                case 0x01:   // Symbol: V
                    updateAsciiString_(destStr, stringLen, L'V');                                     
                    break;
                case 0x04:   // Symbol: Arrow up
                    updateAsciiString_(destStr, stringLen, L'H');
                    updateAsciiString_(destStr, stringLen, L'i');  
                    updateAsciiString_(destStr, stringLen, L'g');
                    updateAsciiString_(destStr, stringLen, L'h');                                       
                    updateAsciiString_(destStr, stringLen, L' ');                                                      

                    break;
                case 0x03:   // Symbol: Arrow down
                    updateAsciiString_(destStr, stringLen, L'L');
                    updateAsciiString_(destStr, stringLen, L'o');  
                    updateAsciiString_(destStr, stringLen, L'w');
                    updateAsciiString_(destStr, stringLen, L' ');                                       
                    break;

                case L'N':
                case L'J':
                case L'A':
                case L'T':    // Remove formating variables. i.e Italics,
                case L'I':    // Thin 
                    if((index + 1) < wcslen(srcStr))
                    {
                        if(srcStr[(index + 1)] == L':' || 
                          (srcStr[(index)] == L'T' && srcStr[(index + 1)] == L','))
                        {
                            index ++;
                        }
                        else
                        {
                            updateAsciiString_(destStr, stringLen, srcStr[index]);                            
                        }
                    }
                    else
                    {
                        updateAsciiString_(destStr, stringLen, srcStr[index]);                            
                    }
                    break;

                case L'S':   // Remove Subscript

                    if((index + 1) < wcslen(srcStr))
                    {
                        if(srcStr[(index + 1)] == L':')
                        {
                            index ++;

                        }
                        else
                        {
                            updateAsciiString_(destStr, stringLen, srcStr[index]);                            
                        }

                    }
                    else
                    {
                        updateAsciiString_(destStr, stringLen, srcStr[index]);                            
                    }
                    break;
                case L'p':
                case L'x':
                case L'y':
                case L'c':
                case L'X':
                case L'Y':
                case L'o':   // Remove commands such as point size,
                            // font color, and font positions.
                    if((index +1) < wcslen(srcStr))
                    {
                        if(srcStr[(index + 1)] == L'=')
                        {
                            index = index + 2;
                            // Skip all commands until a colon is 
                            // reached.
                            while(srcStr[index] != L':')
                            {
                                index ++;
                            }
                        }
                        else
                        {
                            updateAsciiString_(destStr, stringLen, srcStr[index]);                            
                        }

                    }
                    else
                    {
                        updateAsciiString_(destStr, stringLen, srcStr[index]);                            
                    }
                    break;
                default:    // Must be an ASCII character.
                    updateAsciiString_(destStr, stringLen, srcStr[index]);                            
                    break;
            }
    }

    // Add a space
    updateAsciiString_(destStr, stringLen, L' ');                            

    // Adding a null terminator.
    updateAsciiString_(destStr, stringLen, L'\0');                            

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateAsciiString_(char * destStr, Int32 stringLen, char character)
//
//@ Interface-Description
//  This method updates the destStr with a character.
//  Parameters are as follows:
// 
//      destStr        Pointer to the return Alarm text 
//      stringLen      Length of the return Alarm text 
//      character      The character that will append to the destStr.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method checks to verify that the current string position must 
//  be less than the maximum string length of destStr.  If the condition
//  is true, store the character, otherwise store a null terminated
//  character at the end of the destStr.
//---------------------------------------------------------------------
//@ PreCondition
// The 'destStr' must not be NULL and stringLen must be greater than zero.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void 
AlarmLogSubScreen::updateAsciiString_(wchar_t * destStr, Int32 stringLen, wchar_t character)
{
    CALL_TRACE("AlarmLogSubScreen::updateAsciiString_(char * destStr, Int32 stringLen, char character)");

    // Check for illegal null parameters
    CLASS_ASSERTION(destStr != NULL);
    CLASS_ASSERTION(stringLen > 0);

    // Verify that the current string position must be less than 
    // the maximum string length of destStr.  If the condition
    // is true, store the character, otherwise store a null terminated
    // character at the end of the destStr.
    if(stringPos_ < (stringLen -1))
    {    
        destStr[stringPos_] = character;
        stringPos_ ++;
    }
    else
    {
        destStr[(stringLen -1)] = L'\0';
    }
    
}


