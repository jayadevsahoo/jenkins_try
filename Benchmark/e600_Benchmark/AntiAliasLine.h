#pragma once

class AntiAliasLine
{
private:
	static void DrawPixel(CDC *pDC, short x, short y, short color);

public:
	static void Draw( CDC *pDC, short X0, short Y0, short X1, short Y1, short BaseColor, short NumLevels, unsigned short IntensityBits);
	static void Draw( CDC *pDC, int X0, int Y0, int X1, int Y1, COLORREF clrLine );
	AntiAliasLine(void);
	~AntiAliasLine(void);
};
