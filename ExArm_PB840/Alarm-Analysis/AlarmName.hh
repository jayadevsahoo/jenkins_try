#ifndef AlarmName_HH
#define AlarmName_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================
//
//====================================================================
// Filename: AlarmName - Enumerated names for Alarms
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmName.hhv   25.0.4.0   19 Nov 2013 13:51:16   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By:   hct    Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct    Date: 05/22/95         DR Number:   7
//      Project:   Sigma   (R8027)
//      Description:
//         Added alarm names A241, A242, A243 for battery alarms to reflect 
//         changes to Non-Technical Alarm text and structure as per changes
//         to SRS.
//  
//   Revision 003   By:   hct    Date: 02/26/97         DR Number:  1793
//      Project:   Sigma   (R8027)
//      Description:
//         Delete WEAK BATTERY alarm.
//  
//   Revision 004   By:   hct    Date: 09/29/97         DR Number:  1475
//      Project:   Sigma   (R8027)
//      Description:
//         Add VOLUME LIMIT alarm.
//  
//   Revision 005   By:   hct      Date: 01/13/99       DR Number: 5322
//      Project:   Sigma   (R8O27)
//      Description:
//         Add ATC functionality.
//
//   Revision 006   By:   hct      Date: 08/11/00       DR Number: 5313
//      Project:   PAV
//      Description:
//         Add PAV alarms.
//
//  Revision: 007   By:   hct      Date: 30-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      Create VTPC alarms.
//
//  Revision: 008   By:   hct    Date: 24-OCT-2000      DR Number: 5755
//  Project:  VTPC
//  Description:
//      Differentiate between spont and mand breaths for VTPC alarms.
//
//  Revision:009   By:   heatherw Date: 8-MAR-2002     DR Number: 5790
//  Project:  VTPC
//  Description:
//      Added functionality for Vti Spont Pressure and Volume alarms.
//
//   Revision: 010   By:   erm    Date: 11-MAR-2002      DR Number: 5790
//  Project:  VTPC
//  Description:
//      Added Mand inspired volume alarm A390.
//
//   Revision: 011   By:   gdc    Date: 21-Feb-2005      DR Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - NIV1
//      Modifications to support NIV.
//      Removed obsolete A370 (low insp pressure) alarm.
//      Added A400 (Low Ppeak alarm).
//
//  Revision: 012   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 013   By: rhj    Date: 23-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added A1000
// 
//====================================================================

//@ Usage-Classes
#include "Sigma.hh"


//@ Type: AlarmName
// Enumeration for Alarm::name_.
 enum AlarmName {ALARM_NAME_NULL = -1,   
                 A5,   A10,  A15,  A20,  A25,  A30,  A35,  A40,  A45,  A50,  
                 A55,  A60,  A65,  A70,  A75,  A80,  A85,  A110, A205, A210, 
                 A215, A220, A225, A230, A235, A240, A241, A243, A250, A255,
                 A260, A265, A270, A280, A285, A290, A295, A300, A305, A310,
                 A315, A335, A345, A355, A360, A365, A366, A380, A385,
                 A390, A400, A1000,  

                 MAX_NUM_ALARMS
                  
                 };

#endif // AlarmName_HH
