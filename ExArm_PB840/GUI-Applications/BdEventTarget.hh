#ifndef BdEventTarget_HH
#define BdEventTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BdEventTarget - A target for new breath events.
// Classes can specify this class as an additional parent class and register to
// be informed of occurrences of new breath events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BdEventTarget.hhv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 003   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00 
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd		Date:  18-MAY-95	DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "EventData.hh" 
#include "GuiAppClassIds.hh"
//@ End-Usage

class BdEventTarget
{
public:
	virtual void bdEventHappened(EventData::EventId eventId,
			                	EventData::EventStatus eventStatus,
								EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	BdEventTarget(void);
	virtual ~BdEventTarget(void);


private:
	// these methods are purposely declared, but not implemented...
	BdEventTarget(const BdEventTarget&);	// not implemented...
	void operator=(const BdEventTarget&);	// not implemented...
};


#endif // BdEventTarget_HH 
