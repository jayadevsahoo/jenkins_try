// TabPowerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "swat.h"
#include "TabPowerDlg.h"

// CTabPowerDlg dialog

IMPLEMENT_DYNAMIC(CTabPowerDlg, CDialog)

CTabPowerDlg::CTabPowerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTabPowerDlg::IDD, pParent)
	, m_nPower(0)
	, m_strOption(_T(""))
	, m_strOnOff(_T(""))
{

}

CTabPowerDlg::~CTabPowerDlg()
{
}

void CTabPowerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EXPLORER_RESET, m_webCtrl);
	DDX_CBString(pDX, IDC_COMBO_OPTIONS, m_strOption);
	DDX_CBString(pDX, IDC_COMBO_ONOFF, m_strOnOff);
}


BEGIN_MESSAGE_MAP(CTabPowerDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CTabPowerDlg::OnBnClickedButtonRefresh)
	ON_BN_CLICKED(IDC_BUTTON_APPLY, &CTabPowerDlg::OnBnClickedButtonApply)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON1, &CTabPowerDlg::OnBnClickedButtonDefault)
	ON_BN_CLICKED(IDC_BUTTON_HIT_S, &CTabPowerDlg::OnBnClickedButtonHitS)


END_MESSAGE_MAP()


// CTabPowerDlg message handlers

BOOL CTabPowerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();


/*	CString strIP = ((CSwatApp*)AfxGetApp())->m_strPowerSwitch_IPAddr;
 	if(strIP.IsEmpty())
	{
		((CSwatApp*)AfxGetApp())->SaveString2LocalTempFile(TEMP_FILE_POWER_SWITCH_MISSING_ERROR,"Power Switch IP (power_switch_ip) not found in the SWATinit file.");
		strIP = TEMP_FILE_POWER_SWITCH_MISSING_ERROR;
	}
	m_webCtrl.Navigate(strIP,NULL,NULL,NULL,NULL);
	m_SwitchState = NOT_LOGGED_IN;
*/

	SetTimer(123456, 500, NULL);


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
BEGIN_EVENTSINK_MAP(CTabPowerDlg, CDialog)
	ON_EVENT(CTabPowerDlg, IDC_EXPLORER_RESET, 259, CTabPowerDlg::DocumentCompleteExplorerReset, VTS_DISPATCH VTS_PVARIANT)
END_EVENTSINK_MAP()

void CTabPowerDlg::DocumentCompleteExplorerReset(LPDISPATCH pDisp, VARIANT* URL)
{
//	if(((CSwatApp*)AfxGetApp())->m_Power_switch_connected != CONNECTED) return;
	///////////////////////////////////////////////////


//	ClickIfTimeOut();

/*	
	m_SwitchState = GetStatus();//don't click anything in getstatus which create endless loop

	if(m_SwitchState == LOGGIN_ERROR)
	{
		LOG_MESSAGE(LM_INFO,"logginerror %s", __FUNCTION__);
		ClickLogInError();
	}
	if(m_SwitchState == NOT_LOGGED_IN)
	{
		LOG_MESSAGE(LM_INFO,"not_loggedIn %s", __FUNCTION__);
		LogIn();
	}
*/	
}


bool CTabPowerDlg::LogIn()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);

	void* pElem= ElemFromID(L"Username", IID_IHTMLInputElement) ;
	IHTMLInputElement* pTextBoxUser= (IHTMLInputElement*)pElem;

	HRESULT hr = pTextBoxUser->put_value( L"some" ); // populate a text box
	hr = pTextBoxUser->put_value( L"admin" ); // populate a text box

	pElem= ElemFromID(L"Password", IID_IHTMLInputElement) ;
	IHTMLInputElement* pTextBoxPswd= (IHTMLInputElement*)pElem;

	hr= pTextBoxPswd->put_value( L"some2overwrite" ); 
	hr= pTextBoxPswd->put_value( L"12345678" ); 

	pElem= ElemFromID(L"Submitbtn", IID_IHTMLElement);
	IHTMLElement* pSubmit= (IHTMLElement*)pElem;

	hr= pSubmit->click(); 
	m_SwitchState = LOGGED_IN;

	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);
	return true;

}

bool CTabPowerDlg::ClickLogInError()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	HRESULT hr;
	IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
	if(pDoc == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	CComBSTR bstr;
	pDoc->get_title(&bstr);
	if(CComBSTR(bstr)==CComBSTR(L"TF-33x WebControl"))
	{
		//if there was any error in logging, click ok and set logged in to false so it tries to log in again.
		//ok button on error html page has following:
		/*<input TYPE="button" NAME="back" VALUE="OK" ONCLICK="window.location='/'"> */
		void* pElem= ElemFromID(L"back", IID_IHTMLElement);
		IHTMLElement* pSubmit= (IHTMLElement*)pElem;

		((CSwatApp*)AfxGetApp())->LogStatus("Error logging in, retrying now...");
//		m_SwitchState = NOT_LOGGED_IN;
		hr= pSubmit->click(); 
		return false;
	};
	return true;

}


void CTabPowerDlg::Refresh()
{
/*	static bool b = true;

	CString IPCmd0 = "http://admin:12345678@192.168.0.50/Set.cmd?CMD=SetPower+P60=0+P61=0+P62=0+P63=0";
	CString IPCmd1 = "http://admin:12345678@192.168.0.50/Set.cmd?CMD=SetPower+P60=1+P61=0+P62=0+P63=0";



	if(b) m_webCtrl.Navigate( IPCmd1 ,NULL,NULL,NULL,NULL);
	else m_webCtrl.Navigate( IPCmd0 ,NULL,NULL,NULL,NULL);

	b  = !(b);

	m_webCtrl.Refresh();
*/	
}


//-------------------------------------------------------
// Returns a desired type of IHTMLElement pointer
// to provide access to the object with a specific ID
//
void* CTabPowerDlg::ElemFromID( LPWSTR szID, IID nTypeIID )
{
//	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	HRESULT hr;
	IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
	IHTMLElementCollection* pAll=  NULL;
 
	hr= pDoc->get_all( &pAll ); 

	CComVariant vElement( szID ); // the id or name of the control
	CComVariant vIndex(0,VT_I4);  // 0 (presume it's not a collection)
 
	IDispatch* pDisp;
 	hr= pAll->item(vElement,vIndex,&pDisp);
	if(pDisp == NULL ) return NULL;
 
	void* pElement;   // will coerce to desired type later
 	hr= pDisp->QueryInterface( nTypeIID,(void**)&pElement);

//	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);
	return( pElement );
}

power_page_status_t CTabPowerDlg::GetStatus()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	//check if login error
	{
		IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
		if(pDoc == NULL )
		{
			LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
			m_SwitchState =  ERROR_GETTING_STATE;
		}
		CComBSTR bstr;
		pDoc->get_title(&bstr);

		if(CComBSTR(bstr)==CComBSTR(L"TF-33x WebControl"))
		{	m_SwitchState = LOGGIN_ERROR;
			return LOGGIN_ERROR;
		}
	}
	//check if on login page (not_logged_in)
	{
		void* pElem= ElemFromID(L"Username", IID_IHTMLInputElement) ;
		if(pElem != NULL) 
		{
			LOG_MESSAGE(LM_INFO,"pTextBoxUser is not nill %s", __FUNCTION__);
			return NOT_LOGGED_IN;
		}
	}
	//check if two frames open( logged_in)
	{
		LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
		HRESULT hr;
		IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
		if(pDoc == NULL )
		{
			LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
			return ERROR_GETTING_STATE;
		}

		IHTMLFramesCollection2 *pAllFrames = NULL;
		hr = pDoc->get_frames(&pAllFrames);
		if(hr != S_OK )
		{
			LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
			return ERROR_GETTING_STATE;
		}
		if(pAllFrames == NULL )
		{
			LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
			return ERROR_GETTING_STATE;
		}

		CComVariant vIndex(1,VT_I4);  // Get frame at location 1
		CComVariant vFrame(VT_DISPATCH);  

		hr = pAllFrames->item(&vIndex, &vFrame);
		if(hr == S_OK )
		{//got two frames, it's logged in
			return LOGGED_IN;
		}
	}


	return ERROR_GETTING_STATE;




}
bool CTabPowerDlg::ClickLinkFrame0( )
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	
	
	HRESULT hr;
	IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
	if(pDoc == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	IHTMLFramesCollection2 *pAllFrames = NULL;
	hr = pDoc->get_frames(&pAllFrames);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if(pAllFrames == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	CComVariant vIndex(0,VT_I4);  // Get frame at location 0?
	CComVariant vFrame(VT_DISPATCH);  

	hr = pAllFrames->item(&vIndex, &vFrame);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	void* pVoid;
	hr= vFrame.pdispVal->QueryInterface( IID_IHTMLWindow2,(void**)&pVoid);
	if(vFrame.pdispVal == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLWindow2* pWin= (IHTMLWindow2*)pVoid;
	IHTMLDocument2* pDocF0;
	pWin->get_document(&pDocF0);
	if(pWin == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if( pDocF0 == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLElementCollection* pAll=  NULL;
	hr= pDocF0->get_all( &pAll ); 
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if( pAll == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}



	//click link
	CComVariant vIndex0(0,VT_I4);  // 0 
	IDispatch* pDisp;
	void* pElement; 
	


	CComVariant vElementApply( L"Power Controls" ); 
	if( pAll == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
 	hr= pAll->item(vElementApply, vIndex0, &pDisp);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if(pDisp == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
 	hr= pDisp->QueryInterface( IID_IHTMLElement,(void**)&pElement);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLElement* pSubmit= (IHTMLElement*)pElement;
	if(pSubmit==NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	hr= pSubmit->click(); 
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);
	return true;
}
bool CTabPowerDlg::SwitchOnOff(bool bOnOff)
{
	if(((CSwatApp*)AfxGetApp())->m_strPowerSwitch_IPAddr.IsEmpty()) 
	{
		LOG_MESSAGE(LM_INFO,"Pwr IP empty in %s returning", __FUNCTION__);
		return false;
	}


	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	HRESULT hr;
	IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
	if(pDoc == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	IHTMLFramesCollection2 *pAllFrames = NULL;
	hr = pDoc->get_frames(&pAllFrames);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if(pAllFrames == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	CComVariant vIndex(1,VT_I4);  // Get frame at location 1
	CComVariant vFrame(VT_DISPATCH);  

	hr = pAllFrames->item(&vIndex, &vFrame);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	void* pVoid;
	hr= vFrame.pdispVal->QueryInterface( IID_IHTMLWindow2,(void**)&pVoid);
	if(vFrame.pdispVal == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLWindow2* pWin= (IHTMLWindow2*)pVoid;
	IHTMLDocument2* pDocF0;
	pWin->get_document(&pDocF0);
	if(pWin == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if( pDocF0 == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLElementCollection* pAll=  NULL;
	hr= pDocF0->get_all( &pAll ); 
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if( pAll == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	//click ON
	CComVariant vElementF0( L"P60" ); // the id or name of the control
	CComVariant vIndex0(0,VT_I4);  // 0 
	CComVariant vIndex1(1,VT_I4);
	IDispatch* pDisp;

	if(bOnOff)
	{
 		hr= pAll->item(vElementF0, vIndex0, &pDisp);
	}
	else
	{
		hr= pAll->item(vElementF0, vIndex1, &pDisp);
	}

	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if( pDisp == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	void* pElement;   
 	hr= pDisp->QueryInterface( IID_IHTMLElement,(void**)&pElement);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLElement* pSubmit= (IHTMLElement*)pElement;
	if( pSubmit == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	hr= pSubmit->click(); 
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	//click APPLY
	CComVariant vElementApply( L"Apply" ); 
	if( pAll == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
 	hr= pAll->item(vElementApply, vIndex0, &pDisp);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if(pDisp == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
 	hr= pDisp->QueryInterface( IID_IHTMLElement,(void**)&pElement);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	pSubmit= (IHTMLElement*)pElement;
	if(pSubmit==NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	hr= pSubmit->click(); 
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	

	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);

	return true;
	
}


bool CTabPowerDlg::ClickIfTimeOut()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	HRESULT hr;
	IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
	if(pDoc == NULL )
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}

	IHTMLFramesCollection2 *pAllFrames = NULL;
	hr = pDoc->get_frames(&pAllFrames);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}
	if(pAllFrames == NULL )
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}

	CComVariant vIndex(1,VT_I4);  // Get frame at location 1
	CComVariant vFrame(VT_DISPATCH);  

	hr = pAllFrames->item(&vIndex, &vFrame);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}
	void* pVoid;
	hr= vFrame.pdispVal->QueryInterface( IID_IHTMLWindow2,(void**)&pVoid);
	if(vFrame.pdispVal == NULL )
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLWindow2* pWin= (IHTMLWindow2*)pVoid;
	IHTMLDocument2* pDocF0;
	pWin->get_document(&pDocF0);
	void* pElement;   
	if(pWin == NULL )
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}
	if( pDocF0 == NULL)
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLElementCollection* pAll=  NULL;
	hr= pDocF0->get_all( &pAll ); 
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}
	if( pAll == NULL)
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}


	//click Re Login(Submit3)
	CComVariant vIndex0(0,VT_I4);  // 0 
	IDispatch* pDisp;

	CComVariant vElementApply( L"Submit3" ); 
	if( pAll == NULL)
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}

 	hr= pAll->item(vElementApply, vIndex0, &pDisp);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"no time out yet %s", __FUNCTION__);
		return false;
	}
	if(pDisp == NULL)
	{
		LOG_MESSAGE(LM_INFO,"no time out yet %s", __FUNCTION__);
		return false;
	}
 	hr= pDisp->QueryInterface( IID_IHTMLElement,(void**)&pElement);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLElement* pSubmit = (IHTMLElement*)pElement;
	if(pSubmit==NULL)
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}
	hr= pSubmit->click(); 
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
		return false;
	}

	LOG_MESSAGE(LM_ERROR,"Exit %s", __FUNCTION__);
	return true;

}
void CTabPowerDlg::OnBnClickedButtonRefresh()
{
	CString strIP = ((CSwatApp*)AfxGetApp())->m_strPowerSwitch_IPAddr;
 	if(strIP.IsEmpty())
	{
		((CSwatApp*)AfxGetApp())->SaveString2LocalTempFile(TEMP_FILE_POWER_SWITCH_MISSING_ERROR,"Power Switch IP (power_switch_ip) not found in the SWATinit file.");
		strIP = TEMP_FILE_POWER_SWITCH_MISSING_ERROR;
	}
	m_webCtrl.Navigate(strIP,NULL,NULL,NULL,NULL);
//	m_SwitchState = NOT_LOGGED_IN;

	//m_webCtrl.Refresh();
}

void CTabPowerDlg::OnBnClickedButtonApply()
{
	CSwatApp* pApp = ((CSwatApp*)AfxGetApp());
	UpdateData(TRUE);


	CString strOn = "On";
	CString strOff = "Off";


	int nPortNum = -1;
	bool bToSwitchOnOROff = 0;

	if((m_strOption.Find("Vent switch")!=-1)	  && (m_strOnOff==strOn))
	{
		nPortNum = atoi(pApp->m_strPortVentSwitch);	bToSwitchOnOROff = 1;
	}
	else if((m_strOption.Find("Vent switch")!=-1) && (m_strOnOff==strOff))
	{
		nPortNum = atoi(pApp->m_strPortVentSwitch);	bToSwitchOnOROff = 0;
	}

	else if ((m_strOption.Find("Service mode switch")!=-1)&& (m_strOnOff==strOn))
	{
		nPortNum = atoi(pApp->m_strPortServiceMode);	bToSwitchOnOROff = 1;
	}
	else if ((m_strOption.Find("Service mode switch")!=-1)&& (m_strOnOff==strOff))
	{
		nPortNum = atoi(pApp->m_strPortServiceMode);	bToSwitchOnOROff = 0;
	}

	else if ((m_strOption.Find("Power")!=-1)&& (m_strOnOff==strOn))
	{
		nPortNum = atoi(pApp->m_strPortPower);	bToSwitchOnOROff = 1;
	}
	
	else if ((m_strOption.Find("Power")!=-1)&& (m_strOnOff==strOff))
	{
		nPortNum = atoi(pApp->m_strPortPower);	bToSwitchOnOROff = 0;
	}
	else 
	{
		AfxMessageBox("Error in vent options, please select valid options.");
		return;
	}

	((CSwatApp*)AfxGetApp())->Power_on_off_vent("swat", nPortNum, bToSwitchOnOROff);

}

void CTabPowerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if(((CSwatApp*)AfxGetApp())->m_bScriptRunning)
	{
		CWnd* hWnd = (CWnd*)GetDlgItem(IDC_BUTTON_APPLY);
		if(hWnd) hWnd->EnableWindow(FALSE);
		hWnd = (CWnd*)GetDlgItem(IDC_BUTTON1);
		if(hWnd) hWnd->EnableWindow(FALSE);
	
	}
	else
	{
		CWnd* hWnd = (CWnd*)GetDlgItem(IDC_BUTTON_APPLY);
		if(hWnd) hWnd->EnableWindow(TRUE);
		hWnd = (CWnd*)GetDlgItem(IDC_BUTTON1);
		if(hWnd) hWnd->EnableWindow(TRUE);
	
	}


	CDialog::OnTimer(nIDEvent);
}

void CTabPowerDlg::OnBnClickedButtonDefault()
{
	CSwatApp* pApp = ((CSwatApp*)AfxGetApp());
	if(((CSwatApp*)AfxGetApp())->Power_on_off_vent("swat", atoi(pApp->m_strPortVentSwitch),		1)==FALSE) return;
	if(((CSwatApp*)AfxGetApp())->Power_on_off_vent("swat", atoi(pApp->m_strPortServiceMode),	0)==FALSE) return;
	if(((CSwatApp*)AfxGetApp())->Power_on_off_vent("swat", atoi(pApp->m_strPortPower),			1)==FALSE) return;
}


void CTabPowerDlg::OnBnClickedButtonHitS()
{
	CSwatApp* pApp = ((CSwatApp*)AfxGetApp());
	pApp->TurnOnHitS(false);

}