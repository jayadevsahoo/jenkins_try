#include "stdafx.h"
#ifdef SIGMA_BD_CPU      // compiled only for BD CPU
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename: SafeState.cc - Power on self test Safe State test
//
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains the functions to test the BDU safe state system.
//  The VENT-INOP test verifies the ventilator is not in the VENT-INOP
//  state, a condition which inhibits software control of the ventilator.
//  The Safe-State system test assures the ventilator's safe state system
//  is operational that will allow the patient to breath through a circuit 
//  open to the atmosphere.  A failure of either of these conditions results
//  in a MAJOR POST failure.
//---------------------------------------------------------------------
//@ Rationale
//    The requirements for testing the safe state during POST are
//    satisifed in this module.
//---------------------------------------------------------------------
//@ Implementation-Description
//  >Von
//  
//  During POST vent-inop should not be indicated.  There is a hardware
//  I/O register that is set when vent-inop occurs, this is must be
//  checked before the safe state is checked.
//
//  The safe state of the sigma system allows the patient to inspire
//  room air and exhale.   This is accomplished using a safety valve to
//  permit the spontaneously-breathing patients to breathe through the
//  valve.   This test is performed after the RAM test, but before any
//  of the breath delivery hardware is used.   Safe state must be in
//  progress through the majority of time spent in POST, but as some
//  point safe state must be exited to allow access to the pneumatic
//  hardware.
//
//  The basis of this test is to measure current applied to the PSOLS,
//  exhalation valve, and safety valve, using the loopback signals for
//  these devices.  The loopback signals are read from the ADC system
//  on the analog interface PCB.  During safe state no power should
//  applied to these devices, so the loopback currents should be zero.
//  In addition if a value written to these DAC channels, no current
//  loopback should be read.
//
//
//  What is tested
//
//  Safe state switch (normally open) - This electronic switch disables
//  power to the PSOLS, exhalation valve drivers, and safety valve.
//  During ventilation this relay is closed allowing power to be
//  applied to the pneumatic system.  This test simply verifies the
//  switch is open.
//
//  Safety Valve Loopback - This is an A/D channel that measures the
//  current that is applied to the safety valve driver.  It is read to
//  verify that no current exists.
//
//  Safety Valve Sol Driver - This electronic circuit controls power to
//  the safety valve solenoid, it is under control of the enable
//  safe-state signal.  In safe state this driver is not active.
//
//  Air/O2/Exh DACs - The DAC channels for these devices can not be
//  tested until safe state is exited.  A safe  current value is
//  applied, then measured back through respective loopback signals.
//  The safe current is a defined limit (CLOSE_LEVEL) which is designed
//  not to open the valves if this is applied.
//
//  >Voff
//---------------------------------------------------------------------
//@ Fault Handling
//    Faults during POST cause the test in progress to fail.
//---------------------------------------------------------------------
//@ Restrictions
//    Active control of the safety valve cannot be performed during POST.
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/SafeState.ccv   25.0.4.0   19 Nov 2013 14:17:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004    By: Mitesh Raval   Date: 14-Oct-2010  SCR Number: 6671
//  Project: XENA2
//  Description:
//       Occurrences of DS1286 renamed to Delay.
//
//  Revision: 003   By: Gary Cederquist   Date: 08-May-1998  DR Number: 5084
//    Project:   Sigma   (R8027)
//    Description:
//       SERVICE_REQUIRED now returned by Vent-INOP test instead of
//       MAJOR.  POST nows logs SERVICE_REQUIRED as a MINOR POST error
//       but then stops POST execution as it does for major MAJOR POST 
//       errors.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "BdIoUtil.hh"
#include "PostAdcChannels.hh"
#include "Analog.hh"
#include "Delay.hh"

#ifdef SIGMA_DEBUG
#include <stdio.h>
#endif //SIGMA_DEBUG

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: TestVentInop   (void)
//
//@ Interface-Description
//  Verifies that the ventilator is not in the VENT-INOP state.
//  $[11091]
//---------------------------------------------------------------------
//@ Implementation-Description
//    none
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
Criticality
TestVentInop   (void)
{
   Uint8 ventInopInactive = *SAFETY_NET_READ_REG & VENT_INOP_INACTIVE;

UNIT_TEST_POINT(POSTUT_25)
   if ( ventInopInactive )
   {
      // $[TI1.1]
      return PASSED;
   }
   else
   {
      // $[TI1.2]
      return SERVICE_REQUIRED;
   }
}



//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: TestSafeStateSystem   (void)
//
//@ Interface-Description
//  Verifies that the safe state relay is operational.  Returns a MAJOR
//  failure if it is not.
//
//  TECHNIQUE:
//
//  This test verifies that the safe state relay does not allow current
//  to be applied to the PSOLS, exhalation valve, or safety valve.  
//  A small (safe) current is applied to these devices with the relay 
//  open, the loopback current is measured.  No current should be read
//  from the loop back channels.
//
//  For the safety valve, no current need be applied to the circuit
//  to determine if safe state is operational.  The AI PCB provides
//  a direct measurement by reading SV_PWR_SWITCHED_SIDE.
//
//
//  Step 1:  Read the loopback currents of the Air PSOL, O2 PSOL, exh
//  valve, and the safety valve using the ADC.  The current should be
//  near zero with a small trickle current.   Read flow of Air & O2
//  sensors, both should read 0lpm with a margin of measurement
//  error.   The test fails (MAJOR FAILURE) if an unacceptable amount
//  of current or flow is detected.  $[11057]
//
//  Step 2: Apply a small current to the Air PSOL.  Read the loopback
//  current of the Air PSOL from the ADC mux, no current should exist
//  as the safe state has disabled the PSOL.  Read the Air & O2
//  sensors, both should read 0lpm with a margin of measurement error.
//  The current applied should be approximately equal to CLOSE_LEVEL
//  with a nominal current 100ma below the PSOL liftoff current.  The
//  test fails (MAJOR FAILURE) if an unacceptable amount of current or
//  flow is detected.
//
//  Step 3: Repeat step 2 for the O2 psol.
//
//  Step 4: Repeat step 2 for the exhalation motor drive control.  The
//  value to apply to the DAC is a nominal zero level peep pressure.
//
//  Step 5:  Attempt to close the safety valve with safe state active.
//  Read the loopback current of the safety valve from the ADC mux, no
//  current should exist as the safe state has disabled the valve.
//  $[11058]
//
//  For this test, the safe state circuit has disconnected power to 
//  the DACs of the pneumatic devices.  The loopback DAC channel and 
//  ADC are operational during safe state.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

static Uint16 const CLOSE_LEVEL = 400;

Criticality
TestSafeStateSystem   (void)
{
	static Uint32 const MAX_LOOPS = 10;
	Uint32 loops = 0;
	Boolean fault = FALSE;
   // for better readability and optimization
   const TestTableStruct * testTable = PostAnalogDeviceTestTable;

   for (loops = 0; loops < MAX_LOOPS; loops++)
   {
		// check that whether safe state is on
		Analog::WaitAdcNewSample();

		UNIT_TEST_POINT(POSTUT_26)

		// step 1
		Uint16 svSwitchedSide = Analog::ReadAdc(PostAdcChannels::SAFETY_VALVE_SWITCHED_SIDE);
		Uint16 o2PsolCurrent  = Analog::ReadAdc(PostAdcChannels::O2_PSOL_CURRENT);
		Uint16 airPsolCurrent = Analog::ReadAdc(PostAdcChannels::AIR_PSOL_CURRENT);
		Uint16 exhMotorCurrent= Analog::ReadAdc(PostAdcChannels::EXHALATION_MOTOR_CURRENT);
		Uint16 svCurrent      = Analog::ReadAdc(PostAdcChannels::SAFETY_VALVE_STATE);
		Uint16 o2Flow         = Analog::ReadAdc(PostAdcChannels::O2_FLOW_SENSOR);
		Uint16 airFlow        = Analog::ReadAdc(PostAdcChannels::AIR_FLOW_SENSOR);

		UNIT_TEST_POINT(POSTUT_26_1)
		if (Analog::OutOfRange( svSwitchedSide
								,testTable[PostAdcChannels::SAFETY_VALVE_SWITCHED_SIDE])
			|| Analog::OutOfRange( o2PsolCurrent
								   ,testTable[PostAdcChannels::O2_PSOL_CURRENT] ) 
			|| Analog::OutOfRange( airPsolCurrent
								   ,testTable[PostAdcChannels::AIR_PSOL_CURRENT] ) 
			|| Analog::OutOfRange( exhMotorCurrent
								   ,testTable[PostAdcChannels::EXHALATION_MOTOR_CURRENT] ) 
			|| Analog::OutOfRange( svCurrent
								   ,testTable[PostAdcChannels::SAFETY_VALVE_STATE] ) 
			|| Analog::OutOfRange( o2Flow
								   ,testTable[PostAdcChannels::O2_FLOW_SENSOR] ) 
			|| Analog::OutOfRange( airFlow
								   ,testTable[PostAdcChannels::AIR_FLOW_SENSOR]))
		{
			Delay::WaitForTicks(3); // wait at least 20 ms - 40 ms 
			fault = TRUE;
		}
		else
		{
			fault = FALSE;
			break; // exit loop
		}
   }

   if (fault)
   {
	   return MAJOR;
   }

   for (loops = 0; loops < MAX_LOOPS; loops++)
   {
		// step 2, 3, 4
		WriteWord( AIR_PSOL_DAC_PORT,  CLOSE_LEVEL);
		WriteWord( O2_PSOL_DAC_PORT,   CLOSE_LEVEL);
		WriteWord( EXH_VALVE_DAC_PORT, CLOSE_LEVEL);

		// wait for inductive current to build before sampling
		Delay::WaitForTicks(5);	   //  wait 40-60ms per Danis Carter 24-MAR-97

		Analog::WaitAdcNewSample();

		Uint16 o2Flow          = Analog::ReadAdc(PostAdcChannels::O2_FLOW_SENSOR);
		Uint16 airFlow         = Analog::ReadAdc(PostAdcChannels::AIR_FLOW_SENSOR);
		Uint16 exhMotorCurrent = Analog::ReadAdc(PostAdcChannels::EXHALATION_MOTOR_CURRENT);

		UNIT_TEST_POINT(POSTUT_26_2)
		if (Analog::OutOfRange( exhMotorCurrent
								,testTable[PostAdcChannels::EXHALATION_MOTOR_CURRENT]) 
			|| Analog::OutOfRange( o2Flow
								   ,testTable[PostAdcChannels::O2_FLOW_SENSOR]) 
			|| Analog::OutOfRange( airFlow
								   ,testTable[PostAdcChannels::AIR_FLOW_SENSOR]))
		{
			Delay::WaitForTicks(3); // wait at least 20 ms
			fault = TRUE;
		}
		else
		{
			fault = FALSE;
			break; // exit loop
		}
   }

   if (fault)
   {
	   return MAJOR;
   }

   for (loops = 0; loops < MAX_LOOPS; loops++)
   {
	   // step 5
	   *WRITE_IO_PORT = SAFETY_VALVE_CLOSE_CMD | BD_AUDIO_ALARM_OFF;
	
	   // wait for inductive current to build before sampling
	   Delay::WaitForTicks(16);    //  wait 150-170ms per Danis Carter 24-MAR-97
	
	   Analog::WaitAdcNewSample();
	
	   Uint16 svCurrent      = Analog::ReadAdc(PostAdcChannels::SAFETY_VALVE_STATE);
	
	UNIT_TEST_POINT(POSTUT_26_3)
	   if ( Analog::OutOfRange( svCurrent
							   ,testTable[PostAdcChannels::SAFETY_VALVE_STATE]) ) 
	   {
		   Delay::WaitForTicks(3); // wait at least 20 ms
		   fault = TRUE;
	   }
	   else
	   {
		   fault = FALSE;
		   break; // exit loop
	   }
   }

   if (fault)
   {
	   return MAJOR;
   }

   *WRITE_IO_PORT = SAFETY_VALVE_OPEN_CMD | BD_AUDIO_ALARM_OFF;

   return PASSED;
}

#endif  // SIGMA_BD_CPU
