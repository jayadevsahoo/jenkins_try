#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SortIterC_AlarmUpdate - Iterator for 'SortC_AlarmUpdate'
//---------------------------------------------------------------------
//@ Interface-Description
//  This provides an iterator for a sorted list of data items where
//  each item is of type 'AlarmUpdate'.  Iterators are used to "step" through
//  the elements within a linked list.  The iterator is strictly a
//  constant reference to its list -- no changes to the list can be made
//  with an iterator.  Because of the referencing to a list, it is
//  dangerous to change/destroy the list while this iterator still
//  exists, therefore iterators should be used in very limited scopes.
//  Many iterators may be attached to the same list; iterators may
//  attach to many different lists during its lifetime.
//
//  This iterator can attach to abstract, sorted lists containing
//  copies of data items of type 'AlarmUpdate'.  Any fixed, sorted list of
//  copies of data items of type 'AlarmUpdate' can be used with this iterator,
//  no matter what 'SIZE' parameter is specified.
//---------------------------------------------------------------------
//@ Rationale
//  Iteration through a constant list is often needed; this provides
//  a means of viewing the items in a constant list without changing
//  the list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A constant pointer to the list, and to a particular node in
//  the list are kept within this iterator.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The list that this iterator references should NOT be changed
//  while the iterator refers to that list.  If a change occurs to an
//  iterator's list, the iterator should be re-attached to the list before
//  using the iterator again.  (See 'SortC_AlarmUpdate' for restrictions
//  on its use.)
//---------------------------------------------------------------------
//@ Invariants
//  (pIterList_ != NULL)
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/SortedIterC.C_v   25.0.4.0   19 Nov 2013 14:03:50   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SortIterC_AlarmUpdate.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  search(item)
//
//@ Interface-Description
//  Search this iterator's list for a node equivalent to 'item'.
//  If an equivalence is found, this iterator's current node is set
//  to the equivalent node, and 'SUCCESS' is returned.  If NO
//  equivalence is found, then 'FAILURE' is returned, and the
//  iterator is left unchanged.  The search will NOT loop around
//  either end.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Near the end of this method a pointer to a constant node,
//  'pFoundNode', has its const-ness cast away.  This is due to the
//  fact that 'findNode_()' is a constant method (to allow its use
//  in constant public methods) that returns a pointer.  To ensure
//  consistent const-ness of 'findNode_()', the pointer that is
//  returned is a pointer to a CONSTANT node.  The const-ness of
//  the returned node is not necessary in the design of 'findNode_()',
//  but rather a consistency of design of that method.  Therefore,
//  when this method casts the const-ness of 'pFoundNode' away, it
//  is an acceptable case of working properly with a proper design.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (status == SUCCESS) ==> (currentItem() == item)
//  (status == FAILURE) ==> (pIterCurrent_ == OLD{pIterCurrent_})
//@ End-Method
//=====================================================================

SigmaStatus
SortIterC_AlarmUpdate::search(const AlarmUpdate& item)
{
  CALL_TRACE("search(item)");

  SigmaStatus  status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this iterator's list is not empty, therefore use its list to find
    // a node with an item that is equivalent to 'item'...
    const DNodeC_AlarmUpdate*  pFoundNode = pIterList_->findNode_(item);

    if (pFoundNode->getItem() == item)
    {   // $[TI1.1]
      // the matching node is found, now make sure that 'pFoundNode'
      // points to the first node that is equivalent to 'item', therefore,
      // while not at the beginning of this list, check each previous
      // node for an item that is equivalent to 'item'...
      for (; pFoundNode != getFirst_()  &&
                             item == pFoundNode->getPrevPtr()->getItem();
           pFoundNode = pFoundNode->getPrevPtr())
      {   // $[TI1.1.1] -- at least one iteration...
        // do nothing...
      }   // $[TI1.1.2] -- no iterations...

      // set this iterator's current node to the found node...
      setCurrent_((DNodeC_AlarmUpdate*)pFoundNode);    // "const cast away:"
      status = SUCCESS;
    }   // $[TI1.2] -- no match was found...
  }   // $[TI2] -- this iterator's list is currently empty...

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//	                [static]
//
//@ Interface-Description
//	Report the software fault that occured within the source code
//	of this class.  The fault, indicated by 'softFaultID', occured
//	at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

void
SortIterC_AlarmUpdate::SoftFault(const SoftFaultID softFaultID,
			     const Uint32      lineNumber,
			     const char*       pFileName,
			     const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, SORT_ITER_C,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Friend Functions...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  operator<<(ostr, sortIter)  [Friend Function]
//
// Interface-Description
//  Dump the current state of this iterator's list out to 'ostr'
//  in an ASCII format.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const SortIterC_AlarmUpdate& sortIter)
{
  CALL_TRACE("::operator<<(ostr, sortIter)");

  ostr << "SortIterC_AlarmUpdate {\n";

  if (!sortIter.isEmpty())
  {
    const DNodeC_AlarmUpdate*  pNode = sortIter.getFirst_();

    do
    {
      ostr << ((pNode == sortIter.getCurrent_()) ? "->" : "  ")
	   << pNode->getItem() << '\n';
    } while ((pNode = pNode->getNextPtr()) != sortIter.getFirst_());
  }

  ostr << "};\n";
  ostr.flush();

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
