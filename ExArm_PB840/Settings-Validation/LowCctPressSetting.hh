
#ifndef LowCctPressSetting_HH
#define LowCctPressSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2005, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  LowCctPressSetting - Low circuit pressure setting 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/LowCctPressSetting.hhv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: gdc    Date: 02-Mar-2009    SCR Number: 6160
//	Project:  S840
//	Description:
//  	Enhanced to set this alarm limit OFF when set less than PEEP+5
//		in VC+ per modified SRS requirements.
//
//  Revision: 001  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Initial version.
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
//@ End-Usage


class LowCctPressSetting : public BatchBoundedSetting
{
	public:
		LowCctPressSetting(void); 
		virtual ~LowCctPressSetting(void);

		virtual Applicability::Id  getApplicability( const Notification::ChangeQualifier qualifierId ) const;

		virtual SettingValue  getNewPatientValue(void) const;

		virtual const BoundStatus*  acceptPrimaryChange( const SettingId::SettingIdType primaryId );

		virtual void  acceptTransition(const SettingId::SettingIdType settingId,
									   const DiscreteValue            newValue,
									   const DiscreteValue            currValue);

		static void  SoftFault(const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL);

	protected:
		virtual void updateConstraints_(void);

	private:
		LowCctPressSetting(const LowCctPressSetting&);	// not implemented...
		void  operator=(const LowCctPressSetting&);	// not implemented...
};


#endif // LowCctPressSetting_HH 
