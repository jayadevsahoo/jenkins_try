
#ifndef TestDataPacket_HH
#define TestDataPacket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct : TestDataPacket - <struct that contains all the information about a breath>
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/TestDataPacket.hhv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "SmTestId.hh"
#include "SmDataId.hh"
#include "SmPromptId.hh"
#include "SmStatusId.hh"


// Type: TestDataPacket
// TestDataPacket holds the test data packet with the following information:
// testId: Current test identifier.
// 
struct TestDataPacket {
	SmTestId::ServiceModeTestId testId;
	Int dataCount;
	SmDataId::ItemizedTestDataId startDataIndex;
	Real32 pointData[SmDataId::NUM_ITEMIZED_TEST_DATA_ITEM];
};



// Type: TestPromptPacket
// TestPromptPacket holds the test prompt packet with the following information:
// testId: Current test identifier.
// prompt: The prompt data to be transmitted.
// 
struct TestPromptPacket {
	SmTestId::ServiceModeTestId testId;
	SmPromptId::PromptId prompt;
	SmPromptId::KeysAllowedId keysAllowed;
};


// Type: TestStatusPacket
// TestStatusPacket holds the test status packet with the following information:
// testId: Current test identifier.
// status: The status data to be transmitted.
// 
struct TestStatusPacket {
	SmTestId::ServiceModeTestId testId;
	SmStatusId::TestStatusId status;
	Boolean repeatTestRequested;
	SmStatusId::TestConditionId condition;
};

#endif // TestDataPacket_HH 
