//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2009, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Filename: cxxconde.c - execute static constructors/destructors
//---------------------------------------------------------------------
//@ Version-Information
//  @(#) $Header:   /840/Baseline/Operating-System/vcssrc/cxxconde.c_v   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  27-Jan-2009    SCR Number: 6461 
//  Project:  840S
//  Description:
//	Initial version.
//=====================================================================

//**************************************************************************
// THIS INFORMATION IS PROPRIETARY TO MICROTEC RESEARCH, INC.                                                 
//--------------------------------------------------------------------------
// Copyright (c) 1991-1993 Microtec Research, Inc.							
// All rights reserved                                                      
//**************************************************************************
//
//	@(#)cxxconde.c	1.2 9/18/91												
//
//**************************************************************************
//  _cxxinit:	execute static constructors for C++		
//  _cxxfini:	execute static destructors for C++
//	
//  NOTE:	These routines were written to function in linear address
//		spaces.  If they are to be used on a paged or segmented
//		target, __cxxfini may have to be modified to prevent
//		caused by the possiblity of address wrapping
//
//  HISTORY:	01/24/91 jpc:	written
//		04/12/91 jpc:   fixed dereferencing problem in cxxfini.
//		09/18/91 jpc:   fixed dereferencing problem in _cxxinit
//		02/10/92 atul: Added PIX support for sparc.
//***************************************************************************/

#include "targ_mac.h"
#include "pix.h" // Position independent data macros.

/* Commentary:
	The tweeking of the _cxxinit and _cxxfini function is 
	required for target optimization and possible linker
	bug.
	_cxxinit and _cxxfini function operate on pixinit and
	initfini sections. These sections store addresses of
	function pointers in the 4 bytes.
	The 960 target stores each function pointer in the
	16 byte space, with 12 bytes containing junk value.
	The 960 target is aligned to 16 bytes for optimization
	reasons. (Access of 16 bytes is faster than access of
	4 bytes on 960.)
	Thus, all function pointers in the pixinit and initfini
	section gets aligned to 16 bytes, though only
	4 bytes are used for storing function pointer value.	
	Beside this, there is a linker bug, that is, when 	
	all sections elements in different modules are merged
	by the linker, then the last element is not aligned for
	16 bytes. Therefore, the size of the section is not a
	factor of 16. To solve this problem PIX_INIT_DIFF and
	INIT_FINI_DIFF macros are defined to adjust the size of
	a section for the _cxxinit and _cxxfini routine at run time.

	The relative location of sti and std routines depends on the
	alignment rules of the target.
	STI_DIST and STD_DIST define the distance between
	two sti or std function pointers.
*/

// The size of a word for a generic target.
#define		A_WORD 		(sizeof(void(**)()))  // 4 bytes.

// Defines distance between two consecutive words in a section.
// PIX_INIT_DIFF: The padding needed for adjusting size of pixinit section.
// INIT_FINI_DIFF: The padding needed for adjusting size of initfini section.
// STI_DIST: The distance between two sti function pointers in initfini section.
// STD_DIST: The distance between two std function pointers in initfini section.

#ifndef WORD_SIZE
// If WORD_SIZE is more than 4 bytes then define WORD_SIZE in the
// targ_mac.<target> header file.
	#define      WORD_SIZE       A_WORD
	#define      STI_DIST	     2*WORD_SIZE
	#define      STD_DIST	     2*WORD_SIZE
	#define      PIX_INIT_DIFF    0
	#define      INIT_FINI_DIFF   0
#else
	#define      STI_DIST	     WORD_SIZE
	#define      STD_DIST	     WORD_SIZE
	#define      PIX_INIT_DIFF   (WORD_SIZE - A_WORD) 
	#define      INIT_FINI_DIFF  (WORD_SIZE - 2*A_WORD) 
#endif // WORD_SIZE


typedef void (*FPTR)();

void _main(void) 
{
	void (**funct)();
	void (**functBegin)();
	unsigned long size;

#if _PIC || _PID
	__init_pix_code_data();		// Calculates data and code start.
#endif // *** PIC ***

#if  _PIC || _PID
	size =  GET_SIZE_PIX;
	if ( size )		// Process only if pixinit section exists.
	{
		size += PIX_INIT_DIFF;
		functBegin = (GET_START_ADDR_PIX);
		funct = (void(**)()) ((char *)functBegin+ size );
		while ( funct > functBegin )
		{
			funct = (void(**)()) ((char *) funct - WORD_SIZE);
			if ( PIX_REF(funct) )
			{
				FPTR fptr = (FPTR) (PIX_ADD(PIX_REF(funct))) ;
				(*(fptr)) ();
			}
		}
	}
#endif // _PIC || _PID

	size = (unsigned long) GET_SIZE;
	if ( size )
	{
		size += INIT_FINI_DIFF;

		functBegin = (GET_START_ADDR);
		funct = (void(**)()) ((char *)functBegin + size );

		while ( funct > functBegin )
		{
			funct = (void(**)()) ((char *) funct - (STI_DIST));
			if ( PIX_REF(funct) )
			{
				FPTR fptr = (FPTR) (PIX_ADD(PIX_REF(funct))) ;
				(*(fptr)) ();
			}
		}
	}
}

#ifdef SIGMA_DEVELOPMENT
_cxxfini () 
{
	void (**funct)();
	void (**functEnd)();
	unsigned long size;

	size =  GET_SIZE;
	if ( size )
	{
		size += INIT_FINI_DIFF;

		funct = GET_START_ADDR;

		functEnd = (void(**)()) ((char *) funct + size);
		funct = (void(**)()) ((char *) funct + A_WORD);

		while ( funct < functEnd )
		{
			if ( PIX_REF(funct) )
			{
				FPTR fptr = (FPTR) (PIX_ADD(PIX_REF(funct))) ;
				(*(fptr)) ();
			}
			funct = (void(**)()) ((char *) funct + (STD_DIST));
		}
	}
}
#endif // SIGMA_DEVELOPMENT
