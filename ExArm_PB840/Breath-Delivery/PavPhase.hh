#ifndef PavPhase_HH
#define PavPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2000, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PavPhase - Implements proportional assist ventilation inspiration.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/PavPhase.hhv   10.7   08/17/07 09:40:44   pvcs  
//
//@ Modification-Log
//
//  Revision: 002   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 001   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "CompensationBasedPhase.hh"
		  
//@ Usage-Classes

//@ End-Usage

class PavPhase : public CompensationBasedPhase
{
  public:
    PavPhase( void) ;
    ~PavPhase( void) ;

    inline Real32 getPercentSupport( void) const ;
    inline void reInitPercentSupport( void) ;
    inline Real32 getElasticTarget( void) ;
    inline Real32 getResistiveTarget( void) ;

    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;

    virtual void newBreath( void) ;
    virtual void relinquishControl(  const BreathTrigger& trigger);

	virtual void  activate  (const BreathPhase* pPrevSpontPhase,
							 const BreathPhase* pLostSpontPhase);
	virtual void  deactivate(const Trigger::TriggerId triggerId);

  protected:
	virtual void determineResistivePressure_( const Real32 lungFlow) ;
	virtual void determineElasticPressure_( const Real32 lungVolume) ;
	virtual void updatePercentSupport_( void) ;

	//@ Data-Member: elasticTarget_
	//  elastic target in cmH2O
	Real32 elasticTarget_ ;

	//@ Data-Member: resistiveTarget_
	//  resistive target in cmH2O
	Real32 resistiveTarget_ ;

  private:
    PavPhase( const PavPhase&) ;  			// not implemented...
    void   operator=( const PavPhase&) ; 	// not implemented...

    //@ Data-Member: numPaBreaths_
    // number of PA breaths
    Uint32 numPaBreaths_ ;

} ;

// Inlined methods
#include "PavPhase.in"

#endif // PavPhase_HH 


