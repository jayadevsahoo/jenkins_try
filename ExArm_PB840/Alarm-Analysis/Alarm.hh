#ifndef Alarm_HH
#define Alarm_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================
//
//====================================================================
// Class: Alarm - Alarm-Analysis class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/Alarm.hhv   25.0.4.0   19 Nov 2013 13:51:12   pvcs  $
//
//@ Modification-Log
//
//  Revision 001    By: hct     Date: 01/01/95            DR Number:   n/a
//      Project:    Sigma   (R8027)
//      Description:
//          Initial version (Integration baseline)
//
//   Revision 002   By:   hct      Date: 01/18/96         DR Number:   659
//      Project:   Sigma   (R8027)
//      Description:
//         Use OsTimeStamp for interval timing instead of TimeStamp.
//  
//   Revision 003   By:   gbs      Date: 05/16/96         DR Number:   981
//      Project:   Sigma   (R8027)
//      Description:
//         Fixed syntax warnings generated by new MRI compiler.  These changes
//         do not affect Unit Test.
//  
//   Revision 004   By:   hct      Date: 12/03/96         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.  Don't need to send LOSS_PAT_DATA_MON_EN
//         event to the other CPU so Set and ClearNoPatientDataDisplayStatus
//         don't need the notify parameter.  Move Send/ReceiveEvent to 
//         AlarmManager.cc.
//  
//   Revision 005	By:   hct	   Date: 01/12/98       DR Number: 5001
//      Project:   Sigma   (R8O27)
//      Description:
//		   Added ListActivePDResetAlarmConditions().
//
//   Revision 006	By:   sah	   Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//		   Removed all referenced to newly-obsoleted class ('AlarmMessages').
//  
//   Revision 007   By:   sah      Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//         Removed all references to newly-obsoleted class ('AlarmMessages').
//
//   Revision 008   By:   hct    Date: 07/01/98         DR Number:  5109
//      Project:   Sigma   (R8027)
//      Description:
//         Changed High Circuit Pressure alarm per DCS.
//
//   Revision 009   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 010   By:   yyy      Date: 08/18/99         DR Number:   5513
//      Project:   ATC
//      Description:
//         Modified to handle alarm lock and break through.
// 
//   Revision 011   By:   rhj      Date: 01/16/09        SCR Number:  6452
//      Project:  840S
//      Description:
//         Added setRemedyMessage().
// 
//====================================================================

//@ Usage-Classes
#    include "Sigma.hh"

#include "Alarm_Analysis.hh"
#include "AlarmAugmentationList.hh"
#include "AlarmList.hh"
#include "IpcIds.hh"
#include "MsgQueue.hh"
#include "OperandName.hh"
#include "OperatingParamEventName.hh"
#include "OsTimeStamp.hh"
#include "MessageNameArray.hh"
#include "TimeStamp.hh"
#include "XmitData.hh"
#include "AlarmQueueMsg.hh"

class AlarmAnalysisInformation;
class DependentInformation;
class OperatingCondition;
class OperatingGroup;
class OperandNameList;

//@ End-Usage




class Alarm 
{
  public:
   
    // Enumerator used to identify the nature of an AlarmUpdate object.
    enum UpdateType {
       UPDATE_TYPE_NULL = -1,
       AUTO_RESET_UT,
       PAT_DATA_RESET_UT, 
       ALARM_SILENCE_UT, 
       END_ALARM_SILENCE_UT, 
       AUGMENTATION_UT, 
       DETECTION_UT, 
       MANUAL_RESET_UT,
       INDEPENDENT_UT,
       END_ALARM_SILENCE_HIGH_URGENCY_UT,
       MAX_NUM_UPDATE_TYPE};

    // Urgency enumerator.
    enum Urgency {URGENCY_TYPE_NULL = -1, NORMAL_URGENCY, LOW_URGENCY, 
       MEDIUM_URGENCY, HIGH_URGENCY, MAX_NUM_URGENCIES};


 
    Alarm(const AlarmName n,
          AlarmAnalysisInformation& rInfo,
          OperatingGroup& rCausedBy,
          OperandName resetConditionName,
          Boolean patDataAlarm,
          MessageName base,
          Boolean breakThroughSilence = FALSE);

    Alarm(const AlarmName n,
          AlarmAnalysisInformation& rInfo,
          OperatingGroup& rCausedBy,
          OperandName resetConditionName,
          OperandName resetConditionName2,
          Boolean patDataAlarm,
          MessageName base,
          Boolean breakThroughSilence = FALSE);

    ~Alarm();

    Boolean operator==(const Alarm& rAlarm) const;
    Boolean operator!=(const Alarm& rAlarm) const;
    Boolean operator< (const Alarm& rAlarm) const;
    Boolean operator> (const Alarm& rAlarm) const;
    
    static void Initialize(void);
    static void InitMemory(void);
    static void InitData(void);
    static void AddAlarm(Alarm* pAlarm);
    static void ListManResetAlarmConditions(OperandNameList& rOperandNameList);
    static void ListPDResetAlarmConditions (OperandNameList& rOperandNameList);
    static void ListActivePDResetAlarmConditions (OperandNameList& 
        rOperandNameList);


    Boolean evaluateUpdates(void);
    void    evaluateConditionAugs(void);
    void    evaluateDependencies(void);
    void    evaluateDependentAugs(void);
    void    addAugmentation(AlarmAugmentation& rAugmentation);
    void    setRemedyMessage(MessageName remedyMessageName);


    static void                 SetNoPatientDataDisplayStatus(void);
    static void                 ClearNoPatientDataDisplayStatus(void);

    //@ Inline Methods
    inline static void          AppendTaskQueueEvent(Int32 msg);
    inline static void          AppendTaskQueueEvent(
                                    OperatingParameterEventName name);

    inline static void          ClearUpdateType(void);
    inline static void          SetUpdateType(Alarm::UpdateType updateType);
    inline static UpdateType    GetUpdateType(void);
    inline static               AlarmList GetAlarms(void);

    inline static Boolean       GetPatientDataReset(void);
    inline static void          SetPatientDataReset(void);
    inline static void          ClearPatientDataReset(void);

    inline static Boolean       GetNoPatientDataDisplayStatus(void);

    inline static Boolean       GetNoGuiSettingChangesStatus(void);
    inline static void          SetNoGuiSettingChangesStatus(void);
    inline static void          ClearNoGuiSettingChangesStatus(void);

    inline static Boolean       GetVentInopLedOnStatus(void);
    inline static void          SetVentInopLedOnStatus(void);
    inline static void          ClearVentInopLedOnStatus(void);

    inline static Boolean       GetCancelScreenLockStatus(void);
    inline static void          SetCancelScreenLockStatus(void);
    inline static void          ClearCancelScreenLockStatus(void);


    static void          	SoftFault(const SoftFaultID  softFaultID,
                                          const Uint32       lineNumber,
                                          const char*        pFileName = NULL,
                                          const char*        pPredicate = NULL);


    inline void                    clearUpdateType(void);
    inline Alarm::UpdateType       getUpdateType(void)     const;
    inline void                    clearUpdates(void);
    inline void                    setCauseUpdate(
                                       const TimeStamp& rWhenOccurred,
                                       const OsTimeStamp& rIntervalTime);
    inline Boolean                 getCauseUpdate(void) const;
    inline void                    setAugmentUpdate(
                                       const TimeStamp& rWhenAugmented);
    inline Boolean                 getAugmentUpdate(void)  const;
    inline Boolean                 getIsActive(void)       const;
    inline Boolean                 getIsDependent(void)    const;
    inline OperatingGroup&         getCausedBy(void)       const;
    inline const   TimeStamp&      getWhenOccurred(void)   const;
    inline const   OsTimeStamp&    getIntervalTime(void)   const;
    inline const   TimeStamp&      getWhenAugmented(void)  const;
    inline AlarmName               getName(void)           const;
    inline MessageName             getBaseMessage(void)    const;
    inline MessageName             getAnalysisMessage(Uint32 index) const;
    inline MessageName             getRemedyMessage(Uint32 index)   const;
    inline Uint32                  getAnalysisIndex(void)           const;
    inline Uint32                  getRemedyIndex(void)             const;
    inline MessageNameArray        getCurrentAnalysisMessage(void)  const;
    inline MessageNameArray        getCurrentRemedyMessage(void)    const;
    inline Int16                   getCurrentPriority(void)         const;
    inline Alarm::Urgency          getCurrentUrgency(void)          const;
    inline Alarm::Urgency          getPreviousUrgency(void)         const;
    inline Boolean                 getBreakThroughSilence(void)	    const;

#ifdef SIGMA_UNIT_TEST
    inline Boolean                 getPatientDataAlarm(void)        const;
    inline OperandName             getResetConditionName(void)       const;
#endif // SIGMA_UNIT_TEST

  protected:

  private:
    Alarm();                      // Declared only
    Alarm(const Alarm&);          // Declared only
    void operator=(const Alarm&); // Declared only

    void getInfo_(const AlarmAnalysisInformation& rInfo, 
                  const Alarm::Urgency dependentUrgency);

    void updateRanking_(const AlarmAnalysisInformation& rInfo,
                       const Alarm::Urgency dependentUrgency);
                       
    void initializeMessages_(void);
    void initializeAlarm_(void);
    void setSuperiorAugmentUpdate_(const TimeStamp& rWhenAugmented);
    void setIndirectAugmentUpdate_(const TimeStamp& rWhenAugmented);


    //@ Inline Methods
    inline void setAugmentUpdate_(const TimeStamp& rWhenAugmented);
    inline void setUpdateType_(Alarm::UpdateType updateType);

    //@ Data-Member:    RAlarms_
    // List of references to all Alarm objects.
    static AlarmList& RAlarms_;

    //@ Data-Member:    PatientDataReset_
    // When an Alarm causing Loss of Patient Data Monitoring autoresets,
    // its Reset System Response sets this class attribute to TRUE.  This 
    // attribute is used to reset all alarms (to inactive) which depend on 
    // patient data.
    static Boolean PatientDataReset_;

    //@ Data-Member:    CancelScreenLockStatus_
    // This Boolean attribute is set whenever an Alarm changes state from active
    // to inactive.  It is used by GUI-Applications to cancel screen lock
    // whenever a new alarm occurs.  It is reset at the beginning of each cycle
    // of alarm processing.
    static Boolean CancelScreenLockStatus_;

    //@ Data-Member:    NoPatientDataDisplayStatus_
    // This attribute is a counting semaphore. When an alarm condition which 
    // causes Loss of Patient Data Monitoring is detected, this attribute is
    // incremented.  When that alarm condition resets, this attribute is 
    // decremented.  When alarm processing is finished, the GUI task is 
    // notified and checks this attribute to stop, start or continue the 
    // displays based on patient data.
    static Int32 NoPatientDataDisplayStatus_;

    //@ Data-Member:    NoGuiSettingChangesStatus_
    // This attribute is a counting semaphore. When an alarm condition which 
    // requires that GUI setting changes not be allowed, this attribute is
    // incremented.  When that alarm condition resets, this attribute is 
    // decremented.  When alarm processing is finished, the GUI task is 
    // notified and checks this attribute to stop, start or continue GUI 
    // setting changes.
    static Int32 NoGuiSettingChangesStatus_;

    //@ Data-Member:    VentInopLedOnStatus_
    // This attribute is a counting semaphore. When an alarm condition which 
    // requires that the Vent Inop Status LED be illuminated, this attribute is
    // incremented.  When that alarm condition resets, this attribute is 
    // decremented.  When alarm processing is finished, the GUI task is 
    // notified and checks this attribute to stop, start or continue the 
    // illumination of the Vent Inop Status LED.
    static Int32 VentInopLedOnStatus_;

    //@ Data-Member:    UpdateType_
    // Class variable used in determining the reason for the reevaluation of
    // an Alarm.
    static Alarm::UpdateType UpdateType_;

    //@ Data-Member:    updateType_
    // Instance variable used in determining the reason for the reevaluation
    // of an Alarm.
    Alarm::UpdateType   updateType_;

    //@ Data-Member:    AlarmName name_
    // Enumerator uniquely identifying an alarm which is used as an index into 
    // the RAlarms_ array.
    const               AlarmName name_;

    //@ Data-Member:    isActive_
    // State of the alarm.
    Boolean             isActive_;

    //@ Data-Member:    whenOccurred_
    // When the alarm changed state.
    TimeStamp           whenOccurred_;

    //@ Data-Member:    intervalTime_
    // When the alarm changed state (using interval timer).
    OsTimeStamp         intervalTime_;

    //@ Data-Member:    whenAugmented_
    // Time of the most recent augmentation to the Alarm.
    TimeStamp           whenAugmented_;

    //@ Data-Member:    currentUrgency_
    // Current urgency of the Alarm.
    Alarm::Urgency      currentUrgency_;

    //@ Data-Member:    previousUrgency_
    // The urgency of the Alarm on the previous cycle of alarm processing.
    Alarm::Urgency      previousUrgency_;

    //@ Data-Member:    currentPriority_
    // Current priority of the Alarm.
    Int16               currentPriority_;

    //@ Data-Member:    currentAnalysisMessage_
    // Array containing the message enumerators for the analysis messages.
    MessageNameArray    currentAnalysisMessage_;

    //@ Data-Member:    currentRemedyMessage_
    // Array containing the message enumerators for the remedy messages.
    MessageNameArray    currentRemedyMessage_;

    //@ Data-Member:    isDependent_
    // Is this alarm currently being annunciated as a dependent augmentation
    // to a superior alarm?
    Boolean             isDependent_;

    //@ Data-Member:    causeUpdate_
    // When an OperatingGroup which causes an Alarm changes state, it messages
    // the Alarm to set this attribute.
    Boolean             causeUpdate_;

    //@ Data-Member:    augmentUpdate_
    // When an OperatingGroup which augments an Alarm changes state or an
    // alarm which is dependent to or superior to another alarm changes state,
    // it messages this alarm to set the augmentUpdate_ attribute.
    Boolean             augmentUpdate_;

    //@ Data-Member:    patientDataAlarm_
    // Attribute patientDataAlarm_ is used to identify those Alarm objects
    // marked as Patient Data Alarms in the Alarm Matrices.  Patient data
    // alarms are those alarms which depend on patient data for their
    // detection.  These alarms are not annunciated while an alarm causing
    // Loss of Patient Data Monitoring is active and are reset when an alarm
    // causing Loss of Patient Data Monitoring autoresets.
    Boolean             patientDataAlarm_;

    //@ Data-Member:    baseMessage_
    // This is the enumerator which identifies the text associated with the
    // Alarm Name/Base Message column of the Alarm Matrices.
    MessageName         baseMessage_;

    //@ Data-Member:    rInfo_
    // Attribute rInfo_ is a reference to the object which holds the
    // enumerators identifying the analysis message and remedy message
    // associated with the detection criteria row in the Alarm Matrices.
    AlarmAnalysisInformation& rInfo_;

    //@ Data-Member:    augmentations_
    // This is a list of the dependent and condition augmentations associated
    // with this alarm.
    AlarmAugmentationList augmentations_;

    //@ Data-Member:    rCausedBy_
    // Attribute rCausedBy is a reference to the OperatingGroup which models
    // the detection criteria for this alarm.
    OperatingGroup&     rCausedBy_;

    //@ Data-Member:    resetConditionName_
    // Every Alarm can be thought of as being caused by a core set of operating
    // parameters modeled by OperatingConditions.  There may be other
    // OperatingCondition objects in the detection criteria, but if the core
    // set is not TRUE, the alarm will be inactive.  For example, for the
    // NO AIR SUPPLY alarm to be active, the OperatingCondition modeling Wall
    // Air Present must evaluate to FALSE.  If the Boolean value of this
    // OperatingCondition is negated, the NO AIR SUPPLY alarm will be reset.
    // The resetConditionName_ is used to negate the main OperatingCondition
    // during Manual Reset and Patient Data Reset.
    OperandName resetConditionName_;

    //@ Data-Member:    resetConditionName2_
    // Every Alarm can be thought of as being caused by a core set of operating
    // parameters modeled by OperatingConditions.  There may be other
    // OperatingCondition objects in the detection criteria, but if the core
    // set is not TRUE, the alarm will be inactive.  For example, for the
    // NO AIR SUPPLY alarm to be active, the OperatingCondition modeling Wall
    // Air Present must evaluate to FALSE.  If the Boolean value of this
    // OperatingCondition is negated, the NO AIR SUPPLY alarm will be reset.
    // The resetConditionName_ is used to negate the main OperatingCondition
    // during Manual Reset and Patient Data Reset.
    OperandName resetConditionName2_;

    //@ Data-Member:    analysisIndex_
    // Attribute analysisIndex_ tracks the number of analysis messages in the
    // currentAnalysisMessage_ array.
    Uint32              analysisIndex_;

    //@ Data-Member:    remedyIndex_
    // Attribute remedyIndex_ tracks the number of remedy messages in the
    // currentRemedyMessage_ array.
    Uint32              remedyIndex_;    

    //@ Data-Member:    breakThroughSilence_
    // Attribute breakThroughSilence_ allows an alarm to break through
    // Alarm Silence.
    Boolean             breakThroughSilence_;    


};

    
// Inlined methods
#include "Alarm.in"

#endif // Alarm_HH 
