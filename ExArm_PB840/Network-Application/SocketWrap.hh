
#ifndef	SocketWrap_HH
#define	SocketWrap_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2014, Covidien/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: SocketWrap - Class definition for socket APIs wrappers to provide
//				flexible OS porting and to keep application calls OS independent
//======================================================================

#include "SigmaTypes.hh"

class SocketWrap
{
public:
	//TODO E600 VM: The constructor is made public because there is an intention
	//in the future to inherit from this class an OS specific SocketWrap (Ex. WinSoketWrap,
	//LinuxSocketWrap. etc) and the wrapper APIs in the inherited class are to be ported
	//to that OS. The user apps then instantiate the necessary socket warp 
	//( WinSocketWrap sock; sock.foo(); etc. )
	SocketWrap() {}
	~SocketWrap() {}

	//TODO E600 VM: socket wrapers to be ported to the OS in use and to provide API for 
	//socket and network app. If SocketWrap becomes a base class, make then virtual,
	//the inhertited OS-specific socket wrap is to overwite them to use the corresponding OS calls.
	Uint32 HTONL(Uint32 num);
	Uint16 HTONS(Uint16 num);
	Uint32 NTOHL(Uint32 num);
	Uint16 NTOHS(Uint16 num);
	//add wrapper APIs for other socket calls..

	//This function handles an htonl on float numbers which does not have
	//an API in OS sockets
	Real32 HTONF(Real32 num);
	//This function handles an ntohl on float numbers which does not have
	//an API in OS sockets
	Real32 NTOHF(Real32 num);

private:
	SocketWrap(const SocketWrap&);			// not implemented...
    void   operator=(const SocketWrap&);	// not implemented...

};


#endif //SocketWrap_HH