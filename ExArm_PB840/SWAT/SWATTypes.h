////////////////Copyright (c) 2014 Covidien/////////////////////////////////////
/*******************************************************************************
Bitbucket.org/covidien/e600.git
Name:		SWATTypes.h

Purpose:
	Define the enum data types / error codes for SWAT subsystem.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 06-MAR-2014
PROJECT: PB880 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/

#pragma once

enum SWATStatus
{
	SWAT_SUCCESS				=	0,		// SUCCESS
	SWAT_ERROR					=	1,		// COMMON ERROR
	SWAT_OBJECT_ID_NOT_FOUND	=	2,		// OBJECT ID NOT FOUND
	SWAT_HANDLER_UNDEFINED		=	8,		// SWAT Handler is not defined
    SWAT_COMMAND_SYNTAX_ERROR   =   9,      // SWAT command syntax error
    SWAT_COMMAND_EXECUTE_ERROR  =   10,     
    SWAT_SERVER_NOT_IMPLEMENTED =   11,
    SWAT_CAPTURE_SCREEN_FAILED  =   12,
    SWAT_SS_INDEX_OUT_OF_RANGE  =   13,
    SWAT_SS_ALREADY_EXISTS      =   14,
    SWAT_SS_INIT_ERROR          =   15,
    SWAT_SS_CLEAR_FAILED        =   16,
};

enum SWATActivationLevel
{
    AL_NOT_DISPLAYED            =   0,      // not displayed
    AL_DISPLAYED_NOT_INTERACT   =   1,      // displayed but not interactive
    AL_DISPLAYED_AND_INTERACT   =   2       // displayed and interactive
};

enum SWATSelectionLevel
{
    SL_UP_OFF                   =   0,      // false/up/off/not available
    SL_DOWN_ON                  =   1       // true/down/on
};

#define     SWAT_MAX_MSG_LENGTH	    4096    // SWAT maximum message length