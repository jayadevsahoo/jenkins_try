#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LaptopEventTarget - A target to handle Laptop Request events.
// Classes can specify this class as an additional parent class and register
// to be informed of Laptop Request events.
//---------------------------------------------------------------------
//@ Interface-Description
// Any classes which need to be informed of Laptop Request events (where 
// these requests are communicated to interested objects) need to multiply
// inherit from this class.  Then, when they register for request events with
// the LaptopEventRegistrar class, they will be informed of events via the
// LaptopRequestHappened() or LaptopFailureHappened() method.
//---------------------------------------------------------------------
//@ Rationale
// Used to implement the callback mechanism needed to communicate
// Laptop Request events to objects.
//---------------------------------------------------------------------
//@ Implementation-Description
// If classes need to be informed of Laptop Request events they must do 3
// things: a) multiply inherit from this class, b) define the
// laptopRequestHappened() or laptopFailureHappened()() method of this class and c) register for change
// notices via the LaptopEventRegistrar class.  They will then be informed of
// laptop events via the laptopRequestHappened() or laptopFailureHappened() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// This class should not be instantiated directly.  This class has use only
// when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LaptopEventTarget.ccv   25.0.4.0   19 Nov 2013 14:08:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//	     Integration baseline.
//=====================================================================

#include "LaptopEventTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: laptopFailureHappened
//
//@ Interface-Description
// This method should be redefined in derived classes.  It is via this method
// that Laptop Request events are communicated.  It is passed two parameters,
// "serialFailureCodes", which can be SERIAL_FAILURE_TOUT, or SERIAL_FAILURE_NAK.
// "eventId", which can any one enum in LaptopEventID.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopEventTarget::laptopFailureHappened(SerialInterface::SerialFailureCodes
								serialFailureCodes, Uint8 eventId)
{
	CALL_TRACE("LaptopEventTarget::laptopFailureHappened("
					"SerialInterface::SerialFailureCodes serialFailureCodes,"
					"Uint8 eventId)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: laptopRequestHappened
//
//@ Interface-Description
// This method should be redefined in derived classes.  It is via this method
// that Laptop Request events are communicated.  It is passed two parameters,
// "event Id", which identifies which laptop event was requested.
// "pMsgData Id", which contains the laptop message.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopEventTarget::laptopRequestHappened(LaptopEventId::LTEventId eventId, Uint8 *pMsgData)
{
	CALL_TRACE("LaptopEventTarget::laptopRequestHappened("
					"LaptopEventId::LTEventId eventId, Uint8 *pMsgData)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LaptopEventTarget()  [Default Constructor, Protected]
//
//@ Interface-Description
// The constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LaptopEventTarget::LaptopEventTarget(void)
{
	CALL_TRACE("LaptopEventTarget::LaptopEventTarget(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LaptopEventTarget  [Destructor, Protected]
//
//@ Interface-Description
// Nothing to destruct!
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LaptopEventTarget::~LaptopEventTarget(void)
{
	CALL_TRACE("LaptopEventTarget::~LaptopEventTarget(void)");

	// Do nothing
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LaptopEventTarget::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("LaptopEventTarget::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LAPTOPEVENTTARGET,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
