// BPServer.h: interface for the BootPServer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BPSERVER_H__607BB369_026F_11D1_9F84_00805FCAC1CC__INCLUDED_)
#define AFX_BPSERVER_H__607BB369_026F_11D1_9F84_00805FCAC1CC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

//********* header files required ************/
#include "vtUtils.h"
#include "softdapi.h"
#include "BPSocket.h"
#include "TfSocket.h"
#include "TpSocket.h"

// forward declaration

class BootPSocket;
class TftpSocket;
class TftpClientSocket;

// end forward declaration

class BootPServer  
{
public:
	void setImageId(char *income, int *id);
	int bdImageId;
	int guiImageId;

	BootPServer();
	~BootPServer();
	void SetBdFileName(char *tmp, int imageId);
	void SetGuiFileName(char *tmp, int imageId);
	void SetFilePath(char *tmp);
	void SetAltAddress(char *tmp);
	void AcceptTftpConnection();
	void ErrorControl(int errorNum);
	int  GoLive();
	void ProcessMsg();
	int  GetLastError();
	void EndSession(void);
	char * FilePath();

	// ********* callback from user of this dll *****
	void SetBootPCallback(void (*callback)(int));
	void (*bootPCallback)(int status);

	void SetTftpCallback(void (*callback)(int, int, int));
	void (*tftpCallback)(int id, int cblock, int tblock);
    //************************************************
private:
	// Attributes
	int socketReady;
	int lastError;
	int active;
	int readyForMsg;
	int multiHomed;
	CString serverAddress;
	CString serverBroadcastAddress;
	CString bdAddress;
	CString guiAddress;
	int nextTftpClient;
	TftpClientSocket* connectList[MAX_TFTP_CLIENTS];
	TftpSocket *tftpSocket;
	BootPSocket *incomingSocket;
	char bdFileName[256];
	char guiFileName[256];
	char filePath[256];
	char altAddress[25];

	// operations
	int ReadyToRecieve(void);
	void PrepSockets(void);
	void PrepWinSockEnviroment(void);
};

#endif // !defined(AFX_BPSERVER_H__607BB369_026F_11D1_9F84_00805FCAC1CC__INCLUDED_)
