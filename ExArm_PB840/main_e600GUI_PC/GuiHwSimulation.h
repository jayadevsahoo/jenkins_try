#ifdef SIGMA_GUIPC_CPU
//------------------------------------------------------------------------------
//                   Copyright (c) 2014 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Filename: GuiHwSimulation.h - Header File which handles GUI IO HW Simulations
//---------------------------------------------------------------------

#pragma once

#include "Keys.hh"
#include "UserAnnunciationMsg.hh"
#include "VgaBlink.hh"
#include "IOExpanderXRA1201.h"

class GuiHwSimulationFrm : public CFrameWnd
{
	
public:
	GuiHwSimulationFrm();
protected: 
	DECLARE_DYNAMIC(GuiHwSimulationFrm)

// Attributes
public:
	CSplitterWnd      m_ObjSplitterUper_Lower; 
	BOOL			  m_bSplitterCreated; 
// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~GuiHwSimulationFrm();
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
		virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	



protected:  // control bar embedded members

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()

	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
};

/////////////////////////////////////////////////////////////////////////////
// CMyButton window

class CMyButton : public CButton
{
// Construction
public:
	//This Enum represents the number of buttons supported by the simulated GUI IO
	enum GuiHwSimulatedKey
	{
		GUI_KEY1	=  0,	// 
		GUI_KEY2,		// 
		GUI_KEY3,		//	.
		GUI_KEY4,		//	.
		GUI_KEY5,		//	.
		GUI_KEY6,		//	.
		GUI_KEY7,		//	.
		GUI_KEY8,		//	.
		GUI_KEY9,		//	.
		GUI_KEY10,		//	.
		GUI_KEY11,		//	.
		GUI_KEY12,		//	.
		GUI_KEY13,		//	.
		GUI_KEY14,		// 
		GUI_KEY15,		// 
		GUI_KEY16,		// 
		GUI_KEY17,		// 

		NUMBER_OF_GUI_SIMULATED_KEYS
	};

	CMyButton();

virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
// Attributes
private:

// Operations
public:

// Implementation
public:
	virtual ~CMyButton();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMyButton)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	void PostKnob(void);
	void PostKey(UserAnnunciationMsg::KeyAction action, GuiKeys key);
};

//////////////////////////////////////////////////////////////////////////////////////////////////

class GuiHwSimulationView : public CView
{
	DECLARE_DYNCREATE(GuiHwSimulationView)
private:
	CMyButton	m_Button[CMyButton::GuiHwSimulatedKey::NUMBER_OF_GUI_SIMULATED_KEYS];

	void static SetYellowLed(LedStatus_t State);
	void static SetRedLed(LedStatus_t State);

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	void OnInitialUpdate(); // called first time after construct
	void static SetAlarmLedColor(VgaBlink::AlarmLedColors LedColor);

protected:
	GuiHwSimulationView();           // protected constructor used by dynamic creation
	virtual ~GuiHwSimulationView();


protected:
	DECLARE_MESSAGE_MAP()
public:
};

//////////////////////////////////////////////////////////////////////////////////////////////////

#endif





