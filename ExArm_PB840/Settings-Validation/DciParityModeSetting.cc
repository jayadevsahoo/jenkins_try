#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  DciParityModeSetting - DCI Parity Mode Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the type of parity,
//  if any, used by the ventilator's serial Digital Communication Interface
//  (DCI).  This class inherits from 'BatchDiscreteSetting' and provides the
//  specific information needed for the representation of DCI's parity mode.
//  This information includes the set of values that this setting can have
//  (see 'DciParityModeValue.hh'), and this batch setting's new-patient
//  value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DciParityModeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: hct    Date: 03-DEC-1999    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 006   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Incorporated initial specifications for NeoMode Project. Also,
//      eliminated use of unneeded 'INVALID_DISCRETE_VALUE' bound state.
//
//  Revision: 005   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  25-Sep-1997    DR Number: 1825
//  Project: Sigma (R8027)
//  Description:
//	Added logic to this setting to disallow a value of "ODD" and "EVEN",
//	when data bits is set to 8 bits.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//      Inital version.
//
//=====================================================================

#include "DciParityModeSetting.hh"
#include "DciParityModeValue.hh"
#include "DciDataBitsValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  DciParityModeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01191] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DciParityModeSetting::DciParityModeSetting(SettingId::SettingIdType settingId)
	     : BatchDiscreteSetting(settingId, Setting::NULL_DEPENDENT_ARRAY_,
				    DciParityModeValue::TOTAL_PARITY_MODES),
             settingId_(settingId)
{
  CALL_TRACE("DciParityModeSetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~DciParityModeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DciParityModeSetting::~DciParityModeSetting(void)
{
  CALL_TRACE("~DciParityModeSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  DCI parity mode is ALWAYS changeable.
//
//  $[01191] -- communication settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
DciParityModeSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
DciParityModeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  const AcceptedContext&  rAcceptedContext = ContextMgr::GetAcceptedContext();

  DiscreteValue  newPatientValue;

  if (rAcceptedContext.areBatchValuesInitialized())
  {   // $[TI1]
    // when the Accepted Context has previosly-initialized values, use the
    // previous value of this setting...
    newPatientValue = rAcceptedContext.getBatchDiscreteValue(settingId_);
  }
  else
  {   // $[TI2]
    // $[01191] The setting's new-patient value ...
    newPatientValue = DciParityModeValue::NO_PARITY_VALUE;
  }

  return(newPatientValue);
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
DciParityModeSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean isValid;

  // is the accepted DCI parity value within its range?...
  isValid = DiscreteSetting::isAcceptedValid(); // forward to base class...

  if (isValid  &&
      DiscreteValue(getAcceptedValue()) != DciParityModeValue::NO_PARITY_VALUE)
  {
    const Setting*  pDciDataBits =
	      SettingsMgr::GetSettingPtr(SettingId::DCI_DATA_BITS);

    // even/odd parity is only valid when the data bits is NOT 8-bit mode...
    isValid = (DiscreteValue(pDciDataBits->getAcceptedValue()) !=
					     DciDataBitsValue::BITS_8_VALUE);
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
DciParityModeSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean isValid;

  // is the adjusted DCI data bits value within its range?...
  isValid = DiscreteSetting::isAdjustedValid(); // forward to base class...

  if (isValid  &&
      DiscreteValue(getAdjustedValue()) != DciParityModeValue::NO_PARITY_VALUE)
  {
    const Setting*  pDciDataBits =
			SettingsMgr::GetSettingPtr(SettingId::DCI_DATA_BITS);

    // even/odd parity is only valid when the data bits is NOT 8-bit mode...
    isValid = (DiscreteValue(pDciDataBits->getAdjustedValue()) !=
					     DciDataBitsValue::BITS_8_VALUE);
  }

  return(isValid);
}

#endif  // defined(SIGMA_DEVELOPMENT)



//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DciParityModeSetting::SoftFault(const SoftFaultID  softFaultID,
			        const Uint32       lineNumber,
			        const char*        pFileName,
			        const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          DCI_PARITY_MODE_SETTING, lineNumber, pFileName,
                          pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcDependentValues_(isValueIncreasing)  [virtual]
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
DciParityModeSetting::calcDependentValues_(const Boolean)
{
  CALL_TRACE("calcDependentValues_(isValueIncreasing)");

  const DiscreteValue  DCI_PARITY_MODE_VALUE = getAdjustedValue();
  Setting*  pDciDataBits = NULL;
  DiscreteValue  DCI_DATA_BITS_VALUE;

  switch (settingId_)
  {
    case SettingId::COM1_PARITY_MODE:
      pDciDataBits = SettingsMgr::GetSettingPtr(SettingId::COM1_DATA_BITS);
      DCI_DATA_BITS_VALUE = pDciDataBits->getAdjustedValue();
      break;
    case SettingId::COM2_PARITY_MODE:
      pDciDataBits = SettingsMgr::GetSettingPtr(SettingId::COM2_DATA_BITS);
      DCI_DATA_BITS_VALUE = pDciDataBits->getAdjustedValue();
      break;
    case SettingId::COM3_PARITY_MODE:
      pDciDataBits = SettingsMgr::GetSettingPtr(SettingId::COM3_DATA_BITS);
      DCI_DATA_BITS_VALUE = pDciDataBits->getAdjustedValue();
      break;
    default:
      CLASS_ASSERTION_FAILURE();
  }    

  if (DCI_PARITY_MODE_VALUE != DciParityModeValue::NO_PARITY_VALUE  &&
      DCI_DATA_BITS_VALUE    == DciDataBitsValue::BITS_8_VALUE)
  {   // $[TI1] -- invalid combination of parity and data bits...
    // set this setting's bound status up to indicate that this setting has
    // violated its bound with this "knob delta"; it doesn't matter whether
    // an "upper" or "lower" hard bound state is returned...
    getBoundStatus_().setBoundStatus(::UPPER_HARD_BOUND,
    				     ::DCI_PARITY_MODE_BASED_BITS_ID);
    getBoundStatus_().setViolationId(getId());

    // NOTE:  by setting bound status to an "invalid" state, the base
    //        class's 'calcNewValue()' method will take care of attempting
    //        the "next" value...
  }   // $[TI2] -- the combination of parity with new data bits is valid...

  // For discrete settings, we are always able to settle on a satisfactory value
  return TRUE; 
}
