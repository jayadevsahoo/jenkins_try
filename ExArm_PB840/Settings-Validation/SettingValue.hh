
#ifndef SettingValue_HH
#define SettingValue_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SettingValue - Union of the possible values for a setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingValue.hhv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6066
//  Project:  840S
//  Description:
//		Corrected implementation and use of "union" object.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

//@ Usage-Classes
#include "BoundedValue.hh"
#include "DiscreteValue.hh"
#include "SequentialValue.hh"
//@ End-Usage


class SettingValue
{
	public:

		inline SettingValue(const SettingValue&   settingValue);
		inline SettingValue(const BoundedValue&   initBoundedValue);
		inline SettingValue(const DiscreteValue   initDiscreteValue);
		inline SettingValue(const SequentialValue initSequentialValue);
		SettingValue(void);
		~SettingValue(void);

		// return r-value sub-instances of this instance...
		inline operator  const BoundedValue&   (void) const;
		inline operator  const DiscreteValue&  (void) const;
		inline operator  const SequentialValue&(void) const;

		// return l-value sub-instances of this instance...
		inline operator  BoundedValue&   (void);
		inline operator  DiscreteValue&  (void);
		inline operator  SequentialValue&(void);

		inline void  operator=(const SettingValue& settingValue);

	private:
		// named union...
		union MixData
		{
			//@ Data-Member:  boundedValue_
			// A bounded setting value.
			BoundedValue  boundedValue_;

			//@ Data-Member:  discreteValue_
			// A discrete setting value.
			DiscreteValue  discreteValue_;

			//@ Data-Member:  sequentialValue_
			// A sequential setting value.
			SequentialValue  sequentialValue_;
		};

		//@ Data-Member:  mixedData_
		// Contains all data types supported as a SettingValue in MixData union
		MixData  mixedData_;
};


// Inlined methods
#include "SettingValue.in"


#endif // SettingValue_HH 
