#include "stdafx.h"
#if defined( SIGMA_GUI_CPU )

//=============================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=============================================================================

//============================ C L A S S   D E S C R I P T I O N ==============
//@ Class: BackgroundGuiTests - Individual background tests for the GUI CPU.
//-----------------------------------------------------------------------------
//@ Interface-Description
//  This class consists of static methods that perform the cyclic background
//  tests on the GUI.  These methods are not intended to be called directly,
//  rather they are intended to be attached to a BackgroundCycleTimer
//  object that is assigned to each test in BackgroundCycleApp::Initialize().
//-----------------------------------------------------------------------------
//@ Rationale
//  The class implements the Background test algorithms for the GUI.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  The methods in this class organize all the tests of the GUI CPU in a single
//  file for maintenance purposes.
//
//  Each one of the GUI Background test in this class colaborates with a
//  BackgroundCycleTimer object.  The BackgroundCycleTimer object is responsible
//  for maintaining the schedule of each test and to report an error condition
//  when one is detected.  The associate of a BackgroundCycleTimer object and
//  a GUI test in this class is defined in BackgroundCycleApp::Initialize().
//  When a BackgroundCycleTimer object is created it is passed a pointer to one
//  of the static methods of this class.
//  All the methods in the class has the same signature.
//-----------------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//-----------------------------------------------------------------------------
//@ Restrictions
//  None
//-----------------------------------------------------------------------------
//@ Invariants
//  None
//-----------------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundGuiTests.ccv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 019  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 018  By: gdc    Date:  02-Oct-2001    DR Number: 5969
//  Project:  GuiComms
//  Description:
//		Modified so touchframe reset only performed on status 
//      request cycle and status request command reissued after
//      a timeout.
//		
//  Revision: 017  By: gdc    Date:  04-Jan-2001    DR Number: 5493
//  Project:  GuiComms
//  Description:
//      Modified for new TouchDriver interface.
//		
//  Revision: 016  By: gdc    Date:  18-Nov-1999    DR Number: 5585
//  Project:  ATC
//  Description:
//      Using "two strikes" rule when a failure is reported from the 
//		touch panel. The first failure reported from the touch panel
//		simply resets the touch panel. Two failures on sequential cycles
//		of the touch panel background test will cause a device alert.
//		Error detection and notification is within the the two minute
//		detection requirement. The blocked beam detection and alarm
//		processing unchanged.
//
//  Revision: 015  By: gdc    Date:  19-Apr-1999    DR Number: 5356
//  Project:  ATC
//  Description:
//      Corrected touch panel error code reporting to background fault handler.
//		Each touch failure is now OR'ed into the background error code.
//
//  Revision: 014  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 013 By: iv     Date: 11-Mar-1998   DR Number: DCS 5041, 5049, 5080
//  	Project:  Sigma (R8027)
//		Description:
//		Added error code argument to all tests indcluding saas test; improved
//      TOD test.
//
//  Revision: 012  By: pc    Date:  22-Oct-1997    DR Number: DCS 2570
//      Project:  Sigma (840)
//      Description:
//      Converted SIGMA_INTEGRATION compiler option to work with Sigma Integration Test (aka TestCommand Group)
//         Allows testing of requirements 06126; 06130; 06131.
//
//  Revision: 011  By: iv    Date:  06-Oct-1997    DR Number: DCS 2543
//      Project:  Sigma (840)
//      Description:
//      Store the new event id: BK_TOUCH_SCREEN_RESUME_INFO in the
//      information log.
//
//  Revision: 010  By: iv      Date: 26-Sep-1997    DR Number: DCS 1920
//    Project:  Sigma (R8027)
//  Description:
//      Store the new event id: BK_TOUCH_SCREEN_BLOCKED_INFO in the
//      information log.
//
//  Revision: 009   By: iv   Date:  09-Sep-1997    DR Number: DCS 2449
//      Project:  Sigma (840)
//      Description:
//        In GuiTouchFail() method, set touch fail to TRUE for the default
//        case instead of asserting.
//
//  Revision: 008   By: iv   Date:  06-Aug-1997    DR Number: DCS 2343
//      Project:  Sigma (840)
//      Description:
//        In TouchScreenTest(), added a call to ReinitializeTouch()
//        when a touch failure exists.
//
//  Revision: 007   By: sp   Date: 9-July-1997    DR Number: 2387
//    Project:  Sigma (840)  
//    Description:
//         Added integration test code.
//      
//  Revision: 006  By: sp    Date:  30-Jun-1997    DR Number: 2023
//  	Project:  Sigma (840)
//		Description:
//			Change Keys.inl to Keys.in.
//
//  Revision: 005  By: iv    Date:  29-May-1997    DR Number: 2018
//  	Project:  Sigma (840)
//		Description:
//			Remove obsolete file header Wait.hh.
//
//  Revision: 004   By: iv   Date: 28-Apr-1997    DR Number: 1996
//    Project:  Sigma (840)
//    Description:
//		Added tracing to SRS req. 00524, 00528, 06167. Deleted DCS 06137.
//
//  Revision: 003   By: iv   Date: 25-Apr-1997  DR Number: 1999 
//  Project:  Sigma (840)
//      Description:
//         Eliminated a TI number for a class assertion.
//
//  Revision: 002   By: by   Date: 10-Mar-1997  DR Number: 1777 
//  Project:  Sigma (840)
//      Description:
//         Ignore "High Ambient" fault reported by Carrol Touch firmware.
//
//  Revision: 001   By: by   Date: 12-Dec-1995  DR Number: None
//  Project:  Sigma (840)
//      Description:
//         Initial version.
//
//==============================================================================

#include "BackgroundGuiTests.hh"

//@ Usage-Classes
#include "NovRamManager.hh"
#include "IpcIds.hh"
#include "TouchDriver.hh"
#include "DiagnosticCode.hh"


#ifdef SIGMA_INTEGRATION
#include "TestTrigger.hh"
#endif //SIGMA_INTEGRATION

//@ Constant: MAX_TOUCH_SCREEN_MINOR_FAILURES
//  Maximum number of touch screen blocked errors
//  before a touch screen failure is annunciated
static const Uint8 MAX_TOUCH_SCREEN_MINOR_FAILURES = 3;

//@ Constant: MAX_TOUCH_SCREEN_MAJOR_FAILURES
//  Maximum number of touch screen major failures (eg. ADC errors)
//  before a touch screen failure is annunciated
static const Uint8 MAX_TOUCH_SCREEN_MAJOR_FAILURES = 2;

//@ Constant: MAX_TOUCH_TIMEOUT_COUNT 
//  Maximum number of GUI test cycles before a failure
//  is declared indicating a request for touch screen
//  status was not replied in the allotted time 
static const Uint8 MAX_TOUCH_TIMEOUT_COUNT = 3;

//@ Constant: MAX_TIME_DIFFERENCE 
// maximum time difference between current and
// previous time during the TOD test
static const Uint16 MAX_TIME_DIFFERENCE = 60000;     // 1 minute in ms

//@ Constant: MAX_KEY_DOWN_DURATION 
// at 500 MS resolution with a time-out of 90 seconds
// Maximum time duration for any GUI key to be detected
// depressed before a GUI key stuck failure is annunciated
static const Uint16 MAX_KEY_DOWN_DURATION = 180;


/////////////////////////////////////////////////////////////////////////////
//     Initialize all static private data members
/////////////////////////////////////////////////////////////////////////////

Uint16 BackgroundGuiTests::TouchScreenCounter_ = 0;
Uint32 BackgroundGuiTests::TouchScreenBlockedCounter_ = 0;

Boolean BackgroundGuiTests::TouchScreenFailed_ = FALSE;
Boolean BackgroundGuiTests::TodTestInitialized_ = FALSE;

Boolean BackgroundGuiTests::KeyboardTestFailed_ = FALSE;
Boolean BackgroundGuiTests::SaasAliveTestFailed_ = FALSE;

Boolean BackgroundGuiTests::NovRamChecksumTestFailed_ = FALSE;
Boolean BackgroundGuiTests::TodTestFailed_ = FALSE;

TimeStamp BackgroundGuiTests::PreviousTime_;
TimeStamp BackgroundGuiTests::CurrentTime_;
Uint16 BackgroundGuiTests::GuiKeyTimer_[ NUMBER_OF_GUI_KEYS ];

Uint32 BackgroundGuiTests::TouchScreenFailCounter_ = 0;

#if defined (SIGMA_UNIT_TEST)
Uint16 BackgroundGuiTests::KeyBoardImage_ = 0;
Int    BackgroundGuiTests::SaasStatus_ = 0;
#endif // defined (SIGMA_UNIT_TEST)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  KeyboardTest
//
//@ Interface-Description
//  This method accepts a Uint16 errorCode and returns a BkEventName
//  enumeration.
//
//  This method determines if a GUI key is stuck. 
//  It returns BK_GUI_STUCK_KEY if any key is depressed for more than
//  the maximum time limit, BK_NO_EVENT otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method retrieves the current keyboard image and examines the
//  image in sequence to determine which key(s) is(are) depressed.
//  When a key is depressed, it associated timer is incremented.
//  When any key timer excess the maximum time limit, a GUI keyboard
//  stuck error is declared.  Otherwise, no error is declared.  Once
//  a key is detected as being idle, not depressed, its associated
//  timer is reset.  Once, this test has failed, it is not tested
//  any further.
//
//  $[06135]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
BackgroundGuiTests::KeyboardTest(  Uint16 &errorCode )
{
    Uint16         keyboard; // Keyboard registers
    Uint16         mask;
    BkEventName    rtnValue = BK_NO_EVENT;


    if ( FALSE == KeyboardTestFailed_ )
    {
    // $[TI1]
        // The keyboard test runs every 500 MS.
        // 90 seconds = 180 counts,

#if defined (SIGMA_UNIT_TEST)
        keyboard = KeyBoardImage_;
#else
	// keyboard = ReadKeyBoard();
#endif // defined (SIGMA_UNIT_TEST)

        // Each bit of the keyboard register just read corresponds to a 
        // single key on the GUI panel.
        // Increment the KeyCount for that key if it is pressed.
        for ( Uint8 keyCtr = 0; keyCtr < NUMBER_OF_GUI_KEYS; ++keyCtr )
        {
            mask = 1 << keyCtr ;

            if ( keyboard & mask )      
            {
            // $[TI1.1]
                // key is up, note active low logic
                GuiKeyTimer_[ keyCtr ] = 0;
            }
            else
            {
            // $[TI1.2]
                // key is down, increment the stuck counter
                ++(GuiKeyTimer_[ keyCtr ]);
            }

            if ( GuiKeyTimer_[ keyCtr ] >= MAX_KEY_DOWN_DURATION )
            {
            // $[TI1.3]
                KeyboardTestFailed_ = TRUE;
                rtnValue = BK_GUI_STUCK_KEY;
                errorCode = keyboard;
                break;
            }
            // $[TI1.4]
        }           
    }
    // $[TI2]


    return( rtnValue );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NovRamChecksumTest 
//
//@ Interface-Description
//  This method accepts a Uint16 errorCode and returns a BkEventName
//  enumeration.
//  This method determines if the GUI NOVRAM checksums are valid.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This uses the NovRamManager utility to determine if checksums
//  are valid.
//
//  $[06131] $[00524] $[00528]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
BackgroundGuiTests::NovRamChecksumTest(  Uint16 &errorCode )
{
    BkEventName rtnStatus = BK_NO_EVENT;


    if ( FALSE == NovRamChecksumTestFailed_ )
    {
    // $[TI1]

        if ( NovRamManager::VerifySelf( errorCode ) != NovRamStatus::ALL_ITEMS_VALID )
        {
        // $[TI1.1]
            NovRamChecksumTestFailed_ = TRUE;
            rtnStatus = BK_GUI_NOVRAM_CHECKSUM;
        }
        // $[TI1.2]
    }
    // $[TI2]

#ifdef SIGMA_INTEGRATION
if (TestTrigger::TriggerOnRequirementCheck( 6131 ))
return BK_GUI_NOVRAM_CHECKSUM;
#endif //SIGMA_INTEGRATION


    return( rtnStatus );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  TodTest
//
//@ Interface-Description
//  This method accepts a Uint16 errorCode and returns a BkEventName
//  enumeration.
//  This method executes the system clock time of day test.
//  Returns BK_GUI_TOD_FAIL if clock fails, BK_NO_EVENT otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The time of day clock is tested by reading the current time and 
//  comparing it to a time stamp recorded during the previous test
//  run.  This test should run in a 10 second cycle, the comparision
//  limits are set large to account for the low priority of this task.
//  The validity of a timestamp is automatically checked in the
//  TimeStamp class.
//
//  Whenever the clocks are set by the operator or resynchronized
//  periodically, this test is bypassed.
//
//  $[06126]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
BackgroundGuiTests::TodTest(  Uint16 &errorCode )
{
    BkEventName rtnValue = BK_NO_EVENT;


    if ( FALSE == TodTestFailed_ )
    {
    // $[TI1]

        if ( ( FALSE == TodTestInitialized_ ) ||
             ( TRUE == Background::CheckIfTimeHasChanged() ) )
        {
        // $[TI1.1]
            PreviousTime_.now();  // initialize to the current time
            CurrentTime_.now();   // initialize to the current time

            TodTestInitialized_ = TRUE;
        }
        else
        {
        // $[TI1.2]

            CurrentTime_.now();  // get the current time

            if (   ( CurrentTime_ - PreviousTime_ > MAX_TIME_DIFFERENCE )
               ||  ( PreviousTime_ >= CurrentTime_ )
#ifdef SIGMA_INTEGRATION
               ||  ( TestTrigger::TriggerOnRequirementCheck( 6126 ) )
#endif // SIGMA_INTEGRATION
               )
            {
            // $[TI1.2.1]
                TodTestFailed_ = TRUE;
                rtnValue = BK_GUI_TOD_FAIL;
                errorCode = (PreviousTime_ >= CurrentTime_) ? 1:2 ;
            }
            // $[TI1.2.2]

            // Unconditionally replace the previous time with new time
            PreviousTime_ = CurrentTime_;
        }
    }
    // $[TI2]


    return( rtnValue );
}

#endif // if defined( SIGMA_GUI_CPU ) 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
BackgroundGuiTests::SoftFault( const SoftFaultID  softFaultID,
                               const Uint32       lineNumber,
                               const char*        pFileName,
                               const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SAFETY_NET, BACKGROUNDGUITESTS,
                          lineNumber, pFileName, pPredicate);
}
