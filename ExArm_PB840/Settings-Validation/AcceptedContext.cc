#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: AcceptedContext -  Context for ALL Accepted Settings Values.
//---------------------------------------------------------------------
//@ Interface-Description
//  A context is a storage pool for different kinds of setting values.  The
//  settings, themselves, do not contain their values, the contexts do.  This
//  context is used for managing ALL of the accepted setting values.  The
//  methods of this context provide means of "accepting" any "adjusted"
//  batch setting values, querying the values of various settings, and
//  changing -- and accepting -- the value of all of the non-batch settings.
//
//  This context interfaces with the Adjusted, Recoverable and Pending
//  Contexts.  Though all of those four contexts RECEIVE copies of the
//  values of this context for their own setup, only the Adjusted context
//  PROVIDES its settings values (batch setting values only) for this
//  context to copy.
//
//  Along with the other contexts, this context also interfaces with the
//  NOVRAM data manager ('NovRamManager').  The interface between this
//  context and the NOVRAM data manager is bi-directional.  At startup,
//  this context is initialized with any valid setting values that may
//  already be stored in NOVRAM.  Conversely, when updates occur to any
//  of the Accepted Context's setting values, an update of the persistent
//  copies in NOVRAM is initiated.
//
//  Acceptance of a Vent-Setup causes the storage of the previous batch of
//  accepted setting into the Recoverable Context.  The "recovery" of those
//  previous values, however, must pass through the Adjusted Context before
//  being "accepted".
//
//  Anytime ANY setting value changes, other than initialization, all
//  callbacks registered with this context are activated.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for the accepted context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Privately, this context contains instances of 'BatchSettingValues' and
//  'NonBatchSettingValues' to store all of the setting values managed by
//  this context.  These values managers have callback mechanisms that allow
//  for the registering of callbacks that are activated upon changes to
//  themselves.  These callback mechanisms allow the Accepted Context to
//  monitor the activity of all of its settings easily, despite the fact
//  that there are numerous means by which a change of a setting value can
//  be initiated via this context.  By using these callback mechanisms, the
//  Accepted Context can "piggy-back" its callback activation to the
//  activation of its internal values managers; that is, callbacks that are
//  registered against the Accepted Context are activated via the callback
//  of the internal instances of its values managers.
//
//  As mentioned above, this context contains members to manage the values
//  of the settings.  These same values must be stored in NOVRAM to preserve
//  them across power cycles.  Any update to these members must be
//  accompanied by an update of the NOVRAM copies.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/AcceptedContext.ccv   25.0.4.0   19 Nov 2013 14:27:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 019  By: mnr    Date: 13-Jan-2009    SCR Number: 5987
//  Project:  NEO
//  Description:
//      New default NEONATAL plot scales for Pressure, Volume and Flow.
//
//  Revision: 018  By:  mnr    Date:  07-Jan-2010    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Presets set to default more accurately based on cct type change.
//
//  Revision: 017  By:  mnr    Date:  28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		SRS tracing numbers added
//
//  Revision: 016  By:  mnr    Date:  24-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		With different Presets for NEONATAL, if cct type has just been
//	    changed, default the presets to Empty ("--").
//
//  Revision: 015  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 014   By: rhj   Date:  01-Dec-2006    SCR Number: 6291
//  Project:  RESPM
//  Description:
//       Found a better way to implement a fix for SCR 6291. 
//
//  Revision: 013   By: rhj   Date:  29-Nov-2006    SCR Number: 6291
//  Project:  RESPM
//  Description:
//       Fixed SCR 6291. Added a functionality to prevent waveform 
//       plot settings getting stored into NOVRAM when "foisting" 
//       is in progress. 
//
//  Revision: 012   By: rhj   Date:  27-Oct-2005    SCR Number: 6236
//  Project:  RESPM
//  Description:
//       RESPM Project related changes. 
//
//  Revision: 011  By: sah     Date:  02-Oct-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  removed DEVELOPMENT-only assertions that caused some problems
//         with unit tests
//
//  Revision: 010   By: hct    Date: 11-OCT-2000    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Added single screen compatibility.  Removed call to 
//      SerialInterface::SetupSerialPort().  Replaced by 
//      Settings-Validation subsystem subject/observer paradigm.
//
//  Revision: 009   By: sah    Date: 19-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  ensure FiO2 Enabled and low exh minute volume set to proper
//         value upon acceptance of Same-Patient Setup
//	*  disable Same-Patient Flag upon acceptance of new circuit
//         type
//	*  upon acceptance of SST Setup, set each of the waveform plot
//         settings to a circuit-specific value
//
//  Revision: 008   By: sah   Date:  29-Jun-1999    DR Number: 5455
//  Project:  ATC
//  Description:
//	Original implementation of DCS #5450 caused such slow acceptance
//      of Same- and New-Patient Setups, that comm failures would occur.
//      Therefore, changed notification such that the Accepted Context's
//      observers are only notified for changed settings, while ALL setting
//      observers are notified of a Same- and New-Patient Setup acceptance.
//      
//
//  Revision: 007   By: sah   Date:  28-Jun-1999    DR Number: 5450
//  Project:  ATC
//  Description:
//	Added new private member function, 'notifyAllBatchObservers_()'
//      to solve two problems:  the one for this DCS where when Same-Patient
//      O2% was 21% and New-Patient is 21%, no callback to synchronize
//      alarm observers occurs; and, to ensure that, when an observer of
//      any accepted setting gets notified of an acceptance, all accepted
//      values are available for query.  Also, now using a bit array to
//      keep track of changed batch values, to notify those setting observers
//      after all changes have been made.
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  now derived off of 'ContextSubject' base class
//	*  old 'SettingCallbackMgr' callback mechanism obsoleted, with
//	   new observer/subject mechanism incorporated
//	*  added new 'isSettingChanged()' virtual method
//	*  added new 'areAnyBatchSettingsChanged()' virtual method
//
//  Revision: 005   By: sah   Date:  19-Jan-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  removed UNIT_TEST-only code that is now done with new 'SettingOptions'
//	   class.
//	*  cleaned up requirements referencing
//
//  Revision: 004   By: dosman Date:  23-Jul-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	added SIGMA_UNIT_TEST-only code in order to allow unit test
//	code to work on development unit: when the development GUI is
//	reloaded with software, the settings values in NOVRAM may
//	contain BILEVEL as mode, which will cause an assertion 
//	(without this fix) because the software is not aware of
//	any options (in particular BILEVEL) being enabled.
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: dosman Date:  24-Feb-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//      Have each setting update its own absolute max/min values
//      The updating of absolute max/min values must be done after
//      initialization of the values because mode must be updated
//      first so that mode-dependent absolute max/min values will
//      be set up correctly.
//      Added requirement numbers for tracing to SRS.
//	Deleted removed requirement number.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "AcceptedContext.hh"
#include "SettingConstants.hh"
#include "Fio2EnabledValue.hh"
#include "PatientCctTypeValue.hh"
#include "FlowPlotScaleValue.hh"
#include "PressPlotScaleValue.hh"
#include "VolumePlotScaleValue.hh"

#include "Task.hh"
#include "SettingsXaction.hh"

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#include "Settings_Validation.hh"
#endif  // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
#include "NovRamManager.hh"
#include "SerialInterface.hh"
#include "SettingsMgr.hh"
#include "ApneaIntervalSetting.hh"
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
#include "RecoverableContext.hh"

#include "TimeStamp.hh"
#include "UserClock.hh"
#include "SettingId.hh"
//@ End-Usage

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptedContext()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default accepted settings context.
//
//  If there is a previous patient's setting values available, the Accepted
//  Context will initialized itself based on those values (no callbacks are
//  activated due to these updates).  When the Adjusted Context's
//  initialization occurs, the Adjusted Context initializes itself based on
//  this context's initialized batch setting values.  Thereby, "synchronizing"
//  the two context's batch setting values.
//
//  If there is NO previous patient values, the non-batch setting values of
//  this context are inititialized to their default values, while the batch
//  setting values are left un-initialized -- the acceptance of a New-Patient
//  Setup will initialize this context's batch setting values based on the
//  "adjusted" new-patient values.  Again, "synchronizing" the two contexts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each of the "setting values" instances are registered with this context's
//  callback handler ('RegisterSettingChange_()'), so that the changes of
//  the setting values can be monitored by this context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AcceptedContext::AcceptedContext(void)
			     : ContextSubject(ContextId::ACCEPTED_CONTEXT_ID),
			       bdSafetyPcvFlag_(TRUE)	     
{
  CALL_TRACE("AcceptedContext()");

  //===================================================================
  // initialization of the batch setting values...
  //===================================================================

  const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

  Uint  idx;

  if (areBatchValuesInitialized())
  {   // $[TI1]
    // there are batch setting values in NOVRAM; copy the batch setting
    // values from NOVRAM into the batch value manager instance within the
    // Accepted Context -- no callbacks are activated...
    NovRamManager::GetAcceptedBatchSettings(acceptedBatchValues_);
  }
  else
  {   // $[TI2] -- no persistent batch values; initialize to default...
    // this is an initialization from scratch, therefore initialize ALL of
    // the BATCH SETTING VALUES to their respective default values...
    for (idx  = SettingId::LOW_BATCH_ID_VALUE;
         idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++)
    {
      // update the "accepted" value batch setting indicated by 'idx'
      // to its default value...
      (arrSettingPtrs[idx])->updateToDefaultValue();
    }

    // store the initialized batch setting values into NOVRAM...
    NovRamManager::UpdateAcceptedBatchSettings(acceptedBatchValues_);

    // update the "Accepted-Batch-Settings-Initialized" flag to indicate
    // that initialized batch setting values are now stored in NOVRAM...
    NovRamManager::UpdateBatchSettingsFlag(TRUE);
  }  // end of else...

  // register changes of 'acceptedBatchValues_' for activation of the
  // setting-change callback...
  acceptedBatchValues_.registerCallback(
				  &AcceptedContext::RegisterSettingChange_
				       );


  //===================================================================
  // initialization of the non-batch setting values...
  //===================================================================

  if (areNonBatchValuesInitialized())
  {   // $[TI3]
    // an initialization of the non-batch settings has previously completed,
    // therefore there are non-batch setting values in NOVRAM; copy the
    // non-batch setting values from NOVRAM into the non-batch value manager
    // instance within the Accepted Context -- no callbacks are activated...
    NovRamManager::GetAcceptedNonBatchSettings(acceptedNonBatchValues_);
  }
  else
  {   // $[TI4] -- no persistent non-batch values; initialize to default...
    // this is an initialization from scratch, therefore initialize ALL of
    // the NON-BATCH SETTING VALUES to their respective default values...
    Uint  idx;

    for (idx  = SettingId::LOW_NON_BATCH_ID_VALUE;
         idx <= SettingId::HIGH_NON_BATCH_ID_VALUE; idx++)
    {
      // update the "accepted" value non-batch setting indicated by 'idx'
      // to its default value...
      (arrSettingPtrs[idx])->updateToDefaultValue();
    }

    // store the initialized non-batch setting values into NOVRAM...
    NovRamManager::UpdateAcceptedNonBatchSettings(acceptedNonBatchValues_);

    // update the "Accepted-Non-Batch-Settings-Initialized" flag to indicate
    // that initialized non-batch setting values are now stored in NOVRAM...
    NovRamManager::UpdateNonBatchSettingsFlag(TRUE);
  }  // end of else...

  for (idx  = SettingId::LOW_SETTING_ID_VALUE;
       idx <= SettingId::HIGH_SETTING_ID_VALUE; idx++)
  {
    // to ensure observers of this setting, if any, are updated with the
    // subject's current value, notify all observers...
    (arrSettingPtrs[idx])->notifyAllObservers(Notification::ACCEPTED,
					      Notification::VALUE_CHANGED);
  }

  // register changes of 'acceptedNonBatchValues_' for activation of the
  // setting-change callback...
  acceptedNonBatchValues_.registerCallback(
				  &AcceptedContext::RegisterSettingChange_
					  );


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~AcceptedContext()  [Destructor]
//
//@ Interface-Description
//  Destroy this accepted settings context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AcceptedContext::~AcceptedContext(void)
{
  CALL_TRACE("~AcceptedContext()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  arePatientSettingsAvailable()   [const]
//
//@ Interface-Description
//  Has a New-Patient Setup been completed, thereby making the patient
//  settings available for use?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
AcceptedContext::arePatientSettingsAvailable(void) const
{
  CALL_TRACE("arePatientSettingsAvailable()");

  return(NovRamManager::ArePatientSettingsAvailable());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  areBatchValuesInitialized()  [const]
//
//@ Interface-Description
//  Upon startup, are the batch settings values stored in NOVRAM valid?  In
//  other words, has there been a successful initialization of the accepted
//  batch settings before this point?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
AcceptedContext::areBatchValuesInitialized(void) const
{
  CALL_TRACE("areBatchValuesInitialized()");

  return(NovRamManager::AreBatchSettingsInitialized());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  areNonBatchValuesInitialized()  [const]
//
//@ Interface-Description
//  Upon startup, are the non-batch settings values stored in NOVRAM valid?
//  In other words, has there been a successful initialization of the
//  accepted non-batch settings before this point?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
AcceptedContext::areNonBatchValuesInitialized(void) const
{
  CALL_TRACE("areNonBatchValuesInitialized()");

  return(NovRamManager::AreNonBatchSettingsInitialized());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptNewPatientBatch()
//
//@ Interface-Description
//  Accept the New-Patient setting adjustments.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AcceptedContext::acceptNewPatientBatch(void)
{
  CALL_TRACE("acceptNewPatientBatch()");

  // update the Accepted Context's flag that indicates whether BD is
  // delivering Safety-PCV, or not; this must be done BEFORE the call
  // to 'acceptAdjustedBatch_()', to ensure that any queries done as a
  // result of the activated observer callbacks will get the appropriate
  // accepted batch values...
  setBdSafetyPcvFlag(FALSE);

  acceptAdjustedBatch_(NO_CORRECTION, TRUE);

  // notify all context observers that the entire batch changed...
  notifyAllObservers(Notification::ACCEPTED, Notification::BATCH_CHANGED,
		     SettingId::NUM_BATCH_IDS);

  // clear the flag indicating that we're currently in a New-Patient Setup
  // mode of operation (set by Adjusted Context)...
  ContextMgr::GetAdjustedContext().clearNewPatientSetupFlag();

  // update NOVRAM flag that indicates that the patient settings are
  // available...
  NovRamManager::UpdatePatientSettingsFlag(TRUE);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptSamePatientBatch()  [const]
//
//@ Interface-Description
//  Accept the Same-Patient setting values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The values that are accepted for this operation are already in the 
//  Accepted Context (and NOT in the Adjusted Context), therefore, initiate
//  a transaction from here.
//
//  $[NE02012] -- low exh minute volume must not be OFF with Same-Patient
//                acceptance...
//  $[NE02011] -- FiO2 Enable must not be DISABLED with Same-Patient
//                acceptance...
//---------------------------------------------------------------------
//@ PreCondition
//  (areBatchValuesInitialized())
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AcceptedContext::acceptSamePatientBatch(void)
{
  CALL_TRACE("acceptSamePatientBatch()");
  CLASS_PRE_CONDITION((areBatchValuesInitialized()));

  //-------------------------------------------------------------------
  // Initialize the Adjusted Context with the value of this context...
  //-------------------------------------------------------------------

  AdjustedContext&  rAdjContext = ContextMgr::GetAdjustedContext();

  rAdjContext.adjustGenericBatch();

  //-------------------------------------------------------------------
  // Set Same Patient's FiO2 Monitor value to default...
  //-------------------------------------------------------------------

  // FiO2 monitoring must always be enabled upon acceptance of a Same-Patient
  // Batch...
  acceptedBatchValues_.setDiscreteValue(SettingId::FIO2_ENABLED,
					Fio2EnabledValue::YES_FIO2_ENABLED);

  //-------------------------------------------------------------------
  // Set Same Patient's Low Exhaled Minute Volume Limit value to a
  // non-OFF value...
  //-------------------------------------------------------------------

  const Setting*  pLowExhMinVol =
		    SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MINUTE_VOL);
  const BoundedValue  ADJUSTED_LOW_EXH_MIN_VOL =
					    pLowExhMinVol->getAdjustedValue();

  if (ADJUSTED_LOW_EXH_MIN_VOL == SettingConstants::LOWER_ALARM_LIMIT_OFF)
  {   // $[TI1] -- low exh minute vol is set to 'OFF'...
    const BoundedValue  DEFAULT_LOW_EXH_MIN_VOL =
					  pLowExhMinVol->getNewPatientValue();

    // set to low exh minute volume's new-patient (non-OFF) value...
    acceptedBatchValues_.setBoundedValue(SettingId::LOW_EXH_MINUTE_VOL,
					 DEFAULT_LOW_EXH_MIN_VOL);
  }   // $[TI2] -- low exh minute vol is NOT set to 'OFF'...

  // store these value into NOVRAM...
  NovRamManager::UpdateAcceptedBatchSettings(acceptedBatchValues_);

  // clear the flag indicating that we're currently in a New-Patient Setup
  // mode of operation (set by Adjusted Context)...
  ContextMgr::GetAdjustedContext().clearNewPatientSetupFlag();

  // update the Accepted Context's flag that indicates whether BD is
  // delivering Safety-PCV, or not; this must be done BEFORE the call
  // to 'notifyAllBatchObservers_()', to ensure that any queries done as a
  // result of the activated observer callbacks will get the appropriate
  // accepted batch values...
  setBdSafetyPcvFlag(FALSE);

  // notify all context observers that the entire batch changed...
  notifyAllObservers(Notification::ACCEPTED, Notification::BATCH_CHANGED,
		     SettingId::NUM_BATCH_IDS);

  // set all bits to ensure that all batch setting observers are notified
  // of this Same-Patient acceptance...
  changedBatchValues_.setAllBits();

  // notify all batch setting observers, to ensure synchronization
  // with these previously-accepted batch values...
  notifyAllBatchObservers_();

  // initiate a Vent-Startup Settings' Transaction...
  SettingsXaction::Initiate(::VENT_STARTUP_UPDATE_XMITTED);

  const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

  Uint  idx;

  // reset state of all batch settings...
  for (idx  = SettingId::LOW_BATCH_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++)
  {
    // reset the state of the batch setting indicated by 'idx'
    (arrSettingPtrs[idx])->resetState();
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptVentSetupBatch()
//
//@ Interface-Description
//  Accept the Vent-Setup setting adjustments.  This will correct any
//  invalid apnea settings, when apnea is possible.  The currently accepted
//  batch settings are copied away for possible "recovery" at a later time.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02005]\b\ -- "correct" apnea with non-new-patient batch if invalid... 
//  $[02030]    -- store current batch settings for optional recovery...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaCorrectionStatus
AcceptedContext::acceptVentSetupBatch(void)
{
  CALL_TRACE("acceptVentSetupBatch()");

  // this is an acceptance of a Vent-Setup, therefore store the currently
  // accepted settings for later recovery, BEFORE accepting the new
  // settings...
  ContextMgr::GetRecoverableContext().storeAcceptedSettings();

  return(acceptAdjustedBatch_(CONDITIONAL_CORRECTION));
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptSstBatch()
//
//@ Interface-Description
//  Accept the SST Setup setting adjustments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  None of the Apnea-Constraining settings are changed during SST Setup,
//  therefore no correction is needed.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AcceptedContext::acceptSstBatch(void)
{
  CALL_TRACE("acceptSstBatch()");

  AdjustedContext&  rAdjustedContext = ContextMgr::GetAdjustedContext();

  const DiscreteValue  NEW_CIRCUIT_TYPE =
	      rAdjustedContext.getDiscreteValue(SettingId::PATIENT_CCT_TYPE);
  // must store "current" cct type BEFORE calling 'acceptAdjustedBatch_()'...
  const DiscreteValue  CURR_CIRCUIT_TYPE =
			       getDiscreteValue(SettingId::PATIENT_CCT_TYPE);

  // process the acceptance of this SST Batch...
  acceptAdjustedBatch_(NO_CORRECTION);

  if (NEW_CIRCUIT_TYPE != CURR_CIRCUIT_TYPE)
  {  // $[TI1] -- patient circuit type has been changed...
    //-----------------------------------------------------------------
    // $[NE01011] -- Same-Patient access needs to be disabled, if the
    //               circuit type has changed.
    //-----------------------------------------------------------------

    const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

	Uint  idx;

    // must clear this flag AFTER calling 'acceptAdjustedBatch_()'...
    NovRamManager::UpdatePatientSettingsFlag(FALSE);

    //-----------------------------------------------------------------
    // $[NE01013] -- upon acceptance of SST Setup, set each of the waveform
    //               plot settings to a circuit-specific value...
    //-----------------------------------------------------------------
    switch (NEW_CIRCUIT_TYPE)
    {
    case PatientCctTypeValue::NEONATAL_CIRCUIT :	// $[TI1.1]
      setNonBatchDiscreteValue(SettingId::FLOW_PLOT_SCALE,
			       FlowPlotScaleValue::MINUS_2_TO_2_LPM);
      setNonBatchDiscreteValue(SettingId::VOLUME_PLOT_SCALE,
			       VolumePlotScaleValue::MINUS_2_TO_6_ML);
	  setNonBatchDiscreteValue(SettingId::PRESSURE_PLOT_SCALE,
			     PressPlotScaleValue::MINUS_2_TO_20_CMH2O);
	  //[NV01210] if CCT TYPE has changed set the presets to be default "--"
	  for (idx  = SettingId::TREND_SELECT_1; idx <= SettingId::TREND_PRESET; idx++)
	  {
		// update the "accepted" value non-batch setting indicated by 'idx'
		// to its default value...
		(arrSettingPtrs[idx])->updateToDefaultValue();
	  }
      break;

	case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	// $[TI1.2]
      setNonBatchDiscreteValue(SettingId::FLOW_PLOT_SCALE,
			       FlowPlotScaleValue::MINUS_40_TO_40_LPM);
      setNonBatchDiscreteValue(SettingId::VOLUME_PLOT_SCALE,
			       VolumePlotScaleValue::MINUS_20_TO_200_ML);
	  setNonBatchDiscreteValue(SettingId::PRESSURE_PLOT_SCALE,
			     PressPlotScaleValue::MINUS_20_TO_40_CMH2O);

	  if ( CURR_CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT ) {
		  //[NV01210] if CCT TYPE has changed set the presets to be default "--"
		  for (idx  = SettingId::TREND_SELECT_1; idx <= SettingId::TREND_PRESET; idx++)
		  {
			// update the "accepted" value non-batch setting indicated by 'idx'
			// to its default value...
			(arrSettingPtrs[idx])->updateToDefaultValue();
		  }
	  }
	  break;

	case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI1.3]
      setNonBatchDiscreteValue(SettingId::FLOW_PLOT_SCALE,
			       FlowPlotScaleValue::MINUS_120_TO_120_LPM);
      setNonBatchDiscreteValue(SettingId::VOLUME_PLOT_SCALE,
			       VolumePlotScaleValue::MINUS_100_TO_1000_ML);
	  setNonBatchDiscreteValue(SettingId::PRESSURE_PLOT_SCALE,
			     PressPlotScaleValue::MINUS_20_TO_40_CMH2O);

	  if ( CURR_CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT ) {
		  //[NV01210] if CCT TYPE has changed set the presets to be default "--"
		  for (idx  = SettingId::TREND_SELECT_1; idx <= SettingId::TREND_PRESET; idx++)
		  {
			// update the "accepted" value non-batch setting indicated by 'idx'
			// to its default value...
			(arrSettingPtrs[idx])->updateToDefaultValue();
		  }
	  }
      break;
    default :
      // unexpected circuit type...
      AUX_CLASS_ASSERTION_FAILURE(NEW_CIRCUIT_TYPE);
      break;
    }
  }  // $[TI2] -- patient circuit type has NOT been changed...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptTimeDateBatch()  [const]
//
//@ Interface-Description
//  Accept the Time/Date setting values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  No values are actually copied into the Accepted Context, rather the
//  date & time in the Adjusted Context (if changed) are used to set the
//  real-time clock.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AcceptedContext::acceptTimeDateBatch(void) const
{
  CALL_TRACE("acceptTimeDateBatch()");

  AdjustedContext&  rAdjContext = ContextMgr::GetAdjustedContext();

  const SequentialValue  DAY_VALUE =
			    rAdjContext.getSequentialValue(SettingId::DAY);
  const DiscreteValue    MONTH_VALUE =
			    rAdjContext.getDiscreteValue(SettingId::MONTH);
  const SequentialValue  YEAR_VALUE =
			    rAdjContext.getSequentialValue(SettingId::YEAR);

  const SequentialValue  HOUR_VALUE =
			    rAdjContext.getSequentialValue(SettingId::HOUR);
  const SequentialValue  MINUTE_VALUE =
			    rAdjContext.getSequentialValue(SettingId::MINUTE);

  TimeOfDay  newTimeStruct;

  // initialize 'newTimeStruct' with the accepted time settings...
  newTimeStruct.year   = YEAR_VALUE % 100;
  newTimeStruct.month  = MONTH_VALUE + 1;
  newTimeStruct.date   = DAY_VALUE;
  newTimeStruct.hour   = HOUR_VALUE;
  newTimeStruct.minute = MINUTE_VALUE;

  // initialize to zero those values not specified...
  newTimeStruct.second           = 0;
  newTimeStruct.milliseconds = 0;

  // create a time stamp with the initialized 'newTimeStruct'...
  TimeStamp  newTimeStamp(newTimeStruct);

  // set the clock...
  UserClock::Set(newTimeStamp);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptDciSetupBatch()
//
//@ Interface-Description
//  Accept the DCI Setup setting values.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AcceptedContext::acceptDciSetupBatch(void)
{
  CALL_TRACE("acceptDciSetupBatch()");

  // accept the adjusted DCI settings...
  acceptAdjustedBatch_(NO_CORRECTION);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptServiceSetupBatch()
//
//@ Interface-Description
//  Accept the Service-Mode Setup setting values.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AcceptedContext::acceptServiceSetupBatch(void)
{
  CALL_TRACE("acceptServiceSetupBatch()");

  // accept the adjusted Service-Mode settings...
  acceptAdjustedBatch_(NO_CORRECTION);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getBoundedValue(boundedId)  [const]
//
//@ Interface-Description
//  Return the bounded value of the bounded setting identified by
//  'boundedId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBoundedId(boundedId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
AcceptedContext::getBoundedValue(
			      const SettingId::SettingIdType boundedId
				) const
{
  CALL_TRACE("getBoundedValue(boundedId)");

  BoundedValue  boundedValue;

  if (SettingId::IsBatchBoundedId(boundedId)) 
  {   // $[TI1]
    boundedValue = acceptedBatchValues_.getBoundedValue(boundedId);
  } 
  else if (SettingId::IsNonBatchBoundedId(boundedId)) 
  {   // $[TI2]
    boundedValue = acceptedNonBatchValues_.getBoundedValue(boundedId);
  } 
  else 
  { 
    AUX_CLASS_ASSERTION_FAILURE(boundedId);
  }

  return(boundedValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getDiscreteValue(discreteId)  [const]
//
//@ Interface-Description
//  Return the discrete value of the discrete setting identified by
//  'discreteId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscreteValue
AcceptedContext::getDiscreteValue(
			      const SettingId::SettingIdType discreteId
				 ) const
{
  CALL_TRACE("getDiscreteValue(discreteId)");

  DiscreteValue  discreteValue;

  if (SettingId::IsBatchDiscreteId(discreteId)) 
  {   // $[TI1]
    discreteValue = acceptedBatchValues_.getDiscreteValue(discreteId);
  } 
  else if (SettingId::IsNonBatchDiscreteId(discreteId)) 
  {   // $[TI2]
    discreteValue = acceptedNonBatchValues_.getDiscreteValue(discreteId);
  } 
  else 
  { 
    AUX_CLASS_ASSERTION_FAILURE(discreteId);
  }

  return(discreteValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSequentialValue(sequentialId)  [const]
//
//@ Interface-Description
//  Return the sequential setting value of the setting represented
//  by 'sequentialId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsSequentialId(nonBatchId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SequentialValue
AcceptedContext::getSequentialValue(
			      const SettingId::SettingIdType sequentialId
				   ) const
{
  CALL_TRACE("getSequentialValue(sequentialId)");

  SequentialValue  sequentialValue;

  if (SettingId::IsBatchSequentialId(sequentialId)) 
  {   // $[TI1]
    sequentialValue = acceptedBatchValues_.getSequentialValue(sequentialId);
  } 
  else if (SettingId::IsNonBatchSequentialId(sequentialId)) 
  {   // $[TI2]
    sequentialValue =
    		   acceptedNonBatchValues_.getSequentialValue(sequentialId);
  } 
  else 
  { 
    AUX_CLASS_ASSERTION_FAILURE(sequentialId);
  }

  return(sequentialValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isSettingChanged(settingId)  [const, virtual]
//
//@ Interface-Description
//  Redefined from ContextSubject base class. This query is only relevant
//  to the Adjusted Context, therefore this method will ALWAYS return
//  'FALSE'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
AcceptedContext::isSettingChanged(
			      const SettingId::SettingIdType settingId
				 ) const
{
  CALL_TRACE("isSettingChanged(settingId)");

  return(FALSE);
}    // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  areAnyBatchSettingsChanged()  [const, virtual]
//
//@ Interface-Description
//  Redefined from ContextSubject base class. This query is only relevant
//  to the Adjusted Context, therefore this method will ALWAYS return
//  'FALSE'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
AcceptedContext::areAnyBatchSettingsChanged(void) const
{
  CALL_TRACE("areAnyBatchSettingsChanged()");

  return(FALSE);
}    // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSettingValue(settingId)  [const, virtual]
//
//@ Interface-Description
//  Redefined from ContextSubject base class. This query will return the
//  "accepted" value of the setting indicated by 'settingId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
AcceptedContext::getSettingValue(
			      const SettingId::SettingIdType settingId
			        ) const
{
  CALL_TRACE("getSettingValue(settingId)");

  SettingValue  settingValue;

  if (SettingId::IsBatchId(settingId))
  {  // $[TI1] -- batch setting value...
    settingValue = acceptedBatchValues_.getSettingValue(settingId);
  }
  else
  {  // $[TI2] -- not batch, must be non-batch setting value...
    AUX_CLASS_ASSERTION((SettingId::IsNonBatchId(settingId)), settingId);

    settingValue = acceptedNonBatchValues_.getSettingValue(settingId);
  }

  return(settingValue);
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  areAllBatchValuesValid()  [const]
//
// Interface-Description
//  Are all of the batch setting values within this context valid?
//---------------------------------------------------------------------
// Implementation-Description
//  Each of the batch settings are queried to determine if their value
//  is valid.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
AcceptedContext::areAllBatchValuesValid(void) const
{
  CALL_TRACE("areAllBatchValuesValid()");

  const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

  Boolean  areAllValid = TRUE;
  Uint     idx;

  for (idx  = SettingId::LOW_BATCH_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++)
  {
    if (!(arrSettingPtrs[idx])->isAcceptedValid())
    {
      cout << "Invalid ACCEPTED Batch Setting:" << endl;
      cout << *(arrSettingPtrs[idx]) << endl;

      areAllValid = FALSE;
    }
  }

  return(areAllValid);
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  areAllNonBatchValuesValid()  [const]
//
// Interface-Description
//  Are all of the non-batch setting values within this context valid?
//---------------------------------------------------------------------
// Implementation-Description
//  Each of the non-batch settings are queried to determine if their value
//  is valid.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
AcceptedContext::areAllNonBatchValuesValid(void) const
{
  CALL_TRACE("areAllNonBatchValuesValid()");

  const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

  Boolean  areAllValid = TRUE;
  Uint     idx;

  for (idx  = SettingId::LOW_NON_BATCH_ID_VALUE;
       idx <= SettingId::HIGH_NON_BATCH_ID_VALUE; idx++)
  {
    if (!(arrSettingPtrs[idx])->isAcceptedValid())
    {
      cout << "Invalid ACCEPTED Non-Batch Setting:" << endl;
      cout << *(arrSettingPtrs[idx]) << endl;

      areAllValid = FALSE;
    }
  }

  return(areAllValid);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AcceptedContext::SoftFault(const SoftFaultID softFaultID,
			   const Uint32 lineNumber,
			   const char *pFileName,
			   const char *pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, ACCEPTED_CONTEXT,
			  lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptAdjustedBatch_(correctionRule, isVentStartup)
//
//@ Interface-Description
//  Accept the batch setting adjustments from the Adjusted Context.  Based
//  on 'correctionRule', the apnea settings may be corrected.  A status is
//  returned indicating which apnea settings, if any, were corrected.
//
//  The 'isVentStartup' flag indicates whether a Vent-Startup transaction
//  is to be initiated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02003] -- apnea not possible - accept setting change which would make
//              apnea invalid, without correcting apnea... 
//  $[02004] -- apnea possible - process apnea setting changes normally... 
//  $[02005] -- when to make apnea valid... 
//  $[02082] -- if apnea is not possible, apnea o2 percent shall not
//              be required to equal or exceed non-apnea o2 percent.  
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaCorrectionStatus
AcceptedContext::acceptAdjustedBatch_(
				  const ApneaCorrectionRule correctionRule,
				  const Boolean             isVentStartup
				     )
{
  CALL_TRACE("acceptAdjustedBatch_(correctionRule, isVentStartup)");

  AdjustedContext&  rAdjustedContext = ContextMgr::GetAdjustedContext();

  ApneaCorrectionStatus  correctionStatus = ::NONE_CORRECTED;

  switch (correctionRule) 
  {
  case ::ALWAYS_CORRECT :		// $[TI1]
    // correct the apnea settings whether apnea is possible, or not...
    correctionStatus = rAdjustedContext.correctApneaSettings();
    break;

  case ::CONDITIONAL_CORRECTION :	// $[TI2]
    if (ApneaIntervalSetting::IsApneaPossible(BASED_ON_ADJUSTED_VALUES))
    {   // $[TI2.1]
      // apnea is possible, therefore correct any apnea settings that
      // may be invalid...
      correctionStatus = rAdjustedContext.correctApneaSettings();
    }   // $[TI2.2] -- apnea is NOT possible, therefore no correction... 
    break;

  case ::NO_CORRECTION :		// $[TI3]
    // no apnea correction is to be done...
    break;

  default :
    // 'correctionRule' is an invalid value...
    AUX_CLASS_ASSERTION_FAILURE(correctionRule);
    break;
  };

 
  //===================================================================
  // ACCEPT the adjusted values...
  //===================================================================

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(SIGMA_COMMON)
  // only do this for DEVELOPMENT builds that are NOT build for 'COMMON'
  // CPU, because the 'Contexts_ut' unit test will fail here due to steps
  // taken that a user cannot take...
  SAFE_CLASS_ASSERTION((rAdjustedContext.areAllValuesValid()));
#endif // defined(SIGMA_DEVELOPMENT)  &&  !defined(SIGMA_COMMON)

  // get a constant reference to the current batch values within the
  // Adjusted Context...
  const BatchSettingValues&  rAdjBatchValues =
				      rAdjustedContext.getAdjustedValues();

  // store these new accepted setting values into non-volatile RAM; this
  // MUST be done BEFORE the Settings' Transaction is initiated...
  NovRamManager::UpdateAcceptedBatchSettings(rAdjBatchValues);

  // update the persistent flag to indicate that there are setting values
  // stored in NOVRAM...
  NovRamManager::UpdateBatchSettingsFlag(TRUE);

  if (rAdjustedContext.isInNewPatientSetupMode())
  {  // $[TI8] -- in New-Patient Setup mode...
    // set all bits to ensure that ALL batch observers are notified...
    changedBatchValues_.setAllBits();
  }
  else
  {  // $[TI9] -- NOT in New-Patient Setup mode...
    // clear all bits before updating batch values...
    changedBatchValues_.clearAllBits();
  }

  // copy ALL of the changed, adjusted settings into this context; this
  // will activate the callback of 'acceptedBatchValues_' for every
  // setting value that is different from the currently accepted values...
  // NOTE:  this must be done BEFORE initiating the transaction!
  acceptedBatchValues_.copyFrom(rAdjBatchValues);

  // notify all batch setting observers of their subjects changes...
  notifyAllBatchObservers_();

  // safety check...
  SAFE_CLASS_ASSERTION((areAllBatchValuesValid()));

  // initiate a Settings' Transaction...
  if (isVentStartup)
  {   // $[TI4]
    SettingsXaction::Initiate(::VENT_STARTUP_UPDATE_XMITTED);
  }
  else
  {   // $[TI5]
    SettingsXaction::Initiate(::USER_UPDATE_XMITTED);
  }


  return(correctionStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  notifyAllBatchObservers_()  [static]
//
//@ Interface-Description
//  Notify the batch setting observers, of those batch settings that have
//  changed, that there has been an acceptance of batch setting changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is done seperately of 'RegisterSettingChange_()' to ensure
//  that ALL batch settings have been updated before the first setting
//  notification is sent out.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AcceptedContext::notifyAllBatchObservers_(void)
{
  CALL_TRACE("notifyAllBatchObservers_()");

  SettingSubject*  pSettingSubject;
  Uint             idx;

  for (idx  = SettingId::LOW_BATCH_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++)
  {  // $[TI1] -- this path always taken...
    if (changedBatchValues_.isBitSet(idx))
    {  // $[TI1.1] -- found a changed setting; notify all observers...
      const SettingId::SettingIdType  SETTING_ID =
					      SettingId::SettingIdType(idx);

      pSettingSubject = SettingsMgr::GetSettingSubjectPtr(SETTING_ID);

      // notify the setting's observers of its value change...
      pSettingSubject->notifyAllObservers(Notification::ACCEPTED,
					  Notification::VALUE_CHANGED);
    }  // $[TI1.2] -- this setting unchanged...
  }

  // now that all changes have been processed, clear all bits...
  changedBatchValues_.clearAllBits();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RegisterSettingChange_(settingId)  [static]
//
//@ Interface-Description
//  A setting's value within this context has changed, therefore do
//  the change processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AcceptedContext::RegisterSettingChange_(const SettingId::SettingIdType settingId)
{
  CALL_TRACE("RegisterSettingChange_(settingId)");
  SAFE_AUX_CLASS_PRE_CONDITION((SettingId::IsId(settingId)), settingId);

  AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

#if defined(SIGMA_DEVELOPMENT)
  if (Settings_Validation::IsDebugOn(::ACCEPTED_CONTEXT)) 
  {
    cout << "Changed ACCEPTED Setting:" << endl;
    cout << *(SettingsMgr::GetSettingPtr(settingId));
  }
#endif  // defined(SIGMA_DEVELOPMENT)

  Notification::ContextChangeId  changeId;

  if (SettingId::IsNonBatchId(settingId)) 
  {   // $[TI1] -- a non-batch setting is changing...
    // this is an update to a non-batch setting, therefore it must be
    // updated to NOVRAM immediately...

    // Update NOVRAM if NOVRAM update has not been disabled for this setting
	Setting * pSetting = SettingsMgr::GetSettingPtr(settingId);
	if(!pSetting->isNovRamUpdateDisabled())
	{
    // store the newly accepted setting values into non-volatile RAM...
    NovRamManager::UpdateAcceptedNonBatchSettings(
					 rAccContext.acceptedNonBatchValues_
	     					 );

	}


    SettingSubject*  pSettingSubject =
				  SettingsMgr::GetSettingSubjectPtr(settingId);

    // notify the setting's observers of its value change...
    pSettingSubject->notifyAllObservers(Notification::ACCEPTED,
					Notification::VALUE_CHANGED);

    changeId = Notification::NON_BATCH_CHANGED;
  }
  else
  {  // $[TI2] -- a batch setting is changing...
    // track the changes for setting observer notifications...
    rAccContext.changedBatchValues_.setBit(settingId);

    changeId = Notification::BATCH_CHANGED;
  }

  // notify this context's observers of a setting value change...
  rAccContext.notifyAllObservers(Notification::ACCEPTED, changeId, settingId);
}

