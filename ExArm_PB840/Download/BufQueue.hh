#ifndef  BufQueue_HH
#define	BufQueue_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: BufQueue - Manages a pool of free buffer to maximize
//      the utilization of the buffer space among multiple tasks.
//		The class also maintains a FIFO data block list, a doubly
//		linked list.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/BufQueue.hhv   25.0.4.0   19 Nov 2013 14:02:46   pvcs  $ 
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  05-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================


#include "DownloadTypes.hh"
#include "Semaphore.hh"
#include "DownloadFault.hh"
#include "DownloadCons.hh"

struct NetworkBuf
{
	Int offSet;
	Int data_len;
	Uint32 data[DATA_BLOCK_SIZE/sizeof(Uint32)];
	NetworkBuf *pNext;
	NetworkBuf *pPrev;
};

class BufQueue 
{
  public:
    BufQueue(void); 					
	NetworkBuf *getFreeBuf(Uint waitTime);
	void freeBuf(NetworkBuf *pBuf);
	void reInit(void);
	NetworkBuf *getDataBlock(void);
	void addDataBlock(NetworkBuf *pBuf);
    static void SoftFault(const FaultType softFaultID,
				 const Uint32      lineNumber,
				 const char*       pFileName  = NULL, 
				 const char*       pPredicate = NULL);

#ifdef SIGMA_UNIT_TEST
	void setFreeList(NetworkBuf *pBuf);
	NetworkBuf *getFreeList(void);
#endif // SIGMA_UNIT_TEST

  protected:

  private:
    ~BufQueue(void);					// not implemented.
    BufQueue(const BufQueue&);			// not implemented...
    void operator=(const BufQueue&);	// not implemented...

    //@ Data-Member:  freeList_;
    //  A singly linked list of free network buffer.
	NetworkBuf *freeList_;
	
	//@ Data_Member: freeLock_;
	//  Semaphore used to synchronize access to the buffer free list.
	Semaphore freeLock_;

	//@ Data_Member: dataLock_;
	//  Semaphore used to synchronize access to the data block list.
	Semaphore dataLock_;

    //@ Data-Member:  buffers_[NETWORK_BUFFER_ENTRIES]
	//  An array of network buff space.
	NetworkBuf	buffers_[NETWORK_BUFFER_ENTRIES];

    //@ Data-Member:  pHead_
    //  a head of the doubly linked data block list, points to the latest
	//  added data. More data is added at the head of the data block list.
		NetworkBuf *pHead_;

    //@ Data-Member:  pTail_
    //  a tail of the doubly link data block list, points to the first
	//  in data block. Data block is taken off from the tail of the data
	//  block list.
		NetworkBuf *pTail_;
};


#endif // BufQueue_HH 
