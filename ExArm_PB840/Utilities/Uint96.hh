
#ifndef Uint96_HH
#define Uint96_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  Uint96 - Unsigned 96-bit integer structure.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/Uint96.hhv   25.0.4.0   19 Nov 2013 14:35:16   pvcs  $
//
//@ Modification-Log
//  
//   Revision: 001   By: sah   Date: 06-Mar-1997   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage


//@ Type:  Uint96
// This defines a unsigned, 96-bit "integer", which is the grouping of three
// unsigned, 32-bit integers.
struct Uint96
{
  //@ Data-Member:  upperLongWord_
  // This contains the value of the upper long word.
  Uint32  upperLongWord;

  //@ Data-Member:  middleLongWord_
  // This contains the value of the middle long word.
  Uint32  middleLongWord;

  //@ Data-Member:  lowerLongWord_
  // This contains the value of the lower long word.
  Uint32  lowerLongWord;
};


#endif // Uint96_HH 
