#ifndef BdFailedMessage_HH
#define BdFailedMessage_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  BdFailedMessage - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/BdFailedMessage.hhv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
#include "TaskControlMessage.hh"
//@ End-Usage
 
class BdFailedMessage : public TaskControlMessage {
  public:
    BdFailedMessage();
    BdFailedMessage(const BdFailedMessage& rMessage);

    ~BdFailedMessage();

    const char* nameOf(void) const;
    void dispatch(const AppContext& context) const;

    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);
 
  private:
    void operator=(const BdFailedMessage*);     // declared only 

};
 

#endif // BdFailedMessage_HH
