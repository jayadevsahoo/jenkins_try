#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  LowExhMinVolSetting - Low Exhaled Minute Volume Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the lower limit of
//  exhaled minute volume (in liters), below which the "low exhaled minute
//  volume alarm" shall be activated.  This class inherits from
//  'BoundedSetting' and provides the specific information needed for
//  representation of low exhaled minute volume.  This information includes
//  the interval and range of this setting's values, and this batch setting's
//  new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, but 'updateConstraints_()' is
//  overridden to update this setting's dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/LowExhMinVolSetting.ccv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 016   By: gdc    Date: 25-Aug-2010    SCR Number: 6582
//  Project:  MAT
//  Description:
//      Changed to clip to minimum only for invasive vent type,
// 		not to include CPAP which is an NIV only mode.
//
//  Revision: 015   By: mnr    Date: 28-Dec-2009    SCR Number: 6550
//  Project:  NEO
//  Description:
//      Enforced lower bound check to resolve SCR 6550.
//
//  Revision: 014   By: gdc    Date: 29-Apr-2009    SCR Number: 6478
//  Project:  840S
//  Description:
//      Corrected getNewPatientValue to return unclipped value.
//
//  Revision: 013   By: gdc    Date: 02-Mar-2009    SCR Number: 6478
//  Project:  840S
//  Description:
//      Corrected resolution of new-patient value.
//
//  Revision: 012   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 011  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      NIV mods. Added SRS requirement numbers.  
//
//  Revision: 010  By: gdc    Date: 18-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 009 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 008   By: sah    Date: 18-Apr-2000    DR Number: 5705
//  Project:  NeoMode
//  Description:
//      Changed neonatal's lowest, non-OFF value from 0.050 to 0.010.
//
//  Revision: 007   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//	*  added Neonatal-circuit soft bound, and capability of turning
//         OFF
//      *  eliminated 'get{Max,Min}Limit()' method, now defined by base class
//      *  incorporated circuit-specific new-patient values and ranges
//
//  Revision: 006   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5018
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.15.1.0) into Rev "BiLevel" (1.15.2.0)
//	Modified new-patient value calculation to help "loosen" the
//	spread between the high and low counterparts.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.15.1.0) into Rev "BiLevel" (1.15.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "LowExhMinVolSetting.hh"
#include "SettingConstants.hh"
#include "ModeValue.hh"
#include "PatientCctTypeValue.hh"
#include "VentTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

#define ADULT_PED_MIN_VALUE  Real32(0.050f)
#define NEONATAL_MIN_VALUE   Real32(0.010f)

//  $[02182] -- The setting's range ...
//  $[02185] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	DEFINED_LOWER_ALARM_LIMIT_OFF,// absolute minimum...
	0.0f,		// unused...
	THOUSANDTHS,	// unused...
	NULL
};
static BoundedInterval  Node3_ =
{
	ADULT_PED_MIN_VALUE,// change-point...
	(ADULT_PED_MIN_VALUE - DEFINED_LOWER_ALARM_LIMIT_OFF),// resolution...
	THOUSANDTHS,	// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE2_ =
{
	0.500f,		// change-point...
	0.005f,		// resolution...
	THOUSANDTHS,	// precision...
	&::Node3_
};
static const BoundedInterval  NODE1_ =
{
	5.00f,		// change-point...
	0.05f,		// resolution...
	HUNDREDTHS,		// precision...
	&::NODE2_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	60.0f,		// absolute maximum...
	0.5f,		// resolution...
	TENTHS,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: LowExhMinVolSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LowExhMinVolSetting::LowExhMinVolSetting(void)
: BatchBoundedSetting(SettingId::LOW_EXH_MINUTE_VOL,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  LOW_EXH_MIN_VOL_MAX_ID,	 // maxConstraintId...
					  LOW_EXH_MIN_VOL_MIN_ID)	 // minConstraintId...
{
	CALL_TRACE("LowExhMinVolSetting()");

	// register this setting's soft bound...
	registerSoftBound_(::LOW_EXH_MIN_VOL_SOFT_MIN_ID, Setting::LOWER);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~LowExhMinVolSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LowExhMinVolSetting::~LowExhMinVolSetting(void)
{
	CALL_TRACE("~LowExhMinVolSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is normally changeable except in CPAP when it's not
//  applicable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
LowExhMinVolSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");
	Applicability::Id applicabilityId;

	const Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const DiscreteValue MODE = (qualifierId == Notification::ADJUSTED) ? 
							   pSetting->getAdjustedValue() : pSetting->getAcceptedValue();

	if ( ModeValue::CPAP_MODE_VALUE == MODE )
	{
		applicabilityId = Applicability::INAPPLICABLE;
	}
	else
	{
		applicabilityId = Applicability::CHANGEABLE;
	}

	return applicabilityId;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
LowExhMinVolSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	const Setting*  pPatientCctType =
	SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
	pPatientCctType->getAdjustedValue();

	const Setting*  pVentType = SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE);

	const DiscreteValue  VENT_TYPE = pVentType->getAdjustedValue();

	const Setting*  pModeValue = SettingsMgr::GetSettingPtr(SettingId::MODE);

	Real32  scalingFactor;

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			scalingFactor  = SettingConstants::NP_LOW_EXH_MINUTE_VOL_NEO_SCALE;
			::Node3_.value = NEONATAL_MIN_VALUE;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			scalingFactor  = SettingConstants::NP_LOW_EXH_MINUTE_VOL_PED_SCALE;
			::Node3_.value = ADULT_PED_MIN_VALUE;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			scalingFactor  = SettingConstants::NP_LOW_EXH_MINUTE_VOL_ADULT_SCALE;
			::Node3_.value = ADULT_PED_MIN_VALUE;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	// update the resolution between the lowest change point, and the
	// absolute minimum, "OFF" value...
	::Node3_.resolution = (::Node3_.value - ::LAST_NODE_.value);

	BoundedValue  newPatient;

	switch ( VENT_TYPE )
	{
		case VentTypeValue::NIV_VENT_TYPE :   // includes CPAP mode
			newPatient.value = DEFINED_LOWER_ALARM_LIMIT_OFF;
			newPatient.precision = THOUSANDTHS;
			break;
		case VentTypeValue::INVASIVE_VENT_TYPE :
			{
				const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);
				const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

                // $[02183] The settings new-patient value ...
				newPatient.value = ((scalingFactor * IBW_VALUE) - 0.05f);

				// warp the calculated value to a "click" boundary...
				// but not clipped by current constraints (SCR #6478)
				getBoundedRange().warpValue(newPatient, BoundedRange::WARP_NEAREST, FALSE);

				// clip to absolute minimum...resolution for SCR 6550
				newPatient.value = MAX_VALUE( ::Node3_.value, newPatient.value);	  

				break;
			}
		default:
			AUX_CLASS_ASSERTION_FAILURE(VENT_TYPE);
			break;
	}

	return(newPatient);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
LowExhMinVolSetting::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							LOW_EXH_MINUTE_VOL_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NI02010]\e\ -- transitioning from Invasive to NIV
//  $[NI02011]\a\ -- transitioning from NIV to Invasive
//---------------------------------------------------------------------
//@ PreCondition
//  ((settingId == SettingId::VENT_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
LowExhMinVolSetting::acceptTransition(
									 const SettingId::SettingIdType settingId,
									 const DiscreteValue            newValue,
									 const DiscreteValue        currValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	AUX_CLASS_PRE_CONDITION((settingId == SettingId::VENT_TYPE || settingId == SettingId::MODE), settingId);

	setAdjustedValue(getNewPatientValue());
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02182]   -- setting's range...
//  $[NE02010] -- setting's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
LowExhMinVolSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	BoundStatus  dummyStatus(getId());

	//-------------------------------------------------------------------
	// determine upper limit value...
	//-------------------------------------------------------------------
	{
		BoundedRange::ConstraintInfo  maxConstraintInfo;

		const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

		const BoundedSetting*  pHighExhMinuteVol =
		SettingsMgr::GetBoundedSettingPtr(SettingId::HIGH_EXH_MINUTE_VOL);

		// get the current value of high minute volume alarm setting; this setting
		// must always be lower that the current high minute volume setting...
		BoundedValue  highMinVolBasedMax = pHighExhMinuteVol->getAdjustedValue();

		// NOTE:  a '<=' comparison is used instead of a '<' comparison because
		//        of the decrement done to the value within the if clause, which
		//        equates an '<=' into an '<'...
		if ( highMinVolBasedMax.value <= ABSOLUTE_MAX )
		{	// $[TI1]
			// reset this range's maximum constraint to allow for "warping" of a
			// new maximum value...
			getBoundedRange_().resetMaxConstraint();

			// warp the calculated value to a "click" boundary...
			getBoundedRange_().warpValue(highMinVolBasedMax);

			// calculate the value one "click" below this current high minute volume
			// alarm value, by using low exh minute volume's bounded range instance...
			getBoundedRange_().decrement(1, highMinVolBasedMax, dummyStatus);

			// the high exhaled minute volume-based maximum is more restrictive,
			// therefore save its value and id as the maximum bound value and id...
			maxConstraintInfo.id    = LOW_EXH_MIN_VOL_MAX_BASED_HIGH_ID;
			maxConstraintInfo.value = highMinVolBasedMax.value;
		}
		else
		{	// $[TI2]
			// the absolute maximum is more restrictive, therefore save its value
			// and id as the maximum bound value and id...
			maxConstraintInfo.id    = LOW_EXH_MIN_VOL_MAX_ID;
			maxConstraintInfo.value = ABSOLUTE_MAX;
		}

		// this setting has no maximum soft bounds...
		maxConstraintInfo.isSoft = FALSE;

		getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
	}

	//-------------------------------------------------------------------
	// determine lower limit value...
	//-------------------------------------------------------------------
	{
		const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

		const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

		const Setting*  pVentType =
		SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE);

		const DiscreteValue  VENT_TYPE =
		pVentType->getAdjustedValue();

		BoundedRange::ConstraintInfo  minConstraintInfo;

		switch ( PATIENT_CCT_TYPE_VALUE )
		{
			case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI3]
				// set to lowest, non-OFF value...
				::Node3_.value = NEONATAL_MIN_VALUE;

				if ( isSoftBoundActive_(::LOW_EXH_MIN_VOL_SOFT_MIN_ID) )
				{	// $[TI3.1] -- low minute vol's soft lower bound is active...
					Real32  dummyMaxValue;

					// get the lower soft-bound value...
					findSoftMinMaxValues_(minConstraintInfo.value, dummyMaxValue);

					minConstraintInfo.id     = LOW_EXH_MIN_VOL_SOFT_MIN_ID;
					minConstraintInfo.isSoft = TRUE;
				}
				else
				{	// $[TI3.2] -- low minute volume's soft bound is NOT active...
					minConstraintInfo.id     = LOW_EXH_MIN_VOL_MIN_ID;
					minConstraintInfo.value  = ::LAST_NODE_.value;	// return "OFF"...
					minConstraintInfo.isSoft = FALSE;
				}
				break;
			case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI4]
				// set to lowest, non-OFF value...
				::Node3_.value = ADULT_PED_MIN_VALUE;
				minConstraintInfo.id     = LOW_EXH_MIN_VOL_MIN_ID;
				minConstraintInfo.value  = ::Node3_.value;
				minConstraintInfo.isSoft = FALSE;
				break;
			default :
				// unexpected patient circuit type value...
				AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
				break;
		}

		switch ( VENT_TYPE )
		{
			case VentTypeValue::INVASIVE_VENT_TYPE :
				break;
			case VentTypeValue::NIV_VENT_TYPE :
				minConstraintInfo.id     = LOW_EXH_MIN_VOL_MIN_ID;
				minConstraintInfo.value  = ::LAST_NODE_.value;	// return "OFF"...
				minConstraintInfo.isSoft = FALSE;
				break;
			default:
				AUX_CLASS_ASSERTION_FAILURE(VENT_TYPE);
				break;
		}
		// update the resolution between the lowest change point, and the
		// absolute minimum, "OFF" value...
		::Node3_.resolution = (::Node3_.value - ::LAST_NODE_.value);

		getBoundedRange_().updateMinConstraint(minConstraintInfo);
	}
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NE02010] -- setting's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
LowExhMinVolSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
										   Real32& rSoftMaxValue) const
{
	CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

	const Setting*  pPatientCctType =
	SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
	pPatientCctType->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			// lower soft-bound at lowest, non-OFF value...
			rSoftMinValue = ::Node3_.value;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2]
			// no lower soft-bounds for these circuit types...
			rSoftMinValue = ::LAST_NODE_.value;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	// no upper soft-bound for this setting...
	rSoftMaxValue = ::INTERVAL_LIST_.value;
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute maximum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic maximum constraint.
//
//  $[02182] -- The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
LowExhMinVolSetting::getAbsoluteMaxValue_(void) const
{
	CALL_TRACE("getAbsoluteMaxValue_()");

	Real32  absoluteMaxValue;

	const Setting*  pPatientCctType =
	SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
	pPatientCctType->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			absoluteMaxValue = 10.0f;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			absoluteMaxValue = 30.0f;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			absoluteMaxValue = 60.0f;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMaxValue);
}                                                
