#ifndef AdjustPanel_HH
#define AdjustPanel_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: AdjustPanel - Routes input from the Knob, Accept and Clear Keys
// to application objects.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/AdjustPanel.hhv   25.0.4.0   19 Nov 2013 14:11:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc    Date: 21-May-2007   SCR Number: 6237
//  Project:  TREND
//  Description:
//      Added IsFocusOnSetting() method to return TRUE when focus
//		is on a feedback sensitive object.
//
//  Revision: 007   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 006  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 005  By: yyy      Date: 06-Feb-1997   DCR Number: 655
//    Project:  Sigma (R8027)
//    Description:
//      Changed KnobEvent from void to Int16
//
//  Revision: 004  By: yyy      Date: 30-July-1996   DCR Number: 1184 
//    Project:  Sigma (R8027)
//    Description:
//      Added DisableKnobRotateSound() method.
//
//  Revision: 003  By: gdc      Date: 15-Feb-1996  DCR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	    Removed ifndef / endif around include directives
//
//  Revision: 002  By: yyy      Date: 25-Jan-1996  DCR Number: 655 
//    Project:  Sigma (R8027)
//    Description:
//      Changed the TakeNonPersistentFocus and TakePersistentFocus method to accept a second parameter.
//      This second parameter is a flag which determine if the focus will be a default focus or not.
//      Added isFocusPersistent flag to tell an object is a persistent or nonpersistent focus.
//      Added IsDefaultFocusPersistent_ to tell an default objcet is a persistent or nonpersistent focus.
//  
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"

//@ Usage-Classes
#include "AdjustPanelTarget.hh" 
//@ End-Usage

class AdjustPanel
{
public:
	
	//@ Type: AdjustPanelStatus
	// Identifies the three major states the Adjust focus events can be in:
	enum AdjustPanelStatus
	{
		NO_FOCUS,
		UPPER_SCREEN_FOCUS,
		LOWER_SCREEN_FOCUS,
		NUM_FOCUS
	};

	static void	AcceptKeyPressHappened(void);
	static void	AcceptKeyReleaseHappened(void);
	static void	ClearKeyPressHappened(void);
	
	static Int16 KnobEvent(Int16 delta);

	static void TakeNonPersistentFocus(AdjustPanelTarget *pTarget, Boolean setAsDefault = FALSE, Boolean defaultEnableKnobSound = TRUE);
	static void TakePersistentFocus(AdjustPanelTarget *pTarget, Boolean setAsDefault = FALSE, Boolean enableKnobSound = TRUE);

	static void DisableKnobRotateSound(void);
	static Boolean IsFocusOnSetting(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

#if defined SIGMA_UNIT_TEST
	static AdjustPanelTarget *GetPFocus(void);
#endif	// SIGMA_UNIT_TEST
								 
protected:

private:
	AdjustPanel(void);					// not implemented...
	~AdjustPanel(void);					// not implemented...
	AdjustPanel(const AdjustPanel&);	// not implemented...
	void operator=(const AdjustPanel&);	// not implemented...

	static void SetUpKnobRotateSound_(void);

	//@ Data-Member: PFocus_
	// The PFocus_ is the object which receives the message that the
	// knob has been turned, or that an accept or clear has been pressed.  If
	// this is NULL, then no object has focus.
	static AdjustPanelTarget *PFocus_;

	//@ Data-Member: IsFocusPersistent_
	// This flag indicates if the current focus is "persistent".  Objects not on the
	// Lower screen should take the "persistent" focus and vice versa.  This is needed so
	// that, for example, if an object on the Lower screen clears the non-persistent focus
	// then it will not affect the focus if that focus is persistent.
	static Boolean IsFocusPersistent_;

	//@ Data-Member: PDefaultFocus_
	// If the focus is cleared and PDefaultFocus_ is set then this target will regain the
	// focus automatically.
	static AdjustPanelTarget *PDefaultFocus_;

	//@ Data-Member: IsDefaultFocusPersistent_
	// Flag to indicate if the default focus is persistent or not.
	static Boolean IsDefaultFocusPersistent_;

	//@ Data-Member: DefaultEnableKnobSound_
	// Flag to indicate if the default focus requires the knob sound or not.
	static Boolean DefaultEnableKnobSound_;
};


#endif // AdjustPanel_HH 
