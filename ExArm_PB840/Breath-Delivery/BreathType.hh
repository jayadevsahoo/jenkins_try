
#ifndef BreathType_HH
#define BreathType_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Enum: BreathType - Type of breath that occurs.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathType.hhv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
//@ Usage-Classes
//@ End-Usage


//@ Type:  BreathType
// Type of breath that occurs (patient data).
enum BreathType {
  CONTROL = 0,
  LOWEST_BREATH_TYPE_VALUE = CONTROL,
  ASSIST,
  SPONT,
  NON_MEASURED,
  HIGHEST_BREATH_TYPE_VALUE = NON_MEASURED
};


#endif // BreathType_HH 
