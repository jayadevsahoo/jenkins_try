#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename:  CrcUtilities - Cyclic-Redundancy-Code Utilities.
//---------------------------------------------------------------------
//@ Interface-Description
//  The cyclic-redundancy-code is used to checksum a block of memory.
//  These functions are responsible for providing the services necessary
//  to calculate and verify CRC codes. 
//---------------------------------------------------------------------
//@ Rationale
//  These utilities are needed to verify the correctness of various
//  memory blocks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A table, generated using the CCITT 16-bit polynomial of 'x16 + x12
//  + x5 + 1', is used to "accumulate" the CRC value for calculation and
//  verification.  This is the same CRC polynomial that is used by
//  the 7200 project.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Standard pre-condition tests are used.
//---------------------------------------------------------------------
//@ Restrictions
//  These utilities don't have a way to ensure that the array being
//  used ('arrBytes') is, at least, the size indicated by the size
//  argument passed ('sizeOfBlock'), therefore the following restriction
//  is to be met by the client(s) of these utilities:
//
//          sizeof(arrBytes[]) >= sizeOfBlock
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/CrcUtilities.ccv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By: sah   Date: 04-Mar-1997   DCS Number: 1809
//   Project:  Sigma (R8027)
//   Description:
//	Modify calculation of CRC value such that a zeroed out buffer
//	does NOT produce a CRC of zero value.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//=====================================================================

#include "CrcUtilities.hh"
#include "UtilityClassId.hh"

//@ Usage-Classes
//@ End-Usage


//@ Code...

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

//=====================================================================
//
//  This implementation is based on the following article:
//
//  Perez, Aram. IEEE MICRO. "Byte-wise CRC Calculations". June 1983.
//
//  The table is created by using the following C algorithm and
//  capturing the output to stdout.
//
//      1:  #include <stdio.h>
//      2:  unsigned int mask = 0x8408 /*CRC-16 */;
//      3:
//      4:  main ()
//      5:  {
//      6:      unsigned int CRC, bits, i, feedback;
//      7:      unsigned int data = 0;
//      8:      while(1)  {
//      9:          CRC = 0x0000;
//     10:           bits = data;
//     11:           for (i= 0; i<8; i++) {
//     12:               if ((bits ^ CRC) & 0x0001) feedback = mask;
//     13:               else feedback = 0;
//     14: 
//     15:               bits >>=1;
//     16:               CRC >>=1;
//     17:               CRC ^= feedback;
//     18:           }
//     19:           data++;
//     20:           printf("0x%04X", CRC);
//     21:           if (data > 255) {
//     22:               printf("\n");
//     23:               exit(0);
//     24:           }
//     25:           else if (!(data % 8)) printf(",\n");
//     26:           else printf(", ");
//     27:       }
//     28:   }
//
//=====================================================================

static const CrcValue  CRC_TABLE_[256] =
{
  0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
  0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
  0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
  0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
  0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
  0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
  0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
  0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
  0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
  0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
  0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
  0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
  0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
  0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
  0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
  0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
  0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
  0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
  0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
  0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
  0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
  0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
  0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
  0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
  0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
  0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
  0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
  0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
  0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
  0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
  0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
  0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};


//=====================================================================
//
//  Static Functions (private to this file)...
//
//=====================================================================

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  Accumulate_(aByte, rCrc)  [static]
//
//@ Interface-Description
//  Calculates the cyclic redundany code for a byte stream.  Bytes
//  are passed to this method sequentially from the caller,
//  via 'aByte'.  On each call the input value is CRC'ed to the
//  accumulated CRC value ('rCrc') and the accumulated CRC value
//  updated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  1. Exclusive-OR the input byte with the low-order byte of the CRC
//     to get the index to the lookup table.
//  2. Shift the CRC eight bits to the right.
//  3. Exclusive-OR the CRC register with the contents of the table.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

inline void
Accumulate_(const Byte aByte, CrcValue& rCrc)
{
  CALL_TRACE("::Accumulate_(aByte, rCrc)");
  rCrc = ((rCrc >> 8) ^ ::CRC_TABLE_[aByte ^ (rCrc & 0x00ff)]);
}   // $[TI1]


//=====================================================================
//
//  External Functions...
//
//=====================================================================

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  CalculateCrc(arrBytes, sizeOfBlock)  [extern]
//
//@ Interface-Description
//  This calculates the CRC for the block of bytes given by 'arrBytes'.
//  The size of the block is given by 'sizeOfBlock'.  The calculated
//  CRC is returned from this function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each byte is sequentially passed to the byte CRC accumulator.
//  The value returned is the result of the CRC applied to the entire
//  array of bytes.
//---------------------------------------------------------------------
//@ PreCondition
//  (sizeOfBlock > 0)
//  (arrBytes is, at least, 'sizeOfBlock' bytes big)
//---------------------------------------------------------------------
//@ PostCondition
//  (::VerifyCrc(arrBytes, sizeOfBlock,
//  	         ::CalculateCrc(arrBytes, sizeOfBlock)) == SUCCESS)
//@ End-Free-Function
//=====================================================================

CrcValue
CalculateCrc(const Byte arrBytes[], const size_t sizeOfBlock)
{
  CALL_TRACE("::CalculateCrc(arrBytes, sizeOfBlock)");
  FREE_PRE_CONDITION((sizeOfBlock > 0), UTILITIES, CRC_UTILITIES);
  FREE_PRE_CONDITION((arrBytes != NULL), UTILITIES, CRC_UTILITIES);

  CrcValue  calculatedCrc = 0;

  for (Uint32 idx = 0; idx < sizeOfBlock; idx++)
  {
    // "accumulate" the CRC for each byte in the block...
    ::Accumulate_(arrBytes[idx], calculatedCrc);
  }

  // NOTE:  should use these 'CrcValue' constants, rather than the literal
  //	    values, because the compiler will treat the literals as signed
  //	    32-bit integers, but 'CrcValue' is an unsigned 16-bit integer...
  static const CrcValue  ALL_BITS_OFF_CRC_  = (CrcValue)0u;
  static const CrcValue  ALL_BITS_ON_CRC_   = (CrcValue)~ALL_BITS_OFF_CRC_;
  static const CrcValue  DEFAULT_CRC_VALUE_ = (CrcValue)0x0f;

  if (calculatedCrc == ALL_BITS_OFF_CRC_  ||
      calculatedCrc == ALL_BITS_ON_CRC_)
  {   // $[TI1]
    // a completely zeroed out memory space (e.g., 'zerovars') could pass
    // a CRC test, because the resulting CRC would be zero, also, a memory
    // space with all '0xffff' values would also pass its CRC test, therefore
    // set to anything other than zero...
    calculatedCrc = DEFAULT_CRC_VALUE_;
  }   // $[TI2]

  return(calculatedCrc);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  VerifyCrc(arrBytes, sizeOfBlock, referenceCrc)
//  			[extern]
//
//@ Interface-Description
//  This verifies the data within the given block ('arrBytes'), of the
//  given size ('sizeOfBlock'), against a reference CRC ('referenceCrc').
//  If the reference CRC agrees with the calculated CRC of this block,
//  then 'SUCCESS' is returned, else 'FAILURE' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The CRC for the word block is calculated.  The result from the
//  the calculation is then verified against the reference CRC.
//---------------------------------------------------------------------
//@ PreCondition
//  (sizeOfBlock > 0)
//  (arrBytes is, at least, 'sizeOfBlock' bytes big)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

SigmaStatus
VerifyCrc(const Byte     arrBytes[],
	  const size_t   sizeOfBlock,
	  const CrcValue referenceCrc)
{
  CALL_TRACE("::VerifyCrc(arrBytes, sizeOfBlock, referenceCrc)");
  SAFE_FREE_PRE_CONDITION((sizeOfBlock > 0), UTILITIES, CRC_UTILITIES);
  SAFE_FREE_PRE_CONDITION((arrBytes != NULL), UTILITIES, CRC_UTILITIES);

  CrcValue  calculatedCrc = ::CalculateCrc(arrBytes, sizeOfBlock);

  return((calculatedCrc == referenceCrc) ? SUCCESS	// $[TI1]
					 : FAILURE);	// $[TI2]
}
