
#ifndef EstTestResultData_HH
#define EstTestResultData_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  EstTestResultData -  Manage the EST service test data to be
//                            saved into or loaded from NovRam.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/EstTestResultData.hhv   25.0.4.0   19 Nov 2013 14:21:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By:  yyy    Date:  30-JUL-97    DR Number: 2013,2205
//    Project:  Sigma (R8027)
//    Description:
//      Updated the data item for Nurse Call test, exh valve velocity transducer test.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "ServiceDataClassIds.hh"
#include "SmTestId.hh"
#include "SmDataId.hh"
#include "TestResultData.hh"


class EstTestResultData : public TestResultData
{
public:
    EstTestResultData(void);
    ~EstTestResultData(void);
    EstTestResultData& operator=(const EstTestResultData&); 

    // Load the test result data from NovRam into the current
    // TestResultData object.
    virtual void loadDataFromNovRam(void);

    // Update TestResult data into NovRam.
    virtual void testCompleted(void);

    static void  SoftFault(const SoftFaultID softFaultID,
                                  const Uint32      lineNumber,
                                  const char*       pFileName  = NULL,
                                  const char*       pPredicate = NULL);

private:				  
    EstTestResultData(const EstTestResultData&); 

    //@ Type: SummarizedEstTestDataItemId
    // This enum lists all of the Service mode tests data generated by the
    // service mode.
    enum SummarizedEstTestDataItemId
    {
		NULL_SUMMARIZED_TEST_DATA_ITEM   = -1,
		SUMMARIZED_TEST_DATA_ITEM_START  = 0,
		
		// GAS_SUPPLY_TEST_ID
		GAS_SUPPLY_TEST_START_INDEX=SUMMARIZED_TEST_DATA_ITEM_START,
		GAS_SUPPLY_TEST_END_INDEX=GAS_SUPPLY_TEST_START_INDEX +
										SmDataId::GAS_SUPPLY_TEST_END,
		
		// GUI_KEYBOARD_TEST_ID
		GUI_KEYBOARD_TEST_START_INDEX,
		GUI_KEYBOARD_TEST_END_INDEX=GUI_KEYBOARD_TEST_START_INDEX +
										SmDataId::GUI_KEYBOARD_TEST_END,

		// GUI_KNOB_TEST_ID
		GUI_KNOB_TEST_START_INDEX,
		GUI_KNOB_TEST_END_INDEX=GUI_KNOB_TEST_START_INDEX +
										SmDataId::GUI_KNOB_TEST_END,
											
		// GUI_LAMP_TEST_ID
		GUI_LAMP_TEST_START_INDEX,
		GUI_LAMP_TEST_END_INDEX=GUI_LAMP_TEST_START_INDEX +
										SmDataId::GUI_LAMP_TEST_END,
											
		// BD_LAMP_TEST_ID
		BD_LAMP_TEST_START_INDEX,
		BD_LAMP_TEST_END_INDEX=BD_LAMP_TEST_START_INDEX +
										SmDataId::BD_LAMP_TEST_END,
											
		// GUI_AUDIO_TEST_ID
		GUI_AUDIO_TEST_START_INDEX,
		GUI_AUDIO_TEST_END_INDEX=GUI_AUDIO_TEST_START_INDEX +
										SmDataId::GUI_AUDIO_TEST_END,
											
		// GUI_NURSE_CALL_TEST_ID
		GUI_NURSE_CALL_TEST_START_INDEX,
		GUI_NURSE_CALL_TEST_END_INDEX=GUI_NURSE_CALL_TEST_START_INDEX +
										SmDataId::GUI_NURSE_CALL_TEST_END,
											
		// BD_AUDIO_TEST_ID
		BD_AUDIO_TEST_START_INDEX,
		BD_AUDIO_TEST_END_INDEX=BD_AUDIO_TEST_START_INDEX +
										SmDataId::BD_AUDIO_TEST_END,
		
		// FS_CROSS_CHECK_TEST_ID
		FS_CROSS_CHECK_TEST_START_INDEX,
		FS_CROSS_CHECK_TEST_END_INDEX=FS_CROSS_CHECK_TEST_START_INDEX +
										SmDataId::FS_CROSS_CHECK_TEST_END,

		// CIRCUIT_PRESSURE_TEST_ID
		CIRCUIT_PRESSURE_TEST_START_INDEX,
		CIRCUIT_PRESSURE_TEST_END_INDEX=CIRCUIT_PRESSURE_TEST_START_INDEX +
										SmDataId::CIRCUIT_PRESSURE_TEST_END,
		
		// SAFETY_SYSTEM_TEST_ID
		SAFETY_SYSTEM_TEST_START_INDEX,
		SAFETY_SYSTEM_TEST_END_INDEX=SAFETY_SYSTEM_TEST_START_INDEX +
										SmDataId::SAFETY_SYSTEM_TEST_END,

		// EXH_VALVE_SEAL_TEST_ID
		EXH_VALVE_SEAL_TEST_START_INDEX,
		EXH_VALVE_SEAL_TEST_END_INDEX=EXH_VALVE_SEAL_TEST_START_INDEX +
										SmDataId::EXH_VALVE_SEAL_TEST_END,

		// EXH_VALVE_TEST_ID
		EXH_VALVE_TEST_START_INDEX,
		EXH_VALVE_TEST_END_INDEX=EXH_VALVE_TEST_START_INDEX +
										SmDataId::EXH_VALVE_TEST_END,

		// SM_LEAK_TEST_ID
		SM_LEAK_TEST_START_INDEX,
		SM_LEAK_TEST_END_INDEX=SM_LEAK_TEST_START_INDEX +
										SmDataId::SM_LEAK_TEST_END,

		// EXH_HEATER_TEST_ID
		EXH_HEATER_TEST_START_INDEX,
		EXH_HEATER_TEST_END_INDEX=EXH_HEATER_TEST_START_INDEX +
											SmDataId::EXH_HEATER_TEST_END,
				
		// GENERAL_ELECTRONICS_TEST_ID
		GENERAL_ELECTRONICS_TEST_START_INDEX,
		GENERAL_ELECTRONICS_TEST_END_INDEX=GENERAL_ELECTRONICS_TEST_START_INDEX +
											SmDataId::GENERAL_ELECTRONICS_TEST_END,
		
		// GUI_TOUCH_TEST_ID
		GUI_TOUCH_TEST_START_INDEX,
		GUI_TOUCH_TEST_END_INDEX=GUI_TOUCH_TEST_START_INDEX +
											SmDataId::GUI_TOUCH_TEST_END,
											
		// GUI_SERIAL_PORT_TEST_ID
		GUI_SERIAL_PORT_TEST_START_INDEX,
		GUI_SERIAL_PORT_TEST_END_INDEX=GUI_SERIAL_PORT_TEST_START_INDEX +
											SmDataId::GUI_SERIAL_PORT_TEST_END,
											
		// BATTERY_TEST_ID
		BATTERY_TEST_START_INDEX,
		BATTERY_TEST_END_INDEX=BATTERY_TEST_START_INDEX +
											SmDataId::BATTERY_TEST_END,
		
		NUM_SUMMARIZED_TEST_DATA_ITEM
    };

    // Initialize the testDataArray_ 	
     void initializeDataArray_();

    //@Data-Member: testInfo_[];
    // The following array contains Test Data Processing objects
    // that store the indexes into the testDataArray_ and the number of test
    // data items in the EST tests.
    TestDataStruct testInfo_[SmTestId::EST_TEST_MAX];
    
    //@Data-Member: testDataArray_;
    // The following array contains all test data values for all possible test
    // items in the EST tests.
    Real32 testDataArray_[NUM_SUMMARIZED_TEST_DATA_ITEM];
    
};


#endif // EstTestResultData_HH 
