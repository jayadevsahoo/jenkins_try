#ifndef	OsUtil_HH
# define OsUtil_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: OsUtil.hh - Low-level OS utilities needed by the Application
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/Operating-System/vcssrc/OsUtil.hhv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//  Revision  009  By: gdc     Date: 27-Jan-2011     SCR Number: 6706
//  Project:  XENA2
//  Description:
//      Moved IsXena2Config() function to CpuDevice in POST-Library.
//
//  Revision  008  By: mnr     Date: 24-Sep-2010     SCR Number: 6671,6672
//  Project:  XENA2
//  Description:
//      Added IsXena2Config() function.
//
//  Revision: 007  By:  gdc    Date:  24-Aug-2009    SCR Number: 6147
//  Project:  XB
//  Description:
//		Removed IsVentInopActive and IsFanAlarmOn methods as dead code.
//
//  Revision: 006  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision  005  By: Rhomere Jimenez  Date: 22-June-2006  DR Number: 6192
//      Project:   Xena
//      Description:
//         Added IsXenaConfig() function.
//
//   Revision: 004    By: gdc       Date: 07-Feb-2001   DCS Number: 5493
//      Project: GuiComms
//      Description:
//               Added IsGuiCommsConfig() function.
//
//   Revision 003   By: Gary Cederquist Date: 18-FEB-1998 DR Number: XXXX
//      Project:   Sigma   (R8027)
//      Description:
//         Added IsUpperDisplayColor() and IsLowerDisplayColor() functions.
//
//   Revision 002   By: Gary Cederquist Date: 01-AUG-1997 DR Number: 2345
//      Project:   Sigma   (R8027)
//      Description:
//         Removed superfluous function declarations.
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================

#include "MemoryMap.hh"

#define FAULT_HANDLER_MASK	(5)	// masks off all but timer & NMI

//  TRAP number for Sigma TRAP handler

//TODO E600 commenting out asm lines
//asm("SIGMATRAP   EQU         1");

//  Sigma TRAP handler function codes
//asm("INITREBOOT  EQU         1");
//asm("SETPROCIPL  EQU         2");

//@ Begin-Free-Function
void	BoardInit(void);
Uint8	SetProcessorIPL(Uint8 ipl);
Boolean IsXenaConfig(void);

#ifdef SIGMA_GUI_CPU
Boolean	IsAudioAttnOn(void);
Boolean	IsAudioAckOn(void);
Boolean	IsAudioBusyOn(void);
void	SetVentInop(void);
void	ClearRemoteAlarm(void);
void	SetRemoteAlarm(void);
void	ResetSaas(void);
Boolean IsUpperDisplayColor(void);
Boolean IsLowerDisplayColor(void);
Boolean IsGuiCommsConfig(void);
#endif // SIGMA_GUI_CPU

void	ClearFlashWriteEnable(void);
void	SetFlashWriteEnable(void);
void	WriteDiagLeds(Uint8 value);

#ifdef SIGMA_COMMON
void    StopAudibleAlarm(void);
#endif // SIGMA_COMMON

//@ End-Free-Function

#endif // OsUtil_HH
