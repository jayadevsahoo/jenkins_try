
#ifndef SettingXmitData_HH
#define SettingXmitData_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  SettingXmitData - Setting Transmission Data Structure.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingXmitData.hhv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//            Initial version 
//  
//====================================================================
#include "SocketWrap.hh"
#include "SettingId.hh"


//@ Type:  SettingsXactionId
// Transaction ID for the communication of settings.
enum SettingsXactionId
{
  // Transaction initiation...
  INITIATE_TRANSACTION        = 4,	// GUI begins the transaction...

  // Transaction #1...
  CONNECTION_INITIATED        = 5,	// connection opened by GUI, to BD...

  // Transaction #2...
  CONNECTION_COMPLETED        = 6,	// transmitted from BD to GUI...

  // Transaction #3...
  VENT_STARTUP_UPDATE_XMITTED = 10,	// transmitted from GUI to BD...
  USER_UPDATE_XMITTED         = 11,	// transmitted from GUI to BD...
  EXTERNAL_UPDATE_XMITTED     = 12,	// (reserved for EPT combatability)...
  ONLINE_SYNC_XMITTED         = 13,	// transmitted from GUI to BD...

  // Transaction #4...
  VALIDATE_XMITTED_SETTINGS   = 20,	// transmitted from BD to GUI...

  // Transaction #5...
  ACCEPT_XMITTED_SETTINGS     = 30,	// transmitted from GUI to BD...

  // Transaction #6...
  SETTINGS_XACTION_COMPLETE   = 40,	// transmitted from BD to GUI...

  NACK_OR_ERROR               = 99
};


struct SettingXmitData
{
  Int16  settingXmitId;
  Int8   reserved;
  Int8   boundedPrec;

  union   // anonymous union...
  {
    Real32  boundedValue;
    Int32   discreteValue;
  };
};

#define NUM_SETTING_XMIT_DATA_ELEMENTS	NUM_BD_SETTING_IDS

struct SettingXactionBlock
{
	Uint16           xactionId;
	Uint16           numXmitElems;
	SettingXmitData  arrXmitElems[NUM_SETTING_XMIT_DATA_ELEMENTS];

	void convHtoN()
	{
		SocketWrap sock;
		xactionId = sock.HTONS(xactionId);
		numXmitElems = sock.HTONS(numXmitElems);
		for(Int i = 0; i < NUM_SETTING_XMIT_DATA_ELEMENTS; i++)
		{
			arrXmitElems[i].settingXmitId = 
					sock.HTONS(arrXmitElems[i].settingXmitId);
			arrXmitElems[i].discreteValue = 
					(Int32)sock.HTONL((Uint32)arrXmitElems[i].discreteValue);
		}
	}

	void convNtoH()
	{
		SocketWrap sock;
		xactionId = sock.NTOHS(xactionId);
		numXmitElems = sock.NTOHS(numXmitElems);
		for(Int i = 0; i < NUM_SETTING_XMIT_DATA_ELEMENTS; i++)
		{
			arrXmitElems[i].settingXmitId = 
					sock.NTOHS(arrXmitElems[i].settingXmitId);
			arrXmitElems[i].discreteValue = 
					(Int32)sock.NTOHL((Uint32)arrXmitElems[i].discreteValue);
		}
	}
};


#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
extern Ostream&  operator<<(Ostream& ostr,
			    const SettingXactionBlock& xaction);
#endif // defined(SIGMA_DEVELOPMENT)


#endif // SettingXmitData_HH 
