#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================
#include "AdcData.h"
#include "AdcChannels.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AdcData_t()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Initialize data member.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
AdcData_t::AdcData_t()
{
	memset(data, 0, sizeof(data));
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AdcData_t ()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
AdcData_t::~AdcData_t()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AdcData_t()
//
//@ Interface-Description
//      Copy constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void AdcData_t::operator =(const AdcData_t& adcData)
{
	memcpy(this->data, adcData.data, sizeof(this->data));
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsValid()
//
//@ Interface-Description
//      This method takes one argument, ADC channel id, and returns a Boolean
//		value to indicate if the data for this ADC channel is valid.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      the input must be a valid raw ADC channel
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
bool AdcData_t::IsValid(Uint16 input)
{ 
	if(input < AdcChannels::ADC_FIRST_CHANNEL && input >= AdcChannels::NUM_ADC_CHANNELS_RAW)
		return false;

	if(data[input * NUM_LONG_WORDS_PER_CHANNEL] > 0)
		return true;
	else
		return false;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetValue()
//
//@ Interface-Description
//      This method takes one argument, ADC channel id, and returns the 
//		ADC value for that channel.
//---------------------------------------------------------------------
//@ Implementation-Description
//      
//---------------------------------------------------------------------
//@ PreCondition
//      the input must be a valid raw ADC channel
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
AdcCounts AdcData_t::GetValue(Uint16 input)
{ 
	if(input < AdcChannels::ADC_FIRST_CHANNEL || input >= AdcChannels::NUM_ADC_CHANNELS_RAW)
		return 0;

	AdcCounts value = (AdcCounts)data[((input * NUM_LONG_WORDS_PER_CHANNEL) + 1)];
	return value;
}
