#ifndef DateTimeSettingsSubScreen_HH
#define DateTimeSettingsSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: DateTimeSettingsSubScreen - The subscreen selected by the
// Date/Time Settings button in the Lower Other Screens subscreen,
// and in the ServiceLowerSelectArea.  Allows adjusting of date and time.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DateTimeSettingsSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: rhj   Date:  23-May-2007    SCR Number: 6237 
//  Project:  Trend
//  Description:
//      Added Date International feature.
//
//  Revision: 003  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "BatchSettingsSubScreen.hh"
#include "ClockSettingButton.hh"
#include "DiscreteSettingButton.hh"
#include "SequentialSettingButton.hh"
#include "SubScreenTitleArea.hh"
//@ End-Usage

class DateTimeSettingsSubScreen : public BatchSettingsSubScreen
{
public:
	DateTimeSettingsSubScreen(SubScreenArea *pSubScreenArea);
	~DateTimeSettingsSubScreen(void);

	// Overload SubScreen methods
	virtual void acceptHappened(void);

	// GuiTimerRegistrar virtual method
	void timerEventHappened(GuiTimerId::GuiTimerIdType);

	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


protected:
	// BatchSettingsSubScreen virtual method...
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									  );

private:
	// these methods are purposely declared, but not implemented...
	DateTimeSettingsSubScreen(void);							// not implemented..
	DateTimeSettingsSubScreen(const DateTimeSettingsSubScreen&);	// not implemented..
	void operator=(const DateTimeSettingsSubScreen&);			// not implemented..

	// Constant: MAX_SETTING_BUTTONS_ 
	// Maximum number of buttons that can be displayed in this subscreen.
	// Must be modified when new buttons are added.
	enum { MAX_SETTING_BUTTONS_ = 6 };

	//@ Data-Member: hourButton_
	// The hour setting button.
	ClockSettingButton hourButton_;

	//@ Data-Member: minuteButton_
	// The minute setting button.
	ClockSettingButton minuteButton_;

	//@ Data-Member: dayButton_
	// The day setting button.
	SequentialSettingButton dayButton_;

	//@ Data-Member: monthButton_
	// The month setting button.
	DiscreteSettingButton monthButton_;

	//@ Data-Member: yearButton_
	// The year setting button.
	SequentialSettingButton yearButton_;

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member:  arrSettingButtonPtrs_
	// The subscreen buttons.
	SettingButton*  arrSettingButtonPtrs_[MAX_SETTING_BUTTONS_ + 1];

	//@ Data-Member: dateFormatButton_
	// The date format setting button.
	DiscreteSettingButton dateFormatButton_;

};

#endif // DateTimeSettingsSubScreen_HH 
