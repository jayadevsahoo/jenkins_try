#ifndef HighResolutionTimeStamp_HH
#define HighResolutionTimeStamp_HH

//----------------------------------------------------------------------------
//            Copyright (c) 2014 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

/*! \file HighResolutionTimeStamp.hh
    \brief This file contains API to get times in microseconds.
*/

#include "OsFoundation.hh"

/*! \class HighResolutionTimeStamp
    \brief This class provides relative time APIS to measure performance 
			
	
	The HighResolutionTimeStamp class contains APIs Update, GetTimeUsec and 
	subtraction (-) operator overloaded.
*/
class HighResolutionTimeStamp
{
  public:
	HighResolutionTimeStamp(void);

	~HighResolutionTimeStamp(void);

	/*! \fn Update(void)
		\brief Update the counter i.e.  usecCounter_ to latest value. Return the time in micro seconds since previous update 
		\param[in]	void
		\return		uint64_t
	*/
	uint64_t Update(void);

	/*! \fn GetTimeUsec(void)
		\brief Return difference of current counter and last counet i.e. usecCounter_
		\param[in]	void
		\return		uint64_t
	*/
	uint64_t GetTimeUsec(void) const ;

	/*! \fn uint64_t operator-(const HighResolutionTimeStamp&)
		\brief  subtraction operator, to find difference between to HighResolutionTimeStamp.
	*/
	uint64_t operator-(const HighResolutionTimeStamp& rHRTime) const;

  private:

	/*! \var unit64_t usecCounter_
		\breif This works as running counter 
	*/
	LARGE_INTEGER  usecCounter_;	

	/*! \var LARGE_INTEGER StartingTime
		\breif Stores frequence one time when object create and used same till object scope is live 
	*/
	static LARGE_INTEGER liFrequency_;

	/*! \fn bool diffTime_(void)
		\brief Return difference of two time 
		\param[in]	LARGE_INTEGER 
		\return		int64_t
	*/
	int64_t diffTime_(LARGE_INTEGER &liTime1, LARGE_INTEGER &liTime2) const;
};

#endif // HighResolutionTimeStamp_HH 
