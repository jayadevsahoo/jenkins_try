// DerivedTabCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "E600Util.h"
#include "DerivedTabCtrl.h"
#include "UserMessages.h"
#include "BDSignals.h"

// CDerivedTabCtrl dialog

CDerivedTabCtrl::CDerivedTabCtrl()
{
	m_tabPages[0] = pTabOne = new CTabOne(this);
	m_tabPages[1] = pTabTwo = new CTabTwo(this);
	m_tabPages[2] = pTabThree = new CTabThree(this);
	m_tabPages[3] = pTabFour = new CTabFour(this);
	m_tabPages[4] = pTabFive = new CTabFive(this);
	m_tabPages[5] = pTabSix = new CTabSix(this);

	m_nNumberOfPages = NUM_CTABS;
}

CDerivedTabCtrl::~CDerivedTabCtrl()
{
	for(int nCount=0; nCount < m_nNumberOfPages; nCount++)
	{
		if(m_tabPages[nCount])
		{
			delete m_tabPages[nCount];
		}
	}
}

void CDerivedTabCtrl::Init()
{
	m_tabCurrent = 0;

	m_tabPages[0]->Create(IDD_DIALOG_TAB1, this);
	m_tabPages[1]->Create(IDD_DIALOG_TAB2, this);
	m_tabPages[2]->Create(IDD_DIALOG_TAB3, this);
	m_tabPages[3]->Create(IDD_DIALOG_TAB4, this);
	m_tabPages[4]->Create(IDD_DIALOG_TAB5, this);
	m_tabPages[5]->Create(IDD_DIALOG_TAB6, this);

	pTabOne->pTabTwo = pTabTwo;
	pTabOne->pTabThree = pTabThree;
	pTabOne->pStatusBar = pStatusBar;

	pTabFour->pTabOne = pTabOne;
	pTabFour->pUtilDlg = pDlg;
	pTabFour->pStatusBar = pStatusBar;

	pTabFive->pTabOne = pTabOne;
	
	pTabSix->pTabOne = pTabOne;
	pTabSix->pStatusBar = pStatusBar;

	m_tabPages[0]->ShowWindow(SW_HIDE);
	m_tabPages[1]->ShowWindow(SW_SHOW);
	m_tabPages[2]->ShowWindow(SW_HIDE);
	m_tabPages[3]->ShowWindow(SW_HIDE);
	m_tabPages[4]->ShowWindow(SW_HIDE);
	m_tabPages[5]->ShowWindow(SW_HIDE);

	SetRectangle();

	//pTabOne->PostMessageW(WM_HEADER_STRING, 0, (LPARAM)DefaultHeaderString);
	//pTabTwo->PostMessageW(WM_HEADER_STRING, 0, (LPARAM)DefaultHeaderString);
	//pTabThree->PostMessageW(WM_HEADER_STRING, 0, (LPARAM)DefaultHeaderString);
	pTabTwo->PostMessageW(WM_HEADER_STRING, 0, (LPARAM)(LPCTSTR)GlobalHeaderString);
	pTabThree->PostMessageW(WM_HEADER_STRING, 0, (LPARAM)(LPCTSTR)GlobalHeaderString);
}

void CDerivedTabCtrl::SetRectangle()
{
	CRect tabRect, itemRect;
	int nX, nY, nXc, nYc;

	GetClientRect(&tabRect);
	GetItemRect(0, &itemRect);

	nX=itemRect.left;
	//+++LLnY=itemRect.bottom+1;
	nY=itemRect.bottom-1;
	nXc=tabRect.right-itemRect.left-1;
	nYc=tabRect.bottom-nY-1;

	m_tabPages[0]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_SHOWWINDOW);
	for(int nCount=1; nCount < m_nNumberOfPages; nCount++)
	{
		m_tabPages[nCount]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW);
	}
}


/////////////////////////////////////////////////////////////////////////////
//
void CDerivedTabCtrl::SelectTab(int nextTab) 
{ 
	SetCurFocus(nextTab); 
	if(m_tabCurrent != GetCurFocus())
	{ 
		m_tabPages[m_tabCurrent]->ShowWindow(SW_HIDE); 
		m_tabCurrent=GetCurFocus(); 
		m_tabPages[m_tabCurrent]->ShowWindow(SW_SHOW); 
		m_tabPages[m_tabCurrent]->SetFocus();
	}
	HighlightItem(m_tabCurrent, true);
}

/////////////////////////////////////////////////////////////////////////////
//
int CDerivedTabCtrl::GetSelectedTab()
{
	return m_tabCurrent;
}


BEGIN_MESSAGE_MAP(CDerivedTabCtrl, CTabCtrl)
	ON_WM_LBUTTONDOWN()
	ON_MESSAGE(WM_ETH_MSG_UPDATE_WINDOW, &OnEthMsgUpdateWindow)
	ON_MESSAGE(WM_CAL_MSG_UPDATE_WINDOW, &OnCalMsgUpdateWindow)
	ON_MESSAGE(WM_APP_SHUTDOWN, OnShutdown)
	ON_MESSAGE(WM_ALL_GAINS_RX, &OnAllGainsReceived)
	ON_MESSAGE(WM_SETTINGS_REPLY, &OnSettingsReply)
END_MESSAGE_MAP()

//**********************************************************************************************//
// CDerivedTabCtrl message handlers
void CDerivedTabCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CTabCtrl::OnLButtonDown(nFlags, point);

	if(m_tabCurrent != GetCurFocus()){
		m_tabPages[m_tabCurrent]->ShowWindow(SW_HIDE);
		m_tabCurrent=GetCurFocus();
		m_tabPages[m_tabCurrent]->ShowWindow(SW_SHOW);
		m_tabPages[m_tabCurrent]->SetFocus();
		for(int i = 0; i < m_nNumberOfPages; i++)
		{
			if(i == m_tabCurrent)
				HighlightItem(i, true);
			else
				HighlightItem(i, false);
		}
	}
}

//**********************************************************************************************//
LRESULT CDerivedTabCtrl::OnEthMsgUpdateWindow(WPARAM x, LPARAM y)
{
	pDlg->PostMessageW(WM_ETH_MSG_UPDATE_WINDOW);
	return 0;
}

//**********************************************************************************************//
LRESULT CDerivedTabCtrl::OnCalMsgUpdateWindow(WPARAM x, LPARAM y)
{
	if(x == 2)
		pDlg->PostMessageW(WM_ETH_MSG_UPDATE_WINDOW);
	pTabFour->SendMessage(WM_CAL_STATUS, x, y);
	return 0;
}

/**********************************************************************************************/
LRESULT CDerivedTabCtrl::OnAllGainsReceived(WPARAM x, LPARAM y)
{
	pTabSix->SendMessage(WM_ALL_GAINS_RX, x, y);
	return 0;
}

//**********************************************************************************************//
LRESULT CDerivedTabCtrl::OnShutdown(WPARAM x, LPARAM y)
{
	for(int nCount=0; nCount < m_nNumberOfPages; nCount++)
	{
		m_tabPages[nCount]->SendMessage(WM_APP_SHUTDOWN);
	}

	for(int nCount=0; nCount < m_nNumberOfPages; nCount++)
	{
		delete m_tabPages[nCount];
	}
	return 0;
}

//**********************************************************************************************//
LRESULT CDerivedTabCtrl::OnSettingsReply(WPARAM x, LPARAM y)
{
	pTabSix->SendMessage(WM_SETTINGS_REPLY, x, y);
	return 0;
}