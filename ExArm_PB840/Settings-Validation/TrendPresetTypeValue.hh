
#ifndef TrendPresetTypeValue_HH
#define TrendPresetTypeValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  TrendPresetTypeValue - Values of the Trend preset types.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TrendPresetTypeValue.hhv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: mnr    Date: 07-Jan-2010    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Added new presets enum and comment.
//
//  Revision: 002   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Added more presets for Neo Mode.
//
//  Revision: 001   By: rhj    Date:  05-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Initial version
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct TrendPresetTypeValue
{
	//@ Type:  TrendPresetTypeValueId
	// All of the possible values of the Trend Preset Type Setting.
	enum TrendPresetNonNeoTypeValueId
	{
		// $[TR01148] -- The user shall have the ability to select
		// between the following Trend Preset options ...
		TREND_PRESET_NONE        = 0,
		TREND_PRESET_WEANING     = 1,
		TREND_PRESET_COPD        = 2,
		TREND_PRESET_ARDS        = 3,
		TREND_PRESET_VC_PLUS     = 4,
		TREND_PRESET_PAV         = 5,
		TREND_PRESET_BILEVEL     = 6,
		TREND_PRESET_SIM         = 7,

		TOTAL_TREND_NON_NEO_PRESET_TYPES = 8
	};
	
	// NOTE: If the total number of presets is increased anytime then 
	//       MAX_DROPDOWN_VALUES in DropDownSettingButton.hh needs to be 
	//       updated as well.

	enum TrendPresetNeoTypeValueId
	{
        TREND_PRESET_NEO_VCV = 1,
		TREND_PRESET_NEO_PCV = 2,
		TREND_PRESET_NEO_BPD = 3,
		TREND_PRESET_NEO_SURF = 4,
		TREND_PRESET_NEO_WEANING = 5,
		TREND_PRESET_NEO_SIMV = 6,
		TREND_PRESET_NEO_SPONT = 7,

		TOTAL_TREND_NEO_PRESET_TYPES = 8
	};
};

#define MAX_PRESETS MAX_VALUE( TrendPresetTypeValue::TOTAL_TREND_NEO_PRESET_TYPES, TrendPresetTypeValue::TOTAL_TREND_NON_NEO_PRESET_TYPES )	



#endif // TrendPresetTypeValue_HH 
