#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Cpu - FPU and MMU Power On Self Tests.
//---------------------------------------------------------------------
//@ Interface-Description
// >Von
//   FloatingPointTest:
//      Tests a subset of floating point instructions.
//      It exercises the basic addition, subtraction,
//      multiplication, division and comparison instructions.
//
//   MmuTest:
//      Accesses an unmapped memory location to generate an access fault.
//      The test passes if an exception occurs.
// >Voff
//---------------------------------------------------------------------
//@ Rationale
//    These tests satisfy the MMU and FPU self test functions of POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//    None
//---------------------------------------------------------------------
//@ Restrictions
//    The techniques used in this module are compiler specific and may
//    change the operation of the test if other compiler optimizations
//    are used.
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/Cpu.ccv   25.0.4.0   19 Nov 2013 14:17:02   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

// no instruction reordering for MMU test
#pragma option -nOr

#include "Cpu.hh"

#include "Post_Library.hh"
#include <stdio.h>
#include "fpopt.h"


//============= F R E E   F U N C T I O N    D E S C R I P T I O N ====
//@ Free-Function: FpuRegisterTest (void)
//
//@ Interface-Description
//
//  This function verifies the integrity of the floating point data
//  registers by loading and comparing several data patterns.  Returns
//  MAJOR failure upon verify failure, otherwise returns PASSED.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//
//@ End-Free-Function
//=====================================================================
static Criticality
FpuRegisterTest(void)
{
   register double a,b,c,d,e,f;   // verify as register vars

   struct Double
   {
       Uint32   msw;
       Uint32   lsw;
   };

   static const Double patterns[]
       = {
               { 0x00000000, 0x00000000 }
             , { 0x3fffffff, 0xffffffff }
             , { 0xbfffffff, 0xffffffff }
             , { 0x2a5a5a5a, 0x5a5a5a5a }
             , { 0x15a5a5a5, 0xa5a5a5a5 }
             , { 0xaa5a5a5a, 0x5a5a5a5a }
             , { 0x95a5a5a5, 0xa5a5a5a5 }
         };

   for (int z=0; z < countof(patterns); z++)
   {
      double & test_pattern = *(double*)&patterns[z];

      a = b = c = d = e = f = test_pattern;

UNIT_TEST_POINT(POSTUT_39_1)
      if (   (a != test_pattern)
          || (b != test_pattern)
          || (c != test_pattern)
          || (d != test_pattern)
          || (e != test_pattern)
          || (f != test_pattern) )
      {
         // $[TI1.1]
         return MAJOR;
      }
      // $[TI1.2]
   }
   return PASSED;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FpuTest (void)
//
//@ Interface-Description
//
//  This routine exercises the floating point processor of the 68040.
//  A MAJOR failure is returned if any part of this test fails. $[11084]
//
//  INPUTS:    NONE
//  OUTPUTS:   PASSED, MAJOR
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  A multiple pattern test is used to write and read to the fp
//  registers.  The compiler "register" directive achieves this by
//  forcing the use of the fp registers as a number of patterns are
//  written.  The floating point control register (FPCR), FP status
//  register (FPSR), and condition code (FPCC) register are tested.
//  When these register checks are complete the individual floating
//  point instructions are exercised as indicated in the FPU
//  instructions listed below.
//
//  The fmovem command is called after setting up registers to known
//  values.  A check after the fmovem is completed is performed to
//  verify data was not corrupted.
//
//  The remainder of the test verifies that miscellaneous floating
//  point calculations are computed correctly.
//
//  Exercising FPU instructions involve calculations such as
//  4195835/3145727.    The table below lists the individual
//  instructions that will be exercised.
//
//  Perform a minimum of the following operations  (+-*/)
//
//  X = A/B
//  Y = A*B
//  T = A+B-C
//
//  Instructions used in FPU test
//
//     FADD.S      Add, single precision
//     FBEQ.W      Branch if equal
//     FCMP.S      Compare
//     FDIV.X      Divide
//     FMOVE.S     Move
//     FMUL.S      Multiply
//     FNEG.S      Negate
//     FSUB.S      Subtract
//
//
//    Register variables are used so that the pattern test cycles
//    patterns trhough FP0-FP7.  If automatic variables were to be used
//    the compiler would optimize the use of FP registers and perform
//    tests on the stack.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//
//@ End-Method
//=====================================================================

enum
{
    NUM_DIGITS_ACCURACY = 20
};

Criticality
Cpu::FpuTest(void)
{
   register double a,b,c,d,e,g,h,i,j;

UNIT_TEST_POINT(POSTUT_39)

   if ( FpuRegisterTest() != PASSED )
   {
       // $[TI1.1]
       return MAJOR;
   }
       // $[TI1.2]

   a=4.195835e06;
   b=3.145727e06;
   c=1.67;
   d=3.14;
   e=7.89876;

   // Perform some common floating point computations.
   g = a/b;          // Note - FP0 will contain extended precision
   h = c*d;
   i = -e;
   j = a+b-c;


UNIT_TEST_POINT(POSTUT_39_2)
   if (   ( a < b)
       || ( c > b)
   // The compiler uses a double precision comparison here, not extended.
   // Thus the actual answer of 1.33382044913624.. can only be approximated.
       || ( g < 1.3338204491362)
       || ( g > 1.3338204591363)
       || ( h < 5.2437)  //lower bound
       || ( h > 5.2439)  //upper bound
       || ( i < -7.89877)
       || ( i > -7.89875)
       || ( j < 7341560.32)
       || ( j > 7341562.34) )
   {
       // $[TI2.1]
      return MAJOR;
   }
       // $[TI2.2]

   register double z = 1.0;

UNIT_TEST_POINT(POSTUT_39_3)

   int ii;
   for ( ii = 0; ii < 2*NUM_DIGITS_ACCURACY; ii++)
   {
       a = 1.0;
       b = a - z;
       if (b >= a)
       {
           break;
       }
       z /= 10.0;
   }

   if ( ii != NUM_DIGITS_ACCURACY )
   {
       // $[TI3.1]
       return MAJOR;
   }
       // $[TI3.2]

   return PASSED;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MmuTest (void)
//
//@ Interface-Description
//
//  This routine exercises the MMU of the 68040.  If the MMU fails to
//  detect an illegal access (location 0), a MAJOR failure is returned.
//
//  Steps taken in the test:
//    1. Hook the MmuExceptionTestHandler.
//    2. Try to access address 0 to generate an access fault.
//    3. If the exception is generated then the test passes else the fails.
//
//  $[11085]
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
static volatile Boolean AccessFaultOccurred_;
static const Uint8 ACCESS_FAULT_VECTOR = 2;

Criticality
Cpu::MmuTest(void)
{
	//TODO E600 port this
   Criticality status = PASSED;

/*   DISABLE_INTERRUPTS();

   AccessFaultOccurred_ = FALSE;

UNIT_TEST_POINT(POSTUT_37)

   // install exception test handler
#ifdef E600_840_TEMP_REMOVED
   Handler old_isr = InstallHandler ( ACCESS_FAULT_VECTOR,
                                      Cpu::MmuExceptionTestHandler);
#endif

   // if access fault has already occurred then error
   if ( AccessFaultOccurred_ )
   {
       // $[TI1.1]
      status = MAJOR;
   }
   else
   {
       // $[TI1.2]
      // we use a write access since faulted read instructions
      // are restarted after the exception RTE
      *(volatile Uint*)NULL = 0;

   }

   // force bus synchronization before restoring handler

   //TODO E600 removed asm
   //asm(" nop ");

   // Restore the original handler
#ifdef E600_840_TEMP_REMOVED
   (void) InstallHandler( ACCESS_FAULT_VECTOR, old_isr );
#endif

   // major failure if handler not called
   if ( !AccessFaultOccurred_ )
   {
       // $[TI2.1]
      status = MAJOR;
   }
       // $[TI2.2]

   ENABLE_INTERRUPTS();*/

   return status;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExceptionTestHandler(void)
//
//@ Interface-Description
//  The MMU test installs this handler to process an access fault during
//  the test.  It simply sets a static variable to indicate the handler
//  processed the fault.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

//TODO E600 removed interrupt keyword
void
Cpu::MmuExceptionTestHandler (void)
{
   AccessFaultOccurred_ = TRUE;

   // $[TI1]
UNIT_TEST_POINT(POSTUT_37_1)
}
