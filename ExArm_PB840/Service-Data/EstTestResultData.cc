#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an processing retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//              Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================== C L A S S   D E S C R I P T I O N ================
//@ Class:  EstTestResultData -  Manage the EST service test data to be
//                            saved into or loaded from NovRam.
//---------------------------------------------------------------------
//@ Interface-Description
//>Von
// 
//  Description:
//  ------------
//    This class supports the following services for NovRam through the parent
//    class.
//    1. Loads the previous test result data from NovRam for upper level
//       application (GuiApp) usage.
//    2. Informs the test set completion to NovRam and updates the information.
//
//  Class Key Methods:
//  -------------------
//     1. loadDataFromNovRam -- Request NovRam to fill in the test data result.
//	   2. testCompleted -- Called by upper level applications to inform the
//        NovRam the completion of the test set.
//>Voff
//---------------------------------------------------------------------
//@ Rationale
//    The purpose of this derived class is to provide test specific functions
//    that have to be handle for the particular test type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  To support the information accessibility, EstTestResultData has two
//  important data members: testInfo_ and testDataArray_.
//  The 'testInfo_' is an array of items which knows about the 'count' of each
//  test ID and the starting address of the first data element within the
//  'testDataArray_'.
//  The 'testDataArray_' is an array of test data for Est tests.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/EstTestResultData.ccv   25.0.4.0   19 Nov 2013 14:21:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By:  yyy    Date:  30-JUL-97    DR Number: 2013,2205
//    Project:  Sigma (R8027)
//    Description:
//      Updated the data item for Nurse Call test, exh valve velocity transducer test.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "limits.h"
#include "EstTestResultData.hh"
#include "SmTestId.hh"
#include "NovRamManager.hh"
#include "FaultHandler.hh"

//@ Code...

//====================================================================
//
//  Public Methods...
//
//====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EstTestResultData()  [Default Constructor]
//
//@ Interface-Description
//  Initializes all the private and inherited member variables.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize testDataArray_ and testInfo_ array elements values. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EstTestResultData::EstTestResultData(void) :
		TestResultData(testInfo_, testDataArray_, NUM_SUMMARIZED_TEST_DATA_ITEM)
{
	CALL_TRACE("EstTestResultData::EstTestResultData(void)");

	initializeDataArray_();

	testInfo_[SmTestId::EST_TEST_GAS_SUPPLY_ID].pData = 
						&testDataArray_[0] + GAS_SUPPLY_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_GAS_SUPPLY_ID].count = 
					GAS_SUPPLY_TEST_END_INDEX - GAS_SUPPLY_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_GUI_KEYBOARD_ID].pData =		
						&testDataArray_[0] + GUI_KEYBOARD_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_GUI_KEYBOARD_ID].count =
					GUI_KEYBOARD_TEST_END_INDEX - GUI_KEYBOARD_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_GUI_KNOB_ID].pData			= 
						&testDataArray_[0] + GUI_KNOB_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_GUI_KNOB_ID].count = 
					GUI_KNOB_TEST_END_INDEX - GUI_KNOB_TEST_START_INDEX +1;
					
	testInfo_[SmTestId::EST_TEST_GUI_LAMP_ID].pData			= 
						&testDataArray_[0] + GUI_LAMP_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_GUI_LAMP_ID].count =
						GUI_LAMP_TEST_END_INDEX - GUI_LAMP_TEST_START_INDEX +1;
			
	testInfo_[SmTestId::EST_TEST_BD_LAMP_ID].pData			= 
						&testDataArray_[0] + BD_LAMP_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_BD_LAMP_ID].count =
						BD_LAMP_TEST_END_INDEX - BD_LAMP_TEST_START_INDEX +1;

    //To do E600 tests needs to be revisted for sound class
	testInfo_[SmTestId::EST_TEST_GUI_AUDIO_ID].pData			= 
						&testDataArray_[0] + GUI_AUDIO_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_GUI_AUDIO_ID].count = 
						GUI_AUDIO_TEST_END_INDEX - GUI_AUDIO_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_GUI_NURSE_CALL_ID].pData			= 
						&testDataArray_[0] + GUI_NURSE_CALL_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_GUI_NURSE_CALL_ID].count = 
						GUI_NURSE_CALL_TEST_END_INDEX - GUI_NURSE_CALL_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_BD_AUDIO_ID].pData			= 
						&testDataArray_[0] + BD_AUDIO_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_BD_AUDIO_ID].count = 
						BD_AUDIO_TEST_END_INDEX - BD_AUDIO_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_FS_CROSS_CHECK_ID].pData		= 
						&testDataArray_[0] + FS_CROSS_CHECK_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_FS_CROSS_CHECK_ID].count = 
					FS_CROSS_CHECK_TEST_END_INDEX - FS_CROSS_CHECK_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID].pData		= 
						&testDataArray_[0] + CIRCUIT_PRESSURE_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID].count = 
						CIRCUIT_PRESSURE_TEST_END_INDEX - 
						CIRCUIT_PRESSURE_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_SAFETY_SYSTEM_ID].pData		= 
						&testDataArray_[0] + SAFETY_SYSTEM_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_SAFETY_SYSTEM_ID].count =
					SAFETY_SYSTEM_TEST_END_INDEX - SAFETY_SYSTEM_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_EXH_VALVE_SEAL_ID].pData		= 
						&testDataArray_[0] + EXH_VALVE_SEAL_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_EXH_VALVE_SEAL_ID].count = 
					EXH_VALVE_SEAL_TEST_END_INDEX - EXH_VALVE_SEAL_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_EXH_VALVE_ID].pData			= 
						&testDataArray_[0] + EXH_VALVE_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_EXH_VALVE_ID].count = 
					EXH_VALVE_TEST_END_INDEX - EXH_VALVE_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_SM_LEAK_ID].pData				= 
						&testDataArray_[0] + SM_LEAK_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_SM_LEAK_ID].count = 
					SM_LEAK_TEST_END_INDEX - SM_LEAK_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_EXH_HEATER_ID].pData			= 
						&testDataArray_[0] + EXH_HEATER_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_EXH_HEATER_ID].count = 
					EXH_HEATER_TEST_END_INDEX - EXH_HEATER_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_GENERAL_ELECTRONICS_ID].pData	= 
						&testDataArray_[0] + GENERAL_ELECTRONICS_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_GENERAL_ELECTRONICS_ID].count = 
					GENERAL_ELECTRONICS_TEST_END_INDEX - GENERAL_ELECTRONICS_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_GUI_TOUCH_ID].pData			= 
						&testDataArray_[0] + GUI_TOUCH_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_GUI_TOUCH_ID].count = 
					GUI_TOUCH_TEST_END_INDEX - GUI_TOUCH_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_GUI_SERIAL_PORT_ID].pData		= 
						&testDataArray_[0] + GUI_SERIAL_PORT_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_GUI_SERIAL_PORT_ID].count = 
					GUI_SERIAL_PORT_TEST_END_INDEX - GUI_SERIAL_PORT_TEST_START_INDEX +1;

	testInfo_[SmTestId::EST_TEST_BATTERY_ID].pData				= 
						&testDataArray_[0] + BATTERY_TEST_START_INDEX;
	testInfo_[SmTestId::EST_TEST_BATTERY_ID].count = 
					BATTERY_TEST_END_INDEX - BATTERY_TEST_START_INDEX +1;

													// $[TI]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	initializeDataArray_ [private]
//
//@ Interface-Description
//  Called to set the initial values of elements of testDataArray_ 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the initial values of testDataArray_ elements to INT_MIN. 
//---------------------------------------------------------------------
//@ PreCondition
//	 none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================
void
EstTestResultData::initializeDataArray_(void)
{
	CALL_TRACE("EstTestResultData::initializeDataArray_(void)");

	// Initialize test data array
	for (int i=0; i<NUM_SUMMARIZED_TEST_DATA_ITEM; i++)
	{
    	testDataArray_[i] = INT_MIN;	// Minimum limit on integers, which is
										// the same size as floats on mc6800
	}												// $[TI1]
													// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~EstTestResultData  [Destructor]
//
//@ Interface-Description
//  Empty
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================

EstTestResultData::~EstTestResultData(void)
{
	CALL_TRACE("EstTestResultData::EstTestResultData(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	operator=
//
//@ Interface-Description
//  Assignment operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First, we copy the content of the testDataArray_ from the source object
//  to this object's data storage.  Then we copy the parent's member value.
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================
EstTestResultData& 
EstTestResultData::operator=(const EstTestResultData &estResultData)
{
	CALL_TRACE("EstTestResultData::operator=(const EstTestResultData &estResultData");
	if (this != &estResultData)
	{													// $[TI1]
		// Copying child's part
		for (int i = 0; i < NUM_SUMMARIZED_TEST_DATA_ITEM; i++)
		{												// $[TI1.1]
			testDataArray_[i] = estResultData.testDataArray_[i];
		}												// $[TI1.2]

		// Copying parent's part
		TestResultData::operator=(estResultData);
	}													// $[TI2]
	return(*this);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EstTestResultData()  [Copy Constructor]
//
//@ Interface-Description
//  Copy member values from the given object to the current object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Invoke the assignment operator with the given object.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================

EstTestResultData::EstTestResultData(
								const EstTestResultData & estTestResultData) :
		TestResultData((TestDataStruct *)estTestResultData.testInfo_, 
					   (Real32 *)estTestResultData.testDataArray_, 
						(Int32)NUM_SUMMARIZED_TEST_DATA_ITEM)
{
	CALL_TRACE("EstTestResultData::EstTestResultData(const EstTestResultData"
					"&estTestResultData");
	operator=(estTestResultData);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loadDataFromNovRam 
//
//@ Interface-Description
//	 Called by GUI-Applications to load the test result data from NovRam
//	 into the current TestResultData object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call NovRam method to update local stack with latest test data. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void 
EstTestResultData::loadDataFromNovRam(void)
{
	CALL_TRACE("EstTestResultData::loadDataFromNovRam(void)");

#if defined (SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
	// Call method from NovRam to get test data
	NovRamManager::GetEstTestResultData(*this);
														// $[TI1]
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	testCompleted 
//
//@ Interface-Description
//  Called by Service-Data when an EST test is complete.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call update() method and write data for this test to NovRam. 
//---------------------------------------------------------------------
//@ PreCondition
//	 none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void 
EstTestResultData::testCompleted()
{
	CALL_TRACE("EstTestResultData::testCompleted()");
	
#if defined (SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
	// Call update method from NovRam to update its test data
	NovRamManager::UpdateEstTestResultData(*this);
														// $[TI1]
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                      [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
EstTestResultData::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
	CALL_TRACE("AlarmMsg::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_DATA, EST_TEST_RESULT_DATA, 
							lineNumber, pFileName, pPredicate);
}
