#ifndef TrendDbConfig_HH
#define TrendDbConfig_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbConfig 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbConfig.hhv   25.0.4.0   19 Nov 2013 14:34:36   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  mjf    Date:  18-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
// 
//=====================================================================

#include "TrendSelectValue.hh"  // Setting-Validation

#include "TrendDataMgr.hh"


// forward declare
class TrendDbSampleData;


class TrendDbConfig
{
public:

	//@ Type: ArraySize
	// Array size for the sample data map
	// Value chosen to be the worst case max size of a repository
	enum ArraySize
	{
		MAX_CONFIG_SIZE = 100
	};

	TrendDbConfig( Uint8 maxSlotNumber );
	~TrendDbConfig( void );

	void configSlot( TrendDbSampleData * ptrSampleData, Uint8 slotNumber );

	inline TrendDbSampleData * getSampleData( Uint8 slotNumber ) const;
	inline Int16 getSlotNumber( Int16 trendId ) const;
	inline Uint8 getMaxSlotNumber( void ) const;


	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:

	//@ Data-Member: maxSlotNumber_
	//  Number of slots this database configuration can support
	//  must be less then the MAX_CONFIG_SIZE
	Uint8 maxSlotNumber_;

	//@ Data-Member: sampleDataMap
	//  A map from slot number to sample data pointer       
	TrendDbSampleData *  sampleDataMap_[ MAX_CONFIG_SIZE ];

	//@ Data-Member: trendIdMap
	// map a TrendSelctValueId to a slot number
	Int16 trendIdMap_[ TrendDataMgr::MAX_TREND_ID ];

	// no copy or assignment constructor
	TrendDbConfig( const TrendDbConfig& );		// not implemented
	void operator=( const TrendDbConfig& );		// not implemented

};

// Inlined methods
#include "TrendDbConfig.in"

#endif // TrendDbConfig_HH
