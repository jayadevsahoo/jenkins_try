#ifndef AlarmVolumeHandler_HH
#define AlarmVolumeHandler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmVolumeHandler - Handles setting of Alarm Volume
// (initiated by the Alarm Volume off-screen key).
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmVolumeHandler.hhv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc   Date:  09-May-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added getTargetType to identify feedback sensitive focus.
//
//  Revision: 004   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Adding Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 003  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "AdjustPanelTarget.hh"
#include "KeyPanelTarget.hh"
#include "SettingObserver.hh"
#include "GuiApp.hh"

//@ Usage-Classes
//@ End-Usage

class AlarmVolumeHandler : public AdjustPanelTarget, public SettingObserver,
							public KeyPanelTarget
{
public:
	AlarmVolumeHandler(void);
	~AlarmVolumeHandler(void);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelKnobDeltaHappened(Int32 delta);
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);
	virtual void adjustPanelLoseFocusHappened(void);
	virtual AdjustPanelTarget::TargetType getTargetType(void) const;

	// SettingObserver virtual method
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
							  const SettingSubject*               pSubject);

	// Key Panel Target virtual methods
	virtual void keyPanelPressHappened(KeyPanel::KeyId key);
	virtual void keyPanelReleaseHappened(KeyPanel::KeyId key);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	AlarmVolumeHandler(const AlarmVolumeHandler&);		// not implemented...
	void   operator=(const AlarmVolumeHandler&);		// not implemented...
};


#endif // AlarmVolumeHandler_HH
