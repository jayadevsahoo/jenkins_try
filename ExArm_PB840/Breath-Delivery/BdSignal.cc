#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BdSignal - Set of signals sent over the ethernet.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class collects signals and sends out the data over the ethernet.
//---------------------------------------------------------------------
//@ Rationale
//	The signals sent out can be received by an external device to examine
//	the internal signals of the ventilator to aid in debuggung and design
//	validation.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Methods to collect and send the data are provided.  The data is sent
//	over the ethernet when the buffer is full.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdSignal.ccv   25.0.4.0   19 Nov 2013 13:59:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc   Date:  21-Jan-2011    SCR Number: 6733
//  Project:  PROX
//  Description:
//      Added VCO2 study code for development reference.
//
//  Revision: 005   By: rhj   Date:  05-May-2010    SCR Number: 6436 
//  Project:  PROX
//  Description:
//       Added ProxiInterface data items.
//
//  Revision: 004   By: rhj   Date:  19-Jan-2009    SCR Number: 6455 
//  Project:  840S
//  Description:
//		 Changed RLeakCompMgr.getPressureSlopeInh1() to 
//       RLeakCompMgr.getPressureSlope1() and RLeakCompMgr.getFlowSlopeInh1()
//       to RLeakCompMgr.getFlowSlope1();
// 
//  Revision: 003   By:   rhj    Date: 07-July-2008      SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 002   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 001  By:  syw    Date:  18-Sep-2000    DR Number: 5755
//       Project:  VTPC
//       Description:
//             Initial version
//
//=====================================================================

#include "BdSignal.hh"

//@ Usage-Classes

#include "MainSensorRefs.hh"
#include "ControllersRefs.hh"
#include "PhaseRefs.hh"
#include "MiscSensorRefs.hh"
#include "ValveRefs.hh"
#include "SmRefs.hh"
#include "VentObjectRefs.hh"
#include "RegisterRefs.hh"

#include "BreathPhaseScheduler.hh"
#include "PhasedInContextHandle.hh"
#include "SchedulerId.hh"
#include "SupportTypeValue.hh"
#include "Sensor.hh"
#include "FlowController.hh"
#include "PressureController.hh"
#include "BreathPhase.hh"
#include "Psol.hh"
#include "BdQueuesMsg.hh"
#include "MsgQueue.hh"
#include "BreathRecord.hh"
#include "TemperatureSensor.hh"
#include "TaskControlAgent.hh"
#include "SmFlowController.hh"
#include "SmPressureController.hh"
#include "ExhFlowSensor.hh"
#include "LungData.hh"
#include "PavPhase.hh"
#include "PavManager.hh"
#include "MandTypeValue.hh"
#include "ExhalationValve.hh"
#include "VolumeTargetedController.hh"
#include "PeepController.hh"
#include "LeakCompEnabledValue.hh"
#include "ExhalationPhase.hh"
#include "LeakCompMgr.hh"
#include "BdTasks.hh"
#include "Barometer.hh"
//@ End-Usage

//prox testing 
#include "ProxiInterface.hh"
#include "ProxManager.hh"
extern ProxiInterface* PProxiInterface;

//@ Code...

SmartPacket BdSignal::SData_[2] ;
Int32 BdSignal::SDataCollectIndex_ = 0 ;
Int32 BdSignal::SDataSendIndex_ = 1 ;
Int32 BdSignal::Index_ = 0 ;
Boolean BdSignal::IsTransmitInProg_ = FALSE ;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Collect
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called every BD cycle to collect the signals and to send the data
//		over the ethernet.
//---------------------------------------------------------------------
//@ Implementation-Description
//		A double buffer is used to collect and send the signals.  Based
//		on what scheduler and option is active, the signals are defined.
//		When a buffer is full, the other buffer is used to store while
//		the full buffer gets ready to send by placing a message on the
//		BD_GRAPHICS queue.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BdSignal::Collect(void) 
{
    CALL_TRACE("BdSignal::Collect(void)") ;

    if (Index_ < MAX_SIZE)
    {
        // $[TI1]
        const SchedulerId::SchedulerIdValue schedulerId =
        BreathPhaseScheduler::GetCurrentScheduler().getId();

        DiscreteValue supportType =
        PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE);

        DiscreteValue mandType = PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);

        DiscreteValue  leakCompEnabledValue =
        PhasedInContextHandle::GetDiscreteValue(SettingId::LEAK_COMP_ENABLED);

        if (leakCompEnabledValue == LeakCompEnabledValue::LEAK_COMP_ENABLED &&
            TaskControlAgent::GetBdState() == ::STATE_ONLINE)
        {

            Real32 geTmaxQe(void);
            Real32 getTMaxQ (void);
            Real32 getSlowSlopeMax(void);

            SData_[SDataCollectIndex_].signal[0][Index_]  = ((Sensor&)RO2FlowSensor).getValue(); 
            SData_[SDataCollectIndex_].signal[1][Index_]  = ((Sensor&)RAirFlowSensor).getValue();   
            SData_[SDataCollectIndex_].signal[2][Index_]  = RExhFlowSensor.getValue();   

            SData_[SDataCollectIndex_].signal[3][Index_]  = ((Sensor&)RInspPressureSensor).getValue();
            SData_[SDataCollectIndex_].signal[4][Index_]  = ((Sensor&)RExhPressureSensor).getValue();
			//TODO BVP - E600 Looks like some APIs are not supported.
#if 0
			SData_[SDataCollectIndex_].signal[5][Index_]  =  RLeakCompMgr.getMaxQe();
            SData_[SDataCollectIndex_].signal[6][Index_]  =  RLeakCompMgr.getTMaxQ(); 
            SData_[SDataCollectIndex_].signal[7][Index_]  = RLeakCompMgr.getFlowSlopeMax();
#endif
			SData_[SDataCollectIndex_].signal[8][Index_]  = RLeakCompMgr.getPressureSlope1();
			//TODO BVP - E600 Looks like some APIs are not supported.
#if 0
            SData_[SDataCollectIndex_].signal[9][Index_]  = RLeakCompMgr.getQslope();
#endif
			SData_[SDataCollectIndex_].signal[10][Index_] = RLeakCompMgr.getFlowSlope1();

            SData_[SDataCollectIndex_].signal[11][Index_] = RLeakCompMgr.getAvgSteadyLeakRate();
			//TODO BVP - E600 Looks like some APIs are not supported.
#if 0
            SData_[SDataCollectIndex_].signal[12][Index_] = BdTasks::getCycleTimeUs();
            SData_[SDataCollectIndex_].signal[13][Index_] = BdTasks::getPeakCycle();
#endif
			SData_[SDataCollectIndex_].signal[14][Index_] = RLeakCompMgr.getNetVolume();
			//TODO BVP - E600 Looks like some APIs are not supported.
#if 0
            SData_[SDataCollectIndex_].signal[15][Index_] = RLeakCompMgr.getCurrentNetFlow();
            SData_[SDataCollectIndex_].signal[16][Index_] = RLeakCompMgr.getTau(); 
#endif
			SData_[SDataCollectIndex_].signal[17][Index_] =(Real32) BreathRecord::GetPhaseType();

        }
        else if (schedulerId == SchedulerId::SPONT && supportType == SupportTypeValue::PAV_SUPPORT_TYPE)
        {
            // $[TI2]
            const Real32 CMH20_PER_LPM_TO_CMH20_PER_LPS = 60.0 ;
            Real32 lungFlow = ABS_VALUE(RLungData.getLungFlow()) ;

            // avoid divide by zero
            if (lungFlow < 1.0)
            {
                // $[TI3]
                lungFlow = 1.0 ;
            }
            // $[TI4]

            Real32 rEt = RTube.getPressDrop(lungFlow) / lungFlow ;

            SData_[SDataCollectIndex_].signal[0][Index_] = ((Sensor&)RAirFlowSensor).getValue() ;   
            SData_[SDataCollectIndex_].signal[1][Index_] = RExhFlowSensor.getDryValue() ;
            SData_[SDataCollectIndex_].signal[2][Index_] = RLungData.getLungFlow() ;
            SData_[SDataCollectIndex_].signal[3][Index_] = ((Sensor&)RInspPressureSensor).getValue();
            SData_[SDataCollectIndex_].signal[4][Index_] = ((Sensor&)RExhPressureSensor).getValue();
            SData_[SDataCollectIndex_].signal[5][Index_] = BreathRecord::GetPressWyeInspEst() ;
            SData_[SDataCollectIndex_].signal[6][Index_] = RAirFlowController.getDesiredFlow() ;
            SData_[SDataCollectIndex_].signal[7][Index_] = RPavPhase.getElasticPressure()
                                                           + RPavPhase.getResistivePressure() ;
            SData_[SDataCollectIndex_].signal[8][Index_] = RPavPhase.getElasticPressure() ;
            SData_[SDataCollectIndex_].signal[9][Index_] = RPavPhase.getResistivePressure() ;
            SData_[SDataCollectIndex_].signal[10][Index_] = RPavManager.getCraw() ;
            SData_[SDataCollectIndex_].signal[11][Index_] = 1.0f / RPavManager.getEPatient( PavState::CLOSED_LOOP) ;
            SData_[SDataCollectIndex_].signal[12][Index_] = RPavManager.getRraw() * CMH20_PER_LPM_TO_CMH20_PER_LPS ;
            SData_[SDataCollectIndex_].signal[13][Index_] = rEt * CMH20_PER_LPM_TO_CMH20_PER_LPS ;
            SData_[SDataCollectIndex_].signal[14][Index_] = RPavManager.getRPatient( PavState::CLOSED_LOOP) * CMH20_PER_LPM_TO_CMH20_PER_LPS ;
            SData_[SDataCollectIndex_].signal[15][Index_] = (Real32) BreathRecord::GetPhaseType() ;
            SData_[SDataCollectIndex_].signal[16][Index_] = RPavManager.getPatientResistiveWob() ;
            SData_[SDataCollectIndex_].signal[17][Index_] = RPavManager.getPatientElastanceWob() ;
        }
        else if ((schedulerId == SchedulerId::AC || schedulerId == SchedulerId::SIMV) &&
                 mandType == MandTypeValue::VCP_MAND_TYPE ||
                 schedulerId == SchedulerId::SPONT && supportType == SupportTypeValue::VSV_SUPPORT_TYPE)
        {
            // $[TI5]
            SData_[SDataCollectIndex_].signal[0][Index_] = ((Sensor&)RO2FlowSensor).getValue();   
            SData_[SDataCollectIndex_].signal[1][Index_] = ((Sensor&)RAirFlowSensor).getValue();   
            SData_[SDataCollectIndex_].signal[2][Index_] = RExhFlowSensor.getValue();   
            SData_[SDataCollectIndex_].signal[3][Index_] = ((Sensor&)RInspPressureSensor).getValue();
            SData_[SDataCollectIndex_].signal[4][Index_] = ((Sensor&)RExhPressureSensor).getValue();
            SData_[SDataCollectIndex_].signal[5][Index_] = RO2FlowController.getDesiredFlow();
            SData_[SDataCollectIndex_].signal[6][Index_] = RAirFlowController.getDesiredFlow();

            BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase() ;

            if (pBreathPhase->getPhaseType() == BreathPhaseType::INSPIRATION ||
                pBreathPhase->getPhaseType() == BreathPhaseType::PAV_INSPIRATORY_PAUSE ||
                pBreathPhase->getPhaseType() == BreathPhaseType::INSPIRATORY_PAUSE)
            {
                // $[TI6]
                SData_[SDataCollectIndex_].signal[7][Index_] = RPressureController.getDesiredPressure();
            }
            else
            {
                // $[TI7]
                SData_[SDataCollectIndex_].signal[7][Index_] = RExhalationValve.getPressureCommand() ;
            }

            SData_[SDataCollectIndex_].signal[8][Index_] = BreathRecord::GetPressWyeInspEst() ;
            SData_[SDataCollectIndex_].signal[9][Index_] = RPeepController.getFeedbackPressure() ;
            SData_[SDataCollectIndex_].signal[10][Index_] = RLungData.getInspLungBtpsVolume() ;
            SData_[SDataCollectIndex_].signal[11][Index_] = RLungData.getExhLungBtpsVolume() ;
            SData_[SDataCollectIndex_].signal[12][Index_] = RLungData.getLungFlow() ;
            SData_[SDataCollectIndex_].signal[13][Index_] = 0.0 ;
            SData_[SDataCollectIndex_].signal[14][Index_] = RExhalationValve.getPsolCommand();
			SData_[SDataCollectIndex_].signal[15][Index_] = RVolumeTargetedController.getCPatient() ;
            SData_[SDataCollectIndex_].signal[16][Index_] = RVolumeTargetedController.getCPatientFiltered() ;
            SData_[SDataCollectIndex_].signal[17][Index_] = (Real32) BreathRecord::GetPhaseType() ;
        }
        else
        {
            // $[TI8]
            SData_[SDataCollectIndex_].signal[0][Index_] = ((Sensor&)RO2FlowSensor).getValue();   
            SData_[SDataCollectIndex_].signal[1][Index_] = ((Sensor&)RAirFlowSensor).getValue();   
            SData_[SDataCollectIndex_].signal[2][Index_] = RExhFlowSensor.getValue();

            SData_[SDataCollectIndex_].signal[3][Index_] = ((Sensor&)RInspPressureSensor).getValue();
            SData_[SDataCollectIndex_].signal[4][Index_] = ((Sensor&)RExhPressureSensor).getValue();
			SData_[SDataCollectIndex_].signal[5][Index_] = RExhalationValve.getPsolCommand();

			SData_[SDataCollectIndex_].signal[6][Index_] = ((Sensor&)RFio2Monitor).getCount();
            SData_[SDataCollectIndex_].signal[7][Index_] = RAtmosphericPressureSensor.getCount();


            SData_[SDataCollectIndex_].signal[8][Index_] = RAirFlowController.getDesiredFlow();
            SData_[SDataCollectIndex_].signal[9][Index_] = RO2FlowController.getDesiredFlow();
            SData_[SDataCollectIndex_].signal[10][Index_] = RPressureController.getDesiredPressure();

            SData_[SDataCollectIndex_].signal[11][Index_] = RBreathData.getExhaledTidalVolume();
            SData_[SDataCollectIndex_].signal[12][Index_] = RBreathData.getInspiredLungVolume();
			//TODO BVP - E600 Looks like some APIs are not supported.
#if 0
            SData_[SDataCollectIndex_].signal[13][Index_] = REVDampingGainDacPort.getRegisterImage();
#endif

			//TODO BVP - E600 Looks like some APIs are not supported.
#if 0
            SData_[SDataCollectIndex_].signal[14][Index_] = RExhValveDacPort.getRegisterImage() ;
#endif
			SData_[SDataCollectIndex_].signal[15][Index_] = RVolumeTargetedController.getCPatient() ;
            SData_[SDataCollectIndex_].signal[16][Index_] = RVolumeTargetedController.getCPatientFiltered() ;
            SData_[SDataCollectIndex_].signal[17][Index_] = (Real32) BreathRecord::GetPhaseType();

        }


        if (PProxiInterface)
        {
            ProxiInterface& rProxiInterface = *PProxiInterface;

#if defined(VCO2_STUDY)
            SData_[SDataCollectIndex_].signal[0][Index_]  =  rProxiInterface.getFlow();
            SData_[SDataCollectIndex_].signal[1][Index_]  =  rProxiInterface.getCO2();
            SData_[SDataCollectIndex_].signal[2][Index_]  =  rProxiInterface.getVolumeCO2ExpiredPerMinute();  // VCO2
            SData_[SDataCollectIndex_].signal[3][Index_]  =  rProxiInterface.getEndTidalCO2();
            SData_[SDataCollectIndex_].signal[4][Index_]  =  rProxiInterface.getInspiredCO2();
            SData_[SDataCollectIndex_].signal[5][Index_]  =  rProxiInterface.getMixedExpiredCO2(); //PeCO2
            SData_[SDataCollectIndex_].signal[6][Index_]  =  rProxiInterface.getBarometricPressure();
            SData_[SDataCollectIndex_].signal[7][Index_]  =  rProxiInterface.getStartOfInspirationMark();
            SData_[SDataCollectIndex_].signal[8][Index_]  =  rProxiInterface.getStartOfExpirationMark();
            SData_[SDataCollectIndex_].signal[9][Index_]  =  rProxiInterface.getInspVolume();
            SData_[SDataCollectIndex_].signal[10][Index_] =  rProxiInterface.getExhVolume();
            SData_[SDataCollectIndex_].signal[11][Index_] =  rProxiInterface.getInspiredTime();
            SData_[SDataCollectIndex_].signal[12][Index_] =  rProxiInterface.getExpiredTime();
            SData_[SDataCollectIndex_].signal[13][Index_] =  RLungData.getLungFlow();
            SData_[SDataCollectIndex_].signal[14][Index_] = (Real32) BreathRecord::GetPhaseType();
            SData_[SDataCollectIndex_].signal[15][Index_] =  RAtmosphericPressureSensor.getValue() / 1.36; // mmHg
            SData_[SDataCollectIndex_].signal[16][Index_] =  Real32(rProxiInterface.getCapnostatStatus());
            SData_[SDataCollectIndex_].signal[17][Index_] =  Real32(rProxiInterface.getDataValid());
#else
            SData_[SDataCollectIndex_].signal[0][Index_] =  (Real32) RProxManager.getBreathPhase();
            SData_[SDataCollectIndex_].signal[5][Index_] =  rProxiInterface.getFlow();
            SData_[SDataCollectIndex_].signal[6][Index_] =  rProxiInterface.getPressure();
            //SData_[SDataCollectIndex_].signal[7][Index_] =  rProxiInterface.getVolume();
            //SData_[SDataCollectIndex_].signal[8][Index_] =  RProxManager.getInspiredVolume();
            //SData_[SDataCollectIndex_].signal[9][Index_] =  RProxManager.getExpiredVolume();
#endif
        }

        Index_++;
    }
    // $[TI13]

    if (Index_ >= MAX_SIZE)
    {
        // $[TI14]
        // avoid changing buffers if we are in the middle of transmitting
        if (!IsTransmitInProg_)
        {
            // $[TI15]
            Int32 temp = SDataSendIndex_ ;
            SDataSendIndex_ = SDataCollectIndex_ ;
            SDataCollectIndex_ = temp ;
            BdQueuesMsg msg ;
            msg.eventStatusEvent.eventType = BdQueuesMsg::BD_GRAPHICS ;
            MsgQueue::PutMsg (BD_STATUS_TASK_Q, (Int32)msg.qWord) ;
            Index_ = 0;
        }
        // $[TI16]
    }
    // $[TI17]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Send
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called to send the signals over the ethernet.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The data in the full buffer is sent.  IsTransmitInProg_ is used
//		to prevent the data being sent over from being over written since
//		this method is called from a lower priority task.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
BdSignal::Send(void) 
{
    CALL_TRACE("BdSignal::Send(void)") ;

    // $[TI1]
    IsTransmitInProg_ = TRUE ;
	//Convert to network order before sending. Note that this feasible to it here
	//only because we know the buffer consists entirely of Real32 numbers. If it
	//was a mix or 32 and 16 bit numbers, this wouldnt work. This is a safe thing to
	//do in big-endian or little-endian hardware. At the end of the loop, the buffer
	//will have all Real32's in network byte order on all hardware architectures.
	Uint32* buffer = (Uint32*)&SData_[SDataSendIndex_];
	SocketWrap sock;
	for(int i=0; i < sizeof(SmartPacket)/sizeof(Real32); i++)
	{
		*buffer = sock.HTONL(*buffer);
		buffer++;
	}

    NetworkApp::BroadcastMsg( BD_DEBUG_MSG , (void*)&SData_[SDataSendIndex_],
                              sizeof(SmartPacket), BD_DEBUG_DEST_UDPPORT0) ;
    IsTransmitInProg_ = FALSE ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BdSignal::SoftFault( const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
                     const char*        pFileName,
                     const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, BDSIGNAL,
                             lineNumber, pFileName, pPredicate) ;
}


//====================================================================
//
//  Private Methods...
//
//====================================================================

