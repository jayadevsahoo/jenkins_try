#ifndef MemoryMap_HH
# define MemoryMap_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ====================================================================
//@Filename: MemoryMap.hh - Sigma Hardware Memory Map Definitions.
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/POST-Library/vcssrc/MemoryMap.hhv   25.0.4.0   19 Nov 2013 14:16:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005    By: gdc           Date: 24-Jan-2011  SCR Number: 6671
//    Project: XENA2
//    Description:
//       Added REV_CTRL_REG register definitions. XENA_II etc... Modified
//       for new TOD clock chip ST M48T58.
//
//  Revision: 004    By: Mitesh Raval  Date: 02-Dec-2010  SCR Number: 6671
//    Project: XENA2
//    Description:
//       Added REV_CTRL_REG.
//       Added FPGA_CTR location.
//       Added BQ4822Y RTC base location.
//
//  Revision: 003    By: Gary Cederquist  Date: 09-JUN-1997  DR Number: 1902
//    Project: Sigma (R8027)
//    Description:
//       Added flash serial number location.
//
//  Revision: 002    By: Gary Cederquist  Date: 08-APR-1997  DR Number: 1901
//    Project: Sigma (R8027)
//    Description:
//       Added NOVRAM base and length symbols.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

typedef void (*EntryPtr)(void);

//TODO E600 VM: trace all instances of this preprocessor (E600_840_TEMP_REMOVED)
// and resolve the final condition of it.
#ifdef E600_840_TEMP_REMOVED

#define DIAG_LED_REG            ((Uint8 *) 0xFFBE800C)

                // **  Operating-System
#define IO_REGISTER_1           ((volatile Uint8 *) 0xFFBE8000)

// I/O Register 1 definitions
#define LAN_RESET_BIT           (0x10)
#define AUDIO_RESET_BIT         (0x20)
#define REMOTE_ALARM_BIT        (0x40)
#define BD_CPU_BIT              (0x80)

#define IO_REGISTER_2           ((volatile Uint8 *) 0xFFBE8001)

// I/O Register 2 definitions
#define AUDIO_BUSY_BIT          (0x01)
#define AUDIO_ACK_BIT           (0x80)
#define AUDIO_ATTN_BIT          (0x20)

#define NMI_REGISTER            ((volatile Uint8 *) 0xFFBE8008)
#define REV_CTRL_REG			((volatile Uint8 *) 0xFFBE801C)

// if revControlIOAddress equals to 0x30 then it is a
// "XENA plus Compact Flash card"
// ",temperature sensor and fan tachometer!"
#define XENA_MICRON_FLASH       (0x30)

// if revControlIOAddress equals to 0x31 then it is a
// "XENA with Common Flash Interface-style"
// "(Spansion) flash instead of baseline (Micron-style) flash."
#define XENA_SPANSION_FLASH     (0x31)

// if revControlIOAddress equals to 0x32 then it is a
// XENA2 = "XENA plus RTC for BD"
// XENA2 = "XENA plus RTC and Scarlet VGA chip for GUI"
#define XENA_II       			(0x32)


#define I82C541_BASE            (0xFFBE8011)
#define DS1286_BASE             (0xFFBE8040)
#define STM48T58_BASE           (0xFFBE8078)
#define M68HC901_BASE           (0xFFBE8080)

//This is only available in Xena2 or later
#define FPGA_CTR                ((volatile Uint32 *)(0xFFBE9010))

#define DRAM_BASE               (0x0)
#define DRAM_LENGTH             (0x400000)
#define APP_DRAM_BASE           (0x8000)
#define APP_DRAM_LENGTH         (DRAM_LENGTH-APP_DRAM_BASE)

#define BOOT_PROM_BASE          (0xFF000000)
#define BOOT_PROM_LENGTH        (0x80000)
#define BOOT_PROM_TOC           (0xFF07FF00)
#define BOOT_PROM_CHECKSUM      ((Uint32*) 0xFF07FFFC)

// addresses for PBMON in flash
#define PBMON_MAGIC_COOKIE_MEM  ((Uint32*) 0xFF640400)
#define PBMON_MAGIC_COOKIE      (0xC0DEF00D)
#define PBMON_ENTRY             (0xFF640800)

// AutoVector Reset locations
#define AUTOVECTOR_3_RESET_REG  ((Uint8 *) 0xFFBE8030)
#define AUTOVECTOR_4_RESET_REG  ((Uint8 *) 0xFFBE8031)
#define AUTOVECTOR_6_RESET_REG  ((Uint8 *) 0xFFBE8032)
#define AUTOVECTOR_5_RESET_REG  ((Uint8 *) 0xFFBE8033)

// DRAM parity test circuit register
#define WRITE_WRONG_PARITY_REG  ((Uint8 *) 0xFFBE8034)

// Safety Net registers
#define SAFETY_NET_CTL_A        ((Uint8 *) 0xFFBEB008)
#define SAFETY_NET_CTL_B        ((Uint8 *) 0xFFBEB00A)

#define AUTOVECTOR_ENABLE_REG   ((Uint8 *) 0xFFBE802C)
#define ENABLE_AUTOVEC_NONE     (0x00)
#define ENABLE_AUTOVEC_3        (0x01)
#define ENABLE_AUTOVEC_4        (0x02)
#define ENABLE_AUTOVEC_5        (0x04)
#define ENABLE_AUTOVEC_6        (0x08)

// Watchdog addresses
#define WATCHDOG_STROBE_REG     ((Uint8 *) 0xFFBE9000)
#define WATCHDOG_BITE_CLEAR_REG ((Uint8 *) 0xFFBE803C)


// GUI specific defines - defined for both since they referenced in POST

#define GUI_IO_BASE             (0xFFBEA000)

#define FP_LED                  ((Uint16 *)(GUI_IO_BASE + 0x04))

#define KEYS_LED_1              ((Uint8 *) (GUI_IO_BASE + 0x32))
#define KEYS_1                  ((volatile Uint8 *) (GUI_IO_BASE + 0x30)) //MSB
#define KEYS_2                  ((volatile Uint8 *) (GUI_IO_BASE + 0x31)) //LSB

#define KNOB_HIGH               ((volatile Uint8 *) (GUI_IO_BASE + 0x08))
#define KNOB_LOW                ((volatile Uint8 *) (GUI_IO_BASE + 0x09))
#define KNOB_RESET              ((Uint8 *) (GUI_IO_BASE + 0x0A))

#define TOUCH_CMD               ((Uint8 *) (GUI_IO_BASE + 0x00))
#define TOUCH_REPORT            ((volatile Uint8 *) (GUI_IO_BASE + 0x00))
#define TOUCH_RESET             ((Uint8 *) (GUI_IO_BASE + 0x02))
#define TOUCH_STAT              ((volatile Uint8 *) (GUI_IO_BASE + 0x01))

                // **  VGA-Graphics-Driver
// lower display is IP A: mapped at 8MB beginning at 0x2000000;
// upper display is IP B: mapped at 8MB beginning at 0x2800000
#define REG_BASE0               (0x02000000)    // lower display base; Video RAM for Scarlet lower
#define REG_BASE1               (0x02800000)    // upper display base; Video RAM for Scarlet upper
#define VGA_MEM_SIZE            (0x200000)      // includes regs + vRAM

#define SCAR_HOST_BASE0         (0x027C0000)    // Scarlet lower display base
#define SCAR_HOST_BASE1         (0x02FC0000)    // Scarlet upper display base


#define CONTRAST                ((Uint16 *) (GUI_IO_BASE + 0x0C))
#define BACKLIGHT               ((Uint8 *)  (GUI_IO_BASE + 0x10))
#define BLINK                   (0xFFBEA032)

#endif

#endif // MemoryMap_HH
