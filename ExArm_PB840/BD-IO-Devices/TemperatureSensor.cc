#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TemperatureSensor - Defines the implementation for all
//      temperature sensors.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class is derived from the Sensor class.  It contains Sensor class
//		specialization for a temperature sensor.  A method is implemented
//		to convert the AdcCounts to temperature in degrees celsius.
//
//		This class provides support for the BD_CPU and the GUI_CPU.
//---------------------------------------------------------------------
//@ Rationale
//      Abstraction of all temperature sensors.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Each temperature sensor uses one of the temperature
//      conversion tables.  A pointer to the correct conversion
//      table is setup during construction.  This pointer is used
//      by rawToEngValue_() to convert the ADC counts into an engineering
//      value.  Two conversion tables exist, one for the three flow sensors,
//		and a second for the exh valve and exh heater temperature sensors.
//
// $[00401]
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/TemperatureSensor.ccv   25.0.4.0   19 Nov 2013 13:54:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 011 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 010  By: syw     Date:  16-Sep-1997    DR Number: 2427
//  	Project:  Sigma (840)
//		Description:
//			Added requirement traceability.
//
//	Revision: 009  By:  syw    Date:  28-Jul-1997    DR Number: DCS 2327
//      Project:  Sigma (R8027)
//      Description:
//			Initialize Alpha_ data member.  Define tau values.
//
//  Revision: 008 By: syw    Date: 07-Jul-1997   DR Number: 2283
//  	Project:  Sigma (R8027)
//			Define updateValue() on BD_CPU to process inverse alpha filters.
//			Added tempOffset_ data member.
//
//  Revision: 007 By: syw    Date: 03-Mar-1997   DR Number: DCS 1780, 1827
//  	Project:  Sigma (R8027)
//			Eliminate modified temperature.  Delete updateValue() on BD_CPU
//
//  Revision: 006 By: syw    Date: 25-Feb-1997   DR Number: DCS 1795, 1827
//  	Project:  Sigma (R8027)
//		Description:
//			Flow temperature sensor table is now larger, check pointer
//			to determine which algorithm to use.  Temperature sensors are
//			computed as before except for the exh flow temperature
//			sensor which uses a fixed temperature that is obtained near
//			the end of exhalation.
//
//  Revision: 005 By: syw    Date: 22-Aug-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate compiler warnings.
//
//  Revision: 004 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added BD_CPU macros.  Added constructor for GUI CPU.
//
//  Revision: 003 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added updateValue() with counts as and argument for TUV support.
//
//  Revision: 002  By: syw    Date: 05-Dec-1995   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added interface in constructor for SafetyNetSensorData *.
//			Eliminate the need to linear interpolate after the data is obtained
//			from the table since the table is built with the resolution
//			required.  Round the index to nearest integer.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
// TODO E600_LL:  TemperatureSensor is not currently used; will need to be reviewed

#include "TemperatureSensor.hh"

#include "MainSensorRefs.hh"
#include "TemperatureConversionTable.hh"

//@ Usage-Classes

#ifdef SPIRO_DEBUG

#include "PrintQueue.hh"
#include <stdio.h>
static char string[80] ;
#include "TaskControlAgent.hh"

#endif // SPIRO_DEBUG

//@ End-Usage

//@ Code...

#if defined (SIGMA_BD_CPU)

Real32 TemperatureSensor::Alpha_ = TRIGGER_ALPHA ;

#endif // defined (SIGMA_BD_CPU)

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TemperatureSensor ()
//
//@ Interface-Description
//      Constructor.  The arguments required are an ADC channel ID, a pointer to
//		SafetyNetSensorData, and a pointer to the thermistor conversion
//		table for the BD_CPU and a pointer to the thermistor conversion
//		for the GUI_CPU.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The arguments passed in are used to initialize the corresonding
//		data members.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

TemperatureSensor::TemperatureSensor( SafetyNetSensorData *pData,
									  const AdcChannels::AdcChannelId id,
									  const Real32* pThermistorTable)
: Sensor(id, pData), pVoltTemperatureTable_(pThermistorTable)	// $[TI1]
{
	CALL_TRACE("TemperatureSensor::TemperatureSensor( \
				SafetyNetSensorData *pData, \
				const AdcChannels::AdcChannelId id, \
				 const Real32* pThermistorTable)") ;

	// $[TI3]
	tau_ = 0.0 ;
	predictedTemperature_ = 35.0 ;
	tempOffset_ = 0.0 ;
	
	a_ = 0.0 ;
	b_ = 0.0 ;
	c_ = 0.0 ;
	d_ = 0.0 ;
	filterEnabled_ = TRUE;
}

#else

TemperatureSensor::TemperatureSensor( const Real32* pThermistorTable)
: Sensor(), pVoltTemperatureTable_(pThermistorTable)				// $[TI2]
{
	CALL_TRACE("TemperatureSensor::TemperatureSensor( \
				const Real32* pThermistorTable)") ;

	// $[TI4]
	tempOffset_ = 0.0 ;
}

#endif // defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TemperatureSensor()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
TemperatureSensor::~TemperatureSensor( void)
{
	CALL_TRACE("TemperatureSensor::~TemperatureSensor( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValue
//
//@ Interface-Description
//      This method has the temperature counts as an argument on the GUI_CPU and
//		no arguments on the BD_CPU.  It has no return value.  This method is
//		used to overload the base class method.
//---------------------------------------------------------------------
//@ Implementation-Description
//		GUI_CPU:
//			Update engValue_ with the return of rawToEngValue_().
//
//		BD_CPU:
//			Call base class Sensor::updateValue() to obtain the temperature.
//			Compute predicted temperature (output of inverse alpha filter and
//			then the output of an alpha filter).
// 		$[04331]
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_GUI_CPU)

void
TemperatureSensor::updateValue( const AdcCounts count)
{
	CALL_TRACE("TemperatureSensor::updateValue( const AdcCounts count)") ;
	
	// $[TI1]

    engValue_ = rawToEngValue_( count) + tempOffset_ ;
}

#else

void
TemperatureSensor::updateValue( void)
{
	CALL_TRACE("TemperatureSensor::updateValue( void)") ;

	Real32 prevValue = engValue_ ;
	
	Sensor::updateValue() ;

	engValue_ += tempOffset_ ;

#ifdef INTEGRATION_TEST_ENABLE
	Sensor::updateVirtualValue();
#endif // INTEGRATION_TEST_ENABLE

	Real32 alphaInverse = tau_ / (tau_ + CYCLE_TIME_MS) ;
	
	Real32 inverseFilter = (engValue_ - alphaInverse * prevValue) / (1.0F - alphaInverse) ;

	if ( filterEnabled_)
	{
	    predictedTemperature_ = Alpha_ * predictedTemperature_ + (1.0F - Alpha_) * inverseFilter ;
	}
	else
    {
	    predictedTemperature_ = engValue_ ;    // turn off filters in PAV if calculating a resistance value 
    }
}
#endif // defined (SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateTau
//
//@ Interface-Description
//      This method has the flow as an argument and returns nothing.  This
//		method computes the tau based on flow rate.
//---------------------------------------------------------------------
//@ Implementation-Description
//		tau computed based on a polynomial fit.
// 		$[04331]
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

void
TemperatureSensor::calculateTau( const Real32 flow)
{
	CALL_TRACE("TemperatureSensor::calculateTau( const Real32 flow)") ;

	const Real32 MIN_FLOW = 10.0F ;
	
	Real32 tempFlow = flow ;
	
	if (tempFlow < MIN_FLOW)
	{
		// $[TI1.1]
		tempFlow = MIN_FLOW ;
	}	// implied else $[TI1.2]
		
	tau_ = (a_ + b_ * tempFlow) / (1.0F + c_ * tempFlow + d_ * tempFlow * tempFlow) * 1000.0F ;
}

#endif // defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTauCoeffs
//
//@ Interface-Description
//      This method has the 4 coefficients in the polynomial fit of the tau
//		as arguments and returns nothing.  This	method sets the values
//		of the coefficients to the values passed in.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set coefficients to the values passed in.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

void
TemperatureSensor::setTauCoeffs( const Real32 a, const Real32 b, const Real32 c, const Real32 d)
{
	CALL_TRACE("TemperatureSensor::setTauCoeffs( const Real32 a, const Real32 b, \
				const Real32 c, const Real32 d)") ;

	// $[TI1]
	a_ = a ;
	b_ = b ;
	c_ = c ;
	d_ = d ;
}

#endif // defined (SIGMA_BD_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
TemperatureSensor::SoftFault( const SoftFaultID  softFaultID,
			                  const Uint32       lineNumber,
              			      const char*        pFileName,
			                  const char*        pPredicate)
{
  	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	
  	FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, TEMPERATURESENSOR,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: rawToEngValue_()
//
//@ Interface-Description
//      This method takes the argument ADC counts and returns the
//      corresponding temperature.  
//---------------------------------------------------------------------
//@ Implementation-Description
//      Use the AdcCount to index into the VoltTemperatureTable
//      to get temperature in C.
//---------------------------------------------------------------------
//@ PreCondition
//      count <= MAX_COUNT_VALUE
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Real32
TemperatureSensor::rawToEngValue_( const AdcCounts count) const
{
	CALL_TRACE("TemperatureSensor::rawToEngValue_( const AdcCounts count)") ;

    CLASS_PRE_CONDITION( count <= MAX_COUNT_VALUE ) ;

	Int32 index =0 ;
	
	if (pVoltTemperatureTable_ == (Real32 *)&::FlowSensorThermistorTable)
	{
		// $[TI1.1]
    	index = count ;
    }
    else if (pVoltTemperatureTable_ == (Real32 *)&::ExhHeaterCoilThermistorTable)
    {
		// $[TI1.2]
	    // The temperature sensor returns 0 to MAX_COUNT_VALUE values, the temperature
    	// conversion table has 1024 entries.  1024 / (MAX_COUNT_VALUE + 1) = 1/4
	    // Add 2/4 to round index up
	    
		index = (Int32)((count + 2) / 4) ;
    }
    else
    {
    	CLASS_PRE_CONDITION(
    			pVoltTemperatureTable_ == (Real32 *)&::FlowSensorThermistorTable ||
    			pVoltTemperatureTable_ == (Real32 *)&::ExhHeaterCoilThermistorTable) ;
    }

    Real32 temperature = pVoltTemperatureTable_[index] ;

    return( temperature) ; 
}



//=====================================================================
//
//  Private Methods...
//
//=====================================================================


