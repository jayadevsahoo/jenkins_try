#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  Plot1TypeSetting - Type of Wavform for Plot #1 Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a non-batch, discrete setting that indicates the type of plot
//  (e.g., pressure-vs.-time) shown for Plot #1.  This class inherits from
//  'NonBatchDiscreteSetting' and provides the specific information needed
//  for the representation of type of Waveform Plot #1.  This information
//  includes the set of values that this setting can have (see
//  'Plot1TypeValue.hh'), and this non-batch setting's default value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Plot1TypeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 003   By: dosman    Date:  07-Apr-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getDefaultValue()
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//  Inital version.
//
//=====================================================================

#include "Plot1TypeSetting.hh"
#include "Plot1TypeValue.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  Plot1TypeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01151] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Plot1TypeSetting::Plot1TypeSetting(void)
      : NonBatchDiscreteSetting(SettingId::PLOT1_TYPE,
			        Setting::NULL_DEPENDENT_ARRAY_,
			        Plot1TypeValue::TOTAL_PLOT1_TYPES)
{
  CALL_TRACE("Plot1TypeSetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~Plot1TypeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Plot1TypeSetting::~Plot1TypeSetting(void)
{
  CALL_TRACE("~Plot1TypeSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is ALWAYS changeable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
Plot1TypeSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
Plot1TypeSetting::getDefaultValue(void) const
{
  CALL_TRACE("getDefaultValue()");

  // $[01162] The new-patient waveforms to be displayed ...
  return(DiscreteValue(Plot1TypeValue::PRESSURE_VS_TIME));    // $[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Plot1TypeSetting::SoftFault(const SoftFaultID  softFaultID,
   			    const Uint32       lineNumber,
			    const char*        pFileName,
			    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          PLOT_1_TYPE_SETTING, lineNumber, pFileName,
                          pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
