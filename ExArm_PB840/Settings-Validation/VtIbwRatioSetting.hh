
#ifndef VtIbwRatioSetting_HH
#define VtIbwRatioSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  VtIbwRatioSetting - Tidal Volume-to-IBW Ratio.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/VtIbwRatioSetting.hhv   25.0.4.0   19 Nov 2013 14:27:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  16-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added this setting, as a tracking setting, for informational
//         display
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class VtIbwRatioSetting : public BatchBoundedSetting, public SettingObserver
{
  public:
    VtIbwRatioSetting(void); 
    virtual ~VtIbwRatioSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    // SettingObserver methods...
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;
    virtual void  settingObserverInit(void);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif // defined (SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual BoundedValue  calcBasedOnSetting_(
			    const SettingId::SettingIdType basedOnSettingId,
			    const BoundedRange::WarpDir    warpDirection,
			    const BasedOnCategory          basedOnCategory
					     ) const;

  private:
    VtIbwRatioSetting(const VtIbwRatioSetting&);	// not implemented...
    void  operator=(const VtIbwRatioSetting&);	// not implemented...
};


#endif // VtIbwRatioSetting_HH 
