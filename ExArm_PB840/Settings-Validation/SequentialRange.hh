
#ifndef SequentialRange_HH
#define SequentialRange_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SequentialRange - Range of 'SequentialValue'.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SequentialRange.hhv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.5.1.0) into Rev "BiLevel" (1.5.2.0)
//	Added new methods needed for supporting the ability of sequential
//	settings to disable their bounds.
//
//  Revision: 002   By: sah    Date:  07-May-1997    DCS Number: 2034
//  Project: Sigma (R8027)
//  Description:
//	As a result of this DCS, I'm making sure the same problem can't
//	happen with sequential settings, by defining two new methods
//	for resetting the minimum and maximum constraint values to their
//	corresponding absolute limits (see 'reset{Min,Max}Constraint()').
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "SettingClassId.hh"
#include "SettingBoundTypes.hh"
#include "BoundStatus.hh"

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif  // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
#include "SequentialValue.hh"
//@ End-Usage


class SequentialRange
{
#if defined(SIGMA_DEVELOPMENT)
    // Friend:  operator<<(ostr, range)
    // This needs access to the private members.
    friend Ostream&  operator<<(Ostream&               ostr,
    				const SequentialRange& sequentialRange);
#endif  // defined(SIGMA_DEVELOPMENT)

  public:
    SequentialRange(const SequentialValue maxValue,
		    const SettingBoundId     maxConstraintId,
		    const SequentialValue resolution,
		    const SettingBoundId     minConstraintId,
		    const SequentialValue minValue);
    ~SequentialRange(void);

    Boolean isLegalValue(const SequentialValue sequentialValue) const;

    inline SequentialValue  getMaxValue(void) const;
    inline SequentialValue  getMinValue(void) const;

    inline SequentialValue  getMaxConstraintValue(void) const;
    inline SequentialValue  getMinConstraintValue(void) const;

    void  testValue(SequentialValue& rProposedValue,
		    BoundStatus&     rBoundStatus) const;

    void  increment(const Uint       numClicks,
		    SequentialValue& rCurrValue,
		    BoundStatus&     rBoundStatus) const;
    void  decrement(const Uint       numClicks,
		    SequentialValue& rCurrValue,
		    BoundStatus&     rBoundStatus) const;

    inline void  updateMaxConstraint(const SettingBoundId   maxConstraintId,
				   const SequentialValue maxConstraintValue);
    inline void  updateMinConstraint(const SettingBoundId   minConstraintId,
				   const SequentialValue minConstraintValue);

    inline void  resetMaxConstraint(void);
    inline void  resetMinConstraint(void);

    static Boolean  IsLegalSequence(const SequentialValue intervalMax,
				    const SequentialValue intervalRes,
				    const SequentialValue intervalMin);

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  private:
    SequentialRange(const SequentialRange&);	// not implemented...
    SequentialRange(void);			// not implemented...
    void  operator=(const SequentialRange&);	// not implemented...

    //@ Data-Member:  maxConstraintValue_
    // This is the maximum allowable value for this sequence, at any given
    // time.
    SequentialValue  maxConstraintValue_;

    //@ Data-Member:  maxConstraintId_
    // This is the ID of the maximum constraint.
    SettingBoundId  maxConstraintId_;

    //@ Data-Member:  minConstraintValue_
    // This is the minimum allowable value for this sequence, at any given
    // time.
    SequentialValue  minConstraintValue_;

    //@ Data-Member:  minConstraintId_
    // This is the ID of the minimum constraint.
    SettingBoundId  minConstraintId_;

    //@ Constant:  ABSOLUTE_MAX_
    // This constant is the absolute maximum allowable value for this
    // sequence.
    const SequentialValue  ABSOLUTE_MAX_;

    //@ Constant:  RESOLUTION_
    // This is the resolution for this sequence.
    const SequentialValue  RESOLUTION_;

    //@ Constant:  ABSOLUTE_MIN_
    // This constant is the absolute minimum allowable value for this
    // sequence.
    const SequentialValue  ABSOLUTE_MIN_;
};


// Inlined methods...
#include "SequentialRange.in"


#endif  // SequentialRange_HH
