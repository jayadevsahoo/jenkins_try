#include "stdafx.h"
#include "swat.h"
#include "BatchBuilderLua.h"

 
const char Batch::className[] = "Batch";

const Luna<Batch>::RegType Batch::Register[] = 
{
		{ "setbatchID",&Batch::setbatchID},

		{ "mark",&Batch::mark},	
		
		{ "setindex",&Batch::setindex},
		{ "skipindex",&Batch::skipindex},
	
		{ "repeatn",&Batch::repeatn},
		{ "repeattime",&Batch::repeattime},
		{ "waittime",&Batch::waittime},

		{ "repeatevent",&Batch::repeatevent},
		{ "waitevent",&Batch::waitevent},
		{ "signalevent",&Batch::signalevent},

		{ "setsensor",	&Batch::setsensor },
		{ "addsensor",	&Batch::addsensor},
		{ "unsetsensor",&Batch::unsetsensor},

		{ "setrawsensor",	&Batch::setrawsensor },
		{ "unsetrawsensor", &Batch::unsetrawsensor },

        { "addrawsensor", &Batch::addrawsensor },
        { "simulateguitouch", &Batch::simulateguitouch },
        { "simulatebezelkey", &Batch::simulatebezelkey },


		{ "send",		&Batch::send},
		
		{ "end",		&Batch::end},
		{ "resetall",		&Batch::resetall},
		
		{ 0 }
	
};


int Batch::end(lua_State *L)
{

	int n = lua_gettop(L); 
	if(n==2)
	{
		m_batch->end( );
	}
	else 
	{
		LOG_MESSAGE(LM_ERROR, "BATCH ERROR: end( ) requires NO param");
		
	}
	return 0;
}

int Batch::resetall(lua_State *L)
{
	LOG_MESSAGE(LM_INFO, "resetall( )");

	m_batch->resetall( );

	return 0;
}




int Batch::mark(lua_State *L)
{
	
	int nIndex;
	int n = lua_gettop(L); 
	if(n==2)
	{
		nIndex = lua_tointeger(L, -1);
		if(nIndex == 0)
		{
			LOG_MESSAGE(LM_ERROR, "Batch Mark got Null parameter.");
			return 0;
		}
		m_batch->mark(nIndex);
	}
	else if(n==1)
	{
		m_batch->mark( );
	}
	else 
	{
		LOG_MESSAGE(LM_ERROR, "Batch Mark requires one or no param");
	}
	return 0;

}

int Batch :: setbatchID(lua_State *L )
{
	int n = lua_gettop(L); 
	if(n==2)
	{
		strcpy(chBatchID, lua_tostring(L, -1)); 
	}
	else 
	{
		LOG_MESSAGE(LM_ERROR,"BATCH ERROR: setbatchID( ) requires one param");
	}

	return 0;
}
int Batch :: setindex (lua_State *L)
{
	int nIndex;
	int n = lua_gettop(L); 
	if(n==2)
	{
		nIndex = lua_tointeger(L, -1);
		m_batch->setindex(nIndex);
	}
	else 
	{
		LOG_MESSAGE(LM_ERROR,"BATCH ERROR: setindex( ) requires one param");
	}
	return 0;
}

int Batch :: skipindex (lua_State *L)
{
	LOG_MESSAGE(LM_INFO,"skipindex( )");
	int nIndex;
	int n = lua_gettop(L); 
	if(n==2)
	{
		nIndex = lua_tointeger(L, -1);
		m_batch->skipindex(nIndex);
	}
	else 
	{
		LOG_MESSAGE(LM_ERROR,"BATCH ERROR: skipindex( ) requires one param");
	}
	return 0;
}





int Batch::setsensor(lua_State *L)
{
		LOG_MESSAGE(LM_INFO,"setsensor( )");
		int nSensorID;
		float fValue;
		int n = lua_gettop(L); 
		if(n==2)
		{
			nSensorID = lua_tointeger(L, -1);
			m_batch->setsensor(nSensorID);
		}
		else if(n == 3)
		{
			fValue		= (float)lua_tonumber(L, -1);
			nSensorID	= lua_tointeger(L, -2);
			
			if(nSensorID == 0) {
				LOG_MESSAGE(LM_ERROR, "BATCH ERROR: setsensor got nil as sensorID");
				return 0;
			}

			m_batch->setsensor(nSensorID, fValue);
		}
		else
		{
			LOG_MESSAGE(LM_ERROR, "BATCH ERROR: Setsensor( ) needs two Paramas");
		}

		return 0;
}

int Batch::setrawsensor(lua_State *L)
{
		LOG_MESSAGE(LM_INFO,"setrawsensor( )");
		int nSensorID;
		unsigned int fValue;
		int n = lua_gettop(L); 
		/*if(n==2)
		{
			nSensorID = lua_tointeger(L, -1);
			m_batch->setrawsensor(nSensorID);
		}
		else */
		if(n == 3)
		{
			fValue		= (unsigned int)lua_tonumber(L, -1);
			nSensorID	= lua_tointeger(L, -2);
			
			if(nSensorID == 0) {
				LOG_MESSAGE(LM_ERROR, "BATCH ERROR: setrawsensor got nil as sensorID");
				return 0;
			}

			m_batch->setrawsensor(nSensorID, fValue);
		}
		else
		{
			LOG_MESSAGE(LM_ERROR, "BATCH ERROR: setrawsensor( ) needs two Paramas");
		}

		return 0;
}



int Batch::repeatn    (lua_State *L )
{	
	LOG_MESSAGE(LM_INFO,"repeatn( )");
	int nTimes;
	int n = lua_gettop(L); 
	if(n==2)
	{
		nTimes = lua_tointeger(L, -1);
		m_batch->repeatn(nTimes);
	}
	else 
	{
		LOG_MESSAGE(LM_ERROR,"BATCH ERROR: repeatn( ) requires only one param");
	}
	return 0;
}

int Batch::repeattime (lua_State *L )
{	LOG_MESSAGE(LM_INFO,"repeattime( )");
	int nTimes;
	int n = lua_gettop(L); 
	if(n==2)
	{
		nTimes = lua_tointeger(L, -1);
		m_batch->repeattime(nTimes);
	}
	else 
	{
		LOG_MESSAGE(LM_ERROR,"BATCH ERROR: Setsensor( ) requires only one param: sensorID");
	}
	return 0;
}

int Batch::waittime   (lua_State *L )
{	
	LOG_MESSAGE(LM_INFO,"waittime( )");
	int nTime;
	int n = lua_gettop(L); 
	if(n==2)
	{
		nTime = lua_tointeger(L, -1);
		/*if(nTime == 0) {
				LOG_MESSAGE(LM_ERROR, "BATCH ERROR: waittime got nil param");
				return 0;
		}*/ //nil turns 0 from lua to c++, 0 is valid for waittime, removing this check

		m_batch->waittime(nTime);
	}
	else 
	{
		LOG_MESSAGE(LM_ERROR, "BATCH ERROR: waittime requires one param: number of cycles");
	}
	return 0;
}


int Batch::addsensor  (lua_State *L)
{
		LOG_MESSAGE(LM_INFO,"addsensor( )");
		int nSensorID;
		float fValue;
		int n = lua_gettop(L); 
		
		if(n == 3)
		{
			fValue		= (float)lua_tonumber(L, -1);
			nSensorID	= lua_tointeger(L, -2);

			if(nSensorID == 0) {
				LOG_MESSAGE(LM_ERROR, "BATCH ERROR: addsensor got nill as sensorID");
				return 0;
			}

			m_batch->addsensor(nSensorID, fValue);
		}
		else
		{
			LOG_MESSAGE(LM_ERROR, "BATCH ERROR: addsensor requires two params");
		}

		return 0;
}



int Batch::repeatevent(lua_State *L)
{
		LOG_MESSAGE(LM_INFO,"repeatevent( )");
		int iEventId;
		int iTimeExpire;
		int n = lua_gettop(L); 
		if(n == 3)
		{
			iTimeExpire	= (int)lua_tonumber(L, -1);
			iEventId	= lua_tointeger(L, -2);
			m_batch->repeatevent(iEventId, iTimeExpire);
		}
		else if(n == 2)
		{
			iEventId	= lua_tointeger(L, -1);
			m_batch->repeatevent(iEventId);
		}
		else
		{
			LOG_MESSAGE(LM_ERROR,"BATCH ERROR: repeatevent( ) requires two or one param");
		}

		return 0;
}

int Batch::waitevent (lua_State *L)
{
		char chBuffer[80];
		int iEventId;
		int iTimeExpire;
		int n = lua_gettop(L); 
		if(n == 3)
		{
			iTimeExpire	= (int)lua_tonumber(L, -1);
			iEventId	= lua_tointeger(L, -2);
			sprintf(chBuffer, "waitevent(%d, %d)",iEventId, iTimeExpire);
			LOG_MESSAGE(LM_INFO, chBuffer);
			m_batch->waitevent(iEventId, iTimeExpire);
		}
		else if(n == 2)
		{
			iEventId	= lua_tointeger(L, -1);
			sprintf(chBuffer, "waitevent(%d)",iEventId);
			LOG_MESSAGE(LM_INFO, chBuffer);
			m_batch->waitevent(iEventId);

		}
		else
		{
			LOG_MESSAGE(LM_ERROR, "BATCH ERROR: waitevent( ) needs two or one param");
		}

		return 0;
}

int Batch::signalevent (lua_State *L)
{
		LOG_MESSAGE(LM_INFO,"signalevent( )");
		int iEventId;
		int n = lua_gettop(L); 
		if(n == 2)
		{
			iEventId	= lua_tointeger(L, -1);
			if(iEventId == 0) {
				LOG_MESSAGE(LM_ERROR, "BATCH ERROR: signalevent( ) got Null param.");
				return 0;
			}
			m_batch->signalevent(iEventId);
		}
		else
		{
			LOG_MESSAGE(LM_ERROR, "BATCH ERROR: signalevent( ) needs one param");
		}
		return 0;
}




int Batch::unsetsensor(lua_State *L)
{
		LOG_MESSAGE(LM_INFO,"unsetsensor( )");
		int nSensorID;
		int n = lua_gettop(L); 
		if(n==2)
		{
			nSensorID = lua_tointeger(L, -1);
			if(nSensorID == 0) {
				LOG_MESSAGE(LM_ERROR, "BATCH ERROR: unsetsensor( ) got Null param.");
				return 0;
			}
			m_batch->unsetsensor(nSensorID);
		}
		else 
		{
			LOG_MESSAGE(LM_ERROR, "BATCH ERROR: unsetsensor( ) requires one param");
		}
		return 0;
}

int Batch::unsetrawsensor(lua_State *L)
{
		LOG_MESSAGE(LM_INFO,"unsetsensor( )");
		int nSensorID;
		int n = lua_gettop(L); 
		if(n==2)
		{
			nSensorID = lua_tointeger(L, -1);
			if(nSensorID == 0) {
				LOG_MESSAGE(LM_ERROR, "BATCH ERROR: unsetrawsensor( ) got Null param.");
				return 0;
			}
			m_batch->unsetrawsensor(nSensorID);
		}
		else 
		{
			LOG_MESSAGE(LM_ERROR, "BATCH ERROR: unsetrawsensor( ) requires one param");
		}
		return 0;
}

int Batch::addrawsensor(lua_State *L)
{
        LOG_MESSAGE(LM_INFO,"addrawsensor( )");
		int nSensorID;
		unsigned int fValue;
		int n = lua_gettop(L); 

        if(n == 3)
		{
			fValue		= (unsigned int)lua_tonumber(L, -1);
			nSensorID	= lua_tointeger(L, -2);
			
			if(nSensorID == 0) {
				LOG_MESSAGE(LM_ERROR, "BATCH ERROR: addrawsensor got nil as sensorID");
				return 0;
			}

			m_batch->addrawsensor(nSensorID, fValue);
		}
		else
		{
			LOG_MESSAGE(LM_ERROR, "BATCH ERROR: addrawsensor( ) needs two Paramas");
		}

		return 0;
}

int Batch::simulateguitouch(lua_State *L)
{
    LOG_MESSAGE(LM_INFO,"simulateguitouch( )");

    unsigned int nX = 0;
    unsigned int nY = 0;

    int n = lua_gettop(L);

    if ( n == 3 )
    {
        nX = (unsigned int)lua_tonumber(L, -2);
        nY = (unsigned int)lua_tonumber(L, -1);
        
        m_batch->simulateguitouch( nX, nY );
    }
    else
    {
        LOG_MESSAGE(LM_ERROR, "BATCH ERROR: simulateguitouch( ) needs two Paramas");
    }

    return 0;
}

int Batch::simulatebezelkey(lua_State *L)
{
    LOG_MESSAGE(LM_INFO,"simulatebezelkey( )");

    unsigned int keyId = 0;
    unsigned int onOff = 0;

    int n = lua_gettop(L);

    if ( n == 3 )
    {
        keyId = (unsigned int)lua_tonumber(L, -2);
        onOff = (unsigned int)lua_tonumber(L, -1);
        
        m_batch->simulatebezelkey( keyId, onOff );
    }
    else
    {
        LOG_MESSAGE(LM_ERROR, "BATCH ERROR: simulatebezelkey( ) needs two Paramas");
    }

    return 0;
}

int Batch::WriteToVent(int nCommandType )
{
			CSwatApp* pApp = (CSwatApp*)AfxGetApp();

			//return if stop has been triggered
			if(pApp->m_bRun == false) return 0;

			int i=1;

			struct sockaddr_in si_other;
			int s,  slen=sizeof(si_other);
 
			s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			memset((char *) &si_other, 0, sizeof(si_other));
			si_other.sin_family = AF_INET;
			si_other.sin_port = htons(SEND_BATCH_PORT);
			si_other.sin_addr.s_addr =inet_addr("192.168.0.3");
			


			if(nCommandType==0)
			{
				if(m_batch->getLength() >= 20000)
				{
					
					CString strMess;
					strMess.Format("size of batchID '%s' is %d, more than limit of 20000. Batch not Sent!",chBatchID, m_batch->getLength());
					AfxMessageBox(strMess);
					closesocket(s);
					return 0;
				}
				int i=sendto(s, m_batch->getBatch(), m_batch->getLength(), 0, (sockaddr *)&si_other, slen);


				LOG_MESSAGE(LM_INFO, "Batch file size : %d", m_batch->getLength());

				/*temp log for debuggging only
				CTime time = CTime::GetCurrentTime();
				CString strLog;
				strLog.Format("%s  : Batch file size %d \n", time.Format("%H:%M:%S"), m_batch->getLength());
				CStdioFile file( TEMP_DEBUG_LOG, CFile::modeCreate | CFile::modeNoTruncate|CFile::modeWrite | CFile::typeText );
				file.SeekToEnd();
				file.WriteString(strLog);
				file.Close();
				//debug log over*/

				if(i != m_batch->getLength())
				{
					int n = WSAGetLastError( );
					AfxMessageBox("sending batch to vent failed with windows socket error code: %i",n);
					LOG_MESSAGE(LM_ERROR, "sending batch to vent failed with windows socket error code: %i",n);
				}
				LOG_MESSAGE(LM_INFO, "Command 0 : batch send returned %i",i);
			}
			else if(nCommandType ==1)
			{
				int i=sendto(s, m_batch->generateSwatEnable(), sizeof(NetworkMessageHeader)+sizeof(CommandInfo), 0, (sockaddr *)&si_other, slen);

				if(i == sizeof(NetworkMessageHeader)+sizeof(CommandInfo))
				{
					pApp->m_bVent_Batch_enabled = true;
				}
				else
				{
					int n = WSAGetLastError( );
					AfxMessageBox("enabling SWAT batch on vent failed with windows socket error code: %d",n);

				}

				LOG_MESSAGE(LM_INFO, "Command 1 : batch enable returned %i",i);

			}
			else if(nCommandType ==2)
			{
				int i=sendto(s, m_batch->generateSwatDisable(), sizeof(NetworkMessageHeader)+sizeof(CommandInfo), 0, (sockaddr *)&si_other, slen);
				if(i == sizeof(NetworkMessageHeader)+sizeof(CommandInfo))
				{
					pApp->m_bVent_Batch_enabled = false;
				}
				else
				{
					int n = WSAGetLastError( );
					AfxMessageBox("disabling SWAT batch on vent failed with windows socket error code: %d",n);

				}


				LOG_MESSAGE(LM_INFO, "Command 2 : batch disable returned %i",i);
			}
			else
			{
				LOG_MESSAGE(LM_ERROR, "wrong param passed: %i",nCommandType);
				AfxMessageBox("EEEEEEEERRRRRRRRRRRRRRRRRORRRRRRRRRRRRRRRRRR");
			}

//			WSACleanup();

			closesocket(s);

			return 0;
};


int Batch::send(lua_State *L)
{
		WriteToVent(0);
		return 0;
}
