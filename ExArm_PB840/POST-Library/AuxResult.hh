#ifndef AuxResult_HH
#define AuxResult_HH
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
// Class: AuxResult - POST Auxiliary Result Data
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/AuxResult.hhv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By: Gary Cederquist  Date: 12-MAY-1997  DR Number: 2085
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version
//
//=====================================================================

#include "Sigma.hh"


class  AuxResult
{
  public:
    AuxResult();

    AuxResult(  const Uint8 testNumber
              , const Uint8 vectorNumber   = 0xff 
              , const Uint8 nmiSource      = 0xff
              , const Uint8 auxCode        = 0xff
              , Byte* const programCounter = (Byte*)0xffffffff
             );

    AuxResult( const AuxResult & rAuxResult );
    void operator=( const AuxResult & rAuxResult );

    ~AuxResult();

    inline Uint8         getTestNumber(void) const;
    inline Uint8         getVectorNumber(void) const;
    inline Uint8         getNmiSource(void) const;
    inline Uint8         getAuxCode(void) const;
    inline const Byte *  getProgramCounter(void) const;

  private:
    Uint8   testNumber_;
    Uint8   vectorNumber_;
    Uint8   nmiSource_;
    Uint8   auxCode_;
    Byte *  programCounter_;
};

#include "AuxResult.in"

#endif // AuxResult_HH
