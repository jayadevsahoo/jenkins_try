#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmSlider - Displays a slider for setting high and low
// alarm limits, containing buttons for setting the limits and a graphical
// pointer displaying the current value of the associated patient datum.
//---------------------------------------------------------------------
//@ Interface-Description
// The display of the alarm slider consists of the following components: a long
// vertical slider box along which settings and data "slide", a numeric value
// at the top of the slider indicating the maximum value of the slider, a
// numeric value at the bottom indicating the minimum value, high and/or low
// alarm limit setting buttons which allow the operator to set the alarm limits
// (these buttons move along the slider according to their current value), a
// pointer which displays the current value of the patient datum to which the
// alarm limits apply (this pointer also slides), a label at the bottom of the
// slider which displays the symbol for the patient datum and a label at the
// top displaying the units in which the datum is measured.
//
// The slider receives alarm limit setting changes from the Settings-Validation
// subsystem (via valueUpdate()) which enable it to position the
// alarm limit buttons at the correct position on the slider.  Similarly,
// patientDataChangeHappened() receives changes from the Patient Data subsystem
// to correctly position the patient datum pointer.
//
// Once created an object of this class works mainly by itself.  It simply
// requires to be activated (via activate()) before being displayed in order to
// sync the values it displays with the current state of the
// Settings-Validation and Patient Data subsystems.
//
// The AlarmSlider class is a BdEventTarget. The virtual method 
// bdEventHappened() is redefined here and is invoked when the Breath-Delivery 
// subsystem notifies us that a Vent-INOP, SVO, Occlusion or Patient Disconnect
// event occurs.
//---------------------------------------------------------------------
//@ Rationale
// Encapsulates all the functionality needed to display and use an
// alarm slider on the Alarm Setup subscreen in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Most of the code in this class goes into the setup of the alarm slider's
// display which is performed in the constructor.  After constructing the
// display the main function of the slider is to react to changes in the alarm
// limit settings, the applicability of setting ids, and to the value of the patient
// datum in order to "slide" the various components along the slider.  Changes
// to the alarm limit settings are communicated through the valueUpdate() method
// and changes in the patient datum through the patientDataChangeHappened() method.
//
// There is an overloaded Drawable::activate() method provided which should be
// called before the slider is displayed.  The activate() method puts the
// slider in sync with the current alarm limit settings and patient datum
// value.  Note: the alarm slider gets out of sync with the Settings-Validation
// and Patient Data subsystems because it ignores change-happened notices when
// it is not being displayed onscreen.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling strategies apart from the usual
// assertions and pre-conditions which verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// This class must be created with at least one alarm limit setting
// button.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmSlider.ccv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 018 By: gdc    Date: 15-Feb-2005   DR Number: 6144
//  Project:  NIV1
//	Description:
//      DCS 6144 - NIV1 - changes to support low circuit pressure limit
//      added parameter to define slider's auto-scaling behavior
//
//  Revision: 017 By: srp    Date: 28-May-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 016  By:  erm	   Date:  25-Mar-2002    DCS Number:  5790
//  Project:  VTPC
//  Description:  
//     Added support to change associated alarm.
//
//  Revision: 015  By:  heatherw	   Date:  04-Mar-2002    DCS Number:  5790
//  Project:  VTPC
//  Description:
//      Added new method 'setLabelText' so the text can be changed for
//      a TextField.
//      Added new method 'setUpperButtonMessageId' so the help message
//      associated with the upper alarm button could be set.
//
//  Revision: 014  By:  healey	   Date:  09-Mar-2000    DCS Number:  5637
//  Project:  NeoMode
//  Description:
//      Fixed code to not update data pointer if hidePointer_ flag was
//      set to TRUE.
//
//  Revision: 013  By:  healey	   Date:  22-Feb-2000    DCS Number:  5653
//  Project:  NeoMode
//  Description:
//      Fixed updateValueDisplay()  so value pointer always display correct 
//      data.
//
//  Revision: 012  By:  healey	   Date:  08-Feb-2000    DCS Number:  5504
//  Project:  NeoMode
//  Description:
//      Added recent-activity range to alarm bars whereby the standard deviation 
//      the last N consecutive breaths is displayed.
//
//  Revision: 011  By:  healey	   Date:  30-Jul-1999    DCS Number:  5327
//  Project:  NeoMod
//  Description:
//      Added auto-scaling capability into alarm sliders.
//
//  Revision: 010  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 008  By:  hhd	   Date:  01-Feb-1999    DCS Number: 5322  
//  Project:  ATC
//  Description:
//		Initial version.
//	
//  Revision: 007  By:  gdc    Date:  02-Sep-1998    DCS Number: 5153
//  Project:  Color
//  Description:
//      Added traceable ID's for Color Project requirements.
//
//  Revision: 006  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 005  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 004  By:  yyy    Date:  10-Sep-97    DR Number: 2169
//    Project:  Sigma (R8027)
//    Description:
//      Removed obsolete SRS references.
//
//  Revision: 003  By:  hhd    Date:  18-AUG-97    DR Number: 2321
//    Project:  Sigma (R8027)
//    Description:
//      English and German use a different field height for the value box
//	 	  than other European languages.
//
//  Revision: 002  By: yyy      Date: 10-Jun-1997  DR Number: 2201
//    Project:  Sigma (R8027)
//    Description:
//      Removed the guiEventHappened qualifier SETTINGS_LOCKOUT.  The alarm
//		slider buttons shall be set to flat when either in IsSettingsLockedOut or
//		IsSettingsTransactionSuccess is FALSE.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "Colors.hh"
#include "AlarmSlider.hh"
#include "AlarmRepository.hh"

#include <math.h>
#include "Sigma.hh"

//@ Usage-Classes
#include "AlarmDci.hh"
#include "AlarmName.hh"
#include "BdEventRegistrar.hh"
#include "BdGuiEvent.hh"
#include "GuiEventRegistrar.hh"
#include "PatientDataRegistrar.hh"
#include "SettingConstants.hh"
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
const Int32 AlarmSlider::CONTAINER_WIDTH = 105;
const Int32 AlarmSlider::CONTAINER_HEIGHT = 244;
static const Int32 BUTTON_X_ = 6;
static const Int32 BUTTON_Y_ = 0;
static const Int32 VALUE_FIELD_WIDTH_ = 40;
static const Int32 VALUE_FIELD_HEIGHT_ = 15;
static const Int32 VALUE_TRIANGLE_WIDTH_ = 7;
static const Int32 SLIDER_X_ = VALUE_FIELD_WIDTH_ + VALUE_TRIANGLE_WIDTH_;
static const Int32 SLIDER_Y_ = 38;
static const Int32 SLIDER_WIDTH_ = 8;
static const Int32 SLIDER_HEIGHT_ = 156;
static const Int32 NUMERIC_FIELD_HEIGHT_ = 25;
static const Int32 MAX_FIELD_Y_ = SLIDER_Y_ - NUMERIC_FIELD_HEIGHT_;
static const Int32 MAX_LABEL_WIDTH_ = SLIDER_X_ + SLIDER_WIDTH_ - 3;
static const Int32 MIN_LABEL_Y_OFFSET_ = 21;
static const Int32 VALID_NUM_SAMPLES_TO_DISPLAY = 2;  // valid number of samples before
                                                      // displaying std deviation bar


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmSlider()  [Constructor]
//
//@ Interface-Description
// Creates an alarm slider for setting alarm limits.  The parameters are as
// follows:
// >Von
//	LOWER_SETTING_ID_	The setting id for the lower limit alarm setting button.
//					Can be SettingId::NULL_SETTING_ID if there is no applicable
//					lower alarm limit.
//	lowerMessageId	The string id of the help message displayed when the lower
//					limit alarm setting button is pressed down.  Should be
//					NULL_STRING_ID if there is no lower button.
//	UPPER_SETTING_ID_	The setting id for the upper limit alarm setting button.
//					Can be SettingId::NULL_SETTING_ID if there is no applicable
//					upper alarm limit.  At least one of LOWER_SETTING_ID_ and
//					UPPER_SETTING_ID_ must not be SettingId::NULL_SETTING_ID.
//	upperMessageId	The string id of the help message displayed when the upper
//					limit alarm setting button is pressed down.  Should be
//					NULL_STRING_ID if there is no upper button.
//	currValueId		The patient data id which specifies which patient data
//					value will be displayed in the graphical patient data
//					pointer which appears at the left of the slider bar.
//	minLabel		The string id of the label displayed below the minimum
//					value of the slider.  This should be the symbol for the
//					patient datum.
//	maxLabel		The string id of the label displayed above the maximum
//					value of the slider.  This should be the symbol for the
//					units in which the patient datum is measured.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor creates all the components of the alarm slider's
// display.  It also registers for receiving update notices for changes
// to the alarm limit settings and the patient datum so that it knows
// when to slide the buttons and pointer.
//
// $[01034] All of these buttons must be of the select type ...
// $[01043] An alarm slider consists of ...
// $[01044] An alarm slider displays these fixed labels ...
// $[01045] Alarm limits are represented by movable batch setting buttons ...
//---------------------------------------------------------------------
//@ PreCondition
// One out of LOWER_SETTING_ID_ and UPPER_SETTING_ID_ must be non-null.
// currValueId must be a valid PatientDataId::PatientItemId.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmSlider::AlarmSlider(SettingId::SettingIdType    lowerSettingId,
						StringId                     lowerMessageId,
						SettingId::SettingIdType     upperSettingId,
						StringId                     upperMessageId,
						AlarmName                    lowerAlarmName,
						AlarmName                    upperAlarmName,
						PatientDataId::PatientItemId patientDataId,
						StringId                     minLabel,
						StringId                     maxLabel,
						SubScreen*                   pFocusSubScreen,
						Real32*					 	 pSummationBuffer,
						Real32*						 pDeviationBuffer,
						Uint32 						 bufferSize,
						Boolean						 autoScalingOn) :
		min_(0.0f),
		value_(0.0f),
		max_(0.0f),
		LOWER_SETTING_ID_(lowerSettingId),
		UPPER_SETTING_ID_(upperSettingId),
		patientDataHandle_(patientDataId),
		minLabel_(minLabel),
		minField_(TextFont::TWELVE, 4, CENTER),
		lowerButton_(BUTTON_X_, BUTTON_Y_, LOWER_SETTING_ID_, lowerMessageId,
					 pFocusSubScreen),
		lowerTriangle_(0, 0, BUTTON_X_, 0, BUTTON_X_, BUTTON_X_),
		valueDisplay_(TextFont::TWELVE, 4, CENTER),
		valueBox_(0, 0, VALUE_FIELD_WIDTH_, VALUE_FIELD_HEIGHT_),
		valueTriangle_(VALUE_FIELD_WIDTH_ + VALUE_TRIANGLE_WIDTH_,
					VALUE_FIELD_HEIGHT_ / 2, VALUE_FIELD_WIDTH_, 0,
					VALUE_FIELD_WIDTH_, VALUE_FIELD_HEIGHT_ - 1),
		lowerAlarmName_(lowerAlarmName),
		upperAlarmName_(upperAlarmName),
		hidePointer_(FALSE),
		sliderBox_(SLIDER_X_, SLIDER_Y_, SLIDER_WIDTH_, SLIDER_HEIGHT_),
		upperButton_(BUTTON_X_, BUTTON_Y_, UPPER_SETTING_ID_, upperMessageId,
					 pFocusSubScreen),
		upperTriangle_(0, AlarmLimitSettingButton::BUTTON_HEIGHT - 1,
					BUTTON_X_,
					AlarmLimitSettingButton::BUTTON_HEIGHT - 1,
					BUTTON_X_,
					AlarmLimitSettingButton::BUTTON_HEIGHT
					- BUTTON_X_ - 1),
		maxField_(TextFont::TWELVE, 4, RIGHT),
		maxLabel_(maxLabel),
		stdDeviationBuffer_(pSummationBuffer, pDeviationBuffer, bufferSize),
		stdDeviationBox_(SLIDER_X_+1, SLIDER_Y_, SLIDER_WIDTH_-2, SLIDER_HEIGHT_),
	  	isAutoScalingOn_(autoScalingOn)
{
	CALL_TRACE("AlarmSlider::AlarmSlider("
						"SettingId::SettingIdType LOWER_SETTING_ID_, "
						"StringId lowerMessageId, "
						"SettingId::SettingIdType upperSettingId, "
						"StringId upperMessageId, "
						"PatientDataId::PatientItemId patientDataId, "
						"StringId minLabel, StringId maxLabel)");

	CLASS_PRE_CONDITION((LOWER_SETTING_ID_ != SettingId::NULL_SETTING_ID)
						|| (UPPER_SETTING_ID_ != SettingId::NULL_SETTING_ID));
	CLASS_PRE_CONDITION((patientDataId != PatientDataId::NULL_PATIENT_DATA_ITEM)
						&& (patientDataId < PatientDataId::NUM_TOTAL_PATIENT_DATA));


	// Dimension the whole slider container

	setWidth(CONTAINER_WIDTH);
	setHeight(CONTAINER_HEIGHT);

	// Position and color the Min and Max labels and add them to the container
	minLabel_.setY(SLIDER_Y_ + SLIDER_HEIGHT_ + MIN_LABEL_Y_OFFSET_);

	minLabel_.setColor(Colors::WHITE);
	maxLabel_.setColor(Colors::WHITE);

	addDrawable(&minLabel_);
	addDrawable(&maxLabel_);

	// Register for changes to the patient datum
	PatientDataRegistrar::RegisterTarget(patientDataId, this);

    // Register for BD events
    BdEventRegistrar::RegisterTarget(EventData::OCCLUSION, this);
    BdEventRegistrar::RegisterTarget(EventData::PATIENT_CONNECT, this);
    BdEventRegistrar::RegisterTarget(EventData::SVO, this);
    BdEventRegistrar::RegisterTarget(EventData::VENT_INOP, this);
 
	// Set up the Slider Box
	sliderBox_.setColor(Colors::LIGHT_BLUE);
	sliderBox_.setFillColor(Colors::LIGHT_BLUE);

	// Set up the standard deriviation Box
	stdDeviationBox_.setColor(Colors::EXTRA_DARK_BLUE);
	stdDeviationBox_.setFillColor(Colors::EXTRA_DARK_BLUE);

	// Set up the Minimum Value field color and dimensions
	minField_.setColor(Colors::WHITE);
	minField_.setX(SLIDER_X_);
	minField_.setY(SLIDER_Y_ + SLIDER_HEIGHT_);
	minField_.setWidth(SLIDER_WIDTH_);
	minField_.setHeight(NUMERIC_FIELD_HEIGHT_);

	// Set up the Maximum Value field color and dimensions
	maxField_.setColor(Colors::WHITE);
	maxField_.setX(0);
	maxField_.setY(MAX_FIELD_Y_);
	maxField_.setWidth(MAX_LABEL_WIDTH_);
	maxField_.setHeight(NUMERIC_FIELD_HEIGHT_);

	// Add Min and Max values and Slider Box to container
	addDrawable(&minField_);
	addDrawable(&sliderBox_);
	addDrawable(&stdDeviationBox_);
	addDrawable(&maxField_);

	// Color triangles used for slider button pointers
	lowerTriangle_.setColor(Colors::BLACK);
	lowerTriangle_.setFillColor(Colors::BLACK);
	upperTriangle_.setColor(Colors::BLACK);
	upperTriangle_.setFillColor(Colors::BLACK);

	// Set up the Value Pointer container and contents
	valuePointer_.setWidth(VALUE_FIELD_WIDTH_ + VALUE_TRIANGLE_WIDTH_);
	valuePointer_.setHeight(VALUE_FIELD_HEIGHT_);

	valueDisplay_.setWidth(VALUE_FIELD_WIDTH_);
	valueDisplay_.setHeight(VALUE_FIELD_HEIGHT_);
	valueDisplay_.setValue(value_);

	valueBox_.setColor(Colors::WHITE);
	valueBox_.setFillColor(Colors::WHITE);
	valueTriangle_.setColor(Colors::WHITE);
	valueTriangle_.setFillColor(Colors::WHITE);
	valueDisplay_.setColor(Colors::BLACK);

	valuePointer_.addDrawable(&valueBox_);
	valuePointer_.addDrawable(&valueTriangle_);
	valuePointer_.addDrawable(&valueDisplay_);

	valuePointer_.setY(valueToPoint_(value_) - VALUE_FIELD_HEIGHT_ / 2);

	addDrawable(&valuePointer_);

	// Add Alarm Limit Setting buttons.  NOTE: these buttons must be added
	// at the very end because they have to draw on top of all other drawables
	// in the Alarm Slider container.

	// Check if lower setting button is needed
	if (LOWER_SETTING_ID_ != SettingId::NULL_SETTING_ID)
	{													// $[TI1]
		// Set up Lower Limit Button container and contents
		lowerButtonCntnr_.setWidth(BUTTON_X_ +
								AlarmLimitSettingButton::BUTTON_WIDTH);
		lowerButtonCntnr_.setHeight(BUTTON_Y_ +
								AlarmLimitSettingButton::BUTTON_HEIGHT);

		// Fill Lower Limit Button container with button and triangle
		lowerButtonCntnr_.addDrawable(&lowerButton_);
		lowerButtonCntnr_.addDrawable(&lowerTriangle_);
		lowerButtonCntnr_.setX(SLIDER_X_ + SLIDER_WIDTH_ / 2 - 1);
		lowerButtonCntnr_.setY(SLIDER_Y_ + SLIDER_HEIGHT_);

		// Put Lower Limit button into AlarmSlider
		addDrawable(&lowerButtonCntnr_);
	}													// $[TI2]

	// Check if upper setting button is needed
	if (UPPER_SETTING_ID_ != SettingId::NULL_SETTING_ID)
	{													// $[TI3]
		// Set up Upper Limit button container and contents
		upperButtonCntnr_.setWidth(BUTTON_X_ +
								AlarmLimitSettingButton::BUTTON_WIDTH);
		upperButtonCntnr_.setHeight(BUTTON_Y_ +
								AlarmLimitSettingButton::BUTTON_HEIGHT);

		// Fill Lower Limit Button container with button and triangle
		upperButtonCntnr_.addDrawable(&upperButton_);
		upperButtonCntnr_.addDrawable(&upperTriangle_);
		upperButtonCntnr_.setX(SLIDER_X_ + SLIDER_WIDTH_ / 2 - 1);

		// Put Upper Limit button into AlarmSlider
		addDrawable(&upperButtonCntnr_);
	}													// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmSlider  [Destructor]
//
//@ Interface-Description
// Destroys the alarm slider.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmSlider::~AlarmSlider(void)
{
	CALL_TRACE("AlarmSlider::~AlarmSlider(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
// Called to notify this object when a Breath-Delivery subsystem that a 
// Vent-INOP, SVO, Occlusion or Patient Disconnect event occured.
//---------------------------------------------------------------------
//@ Implementation-Description
// For any of the aformentioned events, the patient data pointers and 
// standard deviation box are hidden and to remove them from the display.
//---------------------------------------------------------------------
//@ PreCondition
//	none						 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
AlarmSlider::bdEventHappened(EventData::EventId eventId,
                EventData::EventStatus eventStatus,
                EventData::EventPrompt eventPrompt)
{
	// If we're in SVO, Occlusion or Patient Disconnect then hide the 
	// patient data pointers and wait to display them until new data
	// is available
	if (
 BdGuiEvent::GetEventStatus(EventData::SVO) == EventData::ACTIVE  ||
 BdGuiEvent::GetEventStatus(EventData::OCCLUSION) == EventData::ACTIVE  ||
 BdGuiEvent::GetEventStatus(EventData::PATIENT_CONNECT) != EventData::ACTIVE   ||
 BdGuiEvent::GetEventStatus(EventData::VENT_INOP) == EventData::ACTIVE
		)
	{												// $[TI1.1]
		hidePatientDataPointer();
		hideStdDeviationBox();
	}												// $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Virtual method inherited from Drawable.  Must be called every time the
// slider is about to be displayed.  Syncs the display of the slider with the
// current data in the Settings-Validation and Patient Data subsystems.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call our updateButtonPosition_() and updateValueDisplay_()
// methods to update the positions of the buttons and patient datum
// pointer.  Also, call the activate() method on the setting buttons
// in order to sync their displays with the current settings.
// $[NE01000] The alarm bars shall support a dynamic scaling ... 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::activate(void)
{
	CALL_TRACE("AlarmSlider::activate(void)");

	//-----------------------------------------------------------------
	// Calculate the scaling factors...
	//-----------------------------------------------------------------

	if (LOWER_SETTING_ID_ != SettingId::NULL_SETTING_ID)
	{													// $[TI7]
		// Set the max using the lower setting handle max.
		max_ = ceil(getSubjectPtr_(LOWER_SETTING_ID_)->getMaxLimit());
	}													// $[TI8]

	if (UPPER_SETTING_ID_ != SettingId::NULL_SETTING_ID)
	{													// $[TI9]
		// Set the max using the upper setting handle max (may already have
		// been set above but max_ will be same value for both upper and
		// lower settings).
		max_ = ceil(getSubjectPtr_(UPPER_SETTING_ID_)->getMaxLimit());
	}													// $[TI10]

	// Calculate and store all allowed high limits.
	fullMax_ = max_;
	halfMax_ = ceil(max_ / 2);
	oldMax_ = max_;

	// Set values of Min and Max fields
	minField_.setValue(min_);
	maxField_.setValue(max_);

	// Calculate scaling factor for positioning graphicals in valueToPoint_().
	scaleFactor_ = SLIDER_HEIGHT_ / (max_ - min_);
	changedScale_ = FALSE;


	if (LOWER_SETTING_ID_ != SettingId::NULL_SETTING_ID)
	{  // $[TI1]
		attachToSubject_(LOWER_SETTING_ID_,
						 Notification::APPLICABILITY_CHANGED);
		attachToSubject_(LOWER_SETTING_ID_,
						 Notification::VALUE_CHANGED);

		lowerButton_.activate();
		updateButtonPosition_(&lowerButton_, getSubjectPtr_(LOWER_SETTING_ID_));
	}  // $[TI2]

	if (UPPER_SETTING_ID_ != SettingId::NULL_SETTING_ID)
	{  // $[TI3]
		attachToSubject_(UPPER_SETTING_ID_,
						 Notification::APPLICABILITY_CHANGED);
		attachToSubject_(UPPER_SETTING_ID_,
						 Notification::VALUE_CHANGED);

		upperButton_.activate();
		updateButtonPosition_(&upperButton_, getSubjectPtr_(UPPER_SETTING_ID_));
	}  // $[TI4]

	// Update the graphical patient datum pointer if hidePointer_ is 
	// not turned on.
	if (!hidePointer_)
	{													// $[TI5]
		updateValueDisplay_();
	}													// $[TI6]

	updateAlarmDisplay_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Prepare for the deactivation of this slider.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Detach both the lower and upper setting ids from the Context Subject
//  then deactivate the lower the upper buttons on the slider.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::deactivate(void)
{
	CALL_TRACE("deactivate()");

	setShow(FALSE);

	if (LOWER_SETTING_ID_ != SettingId::NULL_SETTING_ID)
	{  // $[TI1]
		detachFromSubject_(LOWER_SETTING_ID_,
						   Notification::APPLICABILITY_CHANGED);
		detachFromSubject_(LOWER_SETTING_ID_,
						   Notification::VALUE_CHANGED);

		lowerButton_.deactivate();
	}  // $[TI2]

	if (UPPER_SETTING_ID_ != SettingId::NULL_SETTING_ID)
	{  // $[TI3]
		detachFromSubject_(UPPER_SETTING_ID_,
						   Notification::APPLICABILITY_CHANGED);
		detachFromSubject_(UPPER_SETTING_ID_,
						   Notification::VALUE_CHANGED);

		upperButton_.deactivate();
	}  // $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened
//
//@ Interface-Description
// Called when the patient datum value has changed.  The patient datum
// pointer's value and position are changed accordingly.  It is passed
// 'patientDataId', the patient data id for the value.
//---------------------------------------------------------------------
//@ Implementation-Description
// First, turn off hidePointer_ flag.  If the AlarmSlider is not
// currently visible then we simply return.   Then call updateValueDisplay_().
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::patientDataChangeHappened(PatientDataId::PatientItemId)
{
	CALL_TRACE("AlarmSlider::patientDataChangeHappened("
						"PatientDataId::PatientItemId patientDataId)");

	// Turn off hidePointer flag 
	hidePointer_ = FALSE;

	// Get the new value from the Patient Data subsystem
	const BoundedBreathDatum &DATUM = patientDataHandle_.getBoundedValue();
	stdDeviationBuffer_.updateBuffer(DATUM.data.value);

	// Ignore events when invisible.
	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	// Update the patient datum value display.
	updateValueDisplay_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: alarmEventHappened
//
//@ Interface-Description
//  Called when the alarm state has changed.  The alarm information 
//  is changed accordingly.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the slider is not visible then returns immediately. Otherwise, 
//  calls updateValueDisplay_ to update the alarm information.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::alarmEventHappened(void)
{
	CALL_TRACE("AlarmSlider::alarmEventHappened(void)");

	// Ignore events when invisible.
	if (isVisible())
	{													// $[TI1.1]
		// Update the patient datum value display.
		updateAlarmDisplay_();
	}													// $[TI1.2]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: applicabilityUpdate
//
//@ Interface-Description
//  Called when applicability of the setting ids have changed.
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update the slider according to the applicability of the setting ids
//  on this slider (graphics affected: lower button, upper button, slider).
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::applicabilityUpdate(
                              const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject
								)
{
  CALL_TRACE("applicabilityUpdate(qualifierId, pSubject)");

  // get the applicability of "this" setting subject...
  const Applicability::Id  APPLICABILITY = pSubject->getApplicability(qualifierId);

  const SettingId::SettingIdType  SETTING_ID = pSubject->getId();

  // VTi slider is the only one which can be shown or hidden depending
  // on TC option.  It only has 1 setting button for the upper setting limit.
  // one of the settings is now inapplicable, while the other
  // setting is NULL, therefore the whole slider is to be hidden...

  // With the introduction of the low circuit pressure alarm limit, this 
  // algorithm has changed to show or hide the alarm slider button and
  // its pointer based on the underlying setting's applicability.

  // This algorithm hides the button when the setting is not changeable.
  // If the setting associated with this applicability update is not 
  // changeable and the other setting limit is NULL or not visible then
  // the entire slider is hidden. 

  if (SETTING_ID == LOWER_SETTING_ID_)
  {    // $[TI1.1]
    if (APPLICABILITY == Applicability::CHANGEABLE)
    {    // $[TI2.1]
	  lowerButtonCntnr_.setShow(TRUE);
	  setShow(TRUE);
    }
    else 
    {    // $[TI2.2]
	  lowerButtonCntnr_.setShow(FALSE);
	  // $[TI4.1] $[TI4.2]
	  setShow(UPPER_SETTING_ID_ != SettingId::NULL_SETTING_ID && upperButtonCntnr_.isVisible());
    }
  }
  else if (SETTING_ID == UPPER_SETTING_ID_)
  {    // $[TI1.2]
    if (APPLICABILITY == Applicability::CHANGEABLE)
    {    // $[TI3.1]
	  upperButtonCntnr_.setShow(TRUE);
	  setShow(TRUE);
    }
    else 
    {    // $[TI3.2]
	  upperButtonCntnr_.setShow(FALSE);
	  // $[TI5.1] $[TI5.2]
	  setShow(LOWER_SETTING_ID_ != SettingId::NULL_SETTING_ID && lowerButtonCntnr_.isVisible());
    }
  }
  else
  {    // $[TI1.3]
    AUX_CLASS_ASSERTION_FAILURE(SETTING_ID);
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
//  Called when setting values on this slider has changed.
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update button position on the upper or lower button.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::valueUpdate(const Notification::ChangeQualifier,
					     const SettingSubject* pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	// Ignore event is we're invisible
	if (!isVisible())
	{													// $[TI1]
		// Ignore event
		return;
	}													// $[TI2]

	const SettingId::SettingIdType  SETTING_ID = pSubject->getId();

	// Update position of appropriate button
	if (SETTING_ID == LOWER_SETTING_ID_)
	{													// $[TI3]
		updateButtonPosition_(&lowerButton_, pSubject);
	}
	else if (SETTING_ID == UPPER_SETTING_ID_)
	{													// $[TI4]
		updateButtonPosition_(&upperButton_, pSubject);
	}
	else
	{
		// unexpected setting id...
		AUX_CLASS_ASSERTION_FAILURE(SETTING_ID);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateScale_ [Private]
//
//@ Interface-Description
// Called to update the scale factor and the max value displayed on slider.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Based on the current setting value, determine if a change of scale
//	is necessary, then update the display as appropriate.
// $[NE01000] The alarm bars shall support a dynamic scaling ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
AlarmSlider::updateScale_(Real32 value)
{
	if (value <= halfMax_)
	{									// $[TI1]
		max_ = halfMax_;
	}
	else
	{									// $[TI2]
		max_ = fullMax_;
	}

	changedScale_ = (max_ != oldMax_);
	 
	// Scaling changes so update max field.
	if (changedScale_)
	{									// $[TI3]
		scaleFactor_ = SLIDER_HEIGHT_ / (max_ - min_);

		maxField_.setValue(max_);
		oldMax_ = max_;
	}									// $[TI6]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateLowerButtonPosition_ [Private]
//
//@ Interface-Description
// Called when the position of a lower Alarm Limit setting buttons needs updating.
// Passed one parameter, the value displayed on the lower button.
//---------------------------------------------------------------------
//@ Implementation-Description
// We position the lower button an equivalent amount along the slider according
// to the button's current setting value.
// However, if the button's setting is the "OFF" value, position the button
// at the appropriate end of the slider.
//
// $[01046] Each batch setting button is positioned according to its value ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
AlarmSlider::updateLowerButtonPosition_(Real32 value)
{
	// Update the lower limit setting button.  This is necessary after a
	// change in the upper setting value because an auto-scaling of
	// the alarm slider might have happened which screwed up the position
	// of the lower setting button.

	// Check to see if the alarm setting has been switched off!
	if (value == SettingConstants::LOWER_ALARM_LIMIT_OFF)
	{												// $[TI1]
		// Setting switched off.  Position container at bottom of
		// slider.
		lowerButtonCntnr_.setY(valueToPoint_(min_));
	}
	else
	{												// $[TI2]
		// Position button according to setting value
		lowerButtonCntnr_.setY(valueToPoint_(value));
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateButtonPosition_ [Private]
//
//@ Interface-Description
// Called when the position of an Alarm Limit setting buttons needs updating.
// Passed one parameter, 'pButton', a pointer to the button which needs
// positioning
//---------------------------------------------------------------------
//@ Implementation-Description
// We check which button needs positioning, then position it an equivalent
// amount along the slider according to the button's current setting value.
// However, if the button's setting is the "OFF" value, position the button
// at the appropriate end of the slider.
// Auto-scale only alarms with two limits.  Auto-scaling will affect all
// buttons and pointers along the slider.
// $[01046] Each batch setting button is positioned according to its value ...
// $[NE01000] The alarm bars shall support a dynamic scaling ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::updateButtonPosition_(AlarmLimitSettingButton* pButton,
								   const SettingSubject*    pSubject)
{
	Real32 value = ((BoundedValue)pSubject->getAdjustedValue()).value;


	// Is it the lower button which needs positioning?
	if (pButton == &lowerButton_)
	{													// $[TI1]
		updateLowerButtonPosition_(value);
	}
	else
	{													// $[TI2]
		// Do auto-scaling only on alarm sliders with both upper and lower setting
		// limits and only with auto-scaling turned on
		if (isAutoScalingOn_ && LOWER_SETTING_ID_ != SettingId::NULL_SETTING_ID)
		{												// $[TI3]
			updateScale_(value);
		}												// $[TI4]

		// Check to see if the alarm setting has been switched off!
		if (value == SettingConstants::UPPER_ALARM_LIMIT_OFF)
		{												// $[TI5]
			// Setting switched off.  Position container at top of
			// slider.
			upperButtonCntnr_.setY(valueToPoint_(max_) -
								AlarmLimitSettingButton::BUTTON_HEIGHT);
		}
		else
		{												// $[TI6]
			// Position button according to setting value
			upperButtonCntnr_.setY(valueToPoint_(value) -
								AlarmLimitSettingButton::BUTTON_HEIGHT);
		}

		if (changedScale_ && (LOWER_SETTING_ID_ != SettingId::NULL_SETTING_ID))
		{												// $[TI7]
			// Update lower button position, too.
			// Calculate lower setting limit value.
			BoundedValue lowerSettingValue = getSubjectPtr_(LOWER_SETTING_ID_)->getAdjustedValue();
			updateLowerButtonPosition_(lowerSettingValue.value);

			if (!hidePointer_)
			{// $[TI7.1]
				// Update the patient data value pointer.
				updateValueDisplay_();
			}// $[TI7.2]
														// $[TI8]
		}
   	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValueDisplay_ [Private]
//
//@ Interface-Description
// Called when the patient datum value graphical needs updating.  We set
// the value and position the pointer according to this value.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply position and display the new value except in the case where the
// patient datum has timed out (due to not being measured for a certain
// period).  In that case we hide the pointer.
//
// $[01110] When the mode is SIMV, the current measured value ...
// $[01112] When the mode is SPONT, the current measured value ..
// $[NEO1009] The activity range shall not be displayed until at least 
// two breaths of data values are available for the slider..
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::updateValueDisplay_(void)
{
	// If the patient datum has timed out (due to not being measured for a
	// certain period) then remove the display of the value and nothing more.
	const BoundedBreathDatum &DATUM = patientDataHandle_.getBoundedValue();
	Real32 positionValue;		// The value to position the patient data
								// pointer at.  Could be different to
								// patient data value if value is beyond
								// min/max of slider.

	if (DATUM.timedOut)
	{											// $[TI1]
		valuePointer_.setShow(FALSE);			// Hide the value display
		stdDeviationBox_.setShow(FALSE);
		stdDeviationBuffer_.reset();
		return;
	}											// $[TI2]

	// No timeout has occurred so let's display the new value.

	// Display standard deviation box only if number of samples is two or more
	Uint32 numSamples = stdDeviationBuffer_.getNumSamples();

	if (numSamples < VALID_NUM_SAMPLES_TO_DISPLAY)
	{ // $[TI18]
		stdDeviationBox_.setShow(FALSE);
	}
	else
	{ // $[TI19]

		Real32 upperStdDev = stdDeviationBuffer_.getUpper();
		Real32 lowerStdDev = stdDeviationBuffer_.getLower();
		
		// check if standard deviations are greater than upper or lower limit
		// if true then bar is out of range and don't show bar
		if ((lowerStdDev > max_) || (upperStdDev < min_))
		{// $[TI19.1]	
			stdDeviationBox_.setShow(FALSE);
		}
		else
		{// $[TI19.2]
			if (upperStdDev > max_)
			{// $[TI19.2.1]
				upperStdDev = max_;
			}
			// $[TI19.2.2]

			if (lowerStdDev < min_)
			{// $[TI19.2.3]
				lowerStdDev = min_;  //...this shouldn't happen
			}
			// $[TI19.2.4]

			Int32 upperY = valueToPoint_(upperStdDev);
			Int32 lowerY = valueToPoint_(lowerStdDev);
				
			Int32 height = lowerY - upperY;
			height = MAX_VALUE(1, height);
	
			// Display the new value
			stdDeviationBox_.setY(upperY);
			stdDeviationBox_.setHeight(height);
			stdDeviationBox_.setShow(TRUE);				// Make sure it's displayed
		}
	}

	// Get the new value from the Patient Data subsystem
	value_ = DATUM.data.value;
	positionValue = value_;

	// Display the new value
	valuePointer_.setShow(TRUE);				// Make sure it's displayed
	valueDisplay_.setValue(value_);				// Set the value
	valueDisplay_.setPrecision(DATUM.data.precision);		//	and precision
	
	// If the value is out of the min/max range of the slider warp it to
	// the min or max before positioning it.
	//  Since the clipped levels are differant for GUI display than the BD levels
	//  the Gui can not rely on the DATUM.clipped value... cje @ Aug 96
	if (positionValue < min_)
	{													// $[TI3]
		positionValue = min_;
		valueDisplay_.setColor( Colors::BLACK_ON_YELLOW_BLINK_SLOW );
	}
	else if (positionValue > max_)
	{													// $[TI4]
		positionValue = max_;
		valueDisplay_.setColor( Colors::BLACK_ON_YELLOW_BLINK_SLOW );
	}													// $[TI5]
	else
	{
		valueDisplay_.setColor( Colors::BLACK );
	}
			
	// Position the value pointer in relation to its value!
	valuePointer_.setY(valueToPoint_(positionValue) - VALUE_FIELD_HEIGHT_ / 2);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateAlarmDisplay_ [Private]
//
//@ Interface-Description
// Called when the alarm datum graphic needs updating.  We set the color
// of the slider box and button title accordingly.
//---------------------------------------------------------------------
//@ Implementation-Description
// Based on the urgency of the alarm associated with the lower or upper
// slider button, this method sets the color of the title (alarm symbol)
// to the appropriate color using the setTitleHighlightColor() and
// setTitleHighlighted() methods. It sets the color of the slider itself
// using the setColor() and setFillColor() methods.
//
// $[CL01002] The alarm setting bars shall indicate which, if any, of
// corresponding alarms are active.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::updateAlarmDisplay_(void)
{
	// set to default colors until we determine the state of the alarm
	sliderBox_.setColor(Colors::LIGHT_BLUE);
	sliderBox_.setFillColor(Colors::LIGHT_BLUE);

	upperButton_.setTitleColor( Colors::BLACK );
	lowerButton_.setTitleColor( Colors::BLACK );

	lowerButton_.setTitleHighlighted(FALSE);
	upperButton_.setTitleHighlighted(FALSE);

	if ( lowerAlarmName_ != ALARM_NAME_NULL )
	{														// $[TI1.1]
		Alarm::Urgency urgency = AlarmRepository::GetStatus(lowerAlarmName_);

		switch ( urgency )
		{
			case Alarm::HIGH_URGENCY:						// $[TI2.1]
				lowerButton_.setTitleHighlighted(TRUE);
				lowerButton_.setTitleHighlightColor( Colors::WHITE_ON_BLACK_BLINK_FAST );
				sliderBox_.setColor(Colors::RED);
				sliderBox_.setFillColor(Colors::RED);
				break;
			case Alarm::MEDIUM_URGENCY:						// $[TI2.2]
				lowerButton_.setTitleHighlighted(TRUE);
				lowerButton_.setTitleHighlightColor( Colors::BLACK_ON_LIGHT_GREY_BLINK );
				sliderBox_.setColor(Colors::YELLOW);
				sliderBox_.setFillColor(Colors::YELLOW);
				break;
			case Alarm::LOW_URGENCY:						// $[TI2.3]
				lowerButton_.setTitleHighlighted(TRUE);
				lowerButton_.setTitleHighlightColor( Colors::BLACK_ON_LIGHT_GREY_BLINK );
				sliderBox_.setColor(Colors::YELLOW);
				sliderBox_.setFillColor(Colors::YELLOW);
				break;
			default:										// $[TI2.4]
				break;
		}
	}														// $[TI1.2]
				
	if ( upperAlarmName_ != ALARM_NAME_NULL )
	{														// $[TI3.1]
		Alarm::Urgency urgency = AlarmRepository::GetStatus(upperAlarmName_);
	
		switch ( urgency )
		{
			case Alarm::HIGH_URGENCY:						// $[TI4.1]
				upperButton_.setTitleHighlighted(TRUE);
				upperButton_.setTitleHighlightColor( Colors::WHITE_ON_BLACK_BLINK_FAST );
				sliderBox_.setColor(Colors::RED);
				sliderBox_.setFillColor(Colors::RED);
				break;
			case Alarm::MEDIUM_URGENCY:						// $[TI4.2]
				upperButton_.setTitleHighlighted(TRUE);
				upperButton_.setTitleHighlightColor( Colors::BLACK_ON_LIGHT_GREY_BLINK );
				sliderBox_.setColor(Colors::YELLOW);
				sliderBox_.setFillColor(Colors::YELLOW);
				break;
			case Alarm::LOW_URGENCY:						// $[TI4.3]
				upperButton_.setTitleHighlighted(TRUE);
				upperButton_.setTitleHighlightColor( Colors::BLACK_ON_LIGHT_GREY_BLINK );
				sliderBox_.setColor(Colors::YELLOW);
				sliderBox_.setFillColor(Colors::YELLOW);
				break;
			default:										// $[TI4.4]
				break;
		}
	}														// $[TI3.2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueToPoint_
//
//@ Interface-Description
// Translates a floating point value into the equivalent Y co-ordinate
// on the slider which is returned as a Uint16.
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the previously calculated scaleFactor_ value we use a simple
// calculation to return the correct Y coordinate.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint16
AlarmSlider::valueToPoint_(Real32 sliderValue)
{
	return ((Uint16)(SLIDER_Y_ + SLIDER_HEIGHT_ - scaleFactor_ * sliderValue
						+ 0.5));
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hidePatientDataPointer 
//
//@ Interface-Description
//	This method is called when we want to hide the Patient Data pointer
//	from being displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply set the show flag of the valuePointer to FALSE and the
//	hidePointer_ flag to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void 
AlarmSlider::hidePatientDataPointer()
{
	// $[TI1]
	valuePointer_.setShow(FALSE);
	hidePointer_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hideStdDeviationBox
//
//@ Interface-Description
//	This method is called when we want to hide the Patient Data standard
//	deviation box from being displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply set the show flag of the stdDeviationBox to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void 
AlarmSlider::hideStdDeviationBox()
{
	// $[TI1]
	stdDeviationBox_.setShow(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setLabelText
//
//@ Interface-Description
//	This method is called when we want change the text of 
//  minLabel which is a text field.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply set the text on the minLabel by calling the setText method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void 
AlarmSlider::setLabelText(const wchar_t * pString)
 
{
   minLabel_.setText(pString);
   addDrawable(&minLabel_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setUpperButtonMessageId
//
//@ Interface-Description
// We are passed a messageId specifying the messageId shall be displayed
// for this setting button.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply store the messageId in upperButton_.
//---------------------------------------------------------------------
//@ PreCondition
//  N/A
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::setUpperButtonMessageId(const StringId messageId)
{
  	// Store the next message in the upper button
	upperButton_.setMessageId(messageId);					// $[TI1]
    upperButtonCntnr_.addDrawable(&upperButton_);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setUpperAlarm
//
//@ Interface-Description
//  Called when the upper alarm changes. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  update local varible upperAlarmName_
//---------------------------------------------------------------------
//@ PreCondition
//  newAlarm must be a valid Alarm
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::setUpperAlarm(AlarmName newAlarm)
{
       CALL_TRACE("AlarmSlider::setUpperAlarm( newAlarm )");

       CLASS_PRE_CONDITION( newAlarm >= ALARM_NAME_NULL && 
                                         newAlarm < MAX_NUM_ALARMS );

      // $[TI1]
      upperAlarmName_ = newAlarm;
       
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmSlider::SoftFault(const SoftFaultID  softFaultID,
					   const Uint32       lineNumber,
					   const char*        pFileName,
					   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMSLIDER,
									lineNumber, pFileName, pPredicate);
}
