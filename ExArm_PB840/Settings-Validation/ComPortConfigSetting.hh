 
#ifndef ComPortConfigSetting_HH
#define ComPortConfigSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  ComPortConfigSetting - Communications Port Device Selection
//          Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ComPortConfigSetting.hhv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: hct    Date:  14-FEB-2000    DR Number: 5493
//  Project: GUIComms
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
//@ End-Usage


class ComPortConfigSetting : public BatchDiscreteSetting
{
  public:
    ComPortConfigSetting(SettingId::SettingIdType settingId);
    virtual ~ComPortConfigSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;
    virtual Boolean isEnabledValue(const DiscreteValue value) const; 

    
    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
   virtual Boolean  calcDependentValues_(const Boolean isValueIncreasing);
  private:
    ComPortConfigSetting(void);			// not implemented...
    ComPortConfigSetting(const ComPortConfigSetting&);	// not implemented...
    void  operator=(const ComPortConfigSetting&);	        // not implemented...

    //@ Data-Member:  settingId_
    // Setting Id for a comm port's device selection setting.
	SettingId::SettingIdType  settingId_;

};


#endif // ComPortConfigSetting_HH 
