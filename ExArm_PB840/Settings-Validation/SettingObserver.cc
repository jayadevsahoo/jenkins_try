#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N ================
//@ Class:  SettingObserver - SettingObserver Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a base class for all classes that wish to monitor the state
//  of any setting.  The public interface includes three virtual methods
//  that a derived class may override:  'applicabilityUpdate()' is to be
//  overridden to respond to applicability changes to a monitored setting;
//  'valueUpdate()' is to be overridden to respond to value changes to a
//  monitored setting; and 'doRetainAttachment()' is to be overridden
//  to return a 'TRUE' (instead of the default 'FALSE') for those observers
//  that wish to retain their subject attachments across transitions to and
//  from SST.
//
//  The derived classes of this class also get access to the following
//  protected methods:  'getSubjectPtr_()' which can provide access to a
//  particular subject; and 'attachToSubject_()' and 'detachFromSubject_()'
//  for managing the monitoring of subjects.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingObserver.ccv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//===================================================================

#include "SettingObserver.hh"

//@ Usage-Classes
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Public Methods...
//
//====================================================================

//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method: ~SettingObserver()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this SettingObserver.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingObserver::~SettingObserver(void)
{
  CALL_TRACE("~SettingObserver()");
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  applicabilityUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all derived
//  classes that wish respond to changes in the applicability of this
//  observer's monitored subject.  The default response is to ignore the
//  applicability changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingObserver::applicabilityUpdate(const Notification::ChangeQualifier,
				     const SettingSubject*)
{
  CALL_TRACE("applicabilityUpdate(qualifierId, pSubject)");

  // the default behavior is to do nothing...
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all derived
//  classes that wish respond to changes in the value of this observer's
//  monitored subject.  The default response is to ignore the value changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingObserver::valueUpdate(const Notification::ChangeQualifier,
			     const SettingSubject*)
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");

  // the default behavior is to do nothing...
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all derived
//  observer classes that wish to retain their attachment to the subject
//  indicated by 'pSubject'.  With task-level state changes, all subjects
//  are informed to detach from their observers.  This method allows for
//  those observers who will continue monitoring the subject across the
//  state changes, to stay attached.
//
//  This method returns the default response of 'FALSE' -- meaning don't
//  preserve the attachment.  Those observer classes that do want to
//  preserve the attachment, need to override this method and return 'TRUE'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SettingObserver::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(FALSE);
}  // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingObserver::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, SETTING_OBSERVER,
                          lineNumber, pFileName, pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  SettingObserver()  [Constructor]
//
//@ Interface-Description
//  Create a default setting subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingObserver::SettingObserver(void)
{
  CALL_TRACE("SettingObserver()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  attachToSubject_(subjectId, changeId)  [const]
//
//@ Interface-Description
//  Attach this observer to the subject identified by 'subjectId', for
//  monitoring the type of change indicated by 'changeId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsId(subjectId))
//  (changeId < SettingSubject::NUM_SETTING_CHANGE_TYPES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingObserver::attachToSubject_(
			      const SettingId::SettingIdType      subjectId,
			      const Notification::SettingChangeId changeId
			         )
{
  CALL_TRACE("attachToSubject_(subjectId, changeId)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsId(subjectId)), subjectId);
  AUX_CLASS_PRE_CONDITION((changeId < Notification::NUM_SETTING_CHANGE_TYPES),
			  ((subjectId << 16) | changeId));

  SettingsMgr::GetSettingSubjectPtr(subjectId)->attachObserver(this, changeId);
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  detachFromSubject_(subjectId, changeId)  [const]
//
//@ Interface-Description
//  Detach this observer from monitoring the type of change indicated by
//  'changeId', of its subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsId(subjectId))
//  (changeId < SettingSubject::NUM_SETTING_CHANGE_TYPES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingObserver::detachFromSubject_(
			      const SettingId::SettingIdType      subjectId,
			      const Notification::SettingChangeId changeId
				   ) const
{
  CALL_TRACE("detachToSubject_(subjectId, changeId)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsId(subjectId)), subjectId);
  AUX_CLASS_PRE_CONDITION((changeId < Notification::NUM_SETTING_CHANGE_TYPES),
			  ((subjectId << 16) | changeId));

  SettingsMgr::GetSettingSubjectPtr(subjectId)->detachObserver(this, changeId);
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  activateSubject_(subjectId, qualifierId)  [const]
//
//@ Interface-Description
//  Activate the setting subject, indicated by 'subjectId', and process
//  this activation based on 'qualifierId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsId(subjectId))
//  (qualifierId < Notification::NUM_CHANGE_QUALIFIERS)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingObserver::activateSubject_(
			      const SettingId::SettingIdType      subjectId,
			      const Notification::ChangeQualifier qualifierId
				 )
{
  CALL_TRACE("activateSubject_(subjectId, qualifierId)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsId(subjectId)), subjectId);
  AUX_CLASS_PRE_CONDITION((qualifierId < Notification::NUM_CHANGE_QUALIFIERS),
			  ((subjectId << 16) | qualifierId));

  SettingsMgr::GetSettingSubjectPtr(subjectId)->updateApplicability(qualifierId);
}   // $[TI1]
