
#ifndef UtilityClassId_HH
#define UtilityClassId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  UtilityClassId - Ids of all of the Utilities Subsytem's Modules.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/UtilityClassId.hhv   25.0.4.0   19 Nov 2013 14:35:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: gdc     Date: 16-Jun-1998   DCS Number: 5055
//  Project:  Color
//  Description:
//      Initial release.
//
//   Revision: 002   By: sah   Date: 04-Mar-1997   DCS Number: 1814
//   Project:  Sigma (R8027)
//   Description:
//	Moved 'DiagnosticCode' to another subsystem.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//        Initial version 
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


//@ Type:  UtilityClassId
// Id for all of this subsystem's classes.
enum UtilityClassId
{
  BIT_UTILITIES = 0,
  CALL_TRACE_CLASS = 1,
  CRC_UTILITIES = 2,
  FAULT_HANDLER = 3,
  MATH_UTILITIES = 4,
  PLACEMENT_NEW = 5,
  UTILITIES_CLASS = 6,
  STDIO = 7,
  GAINSMANAGER_CLASS = 8,
  ALLOCATOR = 9,
  POOL_ALLOCATOR = 10,
  XALLOCATOR = 11,
  PDSIMULATIONSERVER_CLASS = 12,
  STRING_CONVERTER = 13,
  NUM_UTILITY_CLASSES
};


#endif // UtilityClassId_HH 
