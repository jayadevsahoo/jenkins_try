#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendCursorSlider - A slider for the trend cursor position
//---------------------------------------------------------------------
//@ Interface-Description 
//  The TrendCursorSlider is a Container that consists of a horizontal
//  bar or timeline along which a cursor button moves to indicate a
//  selected row within a set of trending data. The
//  TrendTableSubScreen and TrendGraphsSubScreen provide this slider to
//  position a cursor within the trending data presented in both
//  graphical and tabular forms. This slider contains a setting button
//  that moves along the horizontal timeline and sets the trend cursor
//  position setting as the setting changes.
// 
//  This class contains all the graphical elements for the slider
//  including the timeline, axis markers and date/time labels, the
//  cursor setting button that displays a time from the selected trend
//  data, a triangular pointer pointing to a single location on the
//  timeline and a cursor line to overlay the graphical data. The time
//  axis with the timeline and date/time labels as well as the cursor
//  line can be enabled or disabled by the client via "set" methods.
// 
//  The slider receives trend cursor position setting changes via
//  SettingObserver::valueUpdate() from the Setting-Validation subsystem
//  that will drive it to reposition the cursor on the slider.
// 
//  The TrendCursorSlider is a TrendEventTarget that registers with the
//  TrendDataSetAgent for callbacks when new trend data is ready. 
//  TrendDataSetAgent calls back trendDataReady that updates the cursor
//  and slider with the current cursor setting and trend data.
// 
//  The TrendCursorSlider is a ButtonTarget for receiving button up/down
//  events when its cursor button is pressed. Upon receiving a button
//  down callback, the cursor line (if enabled) is drawn overlaying the
//  graphs. The cursor line overlay is removed upon receiving a button
//  up event.
// 
//  Once constructed, the object should be activated via a call to
//  activate() to synchronize it with the current settings and data.
// 
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the functionality needed to display and use trending
//  data cursor.
// 
//---------------------------------------------------------------------
//@ Implementation-Description 
//  The constructor sets up most of the graphical elements. The activate
//  method synchronizes the graphical elements with the data it
//  represents prior to display.
// 
//  This class uses the DirectDraw overlay feature of the
//  GUI-Foundations graphics library to draw the cursor line over the
//  graphs. Since both the graphs and the cursor line are drawn directly
//  in the frame buffer, the graphics library emulates an overlay
//  graphics plane for the cursor line. As the cursor line is drawn, the
//  graphics library first saves the underlay graphics in off-screen
//  memeory. When the cursor is moved, the underlay graphics are
//  restored from off-screen memory.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  No special fault handling strategies apart from the usual assertions
//  and pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendCursorSlider.ccv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date: 26-Feb-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//  	Initial version.
//=====================================================================

#include "TrendCursorSlider.hh"


//@ End-Usage
#include "AdjustPanel.hh"
#include "BaseContainer.hh"
#include "MiscStrs.hh"
#include "SettingConstants.hh"
#include "SettingSubject.hh"
#include "TextUtil.hh"
#include "TrendDataMgr.hh"
#include "TrendDataSet.hh"
#include "TrendDataSetAgent.hh"
//@ Usage-Classes

//@ Code...

// Initialize static constants.
static const Int32 SLIDER_BOX_WIDTH_ = TrendDataMgr::MAX_ROWS + 1;
static const Int32 SLIDER_BOX_HEIGHT_ = 1;
static const Int32 CURSOR_TRIANGLE_HEIGHT_ = 5;

static const Int32 TICK_HEIGHT_ = 2;
static const Int32 NUM_X_DIVISION_ = 6;
static const Int32 TICK_INTERVAL_ = (SLIDER_BOX_WIDTH_ - 1) / NUM_X_DIVISION_;
static const Int32 X_LABEL_GAP_ = 3;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendCursorSlider()  [Constructor]
//
//@ Interface-Description
//	Creates a trend cursor slider.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Initialize member data. Set up and position the graphical elements
//  of the slider.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
TrendCursorSlider::TrendCursorSlider(SubScreen* pFocusSubScreen) 
:   cursorSettingButton_(   SettingId::TREND_CURSOR_POSITION,
							MiscStrs::TREND_CURSOR_HELP_MSG, 
							pFocusSubScreen),
	cursorTriangle_(        0, 0, 
							2 * (CURSOR_TRIANGLE_HEIGHT_ - 1), 0,
							CURSOR_TRIANGLE_HEIGHT_ - 1, CURSOR_TRIANGLE_HEIGHT_ - 1),
	sliderBox_(             0, 0, SLIDER_BOX_WIDTH_, SLIDER_BOX_HEIGHT_),
	cursorLine_(            0, 0, 0, 0),
	showCursorLine_(        TRUE),
	isCursorPressed_(       FALSE),
	rTrendDataSetAgent_(    TrendDataSetAgent::GetTrendDataSetAgent()),
	isCursorLineShown_(     FALSE),
	CURSOR_BUTTON_WIDTH_(   cursorSettingButton_.getWidth()),
	CURSOR_BUTTON_HEIGHT_(  cursorSettingButton_.getHeight())
{
	// position the drawables within this container
	cursorSettingButton_.setX(0);
	cursorSettingButton_.setY(0);
	cursorTriangle_.setX(CURSOR_BUTTON_WIDTH_ / 2 - CURSOR_TRIANGLE_HEIGHT_ + 1);
	cursorTriangle_.setY(CURSOR_BUTTON_HEIGHT_);
	cursorLine_.setX(CURSOR_BUTTON_WIDTH_ / 2);
	cursorLine_.setY(CURSOR_BUTTON_HEIGHT_ + CURSOR_TRIANGLE_HEIGHT_);

	// position axis container drawables
	sliderBox_.setX(CURSOR_BUTTON_WIDTH_ / 2);
	sliderBox_.setY(TICK_HEIGHT_);
	sliderBox_.setColor(Colors::WHITE);
	axisContainer_.addDrawable(&sliderBox_);

	CLASS_ASSERTION(NUM_X_DIVISION_ < MAX_TICK_MARKS_);

	wchar_t tempBuf[40];

	for (Int32 i=0; i <= NUM_X_DIVISION_; i++)
	{
		Int32 xPos = sliderBox_.getX() + i * TICK_INTERVAL_;
		tickMark_[i].setVertices(0, 0, 0, TICK_HEIGHT_);
		tickMark_[i].setX(xPos);
		tickMark_[i].setY(0);
		tickMark_[i].setColor(Colors::WHITE);
		axisContainer_.addDrawable(&tickMark_[i]);

		// position tick value labels using zeroes for proper spacing
		swprintf(tempBuf, L"{p=8,y=7:00:00}");
		timeLabels_[i].setText(tempBuf);
		timeLabels_[i].setX(xPos - timeLabels_[i].getWidth() / 2);
		timeLabels_[i].setY(sliderBox_.getY() + SLIDER_BOX_HEIGHT_ + X_LABEL_GAP_);
		swprintf(tempBuf, L"{p=8,y=8: }");
		timeLabels_[i].setText(tempBuf);

		swprintf(tempBuf, L"{p=8,y=8:00-00}");
		dateLabels_[i].setText(tempBuf);
		dateLabels_[i].setX(xPos - dateLabels_[i].getWidth() / 2);
		dateLabels_[i].setY(sliderBox_.getY() + SLIDER_BOX_HEIGHT_ + X_LABEL_GAP_ + 10);
		swprintf(tempBuf, L"{p=8,y=8: }");
		dateLabels_[i].setText(tempBuf);

		if (i == 0 || i == NUM_X_DIVISION_)
		{
			timeLabels_[i].setColor(Colors::WHITE);
			dateLabels_[i].setColor(Colors::WHITE);
		}
		else
		{
			timeLabels_[i].setColor(Colors::LIGHT_BLUE);
			dateLabels_[i].setColor(Colors::LIGHT_BLUE);
		}
		axisContainer_.addDrawable(&timeLabels_[i]);
		axisContainer_.addDrawable(&dateLabels_[i]);
	}

	axisContainer_.setX(0);
	axisContainer_.setY(CURSOR_BUTTON_HEIGHT_ + 2);
	axisContainer_.setHeight(SLIDER_BOX_HEIGHT_ + X_LABEL_GAP_ + 20);
	axisContainer_.setWidth(SLIDER_BOX_WIDTH_ + CURSOR_BUTTON_WIDTH_);

	addDrawable(&axisContainer_);

	// add the trend cursor button
	cursorTriangle_.setColor(Colors::YELLOW);
	cursorTriangle_.setFillColor(Colors::YELLOW);

	cursorButtonCntnr_.setWidth(CURSOR_BUTTON_WIDTH_);
	cursorButtonCntnr_.setHeight(CURSOR_BUTTON_HEIGHT_ + CURSOR_TRIANGLE_HEIGHT_);

	// add the cursor container to this container at upper left
	cursorButtonCntnr_.setX(0);
	cursorButtonCntnr_.setY(0);

	cursorSettingButton_.setButtonCallback(this); 
	cursorSettingButton_.usePersistentFocus(); 
	cursorSettingButton_.setSingleSettingAcceptEnabled(TRUE); 

	// Add the button and triangle to cursorButtonCntnr_ 
	cursorButtonCntnr_.addDrawable(&cursorSettingButton_);
	cursorButtonCntnr_.addDrawable(&cursorTriangle_);

	// add the cursor/triangle container to this slider
	addDrawable(&cursorButtonCntnr_);

	// since the cursor line is drawn in the overlay plane, it should not 
	// be included in the normal refresh cycle so setShow is set FALSE
	cursorLine_.setShow(FALSE);
	cursorLine_.setColor(Colors::YELLOW);
	addDrawable(&cursorLine_);

	// Dimension the whole container

	// allow for the cursor button to extend half-way over each end of the slider box
	setWidth(SLIDER_BOX_WIDTH_ + CURSOR_BUTTON_WIDTH_);

	// allow for all the graphical elements except the cursor line that
	// is drawn directly on the display 
	setHeight(axisContainer_.getY() + axisContainer_.getHeight());

	// we attach to the settings callback in activate()

	// Register this class to the trend data set agent to notify of
	// data set changes through trendDataReady
	TrendDataSetAgent::RegisterTarget(this);  

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendCursorSlider  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendCursorSlider::~TrendCursorSlider(void)
{
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Virtual method inherited from Drawable. Must be called every time
//  the slider is about to be displayed. Syncs the display of the
//  slider with the current data in the Trend-Database and
//  Settings-Validation subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Updates the position of the cursor using updateCursor_().
//  Calls the cursor setting button activate() method to start accepting
//  changes to the trend cursor position setting through this button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendCursorSlider::activate(void)
{
	attachToSubject_(SettingId::TREND_CURSOR_POSITION, Notification::VALUE_CHANGED);

	cursorSettingButton_.activate();

	// get the cursor to the current location setting 
	updateCursor_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//	Prepare for the deactivation of this slider.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Detach setting ids from the Context Subject then deactivate the
//  setting button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendCursorSlider::deactivate(void)
{
	detachFromSubject_(SettingId::TREND_CURSOR_POSITION, Notification::VALUE_CHANGED);

	cursorSettingButton_.deactivate();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
//  Called when setting values on this slider has changed.
// >Von
//  qualifierId		The context in which the change happened, either
//                  ACCEPTED or ADJUSTED.
// 
//  pSubject		The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update cursor button position using updateCursor_().
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendCursorSlider::valueUpdate(const Notification::ChangeQualifier,
									const SettingSubject* pSubject)
{
	// Ignore event if we're invisible
	if (isVisible())
	{
		const SettingId::SettingIdType  SETTING_ID = pSubject->getId();
		AUX_CLASS_ASSERTION(SETTING_ID == SettingId::TREND_CURSOR_POSITION, SETTING_ID);

		// Update position of appropriate button
		updateCursor_();
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: trendDataReady [virtual]
//
//@ Interface-Description 
//  Callback from TrendDataSetAgent when a new trend dataset is
//  available. Updates the time axis based on this new data. The cursor
//  position is driven by the trend cursor setting callback and doesn't
//  need to be updated here.
//---------------------------------------------------------------------
//@ Implementation-Description 
//	none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendCursorSlider::trendDataReady(TrendDataSet& rTrendDataSet)
{
	updateXAxis_(rTrendDataSet);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateCursor_ [Private]
//
//@ Interface-Description 
//  Called to update the position of the cursor to the current trend
//  cursor position setting. Also sets the cursor button time to the
//  corresponding time stamp from the current trend dataset.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Gets the trend cursor position from the setting object. Gets the
//  time stamp for the row corresponding to the cursor position from the
//  trend dataset.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendCursorSlider::updateCursor_(void)
{
	// cursorPosition is the displacement from the right hand side of the graph
	Real32 cursorPosition = 
		-BoundedValue(getSubjectPtr_(SettingId::TREND_CURSOR_POSITION)->getAcceptedValue()).value;

	Int32 xPos = sliderBox_.getX() + SLIDER_BOX_WIDTH_ - cursorPosition - 1;

	// Position button according to setting value
	cursorButtonCntnr_.setX( xPos - CURSOR_BUTTON_WIDTH_ / 2 );

	// get the latest valid trend data set from the TrendDataSetAgent
	const TrendDataSet& rTrendDataSet = rTrendDataSetAgent_.getTrendDataSet();

	Boolean isCurrentTime = (rTrendDataSetAgent_.isDataSetMostCurrent()) && (cursorPosition == 0);

	cursorSettingButton_.setTime(rTrendDataSet.getTrendTimeStamp(cursorPosition), isCurrentTime);

	cursorButtonCntnr_.setShow(TRUE);

	if (isVisible())
	{
		if (showCursorLine_ && isCursorActive())
		{
			// draw the cursor line
			setDirectDraw(TRUE);
			cursorLine_.removeOverlay();
			cursorLine_.setX(xPos);
			cursorLine_.drawOverlay();
			setDirectDraw(FALSE);
			isCursorLineShown_ = TRUE;
		}

		// repaint cursor button now so we don't lag behind the cursor line 
		getBaseContainer()->repaint();
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateXAxis
//
//@ Interface-Description
//  Labels the X axis with time and date labels.  
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Exits immediately if the axis container is not visible. Otherwise
//  gets the time stamp for each tick on the time axis from the trend
//  dataset and labels the tick accordingly. Note: a row in the dataset
//  is a column on the display so each pixel on the time axis (X axis)
//  is a row in the dataset.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void TrendCursorSlider::updateXAxis_(const TrendDataSet& rTrendDataSet)
{
	if (!axisContainer_.isVisible())
	{
		return;
	}

	wchar_t timeBuf[40];
	wchar_t dateBuf[40];

	CLASS_ASSERTION(NUM_X_DIVISION_ < countof(timeLabels_));
	CLASS_ASSERTION(NUM_X_DIVISION_ < countof(dateLabels_));

	Int32 index = NUM_X_DIVISION_;

	for (Int32 row = 0; row < TrendDataMgr::MAX_ROWS; row++)
	{
		if ((row == 0) || ((row + 1) % TICK_INTERVAL_ == 0))
		{
			if (rTrendDataSet.getTrendTimeStamp(row).isInvalid())
			{
				swprintf(timeBuf, L"{p=8,y=8:------}");
				swprintf(dateBuf, L"{p=8,y=8: }");
			}
			else
			{
				TextUtil::FormatTime(timeBuf, L"{p=8,y=7:%H:%M}", 
									 rTrendDataSet.getTrendTimeStamp(row));
				TextUtil::FormatTime(dateBuf, L"{p=8,x=1,y=8:%p}",
									 rTrendDataSet.getTrendTimeStamp(row));
			}
			timeLabels_[index].setText(timeBuf);
			dateLabels_[index].setText(dateBuf);
			index--;
		}
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened [virtual]
//
//@ Interface-Description 
//  This is a ButtonTarget virtual method called when the cursor button
//  is pressed down. It updates the cursor according to the current
//  cursor settings.
//---------------------------------------------------------------------
//@ Implementation-Description 
//	Calls updateCursor_ to do the real work. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendCursorSlider::buttonDownHappened(Button *pButton, Boolean)
{
	CLASS_PRE_CONDITION(pButton == &cursorSettingButton_);

	updateCursor_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened [virtual]
//
//@ Interface-Description 
//  This is a ButtonTarget virtual method called when the cursor setting
//  button is popped up. We remove the cursor line overlay if it's
//  displayed.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Uses the DirectDraw overlay feature to remove the line directly from
//  the frame buffer and restore the graphics that were there prior to
//  cursor line overlay.
//---------------------------------------------------------------------
//@ PreCondition
//  The callback is for the cursor setting button.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void TrendCursorSlider::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
	CLASS_PRE_CONDITION(pButton == &cursorSettingButton_);

	if (isVisible() && showCursorLine_)
	{
		setDirectDraw(TRUE);
		cursorLine_.removeOverlay();
		setDirectDraw(FALSE);
		isCursorLineShown_ = FALSE;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setShowAxis
//
//@ Interface-Description
//  Enables/disables the display of the axis & labels.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendCursorSlider::setShowAxis(Boolean showAxis)
{
	axisContainer_.setShow(showAxis);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableCursor
//
//@ Interface-Description
//	Disables (sets to flat) the cursor button. Used by client to 
//	deactivate cursor during a database request.
//---------------------------------------------------------------------
//@ Implementation-Description 
//	Set cursor button flat. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendCursorSlider::disableCursor(void)
{
	cursorSettingButton_.setToFlat();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableCursor
//
//@ Interface-Description
//  Enables (sets to up) the cursor button. Used by client to reactivate
//  cursor after database request is complete.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  If the button is flat (disabled) then pop it up, otherwise do
//  nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendCursorSlider::enableCursor(void)
{
	if (cursorSettingButton_.getButtonState() == Button::FLAT)
	{
		cursorSettingButton_.setToUp();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendCursorSlider::SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDCURSORSLIDER,
							lineNumber, pFileName, pPredicate);
}
