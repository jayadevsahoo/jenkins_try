#include "stdafx.h"
#include "AmbientLightSensor.h"
#include "IOExpanderXRA1201.h"
#include "GlobalObjects.h"

AmbientLightSensor_t::AmbientLightSensor_t(HANDLE FileHandle, uint8_t BusAddress, I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout = 30) : 
					I2C(FileHandle, BusAddress, SubAddrMode, BaudIndex)
{
	UNUSED_SYMBOL(Timeout);	
// Resolution Mode
// High 0x10 120 ms
// Med  0x13  16 ms
// Low  0x16 2.9 ms
    ResolutionModeSetting = 0x13; // Med
//    InstructionSetting = 0x23;    // One shut
}

AmbientLightSensor_t::~AmbientLightSensor_t(void)
{
}
//#############################################################################
//
//  ToggleTheAmbientLightSensor - This function toggles ambient sensor to 
//  initiate it
//
//#############################################################################

void AmbientLightSensor_t::ToggleTheAmbientLightSensor(void)
{ 
}
static const BYTE AMBIENT_SENSOR_DELAY = 180;

//#############################################################################
//
//  I2cThread_t::ProgramAmbientLightSensor
// 
//#############################################################################

bool AmbientLightSensor_t::ProgramAmbientLightSensor(BYTE ResolutionMode)
{
	bool	bResult = true;
    ResolutionModeSetting = ResolutionMode;
    ToggleTheAmbientLightSensor();
    Sleep(24); // wait for I2C address is settelling
	if(I2C::WriteDevice(ResolutionMode) == I2C_SUCCESSFUL) // Now read the two data bytes
    {
        bResult = true;
    }
    else
    {
        bResult = false;
    }	
    Sleep(AMBIENT_SENSOR_DELAY);
    return bResult;
}
//#############################################################################
//
//  I2cThread_t::ReadAmbientLightSensor
//
//#############################################################################

bool AmbientLightSensor_t::ReadAmbientLightSensor(uint16_t *Reading)
{
    union 
    { 
      uint16_t WReading;
      uint8_t   Readings[2];
    } WordArray;

	bool	bResult;
    size_t size = sizeof(uint16_t);

	if(I2C::ReadDevice(WordArray.Readings, size) == I2C_SUCCESSFUL) // Now read the two data bytes
    {
        bResult = true;
        *Reading = WordArray.WReading;
    }
    else
    {
        bResult = false;
    }	
    return bResult;
} 
