/*****************************************************************************/
/*   Module Name:    registry.h                                         
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                  
/*   Description of Change                          By                Date           
/*   ---------------------                         -----             ------         
/*   Added a definition Alarm Log file key.        RHJ             27 Nov,2007    
/*                                                        
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _REGISTRY_H_
#define _REGISTRY_H_

/*
	SAMPLE REGISTRY LAYOUT:
	
HKEY_LOCAL_MACHINE*
	|
	|-Software
		|
		|-MNPB*
			|
			|-CPVT*
				|
				|-LANGUAGES=",english,french,spanish,german"
				|
				|-USERS=",John,Rob,Dave"
				|
				|-840 Params*
				|	|-AUTODETECT=...
				|	|-BAUD=...
				|	|-DATABITS=...
				|	|-PARITY=...
				|	|-PORT=...
				|	|-STOPBITS=...
				|	
				|-PTS Params*
				|	|-BAUD=...
				|	|-DATABITS=...
				|	|-PARITY=...
				|	|-PORT=...
				|	|-STOPBITS=...
				|
				|-DEFAULT OPTIONS*
				|	|-LANGUAGE="spanish"
				|
				|-CSE ENABLED*
				|	|-DEFAULT="1"
				|
				|-REPORTS*
				|	|-Crystal Reports=...
				|	|-Diag Log=...
				|	|-Info Log=...
				|	|-Test Log=...
				|
				|-Users
					|
					|-John*
					|	|-LANGUAGE="english"
					|	|-Password=""
					|
					|-Rob*
					|	|-LANGUAGE="german"
					|	|-Password="hello"
				:
				:

  *=KEY

*/

#define CPVT_HKEY   HKEY_LOCAL_MACHINE
#define CPVT_SUBKEY "SOFTWARE\\MNPB\\840VTS"

//option keys
#define LANG_KEY	"LANGUAGE"
#define LANGS_KEY	"LANGUAGES"
#define TEST_KEY	"TESTS"
#define REG_DEF_OPTIONS_KEY	"DEFAULT OPTIONS"

#define CSE_HKEY	CPVT_HKEY
#define CSE_SUBKEY	"SOFTWARE\\MNPB\\840VTS\\CSE ENABLED"

#define PWD_HKEY	CPVT_HKEY
#define PWD_SUBKEY	"SOFTWARE\\MNPB\\840VTS\\USERS"
#define PWD_NAMELIST "USERS"
#define PWD_NAMECOUNT "USERCOUNT"
#define PWD_FIELD	"PASSWORD"

#define COM_HKEY	CPVT_HKEY
#define COM_SUBKEY 	CPVT_SUBKEY
#define COM_840KEY	"840 Params"
#define COM_PTSKEY	"PTS Params"
#define COM_PORTKEY		"PORT"
#define COM_BAUDKEY		"BAUD"				  
#define COM_DBKEY		"DATABITS"
#define COM_SBKEY		"STOPBITS"
#define COM_PARITYKEY	"PARITY"
#define COM_AUTODETECTKEY	"AUTODETECT"
#define COM_BUFF_SIZE 30

#ifdef RICH_REPORT
#define RPT_HKEY			CPVT_HKEY
#define RPT_SUBKEY 			"SOFTWARE\\MNPB\\840VTS\\REPORTS"
#define RPT_RPTDIR_KEY		"Crystal Reports" 		// location of Crystal Report files
#define RPT_DIAG_DIR_KEY	"Diag Log"				// last diag log file dir selected by user
#define RPT_INFO_DIR_KEY	"Info Log"				// last info log file dir selected by user
#define RPT_TEST_DIR_KEY	"Test Log"				// last test log file dir selected by user
#define RPT_ALARM_DIR_KEY	"Alarm Log"				// last test log file dir selected by user
#else
#define LOG_HKEY			CPVT_HKEY
#define LOG_SUBKEY			"SOFTWARE\\MNPB\\840VTS\\LOG INFO"
#define	LOG_DIAGPATH_KEY		"DIAG PATH"
#define LOG_INFOPATH_KEY		"INFO PATH"
#define LOG_TESTPATH_KEY		"TEST PATH"
#define LOG_ALARMPATH_KEY		"ALARM PATH"
#define	LOG_DIAGFILE_KEY		"DIAG FILE"
#define LOG_INFOFILE_KEY		"INFO FILE"
#define LOG_TESTFILE_KEY		"TEST FILE"
#define LOG_ALARMFILE_KEY		"ALARM FILE"
#define LOG_MAX_PATH		MAX_PATHNAME_LEN
#endif

#define INFO_HKEY			CPVT_HKEY
#define INFO_SUBKEY			"SOFTWARE\\MNPB\\840VTS\\OWNER INFO"
#define INFO_FIELD1			"INFO1"
#define INFO_FIELD2			"INFO2"
#define INFO_FIELD3			"INFO3"
#define INFO_FIELD4			"INFO4"
#define INFO_FIELD5			"INFO5"
#define MAX_INFO_LEN		50

#define MAX_LANG_LENGTH 10

#define MAX_PWD_LEN      15 //does not include string terminator '\0'
#define MAX_USERNAME_LEN 20 //does not include string terminator '\0'
#define MAX_USERS        12

#define RESERVED_ZERO 0    // third parameter for RegCreateKeyEx  must be zero
                           // third parameter for RegOpenKeyEx    must be zero
                           // third parameter for RegSetValueEx   must be zero
#define RESERVED_NULL NULL // third parameter for RegQueryValueEx must be NULL

enum REG_Errors{
	REG_UNKNOWN_ERROR = -1,
	REG_OK            =  0,
	REG_SET_ERROR,
	REG_NOT_FOUND,
};

char* GetLangList(void);
void ProcessOptions(void);
int LoadUserOptions(char* UserName);
int SaveUserLanguage(char* UserName, char* Lang);
int GetCSEEnabled(void);

#ifdef RICH_REPORT
#include "report.h" // LOGTYPES typedef
int LoadUserReportDirectory(LOGTYPES type, char* path_name);
int SaveUserReportDirectory(LOGTYPES type, char* path_name);
#else
int SetLogInfo(char* path, char* LogKey);
int GetLogInfo(char* path, char* LogKey);
#endif // #ifdef RICH_REPORT

int RegGetInfoFields(char* Info1, char* Info2, char* Info3, char* Info4, char* Info5); 

#endif // #ifndef _REGISTRY_H_
