/*****************************************************************************/
/*   Module Name:     volume.c                                             
/*   Purpose:       This module contains the code which performs the       
/*                   volume testing for PTS 2000.                                           
/*                                                                           
/*   Requirements:   This module implements requirements detailed in        
/*                   72361-93                                                     
/*                                                                           
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Modified TransmitRequest routine 		Dave Richardson   23 Nov, 2002
/*   for PTS 2000 serial communications 
/*   so as to process not just the 
/*   CarriageReturn character at the end 
/*   of a message, but also the LineFeed 
/*   which was being left behind in the 
/*   incoming buffer.
/*                                          
/*   $Revision::   1.2      $                                                  
/*                                                                          
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/testutil/Volume.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:50   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   23 Nov 2002 11:43:40   Richardson
 * Updated per SCR # 17

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/********************/
/** Special Notes ***/
/********************/
/* Cvi Function int ComReadByte(int), return int but only use lower 8 bits */


/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <rs232.h>
#include <stdio.h>
#include <utility.h>

#include "72500.h"
#include "pts.h"
#include "volume.h"

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#define ERROR         1
#define NOERROR       0

#define MAX_FLOW_POINTS 2000

/***********************************************/
/************ STRUCTURE DEFINITIONS ************/
/***********************************************/

typedef struct volumeData
{
	double value;            // Volume calulated from dry flow
	double btps;             // Volume with BTPS Comp added
	double bar;              // Barometric reading used for the BTPS calulation
	double temp;             // Tempature in C of the gas flow
	double maxFlow;          // Max flow detected during samples
	double maxPostion;       // Time in mS from trigger to the Max Flow
} VolumeData;

typedef struct trigData
{
	int sampleRate;                                        // 1,5,10,20 mS sampling rate
	int numberOfSamples;                                   // Number of samples to take
	int timeOut;                                           // Number of seconds to wait for trigger before returning
	double endCut;                                         // used to seperate the breath
} TrigData;

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

TrigData trig;
VolumeData volume;
double trigThreshold;
int inspTimeLimit;

double FlowData[MAX_FLOW_POINTS];
int stopCount;
unsigned int numberOfPoints;
int  GS_RANGE = 4;
int O2_Set;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

int Init(void);
int MeasureHighFlowTemp(double *MeasuredVal);
int TransmitRequest(void);
int RetreivePoints(void) ;

/*****************************************************************************/
/* Name:     SetO2Gas                                                    
/*                                                                           
/* Purpose:  Sets O2 gas setting
/*                                          
/*                                                                           
/* Example Call:  void SetO2Gas(void);                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           O2_Set                                                          
/*                                                                           
/* Globals Used:  <None>                                                            
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetO2Gas(void)
{
	O2_Set = 1;

	return;
}


/*****************************************************************************/
/* Name:     SetUpVolumeOn                                                   
/*                                                                           
/* Purpose:  Sets the volume reading parameters
/*                                          
/*                                                                           
/* Example Call:   int SetUpVolumeOn(int sampleRate, int numberOfSamples,
/*                  int inspTime, int TimeOut);                                    
/*                                                                           
/* Input:   sampleRate - volume sample rate
/*			numberOfSamples - number of samples
/*			inspTime - insperation time
/*			TimeOut - timeout setting
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           trig                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   result of Init()                                                         
/*                                                                           
/*****************************************************************************/
int SetUpVolumeOn(int sampleRate, int numberOfSamples,
                  int inspTime, int TimeOut)
{
	trig.sampleRate      = sampleRate;
	trig.numberOfSamples = numberOfSamples;
	trig.timeOut         = TimeOut;
	inspTimeLimit        = inspTime;

	return Init();
}


/*****************************************************************************/
/* Name:     MeasureVentVolume                                                      
/*                                                                           
/* Purpose:  Measures the vent volume
/*                                          
/*                                                                           
/* Example Call:  int MeasureVentVolume(double *btps ,double *atp));                                    
/*                                                                           
/* Input:    btps - 
/*			 atp - 
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   error code                                                         
/*                                                                           
/*****************************************************************************/
int MeasureVentVolume(double *btps ,double *atp)
{
	int err = 0;

	err = TransmitRequest();
	if(!err){
		//if(trig.endCut > (double)inspTimeLimit){
		//	err = ERR_BAD_END_CUT;
		//}
	}

	(*btps) = volume.btps;
	(*atp) = volume.value;
	O2_Set = 0;

	return(err);
}


/*****************************************************************************/
/* Name:    Init                                                  
/*                                                                           
/* Purpose:  sets up volume structure
/*                                          
/*                                                                           
/* Example Call:  int Init();                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ERROR or NOERROR                                                       
/*                                                                           
/*****************************************************************************/
int Init()
{
	int i;

	if(MeasureHighFlowTemp(&volume.temp) == ERROR){
		return ERROR;
	}
	if(MeasureBarometricPressure(&volume.bar) == ERROR){
		return ERROR;
	}

	volume.maxFlow    = 0.0;
	volume.maxPostion = 0.0;
	volume.btps       = 0.0;
	volume.value      = 0.0;

	for(i = 0; i < MAX_FLOW_POINTS; i++){
		FlowData[i] = 0.0;
	}

	return NOERROR;
}


/*****************************************************************************/
/* Name:     TransmitRequest                                                   
/*                                                                           
/* Purpose:  Sends request to the PTS 2000
/*                                          
/*                                                                           
/* Example Call:   int TransmitRequest(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   -1 on error or result of retrievepoints()                                                       
/*                                                                           
/*****************************************************************************/
int TransmitRequest(void)
{
	int ack, crc;
	int b1, b2, b3;
	char buff[20], reply[10];
	int outCount, i;

	buff[0] = '\0';

	sprintf(buff, "SS%d\r", trig.sampleRate);     //set sample rate
	outCount = strlen(buff);

	if(SendPTS(buff, outCount) != SUCCESS){
		return -1;
	}

	b1 = RecvPTSByte(); // ack or nak
	b2 = RecvPTSByte(); // CR
	b3 = RecvPTSByte(); // LF
	if(b1 < 0 || b2 < 0 || b3 < 0){
		return -1;
	}

	if(b1 == NAK){
		return -1;
	}

	FlushInQPTS();

	if(SendPTS("STF+\r", 5) != SUCCESS){
		return -1;
	}

	b1 = RecvPTSByte(); // ack or nak
	b2 = RecvPTSByte(); //CR
	b3 = RecvPTSByte(); //LF
	if(b1 < 0 || b2 < 0 || b3 < 0){
		return -1;
	}

	if(b1 == NAK){
		return -1;
	}

	FlushInQPTS();

	if(O2_Set){
		sprintf(buff, "FB1%d\r", trig.numberOfSamples);
	}
	else{
		sprintf(buff, "FB0%d\r", trig.numberOfSamples); // set high flow burst points
	}

	outCount = strlen(buff);

	if(SendPTS(buff, outCount) != SUCCESS){
		return -1;
	}

	FlushInQPTS();

	return( RetreivePoints());
}


/*****************************************************************************/
/* Name:     RetreivePoints                                               
/*                                                                           
/* Purpose:  reads data from the PTS 2000
/*                                          
/*                                                                           
/* Example Call:   int RetreivePoints(void);                                    
/*                                                                           
/* Input:    <None>     
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   -1=Error, else 0                                                         
/*                                                                           
/*****************************************************************************/
int RetreivePoints(void)
{
	int x;
	int thresholdFlag;
	int count;
	int stopFlag;
	int kflag;
	int timeFlag = 0;
	int index;
	unsigned int reading;
	unsigned char b1, b2, b3;
	double timeout, next, d, sum1, sum2;
	int numBytesExpected;

	d = ( (double)(trig.sampleRate * trig.numberOfSamples) / 1000.0) * .20 ;
	Delay(  ((double)(trig.sampleRate * trig.numberOfSamples) / 1000.0) - d );

	timeout = Timer() + trig.timeOut;

	numBytesExpected = (trig.numberOfSamples * 2) + 4; // +2 for number of data points and +2 for CRC

	do{
		if(Timer() > timeout){
			timeFlag = 1;
			break;
		}
	} while((CharsInPTSQ() != numBytesExpected) && (timeFlag != 1));

	if(timeFlag){ // if time out
		ComBreakPTS(255);
		FlushInQPTS();
		FlushOutQPTS();
		Delay(1.0);
		return( -1 );
	}

	b1 = RecvPTSByte();
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return -1;
	}

	numberOfPoints = (unsigned int)b1;
	numberOfPoints = ((numberOfPoints<<8)+((unsigned int) b2&0x00ff));

	if(numberOfPoints != trig.numberOfSamples){
		return -1;
	}

	for(x = 0; x < numberOfPoints; x++){ // change back to numberOfPoints
		// F L O W   R E A D I N G
		// This for loop must be tight !!!!!!!
		b1 = RecvPTSByte();
		b2 = RecvPTSByte();
		if(b1 < 0 || b2 < 0){
			return -1;
		}

		reading = (unsigned int)b1;
		reading = ((reading << 8) + ((unsigned int)b2 & 0x00ff));

		FlowData[x] = (double)reading;
	}

	b1 = RecvPTSByte(); // CRC
	b2 = RecvPTSByte();
	if(b1 < 0 || b2 < 0){
		return -1;
	}

	kflag = 0;
	stopFlag = 0;
	stopCount = 0;
	thresholdFlag = 0;

	for(count = 0; count < numberOfPoints; count++){ // so you can see all converted data
		FlowData[count] = FlowData[count] / 100.0;
	}

	trig.endCut = inspTimeLimit + 10;
	// determine volume up to mark

	for(count = 0; count <= trig.endCut ; count++){
		volume.value = FlowData[count] + volume.value;
	}

	volume.value *= ((double)trig.sampleRate / 1000.0);
	volume.value /= 60;

	volume.value = (volume.value * 1000.0);
	volume.btps = volume.value * ((310.15 * 1033.66) / (294.25 * ((volume.bar * 70.3) - 64.037)) );

	return SUCCESS;
}


/*****************************************************************************/
/* Name:    MeasureHighFlowTemp                                                 
/*                                                                           
/* Purpose:  Measures the high flow temperature from the PTS
/*                                          
/*                                                                           
/* Example Call:  int MeasureHighFlowTemp(double *MeasuredVal);                                    
/*                                                                           
/* Input:   MeasuredVal - value from the PTS          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ERROr or NOERROR                                                         
/*                                                                           
/*****************************************************************************/
int MeasureHighFlowTemp(double *MeasuredVal)
{
	unsigned int reading;
	int b1, b2, b3;

	FlushInQPTS();

	if(SendPTS("T\r", 2) != SUCCESS){
		return -1;
	}

	b1 = RecvPTSByte();
	b2 = RecvPTSByte();
	b3 = RecvPTSByte();
	if(b1 < 0 || b2 < 0 || b3 < 0){
		return -1;
	}

	reading = (unsigned int)b1;
	reading = ((reading << 8) + ((unsigned int)b2 & 0x00ff));
	*MeasuredVal = (double)reading/100.0;

	if((b1+b2&0xff)== b3)
		return NOERROR;
	else
		return ERROR;
}

