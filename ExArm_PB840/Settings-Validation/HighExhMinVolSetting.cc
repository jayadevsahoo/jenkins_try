#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  HighExhMinVolSetting - High Exhaled Minute Volume Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the upper limit of
//  exhaled minute volume (in liters), above which the "high exhaled minute
//  volume alarm" shall be activated.  This class inherits from
//  'BoundedSetting' and provides the specific information needed for
//  representation of high exhaled minute volume.  This information includes
//  the interval and range of this setting's values, and this batch
//  setting's new-patient value (see 'getNewPatientValue()').
//
//  This setting is an alarm limit setting, therefore this class overrides
//  the 'getMaxLimit()' and 'getMinLimit()' methods to provide access (for
//  the GUI) to this alarm's maximum and minimum limits.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, but 'updateConstraints_()' is
//  overridden to update this setting's dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HighExhMinVolSetting.ccv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: gdc    Date: 29-Apr-2009    SCR Number: 6478
//  Project:  840S
//  Description:
//      Corrected getNewPatientValue to return unclipped value.
//
//  Revision: 009   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 008   By: rhj    Date: 17-Jan-2006    DR Number: 6173
//  Project:  PAV4
//  Description:
//     Modified updateConstraints_() in order to prevent 
//     High Ve, tot equal to Low Ve, tot, when Low 
//     Ve, tot is set to 0.100.       
//
//  Revision: 007   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  eliminated 'get{Max,Min}Limit()' method, now defined by base class
//      *  incorporated circuit-specific new-patient values and ranges
//
//  Revision: 006   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5018
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.19.1.0) into Rev "BiLevel" (1.19.2.0)
//	Modified new-patient value calculation to help "loosen" the
//	spread between the high and low counterparts.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.19.1.0) into Rev "BiLevel" (1.19.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "HighExhMinVolSetting.hh"
#include "ModeValue.hh"
#include "SettingConstants.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

#define DEFAULT_MAX_VALUE  Real32(100.0f)


//  $[02138] -- The setting's range ...
//  $[02141] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	0.10f,		// absolute minimum...
	0.0f,		// unused...
	THOUSANDTHS,	// unused...
	NULL
};
static const BoundedInterval  NODE3_ =
{
	0.500f,		// change-point...
	0.005f,		// resolution...
	THOUSANDTHS,	// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE2_ =
{
	5.00f,		// change-point...
	0.05f,		// resolution...
	HUNDREDTHS,		// precision...
	&::NODE3_
};
static BoundedInterval  Node1_ =
{
	DEFAULT_MAX_VALUE,		// change-point...
	0.5f,		// resolution...
	TENTHS,		// precision...
	&::NODE2_
};
static BoundedInterval  IntervalList_ =
{
	DEFINED_UPPER_ALARM_LIMIT_OFF,// absolute maximum...
	(DEFINED_UPPER_ALARM_LIMIT_OFF - DEFAULT_MAX_VALUE),// resolution...
	TENTHS,		// precision...
	&::Node1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: HighExhMinVolSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, the value interval
//  range is initialized.
//
//  The resolution for the "max" range below, is calculated by taking
//  the difference between the absolute maximum value and the next lower
//  change-point.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighExhMinVolSetting::HighExhMinVolSetting(void)
: BatchBoundedSetting(SettingId::HIGH_EXH_MINUTE_VOL,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::IntervalList_,
					  HIGH_EXH_MIN_VOL_MAX_ID,	 // maxConstraintId...
					  HIGH_EXH_MIN_VOL_MIN_ID)	 // minConstraintId...
{
	CALL_TRACE("HighExhMinVolSetting()");
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~HighExhMinVolSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighExhMinVolSetting::~HighExhMinVolSetting(void)
{
	CALL_TRACE("~HighExhMinVolSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is normally changeable except in CPAP.
//  $[LC02003]\b\ in CPAP, high Ve,tot to shall not be viewable
//  $[LC02001]\b\ transition from CPAP, high Ve,tot shall be viewable
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id HighExhMinVolSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");
	Applicability::Id applicabilityId;

	const Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const DiscreteValue MODE = (qualifierId == Notification::ADJUSTED) ? 
		pSetting->getAdjustedValue() : pSetting->getAcceptedValue();

	if ( ModeValue::CPAP_MODE_VALUE == MODE )
	{
		applicabilityId = Applicability::INAPPLICABLE;
	}
	else
	{
		applicabilityId = Applicability::CHANGEABLE;
	}

	return applicabilityId;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue HighExhMinVolSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	Real32  scalingFactor;

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			scalingFactor = SettingConstants::NP_HIGH_EXH_MINUTE_VOL_NEO_SCALE;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			scalingFactor = SettingConstants::NP_HIGH_EXH_MINUTE_VOL_PED_SCALE;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			scalingFactor = SettingConstants::NP_HIGH_EXH_MINUTE_VOL_ADULT_SCALE;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);
	const Real32 IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;
	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const DiscreteValue  MODE_VALUE = pMode->getAdjustedValue();

	BoundedValue boundedValue;

	// $[02139] The settings new-patient value ...
	boundedValue.value = (scalingFactor * IBW_VALUE) + 0.05f;

	if ( MODE_VALUE == ModeValue::CPAP_MODE_VALUE )
	{
		boundedValue.value = DEFINED_UPPER_ALARM_LIMIT_OFF;
	}

	// warp the calculated value to a "click" boundary...
	// but not clipped by current constraints (SCR #6478)
	getBoundedRange().warpValue(boundedValue, BoundedRange::WARP_NEAREST, FALSE);	 

	return(boundedValue);
}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  The setting is only interested in the transition of mode type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Transition rules.
//  $[LC02001] transition from CPAP...
//  $[LC02003] transition to CPAP...
//---------------------------------------------------------------------
//@ PreCondition
//  settingId == SettingId::MODE
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void HighExhMinVolSetting::acceptTransition(const SettingId::SettingIdType settingId,
											const DiscreteValue  newValue,
											const DiscreteValue  currValue)
{
	AUX_CLASS_PRE_CONDITION(settingId == SettingId::MODE, settingId);

	setAdjustedValue(getNewPatientValue());
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	HighExhMinVolSetting::SoftFault(const SoftFaultID  softFaultID,
									const Uint32       lineNumber,
									const char*        pFileName,
									const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							HIGH_EXH_MINUTE_VOL_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the maximum lower bound, and set the lower limit to it.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	HighExhMinVolSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	//-------------------------------------------------------------------
	// update upper limit value...
	//-------------------------------------------------------------------
	{
		const Setting*  pPatientCctType =
			SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

		const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
			pPatientCctType->getAdjustedValue();

		// update the upper-most change point, based on the circuit type...
		switch ( PATIENT_CCT_TYPE_VALUE )
		{
			case PatientCctTypeValue::NEONATAL_CIRCUIT :	// $[TI1]
				::Node1_.value = 10.0f;	// L/min...
				break;
			case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	// $[TI2]
				::Node1_.value = 30.0f;	// L/min...
				break;
			case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI3]
				::Node1_.value = 100.0f; // L/min...
				break;
			default :
				// unexpected patient circuit type value...
				AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
				break;
		}

		// update the resolution between the upper-most change point, and the
		// absolute maximum, "OFF" value...
		::IntervalList_.resolution = (::IntervalList_.value - ::Node1_.value);
	}

	//-------------------------------------------------------------------
	// determine lower limit value...
	//-------------------------------------------------------------------
	{
		BoundedRange::ConstraintInfo  minConstraintInfo;
		BoundStatus                   dummyStatus(getId());

		const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

		const BoundedSetting*  pLowExhMinuteVol =
			SettingsMgr::GetBoundedSettingPtr(SettingId::LOW_EXH_MINUTE_VOL);

		// get the current value of low minute volume alarm setting; this setting
		// must always be higher that the current low minute volume setting...
		BoundedValue  lowMinVolBasedMin = pLowExhMinuteVol->getAdjustedValue();

		// NOTE:  a '>=' comparison is used instead of a '>' comparison because
		//        of the increment done to the value within the if clause, which
		//        equates an '>=' into an '>'...

		// In order to fix SCR 6173, the ABSOLUTE_MIN value of 0.100 must be 
		// multiplied by 1000 as with the low exhaled minute volume and compare them
		// by a -1. This allows the minimum value of the high exhaled minute volume 
		// setting alarm to be 0.05 higher than the value of the low exhaled 
		// minute volume setting alarm. 
		if ( (lowMinVolBasedMin.value * 1000) >= ( (ABSOLUTE_MIN * 1000) - 1) )
		{	// $[TI4]
			// reset this range's minimum constraint to allow for "warping" of a
			// new minimum value...
			getBoundedRange_().resetMinConstraint();

			// warp the calculated value to a "click" boundary...
			getBoundedRange_().warpValue(lowMinVolBasedMin);

			// calculate the value one "click" above this current low minute volume
			// alarm value, by using high exh minute volume's bounded range
			// instance...
			getBoundedRange_().increment(1, lowMinVolBasedMin, dummyStatus);

			// the low exhaled minute volume-based minimum is more restrictive,
			// therefore save its value and id as the minimum bound value and id...
			minConstraintInfo.id    = HIGH_EXH_MIN_VOL_MIN_BASED_LOW_ID;
			minConstraintInfo.value = lowMinVolBasedMin.value;
		}
		else
		{	// $[TI5]
			// the absolute minimum is more restrictive, therefore save its value
			// and id as the minimum bound value and id...
			minConstraintInfo.id    = HIGH_EXH_MIN_VOL_MIN_ID;
			minConstraintInfo.value = ABSOLUTE_MIN;
		}

		// this setting has no minimum soft bounds...
		minConstraintInfo.isSoft = FALSE;

		getBoundedRange_().updateMinConstraint(minConstraintInfo);
	}
}
