#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Sound - Interface to the Sound system, allowing the activation
// of the various sounds that the ventilator needs to make.
//---------------------------------------------------------------------
//@ Interface-Description
// This class interfaces to the Application-level sound system.  There are 9 different sounds that can be made
// by the ventilator, identified by the following constants: CONTACT, RELEASE, ACCEPT,
// LOW_URG_ALARM, INVALID_ENTRY, KNOB, ALARM_VOLUME, MEDIUM_URG_ALARM and
// HIGH_URG_ALARM.  These are defined in the SoundId enum in Sound.hh.  The
// first four are once-off sounds; when activated they sound for a single short
// period.  The latter three are continuous sounds, they stay sounding when
// activated until explicitly canceled. By continuous, we do not always mean a
// constant tone; these may consist of a series of sounds that are repeated.
//
// Sounds can be activated by passing one of the constants to the method
// Start().  Deactivation of the ALARM_VOLUME sound is accomplished via the
// CancelAlarmVolume() method, while both the MEDIUM_URG_ALARM and
// HIGH_URG_ALARM are canceled by CancelAlarm().
//
// The ALARM_VOLUME sound is used to set the volume of the alarm sounds.
// Explanation: while the user is holding down the alarm volume key, this
// continuous tone is produced, so that the user can hear the effect of volume
// changes (accomplished by turning the knob).  Changes to the Alarm Volume
// setting are communicated to the windows via a call to SetAlarmVolume(),
// passing the new volume setting as a parameter.  The volume setting ranges
// from MIN_ALARM_VOL to MAX_ALARM_VOL.
//---------------------------------------------------------------------
//@ Rationale
// This class is necessary to insulate the application from the low-level
// sound generation routines.
//---------------------------------------------------------------------
//@ Implementation-Description
// 
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Sound.ccv   25.0.4.0   19 Nov 2013 14:11:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 007   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 006  By: yyy      Date: 01-Dec-1998  DCR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding sound.
//
//  Revision: 005  By: mpm      Date: 02-Apr-1996  DCR Number: 1961
//    Project:  Sigma (R8027)
//    Description:
//      Changed to call StartAlarmVolumeSound().
//
//  Revision: 004  By: ceb      Date: 07-Mar-1996  DCR Number: 1953
//    Project:  Sigma (R8027)
//    Description:
//      Added RELEASE and KNOB generic cases for sounds.  RELEASE will generate a GUI_IO_RELEASE sound,
//      KNOB will generate a GUI_IO_ROTARY sound.
//
//  Revision: 003  By: ceb      Date: 29-Feb-1996  DCR Number: 1960
//    Project:  Sigma (R8027)
//    Description:
//      Modifications made to match GUI-IO-Devices SaasSound.cc.  ALARM_LOW/SET/MEDIUM_URGENCY replaced
//      by GUI_IO_LOW_URG_ALARM, GUI_IO_VOLUME_ALARM, GUI_IO_MED_URG_ALARM.
//
//  Revision: 002  By: ceb      Date: 16-Nov-1995  DCR Number: 616
//    Project:  Sigma (R8027)
//    Description:
//      Changed refs to GUI-IO-Devices routines to reflect name changes there.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Sound.hh"

#ifdef SIGMA_GUIPC_CPU
#include <mmsystem.h> //To see PlaySound
#endif



//@ Code...

// knob sound enable (TRUE) or not (FALSE)
Boolean Sound::KnobSoundFlag_;

// Key sound enables (TRUE) or not (FALSE)
Boolean Sound::KeySoundFlag_[NUMBER_OF_GUI_KEYS];

// Current alarm sound id
Sound::SoundId Sound::AlarmSound_;

// Flag indicating the playing of Volume alarm sound
Boolean Sound::VolumeAlarmFlag_;

// Flag indicating saved user sound volume
Uint32 Sound::SavedUserSoundVolume_;

// Flag indicating saved alarm sound volume
Uint32 Sound::SavedAlarmSoundVolume_;

// Audible Failure Count
Uint32 Sound::AudibleFailureCount_;

// If we get multiple failures, trigger a assert
static const int MAX_AUDIBLE_FAILURES = 5;

//  struct for an entry in the Sound to Resource
struct SoundToResourceEntry
{
	Sound::SoundId soundID;  //Flags for playing the sound
	Uint32	resourceID;
    Uint32	properties;
    Boolean		alarmsound;
};


static const Int MAX_SOUND_ID = 11;

//  Array containing pointers to SoundToResourceTable indexed by 
//  the SoundId's identifier.
const SoundToResourceEntry *PSoundToResourceTableBySoundID[MAX_SOUND_ID+1];

const SoundToResourceEntry SoundToResourceTable[] =
{
// Sound ID                                  Resource ID       Properties								isAlarmSound
// ---------------------        --------------   ----  -------------
   {Sound::SoundId::CONTACT ,				 IDR_PRESS_WAVE,   SND_RESOURCE | SND_ASYNC,				FALSE}
  ,{Sound::SoundId::ACCEPT ,			     IDR_ACCEPT_WAVE,  SND_RESOURCE | SND_ASYNC,				FALSE}
  ,{Sound::SoundId::INVALID_ENTRY ,		     IDR_REJECT_WAVE,  SND_RESOURCE | SND_ASYNC,				FALSE}

  ,{Sound::SoundId::ALARM_VOLUME ,		     IDR_VOLUME_ALARM, SND_RESOURCE | SND_LOOP | SND_ASYNC,		FALSE}
  ,{Sound::SoundId::LOW_URG_ALARM ,		     IDR_LOW_ALARM,    SND_RESOURCE | SND_ASYNC,				TRUE}
  ,{Sound::SoundId::MEDIUM_URG_ALARM ,	     IDR_MEDIUM_ALARM, SND_RESOURCE | SND_LOOP | SND_ASYNC,		TRUE}
  ,{Sound::SoundId::HIGH_URG_ALARM ,	     IDR_HIGH_ALARM,   SND_RESOURCE | SND_LOOP | SND_ASYNC,		TRUE}

  ,{Sound::SoundId::RELEASE ,			     IDR_RELEASE_WAVE, SND_RESOURCE | SND_ASYNC,  				FALSE}
  ,{Sound::SoundId::KNOB ,				     IDR_ROTARY_WAVE,  SND_RESOURCE | SND_ASYNC,  				FALSE}
  ,{Sound::SoundId::REMIND ,			     IDR_REMIND_WAVE,  SND_RESOURCE | SND_ASYNC,  				FALSE}

  ,{Sound::SoundId::NO_SOUND,			     NULL,			   NULL,									FALSE}
};
 
const Uint NumEntries_ 
           = sizeof(SoundToResourceTable) / sizeof(SoundToResourceEntry);

//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize		[static]
//
//@ Interface-Description
// Initialization of Sound Class. Takes no arguments and returns void.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Disable all key and knob sounds.
//
//  Called by 'GuiIoDevicesInitialize()' to initialize Sound data.
//  This routine sets all sound flags to 'False'.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void
Sound::Initialize( void)
{
   CALL_TRACE("Sound::Initialize(void)");

   Int32	key;

   // initialize alarm id to NO SOUND
   AlarmSound_ = NO_SOUND;

   // Indicate Volume Alarm NOT playing
   VolumeAlarmFlag_ = FALSE;

   // Disable knob sounds
   KnobSoundFlag_ = FALSE;

   //Clear saved volume 
   SavedAlarmSoundVolume_ = MIN_ALARM_VOL;
   SavedUserSoundVolume_ = MIN_USER_VOL;

   //SetUserVolumeLevel
   SetUserVolume(DEFAULT_USER_VOL);

   for (key = 0; key < NUMBER_OF_GUI_KEYS; key++)
   {			//$[TI1] -- this branch ALWAYS executes
      // initial configuration is to make no sound for
      // all keys.  Can be configured for individual keys using
      //	'EnableKeySound( GuiKeys)'

      KeySoundFlag_[key] = FALSE;
   }			

    for (int i=0; i < NumEntries_; i++)
    {
		//Initializes PSoundToResourceTableBySoundID entries by indexing into the 
		//  table by soundID.
        const SoundToResourceEntry & resourceEntry = SoundToResourceTable[i];
        PSoundToResourceTableBySoundID[resourceEntry.soundID] = &resourceEntry;
    }

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEntryBySoundId(const Sound::SoundId id)
//
//@ Interface-Description
//  Returns the SoundToResourceTable that matches the id argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
const SoundToResourceEntry & GetEntryBySoundId(const Sound::SoundId id)
{
    CALL_TRACE("GetEntryBySoundId(Sound::SoundId)");
 
    return *PSoundToResourceTableBySoundID[id];
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Start   [static]
//
//@ Interface-Description
// Starts a particular sound.  Passed the id of the sound to be "started".
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls windows API to generate sounds from the wav files loaded from the resources 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Sound::Start(SoundId soundId)
{
   CALL_TRACE("Sound::Start(Id soundId)");

   CLASS_PRE_CONDITION( soundId >= Sound::CONTACT && soundId <= Sound::NO_SOUND);
   CLASS_ASSERTION( PSoundToResourceTableBySoundID[soundId] != NULL);

   const SoundToResourceEntry & resourceEntry = GetEntryBySoundId(soundId);

   Boolean PlayResult = TRUE;
   Boolean isAlarmSound = resourceEntry.alarmsound;
   Uint32  resourceId	= resourceEntry.resourceID;					//WAV files mapped from ExArm.rc2
   Uint32  soundProperties = resourceEntry.properties;

   if(resourceEntry.soundID == Sound::ALARM_VOLUME)
   {
	   // Indicate Volume Alarm playing
       VolumeAlarmFlag_ = TRUE; 
	   PlayResult = PlaySound(MAKEINTRESOURCE(resourceId), ::AfxGetInstanceHandle(), soundProperties);
   }
   else if(isAlarmSound)
   {
       AlarmSound_ = resourceEntry.soundID; // save current alarm id

	   // Check if a Volume Alarm sound is NOT currently playing.
	   // If not playing, start the alarm sound, otherwise the Volume
	   // Alarm sound has priority.

       if(VolumeAlarmFlag_)
	   {
		   // If VolumeAlarmFlag_ is true,
		   // This means that alarms are not sounded while adjusting volume.
		   // The alarm is re-asserted when the alarm volume sound is canceled.
			PlayResult = TRUE;
	   }
       else
	   {
			PlayResult = PlaySound(MAKEINTRESOURCE(resourceId), ::AfxGetInstanceHandle(), soundProperties);
	   }
   } 
   else if(soundId == Sound::NO_SOUND )
   {
	   CancelAlarmVolume();
	   CancelAlarm();
   }
   else //User Sound
   {
        PlayResult = PlaySound(MAKEINTRESOURCE(resourceId), ::AfxGetInstanceHandle(), soundProperties);

		//TODO E600we are just restarting the continous sounds which were playing while the user sound was played 
		//as the PlaySound API does not support Mixing the sounds and has to be stopped to play other sounds.
		if(AlarmSound_ != Sound::NO_SOUND && AlarmSound_ != Sound::LOW_URG_ALARM)
		{
			 const SoundToResourceEntry & resourceEntry = GetEntryBySoundId(AlarmSound_);

			 resourceId	= resourceEntry.resourceID;
			 soundProperties = resourceEntry.properties;

			// If an alarm should be playing, restart the alarm.
			PlayResult = PlaySound(MAKEINTRESOURCE(resourceId), ::AfxGetInstanceHandle(), soundProperties);
		}

		if(VolumeAlarmFlag_)
		{
			 const SoundToResourceEntry & resourceEntry = GetEntryBySoundId(Sound::ALARM_VOLUME);

			 resourceId	= resourceEntry.resourceID;
			 soundProperties = resourceEntry.properties;

			// Keep playing Alarm Volume 
			PlayResult = PlaySound(MAKEINTRESOURCE(resourceId), ::AfxGetInstanceHandle(), soundProperties);
		}
   }

	if (PlayResult)
	{
		AudibleFailureCount_ = 0;
	}
	else
	{	// Assert If we get multiple failures
		CLASS_ASSERTION (++AudibleFailureCount_ > MAX_AUDIBLE_FAILURES);
	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CancelAlarm   [static]
//
//@ Interface-Description
// Cancels the pulsing Medium or High Urgency Alarm sound, if it is
// currently sounding.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Sound::CancelAlarm(void)
{
   CALL_TRACE("Sound::CancelAlarm(void)");

   AlarmSound_ = NO_SOUND;			// set current alarm id to no sound

   // Check if a Volume Alarm sound is NOT currently playing.
   // If not playing, cancel any alarm sound, otherwise the Volume
   // Alarm sound has priority.

   if (!VolumeAlarmFlag_)
   {		//$[TI1]  
	 const SoundToResourceEntry & resourceEntry = GetEntryBySoundId(AlarmSound_);

	 Uint32  resourceId	= resourceEntry.resourceID;
	 Uint32  soundProperties = resourceEntry.properties;

    // Cancel any alarm sound currently playing
	PlaySound(MAKEINTRESOURCE(resourceId), ::AfxGetInstanceHandle(), soundProperties);
   }		//$[TI2]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CancelAlarmVolume   [static]
//
//@ Interface-Description
// Cancels the continuous Alarm Volume sound, if it is currently sounding.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Sound::CancelAlarmVolume(void)
{
   CALL_TRACE("Sound::CancelAlarmVolume(void)");

   Int32 rval;

   // Indicate Volume Alarm sound is no longer playing

   VolumeAlarmFlag_ = FALSE;

	 const SoundToResourceEntry & resourceEntry = GetEntryBySoundId(AlarmSound_);

	 Uint32  resourceId	= resourceEntry.resourceID;
	 Uint32  soundProperties = resourceEntry.properties;

	// If an alarm should be playing, restart the alarm.
	PlaySound(MAKEINTRESOURCE(resourceId), ::AfxGetInstanceHandle(), soundProperties);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetAlarmVolume   [static]
//
//@ Interface-Description
// Sets the alarm volume to the passed "alarmVolume" value. This value must be
// within the range of MIN_ALARM_VOL to MAX_ALARM_VOL.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// Note that when set to the quietest level (the min value), the volume is
// still audible (not off).
//---------------------------------------------------------------------
//@ PreCondition
//	alarmVolume >= MIN_ALARM_VOL &&	alarmVolume <= MAX_ALARM_VOL
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Sound::SetAlarmVolume(Int32 alarmVolume)
{
    CALL_TRACE("Sound::SetAlarmVolume(Int32 alarmVolume)");
    CLASS_PRE_CONDITION(alarmVolume >= MIN_ALARM_VOL &&
                                                alarmVolume <= MAX_ALARM_VOL);


    DWORD VolumeOut = alarmVolume;
	Uint32 i;

	//New Alarm volume setting. The low-order word contains the left-channel volume setting, 
	//and the high-order word contains the right-channel setting. A value of 0xFFFFFFFF 
	//represents full volume, and a value of 0x00000000 is silence. If a device does not 
	//support both left and right volume control, the low-order word of VolumeOut 
	//specifies the volume level, and the high-order word is ignored.

	Uint32 realMaXVolume =  65535; //0xFFFF 

	VolumeOut = (alarmVolume*realMaXVolume)/MAX_ALARM_VOL;

	//Save alarmVolume
	SavedAlarmSoundVolume_ = VolumeOut;

	//Appy the saved UserSound to higher order word else UserSound will go mute
	VolumeOut = ((SavedUserSoundVolume_ << 16) | VolumeOut);

    waveOutSetVolume(0, VolumeOut);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetUserVolume   [static]
//
//@ Interface-Description
// Sets the user volume to the passed "userSoundVolume" value. This value must be
// within the range of MIN_USER_VOL to MAX_USER_VOL.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// Note that when set to the quietest level (the min value), the volume is
// still audible (not off).
//---------------------------------------------------------------------
//@ PreCondition
//	userSoundVolume >= MIN_USER_VOL &&	userSoundVolume <= MAX_USER_VOL
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Sound::SetUserVolume(Int32 userSoundVolume)
{
    CALL_TRACE("Sound::SetUserVolume(Int32 userSoundVolume)");
    CLASS_PRE_CONDITION(userSoundVolume >= MIN_USER_VOL &&
                                                userSoundVolume <= MAX_USER_VOL);

    DWORD VolumeOut = userSoundVolume;
	Uint32 i;

	//New user volume setting. The low-order word contains the left-channel volume setting, 
	//and the high-order word contains the right-channel setting. A value of 0xFFFFFFFF 
	//represents full volume, and a value of 0x00000000 is silence. If a device does not 
	//support both left and right volume control, the low-order word of VolumeOut 
	//specifies the volume level, and the high-order word is ignored.

	Uint32 realMaXVolume =  65535; // 0xFFFF

	VolumeOut = (userSoundVolume*realMaXVolume)/MAX_ALARM_VOL;

	//Save userVolume
	SavedUserSoundVolume_ = VolumeOut;

	//Shift the bit by 16 for Right Channel 
	VolumeOut <<= 16;

	//Appy the saved UserSound to lower order word else AlarmSound will go mute
	VolumeOut = VolumeOut | SavedAlarmSoundVolume_;

    waveOutSetVolume(0, VolumeOut);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: KeyPressSound		[static]
//
//@ Interface-Description
// Generate a key press sound. Takes a GuiKeys argument and returns an
// integer.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	none
//
//---------------------------------------------------------------------
//@ PreCondition
//  CLASS_PRE_CONDITION( (key >= 0) && (key < NUMBER_OF_GUI_KEYS));
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void Sound::KeyPressSound( GuiKeys key)
{
   CLASS_PRE_CONDITION( (key >= 0) && (key < NUMBER_OF_GUI_KEYS));

   if (KeySoundFlag_[key])
   {		//$[TI1]
	   Start( SoundId::CONTACT);
   }		//$[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: KeyReleaseSound		[static]
//
//@ Interface-Description
//  Generate a key release sound. Takes a GuiKeys argument and returns 
//  an integer.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// This method is called when any key is released.  No sound is made on
// generic key releases.  In some cases GUI-Apps may produce a sound. This
// routine is provided for possible future sound generation on key release.
//---------------------------------------------------------------------
//@ PreCondition
//  CLASS_PRE_CONDITION( (key >= 0) && (key < NUMBER_OF_GUI_KEYS));
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void Sound::KeyReleaseSound( GuiKeys key)
{
   CLASS_PRE_CONDITION( (key >= 0) && (key < NUMBER_OF_GUI_KEYS));

   // No automatic sound is made on key release.  This routine is provided for
   // possible future sound generation on key release.

   			//$[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: KnobRotateSound		[static]
//
//@ Interface-Description
// Generate an knob click sound. Takes no arguments and returns an integer.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void Sound::KnobRotateSound( void)

{
   if (KnobSoundFlag_)
   {		//$[TI1]
	  Start( SoundId::KNOB);
   }		//$[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
Sound::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("Sound::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, SOUND,
							lineNumber, pFileName, pPredicate);
}
