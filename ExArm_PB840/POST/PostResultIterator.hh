#ifndef PostResultIterator_HH
#define PostResultIterator_HH


//=====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation of
// California.
//
//        Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
// Class: PostResultIterator
//
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/PostResultIterator.hhv   25.0.4.0   19 Nov 2013 14:17:06   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 002    By: Gary Cederquist  Date: 15-MAY-1997  DR Number: 2085
//    Project: Sigma (R8027)
//    Description:
//       Added accessors to POST auxiliary result data.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Post.hh"
#include "postnv.hh"


class PostResultIterator
{
  public:
    PostResultIterator(void);
    ~PostResultIterator(void);
    Uint8             getTestNumber(void) const;
    Criticality       getTestResult(void) const;
    const AuxResult * getPAuxResult(void) const;
    Boolean           next(void);

  private:
    PostResultIterator(const PostResultIterator&);
 
    //@ Data-Member:  testNumber_
    //  Current testNumber from results in Kernel NOVRAM
    Uint8           testNumber_;
 
    //@ Data-Member:  testResult_
    //  Current testResult_ from results in Kernel NOVRAM
    Criticality     testResult_;
 
    //@ Data-Member:  pAuxResult_
    //  Pointer to latest AuxResult for this test number in
    //  the  auxiliary results in Kernel NOVRAM
    const AuxResult *     pAuxResult_;
 
};

#endif	// PostResultIterator_HH
