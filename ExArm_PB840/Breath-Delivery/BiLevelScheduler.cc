#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BiLevelScheduler - the active BreathPhaseScheduler when the 
//  current mode is BiLevel.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for scheduling breath phases during BiLevel mode.
// The class is also responsible for relinquishing control to the next valid
// scheduler and for assuming control when BiLevel mode is requested.  BiLevel mode
// can be requested by the operator or upon emergency mode recovery and
// power-up sequence, when transition to the previous mode is required. The
// BiLevel scheduler has a cycle that is determined by the respiratory rate
// setting and peep high time.  This scheduler is attempting to deliver
// a mandatory breath every BiLevel cycle. To implement this cyclical
// behavior, the BiLevelScheduler is defined as a client of an IntervalTimer.
// Note that mode transition, as a response for setting change, occurs once
// the new mode setting is accepted (by the operator).  This scheduler is
// responsible for enabling valid mode triggers for BiLevel mode and for
// enabling breath triggers that are not under the breath phase direct
// control. The BiLevelScheduler is responsible for handling user events
// like 100% O2, manual inspiration request, etc. and for handling setting
// change events like rate and mode changes.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates all the breathing rules specified for BiLevel mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The class implements methods for breath and mode transitions and for
// breath data updates. The BiLevel scheduler is responsible for:
// - Determining the next breath when a breath trigger becomes active.
// - Initiating the activities required for the proper start of a new breath:
//    phase in settings, update breath record, set breath triggers, 
//    enable breath triggers, and phase out the previous breath phase. 
// - Relinquishing control when a mode trigger becomes active.
//    Activities while control is relinquished include: 
//    disable out of date breath triggers, determine which is the next
//    valid scheduler, enable breath triggers for the next scheduler,
//    and instruct the next scheduler to take control.
// - Taking control when the previous scheduler relinquishing control. 
//    Activities while taking control include:
//    register self with the BreathPhaseScheduler object,  and activate
//    the mode trigger list.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BiLevelScheduler.ccv   25.0.4.0   19 Nov 2013 13:59:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 018   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 017   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//           
//  Revision: 016   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added case to reject RM maneuvers during BiLevel.
//
//  Revision: 015   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added pav pause handle during mode transition.
//
//  Revision: 014  By: syw     Date:  09-Oct-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added VTPC handle.
//
//  Revision: 013  By: syw     Date:  10-Feb-2000    DR Number: 5634
//  Project:  NeoMode
//  Description:
//		Changed initialization of biLevelPeepCycleTimeMs_ and
//		rCycleTimer_.setCurrentTime from 10000 to 60000 to handle
//		worst case resp rates (1 bpm).
//
//  Revision: 012  By: syw     Date:  18-Jul-1999    DR Number: 5471, 5481
//  Project:  ATC
//  Description:
//		During a NULL_INSPIRATION, use the current breathType instead of
//		BreathRecord::GetBiLevelFirstBreathType().
//
//  Revision: 011  By: yyy     Date:  13-Jul-1999    DR Number: 5466
//  Project:  ATC
//  Description:
//      Set readyForSettingChange_ to TRUE when the second setting Id
//		requests setting change occurs so we are ready to process the
//		next batch of setting changes.
//
//  Revision: 010  By: sah     Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//      Added in new 'PEEP_HIGH_TIME' setting.
//
//  Revision: 009  By:  healey    Date:  14-Jan-99    DR Number: DCS 5322, 5367
//       Project:  ATC
//       Description:
//          Added checking for TC support type to set up corresponding phase
//          Added checking for PAUSE_PRESS trigger
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//      Initial ATC release
//
//  Revision: 007  By:  yyy    Date:  10-Nov-98    DR Number: 5263, 5264
//       Project:  Sigma (R8027)
//       Description:
//          Added checking of inspiratory/expiratory phase when calculating
//			the peepHighToLowTransitionTime_.  During inspiratory mode
//			transition, this value should set to 0.
//			Make sure the setBiLevelModeChangeCondition() is always called
//			before enabling the AsapInspTrigger.
//
//  Revision: 006  By:  yyy    Date:  05-Nov-98    DR Number: 5252
//       Project:  Sigma (R8027)
//       Description:
//             Mapping SRS to code. On 18-NOV-1998, completed the change
//			   by removing requirement: 04333 (IV.)
//
//  Revision: 005  By:  yyy    Date:  19-Oct-1998    DR Number: 5217, 5139
//       Project:  BiLevel
//       Description:
//      	Removed FORNOW statement and fixed transition from disconnect.
//
//  Revision: 004  By:  iv    Date: 08-Sep-1998     DR Number:DCS 5140
//       Project:  BiLevel
//       Description:
//             Added a call to enable volume criteria for the disconnect
//             trigger when a no inspiration condition is detected in
//             startExhalation_().
//
//  Revision: 003  By:  syw    Date:  17-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (R8027)
//       Description:
//			Call RPeep.setPeepChange if breath is to be Peep Recovery.
//
//  Revision: 002  By:  dosman Date:  11-May-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             added,fixed TIs
//
//  Revision: 001  By:  yyy    Date:  10-Dec-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BiLevelScheduler.hh"
#include "BiLevelLowToHighInspTrigger.hh"
#include "BiLevelHighToLowExpTrigger.hh"

#include "BreathMiscRefs.hh"
#include "PhaseRefs.hh"
#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "SchedulerRefs.hh"
#include "BdDiscreteValues.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
#include "BreathSet.hh"
#include "BreathTriggerMediator.hh"
#include "UiEvent.hh"
#include "BdSystemStateHandler.hh"
#include "ModeTriggerMediator.hh"
#include "BreathTrigger.hh"
#include "ModeTrigger.hh"
#include "TimerBreathTrigger.hh"
#include "ApneaInterval.hh"
#include "IntervalTimer.hh"
#include "O2Mixture.hh"
#include "Peep.hh"
#include "PendingContextHandle.hh"
#include "PhasedInContextHandle.hh"
#include "PhaseInEvent.hh"
#include "BreathPhase.hh"
#include "InspPauseManeuver.hh"
#include "ManeuverRefs.hh"
#include "InspiratoryPausePhase.hh"
#include "AsapInspTrigger.hh"
#include "DisconnectTrigger.hh"
#include "VolumeTargetedManager.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

//@ Code...

//@ Constant T_HI_INTERVAL_FRACTION
static const Real32 T_HI_INTERVAL_FRACTION = 0.3F;

//@ Constant T_HI_MIN_INTERVAL
static const Real32 T_HI_MIN_INTERVAL = 3000.0F;

//@ Constant T_LO_INTERVAL_FRACTION
static const Real32 T_LO_INTERVAL_FRACTION = 0.4F;

//@ Constant T_LO_MIN_INTERVAL
static const Real32 T_LO_MIN_INTERVAL = 4000.0F;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BiLevelScheduler [Constructor]
//
//@ Interface-Description
//        Constructor.
// $[04119] $[04125] $[04130] $[04141]
// The method takes a reference to a IntervalTimer. It calls the base class
// constructors and initializes some data members.
// The list of valid mode triggers, used by the mode trigger mediator is
// initialized as well. The triggers are ordered based on priority - the most urgent
// is first. The list also guarantees that when a trigger fires - the triggers
// that follow it do not have to be processed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor takes no arguments. It invokes the base class
// constructor with the BiLevel id argument. It initializes the data
// member pModeTriggerList_. 
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
// The number of elements on the trigger list is asserted
// to be MAX_NUM_BILEVEL_TRIGGERS and MAX_NUM_BILEVEL_TRIGGERS to be less than
// MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static const Int32 MAX_NUM_BILEVEL_TRIGGERS = 5;
BiLevelScheduler::BiLevelScheduler(IntervalTimer& rSyncTimer) :
    BreathPhaseScheduler(SchedulerId::BILEVEL), 
    TimerTarget(),
    rCycleTimer_(rSyncTimer),
    peepState_(Peep::PEEP_LOW),
    newPeepState_(Peep::PEEP_LOW),
    readyForSettingChange_(TRUE),
    firstBreathType_(::CONTROL),
    currentBreathType_(::CONTROL),
    highPeepSpontCounter_(0),
    startTimeOfSpontBreath_(0),
    peepHighToLowTransitionTime_(0),
    spontOccuredAtPeepLow_(FALSE),
	highPressureOccured_ (FALSE) 
{
    // $[TI1]
    CALL_TRACE("BiLevelScheduler::BiLevelScheduler(IntervalTimer& rSyncTimer)");

    Int32 ii = 0;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RDisconnectTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&ROcclusionTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RApneaTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RSettingChangeModeTrigger;
    pModeTriggerList_[ii] = (ModeTrigger*)NULL;

    CLASS_ASSERTION(ii == MAX_NUM_BILEVEL_TRIGGERS &&
                        MAX_NUM_BILEVEL_TRIGGERS < MAX_MODE_TRIGGERS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BiLevelScheduler [Destructor]
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
BiLevelScheduler::~BiLevelScheduler()
{
    CALL_TRACE("BiLevelScheduler::~BiLevelScheduler()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathPhase
//
//@ Interface-Description
// This method accepts a breath trigger as an argument and has no return value.
// It determines which breath phase to start, based on the trigger id, 
// the current phase, and the vent settings.
// Once the next breath phase is determined, the following activities take place:
// - phase-in new settings for start of inspiration or start of exhalation, 
//   and update the applied O2%, apnea interval and peep value. 
// - Update the current breath record with new data.
// - Change the active breath trigger list, disabling the triggers on the
//   current list.
// - Instruct the current breath phase to relinquish control and setup
//   the new breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The activities handled by this method are dependent on the current breath
// phase. The current breath phase is retrieved from the BreathPhase object.
// Before any phase starts, the BreathTriggerMediator is used to reset the
// old phase trigger list, and to set the new phase trigger list,  the old 
// phase relinquishes control, and the new phase registers itself with the 
// BreathPhase object. New settings are phased in - when relevant, the applied
// O2 percent is updated if necessary, the apnea interval is updated for start
// of exhalation, and the breath record gets updated. Note that the above
// activities are either done in this method, the method startInspiration_(),
// the method handleInspBreath_(), the method startExhalation_() and the
// method handleExpBreath_().
// A switch statement is implemented for the various breath phases which are:
// NON_BREATHING, EXHALATION, INSPIRATION, EXPIRATORY_PAUSE, INSPIRATORY_PAUSE,
// and BILEVEL_PAUSE_PHASE.
//
// When current phase is EXHALATION, an expiratory pause phase starts if the
// time trigger:BL_LOW_TO_HIGH_INSP is detected and expiratory pause request is
// pending.  An inspiratory pause phase or biLevel spont pause phase starts if
// the time trigger:BL_HIGH_TO_LOW_EXP is detected and inspiratory pause request
// is pending.  If the breath trigger:OPERATOR_INSP is detected during peep
// high, an exhalation starts and it transitions to peep low.  Otherwise the
// breath type is determined and inspiration is delivered.  Inspiration begins by
// calling the method startInspiration_(). Before that method is called, the
// peep state and breath type is set (e.g. CONTROL, ASSIST, etc.) by
// handleInspBreath_().
//
// When current phase is INSPIRATION, an inspiratory pause phase starts if the
// breath trigger:IMMEDIATE_EXP is detected and inspiratory pause request is
// pending.  Otherwise, exhalation begins by call the method startExhalation().
// Before that method is called, the peep state is set by handleExpBreath_().
//
// When the breath phase is EXPIRATORY PAUSE, the expiratory pause event status is
// set to either COMPLETE or CANCEL, and the BiLevel cycle is extended by the
// pause elapsed time before starts inspiration.
//
// When the breath phase is INSPIRATORY_PAUSE or BILEVEL_PAUSE_PHASE, the
// inspiratory pause event status is set to either COMPLETE or CANCEL,
// and starts the exhalation.
//
// When current phase is NON_BREATHING, the breathType is set to SPONT to
// start peep recovery inspiration.
//---------------------------------------------------------------------
//@ PreCondition
// For each case in the switch statement, the expected range of valid
// triggers and/or previous schedulers is checked. The valid range for
// phase type is also checked.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BiLevelScheduler::determineBreathPhase(const BreathTrigger& breathTrigger) 
{

    CALL_TRACE("BiLevelScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)");

    // the type of breath to communicate to the user
    ::BreathType breathType = NON_MEASURED;

    const Trigger::TriggerId triggerId = breathTrigger.getId();

    // Get the scheduler id from the current breath record. Note that the current
    // breath record stores the id of the scheduler that was active at the time the
    // record was created.  
    const SchedulerId::SchedulerIdValue previousSchedulerId =
            (RBreathSet.getCurrentBreathRecord())->getSchedulerId();

    // A pointer to the current breath phase
    BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
    // determine the current phase type (e.g. EXHALATION, INSPIRATION)
    const BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();
	BreathRecord* pCurrentRecord = RBreathSet.getCurrentBreathRecord();

    switch (phase)
    {
        case BreathPhaseType::NON_BREATHING:
        // $[TI1]
            // $[04321] Deliver a peep recovery inspiration:
            BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
            peepState_ = Peep::PEEP_LOW;
            newPeepState_ = Peep::PEEP_LOW;
			phaseInPendingBatch_();
            startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
            break;

        case BreathPhaseType::EXHALATION:
        // $[TI2]
            //$[04243] $[04216] $[04217]
            // Check if time trigger and expiratory pause is requested --

            // check conditions for expiratory pause
            if( (triggerId == Trigger::BL_LOW_TO_HIGH_INSP) &&
                (RExpiratoryPauseEvent.getEventStatus() == EventData::PENDING) )
            {
             // $[TI2.1]
                CLASS_ASSERTION(peepState_ == Peep::PEEP_LOW);

                //start expiratory pause ....
                // $[04221] for the sake of i:e and expiratory time calculation, the breath
                // record considers the expiratory pause phase as part of the expiratory
                // phase, therefore no new phase update for the breath record is necessary.
                // Reset currently active triggers and setup triggers that are active during
                // pause:
                RBreathTriggerMediator.resetTriggerList();
                RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_PAUSE_LIST);

                // notify the user that expiratory pause is active:
                RExpiratoryPauseEvent.setEventStatus(EventData::ACTIVE);

                // current breath phase is given a chance to phase-out
                pBreathPhase->relinquishControl(breathTrigger);
                
                // register an expiratory pause:
                BreathPhase::SetCurrentBreathPhase((BreathPhase&)RExpiratoryPausePhase,
                                                    breathTrigger);
                // update the breath record
                (RBreathSet.getCurrentBreathRecord())->startExpiratoryPause();

                // start the pause
				// $[BL04042] If an expiratory pause is active, the PEEP low phase ...
				BreathPhase::NewBreath() ;
            }
            // check conditions for inspiratory pause
            else if( triggerId == Trigger::BL_HIGH_TO_LOW_EXP &&
            		 RInspPauseManeuver.isManeuverAllowed() &&
                     RInspiratoryPauseEvent.getEventStatus() == EventData::PENDING )
            {
                // $[TI2.2]
                CLASS_ASSERTION(peepState_ == Peep::PEEP_HIGH);

	            //Phase in new settings (for start of exhalation)
    	        PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_EXHALATION);

#ifdef INTEGRATION_TEST_ENABLE
    	        // Signal Event for swat
    	        swat::EventManager::signalEvent(swat::EventManager::START_OF_PEEP_LOW);
#endif // INTEGRATION_TEST_ENABLE


                //start inspiratory pause when in mandatory breath....
			    // $[BL04005]
			    // $[BL04006]
				// $[BL04009] :b inspiratoyr pause phase starts when transitions
				// from a peep high to peep low interval.
                // Reset currently active triggers and setup triggers that are active during
                // pause:
                RBreathTriggerMediator.resetTriggerList();
                RBreathTriggerMediator.changeBreathListName(
                                                BreathTriggerMediator::INSP_PAUSE_LIST);
                // notify the user that inspiratory pause is active:
                RInspiratoryPauseEvent.setEventStatus(EventData::ACTIVE);
                // current breath phase is given a chance to phase-out
                pBreathPhase->relinquishControl(breathTrigger);

            	if (pCurrentRecord->getBreathType() != ::SPONT)
            	{
    	            // $[TI2.2.1]
	                // register an inspiratory pause phase:
				    // $[BL03012] :a pause is part of inspiration phase when
				    // calculating I:E ratio.
    	            BreathPhase::SetCurrentBreathPhase((BreathPhase&)RInspiratoryPausePhase,
                                                                        breathTrigger);
        	        // update the breath record
            	    (RBreathSet.getCurrentBreathRecord())->startInspiratoryPause();
                	// start the pause
					// $[BL04015] If an inspiratory pause is active, the PEEP high phase ...
					BreathPhase::NewBreath() ;
            	}
            	else
            	{
	                // $[TI2.2.2]
				    // $[BL03012] :b pause is part of exhalation phase when
				    // at least one spont breath has taken place.
	                // register an bilevel spont pause:
    	            BreathPhase::SetCurrentBreathPhase((BreathPhase&)RBiLevelSpontPausePhase,
                                                                        breathTrigger);
        	        // update the breath record
            	    (RBreathSet.getCurrentBreathRecord())->startBilevelPausePhase();
                	// start the pause
					// $[BL04015] If an inspiratory pause is active, the PEEP high phase ...
					BreathPhase::NewBreath() ;
            	}
            }
            else if ( (triggerId == Trigger::BL_LOW_TO_HIGH_INSP) &&
                      (previousSchedulerId != SchedulerId::BILEVEL) )
            {
            // $[TI2.3]
                // previous scheduler is not BiLevel, therefore the time trigger should have
                // been disabled:
                CLASS_ASSERTION(peepState_ == Peep::PEEP_LOW);
                AUX_CLASS_ASSERTION_FAILURE(previousSchedulerId);
            }
			else if (triggerId == Trigger::OPERATOR_INSP && peepState_ == Peep::PEEP_HIGH)
			{
		        // $[BL04001]
				// $[TI2.4]
				// When in peep high and manual inspiration is initiated
				// we should force an exhalation and transition to peep low.
				// This is a special case of exhalation (peep high) that is triggered
				// from within exhalation (peep low).

		        // Start exhalation when appropriate newPeepState_ are set.
		        newPeepState_ = Peep::PEEP_LOW;
				startExhalation_(breathTrigger, pBreathPhase, TRUE);
			}
			else if (triggerId == Trigger::BL_HIGH_TO_LOW_EXP)
			{
                                // $[TI2.5]
				// When in peep high and BL_HIGH_TO_LOW_EXP occurred,
				// we should force another exhalation and transition to peep low.
				// This is a special case of exhalation (peep high) that is triggered
				// from within exhalation (peep low).
				// $[BL04035] exhalation transitions from peep high to low
				// when time elapsed during peep high inspiration
				SAFE_CLASS_ASSERTION( peepState_ == Peep::PEEP_HIGH) ;

#ifdef INTEGRATION_TEST_ENABLE
    	        // Signal Event for swat
    	        swat::EventManager::signalEvent(swat::EventManager::START_OF_PEEP_LOW);
#endif // INTEGRATION_TEST_ENABLE

		        // Start exhalation when appropriate newPeepState_ are set.
		        newPeepState_ = Peep::PEEP_LOW;
				startExhalation_(breathTrigger, pBreathPhase, TRUE);
			}
			else if ((triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP))
			{
		        newPeepState_ = Peep::PEEP_LOW;

                // Set the breath type based on the peepState_ in preparing
                // for inspiration.
	            handleInspBreath_(triggerId, breathType);

	            // Start inspiration when appropriate breathType and newPeepState_
	            // are set.
	            startInspiration_(breathTrigger, pBreathPhase, breathType);
			}
            else
            {
            // $[TI2.6]
				if (triggerId == Trigger::PEEP_RECOVERY)
				{
       					// $[TI2.6.1]
					CLASS_ASSERTION( BreathPhaseScheduler::PeepRecovery_ ==
										BreathPhaseScheduler::START &&
									 previousSchedulerId == SchedulerId::OCCLUSION );
				} 
				// $[TI2.6.2]
                // Set the breath type based on the peepState_ in preparing
                // for inspiration.
	            handleInspBreath_(triggerId, breathType);

	            // Start inspiration when appropriate breathType and newPeepState_
	            // are set.
	            startInspiration_(breathTrigger, pBreathPhase, breathType);
            }
            break;

        case BreathPhaseType::INSPIRATION:
        {
        // $[TI3]
            // Check the validity of the triggers. Once triggers are checked, settings for
            // new exhalation can be phased in.

			//Phase in new settings (for start of exhalation)
			PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_EXHALATION);

            // The trigger should be one of the following legal triggers (note that
            // inspiratory phase can be a spont inspiration during mode transition):
            CLASS_PRE_CONDITION( (triggerId == Trigger::DELIVERED_FLOW_EXP) || 
			                (triggerId == Trigger::HIGH_PRESS_COMP_EXP) || 
			                (triggerId == Trigger::LUNG_FLOW_EXP) || 
            			    (triggerId == Trigger::LUNG_VOLUME_EXP) || 
                            (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) || 
                            (triggerId == Trigger::PRESSURE_EXP) || 
                            (triggerId == Trigger::BACKUP_TIME_EXP) || 
                            (triggerId == Trigger::IMMEDIATE_EXP) ||
                            (triggerId == Trigger::HIGH_VENT_PRESSURE_EXP) );

			// Boolean flag checks if inspiratory pause phase is allowed
			// The inspiratory pause phase is allowed only when its event status is PENDING
			// or the Insp. pause maneuver is ACTIVE (due to plateau time)
			Boolean isInspiratoryPausePhaseAllowed =
						(RInspiratoryPauseEvent.getEventStatus() == EventData::PENDING ||
	                     RInspPauseManeuver.getManeuverState() == PauseTypes::ACTIVE);
                     
			// Boolean flag checks if inspiratory pause phase can be executed now
			// When we are doing a mode change, or transitioning from apnea, or
			// occlusion into bilevel, we have to posponding any insp pause active request
			// and delay it to next breath cycle.  Otherwise, apnea may be declared
			// unexpected.
			Boolean isInspiratoryPausePhaseExecutableNow =
                     	(BreathPhaseScheduler::ModeChange_ != TRUE &&
	                	 RBreathSet.getCurrentBreathRecord()->getSchedulerId() != SchedulerId::APNEA &&
    	            	 RBreathSet.getCurrentBreathRecord()->getSchedulerId() != SchedulerId::OCCLUSION);
    	            	 
            // check conditions for inspiratory pause
            if( (triggerId == Trigger::IMMEDIATE_EXP) &&
            		RInspPauseManeuver.isManeuverAllowed() &&
            		isInspiratoryPausePhaseAllowed &&
            		isInspiratoryPausePhaseExecutableNow)
            {
                 // $[TI3.1]
                //start inspiratory pause when in mandatory breath....
			    // $[BL04005]
			    // $[BL04006]
				// $[BL04009] :a start inspiratoyr pause phase at end of inspiration
                // Reset currently active triggers and setup triggers that are active during
                // pause:
                RBreathTriggerMediator.resetTriggerList();
                RBreathTriggerMediator.changeBreathListName(
                                                BreathTriggerMediator::INSP_PAUSE_LIST);

                // notify the user that inspiratory pause is active:
                RInspiratoryPauseEvent.setEventStatus(EventData::ACTIVE);
                // current breath phase is given a chance to phase-out
                pBreathPhase->relinquishControl(breathTrigger);
                // register an inspiratory pause phase:
                BreathPhase::SetCurrentBreathPhase((BreathPhase&)RInspiratoryPausePhase,
                                                                        breathTrigger);
                // update the breath record
                (RBreathSet.getCurrentBreathRecord())->startInspiratoryPause();
                // start the pause
				// $[BL04015] If an inspiratory pause is active, the PEEP high phase ...
				BreathPhase::NewBreath() ;
            }
            else
            {
                 // $[TI3.2]

				if (BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::ACTIVE)
				{
					// $[TI3.2.1]
        	        CLASS_ASSERTION(peepState_ == Peep::PEEP_LOW);
					BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::OFF;
					((BreathTrigger&)RPeepRecoveryMandInspTrigger).enable();
				} 
				else
				{
					// $[TI3.2.2]
					// Set the newPeepState_
					handleExpBreath_(triggerId, pCurrentRecord->getBreathType());
				}

		        // Start exhalation when appropriate newPeepState_ are set.
				startExhalation_(breathTrigger, pBreathPhase);
            }

            break;
        }
        
        case BreathPhaseType::EXPIRATORY_PAUSE: 
        {
        // $[TI4]          
            // set the pause event status to notify user of exp pause status:
            EventData::EventStatus eventStatus;
            eventStatus = (triggerId==Trigger::PAUSE_COMPLETE) ?
                                EventData::COMPLETE:     // $[TI4.4]
                                EventData::CANCEL;       // $[TI4.5]
            RExpiratoryPauseEvent.setEventStatus(eventStatus);

		 	// $[BL04071] :m terminate pause event and start inspiration
		    // $[04220] :m terminate active manual expiratory pause
	        // $[01253] $[03008] $[04010] It's time to start new inspiration:

            //Determine the type of inspiration to be delivered:
            if ( (triggerId == Trigger::PAUSE_TIMEOUT) ||
                 (triggerId == Trigger::PAUSE_COMPLETE) || 
                 (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_INSP) ||
                 (triggerId == Trigger::IMMEDIATE_BREATH) ) 
            {
            // $[TI4.1]
                breathType = ::CONTROL; 
            }
            else if(triggerId == Trigger::PAUSE_PRESS)
            {
            // $[TI4.2]
                breathType = ::ASSIST;
            }             
            else
            {
            // $[TI4.3]
                AUX_CLASS_ASSERTION_FAILURE(triggerId);
            }

	        // Start inspiration when appropriate newPeepState_ are set.
			newPeepState_ = Peep::PEEP_HIGH;
		    // $[04242] peep low phase is extended by the duration
		    // of the pause phase

			phaseInPendingBatch_();
            startInspiration_(breathTrigger, pBreathPhase, breathType);
        }
            break;
        
        case BreathPhaseType::INSPIRATORY_PAUSE: 
        case BreathPhaseType::BILEVEL_PAUSE_PHASE: 
            // $[TI5]
            // set the pause event status to notify user of insp pause status:
            EventData::EventStatus eventStatus;
                // $[TI5.1] COMPLETE
                // $[TI5.2] CANCEL
            eventStatus = (triggerId==Trigger::PAUSE_COMPLETE) ?
                                EventData::COMPLETE:EventData::CANCEL;
            RInspiratoryPauseEvent.setEventStatus(eventStatus);

            //The trigger should have been one of the above triggers
		    // $[BL04078] :m terminate active manual inspiratory pause
		    // $[BL04012] :m terminate active auto inspiratory pause
            AUX_CLASS_ASSERTION( (triggerId == Trigger::PAUSE_TIMEOUT) || 
                             (triggerId == Trigger::PAUSE_COMPLETE) ||
                             (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) || 
                             (triggerId == Trigger::PAUSE_PRESS) ||
                             (triggerId == Trigger::IMMEDIATE_BREATH),
                             triggerId );

	        // Start exhalation when appropriate newPeepState_ are set.
	        newPeepState_ = Peep::PEEP_LOW;
		    // $[04215] peep high phase is extended by the duration
		    // of the pause phase
			startExhalation_(breathTrigger, pBreathPhase, TRUE);
            break;

        case BreathPhaseType::PAV_INSPIRATORY_PAUSE:
	        // $[TI7]
            AUX_CLASS_ASSERTION( triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP ||
                             	 triggerId == Trigger::IMMEDIATE_BREATH ||
                             	 triggerId == Trigger::PAUSE_COMPLETE,
                             	 triggerId ) ;
            startExhalation_(breathTrigger, pBreathPhase);
        	break ;
        
        default:
        // $[TI6]
            AUX_CLASS_ASSERTION_FAILURE(phase);
    } //switch                                                        
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
// The method accepts a mode trigger reference as an argument and has no return
// value.  Before the new mode is instructed to take control, the BiLevel time
// trigger and the peep recovery insp trigger are disabled, the asap trigger is
// enabled (on transition to AC, and Apnea modes), and all mode triggers on the
// current trigger list get reset.  Since BiLevel mode may follow SVO scheduler, a
// NON_BREATHING breath phase is possible. In that case, any breath delivery is
// avoided until the completion of the transitional NON_BREATHING phase that
// guarantees a complete closure of the safety valve.  Therefore, no breath
// triggers are enabled when a transition to a breathing mode is detected.
// When relinquishing control in peep high state during exhalation, we need to
// enable the PeepReductionExpTrigger to transition to peep low state as fast
// as possible.  If the peep state is peep low and the breathing mode is CONTROL,
// ASSIST or APNEA then we shall enable the AsapInspTrigger, this immediate
// breath trigger is enabled, the rest of the triggers on the active list are
// disabled to ensure safe transition to the emergency mode.  No breath triggers
// are enabled on transition to Spont mode.  The argument passed, provides
// the method with the information required to determine the next scheduler.
//---------------------------------------------------------------------
//@ Implementation-Description
// Individual breath triggers are enabled and disabled by directly
// accessing the trigger instance.
// Disabling all triggers on the current list is accomplished by 
// using the only one instance of the breath trigger mediator,
// calling the method resetTriggerList(). 
// The mode trigger id passed as an argument is checked to
// determine the requested scheduler which then instructed to
// take control.
// The data member peepHighToLowTransitionTime_ is set during mode change
// to ensure the proper breath duration is maintained for asap trigger.
//---------------------------------------------------------------------
//@ PreCondition
// The mode setting is checked for valid range of values.
// The trigger id is checked for valid range of mode triggers.
// When the mode trigger is either apnea or mode change, the non breathing
// phase is checked to be SafetyValveClosePhase.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BiLevelScheduler::relinquishControl(const ModeTrigger& modeTrigger)
{
    CALL_TRACE("BiLevelScheduler::relinquishControl(const ModeTrigger& modeTrigger)");

    const Trigger::TriggerId modeTriggerId = modeTrigger.getId();

    // get the type of the current breath phase:
    const BreathPhaseType::PhaseType phase = BreathRecord::GetPhaseType();

    // disable the inspiratory time trigger, to avoid BiLevel inspiration trigger
    // during the new mode
    ((BreathTrigger&)RBiLevelLowToHighInspTrigger).disable();
    
    // disable the expiratory time trigger, to avoid BiLevel expiratory trigger
    // during the new mode
    ((BreathTrigger&)RBiLevelHighToLowExpTrigger).disable();
    
    // make sure the syncTimer is stopped to avoid SYNC_INTERVAL and
    // SPONT_INTERVAL state change.
    rCycleTimer_.stop();
    
    // disable the peep recovery trigger, to avoid its interference with
    // the next scheduler.
    ((BreathTrigger&)RPeepRecoveryInspTrigger).disable();

    // reset all mode triggers for this scheduler
    RModeTriggerMediator.resetTriggerList();

    // check what mode to switch to
    DiscreteValue mode = PendingContextHandle::GetDiscreteValue(SettingId::MODE);

	// Note the order of the next two blocks is very important for
	// triggering the asap trigger.
	// We always want to set the asap trigger conditions by calling
	// setBiLevelModeChangeCondition() before enabling the asap trigger.
	
    if(modeTriggerId == Trigger::SETTING_CHANGE_MODE)
    {
    // $[TI3]
		Boolean spontAndModeChangeOccuredAtPeepLow = FALSE;
		Boolean isInspiratoryPhase = FALSE;
		
    	if (peepState_ == Peep::PEEP_LOW)
    	{
			// $[TI3.1]
    		if (currentBreathType_ == ::SPONT)
    		{
				// $[TI3.1.1]
	    		if (spontOccuredAtPeepLow_)
    			{
					// $[TI3.1.1.1]
    				spontAndModeChangeOccuredAtPeepLow = TRUE;
					peepHighToLowTransitionTime_ = 0;
					startTimeOfSpontBreath_ = 0;
	    		}
    			else
    			{
					// $[TI3.1.1.2]
    				// peepHighToLowTransitionTime_ is set when peep transition
    				// occurs during peep high to peep low exhalation.
    				// Since the start of a spont breath (new inspiration) resets
    				// the breath duration, so we need to substract the
    				// startTimeOfSpontBreath_ from the peepHighToLowTransitionTime_
    				// in order to sync with the new breath duration.
	    			peepHighToLowTransitionTime_ -= startTimeOfSpontBreath_;
    			}
    		}
			// $[TI3.1.2]
    	}
    	else if (peepState_ == Peep::PEEP_HIGH)
    	{	// $[TI3.2]
			if (phase == BreathPhaseType::EXHALATION)
			{
				// $[TI3.2.1]
			    // Remember the time when the peep stays high. relinquishControl()
			    // occurs at peep high and we have not had a chance to set the
			    // peepHighToLowTransitionTime_ yet (it only occurs when
		    	// peep high transitions to peep low during exhalation).  So
			    // the current time will be use as peepHighToLowTransitionTime_.
				peepHighToLowTransitionTime_ = rCycleTimer_.getCurrentTime();
			
    			if (currentBreathType_ == ::SPONT)
    			{
					// $[TI3.2.1.1]
	   				// Since the start of a spont breath (new inspiration) resets
   					// the breath duration, we need to subtract the
   					// startTimeOfSpontBreath_ from the peepHighToLowTransitionTime_
   					// in order to sync with the new breath duration.
    				peepHighToLowTransitionTime_ -= startTimeOfSpontBreath_;
	    		}
				// $[TI3.2.1.2]
			}
			else
			{
				// $[TI3.2.2]
				isInspiratoryPhase = TRUE;
			}
    	}
    	else
    	{
			// $[TI3.3]
            CLASS_PRE_CONDITION(peepState_ == Peep::PEEP_HIGH ||
                                peepState_ == Peep::PEEP_LOW);
    	}

		RAsapInspTrigger.setBiLevelModeChangeCondition(
							peepHighToLowTransitionTime_,
							currentBreathType_,
							isInspiratoryPhase,
							spontAndModeChangeOccuredAtPeepLow);

        // $[04230] $[04231]
        if(mode == ModeValue::AC_MODE_VALUE)
        {
        // $[TI3.4]
            ((BreathPhaseScheduler&)RAcScheduler).takeControl(*this);
        }
        else if ((mode == ModeValue::SPONT_MODE_VALUE) || (mode == ModeValue::CPAP_MODE_VALUE))
        {
        // $[TI3.5]
            // $[04253]
            ((BreathPhaseScheduler&)RSpontScheduler).takeControl(*this);
        } 
        else if(mode == ModeValue::SIMV_MODE_VALUE)
        {
        // $[TI3.6]
            // $[04253]
            ((BreathPhaseScheduler&)RSimvScheduler).takeControl(*this);
        } 
		else
        {
			AUX_CLASS_ASSERTION_FAILURE(mode);
        }
    }
    else if(modeTriggerId == Trigger::APNEA)
    {
    // $[TI4]
    	// We do not need to call setBiLevelModeChangeCondition() to adjust
    	// the asap trigger time for apnea because the apnea time requirement
    	// has been met and all we need to do is let apnea to take control.
        ((BreathPhaseScheduler&)RApneaScheduler).takeControl(*this);
    }
    else if(modeTriggerId == Trigger::OCCLUSION)
    {
    // $[TI5]
        ((BreathPhaseScheduler&)ROscScheduler).takeControl(*this);
    }
    else if(modeTriggerId == Trigger::DISCONNECT)
    {
    // $[TI6]
        ((BreathPhaseScheduler&)RDisconnectScheduler).takeControl(*this);
    }
    else if(modeTriggerId == Trigger::SVO)
    {
    // $[TI7]
        ((BreathPhaseScheduler&)RSvoScheduler).takeControl(*this);
    }

    if ( (modeTriggerId == Trigger::SETTING_CHANGE_MODE) ||
         (modeTriggerId == Trigger::APNEA) )
    {
    // $[TI1]
        if (phase != BreathPhaseType::NON_BREATHING)
        {
		// $[TI1.1]
           	if (peepState_ == Peep::PEEP_HIGH)
           	{
			// $[TI1.1.1]
				BreathPhaseScheduler::BlPeepTransitionState_ =
                                             BreathPhaseScheduler::HIGH_TO_LOW;
    
                if (phase == BreathPhaseType::EXHALATION)
                {
				// $[TI1.1.1.1]
            		// When in peep high and exhalation state, we need to transition
             		// to peep low on the next BD cycle to trigger a change in the
            		// active breath phase as fast as possible.
			    	((BreathTrigger&)RPeepReductionExpTrigger).enable();
			    }
			    else
			    {
				// $[TI1.1.1.2]
	        	   // enable the asap trigger only if the mode is not spont. 
    	        	if ((mode != ModeValue::SPONT_MODE_VALUE) && (mode != ModeValue::CPAP_MODE_VALUE))
	    	        {
					// $[TI1.1.1.2.1]
						((BreathTrigger&)RAsapInspTrigger).enable();
            		}
					// $[TI1.1.1.2.2]
			    }
           	}
           	else
           	{
	           // $[TI1.1.2]
    	       //$[04263]
            	if ( ((mode != ModeValue::SPONT_MODE_VALUE) && (mode != ModeValue::CPAP_MODE_VALUE)) 
				     || (modeTriggerId == Trigger::APNEA))
	        	{
				// $[TI1.1.2.1]
					((BreathTrigger&)RAsapInspTrigger).enable();
            	}
				// $[TI1.1.2.2] -- implied else
            }
        }
        else // phase == BreathPhaseType::NON_BREATHING
        {
            // $[TI1.2]
            // If the breath phase is a non breathing phase, it is the
            // safety valve closed phase that is in progress, and since a time trigger
            // is already set to terminate that phase, there is no need to set any
            // breath trigger.
            CLASS_PRE_CONDITION(BreathPhase::GetCurrentBreathPhase()
                                         == (BreathPhase*)&RSafetyValveClosePhase);
        }
    }
    else
    {
        // $[TI2]
        //Reset currently active triggers and then enable the only valid breath
        // trigger for that transition.
        RBreathTriggerMediator.resetTriggerList();
        ((BreathTrigger&)RImmediateBreathTrigger).enable();
        // make sure trigger is valid:
        CLASS_PRE_CONDITION( 
            (modeTriggerId == Trigger::OCCLUSION) ||
            (modeTriggerId == Trigger::DISCONNECT) ||
            (modeTriggerId == Trigger::SVO) );
    }

    BreathRecord::SetIsHighPeep(FALSE);
    BreathRecord::SetBiLevelPhase(BreathRecord::NORMAL_INSP_EXH);

    peepState_ = Peep::PEEP_LOW;
    newPeepState_ = Peep::PEEP_LOW;
}      

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description
// The method is invoked by the method relinquishControl() from the scheduler
// instance that relinquishes control.  It accepts a breath phase scheduler
// reference as an argument and has no return value. The base class method for
// takeControl() is invoked to handle activities common to all schedulers.
// Peep recovery flag is set on transition from a non breathing mode. The
// method is responsible for extending the apnea interval when a transition to
// a breathing mode is in progress.  The NOV RAM instance rBdSystemState is
// updated with the new scheduler to enable BD system to properly recover from
// power interruption.  The BiLevel scheduler data members are initialized
// to indicate the start of a new BiLevel interval.  The scheduler registers with
// the BreathPhaseScheduler as the current scheduler, and the mode triggers
// relevant to that mode are set to be enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler reference passed as an argument is used to get the id of the
// current scheduler. The apnea interval is extended only if the id is SPONT,
// AC or APNEA. The apnea extension is accomplised by invoking the method
// extendIntervalForModeChange() in the apnea interval instance. The static
// method of BreathPhaseScheduler is used to register the new scheduler. The
// BdSystemStateHandler is used to update the NOV RAM object for BdSystemState
// with the new scheduler id.  The BiLevel scheduler data members are
// initialized to indicate mandatory interval, first BiLevel breath, and zero
// extension for the BiLevel interval.  The data member pModeTriggerList_
// is passed as an argument to the method in the mode trigger mediator object
// that is responsible for changing the current mode trigger list.
//---------------------------------------------------------------------
//@ PreCondition
// The CLASS_PRE_CONDITION method is used to validate the IDs of the
// valid breath phase schedulers.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BiLevelScheduler::takeControl(const BreathPhaseScheduler& scheduler) 
{
    CALL_TRACE("BiLevelScheduler::takeControl(const BreathPhaseScheduler& scheduler)");

    // Invoke the base class takeControl method
    BreathPhaseScheduler::takeControl(scheduler);
    
    SchedulerId::SchedulerIdValue schedulerId = scheduler.getId();
    
    if( (schedulerId == SchedulerId::AC) ||
        (schedulerId == SchedulerId::SIMV) ||
        (schedulerId == SchedulerId::SPONT) ||
        (schedulerId == SchedulerId::APNEA) )
    {
    // $[TI1]
        // $[BL4049] :b On transition to BiLevel mode, the apnea interval should be extended
        // if necessary.
        // Note that the initialization of the following data members might be overwritten
        // in determineBreathPhase() - if the transition is done during a NON_BREATHING
        // phase.  
        RApneaInterval.extendIntervalForModeChange();  
    }
    else if((schedulerId == SchedulerId::DISCONNECT) || 
            (schedulerId == SchedulerId::STANDBY) ||
            (schedulerId == SchedulerId::SVO) ||
            (schedulerId == SchedulerId::OCCLUSION) ||
            (schedulerId == SchedulerId::POWER_UP) )
    {
    // $[TI2]
        BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
    }
    else
    {
    // $[TI3]
        CLASS_ASSERTION(schedulerId == SchedulerId::SAFETY_PCV);
    }

    // update the reference to the newly active breath scheduler 
    BreathPhaseScheduler::SetCurrentScheduler_((BreathPhaseScheduler&)(*this));

    //The BreathRecord is not being updated with the new scheduler id, so that
    // the identity of the scheduler that originated this breath is maintained. 
    // until a new inspiration starts.

    // Initialize the cycle time and syncCycleTimeMs_ to an arbitrary worst case value.
    // This value does not affect ventilation.  
    biLevelPeepCycleTimeMs_ = 60000;
    syncCycleTimeMs_ = 0;

    // Reset the bilevel timer
   	rCycleTimer_.stop();
   	rCycleTimer_.setCurrentTime(60000);

    // update the BD state, in case of power interruption
    BdSystemStateHandler::UpdateSchedulerId(getId()); 

    // Set the mode triggers for that scheduler.
    enableTriggers_();    
    RModeTriggerMediator.changeModeTriggerList(pModeTriggerList_);

    // $[BL04050]
    // $[BL04051]
    // $[BL04048] transition to BiLevel shall cause the ventilator
    // to phase in a VIM 
    peepState_ = Peep::PEEP_LOW;
    newPeepState_ = Peep::PEEP_LOW;
    readyForSettingChange_ = TRUE;
} 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
// This method takes a reference to an IntervalTimer as an argument and has no
// return value. The BiLevel scheduler is a client of the timer server that is
// responsible to notify (by invoking this function) the end of the current
// BiLevel interval.  This routine sets up timeout for either the
// BiLevelLowToHighInspTrigger or BiLevelHighToLowExpTrigger, and enable it.
//---------------------------------------------------------------------
//@ Implementation-Description
// The IntervalTimer reference passed in, rTimer, is that of the cycle timer
// server for the BiLevel scheduler. To initialize the new interval, the
// following activities are taken place:
// Stop the timer, but do not reset the currentTime_, so we can check
// if the SYNC/SPONT time interval has been reached later.  Check
// the current breath phase type, and setup the  timeout flag for the
// corresponding triggers:
// BiLevelLowToHighInspTrigger when in peep low or BiLevelHighToLowExpTrigger
// when in peep high.
//---------------------------------------------------------------------
//@ PreCondition
// The id of the timer server is checked to match the BiLevel sync timer id.
// Peep recovery is off.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BiLevelScheduler::timeUpHappened(const IntervalTimer& rTimer)
{
    CALL_TRACE("BiLevelScheduler::timeUpHappened(const IntervalTimer& rTimer)");

    // make sure the caller is the proper server
    CLASS_PRE_CONDITION(&rTimer == &rCycleTimer_);
    CLASS_ASSERTION(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::OFF);

	// Stop the timer, but do not reset the currentTime_,
	// so we can check if the SYNC/SPONT time interval has been
	// reached later.
    rCycleTimer_.stop();
    	
	if (peepState_ == Peep::PEEP_LOW)
	{
		// $[TI1]
		RBiLevelLowToHighInspTrigger.timeUpHappened(TRUE,
													currentBreathType_,
													phasedInPeepLowTimeMs_,
													BreathPhaseScheduler::RateChange_);
		BreathPhaseScheduler::RateChange_ = FALSE;
	}
	else if (peepState_ == Peep::PEEP_HIGH)
	{
		// $[TI2]
		RBiLevelHighToLowExpTrigger.timeUpHappened(TRUE);
	}
	else
	{
        CLASS_ASSERTION_FAILURE();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFirstBreathtype
//
//@ Interface-Description
//	The method takes no arguments and returns the bilevel mandatory breath
//  type.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The data member firstBreathType_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
::BreathType
BiLevelScheduler::getFirstBreathType(void) const
{
	// $[TI1]
	CALL_TRACE("BilevelScheduler::getFirstBreathType(void)");
	   
    return (firstBreathType_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BiLevelScheduler::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BILEVELSCHEDULER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: settingChangeHappened_
//
//@ Interface-Description
// The method takes a SettingId id as an argument and has no return value.  The
// implementation of this method considers multiple rate/inspiratory time change
// events for the same BiLevel interval. A mode change is checked first and if
// true, the setting change mode trigger is enabled.  For rate change, we
// update the pendingPeepHighTimeMs_ and the pendingPeepLowTimeMs_ based on
// the respiratory rate and peep high time.  These two pending timers shall be
// compared against their corresponding "phased in" time to set the peepHighChanged_
// and the peepLowChanged_ flags.  If the current breath phase is
// exhalation, then we need to adjust the breath interval based on the
// pending timers.
//---------------------------------------------------------------------
//@ Implementation-Description
// The client of this method may use only one SettingId as an argument. The
// method checks the id value and sets the static data members ModeChange_ and
// RateChange_ accordingly. For a mode change, the mode trigger instance:
// rSettingChangeModeTrigger is enabled.  For rate change/inspiratory time,
// it follows the rules mentioned in the interface-description.  If the user
// changed both rate and inspiratory time, then only one set of setting
// changes will be recognized as controlled by readyForSettingChange_.
//---------------------------------------------------------------------
//@ PreCondition
// The setting id is within the folowing range:
//   id >= SettingId::LOW_BATCH_BD_ID_VALUE && id <= SettingId::HIGH_BATCH_BD_ID_VALUE
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
BiLevelScheduler::settingChangeHappened_(const SettingId::SettingIdType id) 
{
    CALL_TRACE("BiLevelScheduler::settingChangeHappened_(const SettingId::SettingIdType id)");

    if (id == SettingId::MODE)
    {
    // $[TI1]
        // Check the mode value. Ignore any pending rate changes. Rate change, if any, will be
        // updated on the next inspiration. 
        const DiscreteValue mode = PendingContextHandle::GetDiscreteValue(SettingId::MODE);

        if (mode != ModeValue::BILEVEL_MODE_VALUE)
        {
        // $[TI1.1]
            BreathPhaseScheduler::ModeChange_ = TRUE;    
            BreathPhaseScheduler::RateChange_ = FALSE;
	        ((Trigger&)RSettingChangeModeTrigger).enable();
        }
        // $[TI1.2]
    }
    else if (!BreathPhaseScheduler::ModeChange_)
    {
	// $[TI2]
    	// Processing setting change only when first setting changed is acknowledged.
    	// The second setting change will be ignored.  Flag readyForSettingChange_
    	// signifies the previous setting changes have been processed.
    	if (id == SettingId::RESP_RATE || id == SettingId::PEEP_HIGH_TIME)
    	{
		// $[TI2.1]
	    	if (readyForSettingChange_)
    		{
			// $[TI2.1.1]
		        // Get pending respiratory rate setting
				Real32 pendingRate = PendingContextHandle::GetBoundedValue(SettingId::RESP_RATE).value;
    
				// Get inspiratory time for peep high:
				pendingPeepHighTimeMs_ = (Int32)
						(PendingContextHandle::GetBoundedValue(SettingId::PEEP_HIGH_TIME).value);

				// Calculate expiratory time for peep low:
				pendingPeepLowTimeMs_ = (Int32)((60.0F * 1000.0F / pendingRate) - pendingPeepHighTimeMs_);

				// Setting callback triggers when the block of setting changes
				// are all in the PendingContextHandle.  We just look ahead for the
				// RESP_RATE and INSP_TIME to set readyForSettingChange_ flag accordingly.  
	    		// readyForSettingChange_ : TRUE  - only one setting has been changed,
	    		//									so after we process this setting change
	    		//									we can allow the next setting change
	    		//									to occur (process it).
	    		//						 	FALSE - both settings have changed.
	    		//									Since there are two setting changes
	    		//									and we only need to do the change once.
	    		//									When the first setting change notice comes,
	    		//									set the readyForSettingChange_ to FALSE
	    		//									to avoid the change being processed twice.
	    		//									When the second setting change notice
	    		//									comes, we simply reset it (set to TRUE)
	    		//									so we are ready to process next batch of
	    		//									setting changes.
    			Boolean samePeepHighTime = (pendingPeepHighTimeMs_ == phasedInPeepHighTimeMs_);
    			Boolean sameRespRate = (pendingRate ==
									   PhasedInContextHandle::GetBoundedValue(SettingId::RESP_RATE).value);
    			readyForSettingChange_ = (samePeepHighTime ^ sameRespRate);

	    		if (pendingPeepHighTimeMs_ < phasedInPeepHighTimeMs_)
	    		{
				// $[TI2.1.1.1]
	    			peepHighChanged_ = TRUE;
	    		}
	    		else
	    		{
	    		// $[TI2.1.1.2]
	    			peepHighChanged_ = FALSE;
	    		}

	    		if (pendingPeepLowTimeMs_ < phasedInPeepLowTimeMs_)
	    		{
	    		// $[TI2.1.1.4]
	    			peepLowChanged_ = TRUE;
	    		}
	    		else
	    		{
	    		// $[TI2.1.1.5]
	    			peepLowChanged_ = FALSE;
	    		}

				BreathPhaseScheduler::RateChange_ = (peepLowChanged_ || peepHighChanged_);

			    // A pointer to the current breath phase
			    BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
			    // determine the current phase type (e.g. EXHALATION, INSPIRATION)
			    const BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();

				if (phase == BreathPhaseType::EXHALATION)
				{
				// $[TI2.1.1.6]
					adjustIntervalForRateChange_();
				}
				// $[TI2.1.1.7] -- implied else
			    //$[BL04043] The inspiration phase, during which the breath timing ..
			}
	    	else
    		{
			// $[TI2.1.2]
				readyForSettingChange_ = TRUE;
				CLASS_ASSERTION(id >= SettingId::LOW_BATCH_BD_ID_VALUE &&
								id <= SettingId::HIGH_BATCH_BD_ID_VALUE );
	    	}
    	}
    	else
    	{
		// $[TI2.2]
    		readyForSettingChange_ = TRUE;
    	}
    }
    else
    {    // $[TI3]
        CLASS_ASSERTION( id >= SettingId::LOW_BATCH_BD_ID_VALUE &&
                     id <= SettingId::HIGH_BATCH_BD_ID_VALUE );
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts a EventId and a Boolean for the event status as
// arguments.  It returns a EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247] $[01255]
// The argument of type EventId specifies what is the active event.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is accepted by
// invoking a call to the static method:
// BreathPhaseScheduler::AcceptManualInspiration_(eventStatus).
// The alarm reset event is ignored, while the eventStatus argument value is checked for
// TRUE.
// The expiratory pause event is accepted by invoking a call to the static method:
// BreathPhaseScheduler::AcceptExpiratoryPause_(eventStatus).
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

EventData::EventStatus
BiLevelScheduler::reportEventStatus_(const EventData::EventId id,
                                                     const Boolean eventStatus)
{
    CALL_TRACE("BiLevelScheduler::reportEventStatus_(const EventData::EventId id,\
                                                     const Boolean eventStatus)");
    EventData::EventStatus rtnStatus = EventData::IDLE;

    switch (id)
    {
        case EventData::MANUAL_INSPIRATION:
        // $[TI1]
            rtnStatus =
                    BreathPhaseScheduler::AcceptManualInspiration_(eventStatus);
            break;

        case EventData::ALARM_RESET:
        // $[TI2]
            // ignore alarm reset
            CLASS_PRE_CONDITION(eventStatus);
            break;

        case EventData::EXPIRATORY_PAUSE:
        // $[TI3]
            rtnStatus =
                    BreathPhaseScheduler::AcceptExpiratoryPause_(eventStatus);
            break;

        case EventData::INSPIRATORY_PAUSE:
        // $[TI4]
            rtnStatus =
                    BreathPhaseScheduler::AcceptInspiratoryPause_(eventStatus);
            break;

		case EventData::NIF_MANEUVER:
		case EventData::P100_MANEUVER:
		case EventData::VITAL_CAPACITY_MANEUVER:
			// $[RM12031] RM maneuvers cannot be performed in BiLevel
			rtnStatus = EventData::REJECTED;
			break;

        default:
        // $[TI5]
            AUX_CLASS_ASSERTION_FAILURE(id);
    }    

    return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableTriggers_
//
//@ Interface-Description
// $[04119] $[04125] $[04130] $[04141]
// The method takes no arguments and returns no value.  A request to enable
// BiLevel scheduler valid mode triggers is issued whenever the BiLevel scheduler is
// taking control. Immediate triggers are not set here since by design they are
// only set once the trigger they instanciate becomes active.
//---------------------------------------------------------------------
//@ Implementation-Description
// The mode trigger instances are accessed directly with a call to their
// setIsEnableRequested() method.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
BiLevelScheduler::enableTriggers_(void) 
{
// $[TI1]
    CALL_TRACE("BiLevelScheduler::enableTriggers_(void)");

    ((ModeTrigger&)RApneaTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)ROcclusionTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RDisconnectTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RSvoTrigger).setIsEnableRequested(TRUE);
    
    // Note that the immediate, or event driven triggers are not
    // enabled here: rSettingChangeModeTrigger
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: phaseInPendingBatch_
//
//@ Interface-Description
// The method has no arguments and no returned value. It calculates
// the phasedInPeepHighTimeMs_ and phasedInPeepLowTimeMs_ and sets the
// biLevelPeepCycleTimeMs_.
//---------------------------------------------------------------------
//@ Implementation-Description
// The method collaborates with the handle PhasedInContextHandle to phase in new
// settings and sets the biLevelPeepCycleTimeMs.  A check that the scheduler
// matches the mode setting is done by calling the method
// VerifyModeSynchronization_().
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BiLevelScheduler::phaseInPendingBatch_(void)
{
    const SchedulerId::SchedulerIdValue previousSchedulerId =
            (RBreathSet.getCurrentBreathRecord())->getSchedulerId();

	// When curent breath phase is EXHALATION, get ready for
	// inspiration.
    // Phase in new settings (for start of inspiration)
		// $[BL04047]
    PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
    swat::EventManager::signalEvent(swat::EventManager::START_OF_PEEP_HIGH);
#endif // INTEGRATION_TEST_ENABLE

    // verify BD is in synch with GUI:
    BreathPhaseScheduler::VerifyModeSynchronization_(getId());

	// Get cycle time for peep high:
	phasedInPeepHighTimeMs_ = (Int32)(PhasedInContextHandle::GetBoundedValue(SettingId::PEEP_HIGH_TIME).value);

	// phased in respiratory rate setting
	const Real32 rate =
			PhasedInContextHandle::GetBoundedValue(SettingId::RESP_RATE).value;

	// $[BL04020] bilevel breath cycle 
	Real32 respRateCycleTimeMs = 60.0F * 1000.0F / rate;

	// Get cycle time for peep low:
	// $[BL04025] duration of peep low phase
	phasedInPeepLowTimeMs_ = (Int32) respRateCycleTimeMs - phasedInPeepHighTimeMs_;

	if (peepState_ == Peep::PEEP_HIGH)
	{
		// $[TI1]
		biLevelPeepCycleTimeMs_ = phasedInPeepHighTimeMs_;
	}
	else if (peepState_ == Peep::PEEP_LOW)
	{
		// $[TI2]
		biLevelPeepCycleTimeMs_ = phasedInPeepLowTimeMs_;
	}
	else
	{
		// $[TI3]
		AUX_CLASS_ASSERTION_FAILURE(peepState_);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: handleInspBreath_
//
//@ Interface-Description
// The method takes a breath trigger Id, and a breath type reference as
// arguments.  It does not return any value.  When the next phase
// is determined to be inspiration, this method is called to perform all the
// standard procedures required to setup the start inspiration: the breathType
// and newPeepState_.
//---------------------------------------------------------------------
//@ Implementation-Description
// Phase in the setting and calculate the syncCycleTimeMs_ based on
// the newly calculated biLevelPeepCycleTimeMs.
// 
// When in Peep Low state, then do the following setup.
//		condition				interval		breathType	newPeepState_
// 1. During mode change and
// 	  ASAP_INSP,
//	  PRESSURE_BACKUP_INSP,
//	  PRESSURE_INSP,
//	  NET_FLOW_INSP,
//    OPERATOR_INSP,
//	  PEEP_RECOVERY_MAND_INSP,					CONTROL		PEEP_HIGH
//
// 2. Not during mode change and
// 2a.OPERATOR_INSP,
// 	  ASAP_INSP,
//	  PEEP_RECOVERY_MAND_INSP,
//	  BL_LOW_TO_HIGH_INSP						CONTROL		PEEP_HIGH
//
// 2b.NET_FLOW_INSP,
//	  PRESSURE_BACKUP_INSP,
//	  PRESSURE_INSP				spont			SPONT		same peep state
//
// 2c.NET_FLOW_INSP,
//	  PRESSURE_BACKUP_INSP,
//	  PRESSURE_INSP				sync			ASSIST		PEEP_HIGH
//
// 2d.PEEP_RECOVERY								SPONT		same peep state
//
// When in Peep High state, then do the following setup.
//		condition				breathType			newPeepState_
// 1. NET_FLOW_INSP
//	  PRESSURE_BACKUP_INSP
//	  PRESSURE_INSP				SPONT				same peep state
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BiLevelScheduler::handleInspBreath_(Trigger::TriggerId triggerId,
								 	::BreathType &breathType)
{
    const SchedulerId::SchedulerIdValue previousSchedulerId =
            (RBreathSet.getCurrentBreathRecord())->getSchedulerId();

	phaseInPendingBatch_();

	setSyncCycleTime_();

	if (peepState_ == Peep::PEEP_LOW)
	{
		// $[TI6]
		if (BreathPhaseScheduler::ModeChange_ == TRUE)
		{
			// $[TI6.1]
			// $[BL04049] The first bilevel breath (the VIM breath) shall be ...
			if (triggerId == Trigger::ASAP_INSP ||
				triggerId == Trigger::PRESSURE_BACKUP_INSP ||
				triggerId == Trigger::PRESSURE_INSP ||
				triggerId == Trigger::NET_FLOW_INSP ||
				triggerId == Trigger::NET_FLOW_BACKUP_INSP ||
				triggerId == Trigger::OPERATOR_INSP ||
				triggerId == Trigger::PEEP_RECOVERY_MAND_INSP)
			{
				// $[TI6.1.1]
				// $[BL04040] a peep low to peep high transition is
				// considered a mandatory inspiration.
				// When during mode change and patient trigger occured
				// then we want to honor this trigger but set as an
				// CONTROL type.
				breathType = ::CONTROL;
				newPeepState_ = Peep::PEEP_HIGH;
			}
			else
			{
				// $[TI6.1.2]
				AUX_CLASS_ASSERTION_FAILURE(triggerId);
			}			
		}
		else if (triggerId == Trigger::OPERATOR_INSP ||
			triggerId == Trigger::ASAP_INSP ||
			triggerId == Trigger::PEEP_RECOVERY_MAND_INSP ||
			triggerId == Trigger::BL_LOW_TO_HIGH_INSP)
		{	
			// $[TI6.2]
	        // $[BL04000]
			// forces transition from peep low to peep high
			// $[BL04040] a peep low to peep high transition is
			// considered a mandatory inspiration.
			// Handle manual inspiration (10.2.1.ae), peep recovery, and apnea
			breathType = ::CONTROL;
			newPeepState_ = Peep::PEEP_HIGH;
		}
		else if (triggerId == Trigger::NET_FLOW_INSP || 
				 triggerId == Trigger::PRESSURE_BACKUP_INSP ||
				 triggerId == Trigger::NET_FLOW_BACKUP_INSP ||
				 triggerId == Trigger::PRESSURE_INSP)
		{	
			// $[TI6.3]
			// Handle patient triggered breath
			if (rCycleTimer_.getCurrentTime() < syncCycleTimeMs_)
			{
				// $[TI6.3.1]
		        // $[BL04036] during spont. interval, a patient effort ..
				// $[BL04029] duration of the spontaneous interval
				// Handling patient Insp trigger, simply deliver bilevel
				breathType = ::SPONT;
				newPeepState_ = peepState_;
			}
			else
			{
				// $[TI6.3.2]
				// $[BL04037] during sync interval, patient triggered breath
				// forces transition from peep low to peep high
				// $[BL04040] a peep low to peep high transition is
				// considered a mandatory inspiration.
				// Handling patient Insp trigger, simply deliver bilevel
				breathType = ::ASSIST;
				newPeepState_ = Peep::PEEP_HIGH;
			}
		}
		else if (triggerId == Trigger::PEEP_RECOVERY)
		{
			// $[TI6.4]
			// Handling peep recovery trigger, simply deliver bilevel
			breathType = ::SPONT;
			newPeepState_ = peepState_;
		}
		else if (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP)
		{
			breathType = ::CONTROL;
			newPeepState_ = peepState_;
		}
		else
		{
			// $[TI6.5]
			AUX_CLASS_ASSERTION_FAILURE(triggerId);
		}
	}
	else if (peepState_ == Peep::PEEP_HIGH)
	{
		// $[TI7]
		if (triggerId == Trigger::NET_FLOW_INSP || 
			triggerId == Trigger::NET_FLOW_BACKUP_INSP ||
			triggerId == Trigger::PRESSURE_BACKUP_INSP ||
			triggerId == Trigger::PRESSURE_INSP)
		{
			// $[TI7.1]
			// Handling patient Insp trigger, simply deliver bilevel
			breathType = ::SPONT;
			newPeepState_ = peepState_;
		}
		// during high pressures transition to PEEP LOW
		else if (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) 
		{
			breathType = ::CONTROL;
			newPeepState_ = Peep::PEEP_LOW;
		}
		else
		{
			// $[TI7.2]
			AUX_CLASS_ASSERTION_FAILURE(triggerId);
		}
	}
	else
	{
		// $[TI8]
		AUX_CLASS_ASSERTION_FAILURE(peepState_);
	}
	// keep track of high circuit pressures
	if (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP)
	{
		highPressureOccured_ = TRUE;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startInspiration_ 
//
//@ Interface-Description
// The method takes a breath trigger reference, a breath phase pointer, and a
// breath type as arguments. It does not return any value.  When the next phase
// is determined to be inspiration, this method is called to perform all the
// standard procedures required to start inspiration: Any pending mode or rate
// change status is cleared, new settings should be phased in, the scheduler is
// checked to be in sync with the mode setting. a new breath record is created,
// the oxygen mix, the apnea interval, and peep get updated, the breath trigger
// list is changed for inspiration list, and the previous breath relinquishes
// control, For control or assist breath type, the breath phase is set to
// reference the LowToHighPeep breath phase. For spont breath type, if in
// peep recovery state, then peep recovery state is set to active and the
// breath phase is set to reference the PeepRecovery breath phase.  if the 
// peepState_ is PEEP_LOW, then the breath phase is set to reference the 
// Psv breath phase.  if the  peepState_ is PEEP_HIGH, then the breath phase is
// set to reference the HiLevelPsv breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The method creates a new breath record using the object rBreathSet.  The
// actual oxygen mix, apnea interval, and peep are updated using the objects
// RO2Mixture,RApneaInterval and RPeep.  The breath trigger mediator instance
// RBreathTriggerMediator is used to set the inspiratory breath trigger list.
// The breath phase instance pointed to by argument pBreathPhase is
// instructed to phase out.  The static method BreathPhase::SetCurrentBreathPhase
// is used to set up the new breath phase.  If a peep transition is required,
// then setPeepTransition_() shall be invoked else setSyncCycleTime_() and
// enableBilevelTrigger_() shall be invoked to setup the peep low/high timer.
// A new breath record shall be opened.  The breath period shall be set only
// after the breath record is opened.  Finally, we start the a new
// inspiration by calling the breath phase's newBreath() method.
//---------------------------------------------------------------------
//@ PreCondition
// Check ranges for mandatory type and breath type. Check peep recovery flag for
// true in case of a spont breath.
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================

void
BiLevelScheduler::startInspiration_(const BreathTrigger& breathTrigger,
                BreathPhase* pBreathPhase,  BreathType breathType)
{

    CALL_TRACE("BiLevelScheduler::startInspiration_(const BreathTrigger& breathTrigger,\
                BreathPhase* pBreathPhase,  BreathType breathType)");

    // pointer to a breath phase
    BreathPhase* pPhase = NULL;
    // variables to store setting values:
    DiscreteValue mandatoryType = 0;
    // flags peep recovery in process
    Boolean isPeepRecovery = TRUE;
	Boolean isMandBreath = FALSE;

    spontOccuredAtPeepLow_ = FALSE;

	DiscreteValue supportType = 
			PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE);

    // mand type forced to PCV_MAND_TYPE during BiLevel mode
	RVolumeTargetedManager.updateData( MandTypeValue::PCV_MAND_TYPE, supportType) ;

	if (breathType == ::SPONT)
	{
	// $[TI1]
        // deliver a peep recovery phase for peep recovery state = start
        if(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::START)
        {	
			// $[TI1.1]
            BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::ACTIVE;
            isPeepRecovery = TRUE;
            pPhase = (BreathPhase*)&RPeepRecoveryPhase;
        }
        else 
        {	
			// $[TI1.2]
            isPeepRecovery = FALSE;

			if (supportType == SupportTypeValue::PSV_SUPPORT_TYPE || 
				supportType == SupportTypeValue::OFF_SUPPORT_TYPE) 
			{
				// $[TI1.2.1]
				// set the support type for the spontaneous
				if (peepState_ == Peep::PEEP_LOW)
				{
					// $[TI1.2.1.1]
					pPhase = (BreathPhase*)&RPsvPhase;
				}
				else 
				{
					// $[TI1.2.1.2]
					pPhase = (BreathPhase*)&RHiLevelPsvPhase;
				}
			}
			else if (supportType == SupportTypeValue::ATC_SUPPORT_TYPE)
			{
				// $[TI1.2.2]
				pPhase = (BreathPhase*)&RTcvPhase;
			}
			else
			{   // $[TI2.2.3]
				AUX_CLASS_ASSERTION_FAILURE(supportType);
			}
        }
       
        mandatoryType = ::NULL_MANDATORY_TYPE;

        startTimeOfSpontBreath_ = rCycleTimer_.getCurrentTime();
        if (peepState_ == Peep::PEEP_LOW)
        {
			// $[TI1.4]
	        spontOccuredAtPeepLow_ = TRUE;
        }	// $[TI1.5]
	}
	else if (breathType == ::CONTROL || breathType == ::ASSIST)
	{
		// $[TI2]
		isPeepRecovery = FALSE;
        
        // get the mandatory type
        mandatoryType = MandTypeValue::PCV_MAND_TYPE;
        
		isMandBreath = TRUE;
        pPhase = (BreathPhase*)&RLowToHighPeepPhase;
        startTimeOfSpontBreath_ = 0;
	}
    else
    {
	// $[TI3]
        CLASS_PRE_CONDITION( (breathType == ::CONTROL) ||
                             (breathType == ::ASSIST) ||
                             (breathType == ::SPONT) );
    }

	// deliver insp.

    //Reset currently active triggers and setup triggers that are active during inspiration:
    RBreathTriggerMediator.resetTriggerList();

    RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::INSP_LIST);

    // 1. phase-out current breath phase
    pBreathPhase->relinquishControl(breathTrigger);

    // $[04118] $[04122]
    // 2. Once settings are phased in, determine the O2 mix, the
    // apnea interval, and the peep for this phase:
    RO2Mixture.determineO2Mix(*this, (BreathPhase&)*pPhase);
    RApneaInterval.newPhase(BreathPhaseType::INSPIRATION);
	RPeep.updatePeep(BreathPhaseType::INSPIRATION, (Peep::PeepState) peepState_,
					 (Peep::PeepState) newPeepState_);

	if (pPhase == (BreathPhase*)&RPeepRecoveryPhase)
	{	// $[TI6]
	    RPeep.setPeepChange( TRUE) ;
	}	// $[TI7]

    // $[04227] register new phase:
	// 3. register new phase:
    BreathPhase::SetCurrentBreathPhase(*pPhase, breathTrigger);

	// 4. Update the breath record
	determineBiLevelPhase_(breathTrigger, BreathPhaseType::INSPIRATION, isMandBreath);

	peepLowChanged_ = FALSE;
	peepHighChanged_ = FALSE;
	
	if (peepState_ != newPeepState_)
	{
		// $[TI4]
		if (newPeepState_ == Peep::PEEP_HIGH)
		{
			// $[TI4.1]
		    // Reset the time when the peep stays high
			peepHighToLowTransitionTime_ = 0;
		}
		// $[TI4.2]

		setPeepTransition_(breathType);
 	}
 	else
 	{	// $[TI5]
 		setSyncCycleTime_();
	 	enableBilevelTrigger_();
 	}
 	
	// $[TI6] -- implied else

    BreathPhaseScheduler::ModeChange_ = FALSE;    
    BreathPhaseScheduler::RateChange_ = FALSE;

    RBreathSet.newBreathRecord(SchedulerId::BILEVEL, mandatoryType, breathType,
                            BreathPhaseType::INSPIRATION, isPeepRecovery, isMandBreath);

	// phased in respiratory rate setting has to be done after newBreathRecord call
	const Real32 rate =
			PhasedInContextHandle::GetBoundedValue(SettingId::RESP_RATE).value;

	// $[BL04020] bilevel breath cycle 
	Real32 respRateCycleTimeMs = 60.0F * 1000.0F / rate;
	
    BreathRecord::SetBreathPeriod(respRateCycleTimeMs);
    
	// 5. start inspiration
	BreathPhase::NewBreath() ;

	// 6. memorize the current breath type
	currentBreathType_ = breathType;
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: handleExpBreath_
//
//@ Interface-Description
// The method takes a breath trigger Id and breath type as arguments.  It
// does not return any value.  When the next phase is determined to be
// expiration, this method is called to perform all the standard procedures
// required to setup the newPeepState_.
//---------------------------------------------------------------------
//@ Implementation-Description
// When in Peep Low state, then do the following setup.
//		condition							newPeepState_
// 1. DELIVERED_FLOW_EXP
//	  HIGH_CIRCUIT_PRESSURE_EXP
//	  PRESSURE_EXP
//	  BACKUP_TIME_EXP
//	  IMMEDIATE_EXP
//	  HIGH_VENT_PRESSURE_EXP				same peep state
//
// When in Peep High state, then do the following setup.
//		condition					interval	breathType	newPeepState_
// 1. DELIVERED_FLOW_EXP
//	  HIGH_CIRCUIT_PRESSURE_EXP
//	  PRESSURE_EXP
//	  BACKUP_TIME_EXP
//	  HIGH_VENT_PRESSURE_EXP 		spont					same peep state
// 2. DELIVERED_FLOW_EXP
//	  HIGH_CIRCUIT_PRESSURE_EXP
//	  PRESSURE_EXP
//	  BACKUP_TIME_EXP
//	  HIGH_VENT_PRESSURE_EXP 		sync		spont		PEEP_LOW
// 3. DELIVERED_FLOW_EXP
//	  HIGH_CIRCUIT_PRESSURE_EXP
//	  PRESSURE_EXP
//	  BACKUP_TIME_EXP
//	  HIGH_VENT_PRESSURE_EXP 		sync		others		same peep state
// 4. IMMEDIATE_EXP											PEEP_LOW
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BiLevelScheduler::handleExpBreath_(Trigger::TriggerId triggerId,
									::BreathType breathType)
{
	// When curent breath phase is INSPIRATION, get ready for
	// expiration.
	if (peepState_ == Peep::PEEP_LOW)
	{
		// $[TI1]
		if (triggerId == Trigger::DELIVERED_FLOW_EXP ||
            triggerId == Trigger::HIGH_PRESS_COMP_EXP || 
			triggerId == Trigger::LUNG_FLOW_EXP || 
			triggerId == Trigger::LUNG_VOLUME_EXP || 
			triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP ||
			triggerId == Trigger::PRESSURE_EXP ||
			triggerId == Trigger::BACKUP_TIME_EXP ||
			triggerId == Trigger::IMMEDIATE_EXP ||
			triggerId == Trigger::HIGH_VENT_PRESSURE_EXP)
		{
			// $[TI1.1]
			newPeepState_ = peepState_;
		}
		else
		{
			// $[TI1.2]
			AUX_CLASS_ASSERTION_FAILURE(triggerId);
		}
	}
	else if (peepState_ == Peep::PEEP_HIGH)
	{
		// $[TI2]
		if (triggerId == Trigger::DELIVERED_FLOW_EXP ||
            triggerId == Trigger::HIGH_PRESS_COMP_EXP || 
			triggerId == Trigger::LUNG_FLOW_EXP || 
			triggerId == Trigger::LUNG_VOLUME_EXP || 
			triggerId == Trigger::PRESSURE_EXP ||
			triggerId == Trigger::BACKUP_TIME_EXP ||
			triggerId == Trigger::HIGH_VENT_PRESSURE_EXP)
		{
			// $[TI2.1]
			if (rCycleTimer_.getCurrentTime() < syncCycleTimeMs_)
			{	
				// $[BL04027] duration of the spontaneous interval
				// $[BL04032] set peep to peep high when in spont. interval
				// $[TI2.1.1]
				// SPONT interval
				newPeepState_ = peepState_;
			}
			else
			{
				// $[TI2.1.2]
				if (breathType == SPONT)
				{
					// $[TI2.1.2.1]
					// $[BL04033] set peep to peep low when in sync. interval
					// $[BL04034] exhalation transitions from peep high to low
					// $[BL04041] a peep high to peep low transition is
					// considered a mandatory expiration.
					// when time elapsed during peep high exhalation
					// SYNC interval
					newPeepState_ = Peep::PEEP_LOW;
				}
				else
				{
					// $[TI2.1.2.2]
					newPeepState_ = peepState_;
				}
			}
		}
		else if (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP)
		{
			// transition to peep low
			newPeepState_ = Peep::PEEP_LOW;
		}
		else if (triggerId == Trigger::IMMEDIATE_EXP)
		{
			// $[TI2.2]
			// When Thigh time is reached, and patient is in inspiration,
			// force the termination of the inspiration.
			newPeepState_ = Peep::PEEP_LOW;
		}
		else
		{
			// $[TI2.3]
			AUX_CLASS_ASSERTION_FAILURE(triggerId);
		}
	}		
	else
	{
		// $[TI3]
		AUX_CLASS_ASSERTION_FAILURE(peepState_);
	}
	// keep track of high circuit pressures
	if (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP)
	{
		highPressureOccured_ = TRUE;
	}
}	


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startExhalation_
//
//@ Interface-Description
// This method accepts a breathTrigger reference, a BreathPhase pointer,
// and noInspiration as arguments. It does not return any value.
// Activities for start of exhalation are:
// - Update the applied O2%. Update the apnea interval.  Update the peep.
// - Change the active breath trigger list, disable the breath triggers on the
//   current list.
// - Instruct the current breath phase to relinquish control and setup
//   the new breath phase.
// - If peep state changed then call setPeepTransition_() to setup new
//	 peep state and peep cycle timer.
// - If rate change has been requested during inspiration, then invoke
//   adjustIntervalForRateChange_() to ajust breath duration accordingly.
// - If it is a exhalation to exhalation case (indicated by noInspiration flag),
//	 create a new breath record with NULL_INSPIRATION phase type and disable
//	 volume checking for disconnect trigger else update the breath record.
// - Update the current breath record with new data.
//---------------------------------------------------------------------
//@ Implementation-Description
// Reset currently active triggers and setup triggers that are active during
// exhalation.  Turn peep recovery off for peep recovery inspiration.
// The applied O2 percent is updated if necessary, the apnea interval is
// updated for start of exhalation, and the breath phase gets updated.
// If peep state changed then adjust the peep timer according by calling
// setPeepTransition_.  Create a new breath record with NULL_INSPIRATION phase
// type when transition from exh to exh.  Otherwise, update the breath record.
// For details see the Interface-Description section.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BiLevelScheduler::startExhalation_(const BreathTrigger& breathTrigger,
				BreathPhase* pBreathPhase, const Boolean noInspiration)
{
#ifdef INTEGRATION_TEST_ENABLE
    	        // Signal Event for swat
    	        swat::EventManager::signalEvent(swat::EventManager::START_OF_EXH);
#endif // INTEGRATION_TEST_ENABLE

	// Reset currently active triggers and setup triggers that are active during
	// inspiration:
	RBreathTriggerMediator.resetTriggerList();
	RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);
	
	// only enable high pressure trigger if no high pressure trigger have been
	// hit.  Keeps from ping ponging from transitions from peep low to peep high.
	if (highPressureOccured_ == FALSE)
	{
		((Trigger&)RHighCircuitPressureExpTrigger).enable();
	}
	else
	{
		highPressureOccured_ = FALSE;
	}

	pBreathPhase->relinquishControl(breathTrigger);
            
	// determine the O2 mix, the apnea interval, and the peep for this phase:
	RO2Mixture.determineO2Mix(*this, (BreathPhase&)RExhalationPhase);
	RApneaInterval.newPhase(BreathPhaseType::EXHALATION);
	RPeep.updatePeep(BreathPhaseType::EXHALATION, (Peep::PeepState) peepState_,
					(Peep::PeepState) newPeepState_);

	// register new phase:
	BreathPhase::SetCurrentBreathPhase((BreathPhase&)RExhalationPhase, breathTrigger);

	determineBiLevelPhase_(breathTrigger, BreathPhaseType::EXHALATION,
						   (noInspiration) ? FALSE : BreathRecord::GetIsMandBreath(),
						   noInspiration);
	
	if (peepState_ != newPeepState_)
	{
		// $[TI1]
		if (newPeepState_ == Peep::PEEP_LOW)
		{
			// $[TI1.1]
		    // Remember the time when the peep stays high
		    // We need to remember this time when peep transition
		    // occures.  
			peepHighToLowTransitionTime_ = rCycleTimer_.getCurrentTime();
		}
		else
		{
		// $[TI1.2]
	        AUX_CLASS_ASSERTION_FAILURE(newPeepState_);
		}

		setPeepTransition_();
 	}
 	else if (peepLowChanged_ || peepHighChanged_)
 	{
		// $[TI2]
 		adjustIntervalForRateChange_();
 	}
	// $[TI3] -- implied else

	if (noInspiration)
	{
		// $[TI4]
		// Create a new breath record with NULL_INSPIRATION phase type
		// mandatoryType, and breathType
		
    	RBreathSet.newBreathRecord(SchedulerId::BILEVEL, ::NULL_MANDATORY_TYPE,
    							   (RBreathSet.getCurrentBreathRecord())->getBreathType(),
                            	   BreathPhaseType::NULL_INSPIRATION);
		RDisconnectTrigger.enableVolumeCriteria();
	}
	else
	{
		// $[TI5]
		//Update the breath record
		(RBreathSet.getCurrentBreathRecord())->newExhalation();
	} 

	// start exhalation
	BreathPhase::NewBreath() ;
	 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPeepTransition_
//
//@ Interface-Description
// This method accepts a breathType as arguments. It does not return any
// value.  Set the new peep state, and the target BiLevel timer for the
// coming peep state.
//---------------------------------------------------------------------
//@ Implementation-Description
// The phase-in setting context is used to calculate the biLevelPeepCycleTimeMs_
// based on the new peep state. If new peep state is peep high, then
// phasedInPeepHighTimeMs_ shall be used to set the bilevel timer, inform
// the breath record that we are in peep high state, inform the breath phase
// scheduler that LOW_TO_HIGH transition is happening, and clear the
// timeUp flag in BiLevelLowToHighInspTrigger.  If the new peep state is peep
// low then we setup the biLevelPeepCycleTimeMs_ by either calling
// adjustIntervalForRateChange_().  If the peepLowChanged_ is set or setting
// it using phasedInPeepLowTimeMs_.  In addition, we need to inform the
// breath record that we are in peep low state, inform the breath phase
// scheduler that HIGH_TO_LOW transition is happening, and clear the
// timeUp flag in BiLevelHighToLowExpTrigger.  Finally, we need to update
// the sync cycle timer by calling setSyncCycleTime_(), enable the
// appropriated bilivel trigger, update the rCycleTimer_ using
// biLevelPeepCycleTimeMs_ and restart the cycle timer.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BiLevelScheduler::setPeepTransition_(BreathType breathType)
{
    peepState_ = newPeepState_;

	// Peep state transition occured, we need to resync the timer
	if (peepState_ == Peep::PEEP_HIGH)
	{
		// $[TI1]
		// $[BL04023] duration of peep high phase
		biLevelPeepCycleTimeMs_ = phasedInPeepHighTimeMs_;
		firstBreathType_ = breathType;
		if (breathType != NON_MEASURED)
		{
			// $[TI1.1]
			BreathRecord::SetBiLevelFirstBreathType(breathType);
		}
		// $[TI1.2] -- implied else
		BreathRecord::SetIsHighPeep(TRUE);
        BreathPhaseScheduler::BlPeepTransitionState_ = LOW_TO_HIGH;
		RBiLevelLowToHighInspTrigger.timeUpHappened(FALSE);
	}
	else if (peepState_ == Peep::PEEP_LOW)
	{
		// $[TI2]
		if (peepLowChanged_)
		{
			// $[TI2.1]
			// The flag peepLowChanged_ is set only
			// when the peep state is high and peep low time has been
			// shortened (see settingChangeHappened_ for detail).
			adjustIntervalForRateChange_();
		}
		else
		{
			// $[TI2.2]
			// Calculate inspiratory time for peep low using phasedIn context:
			// $[BL04025] duration of peep low phase
			biLevelPeepCycleTimeMs_ = phasedInPeepLowTimeMs_;
		}
		
   	    BreathPhaseScheduler::BlPeepTransitionState_ = HIGH_TO_LOW;
		BreathRecord::SetIsHighPeep(FALSE);
		RBiLevelHighToLowExpTrigger.timeUpHappened(FALSE);
	}
	else
	{
		// $[TI3]
		AUX_CLASS_ASSERTION_FAILURE(peepState_);
	}

	setSyncCycleTime_();
	enableBilevelTrigger_();
	
    // adjust the target BiLevel sync time
   	rCycleTimer_.setTargetTime(biLevelPeepCycleTimeMs_);

    // restart the BiLevel sync timer
	rCycleTimer_.restart();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableBilevelTrigger_
//
//@ Interface-Description
// This method takes no argument and has no return value.  It simply enables
// the appropriated triggers based on the peep state.
//---------------------------------------------------------------------
//@ Implementation-Description
// The BiLevelLowToHighInspTrigger is enabled when in peep low, or the
// BiLevelHighToLowExpTrigger is enabled when in peep high.
//---------------------------------------------------------------------
//@ PreCondition
// n/a
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
BiLevelScheduler::enableBilevelTrigger_(void)
{
    CALL_TRACE("BiLevelScheduler::enableBilevelTrigger_()");

	if (peepState_ == Peep::PEEP_LOW)
	{
		// $[TI1]
		RBiLevelLowToHighInspTrigger.enable();

        // Disable the high to low insp trigger
        RBiLevelHighToLowExpTrigger.disable();
	}
	else if (peepState_ == Peep::PEEP_HIGH)
	{
		// $[TI2]
		RBiLevelHighToLowExpTrigger.enable();

        // Disable the low to high insp trigger
        RBiLevelLowToHighInspTrigger.disable();
	}
	else
	{
        CLASS_ASSERTION_FAILURE();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustIntervalForRateChange_
//
//@ Interface-Description
// This method takes no arguments. It does not return any value.
// Use the pendingPeepHighTimeMs_ or pendingPeepLowTimeMs_ to
// adjust the target time for rCycleTimer_.
//---------------------------------------------------------------------
//@ Implementation-Description
// If current peep state is high and peepHighChanged_ then we shall
// use the pendingPeepHighTimeMs_ as the new target time.  If the
// current peep state is low and peepLowChanged_ then we adjust the
// cycle timer based on the two following conditions:
// If rate changed during peep low for a spont breath, then we shall
// wait for startTimeOfSpontBreath_ + 3.5 * inspiratory time.
// All other cases shall obey the 3.5 * inspiratory time rule.
//---------------------------------------------------------------------
//@ PreCondition
//  	BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::OFF
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BiLevelScheduler::adjustIntervalForRateChange_(void)
{
	CALL_TRACE("BiLevelScheduler::adjustIntervalForRateChange_(void)");

	CLASS_ASSERTION(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::OFF);

	// Get current insp time from breath record.
	Real32 inspTime = BreathRecord::GetInspiratoryTime();

	// the minimum breath cycle time during rate transition:
	Int32 minCycleTime = (Int32)(2.5F * inspTime);
	Int32 newBreathCycle;
		
	// Get current breath type
    BreathType breathType = (RBreathSet.getCurrentBreathRecord())->getBreathType();
    	
	if (peepState_ == Peep::PEEP_HIGH)
	{
	// $[TI1]
		if (peepHighChanged_)
		{
		// $[TI1.1]
			// $[BL04066] If an inspiratory time decrease is detected
			// during the PEEP high interval...
			// Forcing the cycle timer to maintain the pending
			// Peep hight time so we can trigger the
			// BiLevelHighToLowExpTrigger to happen on the next
			// cycle.
			rCycleTimer_.setTargetTime(pendingPeepHighTimeMs_);
			peepHighChanged_ = FALSE;
		}
		// $[TI1.2] -- implied else
	}
	else if (peepState_ == Peep::PEEP_LOW)
	{
	// $[TI2]
		if (peepLowChanged_)
		{
		    //$[BL04067] If an expiratory time decrease is detected during ..
		    //$[BL04044] When a breath timing change is pending, the start time ..
		// $[TI2.1]
			if (breathType == ::SPONT && spontOccuredAtPeepLow_)
			{
			// $[TI2.1.1]
				// When spont breath occured at peep low time
				// then we need to readjust the peep cycle time
				// based on the start of spont breath time plus
				// 3.5 * Ti
				minCycleTime =(Int32) (minCycleTime +(inspTime + startTimeOfSpontBreath_));
			}
			// $[TI2.1.2] -- implied else
			
			// When spont breath occured at peep high time
			// then we only need to readjust the peep cycle
			// time based on 2.5 * Ti.  This is the reason
			// why we don't need to do any adjustment at all.

			minCycleTime = MAX_VALUE(pendingPeepLowTimeMs_, minCycleTime);
			newBreathCycle = MIN_VALUE(phasedInPeepLowTimeMs_, minCycleTime);
			peepLowChanged_ = FALSE;
			rCycleTimer_.setTargetTime(newBreathCycle);
			biLevelPeepCycleTimeMs_ = newBreathCycle;
		}
		// $[TI2.2] -- implied else
	}
	else
	{
	// $[TI3] -- implied else
        AUX_CLASS_ASSERTION_FAILURE(peepState_);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSyncCycleTime_
//
//@ Interface-Description
// This method takes no parameters.  It simply uses biLevelPeepCycleTimeMs_
// to calculate syncCycleTimeMs_.
//---------------------------------------------------------------------
//@ Implementation-Description
// The syncCycleTimeMs_ is calculated based on the formula as defined by
// the requirement.  A minimum sync time will be maintained at least
// 150 ms.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BiLevelScheduler::setSyncCycleTime_(void)
{
	// Peep state transition occured, we need to resync the timer
	if (peepState_ == Peep::PEEP_HIGH)
	{
		// $[TI1]
		// Calculate the transition interval from spont to sync mode.
		// min(0.3 * Ti, 3S)
		// $[BL04028] 
   		syncCycleTimeMs_ = (Int32)(biLevelPeepCycleTimeMs_ * T_HI_INTERVAL_FRACTION);
		syncCycleTimeMs_ = MIN_VALUE(syncCycleTimeMs_,(Int32) T_HI_MIN_INTERVAL);
	}
	else if (peepState_ == Peep::PEEP_LOW)
	{
		// $[TI2]
		// Calculate the transition interval from spont to sync mode.
		// min(0.3 * Te, 7.5S)
		// $[BL04030] 
   		syncCycleTimeMs_ = (Int32)(biLevelPeepCycleTimeMs_ * T_LO_INTERVAL_FRACTION);
		syncCycleTimeMs_ = MIN_VALUE(syncCycleTimeMs_,(Int32) T_LO_MIN_INTERVAL);
	}
	else
	{
		// $[TI3]
		AUX_CLASS_ASSERTION_FAILURE(peepState_);
	}

	syncCycleTimeMs_ = biLevelPeepCycleTimeMs_ - syncCycleTimeMs_;
	// But the syncCycleTimeMs_ should never be < 150 ms
	if (syncCycleTimeMs_ < 150)
	{
		// $[TI4]
		syncCycleTimeMs_ = 150;
	}
	// $[TI5]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBiLevelPhase_
//
//@ Interface-Description
// This method accepts breathTrigger, phase, isFirstMandBreath, and
// noInspiration as arguments. It does not return any value.
// This method informs breath record of various bilevel phases.
//---------------------------------------------------------------------
//@ Implementation-Description
// When in inspiration phase set the bilevel phases as such:
// 		For first mandatory breath set LOW_TO_HIGH_INSP.
//		Else if currently in peep high state
//				if this is the first spont breath set FIRST_HIGH_SPONT_INSP
//				else set NORMAL_INSP_EXH
// 		Else if currently in peep low state set NORMAL_INSP_EXH
// When in expiration phase set the bilevel phases as such:
// 		For first mandatory breath
//			if this is a IMMEDIATE_EXP triggered set NORMAL_INSP_EXH.
//		Else if transition from exh to exh
//					if it is a mandatory breath set NORMAL_INSP_EXH
//					else set HIGH_SPONT_EXH_TO_LOW_EXH
//		Else set NORMAL_INSP_EXH
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BiLevelScheduler::determineBiLevelPhase_(const BreathTrigger& breathTrigger,
										 BreathPhaseType::PhaseType phase,
										 Boolean isFirstMandBreath,
										 Boolean noInspiration)
{
	CALL_TRACE("BiLevelScheduler::determineBiLevelPhase_(BreathPhaseType phase, Boolean isFirstMandBreath)");

    const Trigger::TriggerId triggerId = breathTrigger.getId();
	
	if (phase == BreathPhaseType::INSPIRATION)
	{
		// $[TI1]
		if (isFirstMandBreath)
		{
			// $[TI1.1]
			BreathRecord::SetBiLevelPhase(BreathRecord::LOW_TO_HIGH_INSP);
			highPeepSpontCounter_ = 0;
		}
		else
		{
			// $[TI1.2]
			if (peepState_ == Peep::PEEP_HIGH)
			{
				// $[TI1.2.1]
				++highPeepSpontCounter_;
				if (highPeepSpontCounter_ == 1)
				{
					// $[TI1.2.1.1]
					BreathRecord::SetBiLevelPhase(BreathRecord::FIRST_HIGH_SPONT_INSP);
				}
				else
				{
					// $[TI1.2.1.2]
					BreathRecord::SetBiLevelPhase(BreathRecord::NORMAL_INSP_EXH);
				}
			}
			else if (peepState_ == Peep::PEEP_LOW)
			{
				// $[TI1.2.2]
				BreathRecord::SetBiLevelPhase(BreathRecord::NORMAL_INSP_EXH);
			}
			else
			{
				// $[TI1.2.3]
                AUX_CLASS_ASSERTION_FAILURE(peepState_);
			}
		}
	}
	else
	{
		// $[TI2]
		if (isFirstMandBreath)
		{
			// $[TI2.1]
			if (triggerId == Trigger::IMMEDIATE_EXP)
			{
				// $[TI2.1.1]
				BreathRecord::SetBiLevelPhase(BreathRecord::NORMAL_INSP_EXH);
			}
			else
			{
				// $[TI2.1.2]
				BreathRecord::SetBiLevelPhase(BreathRecord::LOW_TO_HIGH_EXH);
			}
		}
		else
		{
			// $[TI2.2]
			if (noInspiration)
			{
				// $[TI2.2.1]
				if (BreathRecord::GetIsMandBreath())
				{
					// $[TI2.2.1.1]
					BreathRecord::SetBiLevelPhase(BreathRecord::NORMAL_INSP_EXH);
				}
				else
				{
					// $[TI2.2.1.2]
					BreathRecord::SetBiLevelPhase(BreathRecord::HIGH_SPONT_EXH_TO_LOW_EXH);
				}
			}
			else
			{
				// $[TI2.2.2]
				BreathRecord::SetBiLevelPhase(BreathRecord::NORMAL_INSP_EXH);
			}
		}
	}
}
