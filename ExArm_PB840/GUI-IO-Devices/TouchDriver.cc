#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2001, Puritan-Bennett Corporation
//=====================================================================

// =========================== C L A S S     D E S C R I P T I O N ====
//@ Class: TouchDriver - Touch Screen application interface and base class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains the methods that provide the interface between
//  the touch screen and the application. 
//
//  There are two parts to this class, the top-level application 
//  interface and the base class methods.  The application interface 
//  provides delegates the touch data from the GUI  
//  and sends touch event notifications to the application.
//---------------------------------------------------------------------
//@ Rationale
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-IO-Devices/vcssrc/TouchDriver.ccv   25.0.4.0   19 Nov 2013 14:12:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date: 07-Aug-2008     SCR Number: 6439
//  Project:  840S
//  Description:
//  Fixed "drill-down" problem by slowing down touch events that 
//  occur when the user drags his finger across the touch screen.
//  This was implemented by not reporting touch events before a
//  proceeding release event and by slowing down the response to 
//  sequential release/touch events by 30ms.
//
//  Revision: 003  By:  gdc    Date:  07-APR-2008    SCR Number: 6407
//  Project:  TREND2
//  Description:
//  Removed redefinition of countof macro to resolve compiler warning.
//
//  Revision: 002  By:  quf    Date:  18-Dec-2001    DCS Number: 5977
//  Project:  GuiComms
//  Description:
//  Ignore an error report if it has an invalid report length, rather
//	than assert.
//
//  Revision: 001  By:  gdc    Date:  03-Jan-2001    DCS Number: 5493
//  Project:  GuiComms
//  Description:
//    Initial version to coding standards (Integration baseline).
//=====================================================================

#include "Sigma.hh"
#include "TouchDriver.hh"
#include "GuiIo.hh"

//@ Code...

static  const   Int32   NO_TOUCH_   = 0xFFFFFFFF;

Boolean TouchDriver::TouchInputAllowed_ = FALSE;
IpcId TouchDriver::touchQueueId_ = IpcId::USER_ANNUNCIATION_Q;
Boolean TouchDriver::isTouchScreenReleased_ = FALSE;
Int16 TouchDriver::Col_ = 0;
Int16 TouchDriver::Row_ = 0;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  TouchDriver [constructor]
//
//@ Interface-Description
//  Constructs the TouchDriver base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
TouchDriver::TouchDriver(void):
 prevCol_(NO_TOUCH_)
, prevRow_(NO_TOUCH_)
{

	touchDelay_.now();
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~TouchDriver [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
TouchDriver::~TouchDriver(void)
{
	// no code
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  postTouch_
//
//@ Interface-Description
//  Post touch event messages. Takes a UserAnnunciationMsg::TouchAction.
//  Returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Post the specified action and coordinates to the userInputQueue_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
	TouchDriver::postTouch_(UserAnnunciationMsg::TouchAction action)
{
	UserAnnunciationMsg touchMsg;
	MsgQueue  userInputQueue(touchQueueId_);

	touchMsg.qWord = 0;
	touchMsg.touchParts.eventType = UserAnnunciationMsg::INPUT_FROM_TOUCH;
	touchMsg.touchParts.action = action;

	userInputQueue.putMsg((Uint32)touchMsg.qWord);
	//$[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsTouchScreenReleased (static)
//
//@ Interface-Description
// Returns TRUE if the touch screen has been clear 
// for WAIT_BETWEEN_MESSAGES_ (50 ms) or more.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================

Boolean
	TouchDriver::IsTouchScreenReleased(void)
{
	return isTouchScreenReleased_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  handleCoordinateReport_
//
//@ Interface-Description
// Inform the application  of a new (and different)
// set of touch coordinates [col,row]
//---------------------------------------------------------------------
//@ Implementation-Description
// Inform the higher-level application of the touch screen activity by putting
// a message on its queue. 
//
// Throttling of touch screen input is performed to prevent saturating
// the application Q with touch screen messages due to large amounts of
// touch screen activity.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TouchDriver::handleCoordinateReport_(Int32 col, Int32 row, CoordinateEvent event)
{
	UserAnnunciationMsg::TouchAction  action;
	UserAnnunciationMsg::TouchAction  actionRelease;

	//Post TOUCH_RELEASED event follwed by 
	//TOUCH_PRESSED event.
	if ( event == TOUCH_RELEASED)
	{
		if(!isTouchScreenReleased_)
		{
			actionRelease = UserAnnunciationMsg::SCREEN_RELEASE;
			prevCol_ = NO_TOUCH_;
			prevRow_ = NO_TOUCH_;
			TouchInputAllowed_ = FALSE;
			isTouchScreenReleased_ = TRUE;
			postTouch_(actionRelease);
		}
	}
	else if ( event == TOUCH_PRESSED)
	{
		static const Int32 DELAY_TOUCH_MS = 30;

		// ignore "press" events for 30ms following proceeding "press" event
		// and then process "press" event only if proceeding release event detected
		if ( TouchInputAllowed_ &&
			(touchDelay_.getPassedTime() > DELAY_TOUCH_MS) && 
			 (prevCol_ == NO_TOUCH_) && (prevRow_ == NO_TOUCH_ ) )
		{
			action = UserAnnunciationMsg::SCREEN_TOUCH;
			Col_ = col;
			Row_ = row;
			TouchInputAllowed_ = FALSE;
			isTouchScreenReleased_ = FALSE;
			postTouch_(action);
			touchDelay_.now();
		}
		// else do nothing when receiving another touch event within 30ms
		// of the first one or without an intervening release event -
		// this filters multiple touch events from one user action 
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(event);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize (static)
//
//@ Interface-Description
// This initialization function is called before application tasks are
// executed. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Init destination queue selection, init queue flow-control flag to
// enable posting of touch events.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================

void
	TouchDriver::Initialize(void)
{
	TouchInputAllowed_ = TRUE;
	// $[TI1]
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  routeTouchEvents_
//
//@ Interface-Description
//  Sets the queue identifier where subsequent touch events should be 
//  posted.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assert if the specified message queue is not a valid ID or 0 (NULL_ID).
//  The special NULL_ID, a default value for the argument, reestablishes
//  the USER_ANNUNCIATION_Q as the queue to post to.  If a new message 
//  queue is specified, reset the gating flag 'TouchInputAllowed_' to 
//  allow posting of touch events to the new queue. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	TouchDriver::routeTouchEvents_(IpcId queueId)
{
	IpcId oldQueueId = touchQueueId_;

	// trap bogus MsgQueue IDs
	AUX_CLASS_ASSERTION(
					   (queueId >= NULL_ID  &&  queueId < MAX_QID), queueId);

	// special case of NULL_ID re-establishes the initial (GUI-Apps) Q
	if ( queueId == NULL_ID )
	{											  //$[TI1]
		touchQueueId_ = IpcId::USER_ANNUNCIATION_Q;
	}
	else
	{											  //$[TI2]
		touchQueueId_ = queueId;
	}

	// there has been a new message queue established
	if ( touchQueueId_ != oldQueueId )
	{											  //$[TI3]
		// so reset any TouchTask history/gating of prior events
		TouchInputAllowed_ =TRUE;	// posting is not blocked
	}											  //$[TI4]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RouteTouchEvents [static]
//
//@ Interface-Description
//  Calls routeTouchEvents_() of the instantiated TouchDriver.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================

void 
	TouchDriver::RouteTouchEvents(IpcId queueId)
{
	routeTouchEvents_(queueId);
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AckTouchInput
//
//@ Interface-Description
//  Called by GUI-Applications to acknowledge receipt of touch event
//  allowing the TouchDriver to send the next one. This provides a flow
//  control mechanism to prevent touch events filling the GUI-App
//  queue.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TouchDriver::AckTouchInput(void)
{
	TouchInputAllowed_ = TRUE;
} 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
	TouchDriver::SoftFault(const SoftFaultID  softFaultID,
						   const Uint32       lineNumber,
						   const char*        pFileName,
						   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, GUI_IO_DEVICES,
							TOUCH_DRIVER, 
							lineNumber, pFileName, pPredicate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ReadTouch
//
//
//@ Interface-Description
//   This method is called to retrieve the current selected x and y
//   co-ordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//   none
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void TouchDriver::ReadTouch(Int16 &x, Int16 &y)
{
	x = Col_;
	y = Row_;
}

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================
