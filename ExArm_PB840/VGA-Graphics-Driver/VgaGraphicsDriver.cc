#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: VgaGraphicsDriver - Interface class for VGA device functions.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is this subsystem's representative class.  It's main
//  function is to provide a subsystem-wide initialization method, to
//  be called within 'InitializeSubsystems()'.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaGraphicsDriver.ccv   25.0.4.0   19 Nov 2013 14:37:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: gdc   Date:  27-Jan-2011      SCR Number: 6706
//  Project:  XENA2
//  Description:
//   Modified to use CpuDevice::IsXena2Config.
//   
//  Revision: 005  By: mnr   Date:  24-Sep-2010      SCR Number: 6672
//  Project:  XENA2
//  Description:
//   Methods added for supporting two VGA graphics chips based on 
//   hardware configuration.
//
//  Revision: 004  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//  Removed DELTA project single screen support.
//
//  Revision: 003   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//  Implemented single screen compatibility.
//
//  Revision: 002   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//  Optimized VGA library for Neonatal project to use BitBlt and Windows
//
//  Bitmap structures.
//  Revision: 001   By: sah   Date: 01-Aug-1997   DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Initial version to coding standards (Integration baseline).
//
//=====================================================================
#include "VgaGraphicsDriver.hh"

//@ Usage-Classes
#include "VgaDevice.hh"
#include "VgaFontData.hh"
#include "VgaBlink.hh"
#include "VgaDisplayId.hh"
#include "DisplayContext.hh"
#include "VgaDisplayContext.hh"
#include "CpuDevice.hh"

#ifdef SIGMA_GUIPC_CPU  
#include "ExArm.h"
#else
#include "main_e600GUI.h"
#endif
//@ End-Usage


CDC* VgaGraphicsDriver::ptrLowerCDC_ = NULL;
CDC* VgaGraphicsDriver::ptrUpperCDC_ = NULL;
//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize
//
//@ Interface-Description
//  This method is reponsible for ensuring the initialization of all of
//  this subsystem's classes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaGraphicsDriver::Initialize(void)
{
  GetUpperLowerWndCDCptrs();
  // 'VgaDevice' must be initialized first...
  VgaDevice::Initialize();

  VgaFontData::Initialize();
  VgaBlink::Initialize(); 
}   // $[TI1]

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetUpperDisplayContext
//
//@ Interface-Description
//  This method returns a reference to the DisplayContext for the upper
//  screen based on the hardware configuration.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize the static on the first call only. Returns the reference
//  to the static object.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DisplayContext &
VgaGraphicsDriver::GetUpperDisplayContext(void)
{
    return GetLegacyUpperDisplayContext_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetLegacyUpperDisplayContext_
//
//@ Interface-Description
//  Returns a reference to the Upper Screen DisplayContext for the 
//  pre-Xena2 ventilator configuration (ie. the DisplayContext associated
//  with the Chips & Tech part).
//---------------------------------------------------------------------
//@ Implementation-Description
//  Constructs the DisplayContext statically on first call and returns
//  its reference.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DisplayContext &
VgaGraphicsDriver::GetLegacyUpperDisplayContext_(void)
{
  static VgaDisplayContext displayContext( ::UPPER_VGA_DISPLAY );
  if(ptrUpperCDC_ == NULL) 
  {
	  GetUpperLowerWndCDCptrs();
	  displayContext.setCDC(ptrUpperCDC_);
  }
  else  
  {
      displayContext.setCDC(ptrUpperCDC_);
  }
  return displayContext;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetLegacyLowerDisplayContext
//
//@ Interface-Description
//  Returns a reference to the Lower Screen DisplayContext for the 
//  pre-Xena2 ventilator configuration (ie. the DisplayContext associated
//  with the Chips & Tech part).
//---------------------------------------------------------------------
//@ Implementation-Description
//  Constructs the VgaDisplayContext statically on first call and returns
//  its reference.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
DisplayContext &
VgaGraphicsDriver::GetLegacyLowerDisplayContext_(void)
{
  static VgaDisplayContext displayContext( ::LOWER_VGA_DISPLAY );
 
  if(VgaGraphicsDriver::ptrLowerCDC_ == NULL) 
  {
	  GetUpperLowerWndCDCptrs();
	  displayContext.setCDC(VgaGraphicsDriver::ptrLowerCDC_);
  }
  else  
  {
      displayContext.setCDC(VgaGraphicsDriver::ptrLowerCDC_);
  }

  return displayContext;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetLowerDisplayContext
//
//@ Interface-Description
//  Returns a reference to the lower screen DisplayContext appropriate
//  to the hardware configuration.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Constructs the DisplayContext statically on first call and returns
//  its reference.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
DisplayContext &
VgaGraphicsDriver::GetLowerDisplayContext(void)
{
    return GetLegacyLowerDisplayContext_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDisplayContext
//
//@ Interface-Description
//  Returns a reference to either upper or lower DisplayContext based 
//  on the displayId supplied.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls proper methods based on displayId being UPPER_VGA_DISPLAY or not.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DisplayContext &
VgaGraphicsDriver::GetDisplayContext(VgaDisplayId displayId)
{
    if ( UPPER_VGA_DISPLAY == displayId )
    {
        return GetUpperDisplayContext();
    }
    else
    {
        return GetLowerDisplayContext();
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetUpperLowerWndCDCptrs
//
//@ Interface-Description
//  To initialize the CDC pointers.
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  CDC pointers are initialized from the Application object CDC pointers.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void VgaGraphicsDriver::GetUpperLowerWndCDCptrs(void) 
{


#ifdef SIGMA_GUIPC_CPU
		VgaGraphicsDriver::ptrUpperCDC_ = ((CExArmApp *)AfxGetApp())->ptrUpperCDC_;
		VgaGraphicsDriver::ptrLowerCDC_ = ((CExArmApp *)AfxGetApp())->ptrLowerCDC_;
#else
		VgaGraphicsDriver::ptrUpperCDC_ = ((Cmain_e600GUIApp *)AfxGetApp())->ptrUpperCDC_;
		VgaGraphicsDriver::ptrLowerCDC_ = ((Cmain_e600GUIApp *)AfxGetApp())->ptrLowerCDC_;
#endif
}
