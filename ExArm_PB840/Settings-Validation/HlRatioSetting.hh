
#ifndef HlRatioSetting_HH
#define HlRatioSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  HlRatioSetting - Th-to-Tl Ratio Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HlRatioSetting.hhv   25.0.4.0   19 Nov 2013 14:27:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  soft-limit framework was re-designed such that settings with
//         soft limits need only define new 'findSoftMinMaxValues_()'
//         method, therefore all other "soft" methods were removed
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class HlRatioSetting : public BatchBoundedSetting, public SettingObserver
{
  public:
    HlRatioSetting(void); 
    virtual ~HlRatioSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    virtual const BoundStatus*  acceptPrimaryChange(
				    const SettingId::SettingIdType primaryId
						   );

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
				   const DiscreteValue            newValue,
				   const DiscreteValue            currValue);

    // SettingObserver methods...
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;

    virtual void  settingObserverInit(void);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif // defined (SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual void  updateConstraints_(void);

    virtual void  findSoftMinMaxValues_(Real32& rSoftMinValue,
					Real32& rSoftMaxValue) const;

    virtual BoundedValue  calcBasedOnSetting_(
			      const SettingId::SettingIdType basedOnSettingId,
			      const BoundedRange::WarpDir    warpDirection,
			      const BasedOnCategory          basedOnCategory
					     ) const;

  private:
    HlRatioSetting(const HlRatioSetting&);	// not implemented...
    void  operator=(const HlRatioSetting&);	// not implemented...
};


#endif // HlRatioSetting_HH 
