#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: CommTaskAgent - Communcations Task (Network-App) Agent.
//---------------------------------------------------------------------
//@ Interface-Description
//  The CommTaskAgent class is Task Control's interface to the Network-
//  Application task.  It maintains the current communications state
//  based on updates received from the Network-Application task.  
//  The accessor method IsCommunicationsUp returns the current state
//  to the caller.  Upon receiving a communications status update, this
//  class uses the Sys-Init finite state machine to process a 
//  communications up or communications down message.
//---------------------------------------------------------------------
//@ Rationale
//  The CommTaskAgent class encapsulates the interface to the 
//  Network-Applications task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The static function Receive processes communications events from 
//  Network-Application.  IsCommunicationsUp returns the state of 
//  communications with the other processor.  Constructs a
//  CommUpMessage or CommDownMessage which is processed through the
//  Task Control finite state machine. The NetworkUp/NetworkDown 
//  methods execute within the Network-Application task thread at 
//  its priority.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/CommTaskAgent.ccv   25.0.4.0   19 Nov 2013 14:33:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "CommTaskAgent.hh"

//@ Usage-Classes
#include "CommDownMessage.hh"
#include "CommUpMessage.hh"
#include "FsmState.hh"
#include "TaskControlAgent.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

static Boolean   CommunicationsUp_ = FALSE;

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NetworkUp
//
//@ Interface-Description
//  The NetworkUp method is messaged by Network-Application 
//  when communication is established between the GUI and BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the finite state machine to receive and process 
//  the CommUpMessage.  Message is queued to Task Control Queue
//  which serializes access to the finite state machine and shared
//  data.
//  Updates the private CommunicationsUp_ state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


void
CommTaskAgent::NetworkUp(void)
{
    CALL_TRACE("NetworkUp(void)");

    CommunicationsUp_ = TRUE;
    CommUpMessage  * pMessage = new CommUpMessage;
    TaskControlAgent::AddEvent(pMessage);
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NetworkDown
//
//@ Interface-Description
//  The NetworkDown method is messaged by Network-Application 
//  when communication is lost between the GUI and BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the finite state machine to receive and process
//  the CommDownMessage.  Message is queued to Task Control Queue
//  which serializes access to the finite state machine and shared
//  data.
//  Updates the private CommunicationsUp_ state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


void
CommTaskAgent::NetworkDown(void)
{
    CALL_TRACE("NetworkDown(void)");

    CommunicationsUp_ = FALSE;
    CommDownMessage  * pMessage = new CommDownMessage;
    TaskControlAgent::AddEvent(pMessage);
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsCommunicationsUp
//
//@ Interface-Description
//  The IsCommunicationsUp method returns the state of 
//  communications between the GUI and BD.  Returns TRUE when
//  communication is up and returns FALSE when it is down.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the private state IsCommunicationsUp_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


Boolean
CommTaskAgent::IsCommunicationsUp(void)
{
    return CommunicationsUp_;
    // $[TI1]
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
CommTaskAgent::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_COMMTASKAGENT,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================



