#ifndef DependentAugmentation_HH
#define DependentAugmentation_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: DependentAugmentation - 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/DependentAugmentation.hhv   25.0.4.0   19 Nov 2013 13:51:22   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "AlarmAugmentation.hh"
//@ End-Usage


class DependentAugmentation : public AlarmAugmentation 
{
  public:
    DependentAugmentation(const           AlarmAnalysisInformation& rInfo,
                          Alarm&          rAlarm,
                          Alarm&          rDependentAlarm);
    virtual ~DependentAugmentation(void);

    inline Alarm&           getDependentAlarm   (void) const;

    virtual Boolean operator==(const AlarmAugmentation& rAug) const;
    virtual Boolean operator!=(const AlarmAugmentation& rAug) const;

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
  protected:

  private:
    // these methods are purposely declared, but not implemented...
    //DependentAugmentation_(const DependentAugmentation&);     // not implemented...
    //void   operator=      (const DependentAugmentation&);     // not implemented...


    //@ Data-Member:    rDependentAlarm_
    // Refefence to the dependent Alarm in the superior Alarm /
    // DependentAugmentation / dependent Alarm relationship.
    Alarm& rDependentAlarm_;

};


// Inlined methods
#include "DependentAugmentation.in"


#endif // DependentAugmentation_HH 
