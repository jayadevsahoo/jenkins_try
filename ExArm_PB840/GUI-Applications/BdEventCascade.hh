#ifndef BdEventCascade_HH
#define BdEventCascade_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BdEventCascade - A BdEventTarget which stores a list
// of BdEventTargets.  Allows multiple BdEventTargets to
// register for changes to a single setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BdEventCascade.hhv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd		Date:  22-MAY-95	DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "BdEventTarget.hh"

//@ Usage-Classes
#include "GuiAppClassIds.hh"
//@ End-Usage


class BdEventCascade : public BdEventTarget
{
public:
	BdEventCascade(void);
	~BdEventCascade(void);

	// Other BdEventTarget's register with this method.
	void addTarget(BdEventTarget *pTarget);

	// Setting change notices are communicated through here.
	void bdEventHappened(EventData::EventId eventId,
							EventData::EventStatus eventStatus,
							EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT); 
	static void SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	BdEventCascade(const BdEventCascade&);	// not implemented...
	void operator=(const BdEventCascade&);		// not implemented...

	enum
	{
		//@ Constant: BEC_MAX_TARGETS_
		// The maximum number of target pointers that can be stored by this
		// cascade
		BEC_MAX_TARGETS_ = 15
	};

    // @ Data-Member: targets_
    // The array which holds pointers to all of the targets which had
    // been registered for Bd event callback.
    BdEventTarget *targets_[BEC_MAX_TARGETS_];

    //@ Data-Member: numTargets_
    // The number of targets stored in 'targets_[]'.
    Uint numTargets_;
};


#endif // BdEventCascade_HH 
