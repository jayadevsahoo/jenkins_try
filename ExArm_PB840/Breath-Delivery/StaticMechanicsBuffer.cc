#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: StaticMechanicsBuffer - circular buffer for static mechanics
//		storage.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class provides static methods to obtain the pre-allocated buffers for
//	computing static mechanics (time, pressure, time^2, time * pressure,
//	inspFlow, and plateau pressure buffers).  Methods are also provided
//	to obtain the number of elements of the buffers.
//---------------------------------------------------------------------
//@ Rationale
//	To encapsulate the static mechanics buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The buffers are statically created when a "get" method is called.  The
//	number of elements of each buffer is predetermined and can not be changed.
//	 $[BL04055] $[BL04056] $[BL04057]
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/StaticMechanicsBuffer.ccv   10.7   08/17/07 09:43:34   pvcs  
//
//@ Modification-Log
//
//  Revision: 003  By: rhj     Date:  06-May-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added GetNetLeakFlowBuffer() and GetExhWyePressureBuffer()
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  05-Mar-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "StaticMechanicsBuffer.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTimeBuffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the time
//		buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticMechanicsBuffer::GetTimeBuffer( void)
{
	CALL_TRACE("StaticMechanicsBuffer::GetTimeBuffer( void)") ;
	
	// $[TI1]

	static Real32 TimeBuffer[LR_BUFFER_SIZE] ;

	return( &TimeBuffer[0]) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPressureBuffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the pressure
//		buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticMechanicsBuffer::GetPressureBuffer( void)
{
	CALL_TRACE("StaticMechanicsBuffer::GetPressureBuffer( void)") ;
	
	// $[TI1]

	static Real32 PressureBuffer[LR_BUFFER_SIZE] ;

	return( &PressureBuffer[0]) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTime2Buffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the time
//		squared buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticMechanicsBuffer::GetTime2Buffer( void)
{
	CALL_TRACE("StaticMechanicsBuffer::GetTime2Buffer( void)") ;
	
	// $[TI1]

	static Real32 Time2Buffer[LR_BUFFER_SIZE] ;

	return( &Time2Buffer[0]) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTimePressureBuffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the
//		time-pressure buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticMechanicsBuffer::GetTimePressureBuffer( void)
{
	CALL_TRACE("StaticMechanicsBuffer::GetTimePressureBuffer( void)") ;
	
	// $[TI1]

	static Real32 TimePressureBuffer[LR_BUFFER_SIZE] ;

	return( &TimePressureBuffer[0]) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetInspFlowBuffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the
//		insp flow buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticMechanicsBuffer::GetInspFlowBuffer( void)
{
	CALL_TRACE("StaticMechanicsBuffer::GetInspFlowBuffer( void)") ; 	

	// $[TI1]

	static Real32 InspFlowBuffer[INSP_FLOW_BUFFER_SIZE] ;

	return( &InspFlowBuffer[0]) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPlateauPressureBuffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the
//		plateau pressure buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticMechanicsBuffer::GetPlateauPressureBuffer( void)
{
	CALL_TRACE("StaticMechanicsBuffer::GetPlateauPressureBuffer( void)") ;
	
	// $[TI1]

	static Real32 PlateauPressureBuffer[PLATEAU_PRESS_BUFFER_SIZE] ;

	return( &PlateauPressureBuffer[0]) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetExhPressureBuffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the
//		exh. pressure buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticMechanicsBuffer::GetExhPressureBuffer( void)
{
	CALL_TRACE("StaticMechanicsBuffer::GetExhPressureBuffer( void)") ;
	
	// $[TI1]

	static Real32 ExhPressureBuffer[EXH_PRESS_BUFFER_SIZE] ;

	return( &ExhPressureBuffer[0]) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetExhWyePressureBuffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the
//		exh. pressure buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticMechanicsBuffer::GetExhWyePressureBuffer( void)
{
	CALL_TRACE("StaticMechanicsBuffer::GetExhWyePressureBuffer( void)") ;
	
	// $[TI1]

	static Real32 ExhWyePressureBuffer[EXH_WYE_PRESS_BUFFER_SIZE] ;

	return( &ExhWyePressureBuffer[0]) ;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetNetLeakFlowBuffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the
//		exh. pressure buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticMechanicsBuffer::GetNetLeakFlowBuffer( void)
{
	CALL_TRACE("StaticMechanicsBuffer::GetNetLeakFlowBuffer( void)") ;
	
	// $[TI1]

	static Real32 NetLeakFlowBuffer[NET_LEAK_FLOW_BUFFER_SIZE] ;

	return( &NetLeakFlowBuffer[0]) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~StaticMechanicsBuffer()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

StaticMechanicsBuffer::~StaticMechanicsBuffer(void)
{
	CALL_TRACE("StaticMechanicsBuffer::~StaticMechanicsBuffer(void)") ;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
StaticMechanicsBuffer::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, STATICMECHANICSBUFFER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================






