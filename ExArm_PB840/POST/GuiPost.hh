#ifndef GuiPost_HH
#define GuiPost_HH
#ifdef SIGMA_GUI_CPU

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
// Class: GuiPost - GUI CPU Power On Self Test 
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/GuiPost.hhv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 05-MAY-1999  DR Number: 5203
//    Project: 840 ATC
//    Description:
//       Implemented the PrevPostFailureTests to allow entry into 
//		 service mode after a previous POST failure. The PrevPostFailureTests
//		 skips all POST tests except for the service switch test that
//		 provides entry into service mode. POST bypasses the normal test
//		 sequence if a previous POST failure exists since running the 
//		 tests again will result in another POST failure which would not
//		 allow entry into service mode where a better diagnosis of the 
//		 problem can occur.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//
//=====================================================================

#include "Post.hh"

class GuiPost : public Post
{
  public:
    static const PostTestItem * GetTestItems(void);

  private:
    GuiPost();         // not implemented
    ~GuiPost();        // not implemented

    static const PostTestItem TestItems_[];
    static const PostTestItem PrevPostFailureTests_[];
};


#endif  // SIGMA_GUI_CPU
#endif  // GuiPost_HH
