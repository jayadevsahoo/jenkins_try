#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SoftwareOptions - storage for the ventilator software options.
//---------------------------------------------------------------------
//@ Interface-Description
//	Methods to check if an option is available are implemented, to obtain
//	the customer option field data, to set the data members, to determine
//	whether to set the flash serial number drung est, to bypass the verification
//	of serial numbers with flash, if standard development options, is sales
//	data key type, to initialize data key operations hours.
//---------------------------------------------------------------------
//@ Rationale
//	Abstraction of the ventilator software options.
//---------------------------------------------------------------------
//@ Implementation-Description
//	A structure is used to contain the bit fields of the configuration code.
// $[BL00400]
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/SoftwareOptions.ccv   25.0.4.0   19 Nov 2013 13:54:12   pvcs  $
//
//@ Modification-Log  
//
//  Revision: 017   By: gdc    Date: 17-Feb-2009    SCR Number: 6019
//  Project:  840S
//  Description:
//      Modified to increase data key reliability. Corrected reverse
//      logic problem in isUpdateHours and renamed method to
//      isDataKeyHoursUpdateEnabled.
//
//  Revision: 016   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 015  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 014   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 013   By: rhj   Date: 24-Jan-2005   DR Number: 6153
//  Project:  PAV
//  Description:
//      Found a better way to implement DCS number: 6153. 
//      Reverted back to rev 9.1.
//
//  Revision: 012   By: gfu   Date: 06-Mar-2003   DR Number: 6041
//  Project:  PAV
//  Description:
//		Re-instate the PAV option as indicated by the data key.
//
//  Revision: 011   By: erm   Date: 19-Dec-2002   DR Number: 6028 
//  Project:  EMI
//  Description:
//  Changes made to allow 840 to convert a mfg datakey to production datakey.
//  Added the copyConfigurationCode()
//   
//
//  Revision: 010   By: quf   Date: 24-Oct-2001   DR Number:  5493
//  Project:  GUIComms
//  Description:
//  Added SINGLE_SCREEN_OPTION conditional compile in constructor to
//	prevent unnecessary reboot during 1st power-up after download
//	when the "single screen" data key bit is not set.  This reboot
//	is not necessary for the GUIComms project but should be
//	reinstated for the Delta project.
//  Reverted back to default to FALSE in IsOptionEnabled(), otherwise all
//	options other than "single screen" are forced into a non-changeable
//	enabled state when a Manufacturing key is used, and in a Manufacturing
//	key all options are required to be disabled.
//
//  Revision: 009   By: syw   Date: 23-Aug-2000   DR Number:  5753
//  Project:  Delta
//  Description:
//      Handle SINGLE_SCREEN enum.
//
//  Revision: 008   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  modified to (for now) always return 'FALSE' for PAV option,
//         when not built under PAV project
//
//  Revision: 010   By: quf   Date: 24-Oct-2001   DR Number:  5493
//  Project:  GUIComms
//  Description:
//  Added SINGLE_SCREEN_OPTION conditional compile in constructor to
//	prevent unnecessary reboot during 1st power-up after download
//	when the "single screen" data key bit is not set.  This reboot
//	is not necessary for the GUIComms project but should be
//	reinstated for the Delta project.
//  Reverted back to default to FALSE in IsOptionEnabled(), otherwise all
//	options other than "single screen" are forced into a non-changeable
//	enabled state when a Manufacturing key is used, and in a Manufacturing
//	key all options are required to be disabled.
//
//  Revision: 009   By: syw   Date: 23-Aug-2000   DR Number:  5753
//  Project:  Delta
//  Description:
//      Handle SINGLE_SCREEN enum.
//
//  Revision: 008   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  modified to (for now) always return 'FALSE' for PAV option,
//         when not built under PAV project
//
//  Revision: 007   By: sah   Date: 30-Mar-2000   DR Number:  5671
//  Project:  NeoMode
//  Description:
//      Added new method, 'getDataKeyType()', for allowing the distinguishing
//      between data key types.
//
//  Revision: 006   By: sah   Date: 24-Sep-1999   DR Number: 5424
//  Project:  NeoMode
//  Description:
//      Added capability for user-controlled option states.  Also,
//      redesigned with a static, global interface, with enums for
//      the testing of an option state, rather than unique methods.
//
//  Revision: 005 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004  By:  syw    Date: 19-Nov-1998    DR Number: DCS 5218
//       Project:  BiLevel
//       Description:
//          Added isOptionsFieldOkay() method.  Added !optionsFieldOkay_ &&
//			configCode_.keyType != ORIGINAL condition to isSetFlashSerialNumber().
//
//  Revision: 003  By:  syw   Date:  16-Nov-1998    DR Number: 5252
//       Project:  BiLevel
//       Description:
//			Added requirement tracing.
//
//  Revision: 002  By:  syw    Date: 24-Aug-1998    DR Number: DCS 5144
//       Project:  Sigma (840)
//       Description:
//          Implement new data key layout with configuration code and
//			validation code layout.
//
//  Revision: 001  By:  syw    Date: 04-Apr-1998    DR Number: none
//       Project:  Sigma (840)
//       Description:
//             BiLevel initial version.
//
//=====================================================================

#include "SoftwareOptions.hh"

//@ Usage-Classes

#include "DataKey.hh"
#include "NovRamManager.hh"
#include "UserOptions.hh"
#include "InitiateReboot.hh"

//@ End-Usage

//@ Data...

SoftwareOptions  SoftwareOptions::GlobalSoftwareOptions_;


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SoftwareOptions()
//
//@ Interface-Description
//		Default Constructor.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		Initialize the option field to zero (no options enabled).
//--------------------------------------------------------------------- 
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 
SoftwareOptions::SoftwareOptions( void)
{
	CALL_TRACE("SoftwareOptions::SoftwareOptions( void)") ;

	// $[TI1]

	configCode_.customerOptions = 0 ;
	configCode_.developmentOptions = 0 ;
	configCode_.keyType = 0 ;
	configCode_.ventType = 0 ;
	configCode_.reserved = 0 ;

	validationCode_ = 0 ;
	optionsFieldOkay_ = FALSE ;
	dataKeyType_ = DataKey::PRODUCTION ;

#if defined(SIGMA_DEVELOPMENT)
	optionsFieldOkay_ = TRUE ;
#endif // defined(SIGMA_DEVELOPMENT)
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SoftwareOptions()
//
//@ Interface-Description
//		Destructor.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		none
//--------------------------------------------------------------------- 
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 
SoftwareOptions::~SoftwareOptions( void)
{
	CALL_TRACE("SoftwareOptions::~SoftwareOptions( void)") ;	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SoftwareOptions()
//
//@ Interface-Description
//		Copy Constructor.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		copy all data members to the new object.
//--------------------------------------------------------------------- 
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 
SoftwareOptions::SoftwareOptions( const SoftwareOptions& rSoftwareOptions)
{
	CALL_TRACE("SoftwareOptions::SoftwareOptions( const SoftwareOptions& rSoftwareOptions)") ;
	
	// copy 'rSoftwareOptions' into this instance...
	operator=(rSoftwareOptions);

	if (isStandardDevelopmentFunctions())
	{	// $[TI1] -- Development Options' functions are available...
		UserOptions  userOptions;

		NovRamManager::GetUserOptions(userOptions);

		if (userOptions.isInitialized())
		{	// $[TI1.1] -- user has initialized these options...
			// override datakey options with these user-configured options...
			configCode_.customerOptions = userOptions.getOptionBits();
		}	// $[TI1.2]
	}	// $[TI2]

	// update global reference...
	SoftwareOptions::GlobalSoftwareOptions_ = *this;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//		This method has a reference to a SoftwareOptions as an argument
//		and returns nothing.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		Copy all data members to the new object if it is not the same object.
//--------------------------------------------------------------------- 
//@ PreCondition
//		this == &rOptions
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 
void
SoftwareOptions::operator=( const SoftwareOptions& rOptions)
{
	CALL_TRACE("SoftwareOptions::operator=( const SoftwareOptions& rOptions)") ;

	if (this != &rOptions)
	{	// $[TI1]
		configCode_.customerOptions = rOptions.configCode_.customerOptions ;
		configCode_.developmentOptions = rOptions.configCode_.developmentOptions ;
		configCode_.keyType = rOptions.configCode_.keyType ;
		configCode_.ventType = rOptions.configCode_.ventType ;
		configCode_.reserved = rOptions.configCode_.reserved ;

		validationCode_ = rOptions.validationCode_ ;
		optionsFieldOkay_ = rOptions.optionsFieldOkay_ ;
		serialNumber_ = rOptions.serialNumber_ ;
		dataKeyType_ = rOptions.dataKeyType_ ;
	}	// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsOptionEnabled(optionId)
//
//@ Interface-Description
//		Is the option, indicated by 'optionId', currently enabled?
//--------------------------------------------------------------------- 
//@ Implementation-Description
//--------------------------------------------------------------------- 
//@ PreCondition
//		(optionId >= 0  &&  optionId < NUM_OPTIONS) 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

Boolean
SoftwareOptions::IsOptionEnabled(const SoftwareOptions::OptionId optionId)
{
	//TODO: BVP - E600 Until the real code comes in, just assume
	//all options are enabled.

	switch( optionId )
	{
	case NEO_MODE_LOCKOUT:
	case PAV:
	case ATC:
		return FALSE;
		break;
	default:
	return TRUE;
	break;
	}

	
#if 0
	CALL_TRACE("SoftwareOptions::IsOptionEnabled(optionId)") ;
	AUX_CLASS_PRE_CONDITION((optionId >= 0  &&  optionId < NUM_OPTIONS),
							optionId);

	Boolean  isOptionEnabled = FALSE;

	if (GlobalSoftwareOptions_.optionsFieldOkay_  &&
		(GlobalSoftwareOptions_.configCode_.keyType != MANUFACTURING))
	{	// $[TI1]
		const Uint32  OPTION_MASK = (1u << optionId);

		// $[TI1.1] (TRUE)  $[TI1.2] (FALSE)
		isOptionEnabled =
	 ((GlobalSoftwareOptions_.configCode_.customerOptions & OPTION_MASK) != 0);
	}	// implied else $[TI2]

	return(isOptionEnabled);
#endif
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EnableOption(optionId)
//
//@ Interface-Description
//		Enable the option identified by 'optionId'.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		Only to be used by the Development Options Subscreen.
//--------------------------------------------------------------------- 
//@ PreCondition
//		(optionId >= 0  &&  optionId < NUM_OPTIONS)
//---------------------------------------------------------------------
//@ PostCondition 
//      none
//@ End-Method 
//===================================================================== 

void
SoftwareOptions::EnableOption(const SoftwareOptions::OptionId optionId)
{
	CALL_TRACE("SoftwareOptions::EnableOption(optionId)") ;
	AUX_CLASS_PRE_CONDITION((optionId >= 0  &&  optionId < NUM_OPTIONS),
							optionId);

    GlobalSoftwareOptions_.configCode_.customerOptions |= (1u << optionId);
	
	UserOptions  userOptions;

	userOptions.setOptionBits(GlobalSoftwareOptions_.configCode_.customerOptions);

	// store the user's modification to the customer options...
	NovRamManager::StoreUserOptions(userOptions);
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisableOption(optionId)
//
//@ Interface-Description
//		Disable the option identified by 'optionId'.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		Only to be used by the Development Options Subscreen.
//--------------------------------------------------------------------- 
//@ PreCondition
//		(optionId >= 0  &&  optionId < NUM_OPTIONS)
//---------------------------------------------------------------------
//@ PostCondition 
//      none
//@ End-Method 
//===================================================================== 

void
SoftwareOptions::DisableOption(const SoftwareOptions::OptionId optionId)
{
	CALL_TRACE("SoftwareOptions::DisableOption(optionId)") ;
	AUX_CLASS_PRE_CONDITION((optionId >= 0  &&  optionId < NUM_OPTIONS),
							optionId);

    GlobalSoftwareOptions_.configCode_.customerOptions &= ~(1u << optionId);

	UserOptions  userOptions;

	userOptions.setOptionBits(GlobalSoftwareOptions_.configCode_.customerOptions);

	// store the user's modification to the customer options...
	NovRamManager::StoreUserOptions(userOptions);
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: validateData
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called to determine if the option field is okay.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		A validation code is generated.  If this validation code is the same as
//		the data stored in the data key (validationCode_) then the option
//		field is valid.  The keyType must be less than or equal to
//		LAST_VALID_KEY_TYPE to be valid.
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 

void
SoftwareOptions::validateData( void)
{
	CALL_TRACE("SoftwareOptions::validateData( void)") ;

	Uint32 validationCode = generateValidationCode() ;
	
	if (validationCode == validationCode_ && configCode_.keyType <= LAST_VALID_KEY_TYPE)
	{
		// $[TI1]
		optionsFieldOkay_ = TRUE ;
	}
	else
	{
		// $[TI2]
		optionsFieldOkay_ = FALSE ;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isSetFlashSerialNumber
//
//@ Interface-Description
//		This method has no arguments and returns the status of whether to
//		set the serial number stored in the data key to flash.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		Method returns true if the option field is okay and (ENGINEERING
//		or STANDARD).  It also returns true if the option field not okay
//		and key type is ORIGINAL and PRODUCTION (old formattted data key).
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 

Boolean
SoftwareOptions::isSetFlashSerialNumber( void) const
{
	CALL_TRACE("SoftwareOptions::isSetFlashSerialNumber( void)") ;
	
	Boolean rtnValue = FALSE ;

	if (	optionsFieldOkay_ &&
			(configCode_.keyType == ENGINEERING ||
				configCode_.keyType == STANDARD)
		||
			!optionsFieldOkay_ &&
				configCode_.keyType == ORIGINAL &&
				dataKeyType_ == DataKey::PRODUCTION
		||
			!optionsFieldOkay_ && configCode_.keyType != ORIGINAL
			)
	{
		// $[TI1]
		rtnValue = TRUE ;
	}	// implied else $[TI2]

	return( rtnValue) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isVerifySerialNumber
//
//@ Interface-Description
//		This method has no arguments and returns the status of whether the
//		system needs to check if the serial number matches flash.  Method
//		available only on GUI_CPU.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		Method returns true if the option field is okay and (SALES or
//		MANUFACTURING).  It also returns true if the option field not okay
//		and key type is ORIGINAL and DEMO (old formattted data key).
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 

#if defined (SIGMA_GUI_CPU) || defined (SIGMA_DEVELOPMENT)

Boolean
SoftwareOptions::isVerifySerialNumber( void) const
{
	CALL_TRACE("SoftwareOptions::isVerifySerialNumber( void)") ;
		
	Boolean rtnValue = TRUE ;

	if (	optionsFieldOkay_ &&
			(configCode_.keyType == SALES ||
				configCode_.keyType == MANUFACTURING)
		||
			!optionsFieldOkay_ &&
			configCode_.keyType == ORIGINAL &&
			dataKeyType_ == DataKey::DEMO)
	{
		// $[TI1]
		rtnValue = FALSE ;
	}	// implied else $[TI2]
	rtnValue = TRUE ;  //TODO E600 - Hacked-4 -added: Assuming:- Serial number is varified and it matches - for GUI First_Normal_Vent_Scr
	return( rtnValue) ;
}

#endif // defined (SIGMA_GUI_CPU) || defined (SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isDataKeyHoursUpdateEnabled
//
//@ Interface-Description
//  This method has no arguments and returns a Boolean value. It returns
//  TRUE if operational hour updates to the data key are enabled. This 
//  condition is based on the data key type. Method available only on 
//  BD_CPU.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//	Method returns false if the option field is okay and (SALES or
//	MANUFACTURING).  It also returns false if the option field not okay
//	and ORIGINAL and DEMO (old formattted data key).  And also if
//	OPTION_FORMATTED or > LAST_VALID_KEY_TYPE. And also if the option
//	field not okay and (OPTION_FORMATTED or STANDARD or ENGINEERING or
//	SALES or MANUFACTURING.
//  $[BL00400] operational hours functionality enabled for...
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 

#if defined (SIGMA_BD_CPU) || defined (SIGMA_DEVELOPMENT)

Boolean
SoftwareOptions::isDataKeyHoursUpdateEnabled(void) const
{
	Boolean isUpdateEnabled = TRUE ;

	// don't update data key hours if any of the following conditions are met
	if (	(optionsFieldOkay_ && (configCode_.keyType == SALES || configCode_.keyType == MANUFACTURING))
		||
			(!optionsFieldOkay_ && configCode_.keyType == ORIGINAL && dataKeyType_ == DataKey::DEMO )
		||
			(configCode_.keyType == OPTION_FORMATTED)
		||
			(configCode_.keyType > LAST_VALID_KEY_TYPE)
		||
			(!optionsFieldOkay_ && (configCode_.keyType == OPTION_FORMATTED ||
									configCode_.keyType == STANDARD ||
									configCode_.keyType == ENGINEERING ||
									configCode_.keyType == SALES ||
									configCode_.keyType == MANUFACTURING)) )
	{
		isUpdateEnabled = FALSE ;
	}
	
	return isUpdateEnabled;
}

#endif // defined (SIGMA_BD_CPU) || defined (SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isStandardDevelopmentFunctions
//
//@ Interface-Description
//		This method has no arguments and returns the status of whether the
//		system will allow EST Development options.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		Method returns true if the option field is okay and (ENGINEERING or
//		MANUFACTURING).  It also returns true if the option field not okay
//		and ORIGINAL and DEMO (old formattted data key).
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 

Boolean
SoftwareOptions::isStandardDevelopmentFunctions( void) const
{
	//TODO: BVP - E600 Until the real code comes in, just assume
	//we are under "development"
	return TRUE;
#if 0
	CALL_TRACE("SoftwareOptions::isStandardDevelopmentFunctions( void)") ;
	
	Boolean rtnValue = FALSE ;

	if (	optionsFieldOkay_ &&
			(configCode_.keyType == ENGINEERING ||
				configCode_.keyType == MANUFACTURING)
		||
			!optionsFieldOkay_ &&
			configCode_.keyType == ORIGINAL &&
			dataKeyType_ == DataKey::DEMO)
	{
		// $[TI1]
		rtnValue = TRUE ;
	}	// implied else $[TI2]
	
	return( rtnValue) ;
#endif

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isKeyTypeSales
//
//@ Interface-Description
//		This method has no arguments and returns the status of whether the
//		key type is SALES.  Method available only on GUI_CPU.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		Method returns true if the kye type is SALES.
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 

#if defined (SIGMA_GUI_CPU) || defined (SIGMA_DEVELOPMENT)

Boolean
SoftwareOptions::isKeyTypeSales( void) const
{
	CALL_TRACE("SoftwareOptions::isKeyTypeSales( void)") ;
	
	Boolean rtnValue = FALSE ;

	if (configCode_.keyType == SALES)
	{
		// $[TI1]
		rtnValue = TRUE ;
	}	// implied else $[TI2]

	return( rtnValue) ;
}

#endif // defined (SIGMA_GUI_CPU) || defined (SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getDataKeyType()
//
//@ Interface-Description
//		This method has no arguments and returns the enum KeyTypes to
//		indicate the type of data key.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 


SoftwareOptions::KeyTypes
SoftwareOptions::getDataKeyType( void) const
{
		//TODO: BVP - E600 Until the real code comes in, just assume
	//we are under "development"
	return SoftwareOptions::ENGINEERING;
#if 0
	CALL_TRACE("SoftwareOptions::getDataKeyType( void)") ;

	Uint32  keyType;

	if (optionsFieldOkay_)
	{
		// $[TI1]
		keyType = configCode_.keyType;
	}
	else
	{
		// $[TI2]
		if (configCode_.keyType == ORIGINAL &&
				(dataKeyType_ == DataKey::DEMO ||
				 dataKeyType_ == DataKey::PRODUCTION))
		{
			// $[TI2.1]
			keyType = dataKeyType_;
		}
		else
		{
			// $[TI2.2]
			keyType = OPTION_FORMATTED;
		}
	}

	return((SoftwareOptions::KeyTypes)keyType) ;
#endif
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isOptionsFieldOkay()
//
//@ Interface-Description
//		This method has no arguments and returns whether the options filed is
//		okay.  Method available only on GUI_CPU.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		Returns optionsFieldOkay_ data member.
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

#if defined (SIGMA_GUI_CPU) || defined (SIGMA_DEVELOPMENT)

Boolean
SoftwareOptions::isOptionsFieldOkay( void) const
{
	CALL_TRACE("SoftwareOptions::isOptionsFieldOkay( void)") ;

	Boolean rtnValue = optionsFieldOkay_ ;

	if (configCode_.keyType == ORIGINAL &&
			(dataKeyType_ == DataKey::DEMO ||
			 dataKeyType_ == DataKey::PRODUCTION))
	{
		// $[TI1]
		rtnValue = TRUE ;
	}	// implied else $[TI2]

	return( rtnValue) ;
}

#endif // defined (SIGMA_GUI_CPU) || defined (SIGMA_DEVELOPMENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: generateValidationCode
//
//@ Interface-Description
//		This method has no arguments and returns the validation code generated.
//		This method is called to generate a validation code based on the
//		serial number and configuration code data.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		A cumulative sum is computed for each byte of the configCode_.  The
//		value of each byte is squared and is multiplied by a corresponding
//		weight factor.  A cumulative sum is computed for each byte of the
//		serial number string in the same manner.  Both cumulative sums are
//		then added together.  A scaling factor (1 to MAX_FACTOR) is determined
//		using the modulus function.  The validationCode is the product of the
//		total and the scaling factor.
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 
Uint32
SoftwareOptions::generateValidationCode( void) const
{
	CALL_TRACE("SoftwareOptions::generateValidationCode_( void)") ;

	// $[TI1]
	
	Uint32 weightFactor[SERIAL_NO_SIZE] = {1, 2, 3, 5, 7, 11, 13, 17, 19, 23} ;
	
	const char *pString = serialNumber_.getString() ;
	const Uint8 *pByte = (const Uint8 *)&configCode_ ;
	Uint32 configChecksum = 0 ;
	Uint32 snChecksum = 0 ;

	Uint32 ii;
	for (ii=0; ii < 8; ii++)
	{
		configChecksum += *(pByte + ii) * *(pByte + ii) * weightFactor[ii] ;	
	}
	
	for (ii=0; ii < SERIAL_NO_SIZE; ii++)
	{
		snChecksum += *(pString + ii) * *(pString + ii) * weightFactor[ii] ;	
	}

	Uint32 total = snChecksum + configChecksum ;

	const Uint32 MAX_FACTOR = 412 ;
	
	Uint32 factor = (total % MAX_FACTOR) + 1 ;

	Uint32 validationCode = total * factor ;

	return( validationCode) ;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: copyConfigurationCode()
//
//@ Interface-Description
//  This method has one argument ConfigurationCode Reference, it
//  copies the member configCode_ to argument
//--------------------------------------------------------------------- 
//@ Implementation-Description
//		returns nothing
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void 
SoftwareOptions::copyConfigurationCode(ConfigurationCode & configCode)
{
	
	
	configCode.customerOptions     = configCode_.customerOptions;
	configCode.developmentOptions  = configCode_.developmentOptions; 
	configCode.keyType =  configCode_.keyType;
	configCode.ventType    =  configCode_.ventType;
	configCode.reserved    =  configCode_.reserved;
	
	
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SoftwareOptions::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("SoftwareOptions::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, SOFTWAREOPTIONS,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================



