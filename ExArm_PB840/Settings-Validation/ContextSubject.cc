#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N ================
//@ Class:  ContextSubject - ContextSubject Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class -- which is derived off of 'Subject' -- is to be a base
//  class for all classes that can be monitored by 'ContextObserver'
//  instances.  This class defines three methods used within this dynamic
//  monitoring mechanism:  'attachObserver()' and 'detachObserver()' which
//  provide a means for an observer to manage its monitoring of its
//  subjects; and 'notifyAllObservers()' which is called anytime the state
//  of this subject changes, and -- via the observer's "update" methods --
//  notifies all attached observers of the change.
//
//  This class also overrides the protected 'detachFromAllObservers_()'
//  method, which is called when the system is transitioning to and from
//  SST.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has a two-dimensional array of ContextObserver pointers
//  that is used to keep track of which observers are currently monitoring
//  each subject instance.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ContextSubject.ccv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//===================================================================

#include "ContextSubject.hh"

//@ Usage-Classes
#include "ContextObserver.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Public Methods...
//
//====================================================================

//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method: ~ContextSubject()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this ContextSubject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ContextSubject::~ContextSubject(void)
{
  CALL_TRACE("~ContextSubject()");
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  attachObserver(pObserver, changeId)
//
//@ Interface-Description
//  Attach (register) an observer to monitor the change type, given by
//  'changeId', of this context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (changeId < Notification::NUM_CONTEXT_CHANGE_TYPES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextSubject::attachObserver(ContextObserver*                    pObserver,
			       const Notification::ContextChangeId changeId)
{
  CALL_TRACE("attachObserver(pObserver, changeId)");
  AUX_CLASS_PRE_CONDITION((changeId < Notification::NUM_CONTEXT_CHANGE_TYPES),
			  ((getId() << 16) | changeId));

  ContextObserver**  arrObserverPtrs = observerTable_[changeId];

  Uint oIdx;
  for (oIdx = 0u; oIdx < ContextSubject::MAX_OBSERVERS_; oIdx++)
  {  // $[TI1] -- this path is ALWAYS taken...
    if (arrObserverPtrs[oIdx] == NULL)
    {  // $[TI1.1] -- found an unused slot...
      arrObserverPtrs[oIdx] = pObserver;
      break;
    }
    else if (arrObserverPtrs[oIdx] == pObserver)
    {  // $[TI1.2] -- the observer is already attached...
      break;
    }  // $[TI1.3] -- keep looking for an unused slot...
  }

  // make sure an empty slot was found; if not, maybe 'MAX_OBSERVERS_' needs
  // to be increased...
  AUX_CLASS_ASSERTION((oIdx < ContextSubject::MAX_OBSERVERS_),
		      ((getId() << 16) | oIdx));
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  detachObserver(pObserver, changeId)
//
//@ Interface-Description
//  Detach (unregister) an observer that is monitoring a change type,
//  given by 'changeId', of this context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (changeId < Notification::NUM_CONTEXT_CHANGE_TYPES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextSubject::detachObserver(const ContextObserver*              pObserver,
			       const Notification::ContextChangeId changeId)
{
  CALL_TRACE("attachObserver(pObserver, changeId)");
  AUX_CLASS_PRE_CONDITION((changeId < Notification::NUM_CONTEXT_CHANGE_TYPES),
			  ((getId() << 16) | changeId));

  ContextObserver**  arrObserverPtrs = observerTable_[changeId];

  for (Uint oIdx = 0u; oIdx < ContextSubject::MAX_OBSERVERS_; oIdx++)
  {  // $[TI1] -- this path is ALWAYS taken...
    if (arrObserverPtrs[oIdx] == pObserver)
    {  // $[TI1.1] -- found the 'pObserver' slot...
      arrObserverPtrs[oIdx] = NULL;
      break;
    }  // $[TI1.2] -- keep looking for the 'pObserver' slot...
  }
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  notifyAllObservers(qualifierId, changeId, settingId)  [const]
//
//@ Interface-Description
//  Notify all observers that are monitoring this setting's change type
//  that is identified by 'changeId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (changeId == Notification::BATCH_CHANGED  ||
//   changeId == Notification::NON_BATCH_CHANGED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextSubject::notifyAllObservers(
			  const Notification::ChangeQualifier qualifierId,
			  const Notification::ContextChangeId changeId,
			  const SettingId::SettingIdType      settingId
				  ) const
{
  CALL_TRACE("notifyAllObservers(qualifierId, changeId, settingId)");

  ContextObserver::UpdateMethodPtr  updateMethodPtr;

  // get a pointer to the observer method that is to receive the
  // notification...
  switch (changeId)
  {
  case Notification::BATCH_CHANGED :
    updateMethodPtr = &ContextObserver::batchSettingUpdate;
    break;
  case Notification::NON_BATCH_CHANGED :
    updateMethodPtr = &ContextObserver::nonBatchSettingUpdate;
    break;
  case Notification::NUM_CONTEXT_CHANGE_TYPES :
  default :
    AUX_CLASS_ASSERTION_FAILURE((getId() << 16) | changeId);
    break;
  }

  ContextObserver *const *  arrObserverPtrs = observerTable_[changeId];

  for (Uint oIdx = 0u; oIdx < ContextSubject::MAX_OBSERVERS_; oIdx++)
  {  // $[TI1] -- this path is ALWAYS taken...
    if (arrObserverPtrs[oIdx] != NULL)
    {  // $[TI1.1] -- found an observer...
      (arrObserverPtrs[oIdx]->*updateMethodPtr)(qualifierId, this, settingId);
    }  // $[TI1.2]
  }
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  isSettingChanged(settingId)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return a boolean indicating whether the indicated setting has changed
//  from its currently-accepted value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getSettingValue(settingId)  [virtual, const]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return this context's value for the setting indicated by 'settingId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextSubject::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, CONTEXT_SUBJECT,
                          lineNumber, pFileName, pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ContextSubject(settingId)  [Constructor]
//
//@ Interface-Description
//  Create a default setting subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (ContextId::IsId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ContextSubject::ContextSubject(const ContextId::ContextIdType contextId)
			       : CONTEXT_ID_(contextId)
{
  CALL_TRACE("ContextSubject(contextId)");
  AUX_CLASS_PRE_CONDITION((ContextId::IsId(contextId)), contextId);

  initObserverTable_();
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  detachFromAllObservers_()  [virtual, const]
//
//@ Interface-Description
//  Detach this subject from all observers.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextSubject::detachFromAllObservers_(void)
{
  CALL_TRACE("detachFromAllObservers_()");

  for (Uint cIdx = 0u; cIdx < Notification::NUM_CONTEXT_CHANGE_TYPES; cIdx++)
  {  // $[TI1] -- this path is ALWAYS taken...
    for (Uint oIdx = 0u; oIdx < MAX_OBSERVERS_; oIdx++)
    {  // $[TI1.1] -- this path is ALWAYS taken...
      observerTable_[cIdx][oIdx] = NULL;
    }
  }
}



//====================================================================
//
//  Private Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  initObserverTable_()  [virtual, const]
//
//@ Interface-Description
//  Initialize this instance's observer table to all 'NULL' pointers.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextSubject::initObserverTable_(void)
{
  CALL_TRACE("initObserverTable_()");

  for (Uint cIdx = 0u; cIdx < Notification::NUM_CONTEXT_CHANGE_TYPES; cIdx++)
  {
    for (Uint oIdx = 0u; oIdx < MAX_OBSERVERS_; oIdx++)
    {
      observerTable_[cIdx][oIdx] = NULL;
    }
  }
}   // $[TI1]
