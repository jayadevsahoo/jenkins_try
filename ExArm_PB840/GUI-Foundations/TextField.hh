#ifndef TextField_HH
#define TextField_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: TextField - This class defines objects which can appear as character
// strings on the screen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/TextField.hhv   25.0.4.0   19 Nov 2013 14:11:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date:  01-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Implemented positionInContainer for specialized positioning of 
//  text fields.
// 
//  Revision: 010   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 009   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Added access to internal string, via 'operator const char*()'
//      conversion operator.  This was needed to support the display
//      of auxillary title text.
//
//  Revision: 008   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//      Optimized VGA library for Neonatal project to use BitBlt and Windows
//       Bitmap structures.
//
//  Revision: 007   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 006  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 005  By: gdc      Date: 15-Feb-1996  DR Number: 981
//    Project:  Sigma (R8027)
//    Description:
//      Corrected minor syntax errors detected by new compiler.
//
//  Revision: 004  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//      Removed ifndef / endif around include directives
//
//  Revision: 003  By: mpm      Date: 25-Aug-1995  DCR Number: 548
//    Project:  Sigma (R8027)
//    Description:
//      Added the getUnusedVerticalHeight() feature.
//
//  Revision: 002  By: mpm      Date: 02-Aug-1995  DCR Number: 486 
//    Project:  Sigma (R8027)
//    Description:
//      Made destructor virtual as per coding standard requirements.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//      Integration baseline.
//
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "Drawable.hh"

//@ Usage-Classes

#include "Colors.hh"
#include "TextFont.hh"

//@ End-Usage


class TextField : public Drawable
{
  public:

	// Constant: MAX_STRING_LENGTH
	// This is the longest string that you can pass in to the TextField
	enum {MAX_STRING_LENGTH = 200 };

	TextField(const wchar_t* pString);  
	TextField(void);

	virtual ~TextField(void);

	void setText(const wchar_t* string);  
	inline Int32 getUnusedVerticalHeight(void);

	// These are redefined versions of methods in Drawable:
	//  If called for a TextField, an error will be generated.
	//  TextFields handle their own sizing.
	virtual void setHeight(Int height);
	virtual void setWidth(Int width);

	// virtual from Drawable
	virtual void draw(const Area &);
	virtual void positionInContainer(const Gravity gravity);

	// conversion operator...
	inline  operator const wchar_t*(void) const;  

	static void SoftFault(const SoftFaultID softFaultID,
						 const Uint        lineNumber,
						 const char*       pFileName  = NULL, 
						 const char*       pPredicate = NULL);

  protected:

  private:

	// these methods are purposely declared, but not implemented...
	TextField(const TextField&);                   // not implemented...
	void operator=(const TextField&);              // not implemented...

	void updateArea_(void);
	void validateAndRenderTextField_(Boolean setArea);

	void renderText_(const wchar_t buffer[],  
					Int &rBufIndex,
					Int &rX,
					Int &rY,
					Colors::ColorS color,
					Int yOffset,
					Boolean setArea=FALSE, 
					Int size=(Int) TextFont::NO_SIZE,
					TextFont::Style style = TextFont::NORMAL
					);

	void subscriptDrop_(Int &yOffset, Int &size);

	Int getValue_(const wchar_t buffer[], Int &rBufIndex,  
					Int minValue, Int maxValue);

	inline Boolean isDigit_(const wchar_t c);  

	void dumpTextString_(void);

	//@ Data-Member: MAX_SYMBOL_NAME_LENGTH
	// This is the longest symbol name that can be embedded in the text
	static const Int MAX_SYMBOL_NAME_LENGTH;

	//@ Data-Member: MAX_RECURSION_DEPTH
	// This is the greatest depth of recursion allowed in text rendering.
	static const Int MAX_RECURSION_DEPTH;
	
	//@ Data-Member: MAX_VGA_OFFSET
	// This is the greatest offset allowed to be able to derivated from the
	// VGA screen coordinate.
	static const Int MAX_VGA_OFFSET;
	
	//@ Data-Member: string_
	// Contains the string to be rendered.
	wchar_t string_[MAX_STRING_LENGTH+1];   

	//@ Data-Member: unusedVerticalHeight_
	// The number of unused vertical pixels above the top of the text.
	// This gives a count of the "wasted" space from the top of the TextField
	// to the top of the character that is rendered at the highest point
	// in the TextField.
	Int32 unusedVerticalHeight_;

	//@ Data-Member: baselineFontSize_
	// The first font size encountered in the cheap text string used
	// as the baseline font size for vertically positioning the text 
	// within its parent container.
	Int32 baselineFontSize_;

};


// Inlined methods
#include "TextField.in"


#endif // TextField_HH 
