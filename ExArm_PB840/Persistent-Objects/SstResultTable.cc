#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: SstResultTable - SST Result Table.
//---------------------------------------------------------------------
//@ Interface-Description
//  This persistent table contains SST's result information.  When an SST
//  test is run, information (e.g., time of run and ending result) is stored.
//
//  This log is derived from 'TestResultTable', which manages the internal
//  array of log entries.  There are no virtual methods -- THEY ARE NOT
//  ALLOWED!
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  This table, working with 'TestResultTable', manages an internal fixed
//  array of table entries ('ResultTableEntry').  The fixed array is defined
//  in this class, with a pointer to the array stored in this class's base
//  class.  This allows common functionality between the the other result
//  tables to be shared in the common base class.
//
//  The base class is also responsible for protecting against multiple
//  threads, therefore this class should NOT be using any semaphores
//  to protect itself -- the base class already has protection.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/SstResultTable.ccv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Table
//
//  Revision: 004  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Added calling of 'NovRamManager::ReportAuxillaryInfo()' to
//	initialization and verification methods.
//
//  Revision: 003  By:  sah    Date:  18-Jul-1997    DCS Number: 2246
//  Project:  Sigma (R8027)
//  Description:
//	Removed 'overrideFailures()', which is now implemented in base
//	class.
//
//  Revision: 002  By:  sah    Date:  15-Jul-1997    DCS Number: 2175
//  Project:  Sigma (R8027)
//  Description:
//	Added one static method ('IntermediateResultLogged()') for handling
//	the forwarding of this request to the one instance of this class.  Also,
//	simplified the removal of the overall result diagnostic via a new
//	static method of SstDiagLog.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "SstResultTable.hh"

//@ Usage-Classes...
#include "Array_ResultTableEntry.hh"
#include "NonVolatileMemory.hh"
#include "NovRamManager.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SstResultTable()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default diagnostic code log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//  (isEmpty())
//@ End-Method
//=====================================================================

SstResultTable::SstResultTable(void)
	   : TestResultTable(arrEntries_,
			     ::MAX_SST_RESULTS,
			     NovRamUpdateManager::SST_RESULT_TABLE_UPDATE)
{
  CALL_TRACE("SstResultTable()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SstResultTable()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this fault log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

SstResultTable::~SstResultTable(void)
{
  CALL_TRACE("~SstResultTable()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize()  [const]
//
//@ Interface-Description
//  A reset sequence occurred, therefore verify the integrity of this
//  instance.  If this table is determined to be valid, 
//  'NovRamStatus::ITEM_VERIFIED' is returned.  If the internal
//  infrastructure is valid, but some of the result entries are invalid,
//  'NovRamStatus::ITEM_RECOVERED' is returned.  If this table's
//  infrastructure is invalid, the table is fully initialized (from scratch)
//  and 'NovRamStatus::ITEM_INITIALIZED' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use the base class's initialization method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
SstResultTable::initialize(void)
{
  CALL_TRACE("initialize()");

  NovRamStatus::InitializationId  initStatusId;

  // forward to base class's method...
  initStatusId = initTable_(::MAX_SST_RESULTS);

  switch (initStatusId)
  {
  case NovRamStatus::ITEM_INITIALIZED :		// $[TI1]
    // a full initialization of this diagnostic log is needed...
    new (this) SstResultTable();
    break;
  case NovRamStatus::ITEM_RECOVERED :
  case NovRamStatus::ITEM_VERIFIED :		// $[TI2]
    // do nothing...
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(initStatusId);
    break;
  };

  // at this point, the entire log should be valid...
  SAFE_CLASS_POST_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID),
  			    SstResultTable);

  if (initStatusId != NovRamStatus::ITEM_VERIFIED)
  {   // $[TI3]
    NovRamManager::ReportAuxillaryInfo(this, sizeof(SstResultTable),
				       initStatusId);
  }   // $[TI4]

  return(initStatusId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifySelf()  [const]
//
//@ Interface-Description
//  Verify the integrity of this SST Result Table.  This involves
//  verifying the integrity of each of this table's entries.  If this table
//  is determined to be valid, 'NovRamStatus::ITEM_VALID' is returned.  If
//  this table is not valid, 'NovRamStatus::ITEM_INVALID' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID  ||
//   verifySelf() == NovRamStatus::ITEM_INVALID)
//@ End-Method
//=====================================================================

NovRamStatus::VerificationId
SstResultTable::verifySelf(void) const
{
  CALL_TRACE("verifySelf()");

  const NovRamStatus::VerificationId  VERIF_STATUS =
					    verifyTable_(::MAX_SST_RESULTS);

  if (VERIF_STATUS != NovRamStatus::ITEM_VALID)
  {  // $[TI1]
    NovRamManager::ReportAuxillaryInfo(this, sizeof(SstResultTable));
  }  // $[TI2]

  return(VERIF_STATUS);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  repeatingTest(testNumber)
//
//@ Interface-Description
//  Repeat the SST test given by 'testNumber'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SstResultTable::repeatingTest(const Uint testNumber)
{
  CALL_TRACE("repeatingTest(testNumber)");
  CLASS_PRE_CONDITION((testNumber < ::MAX_SST_RESULTS));

  ResultEntryData  entryData;
  
  arrEntries_[testNumber].getResultData(entryData);

  // "back-out" all of the diagnostics that may have been produced by this
  // test from the run that just completed...
  SstDiagLog::BackoutDiagsOfLastRun(entryData.getTestStartSeqNumber());

  startingTest(testNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCurrEntries(rArrEntries)  [const]
//
//@ Interface-Description
//  Initialize the array 'rArrEntries' to contain all of the current
//  result table entries.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

void
SstResultTable::getCurrEntries(
			    AbstractArray(ResultTableEntry)& rArrEntries
			      ) const
{
  CALL_TRACE("getCurrEntries()");
  CLASS_PRE_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID));
  SAFE_CLASS_PRE_CONDITION((rArrEntries.getNumElems() >=
  						::MAX_SST_RESULTS));

  // store all of the current entries from the base class into
  // 'rArrEntries'...
  copyCurrEntries_(rArrEntries);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IntermediateResultLogged(diagCode)  [static]
//
//@ Interface-Description
//  This method is used to forward the fact that an intermediate SST
//  test result has been logged, to the SST result table.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (diagCode.getDiagCodeTypeId() == DiagnosticCode::SHORT_SELF_TEST_DIAG)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SstResultTable::IntermediateResultLogged(const DiagnosticCode& diagCode)
{
  CALL_TRACE("IntermediateResultLogged(diagCode)");
  SAFE_CLASS_PRE_CONDITION((diagCode.getDiagCodeTypeId() ==
				      DiagnosticCode::SHORT_SELF_TEST_DIAG));

  DiagnosticCode::ShortSelfTestBits  sstCode =
					diagCode.getShortSelfTestCode();

  NonVolatileMemory::P_INSTANCE_->sstResultTable_.intermediateResultLogged(
					   sstCode.testNumber,
					   (TestResultId)sstCode.failureLevel
									  );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//				[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
SstResultTable::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, SST_RESULT_TABLE,
                          lineNumber, pFileName, pPredicate);
}
