
#ifndef PendingContextHandle_HH
#define PendingContextHandle_HH

//====================================================================
// This is a  proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: PendingContextHandle - Handle to the Pending Settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PendingContextHandle.hhv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "SettingId.hh"
#include "BoundedValue.hh"
#include "DiscreteValue.hh"

//@ Usage-Classes
#include "SettingCallbackMgr.hh"
//@ End-Usage


class PendingContextHandle
{
  public:
    static Boolean  IsVentStartupPending(void);

    static BoundedValue   GetBoundedValue (
				  const SettingId::SettingIdType boundedId
				  	  );
    static DiscreteValue  GetDiscreteValue(
				  const SettingId::SettingIdType discreteId
				  	  );

#if defined(SIGMA_DEVELOPMENT)
    static void  SetVentStartupFlag(void);

    static void  SetBoundedValue(
			      const SettingId::SettingIdType boundedId,
			      const BoundedValue&            boundedValue
			        );
    static void  SetDiscreteValue(
			      const SettingId::SettingIdType discreteId,
			      DiscreteValue                  discreteValue
				 );
#endif // defined(SIGMA_DEVELOPMENT)

    static void  RegisterSettingEvent(
		      const SettingId::SettingIdType         settingId,
		      SettingCallbackMgr::SettingCallbackPtr pEventCallback
				     );

    static void   SoftFault(const SoftFaultID softFaultID,
			    const Uint32      lineNumber,
			    const char*       pFileName  = NULL, 
			    const char*       pPredicate = NULL);
  
  private:
    PendingContextHandle(const PendingContextHandle&); // not implemented...
    PendingContextHandle(void);		               // not implemented...
    ~PendingContextHandle(void);                       // not implemented...
    void  operator=(const PendingContextHandle&);      // not implemented...
};


#endif // PendingContextHandle_HH 
