#pragma once

#ifdef _WIN32_WCE
#error "CDHtmlDialog is not supported for Windows CE."
#endif 

// CMultiTimeSrptExecutionDlg dialog

class CMultiTimeSrptExecutionDlg : public CDHtmlDialog
{
	DECLARE_DYNCREATE(CMultiTimeSrptExecutionDlg)

public:
	CMultiTimeSrptExecutionDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMultiTimeSrptExecutionDlg();
// Overrides
	HRESULT OnButtonOK(IHTMLElement *pElement);
	HRESULT OnButtonCancel(IHTMLElement *pElement);

// Dialog Data
	enum { IDD = IDD_MULTI_TIMES_SCRT_EXE_DLG, IDH = IDR_HTML_MULTITIMESRPTEXECUTIONDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	DECLARE_DHTML_EVENT_MAP()
};
