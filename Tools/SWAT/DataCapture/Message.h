//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Message.h
//----------------------------------------------------------------------------

#ifndef MESSAGE_H
#define MESSAGE_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"EpochTimeStamp.h"
#include<boost/shared_ptr.hpp>

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class Message
{

   public:
   Message(boost::shared_ptr<char> data, unsigned int iLength);
   Message();  
   ~Message();

   boost::shared_ptr<char>  get();
   void set(boost::shared_ptr<char> data, unsigned int);

   // Time related calls
   EpochTimeStamp &getTimeStamp() ;
   void  setTimeStamp(const EpochTimeStamp &rTimeStamp);
   void  setTimeStamp();
   inline unsigned long getLength()
   {
        return m_iLength;
   }

   private:

   EpochTimeStamp m_time;
   boost::shared_ptr<char> m_raw_data; 
   unsigned long m_iLength;

};

#endif
