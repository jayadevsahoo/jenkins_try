#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: PiStuckTest - BD Safety Net Test for the detection of
//                       inspiratory pressure transducer stuck condition
//                       due to a malfunctioning solenoid
//---------------------------------------------------------------------
//@ Interface-Description
//  The checkCriteria() method of this class is called from the
//  newCycle() method of the SafetyNetTestMediator class.  The
//  checkCriteria() method invokes methods in the Sensor class to obtain 
//  the value of the inspiratory and expiratory pressure sensor readings.
//  It also interfaces with the BreathRecord to determine the phase type
//  of the breath. 
//---------------------------------------------------------------------
//@ Rationale
//  Used to verify that the inspiratory pressure transducer it NOT stuck.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The safety net test is defined in the checkCriteria() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PiStuckTest.ccv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: iv     Date:  18-May-1999    DR Number: 5322
//       Project:  ATC
//       Description:
//          Modified checkCriteria() to detect possible Pi stuck during
//          exhalation.
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//      Project:  ATC
//      Description:
//          Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code when fault is detected.
//
//  Revision: 005  By:  iv    Date: 22-Oct-1997     DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//           Reworked per formal code review.
//
//  Revision: 004  By:  iv    Date: 15-Oct-1997     DR Number: DCS 1649
//       Project:  Sigma (R8027)
//       Description:
//           Revise detection algorithm to eliminate dependency on disconnect.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1780, 1827
//       Project:  Sigma (840)
//       Description:
//            Changed rExhDryFlowSensor.getValue() to rExhFlowSensor.getDryValue().
//
//  Revision: 001  By:  by    Date:  05-Sep-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "PiStuckTest.hh"
#include "MainSensorRefs.hh"

//@ Usage-Classes
#include <math.h>
#include "BreathPhaseScheduler.hh"
#include "BreathRecord.hh"
#include "BreathPhase.hh"
#include "BreathMiscRefs.hh"
#include "PressureSensor.hh"
#include "PressureXducerAutozero.hh"
#include "ExhFlowSensor.hh"
#include "SchedulerId.hh"
#include "NovRamManager.hh"
#include "PhaseRefs.hh"
#include "Peep.hh"

//@ End-Usage

//@ Code...

static const Real32 MIN_PE_PRESSURE = 1.5F;
static const Int8 MAX_NUM_PI_STUCK_BREATHS = 3;

// initialization of static members:
Boolean PiStuckTest::PiStuckDuringExhalation_ = FALSE;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PiStuckTest()  
//
//@ Interface-Description
//  Default Constructor.  Passes two arguments to the base class
//  contructor to define the error code and the time criterion.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The backgndEventId_ and maxCyclesCriteriaMet_ data members are set by the
//  base class constructor.  The background event id is a unique identifier
//  that is placed in the diagnostic log if the background check detects a
//  problem.  The maximum Cycles that the criteria can be met before the
//  Background subsystem is notified is MAX_PRESSURE_STUCK_TEST_CYCLES.  Other
//  data members are initialized here. Data members inspiratoryResistance_ and
//  expiratoryResistance_ are initialzed to each respective limb of resistance
//  calculated during EST.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

PiStuckTest::PiStuckTest( void ) :
SafetyNetTest( BK_PI_STUCK, MAX_PRESSURE_STUCK_TEST_CYCLES )
{

    CALL_TRACE("PiStuckTest(void)");
       
    // $[TI1]    
    piStuckFlag_ = FALSE;
    bkEventReported_ = FALSE;

    NovRamManager::GetInspResistance( inspiratoryResistance_ );
    NovRamManager::GetExhResistance( expiratoryResistance_ );
    detectionDisqualified_ = FALSE;
    numPiStuckBreaths_ = 0;
    phaseType_ = BreathPhaseType::NON_BREATHING;
    PiStuckDuringExhalation_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PiStuckTest() 
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

PiStuckTest::~PiStuckTest(void)
{
    CALL_TRACE("~PiStuckTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCriteria()
//
//@ Interface-Description
//  This method takes no arguments and returns a BkEventName.
//  This method is responsible for the detection of inspiratory
//  pressure transducer stuck to zero (atmospheric pressure) condition.
//  If the background check criteria have been met for the required
//  number of cycles, the backgndEventId for the class is returned;
//  otherwise BK_NO_EVENT is returned to indicate to the caller that
//  the Background subsystem should not be notified.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method first checks the status of the backgndCheckFailed_
//  data member.  If it is TRUE, the test is no longer run since
//  this Background event is not auto-resettable and the Background
//  subsystem only needs to be informed of an event once.  In this
//  case, BK_NO_EVENT is returned to the caller.
//  To avoid false detection of a fault, the test is not run during an
//  autozero maneuver.
//  The test runs only during inspiration or exhalation and never
//  runs during a non breathing mode.  The BreathRecord::GetPhaseType is invoked
//  to determine whether the ventilator is in inspiration or expiration.
//  The method also calls the getValue() methods for the expiratory and inspiratory
//  sensors.
//  The method checks for the start of either exhalation or inspiration. It resets
//  counters and flag for start of inspiration only, as follows:
//  numCyclesCriteriaMet_, PhaseType, detectionDisqualified_.
//
//  The Pi criteria must be met for 4 consecutive breaths before Pi stuck fault
//  can be declared.
//
//  Check during inspiration:
//  -------------------------
//  If the Pi stuck criteria is met, for the fourth consecutive breath,
//  Pi stuck fault is declared.
//
//  Check during exhalation (including expiratory pause):
//  -------------------------
//  A check for Pi stuck is done as follows:
//  if ( (BreathRecord::GetPressWyeExpEst() > RPeep.getActualPeep() + 2.0f) &&
//       (BreathRecord::GetPressWyeInspEst() < -2.0f) )
//     for maxCyclesCriteriaMet_ cycles.
//  If this check evaluates to true, a flag is set to signal the exhalation controller
//  to use the exhalation pressure sensor. No Pi stuck fault is declared.
//
//  The Pi stuck criteria during inspiration is as follows:
//  {  fabs( inspPressure ) < MAX_PRES_SENSOR_STUCK_LIMIT
//     AND
//     exhPressure > MIN_PE_PRESSURE
//  } for maxCyclesCriteriaMet_ cycles.
//
//  The disqualifying criteria is as follows:
//  {  fabs(inspPressure)  > MAX_PRES_SENSOR_STUCK_LIMIT 
//       OR
//       Non breathing mode become active
//  } for one occurence.
//  $[06185]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
PiStuckTest::checkCriteria( void )
{
    CALL_TRACE("PiStuckTest::checkCriteria (void)") ;
    
    BkEventName rtnValue = BK_NO_EVENT;

    /////////////////////////////////////////////////////////
    // Only run the check if it hasn't already failed and
    // if autozero maneuver is in progress
    /////////////////////////////////////////////////////////
    BreathPhaseType::PhaseType phaseType = BreathRecord::GetPhaseType();
    if ( ! backgndCheckFailed_ && ! RPressureXducerAutozero.getIsAutozeroInProgress() )
    {
    // $[TI1]
        Real32 inspPressure = RInspPressureSensor.getValue();
        SchedulerId::SchedulerIdValue schedulerId = (BreathPhaseScheduler::GetCurrentScheduler()).getId();
        if ( BreathPhaseType::INSPIRATION == phaseType && schedulerId != SchedulerId::OCCLUSION )
        {
        // $[TI1.1]
            if ( phaseType_ != BreathPhaseType::INSPIRATION ) 
            {
            // $[TI1.1.1]
                phaseType_ = BreathPhaseType::INSPIRATION;
                numCyclesCriteriaMet_ = 0;
                PiStuckDuringExhalation_ = FALSE;
                detectionDisqualified_ = FALSE;
                if ( piStuckFlag_ )
                {
                // $[TI1.1.1.1]
                    piStuckFlag_ = FALSE;
                    ++numPiStuckBreaths_;
                }
                else
                {
                // $[TI1.1.1.2]
                    numPiStuckBreaths_ = 0;
                }
            } // $[TI1.1.4]
            // check inspiratory pressure transducer stuck criteria
            if ( fabs( inspPressure ) < MAX_PRES_SENSOR_STUCK_LIMIT )
            {
            // $[TI1.1.2]
                Real32 exhPressure = RExhPressureSensor.getValue();
                if ( exhPressure > MIN_PE_PRESSURE && !detectionDisqualified_ )
                {
                // $[TI1.1.2.1]
                    if ( ++numCyclesCriteriaMet_ >= maxCyclesCriteriaMet_ )
                    {
                    // $[TI1.1.2.1.1]
                        if ( numPiStuckBreaths_ >= MAX_NUM_PI_STUCK_BREATHS )
                        {
                        // $[TI1.1.2.1.1.1]
                            errorCode_ = (Uint16)(inspPressure*100.0F);
                            backgndCheckFailed_ = TRUE;
                        }
                        else
                        {
                        // $[TI1.1.2.1.1.2]
                            piStuckFlag_ = TRUE;
                        }
                    }
                    // $[TI1.1.2.1.2]
                }
                else
                {
                // $[TI1.1.2.2]
                    numCyclesCriteriaMet_ = 0;
                }
            }
            else
            {
            // $[TI1.1.3]
                resetCriteria_();
            }
        }
        else if ( ( BreathPhaseType::EXHALATION == phaseType ||
                    BreathPhaseType::EXPIRATORY_PAUSE == phaseType) &&
                    schedulerId != SchedulerId::OCCLUSION )
        {
        // $[TI1.2]
            if ( phaseType_ != phaseType )
            {
            // $[TI1.2.3]
                phaseType_ = phaseType;
                PiStuckDuringExhalation_ = FALSE;
                numCyclesCriteriaMet_ = 0;
            } // $[TI1.2.4]

            if ( fabs( inspPressure ) > MAX_PRES_SENSOR_STUCK_LIMIT )
            {
            // $[TI1.2.1]
                resetCriteria_();
            }
            else
            {
            // $[TI1.2.2]
                if ( (BreathRecord::GetPressWyeExpEst() > RPeep.getActualPeep() + 2.0f) &&
                         (BreathRecord::GetPressWyeInspEst() < -2.0f) )
                {
                // $[TI1.2.2.1]
                    if ( ++numCyclesCriteriaMet_ >= maxCyclesCriteriaMet_ )
                    {
                    // $[TI1.2.2.1.1]
						PiStuckDuringExhalation_ = TRUE;
                    }// $[TI1.2.2.1.2]
                }// $[TI1.2.2.2]
            } 
        }
        else
        {
        // $[TI1.3]
            phaseType_ = BreathPhaseType::NON_BREATHING;
            resetCriteria_();
        }
    }
    // $[TI2]

    // Report Pi stuck conditon to Safety-Net if not already reported
    if ( ( backgndCheckFailed_ ) && ( ! bkEventReported_ ) )
    {
    // $[TI4]
        bkEventReported_ = TRUE;
        rtnValue = backgndEventId_;
    }
    // $[TI5]

    return( rtnValue );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetCriteria_()
//
//@ Interface-Description
//  The method has no arguments and no return value.  It resets the Pi
//  stuck condition by reseting the appropriate flags and counters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Private data memebrs are assigned initial reset values. 
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
PiStuckTest::resetCriteria_( void )
{
	CALL_TRACE("PiStuckTest::resetCriteria_( void )");
    // $[TI1]

    detectionDisqualified_ = TRUE;
    numCyclesCriteriaMet_ = 0;
    piStuckFlag_ = FALSE;
    numPiStuckBreaths_ = 0; 
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
PiStuckTest::SoftFault( const SoftFaultID  softFaultID,
                        const Uint32       lineNumber,
                        const char*        pFileName,
                        const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PISTUCKTEST,
                           lineNumber, pFileName, pPredicate );
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


