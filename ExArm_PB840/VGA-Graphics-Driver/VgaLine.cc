#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: VgaLine - VGA line drawing routines.
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains the line drawing routines for the VGA graphics
//  library.  These include methods for drawing single-pixel, horizontal,
//  vertical and diagonal lines.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  All of these methods do some initial clipping processing, then forward
//  the draw request on to a corresponding assembly-level routine.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaLine.ccv   25.0.4.0   19 Nov 2013 14:37:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 002   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001   By: sah   Date: 09-Jun-1997   DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Initial version to coding standards (Integration baseline).
//
//=====================================================================  
#include "VgaLine.hh" 

//@ Usage-Classes
#include "VgaRectangle.hh"
#include "DisplayContext.hh" 
#include "GraphicsContext.hh" 
//@ End-Usage


//=====================================================================  
//
//  Public Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DrawLine
//
//@ Interface-Description
//  Draw a line on the VGA display, N pixels wide. Uses Bresenham's
//  algorithm if the line is not either horizontal or vertcal. Diagonal
//  lines with width > 1 are drawn as single width lines. Wider 
//  diagonal lines require using trancendental functions which involves
//  too much computational complexity for our system.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Vertical and Horizontal lines are drawn using VgaRectangle::FillBox.
//  Diagonal lines are drawn using Bresenham's line drawing algorithm
//  contained in VgaLine::DrawBresenham_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================  

void
VgaLine::DrawLine( DisplayContext & rDc, GraphicsContext & rGc,
	Int x0, Int y0, Int x1, Int y1 )
{
  // NOTE: lines of width > 1 not supported for non-vertical non-horiz lines
  // defaults to a single width line

  if (x0 == x1)
  {   												// $[TI1.1] vertical line
	VgaRectangle::FillBox( rDc, rGc, x0, y0, x0+rGc.getLineWidth()-1, y1 );
  }
  else if (y0 == y1)
  {   												// $[TI1.2] horizontal line
	VgaRectangle::FillBox( rDc, rGc, x0, y0, x1, y0+rGc.getLineWidth()-1 );
  }
  else
  {   // $[TI1.3] -- neither vertical nor horizontal so do Bresenham's   
    if (rGc.clipLine(&x0, &y0, &x1, &y1))
    {   											// $[TI2.1] inside clip
	  // for width > 1 we just draw a single width line
       const Uint BYTES_PER_ROW = rDc.getWidth();
       const Byte COLOR = rGc.getForegroundColor();
	   RgbQuad rgb = {0,0,0,0};
	   rDc.getColor(COLOR,rgb);
	   COLORREF crWndCol = RGB(rgb.rgbRed,rgb.rgbGreen,rgb.rgbBlue);

	   HPEN hNewPen = ::CreatePen(PS_SOLID,1,crWndCol);
       HPEN hOldPen = (HPEN)::SelectObject(rDc.getCDC()->GetSafeHdc(),hNewPen);

	   rDc.getCDC()->MoveTo(x0,y0);
	   rDc.getCDC()->LineTo(x1,y1);

	   ::SelectObject( rDc.getCDC()->GetSafeHdc(), hOldPen );
	   ::DeleteObject(hNewPen);

    }   											// $[TI2.2] outside clip
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaLine::SoftFault(const SoftFaultID softFaultID,
		   const Uint32      lineNumber,
		   const char*       pFileName,
		   const char*       pPredicate)
{
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS, 
			  VgaGraphicsDriver::VGA_LINE_ID,
  			  lineNumber, pFileName, pPredicate);
}

//=====================================================================  
//
//  Private Methods...
//
//=====================================================================  

//============== M E T H O D                 D E S C R I P T I O N ====
//@ Method: DrawBresenham_ [private]
//
//@ Interface-Description
// Draw a line on the VGA display using Bresenham's algorithm - used only
// by VgaLine::DrawLine() to draw a diagonal line. The specified 
// DisplayContext and GraphicsContext contain the VGA parameters and
// line color.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// Draw a line using Bresenham's algorithm
//
// Bresenhams algorithm:
//  Given the x- and y-coordinates of the first pixel in the line,
//  you calculate the location of each subsequent pixel by incrementing
//  the x- and y-cooridnates in proportion to the line's slope.  The
//  arithmetic is simpler and faster than the arithmetic involved in
//  using the equation of the line.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
VgaLine::DrawBresenham_( DisplayContext & rDc, GraphicsContext & rGc,
	  Int x0, Int y0, Int x1, Int y1 )
{
  Int dx ;                         // x  delta
  Int dy ;                         // y delta
  Int d ;                          // decision variable
  Int i;							// loop counter	
  Int yinc ;                       // computed increment value
  Int incr1 ;                      // computed increment value
  Int incr2 ;                      // computed increment value
  Uint x ;                         // x starting point of line
  Uint y ;                         // y starting point of line


  Byte * pDisplayMem = rDc.getDisplayFramePtr();
  const Uint BYTES_PER_ROW = rDc.getWidth();
  const Byte COLOR = rGc.getForegroundColor();
 
  // length calculation for the clipped endpoints
  dx = ABS_VALUE(x1-x0);		// length in x direction
  dy = ABS_VALUE(y1-y0);		// length in y direction
 
  rDc.accessDisplay();

  if (dx >= dy)     
  {								// $[TI1.1]    (slope < 1)
    if (x0 > x1)    			// determine starting end point
    {							// $[TI2.1]    right end position
      x = x1;       
      y = y1;
	  if (y1 > y0)
	  {							// $[TI3.1]    y will be decreasing
		yinc = -1;  
	  }
	  else
	  {							// $[TI3.2]    y will be increasing
		yinc = 1;   
	  }
    }
    else            			// start at x0
    {							// $[TI2.2]    left end position
      x = x0;
      y = y0;
	  if (y1 > y0)
	  {							// $[TI4.1]    y will be increasing
		yinc = 1;   
	  }
	  else
	  {							// $[TI4.2]    y will be decreasing
		yinc = -1;  
	  }
    }
 
    // calculate the initial decision variable and increments
    incr1 = 2*dy;
    d = incr1 - dx;				// decision variable
    incr2 = 2*(dy-dx);

    yinc *= BYTES_PER_ROW;
 
    //calculate the first pixel address
    pDisplayMem += (y * BYTES_PER_ROW) + x;
    dx++;           // include starting end point in loop counter
 
    for (i = 0; i < dx; i++)
    {							// $[TI5.1]
      *pDisplayMem++ = COLOR;   // write the pixel
      if (d < 0)                // decision - same y?
      {							// $[TI6.1]
        d += incr1 ;
      }
      else          
      {							// $[TI6.2]     next y, and reset d
        d += incr2 ;
        pDisplayMem += yinc ;
      }
    }							// $[TI5.2]    end for

  }                 			// end if (slope < 1)
  else
  {								// $[TI1.2]    (slope > 1), horizontal line was handled above.
    incr1 = 2*dx;
    d = incr1 - dy;				// decision variable
    incr2 = 2*(dx - dy);
    dy++;						// include starting point in loop count
 
    if (y0 > y1)				// determine the starting end point
	{							// $[TI7.1]
      pDisplayMem += (y1 * BYTES_PER_ROW) + x1;
	}
    else
	{							// $[TI7.2]
      pDisplayMem += (y0 * BYTES_PER_ROW) + x0;
	}
 
    if ( ((y0 > y1) && (x1 < x0)) || ((y0 < y1) && (x1 > x0)) )
    {							// $[TI8.1]     x incremented in positive direction
      for (i = 0; i < dy; i++)
      {							// $[TI9.1]
        *pDisplayMem = COLOR;	// write the pixel
 
        // adjust for next pixel
        pDisplayMem += BYTES_PER_ROW ;    // move to next row
        if ( d < 0 )                
        {						// $[TI10.1]           // decision - same x?
          d += incr1;
        }
        else                                
        {						// $[TI10.2]     next x, reset d
          d += incr2;
          pDisplayMem++ ;		// move to next byte on the row
        }
      } 						// $[TI9.2]     end for
    }
    else						// x incremented in negative direction
    {							// $[TI8.2]
      for (i = 0; i < dy; i++)
      {							// $[TI11.1]
        // indicate pixel to write and write to it
        *pDisplayMem = COLOR;	// write the pixel
 
        // adjust for next pixel
        pDisplayMem += BYTES_PER_ROW ;    // move to next row
        if (d < 0)				// decision - same x?
        {						// $[TI12.1]
          d += incr1 ;
        }
        else					// next x, reset d
        {						// $[TI12.2]
          d += incr2 ;
		  pDisplayMem--;
        }
      }							// $[TI11.2]  end for
    }							// end else
  }								// end else (slope > 1)

}

