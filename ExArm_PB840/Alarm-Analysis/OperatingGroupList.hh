#ifndef OperatingGroupList_HH
#define OperatingGroupList_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: OperatingGroupList - derived from template class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/OperatingGroupList.hhv   25.0.4.0   19 Nov 2013 13:51:24   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "SListR_OperatingGroup_NUM_TLGROUP.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
class OperatingGroup;
//@ End-Usage


class OperatingGroupList : public FixedSListR(OperatingGroup,NUM_TLGROUP) 

{
  public:

    OperatingGroupList(void);
    OperatingGroupList(const OperatingGroupList& rList);
    ~OperatingGroupList(void);

    inline void   operator=(const OperatingGroupList& rList);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
  protected:

  private:

};


// Inlined methods
#include "OperatingGroupList.in"


#endif // OperatingGroupList_HH 
