#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2005, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NumericLimitSettingButton - A setting button which displays a
// title (along with possible units text) and the value of a bounded setting in
// a variable precision numeric field.  Most setting buttons fall into this
// category.
//---------------------------------------------------------------------
//@ Interface-Description The Numeric Limit Setting button is derived
// from the Setting Button class. It is used to adjust the associated
// bounded (numeric real) setting and allow it to be switched OFF if
// a limit is reached. The button consists of a title which
// identifies the setting (usually a symbol along with the symbol for
// the units in which the setting is measured) and the floating point
// setting value itself, positioned in the lower half of the button. It
// also contains a text field to display the "OFF" text when the
// setting is adjusted to an upper or lower limit OFF setting. Current
// setting values are displayed in normal text, adjusted values in
// italic text.
//
// Once created, the Numeric Setting Button normally operates without
// intervention except for needing an activate() call before being displayed.
// However, there are two special methods provided for the Safety Ventilation
// subscreen.  These are setValue() and setPrecision() which allow external
// control of the numeric display of this button.  This is needed because the
// Safety Ventilation subscreen needs to display setting buttons that have
// special values that are not taken straight from the Accepted or Adjusted
// contexts of Settings-Validation subsystem.  Also, the setItalic() member is
// provided for use by the derived TimingSettingButton class (which also uses
// setValue() and setPrecision()).
//
// The updateDisplay_() is called via SettingButton when the button must update
// its displayed setting value.
//---------------------------------------------------------------------
//@ Rationale
// Implements the functionality for adjusting a floating point setting 
// and turning off the setting.
//---------------------------------------------------------------------
//@ Implementation-Description
// The construction parameters set up practically everything for the Numeric
// Setting button: its position, size, color, title, the size and position of
// the numeric value within the button, the enum id of the setting which it
// displays, the subscreen which it may activate, a help message to display
// when it is pressed down and whether it is a main setting button or not.
//
// After construction, most of the operation of this setting button is handled
// by the inherited SettingButton class.  All NumericLimitSettingButton really needs
// to do is to react to changes of the setting which it controls and update the
// numeric value display accordingly.  When the setting changes the
// updateDisplay_() method will be called.  This method handles the updating of
// the numeric value.  Note that setValue(), setPrecision() and setItalic()
// provide a "back door" method for controlling the numeric value display.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/NumericLimitSettingButton.ccv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 003   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 002   By: rhj    Date:  10-May-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//      RESPM related changes.
//
//  Revision: 001   By: gdc    Date:  15-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - NIV1
//      NIV1 project initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Colors.hh"
#include "NumericLimitSettingButton.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "SettingConstants.hh"
#include "SettingSubject.hh"
// TODO E600 remove
//#include "Ostream.hh"
//@ End-Usage

//@ Code...

// number of pixels to position "OFF" text field from bottom of container
static const Int BOTTOM_OFFSET = 2;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NumericLimitSettingButton()  [Constructor]
//
//@ Interface-Description
// Creates a Numeric Setting Button. Passed the following arguments:
// >Von
//	xOrg			The X coordinate of the button.
//	yOrg			The Y coordinate of the button.
//	width			Width of the button.
//	height			Height of the button.
//	buttonType		Type of the button, one of Button::(LIGHT, LIGHT_NON_LIT,
//					DARK).
//	bevelSize		Width of the button's internal border.
//	cornerType		Tells whether its corners are rounded or not, can be
//					Button::(ROUND or SQUARE).
//	borderType		The border type (whether it has an external border or not).
//	numberSize		The size of the numeric field, one from
//					TextFont::(SIX, EIGHT, TEN, TWELVE, FOURTEEN, EIGHTEEN,
//					TWENTY_FOUR).
//	valueCenterX	The X coordinate around which the the numeric value field
//					will be centered within the button.
//	valueCenterY	The Y coordinate around which the the numeric value field
//					will be centered within the button.
//	titleText		The string displayed as the title of the button's numeric
//					value.
//	settingId		The setting whose value the button displays and modifies.
//	pFocusSubScreen	The subscreen which receives Accept key events from the
//					button, normally the subscreen which contains the button.
//	messageId		The help message displayed in the Message Area when this
//					button is pressed down.
//	isMainSetting	Flag which specifies whether this setting button is a
//					"Main" setting.  Main setting buttons have slightly
//					different functionality than normal setting buttons.
//>Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Most of the passed-in parameters are passed straight on to the inherited
// SettingButton.  The main work done is creating and positioning the numeric
// value field which is used to display the setting.  We are passed the center
// point of where this field should be so we calculate the position and size of
// this field, assuming it takes up all the space in the bottom left part of
// the button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NumericLimitSettingButton::NumericLimitSettingButton(Uint16 xOrg, Uint16 yOrg,
					Uint16 width, Uint16 height, Button::ButtonType buttonType,
					Uint8 bevelSize, Button::CornerType cornerType,
					Button::BorderType borderType,
					TextFont::Size numberSize,
					Uint16 valueCenterX, Uint16 valueCenterY,
					StringId titleText, StringId unitsText,
					SettingId::SettingIdType settingId,
					SubScreen *pFocusSubScreen, StringId messageId,
					Boolean isMainSetting, StringId auxTitleText,
					Gravity  auxTitlePos) :

	SettingButton(xOrg, yOrg, width, height, buttonType, bevelSize,
				  cornerType, borderType, titleText,
				  settingId, pFocusSubScreen, messageId, 
				  isMainSetting, auxTitleText, auxTitlePos),
	numericValue_(numberSize, 0, CENTER),
	// Set number of digits to 0 above as we will
	// be sizing the numeric field ourselves rather
	// than letting GUI-Foundation setting it to
	// a default value.
	unitsText_(unitsText),
	numberSize_(numberSize),
	isTimingResolutionEnabled_(FALSE)
{
	// Add and position numeric value field to button's label container
	addLabel(&numericValue_);
  	Container* pParent = numericValue_.getParentContainer();
	numericValue_.setX(0);
	numericValue_.setY(2 * valueCenterY - pParent->getHeight());
	numericValue_.setWidth(2 * valueCenterX);
	numericValue_.setHeight(2 * (pParent->getHeight() - valueCenterY));

	addLabel(&unitsText_);

	// create the "OFF" text field and temporarily add it to position it
	// then remove it until it's needed later
	addLabel(&offText_);
	swprintf(offString_, MiscStrs::SETTING_LIMIT_OFF, 
				numberSize_, numberSize_, 'N');
	offText_.setText(offString_);

	offText_.positionInContainer(GRAVITY_CENTER);
	offText_.setY(pParent->getHeight() - offText_.getHeight() - BOTTOM_OFFSET);
	removeLabel(&offText_);

    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NumericLimitSettingButton  [Destructor]
//
//@ Interface-Description
// Destroys a NumericLimitSettingButton.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NumericLimitSettingButton::~NumericLimitSettingButton(void)
{
	CALL_TRACE("NumericLimitSettingButton::~NumericLimitSettingButton(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValue
//
//@ Interface-Description
// Passed a floating point value and precision which becomes the new value
// displayed in the setting button.  Bypasses the method of retrieving the
// value from the Settings-Validation subsystem in updateDisplay_().
//---------------------------------------------------------------------
//@ Implementation-Description
//  Just calls through to the corresponding 'numericValue_' method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
NumericLimitSettingButton::setValue(Real32 value, Precision precision)
{
	CALL_TRACE("NumericLimitSettingButton::setValue(Real32 value, "
												"Precision precision)");

	numericValue_.setValue(value);
	numericValue_.setPrecision(precision);				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setUnitsText(StringId unitsTitle)
//
//@ Interface-Description
//  Passed a StringId which becomes the new unit text displayed in the 
//  setting button.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Checks if the string is not empty before it calls the setText method 
//  to change the UnitsText_ value.  
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void 
NumericLimitSettingButton::setUnitsText(StringId unitsTitle)
{
    CALL_TRACE("NumericSettingButton::setUnitsText(StringId unitsTitle)");

    if (unitsTitle != ::NULL_STRING_ID)
    {   
        unitsText_.setText(unitsTitle);
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTimingResolutionEnabled
//
//@ Interface-Description
// Enables or disables timing resolution formatting for the numeric 
// field of this setting button. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the private data isTimingResolutionEnabled_ to the value of 
// the boolean parameter.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
NumericLimitSettingButton::setTimingResolutionEnabled(Boolean isEnabled)
{
	isTimingResolutionEnabled_ = isEnabled;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [Protected, virtual]
//
//@ Interface-Description
// Called when the display of this setting button needs to be updated with
// the latest setting value.  Passed a setting context id informing us the
// setting context from which we should retrieve the new setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
// Retrieve the latest setting value from the Settings-Validation subsystem
// and display it.
//---------------------------------------------------------------------
//@ PreCondition
// qualifierId must be a valid value.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
NumericLimitSettingButton::updateDisplay_(
	Notification::ChangeQualifier qualifierId,
	const SettingSubject*         pSubject)
{
	CALL_TRACE("updateDisplay_(qualifierId, pSubject)");

	// Get the current value and precision from the setting and set
	// the button's numeric value accordingly
	BoundedValue settingValue;

	if (qualifierId == Notification::ACCEPTED)
	{                                                   // $[TI1.1]
		settingValue = pSubject->getAcceptedValue();
	}
	else if (qualifierId == Notification::ADJUSTED)
	{                                                   // $[TI1.2]
		settingValue = pSubject->getAdjustedValue();
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(qualifierId);
	}
	
	// Check whether the setting has been switched off
	if (settingValue.value == SettingConstants::LOWER_ALARM_LIMIT_OFF ||
		settingValue.value == SettingConstants::UPPER_ALARM_LIMIT_OFF)
	{                                                   // $[TI2.1]
		// Setting limit switched off so make sure numeric value 
		// and units text are not displayed and add the OFF string.
		removeLabel(&numericValue_);
		removeLabel(&unitsText_);

		addLabel(&offText_);

		// Display text to in italics or not depending on whether 
		// the setting has changed or not
		if (qualifierId == Notification::ADJUSTED  &&  pSubject->isChanged())
		{                                               // $[TI3.1]
			swprintf(offString_, MiscStrs::SETTING_LIMIT_OFF, 
						numberSize_, numberSize_, 'I');
			offText_.setText(offString_);
			offText_.setHighlighted(TRUE);
		}
		else
		{                                               // $[TI3.2]
			swprintf(offString_, MiscStrs::SETTING_LIMIT_OFF, 
						numberSize_, numberSize_, 'N');
			offText_.setText(offString_);
			offText_.setHighlighted(FALSE);
		}

		// Add OFF string
		addLabel(&offText_);
	}
	else
	{                                                   // $[TI2.2]
		// Setting limit enabled so make sure OFF value is not displayed and add
		// the numeric value field.
		removeLabel(&offText_);

		// Set the value and precision of the field
		if (isTimingResolutionEnabled_)
		{
			// Decide which precision to display the seconds value in.  We simply
			// scale the stored precision down by 1000.
			Precision newPrecision;

			switch (settingValue.precision)
			{
			case THOUSANDS:										// $[TI1]
				newPrecision = ONES;
				break;

			case HUNDREDS:										// $[TI2]
				newPrecision = TENTHS;
				break;

			case TENS:											// $[TI3]
				newPrecision = HUNDREDTHS;
				break;

			default:						// All the rest default to THOUSANDTHS
				newPrecision = THOUSANDTHS;						// $[TI4]
				break;
			}

			// Set the timing value to be the stored value (in mSecs) divided by
			// 1000 (secs) along with the new precision
			numericValue_.setValue(settingValue.value / 1000);
			numericValue_.setPrecision(newPrecision);
		}
		else
		{
			numericValue_.setValue(settingValue.value);
			numericValue_.setPrecision(settingValue.precision);
		}

		// If it's an Adjusted setting check to see if it's changed and set
		// the value to italic if so.
		if (qualifierId == Notification::ADJUSTED  &&  pSubject->isChanged())
		{                                               // $[TI4.1]
			numericValue_.setItalic(TRUE);
			numericValue_.setHighlighted(TRUE);
		}
		else
		{                                               // $[TI4.2]
			numericValue_.setItalic(FALSE);
			numericValue_.setHighlighted(FALSE);
		}

		// Add numeric value and units text
		addLabel(&numericValue_);
		addLabel(&unitsText_);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
NumericLimitSettingButton::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, NUMERICLIMITSETTINGBUTTON,
									lineNumber, pFileName, pPredicate);
}
