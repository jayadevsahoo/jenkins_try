#ifndef BATCHBUILDER_H
#define BATCHBUILDER_H

#include<vector>
#include"BatchCommand.h"
#include"BatchCommandInfo.h"
#include"NetworkMessageHeader.h"

//namespace swat{

    class BatchBuilder{
        private:


            std::vector<BatchCommand> m_BatchCommands;

            char *m_pData;

            int m_cnt;
            int m_iMaxCnt;

            static  char m_clEnableSwatNetworkMessageHeader [sizeof(CommandInfo)+sizeof(NetworkMessageHeader)];
            static  char m_clDisableSwatNetworkMessageHeader[sizeof(CommandInfo)+sizeof(NetworkMessageHeader)];


        public:

            BatchBuilder();
            ~BatchBuilder();

            //Stack state management
            void mark();
            void mark(unsigned int iEventId);


            //Index management
            void setindex   (int iIndex);
            void skipindex  (int iIndex);

            void repeatn    (unsigned int iCount);
            void repeattime (unsigned int iTime );
            void waittime   (unsigned int iTime );

            //Event
            void repeatevent(unsigned int iEventId, unsigned int iTimeExpire=0);
            void waitevent  (unsigned int iEventId, unsigned int iTimeExpire=0);
            void signalevent(unsigned int iEventId);

            //Sensor
            void setsensor  (unsigned int iSensorId);
            void setsensor  (unsigned int isensorid, float fvalue);
            void addsensor  (unsigned int isensorid, float fvalue);
            void unsetsensor(unsigned int iSensorId);

			//Raw Sensor
            void setrawsensor  (unsigned int isensorid, unsigned int uiValue);
            void unsetrawsensor(unsigned int iSensorId);
            void addrawsensor(unsigned int iSensorId, unsigned int uiValue);

            //simulate GuiTouch / BezelKey
            void simulateguitouch(unsigned int x, unsigned int y);
            void simulatebezelkey(unsigned int keyId, unsigned int onOff);

            void end();
            void resetall();

            int getLength();


            char *getBatch();
            char *generateSwatEnable();
            char *generateSwatDisable();





	

    };


//}



#endif 
