
#ifndef ExpTimeSetting_HH
#define ExpTimeSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  ExpTimeSetting - Expiratory Time Setting 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ExpTimeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: sah     Date:  18-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added new 'calcDependentValues_()' methods for determining list
//         of dependents
//
//  Revision: 005   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  added 'acceptTransition()' method due to new Transition Rule
//	   implementation
//	*  now an observer of constant parm setting, therefore 'valueUpdate()',
//	   'doRetainAttachment()' and 'settingObserverInit()' overridden from
//	   observer base class
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	Intermediate calculations for this setting no longer need to
//	be "warped" to a resolution value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//             Initial version.
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class ExpTimeSetting : public BatchBoundedSetting, public SettingObserver
{
  public:
    ExpTimeSetting(void); 
    virtual ~ExpTimeSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    virtual const BoundStatus*  acceptPrimaryChange(
				    const SettingId::SettingIdType primaryId
						   );

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
				   const DiscreteValue            newValue,
				   const DiscreteValue            currValue);

    // SettingObserver methods...
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;

    virtual void  settingObserverInit(void);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif // defined (SIGMA_DEVELOPMENT)

    static void   SoftFault(const SoftFaultID softFaultID,
			    const Uint32      lineNumber,
			    const char*       pFileName  = NULL, 
			    const char*       pPredicate = NULL);
 
  protected:
    virtual Boolean calcDependentValues_(const Boolean isValueIncreasing);
    virtual void  updateConstraints_(void);

    virtual BoundedValue  calcBasedOnSetting_(
				const SettingId::SettingIdType basedOnSettingId,
				const BoundedRange::WarpDir    warpDirection,
				const BasedOnCategory          basedOnCategory
					     ) const;

  private:
    ExpTimeSetting(const ExpTimeSetting&);	// not implemented...
    void  operator=(const ExpTimeSetting&);	// not implemented...
};


#endif // ExpTimeSetting_HH 
