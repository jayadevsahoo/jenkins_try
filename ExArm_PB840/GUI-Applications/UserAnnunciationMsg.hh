#ifndef UserAnnunciationMsg_HH
#define UserAnnunciationMsg_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

#include "GuiTimerId.hh"
#include "Keys.hh"
#include "InterTaskMessage.hh"

//=====================================================================
// File: UserAnnunciationMsg - Data structure definition for UserAnnuncation
// VRTX queue messages.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/UserAnnunciationMsg.hhv   25.0.4.0   19 Nov 2013 14:08:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 04  By:  rhj	  Date:  08-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:     
//       Added Trend Data ready event and Trend manager status 
//       event types.
//
//  Revision: 03  By: gdc     Date:  22-Jan-2001    DCS Number: 5493
//  Project:  GuiComms
//  Description:
//	GuiComms initial revision.
//
//  Revision: 002  By:  yyy    Date:  01-Sep-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//             Initial release.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

//@ Type: UserAnnunciationMsg
union UserAnnunciationMsg
{
	//@ Type: UAMEventType
	// This enum identifies the event type
	enum UAMEventType
	{
		INPUT_FROM_TOUCH = FIRST_APPLICATION_MSG,
		INPUT_FROM_KNOB,
		INPUT_FROM_KEY,
		ALARM_UPDATE_EVENT,
		BD_GUI_EVENT,
		PATIENT_DATA_UPDATE_EVENT,
		PATIENT_DATA_TIMER_EVENT,
		TIMER_EVENT,
		SETTINGS_EVENT,
		NOVRAM_UPDATE_EVENT,
		SERVICE_DATA_EVENT,
		SERIAL_REQUEST_EVENT,
		SERIAL_FAILURE_EVENT,
		CAPTURE_DONE_EVENT,
		CAPTURE_PATIENT_DATA_EVENT,
		CAPTURE_ALARM_STATUS_EVENT,
		CAPTURE_WAVEFORMS_EVENT,
		CAPTURE_SETTINGS_EVENT,
		CAPTURE_LOWER_SUB_SCREEN_EVENT,
		PRINTER_ASSIGNMENT_EVENT,
        TREND_DATA_READY_EVENT,
        TREND_MANAGER_STATUS_EVENT,
		VSET_COMMAND_EVENT
	};
	
	//@ Type: TouchAction
	// This enum identifies whether the user touched or released
	// the touch screen.
	enum TouchAction
	{
		SCREEN_TOUCH,
		SCREEN_RELEASE
	};
	
	//@ Type: KeyAction
	// This enum identifies whether the user pressed or released
	// an off-screen key.
	enum KeyAction
	{
		KEY_PRESS,
		KEY_RELEASE
	};
	
	// Touch screen event structure
	struct
	{
		UAMEventType	eventType:	 8;
		TouchAction 	action:		 2;
	} touchParts;

	// Rotary knob event structure
	struct
	{
		UAMEventType 	eventType:	 8;
	} knobParts;

	// Key event structure
	struct
	{
		UAMEventType 	eventType:	 8;
		KeyAction 		action:		 2;
		GuiKeys			keyCode:	 8;
	} keyParts;

	// Alarm update event structure
	struct
	{
		UAMEventType 	eventType:	8;
	} alarmParts;

	// Breath Delivery event structure
	struct
	{
		UAMEventType 	eventType:		8;
		int 			eventId:		8;
		int 			eventStatus:	8;
		int 			eventPrompt:	8;
	} bdEventParts;

	// Patient data event structure
	struct
	{
		UAMEventType 	eventType:	8;
		int				datumId:	8;
	} patientDataParts;

	// Patient data timer event structure
	struct
	{
		UAMEventType 	eventType:	8;
		int				eventId:	8;
	} patientDataTimerParts;

	// Timer event structure
	struct
	{
		UAMEventType 				eventType:	8;
		GuiTimerId::GuiTimerIdType	timerId:	8;
	} timerParts;

	// Settings transaction event structure
	struct
	{
		UAMEventType 	eventType:	8;
		int				transactionSuccess:	8;
	} settingsParts;

	// NovRam update event structure
	struct
	{
		UAMEventType 	eventType:	8;
		int				updateId:	8;
	} novRamUpdateParts;

	// Service data event structure
	struct
	{
		UAMEventType 	eventType:	8;
		int				datumId:	8;
	} serviceDataParts;

	// serial data event structure
	struct
	{
		UAMEventType 	eventType:	8;
		int				failureId:	8;
		int				portNum:	8;
	} serialRequestParts;

    // trend data ready event structure
    struct
    {
        UAMEventType    eventType : 8;
        int             dataSetAddr: 24;	// Use just the lower 24 bits of the addr
    } trendDataReadyParts;

    // trend manager status event structure
    struct
    {
        UAMEventType    eventType : 8;
        int             isEnabled : 8;
        int             isRunning : 8;  // Trend data being collected
    } trendMgrStatusParts;

	// VSET command event structure
	struct
	{
		UAMEventType 	eventType:	8;
	} vsetCommandParts;


	// Use the following member to simply pass the 32 bits of
	// UserAnnunciationMsg data to message queues.
	Uint32 qWord;
};

#endif // UserAnnunciationMsg_HH
