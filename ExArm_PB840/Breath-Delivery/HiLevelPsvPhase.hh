
#ifndef HiLevelPsvPhase_HH
#define HiLevelPsvPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: HiLevelPsvPhase - Implements PSV phase during the high
//		peep level in BiLevel.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/HiLevelPsvPhase.hhv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "PsvPhase.hh"

//@ End-Usage

class HiLevelPsvPhase : public PsvPhase
{
  public:
    HiLevelPsvPhase( void) ;
    virtual ~HiLevelPsvPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;
  
  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) ;

  private:
    HiLevelPsvPhase( const HiLevelPsvPhase&) ;			// not implemented...
    void   operator=( const HiLevelPsvPhase&) ;	// not implemented...

} ;


#endif // HiLevelPsvPhase_HH 
