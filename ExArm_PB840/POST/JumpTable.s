;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================


;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: JumpTable.s - Jump table into Sigma application load module
;
;---------------------------------------------------------------------
;@ Interface-Description
;  This module relocated by the linker to the beginning of the Sigma
;  application load module supplies the entry points for the POST
;  Phase 2 executive and the start of VRTX initialization.  Phase 1
;  (Kernel) POST references Phase 2 POST through this module.  The 
;  boot process configured in Sigma-Os jumps to the start of the load
;  module which containing the jump instruction to VRTX initialization.
;---------------------------------------------------------------------
;@ Rationale
;  This module provides fixed entry points for the Sigma load module.
;---------------------------------------------------------------------
;@ Implementation-Description
;  The first jump instruction branches to VRTX initialization.  The 
;  second one jumps to Phase 2 POST.
;---------------------------------------------------------------------
;@ Fault-Handling
;  The POST fault handlers are still active during this phase of
;  initialization.
;---------------------------------------------------------------------
;  Restrictions
;    None
;---------------------------------------------------------------------
;@ Invariants
;    None
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
; @(#) $Header:   /840/Baseline/POST/vcssrc/JumpTable.s_v   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
;
;@ Modification-Log
;
;  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
;    Project: Sigma (R8027)
;    Description:
;       Added testable item annotations.
;
;  Revision: 002    By: Gary Cederquist  Date: 16-MAY-1997 DR Number: 2121
;    Project: Sigma (R8027)
;    Description:
;       Removed assembler options specified in source in favor of
;       command line options.
;
;  Revision: 001    By: Gary Cederquist   Date: 04-MAR-1997 DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================

                XDEF        _JumpTable
                XDEF        _sys_entry
                XDEF        _post_entry
                XREF        _sys_start
                XREF        _Executive__4PostSFv
                SECTION     code,,C

; void JumpTable() {
_JumpTable:
; // $[TI1]
_sys_entry:     jmp         _sys_start            ; VRTX initialization
; // $[TI2]
_post_entry:    jmp         _Executive__4PostSFv  ; Phase 2 POST
; }

                END
