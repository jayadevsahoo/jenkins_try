//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Pipe.h
//----------------------------------------------------------------------------


#include "stdafx.h"


#include <windows.h> 
#include <tchar.h>
#include <stdio.h> 
#include <strsafe.h>
#include "Pipe.h"
#include "Swat.h"

#define BUFSIZE 1024*5 
 

 
CPipe::CPipe(bool bBD)
{
	m_bBD = bBD;
	g_hChildStd_IN_Rd = NULL;
	g_hChildStd_IN_Wr = NULL;
	g_hChildStd_OUT_Rd = NULL;
	g_hChildStd_OUT_Wr = NULL;
	g_hInputFile = NULL;
	m_bError = false;

}
int CPipe::Run()
{
 SECURITY_ATTRIBUTES saAttr; 
  //AfxMessageBox(" Set the bInheritHandle flag so pipe handles are inherited."); 
   saAttr.nLength = sizeof(SECURITY_ATTRIBUTES); 
   saAttr.bInheritHandle = TRUE; 
   saAttr.lpSecurityDescriptor = NULL; 

// Create a pipe for the child process's STDOUT. 
    if ( ! CreatePipe(&g_hChildStd_OUT_Rd, &g_hChildStd_OUT_Wr, &saAttr, 0) ) 
      ErrorExit(TEXT("StdoutRd CreatePipe")); 

// Ensure the read handle to the pipe for STDOUT is not inherited.
   if ( ! SetHandleInformation(g_hChildStd_OUT_Rd, HANDLE_FLAG_INHERIT, 0) )
      ErrorExit(TEXT("Stdout SetHandleInformation")); 

// Create a pipe for the child process's STDIN. 
   if (! CreatePipe(&g_hChildStd_IN_Rd, &g_hChildStd_IN_Wr, &saAttr, 0)) 
      ErrorExit(TEXT("Stdin CreatePipe")); 

// Ensure the write handle to the pipe for STDIN is not inherited. 
   if ( ! SetHandleInformation(g_hChildStd_IN_Wr, HANDLE_FLAG_INHERIT, 0) )
      ErrorExit(TEXT("Stdin SetHandleInformation")); 
 
   HANDLE hHandle = CreateChildProcess();

// Get a handle to an input file for the parent. 
// This example assumes a plain text file and uses string output to verify data flow. 
 
  // if (argc == 1) 
   //   ErrorExit(TEXT("Please specify an input file.\n")); 

   CString strFileName;
   if(m_bBD)
	   strFileName = TEMP_FILE_BD_BATCH_DWNLD;
   else
	   strFileName = TEMP_FILE_GUI_BATCH_DWNLD;


   g_hInputFile = CreateFile(
  	   strFileName,
       GENERIC_READ, 
       0, 
       NULL, 
       OPEN_EXISTING, 
       FILE_ATTRIBUTE_READONLY, 
       NULL); 

   if ( g_hInputFile == INVALID_HANDLE_VALUE ) 
      ErrorExit(TEXT("CreateFile")); 
 
// Write to the pipe that is the standard input for a child process. 
// Data is written to the pipe's buffers, so it is not necessary to wait
// until the child process is running before writing data.
 
  // AfxMessageBox("write to pipe");

 //  WriteToPipe(); 
   
 
// Read from pipe that is the standard output for child process.
	ReadFromPipe(); 
  
	//Terminate the powerPC process
	DWORD dw;
	if (NULL != hHandle)
	{
		TerminateProcess(hHandle, 0);
		dw = GetLastError();
	}
   
	CloseHandle(g_hInputFile);   

// The remaining open handles are cleaned up when this process terminates. 
// To avoid resource leaks in a larger application, close handles explicitly. 



	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_nDownloading --; // to stop the refresh timer

    return 0; 

}


// Create a child process that uses the previously created pipes for STDIN and STDOUT.
HANDLE CPipe::CreateChildProcess(void)
{ 
   PROCESS_INFORMATION piProcInfo; 
   STARTUPINFO siStartInfo;
   BOOL bSuccess = FALSE; 
 
// Set up members of the PROCESS_INFORMATION structure. 
   ZeroMemory( &piProcInfo, sizeof(PROCESS_INFORMATION) );
 
// Set up members of the STARTUPINFO structure. 
// This structure specifies the STDIN and STDOUT handles for redirection.
   ZeroMemory( &siStartInfo, sizeof(STARTUPINFO) );
   siStartInfo.cb = sizeof(STARTUPINFO); 
   siStartInfo.hStdError = g_hChildStd_OUT_Wr;
   siStartInfo.hStdOutput = g_hChildStd_OUT_Wr;
   siStartInfo.hStdInput = g_hChildStd_IN_Rd;
   siStartInfo.dwFlags |= STARTF_USESTDHANDLES |SW_MINIMIZE; //|SW_SHOWDEFAULT ;
   siStartInfo.wShowWindow = SW_MINIMIZE;

    
// Create the child process. 

   CString strCmdLine;
	//strCmdLine.Format("powerpc-elf-gdb.exe -batch %s -x", TEMP_FILE_BD_BATCH_DWNLD);

	
    
   bSuccess = CreateProcess(NULL, 
      "powerpc-elf-gdb --command=c:\\bd_cmd1.txt",     // command line 
      NULL,          // process security attributes 
      NULL,          // primary thread security attributes 
      TRUE,          // handles are inherited 
	  CREATE_NO_WINDOW,
//	  0,             // creation flags 
      NULL,          // use parent's environment 
      NULL,          // use parent's current directory 
      &siStartInfo,  // STARTUPINFO pointer 
      &piProcInfo);  // receives PROCESS_INFORMATION 

   
   /* If an error occurs, exit the application. 
   if ( ! bSuccess ) 
      ErrorExit(TEXT("CreateProcess"));
   else 
   {
      // Close handles to the child process and its primary thread.
	  // Some applications might keep these handles to monitor the status
	  // of the child process, for example. 
      CloseHandle(piProcInfo.hProcess);
      CloseHandle(piProcInfo.hThread);
   }*/

  // TerminateProcess(piProcInfo.hProcess, 0);

   return piProcInfo.hProcess;
}


void CPipe::WriteToPipe(void) 

// Read from a file and write its contents to the pipe for the child's STDIN.
// Stop when there is no more data. 
{ 
   DWORD dwRead, dwWritten; 
   CHAR chBuf[BUFSIZE];
   BOOL bSuccess = FALSE;
 
   for (;;) 
   { 
      bSuccess = ReadFile(g_hInputFile, chBuf, BUFSIZE, &dwRead, NULL);
      if ( ! bSuccess || dwRead == 0 ) break; 
      
      bSuccess = WriteFile(g_hChildStd_IN_Wr, chBuf, dwRead, &dwWritten, NULL);
      if ( ! bSuccess ) break; 
   } 



// Close the pipe handle so the child process stops reading. 
   if ( ! CloseHandle(g_hChildStd_IN_Wr) ) 
      ErrorExit(TEXT("StdInWr CloseHandle")); 

	  
} 

void CPipe::ReadFromPipe(void) 

// Read output from the child process's pipe for STDOUT
// and write to the parent process's pipe for STDOUT. 
// Stop when there is no more data. 
{ 
   CSwatApp* pApp = (CSwatApp*)AfxGetApp();

   DWORD dwRead, dwWritten; 
   CHAR chBuf[BUFSIZE]; 
   BOOL bSuccess = FALSE;
   //HANDLE hParentStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
   CString strLogFile;

   if(m_bBD)
	   strLogFile = TEMP_FILE_BD_DWNLD_Log;
   else
	   strLogFile = TEMP_FILE_GUI_DWNLD_Log;

   HANDLE hParentStdOut = CreateFile(strLogFile,FILE_ALL_ACCESS,FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL,NULL);



// Close the write end of the pipe before reading from the 
// read end of the pipe, to control child process execution.
// The pipe is assumed to have enough buffer space to hold the
// data the child process has already written to it.
 
   if (!CloseHandle(g_hChildStd_OUT_Wr)) 
      ErrorExit(TEXT("StdOutWr CloseHandle")); 
 
   bool bStop(0);

   for (;;) 
   { 
      bSuccess = ReadFile( g_hChildStd_OUT_Rd, chBuf, BUFSIZE, &dwRead, NULL);

	  //Scan for error strings that indicate failure and report up to GUI.
	  CString str(chBuf);
	  str = str.Left(dwRead);

	  str.Replace("\n", "\r\n");

	  if(m_bBD)
	  {
		pApp->m_nBD_SW_DownloadStatus += 6; // +6 to match the status bar progress
		pApp->m_str_BD_SW_Dwnld_log += str;
	  }
	  else //GUI download
	  {
		  pApp->m_nGUI_SW_DownloadStatus +=3;
		  pApp->m_str_GUI_SW_Dwnld_log += str;
	  }

	  if(-1 !=  str.Find("Load failed"))
	  {
		  m_bError = 1;
	  }


	  if(-1 !=  str.Find("Continuing."))
	  {
		bStop = 1;
	  }
	  else if(-1 != str.Find("refused"))
	  {
		  AfxMessageBox("No connection could be made because the target machine actively refused it.");
		  bStop = 1;
	  }

	  DWORD dw = GetLastError();
      if( ! bSuccess || dwRead == 0 ) break; 
      
	  bSuccess = WriteFile(hParentStdOut, chBuf, 
                           dwRead, &dwWritten, NULL); //store to the log file


	  if( bStop == 1) break;

      if (! bSuccess ) break; 
   } 

   CloseHandle(hParentStdOut);


} 
 
void CPipe::ErrorExit(PTSTR lpszFunction) 

// Format a readable error message, display a message box, 
// and exit from the application.
{ 
    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError(); 

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );

    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, 
        (lstrlen((LPCTSTR)lpMsgBuf)+lstrlen((LPCTSTR)lpszFunction)+40)*sizeof(TCHAR)); 
    StringCchPrintf((LPTSTR)lpDisplayBuf, 
        LocalSize(lpDisplayBuf) / sizeof(TCHAR),
        TEXT("%s failed with error %d: %s"), 
        lpszFunction, dw, lpMsgBuf); 
    MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK); 

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
    ExitProcess(1);
}



