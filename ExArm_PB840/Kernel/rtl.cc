#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================
 
// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: rtl.cc - Run-Time Library replacements for POST
//---------------------------------------------------------------------
//@ Interface-Description
//  This module provides run-time library replacement functions for
//  the memory allocation and deallocation functions new() and delete().
//  Dynamic memory allocation is not allowed in the 840 system so these
//  functions generate a MAJOR POST failure if such an operation is 
//  attempted.  The placement new operator defined here is allowed and
//  returns a pointer to the specified memory location.
//---------------------------------------------------------------------
//@ Rationale
//  Memory allocation is bound at compile time.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Generates a MAJOR POST failure if the dynamic memory allocation or
//  deallocation of memory is attempted.
//---------------------------------------------------------------------
//@ Fault-Handling
//  not applicable
//---------------------------------------------------------------------
//@ Restrictions
//  not applicable
//---------------------------------------------------------------------
//@ Invariants
//  not applicable
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/rtl.ccv   25.0.4.0   19 Nov 2013 14:13:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 15-MAY-1997  DR Number: 2085
//    Project: Sigma (R8027)
//    Description:
//       Initial version.
//
//=====================================================================

#include "postnv.hh"
#include "kerrors.hh"
#include "Post_Library.hh"

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: operator new
//
//@ Interface-Description
//  This function overrides the library version dynamic memory 
//  allocation new operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Changes the current POST error code to an internal failure and 
//  calls MajorPostFailure() to terminate POST.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
void *
operator new(unsigned int)
{
    // $[TI1]
#ifdef E600_840_TEMP_REMOVED
    PKernelNovram->errorCode = POST_ERROR_INTERNAL_FAILURE;
#endif
    MajorPostFailure();    // never returns
    return (void*)0;       // here to keep compiler quiet
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: operator new  [placement new]
//
//@ Interface-Description
//  This function overrides the library version of the placement new
//  operator which is allowed in the 840 system.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a pointer to the memory location specified.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
void *
operator new(size_t, void*  pMemory)
{
    // $[TI2]
    return pMemory;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: operator delete
//
//@ Interface-Description
//  This function overrides the library version dynamic memory 
//  deallocation delete operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Changes the current POST error code to an internal failure and 
//  calls MajorPostFailure() to terminate POST.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
void 
operator delete(void *)
{
    // $[TI1]
#ifdef E600_840_TEMP_REMOVED
    PKernelNovram->errorCode = POST_ERROR_INTERNAL_FAILURE;
#endif
    MajorPostFailure();    // never returns
}
