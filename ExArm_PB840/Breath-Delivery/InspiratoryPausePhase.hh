#ifndef InspiratoryPausePhase_HH
#define InspiratoryPausePhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: InspiratoryPausePhase -  Implements an Inspiratory Pause Phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/InspiratoryPausePhase.hhv   25.0.4.0   19 Nov 2013 13:59:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "BreathPhase.hh"

//@ End-Usage


class InspiratoryPausePhase : public BreathPhase {
  public:
    InspiratoryPausePhase(BreathPhaseType::PhaseType phaseType) ;
	virtual ~InspiratoryPausePhase( void) ;

    virtual void newBreath( void) ;
    virtual void relinquishControl( const BreathTrigger& trigger) ;
    virtual void newCycle( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

  protected:

	virtual void updateFlowControllerFlags_( void) ;

  private:
    InspiratoryPausePhase( void) ;							// not implemented...
    InspiratoryPausePhase( const InspiratoryPausePhase&) ;	// not implemented...
    void   operator=( const InspiratoryPausePhase&) ;		// not implemented...

};


#endif // InspiratoryPausePhase_HH 
