//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Mutex.cpp
//---------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "Mutex.h"
#include <windows.h>


//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// Mutex::Mutex()
//---------------------------
Mutex::Mutex()
{
    m_bActive=true;

        m_Mutex = CreateMutex( 
        NULL,              // default security attributes
        FALSE,             // initially not owned
        NULL);             // unnamed mutex
    
}

//---------------------------
// Mutex::~Mutex()
//---------------------------
Mutex::~Mutex()
{
    int retval=0;
    m_bActive = false;

    CloseHandle(m_Mutex);
    
}

//---------------------------
//  Mutex::lock()
//---------------------------
bool Mutex::lock()
{
    if(m_bActive == true)
    {
        WaitForSingleObject(m_Mutex, INFINITE);
        return  m_bActive;
    }

    return false;
}

//---------------------------
//  Mutex::unlock()
//---------------------------
bool Mutex::unlock()
{
    if(m_bActive == true)
    {
        ReleaseMutex( m_Mutex); 
        return m_bActive;
    }

    return false;
}

