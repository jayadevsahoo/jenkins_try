// FixHTMLDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"


#define FILE_TAG_ORIGINAL _T("Orig_")



// CFixHTMLDlg dialog
class CFixHTMLDlg : public CDialog
{
// Construction
public:
	CFixHTMLDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_FIXHTML_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonStart();

	afx_msg LRESULT OnMessage_FixIt(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessage_FileDirty(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnMessage_Log(WPARAM wParam, LPARAM lParam);
	

	static void FixHTMLNow(CString strHTMLFileName);
	static void ShowWorking(CString strHTMLFileName);

	CString ReadRegistrySetting(CString strParam);

	static void Recurse(CString pstr);
	CString m_strEdit;
	
	static UINT Thread_Work(LPVOID lp);
	CWinThread* m_pWorkThread;

	CFixHTMLApp* pApp;


	
	

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	
	CListCtrl m_List;
	afx_msg void OnBnClickedButtonLoad();
	bool LoadConfig(CString strHTMLPathName);
	static void Log(CString str);

	CString m_strLog;
	afx_msg void OnBnClickedButtonBrowseRootPath();
	afx_msg void OnBnClickedButtonStop();
	afx_msg void OnNMDblclkList2(NMHDR *pNMHDR, LRESULT *pResult);
};
