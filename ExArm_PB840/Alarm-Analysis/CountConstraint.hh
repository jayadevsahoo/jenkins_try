#ifndef CountConstraint_HH
#define CountConstraint_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: CountConstraint - 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/CountConstraint.hhv   25.0.4.0   19 Nov 2013 13:51:22   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "Constraint.hh"
//@ End-Usage


class CountConstraint : public Constraint 
{
  public:

    CountConstraint(Constraint::BooleanOperator minBooleanOp, 
                    Constraint::BooleanOperator maxBooleanOp,
                    Constraint::CalcFunctionPtrType pMinCalcFunction = NULL,
                    Constraint::CalcFunctionPtrType pMaxCalcFunction = NULL);
    virtual ~CountConstraint(void);

    virtual Boolean evaluate          (OperatingGroup& rChildGroup) const;

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
  protected:

  private:
    // these methods are purposely declared, but not implemented...
    CountConstraint (void);                     // not implemented...
    CountConstraint (const CountConstraint&);   // not implemented...
    void   operator=(const CountConstraint&);   // not implemented...

};


#endif // CountConstraint_HH 
