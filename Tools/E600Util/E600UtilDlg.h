
// E600UtilDlg.h : header file
//

#pragma once

#include "DerivedTabCtrl.h"
#include "DerivedStatusBar.h"

// CE600UtilDlg dialog
class CE600UtilDlg : public CDialog
{
// Construction
public:
	CE600UtilDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_E600UTIL_DIALOG };
	CDerivedTabCtrl	m_tabDerivedTabCtrl;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	void PostNcDestroy();

// Implementation
protected:
	HICON m_hIcon;
	CToolBar m_wndToolBar;
	//CStatusBar m_wndStatusBar;
	CDerivedStatusBar m_wndStatusBar;
	int ProgressCount;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	void SetProgress(int Count);
	void SetStatusText(int Pane, LPCTSTR Text);

	afx_msg void OnAppAbout();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnClose();
	afx_msg void OnOK();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnKickIdle();
	afx_msg LRESULT OnEthMsgUpdateWindow(WPARAM x, LPARAM y);
	afx_msg void OnUpdateMsgCount(CCmdUI *pCCmdUI);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAppExit();
};
