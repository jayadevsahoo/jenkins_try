#ifndef MessageName_HH
#define MessageName_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename: MessageName - Enumerated names for Alarm text strings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/MessageName.hhv   25.0.4.0   19 Nov 2013 13:51:22   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By:   hct      Date: 01/01/95         DCS Number:   n/a
//      Project:   Sigma   (R8O27)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 05/22/95         DCS Number:   392
//      Project:   Sigma   (R8027)
//      Description:
//         Changed Technical Alarm text as per changes to SRS.
//  
//   Revision 003   By:   hct    Date: 05/22/95         DR Number:   7
//      Project:   Sigma   (R8027)
//      Description:
//         Changed Non-Technical Alarm text and structure as per changes to SRS.
//  
//   Revision 004   By:   hct    Date: 05/23/96         DR Number:   648
//      Project:   Sigma   (R8027)
//      Description:
//         Added alarm message "No ventilation.".
//  
//   Revision 005   By:   hct    Date: 01/17/97         DR Number:  1654
//      Project:   Sigma   (R8027)
//      Description:
//         Added alarm message "Power loss & return occurred with a
//         pre-existing DEVICE ALERT." and "Check Alarm Log.  EST required.".
//  
//   Revision 006   By:   hct    Date: 02/26/97         DR Number:  1793
//      Project:   Sigma   (R8027)
//      Description:
//         Delete WEAK BATTERY alarm and change analysis messages for 
//         INOPERATIVE BATTERY and LOW BATTERY.
//  
//   Revision 007   By:   hct    Date: 06/19/97         DR Number:  2097
//      Project:   Sigma   (R8027)
//      Description:
//         Add analysis messages for HIGH Pvent.
//  
//   Revision 008   By:   hct    Date: 08/27/97         DR Number:  2322
//      Project:   Sigma   (R8027)
//      Description:
//         Add analysis message and remedy message for BTAT 8.
//  
//   Revision 009   By:   hct    Date: 09/16/97         DR Number:  2503
//      Project:   Sigma   (R8027)
//      Description:
//         Changed messages for BTAT 6 (Touch screen blocked).
//  
//   Revision 010   By:   hct    Date: 09/29/97         DR Number:  1475
//      Project:   Sigma   (R8027)
//      Description:
//         Add VOLUME LIMIT alarm.
//
//   Revision 011	By:   sah	   Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//		   Replaced all references to 'AlarmMessages' with references to
//         the new 'AlarmStrs' class.
//  
//  Revision: 012  By: yyy    Date: 1-Dec-1998  DCS Number: 5104
//  Project: BiLevel (R8027)
//  Description:
//	  Removed HIGH_MINUTE_VOL_REMEDY_MSG enum.
//
//   Revision 013   By:   hct      Date: 01/13/99       DR Number: 5322
//      Project:   Sigma   (R8O27)
//      Description:
//         Add ATC functionality.
//
//   Revision 014   By:   sah      Date: 08/27/99       DR Number: 5513
//      Project:   ATC
//      Description:
//         Modified BTAT #8's primary msg from 'DEVICE ALERT' to 'O2 SENSOR',
//         thereby requiring the need of a new message name.  Also, removed
//         now-obsoleted apnea messages.
//
//   Revision 015   By:   hct      Date: 08/11/00       DR Number: 5313
//      Project:   ATC
//      Description:
//         Added PAV message names.
//
//   Revision 016   By:   hct      Date: 08/30/00       DR Number: 5755
//      Project:   ATC
//      Description:
//         Added VTPC message names.
//
//   Revision 016   By:   hct      Date: 08/30/00       DR Number: 5755
//      Project:   ATC
//      Description:
//         Changed VTPC message names.
//
//  Revision: 017   By:   hct    Date: 24-OCT-2000      DR Number: 5755
//  Project:  VTPC
//  Description:
//      Differentiate between spont and mand breaths for VTPC alarms.
//
//  Revision: 018   By:   heatherw    Date: 7-MAR-2002      DR Number: 5790
//  Project:  VTPC
//  Description:
//       Added the following messages for Vti Alarm Implementation
//       INSPIRED_SPONT_VOLUME_LIMIT_MSG,
//       INSPIRED_MAND_VOLUME_LIMIT_MSG.
//
//  Revision: 019   By:   ljstar    Date: 21-Aug-2003      DR Number: 6086
//  Project:  PAV3
//  Description:
//       SDCR 6086, added LOG keyword
//
//  Revision: 020   By:   gdc    Date: 21-Feb-2005      DR Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - NIV1
//      Modifications to support NIV.
//      Removed obsolete low insp pressure messages.
//      Added new low insp pressure (low Ppeak) messages.
//      Added new low urgency insp too long for NIV message.
//
//  Revision: 021   By:   gdc    Date: 27-May-2005      DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Removed Insp Too Long alarm for NIV. Added High Ti spont alert.
// 
//  Revision: 022   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//  
//  Revision: 023   By:   rhj    Date: 16-Jan-2009      SCR Number: 6452
//  Project:  840S
//  Description:
//        Added CIRCUIT_DISCONNECT_COMPRESSOR_REMEDY_MSG.
//
//  Revision: 024   By:   rhj   Date: 17-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added PROX_INOPERATIVE_MSG and PROX_INOPERATIVE_ANALYSIS_MSG.
//  
//  Revision: 025   By:   rhj   Date: 24-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added PROX_INOPERATIVE_REMEDY_MSG.
// 
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
//@ End-Usage

//@ Type: MessageName
// Enumeration representing the names of all text strings used in the
// Alarm-Analysis subsystem.  The text strings are contained in array 
// AlarmStrs::Text_.
enum MessageName {
  MESSAGE_NAME_NULL = -1,
  DEVICE_ALERT_MSG,
  NO_VENTILATION_MSG,
  NO_VENTILATION_SAFETY_VALVE_CLOSED_MSG,
  PROVIDE_OTHER_VENTILATION_MSG,
  VENTILATION_CONTINUES_MSG,
  VENTILATION_CONTINUES_BD_SPIRO_MSG,
  REPLACE_VENTILATOR_SERVICE_MSG,
  CHECK_PATIENT_REPLACE_VENT_MSG,
  SERVICE_REQUIRED_MSG,
  BD_NOT_AFFECTED_MSG,
  SCREEN_BLOCK_MSG,
  SCREEN_BLOCK_ANALYSIS_MSG,
  SCREEN_BLOCK_REMEDY_MSG,
  NO_BD_STATUS_MSG,
  AC_LOSS_DEVICE_ALERT_MSG,
  CHECK_ALARM_LOG_MSG,
  VENTILATION_WITH_100_O2_MSG,
  VENTILATION_EXCEPT_100_O2_MSG,
  VENTILATION_WITH_21_O2_MSG,
  VENTILATION_EXCEPT_21_O2_MSG,
  COMPROMISED_AIR_DELIVERY_MSG,
  COMPROMISED_O2_DELIVERY_MSG,
  CHECK_PATIENT_MSG,
  VENTILATION_UNAFFECTED_MSG,
  O2_SENSOR_MSG,
  O2_SENSOR_OOR_MSG,
  BD_OK_BAD_SPIRO_MSG,
  BD_OK_BAD_SPIRO_PRESS_TRIG_MSG,
  BD_NOT_AFFECTED_OTHER_FUNCTIONS_MSG,
  APNEA_MSG,
  CHECK_PATIENT_SETTINGS_MSG,
  APNEA_SHORT_DUR_ONE_EVENT_MSG,
  APNEA_LONG_DUR_TWO_EVENTS_MSG,
  HIGH_MINUTE_VOL_MSG,
  HIGH_MINUTE_VOL_LOW_MSG,
  HIGH_MINUTE_VOL_MED_MSG,
  HIGH_MINUTE_VOL_HIGH_MSG,
  HIGH_TIDAL_VOL_MSG,
  HIGH_TIDAL_VOL_REMEDY_MSG,
  HIGH_TIDAL_VOL_LOW_MSG,
  HIGH_TIDAL_VOL_MED_MSG,
  HIGH_TIDAL_VOL_HIGH_MSG,
  HIGH_O2_MSG,
  HIGH_O2_REMEDY_MSG,
  HIGH_O2_LOW_ANALYSIS_MSG,
  HIGH_O2_MEDIUM_ANALYSIS_MSG,
  HIGH_PRES_CIRC_MSG,
  HIGH_PRES_REMEDY_MSG,
  HIGH_PRES_LOW_MSG,
  HIGH_PRES_MED_MSG,
  HIGH_PRES_HIGH_MSG,
  HIGH_PRES_VENT_MSG,
  HIGH_PRES_VENT_LOW_MSG,
  HIGH_PRES_VENT_MED_MSG,
  HIGH_PRES_VENT_HIGH_MSG,
  HIGH_RESP_RATE_MSG,
  HIGH_RESP_RATE_LOW_MSG,
  HIGH_RESP_RATE_MED_MSG,
  HIGH_RESP_RATE_HIGH_MSG,
  AC_POWER_LOSS_MSG,
  AC_POWER_LOSS_ANALYSIS_MSG,
  LOW_OPERATIONAL_TIME_MSG,
  PREPARE_FOR_POWER_LOSS_MSG,
  INOPERATIVE_BATTERY_MSG,
  NON_FUNC_BATTERY_ANALYSIS_MSG,
  NON_FUNC_BATTERY_REMEDY_MSG,
  LOW_BATTERY_MSG,
  LOW_BATTERY_ANALYSIS_MSG,
  LOW_BATTERY_REMEDY_MSG,
  LOW_AC_POWER_MSG,
  LOW_AC_POWER_ANALYSIS_MSG,
  LOW_AC_POWER_REMEDY_MSG,
  LOW_MAND_TIDAL_VOL_MSG,
  LOW_MAND_TIDAL_VOL_REMEDY_MSG,
  LOW_MAND_TIDAL_VOL_LOW_MSG,
  LOW_MAND_TIDAL_VOL_MED_MSG,
  LOW_MAND_TIDAL_VOL_HIGH_MSG,
  LOW_MINUTE_VOL_MSG,
  LOW_MINUTE_VOL_LOW_MSG,
  LOW_MINUTE_VOL_MED_MSG,
  LOW_MINUTE_VOL_HIGH_MSG,
  LOW_SPONT_TIDAL_VOL_MSG,
  LOW_SPONT_TIDAL_VOL_LOW_MSG,
  LOW_SPONT_TIDAL_VOL_MED_MSG,
  LOW_SPONT_TIDAL_VOL_HIGH_MSG,
  LOW_O2_MSG,
  LOW_O2_ANALYSIS_MSG,
  LOW_O2_REMEDY_MSG,
  NO_AIR_SUPPLY_MSG,
  NO_AIR_SUPPLY_REMEDY_MSG,
  CHECK_PATIENT_AIR_SOURCE_MSG,
  COMPRESSOR_INOP_WITH_100_O2_MSG,
  COMPRESSOR_INOP_EXCEPT_100_O2_MSG,
  NO_VENT_CHECK_GAS_MSG,
  NO_O2_SUPPLY_MSG,
  NO_O2_SUPPLY_REMEDY_MSG,
  CHECK_PATIENT_O2_SOURCE_MSG,
  COMPRESSOR_INOPERATIVE_MSG,
  COMPRESSOR_INOPERATIVE_ANALYSIS_MSG,
  REPLACE_COMPRESSOR_MSG,
  COMPRESSOR_NOT_DURING_LOW_AC_POWER_MSG,
  COMPRESSOR_NOT_DURING_AC_POWER_LOSS_MSG,
  CIRCUIT_DISCONNECT_MSG,
  CIRCUIT_DISCONNECT_REMEDY_MSG,
  CIRCUIT_DISCONNECT_COMPRESSOR_REMEDY_MSG,
  CHECK_PATIENT_VENTILATOR_MSG,
  SEVERE_OCCLUSION_MSG,
  LITTLE_NO_VENTILATION_MSG,
  SEVERE_OCCLUSION_REMEDY_MSG,
  INSP_TOO_LONG_MSG,
  INSP_TOO_LONG_REMEDY_MSG,
  INSP_TOO_LONG_LOW_MSG,
  INSP_TOO_LONG_MED_MSG,
  INSP_TOO_LONG_HIGH_MSG,
  PROCEDURE_ERROR_MSG,
  PROCEDURE_ERROR_ANALYSIS_MSG,
  PROCEDURE_ERROR_REMEDY_MSG,
  VOLUME_LIMIT_MSG,
  VOLUME_LIMIT_ANALYSIS_MSG,
  VOLUME_LIMIT_REMEDY_MSG,
  
  INSPIRED_SPONT_VOLUME_LIMIT_MSG,  
  INSPIRED_SPONT_VOLUME_LIMIT_REMEDY_MSG,
  INSPIRED_SPONT_VOLUME_LIMIT_LOW_MSG,  
  INSPIRED_SPONT_VOLUME_LIMIT_MED_MSG,  
  INSPIRED_SPONT_VOLUME_LIMIT_HIGH_MSG, 
  INSPIRED_SPONT_PRESSURE_LIMIT_MSG,  
  INSPIRED_SPONT_TC_PRESSURE_LIMIT_REMEDY_MSG,
  INSPIRED_SPONT_PAV_PRESSURE_LIMIT_REMEDY_MSG,
  INSPIRED_SPONT_PRESSURE_LIMIT_LOW_MSG,  
  INSPIRED_SPONT_PRESSURE_LIMIT_MED_MSG,  
  INSPIRED_SPONT_PRESSURE_LIMIT_HIGH_MSG, 
  INSPIRED_MAND_VOLUME_LIMIT_MSG,  
  INSPIRED_MAND_VOLUME_LIMIT_REMEDY_MSG,  
  INSPIRED_MAND_VOLUME_LIMIT_LOW_MSG,  
  INSPIRED_MAND_VOLUME_LIMIT_MED_MSG,  
  INSPIRED_MAND_VOLUME_LIMIT_HIGH_MSG ,

  COMPENSATION_LIMIT_MSG,
  COMPENSATION_TC_LIMIT_REMEDY_MSG,
  COMPENSATION_PAV_LIMIT_REMEDY_MSG,
  COMPENSATION_LIMIT_LOW_MSG,
  COMPENSATION_LIMIT_MED_MSG,
  COMPENSATION_LIMIT_HIGH_MSG,
  LONG_PAV_STARTUP_MSG,
  LONG_PAV_STARTUP_REMEDY_MSG,
  LONG_PAV_STARTUP_LOW_MSG,
  LONG_PAV_STARTUP_MED_MSG,
  LONG_PAV_STARTUP_HIGH_MSG,
  PAV_RC_NOT_ASSESSED_MSG,
  PAV_RC_NOT_ASSESSED_REMEDY_MSG,
  PAV_RC_NOT_ASSESSED_LOW_MSG,
  PAV_RC_NOT_ASSESSED_MED_MSG,
  VOLUME_NOT_DELIVERED_MSG,
  VOLUME_NOT_DELIVERED_SPONT_LOW_MSG,
  VOLUME_NOT_DELIVERED_SPONT_MED_MSG,
  VOLUME_NOT_DELIVERED_MAND_LOW_MSG,
  VOLUME_NOT_DELIVERED_MAND_MED_MSG,
  VOLUME_NOT_DELIVERED_REMEDY_MSG,
  LOW_PRES_CIRC_MSG,
  LOW_PRES_LOW_MSG,
  LOW_PRES_MED_MSG,
  LOW_PRES_HIGH_MSG,
  LOW_PRES_REMEDY_MSG,

  AUTO_RESET_UT_MSG, 
  PAT_DATA_RESET_UT_MSG, 
  ALARM_SILENCE_UT_MSG, 
  END_ALARM_SILENCE_UT_MSG, 
  AUGMENTATION_UT_MSG, 
  DETECTION_UT_MSG, 
  MANUAL_RESET_UT_MSG,
  INDEPENDENT_UT_MSG,
  END_ALARM_SILENCE_HIGH_URGENCY_UT_MSG,
  ERROR_IN_UPDATE_TYPE_MSG,
  LOW_URGENCY_MSG,
  MEDIUM_URGENCY_MSG,
  HIGH_URGENCY_MSG,
  HIGH_URGENCY_AUTO_RESET_MSG,
  NORMAL_URGENCY_MSG,
  ERROR_IN_URGENCY_MSG,
  LOW_URGENCY_AUDIO_MSG,
  MEDIUM_URGENCY_AUDIO_MSG,
  HIGH_URGENCY_AUDIO_MSG,
  ALARM_SUMMARY_MSG,
  ALARM_AUDIO_MSG,
  NORMAL_URGENCY_AUDIO_MSG,
  ALARM_SILENCE_AUDIO_MSG,
  NOTE_MSG,
  PROX_INOPERATIVE_MSG,
  PROX_INOPERATIVE_ANALYSIS_MSG,
  PROX_INOPERATIVE_REMEDY_MSG,
  MAX_NUM_ALARM_MESSAGES  
 };


#endif // MessageName_HH 
