#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// @Class: ServiceLowerScreenSelectArea - The area in the Lower screen
// which contains the four sub-screen select buttons for EST, Date/time,
// Exit, and Other Screens.
//---------------------------------------------------------------------
//@ Interface-Description
// Creates and displays the four "tab" buttons on the lower screen
// that select the EST, Date/Time, Exit, Other Screens subscreens.  Only
// one instance of this class should be created.
//
// After construction the initialize() method needs to be called in
// order to tie the subscreens to the tab buttons.
//
// One other method is provided, setBlank(), used for blanking this
// area (in case of communication down).
//---------------------------------------------------------------------
//@ Rationale
// One class was needed to control the screen selection buttons for
// the Lower Screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor simply creates the four tab buttons and formulates
// the display of the Lower Screen Select Area.  The initialize()
// method "connects" the tab buttons with their respective subscreens.
// Subscreen selection is automatically handled by the tab buttons.
//
// See the implementation description of UpperScreenSelectArea for
// more details.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created (by LowerScreen).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceLowerScreenSelectArea.ccv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 004  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By: yyy      Date: 15-May-1997  DR Number: 2110
//    Project:  Sigma (R8027)
//    Description:
//      Added SRS 09001 mapping.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ServiceLowerScreenSelectArea.hh"

//@ Usage-Classes
#include "Image.hh"
#include "MiscStrs.hh"
#include "AdjustPanel.hh"
#include "LowerSubScreenArea.hh"
#include "EstTestSubScreen.hh"
#include "DateTimeSettingsSubScreen.hh"
#include "ExitServiceModeSubScreen.hh"
#include "ServiceLowerOtherScreensSubScreen.hh"

#include "SIterR_Drawable.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 407;
static const Int32 CONTAINER_WIDTH_ = 367;
static const Int32 CONTAINER_HEIGHT_ = 49;
static const Int32 BUTTON_WIDTH_ = 91;
static const Int32 BUTTON_HEIGHT_ = 49;
static const Int32 BUTTON_BORDER_ = 3;
static const Int32 OTHER_SCREENS_BITMAP_X_ = 20;
static const Int32 OTHER_SCREENS_BITMAP_Y_ = 5;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceLowerScreenSelectArea()  [Default Constructor]
//
//@ Interface-Description
// Constructs the Lower Screen Select Area.  Needs no parameters.  Created once
// as a member of LowerScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// All four tab buttons are created and appended to this area's container.  A
// long white box is also displayed at the top of the area due to the fact that
// the top of the area sits on the white border of the Lower Subscreen Area
// The grey background of this area would trample on the white border except
// for the addition of this white box.
//
// Bitmaps are created for the Other Screens tab button since icons are used
// instead of text for this button.
// $[09001] EST is only availabe when the ventilator is in service mode.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceLowerScreenSelectArea::ServiceLowerScreenSelectArea(void) :
	estTestTabButton_(0, 0, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
				BUTTON_BORDER_, Button::SQUARE,
				MiscStrs::EST_SETUP_TAB_BUTTON_LABEL, NULL),
	dateTimeSettingsTabButton_(1 * BUTTON_WIDTH_, 0, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
				BUTTON_BORDER_, Button::SQUARE,
				MiscStrs::CONFIG_SETUP_TAB_BUTTON_LABEL, NULL),
	exitServiceModeTabButton_(2 * BUTTON_WIDTH_, 0, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
				BUTTON_BORDER_, Button::SQUARE,
				MiscStrs::EXIT_SERVICE_MODE_TAB_BUTTON_LABEL, NULL),
	serviceLowerOtherScreensTabButton_(3 * BUTTON_WIDTH_, 0, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
				BUTTON_BORDER_, Button::SQUARE, NULL, NULL),
	lowerOtherScreensBitmap_(Image::ROtherScreensIcon),
	topLineBox_(0, 0, CONTAINER_WIDTH_, BUTTON_BORDER_)
{
	CALL_TRACE("ServiceLowerScreenSelectArea::ServiceLowerScreenSelectArea(void)");

	// Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Default background color is Extra Dark Grey
	setFillColor(Colors::EXTRA_DARK_BLUE);

	// Add white line at top
	topLineBox_.setColor(Colors::FRAME_COLOR);
	topLineBox_.setFillColor(Colors::FRAME_COLOR);
	addDrawable(&topLineBox_);

	lowerOtherScreensBitmap_.setX(OTHER_SCREENS_BITMAP_X_);
	lowerOtherScreensBitmap_.setY(OTHER_SCREENS_BITMAP_Y_);
	serviceLowerOtherScreensTabButton_.addLabel(&lowerOtherScreensBitmap_);

	// Add screen select buttons to container
	addDrawable(&estTestTabButton_);
	addDrawable(&dateTimeSettingsTabButton_);
	addDrawable(&exitServiceModeTabButton_);
	addDrawable(&serviceLowerOtherScreensTabButton_);			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceLowerScreenSelectArea  [Destructor]
//
//@ Interface-Description
// Destroys the Lower Screen Select Area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceLowerScreenSelectArea::~ServiceLowerScreenSelectArea(void)
{
	CALL_TRACE("ServiceLowerScreenSelectArea::~ServiceLowerScreenSelectArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
// Called immediately after construction by LowerScreen.  Performs
// initialization of any members that need it.
//---------------------------------------------------------------------
//@ Implementation-Description
// Points the tab buttons to their relevant subscreens.  This could
// not be done on construction as it is questionable as to whether
// the subscreens are created before or after this area.
// $[07009] The Lower screen select area shall continuously display ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerScreenSelectArea::initialize(void)
{
	CALL_TRACE("ServiceLowerScreenSelectArea::initialize(void)");

	// Connect each button to the subscreen which they select
	estTestTabButton_.setSubScreen(LowerSubScreenArea::GetEstTestSubScreen());
	dateTimeSettingsTabButton_.setSubScreen(LowerSubScreenArea::GetDateTimeSettingsSubScreen());
	exitServiceModeTabButton_.setSubScreen(LowerSubScreenArea::GetExitServiceModeSubScreen());
	serviceLowerOtherScreensTabButton_.setSubScreen(LowerSubScreenArea::GetServiceLowerOtherScreensSubScreen());
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setBlank
//
//@ Interface-Description
// Passed a boolean, 'blank', which tells Lower Screen Select Area to either
// blank its area (make all drawable contents invisible) or not.
// This is needed by LowerScreen when it is in communication down mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// Go through the members of this area's container and set each
// drawable's show flag to be the inverse of 'blank'.  However,
// topLineBox_ must always be shown.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerScreenSelectArea::setBlank(Boolean blank)
{
	CALL_TRACE("ServiceLowerScreenSelectArea::setBlank(Boolean blank)");

	// Set the show flag on all drawable contents appropriately
	SIterR_Drawable children(*getChildren_());
	Drawable *pDraw;
  	for (SigmaStatus ok = children.goFirst();
			(SUCCESS == ok) && (pDraw= (Drawable *)&(children.currentItem()));
			ok = children.goNext())
	{
		pDraw->setShow(!blank);
	}

	// The Top Line box must be shown at all times
	topLineBox_.setShow(TRUE); 							// $[TI1]
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerScreenSelectArea::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							SERVICELOWERSCREENSELECTAREA,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
