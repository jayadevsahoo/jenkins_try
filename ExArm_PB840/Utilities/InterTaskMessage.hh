#ifndef InterTaskMessage_HH
#define InterTaskMessage_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation of
// California.
//
//       Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  InterTaskMessage - Common Inter-Task Message Format
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/InterTaskMessage.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage
 
struct InterTaskMessage 
{
    Uint   msgId    : 8;
    Uint   msgData  : 24;
};

enum MessageIdRange
{
    FIRST_TASK_CONTROL_MSG    = 1,
    FIRST_TASK_MONITOR_MSG    = 11,
    FIRST_APPLICATION_MSG     = 101
};

#endif // InterTaskMessage_HH
