// ViewStoredResultsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Swat.h"
#include "ViewStoredResultsDlg.h"

#include "TestResultsDlg.h"


// ViewStoredResultsDlg dialog

IMPLEMENT_DYNAMIC(ViewStoredResultsDlg, CDialog)

ViewStoredResultsDlg::ViewStoredResultsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ViewStoredResultsDlg::IDD, pParent)
	, m_day(0)
	, m_strSession(_T(""))
	, m_strFileName(_T(""))
{

}

ViewStoredResultsDlg::~ViewStoredResultsDlg()
{
}

void ViewStoredResultsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MONTHCALENDAR1, m_DateTimeCtrl);
	DDX_Text(pDX, IDC_EDIT1, m_day);
	DDX_Text(pDX, IDC_EDIT3, m_strSession);
	DDX_Text(pDX, IDC_EDIT4, m_strFileName);
}


BEGIN_MESSAGE_MAP(ViewStoredResultsDlg, CDialog)
	ON_NOTIFY(MCN_SELECT, IDC_MONTHCALENDAR1, &ViewStoredResultsDlg::OnMcnSelectMonthcalendar1)
	ON_BN_CLICKED(IDC_BUTTON_SHOW_RESULTS, &ViewStoredResultsDlg::OnBnClickedButtonShowResults)
END_MESSAGE_MAP()


// ViewStoredResultsDlg message handlers

void ViewStoredResultsDlg::OnMcnSelectMonthcalendar1(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMSELCHANGE pSelChange = reinterpret_cast<LPNMSELCHANGE>(pNMHDR);

	CTime refDateTime;
	m_DateTimeCtrl.GetCurSel(refDateTime);
	int n = refDateTime.GetDay();

	m_day = n;
	UpdateData(0);

	*pResult = 0;
}


void ViewStoredResultsDlg::OnBnClickedButtonShowResults()
{
	UpdateData(1);


	CTestResultsDlg resultDlg;
	resultDlg.SetResultParams( m_strSession , m_strFileName);
	resultDlg.DoModal();
}
