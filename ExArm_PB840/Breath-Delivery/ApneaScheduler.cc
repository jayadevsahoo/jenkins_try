#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ApneaScheduler - This scheduler is responsible for handling
// breathing during apnea.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for scheduling breath phases during apnea.  The
// class is also responsible for relinquishing control to the next valid
// scheduler and for assuming control when apnea is requested.  Apnea is
// requested when the apnea trigger evaluates to true, or upon emergency mode
// recovery and power-up sequence, when transition to the previous mode is
// required.  The scheduler is responsible for enabling valid mode triggers for
// apnea and for enabling breath triggers that are not under the breath phase
// direct control. The apnea scheduler is responsible for handling user events
// like 100% O2, manual inspiration request, etc. and for handling apnea rate
// change setting events.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates all the breathing rules specified for apnea
// breathing. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The class implements methods for breath and mode transitions and for
// breath data updates. The apnea scheduler is responsible for:
// - Determining the next breath when a breath trigger becomes active.
// - Initiating the activities required for the proper start of a new breath:
//    phase in settings, update breath record, set breath triggers, 
//    enable breath triggers, and phase out the previous breath phase, 
// - Relinquishing control when a mode trigger becomes active.
//    Activities while control is relinquished include: 
//    disable out of date breath triggers, determine which is the next
//    valid scheduler, enable breath triggers for the next scheduler,
//    and instruct the next scheduler to take control.
// - Taking control when the previous scheduler is relinquishing control. 
//    Activities while taking control include:
//    register self with the BreathPhaseScheduler object,  and activate
//    the mode trigger list.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ApneaScheduler.ccv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 030   By: mnr    Date: 28-Dec-2008    SCR Number: 6520
//  Project:  NEO
//  Description:
//      More informative assertion than before.
// 
//  Revision: 029   By: rhj    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Added net flow backup inspiratory trigger.
// 
//  Revision: 028   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 027   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Reject RM maneuver requests during Apnea.
//
//  Revision: 026  By: healey     Date:  15-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added new triggers to class precondition
//
//  Revision: 028  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//      Initial ATC release
//
//  Revision: 027  By:  syw    Date:  17-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (R8027)
//       Description:
//			Call RPeep.setPeepChange if breath is to be Peep Recovery.
//
//  Revision: 026  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling of inspiratory pause event.
//
//  Revision: 025  By: iv    Date:  15-Jan-1998   DR Number: DCS 2721
//  	Project:  Sigma (R8027)
//		Description:
//          Added a call to phase-in all settings in method relinquishControl()..
//
//  Revision: 024  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 023  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 022  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 021  By:  iv    Date:  03-Dec-1996    DR Number: DCS 1608, 1541
//       Project:  Sigma (R8027)
//       Description:
//             determineBreathPhase(), case = NON_BREATHING : eliminate assertion
//             and add initialization for peep recovery state.
//             Prepare code for inspections.
//
//  Revision: 020  By:  iv    Date:  15-Nov-1996    DR Number: DCS 1559
//       Project:  Sigma (R8027)
//       Description:
//             Replaced safe class assertion with class assertion in constructor.
//
//  Revision: 019  By:  iv    Date:  06-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Deleted class assertion in determineBreathPhase() for NON_BREATHING
//             case.
//
//  Revision: 018  By:  iv    Date:  04-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changes for unit test.
//
//  Revision: 017  By:  sp    Date:  26-Sep-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed PeepRecoveryTrigger to PeepRecoveryInspTrigger.
//
//  Revision: 016 By:  iv   Date:   09-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Added a check for peep recovery trigger in determineBreathPhase(),
//             and disable the trigger on relinquishControl().
//
//  Revision: 015 By:  iv   Date:   13-Aug-1996    DR Number: DCS 1218
//       Project:  Sigma (R8027)
//       Description:
//             Changed the range of SettingId values as checked in a setting change
//             call back assertion.
//
//  Revision: 014 By:  iv   Date:   02-Aug-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Incorporated PEEP recovery after OSC and Powerup.
//
//  Revision: 013 By:  iv   Date:   20-Jul-1996    DR Number: DCS 10015
//       Project:  Sigma (R8027)
//       Description:
//             In relinquishControl(), for non breathing phase, eliminate the mode id (manual
//             reset) from the assertion - required for cases where the trigger is a result
//             of apnea not possible (auto reset).
//
//  Revision: 012 By:  iv   Date:   11-Jul-1996    DR Number: DCS 10007
//       Project:  Sigma (R8027)
//       Description:
//             Added a class assertion in relinquishControl :
//             CLASS_ASSERTION((BreathPhase*)&RSafetyValveClosePhase.getElapsedTimeMs() <
//                                SV_CLOSE_TIME_MS - CYCLE_TIME_MS)
//             This assertion adds an extra level of protection that ensures
//             system recovery in case of BD stuck in non breathing mode for
//             a long time and "apnea is not possible".
//
//  Revision: 011 By:  iv   Date:   09-Jul-1996    DR Number: DCS 1126
//       Project:  Sigma (R8027)
//       Description:
//             Notify User and Alarms on apnea reset - unconditionally.
//
//  Revision: 010 By:  iv   Date:   03-Jul-1996    DR Number: DCS 1107
//       Project:  Sigma (R8027)
//       Description:
//             Disable the asap trigger when transitioning to spont - required
//             since the introduction of the PeepRecovery phaes. 
//
//  Revision: 009 By:  iv   Date:   21-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Added a Boolean isPeepRecovery to newBreathRecord(...) call.
//
//  Revision: 008 By:  iv   Date:   16-Jun-1996    DR Number: DCS 967, 1055
//       Project:  Sigma (R8027)
//       Description:
//             Fixed class pre condition in determineBreathPhase() during inspiration
//             to consider peep recovery.
//
//  Revision: 007 By:  iv   Date:   04-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Fixed call to base class takeControl() from takeControl().
//             Added a call BdSystemStateHandler::UpdateApneaActiveStatus(TRUE).
//
//  Revision: 006 By:  iv   Date:   07-May-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Changed takeControl(), determineBreathPhase(), and
//             startInspiration_() methods - to allow for a PEEP recovery
//             breath phase to take place on transitions from disconnect,
//             standby, and SVO.
//
//  Revision: 005 By:  iv   Date:   15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated O2_MONITOR_CALIBRATE case in reportEventStatus_().
//
//  Revision: 004 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 003 By:  kam   Date:  01-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Modified interface for reporting ventilator status.  Vent
//             and user event status are posted to the BD Status task
//             via the method VentAndUserEventStatus::PostEventStatus().
//             The BD status task is then responsible for transmitting
//             the status information to the GUI through NetworkApps.
//
//             Also, updated SST_CONFIRMATION case of reportEventStatus_() and
//             added O2_MONITOR_CALIBRATE.  Added call to base class
//             takeControl() from takeControl().
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem for reporting
//             BD alarms and with NetworkApp for sending ventilator status information
//             to the GUI. 
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ApneaScheduler.hh"
#include "BreathMiscRefs.hh"
#include "BdDiscreteValues.hh"
#include "PhaseRefs.hh"
#include "TriggersRefs.hh"
#include "SchedulerRefs.hh"
#include "ModeTriggerRefs.hh"

//@ Usage-Classes
#include "BreathSet.hh"
#include "BreathTriggerMediator.hh"
#include "UiEvent.hh"
#include "BdSystemStateHandler.hh"
#include "BreathTrigger.hh"
#include "ModeTrigger.hh"
#include "ModeTriggerMediator.hh"
#include "TimerBreathTrigger.hh"
#include "ApneaInterval.hh"
#include "O2Mixture.hh"
#include "Peep.hh"
#include "Sensor.hh"
#include "PendingContextHandle.hh"
#include "PhasedInContextHandle.hh"
#include "PhaseInEvent.hh"
#include "BreathPhase.hh"
#include "BdAlarms.hh"
#include "VentAndUserEventStatus.hh"
#include "BiLevelScheduler.hh"
#include "Maneuver.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ApneaScheduler()  [Default Contstructor]
//
//@ Interface-Description
//        Constructor.
// $[04125] $[04130] $[04141] Constructs the class instance with the proper id. The list
// of valid mode triggers, used by the mode trigger mediator is
// initialized as well. The triggers are ordered based on priority - the most urgent
// is first. The list also guarantees that when a trigger fires - the triggers
// that follow it do not have to be processed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor takes no arguments. It invokes the base class
// constructor with the APNEA id argument. It initializes the private data
// member pModeTriggerList_. 
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
// The number of elements on the trigger list is asserted
// to be MAX_NUM_APNEA_TRIGGERS and MAX_NUM_APNEA_TRIGGERS to be less than
// MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static const Int32 MAX_NUM_APNEA_TRIGGERS = 6;
ApneaScheduler::ApneaScheduler(void) : BreathPhaseScheduler(SchedulerId::APNEA)
{
    // $[TI1]
    CALL_TRACE("ApneaScheduler::ApneaScheduler(void)");

    Int32 ii = 0;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RDisconnectTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&ROcclusionTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RApneaAutoResetTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RManualResetTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RApneaTrigger;
    pModeTriggerList_[ii] = (ModeTrigger*)NULL;

    CLASS_ASSERTION(ii == MAX_NUM_APNEA_TRIGGERS &&
                        MAX_NUM_APNEA_TRIGGERS < MAX_MODE_TRIGGERS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ApneaScheduler()  [Destructor]
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
ApneaScheduler::~ApneaScheduler(void)
{
  CALL_TRACE("ApneaScheduler(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathPhase
//
//@ Interface-Description
// This method accepts a breath trigger as an argument. it determines
// which breath phase to start, based on the trigger id, the current phase,
// and the vent settings.
// Once the next breath phase is determined, the following activities take
// place:
// -- phase-in new settings for start of apnea inspiration or apnea exhalation, 
//   and update the applied apnea O2%, the apnea interval and the actual peep. 
// -- Update the current breath record with new data.
// -- Change the active breath trigger list, disabling the triggers on the
//   current list.
// -- Instruct the previous breath phase to relinquish control and setup
//   the new breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The activities handled by this method are dependent on the current breath
// phase. The current breath phase is retrieved from the BreathPhase object.
// Before any phase starts, the BreathTriggerMediator is used to reset the old
// phase trigger list, and to set the new phase trigger list, the old phase
// relinquishes control, and the new phase registers itself with the
// BreathPhase object. New settings are phased in - when relevant, the applied
// O2 percent is updated if necessary, the apnea interval is updated for start
// of exhalation, and the breath record gets updated.  A switch statement is
// implemented for the various breath phases which are: NON_BREATHING,
// EXHALATION, and INSPIRATION.  When current phase is EXHALATION,
// if a peep reduction trigger is detected then exhalation (with null
// inspiration flag) is delivered, follwed by enabling a asap insp. trigger. 
// Otherwise, a call is made to start inspiration.  Before that method is
// called, the breath type is set (e.g. CONTROL, ASSIST, etc.) When current
// phase is NON_BREATHING, the breathType is set to SPONT to start peep
// recovery inspiration.
//---------------------------------------------------------------------
//@ PreCondition
// For each case in the switch statement, the expected range of valid
// triggers and/or previous schedulers is checked. The valid range for
// phase type is also checked.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
ApneaScheduler::determineBreathPhase(const BreathTrigger& breathTrigger) 
{

    CALL_TRACE("ApneaScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)");

    // the type of breath to communicate to the user
    ::BreathType breathType = NON_MEASURED;  

    const Trigger::TriggerId triggerId = breathTrigger.getId();

    // Get the scheduler id from the current breath record. Note that the current
    // breath record stores the id of the scheduler that was active at the time the
    // record was created.  
    const SchedulerId::SchedulerIdValue previousSchedulerId =
            (RBreathSet.getCurrentBreathRecord())->getSchedulerId();

    // A pointer to the current breath phase
    BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
    // determine the current phase type (e.g. EXHALATION, INSPIRATION)
    const BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();

    switch (phase)
    {
        case BreathPhaseType::NON_BREATHING:
        // $[TI1]
            // $[04321] Deliver a peep recovery inspiration:
            BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
            startInspiration_(breathTrigger, pBreathPhase, ::SPONT);

            break;
            
        case BreathPhaseType::EXHALATION:
        // $[TI2]
            if( (triggerId == Trigger::TIME_INSP) &&
                            (previousSchedulerId != SchedulerId::APNEA) )
            { 
                // $[TI2.1]
                //previous scheduler is not APNEA, therefore the time trigger should have
                // been disabled:
                CLASS_ASSERTION_FAILURE();
            }
            else if(triggerId == Trigger::PEEP_REDUCTION_EXP)
            { 
                // $[TI2.4]
                startExhalation_(breathTrigger, pBreathPhase, TRUE);

                // After the peep reduction (peep low) we should enable the 
                // inspiration trigger to start a inspiration.
                ((BreathTrigger&)RAsapInspTrigger).enable();
            }
            else // $[04256] $[03008] $[04010] inspiration is to be delivered
            {
            // $[TI2.2]
                if(triggerId == Trigger::PEEP_RECOVERY)
                {
                // $[TI2.2.4]
                    breathType = ::SPONT;
                    CLASS_ASSERTION( BreathPhaseScheduler::PeepRecovery_ ==
                                      BreathPhaseScheduler::START &&
                                     previousSchedulerId == SchedulerId::OCCLUSION );
                }
                else if( (triggerId == Trigger::OPERATOR_INSP) ||
                        (triggerId == Trigger::PEEP_RECOVERY_MAND_INSP) ||
                        (triggerId == Trigger::ASAP_INSP) ||
                        (triggerId == Trigger::TIME_INSP) )
                {
                // $[TI2.2.1]
                    //This is a VIM breath
                    breathType = ::CONTROL;
                }
                else if( (triggerId == Trigger::NET_FLOW_INSP) || 
                         (triggerId == Trigger::PRESSURE_BACKUP_INSP) || 
						 (triggerId == Trigger::NET_FLOW_BACKUP_INSP) ||
                         (triggerId == Trigger::PRESSURE_INSP)) 
                    // this is a patient triggered breath -- PIM
                {
                    // $[TI2.2.2]
                    breathType     = ::ASSIST;
                }
                else
                {
                    // $[TI2.2.3]
                    //The trigger should be one of the following legal triggers:
                    // immediate trigger is not valid since it trigggers inspiration only
                    // from the NON_BREATHING phase.
                    if( (triggerId != Trigger::NET_FLOW_INSP) || 
						(triggerId != Trigger::NET_FLOW_BACKUP_INSP) ||
                        (triggerId != Trigger::PRESSURE_BACKUP_INSP) || 
                        (triggerId != Trigger::PRESSURE_INSP) || 
                        (triggerId != Trigger::PEEP_RECOVERY_MAND_INSP) || 
                        (triggerId != Trigger::ASAP_INSP) || 
                        (triggerId != Trigger::TIME_INSP) || 
                        (triggerId != Trigger::PEEP_RECOVERY) ||
                        (triggerId != Trigger::OPERATOR_INSP) 
					  )
					{
						// SCR 6520 : Log the trigger id to aid further diagnosis
						AUX_CLASS_ASSERTION_FAILURE(triggerId);
					}

                }
                startInspiration_(breathTrigger, pBreathPhase, breathType);
            }
            break;
        
        case BreathPhaseType::INSPIRATION:
        // $[TI3]
            //Check the validity of the triggers. Once triggers are checked, settings for
            // new exhalation can be phased in.
            //The trigger should be one of the following legal triggers (note that
            //    inspiratory phase can be a spont inspiration on peep recovery breath)
            CLASS_PRE_CONDITION( (triggerId == Trigger::DELIVERED_FLOW_EXP) || 
			                (triggerId == Trigger::HIGH_PRESS_COMP_EXP) || 
			                (triggerId == Trigger::LUNG_FLOW_EXP) || 
            			    (triggerId == Trigger::LUNG_VOLUME_EXP) || 
                            (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) || 
                            (triggerId == Trigger::PRESSURE_EXP) || 
                            (triggerId == Trigger::BACKUP_TIME_EXP) || 
                            (triggerId == Trigger::IMMEDIATE_EXP) || 
                            (triggerId == Trigger::HIGH_VENT_PRESSURE_EXP) );

            // inspiratory pause shall not be active
			startExhalation_(breathTrigger, pBreathPhase);
            break;
        
            // $[TI4] DELETED

        default:
            // $[TI5]
            AUX_CLASS_ASSERTION_FAILURE(phase);
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
// The method accepts a mode trigger reference as an argument.  Before the new
// mode is instructed to take control, the insp time trigger is disabled, the
// asap trigger is enabled (on transition to SIMV, and AC modes), and resets all
// mode triggers on its list.  Since Apnea mode may be activated following SVO,
// it is possible to have a NON_BREATHING breath phase active when apnea is the
// active scheduler. In that case, any breath delivery is avoided until the
// completion of the transitional NON_BREATHING phase that guarantees a complete
// closure of the safety valve.  When this is the case, no breath triggers are
// enabled  when a transition to a different breathing mode is detected.
// The immediate breath trigger is enabled on transition to emergency modes. No
// breath triggers are enabled on transition to Spont mode. When an immediate
// breath trigger is enabled, the rest of the triggers on the active list are
// disabled to ensure safe transition to the emergency mode.  The argument
// passed, provides the method with the information required to determine the
// next scheduler.  Note that no settingChangeModeTrigger is expected here
// since the "normal" settings are phased in immediatly during apnea, and that
// includes the mode setting as well.  The BD Status task is also notified of the exit
// from AV.
//---------------------------------------------------------------------
//@ Implementation-Description
// Individual breath triggers are enabled and disabled by directly
// accessing the trigger instance.
// Disabling all triggers on the current list is accomplished by 
// using the only one instance of the breath trigger mediator,
// calling the method resetTriggerList(). 
// The mode trigger id passed as an argument is checked to
// determine the requested scheduler which then instructed to
// take control.
// rBdAlarms.postBdAlarm() is invoked to notify Alarms when the AV
// condition has reset and VentAndUserEventStatus::PostEventStatus() is
// invoked to notify the BD Status Task of exit from AV.
//---------------------------------------------------------------------
//@ PreCondition
// The mode setting is checked for valid range of values.
// The trigger id is checked for valid range of mode triggers.
// When the mode trigger is either apnea autoreset or manual reset, the
// non breathing phase is checked to be SafetyValveClosePhase.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
ApneaScheduler::relinquishControl(const ModeTrigger& modeTrigger)
{
    CALL_TRACE("ApneaScheduler::relinquishControl(const ModeTrigger& modeTrigger)");

    const Trigger::TriggerId modeTriggerId = modeTrigger.getId();

    // determine the current phase type (e.g. EXHALATION, INSPIRATION)
    const BreathPhaseType::PhaseType phase = BreathRecord::GetPhaseType();

    // Note: any unexpected trigger will cause a soft fault!

    // disable the inspiratory time trigger, to avoid apnea inspiration trigger
    // during the new mode
    RTimeInspTrigger.disable();

    // disable the peep recovery trigger, to avoid its interference with
    // the next scheduler.
    ((BreathTrigger&)RPeepRecoveryInspTrigger).disable();

    // re-enable the apnea trigger, since it disables itself when it evaluates to true
    ((ModeTrigger&)RApneaTrigger).enable();

    RModeTriggerMediator.resetTriggerList();
    
    // phase-in all new vent settings
    PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

    // check what mode to switch to
    DiscreteValue mode = PhasedInContextHandle::GetDiscreteValue(SettingId::MODE);
    
    //$[05080]
    // notify alarms on apnea reset
    RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_NOT_IN_APNEA_VENTILATION);

    // notify BD Status Task of exit from AV
    VentAndUserEventStatus::PostEventStatus (EventData::APNEA_VENT, EventData::CANCEL);
        
    if( (modeTriggerId == Trigger::MANUAL_RESET) ||
        (modeTriggerId == Trigger::APNEA_AUTORESET) )
    {
    // $[TI1]

        if (phase != BreathPhaseType::NON_BREATHING)
        {
           // $[TI1.1]
           // set the asap trigger only if the mode is not spont
           //$[04264] $[04265] $[04266] $[04267] $[04269] $[04270] $[04271]
           if ((mode != ModeValue::SPONT_MODE_VALUE) && (mode != ModeValue::CPAP_MODE_VALUE))
           {
               // $[TI1.1.1]
               ((BreathTrigger&)RAsapInspTrigger).enable();
               // disable the peep recovery mand insp trigger in case we just recovered
               // from a disconnect (or SVO or occlusion)
               ((BreathTrigger&)RPeepRecoveryMandInspTrigger).disable();
           }
           else // mode is spont, disable the asap trigger and the peep recovery mandatory
                // insp trigger - just in case they were enabled before
           {// $[TI1.1.2]
               ((BreathTrigger&)RAsapInspTrigger).disable();
               ((BreathTrigger&)RPeepRecoveryMandInspTrigger).disable();
           }
        }
        else
        // $[TI1.2]
            // phase == BreathPhaseType::NON_BREATHING
            // If the breath phase is a non breathing phase, it is the
            // safety valve closed phase that is in progress, and since a time trigger
            // is already set to terminate that phase, there is no need to set any
            // breath trigger.
        {
            CLASS_PRE_CONDITION( BreathPhase::GetCurrentBreathPhase() ==
                      (BreathPhase*)&RSafetyValveClosePhase &&
                      ((BreathPhase*)&RSafetyValveClosePhase)->getElapsedTimeMs() <
                              SV_CLOSE_TIME_MS + CYCLE_TIME_MS );
        }

        if(mode == ModeValue::SIMV_MODE_VALUE)
        {
        // $[TI1.3]
            ((BreathPhaseScheduler&)RSimvScheduler).takeControl(*this);
        }
        else if ((mode == ModeValue::SPONT_MODE_VALUE) || (mode == ModeValue::CPAP_MODE_VALUE))
        {
        // $[TI1.4]
            ((BreathPhaseScheduler&)RSpontScheduler).takeControl(*this);
        }
        else if(mode == ModeValue::AC_MODE_VALUE)
        {
        // $[TI1.5]
            ((BreathPhaseScheduler&)RAcScheduler).takeControl(*this);
        }
        else if(mode == ModeValue::BILEVEL_MODE_VALUE)
        {
        // $[TI1.7]
            // $[04253]
            ((BreathPhaseScheduler&)RBiLevelScheduler).takeControl(*this);
        } 
        else
        {
        // $[TI1.6]
            // the only legal modes to  switch to from apnea are either SPONT
            // AC or SIMV:
			AUX_CLASS_ASSERTION_FAILURE(mode);
        }
    }
    else
    {
    // $[TI2]
        //Reset currently active triggers and then enable the only valid breath
        // trigger for that transition.
        RBreathTriggerMediator.resetTriggerList();
        ((BreathTrigger&)RImmediateBreathTrigger).enable();
        if(modeTriggerId == Trigger::OCCLUSION)
        {
        // $[TI2.1]
            ((BreathPhaseScheduler&)ROscScheduler).takeControl(*this);
        }
        else if(modeTriggerId == Trigger::DISCONNECT)
        {
        // $[TI2.2]
            ((BreathPhaseScheduler&)RDisconnectScheduler).takeControl(*this);
        }
        else if(modeTriggerId == Trigger::SVO)
        {
        // $[TI2.3]
            ((BreathPhaseScheduler&)RSvoScheduler).takeControl(*this);
        }
		else
		{
			// $[TI2.4]
			// make sure trigger is valid:
			AUX_CLASS_ASSERTION_FAILURE(modeTriggerId);
		}
    }

}      

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description
// The method is invoked by the method relinquishControl() from the scheduler
// object that relinquishes control.  It accepts a breath phase scheduler
// reference as an argument. The base class method for takeControl() is invoked
// to handle activities common to all schedulers.  Peep recovery flag is set on
// transition from a non breathing mode. The method is responsible for
// extending the apnea interval when a transition to a breathing mode is in
// progress.  The NOV RAM instance rBdSystemState is updated with the new
// scheduler to enable BD system to properly recover from power interruption.
// The scheduler registers with the BreathPhaseScheduler as the current
// scheduler, and the mode triggers relevant to that mode are set to be
// enabled.  This method also interfaces with BdAlarms to post the Apnea
// Ventilation condition, which is then passed to the Alarm-Analysis subsystem,
// and this method notifies the BD Status task of the start of AV.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler reference passed as an argument is used to get the id of the
// current scheduler.  The static method of BreathPhaseScheduler is used to
// register the new scheduler. The BdSystemStateHandler is used to update the
// NOV RAM object for BdSystemState with the new scheduler id.  The private
// data member pModeTriggerList_ is passed as an argument to the method in the
// mode trigger mediator object that is responsible for changing the current
// mode trigger list.  RBdAlarms.postBdAlarm() is called to log the Apnea
// condition and VentAndUserEventStatus::PostEventStatus() is invoked to notify
// the BD Status Task of the start of AV.
//---------------------------------------------------------------------
//@ PreCondition
// The CLASS_PRE_CONDITION method is used to validate the IDs of the
// valid breath phase schedulers.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
ApneaScheduler::takeControl(const BreathPhaseScheduler& scheduler) 
{
    CALL_TRACE("ApneaScheduler::takeControl(const BreathPhaseScheduler& scheduler)");

    // Invoke the base class takeControl method
    BreathPhaseScheduler::takeControl(scheduler);
    
    SchedulerId::SchedulerIdValue schedulerId = scheduler.getId();

    // $[05080] 
    // notify alarms on apnea detection
    RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_IN_APNEA_VENTILATION);

	// $[BL04070] :e disable pause event request
	// $[BL04072] :e disable pause event request
	// $[BL04008] :e disable pending pause event
	// $[BL04075] :e cancel pause request
	// $[RM12077] cancel a NIF maneuver if Apnea is detected
	// $[RM12062] cancel a VC maneuver if Apnea is detected
	// $[RM12094] cancel a P100 maneuver if Apnea is detected
	Maneuver::CancelManeuver();

    // notify BD Status Task of start of AV
    VentAndUserEventStatus::PostEventStatus (EventData::APNEA_VENT, EventData::ACTIVE);

    if( (schedulerId == SchedulerId::DISCONNECT) || 
        (schedulerId == SchedulerId::STANDBY) ||
        (schedulerId == SchedulerId::SVO) ||
        (schedulerId == SchedulerId::OCCLUSION) ||
        (schedulerId == SchedulerId::POWER_UP) )
    {
    // $[TI3]
        BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
    }
    else
    {
    // $[TI4]
        CLASS_ASSERTION(
            (schedulerId == SchedulerId::SPONT) || 
            (schedulerId == SchedulerId::SIMV) || 
 	        (schedulerId == SchedulerId::BILEVEL) ||
            (schedulerId == SchedulerId::AC) ); 
    }

    // set the apnea active flag in nov ram
    BdSystemStateHandler::UpdateApneaActiveStatus(TRUE);
    
    // $[04124] update the reference to the newly active breath scheduler 
    BreathPhaseScheduler::SetCurrentScheduler_((BreathPhaseScheduler&)(*this));

    //The BreathRecord is not being updated with the new scheduler id, so that
    // the identity of the scheduler that originated this breath is maintained. 
    // until a new inspiration starts.
    
    // Initialize the cycle time to an arbitrary value. This value does not effect
    // ventilation.
    breathCycleTime_ = 10000;    

    // update the BD state, in case of power interruption
       BdSystemStateHandler::UpdateSchedulerId(getId()); 

    // Set the mode triggers for that scheduler.
    enableTriggers_();    
    RModeTriggerMediator.changeModeTriggerList(pModeTriggerList_);
} 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ApneaScheduler::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, APNEASCHEDULER,
                          lineNumber, pFileName, pPredicate);
}





//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: settingChangeHappened_
//
//@ Interface-Description
// $[04256] 
// The method takes a SettingId as an argument and has no return value.
// The method is called when a setting change is recognized, and before it
// is phased-in. One setting change event is currently considered:
// apnea rate change. There is no mode change that affects apnea ventilation.
// When apnea rate change is recognized, a new target for the breath cycle
// is calculated and is used to update the inspiratory time triggger. Non apnea
// rate change is ignored here.
//---------------------------------------------------------------------
//@ Implementation-Description
// The SettingId argument indicates which setting has changed.  When a rate
// change happens, a new breath cycle, based on the pending rate is calculated.
// If the new pending breath cycle is shorter than the current breath cycle,
// the instance of the inspiratory time trigger -- rTimeInspTrigger -- is
// updated, else the set breath period is restored.
//---------------------------------------------------------------------
//@ PreCondition
// The setting id is within the folowing range:
//   id >= SettingId::LOW_BATCH_BD_ID_VALUE && id <= SettingId::HIGH_BATCH_BD_ID_VALUE
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
ApneaScheduler::settingChangeHappened_(const SettingId::SettingIdType id) 
{
    CALL_TRACE("ApneaScheduler::settingChangeHappened_(const SettingId::SettingIdType id)");

    if(id == SettingId::APNEA_RESP_RATE)
    {
    // $[TI1]
        // pending respiratory rate setting
        const Real32 rate =
            PendingContextHandle::GetBoundedValue(SettingId::APNEA_RESP_RATE).value;

        // pending breath period:
        Int32 pendingBreathPeriod = (Int32)(60.0 * 1000.0 / rate);

        // set inspiratory time:
        Real32 inspTime =
            PhasedInContextHandle::GetBoundedValue(SettingId::APNEA_INSP_TIME).value;

        // the minimum breath cycle time during rate transition:
        Int32 minCycleTime = (Int32)(3.5 * inspTime);
        
        Int32 newBreathCycle = MAX_VALUE(minCycleTime, pendingBreathPeriod);

        // set the target time for the insp trigger to the
        // new pending breath cycle
        newBreathCycle = MIN_VALUE(newBreathCycle, breathCycleTime_);
        RTimeInspTrigger.setTargetTime(newBreathCycle);
    }
    else
    // $[TI2]
    {
        CLASS_ASSERTION( id >= SettingId::LOW_BATCH_BD_ID_VALUE &&
                     id <= SettingId::HIGH_BATCH_BD_ID_VALUE );
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts an EventId and a Boolean for the event status as
// arguments.  It returns an EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247] $[04259] $[04260] $[05016]
// The argument of type EventId indicates which user event is active.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is accepted by
// invoking a call to the static method:
// BreathPhaseScheduler::AcceptManualInspiration_(eventStatus).
// The alarm reset event is accepted by invoking a call to the static method:
// BreathPhaseScheduler::AcceptAlarmReset_(eventStatus).
// The expiratory and inspiratory pause events are rejected by invoking a call to
// the static methods: BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus) and
// BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus) .
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

EventData::EventStatus
ApneaScheduler::reportEventStatus_(const EventData::EventId id,
                                  const Boolean eventStatus)
{
    CALL_TRACE("ApneaScheduler::reportEventStatus_(const EventData::EventId id,\
                                                     const Boolean eventStatus)");

    EventData::EventStatus rtnStatus = EventData::IDLE;

    switch (id)
    {
        case EventData::MANUAL_INSPIRATION:
        // $[TI1]
            rtnStatus =
                    BreathPhaseScheduler::AcceptManualInspiration_(eventStatus);
            break;

        case EventData::ALARM_RESET:
        // $[TI2]
            rtnStatus =
                    BreathPhaseScheduler::AcceptAlarmReset_(eventStatus);
            break;

        case EventData::EXPIRATORY_PAUSE:
        // $[TI3]
            rtnStatus =
                    BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus);
            break;

        case EventData::INSPIRATORY_PAUSE:
        // $[TI4]
		// inspiratory pause shall not be active
		// $[BL04007]
            rtnStatus =
                    BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus);
            break;

		case EventData::NIF_MANEUVER:
		case EventData::P100_MANEUVER:
		case EventData::VITAL_CAPACITY_MANEUVER:
			// $[RM12032] RM maneuver shall be rejected in Apnea...
			rtnStatus = EventData::REJECTED;
			break;

        default:
        // $[TI6]
            AUX_CLASS_ASSERTION_FAILURE(id);
    }    
    return (rtnStatus);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableTriggers_
//
//@ Interface-Description
// $[04262] $[04125] $[04130] $[04141]
// The method takes no arguments and returns no value.  A request to enable AC
// scheduler valid mode triggers is issued whenever apnea is taking control.
// Immediate triggers are not set here since by design they are only set once
// the trigger they instanciate becomes active.
//---------------------------------------------------------------------
//@ Implementation-Description
// The mode trigger instances are accessed directly with a call to their
// setIsEnableRequested() method.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
ApneaScheduler::enableTriggers_(void) 
{
    // $[TI1]
    CALL_TRACE("ApneaScheduler::enableTriggers_(void)");

    ((ModeTrigger&)RApneaAutoResetTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)ROcclusionTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RDisconnectTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RSvoTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RApneaTrigger).setIsEnableRequested(TRUE);

    // Note that the immediate, or event driven triggers are not
    // enabled here: RManualResetTrigger
}

//=====================================================================
//
// private methods
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startInspiration_ 
//
//@ Interface-Description
// The method takes a breath trigger reference, a breath phase pointer, and a
// breath type as arguments. It does not return any value.  When the next phase
// is determined to be inspiration, the method is called to perform all the
// standard procedures required to start inspiration: Any pending mode or rate
// change status is cleared, new settings are phased in, a new breath record is
// created, the oxygen mix, apnea interval and peep get updated, the breath
// trigger list is changed for inspiration list, and the previous breath
// relinquishes control. For control or assist breath type, the time insp
// trigger is set and enabled to determine the time of the next inspiration,
// and the breath phase is set to reference the current mandatory breath phase.
// For spont breath type, the peep recovery state is set to active and the
// breath phase is set to reference the PeepRecovery breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The method collaborates with the handle PhasedInContextHandle to phase in new
// settings. It creates a new breath record using the object rBreathSet.  The
// actual oxygen mix, apneaInterva, and peep are updated using the objects
// rO2Mixture, rApneaInterval, and rPeep.  The breath trigger mediator instance
// rBreathTriggerMediator is used to set the inspiratory breath trigger list.
// The breath trigger instance rTimeInspTriger is restarted with the new cycle
// time. The breath phase instance pointed to by the argument pBreathPhase is
// instructed to phase out.  The static method BreathPhase::SetCurrentBreathPhase
// is used to set up the new mandatory breath type.
//---------------------------------------------------------------------
//@ PreCondition
// Check ranges for mandatory type and breath type. Check peep recovery flag for
// START in case of a spont breath.
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
ApneaScheduler::startInspiration_(const BreathTrigger& breathTrigger, BreathPhase* pBreathPhase,
 BreathType breathType)
{
    CALL_TRACE("ApneaScheduler::startInspiration_(const BreathTrigger& breathTrigger,\
                BreathPhase* pBreathPhase, BreathType breathType)");

    // pointer to a breath phase
    BreathPhase* pPhase = NULL;
    // variable to store setting values:
    DiscreteValue mandatoryType = 0;
    // flags peep recovery in process
    Boolean isPeepRecovery = TRUE;

    BreathPhaseScheduler::ModeChange_ = FALSE;    
    BreathPhaseScheduler::RateChange_ = FALSE;

    //Phase in new settings (for start of inspiration)
    PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

    //Reset currently active triggers and setup triggers that are active during inspiration:
    RBreathTriggerMediator.resetTriggerList();
    RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::INSP_LIST);

    if(breathType == ::CONTROL || breathType == ::ASSIST)
    {
    // $[TI1]
        isPeepRecovery = FALSE;
        // get the mandatory type
        mandatoryType = PhasedInContextHandle::GetDiscreteValue(SettingId::APNEA_MAND_TYPE);
        CLASS_PRE_CONDITION( (mandatoryType == MandTypeValue::VCV_MAND_TYPE) ||
                                        (mandatoryType == MandTypeValue::PCV_MAND_TYPE) );
        // $[04256] register new phase and prepare to deliver a mandatory inspiration:
        pPhase = (mandatoryType == MandTypeValue::VCV_MAND_TYPE) ?
                                        (BreathPhase*)&RApneaVcvPhase : // $[TI1.1]
                                        (BreathPhase*)&RApneaPcvPhase;   // $[TI1.2]
        // $[04009] 
        //Set the time trigger for next inspiration, start its timer and enable the
        //trigger. The trigger is actually re-enabled here after being disabled
        //previously by BreathTriggerMediator method resetTriggerList().
        // $[04256]respiratory rate setting
        const Real32 rate =
            PhasedInContextHandle::GetBoundedValue(SettingId::APNEA_RESP_RATE).value;
        breathCycleTime_ = (Int32)(60.0F * 1000.0F / rate);
        RTimeInspTrigger.restartTimer(breathCycleTime_);
        RTimeInspTrigger.enable();
    }
    else if (breathType == ::SPONT)
    {
    // $[TI2]
        isPeepRecovery = TRUE;
        // deliver a peep recovery phase
        // Set the PeepRecovery_ variable to ACTIVE to flag the activation of the ASAP trigger
        // on the start of the next exhalation. 
        CLASS_ASSERTION(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::START);
        BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::ACTIVE;
        mandatoryType = ::NULL_MANDATORY_TYPE;
        pPhase = (BreathPhase*)&RPeepRecoveryPhase;
    }
    else
    {
    // $[TI3]
        CLASS_PRE_CONDITION( (breathType == ::CONTROL) ||
                             (breathType == ::ASSIST) ||
                             (breathType == ::SPONT) );
    }

    // phase-out current breath phase
    pBreathPhase->relinquishControl(breathTrigger);

    // Once settings are phased in, and the new scheduler is registered,
    // determine the O2 mix, apnea,  and peep for this phase (have to start apnea timer
    // to keep track on apnea elapse time upon apnea reset):
    RO2Mixture.determineO2Mix(*this, (BreathPhase&)*pPhase);
    RApneaInterval.newPhase(BreathPhaseType::INSPIRATION);
    RPeep.updatePeep(BreathPhaseType::INSPIRATION);

	if (pPhase == (BreathPhase*)&RPeepRecoveryPhase)
	{	// $[TI4]
	    RPeep.setPeepChange( TRUE) ;
	}	// $[TI5]

    BreathPhase::SetCurrentBreathPhase((BreathPhase&)*pPhase, breathTrigger);
    //$[04257] Update the breath record
    RBreathSet.newBreathRecord(SchedulerId::APNEA, mandatoryType, breathType,
                                    BreathPhaseType::INSPIRATION, isPeepRecovery);
    // set the breath record with the breath cycle time:
    BreathRecord::SetBreathPeriod((Real32)breathCycleTime_);
    // start inspiration
	BreathPhase::NewBreath() ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startExhalation_ 
//
//@ Interface-Description
// The method takes a breath trigger reference, a breath phase pointer, a
// breath type as arguments, and a null inspiration boolean flag. It does
// not return any value.
//---------------------------------------------------------------------
//@ Implementation-Description
// The phase-in setting context is used to phase-in settings for exhalation.
// Reset currently active triggers and setup triggers that are active during
// expiration.  Turn peep recovery off for peep recovery inspiration.
// The applied O2 percent is updated if necessary, the apnea interval is
// updated for start of exhalation, and the breath phase gets updated.
// Create a new breath record with NULL_INSPIRATION phase type when peep
// reduction occurs due to transition from high peep bilevel state to
// this scheduler.  Otherwise, update the breath record.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
ApneaScheduler::startExhalation_(const BreathTrigger& breathTrigger,
                              BreathPhase* pBreathPhase,
                              const Boolean noInspiration)
{
	// Phase in new settings (for start of exhalation)
	PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_EXHALATION);

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_EXH);
#endif // INTEGRATION_TEST_ENABLE
    
	// Reset currently active triggers and setup triggers that are active during
	// inspiration:
	RBreathTriggerMediator.resetTriggerList();
	RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);

	if (BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::ACTIVE)
	{   // $[TI3.1]
		// $[04323]
		BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::OFF;
		((BreathTrigger&)RPeepRecoveryMandInspTrigger).enable();
	}    // $[TI3.2]

	// phase-out current breath phase
	pBreathPhase->relinquishControl(breathTrigger);
            
	// Once settings are phased in, determine the O2 mix, apnea, and peep.
	RO2Mixture.determineO2Mix(*this, (BreathPhase&)RExhalationPhase);
	RApneaInterval.newPhase(BreathPhaseType::EXHALATION);
	RPeep.updatePeep(BreathPhaseType::EXHALATION);

	// register new phase:
	BreathPhase::SetCurrentBreathPhase((BreathPhase&)RExhalationPhase, breathTrigger);

    if(noInspiration)
    {
    // $[TI2]
        //Update the breath record
        RBreathSet.newBreathRecord(SchedulerId::APNEA,
               ::NULL_MANDATORY_TYPE,
               RBiLevelScheduler.getFirstBreathType(),
               BreathPhaseType::NULL_INSPIRATION,
               FALSE);
    }
    else
    {
    // $[TI1]
        //Update the breath record
        (RBreathSet.getCurrentBreathRecord())->newExhalation();
    }

	// start delivering exhalation
	BreathPhase::NewBreath() ;
}


