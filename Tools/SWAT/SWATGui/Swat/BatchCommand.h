/// @file BatchCommand.h
///
/// @brief
/// Class deffinition for BatchCommand.
///
/// A BatchCommand repersents a single command.


#ifndef BATCHCOMMAND_H
#define BATCHCOMMAND_H
#include "NetworkMessageHeader.h"
#include "uuid.h"

//namespace swat{


    /// @class BatchCommand
    /// 
    /// To add a batch command, you will need to
    /// <ol>
    /// <li>Add to enum a new CMD_ in BatchCommand
    /// <li>Extend COAction and create a class definition for action in Actions.h
    /// <li>Add new class as friend in class BatchExecuter in file BatchExecuter.h 
    /// <li>Add implimentation for new class in Actions.h.
    /// <li>Add to DispatchContext::continueAction the dispatching to the command.
    /// </ol>
    class BatchCommand{
        public:


            ///
            /// Enumeration of commands.
            enum{
                CMD_WAITTIME , 
                CMD_WAITEVENT,
                CMD_END,
                CMD_SETSENSOR,
                CMD_UNSETSENSOR,
                CMD_RESETALL,
                CMD_SIGNALEVENT,
                CMD_REPEATN,
                CMD_REPEATTIME,
                CMD_REPEATEVENT,
                CMD_MARKEVENT,
                CMD_MARK,
                CMD_SETSENSORBUFFERVALUE,
                CMD_SETINDEX,
                CMD_SKIPINDEX,
                CMD_ADDSENSOR,

				CMD_SETRAWSENSOR,
                CMD_UNSETRAWSENSOR,
                CMD_ADDRAWSENSOR,

				CMD_SIMULATE_TOUCH,
				CMD_SIMULATE_BEZEL
				                
            }; 

            ///
            /// @brief Constructor
            /// 
            /// Possible usages
            /// <ul>
            /// <li>BatchCommand(BatchCommand::CMD_SETSENSOR,0,0,fValue,iSensorId); 
            /// <li>BatchCommand(BatchCommand::CMD_MARK       ,1  ,0    ,0.0f  ,0        );
            /// <li>BatchCommand(BatchCommand::CMD_REPEATN    ,0  ,0    ,0.0f  ,iCnt     );
            /// <li>BatchCommand(BatchCommand::CMD_REPEATTIME ,0  ,iCnt ,0.0f  ,0        );
            /// <li>BatchCommand(BatchCommand::CMD_WAITTIME   ,0  ,iTime,0.0f  ,0        );
            /// <li>BatchCommand(BatchCommand::CMD_END        ,0  ,0    ,0.0f  ,0        );
            /// <li>BatchCommand(BatchCommand::CMD_UNSETSENSOR,0  ,0    ,0.0f  ,iSensorId);
            /// </ul>
            /// 
            /// @param iCommandId Command ID
            /// @param iWaitEventId EventId to wait for.
            /// @param iWaitTime Time argument.
            /// @param fArg Floating point argument.
            /// @param iArg Integer argument.
            BatchCommand(
                    unsigned int iCommandId  ,
                    unsigned int iWaitEventId,
                    unsigned int iWaitTime   ,
                    float        fArg        ,
                    unsigned int iArg
                    ):
                m_CommandId(iCommandId)   
            {
            }   

            BatchCommand(
                    unsigned int iCommandId
                    ):
                m_CommandId(iCommandId)

            {
            }   


            BatchCommand(
                    unsigned int iCommandId,
                    unsigned int iArg1
                    ):
                m_CommandId(iCommandId)

            {
                m_Arg[0].ui=iArg1;
            }   

            BatchCommand(
                    unsigned int iCommandId,
                    int iArg1
                    ):
                m_CommandId(iCommandId)

            {
                m_Arg[0].i=iArg1;
            }   

            BatchCommand(
                    unsigned int iCommandId,
                    unsigned int iArg1,
                    float iArg2
                    ):
                m_CommandId(iCommandId)

            {
                m_Arg[0].ui=iArg1;
                m_Arg[1].f =iArg2;
            }   

            BatchCommand(
                    unsigned int iCommandId,
                    unsigned int iArg1,
                    unsigned int iArg2
                    ):
                m_CommandId(iCommandId)

            {
                m_Arg[0].ui=iArg1;
                m_Arg[1].ui =iArg2;
            }   


            ///
            /// @brief Constructor
            /// 
            BatchCommand():
                m_CommandId(0)  
            {
            }


            inline unsigned int GetCommandId  ()
            {
                return m_CommandId;
            }   

            inline float GetFArg(unsigned int n)
            {
                return m_Arg[n].f;
            }

            inline int GetIArg(unsigned int n)
            {
                return m_Arg[n].i;
            }
            inline unsigned int GetUIArg(unsigned int n)
            {
                return m_Arg[n].ui;
            }

        private:

            unsigned int m_CommandId  ;

            union {
                unsigned int ui;
                int           i;
                float         f;
            } m_Arg[2];





    };


//}

#endif



