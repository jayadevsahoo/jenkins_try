
/*****************************************************************************/
/*   Module Name:   MsgPopup.h                                          
/*   Purpose:       Contains definitions to handle generic popup dialogs.
/*					The following dialog box types are handled:
/*					- OK button only
/*					- OK and Cancel button
/*					- OK, Cancel and help button
/*					- OK and Help button
/*					- Data entry dialog with OK button
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Dynamically resize to accomodate       Dave Richardson   20 Nov, 2002
/*   variable length text
/*    
/*   $Revision::   1.2      $                                                
/*                                                                          
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/testutil/MsgPopup.h-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:36   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   20 Nov 2002 14:46:42   Richardson
 * Updated per SCRs # 27

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

#ifndef _MSGPOPUP
#define _MSGPOPUP

// Types of popups:
typedef enum {
	OK_ONLY_DLG=0,
	OK_CANCEL_DLG,
	OK_CANCEL_HLP_DLG,
	OK_HLP_DLG,
	OK_DATA_ENTRY_DLG
} eDIALOGTYPE;

#define NO_HELP 0	// If no help is applicable to this dialog

//#define OK     1
//#define CANCEL 0

// Function Prototypes:
int CPVTMessagePopup (char Title[], char Message[], eDIALOGTYPE dlgType, int helpID);
int CPVTConfirmPopup (char Title[], char Message[], eDIALOGTYPE dlgType, int helpID);
int CPVTPromptPopup (char title[], char message[], eDIALOGTYPE dlgType, int helpID, 
						char* responseBuffer, int maxResponseLen);

#endif // #ifndef _MSGPOPUP
