#ifndef Triangle_HH
#define Triangle_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//						Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Triangle - This is a drawable triangle class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Triangle.hhv   25.0.4.0   19 Nov 2013 14:11:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 004   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 003  By: gdc      Date: 15-Feb-1996  DCR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 002  By: mpm      Date: 02-Aug-1995  DCR Number: 486
//    Project:  Sigma (R8027)
//    Description:
//  
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "Drawable.hh"

//@ Usage-Classes
#include "Colors.hh"
//@ End-Usage

class Triangle : public Drawable
{
public:

	Triangle(Int16 pt1X, Int16 pt1Y, Int16 pt2X, Int16 pt2Y,
			 Int16 pt3X, Int16 pt3Y);
	virtual ~Triangle(void);

	void setFillColor (Colors::ColorS color);

	virtual void setX(Int newX);
	virtual void setY(Int newY);
	virtual void draw(const Area&);

	static void SoftFault(const SoftFaultID  softFaultID,
						  const Uint32	lineNumber,
						  const char*	pFileName  = NULL,
						  const char*	pPredicate = NULL);
protected:

private:

	//@ Data-Member: pt1X_
	// The X coordinate of the triangle's pt1.
	Int16 pt1X_;

	//@ Data-Member: pt1Y_
	// The Y coordinate of the triangle's pt1.
	Int16 pt1Y_;

	//@ Data-Member: pt2X_
	// The X coordinate of the triangle's second point.
	Int16 pt2X_;

	//@ Data-Member: pt2Y_
	// The Y coordinate of the triangle's second point.
	Int16 pt2Y_;

	//@ Data-Member: pt3X_
	// The X coordinate of the triangle's third point.
	Int16 pt3X_;

	//@ Data-Member: pt3Y_
	// The Y coordinate of the triangle's third point.
	Int16 pt3Y_;
 
	//@ Data-Member: fillColor_
	//  fillColor_ is used to store the color of the box.
	Colors::ColorS fillColor_;
};


#endif // Triangle_HH 
