#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  TubeTypeSetting - Tube Type Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the type of tube
//  used on the current patient.  This class inherits from
//  'BatchDiscreteSetting' and provides the specific information needed for
//  the representation of the patient tube type.  This information includes
//  the set of values that this setting can have (see 'TubeTypeValue.hh'),
//  and this batch setting's new-patient value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TubeTypeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 005  By: sah     Date:  02-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added support for new 'VS' spontaneous type
//
//  Revision: 004   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//	PAV project-related changes:
//      *  added support for new 'PA' spontaneous type
//
//  Revision: 003   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 002   By: sah   Date:  06-Jan-1999    DR Number:  5314
//  Project:  ATC
//  Description:
//     Added new 'getApplicability()' method.
//
//  Revision: 001   By: sah   Date:  06-Jan-1999    DR Number:  5322
//  Project:  ATC
//  Description:
//	New ATC-specific setting.
//
//=====================================================================

#include "TubeTypeSetting.hh"
#include "TubeTypeValue.hh"
#include "SupportTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
//@ End-Usage

//@ Code...
static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"TRACH", TubeTypeValue::TRACH_TUBE_TYPE},
	{"ET", TubeTypeValue::ET_TUBE_TYPE},
	{NULL, NULL}
};

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  TubeTypeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TubeTypeSetting::TubeTypeSetting(void)
	: BatchDiscreteSetting(SettingId::TUBE_TYPE,
			       Setting::NULL_DEPENDENT_ARRAY_,
			       TubeTypeValue::TOTAL_TUBE_TYPE_VALUES)
{
	setLookupTable(ParameterLookup_);
}  // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~TubeTypeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TubeTypeSetting::~TubeTypeSetting(void)
{
  CALL_TRACE("~TubeTypeSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01081] -- breath settings applicability
//  $[01117] -- "more" settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
TubeTypeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  Applicability::Id  applicability;

  if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::ATC)  ||
      SoftwareOptions::IsOptionEnabled(SoftwareOptions::PAV))
  {  // $[TI1] -- TC and/or PA is an active option...
    const Setting*  pSupportType =
			SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

    if (pSupportType->getApplicability(qualifierId) !=
						Applicability::INAPPLICABLE)
    {  // $[TI1.1] -- support type is applicable...
      DiscreteValue  supportTypeValue;
      
      switch (qualifierId)
      {
      case Notification::ACCEPTED :		// $[TI1.1.1]
	supportTypeValue = pSupportType->getAcceptedValue();
	break;
      case Notification::ADJUSTED :		// $[TI1.1.2]
	supportTypeValue = pSupportType->getAdjustedValue();
	break;
      default :
	AUX_CLASS_ASSERTION_FAILURE(qualifierId);
	break;
      }

      switch (supportTypeValue)
      {
      case SupportTypeValue::PSV_SUPPORT_TYPE :
      case SupportTypeValue::VSV_SUPPORT_TYPE :
      case SupportTypeValue::OFF_SUPPORT_TYPE :	// $[TI1.1.4]
	applicability = Applicability::INAPPLICABLE;
	break;
      case SupportTypeValue::ATC_SUPPORT_TYPE :
      case SupportTypeValue::PAV_SUPPORT_TYPE :	// $[TI1.1.3]
	applicability = Applicability::CHANGEABLE;
	break;
      default :
	// unexpected 'supportTypeValue'...
	AUX_CLASS_ASSERTION_FAILURE(supportTypeValue);
	break;
      };
    }
    else
    {  // $[TI1.2] -- support type is NOT applicable...
      // tube type can't be applicable if support type isn't...
      applicability = Applicability::INAPPLICABLE;
    }
  }
  else
  {  // $[TI2] -- TC and PA are not active options...
    applicability = Applicability::INAPPLICABLE;
  }

  return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
TubeTypeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  DiscreteValue  newPatientValue;

  // $[TC02036] -- setting's new-patient value...
  newPatientValue = TubeTypeValue::ET_TUBE_TYPE;

  return(newPatientValue);
}  // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TubeTypeSetting::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          TUBE_TYPE_SETTING, lineNumber, pFileName,
                          pPredicate);
}
