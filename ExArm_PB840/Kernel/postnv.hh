#ifndef postnv_HH
#define postnv_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: postnv.hh
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/postnv.hhv   25.0.4.0   19 Nov 2013 14:13:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 004    By: Gary Cederquist  Date: 06-JUN-1997  DR Number: 2183
//    Project: Sigma (R8027)
//    Description:
//       Added sense information field.
//
//  Revision: 003    By: Gary Cederquist  Date: 15-MAY-1997  DR Number: 2085
//    Project: Sigma (R8027)
//    Description:
//       Added auxiliary data region while maintaining compatibility
//       with current FLASH software.
//
//  Revision: 002    By: Gary Cederquist  Date: 08-APR-1997  DR Number: 1901
//    Project: Sigma (R8027)
//    Description:
//       Modified to use symbolic reference for NOVRAM base address.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================


#include "Sigma.hh"
#include "MemoryMap.hh"
#include "kpost.hh"
#include "kerrors.hh"
#include "AuxResultLog.hh"

enum
{
    NUM_UNIT_TESTS = 40
};

#define SCHEDULE_DOWNLOAD      (0xFEEDC0DE)
#define KERNEL_NOVRAM_VERSION  (0xFA07)


struct KernelNovram
{
   //  lastTestInProgress and errorCode are referenced by assembly
   //  routines so don't move them from the beggining of this struct
   volatile Uint8         lastTestInProgress;
   volatile Uint8         errorCode;

   Uint16                 versionCode;
   volatile Uint32        nvPatternTest;
   Uint32                 downloadScheduled;   // 0xFEEDC0DE for download
   volatile Uint8         shutdownState;
   Uint8                  rollingThunderCount;
   Uint8                  checksumIndexInProgress;
   volatile Uint8         ioReg1Cache;          // write-only register cache
   volatile Boolean       postFailed;
   volatile Boolean       testingWatchdog;
   Uint8                  watchdogRetries;
   volatile Boolean       watchdogMinTimeExpired;
   volatile Boolean       unitTestWatchdog;
   volatile Boolean       unitTestRollingThunder;
   volatile Uint8         results[POST_ERROR_MAX_TEST+1];
   AuxResultLog           auxResultLog;
   Uint8                  space[POST_ERROR_MAX_TEST+1-sizeof(AuxResultLog)];
   Boolean                watchdogOccurred;
   Uint32                 sense;
};


//  defined as an extern so emulator can reference more easily
//extern KernelNovram * const PKernelNovram;
#ifdef E600_840_TEMP_REMOVED
static KernelNovram * const PKernelNovram = (KernelNovram*)KERNEL_NOVRAM_BASE;
#endif

static KernelNovram TempKernelNovram;           // This will give the pointer something to reference
static KernelNovram * const PKernelNovram = &TempKernelNovram;

#endif // postnv_HH
