#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: Vga.cc - VGA register access routines.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a set of methods for manipulating the VGA display
//  registers.  All of these methods are private, because only fellow
//  VGA routines need this access (see the list of "friends").
//---------------------------------------------------------------------
//@ Rationale
//  Isolate video access.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The full suite of register manipulation is provided via private
//  methods, so that only this class's methods directly manipulate the
//  internal tables and pointers.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/Vga.ccv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 005   By:gfu     Date:  25-OCT-2004    DCS number: 6145
//  Project New LCD 
//  Description:
//  Modified initialization sequence for color displays to support new LCDs
//
//  Revision: 004   By:erm     Date:  30-OCT-2201    DCS number: 5493
//  Project GuiComms
//  Description:
//  Implemented single screen compatiblity though command line option
//
//  Revision: 003   By: gdc    Date:  13-MAR-2001    DCS Number: 5788
//  Project: GuiComms
//  Description:
//	Corrected module ID name in SoftFault.
//
//  Revision: 002   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms 
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//=====================================================================  

#include "Vga.hh"

//@ Usage-Classes
// TODO E600 remove
//#include "OsUtil.hh"     // for IsUpperDisplayColor()/IsLowerDisplayColor()
#include "MemoryMap.hh"
#include "Task.hh"
#include "VgaDevice.hh"
#include "NmiSource.hh"
#include "CpuDevice.hh"
#include "NovRamManager.hh"
//@ End-Usage

//=====================================================================  
//
//  Static Data Definitions...
//
//=====================================================================  

// VGA Sequencer Registers, values for the 5 main registers
static const Uint8  SEQUENCER_REGS_MODE79_[] =
{
    0x03, 0x01, 0x0f, 0x00, 0x0A
};

// VGA CRT Control Registers
static const Uint8 CRT_CONTROL_REGS_MODE79_[] =
{
    0x61, 0x4f, 0x50, 0x82, 0x53, 0x9f, 0x0b, 0x3e, 	// 00-07
	0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 	// 08-0F
	0xea, 0x0c, 0xdf, 0x50, 0x00, 0xe7, 0x04, 0xe3,		// 10-17
	0xff
};

// Graphics Control Registers, values for the 8 main registers
static const Uint8  GRAPHICS_CONTROL_REGS_MODE79_[] =
{
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0f, 0xff
};

// VGA Attribute Registers
static const Uint8  ATTRIBUTE_REGS_MODE79_[] =
{
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 	// 00-07
	0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 	// 08-0f
	0x01, 0x00, 0x0f, 0x00, 0x00						// 10-14
};

// VGA extended registers,  these are in addition to the Extended
// Registers, indexed at 0x3D6 and data entered at 0x3D7.

//
// These color display values are optimized for simultaneous 
// 640x480 VGA to the 840 TFT panel and a VGA CRT display.
// HSync = 31.47 KHz  HSync pulse width = 4.1uS
// VSync = 59.94 Hz   VSync pulse width = 63.56 uS
//
static const Uint8  EXTENDED_REGS_COLOR_MODE79_[] =
{
	0xD8, 0xFF, 0x81, 0x02, 0x24, 0x00, 0xC0, 0xF4,		// 0 - 7
	0x34, 0x00, 0x00, 0x15, 0x00, 0x00, 0x80, 0x01,		// 8 - F
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		// 10-17
	0xFF, 0x53, 0x00, 0x61, 0x4F, 0x7F, 0xFF, 0x02,		// 18-1F
	0x00, 0x00, 0x00, 0x00, 0x12, 0x59, 0x00, 0x00,		// 20-27
	0x10, 0x4C, 0x00, 0x12, 0x1F, 0x4F, 0x4F, 0x0F,		// 28-2F
	0x03, 0x4E, 0x59, 0x00, 0x00, 0x00, 0x00, 0x00,		// 30-37
	0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,		// 38-3F
	0x01, 0x00, 0x00, 0x00, 0x10, 0x04, 0x00, 0x00,		// 40-47
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x44,		// 48-4F
	0x02, 0xC4, 0x41, 0x0C, 0xF8, 0xE5, 0x00, 0x1B,		// 50-57  54 change to f8 from fa
	0x00, 0x84, 0x00, 0x8F, 0x02, 0x10, 0x80, 0x06,		// 58-5F
	0x88, 0x2E, 0x07, 0x01, 0x0B, 0x26, 0xEA, 0x0C,		// 60-67
	0xDF, 0x00, 0x00, 0x00, 0x0B, 0x00, 0xBD, 0x00,		// 68-6F
	0x80, 0x00, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00,		// 70-77
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3C		// 78-7F
};

static const Uint8  EXTENDED_REGS_GRAYSCALE_MODE79_[] =
{
    0xD8, 0xFF, 0x81, 0x02, 0x24, 0x00, 0x02, 0xF4,		// 0 - 7
    0x34, 0x00, 0x00, 0x15, 0x00, 0x00, 0x80, 0x02,		// 8 - F
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,		// 10-17
    0xFF, 0x55, 0x00, 0x5F, 0x4F, 0x7F, 0xFF, 0x03,		// 18-1F
    0x00, 0x00, 0x00, 0x00, 0x12, 0x59, 0x00, 0x00,		// 20-27
    0x10, 0x4C, 0x00, 0x12, 0x21, 0x50, 0x50, 0x00,		// 28-2F
    0x03, 0x4E, 0x59, 0x00, 0x00, 0x00, 0x00, 0x00,		// 30-37
    0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,		// 38-3F
    0x01, 0x00, 0x00, 0x00, 0x10, 0x04, 0x00, 0x00,		// 40-47
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x44,		// 48-4F
    0x21, 0x67, 0x41, 0x0C, 0x3A, 0xE5, 0x00, 0x23,		// 50-57
    0x00, 0x84, 0x04, 0x8F, 0x02, 0x10, 0x80, 0x06,		// 58-5F
    0x88, 0x2E, 0x07, 0x01, 0x0B, 0x26, 0xEA, 0x0C,		// 60-67
    0xDF, 0x00, 0x00, 0x00, 0x0B, 0x00, 0x26, 0x1B,		// 68-6F
    0x80, 0x00, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00,		// 70-77
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3C 		// 78-7F
};

static const Uint32 VRAM_CHIP_SIZE_ = 524288;  // 512KB

//=====================================================================  
//
//  Static Member Definitions...
//
//=====================================================================  

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Vga [constructor]
//
//@ Interface-Description
//  This method constructs a Vga object and initializes the VGA 
//  registers associated with the specified display. The caller specifies
//  a VGA base address and a mutex ID which is used for synchronized
//  access to this object and its VGA resources.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

Vga::Vga( Uint vgaBaseAddress, Int32 mutexId )
  : pVgaBase_( (Byte*)vgaBaseAddress )
  , pDisplayMemory_( (Byte *)(pVgaBase_ + DISPLAY_MEM) )
  , mutex_( mutexId )
  , frameCount_( 1 )
{
  initMode79();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Vga [destructor]
//
//@ Interface-Description
//  Destructs the Vga object.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

Vga::~Vga( void )
{
  // do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Vga::SoftFault(const SoftFaultID softFaultID,
			const Uint32      lineNumber,
			const char*       pFileName,
			const char*       pPredicate)
{
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
			  VgaGraphicsDriver::VGA_ID, 
			  lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initMode79()
//
//@ Interface-Description
//  Initializes the specified video controller (CHIPS 65545) for mode 79.
//  This mode establishes a 640x480, 256 color, packed pixel display
//  mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize the display for the CRT
//  Wakeup VGA controller.
//  Start frequency synthesizer
//  Delay minimum of 30 ms
//  Set FCTL
//  Remove Write-Protect from CRO-7
//  Load CRTC registers
//  Load Graphics Registers
//  Load Sequencer Registers
//  Load extented registers
//  Call initializePalette()
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

void
Vga::initMode79(void)
{
  // Wakeup VGA controller     
  setReg(VSE, 0xFF);      // Wakeup VGA controller  
  setReg(MSR, 0x01);      // Select 3Dxh for CRTC, FCR, and ST01

  // set up the linear base address
  setReg(XR08, 0x34);

  // enable linear addressing
  setReg(XR0B, 0x10);

  // set up i/o base for 32-bit registers before enabling access
  setReg(XR07, 0xF4);

  // set m clock VCO to go as fast as possible, 65 mHz
  setReg(XR33, 0x20);
  setReg(XR30, 0x03);
  setReg(XR31, 0x52);
  setReg(XR32, 0x23);

  // Delay minimum of 30mS waiting for clock to stabilize
  Task::Delay(0, 40/*ms*/);

  // done with the general chip set up, now set up VGA mode 79

  // select Attribute Index Reg by reading ST01
  selectAttributeIndexReg();

  // Disable video, allowing CPU access to Attribute Color Regs
  setReg(ARX, 0);

  // force synchronous reset
  setReg(SR00, 0x01);

  setReg(MSR, 0xE3);  // Select 3Dxh for CRTC, FCR, and ST01
  setReg(FCR, 0x00);  // FCTL  

  Uint  i;

  // Load Sequencer Registers  
  // SKIP THE SEQUENCER RESET REGISTER (0), start i=1 to skip
  for (i=1; i < sizeof(::SEQUENCER_REGS_MODE79_); i++) 
  {
    setReg(SequencerRegIndex(i), ::SEQUENCER_REGS_MODE79_[i]);
  }

  // Load CRTC registers  
  for (i = 0; i < sizeof(::CRT_CONTROL_REGS_MODE79_); i++) 
  {
    setReg(CrtRegIndex(i), ::CRT_CONTROL_REGS_MODE79_[i]);
  }
  
  // Load Graphics Registers  
  for (i=0; i < sizeof(::GRAPHICS_CONTROL_REGS_MODE79_); i++) 
  {
    setReg(GraphicsRegIndex(i), ::GRAPHICS_CONTROL_REGS_MODE79_[i]);
  }

  // select Attribute Index Reg by reading ST01
  selectAttributeIndexReg();

  for (i = 0; i < sizeof(::ATTRIBUTE_REGS_MODE79_); i++)
  {
    setReg(AttributeRegIndex(i), ::ATTRIBUTE_REGS_MODE79_[i]);
  }

  // set extended registers
  const Uint8 * pExtendedRegs = NULL;
  Uint extendedRegCount = 0;

  if ( IsUpperDisplayColor() )
  {		// $[TI1.1]
    pExtendedRegs = ::EXTENDED_REGS_COLOR_MODE79_;
    extendedRegCount = sizeof(::EXTENDED_REGS_COLOR_MODE79_);
  }
  else
  {		// $[TI1.2]
    pExtendedRegs = ::EXTENDED_REGS_GRAYSCALE_MODE79_;
    extendedRegCount = sizeof(::EXTENDED_REGS_GRAYSCALE_MODE79_);
  }

  for (i=0; i < extendedRegCount; i++ )
  {
    if (i == 0x30)
    {
       setReg(ExtensionRegIndex(0x33), pExtendedRegs[0x33]);
    }
    else if (i == 0x33)
    {
       continue;
    }
 
    setReg(ExtensionRegIndex(i), pExtendedRegs[i]);
  }	

  memorySize_ = getMemorySize_();

  // set 16-bit data path for single DRAM
  if ( memorySize_ == VRAM_CHIP_SIZE_ )
  {		// $[TI3.1]
  	setReg(XR04, pExtendedRegs[4] | 0x01);
  }		// $[TI3.2]

  selectAttributeIndexReg();
  setReg(ARX, 0x20);

  // enable vga - set for normal operation
  setReg(SR00, 0x03);
  
  // load ramdac  
  initializePalette_();

  // finished with initialization of CHIPS 65545
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMemorySize_
//
//@ Interface-Description
//  Return the size of VGA memory in bytes. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Write addresses to VGA memory at 512KB boundaries and read back and
//  match to determine if memory is present or not.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

Uint
Vga::getMemorySize_(void)
{
  const Uint32 LOWER_MEMORY_ADRS = (Uint32)(pDisplayMemory_);
  const Uint32 UPPER_MEMORY_ADRS = (Uint32)(pDisplayMemory_+VRAM_CHIP_SIZE_);

  volatile Uint32 *pLowerMemory = (Uint32*)LOWER_MEMORY_ADRS;
  volatile Uint32 *pUpperMemory = (Uint32*)UPPER_MEMORY_ADRS;

  Uint32 memorySize = VRAM_CHIP_SIZE_;

  *pLowerMemory = LOWER_MEMORY_ADRS;
  *pUpperMemory = UPPER_MEMORY_ADRS;

  if ( *pLowerMemory == LOWER_MEMORY_ADRS &&
	   *pUpperMemory == UPPER_MEMORY_ADRS )
  { // $[TI1.1]
	memorySize *= 2;
  } // $[TI1.2]

  return memorySize;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initializePalette_
//
//@ Interface-Description
//  Initialize the RAMDAC with zeros to display black initially.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize the ramdac on the video chip
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

void
Vga::initializePalette_(void)
{
  // clear colormap cache
  for (Int i=0; i<countof(colormapCache_); i++)
  {
    colormapCache_[i].rgbRed = 0;
    colormapCache_[i].rgbGreen = 0;
    colormapCache_[i].rgbBlue = 0;
    colormapCache_[i].rgbReserved = 0;
  }

  setReg(DACMASK, 0xFF);
  setReg(DACRX, 0x0);
  setReg(DACWX, 0x0);

  for (Uint j = 0; j<256; j++)
  {
    setReg(DACDATA, 0);         // write red  
    setReg(DACDATA, 0);         // write green  
    setReg(DACDATA, 0);         // write blue  
  }
  // $[TI1]
}   


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isSameVideo_
//
//@ Interface-Description
//  Returns TRUE if the two RGB values specified are equivalent when
//  displayed using a 6-bit VGA palette.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
Boolean
Vga::isSameVideo_( const RgbQuad & rgb, const RgbQuad & key ) const
{
  // $[TI1.1] $[TI1.2]
  return (   key.rgbRed >> 2 == rgb.rgbRed >> 2
          && key.rgbGreen >> 2 == rgb.rgbGreen >> 2
          && key.rgbBlue >> 2 == rgb.rgbBlue >> 2 );
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: allocateColor
//
//@ Interface-Description
//  Allocates the colormap entry with the colors specified. If a matching
//  color already exists in the colormap, the index for the matching 
//  entry is returned. Otherwise, a color map entry is allocated and 
//  that index is returned. An index of zero is returned if there are
//  no matching colors and no more allocatable entries.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

void
Vga::allocateColor(Byte & index, const RgbQuad & rgb)
{
  Uint i=0;
  for (i=0; i<countof(colormapCache_); i++)
  {		// $[TI1.1]
    // VGA can only handle 6 bits in palette regs so only compare 6 bits
    if (   isSameVideo_( colormapCache_[i], rgb )
        && (colormapCache_[i].rgbReserved & (CM_READ_ONLY | CM_ALLOCATED) ) 
		    == (CM_READ_ONLY | CM_ALLOCATED) )
    {	// $[TI2.1]
      break;
    }	// $[TI2.2]
  }		// $[TI1.2]
 
  if ( i < countof(colormapCache_) )
  {		// $[TI3.1]
    // color already exists in map
    index = i;
  }
  else
  {		// $[TI3.2]
    // allocate a new color map entry
    for (i=0; i<countof(colormapCache_); i++)
    {
      if ( !colormapCache_[i].rgbReserved )
      {		// $[TI4.1]
        break;
      }		// $[TI4.2]
    }
   
    if ( i < countof(colormapCache_) )
    {	// $[TI5.1]
      storeColor( i, rgb );
	  index = i;
    }
    else
    {	// $[TI5.2]
	  // As a future capability, if there are no more allocable 
	  // colormap entries, this method could search and return 
	  // the "closest" color. For now, we just return color index
	  // zero
      index = 0;  // for no more entries - return index zero
    }
  }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: storeColor
//
//@ Interface-Description
//  Load a RAMDAC color (3 adjacent memory locs) with the desired 6-bit
//  red/green/blue values. Blinking values are stored as read/write
//  entries instead of read-only to allow the Blink task to dynamically
//  change the colormap. Read-only entries can also be returned to the
//  client on a subsequent call to allocateColor whereas an allocated
//  read/write entry is not since it is assumed that the client does
//  not want a blinking color table entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Given a color index into the RAMDAC,
//  alter the color's red/green/blue values to those passed in.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

void
Vga::storeColor(Byte index, const RgbQuad & rgb, Boolean readOnly)
{
  colormapCache_[index].rgbRed = rgb.rgbRed;
  colormapCache_[index].rgbGreen = rgb.rgbGreen;
  colormapCache_[index].rgbBlue = rgb.rgbBlue;
  colormapCache_[index].rgbReserved = CM_ALLOCATED;

  if ( readOnly )
  {		// $[TI1.1]
	colormapCache_[index].rgbReserved |= CM_READ_ONLY;
  }		// $[TI1.2]
 
  setReg(DACWX, index);			// write ramdac index  

  setReg(DACDATA, rgb.rgbRed >> 2);		// write red
  setReg(DACDATA, rgb.rgbGreen >> 2);	// write green
  setReg(DACDATA, rgb.rgbBlue >> 2);	// write blue
} 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getColor
//
//@ Interface-Description
//  Retrieves the color stored in the colormap cache at the specified index.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Given a color index return the color's red/green/blue values.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
Vga::getColor(Byte index, RgbQuad & rgb)
{
  rgb = colormapCache_[index];
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDisplayStartAddress
//
//@ Interface-Description
//  Sets the Display Start Address to the specified address 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
Vga::setDisplayStartAddress(Byte * startAddress)
{
  setReg(XR0C, Uint32(startAddress) >> 18 & 0x03);
  setReg(CR0C, Uint32(startAddress) >> 10 & 0xFF);
  setReg(CR0D, Uint32(startAddress) >> 2  & 0xFF);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetUpperVga
//
//@ Interface-Description
//  Returns a Vga reference for the upper VGA panel.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A static Vga object are defined and instantiated inside this 
//  function. On first call this object is instantiated and the 
//  reference returned. On subsequent calls, only the reference is
//  returned.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

Vga &
Vga::GetUpperVga(void)
{
	// TODO E600 
  static Vga vga(0, UPPER_VGA_ACCESS_MT);
  return vga;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetLowerVga
//
//@ Interface-Description
//  Returns a Vga reference for the lower VGA panel.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A static Vga object are defined and instantiated inside this 
//  function. On first call this object is instantiated and the 
//  reference returned. On subsequent calls, only the reference is
//  returned.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

Vga &
Vga::GetLowerVga(void)
{
  // TODO E600 port
  static Vga vga(0, LOWER_VGA_ACCESS_MT);
  return vga;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetVga
//
//@ Interface-Description
//  Returns a Vga reference for the specified VgaDisplayId. This static
//  method controls the instantiation of Vga objects to the two defined
//  displays.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Two static Vga pointers are defined and initialized inside this 
//  function. On first call these pointers are initialized and the 
//  reference to the appropriate Vga object returned. On subsequent 
//  calls, only the appropriate reference is returned.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

Vga &
Vga::GetVga( VgaDisplayId displayId )
{
  Vga * pVga = NULL;

  if ( displayId == ::UPPER_VGA_DISPLAY )
  {		// $[TI1.1]
	pVga = &Vga::GetUpperVga();
  }
  else if ( displayId == ::LOWER_VGA_DISPLAY )
  {		// $[TI1.2]
	pVga = &Vga::GetLowerVga();
  }
  else
  {
	AUX_CLASS_ASSERTION_FAILURE( displayId );
  }

  return *pVga;
}

