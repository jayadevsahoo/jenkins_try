#ifndef TimerTbl_HH
#define TimerTbl_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TimerTbl - Class that manages a table of MsgTimer entries,
// and includes a Task that checks the Timers and sends a message when
// the time is up. 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/TimerTbl.hhv   25.0.4.0   19 Nov 2013 14:15:20   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
#include "MsgTimer.hh"
//@ End-Usage

class TimerTbl
{
	//@ Friend: MsgTimer
	// 'MsgTimer' needs access to private AddMsgTimer method.
	friend class MsgTimer;

  public:

	//@ Constant: MAX_NUM_MSGTIMERS
	// Maximum number of Msg Timers.
	enum {MAX_NUM_MSGTIMERS = 200};

	//@ Constant: TASK_MSEC_INTERVAL
	// Interval of MsgTimerTask.
	enum {TASK_MSEC_INTERVAL = MsgTimer::MIN_MSEC_REQUEST / 2};	

	//@ Type: TimerEntry
	// A structure of message timer entries.
	struct TimerEntry
	{
		Int32 qId;
		Int32 msg;
		Int32 msec;
		MsgTimer::EventType eventType;
		Boolean isSet;
		Int32 ticsLeft;
	};

	static void MsgTimerTask(void);

	static void Initialize(void);
#ifdef SIGMA_DEVELOPMENT
	static void PrintTimerTable(void);
#endif // SIGMA_DEVELOPMENT

	static void SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL, 
			      const char*       pPredicate = NULL);

  protected:

  private:
	// these methods are purposely declared, but not implemented...
	TimerTbl();				// not implemented...
	~TimerTbl();				// not implemented...
	TimerTbl(const TimerTbl&);		// not implemented...
	void operator=(const TimerTbl&);	// not implemented...

	static Int32 AddMsgTimer_(const Int32 qId, const Int32 msg, 
	                    const Int32 msec, MsgTimer::EventType eventType);

	static void Set_(const Int32 msgTimerId);
	static void Set_(const Int32 msgTimerId, const Int32 msec);
	static void Cancel_(const Int32 msgTimerId);
	static Boolean IsSet_(const Int32 msgTimerId);
	static inline Boolean IsValidId_(const Int32 msgTimerId);

	//@ Data-Member: MsgTimerTable_
	// The table containing all the MsgTimer data. The 0 entry is NULL.
	static TimerEntry MsgTimerTable_[TimerTbl::MAX_NUM_MSGTIMERS+1];

	//@ Data-Member: MsgTimerCount_
	// The running count of the number of entries in the table.
	static Int32 MsgTimerCount_;
};

// Inlined methods
#include "TimerTbl.in"

#endif // TimerTbl_HH 
