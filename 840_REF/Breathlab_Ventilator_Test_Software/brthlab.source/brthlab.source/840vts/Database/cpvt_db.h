/*****************************************************************************/
/*   Module Name:    cpvt_db.h                                            
/*   Purpose:      Purpose                                 
/*        
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 Selected Reports to preview or 
/* 	 delete functions are added.			Lakshmi(CGS)   	  5 Dec,2000
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

//Any function which opens a record set will return -2 if an exception is thrown
#define DBDLL_OPEN_EXCEPTION	-2

struct SSNdate{
				long ID;
				char UUTSN[255];
				char Date[25];
			};

 int AllocSeqID(void);
 int AllocOATestID(void);
 int AddOATest(int ID, int Type, int PF, int Sequence);
 int AddTestRun(int PFflag, char* Fact, double Min, double rdg, double Max, int TestID, int type);
 int SetInfo(char* Info1, char* Info2, char* Info3, char* Info4, char* Info5, int CurrSeq);
 int AddSequence(char* User, int PFtext, int ID, char* UUTPN, char* UUTBDSN, char* UUTBDRunTime, char* UUTRev,
				 char* BDPost, char* GUISN, char* GUIPost, char* Saas, char* CompSN, char* CompRunTime,
				 char* PTSSN, char* PTSVer, char* PTSLastCal, char* PTSNextCal, int PTSOutofCal,
				 char* VentAsset, char* WorkOrder);
 int AddLogHeader(char* FileName, char* BDSN, char* GUISN);
 int AddLogInfo(char* line, int header);
 int ClearLogInfo(void);
 void CompactDB(void);
 void CloseDAOConnect(void);
// void OpenDAOConnect(void);
 void OpenDAOConnect(char *dbFileName);
 int ClearTables(void);
 int ClearInfoSet(void);
 int DeleteSequence(int SeqID);
 struct SSNdate SelectSeqID(int RecId);
