#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BdAlarms - provides interface with the Alarm-Analysis subsystem
//---------------------------------------------------------------------
//@ Interface-Description
//     Methods in the class are invoked from the Breath-Delivery and
//     BD-IO-Devices subsystems to report the occurrence of alarms
//     such as disconnect, occlusion and the loss of a gas supply to
//     the BD Status task from the BD Main task.
//
//     Methods in this class are invoked from the BD Status Task to
//     report the alarms to the Alarm-Analysis subsystem while communications
//     are up and when communications are established.
//
//     Each BD cycle, the newCycle() method is invoked from the BD Main Task
//     to determine if a HIP alarm has occurred or if the procedure error has
//     reset (note: all other alarm and alarm reset conditions are reported 
//     from other parts of the system).  HIP detection requires interfacing
//     with the BreathRecord and PressureSensor classes and the procedure
//     error reset requires interfacing with the PhasedInContextHandle.
//
//     The public method postBdAlarm() provides an interface with the
//     BD Status Task for reporting any alarms that have occurred.
//     Methods within the Breath-Delivery subsystem invoke this method
//     directly to report alarms such as Disconnect and Occlusion.
//     Methods within the BD-IO-Devices subystem use a callback mechanism
//     to report alarms through the static public method PostBdAlarm()
//     which in turn invokes the member function postBdAlarm().
//
//     The BD Status task invokes the method sendStatusToAlarmAnalysis() to
//     send the alarm status to the Alarm-Analysis subsystem if communications
//     is up between the BD and the GUI.  The BD Status Task also invokes the
//     method resyncAlarmStatus() to resynchronize the alarm status with 
//     Alarm-Analysis when comms are restored.  The resyncAlarmStatus() method
//     interfaces with the current breath record to determine the scheduler and
//     invokes initialStatusNotification() methods in several Vent Objects
//     such as GasSupply and Compressor to determine the vent object status
//---------------------------------------------------------------------
//@ Rationale
//     This class provides HIP alarm detection, procedure error reset
//     detection and provides a common interface between Breath-Delivery/
//     BD-IO-Devices and the Alarms-Analysis subsystem.  
//---------------------------------------------------------------------
//@ Implementation-Description
//     The newCycle() method implements the HIP detection algorithm
//     for detecting HIP during breath phases other than inspiration.
//     It also checks for the procedure error reset condition.
//     Note: all other alarms (including HIP during inspiration) and
//     alarm reset condtions are reported directly from other parts
//     of the system.
//
//     The public method alarmTriggerFilter() is invoked whenever a
//     breath trigger fires.  It determines if the fired trigger
//     also corresponds to an alarm trigger; if so, the alarm condition
//     is posted.
//
//     The public method postBdAlarm() is provided to allow other parts
//     the Breath-Delivery subystem to log alarms as they occur.  postBdAlarm()
//     posts the alarm identifier to the BD_STATUS_TASK_Q.  When the BD Status
//     task takes the event off of the queue, it invokes the sendStatusToAlarmAnalysis()
//     method to post the alarm event to the OperatingParameterEventQueue
//     which is monitored by the Alarm-Analysis subsystem.
//
//     The BD Status task also invokes resyncAlarmStatus() to put current
//     alarm status on the OperatingParameterEventQueue when communciations
//     are established.
//
//     The BD-IO-Devices subsystem uses a callback mechanism to log alarms.
// $[05080] $[05100] $[05165] $[05168] $[05172]
//---------------------------------------------------------------------
//@ Fault-Handling
//     n/a.
//---------------------------------------------------------------------
//@ Restrictions
//     Only one instance of this class is to be allowed.
//---------------------------------------------------------------------
//@ Invariants
//     None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdAlarms.ccv   25.0.4.1   20 Nov 2013 17:34:36   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 026  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software. 
// 
//   Revision 025   By: rhj    Date: 15-Sept-2010   SCR Number: 6436 
//   Project:  PROX
//   Description:
//      Added PROX_ALARM_ON_EN and PROX_ALARM_OFF_EN.
//      
//   Revision 024   By: rhj    Date: 26-Jan-2009    SCR Number:  6452
//   Project:  840S
//   Description:
//      Added a new alarm BDALARM_DISCONNECT_WITH_COMPRESSOR.
//      
//  Revision: 023   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 022   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Add new RM maneuver breath phase types.
//
//  Revision: 021   By: gdc   Date:  27-May-2005  DR Number: 6170
//  Project:  NIV2
//  Description:
//     DCS 6170 - NIV2
//     Removed Insp Too Long alarm fro NIV. Added High Ti spont alert.
//     Implemented new Time To Target pressure algorithms using Ti spont limit setting.
//
//  Revision: 020   By: gdc   Date:  10-Feb-2005  DR Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - removed VC+ low inspiratory pressure alarm event
//
//  Revision: 019   By: heatherw   Date:  03-Mar-2002  DR Number: 5790
//  Project:  VCP
//  Description:
//		VTPC project-related changes:
//		Added a new alarm for Vti Mand and Vti Spont Pressure to alarm map.
//      Changed INSPIRED_VOLUME_LIMIT_EN to INSPIRED_SPONT_VOLUME_LIMIT_EN
//
//  Revision: 018   By: syw   Date:  01-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Handle PAV alarms.
//                                   
//  Revision: 017  By:  jja    Date:  20-July-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Handle VC+ alarms.
//
//  Revision: 016  By:  syw    Date:  10-Nov-1999    DR Number: 5578
//       Project:  ATC
//       Description:
//			Removed clearing of gas supply alarms in resyncAlarmStatus().
//
//  Revision: 015  By:  yyy    Date:  26-Oct-1999    DR Number: 5560
//       Project:  ATC
//       Description:
//          Unconditionally send alarm cleared message to alarm subsystem
//			to clear any possible disconnect, svo, occlusion, and apnea 
//			alarms.
//
//  Revision: 014  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//          Eliminate callbacks to Compressor and GasSupply classes since they
//			will interface with the EventFilter class directly.
//
//  Revision: 013  By:  yyy    Date:   14-May-1999    DR Number: DCS 5433
//       Project:  ATC
//       Description:
//           Adding new alarm ids BDALARM_COMPENSATION_LIMIT and BDALARM_INSPIRED_VOLUME            
//
//  Revision: 012  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//           ATC initial release
//
//  Revision: 011  By:  iv    Date:   06-Apr-1998    DR Number: DCS 2754
//       Project:  BiLevel (R8027)
//       Description:
//             Changed newCycle() to notify BdMonitoredData when a HIP event have
//             occurred.
//              
//  Revision: 010  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  BiLevel (R8027)
//       Description:
//             Bilevel initial version.
//
//  Revision: 009  By:  iv    Date:   18-Nov-1997    DR Number: DCS 2610
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated the oldBpsQuality argument in PostBatteryAlarm() and modified
//             the method to eliminate unnecessary code.
//
//  Revision: 008  By: iv   Date:   26-Sep-1997    DR Number: DCS 1475
//  	Project:  Sigma (840)
//		Description:
//			Added a new alarm for high volume limit.
//
//  Revision: 007  By: iv   Date:   02-Jul-1997    DR Number: 2045
//  	Project:  Sigma (840)
//		Description:
//			In newCycle(), HIP events are reported during exhalation only if the
//          scheduler is not OCCLUSION.
//
//  Revision: 006  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
// Revision: 005 By: iv   Date: 05-May-1997   DR Number: DCS 2031
//      Project:  Sigma (840)
//      Description:
//          invoke the method initialStatusNotification() in the PowerSource object. 
//
//  Revision: 004 By: iv   Date:    15-Apr-1997    DR Number: DCS 1843
//       Project:  Sigma (840)
//       Description:
//          The scheduler id is now obtained from the current scheduler not from the
//          breath record.
//
//  Revision: 003 By: syw   Date:   08-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//          Inspection rework.  deleted call to
//			RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_NOT_BATTERY_TIME_LEFT_LT_2_MIN)
//			for SystemBattery::INOPERATIVE case.
//
//  Revision: 002 By: sp   Date:   06-Mar-1996    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  kam    Date:  07-Sep-1995    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

#include "BdAlarms.hh"

#include "MainSensorRefs.hh"
#include "VentObjectRefs.hh"
#include "BdQueuesMsg.hh"

//@ Usage-Classes
#include "BreathRecord.hh"
#include "BreathType.hh"
#include "BreathSet.hh"
#include "PressureSensor.hh"
#include "Fio2Monitor.hh"
//E600 #include "Compressor.hh"
#include "PowerSource.hh"
#include "GasSupply.hh"
#include "IpcIds.hh"
#include "MsgQueue.hh"
#include "PhasedInContextHandle.hh"
#include "SupportTypeValue.hh"
#include "BreathPhaseScheduler.hh"
#include "O2Mixture.hh"
#include "InspPauseManeuver.hh"
#include "ExpPauseManeuver.hh"
#include "NifManeuver.hh"
#include "ManeuverRefs.hh"
#include "EventFilter.hh"
#include "BDIORefs.hh"
#include "VentTypeValue.hh"
#include "LeakCompMgr.hh"
#include "AlarmQueueMsg.hh"

//@ End-Usage

#ifdef SIGMA_UNIT_TEST
#include "BdAlarms_UT.hh"
#endif // SIGMA_UNIT_TEST

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdAlarms()  
//
//@ Interface-Description
//      Default Contstructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Each element of the bdAlarmToAlarmAnalysisMap_ is initialized
//      to its corresponding OperatingParameterEventName identifier.
//      hipThisBreath_, procedureError_ and inStartupDisconnect_ are
//      set to FALSE. alarmCommState_ is set to COMMS_DOWN.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
BdAlarms::BdAlarms(void)
{   
    CALL_TRACE("BdAlarms::BdAlarms(void)");

    // $[TI1]
    hipThisBreath_ = FALSE;
    procedureError_ = FALSE;
    inStartupDisconnect_ = FALSE;
    alarmCommState_ = BdGuiCommSync::COMMS_DOWN;
    
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_WALL_AIR_PRESENT] =
        WALL_AIR_PRESENT_EN;   
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_WALL_AIR_PRESENT] =
        NOT_WALL_AIR_PRESENT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_COMP_AIR_PRESENT] =
        COMP_AIR_PRESENT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_COMP_AIR_PRESENT] =
        NOT_COMP_AIR_PRESENT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_COMP_OPTION_PRESENT] =
        COMP_OPTION_PRESENT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_COMP_OPTION_PRESENT] =
        NOT_COMP_OPTION_PRESENT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_FIO2_HIGH] =
        FIO2_HIGH_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_FIO2_NORMAL] =
        FIO2_NORMAL_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_FIO2_LOW] =
        FIO2_LOW_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_WALL_O2_PRESENT] =
        WALL_O2_PRESENT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_WALL_O2_PRESENT] =
        NOT_WALL_O2_PRESENT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_O2_MONITOR_INSTALLED] =
        O2_MONITOR_INSTALLED_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_O2_MONITOR_INSTALLED] =
        NOT_O2_MONITOR_INSTALLED_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_INTERNAL_BATTERY_POWER] =
        INTERNAL_BATTERY_POWER_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_INTERNAL_BATTERY_POWER] =
        NOT_INTERNAL_BATTERY_POWER_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_BATTERY_TIME_LEFT_LT_2_MIN] =
        BATTERY_TIME_LEFT_LT_2_MIN_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_BATTERY_TIME_LEFT_LT_2_MIN] =
        NOT_BATTERY_TIME_LEFT_LT_2_MIN_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NON_FUNCTIONAL_BATTERY] =
        NON_FUNCTIONAL_BATTERY_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_NON_FUNCTIONAL_BATTERY] =
        NOT_NON_FUNCTIONAL_BATTERY_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_CHARGE_TIME_GT_8_HRS] =
        CHARGE_TIME_GT_8_HRS_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_CHARGE_TIME_GT_8_HRS] =
        NOT_CHARGE_TIME_GT_8_HRS_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_LOW_AC_POWER] =
        LOW_AC_POWER_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_LOW_AC_POWER] =
        NOT_LOW_AC_POWER_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_100_PERCENT_O2_REQUEST] =
        HUNDRED_PERCENT_O2_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_100_PERCENT_O2_REQUEST] =
        NOT_HUNDRED_PERCENT_O2_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_VOLUME_LIMIT] =
        VOLUME_LIMIT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_INSP_TIME_TOO_LONG] =
        INSP_TIME_TOO_LONG_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_HIGH_CIRCUIT_PRESSURE] =
        HIGH_CIRCUIT_PRESSURE_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_VENT_PRESSURE] =
        VENT_PRESSURE_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_IN_APNEA_VENTILATION] =
        IN_APNEA_VENTILATION_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_DISCONNECT] =
        DISCONNECT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_DISCONNECT_WITH_COMPRESSOR] =
        DISCONNECT_WITH_COMPRESSOR_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_STARTUP_DISCONNECT] =
        STARTUP_DISCONNECT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_PROCEDURE_ERROR] =
        PROCEDURE_ERROR_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_SEVERE_OCCLUSION] =
        SEVERE_OCCLUSION_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_IN_APNEA_VENTILATION] =
        NOT_IN_APNEA_VENTILATION_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_DISCONNECT] =
        NOT_DISCONNECT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_STARTUP_DISCONNECT] =
        NOT_STARTUP_DISCONNECT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_PROCEDURE_ERROR] =
        NOT_PROCEDURE_ERROR_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_SEVERE_OCCLUSION] =
        NOT_SEVERE_OCCLUSION_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_COMPENSATION_LIMIT] =
        COMPENSATION_LIMIT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_INSPIRED_SPONT_VOLUME_LIMIT] =
        INSPIRED_SPONT_VOLUME_LIMIT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_INSPIRED_SPONT_PRESSURE_LIMIT]=
        INSPIRED_SPONT_PRESSURE_LIMIT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_INSPIRED_MAND_VOLUME_LIMIT] =
        INSPIRED_MAND_VOLUME_LIMIT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_PAV_STARTUP] =
        PAV_STARTUP_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_PAV_STARTUP] =
        NOT_PAV_STARTUP_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_PAV_ASSESSMENT] =
        PAV_ASSESSMENT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NOT_PAV_ASSESSMENT] =
        NOT_PAV_ASSESSMENT_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_VS_VOLUME_NOT_DELIVERED] =
        VOLUME_NOT_DELIVERED_VS_EN;
    bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_VCP_VOLUME_NOT_DELIVERED] =
        VOLUME_NOT_DELIVERED_VC_EN;

	bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_NON_FUNCTIONAL_PROX] =
        PROX_ALARM_ON_EN;
	bdAlarmToAlarmAnalysisMap_[BdAlarmId::BDALARM_FUNCTIONAL_PROX] =
        PROX_ALARM_OFF_EN;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdAlarms 
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
BdAlarms::~BdAlarms(void)
{   
    CALL_TRACE("BdAlarms::~BdAlarms(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//	This method takes no argument and returns nothing.
//      This method registers call backs with the following objects for
//      Alarm notification:  EventFilter, FiO2Monitor,
//      SystemBattery, PowerSource.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Calls are made to the registerAlarmCallBack() methods of the
//      BD-IO-Devices objects that detect and log alarm conditions.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void	
BdAlarms::Initialize(void)
{
	// $[TI1]
	CALL_TRACE("BdAlarms::Initialize(void)");

    // Register callbacks with the BD-IO-Devices objects that notify
    // this object of BD alarms to be posted to the Alarm-Analysis
    // subsystem
    REventFilter.registerAlarmCallBack(BdAlarms::PostBdAlarm);
	RFio2Monitor.registerAlarmCallBack(BdAlarms::PostBdAlarm);
    RPowerSource.registerAlarmCallBack(BdAlarms::PostBdAlarm);
    RSystemBattery.registerAlarmCallBack(BdAlarms::PostBdAlarm);
    RSystemBattery.registerBatteryAlarmCallBack(BdAlarms::PostBatteryAlarm);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendStatusToAlarmAnalysis
//
//@ Interface-Description
//      This method takes a BdAlarmId as input and returns nothing.
//      It is invoked from the BD Status Task to put alarm information
//      on the OperatingParameterEventQ which is monitored by the
//      Alarm-Analysis subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//      If the alarmCommState_ data member indicates that communications
//      between the BD and the GUI are up, this method puts the
//      alarm identifier on the OPERATING_PARAMETER_EVENT_Q.
//---------------------------------------------------------------------
//@ PreCondition
//      Return status of MsgQueue::PutMsg is Ipc::OK.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void	
BdAlarms::sendStatusToAlarmAnalysis (const BdAlarmId::BdAlarmIdType bdAlarmId)
{

    CALL_TRACE("BdAlarms::sendStatusToAlarmAnalysis (const BdAlarmId::BdAlarmIdType bdAlarmId)");
    CLASS_PRE_CONDITION( bdAlarmId < BdAlarmId::NUM_TOTAL_BDALARMS && 
                         bdAlarmId >= 0) 
    if (alarmCommState_ == BdGuiCommSync::COMMS_UP)
    {
        // $[TI1]
		postAlarmToAlarmAnalysis(bdAlarmId);
    }
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PostBdAlarm
//
//@ Interface-Description
//      This method takes a BdAlarmId and a Boolean as inputs and
//      returns nothing.
// 
//      This method is an interface method to provide a call back
//      capability for a client that needs to notify the Alarm-Analysis
//      subsystem of an alarm condition.  The method is called whenever
//      an alarm is detected by the BD-IO-Devices subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//      A call to the member function that posts the alarm is made.
//      If the argument initInProg boolean is TRUE, postBdAlarmInitStatus()
//      is invoked; otherwise, postBdAlarm() is called.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-MethoD
//=====================================================================
void	
BdAlarms::PostBdAlarm(const BdAlarmId::BdAlarmIdType bdAlarmId, const Boolean initInProg)
{
    CALL_TRACE("BdAlarms::PostBdAlarm(const BdAlarmId::BdAlarmIdType bdAlarmId)");

    if (initInProg)
    {
          // $[TI1]
          // postBdAlarmInitStatus posts directly to the Alarm's OperatingParameterEventQ
          // When this method is called with initInProg set to TRUE it is being called
          // from the BD Status Task after receiving the Comms Up task control message
          RBdAlarms.postBdAlarmInitStatus(bdAlarmId);
    }
    else
    {
          // $[TI2]
          // otherwise, this method is being called from the BD Main task during the
          // normal BD cycle.  Post the alarm to the BD Status Task, which is then
          // responsible for posting to the OperatingParameterEventQ
          RBdAlarms.postBdAlarm(bdAlarmId);
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PostBatteryAlarm
//
//@ Interface-Description
//      This method takes the new BpsQuality and a Boolean as inputs and
//      returns nothing.
// 
//      This method is an interface method to provide a call back
//      capability for a client that needs to notify the Alarm-Analysis
//      subsystem of an alarm condition.  The method is called whenever
//      an alarm is detected by the BD-IO-Devices subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//      A call to the member function that posts the alarm is made.
//      If the passed initInProg boolean is TRUE, postBdAlarmInitStatus()
//      is invoked; otherwise, postBdAlarm() is called.
//---------------------------------------------------------------------
//@ PreCondition
//      newBpsQuality have to match one of the enum type.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void	
BdAlarms::PostBatteryAlarm(const SystemBattery::BpsQuality newBpsQuality, const Boolean initInProg)
{

	CALL_TRACE("BdAlarms::PostBatteryAlarm(const SystemBattery::BpsQuality newBpsQuality,\
	       const Boolean initInProg)");

    if (initInProg)
    {
		// $[TI1]
        // postBdAlarmInitStatus posts directly to the Alarm's OperatingParameterEventQ
        // When this method is called with initInProg set to TRUE it is being called
        // from the BD Status Task after receiving the Comms Up task control message

        switch (newBpsQuality)
        {
            case SystemBattery::NONE:
			// $[TI1.1]
                break;

            case SystemBattery::GOOD:
			// $[TI1.5]
                RBdAlarms.postBdAlarmInitStatus (BdAlarmId::BDALARM_NOT_BATTERY_TIME_LEFT_LT_2_MIN);
                break;

            case SystemBattery::LOW:
			// $[TI1.2]
                RBdAlarms.postBdAlarmInitStatus (BdAlarmId::BDALARM_BATTERY_TIME_LEFT_LT_2_MIN);
                break;

            case SystemBattery::INOPERATIVE:
			// $[TI1.3]
                RBdAlarms.postBdAlarmInitStatus (BdAlarmId::BDALARM_NON_FUNCTIONAL_BATTERY);
                break;

            default:
			// $[TI1.4]
                // Invalid BpsQuality
                CLASS_ASSERTION_FAILURE();
                break;
        }
    }

    else
    {
        // $[TI2]
        // otherwise, this method is being called from the BD Main task during the
        // normal BD cycle.  Post the alarm to the BD Status Task, which is then
        // responsible for posting to the OperatingParameterEventQ

        switch (newBpsQuality)
        {
            case SystemBattery::NONE:
			// $[TI2.10]
                break;
                
            case SystemBattery::GOOD:
			// $[TI2.6]
                RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_NOT_BATTERY_TIME_LEFT_LT_2_MIN);
                break;

            case SystemBattery::LOW:
			// $[TI2.7]
                RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_BATTERY_TIME_LEFT_LT_2_MIN);
                break;

            case SystemBattery::INOPERATIVE:
			// $[TI2.8]
                RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_NON_FUNCTIONAL_BATTERY);
                break;

            default:
			// $[TI2.9]
                // Invalid BpsQuality
                CLASS_ASSERTION_FAILURE();
                break;
        } 

    }  // !initInProgress
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  postBdAlarm()
//
//@ Interface-Description
//      This method takes a BdAlarmId as input and returns nothing.
//      This method is invoked directly throughout the Breath-Delivery
//      subsytem and indirectly (via PostBdAlarm()) from the Breath-Delivery
//      subsystem to log BD alarms such as occlusion, disconnect, and
//      loss of gas supply.  This method posts alarm events 
//      on the BD_STATUS_TASK_Q to the BdStatusTask. 
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method maintains the status of data members 
//	hipThisBreath_, inStartupDisconnect_ and procedureError_.
//      Based on the status of these flags and the argument bdAlarmId,
//      this method determines if the alarm identifier is placed 
//      on the BD_STATUS_TASK_Q.
//
//      Most events are placed on the queue.  The following are exceptions:
//        - if hipThisBreath_ is TRUE, the HIGH_CIRCUIT_PRESSURE alarm
//          ID is not put on the queue
//        - if procedureError_ is TRUE, the PROCEDURE_ERROR alarm ID
//          is not put on the queue
//        - if inStartupDisconnect_ is TRUE, the DISCONNECT alarm ID
//          is not put on the queue
//
//      If a pause maneuver is requested, than the maneuver shall be
//      canceled.
// $[04147] $[04149] 
//---------------------------------------------------------------------
//@ PreCondition
//      bdAlarmId is valid
//	Return status of MsgQueue::PutMsg is Ipc::OK
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
BdAlarms::postBdAlarm (const BdAlarmId::BdAlarmIdType bdAlarmId)
{
  Boolean postMsg = TRUE;
  
  CALL_TRACE("BdAlarms::postBdAlarm (const BdAlarmId::BdAlarmIdType bdAlarmId)");

  if ( bdAlarmId == BdAlarmId::BDALARM_HIGH_CIRCUIT_PRESSURE ||
  	   bdAlarmId == BdAlarmId::BDALARM_VENT_PRESSURE ||
  	   bdAlarmId == BdAlarmId::BDALARM_INSP_TIME_TOO_LONG ||
       bdAlarmId == BdAlarmId::BDALARM_INSPIRED_SPONT_VOLUME_LIMIT ||
       bdAlarmId == BdAlarmId::BDALARM_INSPIRED_SPONT_PRESSURE_LIMIT ||
  	   bdAlarmId == BdAlarmId::BDALARM_INSPIRED_MAND_VOLUME_LIMIT  ||       
  	   bdAlarmId == BdAlarmId::BDALARM_COMPENSATION_LIMIT)
  {
		// $[TI10]
     // $[BL04070] :f disable pause event request
	 // $[BL04072] :f disable pause event request
     // $[BL04008] :f disable pending pause event
	 // $[BL04075] :f cancel manual pause request
	 // $[BL04070] :g disable pause event request
     // $[BL04071] :g terminate active auto expiratory pause
     // $[BL04072] :g disable pause event request
	 // $[BL04070] :g disable pause event request
	 // $[BL04071] :g terminate active auto expiratory pause
	 // $[BL04072] :g disable pause event request
	 // $[04220] :g terminate active manual expiratory pause
	 // $[BL04008] :g disable pending pause event
	 // $[BL04012] :g terminate active pause request
	 // $[BL04078] :g terminate manual active inspirtory pause
	 // $[BL04075] :g cancel manual pause request
     // $[04220] :g terminate active manual expiratory pause
     // $[BL04008] :g disable pending pause event
     // $[BL04012] :g terminate active pause request
	 // $[BL04078] :g terminate manual active inspirtory pause
	 // $[BL04075] :g cancel manual pause request
     // $[BL04070] :n disable pause event request
	 // $[BL04072] :n disable pause event request
     // $[BL04008] :n disable pending pause event
	 // $[BL04075] :n cancel manual pause request
	 // $[RM12062] A Vital Capacity maneuver shall be canceled if...
	 // $[RM12094] A P100 maneuver shall be canceled if...
	 // $[RM12077] A NIF maneuver shall be canceled if...
	 Maneuver::CancelManeuver();

  }	// $[TI11]


  // The status of the HIP condition is maintained within BdAlarms so that
  // the Alarm-Analysis subsytem is notified only once a breath of the event
  if (bdAlarmId == BdAlarmId::BDALARM_HIGH_CIRCUIT_PRESSURE
  		|| bdAlarmId == BdAlarmId::BDALARM_COMPENSATION_LIMIT)
  {
      // $[TI1]
      
      if (hipThisBreath_)
      {
          // $[TI1.1]
          postMsg = FALSE;
      }
      else
      {
          // $[TI1.2]
          hipThisBreath_ = TRUE;
      }    
  }

  // The status of the startup disconnect state is maintained within
  // BdAlarms to distinguish between a startup disconnect and a
  // disconnect
  else if (bdAlarmId == BdAlarmId::BDALARM_STARTUP_DISCONNECT)
  {
      // $[TI2]
      inStartupDisconnect_ = TRUE;
  }
  else if (bdAlarmId == BdAlarmId::BDALARM_NOT_STARTUP_DISCONNECT)
  {
      // $[TI3]
      inStartupDisconnect_ = FALSE;
  }
  
  else if (bdAlarmId == BdAlarmId::BDALARM_DISCONNECT ||
		   bdAlarmId == BdAlarmId::BDALARM_DISCONNECT_WITH_COMPRESSOR)
  {
      // If this method already received indication of the startup
      // disconnect alarm and has not yet received indication of
      // the end of startup disconnect, this disconnect alarm should 
      // not be posted.  The startup disconnect alarm is logged from 
      // the Stand-by scheduler, but the normal disconnect alarm is
      // logged from the Disconnect scheduler which is also used
      // by Startup Disconnect.   So, a disconnect alarm received
      // while the inStartupDisconnect_ flag is set is not sent
      // to the Alarm-Analysis subsystem

      // $[TI4]
      if (inStartupDisconnect_)
      {
          // $[TI4.1]
          postMsg = FALSE;
      }
      // $[TI4.2]
  }

  else if (bdAlarmId == BdAlarmId::BDALARM_PROCEDURE_ERROR)
  {
      // This method is called with the BDALARM_PROCEDURE_ERROR idendifier
      // when the Safety PCV scheduler takes control.  However, it is possible
      // for the vent to go from Safety PCV -> occlusion or disconnect and
      // back to Safety PCV; in this event, subsequent procedure error
      // alarms are not logged.

      // $[TI5]

      if (procedureError_)
      {
          // $[TI5.1]
          postMsg = FALSE;
      }
      else
      {
          // $[TI5.2]
          procedureError_ = TRUE;
      }
  }

  else if (bdAlarmId == BdAlarmId::BDALARM_NOT_PROCEDURE_ERROR)
  {
      // Reset the procedure error flag
      // $[TI6]
      procedureError_ = FALSE;
  }

  else
  {
      // Must be a valid BdAlarmId
      // $[TI7]
      CLASS_PRE_CONDITION (bdAlarmId < BdAlarmId::NUM_TOTAL_BDALARMS);
  }

  // Send the appropriate event to the BD Status Task Queue.  The
  // BDStatusTask will take the event off of the queue and post it
  // to the Alarm-Analysis subsystem
  if (postMsg)
  {
      // $[TI8]
      BdQueuesMsg msg;

      msg.bdAlarmEvent.eventType = BdQueuesMsg::BD_ALARM_EVENT;
      msg.bdAlarmEvent.id = bdAlarmId;

      // Put the event on the BD_STATUS_TASK_Q.  The BdStatusTask will take
      // it off of the queue and send it to Alarm-Analysis
	  Int32 result = MsgQueue::PutMsg(BD_STATUS_TASK_Q, (Int32)msg.qWord);
      CLASS_ASSERTION (result == Ipc::OK);

  }
  // $[TI9]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  postBdAlarmInitStatus()
//
//@ Interface-Description
//      This method takes a BdAlarmId as an input and returns nothing.
//      This method is invoked from the BD Status task to inform the 
//	Alarm-Analysis subsystem of the BD Alarm status after 
//	communications have been established or re-established.
//
//      This method is only called via the callback mechanism via
//      PostBdAlarm() for posting BD-IO-Devices alarm status  when
//      the Comms Up message has been received.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method places the corresponding Alarm identifier from 
//	bdAlarmToAlarmAnalysisMap_ to the argument bdAlarmId on the 
//	OPERATING_PARAMETER_EVENT_Q for the Alarm Analysis subsystem.  
//---------------------------------------------------------------------
//@ PreCondition
//      bdAlarmId is valid
//	Return status of MsgQueue::PutMsg is Ipc::OK
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
BdAlarms::postBdAlarmInitStatus (const BdAlarmId::BdAlarmIdType bdAlarmId)
{
  // $[TI1]
  CALL_TRACE("BdAlarms::postBdAlarmInitStatus (const BdAlarmId::BdAlarmIdType bdAlarmId)");

  // Must be a valid BdAlarmId
  CLASS_PRE_CONDITION (bdAlarmId < BdAlarmId::NUM_TOTAL_BDALARMS);

  // Send the appropriate event to the Operating Parameter Event Queue
  // The Alarm-Analysis subsystem will take the event off of the queue
  postAlarmToAlarmAnalysis(bdAlarmId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  alarmTriggerFilter()
//
//@ Interface-Description
//      This method takes a trigger Id as an input paramter and returns
//      nothing.  Based on the passed trigger Id, this method determines
//      if an alarm needs to be triggered.  If so, the method postBdAlarm
//      is invoked to post the alarm conditon.
//---------------------------------------------------------------------
//@ Implementation-Description
//      If the passed triggerId corresponds to an alarm condition, the
//      alarm condition is logged by a call to postBdAlarm.
//---------------------------------------------------------------------
//@ PreCondition
//      trigger ID is a valid breath trigger
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
BdAlarms::alarmTriggerFilter (const Trigger::TriggerId triggerId)
{
#ifdef SIGMA_UNIT_TEST
	BdAlarms_UT::AlarmType = BdAlarmId::NUM_TOTAL_BDALARMS ;
#endif // SIGMA_UNIT_TEST
   
  CALL_TRACE("BdAlarms::alarmTriggerFilter (const Trigger::TriggerId triggerId)");

  BreathRecord* pCurrentRecord = RBreathSet.getCurrentBreathRecord();
  BreathType  breathType = pCurrentRecord->getBreathType();

  DiscreteValue supportType =
			PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE);

  DiscreteValue ventType =
			PhasedInContextHandle::GetDiscreteValue(SettingId::VENT_TYPE);




  if (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP)
  {
      // $[TI1]
      postBdAlarm (BdAlarmId::BDALARM_HIGH_CIRCUIT_PRESSURE);

  }

  else if (triggerId == Trigger::HIGH_VENT_PRESSURE_EXP)
  {
      // $[TI2]
      postBdAlarm (BdAlarmId::BDALARM_VENT_PRESSURE);

  }

  else if (triggerId == Trigger::BACKUP_TIME_EXP &&
           !BreathRecord::IsPeepRecovery() &&
           !BreathRecord::GetIsMandBreath() &&
		   ventType != VentTypeValue::NIV_VENT_TYPE)
  {
      // $[TI3]
     postBdAlarm (BdAlarmId::BDALARM_INSP_TIME_TOO_LONG);

#ifdef SIGMA_UNIT_TEST
	BdAlarms_UT::AlarmType = BdAlarmId::BDALARM_INSP_TIME_TOO_LONG ;
#endif // SIGMA_UNIT_TEST

  }
  else if (triggerId == Trigger::LUNG_VOLUME_EXP)
  {
     // $[TI5]

     // This is where we will check the appropriate
     // breath type to see if we are SPONT or CONTROL
     // and the appropriate alarm will be signaled.
     
     switch (breathType)
     {
     case ::SPONT:
      {                                       // $[TI5.1]
         if(supportType == SupportTypeValue::PAV_SUPPORT_TYPE || 
            supportType == SupportTypeValue::ATC_SUPPORT_TYPE)
         {                                   // $[TI5.1.1]
            postBdAlarm (BdAlarmId::BDALARM_INSPIRED_SPONT_PRESSURE_LIMIT);
         }
         else
         if(supportType == SupportTypeValue::VSV_SUPPORT_TYPE)
         {                                   // $[TI5.1.2]
            postBdAlarm (BdAlarmId::BDALARM_INSPIRED_SPONT_VOLUME_LIMIT);
         }
      }
      break;
     case ::CONTROL:
     case ::ASSIST:
      {                                      // $[TI5.2]
         postBdAlarm (BdAlarmId::BDALARM_INSPIRED_MAND_VOLUME_LIMIT);
      }
      break;
     default: break;
     };     
  }
  else
  {
      // Must be a valid Breath Trigger
      // $[TI4]
      CLASS_PRE_CONDITION (triggerId >= Trigger::FIRST_BREATH_TRIGGER &&
                          triggerId <= Trigger::LAST_BREATH_TRIGGER);
  }

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  resetAlarms()
//
//@ Interface-Description
//      This method takes no parameters and returns nothing.  It is
//      invoked when the alarm reset key is pressed to update the
//      status of the BdAlarms data members used to keep track of the
//      HIP and Startup Disconnect alarm status.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The data members inStartupDisconnect_ and hipThisBreath_ are
//      set to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
BdAlarms::resetAlarms(void)
{
    // $[TI1]
    CALL_TRACE("BdAlarms::resetAlarms(void)");

    hipThisBreath_ = FALSE;
    inStartupDisconnect_ = FALSE;  
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//      This method takes no parameters and has no return value.  It is
//      called from the main breath delivery task every BD cycle to check if
//      HIP alarm conditions exist during the exhalation or expiratory pause
//      phase and to determine if the procedure error alarm can be reset if
//      it is active.
//
//      The method PhasedInContextHandle::AreBdSettingsAvailable()
//      is invoked to determine if the procedure error can be reset.
//
//      In order to evaluate the HIP condition, this method retrieves the
//      breath phase type from the BreathRecord.  If the breath phase type
//      is inspiration, the triggerCondition_() method of the
//      HighCircuitPressureExpTrigger class detects the alarm and reports
//      it via alarmTriggerFilter().  If the breath phase is exhalation or
//      expiratory pause, and the scheduler is not occlusion, this method
//      retrieves the airway pressure from the BreathRecord object and the
//      operator set high circuit pressure from the PhasedInContextHandle
//      to perform the HIP check.
//
//      If the HIP criteria are met, the postBdAlarm() method is invoked to
//      report the condition to the BD Status Task.
//
//      The current interval number is retrieved from the current
//      breath record; if the interval number is 1, the hipThisBreath_
//      data member is reset.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method gets the interval number from the current breath
//      record and sets hipThisBreath_ to FALSE if the interval number is 1.
//      If a the procedureError_ flag is set, this method calls
//      PhasedInContextHandle::AreBdSettingsAvailable() to determine if the
//      new patient setup is complete.  If so, postBdAlarm() is called to log
//      a procedure error reset.
//      
//      This method then gets the breath phase from the current breath
//      record. If the breath phase is exhalation or expiratory pause,
//      this method retrieves the operator set HIP limit from the
//      PhasedInContextHandle and the airway pressure reading from current 
//      BreathRecord. If the airway pressure is greater than or equal 
//      to the HIP setting, this method invokes postBdAlarm() to post 
//      the HIP alarm to the BD Status Task.
//
//      $[05172], $[05100] 
//---------------------------------------------------------------------
//@ PreCondition
//      breath phase is valid
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BdAlarms::newCycle(void) 
{   
    CALL_TRACE("BdAlarms::newCycle(void)");

    // Reset the hipThisBreath_ data member at the beginning of each breath
    if (BreathRecord::GetIntervalNumber() == 1)
    {
        // $[TI1]
        hipThisBreath_ = FALSE;
    }
    // $[TI2]

    // Check for a reset of the procedure error if one exists
    if (procedureError_ && PhasedInContextHandle::AreBdSettingsAvailable())
    {
        // $[TI3]
        postBdAlarm (BdAlarmId::BDALARM_NOT_PROCEDURE_ERROR);
    }
    // $[TI4]
    
    // determine the current phase type (e.g. EXHALATION, INSPIRATION)
    BreathPhaseType::PhaseType phase = BreathRecord::GetPhaseType();


    // During inspiration, the triggerCondition_() method of the
    // HighCircuitPressureExpTrigger class detects and reports HIP.
	// During NIF/P100, the HighCircuitPressureInspTrigger detects HIP
	// During VCM, the HighCircuitPressureExpTrigger detects HIP
    // During non-breathing phases, the HIP condition is not checked.
    const SchedulerId::SchedulerIdValue schedulerId = BreathPhaseScheduler::GetCurrentScheduler().getId();
	if ( (phase == BreathPhaseType::EXHALATION 
		  || phase == BreathPhaseType::EXPIRATORY_PAUSE)
		 && (schedulerId != SchedulerId::OCCLUSION) )
    {
        // $[TI5]
        Real32 highCircuitPressureValue =
            PhasedInContextHandle::GetBoundedValue(SettingId::HIGH_CCT_PRESS).value;
        // If airway pressure meets/exceeds the HIP limit, the alarm should be triggered
        if ( BreathRecord::GetAirwayPressure() >= highCircuitPressureValue )
        {
            // $[TI5.1]
            // If the HIP condition is met, post the event to the Alarm-Analysis subsystem
            postBdAlarm (BdAlarmId::BDALARM_HIGH_CIRCUIT_PRESSURE);
        }
    }
    else
    {
        CLASS_PRE_CONDITION( (phase >= BreathPhaseType::LOWEST_BREATH_PHASE_VALUE) &&
                             (phase <= BreathPhaseType::HIGHEST_BREATH_PHASE_VALUE) );
        // $[TI6]
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resyncAlarmStatus
//
//@ Interface-Description
//    This method takes no inputs and returns nothing.  This method
//    is called from the BD Status task when communications between
//    the BD and the Gui have been (re-)established.  This method
//    invokes the initialStatusNotification() method in several Vent
//    Object objects to notify the Alarm-Analysis subsystem of the
//    current status of the Fio2Monitor, GasSupply, etc.
//
//    This method also gets the scheduler Id of the current
//    breath record in order to notify Alarms of the scheduler based
//    alarm conditions (Apnea, Disconnect, Occlusion, etc).
//---------------------------------------------------------------------
//@ Implementation-Description
//    The intialStatusNotification() methods of the GasSupply, Compressor,
//    Fio2Monitor and SystemBattery are invoked.  The scheduler Id is then
//    determined.  Based on current  schedulerId, the Apnea, Disconnect,
//    Occlusion and Procedure Error alarm statuses are posted to Alarm-Analysis
//    on the OperatingParameterEventQ.  Finally, the alarmCommState_ is
//    updated to COMMS_UP.
//---------------------------------------------------------------------
//@ PreCondition
//    Return status of MsgQueue::PutMsg is Ipc::OK
//    schedulerId is valid
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
BdAlarms::resyncAlarmStatus(void)
{

    // Do the vent object alarm status initialization
    RGasSupply.initialStatusNotification();
// E600 BDIO    RCompressor.initialStatusNotification();
    RFio2Monitor.initialStatusNotification();
    RSystemBattery.initialStatusNotification();
    RPowerSource.initialStatusNotification();

	// Unconditionally clear the following alarms.
	postAlarmToAlarmAnalysis(BdAlarmId::BDALARM_NOT_IN_APNEA_VENTILATION);
	postAlarmToAlarmAnalysis(BdAlarmId::BDALARM_NOT_SEVERE_OCCLUSION);
                 
    // If the inStartupDisconnect_ data member is TRUE, the disconnect scheduler
    // is active because of Startup Disconnect
    if (inStartupDisconnect_)
    {
                // $[TI9.1]
		postAlarmToAlarmAnalysis(BdAlarmId::BDALARM_NOT_STARTUP_DISCONNECT);
    } 
    else
    {
        // $[TI9.2]
        // Regular disconnect
		postAlarmToAlarmAnalysis(BdAlarmId::BDALARM_NOT_DISCONNECT);
    }
    
    // determine the current scheduler
    SchedulerId::SchedulerIdValue schedulerId =
          (BreathPhaseScheduler::GetCurrentScheduler()).getId(); 

    switch (schedulerId)
    {
        case SchedulerId::AC:
        case SchedulerId::BILEVEL:
        case SchedulerId::SPONT:
        case SchedulerId::SIMV:
        case SchedulerId::POWER_UP:
        case SchedulerId::STANDBY:
        case SchedulerId::SVO:
            // $[TI1]
            // Do nothing here
            break;

        case SchedulerId::APNEA:
            // $[TI2]
            // Notify Alarms of AV
			postAlarmToAlarmAnalysis(BdAlarmId::BDALARM_IN_APNEA_VENTILATION);
            break;
            
        case SchedulerId::DISCONNECT:
            // $[TI3]
            // If the inStartupDisconnect_ data member is TRUE, the disconnect scheduler
            // is active because of Startup Disconnect
            if (inStartupDisconnect_)
            {
                // $[TI3.1]
				postAlarmToAlarmAnalysis(BdAlarmId::BDALARM_STARTUP_DISCONNECT);
            } 

            else
            {

				// When the disconnect sensitivity is limited due to the compressor,
				// use a disconnect alarm with an additional remedy message.
				// Otherwise use the original disconnect alarm message.
				if (RLeakCompMgr.isDiscoSensLimitByComp())
				{
					// $[TI3.2]
					// Regular disconnect with an additional compressor message
					postAlarmToAlarmAnalysis(BdAlarmId::BDALARM_DISCONNECT_WITH_COMPRESSOR);
				}
				else
				{
					// $[TI3.2]
					// Regular disconnect
					postAlarmToAlarmAnalysis(BdAlarmId::BDALARM_DISCONNECT);
				}
            }

            break;

        case SchedulerId::OCCLUSION:
            // $[TI4]
            // Notify Alarms of Occlusion
			postAlarmToAlarmAnalysis(BdAlarmId::BDALARM_SEVERE_OCCLUSION);
            break;
            
        case SchedulerId::SAFETY_PCV:
            // $[TI5]
            // Notify Alarms of Procedure Error
			postAlarmToAlarmAnalysis(BdAlarmId::BDALARM_PROCEDURE_ERROR);
            break;

        default:
            // $[TI6]
            // Invalid scheduler ID
            CLASS_ASSERTION_FAILURE();
            break;
            
    }

    // Sending this alarm event will cause the Alarm-Analysis task to look at the
    // status of the 100% O2 mnvr
    
    BdAlarmId::BdAlarmIdType alarmId;
    if ( O2Mixture::Is100PercentO2Requested() )
    {
        // $[TI7]
        alarmId = BdAlarmId::BDALARM_100_PERCENT_O2_REQUEST;
    }
    else
    {
        // $[TI8]
        alarmId = BdAlarmId::BDALARM_NOT_100_PERCENT_O2_REQUEST;
    }

	postAlarmToAlarmAnalysis(alarmId);

    // Initialization is complete; BdAlarms can start sending Alarm
    // data to the Alarm Task 
    alarmCommState_ = BdGuiCommSync::COMMS_UP;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
BdAlarms::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BDALARMS,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  postAlarmToAlarmAnalysis(const BdAlarmId::BdAlarmIdType bdAlarmId)
//
//@ Interface-Description
//      Helper function that takes and Alarm ID and forms an AlarmQueueMsg
//		and posts it on the OPERATING_PARAMETER_EVENT_Q for the Alarm Analysis
//		to handle.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void BdAlarms::postAlarmToAlarmAnalysis(const BdAlarmId::BdAlarmIdType bdAlarmId)
{
	AlarmQueueMsg alarmMsg;
    alarmMsg.event.eventType = AlarmQueueMsg::OPERATING_PARAMETER_EVENT;
    alarmMsg.event.eventData = bdAlarmToAlarmAnalysisMap_[bdAlarmId];
	Int32 ret = MsgQueue::PutMsg (OPERATING_PARAMETER_EVENT_Q,(const Int32)alarmMsg.qWord);
	CLASS_ASSERTION (ret == Ipc::OK);
}

