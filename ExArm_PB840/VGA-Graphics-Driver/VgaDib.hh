#ifndef VgaDib_HH
#define VgaDib_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VgaDib - Interface class for DIB bitmap functions.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaDib.hhv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: mnr    Date: 24-Sep-2010    SCR Number: 6672
//  Project: XENA2
//  Description:
//      New paramerter isFont added to DisplayBitmap().
//  
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//====================================================================
#include "VgaGraphicsDriver.hh"

//@ Usage-Classes
//@ End-Usage

class DisplayContext;
class GraphicsContext;

struct DibInfo;   
struct RgbQuad;

class VgaDib 
{
  public:
    static void  AllocateColors(DibInfo & rImageDescriptor);
	
    static void  DisplayBitmap(DisplayContext & rDc,
					GraphicsContext & rGc,
					DibInfo & rDibInfo,
			        Int xPos, Int yPos, Boolean isFont=FALSE);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:


	static void Draw24BitWndBitmap( DisplayContext & rDc,
				  Int     xDest,		Int  yDest,
				  Int     xWidth,		Int yHeight,
				  const Byte* pBitmapByte, 
				  const DibInfo & rDibInfo);

	static void GetEightHexDigits(Uint32 i8DigitHexData,int* iHexDigit ); 
};


#endif // VgaDib_HH 
