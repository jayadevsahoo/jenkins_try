
#ifndef ContextMgr_HH
#define ContextMgr_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ContextMgr - Manager of the Context Instances.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ContextMgr.hhv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "Sigma.hh"
#include "SettingClassId.hh"

//@ Usage-Classes
class AcceptedContext;
class AdjustedContext;
class RecoverableContext;
class PhasedInContext;
class PendingContext;
//@ End-Usage


class ContextMgr
{
  public:
    static void  Initialize(void);

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
    // GUI-only contexts...
    static inline AcceptedContext&     GetAcceptedContext   (void);
    static inline AdjustedContext&     GetAdjustedContext   (void);
    static inline RecoverableContext&  GetRecoverableContext(void);
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
    // BD-only contexts...
    static inline PendingContext&   GetPendingContext(void);
    static inline PhasedInContext&  GetPhasedInContext(void);
#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    ContextMgr(const ContextMgr&);	// not implemented...
    ContextMgr(void);			// not implemented...
    ~ContextMgr(void);			// not implemented...
    void  operator=(const ContextMgr&);	// not implemented...

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

    //@ Data-Member:  rAcceptedContext_
    // Reference to the static instance of the currently accepted context.
    static AcceptedContext&  RAcceptedContext_;

    //@ Data-Member:  RAdjustedContext_
    // Reference to the static instance of the adjusted (proposed) context.
    static AdjustedContext&  RAdjustedContext_;

    //@ Data-Member:  RRecoverableContext_
    // Reference to the static instance of the recoverable context.
    static RecoverableContext&  RRecoverableContext_;

#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

    //@ Data-Member:  RPendingContext_
    // Reference to the static instance of the pending context.
    static PendingContext&  RPendingContext_;

    //@ Data-Member:  RPhasedInContext_
    // Reference to the static instance of the phased-in context.
    static PhasedInContext&  RPhasedInContext_;

#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
};


// Inlined methods
#include "ContextMgr.in"


#endif // ContextMgr_HH 
