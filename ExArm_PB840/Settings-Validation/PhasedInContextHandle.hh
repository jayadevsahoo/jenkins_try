
#ifndef PhasedInContextHandle_HH
#define PhasedInContextHandle_HH

//====================================================================
// This is a  proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: PhasedInContextHandle - Handle to the PhasedIn Settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PhasedInContextHandle.hhv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "Sigma.hh"
#include "SettingId.hh"
#include "BoundedValue.hh"
#include "DiscreteValue.hh"

//@ Usage-Classes
#include "PhaseInEvent.hh"
//@ End-Usage


class PhasedInContextHandle
{
  public:
    static Boolean  AreBdSettingsAvailable(void);

    static void   PhaseInSafetyPcvSettings(void);
    static void   PhaseInPendingBatch(
			      const PhaseInEvent::PhaseInState phaseInState
				     );

    static BoundedValue   GetBoundedValue (
				  const SettingId::SettingIdType boundedId
				          );
    static DiscreteValue  GetDiscreteValue(
				   const SettingId::SettingIdType discreteId
				          );

#if defined(SIGMA_DEVELOPMENT)
    static void  SetBoundedValue(
                              const SettingId::SettingIdType boundedId, 
                              const BoundedValue&            boundedValue
                                 );
    static void  SetDiscreteValue(
                              const SettingId::SettingIdType discreteId,
                              DiscreteValue                  discreteValue
                                 );
#endif // defined(SIGMA_DEVELOPMENT)

    static void   SoftFault(const SoftFaultID softFaultID,
			    const Uint32      lineNumber,
			    const char*       pFileName  = NULL, 
			    const char*       pPredicate = NULL);
  
  private:
    PhasedInContextHandle(const PhasedInContextHandle&);// not implemented...
    PhasedInContextHandle(void);		        // not implemented...
    ~PhasedInContextHandle(void);                       // not implemented...
    void  operator=(const PhasedInContextHandle&);      // not implemented...
};


#endif // PhasedInContextHandle_HH 
