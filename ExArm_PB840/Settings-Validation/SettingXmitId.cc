#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SettingXmitId - Ids of all of the Settings.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is used to define IDs for all of the settings that are
//  transmitted to the BD CPU.  The reason the 'SettingId' values are
//  not used across the transmission link, is that when new settings are
//  added, or old ones deleted, the values for other setting IDs change.
//  This affects external monitoring/setting of the settings via the
//  ethernet link (e.g., ARTS Testing).  Therefore, the IDs chosen for
//  each of the settings are "frozen" forever, thereby allowing the
//  'SettingId' values to change freely.
//---------------------------------------------------------------------
//@ Rationale
//  (See above.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingXmitId.ccv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013   By: rhj    Date:  26-Jan-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//       PROX project-related changes.
// 
//  Revision: 012  By: mnr     Date: 07-Jan-2010     SCR Number: 6437
//  Project:  NEO
//  Description:
//       Added SCR number to the previous header.
//
//  Revision: 011  By: mnr     Date: 23-Nov-2009     SCR Number: 6437
//  Project:  NEO
//  Description:
//       Removing duplicate line of code.
//       
//  Revision: 010  By: gdc     Date: 03-MAr-2009     SCR Number: 6478
//  Project:  840S
//  Description:
//       Added missing translation for HIGH_SPONT_INSP_TIME.
//       
//  Revision: 009  By: rhj     Date: 07-July-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 008  By: gdc     Date: 18-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 007  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new volume support level
//      *  since the new Vsupp setting is now using the previously-obsoleted
//         id number, the special "- 1" case of the assertion is no longer
//         needed
//
//  Revision: 006   By: sah    Date: 20-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode project-related changes:
//      *  obsoleted patient type setting
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new BiLevel-specific settings
//
//  Revision: 004   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new ATC-specific settings
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: dosman Date:  23-Feb-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//      Inital BiLevel version.  Added PEEP_HI.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SettingXmitId.hh"

#if defined(SIGMA_DEVELOPMENT)
#  include "Ostream.hh"
#endif // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Definition...
//
//=====================================================================

SettingId::SettingIdType  SettingXmitId::XmitIdConversionTable_[
					    SettingXmitId::NUM_XMIT_ID_VALUES
							       ];


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()  [static]
//
//@ Interface-Description
//  Initialize the internal conversion tables.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingXmitId::Initialize(void)
{
  CALL_TRACE("Initialize()");

  //====================================================================
  //
  // Initialize the Table used to convert from 'SettingXmitId' to
  // 'SettingId'...
  //
  //====================================================================

  //====================================================================
  // Initialize each of the Bounded, BD Settings...
  //====================================================================

  XmitIdConversionTable_[SettingXmitId::APNEA_FLOW_ACCEL_PERCENT] =
				SettingId::APNEA_FLOW_ACCEL_PERCENT;

  XmitIdConversionTable_[SettingXmitId::APNEA_INSP_PRESS] =
				SettingId::APNEA_INSP_PRESS;

  XmitIdConversionTable_[SettingXmitId::APNEA_INSP_TIME] =
				SettingId::APNEA_INSP_TIME;

  XmitIdConversionTable_[SettingXmitId::APNEA_INTERVAL] =
				SettingId::APNEA_INTERVAL;

  XmitIdConversionTable_[SettingXmitId::APNEA_MIN_INSP_FLOW] =
				SettingId::APNEA_MIN_INSP_FLOW;

  XmitIdConversionTable_[SettingXmitId::APNEA_O2_PERCENT] =
				SettingId::APNEA_O2_PERCENT;

  XmitIdConversionTable_[SettingXmitId::APNEA_PEAK_INSP_FLOW] =
				SettingId::APNEA_PEAK_INSP_FLOW;

  XmitIdConversionTable_[SettingXmitId::APNEA_PLATEAU_TIME] =
				SettingId::APNEA_PLATEAU_TIME;

  XmitIdConversionTable_[SettingXmitId::APNEA_RESP_RATE] =
				SettingId::APNEA_RESP_RATE;

  XmitIdConversionTable_[SettingXmitId::APNEA_TIDAL_VOLUME] =
				SettingId::APNEA_TIDAL_VOLUME;

  XmitIdConversionTable_[SettingXmitId::DISCO_SENS] =
				SettingId::DISCO_SENS;

  XmitIdConversionTable_[SettingXmitId::EXP_SENS] =
				SettingId::EXP_SENS;

  XmitIdConversionTable_[SettingXmitId::FLOW_ACCEL_PERCENT] =
				SettingId::FLOW_ACCEL_PERCENT;

  XmitIdConversionTable_[SettingXmitId::FLOW_SENS] =
				SettingId::FLOW_SENS;

  XmitIdConversionTable_[SettingXmitId::HIGH_CCT_PRESS] =
				SettingId::HIGH_CCT_PRESS;

  XmitIdConversionTable_[SettingXmitId::HIGH_INSP_TIDAL_VOL] =
				SettingId::HIGH_INSP_TIDAL_VOL;

  XmitIdConversionTable_[SettingXmitId::HUMID_VOLUME] =
				SettingId::HUMID_VOLUME;

  XmitIdConversionTable_[SettingXmitId::IBW] =
				SettingId::IBW;

  XmitIdConversionTable_[SettingXmitId::INSP_PRESS] =
				SettingId::INSP_PRESS;

  XmitIdConversionTable_[SettingXmitId::INSP_TIME] =
				SettingId::INSP_TIME;

  XmitIdConversionTable_[SettingXmitId::MIN_INSP_FLOW] =
				SettingId::MIN_INSP_FLOW;

  XmitIdConversionTable_[SettingXmitId::OXYGEN_PERCENT] =
				SettingId::OXYGEN_PERCENT;

  XmitIdConversionTable_[SettingXmitId::PEAK_INSP_FLOW] =
				SettingId::PEAK_INSP_FLOW;

  XmitIdConversionTable_[SettingXmitId::PERCENT_SUPPORT] =
				SettingId::PERCENT_SUPPORT;

  XmitIdConversionTable_[SettingXmitId::PEEP] =
				SettingId::PEEP;

  XmitIdConversionTable_[SettingXmitId::PEEP_HIGH] =
				SettingId::PEEP_HIGH;

  XmitIdConversionTable_[SettingXmitId::PEEP_HIGH_TIME] =
				SettingId::PEEP_HIGH_TIME;

  XmitIdConversionTable_[SettingXmitId::PEEP_LOW] =
				SettingId::PEEP_LOW;

  XmitIdConversionTable_[SettingXmitId::PLATEAU_TIME] =
				SettingId::PLATEAU_TIME;

  XmitIdConversionTable_[SettingXmitId::PRESS_SENS] =
				SettingId::PRESS_SENS;

  XmitIdConversionTable_[SettingXmitId::PRESS_SUPP_LEVEL] =
				SettingId::PRESS_SUPP_LEVEL;

  XmitIdConversionTable_[SettingXmitId::RESP_RATE] =
				SettingId::RESP_RATE;

  XmitIdConversionTable_[SettingXmitId::TIDAL_VOLUME] =
				SettingId::TIDAL_VOLUME;

  XmitIdConversionTable_[SettingXmitId::TUBE_ID] =
				SettingId::TUBE_ID;

  XmitIdConversionTable_[SettingXmitId::VOLUME_SUPPORT] =
				SettingId::VOLUME_SUPPORT;

  XmitIdConversionTable_[SettingXmitId::HIGH_SPONT_INSP_TIME] =
				SettingId::HIGH_SPONT_INSP_TIME;

  //====================================================================
  // Initialize each of the Discrete, Batch Settings (BD)...
  //====================================================================

  XmitIdConversionTable_[SettingXmitId::APNEA_FLOW_PATTERN] =
				SettingId::APNEA_FLOW_PATTERN;

  XmitIdConversionTable_[SettingXmitId::APNEA_MAND_TYPE] =
				SettingId::APNEA_MAND_TYPE;

  XmitIdConversionTable_[SettingXmitId::FIO2_ENABLED] =
				SettingId::FIO2_ENABLED;

  XmitIdConversionTable_[SettingXmitId::FLOW_PATTERN] =
				SettingId::FLOW_PATTERN;

  XmitIdConversionTable_[SettingXmitId::HUMID_TYPE] =
				SettingId::HUMID_TYPE;

  XmitIdConversionTable_[SettingXmitId::MAND_TYPE] =
				SettingId::MAND_TYPE;

  XmitIdConversionTable_[SettingXmitId::MODE] =
				SettingId::MODE;

  XmitIdConversionTable_[SettingXmitId::NOMINAL_VOLT] =
				SettingId::NOMINAL_VOLT;

  XmitIdConversionTable_[SettingXmitId::PATIENT_CCT_TYPE] =
				SettingId::PATIENT_CCT_TYPE;

  XmitIdConversionTable_[SettingXmitId::SUPPORT_TYPE] =
				SettingId::SUPPORT_TYPE;

  XmitIdConversionTable_[SettingXmitId::TRIGGER_TYPE] =
				SettingId::TRIGGER_TYPE;

  XmitIdConversionTable_[SettingXmitId::TUBE_TYPE] =
				SettingId::TUBE_TYPE;

  XmitIdConversionTable_[SettingXmitId::VENT_TYPE] =
				SettingId::VENT_TYPE;

  XmitIdConversionTable_[SettingXmitId::LEAK_COMP_ENABLED] =
                SettingId::LEAK_COMP_ENABLED;

  XmitIdConversionTable_[SettingXmitId::PROX_ENABLED] =
				SettingId::PROX_ENABLED;

  //====================================================================
  // NOTE:  the 'XmitIdConversionTable_[]' can be "sparse".
  //====================================================================

#if defined(SIGMA_DEVELOPMENT)
  // make sure that EVERY BD setting ID has a conversion ID setup in the
  // table...
  for (Uint settingId = 0u; settingId < ::NUM_BD_SETTING_IDS; settingId++)
  {
    Boolean  isSettingIdFound = FALSE;
    for (Uint idx = 0u;
      	 !isSettingIdFound  &&  idx < SettingXmitId::NUM_XMIT_ID_VALUES;
	 idx++)
    {   // is 'settingId' in the conversion table?...
      isSettingIdFound = (XmitIdConversionTable_[idx] == settingId);
    }
    AUX_CLASS_ASSERTION((isSettingIdFound), settingId);
  }
  // make sure the number of BD settings match number transmitted...
  AUX_CLASS_ASSERTION((::NUM_BD_SETTING_IDS ==
					  SettingXmitId::NUM_XMIT_ID_VALUES),
		     SettingXmitId::NUM_XMIT_ID_VALUES);
#endif // defined(SIGMA_DEVELOPMENT)
}   // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingXmitId::SoftFault(const SoftFaultID  softFaultID,
			 const Uint32       lineNumber,
			 const char*        pFileName,
			 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
  			  SETTING_XMIT_ID_CLASS, lineNumber, pFileName,
			  pPredicate);
}
