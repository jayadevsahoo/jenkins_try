
#ifndef	SerialPort_HH
# define SerialPort_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  SerialPort - Serial-Interface Serial Port Driver
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/OS-Foundation/vcssrc/SerialPort.hhv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006    By: rpr         Date: 31-March-2010 SCR Number: 6436
//    Project: PROX
//    Description:
//    Added GetCTS function,  returns the Clear to Send Signal
// 
//  Revision: 005    By: gdc         Date: 12-Aug-2010  SCR Number: 6663
//    Project: API/MAT
//    Description:
//       Modified to use cpu_interrupt_disable and restore.
//
//  Revision: 004    By: gdc         Date: 11-Aug-2008  SCR Number: 6427
//    Project: 840S
//    Description:
//       Modified as part of SCR 6427 to fix serial port hang.
//       Moved this file to OS-Foundation from GUI-Serial-Interface to
//       provide support for BD serial port access for prox flow sensor.
//       Changed idleFunction_ to protected virtual so derived classes
//       can provide their own idle function.
//
//
//  Revision: 003    By: quf              Date: 29-Jun-2001  DR Number: 5493
//    Project: GuiComms
//    Description:
//       Changed several private member functions to protected so 
//       derived TouchSerialPort class can access them.
//
//  Revision: 002    By: quf              Date: 04-Dec-00    DR Number: 5784
//    Project: Baseline
//    Description:
//       Modified for Spacelabs monitor compatibility:
//       - return value of isReceiveError() now distinguishes between
//         parity and overflow errors.
//
//  Revision: 001    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//=====================================================================

#include "OsFoundation.hh"

#undef const


class SerialPort
{
  public:

    enum Parity
    {
          NONE
        , ODD
        , EVEN
    };

	enum UartError
	{
		UART_NO_ERROR,
		PARITY_ERROR,
		OVERFLOW_ERROR
	};

	enum SerialPortTimeOut
	{
		READ_INTERWAL_TIME_OUT = MAXDWORD,
		READ_TOTAL_TIMEOUT_MULTIPLIER = 0,
		READ_TOTAL_TIMEOUT_CONSTANT = 0,
		WRITE_TOTAL_TIMEOUT_MULTIPLIER = 0,
		WRITE_TOTAL_TIMEOUT_CONSTANT = 5000
	};

	enum {DATA_READ_DELAY_TIME = 1};

    SerialPort( const Uint8 deviceNumber );
    ~SerialPort();

    void reset(void); 
    Uint read( void * pBuffer, Uint count, Uint milliseconds = 0 );
    Uint write( const void * pBuffer, Uint count, Uint milliseconds = 0 );
	Boolean wait( Uint milliseconds );
	void flushReceiveBuffer(void);
	Boolean GetCTS(void);

	UartError isReceiveError(void);
    void enableLoopback(void);
	void disableLoopback(void);

    void setParameters(  Int32 baudRate
                       , Int32 dataBits
                       , SerialPort::Parity parity
                       , Int32 stopBits );

	static	void	SoftFault(const SoftFaultID softFauldID,
					          const Uint32	lineNumber,
					          const char *	pFileName = NULL,
					          const char *	pPredicate = NULL);

	Uint32 bytesInReceiveBuffer(void);

	Boolean SetCommunicationBaudRate(Uint32 BaudRate);

  protected:
	virtual void idleFunction_(void);
	void  writeReg_(Uint8 registerId, Uint8 value);
	Uint8 readReg_(Uint8 registerId);
	void  disableInterrupts_( void );
	void  restoreInterrupts_( void );



  private:

    SerialPort(void);     // not implemented

    //@ Data-Member: baudRate_
	//  The current baud rate setting for the port.
    Int32 baudRate_;

    //@ Data-Member: parity
	//  The current parity setting for the port.
    SerialPort::Parity parity_;

    //@ Data-Member: dataBits_
	//  The current number of data bits (7 or 8)
    Int32 dataBits_;

    //@ Data-Member: stopBits_
	//  The current number of stop bits (1 or 2)
    Int32 stopBits_;
	
	//@ Data-Member: interruptsDisabled_
	//  Set TRUE when serial device interrupts are disabled, otherwise FALSE.
	Boolean interruptsDisabled_;

	//@ Data-Member: serialPortHandle_
	//  Contains Handel of serial port 
	HANDLE  serialPortHandle_;


	//@ Data-Member: InitComplete_
	//  Contains TRUE if initialisation is complete
    Boolean	InitComplete_;

	//@ Data-Member: driverDebugMask
	// carrried from SerailCommunication class
	Uint32   driverDebugMask_;

};

#endif	// ! SerialPort_HH
