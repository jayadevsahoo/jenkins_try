
#ifndef MailBox_HH
#define MailBox_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MailBox - A specialized queue derived from IpcQueue class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/MailBox.hhv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

#include "Sigma.hh"
#include "OsFoundation.hh"


//@ Usage-Classes
#include "IpcQueue.hh"
//@ End-Usage

//This is an interface to a list of all created mail-box queues in the
//IPC queue list
class MailBoxList
{
public:
	//Initialize the mail boxes. 
	static void   Initialize(void);
    static Int32  PutMsg(const Int32 qId, const Int32 msg);

	static void SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL)
						{
							FaultHandler::SoftFault(softFaultID, OS_FOUNDATION,
													OsFoundation::MAILBOX,
													lineNumber, pFileName, pPredicate);
						}
};

//This is an object of type IpcQueue that can be instantiated
//by the relevant IPC queue ID
class MailBox : public IpcQueue
{
  public:
    MailBox(const Int32 qId);
    ~MailBox(void);

    //@ Type: Length
    // MailBox is a IpcQueue of length one.
    enum Length { LENGTH = 1};

    Int32         overWrite(const Int32 msg);
    Int32         putMsg(const Int32 msg);

    void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	//The old code was mixing between object-oriented and C style.. On one hand, this class is-a IpcQueue
	//that we could use to instanciate any queue by ID, and on the other hand the class is used as static to
	//get access to any OTHER queue object by ID!! this is a recipes for spaghettii code. Therefore, all static
	//interfaces were relocated to their own class, and this one was kept only to avoid changing all its callers
	//all over the code, and was mapped to the actual new static call.
	static Int32 PutMsg(const Int32 qId, const Int32 msg) { MailBoxList::PutMsg(qId, msg); }
  
  protected:

  private:
    // these methods are purposely declared, but not implemented...
    MailBox(void);		        // not implemented...
    MailBox(const MailBox&);		// not implemented...
    void   operator=(const MailBox&);	// not implemented...

};


#endif // MailBox_HH 
