// All rights reserved ADENEO EMBEDDED 2010
//
//  File:  bsp_version.h
//
#ifndef __BSP_VERSION_H
#define __BSP_VERSION_H

#define BSP_VERSION_MAJOR       2
#define BSP_VERSION_MINOR       30
#define BSP_VERSION_QFES		0
#define BSP_VERSION_INCREMENTAL 1

#define BSP_VERSION_STRING      L"BSP_WINCE_ARM_A8 2.30.00.01"

#define BSP_INVALID				0
#define BSP_HT70				1
#define BSP_AURA_G				2
#define BSP_AURA_C				3
#define	BSP_E600_UI				4
#define BSP_E600_BD				5

#define NMI_VERSION_STRING_SIZE	12
#define MONTH_STRING_LENGTH		3
#define YEAR_STRING_LENGTH		4
#define DAY_STRING_LENGTH		2

typedef struct
{
	DWORD	Month;
	DWORD	Year;
	DWORD	Day;
	WCHAR	VersionString[NMI_VERSION_STRING_SIZE];
}NMI_VERSION;

#endif
