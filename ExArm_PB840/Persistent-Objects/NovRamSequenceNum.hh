
#ifndef NovRamSequenceNum_HH
#define NovRamSequenceNum_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  NovRamSequenceNum - NOVRAM Sequence Number Value.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamSequenceNum.hhv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 003  By:  sah    Date:  30-Sep-1997    DCS Number: 2530
//  Project:  Sigma (R8027)
//  Description:
//	Fix incorrect handling of corrupt data, by using a 'DoubleBuffer'
//	instance within the class.
//
//  Revision: 002  By:  sah    Date:  03-Jul-1997    DCS Number: 2133
//  Project:  Sigma (R8027)
//  Description:
//       Modify to ensure NMI-safe integrity of sequence number.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "PersistentObjsId.hh"
#include "NovRamStatus.hh"

//@ Usage-Classes
#include "DblBuff_Uint32.hh"
//@ End-Usage


class NovRamSequenceNum
{
  public:
    NovRamSequenceNum (void);
    ~NovRamSequenceNum(void);

    Uint32  assignNewSequenceNum(void);

    NovRamStatus::InitializationId  initialize(void);
    NovRamStatus::VerificationId    verifySelf(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  private:
    NovRamSequenceNum(const NovRamSequenceNum&);	// not implemented...
    void  operator=(const NovRamSequenceNum&);	// not implemented...

    //@ Data-Member:  sequenceNumBuff_
    // Double-buffered integer for safely storing the current sequence number.
    DoubleBuff(Uint32)  sequenceNumBuff_;
};


#endif // NovRamSequenceNum_HH 
