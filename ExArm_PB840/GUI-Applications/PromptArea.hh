#ifndef PromptArea_HH
#define PromptArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PromptArea - The Prompt Area at the bottom right of the
// Lower screen.  Divided into three zones for displaying different
// types of prompts.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PromptArea.hhv   25.0.4.0   19 Nov 2013 14:08:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/PromptArea.hhv   1.4.1.0   07/30/98 10:19:18   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "Box.hh"
#include "TextField.hh"
#include "PromptStrs.hh"
#include "GuiAppClassIds.hh"
#include "AdjustPanel.hh"
#include "TextButton.hh"
#include "MiscStrs.hh"
#include "ButtonTarget.hh"
#include "ModeValue.hh"
#include "SettingBoundId.hh"

class  SettingSubject; // forward declaration...
//@ End-Usage


class PromptArea : public Container, public ButtonTarget
{
public:
	PromptArea(void);
	~PromptArea(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
					Boolean byOperatorAction);


	//@ Type: PromptType
	// An enumerated type listing the different types of prompt that
	// may be displayed in the area.  Note: Confirmation prompts are
	// handled separately.
	enum PromptType
	{
		//@ Constant: PA_PRIMARY
		// Indicates a Primary prompt, normally displayed in zone 1.
		PA_PRIMARY = 0,

		//@ Constant: PA_ADVISORY
		// Indicates an Advisory message, normally displayed in zone 2.
		PA_ADVISORY,

		//@ Constant: PA_SECONDARY
		// Indicates a Secondary prompt, normally displayed in zone 3.
		PA_SECONDARY,

		//@ Constant: PA_NUMBER_OF_TYPES
		// Evaluates to the number of defined prompt types.  Must be
		// the last definition in the enumerated type.
		PA_NUMBER_OF_TYPES
	};

	//@ Type: PromptPriority
	// An enumerated type listing the different priorities of available
	// prompts.
	enum PromptPriority
	{
		//@ Constant: PA_HIGH
		// Indicates a high priority prompt.
		PA_HIGH = 0,

		//@ Constant: PA_LOW
		// Indicates a low priority prompt.
		PA_LOW,

		//@ Constant: PA_NUMBER_OF_PRIORITIES
		// Evaluates to the number of defined prompt priorities.  Must be
		// the last definition in the enumerated type.
		PA_NUMBER_OF_PRIORITIES
	};

	void setPrompt(PromptType promptType, PromptPriority priority,
												StringId stringId);
	void setDefaultPrompt(StringId stringId);

	void showOverrideButton(SettingSubject*       pSoftBoundSubject,
							const SettingBoundId  softBoundId);
	void hideOverrideButton();

	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	PromptArea(const PromptArea&);		// not implemented...
	void   operator=(const PromptArea&);	// not implemented...

	void updateArea_(void);				// Formats UI of Prompt Area

	//@ Data-Member: stringIds_
	// A two-dimensional array storing the string ids of each different
	// prompt type for each prompt priority.
	StringId stringIds_[PA_NUMBER_OF_PRIORITIES][PA_NUMBER_OF_TYPES];


	//@ Data-Member: defaultStringId_
	// The string id of the Default prompt.  This prompt always has
	// the lowest priority and is shown only if none of the prioritized
	// prompts have been set.  It takes over the complete Prompt Area.
	StringId defaultStringId_;

	//@ Data-Member: primaryText_
	// This is the text field that displays the Primary Message of
	// the Prompt Area in zone 1.
	TextField primaryText_;
	
	//@ Data-Member: advisoryText_
	// This is the text field that displays the Advisory Message of
	// the Prompt Area in zone 2.
	TextField advisoryText_;
	
	//@ Data-Member: secondaryText_
	// This is the text field that displays the Secondary Message of
	// the Prompt Area in zone 3.
	TextField secondaryText_;
	
	//@ Data-Member: defaultText_
	// This is the text field that displays the Default Message of the
	// Prompt Area.  The Default Message takes over the whole of the
	// Prompt Area, all other messages disappear.
	TextField defaultText_;
	
	//@ Data-Member: topLineBox_
	// The dark area at the top of the Prompt Area separating it from
	// the Lower Sub-screen Area.
	Box topLineBox_;

	//@ Data-Member: softBoundOverrideB_ 
	// The text button used to override the soft bound of the violated
	// setting.
	TextButton  softBoundOverrideB_;

	//@ Data-Member: pSoftBoundSubject_
	// Address of the SoftBound Subject used by Settings-Validation 
	// to override soft bounds.
	SettingSubject*  pSoftBoundSubject_;

	//@ Data-Member: softBoundId_
	// Setting's soft bound id.
	SettingBoundId   softBoundId_;

	//@ Constant: BUTTON_X_
	// The X-coordinate of the override button
	static const Int32 BUTTON_X_;

	//@ Constant: BUTTON_Y_
	// The Y-coordinate of the override button
	static const Int32 BUTTON_Y_;

	//@ Constant: BUTTON_WIDTH_
	// The width of the override button
	static const Int32 BUTTON_WIDTH_;

	//@ Constant: BUTTON_HEIGHT_
	// The height of the override button
	static const Int32 BUTTON_HEIGHT_;

	//@ Constant: BUTTON_BORDER_
	// The border width of the override button
	static const Int32 BUTTON_BORDER_;



};

#endif // PromptArea_HH
