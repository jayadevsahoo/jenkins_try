#ifndef TrendEventTarget_HH
#define TrendEventTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: GuiEventTarget - A target for changes in the GUI state.
// Classes can specify this class as an additional parent class and register to
// be informed of state changes such as vent inoperative.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendEventTarget.hhv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  ksg    Date:  18-June-2007    SCR Number: 6237
//  Project:  Trend
//  Description: Initial version.
//====================================================================


#include "GuiApp.hh"

// forward declare
class TrendDataSet;

class TrendEventTarget
{
public:
	virtual void trendDataReady(TrendDataSet& rTrendDataSet);
	virtual void trendManagerStatus( Boolean isEnabled, Boolean isRunning );

protected:
	TrendEventTarget(void);
	virtual ~TrendEventTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	TrendEventTarget(const TrendEventTarget&);	// not implemented...
	void operator=(const TrendEventTarget&);	// not implemented...
};


#endif // TrendEventTarget_HH 
