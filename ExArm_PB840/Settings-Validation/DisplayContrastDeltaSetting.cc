#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  DisplayContrastDeltaSetting - Display Contrast Delta Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, sequential setting that contains the delta between
//  the contrast of the upper screen and the contrast of the lower screen.
//  This class inherits from 'BatchSequentialSetting' and provides the
//  specific information needed for representation of the display contrast
//  differential.  This information includes the interval and range of
//  this setting's values, and this batch setting's new-patient value (see
//  'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains a sequential range class ('SequentialRange')
//  to manage the resolution and range of this setting.  This class has NO
//  dependent settings, or dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DisplayContrastDeltaSetting.ccv   25.0.4.0   19 Nov 2013 14:27:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "DisplayContrastDeltaSetting.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SettingConstants.hh"
#include "OsUtil.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: DisplayContrastDeltaSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02267] -- The setting's range ...
//  $[02269] -- The setting's resolution ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DisplayContrastDeltaSetting::DisplayContrastDeltaSetting(void)
   : BatchSequentialSetting(SettingId::DISPLAY_CONTRAST_DELTA,
			    Setting::NULL_DEPENDENT_ARRAY_,
			    sequentialRange_),

	   sequentialRange_(SettingConstants::MAX_DISPLAY_CONTRAST_DELTA,
			    DISPLAY_CONTRAST_DELTA_MAX_ID,// minConstraintId
			    1,				  // resolution...
			    DISPLAY_CONTRAST_DELTA_MIN_ID,// minConstraintId
			    SettingConstants::MIN_DISPLAY_CONTRAST_DELTA)
{
  CALL_TRACE("DisplayContrastDeltaSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~DisplayContrastDeltaSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DisplayContrastDeltaSetting::~DisplayContrastDeltaSetting(void)
{
  CALL_TRACE("~DisplayContrastDeltaSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Display constrast delta is not available on color-screen systems.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
DisplayContrastDeltaSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  // $[CL02003] -- contrast delta NOT available for color displays...
#if defined(SIGMA_GUI_CPU)
  return((!::IsUpperDisplayColor())
	  ? Applicability::CHANGEABLE		// $[TI1]
	  : Applicability::INAPPLICABLE);	// $[TI2]
#elif defined(SIGMA_COMMON)
  return(Applicability::INAPPLICABLE);
#endif
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this batch setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
DisplayContrastDeltaSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  const AcceptedContext&  rAcceptedContext = ContextMgr::GetAcceptedContext();

  SequentialValue  newPatientValue;

  if (rAcceptedContext.areBatchValuesInitialized())
  {   // $[TI1]
    // when the Accepted Context has previosly-initialized values, use the
    // previous value of this setting...
    newPatientValue = rAcceptedContext.getBatchSequentialValue(
					 SettingId::DISPLAY_CONTRAST_DELTA
							      );
  }
  else
  {   // $[TI2]
    // $[02268] The setting's new-patient value...
    newPatientValue = 0;
  }

  return(newPatientValue);
}


#if defined(SIGMA_DEVELOPMENT)

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DisplayContrastDeltaSetting::SoftFault(const SoftFaultID  softFaultID,
				       const Uint32       lineNumber,
				       const char*        pFileName,
				       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  DISPLAY_CONTRAST_DELTA_SETTING, lineNumber,
			  pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
