#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AveragedStabilityBuffer - class to chek the stability of data buffer.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for checking the stablity of the data buffer.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates stability checking within it.
//---------------------------------------------------------------------
//@ Implementation-Description
// The class implements methods for updating the data buffer, and checking
// the stability.
// updateBuffer() puts the data in the buffer and checks to see how much
// it is deviated from the average.  If it is within the range, then
// increments the valid data counter, else resets the counter to 0.
// isStable() checks if the valid data fill up the data buffer.  If it
// is then we declare the buffer is stabilized.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// n/a
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/AveragedStabilityBuffer.ccv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  20-Apr-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "AveragedStabilityBuffer.hh"

//@ Usage-Classes
#include <math.h>

//@ End-Usage

#ifdef SIGMA_DEVELOPMENT
#include "Ostream.hh"
#endif

#ifdef FORNOW
#include "Task.hh"
#include "PrintQueue.hh"
#include "Ostream.hh"
#endif

//@ Code...

static const Real32 AVERAGED_STABILITY_BOUND = 0.1f;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AveragedStabilityBuffer()  [Default Contstructor]
//
//@ Interface-Description
//        Constructor.
// The constructor takes a pointer to a pre-allocated data buffer, and
// the size of this buffer.  A SummationBuffer is constructed by these
// arguments. 
//---------------------------------------------------------------------
//@ Implementation-Description
//        none
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
// The number of elements on the trigger list is asserted
// to be MAX_NUM_AC_TRIGGERS and MAX_NUM_AC_TRIGGERS to be less than
// MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
AveragedStabilityBuffer::AveragedStabilityBuffer(Real32 *pBuffer, Uint32 bufferSize) :
	SummationBuffer(pBuffer, bufferSize),
	counter_(0)
{
    CALL_TRACE("AveragedStabilityBuffer::AveragedStabilityBuffer(void)");

    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AveragedStabilityBuffer()  [Destructor]
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
AveragedStabilityBuffer::~AveragedStabilityBuffer(void)
{
	CALL_TRACE("AveragedStabilityBuffer::~AveragedStabilityBuffer(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateBuffer()
//
//@ Interface-Description
//		This method has the data to be put in the buffer as an argument and
//		returns nothing. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The data is added to the cumulative sum.  It also checks to see how much
// it is deviated from the average.  If it is within the range, then
// increments the valid data counter, else resets the counter to 0.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
AveragedStabilityBuffer::updateBuffer( const Real32 data)
{
	CALL_TRACE("SummationBuffer::updateBuffer( const Real32 data)") ;

	// Update the data by using the parent class's updateBuffer()
	SummationBuffer::updateBuffer(data);

	// $[BL04058] :b criteria for pressure stability
	if ( fabs(data - getAverage()) <= AVERAGED_STABILITY_BOUND)
	{
				    // $[TI1]
		counter_++;
	}
	else
	{
				    // $[TI2]
		counter_ = 0;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isStable()
//
//@ Interface-Description
//	This method takes no argumnet and returns a boolean value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method checks if the valid data fill up the data buffer.  If it
//  is then we declare the buffer is stablized.  A TRUE is returned.
//  Otherwise, returns FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Boolean
AveragedStabilityBuffer::isStable(void)
{
	CALL_TRACE("SummationBuffer::isStable(void)") ;

	Boolean isStable = FALSE;
	
	if (counter_ >= getBufferSize())
	{
		// $[TI1]
		isStable = TRUE;
		counter_ = 0;
	}
	    // $[TI2]

	return(isStable);
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
AveragedStabilityBuffer::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, AVERAGEDSTABILITYBUFFER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
