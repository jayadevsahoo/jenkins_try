
#ifndef RespRateSetting_HH
#define RespRateSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  RespRateSetting - Respiratory rate setting 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/RespRateSetting.hhv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  incorporated circuit-specific ranges
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
//@ End-Usage

class RespRateSetting : public BatchBoundedSetting 
{
  public:
    RespRateSetting(void); 
    virtual ~RespRateSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    virtual const BoundStatus&  calcNewValue(const Int16 knobDelta);

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
                                   const DiscreteValue            newValue,
                                   const DiscreteValue            currValue);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif  // defined(SIGMA_DEVELOPMENT)

    static Real32  GetBreathPeriod(const BasedOnCategory basedOnCategory);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual Boolean  calcDependentValues_(const Boolean isValueIncreasing);

    virtual void  updateConstraints_(void);

    virtual BoundedValue  calcBasedOnSetting_(
			    const SettingId::SettingIdType basedOnSettingId,
			    const BoundedRange::WarpDir    warpDirection,
			    const BasedOnCategory          basedOnCategory
					     ) const;

    virtual Real32  getAbsoluteMaxValue_(void) const;

  private:
    RespRateSetting(const RespRateSetting&);	// not implemented...
    void  operator=(const RespRateSetting&);	// not implemented...
};


#endif // RespRateSetting_HH 
