#include "stdafx.h"

#ifdef SIGMA_GUI_CPU

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MainSettingsArea - The area on the Lower Screen containing
// the main setting buttons and the main control settings (displaying modes
// and IBW).  This class is part of the GUI-Apps Framework cluster.
//---------------------------------------------------------------------
//@ Interface-Description
// The Main Settings Area creates and displays the various "main" setting
// buttons and the main control settings and IBW value displayed above them.
// It displays the following settings on the status bar (in non-touchable form,
// for reference only):
// >Von
//	1.	Mode
//	2.	Mandatory Type
//	3.	Support Type
//	4.	Trigger Type
//	5.	Ideal Body Weight
// >Voff
// The following settings can be displayed and modified via setting buttons:
// >Von
//	1.	Expiratory Time
//	2.	Flow Pattern
//	3.	Flow Sensitivity
//	4.	I:E Ratio
//	5.	Inspiratory Pressure
//	6.	Inspiratory Time
//	7.	Oxygen Percentage
//	8.	Peak Flow
//	9.	PEEP
//	10.	Plateau Time
//	11.	Pressure Sensitivity
//	12.	Support Pressure
//	13.	Respiratory Rate
//	14.	Tidal Volume
//	15.	Expiratory Sensitivity
// >Voff
// There are several methods provided for controlling the look of this area.
// The setToFlat() method makes all buttons in the area go to a flat
// appearance.  This is used on vent startup for displaying the previous
// patient's settings but not allowing modification of the settings.  The
// setToUp() method restores the buttons to normal operation.  The setBlank()
// method blanks/unblanks the Main Settings Area, used during New Patient
// Setup.
//
// The initialize() method needs to be called once before display of the area
// in order to perform one-time initialization.  Changes to all settings that
// affect the look of the Main Settings Area are trapped in
// valueUpdate() allowing update of the display.  Not all setting
// buttons are visible at once.  This class is mainly responsible for
// determining which settings are applicable and only applicable settings are
// displayed.
//
// The guiEventHappened() method controls the display of this area.  Depending
// on the gui event, we either display, flat, or hide the main setting buttons.
//---------------------------------------------------------------------
//@ Rationale
// A single class to control the whole of the Main Settings Area, including all
// setting buttons and the descriptive line above the buttons.
//---------------------------------------------------------------------
//@ Implementation-Description
// The Main Settings Area has two colored areas, the black Status Area at the
// top and the grey Setting Buttons Area below.  This coloring is achieved by
// coloring the Main Settings Area black and then adding a grey container to it
// which is used to contain the setting buttons.
//
// The content of the Main Settings Area changes according to the values of
// various settings.  These settings are: Mode, Mandatory Type, Spont Type,
// Trigger Type, Ideal Body Weight and the Constant During Rate Change.  Hence,
// we must register as a target of these settings.  Any changes to these
// settings will cause the content of the Main Settings area to change also.
// These changes could be: some setting buttons are removed from the screen,
// some are added to the screen, the titles in the Status Area change or the
// value of the IBW setting changes.  All changes are reported via the
// valueUpdate() method which calls the private updateArea_() method.
// This latter method formulates the correct display of the Main Settings Area
// based on the value of settings mentioned above.
//
// The ultimate control of the main setting display is based on the current
// gui event.  The gui event type, whether we are in patient setup mode, and
// the availability of the previous patient setting controls the display of
// this area.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class needs to be created (by LowerScreen).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MainSettingsArea.ccv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 016   By: rhj    Date: 10-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation related changes.
//
//  Revision: 015   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 014  By: gdc     Date:  26-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 013  By: rhj     Date:  05-May-2006    SCR Number: 6181
//  Project:  PAV4
//  Description:
//      Change Expiratory Sensitivity label from percent to (L/min) 
//      if support type is set to PAV, otherwise it is label as percent.
//
//  Revision: 012  By: gdc     Date:  15-Feb-2005    SCR Number: 6144
//  Project:  NIV
//  Description:
//      Added NIV indicator in status area.
//
//  Revision: 011  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  swapped placement of timing and pressure setting buttons, and
//         re-organized initialization for improved readability
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//      *  added button for new volume support setting
//
//  Revision: 010   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Modified initialization of the "Pi" and "Psupp" buttons to include
//      the use of the "above PEEP" phrase, as an auxillary title.
//
//  Revision: 009  By:  hhd	   Date:  07-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//      Added batch change capability to the MainSettingsArea, whereby multiple
//      settings can be changed and accepted as a group.
//
//  Revision: 008  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		09-Nov-1999 (yyy) - Initial version.
//		17-Nov-1999 (hhd) - Modified to reduce header file dependencies.
//
//  Revision: 007  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 006  By: sah  Date: 08-Feb-1999  DR Number: 5314
//  Project:  ATC (R8027)
//  Description:
//      Changed 'PEEP_HI' to 'PEEP_HIGH'.
//
//  Revision: 005  By: hhd      Date: 28-Jan-1999  DR Number: 
//    Project:  ATC (R8027)
//    Description:
//         Initial version.
//
//  Revision: 004  By: yyy      Date: 01-Dec-1998  DR Number: 5289
//    Project:  BiLevel (R8027)
//    Description:
//     Call RequestUserEvent() to cancel any pending maneuver event.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/MainSettingsArea.ccv   1.31.1.0   07/30/98 10:15:18   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "MainSettingsArea.hh"
#include "Colors.hh"
#include "GuiFoundation.hh"
#include "GuiApp.hh"
#include "MiscStrs.hh"

//@ Usage-Classes
#include "FlowPatternValue.hh"
#include "GuiEventRegistrar.hh"
#include "LowerScreen.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "SIterR_Drawable.hh"
#include "SupportTypeValue.hh"
#include "TriggerTypeValue.hh"
#include "VentTypeValue.hh"
#include "SettingContextHandle.hh"
#include "BdGuiEvent.hh"
#include "ContextSubject.hh"
#include "BreathTimingSubScreen.hh"
#include "LeakCompEnabledValue.hh"
#include "GuiTimerRegistrar.hh"
#include "AcceptedContextHandle.hh"
#include "LanguageValue.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 AREA_X_ = 0;
static const Int32 AREA_Y_ = 0;
static const Int32 AREA_WIDTH_ = 640;
static const Int32 AREA_HEIGHT_ = 159;
static const Int32 STATUS_AREA_HEIGHT_ = 25;
static const Int32 BUTTON_X_OFFSET_ = 2;
static const Int32 BUTTON_WIDTH_ = 106;
static const Int32 BUTTON_HEIGHT_ = 67;
static const Int32 BUTTON_BORDER_ = 5;
static const Int32 IBW_X_ = 533;
static const Int32 IBW_WIDTH_ = 55;
static const Int32 IBW_HEIGHT_ = 25;
static const Int32 KILOGRAM_X_ = 593;
static const Int32 NUMERIC_VALUE_CENTER_X_ = 36;
static const Int32 NUMERIC_VALUE_CENTER_Y_ = 39;
static const Int32 IE_RATIO_VALUE_CENTER_X_ = 46;
static const Int32 IE_RATIO_VALUE_CENTER_Y_ = 39;
static const Int32 LEFT_WING_X1_ = 122;
static const Int32 LEFT_WING_X2_ = 178;
static const Int32 RIGHT_WING_X1_ = 247;
static const Int32 RIGHT_WING_X2_ = 306;
static const Int32 WING_Y_ = 10;
static const Int32 WING_PEN_ = 2;
static const Int32 WINGTIP_WIDTH_ = 10;
static const Int32 WINGTIP_HEIGHT_ = 10;
static const Int32 NIV_BOX_X_ = 0;
static const Int32 NIV_BOX_Y_ = 0;
static const Int32 NIV_BOX_HEIGHT_ = STATUS_AREA_HEIGHT_;
static const Int32 NIV_BOX_WIDTH_ = BUTTON_WIDTH_;
static const Int32 NIV_BOX_PEN_WIDTH_ = 1;

static Uint  FlowPatternInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		    (sizeof(StringId) * (FlowPatternValue::TOTAL_FLOW_PATTERNS - 1)))];
static DiscreteSettingButton::ValueInfo*  PFlowPatternInfo_ =
				 (DiscreteSettingButton::ValueInfo*)::FlowPatternInfoMemory_;

Boolean MainSettingsArea::DisplayLeakComp_ = TRUE;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MainSettingsArea [Default Constructor]
//
//@ Interface-Description
// Constructs the Main Settings Area.  Needs no parameters.  Only one
// instance should be created (by LowerScreen).
//---------------------------------------------------------------------
//@ Implementation-Description
// Creates all the main setting buttons, handles to any settings with
// which we register and various text fields and graphics for displaying
// in the status area.
//
// There is a special container dedicated to holding only the setting
// buttons.  This makes them easier to manipulates as a whole and
// also allows their background color to be grey, different from the
// Main Settings Area itself (black).
//
// We register as targets of the following settings: Mode, Mandatory Type,
// Spont Type, Trigger Type, Ideal Body Weight and the Constant During
// Rate Change.  Changes to these settings will cause an update of
// the Main Settings Area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MainSettingsArea::MainSettingsArea(void) :
	isBlank_(TRUE),
	ibwNumeric_(TextFont::EIGHTEEN, 3, RIGHT),
	kilogramText_(MiscStrs::KILOGRAM_UNIT),
	nivBackground_(NIV_BOX_X_, NIV_BOX_Y_, 
			NIV_BOX_WIDTH_, NIV_BOX_HEIGHT_, NIV_BOX_PEN_WIDTH_),

	//-----------------------------------------------------------------
	// Column #0, Row #0...
	//-----------------------------------------------------------------
	respiratoryRateButton_(BUTTON_X_OFFSET_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::RESPIRATORY_RATE_BUTTON_TITLE_LARGE,
			MiscStrs::ONE_PER_MIN_UNITS_LARGE,
			SettingId::RESP_RATE, NULL,
			MiscStrs::RESPIRATORY_RATE_HELP_MESSAGE,
			TRUE),

	//-----------------------------------------------------------------
	// Column #0, Row #1...
	//-----------------------------------------------------------------
	flowAccelerationButton_(BUTTON_X_OFFSET_, BUTTON_HEIGHT_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, BUTTON_BORDER_,
			Button::SQUARE, Button::NO_BORDER, TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::FLOW_ACCELERATION_BUTTON_TITLE_LARGE,
			MiscStrs::PERCENT_UNITS_LARGE,
			SettingId::FLOW_ACCEL_PERCENT, NULL,
			MiscStrs::FLOW_ACCELERATION_HELP_MESSAGE,
			TRUE),

	//-----------------------------------------------------------------
	// Column #1, Row #0...
	//-----------------------------------------------------------------
	inspiratoryPressureButton_(BUTTON_X_OFFSET_ + BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::INSPIRATORYPRESSURE_BUTTON_TITLE_LARGE,
			GuiApp::GetPressureUnitsLarge(),
			SettingId::INSP_PRESS, NULL,
			MiscStrs::INSPIRATORY_PRESSURE_HELP_MESSAGE,
			TRUE, MiscStrs::ABOVE_PEEP_AUX_TEXT_LARGE, ::GRAVITY_RIGHT),
	peepHighButton_(BUTTON_X_OFFSET_ + BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PEEP_HI_BUTTON_TITLE_LARGE,
			GuiApp::GetPressureUnitsLarge(),
			SettingId::PEEP_HIGH, NULL,
			MiscStrs::PEEP_HI_HELP_MESSAGE,
			TRUE),
	tidalVolumeButton_(BUTTON_X_OFFSET_ + BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_ + 1, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::TIDAL_VOLUME_BUTTON_TITLE_LARGE,
			MiscStrs::ML_UNITS_LARGE,
			SettingId::TIDAL_VOLUME, NULL,
			MiscStrs::TIDAL_VOLUME_VCV_HELP_MSG,
			TRUE),

	//-----------------------------------------------------------------
	// Column #1, Row #1...
	//-----------------------------------------------------------------
	peepLowButton_(BUTTON_X_OFFSET_ + BUTTON_WIDTH_, BUTTON_HEIGHT_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PEEP_LOW_BUTTON_TITLE_LARGE,
			GuiApp::GetPressureUnitsLarge(),
			SettingId::PEEP_LOW, NULL,
			MiscStrs::PEEP_LOW_HELP_MESSAGE,
			TRUE),
	plateauTimeButton_(BUTTON_X_OFFSET_ + BUTTON_WIDTH_,
			BUTTON_HEIGHT_, BUTTON_WIDTH_, BUTTON_HEIGHT_,
			Button::DARK, BUTTON_BORDER_,
			Button::SQUARE, Button::NO_BORDER, TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PLATEAU_TIME_BUTTON_TITLE_LARGE,
			MiscStrs::SEC_UNITS_LARGE,
			SettingId::PLATEAU_TIME, NULL,
			MiscStrs::PLATEAU_TIME_HELP_MESSAGE,
			TRUE),

	//-----------------------------------------------------------------
	// Column #2, Row #0...
	//-----------------------------------------------------------------
	expiratoryTimeButton_(BUTTON_X_OFFSET_ + 2 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_ + 6, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::EXPIRATORY_TIME_BUTTON_TITLE_LARGE,
			MiscStrs::SEC_UNITS_LARGE,
			SettingId::EXP_TIME, NULL,
			MiscStrs::EXPIRATORY_TIME_HELP_MESSAGE,
			TRUE),
	peepLowTimeButton_(BUTTON_X_OFFSET_ + 2 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_ + 6, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PEEP_LOW_TIME_BUTTON_TITLE_LARGE,
			MiscStrs::SEC_UNITS_LARGE,
			SettingId::PEEP_LOW_TIME, NULL,
			MiscStrs::PEEP_LOW_TIME_HELP_MESSAGE,
			TRUE),
	ieRatioButton_(BUTTON_X_OFFSET_ + 2 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			IE_RATIO_VALUE_CENTER_X_, IE_RATIO_VALUE_CENTER_Y_,
			MiscStrs::IE_RATIO_BUTTON_TITLE_LARGE,
			SettingId::IE_RATIO, NULL,
			MiscStrs::IE_RATIO_HELP_MESSAGE,
			TRUE),
	hlRatioButton_(BUTTON_X_OFFSET_ + 2 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			IE_RATIO_VALUE_CENTER_X_, IE_RATIO_VALUE_CENTER_Y_,
			MiscStrs::PEEP_HIGH_PEEP_LOW_RATIO_BUTTON_TITLE_LARGE,
			SettingId::HL_RATIO, NULL,
			MiscStrs::PEEP_HIGH_PEEP_LOW_RATIO_HELP_MESSAGE,
			TRUE),
	inspiratoryTimeButton_(BUTTON_X_OFFSET_ + 2 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::INSPIRATORY_TIME_BUTTON_TITLE_LARGE,
			MiscStrs::SEC_UNITS_LARGE,
			SettingId::INSP_TIME, NULL,
			MiscStrs::INSPIRATORY_TIME_HELP_MESSAGE,
			TRUE),
	peepHighTimeButton_(BUTTON_X_OFFSET_ + 2 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_ + 6, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PEEP_HIGH_TIME_BUTTON_TITLE_LARGE,
			MiscStrs::SEC_UNITS_LARGE,
			SettingId::PEEP_HIGH_TIME, NULL,
			MiscStrs::PEEP_HIGH_TIME_HELP_MESSAGE,
			TRUE),
	peakFlowButton_(BUTTON_X_OFFSET_ + 2 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_ - 3, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PEAK_FLOW_BUTTON_TITLE_LARGE,
			MiscStrs::L_PER_MIN_UNITS_LARGE,
			SettingId::PEAK_INSP_FLOW, NULL,
			MiscStrs::PEAK_FLOW_HELP_MESSAGE,
			TRUE),

	//-----------------------------------------------------------------
	// Column #2, Row #1...
	//-----------------------------------------------------------------
	flowPatternButton_(BUTTON_X_OFFSET_ + 2 * BUTTON_WIDTH_,
			BUTTON_HEIGHT_, BUTTON_WIDTH_, BUTTON_HEIGHT_,
			Button::DARK, BUTTON_BORDER_, Button::SQUARE,
			Button::NO_BORDER, NULL_STRING_ID,
			SettingId::FLOW_PATTERN, NULL,
			MiscStrs::FLOW_PATTERN_HELP_MESSAGE, 24,
			TRUE),

	//-----------------------------------------------------------------
	// Column #3, Row #0...
	//-----------------------------------------------------------------
	pressureSupportButton_(BUTTON_X_OFFSET_ + 3 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PRESSURE_SUPPORT_BUTTON_TITLE_LARGE,
			GuiApp::GetPressureUnitsLarge(),
			SettingId::PRESS_SUPP_LEVEL, NULL,
			MiscStrs::PRESSURE_SUPPORT_HELP_MESSAGE,
			TRUE, MiscStrs::ABOVE_PEEP_AUX_TEXT_LARGE, ::GRAVITY_RIGHT),
	percentSupportButton_(BUTTON_X_OFFSET_ + 3 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PERCENT_SUPPORT_BUTTON_TITLE_LARGE,
			MiscStrs::PERCENT_UNITS_LARGE,
			SettingId::PERCENT_SUPPORT, NULL,
			MiscStrs::PERCENT_SUPPORT_HELP_MESSAGE,
			TRUE),
	volumeSupportButton_(BUTTON_X_OFFSET_ + 3 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::VOLUME_SUPPORT_BUTTON_TITLE_LARGE,
			MiscStrs::ML_UNITS_LARGE,
			SettingId::VOLUME_SUPPORT, NULL,
			MiscStrs::VOLUME_SUPPORT_HELP_MESSAGE,
			TRUE),

	//-----------------------------------------------------------------
	// Column #3, Row #1...
	//-----------------------------------------------------------------
    highSpontInspTimeButton_(BUTTON_X_OFFSET_ + 3 * BUTTON_WIDTH_, 
		    BUTTON_HEIGHT_, BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::HIGH_SPONT_INSP_TIME_BUTTON_TITLE_LARGE,
			MiscStrs::SEC_UNITS_LARGE,
			SettingId::HIGH_SPONT_INSP_TIME, NULL,
			MiscStrs::HIGH_SPONT_INSP_TIME_HELP_MESSAGE,
			TRUE),

	//-----------------------------------------------------------------
	// Column #4, Row #0...
	//-----------------------------------------------------------------
	flowSensitivityButton_(BUTTON_X_OFFSET_ + 4 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::FLOW_SENSITIVITY_BUTTON_TITLE_LARGE,
			MiscStrs::L_PER_MIN_UNITS_LARGE,
			SettingId::FLOW_SENS, NULL,
			MiscStrs::FLOW_SENSITIVITY_HELP_MESSAGE,
			TRUE),
	pressureSensitivityButton_(BUTTON_X_OFFSET_ + 4 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PRESSURE_SENSITIVITY_BUTTON_TITLE_LARGE,
			GuiApp::GetPressureUnitsLarge(),
			SettingId::PRESS_SENS, NULL,
			MiscStrs::PRESSURE_SENSITIVITY_HELP_MESSAGE,
			TRUE),

	//-----------------------------------------------------------------
	// Column #4, Row #1...
	//-----------------------------------------------------------------
	expiratorySensitivityButton_(BUTTON_X_OFFSET_ + 4 * BUTTON_WIDTH_,
			BUTTON_HEIGHT_, BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::EXPIRATORY_SENSITIVITY_BUTTON_TITLE_LARGE,
			MiscStrs::PERCENT_UNITS_LARGE,
			SettingId::EXP_SENS, NULL,
			MiscStrs::EXPIRATORY_SENSITIVITY_HELP_MESSAGE,
			TRUE),

	//-----------------------------------------------------------------
	// Column #5, Row #0...
	//-----------------------------------------------------------------
	oxygenPercentageButton_(BUTTON_X_OFFSET_ + 5 * BUTTON_WIDTH_, 0,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::OXYGEN_PERCENTAGE_BUTTON_TITLE_LARGE,
			MiscStrs::PERCENT_UNITS_LARGE,
			SettingId::OXYGEN_PERCENT, NULL,
			MiscStrs::OXYGEN_PERCENTAGE_HELP_MESSAGE,
			TRUE),

	//-----------------------------------------------------------------
	// Column #5, Row #1...
	//-----------------------------------------------------------------
	peepButton_(BUTTON_X_OFFSET_ + 5 * BUTTON_WIDTH_, BUTTON_HEIGHT_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK,
			BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
			TextFont::TWENTY_FOUR,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::PEEP_BUTTON_TITLE_LARGE,
			GuiApp::GetPressureUnitsLarge(),
			SettingId::PEEP, NULL,
			MiscStrs::PEEP_HELP_MESSAGE,
			TRUE),

	leftWingtip_(LEFT_WING_X1_ - WINGTIP_WIDTH_,
						WING_Y_ + WINGTIP_HEIGHT_,
						LEFT_WING_X1_, WING_Y_, WING_PEN_),
	leftWing_(LEFT_WING_X1_, 
			  WING_Y_, 
			  (GuiApp::GetLanguage() == LanguageValue::CHINESE) ? 
			  LEFT_WING_X2_-20 : LEFT_WING_X2_, 
			  WING_Y_, 
			  WING_PEN_),
	rightWing_((GuiApp::GetLanguage() == LanguageValue::CHINESE) ? 
			   RIGHT_WING_X1_+20 : RIGHT_WING_X1_, 
			   WING_Y_, 
			   RIGHT_WING_X2_, 
			   WING_Y_, 
			   WING_PEN_),
	rightWingtip_(RIGHT_WING_X2_, WING_Y_, 
						RIGHT_WING_X2_ + WINGTIP_WIDTH_,
						WING_Y_ + WINGTIP_HEIGHT_, WING_PEN_)
{
	CALL_TRACE("MainSettingsArea(void)");

	// Size and position the area
	setX(AREA_X_);
	setY(AREA_Y_);
	setWidth(AREA_WIDTH_);
	setHeight(AREA_HEIGHT_);

	// Default background color is black (this is the background color
	// of the Status Area line, the button container will overlay the rest
	// of the Main Settings Area with an Extra Dark Grey background).
	setFillColor(Colors::BLACK);			// Background black


	// set up values for flow pattern....
	::PFlowPatternInfo_->numValues = FlowPatternValue::TOTAL_FLOW_PATTERNS;
	::PFlowPatternInfo_->arrValues[FlowPatternValue::SQUARE_FLOW_PATTERN] =
									MiscStrs::FLOW_PATTERN_SQUARE_VALUE_LARGE;
	::PFlowPatternInfo_->arrValues[FlowPatternValue::RAMP_FLOW_PATTERN] =
									MiscStrs::FLOW_PATTERN_RAMP_VALUE_LARGE;
	flowPatternButton_.setValueInfo(::PFlowPatternInfo_);
	flowPatternButton_.setValueTextPosition(::GRAVITY_TOP);


	nivBackground_.setColor(Colors::YELLOW);
	nivBackground_.setFillColor(Colors::YELLOW);

	ventTypeText_.setColor(Colors::BLACK);
	ventTypeText_.setText(MiscStrs::VENT_TYPE_MSA_NIV_TITLE);

	leakCompText_.setColor(Colors::BLACK);
	leakCompText_.setText(MiscStrs::LEAK_COMP_MSA_ENABLE_TITLE);

	modeText_.setColor(Colors::WHITE);

	// Size ibw value and position kg text
	ibwNumeric_.setX(IBW_X_);
	ibwNumeric_.setWidth(IBW_WIDTH_);
	ibwNumeric_.setHeight(IBW_HEIGHT_);
	ibwNumeric_.setColor(Colors::LIGHT_BLUE);

	kilogramText_.setX(KILOGRAM_X_);
	kilogramText_.setColor(Colors::LIGHT_BLUE);

	// Position and add status line text objects to main container
	mandatoryTypeText_.setX(BUTTON_X_OFFSET_ + BUTTON_WIDTH_);
	mandatoryTypeText_.setColor(Colors::WHITE);

	spontTypeText_.setX(BUTTON_X_OFFSET_ + 3 * BUTTON_WIDTH_);
	spontTypeText_.setColor(Colors::WHITE);

	triggerTypeText_.setX(BUTTON_X_OFFSET_ + 4 * BUTTON_WIDTH_);
	triggerTypeText_.setColor(Colors::WHITE);

	addDrawable(&nivBackground_);
	addDrawable(&modeText_);
	addDrawable(&ventTypeText_);
	addDrawable(&mandatoryTypeText_);
	addDrawable(&spontTypeText_);
	addDrawable(&triggerTypeText_);
	addDrawable(&ibwNumeric_);
	addDrawable(&kilogramText_);
	addDrawable(&leakCompText_);

	// Color and add the wing tips for the Mandatory Type
	leftWingtip_.setColor(Colors::WHITE);
	leftWing_.setColor(Colors::WHITE);
	rightWing_.setColor(Colors::WHITE);
	rightWingtip_.setColor(Colors::WHITE);
	addDrawable(&leftWingtip_);
	addDrawable(&leftWing_);
	addDrawable(&rightWing_);
	addDrawable(&rightWingtip_);

	// Size, color and append the button container
	buttonContainer_.setX(0);
	buttonContainer_.setY(STATUS_AREA_HEIGHT_);
	buttonContainer_.setWidth(AREA_WIDTH_);
	buttonContainer_.setHeight(AREA_HEIGHT_ - STATUS_AREA_HEIGHT_);
	buttonContainer_.setFillColor(Colors::MIDNIGHT_BLUE);
	addDrawable(&buttonContainer_);

	// Register for GUI event change callbacks.
	GuiEventRegistrar::RegisterTarget(this);

	Uint  idx;

	// add setting buttons to array of setting buttons...
	idx = 0u;
	arrMainSettingButtons_[idx++] = &expiratoryTimeButton_;
	arrMainSettingButtons_[idx++] = &peepLowTimeButton_;
	arrMainSettingButtons_[idx++] = &flowAccelerationButton_;
	arrMainSettingButtons_[idx++] = &flowPatternButton_;
	arrMainSettingButtons_[idx++] = &flowSensitivityButton_;
	arrMainSettingButtons_[idx++] = &expiratorySensitivityButton_;
	arrMainSettingButtons_[idx++] = &ieRatioButton_;
	arrMainSettingButtons_[idx++] = &hlRatioButton_;
	arrMainSettingButtons_[idx++] = &inspiratoryPressureButton_;
	arrMainSettingButtons_[idx++] = &inspiratoryTimeButton_;
	arrMainSettingButtons_[idx++] = &peepHighTimeButton_;
	arrMainSettingButtons_[idx++] = &oxygenPercentageButton_;
	arrMainSettingButtons_[idx++] = &peakFlowButton_;
	arrMainSettingButtons_[idx++] = &peepButton_;
	arrMainSettingButtons_[idx++] = &peepLowButton_;
	arrMainSettingButtons_[idx++] = &peepHighButton_;
	arrMainSettingButtons_[idx++] = &plateauTimeButton_;
	arrMainSettingButtons_[idx++] = &pressureSensitivityButton_;
	arrMainSettingButtons_[idx++] = &pressureSupportButton_;
	arrMainSettingButtons_[idx++] = &respiratoryRateButton_;
	arrMainSettingButtons_[idx++] = &tidalVolumeButton_;
	arrMainSettingButtons_[idx++] = &percentSupportButton_;
	arrMainSettingButtons_[idx++] = &volumeSupportButton_;
	arrMainSettingButtons_[idx++] = &highSpontInspTimeButton_;
	arrMainSettingButtons_[idx]   = NULL;

	AUX_CLASS_ASSERTION((idx <= NUM_SETTING_BUTTONS_), idx);

	// add each of the setting buttons to the button container...
	for (idx = 0u; arrMainSettingButtons_[idx] != NULL; idx++)
	{
		buttonContainer_.addDrawable(arrMainSettingButtons_[idx]);
	}

	flowAccelerationButton_.setShowViewable(TRUE);

	// the main settings area needs to ALWAYS be attached to the accepted
	// context subject...
	attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID,
					 Notification::BATCH_CHANGED);

	GuiTimerRegistrar::RegisterTarget(GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER, this);


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MainSettingsArea  [Destructor]
//
//@ Interface-Description
// Destroys the Main Settings Area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MainSettingsArea::~MainSettingsArea(void)
{
	CALL_TRACE("~MainSettingsArea(void)");

	// Do nothing.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
// Called immediately after construction.  Performs initialization of
// any members that need it.  No parameters required.
//---------------------------------------------------------------------
//@ Implementation-Description
// We first call updateArea_() so that the display of the Main Settings
// Area will reflect the current state of the Settings-Validation
// subsystem.
//
// Then we tie all Main Setting buttons to their associated subscreens
// by calling the setSubScreen() and setFocusSubScreen() methods of
// each Main Setting button, passing a pointer to the appropriate
// subscreen.  Currently, buttons are all tied to Breath Timing subscreen.
// Each main setting buttons also knows if the breath timeing diagram
// need to be displayed.  Linking the main setting buttons with the
// breath timing subScreen for value update and user event update.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MainSettingsArea::initialize(void)
{
	CALL_TRACE("initialize(void)");

	// Updating the Main Settings Area.
	updateArea_();

	SubScreen*  pSubScreen;

	// Attached to the breath timing subscreen...
	pSubScreen = LowerSubScreenArea::GetBreathTimingSubScreen();

	for (Uint idx = 0u; arrMainSettingButtons_[idx] != NULL; idx++)
	{
		SettingButton*  pSettingButton = arrMainSettingButtons_[idx];

		Boolean breathTimingDisplay = FALSE;

		switch (pSettingButton->getSettingId())
		{
		case SettingId::EXP_TIME :
		case SettingId::PEEP_LOW_TIME :
		case SettingId::INSP_TIME :
		case SettingId::PEEP_HIGH_TIME :
		case SettingId::IE_RATIO :
		case SettingId::HL_RATIO :
		case SettingId::RESP_RATE :
		case SettingId::TIDAL_VOLUME :
		case SettingId::PEAK_INSP_FLOW :
		case SettingId::PLATEAU_TIME :
		case SettingId::FLOW_PATTERN :				// $[TI1]
			breathTimingDisplay = TRUE;
			break;
		default :									// $[TI2]
			breathTimingDisplay = FALSE;
			break;
		}

		pSettingButton->setSubScreen(pSubScreen);
		pSettingButton->setBreathTimingDisplay(breathTimingDisplay);
		pSettingButton->setFocusSubScreen(pSubScreen);
		pSettingButton->registerValueUpdateCallbackPtr(BreathTimingSubScreen::ValueUpdateHappened);
		pSettingButton->registerUserEventCallbackPtr(BreathTimingSubScreen::UserEventHappened);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateArea_
//
//@ Interface-Description
// Called any time the user interface of the Main Settings Area needs
// changing or updating.  Normally called when one of the following
// settings change: Mode, Mandatory Type, Support Type, Trigger Type,
// Ideal Body Weight and Constant During Rate Change.
//---------------------------------------------------------------------
//@ Implementation-Description
// By examining the value of the settings above we can determine what buttons
// should be displayed.  The contents of the status line are also setup.  The
// status line displays the current Mode, Mandatory Type, Support Type, Trigger
// Type and IBW value.  Here is a list of the breath setting buttons and when
// they are displayed:
// >Von
//	expiratory time					when mandatory type is PCV
//	flow acceleration				when mandatory type is PCV
//									 or support type is PSV and mode is not A/C
//	flow pattern					when mandatory type is VCV
//	flow sensitivity				when trigger type is FLOW
//	I:E ratio						when mandatory type is PCV
//	inspiratory pressure			when mandatory type is PCV
//	inspiratory time				when mandatory type is PCV
//	oxygen percentage				always
//	peak inspiratory flow			when mandatory type is VCV
//	PEEP							always
//	plateau time					when mandatory type is VCV
//	pressure sensitivity			when trigger type is PRESSURE
//	pressure support level			when support type is PSV
//	respiratory rate				when mode is A/C or SIMV
//	tidal volume					when mandatory type is VCV
// >Voff
//
// $[01002] Inactive settings and data must be removed from view ...
// $[01051] If a setting is inapplicable then remove its setting button ...
// $[01071] Main Settings Area displays these buttons in these conditions ...
// $[01072] Main setting buttons always displayed in same place ...
// $[01075] Main Settings Area displays the Control Settings ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MainSettingsArea::updateArea_(void)
{
	CALL_TRACE("updateArea_(void)");

	const ContextSubject *const  P_ACCEPTED_SUBJECT =
							getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

    const DiscreteValue  VENT_TYPE =
        P_ACCEPTED_SUBJECT->getSettingValue(SettingId::VENT_TYPE);

	const DiscreteValue  MODE_VALUE =
					  P_ACCEPTED_SUBJECT->getSettingValue(SettingId::MODE);


	
    const DiscreteValue  LEAK_COMP_ENABLED_VALUE =
	     P_ACCEPTED_SUBJECT->getSettingValue(SettingId::LEAK_COMP_ENABLED);

    // If Leak Compensation is enabled, display "Leak Comp" and start
    // a timer to toggle "Leak Comp" and the current mode setting.
	if (LEAK_COMP_ENABLED_VALUE == LeakCompEnabledValue::LEAK_COMP_ENABLED)
	{
		if (!GuiTimerRegistrar::isActive(GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER))
		{
			GuiTimerRegistrar::StartTimer(GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER);
			timerEventHappened(GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER);
		}
	}
	else
	{
		// Do not display "Leak Comp"
		if (GuiTimerRegistrar::isActive(GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER))
		{		
			timerEventHappened(GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER);
			GuiTimerRegistrar::CancelTimer(GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER);
		}
		if (VENT_TYPE == VentTypeValue::NIV_VENT_TYPE)
		{  // $[TI4.1]
		  nivBackground_.setShow(TRUE);
		  ventTypeText_.setShow(TRUE);
		  modeText_.setColor(Colors::BLACK);
		}
		else
		{  // $[TI4.2]
		  nivBackground_.setShow(FALSE);
		  ventTypeText_.setShow(FALSE);
		  modeText_.setColor(Colors::WHITE);
		}
		leakCompText_.setShow(FALSE);
		modeText_.setShow(TRUE);

	}



	ventTypeText_.setText(MiscStrs::VENT_TYPE_MSA_NIV_TITLE);


    switch (MODE_VALUE)
	{
	case ModeValue::AC_MODE_VALUE:						// $[TI1.1]
		modeText_.setText(MiscStrs::MODE_MSA_AC_TITLE);
		break;

	case ModeValue::SIMV_MODE_VALUE:					// $[TI1.2]
		modeText_.setText(MiscStrs::MODE_MSA_SIMV_TITLE);
		break;

	case ModeValue::SPONT_MODE_VALUE:					// $[TI1.3]
		modeText_.setText(MiscStrs::MODE_MSA_SPONT_TITLE);
		break;

	case ModeValue::CPAP_MODE_VALUE:
		modeText_.setText(MiscStrs::MODE_MSA_CPAP_TITLE);
		break;

	case ModeValue::BILEVEL_MODE_VALUE:					// $[TI1.5]
		modeText_.setText(MiscStrs::MODE_MSA_BILEVEL_TITLE);
		break;

	default:											// $[TI1.4]
		AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
		break;
	}

    const DiscreteValue  MAND_TYPE_VALUE =
				  P_ACCEPTED_SUBJECT->getSettingValue(SettingId::MAND_TYPE);

	// Do Mandatory Type buttons
	switch (MAND_TYPE_VALUE)
	{
	case MandTypeValue::PCV_MAND_TYPE:					// $[TI3.1]
		// Special "PC Manual Insp" title for Spont mode
		if ((MODE_VALUE == ModeValue::SPONT_MODE_VALUE) || (MODE_VALUE == ModeValue::CPAP_MODE_VALUE))
		{												// $[TI3.1.1]
			// Indicate that PCV settings are for Manual Inspiration only
			mandatoryTypeText_.
						setText(MiscStrs::MAND_TYPE_MSA_PC_MAN_INSP_TITLE);
		}
		else
		{												// $[TI3.1.2]
			// Set normal "PCV" title
			mandatoryTypeText_.setText(MiscStrs::MAND_TYPE_MSA_PCV_TITLE);
		}
		break;

	case MandTypeValue::VCV_MAND_TYPE:					// $[TI3.2]
		// Special "VC Manual Insp" title for Spont mode
		if ((MODE_VALUE == ModeValue::SPONT_MODE_VALUE) || (MODE_VALUE == ModeValue::CPAP_MODE_VALUE))
		{												// $[TI3.2.1]
			// Indicate that VCV settings are for Manual Inspiration only
			mandatoryTypeText_.
						setText(MiscStrs::MAND_TYPE_MSA_VC_MAN_INSP_TITLE);
		}
		else
		{												// $[TI3.2.2]
			// Set normal "VCV" title
			mandatoryTypeText_.setText(MiscStrs::MAND_TYPE_MSA_VCV_TITLE);
		}

		// user 'VC' help message...
		tidalVolumeButton_.setMessageId(MiscStrs::TIDAL_VOLUME_VCV_HELP_MSG);
		break;

	case MandTypeValue::VCP_MAND_TYPE:					// $[TI3.3]
		// 'VC+' is not applicable in 'SPONT' mode, therefore set normal
		// title...
		mandatoryTypeText_.setText(MiscStrs::MAND_TYPE_MSA_VCP_TITLE);

		// user 'VC+' help message...
		tidalVolumeButton_.setMessageId(MiscStrs::TIDAL_VOLUME_VCP_HELP_MSG);
		break;

	default:
		AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
		break;
	}

	Applicability::Id  SUPPORT_TYPE_APPLIC =
			SettingContextHandle::GetSettingApplicability(
												ContextId::ACCEPTED_CONTEXT_ID,
												SettingId::SUPPORT_TYPE
														 );

	if (SUPPORT_TYPE_APPLIC != Applicability::INAPPLICABLE)
	{													// $[TI3.4]
		const DiscreteValue  SUPPORT_TYPE_VALUE =
				  P_ACCEPTED_SUBJECT->getSettingValue(SettingId::SUPPORT_TYPE);

		switch (SUPPORT_TYPE_VALUE)
		{
		case SupportTypeValue::PSV_SUPPORT_TYPE:		// $[TI3.4.1]
			spontTypeText_.setText(MiscStrs::SPONT_TYPE_MSA_PSV_TITLE);
			break;

		case SupportTypeValue::OFF_SUPPORT_TYPE:		// $[TI3.4.2]
			spontTypeText_.setText(NULL_STRING_ID);
			break;

	   case SupportTypeValue::ATC_SUPPORT_TYPE:			// $[TI3.4.3]
			spontTypeText_.setText(MiscStrs::SPONT_TYPE_MSA_ATC_TITLE);
			break;

	   case SupportTypeValue::VSV_SUPPORT_TYPE:			// $[TI3.4.4]
			spontTypeText_.setText(MiscStrs::SPONT_TYPE_MSA_VSV_TITLE);
			break;

	   case SupportTypeValue::PAV_SUPPORT_TYPE:			// $[TI3.4.5]
			spontTypeText_.setText(MiscStrs::SPONT_TYPE_MSA_PAV_TITLE);
			break;
 
		default:
			AUX_CLASS_ASSERTION_FAILURE(SUPPORT_TYPE_VALUE);
			break;
		}

        // Change Expiratory Sensitivity label from percent to (L/min) if 
        // support type is set to PAV, otherwise it is label as percent
        if(SUPPORT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE)
        {
            expiratorySensitivityButton_.setUnitsText(MiscStrs::L_PER_MIN_UNITS_LARGE);
            expiratorySensitivityButton_.setMessageId(MiscStrs::EXPIRATORY_SENSITIVITY_PAV_HELP_MESSAGE);

        }
        else
        {
            expiratorySensitivityButton_.setUnitsText(MiscStrs::PERCENT_UNITS_LARGE);
            expiratorySensitivityButton_.setMessageId(MiscStrs::EXPIRATORY_SENSITIVITY_HELP_MESSAGE);

        }        

	}
	else
	{													// $[TI3.5]
		spontTypeText_.setText(NULL_STRING_ID);
	}

	const DiscreteValue  TRIGGER_TYPE_VALUE =
				  P_ACCEPTED_SUBJECT->getSettingValue(SettingId::TRIGGER_TYPE);

	// Do Trigger Type buttons
	switch (TRIGGER_TYPE_VALUE)
	{
	case TriggerTypeValue::FLOW_TRIGGER:				// $[TI3.6.1]
		triggerTypeText_.setText(MiscStrs::TRIGGER_TYPE_MSA_FLOW_TRIG_TITLE);
		break;

	case TriggerTypeValue::PRESSURE_TRIGGER:			// $[TI3.6.2]
		triggerTypeText_.setText(MiscStrs::TRIGGER_TYPE_MSA_PRESSURE_TRIG_TITLE);
		break;

	default:
		AUX_CLASS_ASSERTION_FAILURE(TRIGGER_TYPE_VALUE);
		break;	
	}

	const BoundedValue  IBW_VALUE =
						  P_ACCEPTED_SUBJECT->getSettingValue(SettingId::IBW);

	ibwNumeric_.setValue(IBW_VALUE.value);
	ibwNumeric_.setPrecision(IBW_VALUE.precision);

	// activate all buttons...
	for (Uint idx = 0u; arrMainSettingButtons_[idx] != NULL; idx++)
	{
		(arrMainSettingButtons_[idx])->activate();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setToFlat
//
//@ Interface-Description
// No arguments needed.  Flattens all buttons in the area.  Used on startup of
// the ventilator in order to simply display the last patient's settings and
// during settings lockout mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call the setToFlat() method of all the setting buttons.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MainSettingsArea::setToFlat(void)
{
	CALL_TRACE("setToFlat(void)");

	for (Uint idx = 0u; arrMainSettingButtons_[idx] != NULL; idx++)
	{
		(arrMainSettingButtons_[idx])->adjustPanelLoseFocusHappened();
		(arrMainSettingButtons_[idx])->setToFlat();
	}
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setToUp
//
//@ Interface-Description
// No arguments needed.  Unflattens all buttons in the area.  Used after
// startup of the ventilator when normal operation begins.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call the setToUp() method of all the setting buttons.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MainSettingsArea::setToUp(void)
{
	CALL_TRACE("setToUp(void)");

	// Unflatten all buttons, assuming Settings has not been locked out
	if (!GuiApp::IsSettingsLockedOut())
	{													// $[TI1]
		for (Uint idx = 0u; arrMainSettingButtons_[idx] != NULL; idx++)
		{
			(arrMainSettingButtons_[idx])->setToUp();
		}
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setBlank
//
//@ Interface-Description
// Passed a boolean, 'blank', which tells Main Settings Area to either blank
// its area (make all drawable contents invisible) or not.
//---------------------------------------------------------------------
//@ Implementation-Description
// Loop through all drawables in our container and show/hide them depending
// on the value of 'blank'.  If the area is being shown then make sure all
// the right buttons and stuff are displayed properly by calling updateArea_()
// and then activating all buttons.  If the area is being hidden then change
// the background color to extra dark grey.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MainSettingsArea::setBlank(Boolean blankFlag)
{
	CALL_TRACE("setBlank(Boolean blankFlag)");

	isBlank_ = blankFlag;			// Store blank state

    // Cancel the leak comp lower timer if it is active.
	if (GuiTimerRegistrar::isActive(GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER))
	{		
		GuiTimerRegistrar::CancelTimer(GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER);
	}


	// Set the show flag on all drawable contents appropriately
	SIterR_Drawable children(*getChildren_());
	Drawable* pDraw;

	for (SigmaStatus ok = children.goFirst(); SUCCESS == ok;
		 ok = children.goNext())
	{
		pDraw = (Drawable *)&(children.currentItem());
		pDraw->setShow(!isBlank_);
	}

	// If the drawables are being shown then update the area to make sure
	// the UI is correct
	if (!isBlank_)
	{													// $[TI1]
		setFillColor(Colors::BLACK);
		updateArea_();
	}
	else
	{													// $[TI2]
		setFillColor(Colors::MIDNIGHT_BLUE);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened
//
//@ Interface-Description
// Called when a change in the GUI App event happens.  Passed the id of the
// event which changed.  
//---------------------------------------------------------------------
//@ Implementation-Description
// If the eventId is INOP, GUI_ONLINE_READY, or SETTINGS_TRANSACTION_SUCCESS
// then we do the following process.
//     If we are in patient setup mode, then we have to set all main
//     settings area burtons to flat (not accessable)
//        If there is a previous patient setup then depending on the
//        availability of the same patient settings we either display or hide
//        these main setting buttons.
//        If there is no  previous patient setup then simpily hide these
//        main setting buttons.
//     If we are not in patient setup mode, the depending on theavailability
//     of the same patient settings we either display or hide these main
//     setting buttons.
// If the eventId is COMMUNICATIONS_DOWN or SETTINGS_LOCKOUT then we do the
// following process.
//     Simpily set all main setting area burtons to flat (not accessable)
// if the eventId is SERVICE_READY, then we assert.
//
// Note that when this area is set to flat so the operator can't adjust
// any settings anymore, we are sending a CANCEL command to BD to cancel
// any offKey event if there exists one.
//
// $[01008] If the previous patient settings are not available ...
// $[01011] Flatten Main Settings, hide Lower Screen Select Area and
//			display Vent Start subscreen.
// $[01230] When the ventilator enters its settings lockout mode, the gui ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MainSettingsArea::guiEventHappened(GuiApp::GuiAppEvent eventId)
{
	CALL_TRACE("guiEventHappened(GuiApp::GuiAppEvent eventId)");

	switch (eventId)
	{
	case GuiApp::INOP:										// $[TI1.1]
	case GuiApp::GUI_ONLINE_READY:
	case GuiApp::SETTINGS_TRANSACTION_SUCCESS:
		if (LowerScreen::RLowerScreen.isInPatientSetupMode())
		{													// $[TI1.1.1]
			// In patient setup mode
			// Set all the buttons in this area to be flat.
			setToFlat();

			if (GuiApp::IsVentStartupSequenceComplete())
			{												// $[TI1.1.1.1]
				// There is a previous patient setting
				// then display the setting depending on whether
				// the same patient setting is available.
				setBlank(!SettingContextHandle::IsSamePatientValid());
			}
			else
			{												// $[TI1.1.1.2]
				setBlank(TRUE);
			}
		}
		else
		{													// $[TI1.1.2]
			// Finished patient setup procedure.
			setBlank(!SettingContextHandle::IsSamePatientValid());

			if (GuiApp::IsSettingsLockedOut()  ||
				!GuiApp::IsSettingsTransactionSuccess())
			{												// $[TI1.1.2.1]
				// Set all the buttons in this area to be flat.
				setToFlat();
				BdGuiEvent::RequestUserEvent(EventData::CLEAR_KEY);
			}
			else
			{												// $[TI1.1.2.2]
				setToUp();
			}
		}
		break;
		
	case GuiApp::COMMUNICATIONS_DOWN:						// $[TI1.2]
	case GuiApp::SETTINGS_LOCKOUT:
		setToFlat();
		BdGuiEvent::RequestUserEvent(EventData::CLEAR_KEY);
		break;
		
	case GuiApp::SERVICE_READY:								// $[TI1.3]
		AUX_CLASS_ASSERTION_FAILURE(eventId);
		break;
		
	default:												// $[TI1.4]
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: batchSettingUpdate
//
//@ Interface-Description
//	Called when display needs to be updated after a change in batch setting.
//---------------------------------------------------------------------
//@ Implementation-Description
// We are only interested in changes to the Accepted Context so we
// ignore all others.  Simply call updateArea_() to update our display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MainSettingsArea::batchSettingUpdate(
							 const Notification::ChangeQualifier qualifierId,
							 const ContextSubject*               pSubject,
							 const SettingId::SettingIdType      settingId
									)
{
	CALL_TRACE("batchSettingUpdate(qualifierId, pSubject)");

	// Ignore changes in contexts other than the Accepted Context and any
	// changes while we are blank.
	if (!isBlank_  &&   qualifierId == Notification::ACCEPTED)
	{													// $[TI1]
		// Update the Main Settings Area according to the setting change.
		updateArea_();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
//  This GuiTimerTarget virtual method is called when the 
//  LEAK_COMP_LOWER_DISPLAY_TIMER fires.  This allows to toggle the 
//  display of the current mode setting and the words "Leak Comp"
//  if Leak Comp is enabled.
//---------------------------------------------------------------------
//@ Implementation-Description 
// 
//  When Leak Comp is enabled, update the screen to display either  
//  the current mode setting or "Leak Comp". Otherwise, display only 
//  the current mode setting.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  The timer id must be the LEAK_COMP_LOWER_DISPLAY_TIMER.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void MainSettingsArea::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	const ContextSubject *const  P_ACCEPTED_SUBJECT =
							getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

    const DiscreteValue  VENT_TYPE =
	     P_ACCEPTED_SUBJECT->getSettingValue(SettingId::VENT_TYPE);

    const DiscreteValue  MODE_VALUE =
	     P_ACCEPTED_SUBJECT->getSettingValue(SettingId::MODE);

	switch (timerId)
	{
	case GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER: 

		if (GuiTimerRegistrar::isActive(GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER))
		{		

            // Display Leak Comp
			if (DisplayLeakComp_)
			{
				DisplayLeakComp_ = FALSE;
				if (VENT_TYPE == VentTypeValue::NIV_VENT_TYPE)
				{  
				   nivBackground_.setShow(TRUE);
				   leakCompText_.setColor(Colors::BLACK);
				}
				else
				{  
				  nivBackground_.setShow(FALSE);
				  leakCompText_.setColor(Colors::WHITE);
				}
				leakCompText_.setShow(TRUE);
				modeText_.setShow(FALSE);
				ventTypeText_.setShow(FALSE);
			}
			else  // Don't display leak comp
			{
				DisplayLeakComp_ = TRUE;
				if (VENT_TYPE == VentTypeValue::NIV_VENT_TYPE)
				{  
				   nivBackground_.setShow(TRUE);
				   ventTypeText_.setShow(TRUE);
				   modeText_.setColor(Colors::BLACK);			
				}
				else
				{  
				  nivBackground_.setShow(FALSE);
				  ventTypeText_.setShow(FALSE);
				  modeText_.setColor(Colors::WHITE);
				}
				leakCompText_.setShow(FALSE);
				modeText_.setShow(TRUE);
			}

		}
		break;
	default:
		AUX_CLASS_ASSERTION_FAILURE(timerId);
		break;
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
MainSettingsArea::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, MAINSETTINGSAREA,
									lineNumber, pFileName, pPredicate);
}

#endif // SIGMA_GUI_CPU
