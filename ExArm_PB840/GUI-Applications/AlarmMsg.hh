#ifndef AlarmMsg_HH
#define AlarmMsg_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmMsg - A container for displaying the main alarm messages.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmMsg.hhv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
class AlarmUpdate;
#include "Line.hh"
#include "TouchableText.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage


class AlarmMsg : public Container
{
public:
	AlarmMsg(void);
	~AlarmMsg(void);

	void updateDisplay(const AlarmUpdate& alarmUpdate);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);


protected:

private:
	// these methods are purposely declared, but not implemented...
	AlarmMsg(const AlarmMsg&);			// not implemented...
	void operator=(const AlarmMsg&);	// not implemented...

	//@ Data-Member: baseMsg1_;
	// Text for the 1st line of the alarm base message.
	TouchableText baseMsg1_;

	//@ Data-Member: baseMsg2_;
	// Text for the 2nd line of a two-line alarm base message.
	TextField baseMsg2_;

	//@ Data-Member: analysisMsg1_;
	// Text for the 1st line of the analysis/remedy message.
	TextField analysisMsg1_;

	//@ Data-Member: analysisMsg2_;
	// Text for the 2nd line of the analysis/remedy message.
	TextField analysisMsg2_;

	//@ Data-Member: analysisMsg3_;
	// Text for the 3rd line of the analysis/remedy message.
	TextField analysisMsg3_;

	//@ Data-Member: alarmSeparator_
	// The line which separates this alarm message from the next alarm
	// message.
	Line alarmSeparator_;

};

#endif // AlarmMsg_HH 
