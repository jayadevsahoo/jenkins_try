#include "StdAfx.h"
#include "MultiMediaTimer.h"
#include <algorithm>

MultiMediaTimer::MultiMediaTimer( UINT nPeriod )
{
	MMRESULT mmResult = timeBeginPeriod( nPeriod );
	if ( mmResult == TIMERR_NOCANDO )
		OutputDebugString( L"timeBeginPeriod return TIMERR_NOCANDO" );

	Period_ = nPeriod;
}

MultiMediaTimer::~MultiMediaTimer(void)
{
	vector<MMRESULT>::iterator it = Timers_.begin();
	for ( ; it != Timers_.end(); ++ it )
	{
		timeKillEvent( *it );
	}
	
	Timers_.clear();

	if ( timeEndPeriod( Period_ )== TIMERR_NOCANDO )
		OutputDebugString( L"timeEndPeriod return TIMERR_NOCANDO" );
}

MMRESULT MultiMediaTimer::setEvent( UINT uDelay, LPTIMECALLBACK fptc, DWORD_PTR dwUser )
{
	MMRESULT timerId = timeSetEvent( uDelay, uDelay, fptc, dwUser, TIME_PERIODIC|TIME_CALLBACK_FUNCTION);

	Timers_.push_back( timerId );

	return timerId;
}

MMRESULT MultiMediaTimer::killEvent( UINT nTimerId )
{
	vector<MMRESULT>::iterator it = std::find(Timers_.begin(), Timers_.end(), nTimerId);
	if (  it != Timers_.end() )
	{
		MMRESULT result = timeKillEvent( nTimerId );
		Timers_.erase( it );

		return result;
	}
	else
	{
		OutputDebugString( L"MultiMediaTimer::killEvent Timer ID not found." );
	}

	return TIMERR_NOCANDO;
}
