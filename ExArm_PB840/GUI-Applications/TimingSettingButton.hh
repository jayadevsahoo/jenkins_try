#ifndef TimingSettingButton_HH
#define TimingSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TimingSettingButton - A numeric setting button which displays a
// timing value in seconds, scaled down by 1000 from the stored setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TimingSettingButton.hhv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "NumericSettingButton.hh"

//@ Usage-Classes
//@ End-Usage

class TimingSettingButton : public NumericSettingButton
{
public:
	TimingSettingButton(Uint16 xOrg, Uint16 yOrg, Uint16 width,
				Uint16 height, ButtonType buttonType, Uint8 bevelSize,
				CornerType cornerType, BorderType borderType,
				TextFont::Size numberSize,
				Uint16 valueCenterX, Uint16 valueCenterY,
				StringId titleText, StringId unitsText,
				SettingId::SettingIdType settingId,
				SubScreen *pFocusSubScreen, StringId messageId,
				Boolean isMainSetting = FALSE);
	~TimingSettingButton(void);

	// SettingObserver virtual method
    virtual void  applicabilityUpdate(
                              const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject
                                     );

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
                                const SettingSubject*         pSubject);


private:
	// these methods are purposely declared, but not implemented...
	TimingSettingButton(const TimingSettingButton&);		// not implemented...
	void   operator=(const TimingSettingButton&);	// not implemented...
};


#endif // TimingSettingButton_HH 
