#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FlowSensor - Implements the inspiratory and expiratory flow sensors.
//---------------------------------------------------------------------
//@ Interface-Description
//      The class contains methods for converting the raw ADC count
//      to flow (LPM) and updating the processed value (filtered,
//      slope, filtered slope), for correcting the exhalation flow
//      to dry flow, for reading the processed values, and for
//      initializing the filtered values.
//
//      There are two instances of this class for the exhalation flow sensor.
//		One is the "normal" implementation of the exhalation flow sensor
//		and the other is to compensate the exhalation flows back to dry flows
//		(with no humidity).
//
//      The flow sensors are used by breath delivery and service mode
//      to measure the actual flow.  Methods are implemented to update the
//		raw counts to flow (LPM) and update the filtered, slope, and filtered
//		slope values; to get the filtered value; to get the slope value; to
//      get the filtered slope value; to initialize the filtered,
//		slope, filteredSlope, and previous values; to set the conversion
//		factor used to convert the exhalation flow to dry; and to set the
//		mix factor used to compensate exhalation flow for o2 concentration.
//
//		This class provides support for the BD_CPU and the GUI_CPU.
//---------------------------------------------------------------------
//@ Rationale
//      Abstraction of flow sensors.
//---------------------------------------------------------------------
//@ Implementation-Description
//      All the calculation for the conversion from ADC counts to
//      flow reading is done in the method rawToEngValue_().
//		A temperature sensor is also associated
//      with the flow sensor for temperature compensation.  The class
//      ProcessedValue is used to handle further processing of the flow
//      reading as required.  A correction to dry gas and O2 mix is needed
//      for the exhalation flow measurements.
//
// $[00401] $[04054]
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlowSensor.ccv   26.0.1.0   04 Feb 2013 10:36:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 013 By: syw    Date: 07-Apr-1998   DR Number: DCS 5065
//  	Project:  Sigma (840)
//		 	BiLevel initial version.  Added code to support NEW_SENSOR
//			(conditionally compiled).
//
//  Revision: 012 By: syw    Date: 09-Oct-1997   DR Number: DCS 2536
//  	Project:  Sigma (R8027)
//			Eliminated redundant TIs and code which was left over from
//			DCS 1908.
//
//  Revision: 011 By: syw    Date: 07-Jul-1997   DR Number: DCS 2283
//  	Project:  Sigma (R8027)
//			Use getPredictedTemperature() to obtain flow temperature during
//			non service states on BD_CPU only.  Eliminate need to compensate
//			for temperatureCorrection since it is already handled by the
//			TemperatureSensor class.
//
//  Revision: 010 By: syw    Date: 03-Jul-1997   DR Number: DCS 2282
//  	Project:  Sigma (R8027)
//		Description:
//			Removed calTemp_ and getFlowTemperature_() method since calTemp
//			is now constant.
//
//  Revision: 009 By: syw    Date: 03-Jul-1997   DR Number: DCS 1908
//  	Project:  Sigma (R8027)
//		Description:
//			Use table lookup to obtain correctionFactor instead of sqrt().
//
//  Revision: 008 By: syw    Date: 13-Mar-1997   DR Number: DCS 1827
//  	Project:  Sigma (R8027)
//		Description:
//			Calculate correctionFactor using sqrt() instead of the table.
//			Eliminate mix compensation... moved to ExhFlowSensor class.
//			Delete pSecondaryFlowSensorCalInfo_, O2Percent_, DryFactor_,
//			sensorId_.  Added getFlowTemperature_() to support ExhFlowsensor class.
//
//  Revision: 007 By: syw    Date: 18-Feb-1997   DR Number: DCS 1827
//  	Project:  Sigma (R8027)
//		Description:
//			Change cal temp to 32.0 for exhalation flow sensors.
//
//  Revision: 006 By: syw    Date: 18-Feb-1997   DR Number: DCS 1780
//  	Project:  Sigma (R8027)
//		Description:
//			Added pSecondaryFlowSensorCalInfo_ as arguments to constructor.
//			Added calculation of flow using pSecondaryFlowSensorCalInfo_ in
//			rawToEngValue_().  Changed updateValue() to handle applied mix
//			instead of MixFactor_.  Renamed MixFactor_ to O2Percent_.
//
//  Revision: 005 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added updateValue() with flowCounts and tempCounts as arguments
//			for TUV support.  Added BD_CPU macros.  Added constructor for
//			GUI CPU.
//
//  Revision: 004 By: syw    Date: 13-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added EXH_O2_FLOW_SENSOR.
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SafetyNetSensorData * and FlowSensorCalInfo * to constructor
//			arguments.  Modified updateValue to call the base class updateValue()
//			so the Safety Net Sensor Data can be checked.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================
// TODO E600_LL: to review and get rid of all "#ifdef NEW_SENSOR"
#ifdef NEW_SENSOR
#define private public
#define protected public
#endif // NEW_SENSOR

#include "FlowSensor.hh"

//@ Usage-Classes

#include "TemperatureSensor.hh"
// TODO E600_LL: to check if it is nec. to include
//#include "TaskControlAgent.hh"

//@ End-Usage

#ifdef SIGMA_DEBUG
//#include <stdio.h>
#endif //SIGMA_DEBUG

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlowSensor()
//
//@ Interface-Description
//      Constructor.  This method takes five arguments, SafetyNetSensorData *,
//		adcId, FlowSensorCalInfo *, TemperatureSensor&, alpha for
//		the BD_CPU.  The GUI_CPU has FlowSensorCalInfo *, and 
//		TemperatureSensor& as arguments.  This method has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The data members are initialized to values passed in.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

FlowSensor::FlowSensor(
  SafetyNetSensorData *pData,
  const AdcChannels::AdcChannelId adcId, 
  TemperatureSensor& temperatureSensor,
  const Real32 alpha,
  FlowSensorCalInfo *pFlowSensorCalInfo
) : Sensor(adcId, pData),					    // $[TI1]
    pFlowSensorCalInfo_(pFlowSensorCalInfo),
    rTemperatureSensor_(temperatureSensor),
    processedValues_( alpha)
{
	CALL_TRACE("FlowSensor::FlowSensor( SafetyNetSensorData *pData \
				const AdcChannels::AdcChannelId adcId, \
				TemperatureSensor& temperatureSensor, \
				const Real32 alpha, \
				FlowSensorCalInfo *pFlowSensorCalInfo)") ;
}

#else

FlowSensor::FlowSensor(
  TemperatureSensor& temperatureSensor,
  FlowSensorCalInfo *pFlowSensorCalInfo
) : Sensor(),    								// $[TI3]
    pFlowSensorCalInfo_(pFlowSensorCalInfo), 
    rTemperatureSensor_(temperatureSensor)  
{
	CALL_TRACE("FlowSensor::FlowSensor( \
				TemperatureSensor& temperatureSensor, \
				FlowSensorCalInfo *pFlowSensorCalInfo)") ;
}

#endif // defined (SIGMA_BD_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FlowSensor()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
FlowSensor::~FlowSensor( void)
{
	CALL_TRACE("FlowSensor::~FlowSensor( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValue()
//
//@ Interface-Description
//      This method take no arguments and converts the raw sample to
//		engineering units and update the processed values (slope, filter,
//		etc.).
//---------------------------------------------------------------------
//@ Implementation-Description
//      Convert the raw data into flow (LPM).  Correct for dry
//      gas (exhalation dry flow).  Update the filtered and slope values.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

void
FlowSensor::updateValue( void)
{
	CALL_TRACE("FlowSensor::updateValue( void)") ;

	// $[TI1]

	Sensor::updateValue() ;

    processedValues_.updateValues( engValue_) ;
}

#endif // defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValue
//
//@ Interface-Description
//		This method has the flow and temperature values in counts as
//		arguments and returns the corresponding flow value in lpm.
//		This method differs from the updateValue( void) in that the rawCount
//		is passed in instead of being obtained by the AdcChannels class.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Update the temperature value before rawToEngValue_ uses the
//		value.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

#if defined (SIGMA_GUI_CPU)

Real32
FlowSensor::updateValue( const AdcCounts flowCounts, const AdcCounts tempCounts)
{
	CALL_TRACE("FlowSensor::updateValue( const AdcCounts flowCounts, \
				const AdcCounts tempCounts)") ;

    // $[TI2]
#ifdef NEW_SENSOR
	Real32 newTemp = pFlowSensorCalInfo_->convertTemperature( tempCounts) ;
	rTemperatureSensor_.engValue_ = newTemp ;
#else
	rTemperatureSensor_.updateValue( tempCounts) ;
#endif // NEW_SENSOR

	tempCounts_ = tempCounts;	// E600 needs temperature ADC count to convert to flow
								// just save it here and use it in rawToEngValue_()
	Real32 flow = rawToEngValue_( flowCounts) ;

	return( flow) ;
}

#endif // defined (SIGMA_GUI_CPU)
 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
FlowSensor::SoftFault( const SoftFaultID  softFaultID,
                       const Uint32       lineNumber,
                       const char*        pFileName,
                       const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    
    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, FLOWSENSOR, lineNumber,
                             pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)
 
//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: rawToEngValue_()
//
//@ Interface-Description
//      The method takes one argument, count, and returns the flow (LPM)
//      converted from the value passed in.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Convert ADC counts to temperature corrected voltage.  The
//      flow sensor Z factor is applied to the voltage.  The result
//      is then passed to method flowSensorCalInfo::getFlow() which
//      return the flow.
// 		$[04326]
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Real32
FlowSensor::rawToEngValue_( const AdcCounts count)   const
{
	CALL_TRACE("FlowSensor::rawToEngValue_( const AdcCounts count)") ;

	Real32 flow = 0.0;

#if defined (SIGMA_BD_CPU)

	flow = pFlowSensorCalInfo_->getFlow(count, rTemperatureSensor_.getCount()) ;

#endif // defined (SIGMA_BD_CPU)

#if defined (SIGMA_GUI_CPU)

	flow = pFlowSensorCalInfo_->getFlow(count, tempCounts_) ;
	
#endif	
	return( flow) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================







