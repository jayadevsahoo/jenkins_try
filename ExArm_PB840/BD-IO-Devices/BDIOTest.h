
//*****************************************************************************
//
//  BDIOTest.h - Header file for the BDIOTest_t class.
//
//*****************************************************************************

#ifndef BDIO_TEST_H

#define BDIO_TEST_H

#include "A2dReadingsStructs.h"
#include "DataFilter.h"
#include "SettingId.hh"

enum BdioTest_t
{
	BDIO_TEST_NONE,
	BDIO_TEST_EXH_AZ,
	BDIO_TEST_DOUT,
	BDIO_TEST_DIN,
	BDIO_TEST_INSP_SOLENOID,
	BDIO_TEST_EXH_SOLENOID,
	BDIO_TEST_CROSSOVER_SOLENOID,
	BDIO_TEST_SAFETY_SOLENOID,
	BDIO_TEST_DAC_AIR,
	BDIO_TEST_DAC_O2,
	BDIO_TEST_DAC_EXH,
	BDIO_TEST_DAC_DESIRED_FLOW,
	BDIO_TEST_AIR_FLOW,
	BDIO_TEST_O2_FLOW,
	BDIO_TEST_AIR_FLOW_SENSOR,
	BDIO_TEST_O2_FLOW_SENSOR,
	BDIO_TEST_EXH_FLOW_SENSOR,
	BDIO_TEST_PRESSURE_SENSOR,
	BDIO_CALIBRATION,
	BDIO_TEST
};

enum BdioTestState_t
{
	BDIO_TEST_STATE1,
	BDIO_TEST_STATE2,
	BDIO_TEST_STATE3,
	BDIO_TEST_STATE4,
	BDIO_TEST_STATE5
};

enum BdioCalType_t
{
	CAL_NONE,
	CAL_AIR_PSOL,
	CAL_O2_PSOL,
	CAL_EXH_VALVE,
	CAL_EXH_FLOW_SENSOR,
	CAL_FLOW_SENSOR_OFFSET,
	CAL_PSOL_LIFTOFF,
	CAL_ALL,
	CIRCUIT_CHECK
};

enum BdioTestType_t
{
	TEST_NONE,
	TEST_EXH_SENSOR_AZ,
	TEST_AIR_FLOW,
	TEST_O2_FLOW
};

enum SettingDebugIdType_t
{
	LOW_SETTING_DEBUG_ID,
	COMPLIANCE_ID = LOW_SETTING_DEBUG_ID,
	NUM_BOUNDED_SETTING_DEBUG_IDS,
	NUM_SETTING_DEBUG_IDS=NUM_BOUNDED_SETTING_DEBUG_IDS
};

struct CalStatusMsg_t
{
	UINT16		msgId;
	UINT16		pktSize;
	UINT		seqNumber;	// = 0;
	UINT		calStatus;
	UINT		CheckSum;
};

#define NUM_TEST_SETTING_IDS	22

struct SettingData_t
{
	UINT16	settingId;
	INT8   reserved;
	INT8   boundedPrec;

	union   // anonymous union...
	{
		float  boundedValue;
		INT   discreteValue;
	};
};

struct SettingsPacket_t
{
	UINT16			MsgId;
	UINT16			NumOfSettings;
	SettingData_t	Data[NUM_TEST_SETTING_IDS];
};

struct SettingsRequestPacket_t
{
	UINT16			MsgId;
	UINT16			NumOfSettings;
};

struct SettingsReplyPacket_t
{
	UINT16			MsgId;
	UINT16			NumOfSettings;
	SettingData_t	Data[NUM_TEST_SETTING_IDS];
	UINT			Checksum;
};

struct SettingsDebugPacket_t
{
	UINT16			MsgId;
	UINT16			NumOfSettings;
	SettingData_t	Data[NUM_SETTING_DEBUG_IDS];
};


class BDIOTest_t
{
public:
    BDIOTest_t(void);
    ~BDIOTest_t(void);

	bool newCycle();
	void SetRawReadings(RawA2dReadings_t Readings) { RawA2dReadings = Readings; }
	void SetCalType(BdioCalType_t CalType) 
	{ 
		BdioCalType = CalType; 
		BdioTestCase = BDIO_CALIBRATION;
		bEnaCalStatus = true;
	}
	void SetTestType(BdioTestType_t TestType) { BdioTestType = TestType; BdioTestCase = BDIO_TEST; }
	BdioCalType_t GetCalType() { return BdioCalType; }
	void CancelCal(BdioCalType_t CalType);
	void SendCalStatus(uint32_t Status);
	void ProcessRxSettingsPacket(char *pSettingsPacket);
	void ProcessTxSettingsPacket(char *pSettingsPacket);
	void ProcessRxSettingsDebugPacket(char *pSettingsPacket);

	void Initialize();

protected:

	bool ProcessBdioTest();
	void ConvertToEngineeringUnits(ConvertedA2dReadings_t &Converted);
	void SetCommandValues();
	void ResetAllValves();
	void SendDebugPacket();
	void SendSettingsPacket();

	ConvertedA2dReadings_t ConvertedData;
	RawA2dReadings_t RawA2dReadings;

	BdioTestState_t TestState;
	BdioTest_t BdioTestCase;
	BdioCalType_t BdioCalType;
	BdioTestType_t BdioTestType;

	uint16_t ProgressCount;
	bool bEnaCalStatus;
};


#endif