#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: SafetyNetTest - Base class for the BD safety net tests.
//---------------------------------------------------------------------
//@ Interface-Description
//  Each Safety-Net check interfaces with different classes in the
//  system.  Some of the classes interfaced with include:  Sensor class
//  for the count or engineering value of a sensor reading, BreathRecord
//  for the Phase Type, GasSupply for the status of the air and O2 supplies,
//  Psol for the liftoff.
//---------------------------------------------------------------------
//@ Rationale
//  Used to derive BD runtime Safety-Net checks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Data members which are common to all BD runtime safety net
//  checks are defined in the base class.  The pure virtual method
//  checkCriteria() is declared.  Each derived runtime safety net
//  test defines this method to provide a required safety net check.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SafetyNetTest.ccv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By:  erm   Date:  15-MAY-2003    DR Number: 6054
//  Project:  AVER
//  Description:
//      Increase PRESSURE_SENSOR_STUCK_CRITERIA  to -20
//
//  Revision: 007  By:  sah   Date:  04-Aug-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Obsoleted patient type setting.
//
//  Revision: 006  By: iv     Date:  08-Jun-1999    DR Number: 5425
//  Project:  ATC
//  Description:
//      Added PSOL_COMMAND_OFFSET.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  iv    Date: 23-Nov-1998     DR Number:DCS 5266
//       Project:  Sigma (R8027)
//       Description:
//           Added MAX_EXH_FLOW_TEST_COUNT_LIMIT.
//
//  Revision: 003  By:  iv    Date: 08-Oct-1997     DR Number:DCS 1649
//       Project:  Sigma (R8027)
//       Description:
//           Changed MAX_PRESSURE_STUCK_TEST_CYCLES from 5 to 6.          .
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  05-Sep-1995    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================
#include "SafetyNetTest.hh"

//@ Usage-Classes
#include "MandTypeValue.hh"
#include "PhasedInContextHandle.hh"
#include "ControllersRefs.hh"
#include "VolumeController.hh"
#include "PhaseRefs.hh"
#include "BreathPhase.hh"

//@ End-Usage

//@ Code...

//@ Constant: MAX_PRES_SENSOR_STUCK_LIMIT
//  used as maximum absolute expiratory pressure reading
//  as a test criteria for pressure sensor stuck criterion
const Real32 MAX_PRES_SENSOR_STUCK_LIMIT = 0.3F;

//@ Constant: MAX_PSOL_STUCK_TEST_FLOW_RATE
//  used as maximum flow rate tolerance for PSOL stuck test
const Real32 MAX_PSOL_STUCK_TEST_FLOW_RATE = 100.0F;

//@ Constant: SLOWEST_PSOL_COMMAND_GRADIENT 
//  used in the calculation of low PSOL command limits 
const Real32 SLOWEST_PSOL_COMMAND_GRADIENT = 1.6825F;

//@ Constant: MIN_LOW_PSOL_COMMAND 
//  used in the calculation of low PSOL command limits 
const Real32 MIN_LOW_PSOL_COMMAND = 88.0F;

//@ Constant: QUICKEST_PSOL_COMMAND_GRADIENT 
//  used in the calculation of high PSOL command limits 
const Real32 QUICKEST_PSOL_COMMAND_GRADIENT = 2.6740F;

//@ Constant: MAX_HIGH_PSOL_COMMAND 
//  used in the calculation of high PSOL command limits 
const Real32 MAX_HIGH_PSOL_COMMAND = 216.0F;

//@ Constant: PSOL_COMMAND_AT_10_LPM
//  DAC value for PSOL at 10 LPM
const AdcCounts PSOL_COMMAND_AT_10_LPM = 1362;

//@ Constant: MIN_FLOW_TEST_COUNT_LIMIT 
//  minimum ADC count for flow sensor test
const AdcCounts MIN_FLOW_TEST_COUNT_LIMIT = 116;

//@ Constant: MAX_EXH_FLOW_TEST_COUNT_LIMIT 
//  maximun ADC count for exhalation flow sensor test
const AdcCounts MAX_EXH_FLOW_TEST_COUNT_LIMIT = 3686;

//@ Constant: MAX_FLOW_TEST_LIMIT
//  maximum ADC count for flow sensor test
const Int16 MAX_FLOW_TEST_LIMIT = 250; // LPM
  
//@ Constant: MIN_PSOL_STUCK_OPEN_FLOW_LPM  
// minimum PSOL stuck open test flow rate
const Real32 MIN_PSOL_STUCK_OPEN_FLOW_LPM = 2.0F;
   
//@ Constant: MAX_PSOL_LEAK_LPM 
// maximum PSOL leak rate
const Real32 MAX_PSOL_LEAK_LPM = 0.5F;
   
//@ Constant: MAX_PSOL_CLOSED_FLOW_LPM 
// maximum PSOL closed flow rate tolerance
const Real32 MAX_PSOL_CLOSED_FLOW_LPM = 10.0F;
   
//@ Constant: MAX_CYCLES_PSOL_STUCK_OPEN
// maximum time tolerance for PSOL stuck open
const Int16 MAX_CYCLES_PSOL_STUCK_OPEN = 10;    // 200ms
     
//@ Constant: MAX_CYCLES_PSOL_STUCK
// maximum time tolerance for PSOL stuck
const Int16 MAX_CYCLES_PSOL_STUCK = 10;         // 200ms
      
//@ Constant: MAX_PRESSURE_OOR_CYCLES
// maximum time tolerance for pressure sensor OOR
const Int16 MAX_PRESSURE_OOR_CYCLES = 13;        // 260ms

//@ Constant: MAX_FLOW_HIGH_OOR_CYCLES
// maximum flow rate for OOR high flow
const Int16 MAX_FLOW_HIGH_OOR_CYCLES = 10;      // 200ms
       
//@ Constant: MAX_FLOW_LOW_OOR_CYCLES
// maximum flow rate for OOR low flow
const Int16 MAX_FLOW_LOW_OOR_CYCLES = 2;        // 40ms

//@ Constant: MAX_PRES_TEST_LIMIT
//  Maximum high pressure sensor test limit
const Real32 MAX_PRES_TEST_LIMIT = 140.0F;
//@ Constant: DELAY_AFTER_SUCTIONING_CYCLES
// number of cycles to after suction manuever before background check commences.
const Int16 DELAY_AFTER_SUCTIONING_CYCLES = 500;         // 10 seconds

//@ Constant: SUCTIONING_CYCLES
// number of cycles to declare suction manuever is occurring.
// this number must be smaller than MAX_PRESSURE_OOR_CYCLES
const Int16 SUCTIONING_CYCLES = (MAX_PRESSURE_OOR_CYCLES / 4);    // 60ms

//@ Constant: PRESSURE_SENSOR_SUCTION_CRITERIA 
//  criteria for dtermining a suction manuever is taking place 
const Real32 PRESSURE_SENSOR_SUCTION_CRITERIA = -30.0F;

//@ Constant: PRESSURE_SENSOR_STUCK_CRITERIA 
//  Concurrent criteria for pressure sensor stuck low detection 
const Real32 PRESSURE_SENSOR_STUCK_CRITERIA = -20.0F;

//@ Constant: MIN_PRES_TEST_LIMIT
//  Minimum inspiratory pressure sensor test limit
const Real32 MIN_PRES_TEST_LIMIT = -100.0F;
          
//@ Constant: PRESSURE_STUCK_TEST_PRES_CRITERIA
// threshold for testing pressure sensor stuck
const Real32 PRESSURE_STUCK_TEST_PRES_CRITERIA = 0.5F;
           
//@ Constant: PRESSURE_STUCK_TEST_FLOW_CRITERIA
// threshold for testing flow sensor stuck
const Real32 PRESSURE_STUCK_TEST_FLOW_CRITERIA = 0.5F;

//@ Constant: MAX_PRESSURE_STUCK_TEST_CYCLES
//  Maximum number of cycles the pressure sensor  stuck
//  criteria is met before an event declared
const Int16 MAX_PRESSURE_STUCK_TEST_CYCLES = 6;
 
//@ Constant: PSOL_CURRENT_TO_DAC_SCALE
// max PSOL current to DAC factor
const Real32 PSOL_CURRENT_TO_DAC_SCALE = 4095.0F / 730.0F;
  
//@ Constant: PSOL_COMMAND_OFFSET
// offset to add to the high limit of the PSOL command
const DacCounts PSOL_COMMAND_OFFSET = 200;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SafetyNetTest()
//
//@ Interface-Description
//  Constructor.  This method takes a Background Identifier and an
//  integer as arguments used to initialize protected data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The backgndEventId_ and maxCyclesCriteriaMet_ data members are set
//  to the passed parameters.  backgndCheckFailed_ is initialized to
//  FALSE and numCyclesCriteriaMet_ is set to 0.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

SafetyNetTest::SafetyNetTest( const BkEventName eventId, const Int16 maxCycles ):
    backgndEventId_( eventId ),
    maxCyclesCriteriaMet_( maxCycles ),
    backgndCheckFailed_( FALSE ),
    numCyclesCriteriaMet_( 0 ),
	isSuctioning_ ( 0 ),
    suctioningState_ ( NO_SUCTIONING ),
	numCyclesAfterSuctionCriteriaMet_ ( 0 ),
	numCyclesSuctionCriteriaMet_ ( 0 )
   
{
    // $[TI1]
    CALL_TRACE("SafetyNetTest::SafetyNetTest( const Backgnd::BkEventName id, \
                                              const Int16 maxCycles )");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SafetyNetTest()
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

SafetyNetTest::~SafetyNetTest( void )
{
    CALL_TRACE("SafetyNetTest::~SafetyNetTest (void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//  This method takes four arguments: softFaultID, lineNumber, pFileName,
//  and pPredicate and is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SafetyNetTest::SoftFault( const SoftFaultID  softFaultID,
                          const Uint32       lineNumber,
                          const char*        pFileName,
                          const char*        pPredicate )
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
      
    FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, SAFETYNETTEST,
                             lineNumber, pFileName, pPredicate ) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCurrentDesiredO2Flow_()
//
//@ Interface-Description
//  This method accepts no argument and returns a Real32.
//  This method returns to the client the current O2 desired flow.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Base on the current ventilation type, it invokes the proper
//  controller and retrieves the current O2 desired flow.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

Real32
SafetyNetTest::getCurrentDesiredO2Flow_( void ) const
{
    Real32 currentO2DesiredFlow = 0.0F;

    BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();

    CLASS_PRE_CONDITION( pBreathPhase != NULL );

    if ( (pBreathPhase == (BreathPhase *)&RVcvPhase ) ||
         (pBreathPhase == (BreathPhase *)&RApneaVcvPhase) )
    {
    // $[TI1]
        currentO2DesiredFlow = RO2VolumeController.getDesiredFlow();
    }
    else
    {
    // $[TI2]
        currentO2DesiredFlow = RO2FlowController.getDesiredFlow();
    }

    return( currentO2DesiredFlow );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCurrentDesiredAirFlow_()
//
//@ Interface-Description
//  This method accepts no argument and returns a Real32.
//  This method returns to the client the current air desired flow.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Base on the current ventilation type, it invokes the proper
//  controller and retrieves the current air desired flow.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

Real32
SafetyNetTest::getCurrentDesiredAirFlow_( void ) const
{

    Real32 currentAirDesiredFlow = 0.0F;

    BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();

    CLASS_PRE_CONDITION( pBreathPhase != NULL );

    if ( (pBreathPhase == (BreathPhase *)&RVcvPhase ) ||
         (pBreathPhase == (BreathPhase *)&RApneaVcvPhase) )
    {
    // $[TI1]
        currentAirDesiredFlow = RAirVolumeController.getDesiredFlow();
    }
    else
    {
    // $[TI2]
        currentAirDesiredFlow = RAirFlowController.getDesiredFlow();
    }

    return( currentAirDesiredFlow );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsSuctioning()
//
//@ Interface-Description
//  This method takes no arguments and returns TRUE is suctioning is
//  determine to be active.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  After a suctioning event has been determine to have occurred.
// 
// Exhalation Pressure < PRESSURE_SENSOR_SUCTION_CRITERIA &&
// Inhaltion Pressure < PRESSURE_SENSOR_SUCTION_CRITERIA
// 
// Suctioning is still determine to be TRUE after DELAY_AFTER_SUCTIONING_CYCLES
// This allows for any transients to settle out before anymore background checks
// are performed.
//
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
Boolean SafetyNetTest::IsSuctioning(const Real32 exhPressure,  
                                    const Real32 inspPressure)
{

	switch (suctioningState_)
	{
	
	case NO_SUCTIONING:

		if (exhPressure  < PRESSURE_SENSOR_SUCTION_CRITERIA &&
			inspPressure < PRESSURE_SENSOR_SUCTION_CRITERIA)
		{
			if (++numCyclesSuctionCriteriaMet_ >= SUCTIONING_CYCLES)
			{
				suctioningState_ = SUCTIONING_INPROGRESS;
				numCyclesAfterSuctionCriteriaMet_ = 0;
				numCyclesSuctionCriteriaMet_ = 0;
			}
		}
		else
		{
			numCyclesSuctionCriteriaMet_ = 0;
		}
		break;

	case SUCTIONING_INPROGRESS:

		if (!(exhPressure  < PRESSURE_SENSOR_SUCTION_CRITERIA &&
			  inspPressure < PRESSURE_SENSOR_SUCTION_CRITERIA))
		{
			suctioningState_ = SUCTIONING_DELAY;
		}
		break;

	case SUCTIONING_DELAY:

		if (++numCyclesAfterSuctionCriteriaMet_ >= DELAY_AFTER_SUCTIONING_CYCLES)
		{
			suctioningState_ = NO_SUCTIONING;
			numCyclesSuctionCriteriaMet_ = 0;
			numCyclesAfterSuctionCriteriaMet_ = 0;
		}
		else // restart the delay if suctioning occurred during the delay time
		{
			if (exhPressure  < PRESSURE_SENSOR_SUCTION_CRITERIA &&
				inspPressure < PRESSURE_SENSOR_SUCTION_CRITERIA)
			{
				suctioningState_ = SUCTIONING_INPROGRESS;
				numCyclesSuctionCriteriaMet_ = 0;
				numCyclesAfterSuctionCriteriaMet_ = 0;
			}
		}
		break;

	default:

		// never should get here but if we ever do just reset suctioning
		// state machine to starting point.

		suctioningState_ = NO_SUCTIONING;
		numCyclesAfterSuctionCriteriaMet_ = 0;
		numCyclesSuctionCriteriaMet_ = 0;
		break;

	} // switch (suctioningState)

	if (NO_SUCTIONING == suctioningState_)
	{
		isSuctioning_ = FALSE;
	}
	else 
	{
		isSuctioning_ = TRUE;
	}

	return (isSuctioning_);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


