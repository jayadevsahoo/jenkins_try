#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Fio2CalInfo - This class is the interface between the
//                      Fio2Monitor and the NovRamManager for
//                      retrieval and updates of calibration
//                      information.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is the interface between the NovRamManager and the
//  Fio2Monitor objects.  This class is used as a vehicle for exchange
//  of Fio2Monitor calibration information between the Fio2Monitor and
//  the NovRamManager.
//---------------------------------------------------------------------
//@ Rationale
//  This class is the interface between the NovRamManager and the
//  Fio2Monitor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Two public methods are available for Fio2Monitor to retrieve 
//  and update Fio2Monitor calibration information in the NovRamManager.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//
//@ Modification-Log
//
//  Revision: 004 By: iv     Date: 24-Sep-1999   DR Number: 5536
//  Project:  ATC
//	Description:
//		Changed fio2SensorCalInfo_.countAt0Percent from 1028 to 1020.
//
//  Revision: 003 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002  By: iv    Date:  16-Sep-1997    DR Number: DCS 2337
//     Project:  Sigma (R8027)
//        Description:
//             Change the default value of the O2 sensor calibration
//             at 100% O2.
//
//  Revision: 001  By:  by    Date:  05-Jan-1996    DR Number: 
//
//       Project:  Sigma (R8027)
//
//       Description:
//             Initial version
//
//=====================================================================

#include "Fio2CalInfo.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Fio2CalInfo()  [Constructor]
//
//@ Interface-Description
//  Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The constructor initialized all Fio2Monitor calibration data
//  members to their default values.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

Fio2CalInfo::Fio2CalInfo( void )
{
    // $[TI1]
    CALL_TRACE("Fio2CalInfo::Fio2CalInfo( void )");

	// TODO E600 MS May need to come up with a more accurate number
	// for countAt0Percent.
	fio2SensorCalInfo_.countAt0Percent = 33;
    fio2SensorCalInfo_.countAt100Percent = 3100;
    fio2SensorCalInfo_.calInspPresAt100Percent = 1028.0F;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Fio2CalInfo()  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Not implemented.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

Fio2CalInfo::~Fio2CalInfo(void)
{
     CALL_TRACE("Fio2CalInfo::~Fio2CalInfo(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Fio2CalInfo
//
//@ Interface-Description
//  Copy constuctor.  Takes a reference to a Fio2CalInfo as an
//  argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This copy constructor sets this object equal to the input objects.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

Fio2CalInfo::Fio2CalInfo( const Fio2CalInfo& rFio2CalInfo )
{
    CALL_TRACE("Fio2CalInfo::Fio2CalInfo( const Fio2CalInfo& rFio2CalInfo )") ;
     
    // $[TI2]
    fio2SensorCalInfo_.countAt0Percent 
	    = rFio2CalInfo.fio2SensorCalInfo_.countAt0Percent;
    fio2SensorCalInfo_.countAt100Percent 
	    = rFio2CalInfo.fio2SensorCalInfo_.countAt100Percent;
    fio2SensorCalInfo_.calInspPresAt100Percent 
	    = rFio2CalInfo.fio2SensorCalInfo_.calInspPresAt100Percent;
}
                           
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: =operator
//
//@ Interface-Description
//  = operator.  Takes a reference to a Fio2CalInfo as an
//  argument and returns no value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set every element in the Fio2CalInfo object equal to the object
//  passed in.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Fio2CalInfo::operator=( const Fio2CalInfo& rFio2CalInfo )
{
    CALL_TRACE("Fio2CalInfo::operator=( const Fio2CalInfo& rFio2CalInfo )");

    if ( this != &rFio2CalInfo )
    {
    // $[TI1]
        fio2SensorCalInfo_.countAt0Percent
            = rFio2CalInfo.fio2SensorCalInfo_.countAt0Percent;

        fio2SensorCalInfo_.countAt100Percent
            = rFio2CalInfo.fio2SensorCalInfo_.countAt100Percent;

        fio2SensorCalInfo_.calInspPresAt100Percent
            = rFio2CalInfo.fio2SensorCalInfo_.calInspPresAt100Percent;
    }
    // $[TI2]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//  [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Fio2CalInfo::SoftFault( const SoftFaultID  softFaultID,
                        const Uint32       lineNumber,
                        const char*        pFileName,
                        const char*        pPredicate )
{
      CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

      FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, FIO2CALINFO,
                               lineNumber, pFileName, pPredicate );
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
  
//=====================================================================
//
//  Private Methods...
//
//=====================================================================


