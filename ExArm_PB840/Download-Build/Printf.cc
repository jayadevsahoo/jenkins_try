#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Printf - Used to ensure null functionality in PRODUCTION mode.
//---------------------------------------------------------------------
//@ Interface-Description
//  The purpose of this file is to override and nullify the standard 
//  I/O functions fgetc() and fputc() when the system is compiled for
//  PRODUCTION. This is a precaution to assure that no task issues an
//  I/O request that would place the task in an interminable I/O wait 
//  state since the PRODUCTION kernel is configured without run-time
//  support for console input/output.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Download-Build/vcssrc/Printf.ccv   25.0.4.0   19 Nov 2013 14:02:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc    Date:  27-Jan-2011    SCR Number: 6707
//  Project:  XENA2
//  Description:
//	Override the lower level I/O functions fgetc() and fputc() to
//	inhibit serial port I/O during download.
//
//=====================================================================

//@ Usage-Classes
//@ End-Usage

//=====================================================================
//
//  Global Function...
//
//=====================================================================

#if defined(SIGMA_PRODUCTION)

#include <stdio.h>

extern int
fputc(_ANSIPROT2(int, FILE *))
{
	return EOF;
}

extern int
fgetc(_ANSIPROT1(FILE *))
{
	return EOF;
}

#endif // defined(SIGMA_PRODUCTION)
