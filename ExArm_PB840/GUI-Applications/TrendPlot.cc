#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendPlot - A drawable class that can plot a specified 
// stream of trend data against time for use in the trend graph subscreen.
//---------------------------------------------------------------------
//@ Interface-Description
// The purpose of TrendPlot is to display a plot of specified trended data
// against time.  The plot is displayed on a TrendPlotBasis grid whose X
// axis is displayed with timestamps extracted from the trended data as it
// is plotted and Y axis is scaled dynamically according to the trended
// data. 
// 
// Each data point is connected to its previous data point, thus forming
// a continuous graph.  The trended data is limited to the area of the
// grid. Out-of-range plot points are not displayed.
//
// This class derives from TrendPlotBasis which handles the drawing of
// the grid on which the trended data is plotted. TrendPlot is devoted
// purely to the plotting of the actual trended data.
//
// A trend plot is plotted via calls to the update() method.  
// 
// erase() calls an erase() method in the base class which erases the
// trended plot. The activate() method should always be called
// before TrendPlot is to be displayed.
//---------------------------------------------------------------------
//@ Rationale
// This class handles all the logic for displaying trended data
// against time in a graphical format.
//---------------------------------------------------------------------
//@ Implementation-Description
// The basic functionality of TrendPlot is contained in the update() method
// which is responsible for almost all the trend graph plotting.  
// See update() for more information.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// which verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendPlot.ccv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
// 
//  Revision: 001  By:  ksg	   Date:  20-Jun-2007    SCR Number: 6237
//  Project:  TREND
//  Description:
//		Trend project initial version.
//=====================================================================

#include "TrendPlot.hh"
#include "BaseContainer.hh"
#include "GuiAppClassIds.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendPlot  [Constructor]
//
//@ Interface-Description
//  Default constructor for TrendPlot.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes all data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendPlot::TrendPlot() 
	:   TrendPlotBasis(),
	currX_(0),
	lastX_(0),
	currY1_(0),
	lastY1_(0),
	lineColorLeft_(Colors::WHITE)
{
	CALL_TRACE("TrendPlot::TrendPlot(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendPlot  [Destructor]
//
//@ Interface-Description
//  Destroys TrendPlot.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendPlot::~TrendPlot(void)	
{
	CALL_TRACE("TrendPlot::TrendPlot(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Activates a TrendPlot.  Should be called before the plot is displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call the base class activate() method and call erase() to do some
//  initializing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendPlot::activate(void)
{
	TrendPlotBasis::activate();
	erase();                                    
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
//  Draws the trend plot for a particular parameter provided in the dataset.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If this method is called while this drawable is not visible then the call
//  is ignored.
//
//  The TrendDataSet provided represents a single parameter at up to
//  TrendDataMgr::MAX_ROWS sample points. This function erases the previous
//  trended display, loops through each of the sample points and draws them 
//  successively on the plot, connecting them, from the newest (rightmost)
//  to the oldest (leftmost).
// 
//  Satisfies the following requirements: 
//  [[TR02179] [PRD 1510] Each graph shall display the minimum and
//  maximum range ...
//  [[TR01186] [PRD 1510] The Trend Data minimum and maximum range values
//  shall be automatically calculated ..
//  [[TR01186] [PRD 1511]The graphical data shall be displayed with the
//  oldest data on the leftmost X-axis and...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendPlot::update(const TrendDataSet& rTrendDataSet, Uint16 dataColumn)
{
	CALL_TRACE("update(TrendDataSet&, Uint16)");

	// Ignore call if not visible
	if (!isVisible())
	{
		return;
	}

	// start with a clean slate
	erase();

	// set up the Y axis in the base class
	autoScaleYAxis(rTrendDataSet, dataColumn);

	// Initiate a background repaint so the plot is erased before directDraw is initiated.
	getBaseContainer()->repaint();

	// update the event markers first
	updateEventMarkers_(rTrendDataSet);

	// update the X axis
	updateXAxis(rTrendDataSet);

	Uint16 numRows = rTrendDataSet.getDataSetSize();
	const TrendDataValues& trendData = rTrendDataSet.getTrendDataValues(dataColumn);

	// TRUE when we have the direct draw semaphore
	Boolean isDisplayLocked = FALSE;			 

	// Process the data array, converting each value
	// into a point on the plot
	TrendSelectValue::TrendSelectValueId    colId = trendData.getId();

	plotLine_.setColor(lineColorLeft_);
	plotLine_.setShow(TRUE);

	Boolean isFirstPoint = TRUE;

	// Loop through all rows in the data set (max TrendDataMgr::MAX_ROWS).
	for (Uint16 row = 0; row < numRows; row++)
	{
		currX_ = lastXPixel_ - row;

		AUX_CLASS_ASSERTION(currX_ > 0, currX_);

		// Is there valid data or should a gap be shown
		const TrendValue & trendValue = trendData.getValue( row );
		if (!trendValue.isValid)
		{
			// data is not plotted for "gaps" - restart plot when data is valid
			isFirstPoint = TRUE;
		}
		else
		{
			currY1_ = y1ValueToPoint_( trendValue.data.realValue );

			if (isFirstPoint)
			{
                // The first point has no previous coordinate, so set lastX and lastY to current values
				// This will prevent a connecting line when we start a new graph or 'lift the pen'
				lastX_ = currX_;
				lastY1_ = currY1_;
				isFirstPoint = FALSE;
			}

			if (!isDisplayLocked)
			{
				// Display is available to update. Grab semaphore
				setDirectDraw(TRUE);
				isDisplayLocked = TRUE;
			}

			// draw line from previous to current point
			plotLine_.setVertices( lastX_, lastY1_, currX_, currY1_ );
			plotLine_.drawDirect();

			// if a higher priority task is waiting for the display,
			// unlock the display to yield to the higher priority task
			if (isDisplayLockPending())
			{
				setDirectDraw(FALSE);
				isDisplayLocked = FALSE;
			}

			lastY1_ = currY1_;
			lastX_  = currX_;
		}
	}											 // end of for( row)

	// If we have access to the display we must release it.
	if (isDisplayLocked)
	{
		setDirectDraw(FALSE);
		isDisplayLocked = FALSE;
	}

	// reset and hide the line from the background refresh
	plotLine_.setVertices(0,0,1,1);
	plotLine_.setShow(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateEventMarkers_
//
//@ Interface-Description
//  This method is used to display event indications in the trend plots
//---------------------------------------------------------------------
//@ Implementation-Description
//  Loops through the provided dataset and sets a tick mark above the 
//  graph at the x-location representing the event in the trend log.
//  
//  Satisfies the following requirements:
//  [[TR01107] [PRD 1531] Trend Graph Event Markers shall be indicated
//  by vertical yellow tick marks...
// 
//  [[TR01109] [PRD 1532] If more than one Trend Graph Event Mark is
//  present for a selected timestamp...
//---------------------------------------------------------------------
//@ PreCondition
//  Ensures drawable object is visible
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendPlot::updateEventMarkers_(const TrendDataSet& rTrendDataSet)
{
	CALL_TRACE("updateEventMarkers_(const TrendDataSet&)");

	if (!showEvents_)
	{
		return;
	}

	CLASS_PRE_CONDITION(isVisible());

	static const Int32 EVENT_MARKER_HEIGHT_ = 4;

	// TRUE when we have the semaphore
	Boolean isDisplayLocked = FALSE;             

	// Event markers are displayed above the grid in a 
	// container that's positioned there. Set up the relative
	// position of the event marker within this container and
	// change the X position for each event marker drawn.
	eventMarker_.setVertices(0, 0, 0, EVENT_MARKER_HEIGHT_ - 1);
	eventMarker_.setColor( Colors::YELLOW );
	eventMarker_.setShow(TRUE);

	Uint16 numRows = rTrendDataSet.getDataSetSize();

	for (Uint16 row = 0; row < numRows; row++)
	{
		currX_ = lastXPixel_ - row;
		AUX_CLASS_ASSERTION(currX_ > 0, currX_);

		if (rTrendDataSet.getTrendEvents(row).isAnyEventSet())
		{
			if (!isDisplayLocked)
			{
				// Display is available to update. Grab semaphore
				setDirectDraw(TRUE);
				isDisplayLocked = TRUE;
			}

			// draw event marker
			eventMarker_.setX(currX_);
			eventMarker_.drawDirect();

			// if a higher priority task is waiting for the display,
			// unlock the display to yield to the higher priority task
			if (isDisplayLockPending())
			{
				setDirectDraw(FALSE);
				isDisplayLocked = FALSE;
			}
		}
	}											 // foreach row

	// If we have access to the display we must release it.
	if (isDisplayLocked)
	{
		setDirectDraw(FALSE);
		isDisplayLocked = FALSE;
	}

	// reposition the event marker outside the grid and hide it
	// so the background refresh doesn't draw it
	eventMarker_.setX(0);
	eventMarker_.setShow(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: erase
//
//@ Interface-Description
//  Erases a trend plot.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call the base class's method first and then initialize data members to
//  indicate that the plot has been erased and we'll be starting anew if
//  update() is called next. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendPlot::erase(void)
{
	CALL_TRACE("TrendPlot::erase(void)");

	TrendPlotBasis::erase();
	lastX_ = currX_ = TrendDataMgr::MAX_ROWS - 1; 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendPlot::SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName,
						  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDPLOT,
							lineNumber, pFileName, pPredicate);
}
