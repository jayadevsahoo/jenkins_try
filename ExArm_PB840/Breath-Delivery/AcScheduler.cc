#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AcScheduler - the active BreathPhaseScheduler when the 
//  current mode is AC.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for scheduling breath phases during A/C mode.  The
// class is also responsible for relinquishing control to the next valid
// scheduler and for assuming control when A/C mode is requested.  A/C mode can
// be requested by the operator, upon emergency mode recovery and power-up
// sequence, when transition to the previous mode is required.
// Note that mode transition, as a response for setting change, occurs once the
// new mode setting is accepted (by the operator).
// This scheduler is responsible for enabling valid mode triggers for A/C
// mode and for enabling breath triggers that are not under the breath phase direct
// control. The AcScheduler is responsible for handling user events like 100% O2, 
// manual inspiration request, etc. and for handling setting change events like rate
// and mode changes. 
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates all the breathing rules specified for A/C mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The class implements methods for breath and mode transitions and for
// breath data updates. The ac scheduler is responsible for:
// - Determining the next breath when a breath trigger becomes active.
// - Initiating the activities required for the proper start of a new breath:
//    phase in settings, update breath record, set breath triggers, 
//    enable breath triggers, and phase out the previous breath phase, 
// - Relinquishing control when a mode trigger becomes active.
//    Activities while control is relinquished include: 
//    disable out of date breath triggers, determine which is the next
//    valid scheduler, enable breath triggers for the next scheduler,
//    and instruct the next scheduler to take control.
// - Taking control when the previous scheduler relinquishes control. 
//    Activities while taking control include:
//    register self with the BreathPhaseScheduler object,  and activate
//    the mode trigger list.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariant
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/AcScheduler.ccv   25.0.4.0   19 Nov 2013 13:59:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 036   By: rhj    Date: 05-Jan-2009    SCR Number: 6451
//  Project:  840S
//  Description:
//      Added missing net flow backup inspiratory triggers.
// 
//  Revision: 035   By: rhj    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Added net flow backup inspiratory trigger.
//
//  Revision: 034   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 033   By: gdc   Date:  23-Feb-2007    SCR Number: 6307
//  Project:  RESPM
//  Description:
//		Added missing RM requirement numbers.
//
//  Revision: 032   By: gdc   Date:  25-Oct-2006    DR Number: 6293
//  Project:  RESPM
//  Description:
//		Changed to use PEEP recovery breath where applicable to restart
//      breath timers after an RM maneuver.
//
//  Revision: 031   By: gdc   Date:  25-Oct-2006    DR Number: 6296
//  Project:  RESPM
//  Description:
//		Changed to pressure expiratory trigger to cycle out of
// 		vital capacity inspiratory phase.
//
//  Revision: 030   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added scheduling of RM maneuvers.
//
//  Revision: 029   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added pav pause handle during mode transition.
//
//  Revision: 028  By:  syw    Date:  08-Nov-2000    DR Number: 5794
//       Project:  VTPC
//       Description:
//			Added requirement tracings.
//
//  Revision: 027  By: syw     Date:  09-Oct-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//      Added handle for VolumeTargetedManager class.
//
//  Revision: 026  By: healey     Date:  15-Mar-1999    DR Number: 5322, 5367
//  Project:  ATC
//  Description:
//      Added new triggers to class precondition
//
//  Revision: 025  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//      Initial ATC release
//
//  Revision: 024  By:  syw    Date:  17-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (R8027)
//       Description:
//			Call RPeep.setPeepChange if breath is to be Peep Recovery.
//
//  Revision: 023  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling for Inspiratory Pause event and
//             Inspiratoty Pause phase.
//
//  Revision: 022  By:  iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 021  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 020  By:  iv    Date:  01-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 019  By:  iv    Date:  08-Jan-1996    DR Number: DCS 1339
//       Project:  Sigma (R8027)
//       Description:
//             Added a check during EXPIRATORY_PAUSE for HighCircuitPressureInspTrigger.
//
//  Revision: 018  By:  iv    Date:  08-Jan-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 017  By:  iv    Date:  19-Dec-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 016  By:  iv    Date:  03-Dec-1996    DR Number: DCS 1608, 1541
//       Project:  Sigma (R8027)
//       Description:
//             determineBreathPhase(), case = NON_BREATHING : eliminate assertion
//             and add initialization for peep recovery state.
//             Prepare code for inspections.
//
//  Revision: 015  By:  iv    Date:  15-Nov-1996    DR Number: DCS 1559
//       Project:  Sigma (R8027)
//       Description:
//             Replaced safe class assertion with class assertion in constructor.
//
//  Revision: 014  By:  iv    Date:  06-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Deleted class assertion in determineBreathPhase() for NON_BREATHING
//             case.
//
//  Revision: 013  By:  iv    Date:  04-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changes for unit test.
//
//  Revision: 012  By:  sp    Date:  26-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Changed PeepRecoveryTrigger to PeepRecoveryInspTrigger.
//
//  Revision: 011 By:  iv   Date:   07-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//               Added a check for peep recovery trigger in determineBreathPhase(),
//             and disable the trigger on relinquishControl().
//
//  Revision: 010 By:  iv   Date:   13-Aug-1996    DR Number: DCS 1218
//       Project:  Sigma (R8027)
//       Description:
//               Changed the range of SettingId values as checked in a setting change
//             call back assertion.
//
//  Revision: 009 By:  iv   Date:   02-Aug-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Incorporated PEEP recovery after OSC and Powerup.
//
//  Revision: 008 By:  iv   Date:   21-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Added a Boolean isPeepRecovery to newBreathRecord(...) call.
//
//  Revision: 007 By:  iv   Date:   04-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Fixed call to base class takeControl() from takeControl().
//
//  Revision: 006 By:  iv   Date:   07-May-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Changed takeControl(), determineBreathPhase(), and
//             startInspiration_() methods - to allow for a PEEP recovery
//             breath phase to take place on transitions from disconnect,
//             standby, and SVO.
//
//  Revision: 005 By:  iv   Date:   15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated O2_MONITOR_CALIBRATE case in reportEventStatus_().
//
//  Revision: 004 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 003 By:  kam   Date:  06-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated SST_CONFIRMATION case of reportEventStatus_() and
//             added O2_MONITOR_CALIBRATE.  Added call to base class
//             takeControl() from takeControl().
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem
//             and for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "AcScheduler.hh"
#include "TriggersRefs.hh"
#include "SchedulerRefs.hh"
#include "PhaseRefs.hh"
#include "ModeTriggerRefs.hh"
#include "BdDiscreteValues.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes
#include "BreathSet.hh"
#include "BreathTriggerMediator.hh"
#include "UiEvent.hh"
#include "BdSystemStateHandler.hh"
#include "BreathTrigger.hh"
#include "ModeTrigger.hh"
#include "ModeTriggerMediator.hh"
#include "TimerBreathTrigger.hh"
#include "ApneaInterval.hh"
#include "O2Mixture.hh"
#include "Peep.hh"
#include "PendingContextHandle.hh"
#include "PhasedInContextHandle.hh"
#include "PhaseInEvent.hh"
#include "BreathPhase.hh"
#include "BiLevelScheduler.hh"
#include "InspPauseManeuver.hh"
#include "ManeuverRefs.hh"
#include "VolumeTargetedManager.hh"
#include "NifManeuver.hh"
#include "P100Maneuver.hh"
#include "VitalCapacityManeuver.hh"
#include "PressureInspTrigger.hh"
#include "P100Trigger.hh"
#include "P100PausePhase.hh"
#include "PeepRecoveryInspTrigger.hh"
#include "AsapInspTrigger.hh"
#include "VcmPsvPhase.hh"
#include "NifPausePhase.hh"
//#include "Ostream.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AcScheduler()  [Default Contstructor]
//
//@ Interface-Description
//        Constructor.
// $[04119] $[04125] $[04130] $[04141]
// The constructor takes no arguments. It Constructs the class instance with
// the proper id.  The list of valid mode triggers, used by the mode trigger
// mediator is initialized as well. The triggers are ordered based on priority
// - the most urgent is first. The list also guarantees that when a trigger
// fires - the triggers that follow it do not have to be processed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor invokes the base class constructor with the AC id argument.
// It initializes the private data member pModeTriggerList_.
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
// The number of elements on the trigger list is asserted
// to be MAX_NUM_AC_TRIGGERS and MAX_NUM_AC_TRIGGERS to be less than
// MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static const Int32 MAX_NUM_AC_TRIGGERS = 5;
AcScheduler::AcScheduler(void) : BreathPhaseScheduler(SchedulerId::AC)
{
    CALL_TRACE("AcScheduler::AcScheduler(void)");

    // $[TI1]
    Int32 ii = 0;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RDisconnectTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&ROcclusionTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RApneaTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RSettingChangeModeTrigger;
    pModeTriggerList_[ii] = (ModeTrigger*)NULL;

    CLASS_ASSERTION(ii == MAX_NUM_AC_TRIGGERS &&
                        MAX_NUM_AC_TRIGGERS < MAX_MODE_TRIGGERS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AcScheduler()  [Destructor]
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
AcScheduler::~AcScheduler(void)
{
	CALL_TRACE("AcScheduler::~AcScheduler(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathPhase
//
//@ Interface-Description
// This method accepts a breath trigger as an argument. It determines
// which breath phase to start, based on the trigger id, the current phase,
// and the vent settings.
// Once the next breath phase is determined, the following activities take
// place:
// - phase-in new settings for start of inspiration or start of exhalation, 
//   update the applied O2%, the apnea interval and the peep value. 
// - Update the current breath record with new data.
// - Change the active breath trigger list, disabling the triggers on the
//   current list.
// - Instruct the current breath phase to relinquish control and setup
//   the new breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The activities handled by this method are dependent on the current breath
// phase. The current breath phase is retrieved from the BreathPhase object.
// Before any phase starts, the BreathTriggerMediator is used to reset the old
// phase trigger list, and to set the new phase trigger list, the old phase
// relinquishes control, and the new phase registers itself with the
// BreathPhase object. New settings are phased in - when relevant, the applied
// O2 percent is updated if necessary, the apnea interval is updated for start
// of exhalation, and the breath record gets updated.  A switch statement is
// implemented for the various breath phases which are: NON_BREATHING,
// EXHALATION, INSPIRATION, EXPIRATORY_PAUSE, and INSPIRATORY_PAUSE.
// Inspiration begins by calling the method startInspiration_(). Before that
// method is called, the breath type is set (e.g. CONTROL, ASSIST, etc.)
// When current phase is EXHALATION, a pause phase starts if time trigger is
// detected and pause request is pending. If a peep reduction trigger is
// detected then exhalation (with null inspiration flag) is delivered, followed
// by enabling a asap insp. trigger. Otherwise the breath type is determined
// and inspiration is delivered.  When breath type is EXPIRATORY PAUSE, the
// expiratory pause event status is set to either COMPLETE or CANCEL.
// When current phase is INSPIRATION, a pause phase starts if immediate exp
// trigger is detected and pause request is pending. Otherwise exhalation is
// delivered.  When breath type is INSPIRATORY PAUSE, the inspiratory pause
// event status is set to either COMPLETE or CANCEL.
// When current phase is NON_BREATHING, the breathType is set to SPONT to start
// peep recovery inspiration.
//---------------------------------------------------------------------
//@ PreCondition
// For each case in the switch statement, the expected range of valid
// triggers and/or previous schedulers is checked. The valid range for
// phase type is also checked.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
AcScheduler::determineBreathPhase(const BreathTrigger& breathTrigger) 
{

    CALL_TRACE("AcScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)");

    // the type of breath to communicate to the user
    ::BreathType breathType = NON_MEASURED;

    const Trigger::TriggerId triggerId = breathTrigger.getId();

    // Get the scheduler id from the current breath record. Note that the current
    // breath record stores the id of the scheduler that was active at the time the
    // record was created.  
    const SchedulerId::SchedulerIdValue previousSchedulerId =
            (RBreathSet.getCurrentBreathRecord())->getSchedulerId();

    // A pointer to the current breath phase
    BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
    // determine the current phase type (e.g. EXHALATION, INSPIRATION)
    const  BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();

	//cout << "P=" << phase << " T=" << triggerId << endl;

    switch (phase)
    {
        case BreathPhaseType::NON_BREATHING:
		{
            // $[TI1]
            // $[04321] Deliver a peep recovery inspiration:
            BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
            startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
		}
        break;
                        
        case BreathPhaseType::EXHALATION:
		{
            // $[TI2]
			if ( triggerId == Trigger::ARM_MANEUVER )
			{
				if (RNifManeuver.getManeuverState() == PauseTypes::PENDING ||
					RVitalCapacityManeuver.getManeuverState() == PauseTypes::PENDING)
				{
					// $[RM24010] \3\ During the exhalation phase recognize a NIF maneuver as pending (if not already pending), enable the backup pressure trigger but disable pressure and flow triggers.
                    // $[RM12205] When the VC maneuver is armed, only the backup pressure trigger shall be enabled.

					// this disables all triggers including the control breath trigger
					RBreathTriggerMediator.resetTriggerList();
					// retain expiratory triggers
					RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);
					// enable pressure trigger for NIF/VC breath phase -- all others disabled 
					RPressureBackupInspTrigger.enable();
					// disable the timed breaths until after maneuver
					RTimeInspTrigger.disable();
					// -- still in exhalation as required to activate a NIF/VC maneuver
				}
				else
				{
					// no maneuver pending --- the arm maneuver trigger wasn't disarmed properly
					CLASS_ASSERTION_FAILURE();
				}
			}
			else if ( triggerId == Trigger::DISARM_MANEUVER )
			{
                // $[RM12078] A NIF maneuver shall be cancelled...
                // $[RM12085] An armed Vital Capacity maneuver shall be aborted...

				BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
                startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
			}
			else if ( triggerId == Trigger::PRESSURE_BACKUP_INSP &&
					  RVitalCapacityManeuver.getManeuverState() == PauseTypes::PENDING)
			{
                // $[RM12207] VC maneuver shall transition to active at next patient insp backup pressure trigger.
				// patient effort sensed with vital capacity maneuver pending
				RBreathTriggerMediator.resetTriggerList();
				// set up the trigger list for the VC inspiration
				RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::VCM_INSP_LIST);
				// current breath phase is given a chance to phase-out
				pBreathPhase->relinquishControl(breathTrigger);
				// register a Vital Capacity Maneuver PSV phase:
				BreathPhase::SetCurrentBreathPhase(RVcmPsvPhase, breathTrigger);
				// update the breath record
				//(RBreathSet.getCurrentBreathRecord())->startVitalCapacityPause();
				RTimeInspTrigger.disable();
				//Create a new breath record
				RBreathSet.newBreathRecord(SchedulerId::AC, MandTypeValue::PCV_MAND_TYPE, ::SPONT,
										BreathPhaseType::VITAL_CAPACITY_INSP, FALSE);
				// start the vital capacity inspiratory phase   			
				BreathPhase::NewBreath() ;
			}
    		else if ( triggerId == Trigger::PRESSURE_BACKUP_INSP
    				  && RNifManeuver.getManeuverState() == PauseTypes::PENDING )
			{
				// $[RM12207] NIF shall transition to active at the beginning of the next patient inspiratory effort signaled by the backup pressure trigger. 

				// this is a patient triggered breath with NIF pause pending
				// NIF pause is triggered on patient effort detected by backup pressure trigger (PEEP-2.0)

				//start NIF maneuver...

				// Reset currently active triggers 
				RBreathTriggerMediator.resetTriggerList();
				// Change triggers to those active during an active NIF
				RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::NIF_PAUSE_LIST);
				// current breath phase is given a chance to phase-out
				pBreathPhase->relinquishControl(breathTrigger);
				// register a NIF pause phase:
				BreathPhase::SetCurrentBreathPhase(RNifPausePhase, breathTrigger);
				// update the breath record
				(RBreathSet.getCurrentBreathRecord())->startNifPause();
				// start the pause
				BreathPhase::NewBreath() ;
			}
    		else if ( triggerId == Trigger::P100_MANEUVER )
			{
				// $[RM12306] P100 shall transition to active when the P100 inspiratory trigger conditions (see Control Specification) have been met.

				//start P100 maneuver...

				// Reset currently active triggers 
				RBreathTriggerMediator.resetTriggerList();
				// Change triggers to those active during an active P100 (same as NIF)
				RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::NIF_PAUSE_LIST);
				// current breath phase is given a chance to phase-out
				pBreathPhase->relinquishControl(breathTrigger);
				// register a P100 pause phase:
				BreathPhase::SetCurrentBreathPhase(RP100PausePhase, breathTrigger);
				// update the breath record
				(RBreathSet.getCurrentBreathRecord())->startP100Pause();
				// start the pause
				BreathPhase::NewBreath() ;
			}
			else if ( (triggerId == Trigger::TIME_INSP) &&
				 (RExpiratoryPauseEvent.getEventStatus() == EventData::PENDING) )
            {
				//$[04216] $[04217] if time trigger and expiratory pause is requested --
				// start an expiratory pause:
                 // $[TI2.1]
                //start expiratory pause ...
                //$[04221] for the sake of i:e and expiratory time calculation,
                //the breath record considers the expiratory pause phase as
                //part of the expiratory phase, therefore no new phase update
                //for the breath record is necessary.  Reset currently active
                //triggers and setup triggers that are active during pause:
                RBreathTriggerMediator.resetTriggerList();
                RBreathTriggerMediator.changeBreathListName(
                                                BreathTriggerMediator::EXP_PAUSE_LIST);
                // notify the user that expiratory pause is active:
                RExpiratoryPauseEvent.setEventStatus(EventData::ACTIVE);
                // current breath phase is given a chance to phase-out
                pBreathPhase->relinquishControl(breathTrigger);
                // register an expiratory pause phase:
                BreathPhase::SetCurrentBreathPhase((BreathPhase&)RExpiratoryPausePhase,
                                                                        breathTrigger);
                // update the breath record
                (RBreathSet.getCurrentBreathRecord())->startExpiratoryPause();
                // start the pause
				BreathPhase::NewBreath() ;
            }
            else if( (triggerId == Trigger::TIME_INSP) &&
                                        (previousSchedulerId != SchedulerId::AC) )
            { 
                // $[TI2.2]
                //previous scheduler is not AC, therefore the time trigger should have
                // been disabled:
                CLASS_ASSERTION_FAILURE();
            }
            else if(triggerId == Trigger::PEEP_REDUCTION_EXP)
            { 
                // $[TI2.4]
                startExhalation_(breathTrigger, pBreathPhase, TRUE);

                // After the peep reduction (peep low) we should enable the 
                // inspiration trigger to start a inspiration.
                ((BreathTrigger&)RAsapInspTrigger).enable();
				
	        	// On Peep reduction transition mode, the apnea interval should be extended
    		    // if necessary
                RApneaInterval.extendIntervalForPeepReduction();
            }
            else // $[04226] $[03008] $[04010] inspiration is to be delivered
            {
            // $[TI2.3]
				if (RVitalCapacityManeuver.getManeuverState() == PauseTypes::IDLE_PENDING)
				{
					// $[RM12208] The VC maneuver shall allow for a full exhalation effort after the inspiration.
					if ( (triggerId == Trigger::OPERATOR_INSP) )
					{
						// $[RM12233] A manual inspiration request shall be accepted during the expiratory phase...
						// The operator insp trigger fires only when not in restricted phase of exhalation
						RVitalCapacityManeuver.complete();
						breathType = ::CONTROL;
					}
					else if ( (triggerId == Trigger::NET_FLOW_INSP) ||
							  (triggerId == Trigger::NET_FLOW_BACKUP_INSP) ||
							  (triggerId == Trigger::PRESSURE_BACKUP_INSP) ||
							  (triggerId == Trigger::PRESSURE_INSP) ||
							  (triggerId == Trigger::ASAP_INSP) )
					{
                        // $[RM12064] When a VC maneuver terminates, a PEEP Recovery breath shall be delivered, followed by a resumption of normal breath delivery. 
						// The ASAP trigger fires when VCM exhalation times out
						RVitalCapacityManeuver.complete();
						BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
						breathType = ::SPONT;
						// breath timer is restarted during PEEP recovery so no need to restart here
					}
					else
					{
						AUX_CLASS_ASSERTION_FAILURE(triggerId);
					}
				}
                else if(triggerId == Trigger::PEEP_RECOVERY)
                {
                // $[TI2.3.4]
                    breathType = ::SPONT;
                    CLASS_ASSERTION( BreathPhaseScheduler::PeepRecovery_ ==
                                      BreathPhaseScheduler::START );
                }
				else if ( (triggerId == Trigger::OPERATOR_INSP) ||
						  (triggerId == Trigger::PEEP_RECOVERY_MAND_INSP) ||
						  (triggerId == Trigger::ASAP_INSP) ||
						  (triggerId == Trigger::TIME_INSP) )
                {
                // $[TI2.3.1]
                    //This is a VIM breath
                    breathType = ::CONTROL;
                }
				else if ( (triggerId == Trigger::NET_FLOW_INSP) ||
						  (triggerId == Trigger::NET_FLOW_BACKUP_INSP) ||
						  (triggerId == Trigger::PRESSURE_BACKUP_INSP) ||
						  (triggerId == Trigger::PRESSURE_INSP))
				// this is a patient triggered breath -- PIM
				{
					// $[TI2.3.2]
					breathType     = ::ASSIST;
				}
                else
                {
                    // $[TI2.3.3]
                    // invalid trigger
                    AUX_CLASS_ASSERTION_FAILURE(triggerId);
                }
                startInspiration_(breathTrigger, pBreathPhase, breathType);
            }
		}
        break;

        case BreathPhaseType::INSPIRATION:
        {
            // $[TI3]
            //Check the validity of the triggers. Once triggers are checked, settings for
            //new exhalation can be phased in.
            //The trigger should be one of the following legal triggers (note that
            //inspiratory phase can be a spont inspiration during mode transition):
			AUX_CLASS_PRE_CONDITION( (triggerId == Trigger::DELIVERED_FLOW_EXP) || 
									 (triggerId == Trigger::HIGH_PRESS_COMP_EXP) || 
									 (triggerId == Trigger::LUNG_FLOW_EXP) || 
									 (triggerId == Trigger::LUNG_VOLUME_EXP) || 
									 (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) || 
									 (triggerId == Trigger::PRESSURE_EXP) || 
									 (triggerId == Trigger::BACKUP_TIME_EXP) || 
									 (triggerId == Trigger::IMMEDIATE_EXP) || 
									 (triggerId == Trigger::HIGH_VENT_PRESSURE_EXP),
									 triggerId );

			RVolumeTargetedManager.setExhalationTrigger( triggerId, pBreathPhase) ;
			
            //Phase in new settings (for start of exhalation or inspiratory pause)
            PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_EXHALATION);

            // check conditions for inspiratory pause
            if( (triggerId == Trigger::IMMEDIATE_EXP) &&
            		RInspPauseManeuver.isManeuverAllowed() &&
                    (RInspiratoryPauseEvent.getEventStatus() == EventData::PENDING ||
                     RInspPauseManeuver.getManeuverState() == PauseTypes::ACTIVE) )
            {
                 // $[TI3.3]
                //start inspiratory pause when in mandatory breath....
                // $[VC24020]
			    // $[BL04005]
			    // $[BL04006]
			    // $[BL04009] :a transition to inspiratory pause phase
           	    // Reset currently active triggers and setup triggers that are active during
               	// pause:
                RBreathTriggerMediator.resetTriggerList();
   	            RBreathTriggerMediator.changeBreathListName(
                                               BreathTriggerMediator::INSP_PAUSE_LIST);
       	        // Disable the current timer
           	    RTimeInspTrigger.disable();
                
               	// notify the user that inspiratory pause is active:
                RInspiratoryPauseEvent.setEventStatus(EventData::ACTIVE);
   	            // current breath phase is given a chance to phase-out
       	        pBreathPhase->relinquishControl(breathTrigger);
           	    // register an inspiratory pause phase:
               	BreathPhase::SetCurrentBreathPhase((BreathPhase&)RInspiratoryPausePhase,
                                                                        breathTrigger);
                // update the breath record
   	            (RBreathSet.getCurrentBreathRecord())->startInspiratoryPause();
       	        // start the pause
				BreathPhase::NewBreath() ;
            }
            else
            {
                if(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::ACTIVE)
                {
                // $[TI3.1]
                // $[04323]
                    BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::OFF;
                    ((BreathTrigger&)RPeepRecoveryMandInspTrigger).enable();
                } // $[TI3.2]
                
                startExhalation_(breathTrigger, pBreathPhase);
            }

        }
		break;

        case BreathPhaseType::EXPIRATORY_PAUSE: 
        {
            // $[TI4]
            // set the pause event status to notify user of exp pause status:
            EventData::EventStatus eventStatus;
                // $[TI4.4] COMPLETE
                // $[TI4.5] CANCEL
            eventStatus = (triggerId==Trigger::PAUSE_COMPLETE) ?
                                EventData::COMPLETE:EventData::CANCEL;
            RExpiratoryPauseEvent.setEventStatus(eventStatus);

            // $[04226] $[01253] $[03008] $[04010]
            // Its time to start new inspiration:

            //Determine the type of inspiration to be delivered:
            if( (triggerId == Trigger::PAUSE_TIMEOUT) ||
                (triggerId == Trigger::PAUSE_COMPLETE) ||
                (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_INSP) ||
                (triggerId == Trigger::IMMEDIATE_BREATH) ) 
            {
                // $[TI4.1]
                breathType = ::CONTROL;
            }
            else if(triggerId == Trigger::PAUSE_PRESS)
            {
                // $[TI4.2]
			    // $[BL04071] :m terminate active auto expiratory pause
			    // $[04220] :m terminate active manual expiratory pause
                breathType = ::ASSIST;
            }             
            else
            {
                // $[TI4.3]
                //The trigger should have been one of the above triggers
                AUX_CLASS_ASSERTION_FAILURE(triggerId);
            }

            startInspiration_(breathTrigger, pBreathPhase, breathType);
        }
        break;
        
        case BreathPhaseType::INSPIRATORY_PAUSE: 
	    case BreathPhaseType::BILEVEL_PAUSE_PHASE: 
		{
            // $[TI6]
            // set the pause event status to notify user of insp pause status:
            EventData::EventStatus eventStatus;
                // $[TI6.1] COMPLETE
                // $[TI6.2] CANCEL
            eventStatus = (triggerId==Trigger::PAUSE_COMPLETE) ?
                                EventData::COMPLETE:EventData::CANCEL;
            RInspiratoryPauseEvent.setEventStatus(eventStatus);

            //The trigger should have been one of the above triggers
		    // $[BL04078] :m terminate active manual inspiratory pause
		    // $[BL04012] :m terminate active auto inspiratory pause
            AUX_CLASS_ASSERTION( (triggerId == Trigger::PAUSE_TIMEOUT) || 
                             (triggerId == Trigger::PAUSE_COMPLETE) ||
                             (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) || 
                             (triggerId == Trigger::PAUSE_PRESS) ||
                             (triggerId == Trigger::IMMEDIATE_BREATH),
                             triggerId );
            
            // Continue the current timer only when the previous
            // scheduler was AC, this is due to that only in AC mode the
            // TimeInspTrigger is applicable (generate a CONTROL breath).
            if (previousSchedulerId == SchedulerId::AC)
            {
                // $[TI6.3.1]
	            RTimeInspTrigger.continueTimer();
            }   // $[TI6.3.2]

            startExhalation_(breathTrigger, pBreathPhase);
		}
        break;
        

        case BreathPhaseType::PAV_INSPIRATORY_PAUSE:
		{
            // $[TI7]
            AUX_CLASS_ASSERTION( triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP ||
                             	 triggerId == Trigger::IMMEDIATE_BREATH ||
                             	 triggerId == Trigger::PAUSE_COMPLETE,
                             	 triggerId ) ;
            startExhalation_(breathTrigger, pBreathPhase);
		}
        break ;

		case BreathPhaseType::NIF_PAUSE: 
		{
			// $[RM24010] Upon NIF completion, resume normal ventilation
			// $[RM12110] An active NIF maneuver shall be successfully terminated upon detecting a Manual Inspiration request.
			AUX_CLASS_ASSERTION( triggerId == Trigger::PAUSE_COMPLETE ||
								 triggerId == Trigger::OPERATOR_INSP ||
								 triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_INSP ||
								 triggerId == Trigger::IMMEDIATE_BREATH,
								 triggerId );

			// Deliver a PEEP recovery breath that restarts the breath timers
			BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
			startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
		}
		break; 

		case BreathPhaseType::P100_PAUSE: 
		{
			// $[RM12096] When an active P100 maneuver terminates, breath delivery shall return to the previous scheduler.
			// resume control breath delivery
			if ( triggerId == Trigger::P100_COMPLETE )
			{
				// $[RM12096] P100 completes, deliver an ASSIST breath
				// Complete the inspiration phase 
				startInspiration_(breathTrigger, pBreathPhase, ::ASSIST);
			}
			else if ( triggerId == Trigger::OPERATOR_INSP ||
					  triggerId == Trigger::PAUSE_COMPLETE ||
					  triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_INSP ||
					  triggerId == Trigger::IMMEDIATE_BREATH)
			{
                // $[RM12094] A P100 maneuver shall be canceled for a manual insp request
				// restart the breath timers by delivering a control breath
				startInspiration_(breathTrigger, pBreathPhase, ::CONTROL);			
			}
			else
			{
				// Invalid trigger    
				AUX_CLASS_ASSERTION_FAILURE( triggerId );
			}
		}
		break;
        	
    	case BreathPhaseType::VITAL_CAPACITY_INSP: 
    	{
    		//Check the validity of the triggers. 
    		AUX_CLASS_PRE_CONDITION( triggerId == Trigger::PRESSURE_EXP || 
    								 triggerId == Trigger::PAUSE_COMPLETE ||
    								 triggerId == Trigger::HIGH_PRESS_COMP_EXP || 
    								 triggerId == Trigger::LUNG_FLOW_EXP || 
    								 triggerId == Trigger::LUNG_VOLUME_EXP || 
    								 triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP || 
    								 triggerId == Trigger::IMMEDIATE_EXP || 
    								 triggerId == Trigger::HIGH_VENT_PRESSURE_EXP,
    								 triggerId) ;

    		// if this was a "normal" termination of inspiration then the 
    		// maneuver can proceed with data collection otherwise 
    		// cancel the maneuver.

			if (triggerId != Trigger::PRESSURE_EXP)
			{
                // $[RM12062] A Vital Capacity maneuver shall be canceled if...

				// If the VC inspiratory phase was not completed normally then
				// restart the breath timer here, otherwise the breath timer will
				// be restarted after the VitalCapacityManeuver collects data from
				// the exhalation phase and delivers a PEEP recovery breath.
			    // Restart the breath timer by delivering PEEP recovery breath

				BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
                startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
			}
			else
			{
				// $[RM12208] The VC maneuver shall allow for a full exhalation effort after the inspiration.
				startExhalation_(breathTrigger, pBreathPhase);
			}
    	}
    	break;

		default:
		{
            // $[TI5]
            AUX_CLASS_ASSERTION_FAILURE(phase);
		}
        break;
    } //switch                                                        
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
// The method accepts a mode trigger reference as an argument, and has no
// return value.  Before the new mode is instructed to take control, the ac
// time trigger and the peep recovery insp trigger are disabled, the asap
// trigger is enabled (on transition to SIMV, and Apnea modes), and resets all
// mode triggers on its list. Since AC mode may become active after an SVO, a
// NON_BREATHING breath phase is possible. In that case, any breath delivery is
// avoided until the completion of the transitional NON_BREATHING phase that
// guarantees a complete closure of the safety valve. In this case, it is not
// necessary to enable any breath triggers when a transition to a breathing
// mode is detected.  The immediate breath trigger is enabled on transition to
// emergency modes. No breath triggers are enabled on transition to Spont mode.
// When an immediate breath trigger is enabled, the rest of the triggers on the
// active list are disabled to ensure safe transition to the emergency mode.
// The argument passed, provides the method with the information required to
// determine the next scheduler.
//---------------------------------------------------------------------
//@ Implementation-Description
// Individual breath triggers are enabled and disabled by directly accessing
// the trigger instance.  Disabling all triggers on the current list is
// accomplished by using the only one instance of the breath trigger mediator,
// calling the method resetTriggerList().  The mode trigger id passed as an
// argument is checked to determine the requested scheduler which then
// instructed to take control.
//---------------------------------------------------------------------
//@ PreCondition
// The mode setting is checked for valid range of values.
// The trigger id is checked for valid range of mode triggers.
// When the mode trigger is either apnea or mode change, the non breathing
// phase is checked to be SafetyValveClosePhase.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
AcScheduler::relinquishControl(const ModeTrigger& modeTrigger)
{
    CALL_TRACE("AcScheduler::relinquishControl(const ModeTrigger& modeTrigger)");

    const Trigger::TriggerId modeTriggerId = modeTrigger.getId();

    // get the type of the current breath phase:
    const BreathPhaseType::PhaseType phase = BreathRecord::GetPhaseType();

    // disable the inspiratory time trigger, to avoid ac inspiration trigger
    // during the new mode
    RTimeInspTrigger.disable();

    // disable the peep recovery trigger, to avoid its interference with
    // the next scheduler.
    ((BreathTrigger&)RPeepRecoveryInspTrigger).disable();

    // reset all mode triggers for this scheduler
    RModeTriggerMediator.resetTriggerList();
    
    // check what mode to switch to
    const DiscreteValue mode = PendingContextHandle::GetDiscreteValue(SettingId::MODE);

    if( (modeTriggerId == Trigger::SETTING_CHANGE_MODE) ||
        (modeTriggerId == Trigger::APNEA) )
    {
          // $[TI1]
           if (phase != BreathPhaseType::NON_BREATHING)
           {
            // $[TI1.1]
               //$[04263] $[04248] $[04253]
               // set the asap trigger only if the mode is not spont and is not CPAP. 
            if ( (mode != ModeValue::SPONT_MODE_VALUE) && (mode != ModeValue::CPAP_MODE_VALUE) 
			    || (modeTriggerId == Trigger::APNEA) )
               {    // $[TI1.1.1]
                   ((BreathTrigger&)RAsapInspTrigger).enable();
               }      // $[TI1.1.2]
           }
           else // phase == BreathPhaseType::NON_BREATHING
           {
               // $[TI1.2]
            // If the breath phase is a non breathing phase, it is the
               // safety valve closed phase that is in progress, and since a time trigger
               // is already set to terminate that phase, there is no need to set any
               // breath trigger.
            CLASS_PRE_CONDITION(BreathPhase::GetCurrentBreathPhase()
                                                == (BreathPhase*)&RSafetyValveClosePhase);
           }
    }
    else
    {
        // $[TI2]
        //Reset currently active triggers and then enable the only valid breath
        // trigger for that transition.
        RBreathTriggerMediator.resetTriggerList();
        ((BreathTrigger&)RImmediateBreathTrigger).enable();
        // make sure trigger is valid:
        AUX_CLASS_PRE_CONDITION( 
            (modeTriggerId == Trigger::OCCLUSION) ||
            (modeTriggerId == Trigger::DISCONNECT) ||
            (modeTriggerId == Trigger::SVO), modeTriggerId );
    }
    
    if(modeTriggerId == Trigger::SETTING_CHANGE_MODE)
    {
        // $[TI3]
        if(mode == ModeValue::SIMV_MODE_VALUE)
        {
            // $[TI3.1]
            ((BreathPhaseScheduler&)RSimvScheduler).takeControl(*this);
        }
        else if ((mode == ModeValue::SPONT_MODE_VALUE) || (mode == ModeValue::CPAP_MODE_VALUE))
        {    // $[TI3.2]
            ((BreathPhaseScheduler&)RSpontScheduler).takeControl(*this);
        }
        else if(mode == ModeValue::BILEVEL_MODE_VALUE)
        {
        // $[TI3.4]
            // $[04253]
            ((BreathPhaseScheduler&)RBiLevelScheduler).takeControl(*this);
        } 
        else
        {
 			AUX_CLASS_ASSERTION_FAILURE(mode);
        } 
    }
    else if(modeTriggerId == Trigger::APNEA)
    {
        // $[TI4]
        ((BreathPhaseScheduler&)RApneaScheduler).takeControl(*this);
    }
    else if(modeTriggerId == Trigger::OCCLUSION)
    {
        // $[TI5]
        ((BreathPhaseScheduler&)ROscScheduler).takeControl(*this);
    }
    else if(modeTriggerId == Trigger::DISCONNECT)
    {
        // $[TI6]
        ((BreathPhaseScheduler&)RDisconnectScheduler).takeControl(*this);
    }
    else
    {
        // $[TI7]
        CLASS_ASSERTION(modeTriggerId == Trigger::SVO);
        ((BreathPhaseScheduler&)RSvoScheduler).takeControl(*this);
    }
}      

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description
// The method is invoked by the method relinquishControl() from the scheduler
// instance that relinquishes control.  It accepts a breath phase scheduler
// reference as an argument and has no return value.  The base class method for
// takeControl() is invoked to handle activities common to all schedulers.  The
// method is responsible for extending the apnea interval when a transition to
// a breathing mode is in progress. Peep recovery flag is set on transition
// from a non breathing mode.  The NOV RAM instance rBdSystemState is updated
// with the new scheduler to enable BD system to properly recover from power
// interruption.  The scheduler registers with the BreathPhaseScheduler as the
// current scheduler, and the mode triggers relevant to that mode are set to be
// enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler reference passed as an argument is used to get the id of the
// current scheduler. The apnea interval is extended only if the id is SPONT,
// SIMV or APNEA. The apnea extension is accomplised by invoking the method
// extendIntervalForModeChange() in the apnea interval instance. The static
// method of BreathPhaseScheduler is used to register the new scheduler. The
// BdSystemStateHandler is used to update the NOV RAM object for BdSystemState
// with the new scheduler id, passing in a self reference.  The private data
// member pModeTriggerList_ is passed as an argument to the method in the mode
// trigger mediator object that is responsible for changing the current mode
// trigger list.
//---------------------------------------------------------------------
//@ PreCondition
// The CLASS_PRE_CONDITION method is used to validate the IDs of the
// valid breath phase schedulers.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
AcScheduler::takeControl(const BreathPhaseScheduler& scheduler) 
{
    CALL_TRACE("AcScheduler::takeControl(const BreathPhaseScheduler& scheduler)");

    // Invoke the base class takeControl method
    BreathPhaseScheduler::takeControl(scheduler);
    
    const SchedulerId::SchedulerIdValue schedulerId = scheduler.getId();
    
    if( (schedulerId == SchedulerId::SIMV) ||
        (schedulerId == SchedulerId::SPONT) ||
        (schedulerId == SchedulerId::BILEVEL) ||
        (schedulerId == SchedulerId::APNEA) )
    {
    // $[TI1]
        // $[04120] On transition to AC mode, the apnea interval should be extended
        // if necessary
        RApneaInterval.extendIntervalForModeChange();

        if (schedulerId == SchedulerId::APNEA)
        {
		    // $[TI4]
			RVolumeTargetedManager.reset() ;
		}
	    // $[TI5]
    }
    else if ((schedulerId == SchedulerId::OCCLUSION) || 
             (schedulerId == SchedulerId::DISCONNECT) || 
             (schedulerId == SchedulerId::STANDBY) || 
             (schedulerId == SchedulerId::POWER_UP) || 
             (schedulerId == SchedulerId::SVO) )
    {
    // $[TI2]
        BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;

        if (schedulerId == SchedulerId::DISCONNECT)
        {
		    // $[TI6]
			// $[VC24013a]
			if (PhasedInContextHandle::GetBoundedValue(SettingId::IBW).value <= 7.0)
			{
			    // $[TI9]
				RVolumeTargetedManager.reset() ;
			}
		    // $[TI7]
        }
        else
        {
		    // $[TI8]
			// $[VC24013b]
			RVolumeTargetedManager.reset() ;
        }
    }    
    else
    {
    // $[TI3]
        CLASS_ASSERTION( schedulerId == SchedulerId::SAFETY_PCV); 
		RVolumeTargetedManager.reset() ;
    }

    // update the reference to the newly active breath scheduler 
    BreathPhaseScheduler::SetCurrentScheduler_((BreathPhaseScheduler&)(*this));

    //The BreathRecord is not being updated with the new scheduler id, so that
    // the identity of the scheduler that originated this breath is maintained. 
    // until a new inspiration starts.

    // Initialize the cycle time to an arbitrary value. This value does not effect
    // ventilation.
    breathCycleTime_ = 10000;    

    // update the BD state, in case of power interruption
    BdSystemStateHandler::UpdateSchedulerId(getId()); 

    // Set the mode triggers for that scheduler.
    enableTriggers_();    
    RModeTriggerMediator.changeModeTriggerList(pModeTriggerList_);
} 



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
AcScheduler::SoftFault(const SoftFaultID  softFaultID,
		       const Uint32       lineNumber,
		       const char*        pFileName,
		       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, ACSCHEDULER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: settingChangeHappened_
//
//@ Interface-Description
// The method takes a SettingId as an argument and has no return value.
// The method is called when a setting change is recognized, and before it
// is phased-in. Two setting change events are currently considered:
// rate change, and mode change. When a mode change is recognized, the
// setting change mode trigger is enabled. When a rate change is recongnized,
// a new target for the breath cycle is calculated and is used to update
// the inspiratory time triggger. Apnea rate change is ignored here.
//---------------------------------------------------------------------
//@ Implementation-Description
// The static variables BreathPhaseScheduler::ModeChange_, and
// BreathPhaseScheduler::RateChange_ indicate which setting change is in
// progress. The SettingId argument indicates which setting has changed.
// When a mode change occurs, the instance rSettingChangeModeTrigger is
// enabled. When a rate change happens and no mode change is in progress,
// a new breath cycle, based on the pending rate is calculated. If the
// new pending breath cycle is shorter than the current breath cycle,
// the instance of the inspiratory time trigger -- rTimeInspTrigger --
// is updated, else the set breath period is restored.
//---------------------------------------------------------------------
//@ PreCondition
// The setting id is within the folowing range:
//   id >= SettingId::LOW_BATCH_BD_ID_VALUE && id <= SettingId::HIGH_BATCH_BD_ID_VALUE
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
AcScheduler::settingChangeHappened_(const SettingId::SettingIdType id) 
{
    CALL_TRACE("AcScheduler::settingChangeHappened_(const SettingId::SettingIdType id)");

    if(id == SettingId::MODE)
    {
    // $[TI1]
        // check the mode value.
        const DiscreteValue mode = PendingContextHandle::GetDiscreteValue(SettingId::MODE);
        if(mode != ModeValue::AC_MODE_VALUE)
        {
        // $[TI1.1]
            BreathPhaseScheduler::ModeChange_ = TRUE;    
            BreathPhaseScheduler::RateChange_ = FALSE;
            ((Trigger&)RSettingChangeModeTrigger).enable();
        }
        // $[TI1.2]
    }
    else if(id == SettingId::RESP_RATE)
    {
        // $[TI2]
        // $[04229]
        if(!BreathPhaseScheduler::ModeChange_)
        {    // $[TI2.1]
            // pending respiratory rate setting
            const Real32 rate =
                PendingContextHandle::GetBoundedValue(SettingId::RESP_RATE).value;
            // pending breath period:
            Int32 pendingBreathPeriod = (Int32)(60.0F * 1000.0F / rate);
    
            // set inspiratory time:
            Real32 inspTime =
                PhasedInContextHandle::GetBoundedValue(SettingId::INSP_TIME).value;
    
            // the minimum breath cycle time during rate transition:
            Int32 minCycleTime = (Int32)(3.5F * inspTime);
            
            Int32 newBreathCycle = MAX_VALUE(minCycleTime, pendingBreathPeriod);

            // set the target time for the insp trigger to the
            // new pending breath cycle - this is done unconditionally to overwrite
            // previous rate change effects. 
            newBreathCycle = MIN_VALUE(newBreathCycle, breathCycleTime_);
            RTimeInspTrigger.setTargetTime(newBreathCycle);
        }    // $[TI2.2]
    }    
    else
    {    // $[TI3]
        CLASS_ASSERTION( id >= SettingId::LOW_BATCH_BD_ID_VALUE &&
                     id <= SettingId::HIGH_BATCH_BD_ID_VALUE );
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts a EventId and a Boolean for the event status as
// arguments.  It returns a EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247]
// The argument of type EventId specifies what is the active event.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is accepted by
// invoking a call to the static method:
// BreathPhaseScheduler::AcceptManualInspiration_(eventStatus).
// The alarm reset event is ignored, while the eventStatus argument value is checked for
// TRUE.
// The expiratory and inspiratory pause events are accepted by invoking a call to the
// static methods:
// BreathPhaseScheduler::AcceptExpiratoryPause_(eventStatus).
// BreathPhaseScheduler::AcceptInspiratoryPause_(eventStatus) - respectively.
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

EventData::EventStatus
AcScheduler::reportEventStatus_(const EventData::EventId id, 
                                        const Boolean eventStatus)
{
    CALL_TRACE("AcScheduler::reportEventStatus_(const EventData::EventId id, \
                                        const Boolean eventStatus)");

    EventData::EventStatus rtnStatus = EventData::IDLE;

    switch (id)
    {
        case EventData::MANUAL_INSPIRATION:
        // $[TI1]
            rtnStatus =
                    BreathPhaseScheduler::AcceptManualInspiration_(eventStatus);
            break;

        case EventData::ALARM_RESET:
        // $[TI2]
            // ignore alarm reset
            CLASS_PRE_CONDITION(eventStatus);
            break;

        case EventData::EXPIRATORY_PAUSE:
        // $[TI3]
            rtnStatus =
                    BreathPhaseScheduler::AcceptExpiratoryPause_(eventStatus);
            break;

        case EventData::INSPIRATORY_PAUSE:
        // $[TI4]
            rtnStatus =
                    BreathPhaseScheduler::AcceptInspiratoryPause_(eventStatus);
            break;

    	case EventData::NIF_MANEUVER:
            rtnStatus = BreathPhaseScheduler::AcceptNifManeuver_(eventStatus);
            break;

		case EventData::P100_MANEUVER:
            rtnStatus = BreathPhaseScheduler::AcceptP100Maneuver_(eventStatus);
            break;

		case EventData::VITAL_CAPACITY_MANEUVER:
            rtnStatus = BreathPhaseScheduler::AcceptVitalCapacityManeuver_(eventStatus);
            break;

        default:
        // $[TI6]
            AUX_CLASS_ASSERTION_FAILURE(id);
    }    

    return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableTriggers_
//
//@ Interface-Description
// $[04119] $[04125] $[04130] $[04141]
// The method takes no arguments and returns no value.  A request to enable AC
// scheduler valid mode triggers is issued when the AC scheduler is taking
// control. Immediate triggers are not set here since by design they are only
// set once the trigger they instanciate becomes active.
//---------------------------------------------------------------------
//@ Implementation-Description
// The mode trigger instances are accessed directly with a call to their
// setIsEnableRequested() method.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
AcScheduler::enableTriggers_(void) 
{
// $[TI1]
    CALL_TRACE("AcScheduler::enableTriggers_(void)");
    ((ModeTrigger&)RApneaTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)ROcclusionTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RDisconnectTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RSvoTrigger).setIsEnableRequested(TRUE);

    // Note that the immediate, or event driven triggers are not
    // enabled here: RSettingChangeModeTrigger
}

//=====================================================================
//
// private methods
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startInspiration_ 
//
//@ Interface-Description
// The method takes a breath trigger reference, a breath phase pointer, and a
// breath type as arguments. It does not return any value.  When the next phase
// is determined to be inspiration, this method is called to perform all the
// standard procedures required to start inspiration: Any pending mode or rate
// change status is cleared, new settings are phased in, the scheduler is
// checked to be in sync with the mode setting. a new breath record is created,
// the oxygen mix, the apnea interval, and peep get updated, the breath trigger
// list is changed for inspiration list, and the previous breath relinquishes
// control, For control or assist breath type, the time insp trigger is set and
// enabled to determine the time of the next inspiration, and the breath phase
// is set to reference the mandatory breath phase. For spont breath type, the
// peep recovery state is set to active and the breath phase is set to
// reference the PeepRecovery breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The method collaborates with the handle PhasedInContextHandle to phase in new
// settings. A check that the scheduler matches the mode setting is done by
// calling the method VerifyModeSynchronization_().
// The method creates a new breath record using the object rBreathSet.  The
// actual oxygen mix, apnea interval, and peep are updated using the objects
// rO2Mixture, rApneaInterval and rPeep.  The breath trigger mediator instance
// rBreathTriggerMediator is used to set the inspiratory breath trigger list.
// The breath trigger instance rTimeInspTriger is restarted with the new cycle
// time. The breath phase instance pointed to by argument pBreathPhase is
// instructed to phase out.  The static method BreathPhase::SetCurrentBreathPhase
// is used to set up the new mandatory breath type.
//---------------------------------------------------------------------
//@ PreCondition
// Check ranges for mandatory type and breath type. Check peep recovery flag for
// true in case of a spont breath.
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
AcScheduler::startInspiration_(const BreathTrigger& breathTrigger, BreathPhase* pBreathPhase,
 BreathType breathType)
{

    CALL_TRACE("AcScheduler::startInspiration_(const BreathTrigger& breathTrigger,\
                    BreathPhase* pBreathPhase, BreathType breathType)");

    // pointer to a breath phase
    BreathPhase* pPhase = NULL;;
    // variable to store setting values:
    DiscreteValue mandatoryType;
    
    BreathPhaseScheduler::ModeChange_ = FALSE;    
    BreathPhaseScheduler::RateChange_ = FALSE;

    //Phase in new settings (for start of inspiration)
    PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

    // verify BD is in synch with GUI:
    BreathPhaseScheduler::VerifyModeSynchronization_(getId());

    //Reset currently active triggers and setup triggers that are active during inspiration:
    RBreathTriggerMediator.resetTriggerList();
    RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::INSP_LIST);

    mandatoryType = PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);

    // support type forced to OFF_SUPPORT_TYPE since not relevent in AC mode
	RVolumeTargetedManager.updateData( mandatoryType, SupportTypeValue::OFF_SUPPORT_TYPE) ;
	
    if( breathType == ::CONTROL || breathType == ::ASSIST)
    {
    // $[TI1]
        isPeepRecovery_ = FALSE;

		switch (mandatoryType)
		{
			case MandTypeValue::PCV_MAND_TYPE :
				// $[TI6]
				pPhase = (BreathPhase*)&RPcvPhase;
			    break;
			    
			case MandTypeValue::VCV_MAND_TYPE :
				// $[TI7]
	    		pPhase = (BreathPhase*)&RVcvPhase;
			    break;
			    
			case MandTypeValue::VCP_MAND_TYPE :
				// $[TI8]
				RVolumeTargetedManager.determineBreathType( pPhase, mandatoryType) ;
			    break;
			    
			default :
	    		// unexpected mand type...
		    	AUX_CLASS_ASSERTION_FAILURE(mandatoryType);
			    break;
		}

        // $[04009]
        //Set the time trigger for next inspiration, start its timer and enable the
        //trigger. The trigger is actually re-enabled here after being disabled
        //previously by BreathTriggerMediator method resetTriggerList().

        // respiratory rate setting.
        const Real32 rate =
            PhasedInContextHandle::GetBoundedValue(SettingId::RESP_RATE).value;

        // Breath period calculation. The next inspiration is determined by the
        // TimeInspTrigger. $[04169] $[04190] $[04224] $[04225]
        breathCycleTime_ = (Int32)(60.0F * 1000.0F / rate);
        RTimeInspTrigger.restartTimer(breathCycleTime_);
        RTimeInspTrigger.enable();
    }
    else if (breathType == ::SPONT)
    {
    // $[TI2]
        isPeepRecovery_ = TRUE;
        // deliver a peep recovery phase
        CLASS_ASSERTION(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::START);
        // Set the PeepRecovery_ variable to ACTIVE to flag the activation of the ASAP trigger
        // on the start of the next exhalation. 
        BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::ACTIVE;
        mandatoryType = ::NULL_MANDATORY_TYPE;
        pPhase = (BreathPhase*)&RPeepRecoveryPhase;
    }
    else
    {
    // $[TI3]
        AUX_CLASS_PRE_CONDITION( (breathType == ::CONTROL) ||
                               (breathType == ::ASSIST) ||
                               (breathType == ::SPONT), breathType );
    }

    // phase-out current breath phase
    pBreathPhase->relinquishControl(breathTrigger);

    // $[04118] $[04122]
    // Once settings are phased in, determine the O2 mix, the
    // apnea interval, and the peep for this phase:
    RO2Mixture.determineO2Mix(*this, (BreathPhase&)*pPhase);
    RApneaInterval.newPhase(BreathPhaseType::INSPIRATION);
    RPeep.updatePeep(BreathPhaseType::INSPIRATION);

	if (pPhase == (BreathPhase*)&RPeepRecoveryPhase)
	{	// $[TI4]
	    RPeep.setPeepChange( TRUE) ;
	}	// $[TI5]

    // $[04227] register new phase:
    BreathPhase::SetCurrentBreathPhase(*pPhase, breathTrigger);

    //Update the breath record
    RBreathSet.newBreathRecord(SchedulerId::AC, mandatoryType, breathType,
                            BreathPhaseType::INSPIRATION, isPeepRecovery_);
    // set the breath record with the breath cycle time:
    BreathRecord::SetBreathPeriod((Real32) breathCycleTime_); 
    // start inspiration
	BreathPhase::NewBreath() ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startExhalation_ 
//
//@ Interface-Description
// The method takes a breath trigger reference, a breath phase pointer, a
// breath type as arguments, and a null inspiration boolean flag. It
// does not return any value.  
//---------------------------------------------------------------------
//@ Implementation-Description
// Reset currently active triggers and setup triggers that are active during
// expiration.  Turn peep recovery off for peep recovery inspiration.
// The applied O2 percent is updated if necessary, the apnea interval is
// updated for start of exhalation, and the breath phase gets updated.
// Creat a new breath record with NULL_INSPIRATION phase type when peep
// reduction occures due to transition from high peep bilevel state to
// this scheduler.  Otherwise, update the breath record.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
AcScheduler::startExhalation_(const BreathTrigger& breathTrigger,
                              BreathPhase* pBreathPhase,
                              const Boolean noInspiration)
{
	CALL_TRACE("AcScheduler::startExhalation_(const BreathTrigger& breathTrigger,\
                              BreathPhase* pBreathPhase,\
                              const Boolean noInspiration)");

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_EXH);
#endif // INTEGRATION_TEST_ENABLE

    //Reset currently active triggers and setup triggers that are active during
    //exhalation:
    RBreathTriggerMediator.resetTriggerList();
    RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);
    // phase-out current breath phase
    pBreathPhase->relinquishControl(breathTrigger);
     // determine the O2 mix, the apnea interval, and peep:
    RO2Mixture.determineO2Mix(*this, (BreathPhase&)RExhalationPhase);
    RApneaInterval.newPhase(BreathPhaseType::EXHALATION);
    RPeep.updatePeep(BreathPhaseType::EXHALATION);
     // register new phase:
    BreathPhase::SetCurrentBreathPhase((BreathPhase&)RExhalationPhase, breathTrigger);
    if(noInspiration)
    {
    // $[TI2]
        //Update the breath record
        RBreathSet.newBreathRecord(SchedulerId::AC,
               ::NULL_MANDATORY_TYPE,
               RBiLevelScheduler.getFirstBreathType(),
               BreathPhaseType::NULL_INSPIRATION,
               FALSE);
    }
    else
    {
    // $[TI1]
        //Update the breath record
        (RBreathSet.getCurrentBreathRecord())->newExhalation();
    }
    // start exhalation
	BreathPhase::NewBreath() ;
}

