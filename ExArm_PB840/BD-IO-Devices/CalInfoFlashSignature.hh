#ifndef CalInfoFlashSignature_HH
#define CalInfoFlashSignature_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CalInfoFlashSignature - interface to flash memory
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/CalInfoFlashSignature.hhv   25.0.4.0   19 Nov 2013 13:54:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//		Project:  Sigma (R8027)
//		Description:
//			Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
//@ End-Usage

class CalInfoFlashSignature {
  public:
  	enum Signature
  	{
      EXH_VALVE_SIGNATURE = 0x4D41442A
    };
} ;


#endif // CalInfoFlashSignature_HH
