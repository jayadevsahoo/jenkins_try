#ifndef TaskTable_HH
#define TaskTable_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================
 
// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: TaskTable - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskTable.hhv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  gdc      Date:  08-JUN-95    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//=====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
#include "Task.hh"
//@ End-Usage
 
#include "TaskFlags.hh"
#include "IpcIds.hh"

//  struct containing task information in the TaskTable
struct TaskTableEntry
{
     char*              taskName;
     EntryPtr           entryPoint;
     Int32              priority;
     Int32              flags;
     IpcId              taskIpq;
     unsigned char *    pStack;
     size_t             stackSize;
     Uint32             monitorPeriod;
     IpcId              monitorQueue;
};
     
 
class TaskTable
{
  public:

    static void                  Initialize(void);
    static Int32                 GetNumEntries(void);
    static void                  Print(void);
 
    static const TaskTableEntry& GetTaskTableEntry(const Int32 taskId);
    static Boolean               IsInitialized(void);

    static void     SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL,
			      const char*       pPredicate = NULL);
 
  private:
    TaskTable();      // declared only
    ~TaskTable();     // declared only

    TaskTable(const TaskTable&);       // declared only
    void operator=(const TaskTable&);  // declared only
 
    //@ Data-Member:    NumEntries_
    //  The number of TaskTableEntry's in the Table_
    static const Uint            NumEntries_;

    //@ Data-Member:    Table_
    //  Array of TaskTableEntry's.
    static const TaskTableEntry  Table_[];

    //@ Data-Member:    IsInitialized_
    //  Is the TaskTable class initialized.
    static Boolean               IsInitialized_;
 
};
 

#endif   // TaskTable_HH
