#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N ================
//@ Class:  Subject - Subject Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is to be a base class for all classes that can be monitored
//  by 'SettingObserver' or 'ContextObserver' instances.  This class defines
//  a public, static method that is used by GUI-Apps, to get around their
//  lack of cleanup when transitioning to and from SST.  This problem arises
//  due to the fact that some GUI-Apps classes are attached to some of these
//  subject instances during this transition, and, since GUI-Apps doesn't
//  run any destructors whereby the attachments can be cleared, subsequent
//  'notifyAllObserver()' calls could result in use of dangling pointers.
//  To eliminate this problem the static 'DetachFromAllObservers()' method
//  is provided for GUI-Apps to call during these transitions.  This static
//  method forwards the request on to each subject instance's
//  'detachFromAllObservers_()' virtual method to perform the detachment.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has an internal static array which keeps a pointer to all
//  currently-constructed subjects.  This array of pointers is used by
//  the static 'DetachFromAllObservers()' method to notify each subject.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Subject.ccv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//===================================================================

#include "Subject.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

Subject*  Subject::ArrInstances_[MAX_INSTANCES_];


//====================================================================
//
//  Public Methods...
//
//====================================================================

//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method: ~Subject()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Subject::~Subject(void)
{
  CALL_TRACE("~Subject()");

#if !defined(SIGMA_UNIT_TEST)
  // find this instance's pointer and remove it...
  for (Uint idx = 0u; idx < Subject::MAX_INSTANCES_; idx++)
  {
    if (ArrInstances_[idx] == this)
    {
      ArrInstances_[idx] = NULL;
      break;
    }
  }
#endif // !defined(SIGMA_UNIT_TEST)
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  DetachFromAllObservers()  [static]
//
//@ Interface-Description
//  Detach (unregister) all observers from all subject instances.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Subject::DetachFromAllObservers(void)
{
  CALL_TRACE("DetachFromAllObservers()");

  Uint idx;

  for (idx = 0u; idx < Subject::MAX_INSTANCES_; idx++)
  {  // $[TI1] -- this path is ALWAYS taken...
    Subject*  pSubject = ArrInstances_[idx];

    if (pSubject != NULL)
    {  // $[TI1.1] -- found a subject instance...
      // detach ALL observers from this subject...
      pSubject->detachFromAllObservers_();
    }  // $[TI1.2]
  }
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Subject::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, SUBJECT,
                          lineNumber, pFileName, pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  Subject()  [Constructor]
//
//@ Interface-Description
//  Create a default setting subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Subject::Subject(void)
{
  CALL_TRACE("Subject()");

#if !defined(SIGMA_UNIT_TEST)
  // find an empty slot to store this instance's pointer...
  Uint idx;
  for (idx = 0u; idx < Subject::MAX_INSTANCES_; idx++)
  {  // $[TI1] -- this path is ALWAYS taken...
    if (ArrInstances_[idx] == NULL)
    {  // $[TI1.1] -- found an unused slot...
      ArrInstances_[idx] = this;
      break;
    }  // $[TI1.2] -- keep looking...
  }

  AUX_CLASS_ASSERTION((idx < Subject::MAX_INSTANCES_), idx);
#endif // !defined(SIGMA_UNIT_TEST)
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  detachFromAllObservers_()  [pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived
//  classes to detach all of this subject's observers.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented
