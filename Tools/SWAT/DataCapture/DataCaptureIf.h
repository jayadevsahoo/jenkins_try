//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file DataCaptureIf.h
//----------------------------------------------------------------------------

#ifndef DATACAPTUREIF_H
#define DATACAPTUREIF_H 

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include <string>
#include "ILogger.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------
///Interface to data capture
class DataCaptureIf
{
    public:
    virtual bool start()                       = 0;
    virtual bool stop()                        = 0;
    virtual bool enable_logging()              = 0;
    virtual bool disable_logging()             = 0;
    virtual bool clean_database(unsigned int iTimeMin)                                 = 0;
    virtual bool register_callback(bool (*function)(std::string pdata, void *pUser), void *pUser)   = 0;

    virtual bool register_logger(ILogger *pILogger) = 0;


    virtual bool get(char *pName, void *pData, unsigned int *pDataLength)                           = 0;
    virtual bool set(char *pName, void *pData, unsigned int *pDataLength)                           = 0;
    virtual bool command(char *command)                                                             = 0;
    virtual bool updateClientInfo(char *ip, char *port)                                             = 0;
  //  virtual bool setLogger(bool (*logger_function)(std::string pdata, void *pUser))
};

#endif
