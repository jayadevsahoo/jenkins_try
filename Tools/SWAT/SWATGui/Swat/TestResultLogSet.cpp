// TestResultLogSet.h : Implementation of the CTestResultLogSet class



// CTestResultLogSet implementation

// code generated on Wednesday, October 13, 2010, 4:11 PM

#include "stdafx.h"
#include "TestResultLogSet.h"
IMPLEMENT_DYNAMIC(CTestResultLogSet, CRecordset)

CTestResultLogSet::CTestResultLogSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_test_session_id = "";
	m_test_script_name = "";
	m_test_start_time = "";
//	m_test_report = 0;
//	m_test_status_log = 0;
	m_test_user_name = "";
	m_nFields = 9;
	m_nDefaultType = dynaset;

	m_test_report_name = "";
	m_test_status_log_name = "";


}

// The connection string below may contain plain text passwords and/or
// other sensitive information. Please remove the #error after reviewing
// the connection string for any security related issues. You may want to
// store the password in some other form or use a different user authentication.
CString CTestResultLogSet::GetDefaultConnect()
{
	return _T("DSN=PostgreSQL30;DATABASE=viking_integration_test;SERVER=127.0.0.1;PORT=5432;UID=postgres;PWD=its4me@1234;SSLmode=disable;ReadOnly=0;Protocol=7.4;FakeOidIndex=0;ShowOidColumn=0;RowVersioning=0;ShowSystemTables=0;ConnSettings=;Fetch=100;Socket=4096;UnknownSizes=0;MaxVarcharSize=255;MaxLongVarcharSize=8190;Debug=0;CommLog=0;Optimizer=0;Ksqo=1;UseDeclareFetch=0;TextAsLongVarchar=1;UnknownsAsLongVarchar=0;BoolsAsChar=1;Parse=0;CancelAsFreeStmt=0;ExtraSysTablePrefixes=dd_;;LFConversion=1;UpdatableCursors=1;DisallowPremature=0;TrueIsMinus1=0;BI=0;ByteaAsLongVarBinary=0;UseServerSidePrepare=0;LowerCaseIdentifier=0;XaOpt=1");
}

CString CTestResultLogSet::GetDefaultSQL()
{
	return _T("[TestResults].[test_log]");
}

void CTestResultLogSet::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// Macros such as RFX_Text() and RFX_Int() are dependent on the
// type of the member variable, not the type of the field in the database.
// ODBC will try to automatically convert the column value to the requested type
	RFX_Text(pFX, _T("[test_session_id]"), m_test_session_id);
	RFX_Text(pFX, _T("[test_script_name]"), m_test_script_name);
	RFX_Text(pFX, _T("[test_start_time]"), m_test_start_time);
	RFX_LongBinary(pFX, _T("[test_report]"), m_test_report);
	RFX_LongBinary(pFX, _T("[test_status_log]"), m_test_status_log);
	RFX_Text(pFX, _T("[test_user_name]"), m_test_user_name);

	RFX_Text(pFX, _T("[test_report_name]"), m_test_report_name);
	RFX_Text(pFX, _T("[test_status_log_name]"), m_test_status_log_name);
	RFX_LongBinary(pFX, _T("[test_script_file]"), m_test_script_file);

}
/////////////////////////////////////////////////////////////////////////////
// CTestResultLogSet diagnostics

#ifdef _DEBUG
void CTestResultLogSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CTestResultLogSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


