#ifndef exceptions_HH
#define exceptions_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 
//====================================================================
// Filename:  exceptions - functions defined in exceptions.s
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/exceptions.hhv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//====================================================================
 
//@ Usage-Classes
#include "Sigma.hh"
//@ End-Usage

//@ Begin-Free-Function

extern "C"
{

//  exception vector typedef
typedef void (*EntryPtr)(void);

//  virtual exception vector table defined in exceptions.s
EntryPtr SigmaVirtualEvt[256];
 
//  assembly functions defined in exceptions.s
void SigmaLevel7Catch();
void SigmaFlineCatch();
void SigmaExceptionCatch();
void SigmaTrapHandler();

}  // extern "C"

//@ End-Free-Function


#endif // exceptions_HH
