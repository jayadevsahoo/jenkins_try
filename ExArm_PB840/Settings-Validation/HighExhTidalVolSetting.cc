#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  HighExhTidalVolSetting - High Exhaled Tidal Volume Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the upper limit of
//  exhaled tidal volume (in milliters), above which the "high exhaled tidal
//  volume alarm" shall be activated.  This class inherits from
//  'BoundedSetting' and provides the specific information needed for
//  representation of high exhaled tidal volume.  This information includes
//  the interval and range of this setting's values, and this batch setting's
//  new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, but 'updateConstraints_()' is
//  overridden to update this setting's dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HighExhTidalVolSetting.ccv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014  By: mnr    Date: 21-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Unnecessary comment removed. 
//
//  Revision: 013  By: mnr    Date: 20-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Unused includes removed. 
//
//  Revision: 012  By: mnr    Date: 20-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Unused code removed. 
//
//  Revision: 011   By: mnr    Date: 10-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Using new #define for lower tidal vol bound. 
//
//  Revision: 010   By: gdc    Date: 29-Apr-2009    SCR Number: 6478
//  Project:  840S
//  Description:
//      Corrected getNewPatientValue to return unclipped value.
//
//  Revision: 009   By: gdc    Date: 26-Jan-2009    SCR Number: 6461
//  Project:  840S
//  Description:
//      Implemented #define workaround for compiler bug.
//
//  Revision: 008   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 007   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  eliminated 'get{Max,Min}Limit()' method, now defined by base class
//      *  incorporated circuit-specific new-patient values and ranges
//
//  Revision: 006   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: sah    Date:  08-Jul-1998    DR Number: 5118
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.18.1.0) into Rev "BiLevel" (1.18.2.0)
//	Fixed incorrect bias towards "SPONT" bound.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.18.1.0) into Rev "BiLevel" (1.18.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "HighExhTidalVolSetting.hh"
#include "SettingConstants.hh"
#include "PatientCctTypeValue.hh"
#include "ModeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"

//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

#define DEFAULT_MAX_VALUE  Real32(3000.0f)

//  $[02142] -- The setting's range ...
//  $[02145] -- The setting's resolution ...

// use #define values in static const struct to workaround compiler bug
static const BoundedInterval  LAST_NODE_ =
{
	DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC,
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static const BoundedInterval  NODE3_ =
{
	100.0f,		// change-point...
	1.0f,		// resolution...
	ONES,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE2_ =
{
	400.0f,		// change-point...
	5.0f,		// resolution...
	ONES,		// precision...
	&::NODE3_
};
static BoundedInterval  Node1_ =
{
	DEFAULT_MAX_VALUE,	// change-point...
	10.0f,		// resolution...
	TENS,		// precision...
	&::NODE2_
};
static BoundedInterval  IntervalList_ =
{
	DEFINED_UPPER_ALARM_LIMIT_OFF,// absolute maximum...
	(DEFINED_UPPER_ALARM_LIMIT_OFF - DEFAULT_MAX_VALUE),// resolution...
	TENS,		// precision...
	&::Node1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: HighExhTidalVolSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, the method initializes
//  the value interval.
//
//  The resolution for the "max" range below, is calculated by taking
//  the difference between the absolute maximum value and the next lower
//  change-point.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighExhTidalVolSetting::HighExhTidalVolSetting(void)
: BatchBoundedSetting(SettingId::HIGH_EXH_TIDAL_VOL,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::IntervalList_,
					  HIGH_EXH_TIDAL_VOL_MAX_ID,   // maxConstraintId...
					  HIGH_EXH_TIDAL_VOL_MIN_ID)   // minConstraintId...
{
	CALL_TRACE("HighExhTidalVolSetting()");
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~HighExhTidalVolSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighExhTidalVolSetting::~HighExhTidalVolSetting(void)
{
	CALL_TRACE("~HighExhTidalVolSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is normally changeable except in CPAP.
//  $[LC02003]\b\ in CPAP, high Vte,spont to shall not be viewable
//  $[LC02001]\b\ transition from CPAP, high Vte,spont shall be viewable
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	HighExhTidalVolSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");
	Applicability::Id applicabilityId;

	const Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const DiscreteValue MODE = (qualifierId == Notification::ADJUSTED) ? 
		pSetting->getAdjustedValue() : pSetting->getAcceptedValue();

	if ( ModeValue::CPAP_MODE_VALUE == MODE )
	{
		applicabilityId = Applicability::INAPPLICABLE;
	}
	else
	{
		applicabilityId = Applicability::CHANGEABLE;
	}

	return applicabilityId;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[LC02003]\c\ in CPAP, Ve tot alarm shall be disabled
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue HighExhTidalVolSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	// $[02143] The setting's new patient value ...

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);
	const Real32 IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;
	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const DiscreteValue  MODE_VALUE = pMode->getAdjustedValue();

	BoundedValue  boundedValue;

	// calculate high exhaled tidal volume's IBW-based, new-patient value...
	boundedValue.value = (SettingConstants::NP_HIGH_EXH_TIDAL_VOL_IBW_SCALE * IBW_VALUE);

	if ( MODE_VALUE == ModeValue::CPAP_MODE_VALUE )
	{
		boundedValue.value = DEFINED_UPPER_ALARM_LIMIT_OFF;
	}

	// the new-patient value is "clipped" at the absolute minimum value...
	boundedValue.value = MAX_VALUE(boundedValue.value,	  // $[TI1]
								   getAbsoluteMinValue_());	  // $[TI2]

	// warp the calculated value to a "click" boundary...
	// but not clipped by current constraints (SCR #6478)
	getBoundedRange().warpValue(boundedValue, BoundedRange::WARP_NEAREST, FALSE);	 

	return(boundedValue);
}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  The setting is only interested in the transition of mode type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Transition rules.
//  $[LC02001] transition from CPAP
//  $[LC02003] transition to CPAP
//---------------------------------------------------------------------
//@ PreCondition
//  settingId == SettingId::MODE
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void HighExhTidalVolSetting::acceptTransition(const SettingId::SettingIdType settingId,
											  const DiscreteValue  newValue,
											  const DiscreteValue  currValue)
{
	AUX_CLASS_PRE_CONDITION(settingId == SettingId::MODE, settingId);

	setAdjustedValue(getNewPatientValue());
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	HighExhTidalVolSetting::SoftFault(const SoftFaultID  softFaultID,
									  const Uint32       lineNumber,
									  const char*        pFileName,
									  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							HIGH_EXH_TIDAL_VOL_SETTING, lineNumber, pFileName,
							pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the maximum lower bound, and set the lower limit to it.
//
//  $[02142]
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	HighExhTidalVolSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	//-------------------------------------------------------------------
	// update upper limit value...
	//-------------------------------------------------------------------
	{
		const Setting*  pPatientCctType =
			SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

		const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
			pPatientCctType->getAdjustedValue();

		// update the upper-most change point, based on the circuit type...
		switch ( PATIENT_CCT_TYPE_VALUE )
		{
			case PatientCctTypeValue::NEONATAL_CIRCUIT :	// $[TI1]
				::Node1_.value = 500.0f; // mL...
				break;
			case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	// $[TI2]
				::Node1_.value = 1500.0f; // mL...
				break;
			case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI3]
				::Node1_.value = 3000.0f; // mL...
				break;
			default :
				// unexpected patient circuit type value...
				AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
				break;
		}

		// update the resolution between the upper-most change point, and the
		// absolute maximum, "OFF" value...
		::IntervalList_.resolution = (::IntervalList_.value - ::Node1_.value);
	}

	//-------------------------------------------------------------------
	// Determine minimum constraint...
	//-------------------------------------------------------------------
	{
		BoundedRange::ConstraintInfo  minConstraintInfo;

		// start off with the absolute minimum and id and value...
		minConstraintInfo.id    = HIGH_EXH_TIDAL_VOL_MIN_ID;
		minConstraintInfo.value = getAbsoluteMinValue_();

		const BoundedSetting*  pLowExhMandTidalVol =
			SettingsMgr::GetBoundedSettingPtr(SettingId::LOW_EXH_MAND_TIDAL_VOL);
		const BoundedSetting*  pLowExhSpontTidalVol =
			SettingsMgr::GetBoundedSettingPtr(SettingId::LOW_EXH_SPONT_TIDAL_VOL);

		const Real32  LOW_EXH_MAND_TIDAL_VOL_VALUE  =
			BoundedValue(pLowExhMandTidalVol->getAdjustedValue()).value;
		const Real32  LOW_EXH_SPONT_TIDAL_VOL_VALUE  =
			BoundedValue(pLowExhSpontTidalVol->getAdjustedValue()).value;

		BoundStatus  dummyStatus(getId());

		// get the current value of the two low tidal volume settings; this setting
		// must always be higher that the two low tidal volume settings...
		BoundedValue    lowTidalVolBasedMin;
		SettingBoundId  minTidalVolumeBoundId;

		// NOTE:  a '>=' comparison is used instead of a '>' comparison, to bias
		//        toward MAND limit (for no better reason than that's what B-Tree
		//        expects -- DCR #5118)...
		if ( LOW_EXH_MAND_TIDAL_VOL_VALUE >= LOW_EXH_SPONT_TIDAL_VOL_VALUE )
		{	// $[TI4]
			// low mand tidal volume setting is as restrictive, or more, than
			// its spontaneous counterpart...
			lowTidalVolBasedMin.value = LOW_EXH_MAND_TIDAL_VOL_VALUE;
			minTidalVolumeBoundId     = HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_MAND_ID;
		}
		else
		{	// $[TI5]
			// low spont tidal volume setting is more restrictive than its mandatory
			// counterpart...
			lowTidalVolBasedMin.value = LOW_EXH_SPONT_TIDAL_VOL_VALUE;
			minTidalVolumeBoundId     = HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_SPONT_ID;
		}

		// NOTE:  a '>=' comparison is used instead of a '>' comparison because
		//        of the increment done to the value within the if clause, which
		//        equates an '>=' into an '>'...
		if ( lowTidalVolBasedMin.value >= minConstraintInfo.value )
		{	// $[TI6]
			// reset this range's minimum constraint to allow for "warping" of a
			// new minimum value...
			getBoundedRange_().resetMinConstraint();

			// warp the calculated value to a "click" boundary...
			getBoundedRange_().warpValue(lowTidalVolBasedMin);

			// calculate the value one "click" above this current low tidal volume
			// alarm value, by using the high exh tidal volume's bounded range
			// instance...
			getBoundedRange_().increment(1, lowTidalVolBasedMin, dummyStatus);

			// the low tidal volume-based minimum is more restrictive, therefore
			// save its value and id as the minimum bound value and id...
			minConstraintInfo.id    = minTidalVolumeBoundId;
			minConstraintInfo.value = lowTidalVolBasedMin.value;
		}	// $[TI7] -- absolute minimum bound is more restrictive...

		// this setting has no minimum soft bounds...
		minConstraintInfo.isSoft = FALSE;

		// update the minimum bound to the most restrictive minimum limit...
		getBoundedRange_().updateMinConstraint(minConstraintInfo);
	}
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute minimum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic minimum constraint.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	HighExhTidalVolSetting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");

	Real32  absoluteMinValue;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			absoluteMinValue = SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_MISC;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2]
			absoluteMinValue = SettingConstants::TIDAL_VOL_STANDARD_MIN_VALUE;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMinValue);
}
