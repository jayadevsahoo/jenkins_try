#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbSampleRm
//---------------------------------------------------------------------
//@ Interface-Description
//  Handles collection of the trended RM values (NIF, P100, VC).
//  The setValue() is overloaded so that the value will not get written
//  in the base class, outside of the check for event accepted.  This
//  object is constructed with the PatientDataId for the source data,
//  the TrendSelectId for the trend value, and the EventId for the
//  accepted event.  
// 
//---------------------------------------------------------------------
//@ Rationale
//  Ecapsulates the handling of RM trended values that need an operator
//  accept before the values are used in trending.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The overloaded compressData() method checks for the maneuver-accepted
//  event when compressing the 1Hr timescale. If not 1Hr timescale, a last 
//  sample compression is implemented.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header::   /840/Baseline/Trend-Database/vcssrc/TrendDbSampleRm.ccv   10.7   08/17/07 11:02:36   pvcs  
// 
//@ Modification-Log
//
//  Revision: 002  By:  rhj    Date:  20-July-2007    SCR Number: 6387
//       Project:  Trend
//       Description: 
//           Fixed the Rm results during a Circuit Disconnect/Occlusion.
//
//  Revision: 001  By:  ksg    Date:  17-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
//
//=====================================================================


#include <string.h>
#include "BitUtilities.hh"  // Utilities

#include "Trend_Database.hh"
#include "TrendDbSampleRm.hh"
#include "TrendDbEventReceiver.hh"



//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleRm [constructor]
//
//@ Interface-Description
//  Construct with the PatientDataId of the source data and the
//  TrendSelectValueId of the trendId and the EventId for the accepted
//  event.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleRm::TrendDbSampleRm( PatientDataId::PatientItemId id,
								  TrendSelectValue::TrendSelectValueId trendId,
								  TrendEvents::EventId eventId )
	:   TrendDbSampleData( id, trendId ),
	eventId_(eventId)
{
	clear_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleAvg [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleRm::~TrendDbSampleRm()	
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: compressData
//
//@ Interface-Description
//  Propegates the last accepted maneuver data over the 8 timescales
//---------------------------------------------------------------------
//@ Implementation-Description
//  Checks for the accepted-maneuver event bit before setting the 1Hr value.
// 
//  Satisfies requirements:
//  $[TR01176] After NIF, VC and P100 maneuver values are accepted, the
//  trended value for the associated maneuver...
//  $[TR01203] Trending shall display Gaps for...
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the provided timescale is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleRm::compressData( TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION( (timescale < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES), timescale );

	const TrendDbEventReceiver & eventReceiver_ = TrendDataMgr::GetEventReceiver();
	Uint32 activeEvents = eventReceiver_.getAutoEvents();

	// If a circuit disconnect or an occlusion occured, RM values must be gaped
	if (eventReceiver_.isPatientCircuitOccluded() || !eventReceiver_.isPatientConnected())
	{
		valueArray_[ timescale ].isValid = FALSE;
	}
	else
	{
		valueArray_[ timescale ].isValid = TRUE;
	
		// If not 1Hr timescale, simply copy the event value from the previous timescale
		if (timescale != TrendTimeScaleValue::TREND_TIME_SCALE_1HR)
		{
			valueArray_[timescale] = valueArray_[timescale-1];
		}
		// Otherwise, check for the maneuver-accepted event bit and get the maneuver value
		else if (IsBitSet( &activeEvents, eventId_ - TrendEvents::BEGIN_AUTO_EVENT_ID ))
		{
			valueArray_[ timescale ] = eventReceiver_.getEventValue( eventId_ );
			TREND_MGR_DBG4( 16, "RM accepted, event Id ", eventId_, ", accepted value ", 
						valueArray_[timescale].data.realValue );
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValue
//
//@ Interface-Description
//  Does nothing
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	valid timescale
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleRm::setValue( const TrendValue & newValue, TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION((timescale < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES), timescale );

	// No-op, does nothing the work is done in the compress call
	// Overloads base class to prevent event value from being written outside of this class.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear_
//
//@ Interface-Description
//  overloaded pure virtual function from base class
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleRm::clear_( void )
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
void TrendDbSampleRm::SoftFault(const SoftFaultID  softFaultID,
								const Uint32       lineNumber,
								const char*        pFileName,
								const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_SAMPLE_MANEUVER,
							lineNumber, pFileName, pPredicate);
}





