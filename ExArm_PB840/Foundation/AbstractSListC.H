 
#ifndef AbstractSListC<TYPE>_HH
#define AbstractSListC<TYPE>_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AbstractSListC<TYPE> - Singly-Linked List of 'TYPE' Items.
//--------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSListC.H_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class  Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "SLinkedNodeC<TYPE>.hh"
#include "Heap.hh"
//@ End-Usage


class AbstractSListC<TYPE>
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&                  ostr,
				       const AbstractSListC<TYPE>& sList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

    //@ Friend:  SIteratorC<TYPE>
    // This list's corresponding iterator needs access to the protected
    // members of this list.
    friend class  SIteratorC<TYPE>;

    //@ Friend:  AbstractSListR<TYPE>
    // The singly-linked reference list uses a singly-linked list of
    // void*'s, and needs access to it's protected members.
    friend class  AbstractSListR<TYPE>;

  public:
    virtual ~AbstractSListC<TYPE>(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);

    SigmaStatus  search(const TYPE&      item,
			const SearchFrom startFrom = FROM_FIRST);

    void  insert (const TYPE&          newItem,
		  const InsertionPlace insertPlace = AFTER_CURRENT);
    void  append (const TYPE& newItem);
    void  prepend(const TYPE& newItem);

    SigmaStatus  removeCurrent(void);
    SigmaStatus  removeItem(const TYPE& item);
    void         clearList(void);

    inline const TYPE&  currentItem(void) const;
    inline TYPE&        currentItem(void);

    void  operator=(const AbstractSListC<TYPE>& sList);

    inline Boolean  operator==(const AbstractSListC<TYPE>& sList) const;
    inline Boolean  operator!=(const AbstractSListC<TYPE>& sList) const;

    static void  SoftFault(const SoftFaultID softFaultID,
                           const Uint32      lineNumber,
                           const char*       pFileName = NULL,
                           const char*       pBoolTest = NULL);

  protected:
    inline AbstractSListC<TYPE>(Heap* pHeap);

    // return a pointer to the link that is equivalent to 'item', starting
    // the search from 'pStart'...
    const SLinkedNodeC<TYPE>* findNode_(const TYPE&        item,
				 const SLinkedNodeC<TYPE>* pStartNode) const;

    inline Boolean  isLegalNode_(const SLinkedNodeC<TYPE>* pNode) const;

    inline const SLinkedNodeC<TYPE>*  getFirst_  (void) const;
    inline const SLinkedNodeC<TYPE>*  getCurrent_(void) const;

    inline SLinkedNodeC<TYPE>*  getFirst_  (void);
    inline SLinkedNodeC<TYPE>*  getCurrent_(void);

    inline void  setCurrent_(SLinkedNodeC<TYPE>* pNewCurr);

    Boolean  isEquivTo_(const AbstractSListC<TYPE>& sList) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    AbstractSListC<TYPE>(const AbstractSListC<TYPE>&);	// not implemented...
    AbstractSListC<TYPE>(void);				// not implemented...

    inline void  deallocNode_(SLinkedNodeC<TYPE>*& pOldNode);

    SLinkedNodeC<TYPE>* getPrevNode_(const SLinkedNodeC<TYPE>* pStart) const;

    //@ Data-Member:  rHeapOfNodes_
    // A reference to this list's heap of nodes.
    Heap&  rHeapOfNodes_;

    //@ Data-Member:  pFirst_
    // A pointer to the first node in this list.
    SLinkedNodeC<TYPE>*  pFirst_;

    //@ Data-Member:  pCurrent_
    // A pointer to the current node of this list.
    SLinkedNodeC<TYPE>*  pCurrent_;
};


// Inlined methods...
#include "AbstractSListC<TYPE>.in"


#endif  // AbstractSListC<TYPE>_HH
