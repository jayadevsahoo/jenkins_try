#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDataSet
//---------------------------------------------------------------------
//@ Interface-Description
//  TrendRequestInfo - Encapsulation of the requested data whic includes
// 		Start frame/seq number to retreive the data
// 		Number of rows to retrieve (Max 360)
// 		The trend paramter for each of the rows (Max 8)
// 
// The Class also encapsulates the returned data which includes
//      An Array of Trend values for each column
//      Timestamp for each sample "row"
//      Events for each sample "row"
//      Sequence frame number for each sample "row"
//
// The Class containts a Status, while this Status is in "PROCESSING" the
// Trend Data Manager is working on the request and thge client should
// make no other modifications or read the data until the Trend Data Manager
// is done and the status is no longer "PROCESSING"
//---------------------------------------------------------------------
//@ Rationale
//  Provides safe access the the array of trend data values returned from
//  the Trend Data Manager
//---------------------------------------------------------------------
//@ Implementation-Description
//  The TrendDataSet memory is passed to the manager which access it to
//  full in the trend data vales.  A signal is sent back to the client
//  which then can acces the data.  This is done to minmize the amount of
//  memory read/write operations needed to return trend data.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDataSet.ccv   25.0.4.0   19 Nov 2013 14:34:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjf    Date:  13-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//
//=====================================================================

//@ Usage-Classes
//@ End-Usage
// 
#include <string.h>

#include "Trend_Database.hh"
#include "TrendDataSet.hh"
#include "TrendDataValues.hh"


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDataSet [constructor]
//
//@ Interface-Description
//  Construct the TrendDataSet class with initiale default values
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clears the values
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDataSet::TrendDataSet( void )
{
	clear();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDataSet [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDataSet::~TrendDataSet()	
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
//  Initializes all data to 0x00
//---------------------------------------------------------------------
//@ Implementation-Description
//  All of the contain data initial values need to be zero as default
//  unitialized
//---------------------------------------------------------------------
//@ PreCondition
//	status_ must not be PROCESSING
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataSet::clear( void )
{
	AUX_CLASS_PRE_CONDITION( (status_ != PROCESSING), status_ );

	TREND_MGR_DBG6( 32, "DataSet clear - Addr ", hex, (void *)this, dec,
						", size ", sizeof( TrendDataSet ) );
	memset( (void *)this, 0x00, sizeof( TrendDataSet ) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setRequestInfo
//
//@ Interface-Description
//  Copy the request information into the data set
//  This is used to setup the request before sending it to the TrendDataMgr
//---------------------------------------------------------------------
//@ Implementation-Description
//  Make a deep copy of the request information
//---------------------------------------------------------------------
//@ PreCondition
//	status_ must not be PROCESSING
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataSet::setRequestInfo( const TrendRequestInfo & requestInfo )
{
	AUX_CLASS_PRE_CONDITION( (status_ != PROCESSING), status_ );
	requestData_ = requestInfo;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getRequestInfo_
//
//@ Interface-Description
//  Return a reference to the request information
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	status_ must be PROCESSING
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendRequestInfo & TrendDataSet::getRequestInfo_( void )
{
	AUX_CLASS_PRE_CONDITION( (status_ == PROCESSING), status_ );
	return(requestData_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
TrendDataSet::SoftFault(const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DATA_SET,
							lineNumber, pFileName, pPredicate);
}


