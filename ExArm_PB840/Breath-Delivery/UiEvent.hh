
#ifndef UiEvent_HH
#define UiEvent_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: UiEvent - implements the handling of events originated by the UI.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/UiEvent.hhv   10.7   08/17/07 09:44:54   pvcs  
//
//@ Modification-Log
//
//  Revision: 004   By: gdc   Date:  23-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Refactored for processing of RM maneuver requests.
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  yyy    Date:  01-Oct-1998    DCS Number:
//  Project:  BiLevel
//  Description:
//			Bilevel initial version.
//
//  Revision: 001  By:  iv    Date:  27-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version - per Breath Delivery formal code review 
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "EventData.hh"
#include "UiEventBuffer.hh"
#include "NetworkApp.hh"

//@ Usage-Classes

//@ End-Usage



class UiEvent {
  public:

    //@ Type: UserEventCallbackPtr
    // A pointer to a function used as a call back for user events.
    typedef EventData::EventStatus (*UserEventCallbackPtr) (const EventData::EventId, const Boolean);

    UiEvent(const UserEventCallbackPtr pEventCallback, const EventData::EventId id);
    ~UiEvent(void);

	inline EventData::EventId getId(void) const;
	inline EventData::EventStatus getEventStatus(void) const;
	void setEventStatus(const EventData::EventStatus status,
						const EventData::EventPrompt prompt=EventData::NULL_EVENT_PROMPT);
	void receiveRequest(EventData::RequestedState request);
	void receiveManeuverRequest(EventData::RequestedState request);


	static void Initialize(void);
	static void RequestUserEvent(XmitDataMsgId msgId, void *pData, Uint size);
	static EventData::EventStatus ProcessEvent(const EventData::EventId id,
												const Boolean eventStatus);

	static EventData::EventStatus SstToOnlineEvent(const EventData::EventId id,
											const Boolean eventStatus);
	static void NewCycle(void);
  
    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
				 
  protected:

  private:
    UiEvent(const UiEvent&);		// not implemented...
    void   operator=(const UiEvent&);	// not implemented...
	UiEvent(void); // not implemented...

	static UiEventBuffer& GetUiEventBuffer_(void);
	
	//@ Data-Member: eventStatus_
	// event status
	EventData::EventStatus eventStatus_;

	//@ Data-Member: id_
	// this event's id
	EventData::EventId id_;
	
	//@ Data-Member: pUserEventCallBack_
	// A pointer to a function, triggered by the event 
	UserEventCallbackPtr pUserEventCallBack_;

};



// Inlined methods...
#include "UiEvent.in"


#endif // UiEvent_HH 
