#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SafetySystemTest - safety valve and inspiratory check valve test
//---------------------------------------------------------------------
//@ Interface-Description
//  This class tests the safety valve and inspiratory check valve.
//	SV open is checked for an occlusion, SV loopback current is
//	is tested, and inspiratory check valve functionality
//	is tested.
//	NOTE: SV cracking pressure is tested in GasSupplyTest.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the safety valve/inspiratory check valve test.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method runTest() is called to run the test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SafetySystemTest.ccv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  quf    Date: 06-Oct-1997    DR Number: DCS 2451
//       Project:  Sigma (840)
//       Description:
//			Added temporary increase of SM Manager Task priority during
//			isLoopbackOk_() and measureBleed_(), restored base priority
//			when critical section is completed.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//            Added timeouts to while loops in measureBleed_().
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "SafetySystemTest.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmExhValveController.hh"
#include "SmFlowController.hh"
#include "SmPressureController.hh"
#include "SmUtilities.hh"
#include "SmManager.hh"
#include "TestData.hh"
#include "SmConstants.hh"

#include "Task.hh"

#include "MainSensorRefs.hh"
#include "ValveRefs.hh"
#include "Solenoid.hh"
#include "PressureSensor.hh"
#include "OsTimeStamp.hh"

#if defined(SIGMA_DEVELOPMENT)
#include <stdio.h>
#endif  // defined(SIGMA_DEVELOPMENT)

#ifdef SIGMA_UNIT_TEST
#include "SmUnitTest.hh"
#include "FakeIo.hh"
#include "FakeControllers.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//@ Constant: MAX_NO_CURRENT
// max SV current, SV open
const Real32 MAX_NO_CURRENT   = 21.0;  // mA

//@ Constant: MIN_HIGH_CURRENT
// min SV current, SV closed, high current phase
const Real32 MIN_HIGH_CURRENT = 500.0;  // mA

//@ Constant: MAX_LOW_CURRENT
// max SV current, SV closed, low current phase
const Real32 MAX_LOW_CURRENT  = 350.0;  // mA

//@ Constant: MIN_LOW_CURRENT
// min SV current, SV closed, low current phase
const Real32 MIN_LOW_CURRENT  = 150.0;  // mA

//@ Constant: SV_OPEN_MAX_BACKPRESSURE
// max SV backpressure, SV open
const Real32 SV_OPEN_MAX_BACKPRESSURE = 1.0;  // cmH2O

//@ Constant: MIN_SV_BLEED_DECAY_TIME
// min pressure bleed time after SV is open, insp check valve test
const Real32 MIN_SV_BLEED_DECAY_TIME = 200;  // msec

//@ Constant: MAX_SV_BLEED_DECAY_TIME
// max pressure bleed time after SV is open, insp check valve test
const Real32 MAX_SV_BLEED_DECAY_TIME = 600;  // msec

//@ Constant: BLEED_START_PRESSURE
// insp check valve test starting pressure
const Real32 BLEED_START_PRESSURE = 85.0;  // cmH2O

//@ Constant: BLEED_END_PRESSURE
// insp check valve test ending pressure
const Real32 BLEED_END_PRESSURE = 5.0;  // cmH2O


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SafetySystemTest()  [Default Constructor]
//
//@ Interface-Description
//	 Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SafetySystemTest::SafetySystemTest(void)
{
	CALL_TRACE("SafetySystemTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SafetySystemTest()  [Destructor]
//
//@ Interface-Description
//	 Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SafetySystemTest::~SafetySystemTest(void)
{
	CALL_TRACE("~SafetySystemTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest()
//
//@ Interface-Description
//  This method is used to perform the safety valve/insp check valve test.
//  It takes no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09053]
//	Autozero the pressure transducers and close the exhalation valve.
//	Open safety valve and check for safety valve occlusion.
//	Check safety valve loopback current with valve open then closed.
//	Check inspiratory check valve via a pressure bleed test.
//	Open the exhalation valve.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SafetySystemTest::runTest(void)
{
	CALL_TRACE("SafetySystemTest::runTest(void)");

	RSmUtilities.autozeroPressureXducers();
	RSmExhValveController.close();

	if (isSafetyValveUnoccluded_())
	{
	// $[TI1]
		if (isLoopbackOk_())
		{
		// $[TI1.1]
			Real32 decayTime = measureBleed_(); // msec

			RSmManager.sendTestPointToServiceData
				(SmDataId::SAFETY_SYSTEM_ELAPSED_TIME, decayTime);

			if (decayTime < MIN_SV_BLEED_DECAY_TIME)
			{
			// $[TI1.1.1]
				// Bad inspiratory check valve.
				// Minor failure, Condition 4
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT,
					SmStatusId::CONDITION_4);
			}
			else if (decayTime > MAX_SV_BLEED_DECAY_TIME)
			{
			// $[TI1.1.2]
				// Occluded Safety Valve Bleed.
				// Minor failure, Condition 3
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_3);
			}
			// $[TI1.1.3]
			// implied else: reverse flow test passed
		#ifdef SIGMA_UNIT_TEST
			SmUnitTest::ImpliedElse = TRUE;
		#endif  // SIGMA_UNIT_TEST
		}
		else
		{
		// $[TI1.2]
			// Major Failure, Condition 2
			// Safety Valve driver or loopback.
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_2);
		}
		// end else
	}
	// $[TI2]
	// implied else: SV occlusion test failed.

#ifdef SIGMA_UNIT_TEST
	SmUnitTest::ImpliedElse = TRUE;
#endif  // SIGMA_UNIT_TEST

	RSmExhValveController.open();		
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the fault 
//  macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and 
//  'lineNumber' are essential pieces of information.  The 'pFileName' 
//  and 'pPredicate' strings may be defaulted in the macro to reduce 
//  code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 
void
SafetySystemTest::SoftFault(const SoftFaultID  softFaultID,
			    const Uint32       lineNumber,
			    const char*        pFileName,
			    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, SAFETYSYSTEMTEST,
                          lineNumber, pFileName, pPredicate);

}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isSafetyValveUnoccluded_()
//
//@ Interface-Description
//	This method is used to test if the open safety valve is occluded.
//	No arguments, returns TRUE if occlusion is not detected, else
//	returns FALSE if occlusion is detected or if test flow can't be
//	established.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Open the safety valve, and establish a test flow.  If stable flow
//	can't be established, return FALSE.
//	Measure the inspiratory pressure and turn off the flow.
//	If insp pressure is too high, return FALSE, else return TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
SafetySystemTest::isSafetyValveUnoccluded_(void)
{
	CALL_TRACE("SafetySystemTest::isSafetyValveUnoccluded(void)");

	Boolean testPassed = TRUE;
	const Real32 TEST_FLOW = 60.0;  // lpm

	RSmUtilities.openSafetyValve();
	RSmFlowController.establishFlow( TEST_FLOW, DELIVERY, AIR);

	if (!RSmFlowController.waitForStableFlow())
	{
	// $[TI1]
		// Can't establish flow
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_5);

		testPassed = FALSE;
	}
	else
	{
	// $[TI2]
		Real32 svBackpressure = RInspPressureSensor.getValue();

		RSmManager.signalTestPoint();
		RSmManager.sendTestPointToServiceData( svBackpressure);

		if (svBackpressure > SV_OPEN_MAX_BACKPRESSURE)
		{
		// $[TI2.1]
			// Safety Valve occluded
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_1);

			testPassed = FALSE;

			// close SV if test fails
			RSmUtilities.closeSafetyValve();
		}
		// $[TI2.2]
		// implied else: SV not occluded
		// NOTE: if test passes, leave SV open for next phase
	}
	// end else

	RSmFlowController.stopFlowController();

#ifdef SIGMA_UNIT_TEST
	SmUnitTest::IsSafetyValveUnoccludedRetVal = testPassed;
#endif  // SIGMA_UNIT_TEST

	return( testPassed);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isLoopbackOk_()
//
//@ Interface-Description
//	This method is used to test the SV loopback current.
//  No arguments, returns TRUE if loopback current is OK, else returns
//	FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: SV open from the last test phase.
//
//	Set test flag TRUE (assume test will pass).
//	Increase SM Manager Task priority.
//	Measure SV loopback current with SV open, set test flag FALSE if
//	current is out of range.
//	Close the SV, wait for current to settle, then read the loopback
//	current while in high current phase, set test flag FALSE if current
//	is out of range.
//	Delay then measure the loopback current while the SV is still in the
//	high current phase, set test flag FALSE if current is out of range.
//	Delay then measure the loopback current while the SV is in the
//	low current phase, set test flag FALSE if current is out of range.
//	Restore the SM Manager Task base priority.
//	Return the test flag.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
SafetySystemTest::isLoopbackOk_(void)
{
	CALL_TRACE("SafetySystemTest::isLoopbackOk(void)");

	Boolean testPassed = TRUE;
	Uint32 initialTime = 0;
	Uint32 elapsedTime = 0;
	Real32 current0;
	Real32 current100;
	Real32 current300;
	Real32 current1000;

#ifdef SIGMA_DEBUG
	Uint32 time100, time300, time1000;
#endif  // SIGMA_DEBUG

	// EV should be closed and SV should be open from last test phase

	// increase SM Manager Task priority
	ServiceMode::IncreasePriority();

	// Read the safety valve loopback current
	current0 = RSafetyValve.getCurrent();

	// Check the loopback with no current applied
	if(current0 > MAX_NO_CURRENT)
	{
	// $[TI1]
		// Bad Safety Valve loopback or current driver
		testPassed = FALSE;
	}
	// $[TI2]
	// implied else

	// synchronize to next VRTX tick
	Task::Delay( 0, 10);

	// start counting time
	OsTimeStamp svTimer;

	// Close the Safety Valve
	// NOTE: Do not use SmUtilities.closeSafetyValve(), the
	// delay will interfere with the test timing!!
	RSafetyValve.close();

	// Allow the current to settle
	Task::Delay( 0, 100);
	initialTime = svTimer.getPassedTime();

#ifdef SIGMA_DEBUG
	time100 = initialTime;
#endif  // SIGMA_DEBUG

	// Read the safety valve loopback current @ 100 msec
	current100 = RSafetyValve.getCurrent();

	// Check the loopback during the high current phase
	if (current100 < MIN_HIGH_CURRENT)
	{
	// $[TI3]
		// Bad Safety Valve loopback or high current driver
		testPassed = FALSE;
	}
	// $[TI4]
	// implied else

	// wait until next sample time
	elapsedTime = svTimer.getPassedTime() - initialTime;
	Task::Delay( 0, 200 - elapsedTime);
	initialTime = svTimer.getPassedTime();

#ifdef SIGMA_DEBUG
	time300 = initialTime;
#endif  // SIGMA_DEBUG

	// Read the safety valve loopback current @ 300 msec
	current300 = RSafetyValve.getCurrent();

	// Check the loopback during the high current phase
	if (current300 < MIN_HIGH_CURRENT)
	{
	// $[TI5]
		// Bad driver
		testPassed = FALSE;
	}
	// $[TI6]
	// implied else

	// wait until next sample time
	elapsedTime = svTimer.getPassedTime() - initialTime;
	Task::Delay( 0, 700 - elapsedTime);

#ifdef SIGMA_DEBUG
	time1000 =  svTimer.getPassedTime();
#endif  // SIGMA_DEBUG

	// Read the safety valve loopback current @ 1000 msec
	current1000 = RSafetyValve.getCurrent();

	// Check the loopback during the low current phase
	if (current1000 > MAX_LOW_CURRENT)
	{
	// $[TI7]
		// Bad low current driver
		testPassed = FALSE;
	}
	else if (current1000 < MIN_LOW_CURRENT)
	{
	// $[TI8]
		// Bad low current driver
		testPassed = FALSE;
	}
	// $[TI9]
	// implied else

#ifdef SIGMA_DEBUG
	printf("%u  %u  %u\n", time100, time300, time1000);
#endif  // SIGMA_DEBUG

	// restore SM Manager Task base priority
	ServiceMode::RestorePriority();

	// send the data to the gui
	RSmManager.sendTestPointToServiceData
		(SmDataId::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT1, current0);

	RSmManager.sendTestPointToServiceData
		(SmDataId::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT2, current100);

	RSmManager.sendTestPointToServiceData
		(SmDataId::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT3, current300);

	RSmManager.sendTestPointToServiceData
		(SmDataId::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT4, current1000);

#ifdef SIGMA_UNIT_TEST
	SmUnitTest::IsLoopBackOkRetVal = testPassed;	
#endif  // SIGMA_UNIT_TEST

	return( testPassed);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: measureBleed_()
//
//@ Interface-Description
//	This method is used to test the inspiratory check valve via
//	a pressure bleed time.  It takes no arguments and returns the
//	pressure bleed time in milliseconds.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: safety and exhalation valves close from last test phase.
//
//	Build system pressure to just above the test starting point.
//	Increase SM Manager Task priority.
//	Open the safety valve.  Wait until pressure drops to the test starting
//	pressure then measure the time it takes to get from this pressure
//	to the test end pressure = pressure bleed time.
//	Close the safety valve.  Restore SM Manager Task base priority.
//	Return the pressure bleed time.
//	NOTE: if for some reason system pressure can't be built within
//	a reasonable timeout, the test returns a default pressure bleed time
//	of 0.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32
SafetySystemTest::measureBleed_(void)
{
	CALL_TRACE("SafetySystemTest::measureBleed_(void)");

	const Real32 TARGET_PRESSURE = 95.0;  // cmH2O
	const Real32 FLOW = 5.0;  // lpm
	const Int32 PRESSURIZE_TIMEOUT = RSmUtilities.pressureTimeout(
							STATE_SERVICE, TARGET_PRESSURE, FLOW );
	const Real32 START_PRESSURE_TIMEOUT = 300.0 ;  // msec
	const Real32 END_PRESSURE_TIMEOUT = 2.0F * MAX_SV_BLEED_DECAY_TIME;

	Real32 elapsedTime = 0.0;

	RTestData.resetDeliveredVolume();

	RSmPressureController.establishPressure(
							TARGET_PRESSURE, DELIVERY, AIR, FLOW);

	if (RSmPressureController.waitForPressure(PRESSURIZE_TIMEOUT))
	{
	// $[TI1]
		// increase SM Manager Task priority
		ServiceMode::IncreasePriority();

		RSmPressureController.stopPressureController();
	
		// open the safety valve
		// NOTE: don't use SmUtilities::openSafetyValve() since the
		// built-in delay may cause us to miss the test starting pressure!!
		RSafetyValve.open();
	
		// Find the starting point
		// typical timing: 100 msec for SV to open, 30 msec for pressure
		// to drop from 95 to 85 cmH2O
		OsTimeStamp timer;
		while (RInspPressureSensor.getValue() > BLEED_START_PRESSURE &&
				timer.getPassedTime() < START_PRESSURE_TIMEOUT)
		{
		// $[TI1.1]
			Task::Delay (0, SM_CYCLE_TIME);
		#ifdef SIGMA_UNIT_TEST
			SmUnitTest::WhileDoLoopFlag1 = TRUE;
		#endif  // SIGMA_UNIT_TEST
		}
		// end while

		// Find the ending point
		timer.now();  // reset timer zero point to current time
		while (RInspPressureSensor.getValue() > BLEED_END_PRESSURE &&
				timer.getPassedTime() < END_PRESSURE_TIMEOUT)
		{
		// $[TI1.2]
			Task::Delay (0, SM_CYCLE_TIME);
		#ifdef SIGMA_UNIT_TEST
			SmUnitTest::WhileDoLoopFlag2 = TRUE;
		#endif  // SIGMA_UNIT_TEST
		}
		// end while

		// calculate elapsed time from starting pressure to end pressure
		elapsedTime = (Real32) timer.getPassedTime();

		RSmUtilities.closeSafetyValve();

		// restore SM Manager Task base priority
		ServiceMode::RestorePriority();
	}
	else
	{
	// $[TI2]
		RSmPressureController.stopPressureController();
	}	
	// end else

	return( elapsedTime);
}
