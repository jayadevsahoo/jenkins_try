#include "stdafx.h"
//-------------------------------------------------------------------------------
//                        Copyright (c) 2012 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

#include "SwatHelper.hh"
#include "BreathPhase.hh"
#include "SchedulerId.hh"
#include "BreathPhaseScheduler.hh"
#include "MutEx.hh"

namespace swat
{
    std::list<swat::BatchCommandInfo *> g_List;
    MessageProcessor g_clMessageProcessor(enqueueMessage);
    BatchJobManager  g_clBatchJobManager (20,getNextBatch);


    //--------------------------------------------------------------------
    /// getNextBatch
    ///
    /// Return a pointer to the next batch
    ///
    /// @return pointer to BatchCommandInfo
    //--------------------------------------------------------------------
    BatchCommandInfo *getNextBatch()
    {
		BatchCommandInfo* pCommandInfo = NULL;

		MutEx	batchCommandMutex( BD_BATCH_COMMAND_LIST_MT );
		// lock the access to g_List, wait forever to avoid priority inversion
		batchCommandMutex.request((Uint32) Ipc::NO_TIMEOUT );

		if ( g_List.size() > 0 )
		{
			pCommandInfo = g_List.front();
			g_List.pop_front();
		}

		// release the access to g_List
		batchCommandMutex.release();

        return pCommandInfo;
    }

    //--------------------------------------------------------------------
    /// enqueueMessage
    ///
    /// Puts a message on the list
    ///
    /// @return success
    //--------------------------------------------------------------------
    bool enqueueMessage(BatchCommandInfo* p)
    {
		MutEx	batchCommandMutex( BD_BATCH_COMMAND_LIST_MT );

		// lock the access to g_List, wait forever to avoid priority inversion
		batchCommandMutex.request((Uint32) Ipc::NO_TIMEOUT );

        g_List.push_back(p);

		// release the access to g_List
		batchCommandMutex.release();
		
        return true;
    }


    static  SchedulerId::SchedulerIdValue g_lastScheduler;
    static  BreathPhaseType::PhaseType    g_lastPhase = BreathPhaseType::NULL_INSPIRATION;

    void update()
    {
        BreathPhase *pBreathPhase;

        pBreathPhase = BreathPhase::GetCurrentBreathPhase();


        BreathPhaseType::PhaseType phase = g_lastPhase;

        if(pBreathPhase)
        {
            phase = pBreathPhase->getPhaseType();
        }


        SchedulerId::SchedulerIdValue scheduler = BreathPhaseScheduler::GetCurrentScheduler().getId(); 

        //Event Triggered


        if(phase!=g_lastPhase)
        {
            switch (phase)
            {
                case BreathPhaseType::INSPIRATION:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_INSPIRATION);
                    break;

                case BreathPhaseType::EXHALATION:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_EXHALATION);
                    break;

                case BreathPhaseType::EXPIRATORY_PAUSE:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_EXPIRATORY_PAUSE);
                    break;

                case BreathPhaseType::INSPIRATORY_PAUSE:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_INSPIRATORY_PAUSE);
                    break;

                case BreathPhaseType::PAV_INSPIRATORY_PAUSE:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_PAV_INSPIRATORY_PAUSE);
                    break;

                case BreathPhaseType::NIF_PAUSE:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_NIF_PAUSE);
                    break;

                case BreathPhaseType::P100_PAUSE:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_P100_PAUSE);
                    break;

                case BreathPhaseType::VITAL_CAPACITY_INSP:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_VITAL_CAPACITY_INSP);
                    break;

                case BreathPhaseType::BILEVEL_PAUSE_PHASE:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_BILEVEL_PAUSE_PHASE);
                    break;

                case BreathPhaseType::NON_BREATHING:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_NON_BREATHING);
                    break;

		        case BreathPhaseType::NULL_INSPIRATION:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_NULL_INSPIRATION);
                    break;

                default:
                    ;
            }
        }

        if( scheduler!=g_lastScheduler)
        {
            switch(scheduler)
            {
                case  SchedulerId::APNEA:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_APNEA);
                    break;

                case SchedulerId::AC:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_AC);
                    break;

                case SchedulerId::SPONT:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_SPONT);
                    break;

                case SchedulerId::SIMV:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_SIMV);
                    break;

                case SchedulerId::SAFETY_PCV:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_SAFETY_PCV);
                    break;

                case SchedulerId::BILEVEL:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_BILEVEL);
                    break;

                case SchedulerId::DISCONNECT:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_DISCONNECT);
                    break;

                case SchedulerId::OCCLUSION:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_OCCLUSION);
                    break;

                case SchedulerId::SVO:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_SVO);
                    break;

                case SchedulerId::STANDBY:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_STANDBY);
                    break;

                case SchedulerId::POWER_UP:
                    swat::EventManager::signalEvent(swat::EventManager::EVENT_POWER_UP);
                    break;

                default:
                    ;
            }
        }

        g_lastPhase     = phase;
        g_lastScheduler = scheduler;

    }
}
