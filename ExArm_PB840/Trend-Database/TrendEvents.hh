#ifndef TrendEvents_HH
#define TrendEvents_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation 
//=====================================================================

//=====================================================================
// Class: TrendEvents - Encapsulation of events for one sample/frame
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendEvents.hhv   25.0.4.0   19 Nov 2013 14:34:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: mnr    Date: 17-Feb-2010     DR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX related updates.
//
//  Revision: 005   By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Added SRS tracing and SCR number for previous header.
// 
//  Revision: 004   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Added more events for NEO project.
//
//  Revision: 003   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 002  By:  rhj    Date:  20-Jul-2007    SCR Number: 6383
//       Project:  Trend
//       Description:
//             Added Insp/Exp pause events
// 
//  Revision: 001  By:  mjf    Date:  05-MAR-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
//=====================================================================
//@ Usage-Classes
//@ End-Usage


#include "Trend_Database.hh"


// TrendEvents class
class TrendEvents
{
public:

	// [TR01155] Enum's for the different events that can be stored
	enum EventId
	{
		BEGIN_USER_EVENT_ID             = 0,
		USER_EVENT_SUCTION              = 0,
		USER_EVENT_RX                   = 1,
		USER_EVENT_BLOOD_GAS            = 2,
		USER_EVENT_RESPIRATORY_MANEUVER = 3,
		USER_EVENT_CIRCUIT_CHANGE       = 4,
		USER_EVENT_START_WEANING        = 5,
		USER_EVENT_STOP_WEANING         = 6,
		USER_EVENT_BRONCHOSCOPY         = 7,
		USER_EVENT_X_RAY                = 8,    
		USER_EVENT_RECRUITMENT_MANEUVER = 9,
		USER_EVENT_REPOSITION_PATIENT   = 10,
		USER_EVENT_OTHER                = 11,
		USER_EVENT_SURFACTANT_ADMIN		= 12,
		USER_EVENT_PRONE_POSITION		= 13,
		USER_EVENT_SUPINE_POSITION		= 14,
		USER_EVENT_LEFT_SIDE_POSITION	= 15,
		USER_EVENT_RIGHT_SIDE_POSITION	= 16,
		USER_EVENT_MANUAL_STIMULATION	= 17,
		END_USER_EVENT_ID               = USER_EVENT_MANUAL_STIMULATION,
		MAX_USER_EVENT_ID               = 31,

		BEGIN_AUTO_EVENT_ID             = 32,
		AUTO_EVENT_VENTILATION_INVASIVE = 32,
		AUTO_EVENT_VENTILATION_NIV      = 33,
		AUTO_EVENT_MODE_AC              = 34,
		AUTO_EVENT_MODE_SIMV            = 35,
		AUTO_EVENT_MODE_SPONT           = 36,
		AUTO_EVENT_MODE_BILEVEL         = 37,
		AUTO_EVENT_MANDATORY_VC         = 38,
		AUTO_EVENT_MANDATORY_VC_PLUS    = 39,
		AUTO_EVENT_MANDATORY_PC         = 40,
		AUTO_EVENT_SPONTANEOUS_PS       = 41,
		AUTO_EVENT_SPONTANEOUS_VS       = 42,
		AUTO_EVENT_SPONTANEOUS_NONE     = 43,
		AUTO_EVENT_SPONTANEOUS_PA       = 44,
		AUTO_EVENT_SPONTANEOUS_TC       = 45,

		AUTO_EVENT_TIME_CHANGE          = 46,
		AUTO_EVENT_SAME_PATIENT         = 47,
		AUTO_EVENT_PATIENT_OCCLUSION    = 48,
		AUTO_EVENT_PATIENT_DISCONNECT   = 49,
		AUTO_EVENT_APNEA                = 50,
		AUTO_NIF_MANEUVER_ACCEPTED      = 51,
		AUTO_P100_MANEUVER_ACCEPTED     = 52,
		AUTO_VC_MANEUVER_ACCEPTED       = 53,
		AUTO_EVENT_INSP_MANEUVER        = 54,
		AUTO_EVENT_EXP_MANEUVER         = 55,
		AUTO_EVENT_MODE_CPAP            = 56,
		AUTO_EVENT_O2_SUCTION_BUTTON	= 57,
		AUTO_EVENT_ALARM_VOLUME_CHANGE  = 58,
		AUTO_EVENT_PROX_ENABLED			= 59,
		AUTO_EVENT_PROX_DISABLED        = 60,
		END_AUTO_EVENT_ID               = AUTO_EVENT_PROX_DISABLED,

		MAX_AUTO_EVENT_ID               = 63,

		TOTAL_EVENT_IDS                 = 64
	};

	TrendEvents();
	~TrendEvents();

	TrendEvents( const TrendEvents & source );
	TrendEvents & operator=( const TrendEvents & source );

	inline Boolean isAnyEventSet( void ) const;
	Boolean isEventSet( EventId event ) const;

	void loadUserEvents( Uint32 data );
	void loadAutoEvents( Uint32 data );


	static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char *      pFileName = NULL,
						   const char *      pPredicate = NULL );

private:

	enum Event_Types
	{
		USER_EVENT = 0,
		AUTO_EVENT = 1,
		NUM_EVENT_TYPES = 2
	};

	//@ Data-Member eventData
	// Long value used to hold the Event bits
	Uint32  eventData_[NUM_EVENT_TYPES];
};

// Inline methods
#include "TrendEvents.in"


#endif // TrendEvents_HH
