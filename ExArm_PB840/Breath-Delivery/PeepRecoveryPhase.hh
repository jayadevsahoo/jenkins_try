
#ifndef PeepRecoveryPhase_HH
#define PeepRecoveryPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PeepRecoveryPhase - Implements pressure support inspiration
//		ventilation to recover patient pressure to peep.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PeepRecoveryPhase.hhv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Removed getExhSens_() method as dead code - never called.
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  06-May-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "PsvPhase.hh"

//@ End-Usage

class PeepRecoveryPhase : public PsvPhase
{
  public:
    PeepRecoveryPhase( void) ;
    virtual ~PeepRecoveryPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;
  
  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) ;
	virtual void determineFlowAccelerationPercent_( void) ;
	virtual	Real32 determineMirrorGain_( void) ;

  private:
    PeepRecoveryPhase( const PeepRecoveryPhase&) ;	// not implemented...
    void   operator=( const PeepRecoveryPhase&) ;	// not implemented...

} ;


#endif // PeepRecoveryPhase_HH 
