#ifndef Fio2SensorCalInfo_HH
#define Fio2SensorCalInfo_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Struct: Fio2SensorCalInfo - This struct is a template for the
//                            Fio2Monitor calibration information.
//---------------------------------------------------------------------
// @ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Fio2SensorCalInfo.hhv   25.0.4.0   19 Nov 2013 13:54:04   pvcs  $
//
// @ Modification-Log
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"

struct Fio2SensorCalInfo
{
	Uint16 countAt0Percent;
	Uint16 countAt100Percent;
	Real32 calInspPresAt100Percent;
};
									 
#endif  // Fio2SensorCalInfo_HH

