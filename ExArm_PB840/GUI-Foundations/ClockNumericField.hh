#ifndef ClockNumericField_HH
#define ClockNumericField_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: ClockNumericField - This is a Drawable object which displays a numeric
// value.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/ClockNumericField.hhv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 002   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "NumericField.hh"
#include "TextFont.hh"

//@ Usage-Classes
#include "Precision.hh"
//@ End-Usage

class ClockNumericField : public NumericField
{
public:
	ClockNumericField(TextFont::Size size, Uint numberOfDigits,
											Alignment alignment);
	virtual ~ClockNumericField();

	virtual void draw(const Area &clipArea);

	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

protected:

private:
	ClockNumericField(const ClockNumericField&);  // Declared, but not implemented
	void operator=(const ClockNumericField&);     // Declared, but not implemented

};

#endif // ClockNumericField_HH 
