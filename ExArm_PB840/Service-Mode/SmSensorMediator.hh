#ifndef SmSensorMediator_HH
#define SmSensorMediator_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  SmSensorMediator - Collects sensor data for Service Mode
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmSensorMediator.hhv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date: 25-Aug-1995    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

class SmSensorMediator
{
  public:
    SmSensorMediator(void);
    ~SmSensorMediator(void);

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);

    void newCycle (void);

  protected:

  private:
    SmSensorMediator(const SmSensorMediator&);		// not implemented...
	void operator=(const SmSensorMediator&);		// not implemented...
};


#endif // SmSensorMediator_HH 
