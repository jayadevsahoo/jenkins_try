#ifndef DownloadTypes_HH
#define DownloadTypes_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  Sigma - System-Wide Types, Macros, and Globals.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/DownloadTypes.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $
//
//@ Modifications
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//   Revision: 001   By: jhv   Date: 21-Feb-1996   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include <stddef.h>
#include "NewPlacementOp.hh"
#include "SigmaTypes.hh"

#endif  // DownloadTypes_HH
