#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================
#include "Sigma.hh"
#include "CallTraceMacros.hh"
#include "Barometer_MPL115A2.h"
#include "BitUtilities.hh"

static const uint8_t ON_BOARD_BAROMETER_ADDRESS = 0xC0;
Barometer_MPL115A2* Barometer_MPL115A2::m_instance = NULL;

//TODO: E600 BVP These constants must be consolidated to some utility header file
//somewhere. Right now these are defined in many places.
static const Real32 STD_ATM_PRESSURE_CMH2O = 1033.66F; 	// cmH2O
#define KPA_TO_CMH2O(x) (x)*10.2256


///#############################################################################
///
/// Barometer_MPL115A2::getCount - 
/// Read the ADC values and return the pressure ADC. If the read fails, it 
/// returns 0.
///
//#############################################################################
Uint16 Barometer_MPL115A2::getCount()
{
	return Padc;
}
//#############################################################################
//
//  Barometer_MPL115A2::getPressureRaw - 
//  Return float pressure if valid. STD_ATM_PRESSURE_CMH2O otherwise
//
//#############################################################################
Real32 Barometer_MPL115A2::getPressureRaw()
{
	Real32 ret = 0;

	Uint8 buffer[4];
	if (readAdc_(buffer))
	{
		Padc = (Uint16)combineBits_(buffer[0], buffer[1], 10, 0, 10);
		float Tadc = combineBits_(buffer[2], buffer[3], 10, 0, 10);
		//From MPL115A2 data sheet, the compensated pressure is computed
		//as following:
		//\[P_{comp} = a0 + (b1 + c12 \times T_{adc}) \times P_{adc} + b2 \times T_{adc}\]
		
		double Pcomp = (A0 + (B1+C12*Tadc)*Padc+B2*Tadc);

		//From MPL115A2 data sheet:
		//Pcomp will produce a value of 0 with an input pressure of 50 kPa 
		//and will produce a full-scale value of 1023 with an input pressure
		//of 115 kPa. 
		if((Pcomp >= 0) && (Pcomp < 1024))
		{
			//Pcomp is within reange. Get Kpa values with
			//the following formula:
			//\[P_{kpa} = P_{comp}\times\frac{(115-50)}{1023} + 50\]
			// (115-500)/1023 = 0.063538612
			Pcomp = Pcomp * 0.063538612 + 50;
			//Return must be in cmH2O. Convert from Kpa to cmH2O.
			ret = (Real32)(KPA_TO_CMH2O(Pcomp));

		}
	}

	return ret;
}

//#############################################################################
///Barometer_MPL115A2::getInstance
///Create the only instance of Barometer_MPL115A2, if it doesnt exist already.
///The instance returned is properly initialized, including the read of coefficients
///from the MPL115A2.
//#############################################################################

Barometer_MPL115A2* Barometer_MPL115A2::getInstance()
{
	if(m_instance == NULL)
	{
		static const DWORD WAKE_UP_TIME_MS = 400;
		static const DWORD CONVERSION_TIME_MS = 3;

	//	bool initialized = false;

		HANDLE devHandle = CreateFile(_T("I2C3:"), GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
		if (devHandle != INVALID_HANDLE_VALUE)
		{
			m_instance = new Barometer_MPL115A2(devHandle, ON_BOARD_BAROMETER_ADDRESS, I2C_SUBADDRESS_MODE_1, I2C_SLOWSPEED_MODE, 40);
			if(!m_instance->readCoeficients_())
			{
				//Couldnt read the coefficients from the device. The instance
				//cannot be used.
				delete m_instance;
				m_instance = NULL;
			}

			if(m_instance == NULL)
				//We couldnt create teh instance properly. Close the file handle.
				CloseHandle(devHandle);
		}
	}

	return m_instance;
}

//#############################################################################
///
/// Barometer_MPL115A2::Barometer_MPL115A2
/// Constructor.. Does nothing specific to the chip, only initializes the I2C 
/// baseclass.
///
//#############################################################################

Barometer_MPL115A2::Barometer_MPL115A2(HANDLE FileHandle, uint8_t BusAddress, I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout)
                                : I2C(FileHandle, BusAddress, SubAddrMode, BaudIndex, Timeout),
								Padc(0)
{
}

//#############################################################################
//
//  Barometer_MPL115A2::StartConversion - This performs an initiation of read of Pressure
//  and Temperature
//  Returns true if successful.
//
//#############################################################################

bool Barometer_MPL115A2::startConversion_ (void)
{
    bool ret = false;
//  Barometer_MPL115A2 conversion command + write (0)
    static const uint8_t START_CONVERSION_COMMAND = 0x12;

    if (I2C::WriteDevice(START_CONVERSION_COMMAND, START_CONVERSION_COMMAND) == I2C_SUCCESSFUL) 
		ret = true;

	return ret;
}

//#############################################################################
///Barometer_MPL115A2::combineBits_
///Utility method to extract the 2's complement float, given two bytes. This logic is
///specific to MPL115A2. The chip has a generic float format of an optional sign bit,
///followed by N integer bits, followed by M fraction bits. All floats are represented
///with just two bytes.
//#############################################################################
float Barometer_MPL115A2::combineBits_(uint8_t msb, uint8_t lsb, uint8_t numTotalBits, 
				   bool isSigned, uint8_t numIntBits, uint8_t fractionZeroPad)
{
	float ret = 0;

	uint16_t value = (msb << 8) | lsb;
	value = ExtractBits(value, numTotalBits, 16);

	uint8_t intStartBit = numTotalBits;
	uint8_t numFractionBits = numTotalBits-numIntBits;
	//Get sign adjustment
	int signAdjustment = 0;
	if(isSigned)
	{
		intStartBit--;
		numFractionBits--;
		if(value & CreateBitMask(1, numTotalBits))
			signAdjustment = (int)(-1*pow(2, numIntBits));
	}

	//Get integer
	uint16_t intPart = ExtractBits(value, numIntBits, intStartBit);
	uint16_t fractionPart = ExtractBits(value, numFractionBits, numFractionBits);

	float fraction = (float)((fractionPart)/pow(2, numFractionBits+fractionZeroPad));

	ret = signAdjustment + intPart + fraction;
	return ret;

}
///###################################################################################
/// Reads the conversion coefficients from MPL115A2 and saves to the member variables.
/// There are 4 float coefficiets to read. They are stored in fixed float format, with
/// varying number of integer and decimal bits for each coeffients.  The formats of those
/// fields are as follows:
///
/// a0 Signed, 12 IntBits, 3 FractionBits = S I11 I10 I9 I8 I7 I6 I5 I4 I3 I2 I1 I0 . F2 F1 F0
/// b1 Signed, 2 IntBits, 13 FractionBits = S I1 I0 . F12 F11 F10 F9 F8 F7 F6 F5 F4 F3 F2 F1 F0
/// b2 Signed, 1 IntBit, 14 FractionBits = S I0 . F13 F12 F11 F10 F9 F8 F7 F6 F5 F4 F3 F2 F1 F0
/// c12 Signed, 13 FractionBits, 9 dec pt zero pad bits 
///				= S 0 . 000 000 000 F12 F11 F10 F9 F8 F7 F6 F5 F4 F3 F2 F1 F0
/// NOTE: the format is 2's complement, NOT sign-magnitude.
///###################################################################################

bool Barometer_MPL115A2::readCoeficients_ ()
{
    bool ret = false;
	// Offset for A0_MSB.. all 4 coefficients (total 8 bytes) are stored on consecutive 
	// addresses starting from the A0 MSB
    static const uint8_t A0_MSB_ADDRESS = 0x04;
	uint8_t buffer[8];

	I2cReturnCode_t res = I2C::ReadDevice(A0_MSB_ADDRESS, buffer, 8);
    if ( res == I2C_SUCCESSFUL)
    {
		const bool isSigned = true;
		A0 = combineBits_(buffer[0], buffer[1], 16, isSigned, 12);
		B1 = combineBits_(buffer[2], buffer[3], 16, isSigned, 2);
		B2 = combineBits_(buffer[4], buffer[5], 16, isSigned, 1);
		C12 = combineBits_(buffer[6], buffer[7], 14, isSigned, 0, 9);
		ret = true;
    }

    return ret;
}
//#############################################################################
//
//  Barometer_MPL115A2::Read the ADC values for both pressure and temperature
//
//#############################################################################
bool Barometer_MPL115A2::readAdc_(uint8_t* buffer)
{
	bool ret = false;

	//First send the command to start data conversion. 
	if(startConversion_())
	{
		//The conversion requires a maximum of 3msec, based on 
		//MPL115A2 datasheet.
		Sleep(3);
		//Now read the ADC values
		static const uint8_t PRESSURE_MSB_ADDRESS = 0x00;
		ret = (I2C::ReadDevice(PRESSURE_MSB_ADDRESS, buffer, 4) == I2C_SUCCESSFUL);

	}

	return ret;
}
