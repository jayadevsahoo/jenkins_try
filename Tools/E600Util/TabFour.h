#pragma once

#include "TabOne.h"
#include "DerivedStatusBar.h"

// TabFour dialog

enum CalType_t
{
	CAL_NONE,
	CAL_AIR_PSOL,
	CAL_O2_PSOL,
	CAL_EXH_VALVE,
	CAL_EXH_FLOW_SENSOR,
	CAL_FLOW_SENSOR_OFFSET,
	CAL_PSOL_LIFTOFF,
	CAL_ALL,
	CIRCUIT_CHECK,
	NUM_CALS
};

enum TestType_t
{
	TEST_NONE,
	TEST_EXH_SENSOR_AZ,
	TEST_AIR_FLOW,
	TEST_O2_FLOW
};
	
enum CalStatus_t
{
	CAL_FAILED,
	CAL_PASSED,
	CAL_PENDING,
	CAL_PENDING2
};

class CTabFour : public CDialog
{
	DECLARE_DYNAMIC(CTabFour)

public:
	CTabFour(CWnd* pParent);   // standard constructor
	virtual ~CTabFour();

// Dialog Data
	enum { IDD = IDD_DIALOG_TAB4 };

	CTabOne *pTabOne;
	CDialog *pUtilDlg;
	CStatusBar *pStatusBar;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	void ProcessCalRequest(CalType_t RequestCalType);
	void ProcessTestRequest(TestType_t TestType);
	void ProcessCalCompletion(CalStatus_t Status);
	void GetButtonPointers();

	DECLARE_MESSAGE_MAP()

	CalType_t CalType;
	BOOL bCalInProgress;

public:
	afx_msg void OnBnClickedButtonAirPsolCal();
	afx_msg void OnBnClickedButtonO2PsolCal();
	afx_msg void OnBnClickedButtonExhDriveCal();
	afx_msg void OnBnClickedButtonExhFlowSensorCal();
	afx_msg void OnBnClickedButtonCircuitCheck();
	afx_msg void OnBnClickedButtonAllCals();

	afx_msg LRESULT ProcessCalStatus(WPARAM x, LPARAM y);
	afx_msg void OnBnClickedButtonCancelCal();
	afx_msg void OnBnClickedButtonFlowSensorOffsetCal();
	afx_msg void OnBnClickedButtonPsolLiftoffCal();
	afx_msg void OnBnClickedButtonExhSensorAz();
	afx_msg void OnBnClickedButtonAirFlow();
	afx_msg void OnBnClickedButtonO2Flow();
};
