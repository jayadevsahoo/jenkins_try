
#ifndef SettingXmitId_HH
#define SettingXmitId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SettingXmitId - Ids of all of the Settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingXmitId.hhv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: rhj    Date:  26-Jan-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//       PROX project-related changes.
// 
//  Revision: 010  By: gdc     Date: 03-Mar-2009    SCR Number: 6478
//  Project:  840S
//  Description:
//       Added missing HIGH_SPONT_INSP_TIME.
//       
//  Revision: 009  By: rhj     Date: 07-July-2008   SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 008  By: gdc     Date: 18-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 007  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new volume support level
//
//  Revision: 006   By: sah    Date: 20-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode project-related changes:
//      *  obsoleted patient type setting
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new BiLevel-specific settings
//
//  Revision: 004   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new ATC-specific settings
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: dosman Date:  08-Jan-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial version for BiLevel.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//            Initial version 
//  
//====================================================================

#include "Sigma.hh"
#include "SettingClassId.hh"
#include "SettingId.hh"

//@ Usage-Classes
//@ End-Usage


class SettingXmitId
{
	public:
		//@ Type:  SettingXmitIdType
		// Transmission IDs for all of the BD Settings.
		enum SettingXmitIdType
		{
			APNEA_FLOW_ACCEL_PERCENT      =  0,
			APNEA_INSP_PRESS          =  1,
			APNEA_INSP_TIME           =  2,
			APNEA_INTERVAL            =  3,
			APNEA_MIN_INSP_FLOW       =  4,
			APNEA_O2_PERCENT          =  5,
			APNEA_PEAK_INSP_FLOW      =  6,
			APNEA_PLATEAU_TIME        =  7,
			APNEA_RESP_RATE           =  8,
			APNEA_TIDAL_VOLUME        =  9,
			DISCO_SENS            = 38,
			EXP_SENS              = 10,
			FLOW_ACCEL_PERCENT        = 11,
			FLOW_SENS             = 12,
			HIGH_CCT_PRESS            = 13,
			HIGH_INSP_TIDAL_VOL       = 44,
			HUMID_VOLUME          = 40,
			IBW               = 14,
			INSP_PRESS            = 15,
			INSP_TIME             = 16,
			MIN_INSP_FLOW         = 17,
			OXYGEN_PERCENT            = 18,
			PEAK_INSP_FLOW            = 19,
			PERCENT_SUPPORT           = 41,
			PLATEAU_TIME          = 20,
			PEEP              = 21,
			PEEP_HIGH             = 39,
			PEEP_HIGH_TIME            = 45,
			PEEP_LOW              = 46,
			PRESS_SENS            = 22,
			PRESS_SUPP_LEVEL          = 23,
			RESP_RATE             = 24,
			TIDAL_VOLUME          = 25,
			TUBE_ID               = 42,
			APNEA_FLOW_PATTERN        = 26,
			APNEA_MAND_TYPE           = 27,
			FIO2_ENABLED          = 28,
			FLOW_PATTERN          = 29,
			HUMID_TYPE            = 30,
			MAND_TYPE             = 31,
			MODE              = 32,
			NOMINAL_VOLT          = 33,
			PATIENT_CCT_TYPE          = 34,
			VOLUME_SUPPORT            = 35,
			SUPPORT_TYPE          = 36,
			TRIGGER_TYPE          = 37,
			TUBE_TYPE             = 43,
			VENT_TYPE             = 47,

			// the above values are to remained "frozen" forever; new IDs are to
			// use values larger than the largest already used, while deleted IDs
			// are to remain reserved...

			LEAK_COMP_ENABLED   = 48,
			HIGH_SPONT_INSP_TIME      = 49,
			PROX_ENABLED              = 50,
			NUM_XMIT_ID_VALUES        = 51  
		};

		static void  Initialize(void);

		static inline SettingId::SettingIdType  ConvertToSettingId(
																  const SettingXmitId::SettingXmitIdType settingXmitId
																  );

		static void  SoftFault(const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL,
							   const char*       pPredicate = NULL);

	private:
		SettingXmitId(const SettingXmitId&);	// not implemented...
		SettingXmitId(void);			// not implemented...
		~SettingXmitId(void);			// not implemented...
		void  operator=(const SettingXmitId&);	// not implemented...

		//@ Data-Member:  XmitIdConversionTable_
		// Used for fast conversion from 'SettingXmitId' to 'SettingId'.
		static SettingId::SettingIdType  XmitIdConversionTable_[
															   SettingXmitId::NUM_XMIT_ID_VALUES
															   ];
};


// Inlined Methods...
#include "SettingXmitId.in"


#endif // SettingXmitId_HH 
