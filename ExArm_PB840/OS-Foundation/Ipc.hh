#ifndef Ipc_HH
#define Ipc_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  Ipc - Ipc constants and enumerated types.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/Ipc.hhv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================
#if defined(_WIN32_WCE)
#include <winbase.h>
#endif
#include "Sigma.hh"

//TODO E600 INFINITY is supposed to be from Windows but was redefined here
//because including winbase.h in PC project (Win32) causes compile failure.
#ifndef INFINITE
#define INFINITE            0xffffffffL
#endif

//@ Usage-Classes
//@ End-Usage

struct Ipc
{
    //@ Type: ReturnCode
    // Redefines some of the OS return codes.
    enum ReturnCode 
    {
        OK = 0     ,
        BAD_QID    ,//TODO E600 port to Windows equivalent
        BAD_TID    ,//TODO E600 port to Windows equivalent
        TIMED_OUT  ,
        Q_FULL     ,
        NO_MESSAGE ,//TODO E600 port to Windows equivalent
        NO_MEMORY  ,
    };

    //@ Type: IpcType
    //  IPC types  (form bit mask). Used for Sys-Init debug.
    enum IpcType
    {
        MSG_QUEUE = 0x00000001,
        MAILBOX   = 0x00000002,
        MUTEX     = 0x00000004,
        ALLIPC    = 0x0000000F
    };

    //@ Type: WaitOrder
    //  Defines a task's pend order.
    enum WaitOrder 
    {
        PRIORITY_ORDER, 
        FIFO_ORDER
    };

    //@ Constant: NO_TIMEOUT
    //  This constant is used to set an infinite timeout.
    enum
	{
		NO_TIMEOUT = INFINITE,
		NO_WAIT    = 0
	};
};

#endif // Ipc_HH 
