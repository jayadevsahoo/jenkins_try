#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Covidien
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 2010, Covidien Inc.
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: VsetServer - VSET server
//---------------------------------------------------------------------
//@ Interface-Description
//  This class encapsulates the methods that drive the VSET command
//  and status interface to an external setting's controller. 
//---------------------------------------------------------------------
//@ Rationale
//  Provides a class to encapsulate all processing for the VSET command.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/VsetServer.ccv   25.0.4.0   19 Nov 2013 14:12:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Initial implementation to support VentSet API.
//=====================================================================

#include "VsetServer.hh"

#include <ctype.h>

#include "AdjustPanel.hh"
#include "CriticalSection.hh"
#include "IpcIds.hh"
#include "LowerScreen.hh"
#include "LowerSubScreenArea.hh"
#include "MathUtilities.hh"
#include "MsgQueue.hh"
#include "OsTimeStamp.hh"
#include "SerialInterface.hh"
#include "Setting.hh"
#include "SettingContextHandle.hh"
#include "SoftwareOptions.hh"
#include "Task.hh"
#include "Task.hh"
#include "UserAnnunciationMsg.hh"
#include "VsetText.hh"

static VsetServer* PActiveServer_ = NULL;

#ifdef SIGMA_DEBUG
    #include <syslog.h>
    #define syslog(A) syslog A
    #define openlog(A) openlog A
#else
    #define syslog(A)
    #define openlog(A)
#endif

struct SettingCommandItem
{
    const char* settingName;
    SettingId::SettingIdType settingId;
};

// command table sorted in REVERSE order to match longer strings first
// e.g. PEEPHI should be compared before PEEP
static const SettingCommandItem SettingCommands_[] = 
{
    {"VTSUPP", SettingId::VOLUME_SUPPORT},
    {"VT", SettingId::TIDAL_VOLUME},
    {"VENTTYPE", SettingId::VENT_TYPE},
    {"TUBETYPE", SettingId::TUBE_TYPE},
    {"TUBEID", SettingId::TUBE_ID},
    {"TRIGTYPE", SettingId::TRIGGER_TYPE},
    {"TPL", SettingId::PLATEAU_TIME},
    {"TI", SettingId::INSP_TIME},
    {"TE", SettingId::EXP_TIME},
    {"SPONTTYPE", SettingId::SUPPORT_TYPE},
    {"RR", SettingId::RESP_RATE},
    {"RISETIME%", SettingId::FLOW_ACCEL_PERCENT},
    {"PSUPP", SettingId::PRESS_SUPP_LEVEL},
    {"PSENS", SettingId::PRESS_SENS},
    {"PI", SettingId::INSP_PRESS},
    {"PF", SettingId::PEAK_INSP_FLOW},
    {"PEEPLOWTIME", SettingId::PEEP_LOW_TIME},
    {"PEEPLOW", SettingId::PEEP_LOW},
    {"PEEPHIGHTIME", SettingId::PEEP_HIGH_TIME},
    {"PEEPHIGH", SettingId::PEEP_HIGH},
    {"PEEP", SettingId::PEEP},
    {"O2%", SettingId::OXYGEN_PERCENT},
    {"MODE", SettingId::MODE},
    {"MANDTYPE", SettingId::MAND_TYPE},
    {"LEAKCOMP", SettingId::LEAK_COMP_ENABLED},
    {"IERATIO", SettingId::IE_RATIO},
    {"IBW", SettingId::IBW},  // needs transition
    {"HUMIDVOL", SettingId::HUMID_VOLUME},
    {"HUMIDTYPE", SettingId::HUMID_TYPE},
    {"FLOWSENS", SettingId::FLOW_SENS},
    {"FLOWPAT", SettingId::FLOW_PATTERN},
    {"EXPSENS", SettingId::EXP_SENS},
    {"DISCSENS", SettingId::DISCO_SENS},
    {"CONSTPARM", SettingId::CONSTANT_PARM},
    {"APNVT", SettingId::APNEA_TIDAL_VOLUME},
    {"APNTI", SettingId::APNEA_INSP_TIME},
    {"APNTA", SettingId::APNEA_INTERVAL},
    {"APNRR", SettingId::APNEA_RESP_RATE},
    {"APNPI", SettingId::APNEA_INSP_PRESS},
    {"APNPF", SettingId::APNEA_PEAK_INSP_FLOW},
    {"APNO2%", SettingId::APNEA_O2_PERCENT},
    {"APNMANDTYPE", SettingId::APNEA_MAND_TYPE},
    {"APNFP", SettingId::APNEA_FLOW_PATTERN},
    {"%SUPP", SettingId::PERCENT_SUPPORT},
};

// commands implemented when using a development or manufacturing data key
// not implemented for API external customers
static const SettingCommandItem FactorySettingCommands_[] = 
{
    {"PTCIRCTYPE", SettingId::PATIENT_CCT_TYPE},
    {"O2SENSOR", SettingId::FIO2_ENABLED},
    {"NOMINALVOLT", SettingId::NOMINAL_VOLT},
    {"HIGHINSPVOLLIMIT", SettingId::HIGH_INSP_TIDAL_VOL},
    {"HCPLIMIT", SettingId::HIGH_CCT_PRESS},
    {"APNTPL", SettingId::APNEA_PLATEAU_TIME},
    {"APNRISETIME%", SettingId::APNEA_FLOW_ACCEL_PERCENT},
};

static Int32 InstanceCount_;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:   GetVsetServer
//
//@ Interface-Description
//  Constructs static VsetServer object and returns its reference.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

VsetServer& VsetServer::GetVsetServer(Uint32 portNum)
{
    static VsetServer VsetServer_[SerialInterface::MAX_NUM_SERIAL_PORTS];

    CLASS_PRE_CONDITION(0 <= portNum && portNum < countof(VsetServer_))

    return VsetServer_[portNum];
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  VsetServer [constructor]
//
//@ Interface-Description
//  Constructs the VsetServer object using the specified device name.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

VsetServer::VsetServer() : alertsEnabled_(TRUE)
{
    // null terminate the output buffer to clear output buffer 
    buf_[0] = '\0';
    portNum_ = InstanceCount_++;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  VsetServer [destructor]
//
//@ Interface-Description
//  Default destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VsetServer::~VsetServer( void )
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessBatch
//
//@ Interface-Description	
//  This is the public interface to the VsetServer to initiate a 
//  settings change. It is normally called from the GuiApp task
//  thread to process the VSET batch command that's been parsed and 
//  written to the command line array.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the processBatch() method of the currently "active" VsetServer.
//  Only one server can be active at a time as the PActiveServer_
//  is protected my mutex.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
VsetServer::ProcessBatch(void)
{
    if ( PActiveServer_ )
    {
        PActiveServer_->processBatch();
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Report(const char*)
//
//@ Interface-Description	
//  Writes the specified character string to the output buffer for 
//  later processing by the serial task associated with this VsetServer.
//  All of the Report methods write to the output buffer that is then
//  written to the serial port by the serial port task. This is done
//  to allow processing of settings at a higher priority and serial
//  output at a lower priority.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the report() method of the currently "active" VsetServer.
//  Only one server can be active at a time as the PActiveServer_
//  is protected my mutex.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void VsetServer::Report(const char* statusMsg)
{
    if (PActiveServer_)
    {
        PActiveServer_->report_(statusMsg);
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Report(BoundedValue, const char*)
//
//@ Interface-Description	
//  Writes the status of a setting change to the specified BoundedValue
//  along with the specified character string to the output buffer for 
//  later processing by the serial task associated with this VsetServer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All Report() methods write to the output buffer that is then
//  written to the serial port by the serial port task. This is done
//  to allow processing of settings at a higher priority and serial
//  output at a lower priority.
//  Calls the report() method of the currently "active" VsetServer.
//  Only one server can be active at a time as the PActiveServer_
//  is protected my mutex.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void VsetServer::Report(BoundedValue boundedValue, const char* boundMsg)
{
    char buf[80];
    sprintf(buf,"%.*f %s\n", DecimalPlaces(boundedValue.precision), boundedValue.value, (const char*)(VsetText(boundMsg)));
    if ( PActiveServer_ )
    {
        PActiveServer_->report_(buf);
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Report(const char*, Boolean, const char*)
//
//@ Interface-Description	
//  Writes the status of a DiscreteSetting change to the serial
//  output buffer. The DiscreteSetting class uses this interface to
//  report the status of a setting change request. The first 
//  parameter is the name of the adjusted value that the DiscreteSetting
//  has changed to after attempting to change to the API requested
//  value. The second parameter is a Boolean to indicate whether or not
//  the setting value requested by the API is valid. The third parameter
//  is a character string to indicate the status of the setting change
//  requested by the API. This string is reported back through the 
//  serial port to the API/PC.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All Report() methods write to the output buffer that is then
//  written to the serial port by the serial port task. This is done
//  to allow processing of settings at a higher priority and serial
//  output at a lower priority.
//  Calls the report() method of the currently "active" VsetServer.
//  Only one server can be active at a time as the PActiveServer_
//  is protected my mutex.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void VsetServer::Report(const char* value, Boolean isBoundViolated, const char* boundMsg)
{
    char buf[80];
    if (value == NULL)
    {
        sprintf(buf,"NULL %s\n", (const char*)(VsetText(boundMsg)));
    }
    else
    {
        sprintf(buf,"%s %s\n", value, (const char*)(VsetText(boundMsg)));
    }
    if ( PActiveServer_ )
    {
        PActiveServer_->report_(buf);
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: report(const char*)
//
//@ Interface-Description	
//  Copies the specified character string to the VsetServer's internal 
//  buffer for later transfer to the API/PC over the serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Concatenates the specified status message to the serial output 
//  buffer buf_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
VsetServer::report_(const char* status)
{
    strcat(buf_, status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processCmd(const char*)
//
//@ Interface-Description	
//  This method is designed to execute in a low priority serial task.
//  It processes the specified command line containing a VSET command.
//  It tokenizes the command line into the argv_ command line argument
//  array and sets the active VsetServer to itself  under mutex.
//  It then enqueues a UserAnnunciationMessage to the GuiApp task to
//  call VsetServer::ProcessBatch() to actually attempt the setting
//  change in the higher priority GuiApp thread. This method waits for
//  the ProcessBatch to complete and then writes the status of the 
//  VSET batch command to the serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See interface description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
int
VsetServer::processCmd(const char* cmdLine)
{
    output_();

    argc_ = 0;

    const char* pSrc = cmdLine;
    char* pDest = command_;
    for (int i=0; i<countof(command_)-1 && *pSrc; i++)
    {
        *pDest++ = toupper(*pSrc++);
    }
    *pDest = '\0';

    char echo[256];
    sprintf(echo,"%s\n",command_);
    report_(echo);
    output_();

    syslog((LOG_DEBUG, "%s", command_));

    char * pToken = strtok(command_, " ");

    while ( pToken && argc_ < countof(argv_) )
    {
        argv_[argc_++] = pToken;
        pToken = strtok(NULL, " ");
    }

    {
        // set up the VsetServer pointer under mutex and queue a message
        // to GuiApp to begin processing of the VSET command in its thread
        CriticalSection mutex(VSET_SERVER_MT);
        static OsTimeStamp FrameTime_;
        static Boolean IsTimerInitialized_ = FALSE;
        static const Uint32 VSET_FRAME_TIME_MS = 2000;  // 30 changes per minute

        Uint32 elapsedFrameTime = FrameTime_.getPassedTime();

        // this delay implements a minimum frame time so no more than one
        // VSET command is processed every 2.00 seconds, otherwise the API
		// could crash the GUI for lack of processing cycles. A full settings
		// transaction can be completed in about 1.6-1.8 seconds. We give the
		// processor 0.2 seconds to do other stuff.
        if ( IsTimerInitialized_ && elapsedFrameTime < VSET_FRAME_TIME_MS )
        {
            Task::Delay(0, VSET_FRAME_TIME_MS - elapsedFrameTime);
        }
        FrameTime_.now();
        IsTimerInitialized_ = TRUE;

        // now process the VSET command in the GuiApp thread
        batchStatus_ = -1;
        PActiveServer_ = this;
        UserAnnunciationMsg msg;
        msg.vsetCommandParts.eventType = UserAnnunciationMsg::VSET_COMMAND_EVENT; 
        MsgQueue::PutMsg( USER_ANNUNCIATION_Q, msg.qWord );
        // VsetServer::ProcessBatch(); executes in GuiApp thread - wait until it's done 
        OsTimeStamp waitTime;
        while ( batchStatus_ == -1 && waitTime.getPassedTime() < 10000 )
        {
            Task::Delay(0,50);
        }
        PActiveServer_ = NULL;
    }


    char buf[80];

    switch (batchStatus_)
    {
    case 0:
        sprintf(buf, "VSET BATCH OK\n", batchStatus_);
        break;
    case 255:
        sprintf(buf, "VSET BATCH Usage: VSET CMD [<setting-name> <setting-value>]\n");
        break;
    case 1:
        sprintf(buf, "VSET BATCH Vent startup not completed\n");
        break;
    case 2:
        sprintf(buf, "VSET BATCH Invalid setting name\n");
        break;
    case 3:
        sprintf(buf, "VSET BATCH Invalid setting value\n");
        break;
    default:
        sprintf(buf, "VSET BATCH Internal error\n");
        break;
    }
    report_(buf);
    output_();

    return batchStatus_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processBatch
//
//@ Interface-Description	
//  Processes the command line arguments creating a batch of settings
//  changes that are passed to each setting for processing against the
//  current set of constraints for each setting argument. This method
//  is designed to execute in the higher priority GuiApp thread.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Method
//=====================================================================

void
VsetServer::processBatch(void)
{
    // reject not enough arguments or an odd number of args
    if ( argc_ < 4 || (argc_ % 2) == 1 || strcmp("CMD", argv_[1]) )
    {
        batchStatus_ = 255;
        return;
    }

    if ( !SettingContextHandle::HasVentStartupCompleted() )
    {
        batchStatus_ = 1;
        return;
    }

#if 0
    // this logic doesn't work due to an incomplete or inadequate design 
    // that doesn't accurately maintain the 'changed' state of every setting 
    // it is left here as a comment in case this design or implementation can 
    // be corrected allowing the vent to disable external control during a 
    // GUI initiated settings change 
    if ( SettingContextHandle::AreAnyBatchSettingsChanged() )
    {
        extern Boolean AdjContextDebug;
        AdjContextDebug = TRUE;
        SettingContextHandle::AreAnyBatchSettingsChanged();
        AdjContextDebug = FALSE;

        // should we reject the command from the serial port or just proceed? 
        soundAlert_(Sound::INVALID_ENTRY);
        printf("external control disabled - operator controlled settings change in progress!!\n");
        return 2;
    }
#endif 

    AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);
    LowerSubScreenArea *pLowerSubScreenArea = LowerScreen::RLowerScreen.getLowerSubScreenArea();
    pLowerSubScreenArea->deactivateSubScreen();

    SettingContextHandle::AdjustVentSetupBatch(MAIN_CONTROL_ADJUST_PHASE);
    SettingContextHandle::AdjustVentSetupBatch(BREATH_SETTINGS_ADJUST_PHASE);

    // process each setting change on command line
    for ( int argidx=2; argidx<argc_; )
    {
        SigmaStatus cmdStatus = FAILURE;
        const char* pArg = argv_[argidx++];

        // check the meta settings
        if ( !strcmp("SOUND", pArg) )
        {
            if ( !strncmp("OFF", argv_[argidx++], 3 ) )
            {
                alertsEnabled_ = FALSE;
                report_("VSET STAT SOUND OFF OK\n");
            }
            else
            {
                alertsEnabled_ = TRUE;
                report_("VSET STAT SOUND ON OK\n");
            }
            continue;
        }

        const SettingCommandItem* pLookup = SettingCommands_;
		Int i=0;
        for ( i=0; i<countof(SettingCommands_); pLookup++, i++ )
        {
            if ( !strncmp(pLookup->settingName, pArg, strlen(pLookup->settingName) ) )
            {
                cmdStatus = changeSetting_(pLookup->settingId, pLookup->settingName, argv_[argidx++]);
                break;
            }
        }
        Boolean standardSettingFound = (i < countof(SettingCommands_));
        Boolean factorySettingFound = FALSE;

        if ( !standardSettingFound && SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions() )
        {
            pLookup = FactorySettingCommands_;
            for ( int i=0; i<countof(FactorySettingCommands_); pLookup++, i++ )
            {
                if ( !strncmp(pLookup->settingName, pArg, strlen(pLookup->settingName) ) )
                {
                    cmdStatus = changeSetting_(pLookup->settingId, pLookup->settingName, argv_[argidx++]);
                    break;
                }
            }
            factorySettingFound = (i < countof(FactorySettingCommands_));
        }

        if ( !standardSettingFound && !factorySettingFound )
        {
            SettingContextHandle::AdjustVentSetupBatch(MAIN_CONTROL_ADJUST_PHASE);
            soundAlert_(Sound::INVALID_ENTRY);
            batchStatus_ = 2;
            return;
        }
        if ( cmdStatus == FAILURE )
        {
            // clear the current changes 
            SettingContextHandle::AdjustVentSetupBatch(MAIN_CONTROL_ADJUST_PHASE);
            soundAlert_(Sound::INVALID_ENTRY);
            batchStatus_ = 3;                            
            return;
        }
    }

    // batch OK - accept changes
    SettingContextHandle::AcceptVentSetupBatch();
    soundAlert_(Sound::ACCEPT);
    SettingContextHandle::AdjustVentSetupBatch(MAIN_CONTROL_ADJUST_PHASE);
    batchStatus_ = 0;
    return;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: changeSetting
//
//@ Interface-Description	
//  Changes the setting specified by settingId to the value specified
//  by valueArg.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Method
//=====================================================================
SigmaStatus
VsetServer::changeSetting_(const SettingId::SettingIdType settingId, const char* settingName, const char* valueArg)
{
    SigmaStatus retcode = FAILURE;

    Setting*  pSetting  = SettingsMgr::GetSettingPtr(settingId);

    const Notification::ChangeQualifier  QUALIFIER_ID = 
    SettingId::IsBatchId(pSetting->getId()) ? Notification::ADJUSTED : Notification::ACCEPTED;

    report_("VSET STAT ");
    report_(settingName);
    report_(" ");

    if ( pSetting->getApplicability(QUALIFIER_ID) != Applicability::CHANGEABLE )
    {
        report_("-1 Setting not applicable\n");
    }
    else
    {
        // pSettinig->ventset supplies the setting change status using Report_()
        retcode = pSetting->ventset(valueArg);
    }

    return retcode;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: soundAlert_
//
//@ Interface-Description	
//  Activates the specified "sound" through the sound sub-system if
//  alerts are enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Method
//=====================================================================
void VsetServer::soundAlert_(Sound::SoundId id) const
{
    if ( alertsEnabled_ )
    {
        Sound::Start(id);
    }
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  output_
//
//@ Interface-Description
//  Write the data in the output buffer to the serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the SerialInterface::Write method which outputs the data
//  over the serial port and waits up to the specified timeout of
//  15 seconds for the data to be transmitted. Upon return, the data
//  has been transmitted or a timeout has occured and the output
//  discarded.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void VsetServer::output_(void)
{
    if (buf_[0] != '\0')
    {
        // report is discarded after 15 second timeout
        SerialInterface::Write(buf_, strlen(buf_), 15000, SerialInterface::PortNum(portNum_)); 
        buf_[0] = '\0';
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//  Implicitly gain execution control when a software assertion macro
//  detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
//  macros (among others) detect a fault.  We just call FaultHandler::SoftFault
//  to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void VsetServer::SoftFault(  const SoftFaultID softFaultID
                             , const Uint32      lineNumber
                             , const char *      pFileName
                             , const char *      pPredicate )
{
    FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE,
                            GUI_Serial_Interface::VSET_SERVER_CLASS,
                            lineNumber, pFileName, pPredicate);
}

