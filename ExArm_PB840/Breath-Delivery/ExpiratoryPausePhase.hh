#ifndef ExpiratoryPausePhase_HH
#define ExpiratoryPausePhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ExpiratoryPausePhase -  Implements an Expiratory Pause Phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExpiratoryPausePhase.hhv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: syw   Date: 16-Aug-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added method updateFlowControllerFlags_().
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "BreathPhase.hh"

//@ End-Usage


class ExpiratoryPausePhase : public BreathPhase {
  public:
    ExpiratoryPausePhase( void) ;
	virtual ~ExpiratoryPausePhase( void) ;

    virtual void newBreath( void) ;
    virtual void relinquishControl( const BreathTrigger& trigger) ;
    virtual void newCycle( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

  protected:

	virtual void updateFlowControllerFlags_( void) ;

  private:
    ExpiratoryPausePhase( const ExpiratoryPausePhase&) ;	// not implemented...
    void   operator=( const ExpiratoryPausePhase&) ;		// not implemented...

	//@ Data-Member: highCircuitPressure_
	// high circuit pressure setting value
	Real32 highCircuitPressure_ ;

};


#endif // ExpiratoryPausePhase_HH 
