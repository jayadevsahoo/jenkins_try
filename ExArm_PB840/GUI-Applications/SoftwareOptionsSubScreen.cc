#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2008, Covidien
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SoftwareOptionsSubScreen - Subscreen which allows users to 
//  deactivate or activate different software options.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is derived from subscreen class and contains buttons 
//  for setting all software option bits (Set All), clearing all
//  software option bits (Clear All), reset all software option bits
//  (Use Datakey option) and activating or deactivating individual 
//  software option.
//---------------------------------------------------------------------
//@ Rationale
//  This class is dedicated to managing software options.
//---------------------------------------------------------------------
//@ Implementation-Description
//  For software options, service mode manager is notified to set option 
//  bit to TRUE if option button is enabled or FALSE if option button is disabled.  
//---------------------------------------------------------------------
//@ Fault-Handling
//  None.
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SoftwareOptionsSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  mnr    Date:  28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Implemented NeoMode Advanced and Lockout options.
//
//  Revision: 003  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 002  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 001    By: rhj    Date: 20-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//		Initial version.
//
//=====================================================================

#include "SoftwareOptionsSubScreen.hh"

//@ Usage-Classes
#include "Image.hh"
#include "SoftwareOptions.hh"
#include "UpperSubScreenArea.hh"
#include "SmTestId.hh"
#include "SmManager.hh"
#include "TabButton.hh"
#include "DevelopmentOptionsSubScreen.hh"
#include "ServiceUpperScreen.hh"
#include "Task.hh"
#include "Sound.hh"
//@ End-Usage

//@ Code...


static const Int32 OPTION_RESET_BUTTON_X_ = 535;    
static const Int32 OPTION_RESET_BUTTON_Y_ = 70;    
static const Int32 OPTION_RESET_BUTTON_WIDTH_ = 90; 
static const Int32 OPTION_RESET_BUTTON_HEIGHT_ = 40;   
static const Int32 OPTION_RESET_BUTTON_BORDER_ = 3;


static const Int32 SET_ALL_BUTTON_X_ = 535;    
static const Int32 SET_ALL_BUTTON_Y_ = 120;    
static const Int32 SET_ALL_BUTTON_WIDTH_ = 90; 
static const Int32 SET_ALL_BUTTON_HEIGHT_ = 40;   
static const Int32 SET_ALL_BUTTON_BORDER_ = 3;


static const Int32 CLEAR_ALL_BUTTON_X_ = 535;    
static const Int32 CLEAR_ALL_BUTTON_Y_ = 170;    
static const Int32 CLEAR_ALL_BUTTON_WIDTH_ = 90; 
static const Int32 CLEAR_ALL_BUTTON_HEIGHT_ = 40;   
static const Int32 CLEAR_ALL_BUTTON_BORDER_ = 3;


static const Int32 BACK_BUTTON_X_ = 535;    
static const Int32 BACK_BUTTON_Y_ = 220;    
static const Int32 BACK_BUTTON_WIDTH_ = 90; 
static const Int32 BACK_BUTTON_HEIGHT_ = 40;   
static const Int32 BACK_BUTTON_BORDER_ = 3;


static const Int  SOFTWARE_OPTIONS_Y_  = 20;
static const Int  SOFTWARE_OPTIONS_X_  = 15;
static const Int  SOFTWARE_OPTIONS_BUTTON_OFFSET_X_=100;
static const Int  SOFTWARE_OPTIONS_BUTTON_OFFSET_Y_=30;

static const Int  SOFTWARE_OPTIONS_BUTTON_WIDTH_=40;
static const Int  SOFTWARE_OPTIONS_BUTTON_HEIGHT_=20;
static const Int  SOFTWARE_OPTIONS_BUTTON_BORDER_=2;

static const Int  SOFTWARE_OPTION_BUTTON_BITMAP_XPOS_=9;
static const Int  SOFTWARE_OPTION_BUTTON_BITMAP_YPOS_=0;
static const Int  MAX_NUM_ROWS = 7;

struct OptionInfo_
{
	TextField*  pLabel;
	Button*     pButton;
	Bitmap*     pBitmap;
};

static OptionInfo_  ArrOptionInfo_[SoftwareOptions::NUM_OPTIONS];

static const SmTestId::FunctionId  OptionIdToSmTestIdTable_[2]
	[SoftwareOptions::NUM_OPTIONS] =
{
	{	// 'FALSE' index...
		SmTestId::DISABLE_BILEVEL_OPTION
		, SmTestId::DISABLE_ATC_OPTION
		, SmTestId::DISABLE_NEO_MODE_OPTION
		, SmTestId::DISABLE_RESERVED1_OPTION
		, SmTestId::DISABLE_VTPC_OPTION
		, SmTestId::DISABLE_PAV_OPTION
		, SmTestId::DISABLE_RESP_MECH_OPTION
		, SmTestId::DISABLE_TRENDING_OPTION
		, SmTestId::DISABLE_LEAK_COMP_OPTION
		, SmTestId::DISABLE_NEO_MODE_UPDATE_OPTION
		, SmTestId::DISABLE_NEO_MODE_ADVANCED_OPTION
		, SmTestId::DISABLE_NEO_MODE_LOCKOUT_OPTION
	},

	{	// 'TRUE' index...
		SmTestId::ENABLE_BILEVEL_OPTION
		, SmTestId::ENABLE_ATC_OPTION
		, SmTestId::ENABLE_NEO_MODE_OPTION
		, SmTestId::ENABLE_RESERVED1_OPTION
		, SmTestId::ENABLE_VTPC_OPTION
		, SmTestId::ENABLE_PAV_OPTION
		, SmTestId::ENABLE_RESP_MECH_OPTION
		, SmTestId::ENABLE_TRENDING_OPTION
		, SmTestId::ENABLE_LEAK_COMP_OPTION
		, SmTestId::ENABLE_NEO_MODE_UPDATE_OPTION
		, SmTestId::ENABLE_NEO_MODE_ADVANCED_OPTION
		, SmTestId::ENABLE_NEO_MODE_LOCKOUT_OPTION
	}
};



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SoftwareOptionsSubScreen()  [Constructor]
//
//@ Interface-Description
//	Constructs a SoftwareOptionsSubScreen.  It is passed a pointer to the 
//	SubScreenArea.	
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Initializes data members; sets position, size and color of the upper
//	subscreen area; 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SoftwareOptionsSubScreen::SoftwareOptionsSubScreen(SubScreenArea* pSubScreenArea) :
SubScreen(pSubScreenArea),
	titleArea_(MiscStrs::SOFTWARE_OPTIONS_SUBSCREEN_TITLE, SubScreenTitleArea::SSTA_CENTER),

	optionsResetButton_(OPTION_RESET_BUTTON_X_, OPTION_RESET_BUTTON_Y_,
						OPTION_RESET_BUTTON_WIDTH_, OPTION_RESET_BUTTON_HEIGHT_,
						Button::LIGHT, OPTION_RESET_BUTTON_BORDER_, Button::SQUARE,
						Button::NO_BORDER, MiscStrs::USE_DATAKEY_BUTTON_TITLE),

	setAllButton_(SET_ALL_BUTTON_X_, SET_ALL_BUTTON_Y_,
				  SET_ALL_BUTTON_WIDTH_, SET_ALL_BUTTON_HEIGHT_,
				  Button::LIGHT, SET_ALL_BUTTON_BORDER_, Button::SQUARE,
				  Button::NO_BORDER, MiscStrs::SET_ALL_BUTTON_TITLE),


	clearAllButton_(CLEAR_ALL_BUTTON_X_, CLEAR_ALL_BUTTON_Y_,
					CLEAR_ALL_BUTTON_WIDTH_, CLEAR_ALL_BUTTON_HEIGHT_,
					Button::LIGHT, CLEAR_ALL_BUTTON_BORDER_, Button::SQUARE,
					Button::NO_BORDER, MiscStrs::CLEAR_ALL_BUTTON_TITLE),


	backButton_(BACK_BUTTON_X_, BACK_BUTTON_Y_,
				BACK_BUTTON_WIDTH_, BACK_BUTTON_HEIGHT_,
				Button::LIGHT, BACK_BUTTON_BORDER_, Button::SQUARE,
				Button::NO_BORDER, MiscStrs::BACK_BUTTON_TITLE),

	softwareOptionBiLevelLabel_(MiscStrs::SOFTWARE_OPTION_BILEVEL_LABEL),
	softwareOptionNeoModeLabel_(MiscStrs::SOFTWARE_OPTION_NEOMODE_LABEL),
	softwareOptionTCLabel_(MiscStrs::SOFTWARE_OPTION_TC_LABEL),
	softwareOptionVtpcLabel_(MiscStrs::SOFTWARE_OPTION_VTPC_LABEL),
	softwareOptionPavLabel_(MiscStrs::SOFTWARE_OPTION_PAV_LABEL),
	softwareOptionRespMechLabel_(MiscStrs::SOFTWARE_OPTION_RESP_MECH_LABEL),
	softwareOptionTrendingLabel_(MiscStrs::SOFTWARE_OPTION_TRENDING_LABEL),
	softwareOptionLeakCompLabel_(MiscStrs::SOFTWARE_OPTION_LEAK_COMP_LABEL),
	softwareOptionNeoModeUpdateLabel_(MiscStrs::SOFTWARE_OPTION_NEOMODE_UPDATE_LABEL),
	softwareOptionNeoModeAdvancedLabel_(MiscStrs::SOFTWARE_OPTION_NEOMODE_ADVANCED_LABEL),
	softwareOptionNeoModeLockoutLabel_(MiscStrs::SOFTWARE_OPTION_NEOMODE_LOCKOUT_LABEL),

	optionBiLevelButton_( SOFTWARE_OPTIONS_X_, 0,
						  SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
						  Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER),

	optionTCButton_( SOFTWARE_OPTIONS_X_, 0,
					 SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
					 Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER),

	optionNeoModeButton_( SOFTWARE_OPTIONS_X_ , 0,
						  SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
						  Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER),

	optionVtpcButton_( SOFTWARE_OPTIONS_X_ ,  0,
					   SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
					   Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND,
					   Button::NO_BORDER),

	optionPavButton_( SOFTWARE_OPTIONS_X_ , 0,
					  SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
					  Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER),

	optionRespMechButton_( SOFTWARE_OPTIONS_X_ , 0,
						   SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
						   Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER),

	optionTrendingButton_( SOFTWARE_OPTIONS_X_ ,  0,
						   SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
						   Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER),

	optionLeakCompButton_( SOFTWARE_OPTIONS_X_ ,  0,
						   SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
						   Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER),

	optionNeoModeUpdateButton_( SOFTWARE_OPTIONS_X_ , 0,
							    SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
						        Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER),

	optionNeoModeAdvancedButton_( SOFTWARE_OPTIONS_X_ , 0,
							    SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
						        Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER),


	optionNeoModeLockoutButton_( SOFTWARE_OPTIONS_X_ , 0,
							    SOFTWARE_OPTIONS_BUTTON_WIDTH_, SOFTWARE_OPTIONS_BUTTON_HEIGHT_,
						        Button::LIGHT, SOFTWARE_OPTIONS_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER),

	pCurrentButton_(NULL),

	optionBiLevelEnabledBitmap_(Image::RCheckMarkIcon),
	optionNeoModeEnabledBitmap_(Image::RCheckMarkIcon),
	optionTCEnabledBitmap_(Image::RCheckMarkIcon),
	optionVtpcEnabledBitmap_(Image::RCheckMarkIcon),
	optionPavEnabledBitmap_(Image::RCheckMarkIcon),
	optionRespMechEnabledBitmap_(Image::RCheckMarkIcon),
	optionTrendingEnabledBitmap_(Image::RCheckMarkIcon),
	optionLeakCompEnabledBitmap_(Image::RCheckMarkIcon),
	optionNeoModeUpdateEnabledBitmap_(Image::RCheckMarkIcon),
	optionNeoModeAdvancedEnabledBitmap_(Image::RCheckMarkIcon),
	optionNeoModeLockoutEnabledBitmap_(Image::RCheckMarkIcon)
{
	CALL_TRACE("SoftwareOptionsSubScreen::SoftwareOptionsSubScreen(pSubScreenArea)");

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);


	// Display the screen title
	addDrawable(&titleArea_);

	Uint8 row = 0;
	Uint8 column = 0;
	Uint8 missingOption = 0;

	for (Uint optionIdx = 0u; optionIdx < SoftwareOptions::NUM_OPTIONS; optionIdx++)
	{
		OptionInfo_ *const  POptionInfo = (::ArrOptionInfo_ + optionIdx);

		switch (optionIdx)
		{
		case SoftwareOptions::BILEVEL :				// $[TI1.1]
			POptionInfo->pLabel  = &softwareOptionBiLevelLabel_;
			POptionInfo->pButton = &optionBiLevelButton_;
			POptionInfo->pBitmap = &optionBiLevelEnabledBitmap_;
			break;

		case SoftwareOptions::ATC :					// $[TI1.3]
			POptionInfo->pLabel  = &softwareOptionTCLabel_;
			POptionInfo->pButton = &optionTCButton_;
			POptionInfo->pBitmap = &optionTCEnabledBitmap_;
			break;

		case SoftwareOptions::NEO_MODE :			// $[TI1.2]
			POptionInfo->pLabel  = &softwareOptionNeoModeLabel_;
			POptionInfo->pButton = &optionNeoModeButton_;
			POptionInfo->pBitmap = &optionNeoModeEnabledBitmap_;
			break;

		case SoftwareOptions::VTPC :				// $[TI1.6]
			POptionInfo->pLabel  = &softwareOptionVtpcLabel_;
			POptionInfo->pButton = &optionVtpcButton_;
			POptionInfo->pBitmap = &optionVtpcEnabledBitmap_;
			break;

		case SoftwareOptions::PAV :					// $[TI1.7]
			POptionInfo->pLabel  = &softwareOptionPavLabel_;
			POptionInfo->pButton = &optionPavButton_;
			POptionInfo->pBitmap = &optionPavEnabledBitmap_;
			break;

		case SoftwareOptions::RESP_MECH :
			POptionInfo->pLabel  = &softwareOptionRespMechLabel_;
			POptionInfo->pButton = &optionRespMechButton_;
			POptionInfo->pBitmap = &optionRespMechEnabledBitmap_;
			break;

		case SoftwareOptions::TRENDING :
			POptionInfo->pLabel  = &softwareOptionTrendingLabel_;
			POptionInfo->pButton = &optionTrendingButton_;
			POptionInfo->pBitmap = &optionTrendingEnabledBitmap_;
			break;

		case SoftwareOptions::LEAK_COMP :
			POptionInfo->pLabel  = &softwareOptionLeakCompLabel_;
			POptionInfo->pButton = &optionLeakCompButton_;
			POptionInfo->pBitmap = &optionLeakCompEnabledBitmap_;
			break;
		case SoftwareOptions::NEO_MODE_UPDATE :
			POptionInfo->pLabel  = &softwareOptionNeoModeUpdateLabel_;
			POptionInfo->pButton = &optionNeoModeUpdateButton_;
			POptionInfo->pBitmap = &optionNeoModeUpdateEnabledBitmap_;
			break;

		case SoftwareOptions::NEO_MODE_ADVANCED :
			POptionInfo->pLabel  = &softwareOptionNeoModeAdvancedLabel_;
			POptionInfo->pButton = &optionNeoModeAdvancedButton_;
			POptionInfo->pBitmap = &optionNeoModeAdvancedEnabledBitmap_;
			break;

		case SoftwareOptions::NEO_MODE_LOCKOUT :
			POptionInfo->pLabel  = &softwareOptionNeoModeLockoutLabel_;
			POptionInfo->pButton = &optionNeoModeLockoutButton_;
			POptionInfo->pBitmap = &optionNeoModeLockoutEnabledBitmap_;
			break;

		default :
			// unavailable option...
			POptionInfo->pLabel  = NULL;
			POptionInfo->pButton = NULL;
			POptionInfo->pBitmap = NULL;
			break;
		}


		if (POptionInfo->pButton != NULL)
		{

			// determine the row number and column number.
			row = (optionIdx - missingOption) % MAX_NUM_ROWS ;
			if (((optionIdx - missingOption)) % MAX_NUM_ROWS  == 0)
			{
				column = (optionIdx- missingOption) / MAX_NUM_ROWS ;
			}

			// calculate x and y cordinates for each option
			Uint32 X_VALUE = (SOFTWARE_OPTIONS_X_ +  
							  ( ( column * (175)) ));

			Uint32 Y_VALUE = (SOFTWARE_OPTIONS_Y_ +
							  ((row + 1) * SOFTWARE_OPTIONS_BUTTON_OFFSET_Y_));

			// Set color, place and add field labels of this sofware option
			// label.
			POptionInfo->pLabel->setColor(Colors::WHITE);
			POptionInfo->pLabel->setX(X_VALUE);
			POptionInfo->pLabel->setY(Y_VALUE);
			addDrawable(POptionInfo->pLabel);

			// initialize each option button...
			// monitoring its presses...
			POptionInfo->pButton->setX(X_VALUE + SOFTWARE_OPTIONS_BUTTON_OFFSET_X_);
			POptionInfo->pButton->setY(Y_VALUE - 6);
			addDrawable(POptionInfo->pButton);
			POptionInfo->pButton->setButtonCallback(this);

			// Position the bitmap.
			POptionInfo->pBitmap->setX(SOFTWARE_OPTION_BUTTON_BITMAP_XPOS_);
			POptionInfo->pBitmap->setY(SOFTWARE_OPTION_BUTTON_BITMAP_YPOS_);
		}	// $[TI1.5]
		else
		{
			missingOption++;
		}

	}


	// Set the Button's callback routine.
	backButton_.setButtonCallback(this);
	addDrawable(&backButton_);

	optionsResetButton_.setButtonCallback(this);
	addDrawable(&optionsResetButton_);

	clearAllButton_.setButtonCallback(this);
	addDrawable(&clearAllButton_);


	setAllButton_.setButtonCallback(this);
	addDrawable(&setAllButton_);




}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SoftwareOptionsSubScreen  [Destructor]
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SoftwareOptionsSubScreen::~SoftwareOptionsSubScreen(void)	
{
	CALL_TRACE("SoftwareOptionsSubScreen::~SoftwareOptionsSubScreen(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()		[virtual]
//
//@ Interface-Description
// 	Activate the SoftwareOptionsSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	SoftwareOptionsSubScreen::activate(void)
{
	CALL_TRACE("SoftwareOptionsSubScreen::activate(void)");


	for (Uint optionIdx = 0u; optionIdx < SoftwareOptions::NUM_OPTIONS;
		optionIdx++)
	{	// $[TI1] -- this path is ALWAYS taken...
		OptionInfo_ *const  POptionInfo = (::ArrOptionInfo_ + optionIdx);

		if (POptionInfo->pButton != NULL)
		{
			const SoftwareOptions::OptionId  OPTION_ID =
				(SoftwareOptions::OptionId)optionIdx;

			if (SoftwareOptions::IsOptionEnabled(OPTION_ID))
			{
				POptionInfo->pButton->setToDown();
				POptionInfo->pButton->addLabel(POptionInfo->pBitmap);
			}
			else
			{
				POptionInfo->pButton->setToUp();
				POptionInfo->pButton->removeLabel(POptionInfo->pBitmap);
			}

			if (SoftwareOptions::GetSoftwareOptions().getDataKeyType() !=
				SoftwareOptions::ENGINEERING)
			{
				// only activate these option-set buttons, when using an
				// ENGINEERING data key...
				POptionInfo->pButton->setToFlat();
			}
		}
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()	[virtual]
//
//@ Interface-Description
// Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	SoftwareOptionsSubScreen::deactivate(void)
{
	CALL_TRACE("SoftwareOptionsSubScreen::deactivate(void)");

	if (pCurrentButton_ == &setAllButton_ ||
		pCurrentButton_ == &clearAllButton_ ||
		pCurrentButton_ == &optionsResetButton_)
	{
		// Pop up the button.
		pCurrentButton_->setToUp();
	}

	// Clear prompts
	clearPrompts_();

	pCurrentButton_ = NULL;
}   



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when any button in SoftwareOptionsSubScreen is pressed down.  
//---------------------------------------------------------------------
//@ Implementation-Description
// Check the button pressed.  For options button, enable option bit by
// notifying service mode manager.  For back button, deactivate 
// this subscreen and activate development options subscreen.
// For Set all, clear all, and options reset button 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	SoftwareOptionsSubScreen::buttonDownHappened(Button *pButton,
												 Boolean byOperatorAction)
{
	if (!byOperatorAction && (pButton != NULL))
	{
		// only process this callback if action is initiated by user...
		return;
	}

	// If the user pressed an option button, send enable flag to 
	// Service Mode Manager to process enable the option bit.
	for (Uint optionIdx = 0u; optionIdx < SoftwareOptions::NUM_OPTIONS;
		optionIdx++)
	{	// $[TI1] -- this path is ALWAYS taken...
		OptionInfo_ *const  POptionInfo = (::ArrOptionInfo_ + optionIdx);

		if (POptionInfo->pButton == pButton)
		{
			const SoftwareOptions::OptionId  OPTION_ID =
				(SoftwareOptions::OptionId)optionIdx;

			// enable (TRUE) copy of option bit stored on both CPUs...
			SmManager::DoFunction(::OptionIdToSmTestIdTable_[TRUE][OPTION_ID]);

			POptionInfo->pButton->setToDown();
			POptionInfo->pButton->addLabel(POptionInfo->pBitmap);

			// return from this method...
			return;
		}
	}

	// If the user pressed the "Standard" button,
	// deactivate this subscreen and activate the development
	// options subscreen.
	if (pButton == &backButton_)
	{

		DevelopmentOptionsSubScreen::setLastDisplay(DEVELOPMENTOPTIONSSUBSCREEN);

		// Bounce the back button.
		backButton_.setToUp();

		// Deactivate/dismiss this current subscreen
		getSubScreenArea()->deactivateSubScreen();

		// Fetch the previous screen, the development options subscreen.
		SubScreen* pSubscreen = UpperSubScreenArea::GetDevelopmentOptionsSubScreen();
		CLASS_ASSERTION(pSubscreen != NULL);

		//
		// Fetch the development option tab button.
		//
		TabButton * pTabButton = ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->
								 getDevelopmentOptionTabButton();
		CLASS_ASSERTION(pTabButton != NULL);


		SAFE_CLASS_ASSERTION(pSubscreen && pTabButton);

		// Activate the subscreen and force down the associated tab 
		// button. 
		getSubScreenArea()->activateSubScreen(pSubscreen, pTabButton);
	}

	// If the user pressed, set all, clear all or options reset button,
	// display to the user a prompt message to press the accept button.
	else if ( (pButton == &setAllButton_)   ||
			  (pButton == &clearAllButton_) ||
			  (pButton == &optionsResetButton_)
			)
	{
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);


		//
		// Set all prompts 
		//
		setTestPrompt(PromptArea::PA_PRIMARY, 
					  PromptArea::PA_HIGH,
					  PromptStrs::SM_KEY_ACCEPT_P);
		setTestPrompt(PromptArea::PA_ADVISORY, 
					  PromptArea::PA_HIGH,
					  NULL_STRING_ID);
		setTestPrompt(PromptArea::PA_ADVISORY, 
					  PromptArea::PA_LOW,
					  NULL_STRING_ID);
		setTestPrompt(PromptArea::PA_SECONDARY,
					  PromptArea::PA_HIGH,
					  PromptStrs::DEVELOP_OPTIONS_SCREENS_CANCEL_S);

		// pop up the previous button which was
		// pressed down
		if (pButton != pCurrentButton_ &&
			pCurrentButton_ != NULL)
		{
			pCurrentButton_->setToUp();
		}
		pCurrentButton_ = pButton;

	}



}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// Called when the Clear key is pressed (and we're the current
// Adjust Panel focus). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the current button to up, and clears the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SoftwareOptionsSubScreen::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("adjustPanelClearPressHappened(void)");

	if (pCurrentButton_ != NULL)
	{
		pCurrentButton_->setToUp();
		clearPrompts_();
	}
	pCurrentButton_ = NULL;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
//  Called when any button in SoftwareOptionsSubScreen is pressed up.
//---------------------------------------------------------------------
//@ Implementation-Description
//  For options, set option bit to FALSE.  For set all, clear all and
//  options reset, clear the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	SoftwareOptionsSubScreen::buttonUpHappened(Button *pButton,
											   Boolean byOperatorAction)
{
	if (!byOperatorAction)
	{
		// only process this callback if action is initiated by user...
		return;
	}

	for (Uint optionIdx = 0u; optionIdx < SoftwareOptions::NUM_OPTIONS; optionIdx++)
	{	// $[TI1] -- this path is ALWAYS taken...
		OptionInfo_ *const  POptionInfo = (::ArrOptionInfo_ + optionIdx);

		if (POptionInfo->pButton == pButton)
		{
			const SoftwareOptions::OptionId  OPTION_ID =
				(SoftwareOptions::OptionId)optionIdx;

			// disable (FALSE) copy of option bit stored on both CPUs...
			SmManager::DoFunction(::OptionIdToSmTestIdTable_[FALSE][OPTION_ID]);

			POptionInfo->pButton->setToUp();
			POptionInfo->pButton->removeLabel(POptionInfo->pBitmap);


			// return from this method...
			return;
		}
	}

	// Clear the prompts if set all, clear all or
	// options reset button is up.
	if (pButton == &setAllButton_ ||
		pButton == &clearAllButton_ ||
		pButton == &optionsResetButton_)
	{
		clearPrompts_();    
	}


}   

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Accept key to signal the
// accepting the action.
//---------------------------------------------------------------------
//@ Implementation-Description
// For options Reset button, restore the software options based on 
// the contains of the data key.  For Set all button, it sets all 
// software options enabled.  For Clear all button, it sets all 
// software options disabled.  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	SoftwareOptionsSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("SoftwareOptionsSubScreen::adjustPanelAcceptPressHappened(void)");



	if (pCurrentButton_ == &optionsResetButton_)
	{

		Sound::Start(Sound::ACCEPT);
		// Let the service mode  manager knows the type of Service mode.
		SmManager::DoFunction(SmTestId::RESET_SOFTWARE_OPTIONS);

		Task::Delay(0,600);
		optionsResetButton_.setToUp();
		optionsResetButton_.setButtonType(Button::DARK);
		clearPrompts_();

	}
	else if (pCurrentButton_ == &setAllButton_)
	{

		Sound::Start(Sound::ACCEPT);
		for (Uint optionIdx = 0u; optionIdx < SoftwareOptions::NUM_OPTIONS;
			optionIdx++)
		{	// $[TI1] -- this path is ALWAYS taken...
			OptionInfo_ *const  POptionInfo = (::ArrOptionInfo_ + optionIdx);

			const SoftwareOptions::OptionId  OPTION_ID =
				(SoftwareOptions::OptionId)optionIdx;

			if (POptionInfo->pButton != NULL)
			{
				// enable (TRUE) copy of option bit stored on both CPUs...
				SmManager::DoFunction(::OptionIdToSmTestIdTable_[TRUE][OPTION_ID]);

				POptionInfo->pButton->setToDown();
				POptionInfo->pButton->addLabel(POptionInfo->pBitmap);

				Task::Delay(0,200);
			}
		}
		setAllButton_.setToUp();
		setAllButton_.setButtonType(Button::DARK);
		clearPrompts_();
	}
	else if (pCurrentButton_ == &clearAllButton_)
	{
		Sound::Start(Sound::ACCEPT);
		for (Uint optionIdx = 0u; optionIdx < SoftwareOptions::NUM_OPTIONS; optionIdx++)
		{	// $[TI1] -- this path is ALWAYS taken...
			OptionInfo_ *const  POptionInfo = (::ArrOptionInfo_ + optionIdx);

			const SoftwareOptions::OptionId  OPTION_ID =
				(SoftwareOptions::OptionId)optionIdx;

			if (POptionInfo->pButton != NULL)
			{
				// disable (FALSE) copy of option bit stored on both CPUs...
				SmManager::DoFunction(::OptionIdToSmTestIdTable_[FALSE][OPTION_ID]);

				POptionInfo->pButton->setToUp();
				POptionInfo->pButton->removeLabel(POptionInfo->pBitmap);
				Task::Delay(0,200);
			}
		}
		clearAllButton_.setToUp();
		clearAllButton_.setButtonType(Button::DARK);
		clearPrompts_();

	}

	pCurrentButton_ = NULL;

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the default adjust panel focus is restored.  Tell the
// subscreen to restore the appropriated prompts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls restoreTestPrompt to restore test prompts.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	SoftwareOptionsSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("SoftwareOptionsSubScreen::adjustPanelRestoreFocusHappened(void)");


	restoreTestPrompt();

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearPrompts_()
//
//@ Interface-Description
//  Clears all prompts and removes the current focus.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls setTestPrompt to clear prompts and AdjustPanel
//  TakeNonPersistentFocus to clear the current focus.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	SoftwareOptionsSubScreen::clearPrompts_()
{
	CALL_TRACE("SoftwareOptionsSubScreen::clearPrompts_()");


	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompts
	setTestPrompt(PromptArea::PA_PRIMARY,
				  PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	setTestPrompt(PromptArea::PA_ADVISORY,
				  PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	setTestPrompt(PromptArea::PA_SECONDARY,
				  PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Primary prompts
	setTestPrompt(PromptArea::PA_PRIMARY,
				  PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	setTestPrompt(PromptArea::PA_ADVISORY,
				  PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	setTestPrompt(PromptArea::PA_SECONDARY,
				  PromptArea::PA_LOW, NULL_STRING_ID);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
	SoftwareOptionsSubScreen::SoftFault(const SoftFaultID  softFaultID,
										const Uint32       lineNumber,
										const char*        pFileName,
										const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,  SOFTWARE_OPTIONS_SUBSCREEN,
							lineNumber, pFileName, pPredicate);
}

