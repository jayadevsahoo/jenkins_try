#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: SstSetupSubScreen - Activated by selecting SST Test button on
// the lower subScreen.
//---------------------------------------------------------------------
//@ Interface-Description
// SstSetupSubScreen contains any of the various patient tubing settings that
// allows adjusting of Patient circuit type and Humidification Type.
//
// The subscreen contains a setting button for each of the above settings.
// The Accept key must be pressed in order to accept and apply the 
// settings changes.
//
// The activateHappened()/deactivateHappened() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// acceptHappened() method is called by BatchSettingsSubScreen when the
// settings are being accepted.  buttonDownHappened() and
// buttonUpHappened() are called when any button in the subscreen is
// pressed.  Adjust Panel events are communicated via the various
// Adjust Panel "happened" methods.  
//
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the Sst Setup subscreen in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the Accept key is pressed then BatchSettingsSubScreen will call
// the acceptHappened() method which will accept the setting changes
// and call GuiApp::SetUpdateCompletedBatchProcessFlag() to inform
// GuiApp that we are waiting for the setting transaction completion.
// Once the transaction successful message is sent to
// executeSettingTransactionHappened() the screen will be deactived and
// the SstTestSubScreen will be activated.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SstSetupSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: mnr    Date: 28-Dec-2009     SCR Number:  6548, 6437
//  Project:  NEO
//  Description:
//      Cct type selection fixed for SST based on options.
//
//  Revision: 010   By: gdc    Date: 26-May-2007     SCR Number:  6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 009   By: sah    Date: 23-May-2000     DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//
//  Revision: 008  By: sah    Date: 30-Jun-1999   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Initial NeoMode Project implementation added.
//
//  Revision: 007  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 006  By:  hhd	   Date:  02-Feb-1999    DCS Number:  5322
//  Project:  ATC
//  Description:
//		Initial version.
//
//  Revision: 005  By:  yyy	   Date:  29-Sep-1998    DCS Number: 5137
//  Project:  BiLevel
//  Description:
//		Display appropriate prompts in between state transition.
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003  By:  yyy    Date:  01-Oct-97    DR Number: 2522
//    Project:  Sigma (R8027)
//    Description:
//      Must receive the transaction successful message before test starts.
//		9-Oct-97 Continue working on this DCS.
//
//  Revision: 002  By:  yyy    Date:  22-JULY-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      Changed the patient circuit button same size as the humidifier.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SstSetupSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "PromptStrs.hh"
#include "ServiceLowerScreenSelectArea.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceUpperScreen.hh"

#include "AdjustPanel.hh"
#include "HumidTypeValue.hh"
#include "PatientCctTypeValue.hh"
#include "LowerScreen.hh"
#include "PromptArea.hh"
#include "SettingContextHandle.hh"
#include "Sound.hh"
#include "SubScreenArea.hh"
#include "HumidTypeValue.hh"
#include "ContextSubject.hh"
#include "SstTestSubScreen.hh"
#include "ExitServiceModeSubScreen.hh"
#include "SoftwareOptions.hh"
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
//@ End-Usage

//@ End-Usage

//@ Code...


// Initialize static constants.
static const Uint16 SETTING_BUTTON_Y_ = 130;
static const Uint16 SETTING_BUTTON_WIDTH_ = 162;
static const Uint16 SETTING_BUTTON_HEIGHT_ = 46;
static const Uint16 SETTING_BUTTON_GAP_ = 10;
static const Uint8  BUTTON_BORDER_ = 3;
static const Int32 NUMERIC_VALUE_CENTER_X_ = 60;
static const Int32 NUMERIC_VALUE_CENTER_Y_ = 26;

static const Uint16 EXIT_BUTTON_X_ = 5;
static const Uint16 EXIT_BUTTON_WIDTH_ = 84;
static const Uint16 EXIT_BUTTON_Y_ = 362;
static const Uint16 EXIT_BUTTON_HEIGHT_ = 34;

static const Int32 SST_SUB_SCREEN_DIVIDE_LINE_START_X_ = 0;
static const Int32 SST_SUB_SCREEN_DIVIDE_LINE_END_X_ =
							SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH +
							SST_SUB_SCREEN_DIVIDE_LINE_START_X_ - 1;
static const Int32 SST_SUB_SCREEN_DIVIDE_LINE_Y_ = 353;

static Uint  HumidTypeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		  (sizeof(StringId) * (HumidTypeValue::TOTAL_HUMIDIFIER_TYPES - 1)))];
static Uint  CircuitTypeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		(sizeof(StringId) * (PatientCctTypeValue::TOTAL_CIRCUIT_TYPES - 1)))];

static DiscreteSettingButton::ValueInfo*  PHumidTypeInfo_ =
					(DiscreteSettingButton::ValueInfo*)::HumidTypeInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PCircuitTypeInfo_ =
				  (DiscreteSettingButton::ValueInfo*)::CircuitTypeInfoMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SstSetupSubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the More Settings subscreen.  Passed a pointer to the subscreen
// area which creates it (LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the subscreen container.
// Creates the various buttons displayed in the subscreen and adds them
// to the container.  Registers for callbacks from each button.
//
// $[01269] The SST setup subscreen shall provide access to the following ...
// $[01270] In addition to the settings listed above, the following controls ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstSetupSubScreen::SstSetupSubScreen(SubScreenArea *pSubScreenArea) :
		BatchSettingsSubScreen(pSubScreenArea),
		titleArea_(MiscStrs::SST_SETUP_SUBSCREEN_TITLE, SubScreenTitleArea::SSTA_CENTER),
		humidifierTypeButton_(
				SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH / 2 + SETTING_BUTTON_GAP_,
				SETTING_BUTTON_Y_,
				SETTING_BUTTON_WIDTH_,		SETTING_BUTTON_HEIGHT_,
				Button::LIGHT, 				BUTTON_BORDER_,
				Button::ROUND, 				Button::NO_BORDER,
				MiscStrs::HUMID_TYPE_BUTTON_TITLE,
				SettingId::HUMID_TYPE, 		this,
				MiscStrs::HUMID_TYPE_HELP_MESSAGE, 10),
		humidifierVolumeButton_(
				SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH / 2 + SETTING_BUTTON_GAP_,
				SETTING_BUTTON_Y_ + SETTING_BUTTON_HEIGHT_ + SETTING_BUTTON_GAP_,
				SETTING_BUTTON_WIDTH_, SETTING_BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::HUMID_VOLUME_BUTTON_TITLE_SMALL,
				MiscStrs::TC_ML_UNITS_SMALL,
				SettingId::HUMID_VOLUME, this,
				MiscStrs::HUMID_VOLUME_HELP_MESSAGE),
		patientCircuitTypeButton_(
				SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH / 2 
					- SETTING_BUTTON_WIDTH_ - SETTING_BUTTON_GAP_,
				SETTING_BUTTON_Y_,
				SETTING_BUTTON_WIDTH_,		SETTING_BUTTON_HEIGHT_,
				Button::LIGHT, 				BUTTON_BORDER_,
				Button::ROUND, 				Button::NO_BORDER,
				MiscStrs::CIRCUIT_TYPE_BUTTON_TITLE,
				SettingId::PATIENT_CCT_TYPE,this,
				MiscStrs::CIRCUIT_TYPE_HELP_MSG, 14),
		exitButton_(EXIT_BUTTON_X_,		 	EXIT_BUTTON_Y_,
				EXIT_BUTTON_WIDTH_,			EXIT_BUTTON_HEIGHT_,
				Button::DARK, 				BUTTON_BORDER_,
				Button::SQUARE,				Button::NO_BORDER,
				MiscStrs::SST_EXIT_MSG),
		pCurrentButton_(NULL),
		sstSubScreenDivideLine_(SST_SUB_SCREEN_DIVIDE_LINE_START_X_,
				SST_SUB_SCREEN_DIVIDE_LINE_Y_,
				SST_SUB_SCREEN_DIVIDE_LINE_END_X_,
				SST_SUB_SCREEN_DIVIDE_LINE_Y_),
		verifyHumidVolMsg_(MiscStrs::VERIFY_HUMID_VOL_MESSAGE)
{
	CALL_TRACE("SstSetupSubScreen(SubScreenArea *)");
	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Add the title area
	addDrawable(&titleArea_);

	// Add the screen dividing line.
	sstSubScreenDivideLine_.setColor(Colors::WHITE);
	addDrawable(&sstSubScreenDivideLine_);

	addDrawable(&exitButton_);

    verifyHumidVolMsg_.setColor(Colors::WHITE);
    addDrawable(&verifyHumidVolMsg_);


	// set up values for humidification type....
	::PHumidTypeInfo_->numValues = HumidTypeValue::TOTAL_HUMIDIFIER_TYPES;
	::PHumidTypeInfo_->arrValues[HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER] =
							MiscStrs::HUMID_TYPE_NON_HEATED_VALUE;
	::PHumidTypeInfo_->arrValues[HumidTypeValue::HEATED_TUBING_HUMIDIFIER] =
							MiscStrs::HUMID_TYPE_HEATED_TUBING_VALUE;
	::PHumidTypeInfo_->arrValues[HumidTypeValue::HME_HUMIDIFIER] =
							MiscStrs::HUMID_TYPE_HME_VALUE;
	humidifierTypeButton_.setValueInfo(::PHumidTypeInfo_);


	// set up values for patient circuit type....
	::PCircuitTypeInfo_->numValues = PatientCctTypeValue::TOTAL_CIRCUIT_TYPES;
	::PCircuitTypeInfo_->arrValues[PatientCctTypeValue::ADULT_CIRCUIT] =
							MiscStrs::CIRCUIT_TYPE_ADULT_VALUE;
	::PCircuitTypeInfo_->arrValues[PatientCctTypeValue::PEDIATRIC_CIRCUIT] =
							MiscStrs::CIRCUIT_TYPE_PED_VALUE;
	::PCircuitTypeInfo_->arrValues[PatientCctTypeValue::NEONATAL_CIRCUIT] =
							MiscStrs::CIRCUIT_TYPE_NEO_VALUE;
	patientCircuitTypeButton_.setValueInfo(::PCircuitTypeInfo_);


	Uint  idx;

	idx = 0u;
	arrSettingButtonPtrs_[idx++] = &humidifierTypeButton_;
	arrSettingButtonPtrs_[idx++] = &patientCircuitTypeButton_;
	arrSettingButtonPtrs_[idx++] = &humidifierVolumeButton_;
	arrSettingButtonPtrs_[idx]   = NULL;

	AUX_CLASS_ASSERTION((idx <= MAX_SETTING_BUTTONS_), idx);

	// Add the buttons to the subscreen
	for (idx = 0u; arrSettingButtonPtrs_[idx] != NULL; idx++ )
	{
		addDrawable(arrSettingButtonPtrs_[idx]);
	}

	// Register for callbacks from buttons
	humidifierTypeButton_.setButtonCallback(this);
	patientCircuitTypeButton_.setButtonCallback(this);
	humidifierVolumeButton_.setButtonCallback(this);
	exitButton_.setButtonCallback(this);
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SstSetupSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys SstSetupSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstSetupSubScreen::~SstSetupSubScreen(void)
{
	CALL_TRACE("~SstSetupSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: executeSettingTransactionHappened
//
//@ Interface-Description
// Called when a successful setting transaction event occured.
// Passed no parameters.  
//---------------------------------------------------------------------
//@ Implementation-Description
// Simplily deactivate the upper screen area and exit the SST setup and
// drop to SST test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstSetupSubScreen::executeSettingTransactionHappened(void)
{
	CALL_TRACE("executeSettingTransactionHappened(void)");

	// Deactivate the upper screen area
	UpperSubScreenArea *pUpperSubScreenArea;
	pUpperSubScreenArea = ServiceUpperScreen::RServiceUpperScreen.
								getUpperSubScreenArea();
	pUpperSubScreenArea->deactivateSubScreen();


	// Need to exit the SST setup and drop to SST test.
	getSubScreenArea()->activateSubScreen(
				LowerSubScreenArea::GetSstTestSubScreen(), NULL);

				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called by BatchSettingsSubScreen when the operator presses the Accept
// key. We accept the settings and inform GuiApp that we are now waiting 
// on SettingTransaction event.
//---------------------------------------------------------------------
//@ Implementation-Description
// Make the "Accept" sound, clear the Adjust Panel focus, accept the
// adjusted settings, and inform GuiApp that we are now waiting on
// SettingTransaction event.
// $[01257] The Accept key shall be the ultimate confirmation step for all ...
// $[01258] The GUI shall accept all adjusted settings.
// $[01271] If the user selects the EXIT SST button and press the ACCEPT ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstSetupSubScreen::acceptHappened(void)
{
	CALL_TRACE("acceptHappened(void)");

	if ( pCurrentButton_ != &exitButton_ )
	{												// $[TI1.1]
		// Clear the Adjust Panel focus, make the Accept sound, store the
		// current settings, accept the adjusted settings and deactivate
		// this subscreen.
		Sound::Start(Sound::ACCEPT);

		deactivate();

		// Accept batch setting
		SettingContextHandle::AcceptSstBatch();

		GuiApp::SetUpdateCompletedBatchProcessFlag();
	}
	else
	{												// $[TI1.2]
		deactivate();

		// Need to exit the SST mode and jump to ExitServiceModeSubScreen
		// Activate the Exit service subscreen
		getSubScreenArea()->activateSubScreen(
				LowerSubScreenArea::GetExitServiceModeSubScreen(),NULL);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when the button is pressed down.  Passed a pointer to the
// button as well as 'byOperatorAction', a flag which indicates whether
// the operator pressed the button down or whether the setToDown()
// method was called (flag is ignored).  We display any appropriate
// prompts.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the humidifier or patient circuit type buttons are pressed then
// display the appropriate prompts.
// If the exit button is pressed then grab focus and display appropriate
// prompts for user to confirm exit.
// $[01272] When all settings are complete, the user shall be able to select ...
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstSetupSubScreen::buttonDownHappened(Button *pButton,
									Boolean byOperatorAction)
{
	CALL_TRACE("buttonDownHappened(Button *, Boolean)");
	if (!isVisible())
	{													// $[TI1.1]
		return;
	}													// $[TI1.2]

	if (pButton == &humidifierTypeButton_ ||
		pButton == &patientCircuitTypeButton_ ||
		pButton == &humidifierVolumeButton_)
	{												 	// $[TI2.1]
		exitButton_.setToUp();

		// Set primary prompt to   "Use knob to adjust."
		// Set secondary prompt to "To apply: press ACCEPT."
		//                         "To cancel: touch EXIT SST."
		//
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, 
						PromptArea::PA_HIGH, PromptStrs::USE_KNOB_TO_ADJUST_P);
	}
	else if (pButton == &exitButton_)
	{												 	// $[TI2.2]
		// grabbing focus resets the state of all settings buttons to up
		AdjustPanel::TakeNonPersistentFocus(this);
		//
		// Set primary prompt to "Press ACCEPT to confirm."
		//
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, 
					PromptArea::PA_HIGH,
					PromptStrs::SST_PRESS_ACCEPT_TO_CONFIRM_P);

		// Set advisory prompt to "Exit SST is requested"
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, 
					PromptArea::PA_HIGH,
					PromptStrs::SST_USER_REQUESTING_EXIT_A);

		// Set secondry prompt to "To cancel: touch EXIT SST."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_HIGH, PromptStrs::SST_TEST_CANCEL_S);

	}
	else
	{
		CLASS_ASSERTION_FAILURE();
	}	

	pCurrentButton_ = pButton;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when one of the sub-screen buttons is pressed up.  Passed a
// pointer to the button as well as 'byOperatorAction', a flag which
// indicates whether the operator pressed the button up or whether the
// setToUp() method was called.  We restore prompts appropriately.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the exit button is pressed up then clear the high priority
// prompts. For other buttons, do nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstSetupSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
	CALL_TRACE("buttonUpHappened(Button *, Boolean)");
	// Don't do anything if this subscreen isn't displayed
	if (!isVisible())
	{													// $[TI1.1]
		return;
	}													// $[TI1.2]

	if (pButton == &exitButton_)
	{												 	// $[TI2.1]
		// Clear the high priority prompts
		//
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
	}													// $[TI2.2]

	pCurrentButton_ = NULL;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened()
//
//@ Interface-Description
// This virtual function of BatchSettingsSubScreen is overriden here
// to always clear the primary prompt when panel focus is lost.
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the high priority prompt to null so the lower priority prompt
// if not null will be displayed.
//---------------------------------------------------------------------
//@ PreCondition
// Can only be pressed down by operator.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
SstSetupSubScreen::adjustPanelRestoreFocusHappened(void)
{
    CALL_TRACE("adjustPanelRestoreFocusHappened()");
 
	// $[TI1]
	// Clear the high priority prompts
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_HIGH, NULL_STRING_ID);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SstSetupSubScreen::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
			SSTSETUPSUBSCREEN, lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Hide the lower screen select area, activate setting buttons, and displays
// prompts to guide the operator.
// $[01273] When the ACCEPT key has not yet been pressed... 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstSetupSubScreen::activateHappened_(void)
{
	CALL_TRACE("activateHappened_()");

	// Hide the drawables within the lower screen select area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

    // Display setup title
	titleArea_.setTitle(MiscStrs::SST_SETUP_SUBSCREEN_TITLE);
	
	// Begin adjusting settings
	SettingContextHandle::AdjustSstBatch();

	const DiscreteValue PATIENT_CCT_TYPE = 
		(ContextMgr::GetAcceptedContext()).getDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	// [02282] turn knob to trigger PatientCctTypeSetting::isEnabledValue() check
	if( SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_LOCKOUT) )
	{
        patientCircuitTypeButton_.adjustPanelKnobDeltaHappened(1);
	}

	// SCR 6548 : turn knob to trigger PatientCctType isEnabled() check
	if( !SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE) 
		&& PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT
	   )
	{
		patientCircuitTypeButton_.adjustPanelKnobDeltaHappened(1);
	}

	// Show the exit button...
	exitButton_.setShow(TRUE);
	
	// Activate setting buttons...
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::activate);

	// For Lockout option SST flatten the cct type button.
	// This has to be done after the buttons are activated
	if( SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_LOCKOUT) )
	{
		patientCircuitTypeButton_.setToFlat();
	}

	// Set Primary prompt to "Select and Adjust settings."
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_P);

	// Set the Advisory prompts to "Must match attached equipment!"
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, PromptStrs::SST_MATCH_ATTACHED_EQIP_A);

	// Set Secondary prompt to "To apply: press ACCEPT."
	//						   "To cancel: touch EXIT SST"
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_LOW, PromptStrs::SST_PROCEED_EXIT_S);

	verifyHumidVolMsg_.setShow(FALSE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears the Adjust Panel focus in case we had it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstSetupSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("deactivateHappened_()");

	// Deactivate setting buttons...
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::deactivate);

	exitButton_.setShow(FALSE);
				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdateHappened_
//
//@ Interface-Description
// Called when a setting has changed.  If this screen is visible then the
// Proceed button is displayed (if not already).  Passed the following
// parameters:
// >Von
//	settingId	The enum id of the setting which changed.
//	contextId	The context in which the change happened, either
//				ContextId::ACCEPTED_CONTEXT_ID or
//				ContextId::ADJUSTED_CONTEXT_ID.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// We first check if we're visible.  We ignore all setting changes when this
// subscreen is not active.  If settings have changed from being current
// to proposed then change the screen title and display the Proceed button,
// else if settings have reverted to current from being proposed then
// change back the screen title and remove the Proceed button.
//
// $[01058] Proceed button only displayed on first setting change
// $[01118] As soon as any settings have changed display Proceed button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstSetupSubScreen::valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									   )
{
	CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");

	// only show the verify message, if the humidifier volume is currently
	// shown AND patient circuit type has been changed...
	const Boolean  DO_SHOW_MSG =
		(humidifierVolumeButton_.isVisible()  &&
		 pSubject->isSettingChanged(SettingId::PATIENT_CCT_TYPE));

	verifyHumidVolMsg_.setShow(DO_SHOW_MSG);
}  // $[TI1]
