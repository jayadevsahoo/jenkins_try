
#ifndef Heap_HH
#define Heap_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Heap - Abstract Heap of Memory Blocks.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/Heap.H_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 005   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 004   By: sah   Date: 26-Feb-1997   DCS Number: 1800
//   Project:  Sigma (R8027)
//   Description:
//	Fixed code that is out of spec with Coding Standard.
//  
//   Revision: 003   By: sah   Date: 29-Feb-1996   DCS Number: 719
//   Project:  Sigma (R8027)
//   Description:
//	Fixed a syntax that is incompatible with the new version of the
//	compiler.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "Collection.hh"
#include "FoundationClassId.hh"
#include "TemplateMacros.hh"

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class  Ostream; 
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
//@ End-Usage


class Heap
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilatates debug-only printing of this class's instances.
    friend Ostream&  operator<<(Ostream& ostr, const Heap& heap);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  public:
    virtual ~Heap(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    Boolean  isLegalBlock(const void* pRawMemory) const;

    inline Uint32  getBlockSize(void) const;
    inline Uint32  getMaxBlocks(void) const;

    Uint32         getNumAvailable(void) const;
    inline Uint32  getNumAllocated(void) const;

    void*  allocate  (void);
    void   deallocate(void*& pRawMemory);

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

    //@ Type:  Node
    //  Used as nodes within the free list of blocks.
    struct Node
    {
      Node*  pNext;
    };

  protected:
    Heap(Node*        pInitFreeList,
		 const Uint32 numMaxBlocks,
		 const Uint32 numBlockNodes);

    virtual const Node*  getFirstNode_(void) const = 0; 

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    Heap(const Heap&);		// not implemented...
    Heap(void);				// not implemented...
    void  operator=(const Heap&);	// not implemented...

    //@ Data-Member:  pFreeList_
    // A linked list of free blocks.
    Node*  pFreeList_;

    //@ Constant:  MAX_BLOCKS_
    // Maximum number of blocks in this heap.
    const Uint32  MAX_BLOCKS_;

    //@ Constant:  NODES_PER_BLOCK_
    // Number of nodes in each block of this heap.
    const Uint32  NODES_PER_BLOCK_;
};


//====================================================================
//
//  Overloaded New Operator for this Heap...
//
//====================================================================

//@ Begin-Free-Declarations
// overloaded 'new' that can be called as follows:
//	'new (objectHeap) SomeType()'...
extern void*  operator new(size_t objectSize, Heap& objectHeap);
//@ End-Free-Declarations


// Inlined methods...
#include "Heap.in"


#endif  // Heap_HH
