// A simple program that uses LoadLibrary and 
// GetProcAddress to access myPuts from Myputs.dll. 
 
#include <windows.h> 
#include <stdio.h> 
#include "DataCaptureIf.h"

 
typedef DataCaptureIf * (__cdecl *MYPROC)(); 

DataCaptureIf *g_DataCapture = NULL;



VOID main(VOID) 
{ 
    HINSTANCE hinstLib; 
    MYPROC ProcAdd; 
    BOOL fFreeResult, fRunTimeLinkSuccess = FALSE; 
 
    // Get a handle to the DLL module.
 
    hinstLib = LoadLibrary(TEXT("DataCapture.dll")); 
 
    // If the handle is valid, try to get the function address.
 
    if (hinstLib != NULL) 
    { 
        ProcAdd = (MYPROC) GetProcAddress(hinstLib, "DataCapture"); 
 
        // If the function address is valid, call the function.
 
        if (NULL != ProcAdd) 
        {
            fRunTimeLinkSuccess = TRUE;
            g_DataCapture=(ProcAdd) ();

            g_DataCapture->start();
//	    g_DataCapture->updateClientInfo("10.95.30.108", "7003");

            printf(">>>>%x\n",g_DataCapture);

		while(1);
            Sleep(11000);

            g_DataCapture->stop();

        }
        // Free the DLL module.
 
        fFreeResult = FreeLibrary(hinstLib); 
    }
    else {
         printf("Barf\n"); 

    }

    // If unable to call the DLL function, use an alternative.
    if (! fRunTimeLinkSuccess) 
        printf("Message printed from executable .....\n"); 
}

