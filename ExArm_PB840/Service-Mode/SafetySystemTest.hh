#ifndef SafetySystemTest_HH
#define SafetySystemTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SafetySystemTest - safety valve and inspiratory check valve test
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SafetySystemTest.hhv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

extern const Real32 MAX_NO_CURRENT;
extern const Real32 MIN_HIGH_CURRENT;
extern const Real32 MAX_LOW_CURRENT;
extern const Real32 MIN_LOW_CURRENT;
extern const Real32 SV_OPEN_MAX_BACKPRESSURE;
extern const Real32 MIN_SV_BLEED_DECAY_TIME;
extern const Real32 MAX_SV_BLEED_DECAY_TIME;
extern const Real32 BLEED_START_PRESSURE;
extern const Real32 BLEED_END_PRESSURE;

class SafetySystemTest
{
  public:
    SafetySystemTest(void);
    ~SafetySystemTest(void);

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);
  
    void runTest(void);

  protected:

  private:
    SafetySystemTest(const SafetySystemTest&);	// not implemented...
    void   operator=(const SafetySystemTest&);	// not implemented...

	Boolean isSafetyValveUnoccluded_(void);
	Boolean isLoopbackOk_(void);
	Real32 measureBleed_(void);
};


#endif // SafetySystemTest_HH 
