#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Triangle - This is a drawable triangle class.
//---------------------------------------------------------------------
//@ Interface-Description
// The Triangle class is used to create and manage triangle primitives.  The
// triangle can be of any triangular shape; any three points can be used to
// specify its shape.  The application specifies the 3 point positions
// relative to the upper left corner of its Container.
//
// The constructor calculates the minimum rectangular area needed to enclose
// the triangle, and assigns it to the Triangle's area attribute (inherited
// from Drawable).  The methods for changing screen position (setX() and
// setY(), inherited from parent class Drawable) use the upper left corner of
// this area as the triangle's position.  The inherited methods for changing
// the size of the area, setWidth() and setHeight(), should NOT be used,
// because the actual width is purely a function of the positions of the three
// points.
//
// The shape cannot be changed after construction time.  The position, border
// color and fill color can all be modified by the application after
// construction.  Note that the line width of the border is fixed at a width of
// one.
//
// The Triangle class is derived from the Drawable class, and thus is "aware"
// of the screen management framework.
//---------------------------------------------------------------------
//@ Rationale
// It was deemed necessary to include this object in the GUI Foundation
// library.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class maintains internal variables that specify the Triangle's
// position, shape and fill color.  A method is provided to allow changing the
// fill color.  The border color is set by calling the inherited
// Drawable::setColor() method.
//
// The VGA-Graphics-Driver subsystem provides one routine to
// render a filled Triangle (with edges), and another to draw just the
// outline of the triangle.  If the Triangle object has no fill color, the 
// outline-only routine is called.
//
// Triangle accepts calls to its draw() method from the screen management
// framework.  Changes to a Triangle will not result in changes on the
// screen until the application calls the BaseContainer::repaint() method,
// signaling the framework to update the screen.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Triangle.ccv   25.0.4.0   19 Nov 2013 14:11:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: gdc    Date:  21-May-2007   SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 008   By: gdc    Date:  9-Oct-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 007   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 006   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 005  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 004  By: sah       Date: 06-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid, by removing all semaphore requests/releases from
//      all of the 'draw()' methods.
//
//  Revision: 003  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 002  By: mpm      Date: 29-Mar-1996  DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added calls to GuiFoundation::RequestDisplayAccess() and ReleaseDisplayAccess()
//      in order to allow shared access to the display by separate drawing tasks.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Triangle.hh"

//@ Usage-Classes
#include "BaseContainer.hh"
#include "Container.hh"
#include "GraphicsContext.hh"
#include "DisplayContext.hh"
//@ End-Usage


//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Triangle
//
//@ Interface-Description
// This is the Constructor for Triangle.  It takes 3 pairs of integer
// arguments, each pair specifying a point on the triangle. Note that the upper
// left corner of the smallest rectangle that completely contains the triangle
// is considered to be the position of the triangle.
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets up the area_ of the triangle based on the passed points.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Triangle::Triangle(Int16 pt1X, Int16 pt1Y, Int16 pt2X, Int16 pt2Y,
					Int16 pt3X, Int16 pt3Y) :
	pt1X_(pt1X),
	pt1Y_(pt1Y),
	pt2X_(pt2X),
	pt2Y_(pt2Y),
	pt3X_(pt3X),
	pt3Y_(pt3Y),
	fillColor_(Colors::NO_COLOR)
{
	CALL_TRACE("Triangle::Triangle(Int16 pt1X, Int16 pt1Y, "
					"Int16 pt2X, Int16 pt2Y, Int16 pt3X, Int16 pt3Y)");

	//													$[TI1]
	// Setting the containing area of the triangle
	area_.setX(MIN_VALUE(MIN_VALUE(pt1X_, pt2X_), pt3X_));
	area_.setY(MIN_VALUE(MIN_VALUE(pt1Y_, pt2Y_), pt3Y_));
	area_.setWidth(MAX_VALUE(MAX_VALUE(pt1X_, pt2X_), pt3X_) - getX() + 1);
	area_.setHeight(MAX_VALUE(MAX_VALUE(pt1Y_, pt2Y_), pt3Y_) - getY() + 1);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Triangle 
//
//@ Interface-Description
//    Destructor. Destroys object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Triangle::~Triangle(void)
{
	CALL_TRACE("Triangle::~Triangle(void)");

	//													$[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFillColor
//
//@ Interface-Description
// Changes the fill color of the triangle to the passed "fillColor".  This is
// the color of the interior of the triangle; the color of the border is set
// using the setColor() method inherited from parent class Drawable.  If the
// fill color is set to NO_COLOR, this results in a non-filled triangle.  The
// fill color is set to NO_COLOR during construction.
//---------------------------------------------------------------------
//@ Implementation-Description
// Since this method changes the appearance of this object, this object must
// be marked for re-display at the next repaint; this is done by calling
// the changing() method.  Then the private fillColor_ attribute is set
// to the passed fillColor.
//---------------------------------------------------------------------
//@ PreCondition
//  The passed color must be a legal color or NO_COLOR.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Triangle::setFillColor(Colors::ColorS fillColor)
{
	CALL_TRACE("Triangle::setFillColor(Colors::ColorS)");

	CLASS_PRE_CONDITION((fillColor >= 0 &&
						 fillColor < Colors::NUM_COLORS 
						)
						|| fillColor == Colors::NO_COLOR
					   );

	//								$[TI1]
	changing();

	fillColor_ = fillColor;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setX 	[virtual]
//
//@ Interface-Description
// Redefines the inherited Drawable::setX() method to move the triangle's
// vertices and its bounding area to a new X location.
//---------------------------------------------------------------------
//@ Implementation-Description
// Computes the delta between the old X and the new X position and 
// applies this delta to each of the triangle's vertices.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Triangle::setX(Int newX)
{
	CALL_TRACE("setX(Int)");
	// $[TI1]

	changing();
	Int deltaX = newX - getX();
	pt1X_ += deltaX;
	pt2X_ += deltaX;
	pt3X_ += deltaX;
	area_.setX(newX);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setY 	[virtual]
//
//@ Interface-Description
// Redefines the inherited Drawable::setY() method move the triangle's
// vertices and its bounding area to a new Y location.
//---------------------------------------------------------------------
//@ Implementation-Description
// Computes the delta between the old Y and the new Y position and 
// applies this delta to each of the triangle's vertices.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Triangle::setY(Int newY)
{
	CALL_TRACE("setY(Int)");
	// $[TI1]

	changing();
	Int deltaY = newY - getY();
	pt1Y_ += deltaY;
	pt2Y_ += deltaY;
	pt3Y_ += deltaY;
	area_.setY(newY);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: draw    [virtual]
//
//@ Interface-Description
// This draw routine renders the triangle on the VGA display.  It overrides the
// pure virtual method inherited from Drawable.  The passed parameter
// is ignored.
// The draw method shall only be called through the BaseContainer::repaint().
// The Container::draw() takes care of setting the clip area.
//---------------------------------------------------------------------
//@ Implementation-Description
// The relative coordinates for the Triangle's corner points are transformed
// into absolute (screen) coordinates.  The triangle is then rendered using
// routines from the VGA-Graphics-Driver subsystem, as follows: first, if the
// fillColor_ is not NO_COLOR, the solid interior of the triangle is drawn
// using the DisplayContext::fillTriangle() routine. Then the borders of the
// triangle are rendered using the VGATriangle() routine.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
Triangle::draw(const Area &)
{
	CALL_TRACE("Triangle::draw(const Area&)");

	DisplayContext & rDc = getBaseContainer()->getDisplayContext();
    GraphicsContext & rGc = getBaseContainer()->getGraphicsContext();

	Int x1 = getParentContainer()->convertToRealX(pt1X_);
	Int y1 = getParentContainer()->convertToRealY(pt1Y_);
	Int x2 = getParentContainer()->convertToRealX(pt2X_);
	Int y2 = getParentContainer()->convertToRealY(pt2Y_);
	Int x3 = getParentContainer()->convertToRealX(pt3X_);
	Int y3 = getParentContainer()->convertToRealY(pt3Y_);

	// If we have a fill color then draw a solid triangle in the fill color.
	// This does not draw the outline.
	if (fillColor_ != Colors::NO_COLOR) 
	{	//													$[TI1]
	  rGc.setForegroundColor( fillColor_ );
	  rDc.fillTriangle(rGc, x1, y1, x2, y2, x3, y3);
	}
	//														$[TI2]
														
	// Draw a triangle outline in drawable color
	rGc.setForegroundColor( getColor() );
	rDc.drawTriangle(rGc, x1, y1, x2, y2, x3, y3);
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
Triangle::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, TRIANGLE, lineNumber,
			  pFileName, pPredicate);
}
