#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BdLampTest - Test the BD lamps
//---------------------------------------------------------------------
//@ Interface-Description
//	This class implements the BD lamp test, which tests the BD lamps
//  via user feedback.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the BD lamp test.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method runTest() is responsible for testing the BD lamps.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/BdLampTest.ccv   25.0.4.0   19 Nov 2013 14:23:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  quf    Date: 3-Dec-1997    DR Number: 5291
//       Project:  Sigma (840)
//       Description:
//            Changed ACCEPT_AND_CANCEL_ONLY to
//		 	  ACCEPT_PASS_AND_CANCEL_FAIL_ONLY.
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "BdLampTest.hh"

//@ Usage-Classes

#include "SmManager.hh"
#include "SmPromptId.hh"
#include "VentObjectRefs.hh"
#include "VentStatus.hh"


//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdLampTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdLampTest::BdLampTest(void)
{
	CALL_TRACE("BdLampTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdLampTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdLampTest::~BdLampTest(void)
{
	CALL_TRACE("~BdLampTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest()
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.  It is used
//  to test the BD lamps.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: all BD LEDs are off.
//
//	$[09155]
//	Turn on each BD LED one at a time and prompt user to verify the
//	proper LED is on, ACCEPT = yes, CANCEL = no.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BdLampTest::runTest(void)
{
	CALL_TRACE("BdLampTest::runTest(void)");

	Boolean userExit = FALSE;

	// Turn on the ventilator inoperative indicator
    RVentStatus.turnOnVentInOpLed_();

	// Ask the operator if the proper indicator is lit
	RSmManager.promptOperator (SmPromptId::VENTILATOR_INOPERATIVE_PROMPT,
		SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
	SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction();

	if (operatorAction == SmPromptId::USER_EXIT)
	{
	// $[TI1]
		// Operator Exit
		RSmManager.sendOperatorExitToServiceData();
		userExit = TRUE ;
	}
	else if (operatorAction == SmPromptId::KEY_CLEAR)
	{
	// $[TI2]
		// Ventilator Inop failed to light
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
	}
	else
	{
	// $[TI3]
		// accept key pressed -- no action
		CLASS_ASSERTION ( operatorAction == SmPromptId::KEY_ACCEPT );
	}
	// end else

	// Turn off the ventilator inoperative indicator
    RVentStatus.turnOffVentInOpLed_();

	if (!userExit)
	{
	// $[TI4]
		// Turn on the safety valve open indicator.
	    RVentStatus.turnOnSvoLed_();

		// Ask the operator if the proper indicator is lit
		RSmManager.promptOperator (SmPromptId::SAFETY_VALVE_OPEN_PROMPT,
			SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction();

		if (operatorAction == SmPromptId::USER_EXIT)
		{
		// $[TI4.1]
			// Operator Exit
			RSmManager.sendOperatorExitToServiceData();
			userExit = TRUE ;
		}
		else if (operatorAction == SmPromptId::KEY_CLEAR)
		{
		// $[TI4.2]
			// SVO failed to light
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{
		// $[TI4.3]
			// accept key pressed -- no action
			CLASS_ASSERTION ( operatorAction == SmPromptId::KEY_ACCEPT );
		}
		// end else

		// Turn off the Safety Valve open indicator and
	    RVentStatus.turnOffSvoLed_();
	}
	// $[TI5]
	// implied else

	// Verify the user did not exit
	if (!userExit)
	{
	// $[TI6]
		// Turn on the Loss of GUI indicator.
	    RVentStatus.turnOnGuiLed_();

		// Ask the operator if the proper indicator is lit
		RSmManager.promptOperator (SmPromptId::LOSS_OF_GUI_PROMPT,
			SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction();
	
		if (operatorAction == SmPromptId::USER_EXIT)
		{
		// $[TI6.1]
			// Operator Exit
			RSmManager.sendOperatorExitToServiceData();
			userExit = TRUE ;
		}
		else if (operatorAction == SmPromptId::KEY_CLEAR)
		{
		// $[TI6.2]
			// Loss of GUI failed to light
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
		}
		else
		{
		// $[TI6.3]
			// accept key pressed -- no action
			CLASS_ASSERTION ( operatorAction == SmPromptId::KEY_ACCEPT );
		}
		// end else

		// Turn off the Loss of GUI indicator
	    RVentStatus.turnOffGuiLed_();
	}
	// $[TI7]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BdLampTest::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, BDLAMPTEST,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
