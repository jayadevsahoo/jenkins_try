#include "stdafx.h"
//*****************************************************************************
//
//  CommunicationThread.cpp - Implementation of the 
//  ExhCommunicationInterface class.
//
//*****************************************************************************
#include "ExhCommunicationInterface.h"
#include "GlobalObjects.h"
#include "ExceptionHandler.h"
#include "CalibrationIDs.h"
#include "ExhFlowSensor.h"
#include "BDIORefs.hh"
#include "MainSensorRefs.hh"
#include "Task.hh"

static const Uint16 INTIAL_EXH_AZ_DAC_VALUE	= 1000;
static const Uint16 FINAL_EXH_AZ_DAC_VALUE	= 3000;
static const Uint16 EXH_AZ_DAC_INCREMENT		= 50;

#define EXH_THREAD_EVENT_NAME	TEXT("EXH_THREAD_EVENT")

HANDLE hExhCommThreadEvent = NULL;
bool bFlowDataRequested = false;


ExhCommunicationInterface*          ExhCommunicationInterface::instance_ = NULL;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getInstance
//
//@ Interface-Description
//  Returns the single instance of the ExhCommunicationInterface. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ExhCommunicationInterface* ExhCommunicationInterface::getInstance() 
{
    if(instance_ == NULL)
	{
        instance_ = new ExhCommunicationInterface(BdSerialInterface::GetSerialPort(BdSerialInterface::SERIAL_PORT_2), BdSerialInterface::SERIAL_PORT_2);
		instance_->setSerialParameters_(19200,8,SerialPort::NONE,ONESTOPBIT);
		instance_->purgeline();

		// Find out which type of sensor board before we proceed
		// used for the new sensor board
		instance_->detectSensorBoard();

		instance_->retrieveSerialNumber();
	}

    return instance_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhCommunicationInterface [constructor]
//
//@ Interface-Description
//  Constructor for the ExhCommunicationInterface class. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExhCommunicationInterface::ExhCommunicationInterface( BdSerialPort & rSerialPort,
                                BdSerialInterface::PortNum portNum ) :
BdSerialInterface( rSerialPort, portNum )
{
    ExhCommunicationState = STATE_FEX_STATUS;
	AutozeroRequestState = STATE_AUTOZERO_COMPLETED;
	bFexDataValid = false;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhCommunicationInterface [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExhCommunicationInterface::~ExhCommunicationInterface()   
{
	delete instance_;
	instance_ = NULL;
}
//*****************************************************************************
//
//  ExhCommunicationInterface::RequestAutozero
//
//*****************************************************************************
bool ExhCommunicationInterface::RequestAutozero()
{
	if(!bAutozeroRequested)
	{
		bAutozeroRequested = true;
		return true;
	}
	else
		return false;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhCommunicationInterface::processRequests_
//
//@ Interface-Description
//  Sets up ExhCommunication and Processes requests from it 
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void ExhCommunicationInterface::processRequests_(void)
{
	Uint8 Data = 0;
	Uint32 NumberOfBytesRead = 0;
	Uint32 NumberOfBytesWritten = 0;
	bool bCommStatus = false;
	bool bProcessData = false;
	static Uint32 AZPendingCount = 0;
	static Uint16 FlowDataPendingCount = 0;
	static Uint32 InitialCount =0;
	static Uint16 DacValue = 0;
	Uint32 ElapsedTickCount = 0;
	static Uint32 TimeToWait = 0;
    bool  WriteCommandWasIssued = false;

	// used for the new sensor board
	static Uint32 delayInitialCount = 0;
	static Uint32 delayTimeToWait = 100;
	bool delayInit = false;

	hExhCommThreadEvent = CreateEvent(NULL, FALSE, FALSE, EXH_THREAD_EVENT_NAME);
	if(!hExhCommThreadEvent)
	{
		RETAILMSG(TRUE, (_T("ExhCommunicationThread: Creating hExhCommThreadEvent failed\r\n")));
	}
	else
	{
		Uint32 err = GetLastError();
		if(err != ERROR_ALREADY_EXISTS)
		{
			RETAILMSG(TRUE, (L"ExhCommunicationThread: hExhCommThreadEvent has not been created\r\n"));
		}
	}

	while (true)
	{
	  if(WriteCommandWasIssued)
      {
		while (true)
		{
			Data = DATA_CMD_00;
			NumberOfBytesRead = read(&Data, 1, 0);
			if((NumberOfBytesRead != 1))	// if nothing to read, get out of the loop
			{
				break;
			}

			switch(ExhCommunicationState)
			{
			case STATE_FEX_STATUS:

				FexStatus = Data;
				ExhCommunicationState = STATE_FEX_MSB;
				break;

			case STATE_FEX_MSB:

				FexRawData = Data << 8;
				ExhCommunicationState = STATE_FEX_LSB;
				break;

			case STATE_FEX_LSB:

				FexRawData |= Data;
				bFlowDataRequested = false;
				ExhCommunicationState = STATE_FEX_STATUS;
				bProcessData = true;
				bFexDataValid = true;
				break;

			case STATE_AUTOZERO_STATUS:
				FexStatus = Data;
				if(exhSensorBoard == EXH_SENSOR_BOARD_NEW)
				{
					//There are two new bytes following AZ status.. They indicate.
					//the zero offset value. Read them out and pass to the upper layer
					Uint8 offsetMSB = 0;
					Uint8 offsetLSB = 0;
					NumberOfBytesRead = read(&offsetMSB, 1, 0);
					NumberOfBytesRead = read(&offsetLSB, 1, 0);
					FexRawData = ((offsetMSB & 0x3f) << 6) | (offsetLSB & 0x3f);
				}
				bProcessData = true;
				bFexDataValid = true;
				RETAILMSG(1, (L"AZ Request Done,Status=0x%x, FexRawData=%d\r\n", FexStatus, FexRawData));

			default:
				break;
			}//end of: switch(ExhCommunicationState)
		}
      } // end if


	  if(bFlowDataRequested)
	  {
		FlowDataPendingCount++;		// Track # of ticks since the flow data request was sent
	  }

	  switch(AutozeroRequestState)
	  {
	    case STATE_AUTOZERO_INIT:
			InitialCount = GetTickCount();
			// TODO E600_LL: check for ventilation state
			if(1)//VentilatorStateSetting.Get() != STATE_VENTILATING)
			{
				DacValue = INTIAL_EXH_AZ_DAC_VALUE;
#if defined(SIGMA_BD_CPU)
				AioDioDriver.Write(DAC_EXHALATION_VALVE, DacValue);
#endif
				TimeToWait = EXH_VALVE_CLOSE_TIME;
				AutozeroRequestState = STATE_AUTOZERO_CLOSE_VALVE;
			}
			else
			{
				TimeToWait = 500;
				AutozeroRequestState = STATE_AUTOZERO_WAIT;
			}
			break;
		case STATE_AUTOZERO_CLOSE_VALVE:
			DacValue += EXH_AZ_DAC_INCREMENT;
#if defined(SIGMA_BD_CPU)
			AioDioDriver.Write(DAC_EXHALATION_VALVE, DacValue);
#endif
			if(DacValue >= FINAL_EXH_AZ_DAC_VALUE)
			{
				AutozeroRequestState = STATE_AUTOZERO_WAIT;
			}
			break;
		case STATE_AUTOZERO_WAIT:
			if((GetTickCount() - InitialCount) > TimeToWait)
			{
				AutozeroRequestState = STATE_AUTOZERO_REQUEST;
			}
			break;
	    case STATE_AUTOZERO_REQUEST:
			RETAILMSG(1, (L"ExhComm: Sending AZ Request\r\n"));
			Data = DATA_CMD_40;
			NumberOfBytesWritten = write(&Data, 1, 0);
			AutozeroRequestState = STATE_AUTOZERO_PENDING;		// Stay in this state until autozero status received
			AZPendingCount = GetTickCount();
			ExhCommunicationState = STATE_AUTOZERO_STATUS;		// Signal this thread that autozero request has been sent
            WriteCommandWasIssued = true;
			break;

		case STATE_AUTOZERO_PENDING:
			ElapsedTickCount = GetTickCount() - AZPendingCount;
			Uint8 AzDoneMask;
			AzDoneMask = AUTOZERO_DONE_BIT;

			if(exhSensorBoard == EXH_SENSOR_BOARD_NEW)
			{
				AzDoneMask = AUTOZERO_DONE_BIT_NEW_SENSOR;
			}

			if(bProcessData && (FexStatus & AzDoneMask))
			{
				AutozeroRequestState = STATE_AUTOZERO_COMPLETED;
				if(exhSensorBoard == EXH_SENSOR_BOARD_NEW)
				{
					AutozeroRequestState = STATE_AUTOZERO_DELAY;
				}
				// TODO E600_LL: check for ventilation state
				if(1) //VentilatorStateSetting.Get() != STATE_VENTILATING)
				{
#if defined(SIGMA_BD_CPU)
					AioDioDriver.Write(DAC_EXHALATION_VALVE, 0);
#endif
				}
				RETAILMSG(1, (L"ExhComm: AZ Request Completed\r\n"));
			}
			// if taking too long, terminate
			else if(ElapsedTickCount > 1000)
			{
				RETAILMSG(1, (L"ExhComm: Sensor Not Responding\r\n"));
				AutozeroRequestState = STATE_AUTOZERO_DELAY;
#if defined(SIGMA_BD_CPU)
				AioDioDriver.Write(DAC_EXHALATION_VALVE, 0);
				RExhFlowSensor.SetNoComm();
#endif
			}
			break;

		case STATE_AUTOZERO_DELAY:
			if(!delayInit)
			{
				RETAILMSG(1, (L"ExhComm: Waiting for some time\r\n"));
				delayInitialCount = GetTickCount();
				delayTimeToWait = 100;
				delayInit = true;
			}
			else
			{
				if((GetTickCount() - delayInitialCount) > delayTimeToWait)
				{
					AutozeroRequestState = STATE_AUTOZERO_COMPLETED;
					delayInit = false;
				}
			}
			break;

		case STATE_AUTOZERO_COMPLETED:	// not requested for autozero, send request for sensor status and flow data
			if(bAutozeroRequested)
			{
				AutozeroRequestState = STATE_AUTOZERO_INIT;
				bAutozeroRequested = false;
				RETAILMSG(1, (L"ExhComm: AZ Request Received\r\n"));
			}
			else
			{
				if(!bFlowDataRequested || (FlowDataPendingCount > 2))
				{

					Data = DATA_CMD_01;
					
					if(exhSensorBoard == EXH_SENSOR_BOARD_NEW)
					{
						Data = DATA_CMD_03;
					}
					
					NumberOfBytesWritten = write(&Data, 1, 0);
					ExhCommunicationState = STATE_FEX_STATUS;
					bFlowDataRequested = true;
					FlowDataPendingCount = 0;
					WriteCommandWasIssued = true;
				}
			}
			break;

		default: break;
	  } // end switch

	  if(bProcessData)	// if there is something for the ExhFlowSensor to do, call it
	  {
#ifdef SIGMA_BD_CPU			
		    RExhFlowSensor.ProcessData(FexStatus, FexRawData, bFexDataValid);
#endif
			bProcessData = false;
			bFexDataValid = false;
			FexRawData = 0;
	  }

	  WaitForSingleObject(hExhCommThreadEvent, INFINITE);
	}//end of: while (true)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//  Called every 5ms by main BD cycle to process ??
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ExhCommunicationInterface::newCycle(void)
{
	//TODO E600
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: detectSensorBoard
//
//@ Interface-Description
//  Determine if it is OLd or New sensor board
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void ExhCommunicationInterface::detectSensorBoard(void)
{
	Boolean bResult = SetSensorBoardBaudRate(CBR_115200, 0x03);

	if(!bResult)
	{
		bResult = SetSensorBoardBaudRate(CBR_19200, 0x01);
		if(bResult)
		{
			exhSensorBoard = EXH_SENSOR_BOARD_OLD;
			RETAILMSG(1, (L"Old Sensor Board Detected\r\n"));
		}
		else
		{
			exhSensorBoard = EXH_SENSOR_BOARD_NEW;
			RETAILMSG(1, (L"Sensor Board NOT Detected, defaulting to New Board!!!\r\n"));
		}
	}
	else
	{
		exhSensorBoard = EXH_SENSOR_BOARD_NEW;
		RETAILMSG(1, (L"New Sensor Board Detected\r\n"));
	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReadSerialNumber
//
//@ Interface-Description
//  Return the saved the serial number and its read status from the exhalation flow sensor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
bool ExhCommunicationInterface::readSerialNumber(Uint32& rserialNumber)
{
	rserialNumber = serialNumber;
	return bSerialNumberReadError;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: retrieveSerialNumber
//
//@ Interface-Description
//  retrieve the serial number from the exhalation flow sensor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ExhCommunicationInterface::retrieveSerialNumber()
{
	Uint16 numberOfBytes = 0;
	Uint32 numberOfBytesRead;
	Uint32 numberOfBytesWritten;
	Uint8 command = DATA_CMD_80;
	Uint8 buf[4];
	bSerialNumberReadError = true;


	if(exhSensorBoard == EXH_SENSOR_BOARD_NEW)
	{
		numberOfBytes = bytesInReceiveBuffer();
		if(numberOfBytes > 0)
			numberOfBytesRead = read(buf, numberOfBytes, 0);
		
		memset(buf, 0, sizeof(buf));	

		write(&command, 1, 0);

		
		rSerialPort_.wait(100);
		for(Uint16 i= 0; i<3; i++)
		{
			numberOfBytes = bytesInReceiveBuffer();
			if(numberOfBytes == 4)
			{
				read(buf, numberOfBytes, 0);
				//TODO E600 currently new exhalation flowsenor does not have any
				//option to check if the recieved serial number is a valid or not.
				serialNumber = buf[0]<<24 | buf[1]<<16 | buf[2]<<8 | buf[3];
				RETAILMSG(1, (L"Exhalation Flow Sensor Serial Number = %d\r\n", serialNumber));
				bSerialNumberReadError = false;
			}
			rSerialPort_.wait(10);
		}
	}
	else
	{
		//TODO E600 remove this code when the old exhalation sensor support is retired.
		serialNumber = 0;
		bSerialNumberReadError = false;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DetectSensorBoard
//
//@ Interface-Description
//  Determine if communication with the sensor board is successful with the specified baudrate
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean ExhCommunicationInterface::SetSensorBoardBaudRate(Uint32 baudRate, Uint8 command)
{
	Uint16 numberOfBytes = 0;
	Uint32 numberOfBytesRead;
	Uint32 numberOfBytesWritten;
	Boolean sensorBoardBaudRateAccepted = false;
	Boolean baudrateChangeStatus;
	Uint8 buf[20];

	numberOfBytes = bytesInReceiveBuffer();
	if(numberOfBytes > 0)
		numberOfBytesRead = read(buf, numberOfBytes, 0);
	
	memset(buf, 0, sizeof(buf));	
	baudrateChangeStatus = SetCommunicationBaudRate(baudRate);

	numberOfBytes = write(&command, 1, 0);

	rSerialPort_.wait(100);
	for(int i= 0; i<100; i++)
	{
		numberOfBytes = bytesInReceiveBuffer();
		if(numberOfBytes > 0)
		{
			numberOfBytesRead = read(buf, numberOfBytes, 0);

			if(numberOfBytes == 3)
			{
				sensorBoardBaudRateAccepted = true;
 				break;
			}
		}
		rSerialPort_.wait(10);
	}
	return sensorBoardBaudRateAccepted;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSensorBoardType
//
//@ Interface-Description
//  Returns sensor board type
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExhSensorBoard_t ExhCommunicationInterface::GetSensorBoardType()
{
	return exhSensorBoard;
}