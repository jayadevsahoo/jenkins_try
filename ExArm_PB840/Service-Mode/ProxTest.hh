#ifndef ProxTest_HH
#define ProxTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ProxTest -- Perform PROX self test.
// ---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ProxTest.hhv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: mnr   Date: 04-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		Method parameters updated.
//
//  Revision: 004   By: mnr   Date: 03-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		AutozeroAndLeak and Pressure Cross check test related updates.
//
//  Revision: 003  By:  mnr    Date:  28-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//        ProxSstSubTestId fixed.
//
//  Revision: 002  By:  mnr    Date:  13-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//        More functionality added.
//
//  Revision: 001  By:  mnr    Date:  24-Mar-2010   SCR Number: 6436
//	Project:  PROX4
//	Description:
//		Initial check in.
//
//====================================================================

#include "ServiceMode.hh"
#include "SmDataId.hh"

//@ Usage-Classes
//@ End-Usage

class ProxTest
{
  public:

  //@ Type: ProxSstTestId
	// List of all PROX SST sub-tests
	enum ProxSstSubTestId
	{
		PX_TEST_NOT_START_ID = 0,
		PX_BAROMETRIC_PRESS_CC_TEST_ID = 1,
		PX_AUTO_ZERO_LEAK_TEST_ID = 2,
		PX_PURGE_TEST_ID = 3,
		PX_PRESSURE_CC_TEST_ID = 4,
		PX_FLOW_CC_TEST_ID = 5,
		NUM_PROX_TEST_ID = 6
	};

    ProxTest(void) ;
    ~ProxTest(void) ;

    static void SoftFault(const SoftFaultID	softFaultID,
						  const Uint32		lineNumber,
						  const char			*pFileName  = NULL,
						  const char			*pPredicate = NULL) ;

    void sstTest(void);
	Boolean doSensorTest(void);
    Boolean isProxTestEnabled(void);

  protected:

  private:
    ProxTest(const ProxTest&) ;		// not implemented...
    void operator=(const ProxTest&) ;	// not implemented...

	//setup and connectivity tests
	Boolean doCommTest_(void);
    Boolean doDemoModeSquareWaveTest_(void);
	Boolean doSensorTest_(void);

	//more extensive actual tests
	Boolean doBarometricPressureTest_(void);
	Boolean doAutoZeroAndLeakTest_(void);
	Boolean doPurgeTest_(void);
	Boolean doPressureCrossCheckTest_(void);
	Boolean	doFlowSensorCrossCheckTest_(void);

	//helper methods
	Boolean runAutoZeroTest_(void);
	Boolean pressurizeSystemTest_(Real32 targetPressure);
	Boolean pressureTest_(Real32 targetPressure);
	Boolean runPurgeTest_(Uint8 testNum);
	void simIETransition_(Uint32 delayInMs);
    Boolean waitForProxStableFlow_(Real32 flowTarget, Uint32 timeoutMS);

	//@ Data-Member: proxCurrentTestIndex_
	// The current PROX sub-test id.
	Uint32 proxCurrentSubTestIndex_;

	// Constant: MAX_PROX_FLOW_CROSS_CHECK_TESTS_
	// Maximum number of prox flow cross check tests
	enum { MAX_PX_FLOW_CROSS_TESTS_ = 3 };

	// Constant: MAX_PX_PURGE_TESTS_
	// Maximum number of prox flow cross check tests
	enum { MAX_PX_PURGE_TESTS_ = 2 };

	struct ProxFlowCrossTests
    {
	   SmDataId::ItemizedTestDataId proxflowSmDataId;
	   SmDataId::ItemizedTestDataId ventflowSmDataId;
	   Real32 flowValue;
	   Real32 tolerances;
	};

	struct ProxPurgeTests
    {
	   SmDataId::ItemizedTestDataId pressSmDataId;
	   SmDataId::ItemizedTestDataId minPressSmDataId;
	   Real32 accumatorPressure;
	   Real32 expectedMinPress;
	};

	//@ Data-Member: proxFlowCrossTests
	// Stores test data for each prox flow cross check test.
	ProxFlowCrossTests proxFlowCrossTests_[MAX_PX_FLOW_CROSS_TESTS_];

	//@ Data-Member: proxPurgeTests_
	// Stores test data for each prox purge test.
	ProxPurgeTests proxPurgeTests_[MAX_PX_PURGE_TESTS_];

	//@ Data-Member: isProxTestEnabled_
	// Determines whether to execute this test or not.
	Boolean isProxTestEnabled_;

	//@ Data-Member: skipSensorTest_
	// Indicates to skip the sensor test.
	Boolean skipSensorTest_;

} ;


#endif // ProxTest_HH

