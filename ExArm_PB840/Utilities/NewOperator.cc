#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Filename:  NewOperator - Global Placement New Operator.
//---------------------------------------------------------------------
//@ Interface-Description
//  This defines a global, placement new operator.  This new operator
//  is used to run a constructor on a given block of memory.
//---------------------------------------------------------------------
//@ Rationale
//  This function will be used extensively to initialize static class
//  instances, therefore this is provided for the extra checking
//  of the pointer against 'NULL'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Standard placement-new implementation used.
//---------------------------------------------------------------------
//@ Fault-Handling
//  A standard pre-condition test is used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/NewOperator.ccv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "NewOperator.hh"
#include "UtilityClassId.hh"
#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  External Functions...
//
//=====================================================================

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  operator new(, pMemory)  [Placement New Operator]
//
//@ Interface-Description
//  Run a given constructor on 'pMemory'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (pMemory != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//  ((new (pMemory) Type(...)) != NULL)
//@ End-Free-Function
//=====================================================================

void*
operator new(size_t, void* pMemory)
{
  CALL_TRACE("::operator new(size, pMemory)");
  FREE_PRE_CONDITION((pMemory != NULL), UTILITIES, PLACEMENT_NEW);

  return(pMemory);
}  // $[TI1]
