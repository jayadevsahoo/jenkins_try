#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 
 
//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: PostResultIterator - Iterator for Kernel NOVRAM Post Results
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides an iterator for POST results data located in 
//  NOVRAM.  The caller contructs the iterator which returns results
//  for each test that didn't pass.  Persistent-Objects subsystem 
//  uses this class to transfer POST results to the System Diagnostic
//  log.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides external access to POST result data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST/vcssrc/PostResultIterator.ccv   25.0.4.0   19 Nov 2013 14:17:06   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 002    By: Gary Cederquist  Date: 15-MAY-1997  DR Number: 2085
//    Project: Sigma (R8027)
//    Description:
//       Added accessors to POST auxiliary result data.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
#include "PostResultIterator.hh"
#include "postnv.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PostResultIterator [constructor]
//
//@ Interface-Description
//  PostResultIterator constructor.  Starts the iterator at the null 
//  test with result PASSED.  The first call to the "next" method returns
//  the first non-PASSED test and result.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PostResultIterator::PostResultIterator() 
    :  testNumber_(0)
     , testResult_(PASSED)
     , pAuxResult_(NULL)
{
    CALL_TRACE("PostResultIterator()");
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PostResultIterator [destructor]
//
//@ Interface-Description
//  PostResultIterator destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PostResultIterator::~PostResultIterator()
{
    CALL_TRACE("~PostResultIterator()");
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: next
//
//@ Interface-Description
//  Sets the iterator data to the next failed test in the POST NOVRAM
//  results log.  Returns TRUE when a failed test is available.  Returns
//  FALSE when no more failed tests are located in the results log.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
Boolean
PostResultIterator::next()
{
    CALL_TRACE("next()");
#ifdef SIGMA_DEVELOPMENT
    if (PKernelNovram->versionCode == KERNEL_NOVRAM_VERSION)
    {
#endif
        while ( ++testNumber_ < countof(PKernelNovram->results) )
        {
            if ((testResult_ = (Criticality)PKernelNovram->results[testNumber_])
                != PASSED )
            {
               // $[TI2]
               pAuxResult_
               = AuxResultLog::GetPostAuxResultLog().getLastResult(testNumber_);

               return TRUE;
            }
            // $[TI3]  else
        }
#ifdef SIGMA_DEVELOPMENT
    }
#endif

    testResult_ = PASSED;
    testNumber_ = 0;

    // $[TI1]
    return FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTestNumber
//
//@ Interface-Description
//  Returns the current test number in the iterator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
Uint8
PostResultIterator::getTestNumber() const
{
    CALL_TRACE("getTestNumber()");
    // $[TI1]
    return testNumber_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTestResult
//
//@ Interface-Description
//  Returns the current test result in the iterator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
Criticality
PostResultIterator::getTestResult() const
{
    CALL_TRACE("getTestResult()");
    // $[TI1]
    return testResult_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAuxResult
//
//@ Interface-Description
//  Returns the current AuxResult in the iterator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
const AuxResult *
PostResultIterator::getPAuxResult() const
{
    CALL_TRACE("getAuxPResult()");
    // $[TI1]
    return pAuxResult_;
}

