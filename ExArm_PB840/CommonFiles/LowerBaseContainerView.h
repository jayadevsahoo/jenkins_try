#pragma once

#include "BaseContainerView.h"

// CLowerBaseContainerView view

class CLowerBaseContainerView : public BaseContainerView
{
	DECLARE_DYNCREATE(CLowerBaseContainerView)
private:

public:
	void CreateLowerBaseViewCDC();
	LONG GetOriginY();

protected:
	CLowerBaseContainerView();           // protected constructor used by dynamic creation
	virtual ~CLowerBaseContainerView();

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
public:

};


