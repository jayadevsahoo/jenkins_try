
#ifndef SafeStatePhase_HH
#define SafeStatePhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SafeStatePhase - Implements safe state phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SafeStatePhase.hhv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: syw   Date: 16-Aug-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added method updateFlowControllerFlags_().
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

#include "BreathPhase.hh"

//@ Usage-Classes
//@ End-Usage

class SafeStatePhase : public BreathPhase
{
  public:
    SafeStatePhase( void) ;
    virtual ~SafeStatePhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

    virtual void newCycle(void) ;
    virtual void newBreath(void) ;
  
  protected:

	virtual void updateFlowControllerFlags_( void) ;

  private:
    SafeStatePhase( const SafeStatePhase&) ;	// not implemented...
    void   operator=( const SafeStatePhase&) ;	// not implemented...

};


#endif // SafeStatePhase_HH 
