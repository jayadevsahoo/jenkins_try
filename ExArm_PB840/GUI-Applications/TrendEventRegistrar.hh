#ifndef TrendEventRegistrar_HH
#define TrendEventRegistrar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendEventRegistrar 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendEventRegistrar.hhv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  ksg    Date:  18-June-2007    SCR Number: 6237
//  Project:  Trend
//  Description: Initial version.
// 
//====================================================================


#include "GuiApp.hh"
#include "GuiAppClassIds.hh"

// Forward declare
class TrendEventTarget;

class TrendEventRegistrar
{
public:


	static void TrendDataReady(Uint32 dataSetAddr);
	static void TrendManagerStatus( Boolean isEnabled, Boolean isRunning );
	static void RegisterTarget(TrendEventTarget* pTarget);
	static void Initialize(void);
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:
	// these methods are purposely declared, but not implemented...
	TrendEventRegistrar(void);						// not implemented...
	TrendEventRegistrar(const TrendEventRegistrar&);	// not implemented...
	~TrendEventRegistrar(void);						// not implemented...
	void operator=(const TrendEventRegistrar&);		// not implemented...

	enum
	{
		//@ Constant: MAX_TARGETS_
		// The maximum number of target pointers that can be stored
		MAX_TARGETS_ = 5
	};

	//@ Data-Member: TargetArray_
	// A static array of TrendEventTarget classes
	static TrendEventTarget *TargetArray_[MAX_TARGETS_];

	//@ Data-Member: NumTargets_
	// The number of targets currently registered with this class.
	static Int32 NumTargets_;
};

#endif // TrendEventRegistrar_HH 
