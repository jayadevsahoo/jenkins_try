#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 2008, Nellcor Puritan Bennett Corporation
//=====================================================================

//====================================================================
//@ Class:  SensorDataBuffer
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is a specialize SerialBuffer that buffers serial port data 
//  to the Serial-Interface task from Gui application task.
//  It contains the packet memory and packet map storage required by 
//  the base class SerialBuffer. All methods for this class are located 
//  in the base class.
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates Pressure and Flow reading in to a Buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SensorDataBuffer.ccv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: ERM       Date: 5-Feb-2008  DCS Number: 6441
//  Project: 840S
//  Description:
//  Initial Revision.
//
//
//====================================================================
 
#include "SensorDataBuffer.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SensorDataBuffer[constructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
SensorDataBuffer::SensorDataBuffer( void )
    :   SerialBuffer(  SERIAL_INPUT_MT
	                 , packetMemory_
	                 , SENSOR_DATA_SIZE
	                 , SENSOR_HOLD_COUNT  
	                 , packetMap_ )
{
	//$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SensorDataBuffer [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
SensorDataBuffer::~SensorDataBuffer()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
SensorDataBuffer::SoftFault(const SoftFaultID softFaultID,
	                         const Uint32      lineNumber,
	                         const char*       pFileName,
	                         const char*       pPredicate)
{
  FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE, 
	  GUI_Serial_Interface::SENSOR_DATA_BUFFER_CLASS,
      lineNumber, pFileName, pPredicate);
}


