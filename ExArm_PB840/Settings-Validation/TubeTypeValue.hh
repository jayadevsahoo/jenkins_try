
#ifndef TubeTypeValue_HH
#define TubeTypeValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  TubeTypeValue - Enumerated values for Tube Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TubeTypeValue.hhv   25.0.4.0   19 Nov 2013 14:27:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  06-Jan-1999    DR Number:  5322
//  Project:  ATC
//  Description:
//	New ATC-specific setting values.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct TubeTypeValue
{
  //@ Type:  TubeTypeValueId 
  enum TubeTypeValueId
  {
    // $[TC02035] -- setting values...
    ET_TUBE_TYPE,
    TRACH_TUBE_TYPE,

    TOTAL_TUBE_TYPE_VALUES
  };
};


#endif // TubeTypeValue_HH 
