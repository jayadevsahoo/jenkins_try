#ifndef GasSupply_HH
#define GasSupply_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: GasSupply - Monitoring the gas supply, wall air and O2
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/GasSupply.hhv   25.0.4.0   19 Nov 2013 13:54:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  erm   Date:  10-Aug-2009    DR Number: 6424
//  Project:  840
//  Description:
//		Added debouncer to O2 and air gas switch
//
//  Revision: 003  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//  Project:  840
//  Description:
//		Eliminate callbacks since EventFilter class called explicitly.
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

#include "BdAlarmId.hh"
#include "BinaryIndicator.hh"
#include "Debouncer.hh"

#define MIN_PSI_REQUIRED			9
#define MIN_RECOVERY_PSI_REQUIRED	11

//@ Usage-Classes
//@ End-Usage

class GasSupply
{
    public:

        typedef void (*PSupplyChangeCallBack) (const GasSupply& gasSupply);

        GasSupply(void);
        ~GasSupply(void);

        static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL,
							   const char*       pPredicate = NULL );

		Boolean isAirPresent(void) const;
        Boolean isTotalAirPresent(void) const;
		Boolean isO2Present(void) const;
        Boolean checkAirPresence(void);
        Boolean checkO2Presence(void);

		void setAirBkgndFail( const Boolean airStatus );
		void setO2BkgndFail( const Boolean o2Status );

		Boolean hasAirBkgndFailed( void ) const;
		Boolean hasO2BkgndFailed( void ) const;

        void registerCallBack( PSupplyChangeCallBack pSupplyChangeCallBack );

        void newCycle( void );
        void initialize( void );
        void initialStatusNotification( void );


    protected:

    private:

        GasSupply( const GasSupply& );           // not implemented...
        void operator=( const GasSupply& );    // not implemented...

		Boolean CheckGas( Boolean currStatus, Uint numCyclestoTransition, 
			Uint& numCyclesRecovered, Uint& numCyclesNotPresent, 
			Real32 inletPressure );

		Real32 GetAirInletPressure( void );
		Real32 GetO2InletPressure( void );

		
		//@Constant: GAS_DETECTION_DEBOUNCE_CYCLES
		// how long the signal need to be stable
		static const Uint8 GAS_DETECTION_DEBOUNCE_CYCLES = 20;    // number of 5 msec cycles in 100 ms	

        //@ Data-Member: isAirPresent_
        // indicates if air is available for
    	// the current cycle
        Boolean isAirPresent_;

        //@ Data-Member: isO2Present_
        // indicates if o2 is available for
    	// the current cycle
        Boolean isO2Present_;

        //@ Data-Member: isTotalAirPresent_
        // indicates if air (compressed or wall)
    	// is available for the current cycle
        Boolean isTotalAirPresent_;

        //@ Data-Member: o2BkgndFailureReported_
        // indicates whether O2 supply failure has
    	// been reported to O2Mixture class
        Boolean o2BkgndFailureReported_;

        //@ Data-Member: airBkgndFailureReported_
        // indicates whether air supply failure has
    	// been reported to O2Mixture class
        Boolean airBkgndFailureReported_;

        //@ Data-Member: airBkgndFail_
        // indicates if air supply failed backgound test
        Boolean airBkgndFail_;

        //@ Data-Member: o2BkgndFail_
        // indicates if O2 supply failed backgound test
        Boolean o2BkgndFail_;

        //@ Data-Member: pSupplyChangeCallBack_
        // call back reference for users of GasSupply class
        PSupplyChangeCallBack pSupplyChangeCallBack_;


};

// Inlined methods...
#include "GasSupply.in"

#endif // GasSupply_HH

