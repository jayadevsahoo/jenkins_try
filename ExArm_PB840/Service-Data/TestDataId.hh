
#ifndef TestDataId_HH
#define TestDataId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//  TestDataId - enum ids for all the Test data items.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/TestDataId.hhv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================


#include "Sigma.hh"

class TestDataId
{
public:
	
	//@ Type: TestDataItemId
	// This enum lists all of the test data id that may be accessed
	// through this class.  There three groups of data: COMMAND, STATUS,
	// and DATA type.  The enums declared for the DATA type group
	// is designed such as the enum for each datum is generic to all
	// service test.  It can be reused by all the possible service test.
	enum TestDataItemId
	{
		NULL_TEST_DATA_ITEM   = -1,

		START_TEST_DATA_ITEM,

		FIRST_PROMPT_DATA_ITEM = START_TEST_DATA_ITEM,
			TEST_PROMPT_DATA_ITEM = FIRST_PROMPT_DATA_ITEM,
			TEST_KEY_ALLOWED_DATA_ITEM,
	    LAST_PROMPT_DATA_ITEM = TEST_KEY_ALLOWED_DATA_ITEM,
		
		FIRST_STATUS_DATA_ITEM,
			TEST_STATUS_START_TEST = FIRST_STATUS_DATA_ITEM,
			TEST_STATUS_CONDITION_1,
			TEST_STATUS_CONDITION_2,
			TEST_STATUS_CONDITION_3,
			TEST_STATUS_CONDITION_4,
			TEST_STATUS_CONDITION_5,
			TEST_STATUS_CONDITION_6,
			TEST_STATUS_CONDITION_7,
			TEST_STATUS_CONDITION_8,
			TEST_STATUS_CONDITION_9,
			TEST_STATUS_CONDITION_10,
			TEST_STATUS_CONDITION_11,
			TEST_STATUS_CONDITION_12,
			TEST_STATUS_CONDITION_13,
			TEST_STATUS_CONDITION_14,
			TEST_STATUS_CONDITION_15,
			TEST_STATUS_CONDITION_16,
			TEST_STATUS_CONDITION_17,
			TEST_STATUS_CONDITION_18,
			TEST_STATUS_CONDITION_19,
			TEST_STATUS_CONDITION_20,
		SM_STATUS_DATA_ITEM = TEST_STATUS_CONDITION_20,
			TEST_STATUS_END_TEST,
			TEST_STATUS_NOT_INSTALLED_TEST,
			TEST_STATUS_OPERATOR_EXIT_TEST,
		LAST_STATUS_DATA_ITEM = TEST_STATUS_OPERATOR_EXIT_TEST,

		FIRST_DATA_ITEM,
	    	TEST_DATA_1_ITEM = FIRST_DATA_ITEM,
    		TEST_DATA_2_ITEM,
    		TEST_DATA_3_ITEM,
	    	TEST_DATA_4_ITEM,
    		TEST_DATA_5_ITEM,
    		TEST_DATA_6_ITEM,
	    	TEST_DATA_7_ITEM,
    		TEST_DATA_8_ITEM,
    		TEST_DATA_9_ITEM,
	    	TEST_DATA_10_ITEM,
	    	TEST_DATA_11_ITEM,
    		TEST_DATA_12_ITEM,
    		TEST_DATA_13_ITEM,
	    	TEST_DATA_14_ITEM,
    		TEST_DATA_15_ITEM,
    		TEST_DATA_16_ITEM,
	    	TEST_DATA_17_ITEM,
    		TEST_DATA_18_ITEM,
    		TEST_DATA_19_ITEM,
	    	TEST_DATA_20_ITEM,
	    	TEST_DATA_21_ITEM,
    		TEST_DATA_22_ITEM,
    		TEST_DATA_23_ITEM,
	    	TEST_DATA_24_ITEM,
    		TEST_DATA_25_ITEM,
    		TEST_DATA_26_ITEM,
	    	TEST_DATA_27_ITEM,
    		TEST_DATA_28_ITEM,
    		TEST_DATA_29_ITEM,
	    	TEST_DATA_30_ITEM,
	    	TEST_DATA_31_ITEM,
    		TEST_DATA_32_ITEM,
    		TEST_DATA_33_ITEM,
	    	TEST_DATA_34_ITEM,
    		TEST_DATA_35_ITEM,
    		TEST_DATA_36_ITEM,
    		TEST_DATA_37_ITEM,
    		TEST_DATA_38_ITEM,
    		TEST_DATA_39_ITEM,
    		TEST_DATA_40_ITEM,
    		TEST_DATA_41_ITEM,
    		TEST_DATA_42_ITEM,
    		TEST_DATA_43_ITEM,
	    	TEST_DATA_44_ITEM,
    		TEST_DATA_45_ITEM,
    		TEST_DATA_46_ITEM,
	    LAST_DATA_ITEM = TEST_DATA_46_ITEM,

	    NUM_TEST_DATA_ITEM
	};

	//@ Type: MaxDataEntries
	// This enum lists the maximun number of prompt, status, and data
	// available.
	enum MaxDataEntries
	{
		MAX_PROMPT_ENTRIES = LAST_PROMPT_DATA_ITEM - FIRST_PROMPT_DATA_ITEM + 1,
		MAX_SM_STATUS_ENTRIES = SM_STATUS_DATA_ITEM - FIRST_STATUS_DATA_ITEM + 1,
		MAX_STATUS_ENTRIES = LAST_STATUS_DATA_ITEM - FIRST_STATUS_DATA_ITEM + 1,
		MAX_DATA_ENTRIES = LAST_DATA_ITEM - FIRST_DATA_ITEM + 1 
	};

};

#endif // TestDataId_HH 
