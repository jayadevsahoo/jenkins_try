#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: SafetyNetTestMediator - Runs Safety net tests and checks
//                                 status every new cycle.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is used to check the status of the sensor range safety
//  net checks by invoking the getBackgndEventId() method of the
//  SafetyNetSensorData object for each sensor.  If the returned
//  event identifier indicates that a sensor is out of range, the
//  Background::ReportBkEvent() method is invoked.
//
//  This class also runs a series of BD runtime safety net checks
//  and BD runtime safety net range checks that depend upon more
//  information than a single sensor reading. Each of the tests in
//  the SafetyNetTest and SafetyNetRangeTest classes are executed and
//  the Background subsystem is notified of any background events.
//
//  This class also provides an interface method (logBdSafetyNetEvent)
//  to the Background subsystem for other classes
//  (eg PressureXducerAutozero) to use.
//---------------------------------------------------------------------
//@ Rationale
//  Used to run BD safety net checks that depend upon more than a
//  single sensor reading.  Also used to provide a single interface
//  between BD and the Background subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An array of pointers (pBdSafetyNetTests_) to each of the
//  SafetyNetTest derived objects is set up in the constructor.
//  An array of pointers (pBdSafetyNetRangeTests_) to each of the
//  SafetyNetRangeTest derived objects is set up in the constructor.
//  Each new BD secondary cycle, the the newCycle() method checks the
//  status of each of the sensor out of range checks and reports
//  any failures to the Background subsystem.  The newCycle()
//  method then invokes the checkCriteria() method of each of
//  the SafetyNetTests and SafetyNetRangeTests and reports any
//  failures to the Background subsystem.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SafetyNetTestMediator.ccv   25.0.4.0   19 Nov 2013 14:00:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: gdc   Date:  18-Aug-2009    SCR Number: 6147
//  Project:  XB
//	Description:
//		Report BK Forced Vent-Inop instead of a failed safety-net check
//		caused by the forced vent-inop condition. This is to prevent
// 		prevents false indications of vent component failures when 
// 		forced into vent-inop by the GUI monitoring function.
//
//  Revision: 006  By: syw   Date:  21-Mar-2000    DR Number: 5611
//  Project:  NeoMode
//	Description:
//		Eliminate SvSwitchSideRangeTest class.
//
//  Revision: 005  By: iv     Date:  03-May-1999    DR Number: 5375
//  Project:  ATC
//  Description:
//      Added error code report to autozero faults.
//
//  Revision: 004  By: sah    Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added an error code to ReportBkEvent().
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  20-Sep-1995    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "SafetyNetTestMediator.hh"

#include "SafetyNetRefs.hh"
#include "MainSensorRefs.hh"
#include "MiscSensorRefs.hh"
#include "VentObjectRefs.hh"

//@ Usage-Classes

#include "Sensor.hh"
#include "SensorMediator.hh"
#include "SafetyNetTest.hh"
#include "SafetyNetRangeTest.hh"
#include "SafeState.hh"
#include "PostDefs.hh"
#include "O2PsolStuckOpenTest.hh"
#include "AirPsolStuckOpenTest.hh"
#include "GasSupply.hh"

#include "AirFlowSensorTest.hh"
#include "O2FlowSensorTest.hh"
#include "InspPressureSensorTest.hh"
#include "ExhPressureSensorTest.hh"
#include "AirPsolStuckTest.hh"
#include "O2PsolStuckTest.hh"
#include "PeStuckTest.hh"
#include "PiStuckTest.hh"
#include "InspPressureSensorTest.hh"
#include "ExhPressureSensorTest.hh"
#include "SvOpenedLoopbackTest.hh"
#include "TaskControlAgent.hh"

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SafetyNetTestMediator()
//
//@ Interface-Description
//  Constructor.  Sets up a list of pointers to the safety net tests
//  and safety net range tests that are executed by this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize the pointers to the BD runtime safety net tests and
//  range tests.
//---------------------------------------------------------------------
//@ PreCondition
//  The number of BD runtime safety net tests is NUM_BD_SAFETY_NET_TESTS
//  and the number of range tests is NUM_BD_SAFETY_NET_RANGE_TESTS.
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

SafetyNetTestMediator::SafetyNetTestMediator( void )
{
	// $[TI1]
	CALL_TRACE("SafetyNetTestMediator::SafetyNetTestMediator (void)") ;

	Int16 ii = 0 ;

	//assign tests to the list pBdSafetyNetTests_
	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RAirPsolStuckOpenTest;
	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RAirPsolStuckTest;

	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RO2PsolStuckOpenTest;
	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RO2PsolStuckTest;

	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RPeStuckTest;
	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RPiStuckTest;
	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RInspPressureSensorTest;
	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RExhPressureSensorTest;

	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RExhFlowSensorFlowTest;
	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RAirFlowSensorTest;
	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RO2FlowSensorTest;

	pBdSafetyNetTests_[ii++] = (SafetyNetTest*) &RSvOpenedLoopbackTest;

	CLASS_PRE_CONDITION( ii == NUM_BD_SAFETY_NET_TESTS) ;

	ii = 0;

	//assign tests to the list pBdSafetyNetRangeTests_
	pBdSafetyNetRangeTests_[ii++] = (SafetyNetRangeTest*) &RSvClosedLoopbackTest; 

	CLASS_PRE_CONDITION( ii == NUM_BD_SAFETY_NET_RANGE_TESTS) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SafetyNetTestMediator()
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

SafetyNetTestMediator::~SafetyNetTestMediator (void)
{
	CALL_TRACE("SafetyNetTestMediator::~SafetyNetTestMediator (void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//  This method takes no arguments and has no return value.
//  It first cycles through the list of safety net sensor data to check if
//  any of the sensor OOR (out of range) tests failed.  If a test has
//  failed, the Background subystem is notified.  Next, this method
//  cycles through the list of safety net tests and safety net range
//  tests invoking the checkCriteria() method for the test
//  and notifies the Background subsystem if any tests have failed.
//
//  The Background subystem notifies the Alarm subsystem of any
//  Background events.  The Background subsystem may also
//  put the ventilator into a safe state depending upon which
//  background test failed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  For each sensor data object, the getBackgndEventId() method is
//  called to determine if one of the sensor OOR checks failed.  If
//  so, the takeAction_ method is called to perform any necessary
//  response (eg. switch gas supplies) and Background::ReportBkEvent()
//  is called.
//
//  For each BD run time safety net test and range test, the
//  checkCriteria() method is called, the takeAction_ method is called
//  to perform any necessary response, and Background::ReportBkEvent()
//  is called if the background failure criteria are met.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SafetyNetTestMediator::newCycle( void ) const 
{
	BkEventName backgndEventId;

	CALL_TRACE("SafetyNetTestMediator::newCycle (void)");

	if ( TaskControlAgent::GetBdState() == (SigmaState) STATE_ONLINE )
	{
		// $[TI1]
		/////////////////////////////////////////////////////////////////
		// First check if any of the sensor tests failed.  If so, notify
		// the background subsystem.  These checks must be done before the
		// SafetyNetTest background checks are done.
		/////////////////////////////////////////////////////////////////

		SafetyNetSensorData* pSafetyNetSensorData;
		Uint8 sensorNdx = 0;

		for ( sensorNdx = 0;
			sensorNdx < SensorMediator::MAX_PRIMARY_SENSORS; ++sensorNdx )
		{
			// $[TI1.1]

			pSafetyNetSensorData = RSensorMediator.
								   pPrimarySensorsGroup_[ sensorNdx ]->getSafetyNetSensorDataAddr();

			//////////////////////////////////////////////////////////////
			// if there is not safety net data associated with a sensor,
			// the getSafetyNetSensorDataAddr() returns NULL
			//////////////////////////////////////////////////////////////
			if ( pSafetyNetSensorData != NULL )
			{
				// $[TI1.1.1]

				backgndEventId = pSafetyNetSensorData->getBackgndEventId();

				if ( backgndEventId != BK_NO_EVENT )
				{
					// $[TI1.1.1.1]
					/////////////////////////////////////////////////
					// Perform any required additonal activity;
					// for instance, if the O2 flow sensor
					// is determined to be faulty, the ventilator
					// should switch and use only the air supply
					/////////////////////////////////////////////////
					takeAction_( backgndEventId );

					if (TestVentInop() == ::SERVICE_REQUIRED)
					{
						Background::ReportBkEvent(::BK_FORCED_VENTINOP);
					}
					else
					{
						Uint16 errorCode = pSafetyNetSensorData->getLastRawCountReading();
						// Notify the background subsystem of a failed test
						Background::ReportBkEvent( backgndEventId, errorCode );
					}
				}
				// $[TI1.1.1.2]
			}
			// $[TI1.1.2]
		}

// E600 BDIO
#if 0
		for ( sensorNdx = 0;
			sensorNdx < SensorMediator::MAX_SECONDARY_SENSORS; ++sensorNdx )
		{
			// $[TI1.2]

			pSafetyNetSensorData = RSensorMediator.
								   pSecondarySensorsGroup_[ sensorNdx ]->getSafetyNetSensorDataAddr();

			// if there is not safety net data associated with a sensor,
			// the getSafetyNetSensorDataAddr() returns NULL
			if ( pSafetyNetSensorData != NULL )
			{
				// $[TI1.2.1]

				backgndEventId = pSafetyNetSensorData->getBackgndEventId();

				if ( backgndEventId != BK_NO_EVENT )
				{
					// $[TI1.2.1.1]

					/////////////////////////////////////////////////
					// Perform any required additonal activity;
					// for instance, if the O2 flow sensor
					// is determined to be faulty, the ventilator
					// should switch and use only the air supply
					/////////////////////////////////////////////////
					takeAction_( backgndEventId );

					if (TestVentInop() == ::SERVICE_REQUIRED)
					{
						Background::ReportBkEvent(::BK_FORCED_VENTINOP);
					}
					else
					{
						Uint16 errorCode = pSafetyNetSensorData->getLastRawCountReading();
						// Notify the background subsystem of a failed test
						Background::ReportBkEvent( backgndEventId, errorCode );
					}
				}
				// $[TI1.2.1.2]
			}
			// $[TI1.2.2]
		}
#endif

		// check low voltage reference for background failures
		pSafetyNetSensorData = RLowVoltageReference.getSafetyNetSensorDataAddr();
		backgndEventId = pSafetyNetSensorData->getBackgndEventId();

		if ( backgndEventId != BK_NO_EVENT )
		{
			// $[TI1.3]
			Uint16 errorCode = pSafetyNetSensorData->getLastRawCountReading();
			// Notify the background subsystem of a failed test
			Background::ReportBkEvent( backgndEventId, errorCode );
		}
		// $[TI1.4]

		// run the BD safety net runtime checks
		for ( sensorNdx = 0; sensorNdx < NUM_BD_SAFETY_NET_TESTS; ++sensorNdx )
		{
			// $[TI1.5]

			backgndEventId = pBdSafetyNetTests_[ sensorNdx ]->checkCriteria();

			if ( backgndEventId != BK_NO_EVENT )
			{
				// $[TI1.5.1]
				// Perform any required additonal activity; for instance, if the
				// O2 flow sensor is OOR, the ventilator should switch and use
				// only the air supply
				takeAction_( backgndEventId );

				// if this background test failed because we're in vent-inop
				// then report it as a forced vent-inop condition instead of another
				// type of safety-net test failure
				if ( TestVentInop() == ::SERVICE_REQUIRED )
				{
					Background::ReportBkEvent(::BK_FORCED_VENTINOP);
				}
				else
				{
					Uint16 errorCode = pBdSafetyNetTests_[ sensorNdx ]->getErrorCode();
					// Notify the background subsystem of the failed test
					Background::ReportBkEvent( backgndEventId, errorCode );
				}
			}
			// $[TI1.5.2]
		}

		// run the BD safety net range checks; these are range checks that
		// are either performed only once a breath or depend on some additional
		// data that is not available in the SafetyNetSensorData class.
		for ( sensorNdx = 0; sensorNdx < NUM_BD_SAFETY_NET_RANGE_TESTS; ++sensorNdx )
		{
			// $[TI1.6]

			backgndEventId = pBdSafetyNetRangeTests_[ sensorNdx ]->checkCriteria();

			if ( backgndEventId != BK_NO_EVENT )
			{
				// $[TI1.6.1]
				// if this background test failed because we're in vent-inop
				// then report it as a forced vent-inop condition instead of another
				// type of safety-net test failure
				if ( TestVentInop() == ::SERVICE_REQUIRED )
				{
					Background::ReportBkEvent(::BK_FORCED_VENTINOP);
				}
				else
				{
					Uint16 errorCode = pBdSafetyNetRangeTests_[ sensorNdx ]->getErrorCode();
					// Notify the background subsystem of the failed test
					Background::ReportBkEvent( backgndEventId, errorCode );
				}
			}
			// $[TI1.6.2]
		}
	}
	// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: logBdSafetyNetEvent()
//
//@ Interface-Description
//  This method takes a SafetyNetTestMediator::BdBackgndId as an
//  argument and returns nothing.  It notifies the Background
//  subsystem of the background event via the ReportBkEvent() method.
//---------------------------------------------------------------------
//@ Implementation-Description
//  For each SafetyNetTestMediator::BdBackgndId defined, this
//  method notifies the Background subsystem of the background
//  event.
//
// $[06056]
//---------------------------------------------------------------------
//@ PreCondition
//  bdBackgndId is one of the defined Bd Background Ids
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SafetyNetTestMediator::logBdSafetyNetEvent
(
const SafetyNetTestMediator::BdBackgndId bdBackgndId, const Uint16 errorCode
)
{

	CALL_TRACE("SafetyNetTestMediator::logBdSafetyNetEvent( \
                const SafetyNetTestMediator::BdBackgndId bdBackgndId)");

	switch ( bdBackgndId )
	{
		case SafetyNetTestMediator::PI_AZ_OOR:
			// $[TI1]
			Background::ReportBkEvent( BK_INSP_AUTOZERO_FAIL, errorCode );
			break;

		case SafetyNetTestMediator::PE_AZ_OOR:
			// $[TI2]
			Background::ReportBkEvent( BK_EXH_AUTOZERO_FAIL, errorCode );
			break;

		default:
			// $[TI3]
			CLASS_PRE_CONDITION(
							   (bdBackgndId == SafetyNetTestMediator::PI_AZ_OOR) ||
							   (bdBackgndId == SafetyNetTestMediator::PE_AZ_OOR) );

	}  // end switch    

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//  This method takes four arguments: softFaultID, lineNumber, pFileName,
//  and pPredicate and is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SafetyNetTestMediator::SoftFault( const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;

	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, SAFETYNETTESTMEDIATOR,
							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeAction_ 
//
//@ Interface-Description
//  The method takes a Background Event Identifier as input.  Based
//  on the input, the method determines if any additional action must
//  be performed.  If so, it is initiated from this method.
//
//  The method collaborates with the instance rGasSupply to update the
//  availability of air or oxygen based on the status of background tests.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the Background event is BK_AIR_FLOW_OOR the setAirBkgndFail() method
//  of the GasSupply object is called to record that the air supply
//  (either wall air or compressor) should not be used and
//  therefore the vent should switch to 100% O2.  If the Background event is
//  BK_O2_FLOW_OOR the setO2BkgndFail() method of the GasSupply object is
//  called to record that the O2 supply should not be used and therefore
//  the vent should switch to 21% O2.
//
//  No action is performed if the background event is any other than
//  those specified.  However, it is assumed that this method is only
//  invoked when a background event has been detected.
//---------------------------------------------------------------------
//@ PreCondition
//  backgndEventId != BK_NO_EVENT
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SafetyNetTestMediator::takeAction_( const BkEventName backgndEventId ) const
{

	CALL_TRACE("SafetyNetTestMediator::takeAction_(const BkEventName backgndEventId)");

	if ( backgndEventId == BK_AIR_FLOW_OOR_LOW )
	{
		// $[TI1]
		RGasSupply.setAirBkgndFail( TRUE );
	}
	else if ( backgndEventId == BK_O2_FLOW_OOR_LOW )
	{
		// $[TI2]
		RGasSupply.setO2BkgndFail( TRUE );
	}
	else
	{
		// $[TI3]
		// this method should only be called if a background event has occured
		CLASS_PRE_CONDITION( backgndEventId != BK_NO_EVENT );
	}
}



