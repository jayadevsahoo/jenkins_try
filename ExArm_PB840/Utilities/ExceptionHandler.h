
//#############################################################################
//
//  ExceptionHandler.h - Header file for the ExceptionHandler_t class.
//
//#############################################################################

#ifndef EXCEPTION_HANDLER_H

#define EXCEPTION_HANDLER_H

#include "Sigma.hh"

class ExceptionHandler_t
{
    public:
        ExceptionHandler_t(void);
        ~ExceptionHandler_t(void);

        bool GetExceptionOccurred()
        {
            return ExceptionOccurred;
        }

        bool VentilationStopped()
        {
            return StopVentilation;
        }

        bool GetEventsExceptionOccurred()
        {
            return EventsExceptionOccurred;
        }

        void StopVentilating(int32_t ExceptionCode, char* ThreadName);
        void VentilateWithUi(int32_t ExceptionCode, char* ThreadName);
        void VentilateWithoutUi(int32_t ExceptionCode, char* ThreadName);
        void EventsFailure(int32_t ExceptionCode, char* ThreadName);
      
        int  EvaluateException(LPEXCEPTION_POINTERS p);

    protected:
        void SaveExceptionContext(int32_t ExceptionCode, char* ThreadName);

        EXCEPTION_RECORD    ExceptionRecord;
        CONTEXT             Context;
        bool    ExceptionOccurred;
        bool    StopVentilation;
        bool    EventsExceptionOccurred;
};


#endif

