#include "stdafx.h"

#if defined(SIGMA_GUI_CPU)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: EnglishMiscStrs - Language-independent repository for
// miscellaneous strings displayed on the ventilator screens.
//---------------------------------------------------------------------
//@ Interface-Description
// This class exists for encapsulation purposes only.  All language dependent
// strings that can be displayed on the ventilator (except for prompt strings,
// stored in the PromptStrs class) are static public members of this class and
// can be accessed globally.
//---------------------------------------------------------------------
//@ Rationale
// Convenient encapsulator for strings and provides for language independence.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//
// $[01122] Messages explaining shorthand symbols consist of ...
// $[00638] The language required to be supported at initial ...
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/EnglishMiscStrs.LAv   25.0.4.0   19 Nov 2013 14:07:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 131   By: rhj   Date: 05-Apr-2011    SCR Number: 6759
//  Project:  PROX
//  Description:
//       Added PROX_TOUCHABLE_SPONT_FV_RATIO_LABEL, 
//             PROX_TOUCHABLE_SPONT_FV_RATIO_MSG, 
//             PROX_TOUCHABLE_SPONT_MINUTE_VOL_LABEL,
//             PROX_TOUCHABLE_SPONT_MINUTE_VOL_MSG
// 
//  Revision: 130   By: rhj   Date: 13-Sept-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added flow results during leak test.  
//  
//  Revision: 129  By: rhj     Date: 19-Aug-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//       Fixed mispelled words and changed accumulator text for SST.
// 
//  Revision: 128  By: rhj     Date: 10-Aug-2010   SCR Number: 6638
//  Project:  PROX
//  Description:
//       Added PROX_NOT_AVAILABLE_MSG
//
//  Revision: 127  By: rhj     Date: 19-July-2010   SCR Number: 6612
//  Project:  MAT
//  Description:
//       Remove failure from assertion error log entries.
//
//  Revision: 126   By: rhj   Date: 16-July-2010  SCR Number: 6606
//  Project:  PROX
//  Description:
//		Added PROX_AUTO_ZERO_STATUS_MSG & PROX_PURGE_STATUS_MSG
// 
//  Revision: 125   By: rhj   Date: 16-July-2010    	SCR Number: 6601
//  Project:  PROX
//  Description:
//		Added PROX_ERROR_MESSAGE_TITLE1 & PROX_ERROR_MESSAGE_TITLE2
// 
//  Revision: 124   By: rhj   Date: 08-July-2010    	SCR Number: 6598
//  Project:  PROX
//  Description:
//		Changed reservoir pressure to accumulator pressure
//
//  Revision: 123   By: mnr   Date: 04-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		Added more strings for Prox SST Pressure cross check test.
//
//  Revision: 122   By: mnr   Date: 03-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		Added PX_DISTAL_INSP_PRESSURE_TITLE and PX_WYE_PRESSURE_TITLE.
//	
//  Revision: 121 By: mnr   Date: 26-APR-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Updated MMHG_UNIT_LABEL and PSI_UNIT_LABEL.
//
//  Revision: 120 By: mnr   Date: 16-APR-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added/Updated strings for PROX SST.
//
//  Revision: 119 By: mnr   Date: 13-APR-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added more strings for PROX SST.
//
//  Revision: 118 By: rhj   Date: 22-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added PF symbol for prox flow.
//
//  Revision: 117 By: mnr   Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Serial Number and Revision string related changes for PROX.
//
//  Revision: 116 By: mnr   Date: 17-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      SST, Trending Event and Serial Number related changes for PROX.
//
//  Revision: 115   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 114  By: rhj     Date: 19-July-2010   SCR Number: 6612
//  Project:  MAT
//  Description:
//       Remove failure from assertion error log entries.
//
//  Revision: 113   By: mnr    Date: 01-Mar-2010    SCR Number: 6557
//  Project:  NEO
//  Description:
//      Neomode Advanced and Lockout options renamed.
//  
//  Revision: 112   By: mnr    Date: 23-Feb-2010    SCR Number: 6556
//  Project:  NEO
//  Description:
//      PHILIPS_VALUE_TITLE string added.
// 
//  Revision: 111  By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      New Options and Event strings added.
//
//  Revision: 110  By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      New Preset and Event strings added.
//
//  Revision: 109  By: mnr     Date: 18-Nov-2009   SCR Number: 6517
//  Project:  S840BUILD3
//  Description:
//       Incorporated changes for new symbols H{S:2}O, cmH{S:2}O and O{S:2}.
//
//  Revision: 108  By: gdc    Date:  18-Aug-2009   SCR Number: 6417
//  Project:  XB
//  Description:
//     Added Safety-Net disgnostic messages for revised BD monitor.
//
//  Revision: 107  By:  rhj    Date:  29-Apr-2009    SCR Number: 6504  
//  Project:  840S2
//  Description:
//      Modified SV_KEY_HUNDRED_PERCENT_O2_FAILURE and 
//      SV_BAD_GUI_100_PCT_O2_LED.
// 
//  Revision: 106  By:  rhj    Date:  24-Apr-2009    SCR Number: 6503  
//  Project:  840S2
//  Description:
//      Changed the LEAK_COMP_HELP_MESSAGE's font size to 14.
// 
//  Revision: 105   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S2
//  Description:
//      Modifications to provide for transitioning of tube 
//		I.D. to new patient value when spontaneous type is changed to 
//		PAV with an incompatible tube I.D.. This includes changes to 
//		the user interface to verify transitioned value with flashing
//		verify arrow icon. Change better supports verification icon
//		used for tube type and I.D. as well as humidification type and
//		volume.
// 
//  Revision: 104  By:  rhj    Date:  16-Apr-2009    SCR Number: 6448
//  Project:  840S2
//  Description:
//		Added a subscript "@ PEEP" to exhalation leak rate. 
// 
//  Revision: 103  By:  rhj    Date:  14-Apr-2009    SCR Number: 6495
//  Project:  840S2
//  Description:
//		Added DLOG_EST_SINGLE_TEST_INITIATED, DLOG_TEST_ALL_TESTS_REQUIRED.
// 
//  Revision: 102  By:  rhj    Date:  04-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//		Added single test strings for EST. 
//
//  Revision: 101  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 100  By:  gdc    Date:  15-Jan-2009    SCR Number: 6311
//  Project:  840S
//  Description:
//	Removed maneuver button 'help' messages.
//
//  Revision: 099  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 098   By: erm    Date: 24-Oct-2008    SCR Number: 6441
//  Project:  840S
//  Description:
//      Modified to support RT Wasform at the serial port.
//
//  Revision: 097   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 096   By: gdc   Date: 11-Aug-2008   SCR Number: 6435
//  Project:  840S
//  Description:
//      Added strings for changeable IBW.
//
//  Revision: 095   By: rhj   Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 094   By: rhj   Date: 20-Jul-2007   SCR Number: 6383
//  Project:  Trend
//  Description:
//      Added Expiratory/Inspiratory Pause Event messages.
//
//  Revision: 093   By: gdc   Date: 20-Mar-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added Trending related strings.
//
//  Revision: 092   By: rhj   Date:  20-Feb-2007    SCR Number: 6345
//  Project:  RESPM
//  Description:
//        Removed C20/C.
//
//  Revision: 091   By: rhj   Date:  29-Nov-2006    SCR Number: 6318
//  Project:  RESPM
//  Description:
//       Added RM pause pending and pause active messages.
//
//  Revision: 090   By: rhj   Date:  05-May-2006    SCR Number: 6181
//  Project:  PAV4
//  Description:
//       Added Expiratory Sensitivity label (L/min).  
//
//  Revision: 089   By: rhj   Date:  27-Oct-2005    SCR Number: 6236
//  Project:  RESPM
//  Description:
//       RESPM Project related changes. 
//   
//  Revision: 088   By: gdc   Date:  27-May-2005    SCR Number: 6170
//  Project:  NIV2
//  Description:
//       Removed Insp Too Long alarm for NIV. Added High Ti spont alert.
//   
//  Revision: 087   By: gdc   Date:  15-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//   added strings for NIV
//   adjusted alignment of several strings on More Data subscreen to
//   support new NIV data displays
//      
//  Revision: 086   By: gfu   Date:  11-Aug-2004    SCR Number: 6138
//  Project:  PAV3
//  Description:
//       Modified code per SDCR #6138.  Also added keyword "Log" to file header
//       as requested during code review.      
//
//  Revision: 085   By: gfu   Date: 18-Aug-2003   DR Number: 6083
//  Project:  PAV
//  Description:
//      Changed name of PAV to PAV+ 
//
// Revision: 084   By: erm   Date:  02-Jun-2003    DR Number: 6058
//  Project:  AVER
//  Description:
//    Added support for SplaceLab monitor
//
//  Revision: 083   By: ljs   Date: 01-May-2003   DR Number: 5872
//  Project:  PAV
//  Description:
//      Modified displayed units for Epav, Rpav, Rtot on More Patient Data screen. 
//
//  Revision: 082   By: ljs   Date: 24-Mar-2003   DR Number: 5313
//  Project:  PAV
//  Description:
//      Added Pav code from original PAV branch tip. 
//
//  Revision: 081   By: jja   Date:  23-May-2002    DR Number: 6010
//  Project:  VCP
//  Description:
//      Changed Volume Ventilation Plus option name to VV+ 
//      Remove unused strings containing VALUE_TITLE and VALUE_CHANGED_TITLE
//
//  Revision: 080   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//      Add new string For Shadow Trace button  
//
//  Revision: 079   By: heatherw   Date:  07-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added the following strings to be used with Vti Alarm
//         TOUCHABLE_HIGH_INSP_MAND_TIDAL_VOL_MSG   
//
//  Revision: 078   By: yakovb   Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Added VC+/VS Startup message strings
//
//  Revision: 077   By: heatherw   Date:  04-Mar-2002    DR Number: 5790
//  Project:  VCP
//  Description:
//      VTPC project-related changes:
//      *  added the following strings to be used with the different
//         combinations of VC+ MandType for new Vti Mand alarm.       
//         TOUCHABLE_INSP_MAND_TIDAL_VOL_LABEL
//         TOUCHABLE_INSP_MAND_TIDAL_VOL_UNIT;
//         TOUCHABLE_INSP_MAND_TIDAL_VOL_MSG 
//         TOUCHABLE_INSP_TIDAL_VOL_LABEL; 
//         TOUCHABLE_INSP_TIDAL_VOL_UNIT;
//         TOUCHABLE_INSP_TIDAL_VOL_MSG 
//         
//         HIGH_INH_SPONT_TIDAL_VOL_BUTTON_TITLE_SMALL;  
//         HIGH_INH_MAND_TIDAL_VOL_BUTTON_TITLE_SMALL;  
//         HIGH_INH_TIDAL_VOL_BUTTON_TITLE_SMALL;  
//         
//         INH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL;  
//         INH_TIDAL_VOL_SLIDER_MIN_LABEL; 
//
//         HIGH_INH_SPONT_VOL_HELP_MESSAGE;
//         HIGH_INH_MAND_VOL_HELP_MESSAGE;    
//
//  Revision: 076   By: quf    Date: 23-Jan-2002    DR Number: 5984
//  Project:  GUIComms
//  Description:
//	Added new strings for Serial Loopback Test.
//
//  Revision: 075   By: quf    Date: 10-Oct-2001    DR Number: 5962
//  Project:  GUIComms
//  Description:
//	Restored Baseline (NeoComms) version of TIME_DATE_TITLE_FORMAT,
//	removed O2_SENSOR_DISABLED_MESSAGE and UPPER_SOFTWARE_SALES_LABEL.
//
//  Revision: 074   By: quf    Date: 17-Sep-2001    DR Number: 5943
//  Project:  GUIComms
//  Description:
//	Removed strings NONE_VALUE_TITLE and NONE_VALUE_CHANGED_TITLE.
//
//  Revision: 073   By: quf    Date: 13-Sep-2001    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Print implementation changes:
//	- Removed print configuration strings.
//	- Removed "CAPTURING PRINT DATA" string.
//	- Changed "CANCEL PRINT" to "CANCEL".
//	- Changed "COM 1" to "1", "COM2 to "2", "COM 3" to "3"
//
//  Revision: 072   By: sah   Date:  31-Oct-2000    DR Number: 5780
//  Project:  VTPC
//  Description:
//      Added new 'SST_NEO_FILTER_TEST_DATA_LABEL' to be used with a
//      NEONATAL run of SST.
//
//  Revision: 071  By: gdc	Date: 20-Sep-2000   DCS Number:  5769
//  Project:  GuiComms
//  Description:
//      Added 38400 baud strings.
//
//  Revision: 070  By: gdc	Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 069   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added new PAV-based compliance, resistance, elastance and work-
//         of-breathing index
//      *  added new PA spontaneous type value
//
//  Revision: 068  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new SPONT-based inspiratory time, inspiratory time ratio,
//         and rapid/shallow breathing index
//      *  added new VC+ mandatory type value
//      *  added new VS spontaneous type value
//      *  re-designed discrete setting value strings, to be formatted with
//         point-size and style at runtime; includes elimination of over
//         90 messages, renaming of messages, and reorganizing of this
//         file
//      *  changed all references to 'IBW' to be as a symbol ('IBW')
//
//
//  Revision: 067  By: sah	Date: 18-Apr-2000   DCS Number:  5705
//  Project:  NeoMode
//  Description:
//      Changed help messages of "Pi" and "Psupp" to include the phrase
//      "above PEEP", and added two new strings with the "above PEEP" phrase,
//      but formatted to be used as an auxillary title in a setting button.
//
//  Revision: 066  By: sah	Date: 11-Apr-2000   DCS Number:  5691
//  Project:  NeoMode
//  Description:
//      Renamed 'TOUCHABLE_MEAN_CIRC_*_UNIT' to 'TOUCHABLE_PRESSURE_*_UNIT', to
//      reduce confusion.  Grouped like ids with each other.
//
//  Revision: 065  By: sah	Date: 10-Apr-2000   DCS Number:  5702
//  Project:  NeoMode
//  Description:
//      As part of the translation effort, the patient circuit type and
//      breath timing strings were changed to allow foreign languages
//      more flexibility with placement.
//
//  Revision: 064  By: sah	Date: 07-Apr-2000   DCS Number:  5700
//  Project:  NeoMode
//  Description:
//      Added sub-type fields to "SOFTWARE PROPERTY OF..." string.
//
//  Revision: 063  By: sph	Date: 04-Apr-2000   DCS Number:  5691
//  Project:  NeoMode
//  Description:
//      Change 'PEEP' back to a Vital Patient Data Area value, with
//      unit as part of help message.
//
//  Revision: 062  By: sah	Date: 08-Mar-2000   DCS Number:  5683
//  Project:  NeoMode
//  Description:
//      Re-implemented for dynamic configuration of VentConfig text values.
//      Also, replaced all 'LPM' text with 'L/min' text, and removed
//      unneeded strings.
//
//
//  Revision: 061  By: sph	Date: 17-Feb-2000   DCS Number:  5643
//  Project:  NeoMode
//  Description:
//      Removed superfluous '(%s)' from Pi-end's help message.
//
//  Revision: 060   By: hct    Date: 14-FEB-2000    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 059  By: hhd	Date: 17-Nov-1999   DCS Number:  5412
//  Project:  NeoMode
//  Description:
//      Added HUMID_VOLUME_VERIFY_SETTINGS_MESSAGE.
//
//  Revision: 058  By: btray    Date: 20-Jul-1999   DCS Number:  5424
//  Project:  NeoMode
//  Description:
//      Added strings for enabling software options from Development
//	    Options Subscreen. 
//
//  Revision: 057  By: sah    	Date: 30-Jun-1999   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Initial NeoMode Project implementation added:
//      *  added new, neonatal-specific strings
//      *  added units to VPDA help messages
//      *  enhanced look of Vent Config subscreen
//
//  Revision 056   By:   hhd      Date: 22-Sep-99       DR Number:   5529
//  Project:   Baseline
//  Description:
//	Modified text string TOUCHABLE_END_EXH_PRES_MSG and added "(monitored)".
// 
//  Revision 055   By:   sah      Date: 31-Aug-99       DR Number:   5517
//  Project:   ATC
//  Description:
//      Removed obsoleted requirement.
//
//  Revision 054   By:   sah      Date: 27-Aug-99       DR Number:   5519
//  Project:   ATC
//  Description:
//      Added new symbols.
//
//  Revision 053   By:   yyy      Date: 08/18/99         DR Number:   5513
//  Project:   ATC
//  Description:
//      Added new alarm improvements:
//      *  modified to handle alarm lock and break through help messages.
//
//  Revision: 052  By: dosman    Date:  15-Jul-1999   DR Number: 5470
//  Project:  ATC
//  Description:
//      Fixed Modification-Log
//
//  Revision: 051  By: iv    Date:  07-Jul-1999   DR Number: 5397
//  Project:  ATC
//  Description:
//      Changed 'SV_BK_ADC_LOOPBACK_FAIL' message.
//
//  Revision: 050  By: hhd    Date:  21-Jun-1999   DR Number: 5376
//  Project:  ATC
//  Description:
//     Added new entry for Battery background error. 
//
//  Revision: 049  By: hhd    Date: 4-June-1999  DCS Number:  5418
//  Project:  ATC
//  Description:
//      Added 1 new string per DCS 5418.  Deleted a bunch of strings due to
//      unnecessity.
//
//  Revision: 048  By: hhd    Date: 27-May-1999  DCS Number:  5385
//  Project:  ATC
//  Description:
//	* Added new strings PATIENT_TUBE_TYPE_BUTTON_TITLE3_SMALL and
//	  TUBE_ID_BUTTON_TITLE3_SMALL.
//	  Also, added SST_DATE_TIMESTAMP_LINE1, SST_DATE_TIMESTAMP_LINE2 (27-May-1999).
//	* Refixed SST_DATE_TIMESTAMP_LINE2 (15-Jul-1999).
//	* Centered PATIENT_TUBE_TYPE_BUTTON_TITLE3_SMALL and TUBE_ID_BUTTON_TITLE3_SMALL (16-Jul-1999).
//
//  Revision: 047  By: hhd    Date: 21-May-1999  DCS Number:  5391
//  Project:  ATC
//  Description:
//      Removed string STATUS_CCT_CAL_HUMID_CHANGED_WARNING.
//
//  Revision: 046  By: hhd    Date: 06-May-1999  DCS Number: 5380, 5381
//  Project:  ATC
//  Description:
//      Added TC_MM_UNITS2_SMALL; Changed font style to Bold for 2 strings.
//
//  Revision: 045  By: sah    Date: 29-Apr-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//	* Added 'VERIFY_SETTINGS_MESSAGE' and 'SAME_PATIENT_VERIFY_MESSAGE'
//        strings for use of the new verify-needed mechanism (29-Apr-1999).
//	* Added 'TUBE_TYPE_ET_TEXT' and 'TUBE_TYPE_TRACH_TEXT' (05-May-1999).
//
//  Revision: 044  By: yyy    Date: 16-Mar-1999  DCS Number: 5345
//  Project:  ATC
//  Description:
//	  * Added warning message when exp. pause pressure is not stable.
//	    Added warning message when pause is canceled by alarm or patient
//	    trigger (16-May-1999).
//	  * Modified string positions per DCS 5345 (26-May-1999).
//
//  Revision: 043  By: btray        Date: 12-Feb-1999  DCS Number: 5309 
//  Project: ATC 
//  Description:
//	* Fix for DCS # 5309, Centering of strings that are displayed during calibration
//	  or tests that are done as part of EST/SST, e.g. VENT INOP test (12-Feb-1999).
//	* Centered ATM_PRESSURE_TRANSDUCER_OOR (24-Feb-1999).
//
//  Revision: 042  By: btray        Date: 10-Feb-1999  DCS Number: 5316 
//  Project: ATC 
//  Description:
//      Fix for DCS # 5316, Centering of value displayed within disconnect 
//      sensitivity button. 
//
//  Revision: 041  By: btray    Date: 28-Jan-1999  DCS Number: 5297 
//  Project: ATC
//  Description:
//		The following strings were shortenend:
//		SV_UNABLE_TO_PERFORM_LEAK_TEST
//		SV_UNABLE_TO_PERFORM_LOAD_TEST
//		SV_GUI_TOUCH_SCREEN_NOT_RESPONDING
//
//  Revision: 040  By: hct    Date: 21-Jan-1999  DCS Number: 5322
//  Project: ATC
//  Description:
//	* Added messages for Spontaneous Inspired Tidal Volume (21-Jan-1999).
//	* Added HIGH Pcomp misc string (18-Feb-1999).
//	* Update after label review; removed unnecessary capitalization (10-May-1999).
//
//  Revision: 038  By:  btray    Date: 30-Nov-1998    DCS Number:  5276
//       Project:  BiLevel
//       Description:
//           Changed SOFTWARE_SALES_LABEL to refer to "Mallinckrodt".
//
//  Revision: 037  By:  syw    Date: 19-Nov-1998    DR Number: DCS 5218
//       Project:  BiLevel
//       Description:
//            Added SER_NUM_TROUBLE_SHOOTING_BUTTON_TITLE3
//
//  Revision: 036  By: btray    Date: 05-Nov-1998  DCS Number: 5262
//  Project: BiLevel (R8027)
//  Description:
//	Replaced some more incorrect text with correct symbols.	
//
//  Revision: 035  By: btray    Date: 4-Nov-1998  DCS Number: 5250
//  Project: BiLevel (R8027)
//  Description:
//	Replaced incorrect text with correct symbol.	
//
//  Revision: 034  By: yyy    Date: 3-Nov-1998  DCS Number: 5256
//  Project: BiLevel (R8027)
//  Description:
//	Fixed per qual test results.
//
//  Revision: 033  By: syw      Date: 28-Oct-1998  DR Number: 5215
//    Project:  BiLevel
//    Description:
//	  	Changed CONFIGURATION_CODE_LABEL to VENTILATOR_OPTIONS_LABEL
//
//  Revision: 032  By: btray	   Date:  28-Oct-1998    DCS Number: 5225
//  Project:  BiLevel (R8027)
//  Description:
//	Corrected spelling mistake in OP_HOUR_CONFIRMATION_ERROR and removed
//	unused MANEUVER_BUTTON_TITLE
//
//  Revision: 031  By:  syw	   Date:  27-Jul-1998    DCS Number: 5082
//  Project:  Sigma (840)
//  Description:
//      Added FS_NOT_COMPATIBLE_WITH_SOFTWARE
//
//  Revision: 030  By:  sah	   Date:  30-Jun-1998    DCS Number: 5025
//  Project:  Color
//  Description:
//		Corrected grammar.
//
//  Revision: 029  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/EnglishMiscStrs.INv   1.0.1.0   07/30/98 15:09:42   gdc
//	   Added CONFIGURATION_CODE_LABEL, SOFTWARE_SALES_LABEL
//
//  Revision: 028  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 027  By:  sah	Date:  08-May-1998    DR Number:  5041
//  Project:  Sigma (R8027)
//  Description:
//      Added two new system event codes, for tracking NOVRAM failures.
//
//  Revision: 027  By:  sah    Date:  15-Jan-1998    DR Number: 5004
//  Project:  Sigma (R8027)
//  Description:
//      Incorporated new, shortened symbol names, and transferred from
//      this file's '.in' counterpart to this '.IN' file -- to allow
//      pre-compilation processing of symbol substitutions.  Also, added
//      two new strings that were previously defined outside of this file
//      ('SCROLLBAR_UP_ARROW' and 'SCROLLBAR_DN_ARROW').
//
//  Revision: 026  By:  yyy		Date: 05-JAN-1998    DR Number: 
//       Project:  Sigma (840)
//       Description:
//			Bilevel initial release.
//
//  Revision: 025  By:  hhd		Date: 22-Oct-1997    DR Number: DCS 2288 
//       Project:  Sigma (840)
//       Description:
//			Restored SV_WYE_NOT_BLOCKED.
//
//  Revision: 024  By:  yyy    Date: 10-Oct-1997    DR Number: DCS 2452
//       Project:  Sigma (840)
//       Description:
//			Changed due to ExhValveCalibration modification.
//
//  Revision: 023  By:  yyy    Date:  08-Oct-97    DR Number: DCS 2203
//    Project:  Sigma (840)
//    Description:
//      Updated data for new BD audio test procedure.
//
//  Revision: 022  By:  yyy    Date:  08-Oct-1997    DR Number:   1865
//    Project:  Sigma (R8027)
//    Description:
//      Vent inop test error message cleanup.
//
//  Revision: 021  By:  yyy    Date:  01-Oct-97    DR Number: DCS 2204
//    Project:  Sigma (840)
//    Description:
//      Updated data label/error msg for battery test.
//
//  Revision: 020  By:  sah    Date:  30-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Replaced the system event message for a diagnostic log clearance,
//      with an event message for a task-control transition timeout.
//
//  Revision: 019  By:  hhd	   Date:  30-SEP-1997    DR Number:  2288 
//    Project:  Sigma (R8027)
//    Description:
//      Removed SV_WYE_NOT_BLOCKED. 
//
//  Revision: 018  By:  yyy    Date:  25-SEP-1997    DR Number:   2410
//    Project:  Sigma (R8027)
//    Description:
//      Added Pressure transducer calibration related miscStrs.
//
//  Revision: 017  By:  yyy    Date:  24-SEP-1997    DR Number:   1861
//    Project:  Sigma (R8027)
//    Description:
//      Display more clear diagnostic information for all types of calibration
//		tests and vent inop test.
//
//  Revision: 016  By:  yyy    Date:  10-Sep-97    DR Number: 1923
//    Project:  Sigma (R8027)
//    Description:
//      Changed the name of communications Diagnostic log to the system
//		information log.
//
//  Revision: 015  By:  yyy    Date:  08-Sep-97    DR Number: 2398
//    Project:  Sigma (R8027)
//    Description:
//      Modified the contents of non heated exp tube and heated exp tube
//		to be identical between service and normal ventilation mode.
//
//  Revision: 014  By:  yyy    Date:  08-Sep-97    DR Number: 2439
//    Project:  Sigma (R8027)
//    Description:
//      Changed ML_CM_H2O_LABEL to ML_LABEL.
//
//  Revision: 013  By:  yyy    Date:  20-Aug-97    DR Number: 2356
//    Project:  Sigma (R8027)
//    Description:
//      Modified contents for SV_HIGH_FLOW_TIME_TOO_LOW.
//
//  Revision: 012  By:  yyy    Date:  06-Aug-97    DR Number: 2130
//    Project:  Sigma (R8027)
//    Description:
//      Added SV_WYE_NOT_BLOCKED string.
//
//  Revision: 011  By:  yyy    Date:  30-JUL-97    DR Number: 2313,2279
//    Project:  Sigma (R8027)
//    Description:
//      30-JUL-97 Added SM_FLOW_SENSOR_CAL_BUTTON_TITLE, and SM_TAU_CALIBRATION_BUTTON_TITLE
//		FLOW_SENSOR_CALIBRATION_SUBSCREEN_TITLE, TAU_CALIB_SUBSCREEN_TITLE,
//		TEMP_CHANGE_TOO_SMALL, TEMP_INCREASING, CANNOT_ACHIEVE_MIN_AIR_FLOW,
//		CANNOT_ACHIEVE_MIN_O2_FLOW, AIR_TAU_OOR, O2_TAU_OOR, AIR_OFFSET_OOR,
//		O2_OFFSET_OOR new strings.
//		13-AUG-97  Removed all references for TauCharacterization
//
//  Revision: 010  By:  yyy    Date:  30-JUL-97    DR Number: 2205
//    Project:  Sigma (R8027)
//    Description:
//      Updated the error for exh valve velocity transducer test.
//
//  Revision: 009  By:  yyy    Date:  30-JUL-97    DR Number: 2013
//    Project:  Sigma (R8027)
//    Description:
//      Updated the error for Nurse Call test.
//
//  Revision: 008  By:  yyy    Date:  30-JUL-97    DR Number: 2216
//    Project:  Sigma (R8027)
//    Description:
//      Added SV_LOW_EXPIRATORY_FILTER_DELTA_P new string.
//
//  Revision: 007  By:  yyy    Date:  22-JUNE-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      22-JUNE-97 Renamed waveformsSubScreen related help message for translation.
//      Changed the sequence of declaring some of the misc. strings for
//              SUN or SIGMA_TARGET.
//              Modified the cheap text so the p= is always before y= for easier
//              text extraction.
//              23-JUL-97 Modified EST_LEAK_PASS_MSG, EST_LEAK_FAILED_MSG, and
//              EST_LEAK_ALERT_MSG by adding x= parameter.
//              24-JUL-9 Added x=90 to MiscStrs::SAFETY_SUBSCREEN_SUBTITLE.
//
//  Revision: 006  By:  hhd    Date:  25-JUNE-97    DR Number: 2233
//    Project:  Sigma (R8027)
//    Description:
//      Added BK_DATAKEY_SIZE_ERROR.
//
//  Revision: 005  By:  yyy    Date:  27-MAY-97    DR Number: 2042
//    Project:  Sigma (R8027)
//    Description:
//      Removed SV_SPEAKER_DISCONNECTED and SV_RESONANCE_TEST, changed
//              SV_SELF_TEST_FAILED to SV_SAAS_TEST_FAILED.
//
//  Revision: 004  By:  hhd    Date:  13-MAY-97    DR Number: 2085
//    Project:  Sigma (R8027)
//    Description:
//      Added DLOG_NMI_SHORT and DLOG_EXCEPTION_VECTOR.
//
//  Revision: 003  By: yyy      Date: 07-May-1997  DR Number: 2059
//    Project:  Sigma (R8027)
//    Description:
//      Removed unused message strings.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2004
//    Project:  Sigma (R8027)
//    Description:
//      Changed BK_COMPR_CHECKSUM to BK_COMPR_BAD_DATA.  Added BK_COMPR_UPDATE_SN,
//              and BK_COMPR_UPDATE_PM_HRS error strings.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "MiscStrs.hh"
#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

//
// For VitalPatientDataArea breath status readout
//
StringId MiscStrs::TOUCHABLE_BREATH_STATUS_ASSIST =
        "{p=60,y=59:A}";
StringId MiscStrs::TOUCHABLE_BREATH_STATUS_CONTROL =
        "{p=60,y=60:C}";
StringId MiscStrs::TOUCHABLE_BREATH_STATUS_SPONT =
        "{p=60,y=60:S}";
StringId MiscStrs::NO_VENT_LABEL =
        "{p=24,y=40:No ventilation for:  %s%02d:%02d:%02d}";
StringId MiscStrs::INOP_NO_VENT_LABEL =
        "{p=24,y=40:Vent inop for:  %s%02d:%02d:%02d}";
StringId MiscStrs::VENT_STARTUP_LABEL =
        "{p=24,y=40:Vent startup in progress}";
StringId MiscStrs::WAITING_FOR_PT_LABEL =
        "{p=24,c=13,y=32:Waiting for patient connect"
        "{p=12,x=20,Y=24:Ventilation will begin when connection is detected.}}";
StringId MiscStrs::VENTILATOR_MALFUNCTION_LABEL =
        "{p=24,y=40:System Error!}";
StringId MiscStrs::VENTILATOR_INOPERATIVE_LABEL =
        "{p=24,y=40:Ventilator Inoperative!}";
StringId MiscStrs::SERVICE_REQUIRED_LABEL =
        "{p=24,y=40:Service Mode Required!}";
StringId MiscStrs::SVO_LABEL =
        "{p=24,c=13,y=32:No ventilation"
        "{p=12,x=30,Y=24:Safety valve open.}}";
//
// For VitalPatientDataArea touchable labels
//
StringId MiscStrs::TOUCHABLE_IE_RATIO_LABEL =
        "{p=18,y=21:I:E}";
StringId MiscStrs::TOUCHABLE_IE_RATIO_MSG =
        "{p=14,y=17:I:E{T: = Insp to expiratory time ratio}}";


StringId MiscStrs::TOUCHABLE_TOTAL_RESP_RATE_LABEL =
        "{p=18,y=21:f{S:TOT}}";
StringId MiscStrs::TOUCHABLE_TOTAL_RESP_RATE_MSG =
        "{p=14,y=17:f{S:TOT}{T: = Total respiratory rate (1/min)}}";

StringId MiscStrs::TOUCHABLE_EXH_TIDAL_VOL_LABEL =
        "{p=18,y=21,N:V{S:TE}}";
StringId MiscStrs::TOUCHABLE_PROXIMAL_EXH_TIDAL_VOL_LABEL =
        "{p=18,x=0,y=21,N:V{S:TE}{S:-{c=12,N:Y}}}";
StringId MiscStrs::TOUCHABLE_EXH_TIDAL_VOL_MSG =
        "{p=14,y=17:V{S:TE}{T: = Exhaled tidal volume (mL)}}";
StringId MiscStrs::TOUCHABLE_PROXIMAL_EXH_TIDAL_VOL_MSG =
        "{p=14,y=17:V{S:TE}{S:-{c=12,N:Y}}{T: = Exhaled tidal volume (mL)}}";

StringId MiscStrs::TOUCHABLE_EXH_MINUTE_VOL_LABEL =
        "{p=18,y=21,N:\001{S:E TOT}}";
StringId MiscStrs::TOUCHABLE_PROXIMAL_EXH_MINUTE_VOL_LABEL =
        "{p=18,x=0,y=21,N:\001{S:E TOT}{S:-{c=12,N:Y}}}";

StringId MiscStrs::TOUCHABLE_EXH_MINUTE_VOL_MSG =
        "{p=14,y=17:\001{S:E TOT}{T: = Exhaled minute volume (L/min)}}";
StringId MiscStrs::TOUCHABLE_PROXIMAL_EXH_MINUTE_VOL_MSG =
        "{p=14,y=17:\001{S:E TOT}{S:-{c=12,N:Y}}{T: = Exhaled minute volume (L/min)}}";


StringId MiscStrs::TOUCHABLE_END_EXH_PRES_LABEL =
        "{p=18,y=21:PEEP}";
StringId MiscStrs::TOUCHABLE_END_EXH_PRES_MSG =
        "{p=12,y=17:PEEP{T: = Monitored end exp. pressure (%s)}}";

StringId MiscStrs::TOUCHABLE_MEAN_CIRC_PRES_LABEL =
        "{p=18,y=21:P{S:MEAN}}";
StringId MiscStrs::TOUCHABLE_MEAN_CIRC_PRES_MSG =
        "{p=14,y=17:P{S:MEAN}{T: = Mean airway pressure (%s)}}";

StringId MiscStrs::TOUCHABLE_DELIVERED_VOL_LABEL =
        "{p=18,y=21,N:V{S:TI}}";
StringId MiscStrs::TOUCHABLE_PROXIMAL_DELIVERED_VOL_LABEL =
        "{p=18,x=0,y=21,N:V{S:TI}{S:-{c=12,N:Y}}}";
StringId MiscStrs::TOUCHABLE_DELIVERED_VOL_MSG =
        "{p=14,y=17:V{S:TI}{T: = Delivered Volume (mL)}}";

StringId MiscStrs::TOUCHABLE_PEAK_CIRC_PRES_LABEL =
        "{p=18,y=21:P{S:PEAK}}";
StringId MiscStrs::TOUCHABLE_PEAK_CIRC_PRES_MSG =
        "{p=14,y=17:P{S:PEAK}{T: = Peak circuit pressure (%s)}}";

StringId MiscStrs::APNEA_DISABLED_WARNING_MSG =
        "{p=10,y=16:APNEA{x=0,Y=13:DETECTION{x=0,Y=13:DISABLED}}}";
StringId MiscStrs::APNEA_DETECTION_DISABLED_MESSAGE =
        "{p=14,c=8,y=17:APNEA DETECTION DISABLED}";

StringId MiscStrs::TOUCHABLE_BREATH_STATUS_CONTROL_MSG =
        "{p=14,y=17,T:Control breath}";
StringId MiscStrs::TOUCHABLE_BREATH_STATUS_ASSIST_MSG =
        "{p=14,y=17,T:Assist breath}";
StringId MiscStrs::TOUCHABLE_BREATH_STATUS_SPONT_MSG =
        "{p=14,y=17,T:Spontaneous breath}";

//
// For MoreDataSubScreen touchable labels.
//
StringId MiscStrs::TOUCHABLE_END_INSP_PRES_LABEL =
        "{p=18,y=29:P{S:I END}}";
StringId MiscStrs::TOUCHABLE_END_INSP_PRES_MSG =
        "{p=14,y=17:P{S:I END}{T: = End inspiratory pressure}}";
StringId MiscStrs::TOUCHABLE_PRESSURE_HPA_UNIT =
        "{p=10,y=29:hPa}";
StringId MiscStrs::TOUCHABLE_PRESSURE_UNIT =
        "{p=10,y=29:H{S:2}O{X=-23,Y=-12:cm}}";

StringId MiscStrs::TOUCHABLE_DEL_O2_PERCENT_LABEL =
        "{p=18,y=29:O{S:2}}";
StringId MiscStrs::TOUCHABLE_DEL_O2_PERCENT_MSG =
        "{p=14,y=17:O{S:2}{T: = Delivered oxygen %}}";
StringId MiscStrs::TOUCHABLE_DEL_O2_PERCENT_UNIT =
        "{p=10,y=29:%}";

StringId MiscStrs::TOUCHABLE_SPONT_MINUTE_VOL_LABEL =
        "{p=18,y=29:\001{S:E SPONT}}";
StringId MiscStrs::TOUCHABLE_SPONT_MINUTE_VOL_MSG =
        "{p=14,y=17:\001{S:E SPONT}{T: = Exhaled spont minute volume}}";
StringId MiscStrs::TOUCHABLE_SPONT_MINUTE_VOL_UNIT =
        "{p=10,x=10,y=14:L{x=0:____}{x=2,y=29:min}}";



StringId MiscStrs::TOUCHABLE_INSP_SPONT_TIDAL_VOL_LABEL =
        "{p=18,y=29:V{S:TI SPONT}}";
StringId MiscStrs::TOUCHABLE_INSP_SPONT_TIDAL_VOL_MSG =
        "{p=14,y=17:V{S:TI SPONT}{T: = Spont inspired tidal volume}}";
StringId MiscStrs::TOUCHABLE_INSP_SPONT_TIDAL_VOL_UNIT =
        "{p=10,y=29:mL}";

StringId MiscStrs::TOUCHABLE_INSP_MAND_TIDAL_VOL_LABEL =
        "{p=18,y=29:V{S:TI MAND}}";
StringId MiscStrs::TOUCHABLE_INSP_MAND_TIDAL_VOL_MSG =
        "{p=14,y=17:V{S:TI MAND}{T: = Mand inspired tidal volume}}";
StringId MiscStrs::TOUCHABLE_INSP_MAND_TIDAL_VOL_UNIT =
    	"{p=10,y=29:mL}";
        
StringId MiscStrs::TOUCHABLE_INSP_TIDAL_VOL_LABEL =
        "{p=18,y=29:V{S:TI}}";
StringId MiscStrs::TOUCHABLE_INSP_TIDAL_VOL_MSG =
        "{p=14,y=17:V{S:TI}{T: = Inspired tidal volume}}";
StringId MiscStrs::TOUCHABLE_INSP_TIDAL_VOL_UNIT =
    	"{p=10,y=29:mL}";
        
StringId MiscStrs::TOUCHABLE_PAV_COMPLIANCE_LABEL =
        "{p=18,y=29:C{S:PAV}}";
StringId MiscStrs::TOUCHABLE_PAV_COMPLIANCE_MSG =
        "{p=14,y=17:C{S:PAV}{T: = Estimated lung compliance}}";
StringId MiscStrs::TOUCHABLE_PAV_COMPLIANCE_UNIT =
        "{p=10,x=16,y=15:mL{x=9:____}{x=0,y=29:cmH{S:2}O}}";
StringId MiscStrs::TOUCHABLE_PAV_COMPLIANCE_HPA_UNIT =
	"{p=10,x=7,x=6,y=15:mL{x=0:____}{x=0,y=29:hPa}}";

StringId MiscStrs::TOUCHABLE_PAV_RESISTANCE_LABEL =
        "{p=18,y=29:R{S:PAV}}";
StringId MiscStrs::TOUCHABLE_PAV_RESISTANCE_MSG =
        "{p=14,y=17:R{S:PAV}{T: = Estimated patient resistance}}";
StringId MiscStrs::TOUCHABLE_PAV_RESISTANCE_UNIT =
        "{p=10,x=0,y=12:cmH{S:2}O{x=9,Y=2:____}{x=11,y=29:L/s}}";
StringId MiscStrs::TOUCHABLE_PAV_RESISTANCE_HPA_UNIT =
	"{p=10,x=7,x=0,y=15:hPa{x=0:____}{x=6,y=29:L/s}}";

StringId MiscStrs::TOUCHABLE_PAV_ELASTANCE_LABEL =
        "{p=18,y=29:E{S:PAV}}";
StringId MiscStrs::TOUCHABLE_PAV_ELASTANCE_MSG =
        "{p=14,y=17:E{S:PAV}{T: = Estimated lung elastance}}";
StringId MiscStrs::TOUCHABLE_PAV_ELASTANCE_UNIT =
        "{p=10,x=0,y=12:cmH{S:2}O{x=9,Y=2:____}{x=15,y=29:L}}";
StringId MiscStrs::TOUCHABLE_PAV_ELASTANCE_HPA_UNIT =
	"{p=10,x=7,x=0,y=15:hPa{x=0:____}{x=6,y=29:L}}";

StringId MiscStrs::TOUCHABLE_TOTAL_RESISTANCE_LABEL =
        "{p=18,y=29:R{S:TOT}}";
StringId MiscStrs::TOUCHABLE_TOTAL_RESISTANCE_MSG =
        "{p=14,y=17:R{S:TOT}{T: = Estimated total resistance}}";
StringId MiscStrs::LARGE_MARKER_LABEL =
        "{p=18,x=5,y=29:({x=90:)}}";

StringId MiscStrs::TOUCHABLE_TOTAL_WORK_OF_BREATHING_LABEL_LARGE =
        "{p=18,y=29:WOB{S:TOT}}";
StringId MiscStrs::TOUCHABLE_TOTAL_WORK_OF_BREATHING_MSG =
        "{p=14,y=17:WOB{S:TOT}{T: = WOB done by patient + vent}}";
StringId MiscStrs::TOUCHABLE_PATIENT_WORK_OF_BREATHING_MSG =
        "{p=14,y=17:WOB{S:PT}{T: = WOB done by patient}}";
StringId MiscStrs::TOUCHABLE_ELASTANCE_WORK_OF_BREATHING_MSG =
        "{p=14,y=17:E{T: = Work to overcome elastance}}";
StringId MiscStrs::TOUCHABLE_RESISTANCE_WORK_OF_BREATHING_MSG =
        "{p=14,y=17:R{T: = Work to overcome resistance}}";
StringId MiscStrs::TOUCHABLE_WORK_OF_BREATHING_UNIT =
        "{p=10,x=0,y=29:J/L}";

StringId MiscStrs::TOUCHABLE_PAV_INTRINSIC_PEEP_MSG =
        "{p=14,y=17:PEEP{S:I}{T: = Estimated intrinsic PEEP}}";

StringId MiscStrs::TOUCHABLE_SPONT_PERCENT_TI_LABEL =
        "{p=18,y=29:T{S:I}/T{S:TOT}}";
StringId MiscStrs::TOUCHABLE_SPONT_PERCENT_TI_MSG =
        "{p=14,y=17:T{S:I}/T{S:TOT}{T: = Spontaneous inspiratory percent}}";

StringId MiscStrs::TOUCHABLE_SPONT_FV_RATIO_LABEL =
        "{p=18,y=29:f/V{S:T}}";
        
StringId MiscStrs::TOUCHABLE_SPONT_FV_RATIO_MSG =
        "{p=14,y=17:f/V{S:T}{T: = Rapid/shallow breathing index}}";



StringId MiscStrs::TOUCHABLE_NORMALIZE_SPONT_FV_RATIO_LABEL =
        "{p=18,y=29:f/V{S:T}/kg}";
        
StringId MiscStrs::TOUCHABLE_NORMALIZE_SPONT_FV_RATIO_MSG =
        "{p=12,y=17:f/V{S:T}/kg{T: = Normalized Rapid/shallow breathing index}}";

StringId MiscStrs::TOUCHABLE_SPONT_INSP_TIME_LABEL =
        "{p=18,y=29:T{S:I SPONT}}";
StringId MiscStrs::TOUCHABLE_SPONT_INSP_TIME_MSG =
        "{p=14,y=17:T{S:I SPONT}{T: = Spontaneous inspiratory time}}";
StringId MiscStrs::TOUCHABLE_SPONT_INSP_TIME_UNIT =
        "{p=10,x=0,y=29:sec}";

StringId MiscStrs::WOB_GRAPHIC_TITLE = 
        "{p=10,x=7,y=10:WOB{p=6,Y=16,x=18,T:J/L}}";
StringId MiscStrs::WOB_GRAPHIC_TOTAL_LABEL = 
        "{p=10,y=11,N:WOB{S:TOT}}";
StringId MiscStrs::WOB_GRAPHIC_PATIENT_LABEL = 
        "{p=10,y=11,N:WOB{S:PT}}";
StringId MiscStrs::WOB_GRAPHIC_ELASTANCE_LABEL = 
        "{p=8,y=8,N:E}";
StringId MiscStrs::WOB_GRAPHIC_RESISTANCE_LABEL = 
        "{p=8,y=8,N:R}";

//
// For AlarmMsg and AlarmLogMsg touchable root causes.
//
StringId MiscStrs::TOUCHABLE_HIGH_MINUTE_VOLUME_MSG =
        "{p=14,y=17:\004\001{S:E TOT}{T: = High exhaled total minute volume}}";
StringId MiscStrs::TOUCHABLE_HIGH_TIDAL_VOLUME_MSG =
        "{p=14,y=17:\004V{S:TE}{T: = High exhaled tidal volume}}";
StringId MiscStrs::TOUCHABLE_HIGH_CIRC_PRES_MSG =
        "{p=14,y=17:\004P{S:PEAK}{T: = High circuit pressure}}";
StringId MiscStrs::TOUCHABLE_LOW_CIRC_PRES_MSG =
        "{p=14,y=17:\003P{S:PEAK}{T: = Low circuit pressure}}";
StringId MiscStrs::TOUCHABLE_HIGH_RESP_RATE_MSG =
        "{p=14,y=17:\004f{S:TOT}{T: = High respiratory rate}}";
StringId MiscStrs::TOUCHABLE_LOW_MAND_TIDAL_VOL_MSG =
        "{p=14,y=17:\003V{S:TE MAND}{X=-2,T: = Low exhaled mand tidal volume}}";
StringId MiscStrs::TOUCHABLE_LOW_SPONT_TIDAL_VOL_MSG =
        "{p=14,y=17:\003V{S:TE SPONT}{T: = Low exhaled spont tidal vol}}";
StringId MiscStrs::TOUCHABLE_HIGH_INSP_SPONT_TIDAL_VOL_MSG =
        "{p=14,y=17:\004V{S:TI SPONT}{T: = High inspired spont tidal volume}}";
StringId MiscStrs::TOUCHABLE_HIGH_INSP_MAND_TIDAL_VOL_MSG =
        "{p=14,y=17:\004V{S:TI MAND}{T: = High inspired mand tidal volume}}";        
StringId MiscStrs::TOUCHABLE_LOW_MINUTE_VOLUME_MSG =
        "{p=14,y=17:\003\001{S:E TOT}{T: = Low exhaled total minute volume}}";
StringId MiscStrs::TOUCHABLE_HIGH_COMP_PRES_MSG =
        "{p=14,y=17:\004P{S:COMP}{T: = High compensation pressure}}";
//
// For ApneaVentilationSubScreen...
//
StringId MiscStrs::APNEA_VENT_SUBSCREEN_TITLE =
        "{p=24,y=28:Apnea ventilation in progress.}";
StringId MiscStrs::APNEA_VENT_SUBSCREEN_FOOTNOTE1 =
        "{p=10,y=10:TO ADJUST APNEA SETTINGS:  Touch the APNEA button on the lower screen.}";
StringId MiscStrs::APNEA_VENT_SUBSCREEN_FOOTNOTE2 =
        "{p=10,y=10:TO EXIT APNEA VENTILATION:  Press the \014{p=6,X=-28,o=10:RESET} key below the lower screen.}";
//
// For SafetyVentilationSubScreen...
//
StringId MiscStrs::SAFETY_SUBSCREEN_TITLE =
        "{p=24,y=28:Safety ventilation in progress.}";
StringId MiscStrs::SAFETY_SUBSCREEN_SUBTITLE =
        "{p=18,y=18:Ventilating with the following settings:}";
StringId MiscStrs::SAFETY_SUBSCREEN_FOOTNOTE =
        "{p=10,y=10:Check patient & provide alternative ventilation."
        "{x=0,y=25:TO CANCEL SAFETY VENTILATION:  Complete the patient setup in the lower screen.}}";


//
// Trending Subscreen
//
StringId MiscStrs::TREND_TABLE_SCREEN_NAV_TITLE = 
        "{p=10,y=15:Table}";
StringId MiscStrs::TREND_GRAPH_SCREEN_NAV_TITLE = 
        "{p=10,y=15:Graph}";
StringId MiscStrs::TREND_PRESET_BUTTON_TITLE = 
        "{p=8,y=8:Presets}";
StringId MiscStrs::TREND_PRESET_BUTTON_HELP_MSG =
        "{p=10,y=17,T:Preset is a preconfigured set of Trend parameters.}";
StringId MiscStrs::TREND_PRINT_BUTTON_TITLE = 
        "{p=10,y=15:Print}";
StringId MiscStrs::TREND_EVENT_TAB_BUTTON_TITLE =
        "{p=10,x=6,y=12,T:MANUAL{p=14,x=0,y=29,T:EVENT}}";

StringId MiscStrs::TREND_PRESET_NONE_LABEL =
        "{p=%d,y=%d,%c:- -}";
StringId MiscStrs::TREND_PRESET_WEANING_LABEL =
        "{p=%d,y=%d,%c:Weaning}";
StringId MiscStrs::TREND_PRESET_COPD_LABEL =
        "{p=%d,y=%d,%c:COPD}";
StringId MiscStrs::TREND_PRESET_ARDS_LABEL =
        "{p=%d,y=%d,%c:ARDS}";
StringId MiscStrs::TREND_PRESET_VC_PLUS_LABEL =
        "{p=%d,y=%d,%c:VC+}";
StringId MiscStrs::TREND_PRESET_PAV_PLUS_LABEL =
        "{p=%d,y=%d,%c:PAV+}";
StringId MiscStrs::TREND_PRESET_BILEVEL_LABEL =
        "{p=%d,y=%d,%c:BiLevel}";
StringId MiscStrs::TREND_PRESET_SIM_LABEL =
        "{p=%d,y=%d,%c:LRM}";
StringId MiscStrs::TREND_PRESET_NEO_VCV_LABEL =
		"{p=%d,y=%d,%c:VCV}";
StringId MiscStrs::TREND_PRESET_NEO_PCV_LABEL =
		"{p=%d,y=%d,%c:PCV}";
StringId MiscStrs::TREND_PRESET_NEO_BPD_LABEL =
		"{p=%d,y=%d,%c:BPD}";
StringId MiscStrs::TREND_PRESET_NEO_SURF_LABEL =
		"{p=%d,y=%d,%c:SURF}";
StringId MiscStrs::TREND_PRESET_NEO_SIMV_LABEL =
		"{p=%d,y=%d,%c:N SIMV}";
StringId MiscStrs::TREND_PRESET_NEO_SPONT_LABEL =
		"{p=%d,y=%d,%c:N SPONT}";

StringId MiscStrs::TREND_TIME_SCALE_1HR = 
        "{p=%d,y=%d,%c:1 h}";
StringId MiscStrs::TREND_TIME_SCALE_2HR = 
        "{p=%d,y=%d,%c:2 h}";
StringId MiscStrs::TREND_TIME_SCALE_4HR = 
        "{p=%d,y=%d,%c:4 h}";
StringId MiscStrs::TREND_TIME_SCALE_8HR = 
        "{p=%d,y=%d,%c:8 h}";
StringId MiscStrs::TREND_TIME_SCALE_12HR = 
        "{p=%d,y=%d,%c:12 h}";
StringId MiscStrs::TREND_TIME_SCALE_24HR = 
        "{p=%d,y=%d,%c:24 h}";
StringId MiscStrs::TREND_TIME_SCALE_48HR = 
        "{p=%d,y=%d,%c:48 h}";
StringId MiscStrs::TREND_TIME_SCALE_72HR = 
        "{p=%d,y=%d,%c:72 h}";

StringId MiscStrs::TREND_CURSOR_HELP_MSG = 
        "{p=14,y=17,T:Trend Cursor Position}";

StringId MiscStrs::EVENT_SUBSCREEN_TITLE = 
        "{p=14,y=18:Manual Event Entry}";

StringId MiscStrs::EVENT_CLEAR_BUTTON_TITLE =
        "{p=10,y=20:CLEAR}";
StringId MiscStrs::EVENT_ACCEPT_BUTTON_TITLE =
        "{p=10,y=20:ACCEPT}";
StringId MiscStrs::EVENT_DIALOG_TITLE =
        "{p=8,y=10:ACCEPT OR CLEAR SELECTED EVENTS}";

StringId MiscStrs::EVENT_SUCTION_TITLE_BUTTON = 
        "{p=10,x=25,y=11:1{X=-27,Y=14:Suction}}";
StringId MiscStrs::EVENT_RX_TITLE_BUTTON = 
        "{p=10,x=9,y=11:2{X=-13,Y=14:Rx}}";
StringId MiscStrs::EVENT_BLOOD_GAS_TITLE_BUTTON = 
        "{p=10,x=29,y=11:3{X=-37,Y=14:Blood Gas}}";
StringId MiscStrs::EVENT_RESPIRATORY_MANEUVER_TITLE_BUTTON = 
        "{p=10,x=33,y=11:4{X=-41,Y=11:Respiratory}{X=-67,Y=14:Maneuver}}";
StringId MiscStrs::EVENT_CIRCUIT_CHANGE_TITLE_BUTTON = 
        "{p=10,x=22,y=11:5{X=-25,Y=11:Circuit}{X=-43,Y=14:Change}}";
StringId MiscStrs::EVENT_START_WEANING_TITLE_BUTTON = 
        "{p=10,x=28,y=11:6{X=-20,Y=11:Start}{X=-40,Y=14:Weaning}}";
StringId MiscStrs::EVENT_STOP_WEANING_TITLE_BUTTON = 
        "{p=10,x=28,y=11:7{X=-20,Y=11:Stop}{X=-40,Y=14:Weaning}}";
StringId MiscStrs::EVENT_BRONCHOSCOPY_TITLE_BUTTON = 
        "{p=10,x=41,y=11:8{X=-48,Y=14:Bronchoscopy}}";
StringId MiscStrs::EVENT_XRAY_TITLE_BUTTON = 
        "{p=10,x=13,y=11:9{X=-21,Y=14:X-ray}}";
StringId MiscStrs::EVENT_RECRUITMENT_MANEUVER_TITLE_BUTTON = 
        "{p=10,x=31,y=11:10{X=-46,Y=11:Recruitment}{X=-68,Y=14:Maneuver}}";
StringId MiscStrs::EVENT_RESPOSITION_PATIENT_TITLE_BUTTON = 
        "{p=10,x=25,y=11:11{X=-43,Y=11:Reposition}{X=-53,Y=14:Patient}}";
StringId MiscStrs::EVENT_OTHER_TITLE_BUTTON = 
        "{p=10,x=9,y=11:12{X=-23,Y=14:Other}}";
StringId MiscStrs::EVENT_SURFACTANT_ADMIN_TITLE_BUTTON =
		"{p=10,x=25,y=11:13{x=0,Y=11:Surfactant}{x=10,Y=14:Admin.}}";
StringId MiscStrs::EVENT_PRONE_POSITION_TITLE_BUTTON =
		"{p=10,x=18,y=11:14{X=-27,Y=11:Prone}{X=-46,Y=14:Position}}";
StringId MiscStrs::EVENT_SUPINE_POSITION_TITLE_BUTTON =
		"{p=10,x=18,y=11:15{X=-30,Y=11:Supine}{X=-63,Y=14:Position}}";
StringId MiscStrs::EVENT_LEFT_SIDE_POSITION_TITLE_BUTTON =
		"{p=10,x=20,y=11:16{X=-37,Y=11:Left Side}{X=-52,Y=14:Position}}";
StringId MiscStrs::EVENT_RIGHT_SIDE_POSITION_TITLE_BUTTON =
		"{p=10,x=24,y=11:17{X=-41,Y=11:Right Side}{X=-57,Y=14:Position}}";
StringId MiscStrs::EVENT_MANUAL_STIMULATION_TITLE_BUTTON =
		"{p=10,x=31,y=11:18{X=-31,Y=11:Manual}{X=-60,Y=14:Stimulation}}";

StringId MiscStrs::EVENT_PREV_SCREEN_BUTTON =
        "{p=10,x=5,y=16:Previous{X=-50,Y=19:Screen}}";
StringId MiscStrs::EVENT_NEXT_SCREEN_BUTTON =
	"{p=10,x=7,y=16:Next{x=0,Y=19:Screen}}";

StringId MiscStrs::TREND_EMPTY =
        "{p=%d,y=%d,%c:- -}";
StringId MiscStrs::TREND_DYNAMIC_COMPLIANCE =
        "{p=%d,y=%d,%c:C{S:DYN}}";
StringId MiscStrs::TREND_DYNAMIC_RESISTANCE =
        "{p=%d,y=%d,%c:R{S:DYN}}";
StringId MiscStrs::TREND_END_EXPIRATORY_FLOW =
        "{p=%d,y=%d,%c:EEF}";
StringId MiscStrs::TREND_END_EXPIRATORY_PRESSURE =
        "{p=%d,y=%d,%c:PEEP}";
StringId MiscStrs::TREND_EXHALED_MANDATORY_TIDAL_VOLUME =
        "{p=%d,y=%d,%c:V{S:TE MAND}}";
StringId MiscStrs::TREND_EXHALED_MINUTE_VOLUME =
        "{p=%d,y=%d,%c:\001{S:E TOT}}";
StringId MiscStrs::TREND_SET_EXPIRATORY_SENSITIVITY =
        "{p=%d,y=%d,%c:\\[E{S:SENS}\\]}";
StringId MiscStrs::TREND_EXHALED_SPONTANEOUS_MINUTE_VOLUME =
        "{p=%d,y=%d,%c:\001{S:E SPONT}}";
StringId MiscStrs::TREND_EXHALED_SPONTANEOUS_TIDAL_VOLUME =
        "{p=%d,y=%d,%c:V{S:TE SPONT}}";
StringId MiscStrs::TREND_EXHALED_TIDAL_VOLUME =
        "{p=%d,y=%d,%c:V{S:TE}}";
StringId MiscStrs::TREND_SET_FLOW_ACCELERATION_OVER_RISE_TIME =
        "{p=%d,y=%d,%c:\\[{p=14: \005}\\]}";
StringId MiscStrs::TREND_SET_FLOW_SENSITIVITY =
        "{p=%d,y=%d,%c:\\[\001{S:SENS}\\]}";
StringId MiscStrs::TREND_SET_TH_TL_RATIO =
        "{p=%d,y=%d,%c:\\[T{S:H}:T{S:L}\\]}";
StringId MiscStrs::TREND_I_E_RATIO =
        "{p=%d,y=%d,%c:I:E}";
StringId MiscStrs::TREND_SET_I_E_RATIO =
        "{p=%d,y=%d,%c:\\[I:E\\]}";
StringId MiscStrs::TREND_SET_INSPIRATORY_PRESSURE =
        "{p=%d,y=%d,%c:\\[P{S:I}\\]}";
StringId MiscStrs::TREND_SET_INSPIRATORY_TIME =
        "{p=%d,y=%d,%c:\\[T{S:I}\\]}";
StringId MiscStrs::TREND_MEAN_CIRCUIT_PRESSURE =
        "{p=%d,y=%d,%c:P{S:MEAN}}";
StringId MiscStrs::TREND_NEGATIVE_INSPIRATORY_FORCE =
        "{p=%d,y=%d,%c:NIF}";
StringId MiscStrs::TREND_SET_OXYGEN_PERCENTAGE =
        "{p=%d,y=%d,%c:\\[O{S:2}%%\\]}";
StringId MiscStrs::TREND_OXYGEN_PERCENTAGE =
        "{p=%d,y=%d,%c:O{S:2}%%}";
StringId MiscStrs::TREND_PEAK_CIRCUIT_PRESSURE =
        "{p=%d,y=%d,%c:P{S:PEAK}}";
StringId MiscStrs::TREND_PEAK_EXPIRATORY_FLOW_RATE =
        "{p=%d,y=%d,%c:PEF}";
StringId MiscStrs::TREND_PEAK_SPONTANEOUS_FLOW_RATE =
        "{p=%d,y=%d,%c:PSF}";
StringId MiscStrs::TREND_PLATEAU_PRESSURE =
        "{p=%d,y=%d,%c:P{S:PL}}";
StringId MiscStrs::TREND_VITAL_CAPACITY =
        "{p=%d,y=%d,%c:VC}";
StringId MiscStrs::TREND_STATIC_LUNG_COMPLIANCE =
        "{p=%d,y=%d,%c:C{S:STAT}}";
StringId MiscStrs::TREND_STATIC_AIRWAY_RESISTANCE =
        "{p=%d,y=%d,%c:R{S:STAT}}";
StringId MiscStrs::TREND_INSPIRED_TIDAL_VOLUME =
        "{p=%d,y=%d,%c:V{S:TI}}";
StringId MiscStrs::TREND_TOTAL_RESPIRATORY_RATE =
        "{p=%d,y=%d,%c:f{S:TOT}}";
StringId MiscStrs::TREND_P100 =
        "{p=%d,y=%d,%c:P{S:0.1}}";
StringId MiscStrs::TREND_PAV_COMPLIANCE =
        "{p=%d,y=%d,%c:C{S:PAV}}";
StringId MiscStrs::TREND_PAV_RESISTANCE =
        "{p=%d,y=%d,%c:R{S:PAV}}";
StringId MiscStrs::TREND_PAV_ELASTANCE =
        "{p=%d,y=%d,%c:E{S:PAV}}";
StringId MiscStrs::TREND_SET_PEAK_INSPIRATORY_FLOW =
        "{p=%d,y=%d,%c:\\[\001{S:MAX}\\]}";
StringId MiscStrs::TREND_INTRINSIC_PEEP =
        "{p=%d,y=%d,%c:PEEP{S:I}}";
StringId MiscStrs::TREND_PAV_INTRINSIC_PEEP =
        "{p=%d,y=%d,%c:PEEP{S:I PAV}}";
StringId MiscStrs::TREND_TOTAL_WORK_OF_BREATHING =
		"{p=%d,y=%d,%c:WOB{S:TOT}}";
StringId MiscStrs::TREND_SET_PEEP =
        "{p=%d,y=%d,%c:\\[PEEP\\]}";
StringId MiscStrs::TREND_SET_PEEP_HIGH =
        "{p=%d,y=%d,%c:\\[PEEP{S:H}\\]}";
StringId MiscStrs::TREND_SET_PEEP_HIGH_TIME =
        "{p=%d,y=%d,%c:\\[T{S:H}\\]}";
StringId MiscStrs::TREND_SET_PEEP_LOW =
        "{p=%d,y=%d,%c:\\[PEEP{S:L}\\]}";
StringId MiscStrs::TREND_SET_PEEP_LOW_TIME =
        "{p=%d,y=%d,%c:\\[T{S:L}\\]}";
StringId MiscStrs::TREND_TOTAL_PEEP =
        "{p=%d,y=%d,%c:PEEP{S:TOT}}";
StringId MiscStrs::TREND_SET_PERCENT_SUPPORT =
        "{p=%d,y=%d,%c:\\[%% Supp\\]}";
StringId MiscStrs::TREND_SET_PRESSURE_SENSITIVITY =
        "{p=%d,y=%d,%c:\\[P{S:SENS}\\]}";
StringId MiscStrs::TREND_SET_PRESSURE_SUPPORT_LEVEL =
        "{p=%d,y=%d,%c:\\[P{S:SUPP}\\]}";
StringId MiscStrs::TREND_SET_RESPIRATORY_RATE =
        "{p=%d,y=%d,%c:\\[f\\]}";
StringId MiscStrs::TREND_SPONTANEOUS_RAPID_SHALLOW_BREATHING_INDEX =
        "{p=%d,y=%d,%c:f/V{S:T}}";
StringId MiscStrs::TREND_SPONTANEOUS_INSPIRATORY_TIME_RATIO =
        "{p=%d,y=%d,%c:T{S:I}/T{S:TOT}}";
StringId MiscStrs::TREND_SPONTANEOUS_INSPIRATORY_TIME =
        "{p=%d,y=%d,%c:T{S:I SPONT}}";
StringId MiscStrs::TREND_SET_TIDAL_VOLUME =
        "{p=%d,y=%d,%c:\\[V{S:T}\\]}";
StringId MiscStrs::TREND_PAV_TOTAL_AIRWAY_RESISTANCE =
        "{p=%d,y=%d,%c:R{S:TOT}}";
StringId MiscStrs::TREND_SET_VOLUME_SUPPORT_LEVEL_VS =
        "{p=%d,y=%d,%c:\\[V{S:T SUPP}\\]}";
StringId MiscStrs::TREND_PERCENT_LEAK =
	"{p=%d,y=%d,%c:%%LEAK}";
StringId MiscStrs::TREND_LEAK =
        "{p=%d,y=%d,%c:LEAK}";
StringId MiscStrs::TREND_ILEAK =
        "{p=%d,y=%d,%c:V{S:LEAK}}";
StringId MiscStrs::TREND_EVENT_DETAIL =
        "{p=8,y=10,x=0:EVENT{x=0,Y=10:DETAIL}}";
StringId MiscStrs::TREND_EVENT_DETAIL_LARGE =
        "{p=8,y=12,x=0:EVENT{x=0,Y=10:DETAIL}}";
StringId MiscStrs::TREND_EVENT_ID_LABEL =
        "{p=6,y=6,T:EVENT ID}";
StringId MiscStrs::TREND_DATA_NOT_AVAILABLE_MSG =
        "{p=8,T:DATA NOT AVAILABLE}";


StringId MiscStrs::USER_EVENT_SUCTION =
        "{p=8,x=3,y=10,T:Patient's Airway Suctioned}";
StringId MiscStrs::USER_EVENT_RX =
        "{p=8,x=3,y=10,T:Administered Respiratory Treatment}";
StringId MiscStrs::USER_EVENT_BLOOD_GAS =
        "{p=8,x=3,y=10,T:Blood Gas Analysis Performed}";
StringId MiscStrs::USER_EVENT_RESPIRATORY_MANEUVER =
        "{p=8,x=3,y=10,T:Insp or Exp Pause Maneuver Performed}";
StringId MiscStrs::USER_EVENT_CIRCUIT_CHANGE =
        "{p=8,x=3,y=10,T:Changed Ventilator Breathing Circuit}";
StringId MiscStrs::USER_EVENT_START_WEANING =
        "{p=8,x=3,y=10,T:Weaning Trial Initiated}";
StringId MiscStrs::USER_EVENT_STOP_WEANING =
        "{p=8,x=3,y=10,T:Weaning Trial Terminated}";
StringId MiscStrs::USER_EVENT_BRONCHOSCOPY =
        "{p=8,x=3,y=10,T:Bronchoscopy Procedure Performed}";
StringId MiscStrs::USER_EVENT_X_RAY =
        "{p=8,x=3,y=10,T:X-Ray Performed}";
StringId MiscStrs::USER_EVENT_RECRUITMENT_MANEUVER =
        "{p=8,x=3,y=10,T:Recruitment Maneuver Performed}";
StringId MiscStrs::USER_EVENT_REPOSITION_PATIENT =
        "{p=8,x=3,y=10,T:Patient Repositioned}";
StringId MiscStrs::USER_EVENT_OTHER =
        "{p=8,x=3,y=10,T:Other Event Performed}";

StringId MiscStrs::USER_EVENT_SURFACTANT_ADMIN =
        "{p=8,x=3,y=10,T:Surfactant Administered}";
StringId MiscStrs::USER_EVENT_PRONE_POSITION =
        "{p=8,x=3,y=10,T:Prone Position}";
StringId MiscStrs::USER_EVENT_SUPINE_POSITION =
        "{p=8,x=3,y=10,T:Supine Position}";
StringId MiscStrs::USER_EVENT_LEFT_SIDE_POSITION =
        "{p=8,x=3,y=10,T:Left Side Position}";
StringId MiscStrs::USER_EVENT_RIGHT_SIDE_POSITION =
        "{p=8,x=3,y=10,T:Right Side Position}";
StringId MiscStrs::USER_EVENT_MANUAL_STIMULATION =
        "{p=8,x=3,y=10,T:Manual Stimulation}";

StringId MiscStrs::AUTO_NIF_MANEUVER_ACCEPTED =
        "{p=8,x=3,y=10,T:NIF Maneuver Accepted}";
StringId MiscStrs::AUTO_P100_MANEUVER_ACCEPTED =
        "{p=8,x=3,y=10,T:P100 Maneuver Accepted}";
StringId MiscStrs::AUTO_VC_MANEUVER_ACCEPTED =
        "{p=8,x=3,y=10,T:VC Maneuver Accepted}";
StringId MiscStrs::AUTO_EVENT_VENTILATION_INVASIVE =
        "{p=8,x=3,y=10,T:INVASIVE Vent Type Selected}";
StringId MiscStrs::AUTO_EVENT_VENTILATION_NIV =
        "{p=8,x=3,y=10,T:NIV Vent Type Selected}";
StringId MiscStrs::AUTO_EVENT_MODE_AC =
        "{p=8,x=3,y=10,T:AC Mode Selected}";
StringId MiscStrs::AUTO_EVENT_MODE_SIMV =
        "{p=8,x=3,y=10,T:SIMV Mode Selected}";
StringId MiscStrs::AUTO_EVENT_MODE_SPONT =
        "{p=8,x=3,y=10,T:SPONT Mode Selected}";
StringId MiscStrs::AUTO_EVENT_MODE_CPAP =
        "{p=8,x=3,y=10,T:CPAP Selected}";
StringId MiscStrs::AUTO_EVENT_MODE_BILEVEL =
        "{p=8,x=3,y=10,T:BiLevel Mode Selected}";
StringId MiscStrs::AUTO_EVENT_MANDATORY_VC =
        "{p=8,x=3,y=10,T:VC Mandatory Breath Type Selected}";
StringId MiscStrs::AUTO_EVENT_MANDATORY_VC_PLUS =
        "{p=8,x=3,y=10,T:VC+ Mandatory Breath Type Selected}";
StringId MiscStrs::AUTO_EVENT_MANDATORY_PC =
        "{p=8,x=3,y=10,T:PC Mandatory Breath Type Selected}";
StringId MiscStrs::AUTO_EVENT_SPONTANEOUS_PS =
        "{p=8,x=3,y=10,T:PS Spontaneous Breath Type Selected}";
StringId MiscStrs::AUTO_EVENT_SPONTANEOUS_VS =
        "{p=8,x=3,y=10,T:VS Spontaneous Breath Type Selected}";
StringId MiscStrs::AUTO_EVENT_SPONTANEOUS_NONE =
        "{p=8,x=3,y=10,T:NONE Spontaneous Breath Type Selected}";
StringId MiscStrs::AUTO_EVENT_SPONTANEOUS_PA =
        "{p=8,x=3,y=10,T:PA Spontaneous Breath Type Selected}";
StringId MiscStrs::AUTO_EVENT_SPONTANEOUS_TC =
        "{p=8,x=3,y=10,T:TC Spontaneous Breath Type Selected}";
StringId MiscStrs::AUTO_EVENT_TIME_CHANGE =
        "{p=8,x=3,y=10,T:Reset Ventilator Time/Date Setting}";
StringId MiscStrs::AUTO_EVENT_SAME_PATIENT =
        "{p=8,x=3,y=10,T:Vent Setup using SAME PATIENT Data}";
StringId MiscStrs::AUTO_EVENT_PATIENT_OCCLUSION =
        "{p=8,x=3,y=10,T:Severe Circuit OCCLUSION Detected}";
StringId MiscStrs::AUTO_EVENT_PATIENT_DISCONNECT =
        "{p=8,x=3,y=10,T:Circuit DISCONNECT Detected}";
StringId MiscStrs::AUTO_EVENT_APNEA =         
        "{p=8,x=3,y=10,T:APNEA Detected}";
StringId MiscStrs::AUTO_EVENT_INSP_MANEUVER =         
        "{p=8,x=3,y=10,T:Insp Pause Maneuver Performed}";
StringId MiscStrs::AUTO_EVENT_EXP_MANEUVER =         
        "{p=8,x=3,y=10,T:Exp Pause Maneuver Performed}";

StringId MiscStrs::AUTO_EVENT_O2_SUCTION_BUTTON =         
        "{p=8,x=3,y=10,T:Increase O{S:2}% - 2 minutes.}";
StringId MiscStrs::AUTO_EVENT_ALARM_VOLUME_CHANGE =         
        "{p=8,x=3,y=10,T:Alarm Volume Changed}";

StringId MiscStrs::AUTO_EVENT_PROX_ENABLED =
		"{p=8,x=3,y=10,T:PROX Enabled}";
StringId MiscStrs::AUTO_EVENT_PROX_DISABLED =
		"{p=8,x=3,y=10,T:PROX Disabled}";

StringId MiscStrs::DATE_FORMAT_YY_MM_DD =  
        "{p=%d,y=%d,%c:'YY/MM/DD (MM-DD)}";
StringId MiscStrs::DATE_FORMAT_MM_DD_YY_WITH_DASH =  
        "{p=%d,y=%d,%c:MM/DD/'YY (MM-DD)}";
StringId MiscStrs::DATE_FORMAT_MM_DD_YY_WITH_SLASH =  
        "{p=%d,y=%d,%c:MM/DD/'YY (MM/DD)}";
StringId MiscStrs::DATE_FORMAT_DD_MM_YY =  
        "{p=%d,y=%d,%c:DD/MM/'YY (DD.MM)}";
StringId MiscStrs::DATE_FORMAT_DD_MMM_YY =  
        "{p=%d,y=%d,%c:DD MMM 'YY (DD.MM)}";
StringId MiscStrs::DATE_FORMAT_YY_MMM_DD =  
        "{p=%d,y=%d,%c:'YY MMM DD (MM-DD)}";



StringId MiscStrs::DATE_FORMAT_BUTTON_TITLE =
        "{p=10,y=12,T:Date Format}";
StringId MiscStrs::DATE_FORMAT_HELP_MESSAGE =
        "{p=14,T,y=17:Date Format}";


//
// UpperOtherScreensSubScreen...
//
StringId MiscStrs::UPOTHER_SUBSCREEN_TITLE =
        "{p=14,y=18:Other Screens}";
StringId MiscStrs::UPOTHER_SUBSCREEN_DIAG_CODE =
        "{p=10,y=14:Diagnostic Code{x=40,Y=16:Log}}";
StringId MiscStrs::UPOTHER_SUBSCREEN_OPER_TIME =
        "{p=10,y=14:Operational{x=7,Y=16:Time Log}}";
StringId MiscStrs::UPOTHER_SUBSCREEN_SW_REV =
        "{p=10,x=12,y=14:Ventilator{x=0,Y=16:Configuration}}";
StringId MiscStrs::UPOTHER_SUBSCREEN_TEST_RESULT =
        "{p=10,y=14:SST Result{x=22,Y=16:Log}}";
StringId MiscStrs::UPOTHER_SUBSCREEN_TEST_SUMMARY =
        "{p=10,x=17,y=14:Test{x=0,Y=16:Summary}}";
//
// Text that appears within the Diagnostic Code subscreens
//
StringId MiscStrs::DLOG_MENU_SUBSCREEN_TITLE =
        "{p=14,y=18:Diagnostic Code Log Menu}";
StringId MiscStrs::DLOG_SYS_TITLE =
        "{p=14,y=18:System Diagnostic Log}";
StringId MiscStrs::DLOG_SYSTEM_INFO_TITLE =
        "{p=14,y=18:System Information Log}";
StringId MiscStrs::DLOG_EST_SST_TITLE =
        "{p=14,y=18:EST/SST Diagnostic Log}";
StringId MiscStrs::DLOG_TEST_UNDEFINED = "UNDEFINED";
StringId MiscStrs::DLOG_TEST_PASSED = "PASSED";
StringId MiscStrs::DLOG_TEST_OVERRIDDEN = "OVERRIDDEN";
StringId MiscStrs::DLOG_EST_SINGLE_TEST_INITIATED = "{T:EST: {N:SINGLE TEST INITIATED}}";
StringId MiscStrs::DLOG_TEST_ALL_TESTS_REQUIRED = "ALL TESTS required";
StringId MiscStrs::DLOG_TEST_MINOR_FAILURE = "ALERT";
StringId MiscStrs::DLOG_TEST_FAILED = "FAILED";
StringId MiscStrs::DLOG_TEST_NOT_INSTALLED = "NOT INSTALLED";
StringId MiscStrs::DLOG_TEST_EVENT_LABEL = "TEST/EVENT";
StringId MiscStrs::DLOG_TEST_TYPE_LABEL = "TYPE";
StringId MiscStrs::DLOG_TEST_NOTES_LABEL = "NOTES";
StringId MiscStrs::DLOG_TEST_TIME_DATE_LABEL = "TIME";
StringId MiscStrs::DLOG_TEST_CODE_LABEL = "CODE";
StringId MiscStrs::DLOG_TEST_SELECT_LABEL = "Select";
StringId MiscStrs::DLOG_ASSERTION_FAILURE = "Assertion";
StringId MiscStrs::DLOG_SST_COMPLETED = "{T:SST: {N:COMPLETED}}";
StringId MiscStrs::DLOG_EST_COMPLETED = "{T:EST: {N:COMPLETED}}";
StringId MiscStrs::DLOG_SST = "SST";
StringId MiscStrs::DLOG_EST = "EST";
StringId MiscStrs::DLOG_TRANSITION_TIMEOUT = "State transition timeout.";
StringId MiscStrs::DLOG_CORRUPTED = "CAN'T DISPLAY CORRUPTED LOG";
StringId MiscStrs::DLOG_DATE_TIME_BEFORE_CHANGE = "Date/time before change";
StringId MiscStrs::DLOG_DATE_TIME_CHANGED = "Date/time changed";
StringId MiscStrs::DLOG_DATE_TIME_BEFORE_SYNC = "Date/time before vent clock synchronized.";
StringId MiscStrs::DLOG_DATE_TIME_SYNCHRONIZED = "Date/time vent clock synchronized";
StringId MiscStrs::DLOG_VENT_IN_OP_LATCHED = "Vent Inop Latched.";
StringId MiscStrs::DLOG_INTENTIONAL_RESET_FORCED = "Intentional Reset Forced.";
StringId MiscStrs::DLOG_SETTINGS_XACTION_FAILED = "Settings Transaction Failed.";
StringId MiscStrs::DLOG_SETTINGS_XACTION_SUCCEEDED = "Settings Transaction Succeeded.";
StringId MiscStrs::DLOG_SETTINGS_SYNC_MISMATCH = "Settings sync mismatch.";
StringId MiscStrs::DLOG_NOVRAM_ACCESS_FAULT = "NOVRAM access fault.";
StringId MiscStrs::DLOG_NOVRAM_RESTORE_FAULT = "NOVRAM restore fault.";
StringId MiscStrs::DLOG_OUTCOME = "T:Outcome: ";
StringId MiscStrs::DLOG_TASK = "Task";
StringId MiscStrs::DLOG_LINE = "Line";
StringId MiscStrs::DLOG_NMI = "Non-maskable Interrupt";
StringId MiscStrs::DLOG_NMI_SHORT = "NMI:";
StringId MiscStrs::DLOG_SRCID = "SourceId:";
StringId MiscStrs::DLOG_ERRCODE = "ErrCode:";
StringId MiscStrs::DLOG_EXCEPTION_VECTOR = "EV:";
StringId MiscStrs::DLOG_PROGRAM_COUNTER = "Program counter";
StringId MiscStrs::DLOG_PC = "PC:";
StringId MiscStrs::DLOG_FA = "FA:";
StringId MiscStrs::DLOG_TASK_ID = "TID:";
StringId MiscStrs::DLOG_TEST_NUMBER = "TestNumber";
StringId MiscStrs::DLOG_TYPE_ALERT = "Alert";
StringId MiscStrs::DLOG_TYPE_FAILURE = "Failure";
StringId MiscStrs::DLOG_TYPE_ALERT_CHP_TXT = "{T:Alert}";
StringId MiscStrs::DLOG_WATCHDOG_TIMER_FAILED = "Watchdog Timer Failed.";
StringId MiscStrs::DLOG_BD = "BD";
StringId MiscStrs::DLOG_GUI = "GUI";
StringId MiscStrs::DLOG_UNKNOWN_DIAGCODE_EVENT = "Unknown event";
StringId MiscStrs::DLOG_DATA_CORRUPT = "Data corrupt";
//
// Lower Screen stuff...
//
// TBT
StringId MiscStrs::APNEA_SETUP_TAB_BUTTON_LABEL =
        "{p=14,y=18,T:APNEA{p=10,x=14,y=31,T:SETUP}}";
StringId MiscStrs::APNEA_CURRENT_SETUP_TAB_BUTTON_LABEL =
        "{p=10,x=2,y=11,T:CURRENT{p=14,x=0,y=28,T:APNEA}}";
StringId MiscStrs::APNEA_PROPOSED_SETUP_TAB_BUTTON_LABEL =
        "{p=10,y=11,I:PROPOSED{p=14,x=2,y=28,T:APNEA}}";
StringId MiscStrs::APNEA_SETUP_TEXT =
        "{p=12,y=14:APNEA SETUP}";
// TBT
StringId MiscStrs::EXH_MAND_TIDAL_VOL_SLIDER_MAX_LABEL =
        "{p=8,x=27,y=15:mL}";
StringId MiscStrs::EXH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL =
        "{p=18,x=12,y=20:V{S:TE MAND}}";
StringId MiscStrs::EXPIRATORY_TIME_BUTTON_TITLE_LARGE =
        "{p=18,y=20:T{S:E}}";
StringId MiscStrs::PEEP_LOW_TIME_BUTTON_TITLE_LARGE =
        "{p=18,y=20:T{S:L}}";
StringId MiscStrs::EXPIRATORY_TIME_HELP_MESSAGE =
        "{p=14,y=17:T{S:E}{T: = Expiratory time}}";
StringId MiscStrs::PEEP_LOW_TIME_HELP_MESSAGE =
        "{p=14,y=17:T{S:L}{T: = Low PEEP time}}";
StringId MiscStrs::FLOW_SENSITIVITY_BUTTON_TITLE_LARGE =
        "{p=18,y=20:\001{S:SENS}}";
StringId MiscStrs::FLOW_SENSITIVITY_HELP_MESSAGE =
        "{p=14,y=17:\001{S:SENS}{T: = Flow sensitivity}}";
StringId MiscStrs::EXPIRATORY_SENSITIVITY_BUTTON_TITLE_LARGE =
        "{p=18,y=20:E{S:SENS}}";
StringId MiscStrs::EXPIRATORY_SENSITIVITY_HELP_MESSAGE =
        "{p=14,y=17:E{S:SENS}{T: = Spont expiratory sensitivity %}}";
StringId MiscStrs::EXPIRATORY_SENSITIVITY_PAV_HELP_MESSAGE =
        "{p=14,y=17:E{S:SENS}{T: = Spont expiratory sensitivity (L/min)}}";
StringId MiscStrs::IE_RATIO_BUTTON_TITLE_LARGE =
        "{p=18,y=20:I:E}";
StringId MiscStrs::PEEP_HIGH_PEEP_LOW_RATIO_BUTTON_TITLE_LARGE =
        "{p=18,y=20:T{S:H}:T{S:L}}";
StringId MiscStrs::IE_RATIO_HELP_MESSAGE =
        "{p=14,y=17:I:E{T: = Inspiratory to expiratory ratio}}";
StringId MiscStrs::PEEP_HIGH_PEEP_LOW_RATIO_HELP_MESSAGE =
        "{p=14,y=17:T{S:H}:T{S:L}{T: = PEEP{S:H} time to PEEP{S:L} time ratio}}";
StringId MiscStrs::INSPIRATORYPRESSURE_BUTTON_TITLE_LARGE =
        "{p=18,y=20:P{S:I}}";
StringId MiscStrs::INSPIRATORY_PRESSURE_HELP_MESSAGE =
        "{p=14,y=17:P{S:I}{T: = Inspiratory pressure}}";
StringId MiscStrs::INSPIRATORY_TIME_BUTTON_TITLE_LARGE =
        "{p=18,y=20:T{S:I}}";
StringId MiscStrs::PEEP_HIGH_TIME_BUTTON_TITLE_LARGE =
        "{p=18,y=20:T{S:H}}";
StringId MiscStrs::INSPIRATORY_TIME_HELP_MESSAGE =
        "{p=14,y=17:T{S:I}{T: = Inspiratory time}}";
StringId MiscStrs::PEEP_HIGH_TIME_HELP_MESSAGE =
        "{p=14,y=17:T{S:H}{T: = High PEEP time}}";

	//-------------------------------------------------------------
	// Mode Setting strings...
	//-------------------------------------------------------------
StringId MiscStrs::MODE_BUTTON_TITLE_SMALL =
        "{p=10,y=12,T:Mode}";
StringId MiscStrs::MODE_HELP_MESSAGE =
        "{p=14,y=17:Mode of Ventilation}";
	// setting values...
StringId MiscStrs::MODE_AC_VALUE =
        "{p=%d,y=%d,%c:A/C}";
StringId MiscStrs::MODE_SIMV_VALUE =
        "{p=%d,y=%d,%c:SIMV}";
StringId MiscStrs::MODE_SPONT_VALUE =
        "{p=%d,y=%d,%c:SPONT}";
StringId MiscStrs::MODE_BILEVEL_VALUE =
        "{p=%d,y=%d,%c:BILEVEL}";
StringId MiscStrs::MODE_CPAP_VALUE =
        "{p=%d,y=%d,%c:CPAP}";
	// Vent Setup title bar values...
StringId MiscStrs::MODE_AC_TITLE =
        "{p=12,x=39,y=16,%c:A/C}";

StringId MiscStrs::MODE_SIMV_TITLE =
        "{p=12,x=33,y=16,%c:SIMV}";
StringId MiscStrs::MODE_SPONT_TITLE =
        "{p=12,x=24,y=16,%c:SPONT}";
StringId MiscStrs::MODE_BILEVEL_TITLE =
        "{p=12,x=16,y=16,%c:BILEVEL}";
StringId MiscStrs::MODE_CPAP_TITLE =
        "{p=12,x=33,y=16,%c:CPAP}";
StringId MiscStrs::MODE_APNEA_TITLE =
        "{p=12,x=30,y=16,%c:AV}";
	// Main Settings Area title bar values...
StringId MiscStrs::MODE_MSA_AC_TITLE =
        "{p=18,x=38,y=20:A/C}";
StringId MiscStrs::MODE_MSA_SIMV_TITLE =
        "{p=18,x=31,y=20:SIMV}";
StringId MiscStrs::MODE_MSA_SPONT_TITLE =
        "{p=18,x=23,y=20:SPONT}";
StringId MiscStrs::MODE_MSA_BILEVEL_TITLE =
        "{p=18,x=2,y=20:BILEVEL}";
StringId MiscStrs::MODE_MSA_CPAP_TITLE =
        "{p=18,x=31,y=20:CPAP}";
	// Apnea Ventilation In-Progress title bar values...
StringId MiscStrs::MODE_APNEA_VENT_TITLE =
        "{p=18,x=33,y=20:AV}";
	// Safety-PCV In-Progress title bar value...
StringId MiscStrs::MODE_SAFETY_PCV_TITLE =
        "{p=18,x=28,y=20:A/C}";

		//-------------------------------------------------------------
		// Mandatory Type Setting strings...
		//-------------------------------------------------------------
StringId MiscStrs::MAND_TYPE_BUTTON_TITLE_SMALL =
        "{p=10,y=12,T:Mandatory Type}";
StringId MiscStrs::APNEA_MAND_TYPE_HELP_MESSAGE =
        "{p=14,y=17,T:Apnea mandatory type}";
StringId MiscStrs::MAND_TYPE_HELP_MESSAGE =
        "{p=14,y=17:Type of Mandatory Ventilation}";
	// setting values...
StringId MiscStrs::MAND_TYPE_PCV_VALUE =
        "{p=%d,y=%d,%c:PC}";
StringId MiscStrs::MAND_TYPE_VCV_VALUE =
        "{p=%d,y=%d,%c:VC}";
StringId MiscStrs::MAND_TYPE_VCP_VALUE =
        "{p=%d,y=%d,%c:VC+}";
	// Vent Setup title bar values...
StringId MiscStrs::MAND_TYPE_PCV_TITLE =
        "{p=12,x=93,y=16,%c:PC}";
StringId MiscStrs::MAND_TYPE_VCV_TITLE =
        "{p=12,x=93,y=16,%c:VC}";
StringId MiscStrs::MAND_TYPE_VCP_TITLE =
        "{p=12,x=91,y=16,%c:VC+}";
	// Main Settings Area title bar values...
StringId MiscStrs::MAND_TYPE_MSA_PCV_TITLE =
        "{p=18,x=90,y=20:PC}";
StringId MiscStrs::MAND_TYPE_MSA_VCV_TITLE =
        "{p=18,x=90,y=20:VC}";
StringId MiscStrs::MAND_TYPE_MSA_VCP_TITLE =
        "{p=18,x=83,y=20:VC+}";
StringId MiscStrs::MAND_TYPE_MSA_PC_MAN_INSP_TITLE =
        "{p=14,x=93,y=14:PC{p=8,x=60,Y=9:Manual Insp only}}";
StringId MiscStrs::MAND_TYPE_MSA_VC_MAN_INSP_TITLE =
        "{p=14,x=93,y=14:VC{p=8,x=60,Y=9:Manual Insp only}}";
	// Apnea Ventilation In-Progress title bar values...
StringId MiscStrs::MAND_TYPE_APNEA_VENT_PCV_TITLE =
        "{p=18,x=181,y=20:PC}";
StringId MiscStrs::MAND_TYPE_APNEA_VENT_VCV_TITLE =
        "{p=18,x=181,y=20:VC}";

		//-------------------------------------------------------------
		// Spontaneous Type Setting strings...
		//-------------------------------------------------------------
StringId MiscStrs::SPONT_TYPE_BUTTON_TITLE_SMALL =
        "{p=10,y=12,T:Spontaneous Type}";
StringId MiscStrs::SPONT_TYPE_HELP_MESSAGE =
        "{p=14,y=17:Type of Spontaneous Ventilation}";
	// setting values...
StringId MiscStrs::SPONT_TYPE_OFF_VALUE =
        "{p=%d,y=%d,%c:NONE}";
StringId MiscStrs::SPONT_TYPE_PSV_VALUE =
        "{p=%d,y=%d,%c:PS}";
StringId MiscStrs::SPONT_TYPE_ATC_VALUE =
        "{p=%d,y=%d,%c:TC}";
StringId MiscStrs::SPONT_TYPE_VSV_VALUE =
        "{p=%d,y=%d,%c:VS}";
StringId MiscStrs::SPONT_TYPE_PAV_VALUE =
        "{p=%d,y=%d,%c:PA}";
	// Vent Setup title bar values...
StringId MiscStrs::SPONT_TYPE_OFF_TITLE =
        "{p=12,x=19,y=16,%c:NONE}";
StringId MiscStrs::SPONT_TYPE_PSV_TITLE =
        "{p=12,x=31,y=16,%c:PS}";
StringId MiscStrs::SPONT_TYPE_ATC_TITLE =
        "{p=12,x=31,y=16,%c:TC}";
StringId MiscStrs::SPONT_TYPE_VSV_TITLE =
        "{p=12,x=31,y=16,%c:VS}";
StringId MiscStrs::SPONT_TYPE_PAV_TITLE =
        "{p=12,x=31,y=16,%c:PA}";
	// Main Settings Area title bar values...
StringId MiscStrs::SPONT_TYPE_MSA_PSV_TITLE =
        "{p=18,x=36,y=20:PS}";
StringId MiscStrs::SPONT_TYPE_MSA_ATC_TITLE =
        "{p=18,x=36,y=20:TC}";
StringId MiscStrs::SPONT_TYPE_MSA_VSV_TITLE =
        "{p=18,x=36,y=20:VS}";
StringId MiscStrs::SPONT_TYPE_MSA_PAV_TITLE =
        "{p=18,x=36,y=20:PA}";

		//-------------------------------------------------------------
		// Trigger Type Setting strings...
		//-------------------------------------------------------------
StringId MiscStrs::TRIGGER_TYPE_BUTTON_TITLE_SMALL =
        "{p=10,y=12,T:Trigger Type}";
StringId MiscStrs::TRIGGER_TYPE_HELP_MESSAGE =
        "{p=14,y=17:Trigger Type}";
	// setting values...
StringId MiscStrs::TRIGGER_TYPE_FLOW_VALUE =
        "{p=%d,y=%d,%c:\001-TRIG}";
StringId MiscStrs::TRIGGER_TYPE_PRESSURE_VALUE =
        "{p=%d,y=%d,%c:P-TRIG}";
	// Vent Setup title bar values...
StringId MiscStrs::TRIGGER_TYPE_FLOW_TITLE =
        "{p=12,x=20,y=16,%c:\001{p=10:-TRIG}}";
StringId MiscStrs::TRIGGER_TYPE_PRESSURE_TITLE =
        "{p=12,x=20,y=16,%c:P{p=10:-TRIG}}";
	// Main Settings Area title bar values...
StringId MiscStrs::TRIGGER_TYPE_MSA_FLOW_TRIG_TITLE =
        "{p=18,x=18,y=20:\001{p=14:-TRIG}}";
StringId MiscStrs::TRIGGER_TYPE_MSA_PRESSURE_TRIG_TITLE =
        "{p=18,x=18,y=20:P{p=14:-TRIG}}";
	// Safety-PCV In-Progress title bar value...
StringId MiscStrs::TRIGGER_TYPE_SAFETY_PCV_TITLE =
        "{p=18,x=406,y=20:P{p=14:-TRIG}}";

		//-----------------------------------------------------
		// Vent Type Setting strings...
		//-----------------------------------------------------
StringId MiscStrs::VENT_TYPE_BUTTON_TITLE_SMALL =
        "{p=10,y=12,T:Vent Type}";
StringId MiscStrs::VENT_TYPE_HELP_MESSAGE =
        "{p=14,y=17:Vent Type{T: = Invasive or NIV}}";

		// settings values
StringId MiscStrs::VENT_TYPE_INVASIVE_VALUE =
        "{p=%d,y=%d,%c:INVASIVE}";
StringId MiscStrs::VENT_TYPE_NIV_VALUE =
        "{p=%d,y=%d,%c:NIV}";

		// setup title bar values...
StringId MiscStrs::VENT_TYPE_NIV_TITLE =
        "{p=12,x=2,y=16,%c:N}";
StringId MiscStrs::VENT_TYPE_INVASIVE_TITLE =
        "{p=12,x=2,y=16,%c: }";

		// main settings area title bar values...
StringId MiscStrs::VENT_TYPE_MSA_NIV_TITLE =
        "{p=18,x=1,y=20:N}";
StringId MiscStrs::VENT_TYPE_MSA_INVASIVE_TITLE =
        "{p=18,x=1,y=20: }";

		// used in alarm and status area...
StringId MiscStrs::STATUS_VENT_TYPE_NIV_LABEL =
        "{p=18,x=300,y=36:NIV}";
StringId MiscStrs::TOUCHABLE_VENT_TYPE_NIV_MSG =
        "{p=12,y=17:NIV{T: = Non-Invasive Ventilation}}";
StringId MiscStrs::HIGH_SPONT_INSP_TIME_ALERT =
        "{p=18,x=380,y=36:\004T{S:I SPONT}}";
StringId MiscStrs::TOUCHABLE_HIGH_SPONT_INSP_TIME_MSG =
        "{p=12,y=17:\004T{S:I SPONT}{T: = High spontaneous inspiratory time}}";

		//-----------------------------------------------------------------
	   	// Tube Type Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::TUBE_TYPE_BUTTON_TITLE_SMALL =
        "{p=10,y=12,x=14:Tube Type}";
StringId MiscStrs::TUBE_TYPE_BUTTON_TITLE2_SMALL =
        "{p=10,y=12,T:Tube Type}";
StringId MiscStrs::TUBE_TYPE_BUTTON_TITLE3_SMALL =
        "{p=10,y=12,x=4:Tube Type}";
StringId MiscStrs::TUBE_TYPE_HELP_MESSAGE =
        "{p=14,y=17:Tube Type{T: = ET, TRACH}}";
	// setting values...
StringId MiscStrs::TUBE_TYPE_ET_VALUE =
        "{p=%d,y=%d,%c:ET}";
StringId MiscStrs::TUBE_TYPE_TRACH_VALUE =
        "{p=%d,y=%d,%c:TRACH}";
	// used in Alarm and Status area...
StringId MiscStrs::STATUS_TUBE_TYPE_LABEL =
        "{p=8,x=300,y=27,T:Tube Type:}";
StringId MiscStrs::STATUS_TUBE_TYPE_ET_VALUE =
        "{p=8,x=375,y=27,T:ET}";
StringId MiscStrs::STATUS_TUBE_TYPE_TRAC_VALUE =
        "{p=8,x=375,y=27,T:TRACH}";
	// used in Vent Startup subscreen (part of Same-Patient button label)...
StringId MiscStrs::STATUS_TUBE_TYPE_ET_TEXT = "ET";
StringId MiscStrs::STATUS_TUBE_TYPE_TRACH_TEXT = "TRACH";

		//-----------------------------------------------------------------
	   	// Flow Pattern Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::APNEA_FLOW_PATTERN_HELP_MESSAGE =
        "{p=14,y=17,T:Apnea flow pattern}";
StringId MiscStrs::FLOW_PATTERN_HELP_MESSAGE =
        "{p=14,T,y=17:Flow pattern}";
	// flow pattern setting values...
	// the x and y offsets are hard-coded since the two line symbol and text
	// don't conform to a standard font size that can be automatically positioned
	// The p=%d,y=%d format sub-strings allow the string to be processed by 
	// DiscreteSettingButton::setValueString() as if it were a normal format
	// string, but these values are overriden by the hard-coded values that
	// follow.
StringId MiscStrs::FLOW_PATTERN_SQUARE_VALUE_SMALL =
        "{p=%d,y=%d,p=12,y=22,x=2:\007{p=8,x=0,Y=10,%c:SQUARE}}";
StringId MiscStrs::FLOW_PATTERN_RAMP_VALUE_SMALL =
        "{p=%d,y=%d,p=12,y=22:\006{p=8,x=9,Y=10,%c:RAMP}}";
StringId MiscStrs::FLOW_PATTERN_SQUARE_VALUE_LARGE =
        "{p=%d,y=%d,p=24,y=34,x=2:\007{p=12,x=0,y=50,%c:SQUARE}}";
StringId MiscStrs::FLOW_PATTERN_RAMP_VALUE_LARGE =
        "{p=%d,y=%d,p=24,y=34:\006{p=12,x=9,y=50,%c:RAMP}}";

		//-----------------------------------------------------------------
		// Humidication Type Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::HUMID_TYPE_BUTTON_TITLE =
        "{p=10,y=12,T:Humidification Type}";
StringId MiscStrs::HUMID_TYPE_HELP_MESSAGE =
        "{p=14,y=17,T:Humidification Type}";
	// setting values...
StringId MiscStrs::HUMID_TYPE_NON_HEATED_VALUE =
        "{p=10,p=%d,y=%d:{p=10,%c:Non-heated exp tube}}";
StringId MiscStrs::HUMID_TYPE_HEATED_TUBING_VALUE =
        "{p=10,p=%d,y=%d:{p=10,%c:Heated exp tube}}";
StringId MiscStrs::HUMID_TYPE_HME_VALUE =
        "{p=%d,y=%d,%c:HME}";

	// used in Alarm and Status area...
StringId MiscStrs::STATUS_HUMID_TYPE_LABEL =
        "{p=8,x=5,y=40,T:Humidification Type:}";
StringId MiscStrs::STATUS_HUMID_TYPE_NON_HEATED_VALUE =
        "{p=8,x=110,y=40,T:Non-heated exp tube}";
StringId MiscStrs::STATUS_HUMID_TYPE_HEATED_VALUE =
        "{p=8,x=110,y=40,T:Heated exp tube}";
StringId MiscStrs::STATUS_HUMID_TYPE_HME_VALUE =
        "{p=8,x=110,y=40,T:HME}";

		//-----------------------------------------------------------------
		// O2 Sensor Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::O2_SENSOR_BUTTON_TITLE =
        "{p=10,y=12,T:O{S:2} Sensor}";
StringId MiscStrs::O2_SENSOR_HELP_MESSAGE =
        "{p=14,y=17,T:O{S:2} Sensor Enable/Disable/Cal}";
StringId MiscStrs::O2_SENSOR_DISABLED_MESSAGE =
        "{p=14,c=8,y=17,T:Note: O{S:2} Sensor disabled.}";
	// setting values...
StringId MiscStrs::O2_SENSOR_DISABLED_VALUE =
        "{p=%d,y=%d,%c:Disabled}";
StringId MiscStrs::O2_SENSOR_ENABLED_VALUE =
        "{p=%d,y=%d,%c:Enabled}";
StringId MiscStrs::O2_SENSOR_CALIBRATION_VALUE =
        "{p=%d,y=%d,%c:Calibration}";


		//-----------------------------------------------------------------
		// Patient Circuit Type Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::CIRCUIT_TYPE_BUTTON_TITLE =
        "{p=10,y=12,T:Patient Circuit Type}";
StringId MiscStrs::CIRCUIT_TYPE_HELP_MSG =
        "{p=14,y=17:Patient Circuit Type}";
	// setting values...
StringId MiscStrs::CIRCUIT_TYPE_ADULT_VALUE =
        "{p=%d,y=%d,%c:ADULT}";
StringId MiscStrs::CIRCUIT_TYPE_PED_VALUE =
        "{p=%d,y=%d,%c:PEDIATRIC}";
StringId MiscStrs::CIRCUIT_TYPE_NEO_VALUE =
        "{p=%d,y=%d,%c:NEONATAL}";
	// displayed in new Vent Setup (with changeable IBW) screen...
StringId MiscStrs::CCT_TYPE_LABEL =
        "{p=6,y=8,T:CIRCUIT TYPE:}";
StringId MiscStrs::ADULT_CIRCUIT_TYPE_MSG =
        "{p=10,y=14:ADULT}";
StringId MiscStrs::PED_CIRCUIT_TYPE_MSG =
        "{p=10,y=14:PEDIATRIC}";
StringId MiscStrs::NEO_CIRCUIT_TYPE_MSG =
        "{p=10,y=14:NEONATAL}";
	// used in Alarm and Status area...
StringId MiscStrs::STATUS_CCT_TYPE_LABEL =
        "{p=8,x=5,y=27,T:Circuit Type:}";
StringId MiscStrs::STATUS_CCT_TYPE_ADULT_VALUE =
        "{p=8,x=110,y=27,T:Adult}";
StringId MiscStrs::STATUS_CCT_TYPE_PEDIATRIC_VALUE =
        "{p=8,x=110,y=27,T:Pediatric}";
StringId MiscStrs::STATUS_CCT_TYPE_NEONATAL_VALUE =
        "{p=8,x=110,y=27,T:Neonatal}";

		//-----------------------------------------------------------------
		// Nominal Line Voltage Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::NOMINAL_LINE_VOLTAGE_BUTTON_TITLE =
        "{p=10,y=12,T:Nominal Line Voltage}";
StringId MiscStrs::NOMINAL_LINE_VOLTAGE_HELP_MESSAGE =
        "{p=14,T,y=17:Nominal Line Voltage}";
	// setting values...
StringId MiscStrs::NOMINAL_LINE_VAC_100_VALUE =
        "{p=%d,y=%d,%c:100 v}";
StringId MiscStrs::NOMINAL_LINE_VAC_120_VALUE =
        "{p=%d,y=%d,%c:120 v}";
StringId MiscStrs::NOMINAL_LINE_VAC_220_VALUE =
        "{p=%d,y=%d,%c:220 v}";
StringId MiscStrs::NOMINAL_LINE_VAC_230_VALUE =
        "{p=%d,y=%d,%c:230 v}";
StringId MiscStrs::NOMINAL_LINE_VAC_240_VALUE =
        "{p=%d,y=%d,%c:240 v}";

		//-----------------------------------------------------------------
		// Pressure Unit Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::PRESSURE_UNIT_BUTTON_TITLE =
        "{p=10,y=12,T:Pressure Unit}";
StringId MiscStrs::PRESSURE_UNIT_HELP_MESSAGE =
        "{p=14,T,y=17:Pressure Unit}";
	// setting values...
StringId MiscStrs::PRESSURE_UNIT_CMH2O_VALUE =
        "{p=%d,y=%d,%c:cmH{S:2}O}";
StringId MiscStrs::PRESSURE_UNIT_HPA_VALUE =
        "{p=%d,y=%d,%c:hPa}";

//
// Communication Setup SubScreen
//
StringId MiscStrs::CURRENT_COMM_SETUP_SUBSCREEN_TITLE =
        "{p=14,y=18:Current Communication Setup}";
StringId MiscStrs::PROPOSED_COMM_SETUP_SUBSCREEN_TITLE =
        "{p=14,y=18,I:Proposed {N:Communication Setup}}";

		//-----------------------------------------------------------------
		// Com1 Configuration Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::COM1_CONFIG_BUTTON_TITLE =
        "{p=10,y=12,T:1}";
StringId MiscStrs::COM2_CONFIG_BUTTON_TITLE =
        "{p=10,y=12,T:2}";
StringId MiscStrs::COM3_CONFIG_BUTTON_TITLE =
        "{p=10,y=12,T:3}";
StringId MiscStrs::COM_CONFIG_HELP_MESSAGE =
        "{p=14,T,y=17:COM Port Configuration}";
StringId MiscStrs::DCI_VALUE_TITLE =
                "{p=%d,y=%d,%c:DCI}";
StringId MiscStrs::PRINTER_VALUE_TITLE =
                "{p=%d,y=%d,%c:Printer}";
StringId MiscStrs::SPACELAB_VALUE_TITLE =
                "{p=%d,y=%d,%c:SpaceLabs}";
StringId MiscStrs::SENSOR_VALUE_TITLE =
                "{p=%d,y=%d,%c:Waveforms}";
StringId MiscStrs::PHILIPS_VALUE_TITLE =
                "{p=%d,y=%d,%c:Philips}";
StringId MiscStrs::VSET_VALUE_TITLE =
                "{p=%d,y=%d,%c:VentSet}";

		//-----------------------------------------------------------------
		// Baud Rate Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::DCI_BAUD_RATE_BUTTON_TITLE =
        "{p=10,y=12,T:Baud Rate}";
StringId MiscStrs::DCI_BAUD_RATE_HELP_MESSAGE =
        "{p=14,T,y=17:DCI Baud Rate}";
StringId MiscStrs::SYSTEM_TEST_BAUD_RATE_BUTTON_TITLE =
        "{p=10,y=12,T:External Control Baud Rate}";
StringId MiscStrs::SYSTEM_TEST_BAUD_RATE_HELP_MESSAGE =
        "{p=14,T,y=17:System Test Baud Rate}";
	// setting values...
StringId MiscStrs::BAUD_1200_VALUE =
        "{p=%d,y=%d,%c:1200}";
StringId MiscStrs::BAUD_2400_VALUE =
        "{p=%d,y=%d,%c:2400}";
StringId MiscStrs::BAUD_4800_VALUE =
        "{p=%d,y=%d,%c:4800}";
StringId MiscStrs::BAUD_9600_VALUE =
        "{p=%d,y=%d,%c:9600}";
StringId MiscStrs::BAUD_19200_VALUE =
        "{p=%d,y=%d,%c:19200}";
StringId MiscStrs::BAUD_38400_VALUE =
        "{p=%d,y=%d,%c:38400}";
        
		//-----------------------------------------------------------------
		// DCI Data Bits Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::DCI_DATA_BITS_BUTTON_TITLE = 
        "{p=10,y=12,T:Data Bits}";
StringId MiscStrs::DCI_DATA_BITS_HELP_MESSAGE =
        "{p=14,T,y=17:DCI Data Bits}";
	// setting values...
StringId MiscStrs::DCI_BITS_7_VALUE =
        "{p=%d,y=%d,%c:7}";
StringId MiscStrs::DCI_BITS_8_VALUE =
        "{p=%d,y=%d,%c:8}";

		//-----------------------------------------------------------------
		// DCI Parity Mode Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::DCI_PARITY_MODE_BUTTON_TITLE =
        "{p=10,y=12,T:Parity Mode}";
StringId MiscStrs::DCI_PARITY_MODE_HELP_MESSAGE =
        "{p=14,T,y=17:DCI Parity Mode}";
	// setting values...
StringId MiscStrs::DCI_EVEN_PARITY_VALUE =
        "{p=%d,y=%d,%c:Even}";
StringId MiscStrs::DCI_ODD_PARITY_VALUE =
        "{p=%d,y=%d,%c:Odd}";
StringId MiscStrs::DCI_NO_PARITY_VALUE =
        "{p=%d,y=%d,%c:None}";

		//-----------------------------------------------------------------
	   	// Tube I.D. Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::TUBE_ID_BUTTON_TITLE_SMALL =
        "{p=10,y=12,x=14:Tube I.D. }";
StringId MiscStrs::TUBE_ID_BUTTON_TITLE2_SMALL =
        "{p=10,y=12,T:Tube I.D. }";
StringId MiscStrs::TUBE_ID_BUTTON_TITLE3_SMALL =
        "{p=10,y=12,x=6:Tube I.D. }";
StringId MiscStrs::TUBE_ID_HELP_MESSAGE =
        "{p=14,y=17:Tube I.D.{T: = Tube inside diameter}}";
	// used in Vent Startup subscreen (part of Same-Patient button label)...
StringId MiscStrs::STATUS_TUBE_ID_LABEL =
        "{p=8,x=300,y=40,T:Tube I.D.:}";
StringId MiscStrs::STATUS_TUBE_ID_STRING_FORMAT = "{p=8,x=375,y=40,T:%.1f mm}";


StringId MiscStrs::OXYGEN_PERCENTAGE_BUTTON_TITLE_LARGE =
        "{p=18,y=20:O{S:2}}";
StringId MiscStrs::OXYGEN_PERCENTAGE_HELP_MESSAGE =
        "{p=14,y=17:O{S:2}{T: = Oxygen %}}";

StringId MiscStrs::PEAK_CCT_PRESSURE_SLIDER_MAX_LABEL =
        "{p=8,x=6,y=15:cmH{S:2}O}";
StringId MiscStrs::PEAK_CCT_PRESSURE_SLIDER_MAX_HPA_LABEL =
        "{p=8,x=25,y=15:hPa}";
StringId MiscStrs::PEAK_CCT_PRESSURE_SLIDER_MIN_LABEL =
        "{p=18,x=5,y=20:P{S:PEAK}}";

StringId MiscStrs::PEAK_FLOW_BUTTON_TITLE_LARGE =
        "{p=18,y=20:\001{S:MAX}}";
StringId MiscStrs::PEAK_FLOW_HELP_MESSAGE =
        "{p=14,y=17:\001{S:MAX}{T: = Peak flow}}";

StringId MiscStrs::PEEP_BUTTON_TITLE_LARGE =
        "{p=18,y=20:PEEP}";
StringId MiscStrs::PEEP_HELP_MESSAGE =
        "{p=14,y=17:PEEP{T: = Positive end expiratory pressure}}";
StringId MiscStrs::PEEP_LOW_HELP_MESSAGE =
        "{p=14,y=17:PEEP{S:L}{T: = Low PEEP}}";
StringId MiscStrs::PEEP_LOW_BUTTON_TITLE_LARGE =
        "{p=18,y=20:PEEP{S:L}}";
StringId MiscStrs::PEEP_HI_HELP_MESSAGE =
        "{p=14,y=17:PEEP{S:H}{T: = High PEEP}}";
StringId MiscStrs::PEEP_HI_BUTTON_TITLE_LARGE =
        "{p=18,y=20:PEEP{S:H}}";
StringId MiscStrs::PERCENT_SUPPORT_BUTTON_TITLE_LARGE =
        "{p=18,y=18:% Supp}";
StringId MiscStrs::PERCENT_SUPPORT_HELP_MESSAGE =
        "{p=14,y=17:% Supp{T: = Percent support}}";
StringId MiscStrs::PLATEAU_TIME_BUTTON_TITLE_LARGE =
        "{p=18,y=20:T{S:PL}}";
StringId MiscStrs::PLATEAU_TIME_HELP_MESSAGE =
        "{p=14,y=17:T{S:PL}{T: = Plateau time}}";
StringId MiscStrs::PRESSURE_SENSITIVITY_BUTTON_TITLE_LARGE =
        "{p=18,y=20:P{S:SENS}}";
StringId MiscStrs::PRESSURE_SENSITIVITY_HELP_MESSAGE =
        "{p=14,y=17:P{S:SENS}{T: = Pressure sensitivity}}";
StringId MiscStrs::PRESSURE_SUPPORT_BUTTON_TITLE_LARGE =
        "{p=18,y=20:P{S:SUPP}}";
StringId MiscStrs::PRESSURE_SUPPORT_HELP_MESSAGE =
        "{p=14,y=17:P{S:SUPP}{T: = Support pressure}}";
StringId MiscStrs::RESPIRATORY_RATE_BUTTON_TITLE_LARGE =
        "{p=18,y=20:f}";
StringId MiscStrs::RESPIRATORY_RATE_HELP_MESSAGE =
        "{p=14,y=17:f{T: = Respiratory rate}}";

StringId MiscStrs::TIDAL_VOLUME_BUTTON_TITLE_LARGE =
        "{p=18,y=20:V{S:T}}";
StringId MiscStrs::TIDAL_VOLUME_VCV_HELP_MSG =
        "{p=14,y=17:V{S:T}{T: = Tidal volume}}";
StringId MiscStrs::TIDAL_VOLUME_VCP_HELP_MSG =
        "{p=14,y=17:V{S:T}{T: = Target volume}}";

StringId MiscStrs::VOLUME_SUPPORT_BUTTON_TITLE_LARGE =
        "{p=18,y=20:V{S:T SUPP}}";
StringId MiscStrs::VOLUME_SUPPORT_HELP_MESSAGE =
        "{p=14,y=17:V{S:T SUPP}{T: = Target support volume}}";

StringId MiscStrs::VENT_SETUP_TAB_BUTTON_LABEL =
        "{p=14,y=18,T:VENT{p=10,x=4,y=31,T:SETUP}}";
StringId MiscStrs::VENT_PREVIOUS_SETUP_TAB_BUTTON_LABEL =
        "{p=10,y=11,T:PREVIOUS{p=14,x=2,y=28,T:SETUP}}";
StringId MiscStrs::VENT_CURRENT_SETUP_TAB_BUTTON_LABEL =
        "{p=10,y=11,T:CURRENT{p=14,x=0,y=28,T:SETUP}}";
StringId MiscStrs::VENT_PROPOSED_SETUP_TAB_BUTTON_LABEL =
        "{p=10,y=11,I:PROPOSED{p=14,x=4,y=28,T:SETUP}}";

StringId MiscStrs::ALARM_SETUP_TAB_BUTTON_LABEL =
        "{p=14,y=18,T:ALARM{p=10,x=14,y=31,T:SETUP}}";
StringId MiscStrs::ALARM_CURRENT_SETUP_TAB_BUTTON_LABEL =
        "{p=10,x=2,y=11,T:CURRENT{p=14,x=0,y=28,T:ALARM}}";
StringId MiscStrs::ALARM_PROPOSED_SETUP_TAB_BUTTON_LABEL =
        "{p=10,y=11,I:PROPOSED{p=14,x=2,y=28,T:ALARM}}";

//
// Note that the following are simple strings, not Cheap Text.
//
StringId MiscStrs::MONTH_JANUARY   = "January";
StringId MiscStrs::MONTH_FEBRUARY  = "February";
StringId MiscStrs::MONTH_MARCH     = "March";
StringId MiscStrs::MONTH_APRIL     = "April";
StringId MiscStrs::MONTH_MAY       = "May";
StringId MiscStrs::MONTH_JUNE      = "June";
StringId MiscStrs::MONTH_JULY      = "July";
StringId MiscStrs::MONTH_AUGUST    = "August";
StringId MiscStrs::MONTH_SEPTEMBER = "September";
StringId MiscStrs::MONTH_OCTOBER   = "October";
StringId MiscStrs::MONTH_NOVEMBER  = "November";
StringId MiscStrs::MONTH_DECEMBER  = "December";
StringId MiscStrs::MONTH_INVALID   = "000";
StringId MiscStrs::MONTH_JAN = "Jan";
StringId MiscStrs::MONTH_FEB = "Feb";
StringId MiscStrs::MONTH_MAR = "Mar";
StringId MiscStrs::MONTH_APR = "Apr";
StringId MiscStrs::MONTH_MAY_ABBREV = "May";
StringId MiscStrs::MONTH_JUN = "Jun";
StringId MiscStrs::MONTH_JUL = "Jul";
StringId MiscStrs::MONTH_AUG = "Aug";
StringId MiscStrs::MONTH_SEP = "Sep";
StringId MiscStrs::MONTH_OCT = "Oct";
StringId MiscStrs::MONTH_NOV = "Nov";
StringId MiscStrs::MONTH_DEC = "Dec";

StringId MiscStrs::RESPIRATORY_RATE_SLIDER_MIN_LABEL =
        "{p=18,x=15,y=20:f{S:TOT}}";
StringId MiscStrs::RESPIRATORY_RATE_SLIDER_MAX_LABEL =
        "{p=8,x=15,y=15:1/min}";
StringId MiscStrs::EXH_MINUTE_VOL_SLIDER_MIN_LABEL =
        "{p=18,x=20,y=20:\001{S:E TOT}}";
StringId MiscStrs::EXH_MINUTE_VOL_SLIDER_MAX_LABEL =
        "{p=8,x=14,y=15:L/min}";
StringId MiscStrs::EXH_SPON_TIDAL_VOL_SLIDER_MIN_LABEL =
        "{p=18,x=12,y=20:V{S:TE SPONT}}";
StringId MiscStrs::EXH_SPON_TIDAL_VOL_SLIDER_MAX_LABEL =
        "{p=8,x=27,y=15:mL}";
StringId MiscStrs::INH_SPON_TIDAL_VOL_SLIDER_MIN_LABEL =
        "{p=18,x=12,y=20:V{S:TI SPONT}}";
StringId MiscStrs::INH_SPON_TIDAL_VOL_SLIDER_MAX_LABEL =
        "{p=8,x=27,y=15:mL}";
StringId MiscStrs::INH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL =
        "{p=18,x=12,y=20:V{S:TI MAND}}";
StringId MiscStrs::INH_TIDAL_VOL_SLIDER_MIN_LABEL =        
        "{p=18,x=12,y=20:V{S:TI}}";

StringId MiscStrs::PROXIMAL_INH_SPON_TIDAL_VOL_SLIDER_MIN_LABEL =
        "{p=18,x=12,y=20:V{S:TI SPONT}{S:-{c=12,N:Y}}}";
StringId MiscStrs::PROXIMAL_EXH_SPON_TIDAL_VOL_SLIDER_MIN_LABEL =
        "{p=18,x=12,y=20:V{S:TE SPONT}{S:-{c=12,N:Y}}}";
StringId MiscStrs::PROXIMAL_EXH_MINUTE_VOL_SLIDER_MIN_LABEL =
        "{p=18,x=20,y=20:\001{S:E TOT}{S:-{c=12,N:Y}}}";
StringId MiscStrs::PROXIMAL_INH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL =
        "{p=18,x=12,y=20:V{S:TI MAND}{S:-{c=12,N:Y}}}";
StringId MiscStrs::PROXIMAL_INH_TIDAL_VOL_SLIDER_MIN_LABEL =        
        "{p=18,x=12,y=20:V{S:TI}{S:-{c=12,N:Y}}}";
StringId MiscStrs::PROXIMAL_EXH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL =
        "{p=18,x=12,y=20:V{S:TE MAND}{S:-{c=12,N:Y}}}";


//
// For AlarmLogSubScreen column labels
//
StringId MiscStrs::ALARM_LOG_TIME = "Time";
StringId MiscStrs::ALARM_LOG_EVENT = "Event";
StringId MiscStrs::ALARM_LOG_URGENCY = "Urgency";
StringId MiscStrs::ALARM_LOG_ALARM = "Alarm";
StringId MiscStrs::ALARM_LOG_ANALYSIS = "Analysis";
//
// AlarmLogMsg text.
//
StringId MiscStrs::ALARM_LOG_URGENCY_NORMAL = "Normal";
StringId MiscStrs::ALARM_LOG_URGENCY_LOW = "LOW";
StringId MiscStrs::ALARM_LOG_URGENCY_MEDIUM = "MEDIUM";
StringId MiscStrs::ALARM_LOG_URGENCY_HIGH = "HIGH";
StringId MiscStrs::ALOG_UNKNOWN_ALARM_EVENT = "Unknown";
StringId MiscStrs::ALOG_ALARM_DATA_LOST = "Alarm Data Lost";
StringId MiscStrs::ALOG_ALARM_DATA_CORRUPT = "Alarm data corrupt";
StringId MiscStrs::ALOG_DATE_TIME_BEFORE_CHANGE = "{p=8,x=7,y=13:Date/Time{x=16,Y=11:Before{x=12,Y=11:Change}}}";
StringId MiscStrs::ALOG_DATE_TIME_CHANGED = "{p=8,x=8,y=17:Date/Time{x=10,Y=14:Change}}";
//
// Vent Setup stuff
//
StringId MiscStrs::BREATH_TIMING_UNITS_LABEL =
        "{p=10,y=10,T:sec}";
StringId MiscStrs::PREVIOUS_SETUP_BUTTON_TITLE =
        "{p=8,y=11:PREVIOUS{x=10,Y=11:SETUP}}";
StringId MiscStrs::CONTINUE_BUTTON_TITLE =
        "{p=10,y=16:CONTINUE}";
StringId MiscStrs::FLOW_ACCELERATION_BUTTON_TITLE_SMALL =
        "{p=14,y=14:\005}";
StringId MiscStrs::FLOW_SENSITIVITY_BUTTON_TITLE_SMALL =
        "{p=10,y=12:\001{S:SENS}}";
StringId MiscStrs::HIGH_CIRCUIT_PRESSURE_BUTTON_TITLE_SMALL =
        "{p=10,y=11:\013P{S:PEAK}}";
StringId MiscStrs::LOW_CIRCUIT_PRESSURE_BUTTON_TITLE_SMALL =
        "{p=10,y=11:\012P{S:PEAK}}";
StringId MiscStrs::INSPIRATORY_PRESSURE_BUTTON_TITLE_SMALL =
        "{p=10,y=12:P{S:I}}";
StringId MiscStrs::OXYGEN_PERCENTAGE_BUTTON_TITLE_SMALL =
        "{p=10,y=12:O{S:2}}";
StringId MiscStrs::PEAK_FLOW_BUTTON_TITLE_SMALL =
        "{p=10,y=12:\001{S:MAX}}";
StringId MiscStrs::PEEP_BUTTON_TITLE_SMALL =
        "{p=10,y=12:PEEP}";
StringId MiscStrs::PERCENT_SUPPORT_BUTTON_TITLE_SMALL =
        "{p=10,y=11:% Supp}";
StringId MiscStrs::PLATEAU_TIME_BUTTON_TITLE_SMALL =
        "{p=10,y=12:T{S:PL}}";
StringId MiscStrs::PRESSURE_SENSITIVITY_BUTTON_TITLE_SMALL =
        "{p=10,y=12:P{S:SENS}}";
StringId MiscStrs::PRESSURE_SUPPORT_BUTTON_TITLE_SMALL =
        "{p=10,y=12:P{S:SUPP}}";
StringId MiscStrs::RESPIRATORY_RATE_BUTTON_TITLE_SMALL =
        "{p=10,y=12:f}";
StringId MiscStrs::TIDAL_VOLUME_BUTTON_TITLE_SMALL =
        "{p=10,y=12:V{S:T}}";
StringId MiscStrs::VOLUME_SUPPORT_BUTTON_TITLE_SMALL =
        "{p=10,y=12:V{S:T SUPP}}";

StringId MiscStrs::SOFT_BOUND_OVERRIDE_BUTTON_TITLE =
        "{p=14,y=20,x=2:OK}";
StringId MiscStrs::PEEP_LOW_BUTTON_TITLE_SMALL =
        "{p=10,y=12:PEEP{S:L}}";
StringId MiscStrs::PEEP_HI_BUTTON_TITLE_SMALL =
        "{p=10,y=12:PEEP{S:H}}";

StringId MiscStrs::CURRENT_VENT_SETUP_TITLE =
        "{p=14,y=18:Current Vent Setup}";
StringId MiscStrs::PROPOSED_VENT_SETUP_TITLE =
        "{p=14,y=18,I:Proposed {N:Vent Setup}}";
StringId MiscStrs::NEW_PATIENT_VENT_SETUP_TITLE =
        "{p=14,y=18:New Patient Setup}";
StringId MiscStrs::PREVIOUS_VENT_SETTINGS_TITLE =
        "{p=14,y=18,I:Previous {N:Vent Settings}}";
StringId MiscStrs::PROPOSED_VENT_SETTINGS_TITLE =
        "{p=14,y=18,I:Proposed {N:Vent Settings}}";
StringId MiscStrs::CURRENT_VENT_SETTINGS_TITLE =
        "{p=14,y=18:Current Vent Settings}";
StringId MiscStrs::NEW_PATIENT_VENT_SETTINGS_TITLE =
        "{p=14,y=18:New Patient Settings}";
StringId MiscStrs::VERIFY_SETTINGS_MESSAGE =
        "{p=12,y=16:Match to patient}";

StringId MiscStrs::NEW_IBW_BUTTON_TITLE =
        "{p=10,y=12,T:IBW}";

StringId MiscStrs::EXPIRATORY_SENS_BUTTON_TITLE_SMALL =
        "{p=10,y=12:E{S:SENS}}";
StringId MiscStrs::FLOW_ACCELERATION_BUTTON_TITLE_LARGE =
        "{p=18,y=20:\005}";
StringId MiscStrs::HIGH_INH_SPONT_TIDAL_VOL_BUTTON_TITLE_SMALL =
        "{p=10,y=12:\013V{S:TI SPONT}}";
StringId MiscStrs::HIGH_INH_MAND_TIDAL_VOL_BUTTON_TITLE_SMALL=
        "{p=10,y=12:\013V{S:TI MAND}}";
StringId MiscStrs::HIGH_INH_TIDAL_VOL_BUTTON_TITLE_SMALL=
        "{p=10,y=12:\013V{S:TI}}";
        
StringId MiscStrs::HUMID_VOLUME_BUTTON_TITLE_SMALL =
        "{p=10,y=12,T:Humidifier Volume}";


StringId MiscStrs::DISCONNECTION_SENS_BUTTON_TITLE_SMALL =
        "{p=10,y=12:D{S:SENS}}";

StringId MiscStrs::PROPOSED_DATE_TIME_SETTINGS_TITLE =
        "{p=14,y=18,I:Proposed {N:Time/Date Settings}}";
StringId MiscStrs::CURRENT_MORE_SETTINGS_TITLE =
        "{p=14,y=18:Current Settings}";
StringId MiscStrs::PROPOSED_MORE_SETTINGS_TITLE =
        "{p=14,y=18,I:Proposed {N:Settings}}";
StringId MiscStrs::CHANGE_MAND_TYPE_BUTTON_TITLE =
        "{p=10,x=1,y=14:CHANGE{p=14,x=0,Y=18:VC/PC}}";
StringId MiscStrs::APNEA_INTERVAL_BUTTON_TITLE =
        "{p=10,y=12:T{S:A}}";
StringId MiscStrs::INSPIRATORY_TIME_BUTTON_TITLE_SMALL =
        "{p=10,y=12:T{S:I}}";
StringId MiscStrs::PEEP_HIGH_TIME_BUTTON_TITLE_SMALL =
        "{p=10,y=12:T{S:H}}";
StringId MiscStrs::IE_RATIO_BUTTON_TITLE_SMALL =
        "{p=10,y=12:I:E}";
StringId MiscStrs::PEEP_HIGH_PEEP_LOW_RATIO_BUTTON_TITLE_SMALL =
        "{p=10,y=12:T{S:H}:T{S:L}}";
StringId MiscStrs::EXPIRATORY_TIME_BUTTON_TITLE_SMALL =
        "{p=10,y=12:T{S:E}}";
StringId MiscStrs::PEEP_LOW_TIME_BUTTON_TITLE_SMALL =
        "{p=10,y=12:T{S:L}}";
StringId MiscStrs::CURRENT_APNEA_VENT_TYPE_TITLE =
        "{p=14,y=18:Current Apnea Setup}";
StringId MiscStrs::CURRENT_APNEA_SETTINGS_TITLE =
        "{p=14,y=18:Current Apnea Settings}";
StringId MiscStrs::PROPOSED_APNEA_SETTINGS_TITLE =
        "{p=14,y=18,I:Proposed {N:Apnea Settings}}";
StringId MiscStrs::PROPOSED_APNEA_VENT_TYPE_TITLE =
        "{p=14,y=18,I:Proposed {N:Apnea Setup}}";
StringId MiscStrs::MORE_SETTINGS_BUTTON_TITLE =
        "{p=10,y=23:More Settings}";
StringId MiscStrs::MORE_SETTINGS_BUTTON_L_TITLE =
        "{p=8,x=0,y=10:LC{p=10,x=22,y=23:More Settings}}";

StringId MiscStrs::DATE_TIME_SETTINGS_BUTTON_TITLE =
        "{p=10,y=14:Time/Date{x=8,Y=16:Change}}";
StringId MiscStrs::COMM_SETUP_BUTTON_TITLE =
        "{p=10,y=14:Communication{x=30,Y=16:Setup}}";
StringId MiscStrs::NIF_MANEUVER_BUTTON_TITLE =
        "{p=10,y=23:NIF Maneuver}";
StringId MiscStrs::P100_MANEUVER_BUTTON_TITLE =
        "{p=10,y=23:P{S:0.1} Maneuver}";
StringId MiscStrs::VITAL_CAPACITY_MANEUVER_BUTTON_TITLE =
        "{p=10,y=23:VC Maneuver}";
StringId MiscStrs::RESP_MECH_TITLE =
        "{p=12,y=15:Respiratory Mechanics}";

StringId MiscStrs::OTHER_SCREENS_TITLE =
        "{p=14,y=18:Other Screens}";
StringId MiscStrs::SAME_PATIENT_BUTTON_TITLE =
        "{p=12,y=15:SAME{p=8,x=2,y=26:PATIENT}}";
StringId MiscStrs::NEW_PATIENT_BUTTON_TITLE =
        "{p=12,x=2,y=15:NEW{p=8,x=0,y=26:PATIENT}}";
StringId MiscStrs::SAME_PATIENT_MESSAGE =
        "{p=12,y=25:Ventilate with previous settings (shown above).}";
StringId MiscStrs::SAME_PATIENT_VERIFY_MESSAGE =
        "{p=12,y=25:Note {I:tube I.D.} = %.1fmm, {I:tube type} = %s.}";
StringId MiscStrs::NEW_PATIENT_MESSAGE =
        "{p=12,y=25:Initiate new patient setup.}";
StringId MiscStrs::VENT_START_TITLE =
        "{p=14,y=18:Ventilator Startup}";
StringId MiscStrs::NEW_PATIENT_SETUP_TITLE =
        "{p=14,y=18:New Patient Setup}";
StringId MiscStrs::KILOGRAM_UNIT =
        "{p=14,y=20:kg}";

StringId MiscStrs::IBW_HELP_MESSAGE =
        "{p=14,y=17:IBW{T: = Ideal body weight}}";
StringId MiscStrs::DISCONNECTION_SENSITIVITY_HELP_MESSAGE =
        "{p=14,y=17:D{S:SENS}{T: = Disconnect sensitivity %}}";
StringId MiscStrs::HUMID_VOLUME_HELP_MESSAGE =
        "{p=14,y=17:Empty Humidifier Volume}";
StringId MiscStrs::HUMID_VOLUME_VERIFY_SETTINGS_MESSAGE =
        "{p=10,y=16:Ensure correct humidifier volume}";
StringId MiscStrs::BREATH_TIMING_TITLE =
        "{p=12,y=16:Breath Timing}";
StringId MiscStrs::CANCEL_BREATH_TIMING_BUTTON_TITLE =
        "{p=10,y=18:CANCEL ALL}";
StringId MiscStrs::RESTART_BUTTON_TITLE =
        "{p=10,y=22:RESTART}";

StringId MiscStrs::APNEA_INSPIRATORY_PRESSURE_HELP_MESSAGE =
        "{p=14,y=17:P{S:I}{T: = Apnea inspiratory pressure}}";
StringId MiscStrs::APNEA_INSPIRATORY_TIME_HELP_MESSAGE =
        "{p=14,y=17:T{S:I}{T: = Apnea inspiratory time}}";
StringId MiscStrs::APNEA_INTERVAL_HELP_MESSAGE =
        "{p=14,y=17:T{S:A}{T: = Apnea interval}}";
StringId MiscStrs::APNEA_OXYGEN_PERCENTAGE_HELP_MESSAGE =
        "{p=14,y=17:O{S:2}{T: = Apnea oxygen %}}";
StringId MiscStrs::APNEA_PEAK_FLOW_HELP_MESSAGE =
        "{p=14,y=17:\001{S:MAX}{T: = Apnea peak flow}}";
StringId MiscStrs::APNEA_RESPIRATORY_RATE_HELP_MESSAGE =
        "{p=14,y=17:f{T: = Apnea respiratory rate}}";
StringId MiscStrs::APNEA_TIDAL_VOLUME_HELP_MESSAGE =
        "{p=14,y=17:V{S:T}{T: = Apnea tidal volume}}";

StringId MiscStrs::FLOW_ACCELERATION_HELP_MESSAGE =
        "{p=14,y=17:\005{T: = Rise time %}}";
StringId MiscStrs::HIGH_CIRCUIT_PRESSURE_HELP_MESSAGE =
        "{p=14,y=17:\013P{S:PEAK}{T: = High circuit pressure limit}}";
StringId MiscStrs::LOW_CIRCUIT_PRESSURE_HELP_MESSAGE =
        "{p=14,y=17:\012P{S:PEAK}{T: = Low circuit pressure limit}}";

StringId MiscStrs::IBW_FORMAT_STRING_1 =
        "{p=14,x=472,y=59:IBW = {I:%.*f }%s}";
StringId MiscStrs::KILOGRAM_STRING =
        "{N:kg}";
StringId MiscStrs::IBW_FORMAT_STRING_2 =
        "{p=12,x=564,y=16,%c:%.*f %s}";
StringId MiscStrs::SHORT_SELF_TEST_BUTTON_TITLE =
        "{p=12,y=20:SST}";
StringId MiscStrs::SHORT_SELF_TEST_MESSAGE =
        "{p=12,y=25:Perform Short Self Test (SST) and calibrate circuit.}";
StringId MiscStrs::ALARM_LIMIT_SETTING_BUTTON_TITLE =
        "{p=10,y=11:\014}";
StringId MiscStrs::ALARM_LIMIT_OFF =
        "{p=12,x=4,y=28:OFF}";
StringId MiscStrs::ALARM_LIMIT_OFF_CHANGED =
        "{p=12,x=4,y=28,I:OFF}";
StringId MiscStrs::HIGH_CCT_PRES_HELP_MESSAGE =
        "{p=14,y=17,T:High circuit pressure limit}";
StringId MiscStrs::LOW_CCT_PRES_HELP_MESSAGE =
        "{p=14,y=17,T:Low circuit pressure limit}";
StringId MiscStrs::HIGH_RESP_RATE_HELP_MESSAGE =
        "{p=14,y=17,T:High respiratory rate limit}";
StringId MiscStrs::HIGH_EXH_MIN_VOL_HELP_MESSAGE =
        "{p=14,y=17,T:High exhaled minute volume limit}";
StringId MiscStrs::HIGH_EXH_TIDAL_VOL_HELP_MESSAGE =
        "{p=14,y=17,T:High exhaled tidal volume limit}";
StringId MiscStrs::HIGH_INH_TIDAL_VOL_HELP_MESSAGE =
        "{p=14,y=17,T:High inspired tidal volume limit}";
StringId MiscStrs::HIGH_INH_SPONT_VOL_HELP_MESSAGE =
        "{p=14,y=17,T:High inspired spont tidal volume limit}";
StringId MiscStrs::HIGH_INH_MAND_VOL_HELP_MESSAGE =
        "{p=14,y=17,T:High inspired mand tidal volume limit}";
            
StringId MiscStrs::LOW_EXH_MIN_VOL_HELP_MESSAGE =
        "{p=14,y=17,T:Low exhaled minute volume limit}";
StringId MiscStrs::LOW_EXH_MAND_TIDAL_VOL_HELP_MESSAGE =
        "{p=14,y=17,T:Low exhaled mandatory tidal volume limit}";
StringId MiscStrs::LOW_EXH_SPON_TIDAL_VOL_HELP_MESSAGE =
        "{p=14,y=17,T:Low exhaled spont tidal volume limit}";

StringId MiscStrs::SPONT_MAND_TYPE_MESSAGE =
        "{p=8,y=10,T:Applies to Manual Insp only}";

StringId MiscStrs::VERIFY_HUMID_VOL_MESSAGE =
        "{p=18,x=405,y=255:\004{p=10,X=-120,Y=19:Ensure correct humidifier volume}}";

StringId MiscStrs::BREATH_TIMING_MANUAL_INSP =
        "{p=12,y=11:Manual Insp}";

StringId MiscStrs::MINUTE_VOLUME_SETTING_LABEL =
        "{p=14,y=17:\001{S:E SET}{p=6,Y=-9,x=149,T:L{x=145,Y=2:___}{x=145,Y=8:min}}}";
StringId MiscStrs::MINUTE_VOLUME_SETTING_HELP_MESSAGE =
        "{p=14,y=17:\001{S:E SET}{T: = Set minute volume}}";

StringId MiscStrs::VT_IBW_RATIO_SETTING_LABEL =
        "{p=14,y=17:V{S:T}/IBW{p=6,Y=-9,x=147,T:mL{x=145,Y=2:___}{x=147,Y=8:kg}}}";
StringId MiscStrs::VT_IBW_RATIO_SETTING_VCV_HELP_MSG =
        "{p=14,y=17:V{S:T}/IBW{T: = Tidal volume per weight}}";
StringId MiscStrs::VT_IBW_RATIO_SETTING_VCP_HELP_MSG =
        "{p=14,y=17:V{S:T}/IBW{T: = Target volume per weight}}";

StringId MiscStrs::VSUPP_IBW_RATIO_SETTING_LABEL =
        "{p=14,y=17:V{S:T SUPP}/IBW{p=6,Y=-9,x=147,T:mL{x=145,Y=2:___}{x=147,Y=8:kg}}}";
StringId MiscStrs::VSUPP_IBW_RATIO_SETTING_HELP_MESSAGE =
        "{p=14,y=17:V{S:T SUPP}/IBW{T: = Support volume per weight}}";

StringId MiscStrs::WAVEFORMS_PLOT_SETUP_TITLE =
        "{p=14,y=18:Plot Setup}";
StringId MiscStrs::PLOT_SETUP_BUTTON_TITLE =
        "{p=8,y=10,x=6:PLOT{y=20,x=1: SETUP}}";
StringId MiscStrs::FREEZE_BUTTON_FREEZE_TITLE =
        "{p=8,y=15:FREEZE}";
StringId MiscStrs::PRINT_BUTTON_TITLE =
        "{p=8,y=15:PRINT}";
StringId MiscStrs::HORIZONTAL_SCALE_BUTTON_TITLE =
        "{p=8,y=13:<{X=-4,Y=-5:____}{X=-24,Y=-1:____}{X=-3,Y=6:>}}";
StringId MiscStrs::TIME_SCALE_HELP_MSG =
        "{p=14,y=17,T:Time scale}";
StringId MiscStrs::PRESSURE_SCALE_HELP_MSG =
        "{p=14,y=17,T:Pressure scale}";
StringId MiscStrs::VERTICAL_SCALE_BUTTON_TITLE =
        "{p=18,y=21:\004{X=-13,Y=10:\003}}";
StringId MiscStrs::VOLUME_SCALE_HELP_MSG =
        "{p=14,y=17,T:Volume scale}";
StringId MiscStrs::FLOW_SCALE_HELP_MSG =
        "{p=14,y=17,T:Flow scale}";
StringId MiscStrs::BASELINE_BUTTON_TITLE =
        "{p=10,x=24,y=7,T:___{p=8,X=-11,Y=3:|}{p=6,X=-10,Y=-6,N:_{X=-9,Y=3:<}}{p=6,N,X=7,Y=-3:_{X=-4,Y=3:>}}{p=8,T,x=57,y=30:H{S:2}O{X=-17,Y=-9:cm}}}";
StringId MiscStrs::BASELINE_BUTTON_HPA_TITLE =
        "{p=10,x=24,y=7,T:___{p=8,X=-11,Y=3:|}{p=6,X=-10,Y=-6,N:_{X=-9,Y=3:<}}{p=6,N,X=7,Y=-3:_{X=-4,Y=3:>}}{p=8,T,x=57,y=30:hPa}}";
StringId MiscStrs::BASELINE_HELP_MSG =
        "{p=10,y=10,T:___{p=8,X=-11,Y=3:|}{p=6,X=-10,Y=-6,N:_{X=-9,Y=3:<}}{p=6,N,X=7,Y=-3:_{X=-4,Y=3:>}}{p=14,y=17,T: = P-V baseline pressure}}";
StringId MiscStrs::PLOT1_SETTING_BUTTON_TITLE =
        "{p=10,y=13,T:Plot 1}";
StringId MiscStrs::PLOT1_SETTING_HELP_MSG =
        "{p=14,y=17,T:Plot 1}";
StringId MiscStrs::PLOT2_SETTING_BUTTON_TITLE =
        "{p=10,y=13,T:Plot 2}";
StringId MiscStrs::PLOT2_SETTING_HELP_MSG =
        "{p=14,y=17,T:Plot 2}";
StringId MiscStrs::SHADOW_TRACE_ENABLE_TITLE =
        "{p=10,y=13,T:Shadow Trace}";
StringId MiscStrs::SHADOW_TRACE_ENABLE_MSG =
        "{p=14,y=17,T:Enable/disable shadow display}";

StringId MiscStrs::FREEZING_TITLE =
        "{p=8,x=5,y=19:FREEZING}";
StringId MiscStrs::PRINTING_TITLE =
        "{p=8,x=5,y=19:PRINTING}";

		// these plot setup values ignore the set style, and always display as "normal"
		// (bold)...
StringId MiscStrs::PRESSURE_VS_TIME_VALUE =
        "{p=%d,y=%d,%c,N:Pressure - Time}";
//        "{p=%d,y=%d,%c,N:P{S:CIRC} - time}";
StringId MiscStrs::FLOW_VS_TIME_VALUE =
        "{p=%d,y=%d,%c,N:Flow - Time}";
//        "{p=%d,y=%d,%c,N:\001 - time}";
StringId MiscStrs::VOLUME_VS_TIME_VALUE =
        "{p=%d,y=%d,%c,N:Volume - Time}";
 //       "{p=%d,y=%d,%c,N:V{S:T} - time}";
StringId MiscStrs::PRESSURE_VS_VOLUME_VALUE =
        "{p=%d,y=%d,%c,N:Pressure - Volume}";
 //       "{p=%d,y=%d,%c,N:P{S:CIRC} - V{S:T}}";
StringId MiscStrs::FLOW_VS_VOLUME_VALUE =
        "{p=%d,y=%d,%c,N:Flow - Volume}";
 //       "{p=%d,y=%d,%c,N:\001 - V{S:T}}";
StringId MiscStrs::WOB_GRAPHIC_VALUE =
        "{p=%d,y=%d,%c,N:Work-of-Breathing}";
StringId MiscStrs::NO_PLOT2_VALUE =
        "{p=%d,y=%d,%c,N:NONE}";

StringId MiscStrs::DISABLE_SHADOW_TRACE_VALUE =
        "{p=%d,y=%d,%c:Disabled}";
StringId MiscStrs::ENABLE_SHADOW_TRACE_VALUE =
        "{p=%d,y=%d,%c:Enabled}";

StringId MiscStrs::PRESSURE_PLOT_UNITS =
        "{p=10,x=7,y=10:P{S:CIRC}{p=6,Y=16,x=12,T:cmH{S:2}O}}";
StringId MiscStrs::PRESSURE_PLOT_HPA_UNITS =
        "{p=10,x=7,y=10:P{S:CIRC}{p=6,Y=16,x=20,T:hPa}}";
StringId MiscStrs::VOLUME_PLOT_UNITS =
        "{p=10,x=15,y=12:V{S:T}{p=6,Y=16,x=21,T:mL}}";
StringId MiscStrs::TIME_PLOT_UNITS =
        "{p=8,y=8:s}";
StringId MiscStrs::BIG_FLOW_PLOT_UNITS =
        "{p=8,x=10,y=10:INSP{p=10,x=18,Y=92:\001{p=6,Y=16,x=20,T:L{x=15,Y=2:___}{x=16,Y=8:min}}}{x=12,Y=89:EXP}}";
StringId MiscStrs::SMALL_FLOW_PLOT_UNITS =
        "{p=8,x=10,y=10:INSP{p=10,x=18,Y=42:\001{p=6,Y=16,x=20,T:L{x=15,Y=2:___}{x=16,Y=8:min}}}{x=12,Y=34:EXP}}";

StringId MiscStrs::PROXIMAL_PRESSURE_PLOT_UNITS =
        "{p=10,x=7,y=10:P{S:{c=12,N:Y}}{p=6,Y=16,x=12,T:cmH{S:2}O}}";
StringId MiscStrs::PROXIMAL_PRESSURE_PLOT_HPA_UNITS =
        "{p=10,x=7,y=10:P{S:{c=12,N:Y}}{p=6,Y=16,x=20,T:hPa}}";
StringId MiscStrs::PROXIMAL_VOLUME_PLOT_UNITS =
        "{p=10,x=5,y=12:V{S:T-{c=12,N:Y}}{p=6,Y=16,x=21,T:mL}}";
StringId MiscStrs::PROXIMAL_BIG_FLOW_PLOT_UNITS =
        "{p=8,x=10,y=10:INSP{p=10,x=16,Y=92:\001{S:{c=12,N:Y}}{p=6,Y=16,x=20,T:L{x=15,Y=2:___}{x=16,Y=8:min}}}{x=12,Y=89:EXP}}";
StringId MiscStrs::PROXIMAL_SMALL_FLOW_PLOT_UNITS =
        "{p=8,x=10,y=10:INSP{p=10,x=16,Y=42:\001{S:{c=12,N:Y}}{p=6,Y=16,x=20,T:L{x=15,Y=2:___}{x=16,Y=8:min}}}{x=12,Y=34:EXP}}";

StringId MiscStrs::FREEZE_BUTTON_UNFREEZE_TITLE =
        "{p=8,y=15:UNFREEZE}";
StringId MiscStrs::PRINT_BUTTON_CANCEL_PRINT_TITLE =
        "{p=8,y=15:CANCEL}";
StringId MiscStrs::CARINAL_PRESSURE_LABEL =
        "{p=10,x=3,y=14:P{S:CARI}}";
StringId MiscStrs::CARINAL_PRESSURE_MSG =
        "{p=14,y=17:P{S:CARI}{T: = Estimated carinal pressure}}";
StringId MiscStrs::LUNG_PRESSURE_LABEL =
        "{p=10,x=5,y=14:P{S:LUNG}}";
StringId MiscStrs::LUNG_PRESSURE_MSG =
        "{p=14,y=17:P{S:LUNG}{T: = Estimated lung pressure}}";

// Text that appears on buttons in the Date/Time Change Settings subscreen
StringId MiscStrs::MINUTE_BUTTON_TITLE =
        "{p=10,y=12,T:Minute}";
StringId MiscStrs::MINUTE_HELP_MESSAGE =
        "{p=14,y=17,T:Minute}";
StringId MiscStrs::HOUR_BUTTON_TITLE =
        "{p=10,y=12,T:Hour}";
StringId MiscStrs::HOUR_HELP_MESSAGE =
        "{p=14,y=17,T:Hour}";
StringId MiscStrs::DAY_BUTTON_TITLE =
        "{p=10,y=12,T:Day}";
StringId MiscStrs::DAY_HELP_MESSAGE =
        "{p=14,y=17,T:Day}";
StringId MiscStrs::YEAR_BUTTON_TITLE =
        "{p=10,y=12,T:Year}";
StringId MiscStrs::YEAR_HELP_MESSAGE =
        "{p=14,y=17,T:Year}";

		//-----------------------------------------------------------------
		// Month Setting strings...
		//-----------------------------------------------------------------
StringId MiscStrs::MONTH_BUTTON_TITLE =
        "{p=10,y=12,T:Month}";
StringId MiscStrs::MONTH_HELP_MESSAGE =
        "{p=14,y=17,T:Month}";
	// setting values...
StringId MiscStrs::JANUARY_VALUE = 
        "{p=%d,y=%d,%c:JAN}";
StringId MiscStrs::FEBRUARY_VALUE = 
        "{p=%d,y=%d,%c:FEB}";
StringId MiscStrs::MARCH_VALUE = 
        "{p=%d,y=%d,%c:MAR}";
StringId MiscStrs::APRIL_VALUE = 
        "{p=%d,y=%d,%c:APR}";
StringId MiscStrs::MAY_VALUE = 
        "{p=%d,y=%d,%c:MAY}";
StringId MiscStrs::JUNE_VALUE = 
        "{p=%d,y=%d,%c:JUN}";
StringId MiscStrs::JULY_VALUE = 
        "{p=%d,y=%d,%c:JUL}";
StringId MiscStrs::AUGUST_VALUE = 
        "{p=%d,y=%d,%c:AUG}";
StringId MiscStrs::SEPTEMBER_VALUE = 
        "{p=%d,y=%d,%c:SEP}";
StringId MiscStrs::OCTOBER_VALUE = 
        "{p=%d,y=%d,%c:OCT}";
StringId MiscStrs::NOVEMBER_VALUE = 
        "{p=%d,y=%d,%c:NOV}";
StringId MiscStrs::DECEMBER_VALUE = 
        "{p=%d,y=%d,%c:DEC}";

// For Service mode's diagnostic log
StringId MiscStrs::DIAG_CODE_LOG_SUBSCREEN_TITLE =
        "{p=14,y=18:Diagnostic Code Log}";
StringId MiscStrs::DIAG_UNDEFINED_ERROR = "Undefined error";
StringId MiscStrs::DIAG_UNDEFINED_TEST_CASE = "Undefined test case";
StringId MiscStrs::SV_NO_AC_POWER_CONNECTED = "AC power not connected";
StringId MiscStrs::SV_GAS_NOT_CONNECTED = "Gas not connected";
StringId MiscStrs::SV_OCCLUDED_INSP_LIMB = "Occluded inspiratory limb";
StringId MiscStrs::SV_OCC_EXH_COMPARTMENT = "Occluded expiratory compartment";
StringId MiscStrs::SV_OCC_EXH_FILTER = "Occluded expiratory filter";
StringId MiscStrs::SV_OCCLUDED_EXP_LIMB = "Occluded expiratory limb";
StringId MiscStrs::SV_INSP_LIMB_RESISTANCE_LOW = "Insp limb resistance low";
StringId MiscStrs::SV_EXP_LIMB_RESISTANCE_LOW = "Exp limb resistance low";
StringId MiscStrs::SV_CIRCUIT_NOT_DISCONNECTED = "Patient circuit not disconnected";
StringId MiscStrs::SV_CIRCUIT_NOT_RECONNECTED = "Patient circuit not reconnected";
StringId MiscStrs::SV_TEST_CIRCUIT_NOT_CONNECTED = "Test circuit not connected";
StringId MiscStrs::SV_AIR_PSOL_LEAK = "Air PSOL leak";
StringId MiscStrs::SV_O2_PSOL_LEAK = "O2 PSOL leak";
StringId MiscStrs::SV_NO_FLOW = "Unable to establish flow";
StringId MiscStrs::SV_UNABLE_REACH_MIN_PEAK_FLOW = "Unable to reach min peak flow";
StringId MiscStrs::SV_WYE_NOT_BLOCKED = "Wye not blocked";
StringId MiscStrs::SV_LOW_EXPIRATORY_FILTER_DELTA_P = "Low expiratory filter \177P";
StringId MiscStrs::SV_SAFETY_V_PRESSURE_RELIEF_FAILURE = "SV pressure relief failed";
StringId MiscStrs::SV_BAD_O2_FLOW_SENSOR = "O2 flow sensor cross check failed";
StringId MiscStrs::SV_BAD_O2_PSOL = "O2 PSOL loopback current OOR";
StringId MiscStrs::SV_O2_PSOL_CURRENT_OUT_OF_RANGE = "O2 PSOL current out of range";
StringId MiscStrs::SV_BAD_AIR_FLOW_SENSOR = "Air flow sensor cross check failed";
StringId MiscStrs::SV_UNABLE_TO_ESTABLISH_AIR_FLOW = "Unable to establish air flow";
StringId MiscStrs::SV_O2_PRESSURE_NOT_DETECTED = "O2 pressure not detected";
StringId MiscStrs::SV_AIR_PRESSURE_NOT_DETECTED = "Wall air pressure not detected";
StringId MiscStrs::SV_O2_PRESSURE_DETECTED = "O2 pressure detected";
StringId MiscStrs::SV_AIR_PRESSURE_DETECTED = "Wall air pressure detected";
StringId MiscStrs::SV_O2_ZERO_FLOW_CHECK_FAILED = "O2 zero flow check failed";
StringId MiscStrs::SV_AIR_ZERO_FLOW_CHECK_FAILED = "Air zero flow check failed";
StringId MiscStrs::SV_EXP_ZERO_FLOW_CHECK_FAILED = "Exp zero flow check failed";
StringId MiscStrs::SV_UNABLE_TO_ESTABLISH_EXH_FLOW = "Unable to establish exp flow";
StringId MiscStrs::SV_EXP_VALVE_NOT_CALIBRATED = "Exp valve not calibrated";
StringId MiscStrs::SV_UNABLE_TO_ESTABLISH_O2_FLOW = "Unable to establish O2 flow";
StringId MiscStrs::SV_COMPRESSOR_PRESSURE_DETECTED= "Compressor pressure detected";
StringId MiscStrs::SV_COMPRESSOR_PRESSURE_NOT_DETECTED= "Compressor pressure not detected";
StringId MiscStrs::SV_UNABLE_TO_ESTABLISH_PRESSURE = "Unable to establish pressure";
StringId MiscStrs::SV_BAD_AIR_PSOL = "Air PSOL loopback current OOR";
StringId MiscStrs::SV_AIR_PSOL_CURRENT_OUT_OF_RANGE = "Air PSOL current out of range";
StringId MiscStrs::SV_BAD_INSP_AUTOZERO_SOLE_INSP_PRESS_SENSOR =
        "Bad insp autozero solenoid";
StringId MiscStrs::SV_BAD_EXH_AUTOZERO_SOLE_EXH_PRESS_SENSOR =
        "Bad exp autozero sol";
StringId MiscStrs::SV_CROSS_CHECK_FAILED = "Cross-check failed";
StringId MiscStrs::SV_SAFETY_VALVE_OCCLUDED = "Safety valve occluded";
StringId MiscStrs::SV_BAD_SAFETY_VALVE_LOOPBAK_CURR_DRIVER =
        "Bad safety valve driver or loopback";
StringId MiscStrs::SV_INSP_AUTOZERO_OUT_OF_RANGE =
        "Inspiratory autozero out of range";
StringId MiscStrs::SV_EXH_AUTOZERO_OUT_OF_RANGE =
        "Expiratory autozero out of range";
StringId MiscStrs::SV_FAILED_TO_REACH_TEST_PRESSURE =
        "Failed to reach test pressure";
StringId MiscStrs::SV_BAD_INSP_CHECK_VALVE =
        "Insp check valve test failed";
StringId MiscStrs::SV_BAD_EXH_VALVE = "Bad expiratory valve";
StringId MiscStrs::SV_BAD_EXH_VALVE_CURR_LOOPBK =
        "Exp valve loopback current OOR";
StringId MiscStrs::SV_BAD_EXH_VALVE_SEAL = "Seal test failed";
StringId MiscStrs::SV_EXH_VALVE_TEMP_OOR = "Exp valve temp OOR";
StringId MiscStrs::SV_BAD_CALIBR_EXH_VALVE =
        "Bad calibration";
StringId MiscStrs::SV_UNABLE_TO_PERFORM_LEAK_TEST = "Unable to perform comp. leak test";
StringId MiscStrs::SV_COMP_RUN_MODE_TIME_OOR = "Run mode time OOR";
StringId MiscStrs::SV_COMP_DISABLED_MODE_TIME_OOR = "Disabled mode time OOR";
StringId MiscStrs::SV_COMP_UNABLE_TO_TEST_STANDBY_MODE = "Unable to test standby mode";
StringId MiscStrs::SV_COMP_STANDBY_MODE_TIME_OOR = "Standby mode time OOR";
StringId MiscStrs::SV_O2_NOT_DISCONNECTED = "O2 not disconnected";
StringId MiscStrs::SV_UNABLE_TO_PERFORM_LOAD_TEST =
        "Unable to perform comp. load test";
StringId MiscStrs::SV_COMP_LOAD_TEST_FAILED = "Compressor load test failed";
StringId MiscStrs::SV_COMP_LEAK_DETECTED = "Compressor leak detected";
StringId MiscStrs::SV_LEAK_FAILURE = "Excessive leak";
StringId MiscStrs::SV_COMPL_CALIB_FAILURE = "Excessive compliance";
StringId MiscStrs::SV_COMPL_LOW = "Compliance low";
StringId MiscStrs::SV_BAD_UNABLE_PRESSURIZE_CIRCUIT = "Unable to pressurize circuit";
StringId MiscStrs::SV_HIGH_FLOW_TIME_TOO_LOW = "Compliance calculation failure";
StringId MiscStrs::SV_CHAN_VAL_OOR = "Bad channel value";
StringId MiscStrs::SV_BAD_GUI_SER_PORT = "Bad GUI serial port";


StringId MiscStrs::SV_GUI_TOUCH_FATAL_ERROR = "GUI touch: Error";
StringId MiscStrs::SV_GUI_TOUCH_BLOCKED_BEAM = "GUI touch: Blocked beam";
StringId MiscStrs::SV_GUI_TOUCH_TOO_HIGH_AMBIENT = "Environmental lights too bright.";
StringId MiscStrs::SV_GUI_TOUCH_SCREEN_NOT_RESPONDING = "GUI touch: No response to command.";
StringId MiscStrs::SV_KEY_ACCEPT_FAILURE = "Accept key fails.";
StringId MiscStrs::SV_KEY_CLEAR_FAILURE = "Clear key fails.";
StringId MiscStrs::SV_KEY_EXP_PAUSE_FAILURE = "Exp. Pause key fails.";
StringId MiscStrs::SV_KEY_INSP_PAUSE_FAILURE = "Insp. Pause key fails.";
StringId MiscStrs::SV_KEY_MAN_INSP_FAILURE = "Man Insp fails.";
StringId MiscStrs::SV_KEY_HUNDRED_PERCENT_O2_FAILURE = "Increase O{S:2} (100% O{S:2}) key fails.";
StringId MiscStrs::SV_KEY_INFO_FAILURE = "Info key fails.";
StringId MiscStrs::SV_KEY_ALARM_RESET_FAILURE = "Alarm Reset key fails.";
StringId MiscStrs::SV_KEY_ALARM_SILENCE_FAILURE = "Alarm Silence key fails.";
StringId MiscStrs::SV_KEY_ALARM_VOLUME_FAILURE = "Alarm Volume key fails.";
StringId MiscStrs::SV_KEY_SCREEN_BRIGHTNESS_FAILURE = "Screen brightness fails.";
StringId MiscStrs::SV_KEY_SCREEN_CONTRAST_FAILURE = "Screen contrast key fails.";
StringId MiscStrs::SV_KEY_SCREEN_LOCK_FAILURE = "Screen lock key fails.";
StringId MiscStrs::SV_BAD_KNOB = "Bad knob";
StringId MiscStrs::SV_BAD_GUI_HIGH_ALARM_LED = "GUI High Alarm LED fails.";
StringId MiscStrs::SV_BAD_GUI_MEDIUM_ALARM_LED = "GUI Medium Alarm LED fails.";
StringId MiscStrs::SV_BAD_GUI_LOW_ALARM_LED = "GUI Low Alarm LED fails.";
StringId MiscStrs::SV_BAD_GUI_NORMAL_LED = "GUI Normal LED fails.";
StringId MiscStrs::SV_BAD_GUI_BATT_BACKUP_LED = "GUI Batt Backup LED fails.";
StringId MiscStrs::SV_BAD_GUI_ON_BATT_PWR_LED = "GUI On Batt Pwr LED fails.";
StringId MiscStrs::SV_BAD_GUI_COMP_RDY_LED = "GUI Compressor Ready LED fails.";
StringId MiscStrs::SV_BAD_GUI_COMP_OP_LED = "GUI Compressor Operating LED fails.";
StringId MiscStrs::SV_BAD_GUI_100_PCT_O2_LED = "GUI Increase O{S:2} (100% O{S:2}) LED fails.";
StringId MiscStrs::SV_BAD_GUI_ALARM_SILENCE_LED = "GUI Alarm Silence LED fails.";
StringId MiscStrs::SV_BAD_GUI_SCREEN_LOCK_LED = "GUI Screen Lock LED fails.";
StringId MiscStrs::SV_BAD_BD_STATUS_LEDS = "Bad BD status LEDs";
StringId MiscStrs::SV_BAD_VENT_INOP_LED = "Bad Vent inop LED";
StringId MiscStrs::SV_BAD_SVO_LED = "Bad SVO LED";
StringId MiscStrs::SV_BAD_LOSS_OF_GUI_LED = "Bad Loss of GUI LED";
StringId MiscStrs::SV_SAAS_TEST_FAILED = "SAAS test failed";
StringId MiscStrs::SV_NURSE_CALL_STUCK_ON = "Nurse call stuck on";
StringId MiscStrs::SV_NURSE_CALL_STUCK_OFF = "Nurse call stuck off";
StringId MiscStrs::SV_LOW_EXP_DELTA_P = "Low exp \177P";
StringId MiscStrs::SV_BAD_BD_AUDIO = "Bad BD audio";
StringId MiscStrs::SV_BAD_POWER_FAIL_CAP = "Bad power fail cap";
StringId MiscStrs::SV_BAD_ALARM_CABLE = "Bad alarm cable";
StringId MiscStrs::SV_BAD_EXH_HEATER = "Bad exp heater";
StringId MiscStrs::SV_BATTERY_NOT_CHARGED = "Battery not charged";
StringId MiscStrs::SV_BATTERY_NOT_DISCHARGING = "Battery not discharging";
StringId MiscStrs::SV_BAD_BPS = "Bad Backup Power Supply";
StringId MiscStrs::SV_BATTERY_NOT_CHARGING = "Battery not charging";
StringId MiscStrs::SV_RESTART_BD = "BD Restart failed";
StringId MiscStrs::SV_RESTART_GUI = "GUI Restart failed";
StringId MiscStrs::SV_CF_INTERFACE_ERROR = "Compact Flash interface error";
StringId MiscStrs::SV_CF_FLASH_ERROR = "Compact Flash Device error";
StringId MiscStrs::SV_NETWORKAPP_EPERM = "Socket error EPERM";
StringId MiscStrs::SV_NETWORKAPP_ENOENT = "Socket error ENOENT";
StringId MiscStrs::SV_NETWORKAPP_ESRCH = "Socket error ESRCH";
StringId MiscStrs::SV_NETWORKAPP_EINTR = "Socket error EINTR";
StringId MiscStrs::SV_NETWORKAPP_EIO = "Socket error EIO";
StringId MiscStrs::SV_NETWORKAPP_ENXIO = "Socket error ENXIO";
StringId MiscStrs::SV_NETWORKAPP_E2BIG = "Socket error E2BIG";
StringId MiscStrs::SV_NETWORKAPP_ENOEXEC = "Socket error ENOEXEC";
StringId MiscStrs::SV_NETWORKAPP_EBADF = "Socket error EBADF";
StringId MiscStrs::SV_NETWORKAPP_ECHILD = "Socket error ECHILD";
StringId MiscStrs::SV_NETWORKAPP_EAGAIN = "Socket error EAGAIN";
StringId MiscStrs::SV_NETWORKAPP_ENOMEM = "Socket error ENOMEM";
StringId MiscStrs::SV_NETWORKAPP_EACCES = "Socket error EACCES";
StringId MiscStrs::SV_NETWORKAPP_EFAULT = "Socket error EFAULT";
StringId MiscStrs::SV_NETWORKAPP_ENOTBLK = "Socket error ENOTBLK";
StringId MiscStrs::SV_NETWORKAPP_EBUSY = "Socket error EBUSY";
StringId MiscStrs::SV_NETWORKAPP_EEXIST = "Socket error EEXIST";
StringId MiscStrs::SV_NETWORKAPP_EXDEV = "Socket error EXDEV";
StringId MiscStrs::SV_NETWORKAPP_ENODEV = "Socket error ENODEV";
StringId MiscStrs::SV_NETWORKAPP_ENOTDIR = "Socket error ENOTDIR ";
StringId MiscStrs::SV_NETWORKAPP_EISDIR = "Socket error EISDIR";
StringId MiscStrs::SV_NETWORKAPP_EINVAL = "Socket error EINVAL";
StringId MiscStrs::SV_NETWORKAPP_ENFILE = "Socket error ENFILE";
StringId MiscStrs::SV_NETWORKAPP_EMFILE = "Socket error EMFILE";
StringId MiscStrs::SV_NETWORKAPP_ENOTTY = "Socket error ENOTTY";
StringId MiscStrs::SV_NETWORKAPP_ETXTBSY = "Socket error ETXTBSY";
StringId MiscStrs::SV_NETWORKAPP_EFBIG = "Socket error EFBIG";
StringId MiscStrs::SV_NETWORKAPP_ENOSPC = "Socket error ENOSPC";
StringId MiscStrs::SV_NETWORKAPP_ESPIPE = "Socket error ESPIPE";
StringId MiscStrs::SV_NETWORKAPP_EROFS = "Socket error EROFS";
StringId MiscStrs::SV_NETWORKAPP_EMLINK = "Socket error EMLINK";
StringId MiscStrs::SV_NETWORKAPP_EPIPE = "Socket error EPIPE";
StringId MiscStrs::SV_NETWORKAPP_EDOM = "Socket error EDOM";
StringId MiscStrs::SV_NETWORKAPP_ERANGE = "Socket error ERANGE";
StringId MiscStrs::SV_NETWORKAPP_EWOULDBLOCK = "Socket error EWOULDBLOCK";
StringId MiscStrs::SV_NETWORKAPP_EINPROGRESS = "Socket error EINPROGRESS";
StringId MiscStrs::SV_NETWORKAPP_EALREADY = "Socket error EALREADY";
StringId MiscStrs::SV_NETWORKAPP_ENOTSOCK = "Socket error ENOTSOCK";
StringId MiscStrs::SV_NETWORKAPP_EDESTADDRREQ = "Socket error EDESTADDRREQ";
StringId MiscStrs::SV_NETWORKAPP_EMSGSIZE = "Socket error EMSGSIZE";
StringId MiscStrs::SV_NETWORKAPP_EPROTOTYPE = "Socket error EPROTOTYPE";
StringId MiscStrs::SV_NETWORKAPP_ENOPROTOOPT = "Socket error ENOPROTOOPT";
StringId MiscStrs::SV_NETWORKAPP_EPROTONOSUPPORT = "Socket error EPROTONOSUPPORT";
StringId MiscStrs::SV_NETWORKAPP_ESOCKTNOSUPPORT = "Socket error ESOCKTNOSUPPORT";
StringId MiscStrs::SV_NETWORKAPP_EOPNOTSUPP =  "Socket error EOPNOTSUPP";
StringId MiscStrs::SV_NETWORKAPP_EPFNOSUPPORT = "Socket error EPFNOSUPPORT";
StringId MiscStrs::SV_NETWORKAPP_EAFNOSUPPORT = "Socket error EAFNOSUPPORT";
StringId MiscStrs::SV_NETWORKAPP_EADDRINUSE = "Socket error EADDRINUSE";
StringId MiscStrs::SV_NETWORKAPP_EADDRNOTAVAIL = "Socket error EADDRNOTAVAIL";
StringId MiscStrs::SV_NETWORKAPP_ENETDOWN = "Socket error ENETDOWN";
StringId MiscStrs::SV_NETWORKAPP_ENETUNREACH = "Socket error ENETUNREACH";
StringId MiscStrs::SV_NETWORKAPP_ENETRESET = "Socket error ENETRESET";
StringId MiscStrs::SV_NETWORKAPP_ECONNABORTED = "Socket error ECONNABORTED";
StringId MiscStrs::SV_NETWORKAPP_ECONNRESET = "Socket error ECONNRESET";
StringId MiscStrs::SV_NETWORKAPP_ENOBUFS = "Socket error ENOBUFS";
StringId MiscStrs::SV_NETWORKAPP_EISCONN = "Socket error EISCONN";
StringId MiscStrs::SV_NETWORKAPP_ENOTCONN = "Socket error ENOTCONN";
StringId MiscStrs::SV_NETWORKAPP_ESHUTDOWN = "Socket error ESHUTDOWN";
StringId MiscStrs::SV_NETWORKAPP_ETOOMANYREFS = "Socket error ETOOMANYREFS";
StringId MiscStrs::SV_NETWORKAPP_ETIMEDOUT = "Socket error ETIMEDOUT";
StringId MiscStrs::SV_NETWORKAPP_ECONNREFUSED = "Socket error ECONNREFUSED";
StringId MiscStrs::SV_NETWORKAPP_ELOOP = "Socket error ELOOP";
StringId MiscStrs::SV_NETWORKAPP_ENAMETOOLONG = "Socket error ENAMETOOLONG";
StringId MiscStrs::SV_NETWORKAPP_EHOSTDOWN = "Socket error EHOSTDOWN";
StringId MiscStrs::SV_NETWORKAPP_EHOSTUNREACH = "Socket error EHOSTUNREACH";
StringId MiscStrs::SV_NETWORKAPP_INVALID_COMM_DATA = "Invalid comm. data";
StringId MiscStrs::SV_NETWORKAPP_DATA_TOO_LARGE = "Data too large";
StringId MiscStrs::SV_NETWORKAPP_INVALID_COMM_MSGID = "Invalid comm. msg id";
StringId MiscStrs::SV_NETWORKAPP_NO_ACK_MESSAGE = "No ack msg";
StringId MiscStrs::SV_NETWORKAPP_MAX_ACTIVE_MSG_DELAY =
        "Maximum msg delay exceeded";
StringId MiscStrs::SV_NETWORKAPP_BLOCKIO_NO_ACCEPT_CALLBACK =
        "Block I/O no accept callback";
StringId MiscStrs::SV_NETWORKAPP_BLOCKIO_SOCKET_ALREADY_OPENED =
        "Block I/O socket already opened";
StringId MiscStrs::SV_NETWORKAPP_WRONG_INPUT_COUNT      = "Wrong input count" ;
StringId MiscStrs::SV_NETWORKAPP_MSG_TRANSMISSION_FAILED = "Msg trans failed";
StringId MiscStrs::SV_NETWORKAPP_OUT_OF_ACKBUFFER = "Out of ack buffer." ;
StringId MiscStrs::SV_NETWORKAPP_RECV_TIMEOUT = "Network message received timeout." ;
StringId MiscStrs::SV_NETWORKAPP_CONNECT_TIMEOUT = "Network connect timeout." ;
StringId MiscStrs::SV_STWARE_LOG_ERROR = "Stackware log.";
StringId MiscStrs::SV_STWARE_LOG_DIAGNOSTIC = "Stackware log diagnostic.";
StringId MiscStrs::SV_STWARE_IN_CONTROL_UNLINK_INIFADDR_IFP = "Stackware in control unlink inifaddr ifp.";
StringId MiscStrs::SV_STWARE_IN_CONTROL_UNLINK_INIFADDR_LIST = "Stackware in control unlink inifaddr list.";
StringId MiscStrs::SV_STWARE_IN_CKSUM_C_OUT_OF_DATA = "Stackware in cksum c out of data." ;
StringId MiscStrs::SV_STWARE_XSRN_ADDROUTE_MASK = "Stackware xsrn addroute mask." ;
StringId MiscStrs::SV_STWARE_XSRN_DELETE_INCONSISTENT = "Stackware xsrn delete inconsistent." ;
StringId MiscStrs::SV_STWARE_XSRN_DELETE_NO_ANNOTATION = "Stackware xsrn delete no annotation err." ;
StringId MiscStrs::SV_STWARE_XSRN_DELETE_NO_US = "Stackware xsrn delete no us err." ;
StringId MiscStrs::SV_STWARE_XSRN_DELETE_ORPHANED_MASK = "Stackware xsrn delete orphaned mask." ;
StringId MiscStrs::SV_STWARE_UDP_USRREQ_UNEXPECTED_CONTROL_DATA = "Stackware udp usrreq unexpected control data." ;
StringId MiscStrs::SV_STWARE_XS_STARTUP_NO_UNIT = "Stackware xs startup no unit." ;
StringId MiscStrs::SV_STWARE_NO_MEMORY_FOR_IFADDR = "Stackware no memory for ifaddr." ;
StringId MiscStrs::SV_STWARE_LOOUTPUT_CANNOT_HANDLE_IF = "Stackware looutput cannot handle if." ;
StringId MiscStrs::SV_STWARE_XSRN_INSERT_COMING_OUT = "Stackware xsrn insert coming out." ;
StringId MiscStrs::SV_STWARE_MCOPYDATA_NEGATIVE_OFFSET = "Stackware mcopydata negative offset." ;
StringId MiscStrs::SV_STWARE_MCOPYDATA_NULL_DATA_PTR = "Stackware mcopydata null data ptr." ;
StringId MiscStrs::SV_STWARE_SBDROP_LEN_TOO_LARGE1 = "Stackware sbdrop len too large1." ;
StringId MiscStrs::SV_STWARE_SBDROP_LEN_TOO_LARGE2 = "Stackware sbdrop len too large2." ;
StringId MiscStrs::SV_STWARE_SBCOMPRESS_NO_EOR = "Stackware sbcompress no eor." ;
StringId MiscStrs::SV_STWARE_ARPINPUT_DUPLICATE_IP_ADDR = "Stware arp input duplicate ip addr" ;
StringId MiscStrs::SV_STWARE_XS_REGISTERIF_INVALID_TYPE = "Stware xs registerif invalid type" ;
StringId MiscStrs::SV_STWARE_XS_REGISTERIF_INVALID_FAMILY = "Stware xs registerif invalid type" ;
StringId MiscStrs::SV_STWARE_XS_DRIVIOCTL_INIT_FAILURE = "Stware xs drivioctl init failure" ;
StringId MiscStrs::SV_STWARE_XS_GOODETHERRARP_SHORT_RARP_PKT = "Stware xs goodetherrarp short rarp pkt" ;
StringId MiscStrs::SV_STWARE_XS_GOODETHERRARP_WRONG_HWTYPE = "Stware xs goodetherrarp wrong hwtype" ;
StringId MiscStrs::SV_STWARE_XS_GOODETHERRARP_SHORT_RARP_PKT2 = "Stware xs goodetherrarp short rarp pkt2" ;
StringId MiscStrs::SV_STWARE_XS_GOODETHERRARP_INVALID_PROTOCOL = "Stware xs goodetherrarp invalid protocol" ;
StringId MiscStrs::SV_STWARE_MXS_MBGET_OUT_OF_MBUF = "MXS_MGET: out of mbuf" ;
StringId MiscStrs::SV_STWARE_M_CLALLOC_OUT_OF_CLICK = "Stware m clalloc out of click" ;
StringId MiscStrs::SV_STWARE_M_COPYM_LEN_TOO_LARGE = "Stware m copym len too large";
StringId MiscStrs::SV_STWARE_M_COPYM_OFFSET_TOO_LARGE = "Stware m copym offset too large";
StringId MiscStrs::SV_STWARE_M_COPYDATA_OUT_OF_MBUF = "Stware m copydata out of mbuf";
StringId MiscStrs::SV_STWARE_M_COPYDATA_LEN_TOO_LARGE = "Stware m copydata len too large";
StringId MiscStrs::SV_STWARE_M_COPYDATA_OFFSET_NEGATIVE = "Stware m copydata offset negative";
StringId MiscStrs::SV_STWARE_M_COPYDATA_NULL_POINTER = "Stware m copydata null pointer";
StringId MiscStrs::SV_STWARE_MBUF_MULTI_MFREE = "Stware mbuf freed multiply";
StringId MiscStrs::SV_STWARE_INFINITE_MBUF_LINK = "Stware circle mbuf link";
StringId MiscStrs::SV_STWARE_MALLOC_FAILED = "Stware if_get_memory: malloc failed";
StringId MiscStrs::SV_DCI_PARITY_ERROR = "DCI parity error";
StringId MiscStrs::SV_DCI_INPUT_BUFFER_OVERFLOW_ERROR = "DCI input buffer overflow error";
StringId MiscStrs::SV_DCI_NON_SPECIFIC_ERROR = "DCI command error";
StringId MiscStrs::SV_DCI_UNKNOWN_ERROR = "DCI unknown error";
StringId MiscStrs::SV_SAFETYNET_CHECKSUM_ERROR = "BD Monitor data checksum error";
StringId MiscStrs::SV_SAFETYNET_CYCLE_TIME_ERROR = "BD Monitor cycle time error";
StringId MiscStrs::SV_SAFETYNET_INTERVAL_TIME_ERROR = "BD Monitor interval time error";
StringId MiscStrs::SV_SAFETYNET_SYNC_ACQUIRED = "BD Monitor sync acquired";
StringId MiscStrs::SV_SAFETYNET_SYNC_LOST = "BD Monitor sync lost";


// POST error text 
StringId MiscStrs::SV_POST_PROCESSOR_INITIALIZATION = "Processor Initialization";
StringId MiscStrs::SV_POST_INTEGER_UNIT_TEST =          "Integer Unit Test";
StringId MiscStrs::SV_POST_DRAM_REFRESH_TIMER_TEST = "DRAM Refresh Timer Test";
StringId MiscStrs::SV_POST_KERNEL_DRAM_TEST =           "Kernel DRAM Test";
StringId MiscStrs::SV_POST_BOOT_EPROM_CHECKSUM_TEST = "Boot EPROM Checksum Test";
StringId MiscStrs::SV_POST_PHASE_2_TEST =                       "POST Phase 2 Initialization";
StringId MiscStrs::SV_POST_ADDRESSING_MODE_TEST =       "Addressing Mode Test";
StringId MiscStrs::SV_POST_KERNEL_NOVRAM_TEST =         "Kernel NOVRAM Test";
StringId MiscStrs::SV_POST_ROLLING_THUNDER_TEST =       "Rolling Thunder Test";
StringId MiscStrs::SV_POST_INTERRUPT_CONTROLLER_TEST = "Interrupt Controller Test";
StringId MiscStrs::SV_POST_TIME_OF_DAY_CLOCK_TEST = "Time of Day Clock Test";
StringId MiscStrs::SV_POST_TIMER_TEST =                         "Timer Test";
StringId MiscStrs::SV_POST_WATCHDOG_TIMER_TEST =        "Watchdog Timer Test";
StringId MiscStrs::SV_POST_EEPROM_CHECKSUM_TEST =       "FLASH Memory Checksum Test";
StringId MiscStrs::SV_POST_MEMORY_MANAGEMENT_UNIT_TEST = "Memory Management Unit Test";

StringId MiscStrs::SV_POST_BUS_TIMER_TEST =             "Bus Timer Test";
StringId MiscStrs::SV_POST_NMI_REGISTER_TEST =          "NMI Source Register Test";
StringId MiscStrs::SV_POST_DRAM_TEST =                          "POST DRAM Test";
StringId MiscStrs::SV_POST_LAN_SELFTEST_START =         "Ethernet Self-Test Start";
StringId MiscStrs::SV_POST_LAN_SELFTEST_END =           "Ethernet Self-Test End";
StringId MiscStrs::SV_POST_UNEXPECTED_RESET_UMPIRE_TEST = "Unexpected Reset Umpire Test";
StringId MiscStrs::SV_POST_NOVRAM =                             "POST NOVRAM Test";
StringId MiscStrs::SV_POST_ERROR_FPU =                          "Floating Point Unit Test";
StringId MiscStrs::SV_POST_ERROR_DRAM_PARITY =          "DRAM Parity Circuit Test";
StringId MiscStrs::SV_POST_SAASTEST_START =             "SAAS Self-Test Start";
StringId MiscStrs::SV_POST_SAASTEST_END =                       "SAAS Self-Test End";
StringId MiscStrs::SV_POST_ERROR_RESONANCE_START =      "SAAS Resonance Test Start";
StringId MiscStrs::SV_POST_ERROR_RESONANCE_END =        "SAAS Resonance Test End";
StringId MiscStrs::SV_POST_VENTINOP =                           "Ventilator INOP Test";
StringId MiscStrs::SV_POST_ANALOG_INTERFACE_PCB_TEST = "Analog Interface PCB Test";
StringId MiscStrs::SV_POST_ADC_TEST =                           "ADC Test";
StringId MiscStrs::SV_POST_DAC_TEST =                           "DAC Test";
StringId MiscStrs::SV_POST_ANALOG_DEVICES_TEST =        "Analog Devices Test";
StringId MiscStrs::SV_POST_ERROR_SERIAL_DEVICE =        "BD Serial Device Test";
StringId MiscStrs::SV_POST_SAFESTATE_SYSTEM_TEST =      "Safe State System Test";
StringId MiscStrs::SV_SERVICE_SWITCH_STUCKED =          "Service Switch Stuck Test";
StringId MiscStrs::SV_AC_VOLTAGE_TEST =                         "AC Voltage Test";
StringId MiscStrs::SV_POST_DOWNLOAD_BOOT =                      "Download OS Boot";
StringId MiscStrs::SV_POST_SIGMA_OS_BOOT =                      "Application OS Boot";
StringId MiscStrs::SV_POST_PBMON_BOOT =                         "PB-MON Boot ";
StringId MiscStrs::SV_POST_APPLICATION_BOOT =           "Application Boot";


// Background error text
StringId MiscStrs::SV_BK_NO_EVENT = "No event" ;
StringId MiscStrs::SV_BK_SV_SWITCHED_SIDE_OOR = "Bad safety valve switched side" ;
StringId MiscStrs::SV_BK_EXH_FLOW_OOR = "Bad expiratory flow" ;
StringId MiscStrs::SV_BK_O2_PSOL_CURRENT_OOR = "Bad O2 PSOL current" ;
StringId MiscStrs::SV_BK_AIR_PSOL_CURRENT_OOR = "Bad air PSOL current" ;
StringId MiscStrs::SV_BK_EXH_MOTOR_CURR_OOR = "Bad exp motor current" ;
StringId MiscStrs::SV_BK_EXH_VLV_COIL_TEMP_OOR = "Bad exp valve coil temp." ;
StringId MiscStrs::SV_BK_EXH_PRESS_OOR = "Bad exp pressure" ;
StringId MiscStrs::SV_BK_INSP_PRESS_OOR = "Bad insp pressure" ;
StringId MiscStrs::SV_BK_AIR_FLOW_OOR_HIGH = "Air flow out of range HIGH";
StringId MiscStrs::SV_BK_AIR_FLOW_OOR_LOW = "Air flow out of range LOW";
StringId MiscStrs::SV_BK_O2_FLOW_OOR_HIGH = "O2 flow out of range HIGH";
StringId MiscStrs::SV_BK_O2_FLOW_OOR_LOW = "O2 flow out of range LOW";
StringId MiscStrs::SV_BK_AIR_FLOW_TEMP_OOR = "Bad air flow temp." ;
StringId MiscStrs::SV_BK_O2_FLOW_TEMP_OOR = "Bad O2 flow temp." ;
StringId MiscStrs::SV_BK_EXH_FLOW_TEMP_OOR = "Bad expiratory flow temp" ;
StringId MiscStrs::SV_BK_BD_10V_SUPPLY_MON_OOR = "Bad BD 10V supply" ;
StringId MiscStrs::SV_BK_BD_12V_FAIL_OOR = "Bad BD 12V supply" ;
StringId MiscStrs::SV_BK_BD_15V_FAIL_OOR = "Bad BD 15V supply" ;
StringId MiscStrs::SV_BK_NEG_15V_FAIL_OOR = "Bad BD -15V" ;
StringId MiscStrs::SV_BK_GUI_12V_FAIL_OOR = "Bad GUI 12V supply" ;
StringId MiscStrs::SV_BK_GUI_5V_FAIL_OOR = "Bad GUI 5V supply" ;
StringId MiscStrs::SV_BK_BD_5V_FAIL_OOR = "Bad BD 5V supply" ;
StringId MiscStrs::SV_BK_O2_PSOL_STUCK = "O2 PSOL stuck" ;
StringId MiscStrs::SV_BK_AIR_PSOL_STUCK = "Air PSOL stuck" ;
StringId MiscStrs::SV_BK_AIR_PSOL_STUCK_OPEN = "Air PSOL stuck open" ;
StringId MiscStrs::SV_BK_O2_PSOL_STUCK_OPEN = "O2 PSOL stuck open" ;
StringId MiscStrs::SV_BK_ATM_PRESS_OOR = "Bad atmospheric press OOR" ;
StringId MiscStrs::SV_BK_O2_SENSOR_OOR = "Bad O2 sensor OOR" ;
StringId MiscStrs::SV_BK_O2_SENSOR_OOR_RESET = "Bad O2 sensor OOR reset";
StringId MiscStrs::SV_BK_SVO_CURRENT_OOR = "Bad safety valve current" ;
StringId MiscStrs::SV_BK_PI_STUCK = "Insp pressure stuck" ;
StringId MiscStrs::SV_BK_PE_STUCK = "Exp pressure stuck" ;
StringId MiscStrs::SV_BK_INSP_AUTOZERO_FAIL = "Insp pressure autozero offset failed" ;
StringId MiscStrs::SV_BK_EXH_AUTOZERO_FAIL = "Exp pressure autozero offset failed" ;
StringId MiscStrs::SV_BK_POWER_FAIL_CAP = "Bad power fail capacitor voltage" ;
StringId MiscStrs::SV_BK_ALARM_CABLE = "Alarm cable error" ;
StringId MiscStrs::SV_BK_ADC_FAIL_HIGH = "Analog-Digital converter failed high" ;
StringId MiscStrs::SV_BK_ADC_FAIL_LOW = "Analog-Digital converter failed low" ;
StringId MiscStrs::SV_BK_ADC_LOOPBACK_FAIL = "ADC loopback constant" ;
StringId MiscStrs::SV_BK_TOUCH_SCREEN_FAIL = "Touch screen failed" ;
StringId MiscStrs::SV_BK_TOUCH_SCREEN_BLOCKED = "Touch screen blocked" ;
StringId MiscStrs::SV_BK_TOUCH_SCREEN_RESUME = "Touch screen resumed" ;
StringId MiscStrs::SV_BK_BPS_EVENT_INFO  = "Battery event";
StringId MiscStrs::SV_BK_COMPACT_FLASH_ERROR_INFO  = "Compact Flash error";
StringId MiscStrs::SV_BK_AC_SWITCH_STUCK = "AC switch stuck" ;
StringId MiscStrs::SV_BK_BD_NOVRAM_CHECKSUM = "BD NOVRAM checksum error" ;
StringId MiscStrs::SV_BK_BD_TOD_FAIL = "BD Time of Day failed" ;
StringId MiscStrs::SV_BK_GUI_TOD_FAIL = "GUI Time of Day failed" ;
StringId MiscStrs::SV_BK_GUI_NOVRAM_CHECKSUM = "GUI NOVRAM checksum error" ;
StringId MiscStrs::SV_BK_BPS_VOLTAGE_OOR  = "Bad backup power supply voltage" ;
StringId MiscStrs::SV_BK_BPS_CURRENT_OOR = "Bad backup power supply current" ;
StringId MiscStrs::SV_BK_BPS_MODEL_OOR = "Bad backup power supply model" ;
StringId MiscStrs::SV_BK_EXH_HEATER_OOR = "Bad exp heater" ;
StringId MiscStrs::SV_BK_GUI_STUCK_KEY = "GUI key stuck" ;
StringId MiscStrs::SV_BK_BD_EEPROM_CHECKSUM = "BD EEPROM checksum error" ;
StringId MiscStrs::SV_BK_GUI_EEPROM_CHECKSUM = "GUI EEPROM checksum error" ;
StringId MiscStrs::SV_BK_GUI_SAAS_COMM_FAIL = "GUI SAAS communication failed" ;
StringId MiscStrs::SV_BK_COMPR_ELAPSED_TIMER = "Compressor elapsed timer error" ;
StringId MiscStrs::SV_BK_COMPR_BAD_DATA = "Compressor bad eprom data" ;
StringId MiscStrs::SV_BK_LOSS_OF_GUI_COMM = "Loss of GUI communication" ;
StringId MiscStrs::SV_BK_LOSS_OF_BD_COMM = "Loss of BD communication" ;
StringId MiscStrs::SV_BK_RESUME_GUI_COMM = "Resume GUI communication" ;
StringId MiscStrs::SV_BK_RESUME_BD_COMM = "Resume BD communication" ;
StringId MiscStrs::SV_BK_EST_REQUIRED = "Est required" ;
StringId MiscStrs::SV_BK_GUI_SAAS_AUDIO_FAIL = "GUI SAAS Audio failed" ;
StringId MiscStrs::SV_BK_LV_REF_OOR = "LV Ref out of range" ;
StringId MiscStrs::SV_BK_SV_CURRENT_OOR = "SV current out of range" ;
StringId MiscStrs::SV_BK_MON_ALARMS_FAIL = "Monitor alarms fail";
StringId MiscStrs::SV_BK_MON_APNEA_ALARM_FAIL = "Monitor apnea alarm fails";
StringId MiscStrs::SV_BK_MON_APNEA_INT_FAIL = "Monitor apnea int fails";
StringId MiscStrs::SV_BK_MON_HIP_FAIL = "Monitor hip fails";
StringId MiscStrs::SV_BK_MON_BREATH_TIME_FAIL = "Monitor breath time fails";
StringId MiscStrs::SV_BK_MON_INSP_TIME_FAIL = "Monitor insp time fails";
StringId MiscStrs::SV_BK_MON_NO_DATA = "Monitor no data";
StringId MiscStrs::SV_BK_MON_O2_MIXTURE_FAIL = "Monitor O2 mixture fails";
StringId MiscStrs::SV_BK_MON_DATA_CORRUPTED = "Monitor data corrupted";
StringId MiscStrs::SV_BK_DATAKEY_UPDATE_FAIL = "Data key update failed";
StringId MiscStrs::SV_BK_GUI_WAVEFORM_DROP = "GUI drops a waveform packet";
StringId MiscStrs::SV_BK_BD_WAVEFORM_DROP = "BD drops a waveform packet";
StringId MiscStrs::SV_BK_FORCED_VENTINOP = "BK vent inop occurred";
StringId MiscStrs::SV_BK_TASK_MONITOR_FAIL = "Task Monitor";
StringId MiscStrs::SV_BK_BD_MULT_MAIN_CYCLE_MSGS = "Breath delivery extended control cycle.";
StringId MiscStrs::SV_BK_BD_MULT_SECOND_CYCLE_MSGS = "Breath delivery extended secondary cycle.";
StringId MiscStrs::SV_BK_WATCHDOG_FAILURE = "Watchdog failure occurred";
StringId MiscStrs::SV_BK_INIT_RESUME_GUI_COMM = "Init Resume GUI communication" ;
StringId MiscStrs::SV_BK_INIT_RESUME_BD_COMM = "Init Resume BD communication" ;
StringId MiscStrs::SV_BK_INIT_LOSS_GUI_COMM = "Init Loss of GUI communication" ;
StringId MiscStrs::SV_BK_INIT_LOSS_BD_COMM = "Init Loss of BD communication" ;
StringId MiscStrs::SV_BK_COMPR_UPDATE_SN = "Compressor S/N updated" ;
StringId MiscStrs::SV_BK_COMPR_UPDATE_PM_HRS = "10000 hours stored for elapsed time" ;
StringId MiscStrs::SV_BK_DATAKEY_SIZE_ERROR = "Cannot determine datakey size";
StringId MiscStrs::SV_BK_PROX_ERROR_INFO  = "PROX error";


// Hardware TRAP error text
StringId MiscStrs::SV_TRAP_ACCESS_FAULT = "Access Fault";
StringId MiscStrs::SV_TRAP_ADDRESS_ERROR = "Address Error";
StringId MiscStrs::SV_TRAP_ILLEGAL_INSTRUCTION = "Illegal Instruction";
StringId MiscStrs::SV_TRAP_INTEGER_DIVIDE_BY_ZERO = "Integer Divide by Zero";
StringId MiscStrs::SV_TRAP_PRIVILEGE_VIOLATION = "Privilege Violation";
StringId MiscStrs::SV_TRAP_FORMAT_ERROR = "Format Error";
StringId MiscStrs::SV_TRAP_UNINITIALIZED_INTERRUPT = "Uninitialized Interrupt";

StringId MiscStrs::INTRINSIC_PEEP_MARKER_LABEL =
        "{p=14,x=2,y=19:({x=60:)}}";
StringId MiscStrs::TOUCHABLE_INTRINSIC_PEEP_LABEL =
        "{p=12,y=19:PEEP{S:I}}";
StringId MiscStrs::TOUCHABLE_LARGE_INTRINSIC_PEEP_LABEL =
        "{p=18,y=26:PEEP{S:I}}";
StringId MiscStrs::TOUCHABLE_INTRINSIC_PEEP_MSG =
        "{p=14,y=17:PEEP{S:I}{T: = Intrinsic PEEP}}";
StringId MiscStrs::TOUCHABLE_INTRINSIC_PEEP_UNIT =
        "{p=8,y=19:H{S:2}O{X=-20,Y=-10:cm}}";
StringId MiscStrs::TOUCHABLE_INTRINSIC_PEEP_HPA_UNIT =
        "{p=8,y=19:hPa}";

StringId MiscStrs::TOTAL_PEEP_MARKER_LABEL =
        "{p=14,x=5,y=19:({x=60:)}}";
StringId MiscStrs::TOUCHABLE_TOTAL_PEEP_LABEL =
        "{p=12,y=19:PEEP{S:TOT}}";
StringId MiscStrs::TOUCHABLE_TOTAL_PEEP_MSG =
        "{p=14,y=17:PEEP{S:TOT}{T: = Total PEEP}}";
StringId MiscStrs::TOUCHABLE_TOTAL_PEEP_UNIT =
        "{p=8,y=19:H{S:2}O{X=-20,Y=-10:cm}}";
StringId MiscStrs::TOUCHABLE_TOTAL_PEEP_HPA_UNIT =
        "{p=8,y=19:hPa}";


StringId MiscStrs::VITAL_CAPACITY_MARKER_LABEL =
        "{p=14,x=5,y=19:({x=60:)}}";
StringId MiscStrs::TOUCHABLE_VITAL_CAPACITY_LABEL =
        "{p=12,y=19:VC}";
StringId MiscStrs::TOUCHABLE_VITAL_CAPACITY_MSG =
        "{p=14,y=17:VC{T: = Vital Capacity}}";
StringId MiscStrs::TOUCHABLE_VITAL_CAPACITY_UNIT =
        "{p=8,y=19:mL}";

StringId MiscStrs::NIF_PRESSURE_MARKER_LABEL =
        "{p=14,x=5,y=19:({x=60:)}}";
StringId MiscStrs::TOUCHABLE_NIF_PRESSURE_LABEL =
        "{p=12,y=19:NIF}";
StringId MiscStrs::TOUCHABLE_NIF_PRESSURE_MSG =
        "{p=14,y=17:NIF{T: = Negative Inspiratory Force}}";
StringId MiscStrs::TOUCHABLE_NIF_PRESSURE_UNIT =
        "{p=8,y=19:H{S:2}O{X=-20,Y=-10:cm}}";
StringId MiscStrs::TOUCHABLE_NIF_PRESSURE_HPA_UNIT =
        "{p=8,y=19:hPa}";

StringId MiscStrs::P100_PRESSURE_MARKER_LABEL =
        "{p=14,x=5,y=19:({x=60:)}}";
StringId MiscStrs::TOUCHABLE_P100_PRESSURE_LABEL =
        "{p=12,y=19:P{S:0.1}}";
StringId MiscStrs::TOUCHABLE_P100_PRESSURE_MSG =
        "{p=14,y=17:P{S:0.1}{T: = Occlusion Pressure at 100ms}}";
StringId MiscStrs::TOUCHABLE_P100_PRESSURE_UNIT =
        "{p=8,y=19:H{S:2}O{X=-20,Y=-10:cm}}";
StringId MiscStrs::TOUCHABLE_P100_PRESSURE_HPA_UNIT =
        "{p=8,y=19:hPa}";


StringId MiscStrs::TOUCHABLE_PLATEAU_PRESSURE_LABEL =
        "{p=14,y=19:P{S:PL}}";
StringId MiscStrs::TOUCHABLE_PLATEAU_PRESSURE_MSG =
        "{p=14,x=86,y=17:P{S:PL}{T: = Plateau pressure}}";
StringId MiscStrs::TOUCHABLE_PLATEAU_PRESSURE_UNIT =
        "{p=8,y=19:H{S:2}O{X=-20,Y=-10:cm}}";
StringId MiscStrs::TOUCHABLE_PLATEAU_PRESSURE_HPA_UNIT =
        "{p=8,y=19:hPa}";

StringId MiscStrs::TOUCHABLE_LARGE_STATIC_COMPLIANCE_LABEL =
        "{p=18,y=26:C{S:STAT}}";
StringId MiscStrs::TOUCHABLE_STATIC_COMPLIANCE_LABEL =
        "{p=14,y=19:C{S:STAT}}";
StringId MiscStrs::TOUCHABLE_STATIC_COMPLIANCE_MSG =
        "{p=14,y=17:C{S:STAT} = {T:Static lung compliance}}";
StringId MiscStrs::TOUCHABLE_PAV_COMPLIANCE_WF_LABEL =
        "{p=14,y=19:C{S:PAV}}";
StringId MiscStrs::TOUCHABLE_LUNG_COMPLIANCE_UNIT =
        "{p=8,y=19:cmH{S:2}O{X=-27,Y=-11:mL}{x=0:_______}}";
StringId MiscStrs::TOUCHABLE_LUNG_COMPLIANCE_HPA_UNIT =
        "{p=8,y=19:hPa{X=-27,Y=-10:mL}{x=0:___}}";
StringId MiscStrs::TOUCHABLE_STATIC_COMPLIANCE_UNIT =
        "{p=10,x=16,y=15:mL{x=9:____}{x=0,y=29:cmH{S:2}O}}";
StringId MiscStrs::TOUCHABLE_STATIC_COMPLIANCE_HPA_UNIT =
	    "{p=10,x=7,x=6,y=15:mL{x=0:____}{x=0,y=29:hPa}}";


StringId MiscStrs::TOUCHABLE_LARGE_STATIC_RESISTANCE_LABEL =
        "{p=18,y=26:R{S:STAT}}";
StringId MiscStrs::TOUCHABLE_STATIC_RESISTANCE_LABEL =
        "{p=14,y=19:R{S:STAT}}";
StringId MiscStrs::TOUCHABLE_STATIC_RESISTANCE_MSG =
        "{p=14,y=17:R{S:STAT} = {T:Static airway resistance}}";
StringId MiscStrs::TOUCHABLE_PAV_RESISTANCE_WF_LABEL =
        "{p=14,y=19:R{S:PAV}}";
StringId MiscStrs::TOUCHABLE_AIRWAY_RESISTANCE_UNIT =
        "{p=8,y=21:        L/s{X=-23,Y=-12:cmH{S:2}O}{x=13,Y=3:______}}";
StringId MiscStrs::TOUCHABLE_AIRWAY_RESISTANCE_HPA_UNIT =
        "{p=8,y=21:    L/s{X=-16,Y=-9:hPa}{x=7,Y=0:____}}";
StringId MiscStrs::TOUCHABLE_STATIC_RESISTANCE_UNIT =
        "{p=10,x=0,y=15:cmH{S:2}O{x=0,Y=2:_______}{x=16,y=32:L/s}}";
StringId MiscStrs::TOUCHABLE_STATIC_RESISTANCE_HPA_UNIT =
        "{p=10,x=1,y=15:hPa{x=0:____}{x=5,y=29:L/s}}";



StringId MiscStrs::PAV_STARTUP_STATUS_SMALL_MSG =
        "{p=12,c=13,y=19:PAV STARTUP}";
StringId MiscStrs::PAV_STARTUP_STATUS_LARGE_MSG =
        "{p=18,c=13,y=26:PAV STARTUP}";
StringId MiscStrs::VCP_STARTUP_STATUS_SMALL_MSG =
        "{p=12,c=13,y=19:VC+ STARTUP}";
StringId MiscStrs::VS_STARTUP_STATUS_SMALL_MSG =
        "{p=12,c=13,y=19:VS STARTUP}";
StringId MiscStrs::VCP_STARTUP_STATUS_LARGE_MSG =
        "{p=18,c=13,y=26:VC+ STARTUP}";
StringId MiscStrs::VS_STARTUP_STATUS_LARGE_MSG =
        "{p=18,c=13,y=26:VS STARTUP}";

StringId MiscStrs::R_AND_C_MARKER_LABEL =
        "{p=14,x=5,y=17:({x=67:)}}";
StringId MiscStrs::RM_MARKER_LABEL =
        "{p=18,x=0,y=24:({x=67:)}}";
StringId MiscStrs::PRESS_MARKER_LABEL =
        "{p=14,x=10,y=17:({x=50:)}}";
StringId MiscStrs::MANEUVER_CANCELED =
        "{p=14,y=22,T:Maneuver canceled: detection of trigger or alarm}";
StringId MiscStrs::CORRUPTED_MEASUREMENT =
        "{p=10,y=33,T:Questionable measurement}";
StringId MiscStrs::OUT_OF_RANGE_LABEL =
        "{p=10,y=33,T:Out of range}";
StringId MiscStrs::SUB_THRESHOLD_INPUT_LABEL =
        "{p=10,y=33,T:Sub-threshold input value(s)}";
StringId MiscStrs::INCOMPLETE_EXHALATION_LABEL =
        "{p=10,y=33,T:Incomplete exhalation}";
StringId MiscStrs::NO_PLATEAU_LABEL =
        "{p=10,y=33,T:No plateau}";
StringId MiscStrs::DASH_LABEL =
        "{p=14,y=19:------}";
StringId MiscStrs::STAR_LABEL =
        "{p=14,y=22:*******}";
StringId MiscStrs::RM_DASH_LABEL =
        "{p=14,y=19:--------}";
StringId MiscStrs::RM_STAR_LABEL =
        "{p=14,y=22:*******}";
        
StringId MiscStrs::NEW_PATIENT_MALFUNCTION_MESSAGE =
        "{p=12,y=25:Cannot setup patient.  Ventilator malfunction.}";
StringId MiscStrs::NEW_PATIENT_NO_VENT_MESSAGE =
        "{p=12,y=25:Cannot setup patient. Ventilation is not allowed.}";
StringId MiscStrs::SST_NOT_ALLOWED_PATIENT_MESSAGE =
        "{p=12,y=25:Short self test is not allowed after patient connection.}";
StringId MiscStrs::SST_NOT_ALLOWED_GUI_POST_FAIL_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:GUI POST failed.}}";
StringId MiscStrs::SST_NOT_ALLOWED_BD_POST_FAIL_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:BD POST failed.}}";
StringId MiscStrs::SST_NOT_ALLOWED_GUI_BG_FAIL_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:GUI Background failed.}}";
StringId MiscStrs::SST_NOT_ALLOWED_BD_BG_FAIL_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:BD Background failed.}}";
StringId MiscStrs::SST_NOT_ALLOWED_VENT_INOP_FAIL_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:Vent Inop major failure occurred.}}";
StringId MiscStrs::SST_NOT_ALLOWED_VENT_INOP_TEST_IN_PROGRESS_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:Vent Inop test in progress.}}";
StringId MiscStrs::SST_NOT_ALLOWED_EXH_CAL_REQD_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:Exp valve cal required.}}";
StringId MiscStrs::SST_NOT_ALLOWED_FLOW_SENSOR_INFO_REQD_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:Flow sensor info required.}}";
StringId MiscStrs::SST_NOT_ALLOWED_FLOW_SENSOR_CAL_REQD_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:Flow sensor calibration failed.}}";
StringId MiscStrs::SST_NOT_ALLOWED_GUI_TIMEOUT_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:GUI Timeout occurred.}}";
StringId MiscStrs::SST_NOT_ALLOWED_GUI_INOP_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:GUI Inop occurred.}}";
StringId MiscStrs::SST_NOT_ALLOWED_TUV_FAILED_GUI_INOP_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:BD TUV failed.}}";
StringId MiscStrs::SST_NOT_ALLOWED_BD_INOP_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:BD Inop occurred.}}";
StringId MiscStrs::SST_NOT_ALLOWED_LOCKOUT_MESSAGE =
        "{p=12,y=25:Short self test is not allowed.  Ventilator malfunction.{x=0,Y=16:Timeout/Inop occurred.}}";
StringId MiscStrs::SST_NOT_ALLOWED_BAD_FLASH_TABLE =
        "{p=12,y=16:Short self test is not allowed.  Service required,{x=0,Y=16:use Service Mode to correct.}}";
StringId MiscStrs::SST_NOT_ALLOWED_BAD_SERIAL_NUMBER =
        "{p=12,y=16:Short self test is not allowed.  Service required,{x=0,Y=16:use Service Mode to correct serial number.}}";
StringId MiscStrs::SST_NOT_ALLOWED_EST_MESSAGE =
        "{p=12,y=16:Short self test is not allowed.  EST is required, use{x=0,Y=16:Service Mode.}}";

StringId MiscStrs::TOUCHABLE_LOW_O2_MSG =
        "{p=14,y=17:\003O{S:2}{T: = Low measured oxygen %}}";
StringId MiscStrs::TOUCHABLE_HIGH_O2_MSG =
        "{p=14,y=17:\004O{S:2}{T: = High measured oxygen %}}";
StringId MiscStrs::TOUCHABLE_HIGH_PRES_VENT_MSG =
        "{p=14,y=17:\004P{S:VENT}{T: = High internal ventilator pressure}}";



//
// Help (Info) subscreen text.
//
StringId MiscStrs::HELP_SUBSCREEN_TITLE =
        "{p=14,y=18:Info}";
StringId MiscStrs::GO_BACK_BUTTON_TITLE =
        "{p=8,y=12:      GO BACK}";
StringId MiscStrs::PREV_PAGE_BUTTON_TITLE =
        "{p=10: }";
StringId MiscStrs::NEXT_PAGE_BUTTON_TITLE =
        "{p=10: }";
StringId MiscStrs::HELP_TOPIC_COLUMN_TITLE =
        "Topic";
StringId MiscStrs::HELP_SELECT_COLUMN_TITLE =
        "Select";
StringId MiscStrs::HELP_MAIN_SETTING_TITLE =
        "{p=8,y=25,x=55:Primary settings}";
StringId MiscStrs::HELP_LOWER_SUBSCREEN_TITLE =
        "{p=8,y=25,x=60:Batch settings}";
StringId MiscStrs::HELP_LOWER_SELECT_AREA_TITLE =
        "{p=8,y=10,x=4:Screen Select Area}";
StringId MiscStrs::HELP_MESSAGE_AREA_TITLE =
        "{p=8,y=10,x=4:Symbol Definitions}";
StringId MiscStrs::HELP_PROMPT_AREA_TITLE =
        "{p=8,y=14,x=8:Prompt Area}";
StringId MiscStrs::HELP_VPDA_TITLE =
        "{p=8,y=15,x=53:Vital Patient Data}";
StringId MiscStrs::HELP_ALARM_STATUS_AREA_TITLE =
        "{p=8,y=15,x=52:Alarm and Status}";
StringId MiscStrs::HELP_UPPER_SUBSCREEN_AREA_TITLE =
        "{p=8,y=20,x=40:Miscellaneous patient{x=37,Y=20:data, including graphics}}";
StringId MiscStrs::HELP_UPPER_SCREEN_SELECT_AREA_TITLE =
        "{p=8,y=10,x=47:Screen Select Area}";
StringId MiscStrs::HELP_DIAGRAM_BUTTON_TITLE =
        "{p=10,y=14:DIAGRAM}";
StringId MiscStrs::HELP_SCREEN_DIAGRAM_TITLE = "Upper and Lower Screen Area";
StringId MiscStrs::HELP_BREATH_TIMING_DIAGRAM_TITLE = "Breath Timing Diagram";
StringId MiscStrs::HELP_BREATH_TIMING_UNIT =
        "{p=12,y=12:5 {p=10,T:sec}}";
StringId MiscStrs::HELP_UPPER_SCREEN_DIAGRAM_LABEL =
        "{p=8,x=5,y=15:Upper screen:{x=5,Y=15:monitored information{x=5,Y=15:(alarms, patient data)}}}";
StringId MiscStrs::HELP_LOWER_SCREEN_DIAGRAM_LABEL =
        "{p=8,x=5,y=15:Lower screen:{x=5,Y=15:ventilator settings}}";
//
// Text that can appear on the Ventilator Test Summary subscreen.
//
StringId MiscStrs::VENT_TEST_SUMMARY_TITLE =
        "{p=14,y=18:Ventilator Test Summary}";
StringId MiscStrs::VTS_LAST_RUN_COLUMN_TITLE =
        "Last Run";
StringId MiscStrs::VTS_OUTCOME_COLUMN_TITLE =
        "Outcome";
StringId MiscStrs::VTS_SST_LABEL =
        "SST";
StringId MiscStrs::VTS_EST_LABEL =
        "EST";
StringId MiscStrs::VTS_PASSED_LABEL =
        "PASSED";
StringId MiscStrs::VTS_OVERRIDDEN_LABEL =
        "{c=13:OVERRIDDEN}";
StringId MiscStrs::VTS_MINOR_FAILURE_LABEL =
        "ALERT";
StringId MiscStrs::VTS_MAJOR_FAILURE_LABEL =
        "FAILED";
StringId MiscStrs::VTS_INCOMPLETE_TEST_LABEL =
        "INCOMPLETE";
StringId MiscStrs::VTS_UNDEFINED_RESULT_LABEL =
        "NEVER RUN";
StringId MiscStrs::VTS_CAL_WARNING_VALUE =
        "{p=12,y=18:Failed}";
StringId MiscStrs::VTS_CAL_WARNING_LABEL =
        "{p=12,y=18:Exp. Calibration:}";
StringId MiscStrs::VTS_FLOW_SENSOR_WARNING_LABEL =
        "{p=12,y=18:Flow Sensor Calibration:}";
StringId MiscStrs::VTS_TEST_VENT_INOP_WARNING_LABEL =
        "{p=12,y=18:Vent Inop Test:}";
StringId MiscStrs::VTS_PRESSURE_XDUCER_CAL_WARNING_LABEL =
        "{p=12,y=18:Atm Pressure Calibration:}";
//
// The initialization screen
//
StringId MiscStrs::INIT_SCREEN_MESSAGE =
        "{p=24,y=27,T:NELLCOR PURITAN BENNETT"
        "{p=18,x=110,Y=40,I:840 Ventilator System}}";
StringId MiscStrs::PRESSURE_VOLUME_INSP_AREA_LABEL =
        "{p=10,x=11,y=20:Insp Area}";
//
// Lower Service related subscreen titles.
//
StringId MiscStrs::SERVICE_MODE =
        "{p=24,y=32:SERVICE MODE"
        "{p=12,x=2,Y=24:Ventilator support not available}}";
StringId MiscStrs::SERVICE_COMM_DOWN =
        "{p=24,y=32:SERVICE MODE"
        "{p=12,x=40,Y=24:Communication Down}}";
StringId MiscStrs::EST_SETUP_TAB_BUTTON_LABEL =
        "{p=10,y=21:EST}";
StringId MiscStrs::CALIB_SETUP_TAB_BUTTON_LABEL =
        "{p=10,y=21:CAL}";
StringId MiscStrs::CONFIG_SETUP_TAB_BUTTON_LABEL =
        "{p=10,y=21:DATE/TIME}";
StringId MiscStrs::EXIT_SERVICE_MODE_TAB_BUTTON_LABEL =
        "{p=10,y=21:EXIT}";
StringId MiscStrs::SST_SETUP_SUBSCREEN_TITLE =
        "{p=14,y=18:SST Setup}";
StringId MiscStrs::SST_STATUS_SUBSCREEN_TITLE =
        "{p=14,y=18:SST Status}";
StringId MiscStrs::SST_NOT_VENTILATING =
        "{p=24,y=40:SST: NOT VENTILATING}";
StringId MiscStrs::SST_COMM_DOWN =
        "{p=24,y=40:SST: COMM DOWN}";
StringId MiscStrs::SST_PROPOSED_SETUP_SUBSCREEN_TITLE =
        "{p=14,y=18:SST Proposed Setup}";
StringId MiscStrs::SST_EXIT_MSG =
        "{p=10,y=16:EXIT SST}";

StringId MiscStrs::SST_LOG_COL_1_TITLE = "Time";
StringId MiscStrs::SST_LOG_COL_2_TITLE = "Test";
StringId MiscStrs::SST_LOG_COL_3_TITLE = "Test Data";
StringId MiscStrs::SST_LOG_COL_4_TITLE = "Result";
StringId MiscStrs::SST_COMPLETED_LABEL = "{p=12,y=18:Completed:}";
StringId MiscStrs::SST_OUTCOME_LABAEL = "{p=12,y=18:SST Outcome:}";
StringId MiscStrs::SST_COMPLETED_VALUE = "p=12,y=18:";
StringId MiscStrs::SST_OUTCOME_VALUE = "p=12,y=18:";
StringId MiscStrs::SST_REPEAT_MSG =     "{p=10,y=16:REPEAT}";
StringId MiscStrs::SST_NEXT_MSG = "{p=10,y=16:NEXT}";
StringId MiscStrs::SST_OVERRIDE_MSG = "{p=10,y=16:OVERRIDE}";
StringId MiscStrs::SST_RESTART_SST_MSG = "{p=10,y=16:RESTART SST}";
StringId MiscStrs::SM_CIRCUIT_RESISTANCE_TEST_ID = "Circuit Resistance";
StringId MiscStrs::SM_SST_FILTER_TEST_ID = "Expiratory Filter";
StringId MiscStrs::SM_SST_LEAK_TEST_ID = "Circuit Leak";
StringId MiscStrs::SM_SST_FS_CC_TEST_ID = "SST Flow Sensor Test";
StringId MiscStrs::SM_COMPLIANCE_CALIB_TEST_ID = "Compliance Calibration";
StringId MiscStrs::SM_SST_PROX_STRING_ID = "PROX Test";
StringId MiscStrs::SST_COMPLETE_MSG = "{p=10,y=16:COMPLETE}";

//
// SST Test Result Data title
//
StringId MiscStrs::TEST_RESULT_SUBSCREEN_TITLE =
        "{p=14,y=18:SST Results}";
StringId MiscStrs::SST_CIRCUIT_RESISTANCE_TEST_INSP_LIMB_LABEL =
        "Insp limb \177P:";
StringId MiscStrs::SST_CIRCUIT_RESISTANCE_TEST_EXH_LIMB_LABEL =
        "Exp limb \177P:";
StringId MiscStrs::SST_FILTER_TEST_DATA_LABEL =
        "Exp filter \177P @ 60 L/min:";
StringId MiscStrs::SST_NEO_FILTER_TEST_DATA_LABEL =
        "Exp filter \177P @ 30 L/min:";
StringId MiscStrs::SST_LEAK_TEST_DATA_LABEL =
        "Leak \177P after 10 sec:";
StringId MiscStrs::COMPLIANCE_CALIB_TEST_DATA_LABEL = "Nominal C:";
StringId MiscStrs::CM_H20_LABEL = "cmH{S:2}O";
StringId MiscStrs::ML_LABEL = "mL";
StringId MiscStrs::SST_DATE_TIMESTAMP_LINE1 = "{p=10,y=10:%T}";
StringId MiscStrs::SST_DATE_TIMESTAMP_LINE2 = "{p=8,y=10:%s}";



//
// SST result subscreen
//
StringId MiscStrs::SST_STATUS_LABEL = "{p=12,y=18:SST STATUS:}";
StringId MiscStrs::EMPTY_STRING = "";
StringId MiscStrs::SST_DATE_TIME_LABEL = "{p=12,y=18:Date / Time:}";
//
// EST tests
//
StringId MiscStrs::EXTENDED_SELF_TEST_SUBSCREEN_TITLE =
        "{p=14,y=18:Extended Self Test}";
StringId MiscStrs::EST_STATUS_TITLE =
        "{p=12,y=18:EST STATUS:}";
StringId MiscStrs::EST_LOG_COL_1_TITLE = "Time";
StringId MiscStrs::EST_LOG_COL_2_TITLE = "Test";
StringId MiscStrs::EST_LOG_COL_3_TITLE = "Diagnostic Codes";
StringId MiscStrs::EST_LOG_COL_4_TITLE = "Result";
StringId MiscStrs::EST_LOG_COL_5_TITLE = "Data";
StringId MiscStrs::EST_RUNNING_MSG = "{p=12,y=18:Running}";
StringId MiscStrs::EST_WAITING_MSG = "{p=12,y=18:Waiting}";
StringId MiscStrs::EST_PAUSING_MSG = "{p=12,y=18:Pausing}";
StringId MiscStrs::EST_HALTED_MSG = "{p=12,y=18:Halted}";
StringId MiscStrs::PASSING_OVERALL_MSG = "{p=12,y=18:Passed}";
StringId MiscStrs::OVERRIDEN_OVERALL_MSG = "{p=12,y=18:Overridden}";
StringId MiscStrs::UNDEFINED_OVERALL_MSG = "{p=12,y=18:Never Run}";
StringId MiscStrs::IMCOMPLETE_OVERALL_MSG = "{p=12,y=18:Incomplete}";
StringId MiscStrs::FAILURE_OVERALL_MSG = "{p=12,y=18:Failure}";
StringId MiscStrs::ALERTING_OVERALL_MSG = "{p=12,y=18:Alert}";
StringId MiscStrs::NOT_APPLICABLE_STATUS_MSG = "NOT APPLIED";
StringId MiscStrs::PASSED_STATUS_MSG = "PASSED";
StringId MiscStrs::FAILED_STATUS_MSG = "FAILED";
StringId MiscStrs::ALERT_STATUS_MSG = "ALERT";
StringId MiscStrs::NOT_INSTALLED_STATUS_MSG = "NOT INSTALLED";
StringId MiscStrs::EST_EXIT_MSG = "{p=10,y=18:EXIT EST}";
StringId MiscStrs::EST_REPEAT_MSG =     "{p=10,y=18:REPEAT}";
StringId MiscStrs::EST_NEXT_MSG = "{p=10,y=18:NEXT}";
StringId MiscStrs::EST_OVERRIDE_MSG = "{p=10,y=18:OVERRIDE}";
StringId MiscStrs::SM_GAS_SUPPLY_TEST_ID = "Gas Supply/SV Test";
StringId MiscStrs::SM_GUI_KEYBOARD_TEST_ID = "GUI Keyboard Test";
StringId MiscStrs::SM_GUI_KNOB_TEST_ID = "GUI Knob Test";
StringId MiscStrs::SM_GUI_LAMP_TEST_ID = "GUI Lamp Test";
StringId MiscStrs::SM_BD_LAMP_TEST_ID = "BD Lamp Test";
StringId MiscStrs::SM_GUI_AUDIO_TEST_ID = "GUI Audio Test";
StringId MiscStrs::SM_GUI_NURSE_CALL_TEST_ID = "GUI Nurse Call Test";
StringId MiscStrs::SM_BD_AUDIO_TEST_ID = "BD Audio Test";
StringId MiscStrs::SM_FS_CROSS_CHECK_TEST_ID = "Flow sensors cross check Test";
StringId MiscStrs::SM_CIRCUIT_PRESSURE_TEST_ID = "Circuit Pressure Test";
StringId MiscStrs::SM_SAFETY_SYSTEM_TEST_ID = "Safety System Test";
StringId MiscStrs::SM_EXH_VALVE_SEAL_TEST_ID = "Exp Valve Seal Test";
StringId MiscStrs::SM_EXH_VALVE_TEST_ID = "Exp Valve Test";
StringId MiscStrs::SM_EXH_VALVE_VELOCITY_XDUCER_TEST_ID = "EV Velocity Transducer Test";
StringId MiscStrs::SM_SM_LEAK_TEST_ID = "SM Leak Test";
StringId MiscStrs::SM_EXH_HEATER_TEST_ID = "Exp Heater Test";
StringId MiscStrs::SM_GENERAL_ELECTRONICS_TEST_ID = "Analog Data Display";
StringId MiscStrs::SM_GUI_TOUCH_TEST_ID = "GUI Touch Test";
StringId MiscStrs::SM_GUI_SERIAL_PORT_TEST_ID = "GUI Serial Port Test";
StringId MiscStrs::SM_BATTERY_TEST_ID = "Battery Test";

//
// EST Test Result
//
StringId MiscStrs::EST_TEST_GAS_SUPPLY_RESULT_ID =
        "{p=14,y=18: Gas Supply/SV Test Results}";
StringId MiscStrs::EST_TEST_GUI_KEYBOARD_RESULT_ID =
        "{p=14,y=18: GUI Keyboard Test Results}";
StringId MiscStrs::EST_TEST_GUI_KNOB_RESULT_ID =
        "{p=14,y=18: GUI Knob Test Results}";
StringId MiscStrs::EST_TEST_GUI_LAMP_RESULT_ID =
        "{p=14,y=18: GUI Lamp Test Results}";
StringId MiscStrs::EST_TEST_BD_LAMP_RESULT_ID =
        "{p=14,y=18: BD Lamp Test Results}";
StringId MiscStrs::EST_TEST_GUI_AUDIO_RESULT_ID =
        "{p=14,y=18: GUI Audio Test Results}";
StringId MiscStrs::EST_TEST_GUI_NURSE_CALL_RESULT_ID =
        "{p=14,y=18: GUI Nurse Call Test Results}";
StringId MiscStrs::EST_TEST_BD_AUDIO_RESULT_ID =
        "{p=14,y=18: BD Audio Test Results}";
StringId MiscStrs::EST_TEST_FS_CROSS_CHECK_RESULT_ID =
        "{p=14,y=18: Flow Sensor Test Results}";
StringId MiscStrs::EST_TEST_CIRCUIT_PRESSURE_RESULT_ID =
        "{p=14,y=18: Circuit Pressure Test Results}";
StringId MiscStrs::EST_TEST_SAFETY_SYSTEM_RESULT_ID =
        "{p=14,y=18: Safety System Test Results}";
StringId MiscStrs::EST_TEST_EXH_VALVE_SEAL_RESULT_ID =
        "{p=14,y=18: Exp Valve Seal Test Results}";
StringId MiscStrs::EST_TEST_EXH_VALVE_RESULT_ID =
        "{p=14,y=18: Exp Valve Test Results}";
StringId MiscStrs::EST_TEST_EXH_VALVE_VELOCITY_XDUCER_RESULT_ID =
        "{p=14,y=18: EV Velocity Transducer Test Results}";
StringId MiscStrs::EST_TEST_SM_LEAK_RESULT_ID =
        "{p=14,y=18: SM Leak Test Results}";
StringId MiscStrs::EST_TEST_EXH_HEATER_RESULT_ID =
        "{p=14,y=18: Exp Heater Test Results}";
StringId MiscStrs::EST_TEST_GENERAL_ELECTRONICS_RESULT_ID =
        "{p=14,y=18: Analog Data Display Results}";
StringId MiscStrs::EST_TEST_GUI_TOUCH_RESULT_ID =
        "{p=14,y=18: GUI Touch Test Results}";
StringId MiscStrs::EST_TEST_GUI_SERIAL_PORT_RESULT_ID =
        "{p=14,y=18: GUI Serial Port Test Results}";
StringId MiscStrs::EST_TEST_BATTERY_RESULT_ID =
        "{p=14,y=18: Battery Test Results}";
StringId MiscStrs::EST_TEST_RESULT_CM_H20_UNIT =
        "{p=8,x=20,y=11:cmH{S:2}O}";
StringId MiscStrs::EST_TEST_RESULT_HPA_UNIT =
        "{p=8,x=20,y=11:hPa}";
StringId MiscStrs::EST_TEST_RESULT_VOLT_UNIT =
        "{p=8,x=20,y=11:Volt}";
StringId MiscStrs::EST_TEST_RESULT_AMPS_UNIT =
        "{p=8,x=20,y=11:Amps}";
StringId MiscStrs::EST_TEST_RESULT_COUNT_UNIT =
        "{p=8,x=20,y=11:counts}";
StringId MiscStrs::EST_TEST_RESULT_LPM_UNIT =
        "{p=8,x=20,y=11:L/min}";
StringId MiscStrs::EST_TEST_RESULT_MAMPS_UNIT =
        "{p=8,x=20,y=11:mA}";
StringId MiscStrs::EST_TEST_RESULT_SECOND_UNIT =
        "{p=8,x=20,y=11:sec}";
StringId MiscStrs::EST_TEST_RESULT_MSECOND_UNIT =
        "{p=8,x=20,y=11:msec}";
StringId MiscStrs::EST_TEST_RESULT_DEGREE_C_UNIT =
        "{p=8,x=20,y=11:degree C}";
StringId MiscStrs::EST_TEST_RESULT_ML_UNIT =
        "{p=8,x=20,y=11:mL}";
StringId MiscStrs::EST_TEST_RESULT_PSIA_UNIT =
        "{p=8,x=20,y=11:PSIA}";
StringId MiscStrs::EST_TEST_RESULT_O2_PERCENT_UNIT =
        "{p=8,x=20,y=11:O{S:2}%}";
StringId MiscStrs::EST_TEST_RESULT_ML_CM_H20_UNIT =
        "{p=8,x=20,y=11:mL/cmH{S:2}O}";
StringId MiscStrs::EST_LEAK_PASS_MSG =
        "{p=8,x=10,y=15:PASS}";
StringId MiscStrs::EST_LEAK_FAIL_MSG =
        "{p=8,x=14,y=15:FAIL}";
StringId MiscStrs::GAS_SUPPLY_AIR_PSOL_LEAK_PRESSURE_TITLE =
        "{p=8,y=11:Air PSOL leak}";
StringId MiscStrs::GAS_SUPPLY_O2_PSOL_LEAK_PRESSURE_TITLE =
        "{p=8,y=11:O2 PSOL leak}";
StringId MiscStrs::GAS_SUPPLY_LOW_FLOW_CRACKING_PRESSURE_TITLE =
        "{p=8,y=11:SV cracking pressure}";
StringId MiscStrs::GAS_SUPPLY_HIGH_FLOW_CRACKING_PRESSURE_TITLE =
        "{p=8,y=11:SV peak steady-state pressure}";
StringId MiscStrs::GAS_SUPPLY_AIR_FLOW_SENSOR_0_OFFSET_TITLE =
        "{p=8,y=11:Air flow zero offset}";
StringId MiscStrs::GAS_SUPPLY_O2_FLOW_SENSOR_0_OFFSET_TITLE =
        "{p=8,y=11:O2 flow zero offset}";
StringId MiscStrs::GAS_SUPPLY_EXH_FLOW_SENSOR_0_OFFSET_TITLE =
        "{p=8,y=11:Exp flow zero offset}";
StringId MiscStrs::BD_AUDIO_ALARM_CABLE_VOLTAGE_TITLE =
        "{p=8,y=11:Alarm cable voltage}";
StringId MiscStrs::BD_AUDIO_POWER_FAIL_CAP_VOLTAGE1_TITLE =
        "{p=8,y=11:Initial voltage}";
StringId MiscStrs::BD_AUDIO_TIME_CONSTANT_TITLE =
        "{p=8,y=11:Time constant}";
StringId MiscStrs::FS_CROSS_CHECK_O2_120_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:O2 Flow @ 120 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_O2_120_EXHALATION_FLOW_TITLE =
        "{p=8,y=11:Exp Flow @ 120 L/min O2}";
StringId MiscStrs::FS_CROSS_CHECK_O2_120_PSOL_CURRENT_TITLE =
        "{p=8,y=11:O2 PSOL @ 120 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_O2_60_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:O2 Flow @ 60 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_O2_60_EXHALATION_FLOW_TITLE =
        "{p=8,y=11:Exp Flow @ 60 L/min O2}";
StringId MiscStrs::FS_CROSS_CHECK_O2_60_PSOL_CURRENT_TITLE =
        "{p=8,y=11:O2 PSOL @ 60 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_O2_20_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:O2 Flow @ 20 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_O2_20_EXHALATION_FLOW_TITLE =
        "{p=8,y=11:Exp Flow @ 20 L/min O2}";
StringId MiscStrs::FS_CROSS_CHECK_O2_20_PSOL_CURRENT_TITLE =
        "{p=8,y=11:O2 PSOL @ 20 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_O2_5_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:O2 Flow @ 5 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_O2_5_EXHALATION_FLOW_TITLE =
        "{p=8,y=11:Exp Flow @ 5 L/min O2}";
StringId MiscStrs::FS_CROSS_CHECK_O2_5_PSOL_CURRENT_TITLE =
        "{p=8,y=11:O2 PSOL @ 5 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_O2_1_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:O2 Flow @ 1 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_O2_1_EXHALATION_FLOW_TITLE =
        "{p=8,y=11:Exp Flow @ 1 L/min O2}";
StringId MiscStrs::FS_CROSS_CHECK_O2_1_PSOL_CURRENT_TITLE =
        "{p=8,y=11:O2 PSOL @ 1 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_O2_0_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:O2 Flow @ 0 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_120_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:Air Flow @ 120 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_120_EXHALATION_FLOW_TITLE =
        "{p=8,y=11:Exp Flow @ 120 L/min Air}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_120_PSOL_CURRENT_TITLE =
        "{p=8,y=11:Air PSOL @ 120 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_60_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:Air Flow @ 60 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_60_EXHALATION_FLOW_TITLE =
        "{p=8,y=11:Exp Flow @ 60 L/min Air}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_60_PSOL_CURRENT_TITLE =
        "{p=8,y=11:Air PSOL @ 60 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_20_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:Air Flow @ 20 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_20_EXHALATION_FLOW_TITLE =
        "{p=8,y=11:Exp Flow @ 20 L/min Air}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_20_PSOL_CURRENT_TITLE =
        "{p=8,y=11:Air PSOL @ 20 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_5_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:Air Flow @ 5 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_5_EXHALATION_FLOW_TITLE =
        "{p=8,y=11:Exp Flow @ 5 L/min Air}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_5_PSOL_CURRENT_TITLE =
        "{p=8,y=11:Air PSOL @ 5 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_1_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:Air Flow @ 1 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_1_EXHALATION_FLOW_TITLE =
        "{p=8,y=11:Exp Flow @ 1 L/min Air}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_1_PSOL_CURRENT_TITLE =
        "{p=8,y=11:Air PSOL @ 1 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_0_INSPIRATORY_FLOW_TITLE =
        "{p=8,y=11:Air Flow @ 0 L/min}";
StringId MiscStrs::FS_CROSS_CHECK_AIR_LIFTOFF_COMMAND_TITLE =
        "{p=8,y=11:Air PSOL liftoff cmds}";
StringId MiscStrs::FS_CROSS_CHECK_O2_LIFTOFF_COMMAND_TITLE =
        "{p=8,y=11:O2 PSOL liftoff cmds}";
StringId MiscStrs::CIRCUIT_PRESSURE_INSP_ZERO_COUNTS_TITLE =
        "{p=8,y=11:Insp autozero}";
StringId MiscStrs::CIRCUIT_PRESSURE_EXH_ZERO_COUNTS_TITLE =
        "{p=8,y=11:Exp autozero}";
StringId MiscStrs::TITLE_CMH20_UNIT =
        "cmH{S:2}O";
StringId MiscStrs::TITLE_HPA_UNIT =
        "hPa";
StringId MiscStrs::CIRCUIT_PRESSURE_INSP_PRESSURE_AT_10_TITLE =
        "{p=8,y=11:Insp P @ 10 }";
StringId MiscStrs::CIRCUIT_PRESSURE_EXH_PRESSURE_AT_10_TITLE =
        "{p=8,y=11:Exp P @ 10 }";
StringId MiscStrs::CIRCUIT_PRESSURE_INSP_AUTOZERO_PRESSURE_TITLE =
        "{p=8,y=11:Insp autozero}";
StringId MiscStrs::CIRCUIT_PRESSURE_EXH_AUTOZERO_PRESSURE_TITLE =
        "{p=8,y=11:Exp autozero}";
StringId MiscStrs::CIRCUIT_PRESSURE_INSP_PRESSURE_AT_50_TITLE =
        "{p=8,y=11:Insp P @ 50 }";
StringId MiscStrs::CIRCUIT_PRESSURE_EXH_PRESSURE_AT_50_TITLE =
        "{p=8,y=11:Exp P @ 50 }";
StringId MiscStrs::CIRCUIT_PRESSURE_INSP_PRESSURE_AT_100_TITLE =
        "{p=8,y=11:Insp P @ 100 }";
StringId MiscStrs::CIRCUIT_PRESSURE_EXH_PRESSURE_AT_100_TITLE =
        "{p=8,y=11:Exp P @ 100 }";
StringId MiscStrs::SAFETY_SYSTEM_INSPIRATORY_PRESSURE1_TITLE =
        "{p=8,y=11:SV open @ 60 L/min}";
StringId MiscStrs::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT1_TITLE =
        "{p=8,y=11:Valve open current}";
StringId MiscStrs::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT2_TITLE =
        "{p=8,y=11:Valve current @ 0.1 sec}";
StringId MiscStrs::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT3_TITLE =
        "{p=8,y=11:Valve current @ 0.3 sec}";
StringId MiscStrs::SAFETY_SYSTEM_SAFETY_VALVE_CURRENT4_TITLE =
        "{p=8,y=11:Valve current @ 1.0 sec}";
StringId MiscStrs::SAFETY_SYSTEM_ELAPSED_TIME_TITLE =
        "{p=8,y=11:Reverse flow time}";
StringId MiscStrs::EXH_VALVE_SEAL_EXH_MOTOR_TEMP_TITLE =
        "{p=8,y=11:Exp valve motor temp}";
StringId MiscStrs::EXH_VALVE_SEAL_DELTA_PRESSURE_TITLE =
        "{p=8,y=11:Exp delta P}";
StringId MiscStrs::EXH_VALVE_EXHALATION_PRESSURE_10_TITLE =
        "{p=8,y=11:Exp P @ 10}";
StringId MiscStrs::EXH_VALVE_EXHALATION_PRESSURE_45_TITLE =
        "{p=8,y=11:Exp P @ 45}";
StringId MiscStrs::EXH_VALVE_EXHALATION_PRESSURE_95_TITLE =
        "{p=8,y=11:Exp P @ 95}";
StringId MiscStrs::SM_LEAK_MONITOR_PRESSURE_TITLE =
        "{p=8,y=11:Insp Leak monitored}";
StringId MiscStrs::SM_LEAK_DELTA_PRESSURE_TITLE =
        "{p=8,y=11:Insp P drop}";
StringId MiscStrs::EXH_HEATER_EXHALATION_TEMPERATURE1_TITLE =
        "{p=8,y=11:Initial temp}";
StringId MiscStrs::EXH_HEATER_EXHALATION_TEMPERATURE2_TITLE =
        "{p=8,y=11:Temp: heater on}";
StringId MiscStrs::EXH_HEATER_EXHALATION_TEMPERATURE3_TITLE =
        "{p=8,y=11:Temp: heater off}";
StringId MiscStrs::COMPRESSOR_RUN_TEST_TIME_TITLE =
        "{p=8,y=11:Run mode test time}";
StringId MiscStrs::COMPRESSOR_DISABLED_TEST_TIME_TITLE =
        "{p=8,y=11:Disabled mode test time}";
StringId MiscStrs::COMPRESSOR_STANDBY_TEST_TIME_TITLE =
        "{p=8,y=11:Standby mode test time}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING0_TITLE =
        "{p=8,y=11:Insp P}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING1_TITLE =
        "{p=8,y=11:Exp P}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING2_TITLE =
        "{p=8,y=11:Atm P}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING3_TITLE =
        "{p=8,y=11:O2 Temperature}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING4_TITLE =
        "{p=8,y=11:Air Temperature}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING5_TITLE =
        "{p=8,y=11:Exp Gas Temp}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING6_TITLE =
        "{p=8,y=11:Exp Valve Temp}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING7_TITLE =
        "{p=8,y=11:Exp Heater Temp}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING8_TITLE =
        "{p=8,y=11:O2 Flow}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING9_TITLE =
        "{p=8,y=11:Air Flow}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING10_TITLE =
        "{p=8,y=11:Exp Flow}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING11_TITLE =
        "{p=8,y=11:Fio2}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING12_TITLE =
        "{p=8,y=11:O2 Psol}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING13_TITLE =
        "{p=8,y=11:Air Psol}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING14_TITLE =
        "{p=8,y=11:Exp Valve}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING15_TITLE =
        "{p=8,y=11:Safety Valve}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING16_TITLE =
        "{p=8,y=11:Ac Line}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING17_TITLE =
        "{p=8,y=11:Power Fail Cap}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING18_TITLE =
        "{p=8,y=11:10v Sentry}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING19_TITLE =
        "{p=8,y=11:15v Sentry}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING20_TITLE =
        "{p=8,y=11:Neg 15v Sentry}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING21_TITLE =
        "{p=8,y=11:5v Gui}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING22_TITLE =
        "{p=8,y=11:12v Gui}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING23_TITLE =
        "{p=8,y=11:5v VentHead}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING24_TITLE =
        "{p=8,y=11:12v Sentry}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING25_TITLE =
        "{p=8,y=11:Battery}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING26_TITLE =
        "{p=8,y=11:Battery}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING27_TITLE =
        "{p=8,y=11:Battery Model}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING28_TITLE =
        "{p=8,y=11:Alarm Cable}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING29_TITLE =
        "{p=8,y=11:SV Switched Side}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING30_TITLE =
        "{p=8,y=11:DAC wrap}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING31_TITLE =
        "{p=8,y=11:AI pcba revision}";
StringId MiscStrs::GENERAL_ELECTRONICS_READING32_TITLE =
        "{p=8,y=11:Low voltage reference}";
StringId MiscStrs::BATTERY_VOLTAGE1_TITLE =
        "{p=8,y=11:Initial voltage}";
StringId MiscStrs::BATTERY_VOLTAGE2_TITLE =
        "{p=8,y=11:Current voltage}";
StringId MiscStrs::BATTERY_DELTA_VOLTAGE_TITLE =
        "{p=8,y=11:\177 voltage}";
StringId MiscStrs::DELTA_SUM_EXH_PRESSURE_TITLE =
        "{p=8,y=11:Exp \177P}";

//
// PROX SST related strings
// 
StringId MiscStrs::PX_DISTAL_BAROMETRIC_PRESSURE_TITLE =
        "{p=8,y=11:Distal Barometric Pressure}";
StringId MiscStrs::PX_WYE_BAROMETRIC_PRESSURE_TITLE =
        "{p=8,y=11:Wye Barometric Pressure}";
StringId MiscStrs::PX_ACCUMULATOR_PRESSURE_TITLE =
        "{p=8,y=11:Accumulator Pressure}";
StringId MiscStrs::PX_RESERVOIR_PRESSURE_TITLE =
        "{p=8,y=11:Accumulator Pressure}";
StringId MiscStrs::PX_DISTAL_INSP_PRESSURE_TITLE_AT_10 =
        "{p=8,y=11:Distal Insp Pressure @ 10}";
StringId MiscStrs::PX_WYE_PRESSURE_TITLE_AT_10 =
        "{p=8,y=11:Wye Pressure @ 10}";
StringId MiscStrs::PX_DISTAL_INSP_PRESSURE_TITLE_AT_50 =
        "{p=8,y=11:Distal Insp Pressure @ 50}";
StringId MiscStrs::PX_WYE_PRESSURE_TITLE_AT_50 =
        "{p=8,y=11:Wye Pressure @ 50}";
StringId MiscStrs::PX_WYE_PRESSURE_TITLE =
        "{p=8,y=11:Wye Pressure}";
StringId MiscStrs::PX_AUTO_ZERO_ACCUMULATOR_PRESSURE_TITLE1 =
        "{p=8,y=11:1) Accumulator Pressure}";
StringId MiscStrs::PX_AUTO_ZERO_WYE_PRESSURE_TITLE2 =
        "{p=8,y=11:2) Wye Pressure}";
StringId MiscStrs::PX_AUTO_ZERO_ACCUMULATOR_PRESSURE_TITLE3 =
        "{p=8,y=11:3) Accumulator Pressure}";
StringId MiscStrs::PX_AUTO_ZERO_ACCUMULATOR_PRESSURE_TITLE4 =
        "{p=8,y=11:4) Accumulator Pressure}";
StringId MiscStrs::PX_AUTO_ZERO_ACCUMULATOR_PRESSURE_TITLE5 =
        "{p=8,y=11:5) Accumulator Pressure}";
StringId MiscStrs::PX_AUTO_ZERO_ACCUMULATOR_PRESSURE_TITLE6 =
        "{p=8,y=11:6) Accumulator Pressure}";
StringId MiscStrs::PX_AUTO_ZERO_FLOW_TITLE7 =
        "{p=8,y=11:7) Flow @ 0 L/min}";
StringId MiscStrs::PX_AUTO_ZERO_WYE_PRESSURE_TITLE8 =
        "{p=8,y=11:8) Wye Pressure}";

//
// Lower Service other screen related subscreen titles.
//
StringId MiscStrs::SM_SETUP_BUTTON_TITLE =
        "{p=10,y=27:Service Mode Setup}";
StringId MiscStrs::SM_SYSTEM_TEST_BUTTON_TITLE =
        "{p=10,y=27:External Test Control}";
StringId MiscStrs::SM_CALIBRATION_BUTTON_TITLE =
        "{p=10,y=27:Exp Valve Calibration}";
StringId MiscStrs::SM_VENT_INOP_BUTTON_TITLE =
        "{p=10,y=27:Vent Inop Test}";
StringId MiscStrs::SM_FLOW_SENSOR_CAL_BUTTON_TITLE =
        "{p=10,y=27:Flow Sensor Calibration}";
StringId MiscStrs::SM_PRESSURE_XDUCER_CAL_BUTTON_TITLE =
        "{p=10,x=3,y=18:Atmospheric Pressure"
        "{x=0,Y=15:Transducer Calibration}}";
StringId MiscStrs::SM_OPERATION_HOUR_COPY_BUTTON_TITLE =
        "{p=10,y=27:Datakey Update}";
StringId MiscStrs::SM_SERIAL_LOOPBACK_TEST_BUTTON_TITLE =
        "{p=10,y=27:Serial Loopback Test}";
StringId MiscStrs::SM_COMPACT_FLASH_TEST_BUTTON_TITLE =
        "{p=10,y=27:Compact Flash Test}";
//
// Upper Service related subscreen titles.
//
StringId MiscStrs::SST_RESULT_TAB_BUTTON_LABEL =
        "{p=10,x=10,y=15:SST{x=0,Y=15:RESULT}}";
StringId MiscStrs::DIAG_LOG_TAB_BUTTON_LABEL =
        "{p=10,y=15:DIAG{x=3,Y=15:LOG}}";
StringId MiscStrs::VENT_CONFIG_TAB_BUTTON_LABEL =
        "{p=10,x=10,y=15:VENT{x=0,Y=15:CONFIG}}";
StringId MiscStrs::OP_TIME_LOG_TAB_BUTTON_LABEL =
        "{p=10,y=15:OPERATION{x=22,Y=15:TIME}}";
StringId MiscStrs::TEST_SUMMARY_TAB_BUTTON_LABEL =
        "{p=10,x=17,y=15:TEST{x=0,Y=15:SUMMARY}}";
StringId MiscStrs::START_TEST_TAB_BUTTON_LABEL =
        "{p=10,y=18:START TEST}";
StringId MiscStrs::SOFTWARE_SALES_LABEL =
        "{p=8,x=0,y=15,c=8,T:Software Property of Puritan Bennett ( %s / %s )}";

//
// External remote test
//
// TBT
StringId MiscStrs::EXTERNAL_CONTROL_SUBSCREEN_TITLE =
        "{p=14,y=18:External Control}";
StringId MiscStrs::ESTABLISHING_CONNECTION_IN_PROGRESS_MESSAGE =
        "{p=14,y=16:Establishing Connection}";
// TBT
StringId MiscStrs::REMOTE_SYSTEM_TEST_IN_PROGRESS_MESSAGE =
        "{p=14,y=16:Remote System Test In Progress}";
//
// Service setup subscreen titles.
//
StringId MiscStrs::CURRENT_SERVICE_MODE_SETUP_SUBSCREEN_TITLE =
        "{p=14,y=18:Current Service Mode Settings}";
StringId MiscStrs::PROPOSED_SERVICE_MODE_SETUP_SUBSCREEN_TITLE =
        "{p=14,y=18,I:Proposed {N:Service Mode Settings}}";
StringId MiscStrs::SM_CONTRAST_DELTA_BUTTON_TITLE =
        "{p=10,y=12,T:Contrast Delta}";
StringId MiscStrs::SM_CONTRAST_DELTA_HELP_MESSAGE =
        "{p=14,T,y=17:Contrast Delta}";


//      
// EXHALATION VALVE CALIBRATION
//
StringId MiscStrs::EXH_VALVE_CALIB_SUBSCREEN_TITLE =
        "{p=14,y=18:Expiratory Valve Calibration}";
StringId MiscStrs::EXH_V_CAL_COMPLETE_MSG =
        "{p=12,y=18:Calibration complete}";
StringId MiscStrs::EXH_V_CAL_FAILED_MSG =
        "{p=12,y=18:Calibration failed}";
StringId MiscStrs::EXH_V_CAL_STATUS_LABEL =
        "{p=12,y=18,x=11:STATUS:}";
StringId MiscStrs::EXH_V_CAL_WARNING_MSG_LABEL =
        "{p=12,y=100:Consult manual before performing this procedure.}";
StringId MiscStrs::EXH_V_CAL_RUNNING_MSG =
        "{p=12,y=18:Running}";
StringId MiscStrs::EXH_V_CAL_WAITING_MSG =
        "{p=12,y=18:Waiting}";
StringId MiscStrs::EXH_V_CAL_FLOW_SENSOR_INFO_INVALID =
        "{p=12,x=108,y=19:Flow sensor info invalid}";
StringId MiscStrs::EXH_V_CAL_NO_AIR_CONNECTED =
        "{p=12,x=130,y=19:No air connected}";
StringId MiscStrs::EXH_V_CAL_NO_O2_CONNECTED =
        "{p=12,x=130,y=19:No O2 connected}";
StringId MiscStrs::EXH_V_CAL_INSP_AUTOZERO_FAILED =
        "{p=12,x=98,y=19:Inspiratory autozero failed}";
StringId MiscStrs::EXH_V_CAL_EXH_AUTOZERO_FAILED =
        "{p=12,x=98,y=19:Expiratory autozero failed}";
StringId MiscStrs::EXH_V_CAL_INSP_EXH_AUTOZERO_FAILED =
        "{p=12,x=75,y=19:Both insp and exp autozero failed}";
StringId MiscStrs::EXH_V_CAL_FS_UNABLE_ESTABLISH_FLOW =
        "{p=12,x=24,y=19:Flow sensor cross-check: Unable to establish flow}";
StringId MiscStrs::EXH_V_CAL_BAD_FLOW_SENSOR =
        "{p=12,x=85,y=19:Flow sensor cross-check failed}";
StringId MiscStrs::EXH_V_CAL_BAD_PRESSURE_SENSOR =
        "{p=12,x=77,y=19:Pressure sensor cross-check failed}";
StringId MiscStrs::EXH_V_CAL_PS_UNABLE_BUILD_PRESSURE =
        "{p=12,x=15,y=19:Pressure sensor cross-check: Unable to build pressure}";
StringId MiscStrs::EXH_V_CAL_PS_ALERT =
        "{p=12,x=40,y=19:Pressure sensor alert: approaching spec limit}";
StringId MiscStrs::EXH_V_CAL_TEMP_OOR =
        "{p=12,x=68,y=19:Exp valve temperature out of range}";
StringId MiscStrs::EXH_V_CAL_UNABLE_ESTABLISH_FLOW =
        "{p=12,x=108,y=19:Unable to establish flow}";
StringId MiscStrs::EXH_V_CAL_FAILED_GAIN_RESOLUTION =
        "{p=12,x=68,y=19:Calibration failed: Gain resolution}";
StringId MiscStrs::EXH_V_CAL_FAILED_CURR_LIMIT_EXCEEDED =
        "{p=12,x=54,y=19:Calibration failed: Current limit exceeded}";
StringId MiscStrs::EXH_V_CAL_BAD_LOOPBACK_CURRENT =
        "{p=12,x=80,y=19:Bad exp valve loopback current}";
StringId MiscStrs::EXH_V_CAL_UNABLE_ESTABLISH_PRESSURE =
        "{p=12,x=45,y=19:Calibration failed: Pressure build timeout.}";
StringId MiscStrs::EXH_V_CAL_AC_POWER_NOT_CONNECTED =
        "{p=12,x=105,y=19:AC power not connected}";
StringId MiscStrs::EXH_V_CAL_DIAG_UNDEFINED_ERROR =
        "{p=12,x=133,y=19:Undefined error}";
//
// Flow Sensor Calibration
//
StringId MiscStrs::FLOW_SENSOR_CALIBRATION_SUBSCREEN_TITLE =
        "{p=14,y=18:Flow Sensor Calibration}";
StringId MiscStrs::AIR_OFFSET_OOR =
        "{p=12,x=115,y=19:Air offset out of range}";
StringId MiscStrs::O2_OFFSET_OOR =
        "{p=12,x=117,y=19:O2 offset out of range}";
StringId MiscStrs::CANNOT_ACHIEVE_MIN_AIR_FLOW =
        "{p=12,x=75,y=19:Cannot achieve minimum air flow}";
StringId MiscStrs::CANNOT_ACHIEVE_MIN_O2_FLOW =
        "{p=12,x=75,y=19:Cannot achieve minimum O2 flow}";
//
// INITIALIZE FLOW SENSOR CALIBRATION
//
StringId MiscStrs::VENT_INOP_TEST_SUBSCREEN_TITLE =
        "{p=14,y=18:Vent Inop Test}";
StringId MiscStrs::FLOW_SENSOR_CALIB_SUBSCREEN_TITLE =
        "{p=14,y=18:Initialize Flow Sensor}";
StringId MiscStrs::FS_UNABLE_TO_READ_AIR_FLOW_SENSOR =
        "{p=12,x=7,y=19:Unable to read air flow sensor}";
StringId MiscStrs::FS_UNABLE_TO_READ_O2_FLOW_SENSOR =
        "{p=12,x=6,y=19:Unable to read O2 flow sensor}";
StringId MiscStrs::FS_UNABLE_TO_READ_AIR_TABLE =
        "{p=12,x=-30,y=19:Unable to read the expiratory flow sensor}";
StringId MiscStrs::FS_UNABLE_TO_READ_O2_TABLE =
        "{p=12,x=-35,y=19:Unable to read the expiratory flow sensor}";
StringId MiscStrs::FS_UNABLE_TO_PROGRAM_FLASH =
        "{p=12,x=21,y=19:Unable to program flash}";
StringId MiscStrs::FS_NOT_COMPATIBLE_WITH_SOFTWARE =
        "{p=12,x=-30,y=19:Software not compatible with flow sensors}";
StringId MiscStrs::VENT_INOP_FAILED_MSG =
        "{p=12,x=30,y=19:Vent Inop Test failed}";
StringId MiscStrs::BD_VENT_INOP_CONDITION1 =
        "{p=12,x=30,y=19:Vent Inop Test failed}";

//
// CALIBRATION INFOR DUPLICATION
//
StringId MiscStrs::CAL_INFO_DUP_SUBSCREEN_TITLE =
        "{p=14,y=18:Cal Info Duplication}";
StringId MiscStrs::CAL_INFO_DUP_ERROR_BURNING_FLASH =
        "{p=12,y=12:Unable to burn flash}";
StringId MiscStrs::CAL_INFO_DUP_ERROR_RECEIVE_FLASH_DATA =
        "{p=12,y=12:Unable to receive flash data}";
StringId MiscStrs::CAL_INFO_DUP_FAILED_MSG =
        "{p=12,y=19:Cal Info Dup failed}";

//
// Operational time log subscreen
//
StringId MiscStrs::OPER_TIME_SUBSCREEN_TITLE =
        "{p=14,y=18:Operational Time}";
StringId MiscStrs::COMPRESSOR_OPERATIONAL_HOURS_LABEL =
        "{p=12,y=20:Compressor operational time:}";
StringId MiscStrs::COMPRESSOR_OPERATIONAL_HOURS_UNIT =
        "{p=12,y=20:Hours}";
StringId MiscStrs::SYSTEM_OPERATIONAL_HOURS_LABEL =
        "{p=12,y=20:Ventilator operational time:}";
StringId MiscStrs::SYSTEM_OPERATIONAL_HOURS_UNIT =
        "{p=12,y=20:Hours}";
StringId MiscStrs::OPER_TIME_INCOMPLETE_MSG =
        "{p=12,y=18:Incomplete}";
StringId MiscStrs::OPER_TIME_COMPLETE_MSG =
        "{p=12,y=18:Complete}";
StringId MiscStrs::OPER_TIME_UPDATE_FAILED_MSG =
        "{p=12,y=18:Update failed}";

//
// Compact Flash Test Sub-Screen
//
StringId MiscStrs::COMPACT_FLASH_TEST_SUBSCREEN_TITLE =
        "{p=14,y=18:Compact Flash Test}";
StringId MiscStrs::CF_TEST_COMPLETE_MSG =
        "{p=12,y=18:Complete}";
StringId MiscStrs::CF_TEST_FAILED_MSG =
        "{p=12,y=18:Failed}";
StringId MiscStrs::CF_TEST_NEVER_RUN_MSG =
        "{p=12,y=18:Never Run}";
StringId MiscStrs::CF_INTERFACE_ERROR =
        "{p=12,y=18:Compact Flash Interface Error}";
StringId MiscStrs::CF_FLASH_ERROR =
        "{p=12,y=18:Compact Flash Device Error}";

// title for the Vent Config subscreen...
StringId MiscStrs::VENT_CONFIG_SUBSCREEN_TITLE =
        "{p=14,y=18:Ventilator Configuration}";

// DO NOT TRANSLATE THIS FORMAT FIELD...
StringId MiscStrs::CONFIG_TEXT_FIELD = "{p=10,x=%d,y=%d:%s {x=%d: %s}}";
StringId MiscStrs::PROX_CONFIG_LABEL = "{p=10,x=%d,y=%d:PROX Firmware:}";
StringId MiscStrs::CONFIG_NUM_FIELD = "{p=10,x=%d,y=%d:%s {x=%d: %d}}";

// these are used in the Vent Config subscreen, and MUST include the two spaces
// following the colon...
StringId MiscStrs::SOFTWARE_VERSION_LABEL = "Software:  ";
StringId MiscStrs::POST_VERSION_LABEL = "POST:  ";
StringId MiscStrs::SERIAL_NUM_LABEL = "Serial Number:  ";
StringId MiscStrs::COMPRESSOR_SERIAL_LABEL = "Compressor:  ";
StringId MiscStrs::SAAS_VERSION_LABEL = "SAAS:  ";
StringId MiscStrs::PROX_REV_LABEL = "Rev: ";

// these are used in the Vent Config and Dev Options subscreens, and MUST be
// full Cheap Text strings...
StringId MiscStrs::SOFTWARE_OPTIONS_LABEL =
        "{p=10:Software Options :}";
StringId MiscStrs::SOFTWARE_OPTION_BILEVEL_LABEL =
        "{p=10:BiLevel}";
StringId MiscStrs::SOFTWARE_OPTION_TC_LABEL =
        "{p=10:TC}";
StringId MiscStrs::SOFTWARE_OPTION_NEOMODE_LABEL =
        "{p=10:NeoMode}";
StringId MiscStrs::SOFTWARE_OPTION_VTPC_LABEL =
        "{p=10,x=0:V{p=6: }{p=10:V+}}";
StringId MiscStrs::SOFTWARE_OPTION_PAV_LABEL =
        "{p=10:PAV+}";
StringId MiscStrs::SOFTWARE_OPTION_RESP_MECH_LABEL =
        "{p=10:Resp Mech}";
StringId MiscStrs::SOFTWARE_OPTION_TRENDING_LABEL =
        "{p=10:Trending}";
StringId MiscStrs::SOFTWARE_OPTION_LEAK_COMP_LABEL =
        "{p=10:LC}";
StringId MiscStrs::SOFTWARE_OPTION_NEOMODE_UPDATE_LABEL =
        "{p=10:Neo Update}";
StringId MiscStrs::SOFTWARE_OPTION_NEOMODE_ADVANCED_LABEL =
        "{p=10:NeoMode 2.0}";
StringId MiscStrs::SOFTWARE_OPTION_NEOMODE_LOCKOUT_LABEL =
       "{p=10:Neonatal}";

// Note the folowing symbols are ONLY available in font size 24 point.
// Currently any attempt to display them in any other size font
// will cause a run time error which can be very difficult to trace!
// these are used in the Vent Config subscreen...
StringId MiscStrs::GUI_SYMBOL_LABEL = "{p=24:\032}";
StringId MiscStrs::BD_SYMBOL_LABEL = "{p=24:\235}";
StringId MiscStrs::COMPRESSOR_SYMBOL_LABEL = "{p=24:\220}";

// these are for External Control (laptop) only; MUST be straight text and
// untranslated...
StringId MiscStrs::BILEVEL_OPTION_LABEL = "BiLevel";
StringId MiscStrs::TC_OPTION_LABEL = "TC";
StringId MiscStrs::NEO_MODE_OPTION_LABEL = "NeoMode";
StringId MiscStrs::VTPC_OPTION_LABEL = "VV+";
StringId MiscStrs::PAV_OPTION_LABEL = "PAV+";
StringId MiscStrs::RESP_MECH_OPTION_LABEL = "RespMech";
StringId MiscStrs::TRENDING_OPTION_LABEL = "Trending";
StringId MiscStrs::LEAK_COMP_OPTION_LABEL = "LC";
StringId MiscStrs::NEO_MODE_UPDATE_OPTION_LABEL = "NeoUpdate";
StringId MiscStrs::NEO_MODE_ADVANCED_OPTION_LABEL = "NeoMode2.0";
StringId MiscStrs::NEO_MODE_LOCKOUT_OPTION_LABEL = "Neonatal";

//
// Serial Number subscreen
//
StringId MiscStrs::SER_NUMB_SETUP_SUBSCREEN_TITLE =
        "{p=14,y=18:Serial Number Setup}";
StringId MiscStrs::SER_NUM_SETTING_BUTTON_TITLE =
        "{p=10,y=28:Set serial number to match datakey}";
StringId MiscStrs::SER_NUM_BYPASSING_BUTTON_TITLE =
        "{p=10,y=28:Proceed without setting serial number}";
StringId MiscStrs::SER_NUM_TROUBLE_SHOOTING_BUTTON_TITLE1 =
        "{p=12,y=14:Data Key is not installed}";
StringId MiscStrs::SER_NUM_TROUBLE_SHOOTING_BUTTON_TITLE2 =
        "{p=12,y=14:Serial number(s) mismatch data key}";
StringId MiscStrs::SER_NUM_TROUBLE_SHOOTING_BUTTON_TITLE3 =
        "{p=12,y=14:Invalid Data Key Format}";
StringId MiscStrs::COPY_SN_FAILED_MSG =
        "{p=12,y=19:Failure}";
StringId MiscStrs::SN_CANT_PROGRAM_FLASH =
        "{p=12,y=12:Cannot program flash}";
StringId MiscStrs::NO_DATA_KEY =
        "{p=12,y=12:No Data Key installed}";
StringId MiscStrs::SERIAL_NUMBER_COLUMN1_DISPLAY =
        "{p=10,x=97,y=28:GUI SN:  %s{x=97,Y=20:BD SN :  %s}}";
StringId MiscStrs::SERIAL_NUMBER_COLUMN2_DISPLAY =
        "{p=10,x=2,y=28:GUI DataKey SN:  %s{x=2,Y=20:BD DataKey SN:  %s}}";
//
// SST Leak subscreen
//
StringId MiscStrs::SST_LEAK_TEST_SUBSCREEN_TITLE =
        "{p=14,y=18:SST leak Test}";
StringId MiscStrs::SST_LEAK_PASS_MSG =
        "{p=8,x=10,y=11:PASS}";
StringId MiscStrs::SST_LEAK_FAIL_MSG =
        "{p=8,x=14,y=11:FAIL}";
StringId MiscStrs::SST_LEAK_ALERT_MSG =
        "{p=8,x=7,y=11:ALERT}";

//
// SST Prox Subscreens
// 
StringId MiscStrs::PX_BAROMETRIC_TEST_TITLE =
        "{p=14,y=18:PROX Barometric Pressure Test}";
StringId MiscStrs::PX_PURGE_TEST_TITLE =
        "{p=14,y=18:PROX Purge Test}";
StringId MiscStrs::PX_AUTO_ZERO_LEAK_TEST_TITLE =
        "{p=14,y=18:PROX Autozero and Leak Test}";
StringId MiscStrs::PX_PRESSURE_CC_TEST_TITLE =
        "{p=14,y=18:PROX Pressure Cross Check Test}";
StringId MiscStrs::PX_FLOW_CC_TEST_TITLE =
        "{p=14,y=18:PROX Flow Cross Check Test}";


//
// Pressure transducer calibration subscreen
//
StringId MiscStrs::PRESSURE_XDUCER_SETUP_SUBSCREEN_TITLE =
        "{p=14,y=18:Pressure Transducer Setup}";
StringId MiscStrs::PRESSURE_XDUCER_CAL_SUBSCREEN_TITLE =
        "{p=14,y=18:Pressure Transducer Calib}";
StringId MiscStrs::PRESSURE_XDUCER_CAL_PROPOSED_SUBSCREEN_TITLE =
        "{p=14,y=18,N:Pressure Transducer {I:Proposed} Setup}";
StringId MiscStrs::BAROMETRIC_PRESSURE_BUTTON_TITLE_SMALL =
        "{p=10,y=13,T:Barometric Pressure}";
StringId MiscStrs::ATM_PRESSURE_UPDATING_MSG =
        "{p=12,y=18:Updating}";
StringId MiscStrs::ATM_PRESSURE_CALIBRATED_MSG =
        "{p=12,y=18:Calibrated}";
StringId MiscStrs::ATM_PRESSURE_NOT_CALIBRATED_MSG =
        "{p=12,y=18:Not Calibrated}";
StringId MiscStrs::ATM_PRESSURE_TRANSDUCER_OOR =
        "{p=12,x=50,y=19:Atmospheric Pressure Transducer OOR}";
//
// Operation hour copy subscreen
//
StringId MiscStrs::OPERATION_HOUR_COPY_SUBSCREEN_TITLE =
        "{p=14,y=18:Datakey Update}";
StringId MiscStrs::OP_HOUR_DK_NOT_INSTALLED =
        "{p=12,x=230,y=19:Datakey not installed}";
StringId MiscStrs::OP_HOUR_DK_WRITE_ERROR =
        "{p=12,x=240,y=19:Datakey write error}";
StringId MiscStrs::OP_HOUR_DK_CONFIRMATION_ERROR =
        "{p=12,x=195,y=19:Datakey confirmation error}";
StringId MiscStrs::OP_HOUR_DK_SN_NOT_THE_SAME =
        "{p=12,x=175,y=19:Datakey serial numbers do not match}";
        
//
// Serial Loopback test subscreen
//
StringId MiscStrs::SERIAL_LOOPBACK_TEST_SUBSCREEN_TITLE =
        "{p=14,y=18:Serial Loopback Test}";
StringId MiscStrs::SERIAL_LOOPBACK_TEST_FAILED_MSG =
        "{p=12,y=19:Failed}";
StringId MiscStrs::SERIAL_LOOPBACK_TEST_COMPLETE_MSG =
        "{p=12,y=19:Complete}";

// Symbol translation for user Off-Screen keys
StringId MiscStrs::MANUAL_INSPIRATION_HELP_MESSAGE =
        "{p=14,y=17,T:Manual Inspiration Key}";
StringId MiscStrs::EXPIRATORY_PAUSE_HELP_MESSAGE =
        "{p=14,y=17,T:Expiratory Pause Key}";
StringId MiscStrs::HUNDREDPERCENT_O2_HELP_MESSAGE =
        "{p=14,y=17,T:Hundred Percent O2 Key}";
StringId MiscStrs::PLUS_20_PERCENT_O2_HELP_MESSAGE =
        "{p=14,y=17,T:PLUS 20 Percent O2 Key}";
StringId MiscStrs::INSPIRATORY_PAUSE_HELP_MESSAGE =
        "{p=14,y=17,T:Inspiratory Pause Key}";

StringId MiscStrs::ALARM_RESET_HELP_MESSAGE =
        "{p=14,y=17:{p=10,Y=-4:\014}{p=6,X=-22,Y=7:RESET} {y=17,T: = Alarm Reset Key}}";
StringId MiscStrs::ALARM_SILENCE_HELP_MESSAGE =
        "{p=14,y=17:{p=10,Y=-4:\023} {y=17,T: = Alarm Silence Key}}";
StringId MiscStrs::ALARM_SILENCE_ACTIVE_HELP_MESSAGE =
        "{p=14,y=17,c=13: {y=17,T:Alarm Silence Active}}";
StringId MiscStrs::ALARM_VOLUME_HELP_MESSAGE =
        "{p=14,y=17:{p=10,Y=-4:\022} {y=17,T: = Alarm Volume Key}}";
StringId MiscStrs::SCREEN_BRIGHTNESS_HELP_MESSAGE =
        "{p=14,y=17:{p=10,Y=-4:\021} {y=17,T: = Screen Brightness Key}}";
StringId MiscStrs::SCREEN_CONTRAST_HELP_MESSAGE =
        "{p=14,y=17:{p=10,Y=-4:\020} {y=17,T: = Screen Contrast Key}}";
StringId MiscStrs::SCREEN_LOCK_HELP_MESSAGE =
        "{p=14,y=17:{p=10,Y=-4:\017} {y=17,T: = Screen Lock Key}}";
StringId MiscStrs::HELP_SCREEN_HELP_MESSAGE =
        "{p=14,y=17: ?{T: = Help Screen Key}}";

// Button units fields
// uses absolute x,y coordinate positioning within button

StringId MiscStrs::L_PER_MIN_UNITS_LARGE =
        "{p=10,x=76,y=36:L{x=66:____}{x=68,y=51:min}}";
StringId MiscStrs::L_PER_MIN_UNITS_SMALL = 
        "{p=8,T,x=76,y=23:L{x=68,Y=1:___}{x=70,y=35:min}}";
StringId MiscStrs::ONE_PER_MIN_UNITS_LARGE =
        "{p=10,x=76,y=36:1{x=66:____}{x=68,y=51:min}}";
StringId MiscStrs::ONE_PER_MIN_UNITS_SMALL =
        "{p=8,T,x=76,y=23:1{x=68,Y=1:___}{x=70,y=35:min}}";
StringId MiscStrs::ML_UNITS_LARGE =
        "{p=10,x=73,y=51:mL}";
StringId MiscStrs::ML_UNITS_SMALL =
        "{p=8,T,x=70,y=35:mL}";
StringId MiscStrs::MM_UNITS_LARGE =
        "{p=10,x=73,y=51:mm}";
StringId MiscStrs::MM_UNITS_SMALL =
        "{p=8,T,x=70,y=35:mm}";
StringId MiscStrs::SEC_UNITS_LARGE =
        "{p=10,x=86,y=51:s}";
StringId MiscStrs::SEC_UNITS_SMALL =
        "{p=8,T,x=72,y=35:sec}";
StringId MiscStrs::KG_UNITS_SMALL =
        "{p=10,T,x=90,y=33:kg}";
StringId MiscStrs::TC_ML_UNITS_SMALL =
        "{p=8,T,x=105,y=31:mL}";
StringId MiscStrs::TC_MM_UNITS_SMALL =
        "{p=8,T,x=75,y=31:mm}";
StringId MiscStrs::TC_MM_UNITS2_SMALL =
        "{p=8,T,x=105,y=31:mm}";
StringId MiscStrs::CM_H20_UNITS_LARGE =
        "{p=10,x=68,y=51:H{S:2}O{X=-23,Y=-12:cm}}";
StringId MiscStrs::CM_H20_UNITS_SMALL =
        "{p=8,T,x=70,y=34:H{S:2}O{X=-17,Y=-9:cm}}";
StringId MiscStrs::HPA_UNITS_LARGE =
        "{p=10,x=68,y=51:hPa}";
StringId MiscStrs::HPA_UNITS_SMALL =
        "{p=8,T,x=70,y=35:hPa}";
StringId MiscStrs::PERCENT_UNITS_LARGE =
        "{p=12,x=77,y=51:%}";
StringId MiscStrs::PERCENT_UNITS_SMALL =
        "{p=10,T,x=75,y=35:%}";
StringId MiscStrs::PERCENT_UNITS_SMALL_MORE_SETTINGS_SUB_SCREEN =
        "{p=8,T,x=91,y=35:%}";
StringId MiscStrs::MMHG_UNITS_SMALL =
        "{p=8,T,x=130,y=35:mmHg}";

StringId MiscStrs::TIME_DATE_TITLE_FORMAT =
        "{p=12,y=14:%T     %W}";
StringId MiscStrs::TIME_DATE_PANEL_DATA_FORMAT =
        "{p=10,y=14:%W   %H:%M:%S}";
StringId MiscStrs::LOG_DATE_FORMAT =
        "%P";

// Maneuver panel
StringId MiscStrs::INSP_PAUSE_MANEUVER_TITLE =
        "{p=12,y=12:Inspiratory Pause Maneuver}";
StringId MiscStrs::EXP_PAUSE_MANEUVER_TITLE =
        "{p=12,y=12:Expiratory Pause Maneuver}";
StringId MiscStrs::NIF_MANEUVER_TITLE =
        "{p=12,y=12:NIF Maneuver}";
StringId MiscStrs::P100_MANEUVER_TITLE =
        "{p=12,y=12:P{S:0.1} Maneuver}";
StringId MiscStrs::VITAL_CAPACITY_MANEUVER_TITLE =
        "{p=12,y=12:Vital Capacity Maneuver}";
StringId MiscStrs::PAUSE_PENDING_MSG =
        "{p=10,y=10:Wait for pause to begin}";
StringId MiscStrs::PAUSE_ACTIVE_MSG =
        "{p=10,y=10:Pause active}";
StringId MiscStrs::PAUSE_DONE_MSG =
        "{p=10,y=10:Pause completed}";
StringId MiscStrs::PAUSE_CANCEL_MSG =
        "{p=10,y=10:Pause cancelled}";
StringId MiscStrs::PAUSE_REJECT_MSG =
        "{p=10,y=10:Maneuver cancelled}";
StringId MiscStrs::MANEUVER_NOT_ALLOWED =
        "{p=10,y=10:Maneuver not allowed}";
StringId MiscStrs::END_PAUSE_MSG =
        "{p=10,y=10:To end maneuver: release key}";
StringId MiscStrs::CANCEL_MANEUVER_BUTTON_TITLE =
        "{p=10,y=16:CANCEL}";

        // Respiratory Mechanics Sub-Screen Titles
StringId MiscStrs::NIF_SUBSCREEN_TITLE =
        "{p=14,y=18:NIF Maneuver}";
StringId MiscStrs::P100_SUBSCREEN_TITLE =
        "{p=14,y=18:P{S:0.1} Maneuver}";
StringId MiscStrs::VITAL_CAPACITY_SUBSCREEN_TITLE =
        "{p=14,y=18:Vital Capacity Maneuver}";

StringId MiscStrs::START_MANEUVER_BUTTON_TITLE =
        "{p=10,y=16:START}";
StringId MiscStrs::NIF_START_MANEUVER_BUTTON_TITLE =
        "{p=10,y=20:START}";
StringId MiscStrs::REJECT_MANEUVER_DATA_BUTTON_TITLE =
        "{p=10,y=16:Reject}";
StringId MiscStrs::ACCEPT_MANEUVER_DATA_BUTTON_TITLE =
        "{p=10,y=16:Accept}";
StringId MiscStrs::NIF_START_MSG =
        "{p=10,y=10:To begin: TOUCH and HOLD START button}";
StringId MiscStrs::NIF_REJECTED_MSG =
        "{p=10,y=10:REJECTED To begin: TOUCH and{x=50,Y=15:HOLD START button}}";
StringId MiscStrs::P100_START_MSG =
        "{p=10,y=10:To begin: TOUCH START button}";
StringId MiscStrs::P100_REJECTED_MSG =
        "{p=10,y=10:REJECTED To begin: TOUCH START button}";
StringId MiscStrs::VITAL_CAPACITY_START_MSG =
        "{p=10,y=10:To begin: TOUCH START button}";
StringId MiscStrs::VITAL_CAPACITY_REJECTED_MSG =
        "{p=10,y=10:REJECTED To begin: TOUCH START button}";
StringId MiscStrs::NIF_RELEASE_BUTTON_MSG =
        "{p=10,y=10:Maneuver canceled: Release the{x=50,Y=15:START button }}";


// AlarmSilence panel
StringId MiscStrs::ALARM_SILENCE_TITLE =
        "{p=12,y=11:Alarm Silence In Progress}";

// Hundred percent O2 panel
StringId MiscStrs::PERCENT_O2_NEO_TITLE =
        "{p=12,y=11:+20%% - Delivering %d%% O{S:2} }";
StringId MiscStrs::HUNDRED_PERCENT_O2_NEO_TITLE =
        "{p=12,y=11:+20% - 100% O{S:2} / CAL In Progress}";
StringId MiscStrs::HUNDRED_PERCENT_O2_TITLE =
        "{p=12,y=11:100% O{S:2} / CAL In Progress}";
StringId MiscStrs::HUNDRED_PERCENT_O2_TIME_UNKNOWN =
        "{p=12,y=40,x=60:Remaining time unknown}";

// used as an auxillary title for the 'Pi' and 'Psupp' buttons...
StringId MiscStrs::ABOVE_PEEP_AUX_TEXT_LARGE =
        "{p=8,y=9:ABOVE{X=-26,Y=9:PEEP}}";
StringId MiscStrs::ABOVE_PEEP_AUX_TEXT_SMALL =
        "{p=6,y=6:ABOVE{X=-28,Y=7:PEEP}}";

StringId MiscStrs::SETTING_LIMIT_OFF =
        "{p=%d,y=%d,%c:OFF}";

// text for High Spontaneous Inspiratory Time
StringId MiscStrs::HIGH_SPONT_INSP_TIME_BUTTON_TITLE_LARGE =
        "{p=18,y=20:\013T{S:I SPONT}}";
StringId MiscStrs::HIGH_SPONT_INSP_TIME_BUTTON_TITLE_SMALL =
        "{p=10,y=11:\013T{S:I SPONT}}";
StringId MiscStrs::HIGH_SPONT_INSP_TIME_HELP_MESSAGE =
        "{p=12,y=17:\013T{S:I SPONT}{T: = High spontaneous inspiratory time limit}}";


StringId MiscStrs::TOUCHABLE_DYNAMIC_COMPLIANCE_LABEL =
        "{p=18,y=29:C{S:DYN}}";
StringId MiscStrs::TOUCHABLE_DYNAMIC_COMPLIANCE_MSG =
        "{p=14,y=17:C{S:DYN} {T: = Dynamic Compliance}}";
StringId MiscStrs::TOUCHABLE_DYNAMIC_COMPLIANCE_UNIT =
        "{p=10,x=16,y=15:mL{x=9:____}{x=0,y=29:cmH{S:2}O}}";
StringId MiscStrs::TOUCHABLE_DYNAMIC_COMPLIANCE_HPA_UNIT =
	"{p=10,x=7,x=6,y=15:mL{x=0:____}{x=0,y=29:hPa}}";

StringId MiscStrs::TOUCHABLE_DYNAMIC_RESISTANCE_LABEL =
        "{p=18,y=29:R{S:DYN}}";
StringId MiscStrs::TOUCHABLE_DYNAMIC_RESISTANCE_MSG =
        "{p=14,y=17:R{S:DYN} {T: = Dynamic Resistance}}";
StringId MiscStrs::TOUCHABLE_DYNAMIC_RESISTANCE_UNIT =
        "{p=10,x=0,y=15:cmH{S:2}O{x=0,Y=2:_______}{x=16,y=32:L/s}}";
StringId MiscStrs::TOUCHABLE_DYNAMIC_RESISTANCE_HPA_UNIT =
        "{p=10,x=1,y=15:hPa{x=0:____}{x=5,y=29:L/s}}";



StringId MiscStrs::TOUCHABLE_PEAK_SPONT_FLOW_LABEL =
        "{p=18,y=29:PSF}";
StringId MiscStrs::TOUCHABLE_PEAK_SPONT_FLOW_MSG =
        "{p=14,y=17:PSF {T: = Peak Spontaneous Flow}}";
StringId MiscStrs::TOUCHABLE_PEAK_SPONT_FLOW_UNIT =
        "{p=10,x=10,y=14:L{x=0:____}{x=2,y=29:min}}";

StringId MiscStrs::TOUCHABLE_PEAK_EXP_FLOW_LABEL =
        "{p=18,y=29:PEF}";
StringId MiscStrs::TOUCHABLE_PEAK_EXP_FLOW_MSG =
        "{p=14,y=17:PEF {T: = Peak Expiratory Flow}}";
StringId MiscStrs::TOUCHABLE_PEAK_EXP_FLOW_UNIT =
        "{p=10,x=10,y=14:L{x=0:____}{x=2,y=29:min}}";

StringId MiscStrs::TOUCHABLE_END_EXP_FLOW_LABEL =
        "{p=18,y=29:EEF}";
StringId MiscStrs::TOUCHABLE_END_EXP_FLOW_MSG =
        "{p=14,y=17:EEF {T: = End Expiratory Flow}}";
StringId MiscStrs::TOUCHABLE_END_EXP_FLOW_UNIT =
        "{p=10,x=10,y=14:L{x=0:____}{x=2,y=29:min}}";

StringId MiscStrs::TOUCHABLE_RM_NOT_AVAILABLE_TITLE =
        "{p=14,x=0,y=0:Not available for NIV or BILEVEL}";
StringId MiscStrs::TOUCHABLE_RM_NOT_AVAILABLE_MSG =
        "{p=10,T,y=17:Respiratory Mechanics is not available for NIV or BILEVEL.}";
StringId MiscStrs::RM_NOT_AVAILABLE_SMALL_MSG =
        "{p=8,y=15:Not available for NIV, NEONATAL or BILEVEL.}";

StringId MiscStrs::RM_BUTTON_TITLE =
        "{p=10,y=13:Respiratory{x=2,Y=14:Mechanics}}";
StringId MiscStrs::RM_SMALL_BUTTON_TITLE =
        "{p=8,y=10:RM}";

StringId MiscStrs::BACK_BUTTON_TITLE =
        "{p=10,y=21:STANDARD}";

StringId MiscStrs::RM_PAUSE_PENDING_MSG = 
        "{p=10,y=10:Wait for maneuver to begin}";

StringId MiscStrs::RM_PAUSE_ACTIVE_MSG = 
        "{p=10,y=10:Maneuver active}";




StringId MiscStrs::LEAK_COMP_BUTTON_TITLE =
        "{p=10,y=12,T:Leak Compensation}";
StringId MiscStrs::LEAK_COMP_HELP_MESSAGE =
        "{p=14,y=17,T:Leak Compensation Enable/Disable}";

StringId MiscStrs::LEAK_COMP_ENABLE_VALUE =
        "{p=%d,y=%d,%c:Enabled}";
StringId MiscStrs::LEAK_COMP_DISABLE_VALUE =
        "{p=%d,y=%d,%c:Disabled}";
StringId MiscStrs::TOUCHABLE_PERCENT_LEAK_LABEL =
        "{p=14,y=29:%LEAK}";
StringId MiscStrs::TOUCHABLE_PERCENT_LEAK_MSG =
        "{p=14,y=17:Percent Inspiratory Leak}";
StringId MiscStrs::TOUCHABLE_PERCENT_LEAK_UNIT =
        "{p=10,y=29:%}";

StringId MiscStrs::LEAK_COMP_MSA_ENABLE_TITLE =
        "{p=18,x=35,y=20:LC}";


StringId MiscStrs::LEAK_COMP_SMALL_BUTTON_TITLE =
        "{p=8,y=10:LC}";

StringId MiscStrs::LEAK_COMP_AND_RM_SMALL_BUTTON_TITLE =
        "{p=8,x=0,y=10:LC{X=40:RM}}";

StringId MiscStrs::DISCO_L_PER_MIN_UNITS_SMALL = 
        "{p=8,T,x=105,y=25:L{x=97,Y=1:___}{x=99,y=36:min}}";
StringId MiscStrs::DISCONNECTION_SENSITIVITY_L_PER_MIN_HELP_MESSAGE =
        "{p=14,y=17:D{S:SENS}{T: = Disconnect sensitivity (L/min)}}";


StringId MiscStrs::TOUCHABLE_INSP_TIDAL_VOL_LEAK_LABEL =
        "{p=18,y=29:V{S:TL}}";
StringId MiscStrs::TOUCHABLE_INSP_TIDAL_VOL_LEAK_MSG =
        "{p=14,y=17:V{S:TL}{T: = Estimated Inspiratory Volume}}";

StringId MiscStrs::TOUCHABLE_EXH_LEAK_RATE_LABEL =
        "{p=14,y=29:LEAK{Y=11,x=12,p=8:@ PEEP}}";
StringId MiscStrs::TOUCHABLE_EXH_LEAK_RATE_MSG =
        "{p=14,y=17:Leak Rate at PEEP}";
StringId MiscStrs::TOUCHABLE_EXH_LEAK_RATE_UNIT =
        "{p=10,y=29:L/min}";
        
StringId MiscStrs::LEAK_DETECTED_MSG =
        "{p=12,x=3,y=17:Leak Detected}";

StringId MiscStrs::MODE_MSA_BILEVEL_SMALL_TITLE =
        "{p=14,x=20,y=20:BILEVEL}";
StringId MiscStrs::VENT_TYPE_MSA_NIV_SMALL_TITLE =
        "{p=14,x=1,y=20:N}";

StringId MiscStrs::TOUCHABLE_INSP_LEAK_VOL_LABEL =
        "{p=18,y=29:V{S:LEAK}}";
StringId MiscStrs::TOUCHABLE_INSP_LEAK_VOL_MSG =
        "{p=14,y=17:V{S:LEAK} {T: = Inspiratory Leak Volume}}";
StringId MiscStrs::TOUCHABLE_INSP_LEAK_VOL_UNIT =
        "{p=10,y=29:mL}";


// title for the Software Options subscreen...
StringId MiscStrs::SOFTWARE_OPTIONS_SUBSCREEN_TITLE =
        "{p=14,y=18:Software Options}";
StringId MiscStrs::SOFTWARE_OPTIONS_BUTTON_LABEL =
        "{p=10,x=0,y=17:Software Options}";
StringId MiscStrs::CLEAR_ALL_BUTTON_TITLE =
        "{p=10,y=21:Clear All}";
StringId MiscStrs::SET_ALL_BUTTON_TITLE = 
        "{p=10,y=21:Set All}";

StringId MiscStrs::USE_DATAKEY_BUTTON_TITLE = 
        "{p=8,x=4,y=15:Use Datakey{p=8,x=19,Y=10:Option}}";

// Single Test in EST
StringId MiscStrs::SINGLE_START_TEST_TAB_BUTTON_LABEL =
        "{p=10,y=18:SINGLE TEST}";
StringId MiscStrs::SINGLE_TEST_EST_SUBSCREEN_TITLE =
        "{p=14,y=18:Single Test EST}";
StringId MiscStrs::ALL_TEST_TAB_BUTTON_LABEL =
        "{p=10,y=18:ALL TESTS}";




StringId MiscStrs::PROX_ENABLED_BUTTON_TITLE =
        "{p=10,y=12,T:Prox}";
StringId MiscStrs::PROX_ENABLED_HELP_MESSAGE =
        "{p=14,y=17,T:Prox Enable/Disable}";

StringId MiscStrs::PROX_ENABLE_VALUE =
        "{p=%d,y=%d,%c:Enabled}";
StringId MiscStrs::PROX_DISABLED_VALUE =
        "{p=%d,y=%d,%c:Disabled}";


StringId MiscStrs::CURRENT_PROX_SETUP_SUBSCREEN_TITLE =
        "{p=14,y=18:Current Prox Settings}";
StringId MiscStrs::PROPOSED_PROX_SETUP_SUBSCREEN_TITLE =
        "{p=14,y=18,I:Proposed {N:Prox Settings}}";

StringId MiscStrs::PROX_MANUAL_PURGE_START_MSG =
        "{p=10,y=10:To begin: TOUCH START button}";
StringId MiscStrs::PROX_START_MANUAL_BUTTON_TITLE =
        "{p=10,y=20:START}";

StringId MiscStrs::PROX_MANUAL_TITLE =
        "{p=12,y=12:Prox Manual Purge}";
StringId MiscStrs::PROX_SETUP_BUTTON_TITLE =
        "{p=10,y=23:Prox Setup}";
StringId MiscStrs::PROX_MANUAL_PROGRESS_TITLE =
        "{p=12,y=12:Prox Manual Purge In Progress}";

StringId MiscStrs::PROX_AUTO_ZERO_STATUS_MSG =
        "{p=14,x=5,y=18,T:Prox autozero in progress.}";
StringId MiscStrs::PROX_PURGE_STATUS_MSG =
        "{p=14,x=5,y=18,T:Prox purge in progress.}";

StringId MiscStrs::PROX_TOUCHABLE_INSP_TIDAL_VOL_LABEL =
        "{p=18,y=29:V{S:TI}{S:-{c=12,N:Y}}}";
StringId MiscStrs::PROX_TOUCHABLE_INSP_TIDAL_VOL_MSG =
        "{p=14,y=17:V{S:TI}{S:-{c=12,N:Y}}{T: = Inspired tidal volume}}";

StringId MiscStrs::PROX_TOUCHABLE_INSP_TIDAL_VOL_LEAK_LABEL =
        "{p=18,y=29:V{S:TL}{S:-{c=12,N:Y}}}";
StringId MiscStrs::PROX_TOUCHABLE_INSP_TIDAL_VOL_LEAK_MSG =
        "{p=14,y=17:V{S:TL}{S:-{c=12,N:Y}}{T: = Estimated Inspiratory Volume}}";

StringId MiscStrs::LEAK_COMP_AND_RM_AND_PROX_SMALL_BUTTON_TITLE =
        "{p=8,x=0,y=10:LC{X=40:RM}{X=-17,Y=23:PF}}";
StringId MiscStrs::LEAK_COMP_AND_PROX_SMALL_BUTTON_TITLE =
        "{p=8,x=0,y=10:LC{X=44,Y=23:PF}}";
StringId MiscStrs::RM_AND_PROX_SMALL_BUTTON_TITLE =
        "{p=8,y=10:RM{X=-17,Y=23:PF}}";
StringId MiscStrs::PROX_SMALL_BUTTON_TITLE =
        "{p=8,y=33:PF}";


StringId MiscStrs::SV_PX_COMM_ERROR = "Comm. Error";
StringId MiscStrs::SV_PX_SENSOR_ERROR = "Invalid Sensor";
StringId MiscStrs::SV_PX_DEMO_MODE_ERROR = "Demo Mode Square Wave Failed";
StringId MiscStrs::SV_PX_BAROMETIC_PRESS_CROSS_CHK_ERROR = "P-atm Cross Check Failed";
StringId MiscStrs::SV_PX_AUTO_ZERO_AND_LEAK_ERROR = "Autozero and Leak Test Failed";
StringId MiscStrs::SV_PX_PURGE_ERROR = "Purge Error";
StringId MiscStrs::SV_PX_PRESS_CROSS_CHK_ERROR = "Pressure Cross Check Failed";
StringId MiscStrs::SV_PX_FLOW_SENSOR_CROSS_CHK_ERROR = "Flow Sensor Cross Check Failed";
StringId MiscStrs::PX_PROX_TEST_SKIPPED = "Operator Skipped Prox Test";

StringId MiscStrs::MMHG_UNIT_LABEL =
        "{p=8,x=20,y=11:mmHg}";
StringId MiscStrs::PX_PURGE_TEST_RESERVOIR_PRESSURE_AT_THREE_PSI_TITLE =
        "{p=8,y=11:2) Accumulator Pressure}";

StringId MiscStrs::PX_PURGE_TEST_MIN_RESERVOIR_PRESSURE_AT_SIX_PSI_TITLE =
        "{p=8,y=11:5) Accumulator Pressure}";

StringId MiscStrs::PX_PURGE_TEST_MIN_RESERVOIR_PRESSURE_AT_THREE_PSI_TITLE =
        "{p=8,y=11:3) Accumulator Pressure}";

StringId MiscStrs::PX_PURGE_TEST_RESERVOIR_PRESSURE_AT_SIX_PSI_TITLE =
        "{p=8,y=11:4) Accumulator Pressure}";

StringId MiscStrs::PX_PURGE_TEST_ACCUMATOR_PRESSURE_AT_ZERO_TITLE =
        "{p=8,y=11:1) Accumulator Pressure}";

StringId MiscStrs::PSI_UNIT_LABEL =
        "{p=8,x=20,y=11:psi}";
StringId MiscStrs::LPM_UNIT_LABEL =
        "{p=8,x=20,y=11:L/min}";


StringId MiscStrs::PX_PROX_INSPIRATORY_FLOW_AT_TWENTY_LPM_TITLE =
        "{p=8,y=11:Prox Flow Sensor @ 20 L/min}";
StringId MiscStrs::PX_PROX_INSPIRATORY_FLOW_AT_FIVE_LPM_TITLE =
        "{p=8,y=11:Prox Flow Sensor @ 5 L/min}";
StringId MiscStrs::PX_PROX_INSPIRATORY_FLOW_AT_ONE_LPM_TITLE =
        "{p=8,y=11:Prox Flow Sensor @ 1 L/min}";

StringId MiscStrs::PX_VENT_INSPIRATORY_FLOW_AT_TWENTY_LPM_TITLE =
        "{p=8,y=11:Vent Flow Sensor @ 20 L/min}";
StringId MiscStrs::PX_VENT_INSPIRATORY_FLOW_AT_FIVE_LPM_TITLE =
        "{p=8,y=11:Vent Flow Sensor @ 5 L/min}";
StringId MiscStrs::PX_VENT_INSPIRATORY_FLOW_AT_ONE_LPM_TITLE =
        "{p=8,y=11:Vent Flow Sensor @ 1 L/min}";

StringId MiscStrs::PX_PROX_INSPIRATORY_FLOW_AT_ZERO_LPM_TITLE =
        "{p=8,y=11:Prox Flow Sensor @ 0 L/min}";

StringId MiscStrs::PROX_ERROR_MESSAGE_TITLE1 =
        "{p=12,y=12:Prox unavailable because}";

StringId MiscStrs::PROX_ERROR_MESSAGE_TITLE2 =
        "{p=12,y=12:SST Prox test did not pass}";

StringId MiscStrs::PROX_NOT_AVAILABLE_MSG =
        "{p=12,y=12:Not available for NIV or BILEVEL}";

StringId MiscStrs::PROX_STARTUP_SMALL_MSG =
        "{p=14,x=5,y=18,T:PROX STARTUP}";
StringId MiscStrs::PROX_STARTUP_LARGE_MSG =
        "{p=18,c=13,y=26:PROX STARTUP}";
StringId MiscStrs::PROX_TOUCHABLE_SPONT_FV_RATIO_LABEL =
        "{p=18,y=29:f/V{S:T}{S:-{c=12,N:Y}}}";
StringId MiscStrs::PROX_TOUCHABLE_SPONT_FV_RATIO_MSG =
        "{p=14,y=17:f/V{S:T}{S:-{c=12,N:Y}}{T: = Rapid/shallow breathing index}}";
StringId MiscStrs::PROX_TOUCHABLE_SPONT_MINUTE_VOL_LABEL =
        "{p=18,y=29:\001{S:E SPONT}{S:-{c=12,N:Y}}}";
StringId MiscStrs::PROX_TOUCHABLE_SPONT_MINUTE_VOL_MSG =
        "{p=14,y=17:\001{S:E SPONT}{S:-{c=12,N:Y}}{T: = Exhaled spont minute volume}}";

#endif //defined(SIGMA_GUI_CPU)
