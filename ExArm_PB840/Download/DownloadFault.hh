#ifndef DownloadFault_HH
#define DownloadFault_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1996, Nellcor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  DownloadFault - Fault-Handler for the Download subsystem.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/DownloadFault.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $ 
//@ Modification-Log
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//   Revision: 001   By: jhv   Date: 24-Jan-1996   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//====================================================================

#include "DownloadTypes.hh"
#include "DownloadClassIds.hh"
#include "DownloadFaultMacros.hh"

enum FaultType
{
	PRE_CONDITION,
	ASSERTION
};

class DownloadFault
{
  public:

    static void  SoftFault(const FaultType softFaultId,
			   const SubSystemID subSystemId,
			   const Uint32      moduleId,
			   const Uint32      lineNumber,
			   const char*       pFileName = NULL,
			   const char*       pBoolTest = NULL);
  private:
    DownloadFault(const DownloadFault&);		// not implemented...
    DownloadFault(void);				// not implemented...
    ~DownloadFault(void);			// not implemented...
    void  operator=(const DownloadFault&);	// not implemented...
};


#endif // DownloadFault_HH 
