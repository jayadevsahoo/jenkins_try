
;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================


;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: kerdram.s - Kernel DRAM Tests
;
;---------------------------------------------------------------------
;@ Interface-Description
;
;  Free Functions: 
;     DRAMRefreshTimerTest - tests the the DRAM refresh time is running
;     kernelDRAMTest       - performs tests on the portion of DRAM used by
;                            the kernel POST
;     FastExchangeRAMTest  - performs the Fast Exchange test on the
;                            portion of DRAM not tested in the Kernel test
;
;---------------------------------------------------------------------
;@ Rationale
;    This verifies just enough memory to allow POST to continue testing.
;---------------------------------------------------------------------
;@ Implementation-Description
;
;    This module contains functions to test enough of RAM to run the rest 
;    of POST, 64K of RAM. It also does the complete address line test, 
;    data bus test, and odd offset addressing test.  The ability to access the
;    entire DRAM address space is tested in the this module.  Later DRAM tests 
;    do not need to repeat this function.     
;
;    At this point in POST DRAM may not be in an initialized state.  Memory
;    must be written using patterns that will not generate a parity
;    error.  Likewise reading memory before an initial value has been set
;    may generate a parity error.  The Kernel DRAM test sets RAM to a
;    known pattern during its test.
;
;    The Kernel DRAM block is used subsequently by POST as its stack.  This
;    allows non-POST functions to be called without violating the
;    pretesting rules.   
;    
;    Since the cell integrity test will overwrite all of kernel memory, the
;    test will be designed to use no other memory than what is being
;    tested.  All variables will be kept in registers, and conventional
;    sub-routine calls are not permitted, since they use the stack. 
;
;---------------------------------------------------------------------
;@ Fault-Handling
;    A failure of any test in this file is considered a Major Fault and
;    causes an immediate suspension of testing.  The processor is put
;    into a state from which it cannot exit without a power cycle or reset.
;---------------------------------------------------------------------
;  Restrictions
;    The 68040 data cache controller must be OFF for all memory tests, so
;    as to insure that all memory accesses result in real accesses.  The 
;    instruction cache is turned ON to increase time performance for 
;    this test, implicitly testing the instruction cache.
;---------------------------------------------------------------------
;@ Invariants
;    None
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
; @(#) $Header:   /840/Baseline/Kernel/vcssrc/kerdram.s_v   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
;
;@ Modification-Log
;
;
;  Revision: 004    By: Gary Cederquist  Date: 21-AUG-1997  DR Number: 2361
;    Project: Sigma (R8027)
;    Description:
;       Moved requirement 11034 from the Interrupt Controller Test which
;       was eliminated to the DRAMRefreshTimerTest.
;
;  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
;    Project: Sigma (R8027)
;    Description:
;       Added testable item annotations.
;
;  Revision: 002    By: Gary Cederquist  Date: 16-MAY-1997  DR Number: 2121
;    Project: Sigma (R8027)
;    Description:
;       Removed assembler options from source, using command line instead.
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================

;------------------------------------------------------------------------
;------------- Unit Test Definitions  -----------------------------------
;------------------------------------------------------------------------
                XDEF        DRAMRefreshTimerTest
                XDEF        KernelDramTest
                XREF        CellIntegrityTest
                SECTION     code,,C

               INCLUDE  kernel.inc

;--------- Data for the Cell Integrity Test  -------------------------
         ALIGN 16
Pattern: DCB.L 8,$ffffffff             ;initial memory fill pattern
regList: REG   d2/d3/d4/d5/d6/a1/a2/a5 ;list of registers used in movem

;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: DRAMRefreshTimerTest
;
;@ Interface-Description
;    Verfies operation of the MC68901 timer used for memory refresh.
;
;    INPUTS:  a6.l - return address
;
;    OUTPUTS: d7.l = PASSED, MAJOR
;
;  $[11034]
;---------------------------------------------------------------------
;@ Implementation-Description
;    A countdown verification of the MC68901 timer is
;    performed. The 901 is programmed to countdown at 16us intervals.   
;    The device is monitored to determine if it is counting.  Using the 8mhz
;    input clock of the 901, this is 120 counts.
;    
;    The accuracy of this timer is tested indirectly when the other timers of
;    the system are compared to an external oscillator as described in the
;    previous section.  If this timer refreshes slower than 15 microseconds
;    a potential loss  of data in DRAM is possible.   The subsequent DRAM
;    tests, safety net of software exceptions, and parity checks of DRAM are
;    designed to catch this failure.
;
;    A non-functional 901 controller will fail the countdown verification.
;    A slow 901 timer will cause parity errors.  DRAM refreshes faster 
;    than expected are acceptable.
;    
;    In the best case scenario, 3 instruction cycles of the 24mHz CPU clock
;    should be adaquate for the 901 to count a single count.  
;
;    Timing has been verified using the AMC emulator.  The time between
;    the reads is 900ns which is more than adequate to detect a change
;    in the 901 counter with a frequency of 8Mhz (125ns).
;
;---------------------------------------------------------------------
;  REGISTER USAGE
;
;  Register Modified: d0,d7,a0
;---------------------------------------------------------------------
;@ PreCondition
;---------------------------------------------------------------------
;@ PostCondition
;    none
;@ End-Free-Function
;=====================================================================
; void DRAMRefreshTimerTest() {
DRAMRefreshTimerTest:
;************************************************
;**** test that the DRAM counter is counting ****
;************************************************

                                            UNIT_TEST_POINT POSTUT_10_1
                  move.l   #M68901_TADR,a0    ; pointer to time count
                  nop                         ; flush pipeline to assure read
                                              ; occurs after write to 901
                  move.b   (a0),d0            ; get starting value
                  nop                         ; delay
                  cmp.b    (a0),d0            ; 900ns later... changed?
                                            UNIT_TEST_POINT POSTUT_10_2
                  beq      testFailed         ; no   // $[TI1.1]
                  bra      testPassed         ; yes  // $[TI1.2]
; }

;============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
;@ Free-Function: KernelDRAMTest      
;
;@ Interface-Description
;    Performs a 4 step verification of a small amount of DRAM (64K) used
;    by the Kernel Stack.  All Data lines are tested.  The address lines
;    in the entire (4 MByte) DRAM are tested.  The ability to access each byte
;    of a long word is tested.  A test of each memory location in the 
;    portion of the DRAM used by the KERNAL is performed.
;
;    INPUTS:  a6.l - return address
;
;    OUTPUTS: d7.l = PASSED, MAJOR

;---------------------------------------------------------------------
;@ Implementation-Description
;
;    The DRAM test is implemented in the following subfunctions:
;
;    Data Bus Test
;       This tests the data bus by marching zeros and ones across a given 
;       longword in the memory under test. Opens and shorts in the data lines
;       are designed to be detected.  The test consists of two loops. The
;       first loop marches a 1 (all other bits are 0) from bit 0 upward
;       through bit 31.  The second marches a 0 (all other bits are 1) from
;       bit zero upward through bit 31.  $[11012]a
;
;    Odd Offset Address
;       This tests the ability to read and write bytes, words and longwords
;       at all possible mod 4 offsets from a given address in memory. It
;       tests the ability to decode and fetch/write items at odd addresses.
;
;       This test makes sure that the processor correctly gets all of the
;       bytes of the item addressed, and places them correctly in the result.
;       It operates by writing and reading back at a set of 4  addresses
;       that includes all possible modulo 4 values. The longword values
;       written to these addresses contain a different value in each of
;       their bytes, (such as 0x01020304). This is so errors that result in
;       writing or reading the bytes in the wrong order are caught. A
;       similar test is done for 16 bit words. This test will access the
;       16 bit word located at offsets 0,1,2,3 from the start of two
;       consecutive long words initialized with 0x01020304, and
;       0x05060708, respectively. The byte test will access each byte of the
;       first longword. To test correct write access, for each of these
;       tests, the chosen word or byte will be written, and then entire
;       longword(s) affected will be read and checked for correctness.
;
;    Address Bus Test
;       This tests for opens or shorts in the address bus. The test operates
;       by special testing on bytes whose addresses contain exactly one bit
;       set. Accessing these "address line" bytes A=2**N N=0-21 (4MB) toggles
;       exactly one address line.  This test verifies each DRAM IC in this
;       manner with a write-read verification. $[11012]b $[11039]a
;
;       For each A, the contents of the address are zeroed and checked for
;       zero.  All the other addresses used in the test will be checked to
;       see that they are still 0xFF.  The address at A will then be reset
;       to 0xFF.  This design accesses each individual RAM IC. During the
;       Kernel Stack test, N=0 to 15 would be sufficient to test 64K, but adding
;       6 more iterations allows the full range to be tested without
;       splitting this test into two parts.  The added time is
;       inconsequential (10us).
;
;         Warning(s):  This test will report Failure if N_DRAM_ADR_BITS > 31
;                      For performance considerations, it is assumed that
;                      address bit 31 (N_DRAM_ADR_BITS = 32) is not tested.
;
;
;    Cell Integrity Test
;       This tests the ability of each longword to hold all ones and all
;       zeros. This is the most time consuming part of the memory test, so
;       an attempt has been made to optimize it for speed.  Each long word
;       in the block is written with an initial pattern.  That pattern is
;       read back and verified.  The memory is set to the bitwise inverse
;       of the initial pattern.  That value is then read back and verified.
;       $[11012]c
;
;       The initial pattern chosen produces memory with all 0's at the
;       completion of the test.  That fulfulls C's requirement that all
;       non-initialized static memory be 0. Therefore the initial pattern
;       is the bitwise inverse of 0 i.e 0xffffffff
;
;        Warning: The code is written to take advantage of the fact that 0's
;                 should be in the memory at the end of test.
;---------------------------------------------------------------------
;  REGISTER USAGE
;
;  Register Modified: d0,d1,d2,d3,d4,d5,d6,d7,a0,a1,a2,a4,a5,a6
;---------------------------------------------------------------------
;
;@ PreCondition
;    none
;---------------------------------------------------------------------
;@ PostCondition
;    none
;@ End-Free-Function
;=====================================================================

;>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Entry Point
; void KernelDramTest() {
KernelDramTest:

;**************************************************************************
;*********************   DRAM Data Bus Test  ******************************
; This tests the data bus by marching zeros and ones across a given
; longword in the memory under test. Opens and shorts in the data lines
; should be detected.                          
;**************************************************************************
                STROBE_WATCHDOG
                move.l  #DRAM_BASE_ADR,A0
                moveq   #1,D0

MarchOnes:      move.l  D0,(A0)
                                             UNIT_TEST_POINT POSTUT_7_1
                cmp.l   (A0),D0
                bne     testFailed           ; // $[TI1.1] failed
                                             ; // $[TI1.2] passed
                asl.l   #1,D0
                bne     MarchOnes            ; // $[TI2.1] more
                                             ; // $[TI2.2] done

                moveq   #1,D0
MarchZeros:     not.l   D0
                move.l  D0,(A0)
                                             UNIT_TEST_POINT POSTUT_7_2
                cmp.l   (A0),D0
                bne     testFailed           ; // $[TI3.1] failed
                                             ; // $[TI3.2] passed
                not.l   D0
                asl.l   #1,D0
                bne     MarchZeros           ; // $[TI4.1] more
                                             ; // $[TI4.2] done

;**************************************************************************
;**************** DRAM Odd Offset Address Test   **************************
; This tests the ability to read and write bytes, words and longwords at
; all possible mod 4 offsets from a given address in memory. It tests the
; ability to decode and fetch/write items at odd addresses.
;
;**************************************************************************
                STROBE_WATCHDOG
                move.l  #DRAM_BASE_ADR,A0

;============================================  Read Test
                move.l  #$01020304,(A0)
                move.l  #$05060708,(4,A0)
;-------------------------------------------- Test byte reads
                                             UNIT_TEST_POINT POSTUT_1_1
                cmpi.b  #$01,(A0)
                bne     testFailed           ; // $[TI5.1] failed
                                             ; // $[TI5.2] passed
                                             UNIT_TEST_POINT POSTUT_1_2
                cmpi.b  #$02,(1,A0)
                bne     testFailed           ; // $[TI6.1] failed
                                             ; // $[TI6.2] passed
                                             UNIT_TEST_POINT POSTUT_1_3
                cmpi.b  #$03,(2,A0)
                bne     testFailed           ; // $[TI7.1] failed
                                             ; // $[TI7.2] passed
                                             UNIT_TEST_POINT POSTUT_1_4
                cmpi.b  #$04,(3,A0)
                bne     testFailed           ; // $[TI8.1] failed
                                             ; // $[TI8.2] passed
;------------------------------------------- Test word reads
                                             UNIT_TEST_POINT POSTUT_2_1
                cmpi.w  #$0102,(A0)
                bne     testFailed           ; // $[TI9.1] failed
                                             ; // $[TI9.2] passed
                                             UNIT_TEST_POINT POSTUT_2_2
                cmpi.w  #$0203,(1,A0)
                bne     testFailed           ; // $[TI10.1] failed
                                             ; // $[TI10.2] passed
                                             UNIT_TEST_POINT POSTUT_2_3
                cmpi.w  #$0304,(2,A0)
                bne     testFailed           ; // $[TI11.1] failed
                                             ; // $[TI11.2] passed
                                             UNIT_TEST_POINT POSTUT_2_4
                cmpi.w  #$0405,(3,A0)
                bne     testFailed           ; // $[TI12.1] failed
                                             ; // $[TI12.2] passed
;------------------------------------------- Test longword reads 
                                             UNIT_TEST_POINT POSTUT_3_1
                cmpi.l  #$01020304,(A0)
                bne     testFailed           ; // $[TI13.1] failed
                                             ; // $[TI13.2] passed
                                             UNIT_TEST_POINT POSTUT_3_2
                cmpi.l  #$02030405,(1,A0)
                bne     testFailed           ; // $[TI14.1] failed
                                             ; // $[TI14.2] passed
                                             UNIT_TEST_POINT POSTUT_3_3
                cmpi.l  #$03040506,(2,A0)
                bne     testFailed           ; // $[TI15.1] failed
                                             ; // $[TI15.2] passed
                                             UNIT_TEST_POINT POSTUT_3_4
                cmpi.l  #$04050607,(3,A0)
                bne     testFailed           ; // $[TI16.1] failed
                                             ; // $[TI16.2] passed
;=========================================== Write Test
;------------------------------------------- Test byte writes 
                move.b  #$09,(A0)
                move.b  #$0A,(1,A0)
                move.b  #$0B,(2,A0)
                move.b  #$0C,(3,A0)
                                             UNIT_TEST_POINT POSTUT_4
                cmpi.l  #$090A0B0C,(A0)
                bne     testFailed           ; // $[TI17.1] failed
                                             ; // $[TI17.2] passed
;------------------------------------------- Test word writes
                move.w  #$0102,(A0)
                                             UNIT_TEST_POINT POSTUT_5_1
                cmpi.l  #$01020B0C,(A0)
                bne     testFailed           ; // $[TI18.1] failed
                                             ; // $[TI18.2] passed
                move.w  #$0304,(1,A0)
                                             UNIT_TEST_POINT POSTUT_5_2
                cmpi.l  #$0103040C,(A0)
                bne     testFailed           ; // $[TI19.1] failed
                                             ; // $[TI19.2] passed
                move.w  #$0A0B,(2,A0)
                                             UNIT_TEST_POINT POSTUT_5_3
                cmpi.l  #$01030A0B,(A0)
                bne     testFailed           ; // $[TI20.1] failed
                                             ; // $[TI20.2] passed
                move.w  #$0C0D,(3,A0)
                                             UNIT_TEST_POINT POSTUT_5_4
                cmpi.l  #$01030A0C,(A0)
                bne     testFailed           ; // $[TI21.1] failed
                                             ; // $[TI21.2] passed
                                             UNIT_TEST_POINT POSTUT_5_5
                cmpi.l  #$0D060708,(4,A0)
                bne     testFailed           ; // $[TI22.1] failed
                                             ; // $[TI22.2] passed
;------------------------------------------- Test longword writes
                move.l  #$01020304,(A0)
                move.l  #$05060708,(4,A0)
                                             UNIT_TEST_POINT POSTUT_6_1
                cmpi.l  #$01020304,(A0)
                bne     testFailed           ; // $[TI23.1] failed
                                             ; // $[TI23.2] passed
                move.l  #$0A0B0C0D,(1,A0)
                                             UNIT_TEST_POINT POSTUT_6_2
                cmpi.l  #$010A0B0C,(A0)
                bne     testFailed           ; // $[TI24.1] failed
                                             ; // $[TI24.2] passed
                nop                          ;to assist unit test
                                             UNIT_TEST_POINT POSTUT_6_3
                cmpi.l  #$0D060708,(4,A0)
                bne     testFailed           ; // $[TI25.1] failed
                                             ; // $[TI25.2] passed
                move.l  #$0E0F1011,(2,A0)
                                             UNIT_TEST_POINT POSTUT_6_4
                cmpi.l  #$010A0E0F,(A0)
                bne     testFailed           ; // $[TI26.1] failed
                                             ; // $[TI26.2] passed
                                             UNIT_TEST_POINT POSTUT_6_5
                cmpi.l  #$10110708,(4,A0)
                bne     testFailed           ; // $[TI27.1] failed
                                             ; // $[TI27.2] passed
                move.l  #$12131415,(3,A0)
                                             UNIT_TEST_POINT POSTUT_6_6
                cmpi.l  #$010A0E12,(A0)
                bne     testFailed           ; // $[TI28.1] failed
                                             ; // $[TI28.2] passed
                                             UNIT_TEST_POINT POSTUT_6_7
                cmpi.l  #$13141508,(4,A0)
                bne     testFailed           ; // $[TI29.1] failed
                                             ; // $[TI29.2] passed


;**************************************************************************
;***************   DRAM Address Bus Test  *********************************
; This tests for opens or shorts in the address bus. The test operates by 
; special testing on bytes whose addresses contain exactly one bit set. 
; Accessing these "address line" bytes toggles exactly one address line.
; 
; The test sets all of the address line bytes to a known value, and then
; changes each in sequence. After the change, the byte in question is 
; checked to see that it wrote, and the other address line bytes are checked
; to see that they did not change. If this passes, the byte that was changed
; is changed back to its original value, and the test is repeated on the 
; next byte. Although this is N-squared, there aren't many address line bytes
; (Can't be more than 31) so the test is really fast.
;
; Warning:  This test will report Failure if
;              N_DRAM_ADR_BITS > MAX_NUMBER_OF_ADDRESS_BITS
;           Setting N_DRAM_ADR_BITS to greater than
;           MAX_NUMBER_OF_ADDRESS_BITS without this test would cause an
;           access failure.

                     move.l   #N_DRAM_ADR_BITS,D0

                     beq      address_test_done
                     cmp.l    #TOO_MANY_ADDRESS_BITS,d0 ;bits > max ?
                     bpl      testFailed                ;report failed if so
                     move.l   #DRAM_BASE_ADR,A0
                     moveq.l  #0,d1
                     bset.l   d0,d1              ;d1 - address bit under test
                     move.l   d1,d0              ;d0 - max address bit
                     move.b   #$ff,d2            ;d2 - test value
address_bit_test_loop:
;--------------- Fill all locations with 0xff  -----------------------
                     STROBE_WATCHDOG
                     move.l   d0,d3
fill_with_ff_loop:   move.b   d2,(0,a0,d3.l)
                     asr.l    #1,d3
                     bne.s    fill_with_ff_loop

;--------------- set the test location to 0 ---------------------------
                     clr.b    (0,a0,d1.l)
                                             UNIT_TEST_POINT POSTUT_8

;------------ go through all the address bits and test the data -------
                     move.l   d0,d3
test_memory_loop:    cmp.l    d3,d1 ;is this the test address?
                     bne.s    not_test_address
                     tst.b    (0,a0,d3.l)
                     bne      testFailed            ;should be 00  // $[TI30.1]
                                                    ;              // $[TI30.2]
                     bra      contine_test_loop
not_test_address:    cmp.b    (0,a0,d3.l),d2
                     bne      testFailed            ;should be ff  // $[TI31.1]
                                                    ;              // $[TI31.2]
contine_test_loop:   asr.l    #1,d3
                     bne.s    test_memory_loop

;------- shift the tested address bit right and continue if not 0 -----
                     asr.l    #1,d1
                     bne      address_bit_test_loop
address_test_done:


;**************************************************************************
;*************************  Kernel Ram Test  ******************************
                move.l  #KRAM_START_ADR,A0
                move.l  #KRAM_BYTES,D0

                jmp     CellIntegrityTest

;**************************************************************************
;************ Test completed - return to POST calling routine  ************
testPassed:     moveq.l   #PASSED,d7
;<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Exit Point
; // $[TI32.1]
                jmp     (a6)

;**************************************************************************
;************ Exit point for Tests that failed  ***************************
testFailed:     moveq.l #MAJOR,D7
;<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Exit Point
; // $[TI32.2]
                jmp     (A6)
; }


                END
