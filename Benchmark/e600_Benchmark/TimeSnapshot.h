#pragma once

class TimeSnapshot
{
	DWORD m_dwFirstTime;
	DWORD m_dwEndTime;

	DWORD m_dwBmpTotal;
	DWORD m_dwBitbltTotal;

	DWORD m_dwBmpRecord;
	DWORD m_dwBitbltRecord;

	static TimeSnapshot* instance;
	TimeSnapshot(void);

public:
	
	~TimeSnapshot(void);

	static TimeSnapshot* getInstance();

	void start();
	void stop();

	void bmpStart();
	void bmpEnd();

	void bitbltStart();
	void bitbltEnd();

};
