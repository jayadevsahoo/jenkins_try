#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@  Class: TftpSocket - Provides functionality of the network
//   socket that supports the TFTP file transfer protocol.
//---------------------------------------------------------------------
//@ Interface-Description
//    The Tftp Socket class provides interfaces to a TCP/IP socket
//    connection between the Sigma's download server and a Sigma target
//	  board. The class provides instance methods to download a boot
//    image file over the Ethernet and burn the image into the Sigma
//    Flash memory. It also provides interface to build a table of
//    checksum values and CRC for a newly downloaded file image.
//---------------------------------------------------------------------
//@ Rationale
//	 This class provides  network communication using the
//   socket library routine interface into the TCP/IP code and
//	 supports TFTP protocol.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Tftp protocol is implemented on top of the TCP/IP network
//   protocol therefore sequencing and data integrity of the download
//   data is guaranteed. The Tftp Server serves more than one clients
//   using multiple TftpSocket connections, but a download client
//   has one TCP/IP socket connection to its TFTP server.
//   Both client and server maintains the current data block concept.
//   The server would not move onto next data block until the client
//   acknowledges that it received the current block.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Some of the repeated socket library errors and download failures
//  are displayed on a LED display as a predefined error code.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/TftpSocket.ccv   25.0.4.0   19 Nov 2013 14:02:52   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  <jhv>    Date:  09-mmm-yy    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "TftpSocket.hh"
#include "Download.hh"
#if	defined(SIGMA_SUN)
#elif	defined(SIGMA_CLIENT)
#include "Queue.hh"
#endif //defined(SIGMA_CLIENT)

static char ErrBuf_[300];

extern "C" void LightDiagnosticLED(Uint8 eCode);

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================
#if	defined(SIGMA_CLIENT)
static Uint Smem_[(sizeof(BufQueue)+sizeof(Uint)-1)/sizeof(Uint)];
BufQueue &TftpSocket::RBufQueue =  *((BufQueue *) Smem_);
#endif //defined(SIGMA_CLIENT);

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize(void) 
//
//@ Interface-Description
//	 Creates the static Buffer Queue manager object.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
TftpSocket::Initialize(void)
{
	// $[TI1]
#if	defined(SIGMA_CLIENT)
	new ( &RBufQueue ) BufQueue();
#endif	//defined(SIGMA_CLIENT)
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TftpSocket(void)  [Default Constructor]
//
//@ Interface-Description
//    Constructor. Leave the socket as un-opened.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TftpSocket::TftpSocket(void) : NetSocket()
{
	// $[TI1]
#if	defined(SIGMA_SERVER)
	clientIPAddr_ = 0;
#elif	defined(SIGMA_CLIENT)
	dataRequest_ = 0;
#endif //defined(SIGMA_CLIENT)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TftpSocket(void)  [Destructor]
//
//@ Interface-Description
//   Default Destructor. Close the socket if it is opened.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 none.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TftpSocket::~TftpSocket(void)
{
	// $[TI1]
}

#if	defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: connectToServer( Uint32 serverIPAddr, PortId tftpServerPort,
//            Uint32 waitDuration )
//
//@ Interface-Description
//   This method establishes a TCP/IP socket
//   connection with the TFTP download server machine, the IP address
//   and port id of the server machine are provided to the routine.
//--------------------------------------------------------------------
//@ Implementation-Description
//    The call to the connect socket library routine is repeated
//    up to 15 times after pausing a given wait duration.
//    This is to provide enough time for the other board to come to
//    the socket connection phase. The socket mode is modified to be
//    a non-blocking while connecting to the other board.
//    The occasional long delay in the blocked mode socket connect
//    was noticed during the development.  In this way, the connect
//    call returns before a complete TCP/IP socket connection is
//    finished and the higher level code and check the status
//    periodically.  In this way, the code has more control over
//    when to stop the connection process if there is too long of 
//    delay in connecting to the other board.
//
//    After a socket connection is made, the socket mode is changed
//    back to the blocking mode.
//
//   
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
TftpSocket::connectToServer( Uint32 serverIPAddr, PortId tftpServerPort, 
		Uint32 waitDuration )
{
Int retry, waitRetry;
Saddr serverAddr;
char i;

	CLASS_ASSERTION(opened());

	::bzero( (char *) &serverAddr.in, sizeof(struct sockaddr_in));
	serverAddr.in.sin_port = tftpServerPort;
	serverAddr.in.sin_family = AF_INET;
	serverAddr.in.sin_addr.s_addr=htonl(serverIPAddr);

#if	defined(SIGMA_TARGET)
	i = 1 ;
	if ( ::ioctl(socketId_, FIONBIO, &i) < 0  ) // $[TI1]
	{ 
		Download::ReportError("TftpSocket::connectToServer():ioctl#1(FIONBIO)", errno);
		return(-1);
	} // else $[TI2]
#endif  //defined(SIGMA_TARGET)

	waitRetry = DOWNLOAD_COMM_RETRY*20; // wait up to a minute
	retry = DOWNLOAD_COMM_RETRY;

	while(::connect(socketId_, &serverAddr.sa, sizeof(struct sockaddr))) // $[TI3]
	{
		if (( errno == EISCONN )) // $[TI3.1]
		{
			//connection is made.
			break;
		} // else $[TI3.2]
		if (( errno == EINPROGRESS ) || ( errno == EALREADY )) // $[TI3.3]
		{ 
			if ( --waitRetry < 0 ) // $[TI3.3.1]
			{ 
				Download::ReportError("TftpSocket::connectToServer():connect() failed errno",
					CONNECT_TO_SERVER_TIMEOUT); 
				return(-1);
			} // else $[TI3.3.2]
		} else if (--retry < 0 ) // $[TI3.4]
		{
			Download::ReportError("TftpSocket::connectToServer():connect() DOWNLOAD_COMM_RETRY", errno);
			return(-1);
		}
		// else $[TI3.5]
		Download::DelayTask((long)waitDuration);
    } // $[TI4]

#if		defined(SIGMA_UNIT_TEST)
printf("connectToServer: connect successful");
#endif //defined(SIGMA_UNIT_TEST)
#if		defined(SIGMA_TARGET)
	i = 0;
	if ( ::ioctl(socketId_, FIONBIO, &i) < 0  ) // $[TI5]
	{
		Download::ReportError("TftpSocket::connectToServer():ioctl#2(FIONBIO)", errno);
		return(-1);
	} // else $[TI6]
#endif  //defined(SIGMA_TARGET)
	return(socketId_);
}
#endif	//defined(SIGMA_CLIENT)

#if	defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: downloadFile(char *fileName )
//
//@ Interface-Description
//    Download a bootfile that contains a complete Sigma software image
//    from a download server and burn the downloaded data to system's
//    flash memory. It builds a checksum table based on the downloaded
//    data and later checks against the downloaded image's built-in checksum
//    table to veridy the data integrity of the downloaded data.
//--------------------------------------------------------------------
//@ Implementation-Description
//   
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
TftpSocket::downloadFile( char *fileName)
{
TftpMsgHeader header;
Uint32 errCode;
Uint32 noWait = 0;
xsFDSet_t readMask;
Boolean forever = TRUE;

	if ( sendRRQ_(fileName) < 0 ) // $[TI1]
	{
		return(FAILURE);
	} // else $[TI2]

	fileBlockNum_ = 1;
	RBufQueue.reInit();
	while(forever)
	{
		FD_ZERO(&readMask);
		FD_SET( getSocketId(), &readMask);

		if ( Download::WaitForInput(&readMask, MAX_DATA_WAIT_INTERVAL ) <= 0 ) // $[TI3]
		{
			if (( state_ ==  DATA_WAITING) && (dataRequest_++ < MAX_DATA_RETRY)) // $[TI3.1]
			{
				sendMsg(OP_DATA_REQUEST, NULL, 0, 0 );
				continue;
			}
			else // $[TI3.2]
			{
				dataRequest_ = 0;
				handleClientError_( SERVER_NOT_SENDING_DATA );
				::LightDiagnosticLED( SERVER_NOT_SENDING_DATA );
				return(FAILURE);
			}
		} // else $[TI4]
		dataRequest_ = 0;
		if (readData(&header, sizeof(header)) != sizeof(header)) // $[TI5]
		{
			handleClientError_(COMM_RECV_ERROR);
			::LightDiagnosticLED(COMM_RECV_ERROR);
			return(FAILURE);
		} // else $[TI6]

		switch(header.opCode)
		{
			case OP_DATA:
				// $[TI7]
				if ( recvDataBlock_(&header) ==  FAILURE ) // $[TI7.1]
				{
					return(FAILURE);
				} // else $[TI7.2]

				if ( errCode =  Download::RFlashMemQueue.pendForMsg(noWait)) // $[TI7.3]
				{
					handleClientError_((DownloadError) errCode);
					return(FAILURE);
				} // else $[TI7.4]
				break;

			case OP_END_OF_FILE:
				// $[TI8]
#if	defined(SIGMA_DEBUG)
				printf("downloadFile : OP_END_FILE \n");
#endif //defined(SIGMA_DEBUG)
				return( finishFlashBurning_());

			case OP_ERROR:
				// $[TI9]
#if	defined(SIGMA_DEBUG)
				printf("downloadFile : OP_ERROR \n");
#endif //defined(SIGMA_DEBUG)
				if ( readData(&errCode, sizeof(Uint32)) != sizeof(Uint32)) // $[TI9.1]
				{
					handleClientError_(COMM_RECV_ERROR);
					::LightDiagnosticLED(COMM_RECV_ERROR);
					return(FAILURE);
				} // else $[TI9.2]
				sprintf(ErrBuf_, "Server sent error code :%D ", errCode ); 
				Download::DisplayClientError(ErrBuf_, DOWNLOAD_SERVER_ERROR ); 
				return(FAILURE);

			default:
				// $[TI10]
#if	defined(SIGMA_DEBUG)
				printf("downloadFile : op %d size %d\n", header.opCode, header.pktSize );
#endif //defined(SIGMA_DEBUG)
				handleClientError_( ILLEGAL_TFTP_OP );
				return(FAILURE);
		}
	} // no [TI] needed
	return(SUCCESS); // Never reach here, just to get rid of a Compiler warning.
}
#endif	//defined(SIGMA_CLIENT)

#if	defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: finishFlashBurning_(void)
//
//@ Interface-Description
//		Make sure that all the data downloaded  are burnt into the
//		flash memory successfully. Afterwards, compare the checksum
//		table built during the download and the host sent checksum
//      table. If they match, the download was successful. Rebuild
//      checksum table based on the flash memory contents and make
//	    sure, the newly computed checksum table also match with the
//	    host sent checksum table. If they match, the flash memory
//		burning process was successful.
//--------------------------------------------------------------------
//@ Implementation-Description
//		There is a separate task which actually writes out the
//		accumulated data blocks to the flash memory. Good portion of
//	    the data block has been already burnt out to the flash memory
//      by the time this routine is called. When the task
//	    finishes writing all the accumulated data successfully or
//	    when it encounters an error while writing to the 
//      flash memory, the Flash memory burner task sends
//	    a message to the Download::RFlashMemQueue Queue.
//   
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
TftpSocket::finishFlashBurning_(void)
{
int maxDelay = 0;
Uint32 msg;

	while(!(msg = Download::RFlashMemQueue.pendForMsg(ONE_SECOND))) // $[TI1]
	{
  		if ( maxDelay++ > MAX_FLASH_BURN_TIME ) // $[TI1.1]
	    {
			handleClientError_( FLASH_BURNING_TIMEOUT );
			return(FAILURE);
		} // else $[TI1.2]
	} // $[TI2]

	if ( msg != FLASH_BURNING_DONE ) // $[TI3]
	{
		handleClientError_( (DownloadError) msg ); 
		return(FAILURE);
	} // else $[TI4]


	if ( sendMsg(OP_DOWNLOAD_SUCCESS, (char *) NULL, 0,0 ) < 0) // $[TI5]
	{
		handleClientError_(COMM_SEND_ERROR );
		::LightDiagnosticLED(COMM_SEND_ERROR );
		return(FAILURE);
	} // else $[TI6]
	return(SUCCESS);
}
#endif	//defined(SIGMA_CLIENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: handleClientError_( DownloadError eCode )
//
//@ Interface-Description
//		Process a client error condition. 
//--------------------------------------------------------------------
//@ Implementation-Description
//    Display an appropriate error message for a given error code.
//	  Then send the error code back to the Server so it can inform
//	  an error message on the server's screen. 
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
TftpSocket::handleClientError_(DownloadError eCode)
{

#if	defined(SIGMA_SERVER)
	Download::DisplayClientError(NULL, eCode);
#elif defined(SIGMA_CLIENT)
	if ( ( eCode != COMM_RECV_ERROR ) && ( eCode != COMM_SEND_ERROR ) ) // $[TI1]
	{
		sendErrMsg_(eCode);
	} // else $[TI2]
#endif //defined(SIGMA_CLIENT)
}


#if	defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: recvDataBlock_( TftpMsgHeader *pHeader )
//
//@ Interface-Description
//	  Receive a data block from the TFTP server and queue the data
//    to the flash data block queue.
//--------------------------------------------------------------------
//@ Implementation-Description
//
//	  Send back an acknowledgement message back to the server when a
//    block of data is successfully received in a correct block number
//    sequence. Add the data block to the flash memory data block queue
//    so it can be burnt into the flash memory.
//    A network buffer is allocated from the free buffer queue, this
//    buffer is freed back to the free list after it is burnt into
//    flash memory by another task.
//    The very first block contains the POST checksum table block, save
//    the block content to the chkSum_ data block.
//
//	  It is possible a previous packet is transmitted more than once
//    based on a wait time out request. In that case, do not queue
//    the received data onto the flash memory queue since the same
//    block of data was received and queued  already.
//--------------------------------------------------------------------
//@ PreCondition
//	 	(pHeader)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
TftpSocket::recvDataBlock_(TftpMsgHeader *pHeader)
{
NetworkBuf *pBuf;
xsFDSet_t readMask;

	CLASS_ASSERTION(opened());
	CLASS_PRE_CONDITION(pHeader);
	
	if ((fileBlockNum_ !=  pHeader->blockNum )&&
		(fileBlockNum_ != ( pHeader->blockNum + 1))) // $[TI1]
	{
		handleClientError_(  OUT_OF_SYNC_COMM_DATA );
		return(FAILURE);
	} // else $[TI2]
		
	if (!(pBuf = RBufQueue.getFreeBuf( ONE_MINUTE ))) // $[TI3]
	{
		handleClientError_( OUT_OF_DOWNLOAD_BUFFER );
		return(FAILURE);
	} // else $[TI4]

	FD_ZERO(&readMask);
	FD_SET( getSocketId(), &readMask);
	while( Download::WaitForInput(&readMask, MAX_DATA_WAIT_INTERVAL ) <= 0 ) // $[TI5]
	{
		if ( dataRequest_++ < MAX_DATA_RETRY ) // $[TI5.1]
		{
			sendMsg(OP_DATA_REQUEST, NULL, 0, 0 );
			continue;
		}
		else // $[TI5.2]
		{
			dataRequest_ = 0;
			handleClientError_( SERVER_NOT_SENDING_DATA );
			::LightDiagnosticLED( SERVER_NOT_SENDING_DATA );
			return(FAILURE);
		}
	} // $[TI6]

	dataRequest_ = 0;
	if ( readData(pBuf->data, pHeader->pktSize) != DATA_BLOCK_SIZE ) // $[TI7]
	{
		handleClientError_(INCOMPATIBLE_DATA_BLOCK_SIZE);
		::LightDiagnosticLED(INCOMPATIBLE_DATA_BLOCK_SIZE);
		return(FAILURE);
	} // else $[TI8]

	if ( fileBlockNum_  == ( pHeader->blockNum + 1)) // $[TI9]
	{
		return(SUCCESS);
	} // else $[TI10]

	if ( sendMsg(OP_DATA_ACK,NULL, 0, fileBlockNum_) < 0 ) // $[TI11]
	{
		handleClientError_( COMM_SEND_ERROR );
		::LightDiagnosticLED( COMM_SEND_ERROR );
		return(FAILURE);
	} // else $[TI12]

	pBuf->data_len = pHeader->pktSize;

	// The first block contains the POST checksum block and save the table..
	if ( fileBlockNum_ == 1 ) // $[TI13]
	{
		bcopy((char *)pBuf->data, (char *)&chkSum_, sizeof(EepromStruct) );
		Download::RFlashMemBurner.reInit(&chkSum_);
	} // else $[TI14]
	RBufQueue.addDataBlock(pBuf);
	state_ = DATA_WAITING;
	fileBlockNum_++;
	return(SUCCESS);
}
#endif //defined(SIGMA_CLIENT)

#if		defined(SIGMA_SERVER)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doCommand(void)
//
//@ Interface-Description
//	  Read a TFTP command from a connected client and perform the
//	  requested command. Returns a negative number when the socket
//    gets closed, otherwise it returns a zero value.
//--------------------------------------------------------------------
//@ Implementation-Description
//   
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
TftpSocket::doCommand(void)
{
TftpMsgHeader header;
Uint size;
Int errCode;
	
	if ((size = readData(&header, sizeof(header))) <= 0)
	{
		Download::ReportError("TftpSocket::doCommand():readData() data read failed.",
			errno);
		return(0);
	}
	gotInput();
	if ( size != sizeof(header))
	{
		Download::ReportError(
			"TftpSocket::doCommand():readData() size a wrong number data read.", errno);
		return(0);
	}
	switch(header.opCode)
	{
		case OP_RRQ:
			if ( doRRQ_(&header) >= 0 )
			{
				state_ = DATA_SENT;
			}
			return(0);

		case OP_DATA_REQUEST:
#if	defined(SIGMA_DEBUG)
			printf("Got OP_DATA_REQUEST block id %d\n", fileBlockNum_);
#endif //defined(SIGMA_DEBUG)
			if (state_ != DATA_SENT )
			{
			  	TftpSocket::close("TftpSocket::doCommand: illegal DATA_REQUEST.");
				return(-1);
			}
			if ( (size = sendDataBlock_(CURRENT_BLOCK)) < 0  )
			{
				TftpSocket::close("TftpSocket::doCommand: failed in sending data.");
				return(-1);
			}
			return(0);

		case OP_DATA_ACK:
			if ((state_ !=  DATA_SENT) && (state_ != END_OF_FILE_REACHED))
			{
			  	TftpSocket::close("TftpSocket::doCommand: illegal DATA_ACK.");
				return(-1);
			}

			if ( header.blockNum != fileBlockNum_ )
			{
			  	TftpSocket::close("TftpSocket::doCommand: wrong Block Acked.");
				return(-1);
			}

			if ( (size = sendDataBlock_(NEXT_BLOCK)) < 0  )
			{
				TftpSocket::close("TftpSocket::doCommand: failed in sending data.");
				return(-1);
			}
			else
			{
				if ( size < DATA_BLOCK_SIZE )
				{
					if ( sendMsg(OP_END_OF_FILE, (char *) NULL, 0, 0 ) < 0 )
					{
						TftpSocket::close("TftpSocket::doCommand: failed in sending EOF msg");
						return(-1);
					}
					state_ = END_OF_FILE_REACHED;
				}
				else
				 	state_ = DATA_SENT;
			}
			return(0);

		case OP_DOWNLOAD_SUCCESS:
			if ( state_ == END_OF_FILE_REACHED )
			{
				TftpSocket::close(NULL);
			}
			else
			{
				TftpSocket::close("TftpSocket::doCommand: inconsistent TFTP state");
			}
			return(-1);

		case OP_ERROR:
			if ( readData( &errCode, sizeof(Int) ) == sizeof(Int) )
			{
				handleClientError_(( DownloadError ) errCode );
			}
			TftpSocket::close("");
			return(-1);
		default:
			TftpSocket::close("TftpSocket::doCommand: illegal default TFTP Op code");

			return(-1);
	}
}
#endif //defined(SIGMA_SERVER)

#if		defined(SIGMA_SERVER)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendDataBlock_(DataBlockIndex blockType)
//
//@ Interface-Description
//    Send a block of boot image data file to the TFTP client connected.
//	  If a given blockType is 'CURRENT_BLOCK', the method sends
//    the current data block to the client. If the given blockType is
//    'NEXT_BLOCK', the method reads the next data block from the boot 
//    file and transmit the data to a connected client.
//    It returns a negative number when it fails to send a block of data.
//    The method returns a number of data bytes successfully transmitted.
//	  Except the last block, the method should read a full block of data
//    each time. When a number bytes read in is less than the block size,
//    it indicates the boot file reached the end of file.
//--------------------------------------------------------------------
//@ Implementation-Description
//		The current data block is read into the netBuf_ buffer and
//      left alone until the next block is read in.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int
TftpSocket::sendDataBlock_(DataBlockIndex blockType )
{
int size;

	if ( ! opened() )
	{
		return(-1);
	}

	size = DATA_BLOCK_SIZE;
	if ( blockType == NEXT_BLOCK )
	{
		if ( ( size = bootFile_.read( (Byte *) netBuf_, DATA_BLOCK_SIZE )) < 0 )
		{
			Download::ReportError("TftpSocket::sendDataBlock_():read() IO error while reading the bootfile", errno);
			return(-1);
		}
		else if ( size == 0 )
		{
			return(0);
		}
	}
	fileBlockNum_  = bootFile_.getCurrentBlockNum();
	return(sendMsg(OP_DATA, (char *) netBuf_, size, fileBlockNum_));
}
#endif //defined(SIGMA_SERVER)

#if		defined(SIGMA_SERVER)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRRQ_(TtfpMsgHeader *pHeader )
//
//@ Interface-Description
//		Processes a Tftp RRQ operation. A requested file is opened.
//      And then, the first block data is read in and transmitted
//      a connected TFTP client. If an IO error is encountered 
//      while opening and reading a specified file, the method returns -1.
//		The method returns 0 when the file is empty.
//		The method returns a data block size when it performs the RRQ
//      request successfully.
//--------------------------------------------------------------------
//@ Implementation-Description
//     This routine does more than a typical TFTP RRQ processing.
//     The method is aware that it is handling a bootfile with
//	   a POST checksum table built into. The first data block in
//     the input boot file is the POST checksum table.
//--------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
TftpSocket::doRRQ_(TftpMsgHeader *pHeader)
{
RRQMsg  msg;
Int size;

	if ( state_ != INITIAL_STATE )
	{
	   TftpSocket::close("TftpSocket::doRRQ: initial RRQ at inconsistent TFTP state.");
	   return(-1);
	}

	if ( readData( &msg, pHeader->pktSize ) != pHeader->pktSize )
	{
	   TftpSocket::close("TftpSocket::Comm Error, failed in receiving a file name.");
	   return(-1);
	}


	if (bootFile_.open(msg.fileName) == FAILURE )
	{
	   	sprintf(ErrBuf_,"TftpSocket::doRRQ: failed in opening a bootfile, %s.",
				msg.fileName);
	   	TftpSocket::close(ErrBuf_);
	   	return( -1) ;
	}
	size = bootFile_.getFileSize()/DATA_BLOCK_SIZE;
	targetCPU_ = msg.type;
	printf("%s socket id %d requested %s size %d blocks\n", 
			getTargetName(),getSocketId(),  bootFile_.getFileName(), size);
	fileBlockNum_ = 0;

	if ( (size = sendDataBlock_(NEXT_BLOCK) ) < 0  )
	{
		TftpSocket::close("TftpSocket::doRRQ: failed in sending data.");
		return(-1);
	}
	else if ( size == 0 )
	{
		sprintf(ErrBuf_,"TftpSocket::doRRQ: bootfile %s length zero",msg.fileName);
		TftpSocket::close(ErrBuf_);
		return(-1);
	}
	return(1);
}
#endif //defined(SIGMA_SERVER)

#if	defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendRRQ_(char *fileName )
//
//@ Interface-Description
//	  Send a TFTP file read request message to the TFTP download server
//    using the dedicated TFTP socket connection.
//--------------------------------------------------------------------
//@ Implementation-Description
//	  The dedicated TCP/IP socket connection to the TFTP server
//	  is already made. All you need to do is send RRQ request message
//	  on the socket.
//--------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
TftpSocket::sendRRQ_(char *fileName)
{
RRQMsg msg;

	CLASS_ASSERTION(opened());

	msg.type =  ::BoardType();
	strncpy(msg.fileName, fileName, MAX_FILE_NAME_LENGTH);

	if ( sendMsg( OP_RRQ, (char *) &msg, sizeof(msg), 0 ) != sizeof(msg) ) // $[TI1]
	{
		Download::ReportError("TftpSocket::sendRRQ_():sendMsg() RRQ sendMsg failed", errno);
		return(-1);
	} // else $[TI2]
	state_ = DATA_WAITING;
	return( 1);
}
#endif //defined(SIGMA_CLIENT)

#ifdef SIGMA_SERVER
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSocketId(Int socketId) 
//
//@ Interface-Description
//    Open the socket and set the socket to a initial TFTP state.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//	  The TFTP socket connection is already made before this routine
//    is called. The socketId is a socket connection id between 
//    a Download server and client.
//--------------------------------------------------------------------- 
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
TftpSocket::setSocketId( Int socketId )
{
	CLASS_ASSERTION( !opened());

	socketId_ = socketId;
	state_ = INITIAL_STATE;
}
#endif // SIGMA_SERVER

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: close(char *errMsg) 
//
//@ Interface-Description
//    Close this TFTP socket. Close the network socket associated
//    with this socket. Print out a user friendly message indicating
//    success or failure of the download for this socket connection.
//    Close the bootfile associated with this socket connection.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//   If a NULL errMsg pointer is passed, the method assumes that there
//   was no error during the TFTP download process.
//   Otherwise, the method assumes there was a some sort of failure
//   detected during this TFTP socket download process.
//--------------------------------------------------------------------- 
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
#if	defined(SIGMA_SERVER)
TftpSocket::close( char *errMsg )
#else
TftpSocket::close( char *)
#endif //defined(SIGMA_SERVER)
{
	// $[TI1]
#if	defined(SIGMA_SERVER)
static char ErrBuf2_[300];

	if ( !errMsg )
		sprintf(ErrBuf2_, "Download to %s socket id %d successfully completed.\n",
			getTargetName(), getSocketId());
	else
		sprintf(ErrBuf2_, "Download to %s socket id %d failed: %s\n", getTargetName(),
		getSocketId(), errMsg);
	Download::DisplayMessage(ErrBuf2_);
	bootFile_.close();
#endif //defined(SIGMA_SERVER)
	NetSocket::close();  //close the TFTP socket connection.
	state_ = INITIAL_STATE;
}

#if	defined(SIGMA_SERVER)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTargetName(void)
//
//@ Interface-Description
//		Gets a target CPU type name.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//	    Only two kinds of targets, BD and GUI.
//--------------------------------------------------------------------- 
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
char *
TftpSocket::getTargetName(void)
{
	if ( targetCPU_ == BD_BOARD )
	{
		return("BD");
	}
	else
	{
		return("GUI");
	}
}
#endif //defined(SIGMA_SERVER)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendMsg(Uint16  msgId,char *buf,Int nbytes,Int blockNo)
//
//@ Interface-Description
//    This method sends a requested network message on this socket.
//    The message data is in the 'buf' memory area. The 'nbytes' is 
//    a message data size not including a message header.
//    If a given 'nbytes' value is zero, only the message header
//    portion is sent out to the network without any accompanying data.
//    The method returns a number of data bytes that are
//    successfully transmitted. The method returns -1 value when there
//    is an error. When the socket is not open, the method doesn't
//    transmit any data and returns -1.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    The method forms a TFTP message header and transmits first, then
//    a user given data is transmitted as a separate request.
//--------------------------------------------------------------------- 
//@ PreCondition
//   (nbytes <= MAX_BUF_SIZE)
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
TftpSocket::sendMsg(Uint16  msgId,char *buf,Int nbytes,Int blockNo)
{
TftpMsgHeader header;

	CLASS_PRE_CONDITION( nbytes <= MAX_BUF_SIZE);

	header.opCode = msgId; 
	header.pktSize = nbytes;
	header.blockNum = blockNo;

#if defined(SIGMA_SERVER)
	if (!( blockNo % 100 ))
	{
		printf("To %s socket id %d block %d\n", getTargetName(), getSocketId(), blockNo);
	}
#endif //defined(SIGMA_SERVER)

	if ( sendData((char *) &header, sizeof(header)) != sizeof(header) ) // $[TI1]
	{
		return(-1);
	} // else $[TI2]

	if ( buf ) // $[TI3]
	{
		return ( sendData( buf, nbytes));
	}
	else // $[TI4]
	{
		return(0);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendErrMsg_( DownloadError errorCode )
//
//@ Interface-Description
//    This method sends an error network message through this socket
//	  connection.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    The method forms an error  TFTP message header and transmits it
//    first, then the given data error code is transmitted as a separate
//	  request.
//--------------------------------------------------------------------- 
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
TftpSocket::sendErrMsg_( DownloadError errorCode )
{
TftpMsgHeader header;

	CLASS_ASSERTION(opened());

	header.opCode = OP_ERROR; 
	header.pktSize = sizeof(errorCode);
	if (sendData((char *) &header, sizeof(header)) != sizeof(header)) // $[TI1]
	{
		return;
	} // else $[TI2]
	sendData( (char *) &errorCode, sizeof(errorCode));
}


#if	defined(SIGMA_CLIENT) && defined(SIGMA_UNIT_TEST)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: callFinishFlashBurning(void)
//
//@ Interface-Description
//      Provides access to the private method
//      TftpSocket::finishFlashBurning_(void).
//--------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
TftpSocket::callFinishFlashBurning(void)
{
	return(finishFlashBurning_());
}
#endif	//defined(SIGMA_CLIENT) && defined(SIGMA_UNIT_TEST)


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: callHandleClientError( DownloadError eCode )
//
//@ Interface-Description
//      Provides access to the private method
//      TftpSocket::handleClientError_(DownloadError eCode).
//--------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
TftpSocket::callHandleClientError(DownloadError eCode)
{
	handleClientError_(eCode);
}
#endif // SIGMA_UNIT_TEST


#if	defined(SIGMA_CLIENT) && defined(SIGMA_UNIT_TEST)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: callRecvDataBlock( TftpMsgHeader *pHeader )
//
//@ Interface-Description
//      Provides access to the private method
//      TftpSocket::recvDataBlock_(TftpMsgHeader *pHeader).
//--------------------------------------------------------------------
//@ Implementation-Description
//--------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
TftpSocket::callRecvDataBlock(TftpMsgHeader *pHeader)
{
	return(recvDataBlock_(pHeader));
}
#endif //defined(SIGMA_CLIENT)


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: callSendErrMsg( DownloadError errorCode )
//
//@ Interface-Description
//      Provides access to the private method
//      TftpSocket::sendErrMsg_(DownloadError errorCode).
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
TftpSocket::callSendErrMsg( DownloadError errorCode )
{
	sendErrMsg_(errorCode);
}
#endif // SIGMA_UNIT_TEST


#if defined(SIGMA_UNIT_TEST) && defined(SIGMA_CLIENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: callSendRRQ(char *fileName )
//
//@ Interface-Description
//      Provides access to the private method
//      TftpSocket::sendRRQ_(char *fileName).
//--------------------------------------------------------------------
//@ Implementation-Description
//--------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
TftpSocket::callSendRRQ(char *fileName)
{
	return(sendRRQ_(fileName));
}
#endif // SIGMA_UNIT_TEST && SIGMA_CLIENT


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSocketIdOnly(Int socketId)
//
//@ Interface-Description
//    Set the SocketId_ to the specified value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
TftpSocket::setSocketIdOnly( Int socketId )
{
	socketId_ = socketId;
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFileBlockNum(void)
//
//@ Interface-Description
//    Get the current fileBlockNum_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int
TftpSocket::getFileBlockNum( void )
{
	return(fileBlockNum_);
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFileBlockNum(Int passedFileBlockNum)
//
//@ Interface-Description
//    Set the fileBlockNum_ to the passed fileBlockNum.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
TftpSocket::setFileBlockNum(Int passedFileBlockNum)
{
	fileBlockNum_ = passedFileBlockNum;
}
#endif // SIGMA_UNIT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
TftpSocket::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
           const char*        pFileName,
           const char*        pPredicate)
{
  		DownloadFault::SoftFault(softFaultID, DOWNLOAD,
			TFTP_SOCKET_CLASS, lineNumber, pFileName, pPredicate); 
}
