#ifndef FlowSensor_HH
#define FlowSensor_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: FlowSensor - Implements the inspiratory and expiratory flow sensors.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlowSensor.hhv   26.0.1.0   04 Feb 2013 10:36:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 008 By: syw    Date: 03-Jul-1997   DR Number: DCS 2282
//  	Project:  Sigma (R8027)
//		Description:
//			Removed calTemp_ and getFlowTemperature_() method since calTemp
//			is now constant.
//
//  Revision: 007 By: syw    Date: 18-Feb-1997   DR Number: DCS 1780, 1827
//  	Project:  Sigma (R8027)
//		Description:
//			Moved private declarations to protected so ExhFlowSensor class
//			can have access.  Delete pSecondaryFlowSensorCalInfo_, O2Percent_,
//			DryFactor_, sensorId_, Initialize().  Added getFlowTemperature_() to
//			support ExhFlowsensor class.
//
//  Revision: 006 By: syw    Date: 18-Feb-1997   DR Number: DCS 1780
//  	Project:  Sigma (R8027)
//		Description:
//			Added pSecondaryFlowSensorCalInfo_ data member for computing
//			exhalation flow.  Added above as arguments to constructor.
//			Added setPSecondaryFlowSensorCalInfo() method.
//
//  Revision: 005 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added updateValue() with flowCounts and tempCounts as arguments
//			for TUV support.  Added BD_CPU macros.  Added constructor for
//			GUI CPU.
//
//  Revision: 004 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 003  By: syw    Date:  12-Jan-1996    DR Number: DCS 562
//       Project:  Sigma (R8027)
//       Description:
//             Added setPFlowSensorCalInfo() method.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SafetyNetSensorData * and FlowSensorCalInfo * to constructor
//			arguments.  The FlowSensorCalInfo pointer shall point to FLASH
//			where the calibration information is stored.  Added data member
//			pFlowSensorCalInfo_.  Added setAlpha() method.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "Sensor.hh"
#include "ProcessedValues.hh"
#include "FlowSensorCalInfo.hh"

class TemperatureSensor;

//@ End-Usage

class FlowSensor: public Sensor
{
  public:

#if defined (SIGMA_BD_CPU)

    FlowSensor( SafetyNetSensorData *pData,
    			const AdcChannels::AdcChannelId adcId,
                TemperatureSensor& temperatureSensor,
                const Real32 alpha,
                FlowSensorCalInfo *pFlowSensorCalInfo) ;

#else

    FlowSensor( TemperatureSensor& temperatureSensor,
                FlowSensorCalInfo *pFlowSensorCalInfo) ;

#endif // defined (SIGMA_BD_CPU)

    virtual ~FlowSensor( void) ;

#if defined(SIGMA_DEVELOPMENT)

    static void SoftFault( const SoftFaultID softFaultID,
	   const Uint32        lineNumber,
	   const char*         pFileName  = NULL, 
	   const char*         pPredicate = NULL) ;
#endif

#if defined (SIGMA_BD_CPU)

    inline Real32 getSlope( void) const ;
    inline Real32 getFilteredSlopeValue( void) const ;
    inline Real32 getFilteredValue ( void) const ;
    inline void initValues( const Real32 filtered, const Real32 slope,
    						const Real32 filteredSlope, const Real32 prevValue) ;
    inline void setAlpha( const Real32 alpha) ;
	inline void setPFlowSensorCalInfo( FlowSensorCalInfo *pFlowSensorCalInfo) ;
    
    virtual void updateValue( void) ;

#endif // defined (SIGMA_BD_CPU)

#if defined (SIGMA_GUI_CPU)

	Real32 updateValue( const AdcCounts flowCounts, const AdcCounts tempCounts) ;
	
#endif // defined (SIGMA_GUI_CPU)

  protected:

    virtual Real32 rawToEngValue_( const AdcCounts count) const ;

    //@ Data-Member: pFlowSensorCalInfo_
    // a pointer to a FlowSensorCalInfo instance
    FlowSensorCalInfo *pFlowSensorCalInfo_ ;  

    //@ Data-Member: rTemperatureSensor_ 
    // a reference to a temperature sensor for compensation
    TemperatureSensor& rTemperatureSensor_;

#if defined (SIGMA_BD_CPU)

    //@ Data-Member: processedValues_
    // a ProcessedValues instance
    ProcessedValues processedValues_;

#endif // defined (SIGMA_BD_CPU)

  private:
    // these methods are purposely declared, but not implemented...
    FlowSensor( void) ;                    // default constructor
    FlowSensor( const FlowSensor&) ;       // not implemented
    void operator=( const FlowSensor&) ;   // not implemented

#if defined (SIGMA_GUI_CPU)

	 AdcCounts tempCounts_;
	
#endif // defined (SIGMA_GUI_CPU)

} ;

// Inlined methods
#include "FlowSensor.in"

#endif // FlowSensor_HH 
