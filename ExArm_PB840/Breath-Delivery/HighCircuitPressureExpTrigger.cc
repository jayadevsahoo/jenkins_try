#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: HighCircuitPressureExpTrigger - Triggers if circuit pressure >=
//        maximum of operator set value for the high circuit pressure limit
//	  and allowed pressure overshoot.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class is derived from PressureExpTrigger because both triggers
//    need maximum inspiratory pressure as a factor in the trigger condition.
//    This trigger is based on the circuit pressure measured 
//    and maximum inspiratory pressure. The maximum inspiratory pressure is 
//    set to 0 during non-pressure based breath.
//    The trigger can be enabled or disabled, depending upon the current 
//    breath phase and the applicability of the trigger.  When the pressure 
//    condition calculated by this trigger is true, the trigger is considered 
//    to have "fired".  This trigger is kept on a list contained in the 
//    BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    It is necessary to end inspiration, and proceed directly to exhalation 
//    when the circuit pressure equals or exceeds the valid pressure limit.  
//    This trigger provides a means of monitoring for that condition.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of whether 
//    this trigger is enabled or not.  If the trigger is not enabled, it will 
//    always return state of false.  If it is enabled and on the active list
//    in the BreathTriggerMediator, the condition monitored 
//    by the trigger will be evaluated every BD cycle. The condition detected 
//    by this trigger is circuit pressure measured during inspiration
//    at a value >= maximum of operator set value for the high circuit 
//    pressure limit and allowed pressure overshoot.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/HighCircuitPressureExpTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 006  By: healey     Date:  27-Jan-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added new trigger logic for TcvPhase
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//      Initial ATC release
//
//  Revision: 004  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 003  By:  iv    Date:  12-Oct-1996    DR Number: 1293
//       Project:  Sigma (R8027)
//       Description:
//             get the airway pressure from BreathRecord.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "HighCircuitPressureExpTrigger.hh"

//@ Usage-Classes
#  include "PressureSensor.hh"
#  include "BreathRecord.hh"
#  include "MainSensorRefs.hh"
#include "PhasedInContextHandle.hh"
#include "BreathPhase.hh"
#include "PhaseRefs.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: HighCircuitPressureExpTrigger()  
//
//@ Interface-Description
//	Default Contstructor
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ using PressureExpTrigger's constructor and
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

HighCircuitPressureExpTrigger::HighCircuitPressureExpTrigger(
	void
) : PressureExpTrigger(Trigger::HIGH_CIRCUIT_PRESSURE_EXP)
{
  CALL_TRACE("HighCircuitPressureExpTrigger()");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~HighCircuitPressureExpTrigger()  
//
//@ Interface-Description
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

HighCircuitPressureExpTrigger::~HighCircuitPressureExpTrigger(void)
{
  CALL_TRACE("~HighCircuitPressureExpTrigger()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable()
//
//@ Interface-Description
//      This method takes no argument and has no return value. This method
//      enables the trigger and is responsible for the trigger's
//      initilization
//---------------------------------------------------------------------
//@ Implementation-Description
//      The state data member isEnabled_ is set to true, activating this
//      trigger.  The data members maxInspiratoryPressure_, intervalNumber_
//      and count_ are initialized.  maxInspiratoryPressure_
//      is initialized to 0.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
HighCircuitPressureExpTrigger::enable(void)
{
  CALL_TRACE("enable(void)");
 
   // $[TI1]
  isEnabled_ = TRUE;
  maxInspiratoryPressure_ = 0.0F;
  intervalNumber_ = 0;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
HighCircuitPressureExpTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, HIGHCIRCUITPRESSUREEXPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns 
//    false.  In phases other than TC or PAV, trigger fires if circuit pressure 
//    >= maximum of operator set value for the high circuit pressure
//    limit and allowed pressure overshoot.  For TC and PAV phase, trigger fires
//    if airway pressure >= maximum of operator set value for the high 
//    circuit pressure limit.
//    
//---------------------------------------------------------------------
//@ Implementation-Description
//    If the circuit pressure, as measured by the exhalation pressure 
//    transducer, is greater than or equal to  the valid pressure limit 
//    (maximum of operator set value for the high circuit pressure limit 
//    and allowed pressure overshoot), then this method returns true.
// $[04032] a
// $[04032] c
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
HighCircuitPressureExpTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

  Boolean rtnValue = FALSE;
  Real32 highCircuitPressureValue =
	PhasedInContextHandle::GetBoundedValue(SettingId::HIGH_CCT_PRESS).value;
  Real32 validPressLimit = MAX_VALUE(highCircuitPressureValue, maxInspiratoryPressure_);
  BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase(); 

  if (pBreathPhase == (BreathPhase*)&RTcvPhase 
      || pBreathPhase == (BreathPhase*)&RPavPhase)
  {// $[TI5]
		if ( BreathRecord::GetAirwayPressure() >= highCircuitPressureValue )
	  	{// $[TI5.1]
			rtnValue = TRUE;
	  	}// $[TI5.2]
  }
  else
  {
	  if ( BreathRecord::GetAirwayPressure() >= validPressLimit )
	  {	// $[TI2]
		  rtnValue = TRUE;
	  }// $[TI1]
  }

   
  // FALSE: $[TI4]
  return (rtnValue);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
