#ifndef VtpcvStateDataPacket_HH
#define VtpcvStateDataPacket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct : VtpcvStateDataPacket - structure definition of patient
//   data packet containing Volume Targeted Pressure Control breath
//   start, Startup or Normal.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/VtpcvStateDataPacket.hhv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: yakovb  Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Handle VC+/VS Startup messages.
//
//  Revision: 001  By: sah    Date:  12-Mar-2001    DR Number: 5847
//	Project:  PAV
//	Description:
//		Used to send PAV state prior to first PAV breath.
//
//====================================================================

#include "VtpcvState.hh"

// @Type: VtpcvStateDataPacket
struct   VtpcvStateDataPacket
{
	VtpcvState::Id  vtpcvState;
	
	void convHtoN()
	{
		SocketWrap sock;
		vtpcvState = (VtpcvState::Id)sock.HTONL((Uint32)vtpcvState);
	}

	void convNtoH()
	{
		SocketWrap sock;
		vtpcvState = (VtpcvState::Id)sock.NTOHL((Uint32)vtpcvState);
	}
};

#endif // VtpcvStateDataPacket_HH 
