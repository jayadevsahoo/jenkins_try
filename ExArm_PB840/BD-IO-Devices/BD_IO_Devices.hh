#ifndef BD_IO_Devices_HH
#define BD_IO_Devices_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//Class: BD_IO_Devices - Subsystem definitions, construction, and
//       initialization.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BD_IO_Devices.hhv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 021  By: mnr    Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      Added BigSerialNumber class.
//
//  Revision: 020  By: gdc    Date: 17-Feb-2009   SCR Number: 6019
//  Project:  840S
//  Description:
//      Added DataKeyPower class.
//
//  Revision: 019  By: sah    Date: 31-Jan-2000   DR Number: 5424
//  Project:  NeoMode
//  Description:
//      Added id for new 'UserOptions' class.
//
//  Revision: 018  By:  syw    Date:  08-Nov-1999    DR Number: 5574
//       Project:  840
//       Description:
//			Adeed COMP_ALARM_DELAY.
//
//  Revision: 017  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//			Added EVENTFILTER and DEBOUNCER
//
//  Revision: 016  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method. Also, renamed
//		'SENSOR_MEDIATOR' to 'SENSORMEDIATOR' to follow naming convention.
//
//  Revision: 015  By:  syw    Date: 25-Aug-1998    DR Number:DCS 5144
//       Project:  Sigma (R8027)
//       Description:
//		 	Elimininate OPTIONENCRYPTION class.
//
//  Revision: 014  By:  syw    Date: 04-Dec-1997    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//		 	BiLevel initial version.  Added SOFTWAREOPTIONS and OPTIONENCRYPTION
//			enums..
//
//  Revision: 013  By:  syw    Date: 25-Sep-1997    DR Number: DCS 2410
//       Project:  Sigma (R8027)
//       Description:
//             Added ATMPRESSOFFSET.
//
//  Revision: 012  By:  syw    Date:  03-Jul-97    DR Number: DCS 2279
//       Project:  Sigma (R8027)
//       Description:
//             Added FLOWSENSOROFFSET.
//
//  Revision: 011 By: syw   Date: 13-Mar-1996   DR Number: DCS
//      Project:  Sigma (R8027)
//      Description:
//			Added EXHFLOWSENSOR.
//
//  Revision: 010 By: syw   Date: 19-Dec-1996   DR Number: DCS 1603
//      Project:  Sigma (R8027)
//      Description:
//			Added BitAccessGpio and BitAccessGpioMEDIATOR.
//			Renumbered enums explicitly.  Changed INITIAL_SERIAL_READ_WRITE_IMAGE
//			to INITIAL_SERIAL_WRITE_IMAGE
//
//  Revision: 009 By: syw   Date: 25-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added support for flash serial number determination.
//
//  Revision: 008 By: syw   Date: 04-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SIGMA_GUI_CPU around specific code.
//
//  Revision: 007 By: syw   Date: 04-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Changed Int16 to Uint16 for AdcCounts.  Renamed
//			INITIAL_SERIAL_PORT_IMAGE to INITIAL_SERIAL_READ_WRITE_IMAGE.
//
//  Revision: 006 By: iv    Date: 28-Feb-1996   DR Number: DCS 675
//  	Project:  Sigma (R8027)
//		Description:
//			Sorted and enumerated BD_IO_DevicesClassID's.
//
//  Revision: 005 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 004  By: syw    Date:  25-Jan-1996    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added extern to GENERAL_EEPROM_CLOCK;
//
//  Revision: 003  By: syw    Date:  25-Jan-1996    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added FIO2CALINFO, PSOLLIFTOFF, CALINFOFLASHBLOCK, FLOWSERIALEEPROM
//			to BD_IO_DevicesClassId.  Extern of flow serial eeprom masks.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added FIO2MONITOR, VENTSTATUS, SYSTEMBATTERY, and EXHHEATER
//			for BD_IO_DevicesClassID enum.  Define void (*PAlarmCallBack)() and
//			void (*PVentStatusCallBack)() typedefs.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sigma.hh"

#if defined (SIGMA_BD_CPU)

#include "BdAlarmId.hh"
#include "EventData.hh"

#endif  // defined(SIGMA_BD_CPU)

//@ Usage-Classes
//@ End-Usage


//====================================================================

//@ Type: BD_IO_DevicesClassID
// A list of each class name.  Used by SoftFault

enum BD_IO_DevicesClassID
{
    ADCCHANNELS 				= 0,
    BDIODEVICES 				= 1,
    BINARYCOMMAND 				= 2,
    BINARYINDICATOR 			= 3,
	BITACCESSGPIO	 			= 4,
	BITACCESSGPIOMEDIATOR		= 5,
    BLOCKREADREGISTER 			= 6,
    CALINFOFLASHBLOCK	 		= 7,
    COMPRESSOR 					= 8,
    COMPRESSOREEPROM 			= 9,
    COMPRESSORTIMER 			= 10,
    CONFIGURATION 				= 11,
    CONFIGURATIONAGENT 			= 12,
    DATAKEY 					= 13,
    DATAKEYAGENT	 			= 14,
    EXHHEATER 					= 15,
    EXHVALVETABLE 				= 16,
    EXHALATIONVALVE 			= 17,
    FIO2CALINFO 				= 18,
    FIO2MONITOR 				= 19,
    FLASHMEMORY 				= 20,
    FLOWSENSOR 					= 21,
    FLOWSENSORCALINFO 			= 22,
    FLOWSENSORCOEFFENTRY 		= 23,
    FLOWSERIALEEPROM 			= 24,
    GASSUPPLY 					= 25,
    LINEARSENSOR 				= 26,
    POWERSOURCE 				= 27,
    PRESSURESENSOR 				= 28,
    PROCESSEDVALUES 			= 29,
    PSOL 						= 30,
    PSOLLIFTOFF 				= 31,
    REGISTER 					= 32,
    SAFETYNETSENSORDATA 		= 33,
    SAFETYVALVE 				= 34,
    SENSOR 						= 35,
    SENSORMEDIATOR				= 36,
    SERIALEEPROM 				= 37,
    SERIALNUMBER 				= 38,
    SOLENOID 					= 39,
    SUBMUX 						= 40,
    SYSTEMBATTERY 				= 41,
    TEMPERATURESENSOR 			= 42,
    VENTSTATUS 					= 43,
    EXHFLOWSENSOR				= 44,
    FLOWSENSOROFFSET			= 45,
    ATMPRESSOFFSET				= 46,
    SOFTWAREOPTIONS				= 47,
    EVENTFILTER					= 48,
    DEBOUNCER					= 49,
    USEROPTIONS					= 50,
	DATA_KEY_POWER				= 51,
    BD_SERIAL_INTERFACE_CLASS	= 52,
    PROXI_INTERFACE_CLASS		= 53,
    MERCURY_BOOTCODE_CLASS		= 54,
	BIGSERIALNUMBER				= 55,
	GASINLETPRESSURESENSOR		= 56,
} ;

//@ Type: AdcCounts
// Data format for the ADC input
typedef Uint16 AdcCounts ;
typedef float Flow_t;
typedef float Pressure_t;
typedef float FiO2_t;

//@ Type: DacCounts
// Data format for the DAC output
typedef Uint16 DacCounts ;

#if defined (SIGMA_BD_CPU)

//@ Type: PAlarmCallBack
// Format for the callback method invoked when an alarm is detected by one
// of the BD-IO-Devices objects (eg. Compressor, Battery, Fio2Monitor)
typedef void (*PAlarmCallBack) (const BdAlarmId::BdAlarmIdType bdAlarmId,
                                const Boolean initInProg);

//@ Type: PVentStatusCallBack
// Format for the callback method invoked when a ventilator status event
// is detected by one of the BD-IO-Devices objects (eg. Compressor, Battery)
typedef void (*PVentStatusCallBack) (const EventData::EventId id,
                                     const EventData::EventStatus status,
                                     const EventData::EventPrompt prompt);

//@ Constant: ALPHA
// The value used for filtering sensors.
extern const Real32   ALPHA ;

#endif  // defined(SIGMA_BD_CPU)

// This global pointer is initialized by NV_MemoryStorageThread.
extern void *FLASH_SERIAL_NO_ADDR;
static const int SIZE_OF_FLASH_SERIAL_NO_ADDR = 0x4000;


//=====================================================================
//
//  Breath-Delivery Main Task Cycle Time
//
//====================================================================

//@ Constant: CYCLE_TIME_MS
// cycle time for BD primary task in milliseconds
extern const Int32 CYCLE_TIME_MS ;

// cycle time for BD secondary task in milliseconds
extern const Int32 SECONDARY_CYCLE_TIME_MS ;

//=====================================================================
//
//  Initial SerialReadWritePort image
//
//====================================================================

#if defined (SIGMA_BD_CPU)

extern const Uint16 INITIAL_SERIAL_WRITE_IMAGE ;
extern const Uint16 INITIAL_READ_ONLY_REGISTER_IMAGE ;

#endif  // defined(SIGMA_BD_CPU)

//=====================================================================
//
//  Serial EEPROM Masks
//
//====================================================================

#if defined (SIGMA_BD_CPU)

extern const Uint16 O2_FLOW_EEPROM_IN_DATA   ;
extern const Uint16 AIR_FLOW_EEPROM_IN_DATA  ;
extern const Uint16 EXH_FLOW_EEPROM_IN_DATA  ;

extern const Uint16 O2_FLOW_EEPROM_OUT_DATA  ;
extern const Uint16 AIR_FLOW_EEPROM_OUT_DATA  ;
extern const Uint16 EXH_FLOW_EEPROM_OUT_DATA  ;

extern const Uint16 O2_EEPROM_SELECT   ;
extern const Uint16 AIR_EEPROM_SELECT  ;
extern const Uint16 EXH_EEPROM_SELECT  ;

extern const Uint16 GENERAL_EEPROM_CLOCK  ;
extern const Uint16 GENERAL_EEPROM_WRITE_PROTECT  ;

#endif  // defined(SIGMA_BD_CPU)

//=====================================================================
//
//  ADC conversion constants
//
//=====================================================================

//@ Constant: MAX_COUNT_VALUE
// The maximum value allowed for ADC or DAC.
extern const Int16    MAX_COUNT_VALUE ;

//@ Constant: MAX_VOLT_VALUE
// The maximum voltage for ADC or DAC.
extern const Real32   MAX_VOLT_VALUE ;

//@ Constant: COUNT_TO_VOLT_FACTOR
// The value used to convert ADC counts into volts.
extern const Real32 COUNT_TO_VOLT_FACTOR ;

//@ Constant: COMP_ALARM_DELAY
// delay before reporting compressor status following power up
const Uint32 COMP_ALARM_DELAY = 10000 ; // msec


class BD_IO_Devices
{
  public:
	  static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32        lineNumber,
						   const char*         pFileName  = NULL,
						   const char*         pPredicate = NULL) ;

	static inline Boolean GetSerialNumOkay( void) ;

    static Boolean GetSerialNumInitialSignature( void) ;
    static void Initialize( void) ;

  private:
    // these methods are purposely declared, but not implemented...
    BD_IO_Devices( const BD_IO_Devices&);    // not implemented...
    void   operator= (const BD_IO_Devices&); // not implemented...

    // BD_IO_Devices is never constructed or destructed
    BD_IO_Devices( void);           // not implemented
    ~BD_IO_Devices( void);  // not implemented

	//TODO E600_LL: need to check
	//static Boolean IsSerialNumInitialSignature_( void) ;

#if defined (SIGMA_BD_CPU)
	//TODO E600_LL : need to check
	// static Boolean IsSerialNumOkay_( void) ;
#endif  // defined (SIGMA_BD_CPU)

	//@ Data-Member: Boolean SerialNumInitialSignature_
	// set true if serial number is the initial signature
	static Boolean SerialNumInitialSignature_ ;

	//@ Data-Member: Boolean SerialNumOkay_
	// set if serial number in flash matches the data key's
	static Boolean SerialNumOkay_ ;
} ;

#endif // BD_IO_Devices_HH


