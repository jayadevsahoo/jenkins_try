#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PressureVolumePlot - A loop plot which plots pressure against
// volume and displays the inspiratory area of the loop.
//---------------------------------------------------------------------
//@ Interface-Description
// PressureVolumePlot derives from LoopPlot.  LoopPlot does all the hard
// work of plotting pressure versus volume.  What PressureVolumePlot adds
// is a display of "inspiratory area", whose value is displayed in a
// data field beside the actual plot.
//
// Each time a segment of the waveform is plotted in the normal way via a call
// to update() this call is intercepted in this class allowing us to scan
// the segment and calculate the inspiratory area.
//
// Other methods of LoopPlot that are intercepted are erase() (allowing us
// to erase the calculated inspiratory area value) and setGridArea()
// (which allows us to reposition the inspiratory area display box according
// to changes in the grid's area).
//---------------------------------------------------------------------
//@ Rationale
// This class adds a feature specific to all pressure volume plotting.
//---------------------------------------------------------------------
//@ Implementation-Description
// The main functionality of this class is performed in update().  This is
// where each successive segment of the pressure-volume waveform is scanned
// in order to calculate the inspiratory area value which is displayed within
// a labelled box to the right of the waveform grid.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// which verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PressureVolumePlot.ccv   25.0.4.0   19 Nov 2013 14:08:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "PressureVolumePlot.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "WaveformIntervalIter.hh"
//@ End-Usage

//@ Code...
static const Int32  INSP_AREA_X_OFFSET_ = 42;
static const Int32  INSP_AREA_Y_OFFSET_ = 0;
static const Int32  INSP_AREA_WIDTH_ = 80;
static const Int32  INSP_AREA_HEIGHT_ = 56;
// see DCS #1946, for how this number was calculated...
static const Real32 L_CMH2O_TO_JOULES_ = 0.000032686;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureVolumePlot  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize data members, set the X and Y units of the loop plot to be
// pressure and volume  and setup the display of the inspiratory area
// value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureVolumePlot::PressureVolumePlot(void) :
					inspAreaBox_(0, 0, INSP_AREA_WIDTH_, INSP_AREA_HEIGHT_),
					inspAreaText_(MiscStrs::PRESSURE_VOLUME_INSP_AREA_LABEL),
					inspAreaValue_(TextFont::TWELVE, 0, CENTER),
					workIndex_(0.0)
{
	CALL_TRACE("PressureVolumePlot::PressureVolumePlot(void)");

	setXYUnits(PatientDataId::CIRCUIT_PRESS_ITEM, PatientDataId::NET_VOL_ITEM);

	inspAreaBox_.setColor(Colors::WHITE);
	inspAreaText_.setColor(Colors::WHITE);
	inspAreaValue_.setColor(Colors::WHITE);
	inspAreaValue_.setWidth(INSP_AREA_WIDTH_);
	inspAreaValue_.setHeight(INSP_AREA_HEIGHT_ / 2);
	inspAreaValue_.setPrecision(THOUSANDTHS);			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureVolumePlot  [Destructor]
//
//@ Interface-Description
// Destroys PressureVolumePlot.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureVolumePlot::~PressureVolumePlot(void)
{
	CALL_TRACE("PressureVolumePlot::PressureVolumePlot(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
// Draws successive segments of a waveform plot.  It is passed a reference to a
// WaveformIntervalIter.  This specifies the interval of waveform data that
// must be plotted.  We also calculate the inspiratory area here.
//---------------------------------------------------------------------
//@ Implementation-Description
// The main job here is to scan each interval of data (but only the
// inspiration part) and accumulate the inspiratory area and once the end
// of inspiration is detected display the accumulated value onscreen.  The
// drawing of the loop plot is handled by the LoopPlot base class update()
// method which we call here.
//
// The algorithm for calculating inspiratory area is described below:
//>Von
//	if (in inspiration phase)
//	{
//		work pressure = baseline pressure - current pressure sample
//		if ((work pressure > 0) and (current flow sample >0))
//		{
//			work index = work index + flow * work pressure
//		}
//	}
//	else
//	{
//		inspiratory area = work index converted to joules
//	}
// >Voff
// $[01167] The X-axis of any pressure-volume loop shall correspond ...
// $[01172] The design of any pressure-volume loop shall include ... 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureVolumePlot::update(WaveformIntervalIter& segmentIter)
{
	CALL_TRACE("PressureVolumePlot::update(WaveformIntervalIter& segmentIter)");

	// Ignore call if not visible
	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	// Call base class method to do drawing
	LoopPlot::update(segmentIter);

	// Let's scan the interval's pressure/volume samples
	for (SigmaStatus status = segmentIter.goFirst();
						SUCCESS == status;
						status = segmentIter.goNext())
	{													// $[TI3]
		// Check to see if we're dealing with a sample in an inspiration
		if (segmentIter.getDiscreteValue(PatientDataId::BREATH_PHASE_ITEM)
											== BreathPhaseType::INSPIRATION)
		{												// $[TI4]
			// We are so make sure Inspiratory Area value not shown (will not
			// be displayed until the end of inspiration).
			inspAreaValue_.setShow(FALSE);

			// Calculate work pressure
			Real32 workPressure = getYAxisValue_() - 
				segmentIter.getBoundedValue(PatientDataId::CIRCUIT_PRESS_ITEM);

			// Check to see that work pressure and flow are both positive
			Real32 flow = segmentIter.getBoundedValue(PatientDataId::NET_FLOW_ITEM);
			if ((workPressure > 0.0) && (flow > 0.0))
			{											// $[TI5]
				// Both positive, accumulate in the work index
				workIndex_ += flow * workPressure;
			}											// $[TI6]
		}
		// We're in the exhalation phase.  If we haven't already displayed
		// the inspiratory area then do so.
		else if (!inspAreaValue_.getShow())
		{												// $[TI7]
			// Convert the work index to joules.  Display it and break out of
			// loop
			Real32 workIndexInJoules = workIndex_ * L_CMH2O_TO_JOULES_;

			workIndex_ = 0.0;
			inspAreaValue_.setValue(workIndexInJoules);
			inspAreaValue_.setShow(TRUE);
			break;
		}
		else
		{												// $[TI8]
			// We are in exhalation and the inspiratory value has been shown
			// so skip all other data in the interval.
			break;
		}
	}													// $[TI9]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: erase
//
//@ Interface-Description
// Erases the pressure-volume plot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call the base class's method first and then remove the inspiratory area
// value from the display and reset the work index to 0.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureVolumePlot::erase(void)
{
	CALL_TRACE("PressureVolumePlot::erase(void)");

	LoopPlot::erase();

	inspAreaValue_.setShow(FALSE);
	workIndex_ = 0.0;									// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setGridArea
//
//@ Interface-Description
// Sets the area occupied by the grid within the area of the WaveformPlot
// object itself.  The area is passed via x, y, width and height values.
//---------------------------------------------------------------------
//@ Implementation-Description
// We call the base class's method first and then we reposition the
// inspiratory area drawables so as to be positioned correctly in relation
// to the waveform's grid.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureVolumePlot::setGridArea(Uint32 x, Uint32 y,
												Uint32 width, Uint32 height)
{
	CALL_TRACE("PressureVolumePlot::setGridArea(Uint32 x, Uint32 y, "
											"Uint32 width, Uint32 height)");

	WaveformPlot::setGridArea(x, y, width, height);

	inspAreaBox_.setX(x + width + INSP_AREA_X_OFFSET_);
	inspAreaBox_.setY(y + INSP_AREA_Y_OFFSET_);
	inspAreaText_.setX(inspAreaBox_.getX());
	inspAreaText_.setY(inspAreaBox_.getY());
	inspAreaValue_.setX(inspAreaBox_.getX());
	inspAreaValue_.setY(inspAreaBox_.getY() + INSP_AREA_HEIGHT_ / 2);
														// $[TI1]
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutGrid_ 		[Virtual protected]
//
//@ Interface-Description
// Lays out the grid.  The virtual WaveformPlot parent method does most of the
// layout work, all this method needs to do is display the inspiratory
// area graphics.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call the WaveformPlot parent method first and then display the
// inspiratory area box, text and value drawables.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureVolumePlot::layoutGrid_(void)
{
	CALL_TRACE("PressureVolumePlot::layoutGrid_(void)");

	WaveformPlot::layoutGrid_();

	addDrawable(&inspAreaBox_);
	addDrawable(&inspAreaText_);
	addDrawable(&inspAreaValue_);
	inspAreaValue_.setShow(FALSE);						// $[TI1]
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
PressureVolumePlot::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, PRESSUREVOLUMEPLOT,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
