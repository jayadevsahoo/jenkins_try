/*****************************************************************************/
/*   Module Name:    setupcbk.c                                            
/*   Purpose:      Callback and support functions for the SETUP panel.                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Removed the Metabolic Port Test.       Rhomere Jimenez   10 Oct,2007
/*
/*   In function	TOptionsAllCB  global   Dwarkanath(CGS)	  01 Dec,2000
/*   variable gNumTests is included
/*   instead of numTests.
/*
/*   The AutoDetectPTS and AutoDetect840 	Dwarkanath(CGS)	  01 Dec,2000
/*   function is modified to update the
/*   AUTODETECT panel and the header file
/*   rt_bmps.h is added for the same.
/*
/*   TestExecutive related code is 			Hamsvally(CGS)    02 Oct,2000
/*   removed and replaced correspodnig 
/*   funcitonality.
/*
/*	 Critical Observation during Phase1 
/* 	 is incorportated.						Kathirvel(CGS)    27 Sep,2000
/*
/*   Make button background color           Dave Richardson   19 Nov, 2002
/*   transparent
/*
/*   $Revision::   1.2      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <windows.h>
#include <ansi_c.h>
#include <userint.h>

#include "72500.h"
#include "840.h"
#include "commreg.h"
#include "cpvtmain.h"
#include "detfun.h"
#include "easytab.h"
#include "helpinit.h"
#include "ldb.h"
#include "mainpnls.h"
#include "MsgPopup.h"
#include "pts.h"
#include "pwctrl.h"
#include "registry.h"                
#include "regpwd.h"
#include "rt_bmps.h"
#include "sysmenu.h"
#include "testing.h"
#include "testpnl.h"
#include "usradmin.h"

/***********************************************/
/************ FUNCTION PROTOTYPE  ************/
/***********************************************/
// All the function prototypes are Modified the statement due to Critical Observation.
void ComControlDim840(int panel);
void ComControlDimPTS(int panel);
void ComControlsUpdate(int panel);
void PopulateListFromRegistry(int panel, int control);
void UIDeleteUser(void);
void UIChangeUserPassword(void);
int PopulateRingFromReg(int Bar, int Menu);
void SetUpLanguagePanel(int EasyTab);
int DrawLangMenu(int panel, int control);
void CheckItem(int control);

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     ComControlDim840                                                      
/*                                                                           
/* Purpose:  Toggles 840 port and baud selections on Comm Settings panel
/*                                          
/*                                                                           
/* Example Call:   void ComControlDim840(int panel);                                
/*                                                                           
/* Input:    panel - COMSETTING panel handle        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void ComControlDim840(int panel)
{
	int enabled;

	// if 840 auto-detect is enabled, then dim manual COM controls
	GetCtrlVal(panel, COMSETTING_AUTODET_ENABLE_840, &enabled);
	if(enabled){
		SetCtrlAttribute(panel, COMSETTING_BAUD_840,     ATTR_DIMMED, TRUE);
		SetCtrlAttribute(panel, COMSETTING_COM_PORT_840, ATTR_DIMMED, TRUE);
	}
	else{
		SetCtrlAttribute(panel, COMSETTING_BAUD_840,     ATTR_DIMMED, FALSE);
		SetCtrlAttribute(panel, COMSETTING_COM_PORT_840, ATTR_DIMMED, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     ComControlDimPTS                                                   
/*                                                                           
/* Purpose:  Toggles PTS port and baud selections on Comm Settings panel  
/*                                          
/*                                                                           
/* Example Call:   void ComControlDimPTS(int panel);                                    
/*                                                                           
/* Input:    panel COMSETTING panel handle       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void ComControlDimPTS(int panel)
{
	int enabled;

	// if PTS auto-detect is enabled, then dim manual COM controls
	GetCtrlVal(panel, COMSETTING_AUTODET_ENABLE_PTS, &enabled);
	if(enabled){
		SetCtrlAttribute(panel, COMSETTING_BAUD_PTS,     ATTR_DIMMED, TRUE);
		SetCtrlAttribute(panel, COMSETTING_COM_PORT_PTS, ATTR_DIMMED, TRUE);
	}
	else{
		SetCtrlAttribute(panel, COMSETTING_BAUD_PTS,     ATTR_DIMMED, FALSE);
		SetCtrlAttribute(panel, COMSETTING_COM_PORT_PTS, ATTR_DIMMED, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     ComControlsUpdate                                                    
/*                                                                           
/* Purpose:  Sets initial Comm Settings panel values
/*                                          
/*                                                                           
/* Example Call:   void ComControlsUpdate(int panel);                                    
/*                                                                           
/* Input:    panel - handle of comm settings panel      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void ComControlsUpdate(int panel)
{
	RegComStruct comParams;
	unsigned long comPortsBitMask;
	int err;
	int port;
	char portStr[6];
	int controlIndex = 0;

	comPortsBitMask = ComFindAllValidPorts();

	// dynamically setup the 840 COM port setting control for the user
	// COM1-4 is always shown, but COM5-32 is only shown if we find a port
	err = DeleteListItem(panel, COMSETTING_COM_PORT_840, 0, -1);
	for(port = 1; port <= 32; port++){
		if(comPortsBitMask & 0x1L || port <= 4){
			sprintf(portStr, "COM%d", port);
			err = InsertListItem(panel, COMSETTING_COM_PORT_840, controlIndex, portStr, port);
			controlIndex++;
		}
		comPortsBitMask >>= 1;
	}

	// dynamically setup the PTS COM port setting control for the user
	// COM1-4 is always shown, but COM5-32 is only shown if we find a port
	controlIndex = 0;
	comPortsBitMask = ComFindAllValidPorts();
	err = DeleteListItem(panel, COMSETTING_COM_PORT_PTS, 0, -1);
	for(port = 1; port <= 32; port++){
		if(comPortsBitMask & 0x1L || port <= 4){
			sprintf(portStr, "COM%d", port);
			err = InsertListItem(panel, COMSETTING_COM_PORT_PTS, controlIndex, portStr, port);
			controlIndex++;
		}
		comPortsBitMask >>= 1;
	}

	// set inital 840 control values based upon registry setting
	if(GetCommParams(&comParams, COM_840KEY) == 0){
		if(!comParams.autoDetectDisabled){
			SetCtrlVal(panel, COMSETTING_AUTODET_ENABLE_840, TRUE);
		}
		if(comParams.port != 0){
			SetCtrlVal(panel, COMSETTING_COM_PORT_840, comParams.port);
		}
		if(comParams.baud != 0){
			SetCtrlVal(panel, COMSETTING_BAUD_840, comParams.baud);
		}
	}

	// set inital PTS control values based upon registry setting
	if(GetCommParams(&comParams, COM_PTSKEY) == 0){
		if(!comParams.autoDetectDisabled){
			SetCtrlVal(panel, COMSETTING_AUTODET_ENABLE_PTS, TRUE);
		}
		if(comParams.port != 0){
			SetCtrlVal(panel, COMSETTING_COM_PORT_PTS, comParams.port);
		}
		if(comParams.baud != 0){
			SetCtrlVal(panel, COMSETTING_BAUD_PTS, comParams.baud);
		}
	}

	return;
}


/*****************************************************************************/
/* Name:      TOptionsCallback                                                     
/*                                                                           
/* Purpose:  Handle events for the test options tab in the setup screen
/*		If a test in unchecked, set the tx to skip it
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK TOptionsCallback (int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                   
/*                                                                           
/* Input:    Standard callback arguments          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK TOptionsCallback(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int skip = RUN_MODE_SKIP;
	int index, i;

	if(event == EVENT_COMMIT){
		switch(control){
			case  TESTOPTS_FIO2PERF:
				index = 11;
				break;
			case  TESTOPTS_PEEPPERF:
				index = 10;
				break;
			case  TESTOPTS_PRESPERF:
				index = 9;
				break;
			case  TESTOPTS_VOLPERF:
				index = 8;
				break;
			case  TESTOPTS_FIRMWARETEST:
				index = 7;
				break;
				
			case  TESTOPTS_OXYFLOWTEST:
				index = 6;
				break;
			case  TESTOPTS_AIRFLOWTEST:
				index = 5;
				break;
	
			case  TESTOPTS_CPRESTEST:
				index = 4;
				break;
			case  TESTOPTS_GUIATEST:
				index = 3;
				break;
			case  TESTOPTS_BDATEST:
				index = 2;
				break;
			case  TESTOPTS_APTEST:
				index = 1;
				break;
			case  TESTOPTS_MANUALTEST:
				index = 0;
				break;
			case  TESTOPTS_STOPONFAILS:
				GetCtrlVal(gOPanelHandle[TESTOPTS], control, &index);
				sStopOnFail = index;
				return 0;
			default:
				return 0;
		} // end switch(control)
		GetCtrlVal(gOPanelHandle[TESTOPTS], control, &skip);
		skip = skip ? RUN_MODE_NORMAL : RUN_MODE_SKIP;
	    tDataRec[index].sRunMode =skip; 
		UpdateTestListing(index, skip ? TEST_SKIP : TEST_CLEAR);
	} // end if(event == EVENT_COMMIT)

	return 0;
}


/*****************************************************************************/
/* Name:     TOptionsAllCB                                                   
/*                                                                           
/* Purpose:  Sets all tests to enabled (not skipped)
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK TOptionsAllCB(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                   
/*                                                                           
/* Input:    Standard callback arguments   
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK TOptionsAllCB(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int i;

	if(event != EVENT_COMMIT){
		return 0;
	}
	for(i = 0; i < gNumTests; i++){
		tDataRec[i].sRunMode = RUN_MODE_NORMAL;
        UpdateTestListing(i, TEST_CLEAR);
	}	
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_MANUALTEST,   TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_APTEST,       TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_BDATEST,      TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_GUIATEST,     TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_CPRESTEST,    TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_AIRFLOWTEST,  TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_OXYFLOWTEST,  TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_FIRMWARETEST, TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_VOLPERF,      TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_PRESPERF,     TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_PEEPPERF,     TRUE);
	SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_FIO2PERF,     TRUE);

	return 0;
}


/*****************************************************************************/
/* Name:     PopulateListFromRegistry                                                      
/*                                                                           
/* Purpose:  Fill the user list box in the setup panel with user names from
/*		the registry.
/*                                          
/*                                                                           
/* Example Call:   void PopulateListFromRegistry(int panel, int control);                                  
/*                                                                           
/* Input:    int panel	panel of the list box to fill
/*	int control handle of the list box to fill
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void PopulateListFromRegistry(int panel, int control)
{
	char* Name;
	char* Userlist;

	ClearListCtrl(panel, control);
	
	Userlist = GetUserList();

	if(Userlist == NULL){
		return;
	}

	Name = strtok(Userlist, NameListDelimiter);
	do{
		if(Name != NULL){
			InsertListItem(panel, control, -1, Name, 0);
		}
	} while(Name = strtok(NULL, NameListDelimiter));

	free(Userlist);

	return;
}


/*****************************************************************************/
/* Name:     UIDeleteUser                                                     
/*                                                                           
/* Purpose:  Confirm and delete user profile GUI
/*                                          
/*                                                                           
/* Example Call:  void UIDeleteUser(void);                                
/*                                                                           
/* Input:    <none>        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void UIDeleteUser(void)
{
	int currindex, currlbllen;
	char* currlabel;
	char ConfirmStr[100];

	GetCtrlIndex(gOPanelHandle[USERCONFIG], USERCONFIG_USERLIST, &currindex);
	GetLabelLengthFromIndex(gOPanelHandle[USERCONFIG], USERCONFIG_USERLIST, currindex, &currlbllen);
	currlabel = (char *)malloc(currlbllen + 1); // Modified the statement due to Critical Observation.
	GetLabelFromIndex(gOPanelHandle[USERCONFIG], USERCONFIG_USERLIST, currindex, currlabel);
	sprintf(ConfirmStr, "%s%s?", LDBf(Ldb_SureDeleteUser), currlabel);				   
	if(CPVTConfirmPopup(LDBf(Ldb_Delete_User), ConfirmStr, OK_CANCEL_DLG, NO_HELP)){					   
		DeleteUser(currlabel);
		DeleteListItem(gOPanelHandle[USERCONFIG], USERCONFIG_USERLIST, currindex, 1);
	}
	free(currlabel);

	return;
}


/*****************************************************************************/
/* Name:    UIChangeUserPassword                                                
/*                                                                           
/* Purpose:  Confirm and change user password GUI
/*                                          
/*                                                                           
/* Example Call:   void UIChangeUserPassword(void);                                    
/*                                                                           
/* Input:    <none>        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void UIChangeUserPassword(void)
{
	int currindex, currlbllen;
	int tempPanelHandle, tempCntrlHandle;
	char* currlabel;
	char PassWord[MAX_PWD_LEN + 1], ConfirmPassWord[MAX_PWD_LEN + 1], OldPassWord[MAX_PWD_LEN + 1];
	char ConfirmStr[50];
	int nCtrlLeft;
	int nCtrlWidth;
	int nCtrlWidthNew;

	GetCtrlIndex(gOPanelHandle[USERCONFIG], USERCONFIG_USERLIST, &currindex);
	GetLabelLengthFromIndex(gOPanelHandle[USERCONFIG], USERCONFIG_USERLIST, currindex, &currlbllen);
	currlabel = (char *)malloc(currlbllen + 1); // Modified the statement due to Critical Observation.
	GetLabelFromIndex(gOPanelHandle[USERCONFIG], USERCONFIG_USERLIST, currindex, currlabel);
	gPanelHandle[ADDUSERPNL] = LoadPanel(0, LDBf(Ldb_MainpanelFile), ADDUSERPNL);
	SetPanelAttribute(gPanelHandle[ADDUSERPNL], ATTR_TITLE, LDBf(Ldb_ChangePwdTitle));

	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_OK_BUTTON,     ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_CANCEL_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_ERROR,    ATTR_VISIBLE, FALSE);
	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD, ATTR_DIMMED,  FALSE);

	GetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_TEXTMSG, ATTR_LEFT,  &nCtrlLeft);
	GetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_TEXTMSG, ATTR_WIDTH, &nCtrlWidth);
	SetCtrlVal(gPanelHandle[ADDUSERPNL], ADDUSERPNL_TEXTMSG, LDBf(Ldb_Old_Password));
	GetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_TEXTMSG, ATTR_WIDTH, &nCtrlWidthNew);
	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_TEXTMSG, ATTR_LEFT, (nCtrlLeft + nCtrlWidth) - nCtrlWidthNew);

	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_OK_BUTTON,  ATTR_DIMMED, FALSE);
	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2, ATTR_DIMMED, FALSE);

	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME,   ATTR_CALLBACK_FUNCTION_POINTER, NULL);
	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD,   ATTR_CALLBACK_FUNCTION_POINTER, NULL);
	SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2, ATTR_CALLBACK_FUNCTION_POINTER, NULL);

	PasswordCtrl_ConvertFromString(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME);
	PasswordCtrl_ConvertFromString(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD);
	PasswordCtrl_ConvertFromString(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2);

	PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME,   ATTR_PASSWORD_MAX_LENGTH, MAX_PWD_LEN);
	PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD,   ATTR_PASSWORD_MAX_LENGTH, MAX_PWD_LEN);
	PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2, ATTR_PASSWORD_MAX_LENGTH, MAX_PWD_LEN);
			
	InstallPopup(gPanelHandle[ADDUSERPNL]);
	while(GetUserEvent(1, &tempPanelHandle, &tempCntrlHandle)){
		if(tempCntrlHandle == ADDUSERPNL_OK_BUTTON){
			strcpy(OldPassWord, "");
			strcpy(PassWord, "");
			strcpy(ConfirmPassWord, "");
			PasswordCtrl_GetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME,   ATTR_PASSWORD_VAL, OldPassWord);
			PasswordCtrl_GetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD,   ATTR_PASSWORD_VAL, PassWord);
			PasswordCtrl_GetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2, ATTR_PASSWORD_VAL, ConfirmPassWord);
			
			if(ValidatePassword(currlabel, OldPassWord) != PWD_OK){
				SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_ERROR, ATTR_CTRL_VAL, LDBf(Ldb_Old_Password_wrng));
				SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_ERROR, ATTR_VISIBLE, TRUE);

				DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME);
				DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD);
				DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2);

				PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME,   ATTR_PASSWORD_VAL, "");
				PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD,   ATTR_PASSWORD_VAL, "");
				PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2, ATTR_PASSWORD_VAL, "");
				SetActiveCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME);
			}
			else if(strcmp(PassWord, ConfirmPassWord) != 0){
				SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_ERROR, ATTR_CTRL_VAL, LDBf(Ldb_Password_Mismatch));
				SetCtrlAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_ERROR, ATTR_VISIBLE, TRUE);
								  
				PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME,   ATTR_PASSWORD_VAL, "");
				PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD,   ATTR_PASSWORD_VAL, "");
				PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2, ATTR_PASSWORD_VAL, "");

				DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_USERNAME);
				DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD);
				DefaultCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2);
				SetActiveCtrl(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD);
			}
			else{
				if(ChangeUserPasswd(currlabel, PassWord) == PWD_OK){
					DiscardPanel(gPanelHandle[ADDUSERPNL]);
					gPanelHandle[ADDUSERPNL] = 0;
					break;
				}
			}
			PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD,   ATTR_PASSWORD_VAL, "");
			PasswordCtrl_SetAttribute(gPanelHandle[ADDUSERPNL], ADDUSERPNL_PASSWORD_2, ATTR_PASSWORD_VAL, "");
		} // end if(tempCntrlHandle == ADDUSERPNL_OK_BUTTON)

		if(tempCntrlHandle == ADDUSERPNL_CANCEL_BUTTON){
			DiscardPanel(gPanelHandle[ADDUSERPNL]);
			gPanelHandle[ADDUSERPNL] = 0;
			break;
		}
	} // end while(GetUserEvent(1, &tempPanelHandle, &tempCntrlHandle))
	free(currlabel);

	return;
}


/*****************************************************************************/
/* Name:    UserConfig                                                     
/*                                                                           
/* Purpose:  Callback function for User Configuration tab in setup panel.
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK UserConfig(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK UserConfig(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int currindex, items;

	if(event == EVENT_DISCARD){
		return 0;
	}

	GetCtrlIndex(gOPanelHandle[USERCONFIG], USERCONFIG_USERLIST, &currindex);
	//If no name is selected don't allow deleting or changing password
	if(currindex < 0){
		SetCtrlAttribute(gOPanelHandle[USERCONFIG], USERCONFIG_DELETEUSER, ATTR_DIMMED, TRUE);
		SetCtrlAttribute(gOPanelHandle[USERCONFIG], USERCONFIG_USERPWD,    ATTR_DIMMED, TRUE);
	}
	else{
		SetCtrlAttribute(gOPanelHandle[USERCONFIG], USERCONFIG_DELETEUSER, ATTR_DIMMED, FALSE);
		SetCtrlAttribute(gOPanelHandle[USERCONFIG], USERCONFIG_USERPWD,    ATTR_DIMMED, FALSE);
	}	

	if(event == EVENT_COMMIT){
		switch(control){
			case USERCONFIG_ADDUSER:
				UIAddUser();
				PopulateListFromRegistry(panel, USERCONFIG_USERLIST);
				break;

			case USERCONFIG_DELETEUSER:
				UIDeleteUser();
				PopulateListFromRegistry(panel, USERCONFIG_USERLIST);
				break;

			case USERCONFIG_USERPWD:
				UIChangeUserPassword();
				PopulateListFromRegistry(panel, USERCONFIG_USERLIST);
				break;
		}
	}

	if(event == -99){ // sent by LoadPanels(APPSTART2)
		PopulateListFromRegistry(panel, USERCONFIG_USERLIST);
		GetNumListItems(panel, USERCONFIG_USERLIST, &items);
		if(items > 0){
			SetCtrlVal(panel, USERCONFIG_USERLIST, -1);
		}
	}

	if(event != EVENT_DISCARD){
		if(CurrUserCount() < MAX_USERS){
			SetCtrlAttribute(panel, USERCONFIG_ADDUSER, ATTR_DIMMED, FALSE);
			ResetTextBox(gOPanelHandle[USERCONFIG], USERCONFIG_MAXUSERS_MSG, "");
		}
		else{
			SetCtrlAttribute(gOPanelHandle[USERCONFIG], USERCONFIG_ADDUSER, ATTR_DIMMED, TRUE);
			ResetTextBox(gOPanelHandle[USERCONFIG], USERCONFIG_MAXUSERS_MSG, LDBf(Ldb_Max_Users_Reg));
		}
	}

	return 0;
}


/*****************************************************************************/
/* Name:    SelectLang_Wrapper                                                    
/*                                                                           
/* Purpose:  Callback for selecting language from language tab in setup panel
/*                                          
/*                                                                           
/* Example Call: void CVICALLBACK SelectLang_Wrapper( int menuBarHandle,
/*         int menuItemID, void *callbackData, int panelHandle );                                    
/*                                                                           
/* Input:    standard menu callback arguments        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK SelectLang_Wrapper( int menuBarHandle,
         int menuItemID, void *callbackData, int panelHandle )
{
	char LanguageName[MAX_LANG_LENGTH + 1];
	int nStatus;

	nStatus = GetMenuBarAttribute(menuBarHandle, menuItemID, ATTR_ITEM_NAME, LanguageName);

//	CurrLanguage = GetLangfromString(LanguageName);
	SaveUserLanguage(gCurrOperator, LanguageName);
	SetCtrlVal(gOPanelHandle[LANGCONF], LANGCONF_LANG_SEL, LanguageName);

	return;									  
}


/*****************************************************************************/
/* Name:   PopulateRingFromReg                                                  
/*                                                                           
/* Purpose:  Populates language selection menu from registry
/*                                          
/*                                                                           
/* Example Call:   void PopulateRingFromReg(int Bar, int Menu);                                    
/*                                                                           
/* Input:    Bar - menu bar handle to populate
/*			 Menu - menu handle to populate
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int PopulateRingFromReg(int Bar, int Menu)
{
	int nNumLanguages = 0;
	char* LangList;
	char* Language;

	LangList = GetLangList();

	if(LangList == NULL){
		return 0;
	}

	Language = strtok(LangList, ",\n");
	do{
		if(Language != NULL){
			NewMenuItem(Bar, Menu, Language, -1, 0, SelectLang_Wrapper, Language);
			nNumLanguages++;
			if(GetLangfromString(Language) == CurrLanguage){
				SetCtrlVal(gOPanelHandle[LANGCONF], LANGCONF_LANG_SEL, Language);
			}
		}
	} while(Language = strtok(NULL, ",\n"));

	free(LangList);

	return nNumLanguages;
}


/*****************************************************************************/
/* Name:      SetUpLanguagePanel                                                    
/*                                                                           
/* Purpose:  Populates language selection menu,
/*	     Labels tab with current user.                                         
/*                                                                           
/* Example Call:   void SetUpLanguagePanel();                                    
/*                                                                           
/* Input:    <none>       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/	
void SetUpLanguagePanel(int EasyTab)
{
	int nPromptSize;
	int nNameSize;
	char *buffer = NULL;
	int nNumLanguages = 0;
	int BarHandle, MenuHandle;

	nPromptSize = strlen(LDBf(Ldb_Select_Lang_for));
	nNameSize = strlen(gCurrOperator);
	buffer = (char *)malloc(nPromptSize + nNameSize + 1); // Modified the statement due to Critical Observation.

	if(buffer){
		sprintf(buffer, "%s%s", LDBf(Ldb_Select_Lang_for), gCurrOperator);
		SetCtrlVal(gOPanelHandle[LANGCONF], LANGCONF_LANG_MSG, buffer);
		free(buffer);
	
		BarHandle = NewMenuBar(0);
		MenuHandle = NewMenu(BarHandle, "", -1);
		nNumLanguages = PopulateRingFromReg(BarHandle, MenuHandle);
		DiscardMenuBar(BarHandle);
	}

	if(nNumLanguages <= 0){
		//There are no languages so dimm everything
		EasyTab_SetTabAttribute(gPanelHandle[ADMINPANEL], EasyTab, gOPanelHandle[LANGCONF], ATTR_EASY_TAB_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     DrawLangMenu                                                  
/*                                                                           
/* Purpose:  Draws the language selection dropdown menu
/*                                          
/*                                                                           
/* Example Call:   int DrawLangMenu(int panel, int control);                                    
/*                                                                           
/* Input:    panel - parent panel handle of menu
/*			 control - control to tie menu to (based on possition)
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int DrawLangMenu(int panel, int control)
{
	int BarHandle, MenuHandle;
	int t, l, h, w;
	int nNumLanguages = 0;

	BarHandle = NewMenuBar(0);
	MenuHandle = NewMenu(BarHandle, "", -1);
	nNumLanguages = PopulateRingFromReg(BarHandle, MenuHandle);
	if(nNumLanguages != 0){
		GetCtrlAttribute(panel, control, ATTR_HEIGHT, &h);
		GetCtrlAttribute(panel, control, ATTR_LEFT,   &l);
		GetCtrlAttribute(panel, control, ATTR_TOP,    &t);
		GetCtrlAttribute(panel, control, ATTR_WIDTH,  &w);

		RunPopupMenu(BarHandle, MenuHandle, panel, t+h, l, t, l, h, w);
	}
	DiscardMenuBar(BarHandle);
	
	return 0;
}	


/*****************************************************************************/
/* Name:    LanguageSelect                                                    
/*                                                                           
/* Purpose:  Callback for the language selection button on the setup panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK LanguageSelect(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK LanguageSelect(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event){
		case EVENT_GOT_FOCUS:
		case EVENT_LEFT_CLICK:
			DrawLangMenu(panel, control);
			break;
	}

	return 0;
}


/*****************************************************************************/
/* Name:     CheckItem                                                   
/*                                                                           
/* Purpose:  Makes Data logging options check boxes function like radio buttons
/*                                          
/*                                                                           
/* Example Call:   void CheckItem(int control);                                    
/*                                                                           
/* Input:    int control	The control to set.         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CheckItem(int control)
{
	//Each control in the radio button set has an if else group.
	//If it was commited check it otherwise uncheck it.
	//This way there is always one checked.
	if(control == LOGGINOPTS_CHECKBOX_1)
		SetCtrlVal(gOPanelHandle[LOGGINOPTS], LOGGINOPTS_CHECKBOX_1, TRUE);
	else
		SetCtrlVal(gOPanelHandle[LOGGINOPTS], LOGGINOPTS_CHECKBOX_1, FALSE);

	if(control == LOGGINOPTS_CHECKBOX_2)
		SetCtrlVal(gOPanelHandle[LOGGINOPTS], LOGGINOPTS_CHECKBOX_2, TRUE);
	else
		SetCtrlVal(gOPanelHandle[LOGGINOPTS], LOGGINOPTS_CHECKBOX_2, FALSE);

	if(control == LOGGINOPTS_CHECKBOX_3)
		SetCtrlVal(gOPanelHandle[LOGGINOPTS], LOGGINOPTS_CHECKBOX_3, TRUE);
	else
		SetCtrlVal(gOPanelHandle[LOGGINOPTS], LOGGINOPTS_CHECKBOX_3, FALSE);

	if(control == LOGGINOPTS_CHECKBOX_4)
		SetCtrlVal(gOPanelHandle[LOGGINOPTS], LOGGINOPTS_CHECKBOX_4, TRUE);
	else
		SetCtrlVal(gOPanelHandle[LOGGINOPTS], LOGGINOPTS_CHECKBOX_4, FALSE);

	return;		
}


/*****************************************************************************/
/* Name:      DataLogCB                                                  
/*                                                                           
/* Purpose:  Callback for the datalogging options in the setup panel.
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK DataLogCB(int panel, int control, int event,
/*      	void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callbacdk arguments
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK DataLogCB(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int checked;

	switch(event){
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &checked);
			if(!checked){
				SetCtrlVal(panel, control, 1);	
				return 0;
			}
			else{
				CheckItem(control);
				switch(control){
					case LOGGINOPTS_CHECKBOX_1:
						ChangeReportScreenOption(RPT_ALL);
						break;
					case LOGGINOPTS_CHECKBOX_2:
						ChangeReportScreenOption(RPT_PASSONLY);
						break;
					case LOGGINOPTS_CHECKBOX_3:
						ChangeReportScreenOption(RPT_FAILONLY);
						break;
					case LOGGINOPTS_CHECKBOX_4:
						ChangeReportScreenOption(RPT_NONE);
						break;
					default:
						ChangeReportScreenOption(RPT_ALL);
					break;
				}
			}
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:      AutoDetectPTS                                                
/*                                                                           
/* Purpose:  Callback for autodetect PTS button on com settings panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK AutoDetectPTS(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK AutoDetectPTS(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	RegComStruct comParams;
	int val;

	switch(event){
		case EVENT_COMMIT:
            Register_Setup_CB();

			// Read existing params:
			GetPTSCommParams(&comParams);
			GetCtrlVal(panel, COMSETTING_COM_PORT_PTS,       &comParams.port);
			GetCtrlVal(panel, COMSETTING_BAUD_PTS,           &comParams.baud);
			GetCtrlVal(panel, COMSETTING_AUTODET_ENABLE_PTS, &val);
			comParams.autoDetectDisabled = !val; // Reverse logic variable.

			// Write these new params:
			SetCommParams(&comParams, COM_PTSKEY);
			setButtonBMP(gOPanelHandle[AUTODETECT], AUTODETECT_ACCEPT, BMP_Cancel192);
			InstallPopup(gOPanelHandle[AUTODETECT]);
			EstablishPTS(AUTO_DETECT);
			if(!IsPTSConnected() || !Is840Connected()){
				SetTestButtonMode(TB_DEVICE_NOT_DETECTED);
			}
			else{
				SetTestButtonMode(TB_NORMAL);
				if(IsNextCalOverdue(szNextCalDate)){
					CPVTMessagePopup(LDBf(Ldb_Pts_cal_err), LDBf(Ldb_Pts_cal_expired), OK_ONLY_DLG, NO_HELP);
				}
			}
			break;
	}

	return 0;
}


/*****************************************************************************/
/* Name:    ComPort840                                                    
/*                                                                           
/* Purpose: callback for 840 port selection on com settings panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK ComPort840(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK ComPort840(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	RegComStruct comParams;
	int val;

	switch(event){
		case EVENT_COMMIT:
			if(GetCommParams(&comParams, COM_840KEY) == 0){
				GetCtrlVal(panel, COMSETTING_COM_PORT_840, &val);
				comParams.port = val;
				SetCommParams(&comParams, COM_840KEY);
			}
			break;
	}

	return 0;
}


/*****************************************************************************/
/* Name:     AutoDetect840                                                    
/*                                                                           
/* Purpose:  Callback for autodetect 840 button on com settings panel 
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK AutoDetect840(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK AutoDetect840(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	RegComStruct comParams;
	int val;

	switch(event){
		case EVENT_COMMIT:
			Register_Setup_CB();

			// Read existing params:
			Get840CommParams(&comParams);
			GetCtrlVal(panel, COMSETTING_COM_PORT_840,       &comParams.port);
			GetCtrlVal(panel, COMSETTING_BAUD_840,           &comParams.baud);
			GetCtrlVal(panel, COMSETTING_AUTODET_ENABLE_840, &val);
			comParams.autoDetectDisabled = !val; // Reverse logic variable.

			// Write these new params:
			SetCommParams(&comParams, COM_840KEY);
			setButtonBMP(gOPanelHandle[AUTODETECT], AUTODETECT_ACCEPT, BMP_Cancel192);
			InstallPopup(gOPanelHandle[AUTODETECT]);
			Establish840(AUTO_DETECT);
			if(!IsPTSConnected() || !Is840Connected()){
				SetTestButtonMode(TB_DEVICE_NOT_DETECTED);
			}
			else{
				SetTestButtonMode(TB_NORMAL);
			}
			break;
	}

	return 0;
}


/*****************************************************************************/
/* Name:     ComPortPTS                                                   
/*                                                                           
/* Purpose:  callback for PTS port selection on com settings panel
/*                                          
/*                                                                           
/* Example Call:  int CVICALLBACK ComPortPTS(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK ComPortPTS(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	RegComStruct comParams;
	int val;

	switch(event){
		case EVENT_COMMIT:
			if(GetCommParams(&comParams, COM_PTSKEY) == 0){
				GetCtrlVal(panel, COMSETTING_COM_PORT_PTS, &val);
				comParams.port = val;
				SetCommParams(&comParams, COM_PTSKEY);
			}
			break;
	}

	return 0;
}


/*****************************************************************************/
/* Name:     BaudRate840                                                      
/*                                                                           
/* Purpose:  Callback for 840 baud rate selection on com settings panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK BaudRate840(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK BaudRate840(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
    RegComStruct comParams;
	int val;

	switch(event){
		case EVENT_COMMIT:
			if(GetCommParams(&comParams, COM_840KEY) == 0){
				GetCtrlVal(panel, COMSETTING_BAUD_840, &val);
				comParams.baud = val;
				SetCommParams(&comParams, COM_840KEY);
			}
			break;
	}

	return 0;
}


/*****************************************************************************/
/* Name:     BaudRatePTS                                                  
/*                                                                           
/* Purpose:  Callback for PTS baud rate selection on com settings panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK BaudRatePTS(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK BaudRatePTS(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	RegComStruct comParams;
	int val;

	switch(event){
		case EVENT_COMMIT:
			if(GetCommParams(&comParams, COM_PTSKEY) == 0){
				GetCtrlVal(panel, COMSETTING_BAUD_PTS, &val);
				comParams.baud = val;
				SetCommParams(&comParams, COM_PTSKEY);
			}
			break;
	}

	return 0;
}


/*****************************************************************************/
/* Name:      ComSettingPanel                                                     
/*                                                                           
/* Purpose:  Comm settings panel callback function
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK ComSettingPanel(int panel, int event, void *callbackData,
/*		int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments    
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK ComSettingPanel(int panel, int event, void *callbackData,
		int eventData1, int eventData2)
{
	switch(event){
		case EVENT_GOT_FOCUS:
			break;
		case EVENT_TAB_CHANGED:
			ComControlsUpdate(panel);
			ComControlDim840(panel);
			ComControlDimPTS(panel);
			break;
		case EVENT_LOST_FOCUS:
			break;
		case EVENT_CLOSE:
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
	}

	return 0;
}


/*****************************************************************************/
/* Name:     AutoDetectEnabled840                                                     
/*                                                                           
/* Purpose:  Auto detect 840 button callback 
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK AutoDetectEnabled840(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK AutoDetectEnabled840(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	RegComStruct comParams;
	int val;

	switch(event){
		case EVENT_COMMIT:
			if(GetCommParams(&comParams, COM_840KEY) == 0){
				GetCtrlVal(panel, COMSETTING_AUTODET_ENABLE_840, &val);
				if(val){
					comParams.autoDetectDisabled = 0;
				}
				else{
					comParams.autoDetectDisabled = 1;
				}
				SetCommParams(&comParams, COM_840KEY);
			}

			ComControlDim840(panel);
			break;
	}

	return 0;
}


/*****************************************************************************/
/* Name:    AutoDetectEnablePTS                                                 
/*                                                                           
/* Purpose:  auto detect PTS button callback(on comm settings panel)
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK AutoDetectEnablePTS(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK AutoDetectEnablePTS(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	RegComStruct comParams;
	int val;

	switch(event){
		case EVENT_COMMIT:
			if(GetCommParams(&comParams, COM_PTSKEY) == 0){
				GetCtrlVal(panel, COMSETTING_AUTODET_ENABLE_PTS, &val);
				if(val){
					comParams.autoDetectDisabled = 0;
				}
				else{
					comParams.autoDetectDisabled = 1;
				}
				SetCommParams(&comParams, COM_PTSKEY);
			}

			ComControlDimPTS(panel);
			break;
	}

	return 0;
}


/*****************************************************************************/
/* Name:     AcceptAutoDetect                                                   
/*                                                                           
/* Purpose:  Accept button callback for autodetect panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK AcceptAutoDetect(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:   standard callback arguments       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK AcceptAutoDetect(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event){
		case EVENT_COMMIT:
			StopDetect();
			RemovePopup(panel);
			SetCtrlVal(gOPanelHandle[AUTODETECT], AUTODETECT_AUTODET_STATUS, "");
			ComControlsUpdate(gOPanelHandle[COMSETTING]);
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, control, eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	}

	return 0;
}

