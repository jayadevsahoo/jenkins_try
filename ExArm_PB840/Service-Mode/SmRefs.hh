#ifndef SmRefs_HH
#define SmRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header:  SmRefs - External references for Service Mode objects
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmRefs.hhv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: mnr    Date: 24-Mar-2010     DR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX SST related updates.
//
//  Revision: 008  By:  dosman    Date: 16-Apr-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//		ATC initial release.
//		Removed some SM_TEST code that is no longer needed.
//
//  Revision: 007  By:  syw    Date: 27-Aug-1998    DR Number:
//       Project:  BiLevel
//       Description:
//          BiLevel Initial Release.  Added RDataKeyCopy.
//
//  Revision: 006  By:  quf    Date: 15-Jan-1998    DR Number: DCS 2720
//       Project:  Sigma (840)
//       Description:
//			Added extern PressureSensor& RSecondExhPressureSensor.
//
//  Revision: 005  By:  quf    Date: 25-Sep-1997   DR Number: DCS 2410
//       Project:  Sigma (840)
//       Description:
//			Added atmospheric pressure xducer calibration.
//
//  Revision: 004  By:  quf    Date:  30-Jul-97    DR Number: DCS 2205
//       Project:  Sigma (840)
//       Description:
//			Removed conditional compile of Exh Valve Velocity Transducer test code.
//
//  Revision: 003  By:  syw    Date:  03-Jul-97    DR Number: DCS 2279
//       Project:  Sigma (R8027)
//       Description:
//             Declared extern FlowSensorCalibration& RFlowSensorCalibration.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  syw    Date: 01-Sep-1995    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================


class SmManager ;
extern SmManager& RSmManager ;

#if defined(SM_TEST)

class TestDriver ;
extern TestDriver& RTestDriver ;

#endif // defined(SM_TEST)

#if defined(SIGMA_BD_CPU)

class SmFlowController ;
extern SmFlowController& RSmFlowController ;

class SmPressureController ;
extern SmPressureController& RSmPressureController ;

class SmExhValveController ;
extern SmExhValveController& RSmExhValveController ;

class TestData ;
extern TestData& RTestData ;

class SmGasSupply ;
extern SmGasSupply& RSmGasSupply ;

class ExhValveCalibration ;
extern ExhValveCalibration& RExhValveCalibration ;

class ComplianceCalibration ;
extern ComplianceCalibration& RComplianceCalibration ;

class SmSensorMediator ;
extern SmSensorMediator& RSmSensorMediator ;

class ExhFlowSensor ;
extern ExhFlowSensor& RExhO2FlowSensor ;

class SmSensorMediator ;
extern SmSensorMediator& RSmSensorMediator ;

class SmUtilities ;
extern SmUtilities& RSmUtilities ;

class InspResistanceTest ;
extern InspResistanceTest& RInspResistanceTest ;

class SstFilterTest ;
extern SstFilterTest& RSstFilterTest ;

class GasSupplyTest ;
extern GasSupplyTest& RGasSupplyTest ;

class CircuitPressureTest ;
extern CircuitPressureTest& RCircuitPressureTest ;

class ExhValveSealTest ;
extern ExhValveSealTest& RExhValveSealTest ;

class ExhValvePerformanceTest ;
extern ExhValvePerformanceTest& RExhValvePerformanceTest  ;

class SafetySystemTest ;
extern SafetySystemTest& RSafetySystemTest  ;

class GeneralElectronicsTest ;
extern GeneralElectronicsTest& RGeneralElectronicsTest  ;

class ExhHeaterTest ;
extern ExhHeaterTest& RExhHeaterTest  ;

class LeakTest ;
extern LeakTest& RLeakTest  ;

class FsCrossCheckTest ;
extern FsCrossCheckTest& RFsCrossCheckTest  ;

class BdLampTest ;
extern BdLampTest& RBdLampTest ;

class BdAudioTest ;
extern BdAudioTest& RBdAudioTest ;

class FlowSensorCalibration ;
extern FlowSensorCalibration& RFlowSensorCalibration ;

class BatteryTest ;
extern BatteryTest& RBatteryTest ;

class ProxTest ;
extern ProxTest& RProxTest ;

class AtmPresXducerCalibration ;
extern AtmPresXducerCalibration& RAtmPresXducerCalibration ;

class DataKeyCopy ;
extern DataKeyCopy& RDataKeyCopy ;

class PressureSensor ;
extern PressureSensor& RSecondExhPressureSensor ;

#endif  // defined(SIGMA_BD_CPU)

#ifdef SIGMA_GUI_CPU

class GuiIoTest ;
extern GuiIoTest& RGuiIoTest ;

#endif //SIGMA_GUI_CPU

class SerialNumberInitialize ;
extern SerialNumberInitialize& RSerialNumberInitialize ;

class ForceVentInopTest ;
extern ForceVentInopTest& RForceVentInopTest ;

class CalInfoDuplication ;
extern CalInfoDuplication& RCalInfoDuplication ;

#endif // SmRefs_HH
