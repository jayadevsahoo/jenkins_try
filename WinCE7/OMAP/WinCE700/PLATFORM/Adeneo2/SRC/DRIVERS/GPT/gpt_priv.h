//------------------------------------------------------------------------------
//
// Copyright (C) 2006-2010, Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------

// File:  gpt_priv.h
//
// Private definitions for General Purpose Timer Driver
//
//------------------------------------------------------------------------------

#ifndef __GPT_PRIV_H__
#define __GPT_PRIV_H__

#include "omap_gptimer_regs.h"
#include "gptimer.h"

// Macros to create Unicode function name
#define WIDEN2(x) L ## x
#define WIDEN(x) WIDEN2(x)
#define __WFUNCTION__ WIDEN(__FUNCTION__)

//------------------------------------------------------------------------------
// Defines
#define GPT_FUNCTION_ENTRY() \
    DEBUGMSG(1, (TEXT("++%s\r\n"), __WFUNCTION__))
#define GPT_FUNCTION_EXIT() \
    DEBUGMSG(1, (TEXT("--%s\r\n"), __WFUNCTION__))

#ifdef DEBUG
// Debug zone bit positions
#define ZONEID_INIT                     0
#define ZONEID_DEINIT                   1
#define ZONEID_IOCTL                    2

#define ZONEID_INFO                     12
#define ZONEID_FUNCTION                 13
#define ZONEID_WARN                     14
#define ZONEID_ERROR                    15

// Debug zone masks
#define ZONEMASK_INIT                   (1<<ZONEID_INIT)
#define ZONEMASK_DEINIT                 (1<<ZONEID_DEINIT)
#define ZONEMASK_IOCTL                  (1<<ZONEID_IOCTL)

#define ZONEMASK_INFO                   (1<<ZONEID_INFO)
#define ZONEMASK_FUNCTION               (1<<ZONEID_FUNCTION)
#define ZONEMASK_WARN                   (1<<ZONEID_WARN)
#define ZONEMASK_ERROR                  (1<<ZONEID_ERROR)

// Debug zone args to DEBUGMSG
//#define ZONE_INIT                       DEBUGZONE(ZONEID_INIT)
#define ZONE_DEINIT                     DEBUGZONE(ZONEID_DEINIT)
#define ZONE_IOCTL                      DEBUGZONE(ZONEID_IOCTL)


#define ZONE_INFO                       DEBUGZONE(ZONEID_INFO)
#define ZONE_FUNCTION                   DEBUGZONE(ZONEID_FUNCTION)
#define ZONE_WARN                       DEBUGZONE(ZONEID_WARN)
//#define ZONE_ERROR                      DEBUGZONE(ZONEID_ERROR)
#endif


typedef struct {
    OMAP_GPTIMER_REGS *pGPT;
    CRITICAL_SECTION hGptLock;
    DWORD dwGptIntr;
    DWORD dwGptIrq;
    DWORD dwGptIndex;
    HANDLE hGptIntrEvent;
    HANDLE hTimerThread;
    HANDLE hTimerEvent ;
    LPCTSTR TimerEventString;
    BOOL bCurrentlyOwned;

    UINT32 TimerID;
	UINT32 Mode;			   // Freerun or Periodic
	UINT32 Period;
    UINT32 Posted;             // Timer posted mode seletion: 0x1: Posted, 0x0:non-posted mode

}OMAP_GPT_STRUCT,*POMAP_GPT_STRUCT;

//------------------------------------------------------------------------------
// Functions

DWORD GptInitialize(LPCTSTR pContext);
void GptRelease(POMAP_GPT_STRUCT pController);
BOOL GptTimerCreateEvent(POMAP_GPT_STRUCT pController, LPTSTR);
BOOL GptTimerReleaseEvent(POMAP_GPT_STRUCT pController, LPTSTR);
BOOL GptEnableTimer(POMAP_GPT_STRUCT pController);
void GptDisableTimer(POMAP_GPT_STRUCT pController);
void GptEnableTimerInterrupt(POMAP_GPT_STRUCT pController);
void GptDisableTimerInterrupt(POMAP_GPT_STRUCT pController);
BOOL GptResetTimer(POMAP_GPT_STRUCT pController);
BOOL GptGetTimerCount(POMAP_GPT_STRUCT pController, DWORD *pGptCntValue);
BOOL GptUpdateTimerPeriod(POMAP_GPT_STRUCT pController, DWORD Period);

//------------------------------------------------------------------------------
// Function prototypes
__inline UINT32 GptimerGetReg(POMAP_GPT_STRUCT pController, REG32 *addr);
__inline VOID GPT_TimerSetReg(POMAP_GPT_STRUCT pController, REG32 *addr, UINT32 val);
__inline VOID GptTimerSetCompare(POMAP_GPT_STRUCT pController, UINT32 compare);

VOID  GptTimerStart(POMAP_GPT_STRUCT pController);
VOID  GptTimerStop(POMAP_GPT_STRUCT pController);
//------------------------------------------------------------------------------

#endif   // __GPT_PRIV_H__
