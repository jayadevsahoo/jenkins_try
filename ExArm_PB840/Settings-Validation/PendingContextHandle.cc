#include "stdafx.h"

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//	      Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PendingContextHandle - Handle to the Pending Settings.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides an external interface to the Settings-Validation
//  Subsystem for the functionality relating to the "pending" settings.
//  When an acceptance of a batch of settings occurs, the BD-related
//  settings are copied to the Pending Context, where they await the
//  phase-in process.  This class provides static methods for:  querying
//  whether a particular setting is currently pending, getting the value
//  of the currently pending settings, and registering for callback
//  notification of pending setting changes.
//
//  The purpose of this class is to provide an interface for all of the
//  other subsystems that need to interact with the pending settings.
//  This class provides a clear demarcation line between the functionality
//  of this subsystem's internal classes, and the functionality needed by
//  other subsystems.
//
//  This class is only available on the Breath-Delivery CPU.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a clear border between the Settings-Validation
//  Subsystem, and the subsystems that need functionality relating to the
//  pending settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All of the interface methods are set up as static, non-inline methods.
//  The fact that the methods are static, allows the other subsystems to
//  have access to the needed functionality, without needing access to an
//  instance of the class; direct calls to these methods could be made.
//  By making all of the methods non-inline, the client's dependency on
//  the classes and types needed for the implementation only is eliminated.
//  In other words, the non-inline methods allow for the header files needed
//  for the implementation to be included in the source file and NOT the
//  header file.
//
//  None of these methods are set-up, explicitly, for multiple thread
//  protection.  It is up to the Pending Context to protect its reads from
//  possible update threads.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PendingContextHandle.ccv   25.0.4.0   19 Nov 2013 14:27:34   pvcs  $
//
//@ Modification-Log
//  Revision: 006 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 005   By: sah  Date:  28-Feb-2000    DR Number:  5327
//  Project:  NeoMode
//  Description:
//  Updated for NeoMode
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  using new, BD-only 'SettingCallbackMgr' class
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "PendingContextHandle.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "PendingContext.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsVentStartupPending(settingId)  [static]
//
//@ Interface-Description
//  Is there currently a Vent-Startup update pending in the Pending
//  Context?
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward on to the Pending Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
PendingContextHandle::IsVentStartupPending(void)
{
  CALL_TRACE("IsVentStartupPending()");

  return(ContextMgr::GetPendingContext().isVentStartupPending());
}  // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetBoundedValue(boundedId)  [static]
//
//@ Interface-Description
//  Return a copy of the value of the bounded setting indicated by
//  'boundedId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward on to the Pending Context.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBdBoundedId(boundedId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
PendingContextHandle::GetBoundedValue(const SettingId::SettingIdType boundedId)
{
  CALL_TRACE("GetBoundedValue(boundedId)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdBoundedId(boundedId)));

  return(ContextMgr::GetPendingContext().getBoundedValue(boundedId));
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetDiscreteValue(discreteId)  [static]
//
//@ Interface-Description
//  Return a copy of the value of the discrete setting indicated by
//  'discreteId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward on to the Pending Context.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBdDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscreteValue
PendingContextHandle::GetDiscreteValue(const SettingId::SettingIdType discreteId)
{
  CALL_TRACE("GetDiscreteValue(discreteId)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdDiscreteId(discreteId)));

  return(ContextMgr::GetPendingContext().getDiscreteValue(discreteId));
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetVentStartupFlag()  [static]
//
// Interface-Description
//  Set the Pending Context's Vent-Startup flag to 'FALSE'.
//---------------------------------------------------------------------
// Implementation-Description
//  Forward on to the Pending Context.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  (PendingContextHandle::IsVentStartupPending())
// End-Method
//=====================================================================

void
PendingContextHandle::SetVentStartupFlag(void)
{
  CALL_TRACE("SetVentStartupFlag()");

  ContextMgr::GetPendingContext().setVentStartupFlag();
  SAFE_CLASS_ASSERTION((PendingContextHandle::IsVentStartupPending()));
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetBoundedValue(boundedId, boundedValue)  [static]
//
// Interface-Description
//  Set the value of the bounded setting identified by 'boundedId', to
//  the value given by 'boundedValue'.
//---------------------------------------------------------------------
// Implementation-Description
//  Forward on to the Pending Context.
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsBdBoundedId(boundedId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
PendingContextHandle::SetBoundedValue(
				  const SettingId::SettingIdType boundedId,
				  const BoundedValue&            boundedValue
				     )
{
  CALL_TRACE("SetBoundedValue(boundedId, boundedValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdBoundedId(boundedId)));

  ContextMgr::GetPendingContext().setBoundedValue(boundedId,
						  boundedValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetDiscreteValue(discreteId, discreteValue)  [static]
//
// Interface-Description
//  Set the value of the discrete setting identified by 'discreteId', to
//  the value given by 'discreteValue'.
//---------------------------------------------------------------------
// Implementation-Description
//  Forward on to the Pending Context.
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsBdDiscreteId(discreteId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
PendingContextHandle::SetDiscreteValue(
				const SettingId::SettingIdType discreteId,
				DiscreteValue                  discreteValue
				      )
{
  CALL_TRACE("SetDiscreteValue(discreteId, discreteValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdDiscreteId(discreteId)));

  ContextMgr::GetPendingContext().setDiscreteValue(discreteId,
  						   discreteValue);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RegisterSettingEvent(settingId, pEventCallback)  [static]
//
//@ Interface-Description
//  Register the callback given by 'pEventCallback', such that when the
//  setting indicated by 'settingId' is received from the Accepted Context,
//  the callback will be activated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward on to the Pending Context.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBdId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PendingContextHandle::RegisterSettingEvent(
			const SettingId::SettingIdType         settingId,
			SettingCallbackMgr::SettingCallbackPtr pEventCallback
				          )
{
  CALL_TRACE("RegisterSettingEvent(settingId, pEventCallback)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdId(settingId)));

  SettingCallbackMgr::RegisterSettingCallback(settingId, pEventCallback);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PendingContextHandle::SoftFault(const SoftFaultID  softFaultID,
			        const Uint32      lineNumber,
			        const char*       pFileName,
			        const char*       pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  PENDING_CONTEXT_HANDLE, lineNumber, pFileName,
			  pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
