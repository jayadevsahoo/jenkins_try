#ifndef Fio2Monitor_IN
#define Fio2Monitor_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
// Class: Fio2Monitor - implements the functionality required for 
// interfacing with the FiO2 monitor.
//---------------------------------------------------------------------
// @ Version-Information
// @ (#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/Fio2Monitor.inv   10.7   08/17/07 09:29:44   pvcs  
//
// @ Modification-Log
//
//  Revision: 002 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsInstalled 
//
//@ Interface-Description
//  This method takes no parameters and returns a Boolean value
//  indicating if the FiO2 Monitor is installed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the current status of private data member "isInstalled_",
//  which is update by newCycle().
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
inline Boolean 
Fio2Monitor::IsInstalled( void )
{
    CALL_TRACE("IsInstalled( void )");
	 
	// $[TI1]
	return( IsInstalled_ );
}
			  
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetTargetMix
//
//@ Interface-Description
//  This method accepts a parameter for the new targeted O2 percent and
//  has no return type.  It provides the interface for the Breath-Delivery
//  subsystem to communicate any changes that occur to the targeted
//  delivered O2 percent.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method sets the private data member targetO2Percent_ to the 
//  value of the parameter.  This method must be called during
//  initialization, before the first invocation of the newCycle method.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
inline void
Fio2Monitor::SetTargetMix( const Real32 newMix )
{
	CALL_TRACE("setTargetMix( const Real32 newMix )");

	// $[TI1]
  	TargetO2Percent_ = newMix;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerAlarmCallBack
//
//@ Interface-Description
//  This method is used by a client that needs to be notified when an
//	alarm detected by the Fio2Monitor class is detected.  The method
//  takes a pointer to a function argument and has no return value.
//  Note:  This call back provides service for one client.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The private data member that stores the pointer to the called back
//  function is assigned the argument passed.
//---------------------------------------------------------------------
//@ PreCondition
//	None
//---------------------------------------------------------------------
//@ PostCondition
//	None
//@ End-Method
//=====================================================================
inline void
Fio2Monitor::registerAlarmCallBack(PAlarmCallBack pAlarmCallBack)
{
    CALL_TRACE("Fio2Monitor::registerAlarmCallBack(PAlarmCallBack \
                pAlarmCallBack)");
    // $[TI1]
    pAlarmCallBack_ = pAlarmCallBack;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isFio2SensorOor() 
//
//@ Interface-Description
//  This method takes no parameters and returns a Boolean that
//  indicates if the FiO2 Sensor is Out-Of-Range (OOR: malfunctioning).
//---------------------------------------------------------------------
//@ Implementation-Description
//  Return the value of isO2SensorOor_.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

Boolean inline
Fio2Monitor::isFio2SensorOor( void ) const
{
    // $[TI1]
    CALL_TRACE("Fio2Monitor::isFio2SensorOor( void )");
    
	return( isO2SensorOor_ );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCurrentCalculatedO2Percent()
//
//@ Interface-Description
//  This method takes no parameters and returns a constant Real32.
//  This method returns the last calculated delivered O2 percentage
//  to the client.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Return the value of currentO2Percentage_.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
Real32 inline
Fio2Monitor::getCurrentCalculatedO2Percent( void ) const
{
	CALL_TRACE("Fio2Monitor::getCurrentCalculatedO2Percent( void )");

    // $[TI1]
	return( currentO2Percentage_ );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetAlarmResetStatus() 
//
//@ Interface-Description
//  This method takes no parameters and returns no value.
//  This method set the alarm reset flag in this object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets data member AlarmResetOccurred_ to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
void inline
Fio2Monitor::SetAlarmResetStatus( void )
{
    // $[TI1]
    CALL_TRACE("Fio2Monitor::SetAlarmResetStatus( void )");

	AlarmResetOccurred_ = TRUE;
}

//==================================================================
//
//  Protected Methods...
//
//==================================================================

//==================================================================
//
//  Private Methods...
//
//==================================================================

#endif  //Fio2Monitor_IN

