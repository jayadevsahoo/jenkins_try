#ifndef Download_HH
#define Download_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: Download - provides capability to down load  a Sigma
//       executable image file to a TARGET flash memory. 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/Download.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 004  By: gdc  Date: 22-Feb-2005  DR Number: 6148
//    Project:  NIV1
//    Description:
//       DCS 6148 - move to solaris
//       implemented GetDownloader() to instantiate Download object instead
//       of using placemente new() which is not supported by GNU compiler
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  jhv    Date:  01-05-96    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "DownloadCons.hh"
#include "DownloadTypes.hh"
#include "DownloadError.hh"
#include "sockintf.hh"
#include "DownloadFault.hh"
#include "TftpSocket.hh"
#include "UDPSocket.hh"

#ifdef SIGMA_CLIENT
#include <vrtxil.h>
#endif
#ifdef SIGMA_SERVER
#  include <unistd.h>
#endif

struct ErrMsg 
{ 
  char *msg; Int code;
} ;

#if	defined(SIGMA_CLIENT)
extern TargetTypes BoardType(void);
class Queue;
class FlashMem;
#endif //defined(SIGMA_CLIENT)

class Download 
{
  public:
    Download(void);    
    ~Download(void);   			
    static void Initialize(void);
    static Download& GetDownloader(void);

#if	defined(SIGMA_SERVER)
	static inline void DelayTask(Uint delayMilli );
#endif //defined(SIGMA_SERVER)
#if defined(SIGMA_CLIENT)
	static inline void DelayTask(Uint delayMilli );
#endif  //defined(SIGMA_CLIENT)

	static void ReportError(char *msg, Int errorCode );
	static void ReportError(char *format, char *msg); 
	static inline void ReportError(char *msg );
	static void Task(void);
	static void DisplayMessage(char *msg);
	static void DisplayClientError(char *eMsg=NULL,Uint errCode = 0);
#if	defined(SIGMA_SERVER)
    void serverTask(void);
	static void SetGuiBootFile(char *);
	static void SetBdBootFile(char *);
#endif //defined(SIGMA_SERVER)
#if defined(SIGMA_CLIENT)
	void clientTask(void);

	//@Data-Member: RFlashMemQueue 
	// Flash Memory queue object.
	static Queue &RFlashMemQueue;

	//@Data-Member: RFlashMemBurner 
	// Flash Memory burner object.
	static FlashMem &RFlashMemBurner;

#endif  //defined(SIGMA_CLIENT)

	
//TODO E600 commented out
	//static Int WaitForInput(xsFDSet_t *preadMask,const Int32 waitTime);

    static void SoftFault(const FaultType softFaultID,
		const Uint32      lineNumber,
		const char*       pFileName  = NULL, 
		const char*       pPredicate = NULL);
#ifdef SIGMA_UNIT_TEST
	static void SetInitialized(Boolean value);
#endif // SIGMA_UNIT_TEST

  protected:

  private:
    Download(const Download&);		// not implemented...
    void   operator=(const Download&);	// not implemented...
    //@ Data-Member: Initialized_
	//  a static flag to indicate whether the download class has been
	//  initialized or not.
	static Boolean Initialized_;
#if	defined(SIGMA_SERVER)
	Int acceptConnection_(void);
	SigmaStatus broadcastClientIpAddr_(void);
	Int32 getNextClientIpAddr_(void);
#elif defined(SIGMA_CLIENT)
	SigmaStatus obtainClientIpAddr_(void);
#endif //defined(SIGMA_CLIENT)

	//@ Data-Member: connections_
	//  An array of TFTP socket connections between the server
	//  and clients.

	//TODO E600 commented out
    //TftpSocket  connections_[MAX_CONNECTIONS];

	//@ Data-Member: clientIpAddr_;
	// Ip address of the next client machine.
	Uint32 clientIpAddr_;

	//@ Data-Member: ErrTbl_[]
	//  This table contains all the TFTP client reported error
	//  code and its corresponding error messages.
	static ErrMsg ErrTbl_[] ;

	//@ Data-Member: mainSocket_
	//  The main socket used by a tftp server for listening for a new
	//  tftp connection request from a tftp client.
    NetSocket mainSocket_;

#if	defined(SIGMA_SERVER)
	//@ Data-Member: PGuiBootFile_
	//  A boot file name for the GUI board.
	static char *PGuiBootFile_;

	//@ Data-Member: PBdBootFile_
	//  A boot file name for the BD board.
	static char *PBdBootFile_;

#elif defined(SIGMA_CLIENT)

	//@ Data-Member: bootImageFile_;
	// name of the boot image file that needs to be downloaded.
	char bootImageFile_[MAX_FILE_NAME_LENGTH+1];

#endif //defined(SIGMA_CLIENT)

	//@ Data-Member: serverIpAddr_;
	// Ip address of the server  machine.
	Uint32 serverIpAddr_;

	//@ Data-Member: broadcastSocket_
	//  A udp socket used to broadcast information and gather
	//  broadcast data.
	UDPSocket broadcastSocket_;
};

//include inline methods
#include "Download.in"

#endif // Download_HH 
