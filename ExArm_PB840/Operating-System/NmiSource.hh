#ifndef NmiSource_HH
#define NmiSource_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================
 
 
//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: NmiSource.hh - NMI Source Register Definition
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/NmiSource.hhv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 26-Apr-1999  DCS Number: 5370
//    Project: 840 Cost Reduction
//    Description:
//       Corrected revision code conflict.
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================
 
 
enum NmiSource
{
     NMI_VENT_INOP_B                  = 0x20
    ,NMI_ANALOG_INTERFACE_ERROR       = 0x10 
    ,NMI_POWER_FAIL                   = 0x04 
    ,NMI_VENT_INOP_A                  = 0x08 
    ,NMI_SAAS_FAILURE                 = 0x20 
    ,NMI_UNDER_12VDC                  = 0x10 
    ,NMI_OVER_12VDC                   = 0x08
    ,NMI_OVER_5VDC                    = 0x04 
    ,NMI_ETHERNET_PARITY_ERROR        = 0x02 
    ,NMI_DRAM_PARITY_ERROR            = 0x01

    ,NMI_REV_MASK                     = 0xC0
    ,NMI_REVP3_BOARD                  = 0x40
    ,NMI_REVA_BOARD                   = 0xC0
    ,NMI_REVB_BOARD                   = 0x80
    ,NMI_COST_REDUCTION               = 0x00
};
 
#endif // NmiSource_HH
