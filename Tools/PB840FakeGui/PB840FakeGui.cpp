
// PB840FakeGui.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "PB840FakeGui.h"
#include "MainFrm.h"

#include "PB840SettingsDoc.h"
#include "PB840SettingsView.h"

#include "NetworkApp.hh"
#include "SettingsXaction.hh"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPB840FakeGuiApp

BEGIN_MESSAGE_MAP(CPB840FakeGuiApp, CWinAppEx)
	ON_COMMAND(ID_APP_ABOUT, &CPB840FakeGuiApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinAppEx::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinAppEx::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinAppEx::OnFilePrintSetup)
END_MESSAGE_MAP()


// CPB840FakeGuiApp construction

CPB840FakeGuiApp::CPB840FakeGuiApp()
{

	m_bHiColorIcons = TRUE;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// The one and only CPB840FakeGuiApp object

CPB840FakeGuiApp theApp;


// CPB840FakeGuiApp initialization

BOOL CPB840FakeGuiApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(4);  // Load standard INI file options (including MRU)



	// start user initialization
	{
		// initialize network
		WSADATA wsaData;
		int		iResult;
		// initiates use of Winsock DLL
		iResult = WSAStartup( MAKEWORD(2,2), &wsaData );

		// initialize the setting table
		SettingTable::Initialize();

		// initialize the events for sync
		Event::Initialize();

		// initialize network sockets
		NetworkApp::Initialize();

		// create worker threads
		ThreadNetworkApp_ = AfxBeginThread(NetworkAppThreadProc, NULL);
		ThreadSettingXaction_ = AfxBeginThread(SettingXactionThreadProc, NULL);

		// wait for Xaction thread started
		HANDLE hEvent = Event::Query( Event::EVENT_SETTING_XACTION );
		WaitForSingleObject( hEvent, INFINITE );
	} // end user initialization



	InitContextMenuManager();

	InitKeyboardManager();

	InitTooltipManager();
	CMFCToolTipInfo ttParams;
	ttParams.m_bVislManagerTheme = TRUE;
	theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL,
		RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CPB840SettingsDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CPB840SettingsView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);


	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);


	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();
	return TRUE;
}



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CPB840FakeGuiApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CPB840FakeGuiApp customization load/save methods

void CPB840FakeGuiApp::PreLoadState()
{
	BOOL bNameValid;
	CString strName;
	bNameValid = strName.LoadString(IDS_EDIT_MENU);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
}

void CPB840FakeGuiApp::LoadCustomState()
{
}

void CPB840FakeGuiApp::SaveCustomState()
{
}

// CPB840FakeGuiApp message handlers




BOOL CPB840FakeGuiApp::ExitInstance()
{
	// notify worker threads exit
	::PostThreadMessage( ThreadSettingXaction_->m_nThreadID, WM_QUIT, 0, 0);

	// wait for Xaction thread exited
	HANDLE hEvent = Event::Query( Event::EVENT_SETTING_XACTION );
	WaitForSingleObject( hEvent, INFINITE );

	return TRUE;
}


UINT SettingXactionThreadProc( LPVOID pParam )
{
	SettingsXaction::TaskDriver();
	return 0;
}

UINT NetworkAppThreadProc( LPVOID pParam )
{
	NetworkApp::Task();
	return 0;
}

BOOL CPB840FakeGuiApp::PostWorkerThreadMessage(Uint32 thread, Uint32 msg, WPARAM w, LPARAM l)
{
	Uint32 threadId = 0;

	switch ( thread )
	{
	case NetworkAppThread:
		threadId = ThreadNetworkApp_->m_nThreadID;
		break;

	case XactionThread:
		threadId = ThreadSettingXaction_->m_nThreadID;
		break;

	default:
		break;
	}

	if ( threadId == 0 )
		return FALSE;

	return ::PostThreadMessage( threadId, msg, w, l );
}