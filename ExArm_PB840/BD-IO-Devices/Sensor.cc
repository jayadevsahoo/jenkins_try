#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Sensor - Base class for all sensors
//---------------------------------------------------------------------
//@ Interface-Description
//      The Sensor class collaborates with the AdcChannels class to
//      get sampled values.  Each sensor has an ADC channel where raw data
//		is read from.  A sensor supplies the user with services to get the
//		most recent BD cycle sampled value (either in counts or engineering
//		units) and also to get the new sensor value without interfering with
//		the CYCLE_TIME_MS sample.  The Sensor instances are initialized in
//      BD I/O devices using the sensor mediator.  Methods are implemented
// 		to return the sensor's engineering value; to return the sensor's adc
//		count; to update the sensor's engineering value; to return a freshly
//		read sensor value converted into an engineering value; to convert AdcCounts
//		counts into engineering units (Virtual method).
//
//		This class provides support for the BD_CPU and the GUI_CPU.
//---------------------------------------------------------------------
//@ Rationale
//      Provides a common interface to all sensors.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The rawCount_ is updated/read by GetBdCycleSample()/getCount().
//      The engValue_ is updated/read by updateValue()/getValue().
//		The virtual method rawToEngValue_() is used by updateValue() to
//      convert the AdcCount to engineering units.
//
//      The virtual method rawToEngValue is defined in each derived
//      class.  Each instance contains the ADC channel number maintaining a
//      link to the ADC conversion hardware.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      None
//---------------------------------------------------------------------
//@ Invariants
//      rawCount_ has a range of 0 to MAX_COUNT_VALUE
//      adcId_ has a range of 0 to NUM_ADC_CHANNELS - 1
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Sensor.ccv   25.0.4.0   19 Nov 2013 13:54:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 010 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 009 By: iv     Date: 28-Apr-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added unit test code.
//
//  Revision: 008 By: syw    Date: 07-Oct-1997   DR Number: DCS 2537
//  	Project:  Sigma (R8027)
//		Description:
//			Modified updateValue() to always update the engValue_ via
//			rawToEngValue_() method.  The pSafetyNetSensorData_->checkRange()
//			will modify rawCount (if a reading is out of range) to ensure
//			that the engValue_ will be reasonable.
//
//  Revision: 007 By: syw    Date: 19-May-1997   DR Number: DCS 1620
//  	Project:  Sigma (R8027)
//		Description:
//			Check for !STATE_SERVICE && !STATE_SST instead of STATE_ONLINE
//			so that the safetyNetSensorData is active during initialization.
//
//  Revision: 006 By: syw    Date: 22-Aug-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate compiler warnings.
//
//  Revision: 005 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Don't call rangeCheck() during Service.
//
//  Revision: 004 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added BD_CPU macros.  Added constructor for GUI CPU.
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SafetyNetSensorData * to constructor to initialize the
//			data member.  Call pSafetyNetSensorData->checkRange() method in
//			updateValue() method if pointer is defined and update engValue_
//			only if the sample is within the safety net range .
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sensor.hh"

//@ Usage-Classes

#include "AdcChannels.hh"

//@ End-Usage

//@ Code...

#ifdef SIGMA_UNIT_TEST
#include "MainSensorRefs.hh"
extern Int32 UnitTestNum;
#endif // SIGMA_UNIT_TEST

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Sensor()
//
//@ Interface-Description
//      Constructor.  This method takes an ADC channel Id and a pointer to
//		a SafetyNetSensorData as arguments for the BD_CPU and no arguments
//		for the GUI_CPU.  The arguments passed in are used to set the sensor's
//		id and pointer to SafetyNetSensorData .
//---------------------------------------------------------------------
//@ Implementation-Description
//		BD_CPU implementation:
//      The AdcChannel id is set to the sensor's channel id.  The
//		SafetyNetSensorData pointer is set to pSafetyNetSensorData_.
//      The other data members are initialized after construction
//      of all the BD I/O objects.
//
//		GUI_CPU implementation:
//		none
//---------------------------------------------------------------------
//@ PreCondition
//      The channel id has a range of 0 to NUM_ADC_CHANNELS (enforced by an
//      enum type).
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

Sensor::Sensor( const AdcChannels::AdcChannelId id, SafetyNetSensorData *pData)
: adcId_(id)
, pSafetyNetSensorData_( pData)      // $[TI1]
#ifdef INTEGRATION_TEST_ENABLE
, m_rValue(engValue_, id)
#endif // INTEGRATION_TEST_ENABLE
{
	CALL_TRACE("Sensor::Sensor( const AdcChannels::AdcChannelId id, \
				SafetyNetSensorData *pData)") ;
}

Sensor::Sensor( const AdcChannels::AdcChannelId id)
: adcId_(id)      // $[TI1]
#ifdef INTEGRATION_TEST_ENABLE
, m_rValue(engValue_, id)
#endif // INTEGRATION_TEST_ENABLE
{
	CALL_TRACE("Sensor::Sensor( const AdcChannels::AdcChannelId id)") ;
}

#else

Sensor::Sensor( void)
{
	CALL_TRACE("Sensor::Sensor( void)") ;
}

#endif // defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Sensor ()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Sensor::~Sensor( void)
{
	CALL_TRACE("Sensor::~Sensor( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValue()
//
//@ Interface-Description
//      This method is used every CYCLE_TIME_MS to convert the
//      raw sampled value for this sensor into a value in the
//      correct engineering units.  This value is then stored
//      as the last sampled value. This method is preceded by
//      the method AdcChannels::getBdCycleSampled() to update the raw count.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The method is using the rawToEngValue_() virtual method
//      passing it as an argument rawCount_. A check is	done on the raw
//		count by the safety net subsystem.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

void
Sensor::updateValue( void) 
{
	CALL_TRACE("Sensor::updateValue( void)") ;
    
    AdcCounts rawCount = AdcChannels::GetBdCycleSample( adcId_) ;

#ifdef SIGMA_UNIT_TEST
if ( UnitTestNum == 3)
{
    if (this == (Sensor *)&RO2TemperatureSensor)
    {
       // exceed the high bound
       rawCount = 4000U;
    }
}
#endif // SIGMA_UNIT_TEST

    // TODO E600_LL: to be reviewed when SafetyNet is ready
	/* if (pSafetyNetSensorData_ != NULL)
    {
	    // $[TI1.1]
		pSafetyNetSensorData_->checkRange( rawCount) ;
	    // Note that the checkRange method may have changed the rawCount (to
        // a default value) if the sensor reading was out of range.
	}*/ // implied else $[TI1.2]

	engValue_ = rawToEngValue_( rawCount) ;
    rawCount_ = rawCount ;
}

#endif // defined (SIGMA_BD_CPU)


#if defined (SIGMA_BD_CPU)
#ifdef INTEGRATION_TEST_ENABLE
//------------------------------------------------------------------------------
// updateVirtualValue
//------------------------------------------------------------------------------
void 
Sensor::updateVirtualValue()
{
    swat::VirtualSensorBuffer::update((unsigned int)adcId_);
}

#endif // INTEGRATION_TEST_ENABLE
#endif // defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
 
void
Sensor::SoftFault( const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    
    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, SENSOR, lineNumber,
                             pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: rawToEngValue_()
//
//@ Interface-Description
//      This method takes counts as an argument and returns volts.
//      This is a virtual method and implements a conversion from
//      counts to volts for the base class.  Each derived class
//      must define how to convert from counts to engineering units.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Return the volts converted from counts.
//---------------------------------------------------------------------
//@ PreCondition
//      count must be between 0 and MAX_COUNT_VALUE
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Real32
Sensor::rawToEngValue_( const AdcCounts count) const
{
	CALL_TRACE("Sensor::rawToEngValue_( const AdcCounts count)") ;

    CLASS_PRE_CONDITION( count <= MAX_COUNT_VALUE ) ;

    // $[TI1]

    Real32 value = (Real32)(count) * COUNT_TO_VOLT_FACTOR ; 
    return( value) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================










