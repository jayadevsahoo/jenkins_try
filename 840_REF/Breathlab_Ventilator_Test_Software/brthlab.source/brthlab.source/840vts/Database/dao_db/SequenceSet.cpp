// SequenceSet.cpp : implementation file
//

#include "stdafx.h"
#include "daocpvtdb.h"
#include "SequenceSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSequenceSet

IMPLEMENT_DYNAMIC(CSequenceSet, CDaoRecordset)

CSequenceSet::CSequenceSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CSequenceSet)
	m_ID = 0;
	m_time = (DATE)0;
	m_User = _T("");
	m_P_F = 0;
	m_UUTPN = _T("");
	m_UUTBDSN = _T("");
	m_UUTBDRunTime = _T("");
	m_UUTRev = _T("");
	m_BDPost = _T("");
	m_GUISN = _T("");
	m_GUIPost = _T("");
	m_Saas = _T("");
	m_CompSN = _T("");
	m_CompRunTime = _T("");
	m_PTSSN = _T("");
	m_PTSVer = _T("");
	m_PTSLastCal = _T("");
	m_PTSNextCal = _T("");
	m_PTSOutofCal = FALSE;
	m_VentAsset = _T("");
	m_WorkOrder = _T("");
	m_nFields = 21;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CSequenceSet::GetDefaultDBName()
{
	return _T(DB_PATH_NAME);
}

CString CSequenceSet::GetDefaultSQL()
{
	return _T("[Sequences]");
}

void CSequenceSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CSequenceSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ID]"), m_ID);
	DFX_DateTime(pFX, _T("[time]"), m_time);
	DFX_Text(pFX, _T("[User]"), m_User);
	DFX_Long(pFX, _T("[P/F]"), m_P_F);
	DFX_Text(pFX, _T("[UUTPN]"), m_UUTPN);
	DFX_Text(pFX, _T("[UUTBDSN]"), m_UUTBDSN);
	DFX_Text(pFX, _T("[UUTBDRunTime]"), m_UUTBDRunTime);
	DFX_Text(pFX, _T("[UUTRev]"), m_UUTRev);
	DFX_Text(pFX, _T("[BDPost]"), m_BDPost);
	DFX_Text(pFX, _T("[GUISN]"), m_GUISN);
	DFX_Text(pFX, _T("[GUIPost]"), m_GUIPost);
	DFX_Text(pFX, _T("[Saas]"), m_Saas);
	DFX_Text(pFX, _T("[CompSN]"), m_CompSN);
	DFX_Text(pFX, _T("[CompRunTime]"), m_CompRunTime);
	DFX_Text(pFX, _T("[PTSSN]"), m_PTSSN);
	DFX_Text(pFX, _T("[PTSVer]"), m_PTSVer);
	DFX_Text(pFX, _T("[PTSLastCal]"), m_PTSLastCal);
	DFX_Text(pFX, _T("[PTSNextCal]"), m_PTSNextCal);
	DFX_Bool(pFX, _T("[PTSOutofCal]"), m_PTSOutofCal);
	DFX_Text(pFX, _T("[VentAsset]"), m_VentAsset);
	DFX_Text(pFX, _T("[WorkOrder]"), m_WorkOrder);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CSequenceSet diagnostics

#ifdef _DEBUG
void CSequenceSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CSequenceSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
