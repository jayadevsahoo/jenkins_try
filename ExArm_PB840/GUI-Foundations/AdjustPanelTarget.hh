#ifndef AdjustPanelTarget_HH
#define AdjustPanelTarget_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AdjustPanelTarget - This abstract class allows callbacks from
// the AdjustPanel.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/AdjustPanelTarget.hhv   25.0.4.0   19 Nov 2013 14:11:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: gdc      Date: 21-May-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//	Implemented TargetType to identify feedback sensitive focus
//  objects.
//  
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//  
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"

//@ Usage-Classes
//@ End-Usage

class AdjustPanelTarget
{
public:
	enum TargetType
	{
		TARGET_TYPE_NULL,
		TARGET_TYPE_SETTING
	};

	virtual void adjustPanelKnobDeltaHappened(Int32 delta);
	virtual void adjustPanelLoseFocusHappened(void);
	virtual void adjustPanelRestoreFocusHappened(void);
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelAcceptReleaseHappened(void);
	virtual void adjustPanelClearPressHappened(void);
	virtual AdjustPanelTarget::TargetType getTargetType(void) const;

protected:

	AdjustPanelTarget(void);
	virtual ~AdjustPanelTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	AdjustPanelTarget(const AdjustPanelTarget&); // not implemented...
	void operator=(const AdjustPanelTarget&);	 // not implemented...
};


#endif // AdjustPanelTarget_HH 
