#if !defined(AFX_TOPBASEWINDOW_H__03048BFA_F128_432A_80B4_CEEFE87D4D5D__INCLUDED_)
#define AFX_TOPBASEWINDOW_H__03048BFA_F128_432A_80B4_CEEFE87D4D5D__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// topbasewindow.h : header file
//

#include "ApplicationMessages.h"
#include "MonitorsThread.h"
#include "AioDioDriver.h"
#include "MembraneButtons.h"
//#include "MsgQue.h"

#define READ_MEMBRANE_RATE_MS           (100)

static const int INITIAL_REPEAT_DELAY = (800 / READ_MEMBRANE_RATE_MS) + 1;       // plus one because it decrements on the first call
static const int REPEAT_DELAY = 200 / READ_MEMBRANE_RATE_MS;
static const int REPEAT_COUNT = 5;

/////////////////////////////////////////////////////////////////////////////
// TopbaseWindow_t dialog
#include <vector>
using namespace std;

/*typedef struct tagSingleMessg
{
	DWORD	size;
}SINGLEMESSG;

typedef struct tagMsgQueue
{
	WCHAR			Name[256];
	MSGQUEUEOPTIONS	MsgQueueOpts;
	DWORD			dwMessages;
	DWORD			dwQueueSize;
	LPVOID			lpData; //organized as DWORD size,  data of length size

}MSGQUEUE;
*/
UINT SWATServerThreadProc(LPVOID pParam);

class TopBaseWindow_t : public CDialog, public MsgQue_t  
{
// Construction
public:
	TopBaseWindow_t(UINT IDTemplate, CWnd* ParentPtr);   

    public:

		virtual HWND GetScreenHwnd()=0;
		bool SendMail(uint8_t *MessagePtr, uint16_t MessageSize, int32_t Timeout=0);

		//Pure virtual functions so that the derived class must implemenent and calls to
		//these functions are directed to derived class handlers
		virtual void MembraneButtonPressed(WPARAM wButton, LPARAM RepeatCount, BOOL SendToWaveScreen=FALSE)=0;
		virtual void MembraneButtonReleased(WPARAM wButton, BOOL SendToWaveScreen=FALSE)=0;
		virtual void RePostMessage(HWND hwnd,UINT msg,WPARAM wParam, LPARAM lParam)=NULL;

		virtual void UpdateGraph()=0;

		void ProcessMembraneButtons(BOOL SendToWaveScreen=FALSE);
		virtual LRESULT OnEndDragAndDrop(WPARAM x, LPARAM y)=NULL;
		virtual LRESULT OnVerifyPtInWaveArea(WPARAM x,LPARAM y)=NULL;
		virtual LRESULT WavesSetCursorLocationDirect(WPARAM x, LPARAM y)=NULL;

		virtual void HandleIntraVentMsg(IntraVentMsgIds_t msg, uint8_t* data);

    protected:
		bool GetMail(uint8_t *MessagePtr, uint32_t Timeout=0);//Never block
        bool GetMail(uint8_t *MessagePtr, uint32_t *BytesRead, uint32_t Timeout=0);//Never block
		void ProcessMembraneBtn(int keyId, bool btnPressed, BOOL SendToWaveScreen=FALSE);

        HANDLE ReadHandle;
        HANDLE WriteHandle;

        int32_t MaxEntrySize;
        uint32_t ErrVal;

		int                 RepeatDelay;
		int                 RepeatCount;
		bool                ButtonPreviouslyPressed[NUMBER_OF_MEMBRANE_BUTTONS];

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TopBaseWindow_t)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	virtual LRESULT WindowProc(
	   UINT message,
	   WPARAM wParam,
	   LPARAM lParam 
	);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TOPBASEWINDOW_H__0B048BFA_F128_432A_80B4_CEEFE87D4D5D__INCLUDED_)
