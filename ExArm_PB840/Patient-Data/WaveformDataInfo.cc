#include "stdafx.h"

#if        defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//              Copyright (c) 199t, NellCor/Puritan-Bennett Corporation
//=====================================================================

//================== C L A S S   D E S C R I P T I O N ================
//@ Class:  WaveformDataInfo - Waveform Data Manager class.
//---------------------------------------------------------------------
//@ Interface-Description
//  
//>Von
//    Description:
//    ------------
//     This class provides a waveform data processing agent. A Waveform
//     data agent mainly buffers waveform data from the network
//     in a circular buffer. It also provides ways for other subsystems
//     to access one waveform data at a time from the buffer in the order
//     that the data was arrived. The order of data arrival is maintained
//     in the buffer pool.
//     Large number of buffers are allocated in the circular buffer pool
//     to avoid data loss in the case that data arrival rate exceeds
//     data consumption rate. 
//
//     Waveform buffer is explicitly activated and de-activated by
//     other subsystems. While the waveform buffer is deactivated, 
//     new waveform data gets discarded. 
//
//    Class Features:
//    ---------------
//        1. Maintain a circular buffer pool to store waveform data.
//        2. Maintain tail and head pointers to the buffer, head to
//         add newly arriving data and tail to point to earliest
//           data in the buffer.
//      3. Activate and deactivate the buffer pool, while the buffer is
//            deactivated, new data is discarded and waveform data access
//            is illegal.
//    Class Key methods:
//    ------------------
//    addData  --  adds the pass-in waveform data to the head of the buffer.
//    getData  --  gets the waveform data from the tail end of the
//                 buffer and takes the data off the buffer.
//    activate --  activate the waveform data buffer, starts accumulation
//                  of the newly arrived waveform data.
//      deactivate -- deactivates the waveform data buffer, discards 
//                  existing waveform data in the buffer and stops accepting
//                  new data into the buffer.
//---------------------------------------------------------------------
//@ Rationale
//     This class encapsulates code and data necessary to process 
//     waveform data.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The 'buffer_' is a one way circular link list.
//     The 'pHead_' points to a head of the waveform buffer pool where
//     next waveform packet is added to. that is just ready for
//     receiving a new waveform data. If 'pHead_' is NULL, the waveform 
//     pool is considered deactivated and cannot accept anymore data.
//     A non-null pTail_ points to the first waveform data that can
//     be accessed. The getData method takes the data from 'pTail_' and
//     moves pTail_ pointer to next entry in the buffer. If pTail_ is
//     null, there is no accumulated data in the buffer.
//
//     The waveform buffer pool can hold up to 120 waveform data packets
//     before it starts to drop an oldest waveform data packet from the
//     buffer when a new waveform data is added. It can buffer amount of
//     data generated for 2.4 seconds by the Breath Delivery system since
//     a waveform is generated every 20 millisecond.
//    
//    
//---------------------------------------------------------------------
//@ Fault-Handling
//     Uses Standard Sigma CLASS_ASSERTION and CLASS_PRE_CONDITION
//     to handle software faults.
//>Voff
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/WaveformDataInfo.ccv   25.0.4.0   19 Nov 2013 14:18:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  16-Feb-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================
//====================================================================

#include "WaveformDataInfo.hh"
#if    defined(SIGMA_DEVELOPMENT)
#include "stdio.h"
#endif    //defined(SIGMA_DEVELOPMENT)
#include "IpcIds.hh"
#include "Background.hh"


//@ Code...

//====================================================================
//
//  Static Member Definitions...
//
//====================================================================

static Uint Bmem[(sizeof(MutEx)+sizeof(Uint) -1)/sizeof(Uint)];
MutEx &WaveformDataInfo::RDataLock_ = *(( MutEx *) Bmem);

//====================================================================
//
//  Public Methods...
//
//====================================================================

 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()  
//
//@ Interface-Description
//   Initialize the static member objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Initialize thje RDataLock_ MutEx object.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
WaveformDataInfo::Initialize()
{
    (void) new (&RDataLock_) 
        MutEx(::WAVEFORM_DATA_POOL_MT);        // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WaveformDataInfo()  [Default Constructor]
//
//@ Interface-Description
//   Initializes a circular waveform buffer pool.
//   Marks the waveform buffer pool to be inactive until it gets
//   explicitly activated.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Initialize buffers in the 'Buffer_' array to be one way circular
//    linked buffer list. Set the 'PHead_' to NULL, this indicates
//    the buffer is not activated. And set the PTail_ to NULL, this 
//    indicates there is no waveform data accumulated in the buffer.
//    To make the buffer circular, the last buffer is pointing to the
//    first one.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
WaveformDataInfo::WaveformDataInfo(void)
{
Uint idx;

  CALL_TRACE("WaveformDataInfo::WaveformDataInfo()");

  for ( idx = 0; idx < NUM_TOTAL_WAVEFORM_DATA -1 ; idx++)
  {  // $[TI1]                                                    
      buffer_[idx].pNext = &buffer_[idx+1];
  }

  // To make the buffer circular, the last buffer is poining to the first
  // one.
  buffer_[idx].pNext = &buffer_[0];
  pTail_ = NULL;
  pHead_ = NULL;
}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  activate()  
//
//@ Interface-Description
//   The method activates the waveform buffer pool to start 
//   accepting Waveform Data. Data arrival order is maintained in the
//   circular buffer.
//---------------------------------------------------------------------
//
//@ Implementation-Description
//   While the 'pHead_' is set to a valid waveform buffer pointer,
//   the queue is considered activated.
//   
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
WaveformDataInfo::activate()
{
  CALL_TRACE("WaveformDataInfo::activate()");

  if ( pHead_ )
  {
      return;        // $[TI0.1]
  }

  RDataLock_.request();

  pHead_ = &buffer_[0];
  pTail_ = NULL; // $[TI1]

  RDataLock_.release();

}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  deactivate()  
//
//@ Interface-Description
// This method deactivates Waveform Data buffer and 
// accumulated data on the waveform buffer pool is discarded.
// While a waveform is deactivated, new waveform data will be discarded.
//
//@ Implementation-Description
//---------------------------------------------------------------------
// By setting Waveform tail and head point to a NULL pointer, the queue
// is considered empty. While pHead_ is NULL, the waveform pool is
// inactive.
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
WaveformDataInfo::deactivate()
{
  CALL_TRACE("WaveformDataInfo:deactivate");

  RDataLock_.request();
  pHead_ =  NULL;
  pTail_ = NULL;
  RDataLock_.release();
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  getData(WaveformData *pData)  
//
//@ Interface-Description
// This routine retrieves a waveform data out of the waveform data pool
// in the order that the data is received.
// If the buffer pool is empty, the method returns a FAILURE status.
// Otherwise it returns a SUCCESS status and waveform
// data is copied into a memory pointed by the pData pointer.
//-----------------------------------------------------------------
//@ Implementation-Description
// If (pTail_ == NULL ) then the waveform data pool is empty, otherwise
// pTail_ points to the earliest arrived waveform data.
// After a tail end of data is copied, pTail_ is updated to point
// the next buffer in the buffer chain. If pTail_ is same as pHead_,
// the buffer is empty, then set pTail_ to NULL.
//
// The MutEx protection is necessary since the buffer pool is accessed
// by multiple tasks with different task priorities.
//---------------------------------------------------------------------
//@ PreCondition
//   ( pData )
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
SigmaStatus
WaveformDataInfo::getData(WaveformData *pData)
{
  CALL_TRACE("WaveformDataInfo:getData");
  CLASS_PRE_CONDITION( pData );

  RDataLock_.request();

  if ( !pHead_ )             
  {
      RDataLock_.release();

      return FAILURE;            // $[TI0.1]
  }
               // $[TI0.2]

  if ( pTail_ )
  {            // $[TI1]
       *pData = pTail_->data;
       pTail_ = pTail_->pNext;
       if ( pTail_ == pHead_ )          
       {
            pTail_ = NULL;        // $[TI2]
       }
       RDataLock_.release();

       return SUCCESS;        // $[TI3]
  }
            // $[TI4]

  RDataLock_.release();

  return FAILURE;    

}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  addData(const WaveformData &pData)  
//
//@ Interface-Description
//    Add new data to the waveform data buffer pool, put the
//    data at the head of the queue in order to reserve the data
//    arrival time order.
//    If the buffer is full, discard the tail end of buffer and put
//    a new data to it.
//---------------------------------------------------------------------
//@ Implementaion-Description
//    pHead_ points to the head of the buffer where a new data can be put.
//    If pHead_ catches up with the pTail_ pointer, the buffer pool is full.
//    In this case, a waveform drop message is logged onto the system
//    log NovRam memory.
//    If pTail_ is previously set to NULL, make sure pTail_ points to
//    the data just being added.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
WaveformDataInfo::addData( const WaveformData *pData )
{
  CALL_TRACE("WaveformDataInfo::addData");
  SAFE_CLASS_ASSERTION( pData );

  RDataLock_.request();

   if ( !pHead_ )                            
   {
      RDataLock_.release();

       return;        // $[TI0.1]
   }
    // $[TI0.2]

   if ( pHead_ == pTail_ ) // If the buffer is full.
   {  // $[TI1]

        Background::LogDiagnosticCodeUtil(BK_GUI_WAVEFORM_DROP);
        pTail_ = pTail_->pNext;
   }
   else
   {                                                
            if (pTail_ == NULL )
            {
                 pTail_ = pHead_;    // $[TI2]
            }
    // $[TI3]
   }
   pHead_->data = *pData;
   pHead_ = pHead_->pNext;
   RDataLock_.release();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
WaveformDataInfo::SoftFault(const SoftFaultID  softFaultID,
						    const Uint32       lineNumber,
						    const char*        pFileName,
						    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PATIENT_DATA,
  WAVEFORM_DATA_INFO, lineNumber, pFileName, pPredicate);
}


#endif        //defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
