#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DListC_MemPtr - Abstract Doubly-Linked List of 'MemPtr' Items.
//---------------------------------------------------------------------
//@ Interface-Description
//  This provides an abstraction for a doubly-linked list of generic
//  data items, each of type 'MemPtr'.  The data items of the list are
//  copies of the data items that are added to the list.  There are
//  non-constant, public methods to iterate through the contents of
//  the list, and there's also an iterator class (see
//  'DIterC_MemPtr') to allow iteration of a constant list.  This
//  is an abstract base class and is used by 'FixedDListC<MemPtr,SIZE>'.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a standard abstraction for storing data items
//  in a dynamic collection, where initial placement within the
//  collection is quick and easy.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each list has an internal heap containing the doubly-linked
//  nodes ('DNodeC_MemPtr').  The nodes are allocated from
//  the heap to contain copies of the inserted data.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertion macros are used to ensure
//  correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions have to do with the data type that is chosen for
//  this list.  The data type must provide the following mechanisms,
//  to be allowed to be used in this list:
//>Von
//  MemPtr::MemPtr(const MemPtr& aType);            // copy constructor...
//  MemPtr::operator=(const MemPtr& aType);       // assignment operator...
//  MemPtr::operator==(const MemPtr& aType) const;// equivalence operator...
//  MemPtr::operator!=(const MemPtr& aType) const;// inequivalence operator...
//>Voff
//  For primitive (built-in) types this is already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//  The assignment operator of any class used as a type of this list
//  must be implemented such that upon completion of an assignment
//  between two instances, the instances are equivalent (i.e.
//  'a = b; POST_CONDITION(a == b);').
//---------------------------------------------------------------------
//@ Invariants
//  ((pFirst_ == NULL  &&  pCurrent_ == NULL)  ||
//   (isLegalNode_(pFirst_)  &&  isLegalNode_(pCurrent_)))
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractDListC.C_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "DListC_MemPtr.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DListC_MemPtr()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this list and the items that is contains.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The 'rHeapOfNodes_' alias can NOT be used from within this
//  destructor, because by the time this destructor is called
//  the derived class's destructor has already destroyed the heap!
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

DListC_MemPtr::~DListC_MemPtr(void)
{
  CALL_TRACE("~DListC_MemPtr()");
}               // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  search(item, startFrom, direction)
//
//@ Interface-Description
//  Search the current list for a node equivalent to 'item'.  The
//  search will proceed forward if 'direction' is 'FORWARD', or backward
//  if 'direction' is 'BACKWARD'.  'startFrom' indicates whether the search
//  is to begin at the first node ('FROM_FIRST'), the current node
//  ('FROM_CURRENT'), or the last node ('FROM_LAST').  If an
//  equivalence is found the current node is set to the equivalent
//  node, and 'SUCCESS' is returned.  If NO equivalence is found,
//  then the list is left unchanged, and 'FAILURE' is returned.
//  The search will NOT loop around either end.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Near the end of this method a pointer to a constant node,
//  'pFoundNode', has its const-ness cast away.  This is due to the
//  fact that 'findNode_()' is a constant method (to allow its use
//  in constant public methods) that returns a pointer.  To ensure
//  consistent const-ness of 'findNode_()', the pointer that is
//  returned is a pointer to a CONSTANT node.  The const-ness of
//  the returned node is not necessary in the design of 'findNode_()',
//  but rather a consistency of design of that method.  Therefore,
//  when this method casts the const-ness of 'pFoundNode' away, it
//  is an acceptable case of working properly with a proper design.
//---------------------------------------------------------------------
//@ PreConditions
//  (startFrom == FROM_FIRST  ||  startFrom == FROM_CURRENT  ||
//   startFrom == FROM_LAST)
//  (direction == FORWARD  ||  direction == BACKWARD)
//---------------------------------------------------------------------
//@ PostConditions
//  (status == SUCCESS) --> (currentItem() == item)
//  (status == FAILURE) --> (pCurrent_ == OLD{pCurrent_})
//@ End-Method
//=====================================================================

SigmaStatus
DListC_MemPtr::search(const MemPtr&           item,
			     const SearchFrom      startFrom,
			     const SearchDirection direction)
{
  CALL_TRACE("search(item, startFrom, direction)");

  SigmaStatus  status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this list is NOT empty, therefore search the nodes for an item
    // that matches 'item'...
    const DNodeC_MemPtr*  pFoundNode = NULL;

    // set 'pFoundNode' to the starting node that is indicated by
    // 'startFrom'...
    switch (startFrom)
    {
    case FROM_FIRST:	// $[TI1.1]
      pFoundNode = getFirst_();
      break;

    case FROM_CURRENT:	// $[TI1.2]
      pFoundNode = getCurrent_();
      break;

    case FROM_LAST:	// $[TI1.3]
      pFoundNode = getLast_();
      break;

    default:
      AUX_CLASS_ASSERTION_FAILURE(((startFrom     << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(MemPtr)));	    // bottom 12 bits...
      break;
    }

    // this is a "safe" condition, because 'findNode_()' has this
    // same pre-condition and it ALWAYS checks it...
    SAFE_CLASS_PRE_CONDITION((direction == FORWARD  ||
			      direction == BACKWARD));

    // 'pFoundNode' doesn't contain an item that is equivalent to 'item',
    // therefore use 'findNode_()' to search, starting from
    // 'pFoundNode'...
    pFoundNode = findNode_(item, pFoundNode, direction);

    if (pFoundNode != NULL)
    {   // $[TI1.4]
      // the matching node is found, therefore set the current node to the
      // found node...
      pCurrent_ = (DNodeC_MemPtr*)pFoundNode; // "const cast-away"...
      status = SUCCESS;
    }   // $[TI1.5]
  }   // $[TI2] -- this list is currently empty...

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  insert(newItem, insertPlace)
//
//@ Interface-Description
//  Insert 'newItem' before/after the current list node.  If
//  'insertPlace' is 'AFTER_CURRENT', insert AFTER the current node,
//  otherwise, if 'insertPlace' is 'BEFORE_CURRENT', insert before the
//  current node.  (If the list is empty, then 'insertPlace' is ignored.)
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method allocates a new node from this list's heap, and
//  initializes that new node's item to be equivalent to 'newItem'.
//  Then, according to 'insertPlace', the new node is added to this
//  list.  Finally, the new node is set up as this list's current node.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//  (insertPlace == AFTER_CURRENT  ||  insertPlace == BEFORE_CURRENT)
//---------------------------------------------------------------------
//@ PostConditions
//  (currentItem() == newItem)
//@ End-Method
//=====================================================================

void
DListC_MemPtr::insert(const MemPtr&          newItem,
			     const InsertionPlace insertPlace)
{
  CALL_TRACE("insert(newItem, insertPlace)");
  SAFE_CLASS_PRE_CONDITION((!isFull()));  // checked by 'new (heap)...'

  DNodeC_MemPtr*  pNewNode;

  // allocate, and initialize, a new node from this list's heap...
  pNewNode = new (rHeapOfNodes_) DNodeC_MemPtr(newItem);

  if (!isEmpty())
  {   // $[TI1]
    switch (insertPlace)
    {
    case AFTER_CURRENT:		// $[TI1.1]
      // insert 'pNewNode' AFTER 'pCurrent_'...
      pNewNode->pNext_ = pCurrent_->pNext_;
      pNewNode->pPrev_ = pCurrent_;
      pCurrent_->pNext_->pPrev_ = pNewNode;
      pCurrent_->pNext_ = pNewNode;
      break;
    case BEFORE_CURRENT:	// $[TI1.2]
      // insert 'pNewNode' BEFORE 'pCurrent_'...
      pNewNode->pNext_ = pCurrent_;
      pNewNode->pPrev_ = pCurrent_->pPrev_;
      pCurrent_->pPrev_->pNext_ = pNewNode;
      pCurrent_->pPrev_ = pNewNode;

      if (pCurrent_ == pFirst_)
      {   // $[TI1.2.1]
	// 'pCurrent_' is at the front of the list, therefore 'pFirst_'
	// must be set to 'pNewNode' also...
	pFirst_ = pNewNode;
      }   // $[TI1.2.2] -- not at front of list...

      break;
    default:
      AUX_CLASS_ASSERTION_FAILURE(((insertPlace   << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(MemPtr)));	    // bottom 12 bits...
      break;
    }
  }
  else
  {   // $[TI2] -- this list is currently empty...
    // this list is empty, therefore add 'pNewNode' as the first, and
    // only, node in this list...
    pFirst_ = pNewNode;
    pNewNode->pNext_ = pFirst_;
    pNewNode->pPrev_ = pFirst_;
  }

  pCurrent_ = pNewNode;  // 'pCurrent_' is ALWAYS left as the new node...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  append(newItem)
//
//@ Interface-Description
//  Append 'newItem' to the end of this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses 'insert()' to do the insertion of 'newItem'
//  into this list.  If this list is not empty, then this method
//  sets the current item to the last item in this list, and asks
//  'insert()' to insert after the current item (which is the last
//  item).
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//---------------------------------------------------------------------
//@ PostConditions
//  (isAtLast()  &&  currentItem() == newItem)
//@ End-Method
//=====================================================================

void
DListC_MemPtr::append(const MemPtr& newItem)
{
  CALL_TRACE("append(newItem)");
  SAFE_CLASS_PRE_CONDITION((!isFull()));    // checked by 'insert()'...

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore set 'pCurrent_' to the last
    // node in this list, because 'insert()' will insert based on the
    // current node...
    pCurrent_ = pFirst_->pPrev_;
  }   // $[TI2] -- this list is currently empty...

  // insert 'newItem' after the current node...
  insert(newItem, AFTER_CURRENT);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  prepend(newItem)
//
//@ Interface-Description
//  Prepend 'newItem' to the front of this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses 'insert()' to do the insertion of 'newItem'
//  into this list.  If this list is not empty, then this method
//  sets the current item to the first item in this list, and asks
//  'insert()' to insert before the current item (which is the first
//  item).
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//---------------------------------------------------------------------
//@ PostConditions
//  (isAtFirst()  &&  currentItem() == newItem)
//@ End-Method
//=====================================================================

void
DListC_MemPtr::prepend(const MemPtr& newItem)
{
  CALL_TRACE("prepend(newItem)");
  SAFE_CLASS_PRE_CONDITION((!isFull()));    // checked by 'insert()'...

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore set 'pCurrent_' to the first
    // node in this list, because 'insert()' will insert based on the
    // current node...
    pCurrent_ = pFirst_;
  }   // $[TI2] -- this list is currently empty...

  // insert 'newItem' before the current node...
  insert(newItem, BEFORE_CURRENT);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeCurrent()
//
//@ Interface-Description
//  Remove the current node from this list, and return 'SUCCESS'.
//  If this list is empty, then 'FAILURE' is returned, and the list
//  is left unchanged.  When a node is removed, the new current node
//  is the node that is the "next" node in the list.  If the removed
//  node is the "last" node in the list, the new current node is the
//  "previous" node in the list.  If the removed node is the only
//  node in the list, the list is left empty.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SigmaStatus
DListC_MemPtr::removeCurrent(void)
{
  CALL_TRACE("removeCurrent()");

  SigmaStatus  status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore remove the current node...
    DNodeC_MemPtr*  pOldCurr = pCurrent_;  // store the current node...

    if (pCurrent_ == pFirst_)
    {   // $[TI1.1]
      // 'pCurrent_' is the first item in this list, therefore if
      // the current node is not the only item in this list, then the new
      // current node is the next node, otherwise set the current node
      // to 'NULL' thereby setting the list to empty...
      pCurrent_ = (pCurrent_->pNext_ != pFirst_)
		   ? pCurrent_->pNext_		// $[TI1.1.1]
		   : NULL;			// $[TI1.1.2]
      pFirst_ = pCurrent_;  // set 'pFirst_' to the new first node...
    }
    else
    {   // $[TI1.2] -- NOT currently at the front of list...
      // if 'pCurrent_' is NOT the last item in this list, then move to
      // the next item, otherwise if it is the last node in this list,
      // then move back one...
      pCurrent_ = (pCurrent_->pNext_ != pFirst_)
		   ? pCurrent_->pNext_		// $[TI1.2.1]
		   : pCurrent_->pPrev_;		// $[TI1.2.2]
    }

    // take the old node off this list...
    pOldCurr->pPrev_->pNext_ = pOldCurr->pNext_;
    pOldCurr->pNext_->pPrev_ = pOldCurr->pPrev_;

    deallocNode_(pOldCurr); // deallocate the old node...
    status = SUCCESS;
  }   // $[TI2] -- this list is currently empty...

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeItem(item)
//
//@ Interface-Description
//  Remove the first node item in this list that is equivalent to
//  'item', and return 'SUCCESS'.  If this list is empty or no
//  equivalent node is found, then 'FAILURE' is returned, and the
//  list is left unchanged.  When a node is removed, the new current node
//  is the node that is "next" node in the list.  If the removed node is the
//  "last" node in the list, the new current node is the "previous" node in
//  the list.  If the removed node is the only node in the list, the
//  list is left empty.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses 'search()' to find 'item' and set the current
//  node to it, then uses 'removeCurrent()' to remove the current
//  node.  This method returns 'SUCCESS' when 'removeCurrent()' is
//  called.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SigmaStatus
DListC_MemPtr::removeItem(const MemPtr& item)
{
  CALL_TRACE("removeItem(item)");

  // if this list is not empty and the search for an item equivalent to
  // 'item' is successful, then remove the current item...
  return((!isEmpty()  &&  search(item) == SUCCESS)
	  ? removeCurrent()		// $[TI1]
	  : FAILURE);			// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  clearList()
//
//@ Interface-Description
//  Clear out this list, and leave this list as an empty list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

void
DListC_MemPtr::clearList(void)
{
  CALL_TRACE("clearList()");

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore, starting from the first node,
    // deallocate each node in this list...
    DNodeC_MemPtr*  pNextNode = pFirst_;
    DNodeC_MemPtr*  pCurrNode = pFirst_;

    do
    { // while there are more nodes to deallocate...
      pNextNode = pNextNode->pNext_;  // store the next node...
      deallocNode_(pCurrNode);        // deallocate this node...
      pCurrNode = pNextNode;          // set this node to next node...
    } while (pCurrNode != pFirst_);

    // set this list to be empty...
    pFirst_   = NULL;
    pCurrent_ = NULL;
  }   // $[TI2] -- this list is currently empty...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(dList)  [Assignment Operator]
//
//@ Interface-Description
//  Copy the items from 'dList' into this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (getMaxItems() >= dList.getMaxItems())
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == dList  &&  (isEmpty()  ||
//                currentItem() == dList.currentItem()))
//@ End-Method
//=====================================================================

void
DListC_MemPtr::operator=(const DListC_MemPtr& dList)
{
  CALL_TRACE("operator=(dList)");

  if (this != &dList)
  {   // $[TI1]
    // this list and 'dList' are not the same instance, therefore clear
    // out this list...
    clearList();

    if (!dList.isEmpty())
    {   // $[TI1.1]
      // 'dList' is not empty, therefore the items from 'dList' need to
      // be copied into this list...
      AUX_CLASS_PRE_CONDITION((getMaxItems() >= dList.getMaxItems()),
			    ((getMaxItems()       << 22) |  // top 10 bits...
			     (dList.getMaxItems() << 12) |  // middle 10 bits...
			     sizeof(MemPtr)));		    // bottom 12 bits...

      const DNodeC_MemPtr*  pOtherCurr = dList.getFirst_();
      DNodeC_MemPtr*        pNewCurr   = NULL;

      // step through each of the nodes from 'dList', and insert them into
      // this list...
      do
      {   // $[TI1.1.1] -- ALWAYS at least one iteration...
	// insert the item of the current node from 'dList'...
        insert(pOtherCurr->getItem(), AFTER_CURRENT);

	if (pOtherCurr == dList.getCurrent_())
	{   // $[TI1.1.1.1]
	  // save the corresponding 'pCurrent_', to later set up this list's
	  // 'pCurrent_' to be the same corresponding node as the current
	  // node of 'dList'...
	  pNewCurr = pCurrent_;
	}   // $[TI1.1.1.2] -- NOT equivalent to current of 'dList'...

	pOtherCurr = pOtherCurr->pNext_;
      } while (pOtherCurr != dList.getFirst_());

      // set 'pCurrent_' to the corresponding current of 'dList'...
      pCurrent_ = pNewCurr;
    }   // $[TI1.2] -- 'dlist' is currently empty...
  }   // $[TI2] -- assigning to itself...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//	                [static]
//
//@ Interface-Description
//	Report the software fault that occurred within the source code
//	of this class.  The fault, indicated by 'softFaultID', occured
//	at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

void
DListC_MemPtr::SoftFault(const SoftFaultID softFaultID,
				const Uint32      lineNumber,
				const char*       pFileName,
				const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, ABSTRACT_DLISTC,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  findNode_(item, pStartNode, direction)  [const]
//
//@ Interface-Description
//  Starting from 'pStartNode', and going in the direction indicated
//  by 'direction', search for the node that matches 'item', then
//  return the pointer to that node.  If no match is made then 'NULL'
//  is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (isLegalNode_(pStartNode)  ||  (pStartNode == NULL  &&  isEmpty()))
//  (direction == FORWARD  ||  direction == BACKWARD)
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

const DNodeC_MemPtr*
DListC_MemPtr::findNode_(const MemPtr&               item,
				const DNodeC_MemPtr* pStartNode,
				const SearchDirection     direction) const
{
  CALL_TRACE("findNode_(item, pStartNode, direction)");
  SAFE_CLASS_PRE_CONDITION((isLegalNode_(pStartNode)));

  const DNodeC_MemPtr*  pStopNode = NULL;

  switch (direction)
  {
  case FORWARD:		// $[TI1]
    // stop when pointing at the last node...
    pStopNode = getLast_();

    while (pStartNode != pStopNode  &&  pStartNode->nodeItem_ != item)
    {   // $[TI1.1] -- at least one iteration...
      pStartNode = pStartNode->pNext_;  // search forward...
    }   // $[TI1.2] -- no iterations...

    break;

  case BACKWARD:	// $[TI2]
    // stop when pointing at the first node...
    pStopNode = getFirst_();

    while (pStartNode != pStopNode  &&  pStartNode->nodeItem_ != item)
    {   // $[TI2.1] -- at least one iteration...
      pStartNode = pStartNode->pPrev_;  // search backward...
    }   // $[TI2.2] -- no iterations...

    break;

  default:
    AUX_CLASS_ASSERTION_FAILURE(((direction << 16) | sizeof(MemPtr)));
    break;
  }

  if (pStartNode != NULL  &&  pStartNode->nodeItem_ != item)
  {   // $[TI3]
    // the search looped around the end of this list, because no match
    // was found, therefore return 'NULL'...
    pStartNode = NULL;
  }   // $[TI4] -- a matching item was found...

  return(pStartNode);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEquivTo_(dList)  [const]
//
//@ Interface-Description
//  Determine the equivalence between this list and 'dList', and
//  return a Boolean to indicate whether they're equivalent.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
DListC_MemPtr::isEquivTo_(const DListC_MemPtr& dList) const
{
  CALL_TRACE("isEquivTo_(dList)");

  Boolean isEquiv;

  // the two lists are equivalent if they are at the same address
  // location, OR they're both empty...$[TI1] (TRUE) $[TI2] (FALSE)
  isEquiv = (this == &dList  ||  (isEmpty()  &&  dList.isEmpty()));

  if (!isEquiv  &&  !isEmpty()  &&  !dList.isEmpty())
  {   // $[TI3]
    // both lists are NOT empty, therefore continue checking for
    // equivalence...
    const DNodeC_MemPtr*  pCurrNode1 = pFirst_;
    const DNodeC_MemPtr*  pCurrNode2 = dList.pFirst_;

    do
    {   // $[TI3.1]
      // while the items of the two nodes are equivalent, and the end of
      // either list has not been reached, check for equivalence...
      isEquiv = (pCurrNode1->nodeItem_ == pCurrNode2->nodeItem_);

      if (isEquiv)
      {   // $[TI3.1.1]
	// these two nodes are equivalent, therefore move to the next
	// node...
	pCurrNode1 = pCurrNode1->pNext_;
	pCurrNode2 = pCurrNode2->pNext_;
      }   // $[TI3.1.2] -- the two current items are equivalent...
    } while (isEquiv  &&  pCurrNode1 != pFirst_  &&
		pCurrNode2 != dList.pFirst_);

    if (isEquiv)
    {   // $[TI3.2]
      // each item in this list is equivalent the corresponding item of
      // 'dList', make sure that they BOTH have cycled back to the front
      // of themselves (this ensures that they both have the same number
      // of items)...$[TI3.2.1] (TRUE)  $[TI3.2.2] (FALSE)
      isEquiv = (pCurrNode1 == pFirst_  &&  pCurrNode2 == dList.pFirst_);
    }   // $[TI3.3] -- an item in-equivalence is found...
  }   // $[TI4]

  return(isEquiv);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  print_(ostr)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that provides a means for the
//  derived classes to dump the contents of an instance to 'ostr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented
