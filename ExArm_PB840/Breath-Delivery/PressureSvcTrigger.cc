#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PressureSvcTrigger - Triggers transition to the SVC phase 
//         during Occlusion.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers a transition from the SVO (safety valve open)
//    to the SVC (safety valve closed) phase during Occlusion Status
//    Cycling.  It is contained on a list in the BreathTriggerMediator.
//    When enabled, it is evaluated every cycle.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements the algorithm for determining when to close
//    the safety valve during the initial stages of OSC.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a method for checking inspiratory pressure
//    measured at < SV_CLOSE_PRESS cmH2O.  When this condition is met, the trigger
//    "fires" sending itself to the active BreathPhaseScheduler (OSC).
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureSvcTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "PressureSvcTrigger.hh"

//@ Usage-Classes

#  include "PressureSensor.hh"

#  include "MainSensorRefs.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureSvcTrigger()  
//
//@ Interface-Description
//     Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//     id is passed to the base class constructor.  This constructor 
//     does not need to initialize anything.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureSvcTrigger::PressureSvcTrigger(void) : BreathTrigger(Trigger::PRESSURE_SVC)
{
  CALL_TRACE("PressureSvcTrigger()");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureSvcTrigger()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureSvcTrigger::~PressureSvcTrigger(void)
{
  CALL_TRACE("~PressureSvcTrigger()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PressureSvcTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PRESSURESVCTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)




//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters and returns a Boolean indicating
//    the state of the condition monitored by this method.  If the
//    inspiratory pressure sensor reads a pressure less than 5 cmH2O
//    then this trigger condition is met.
//---------------------------------------------------------------------
//@ Implementation-Description
//    $[04204]
//    This method returns a TRUE value if the pressure measured by the
//    inspiratory pressure transducer is < SV_CLOSE_PRESS cmH2O.  Otherwise, it 
//    returns FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
static const Real32 SV_CLOSE_PRESS = 5.0;
Boolean
PressureSvcTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_(void)");

  Real32 inspPressure = RInspPressureSensor.getValue();
  Boolean returnValue = FALSE;

  if (inspPressure < SV_CLOSE_PRESS)
  {
    // $[TI1]
    returnValue = TRUE;
  }
  // $[TI2]

  //TRUE: $[TI3]
  //FALSE: $[TI4]
  return (returnValue);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

