#ifndef NumericSettingButton_HH
#define NumericSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: NumericSettingButton - A setting button which displays a
// title (along with possible units text) and the value of a bounded setting in
// a variable precision numeric field.  Most setting buttons fall into this
// category.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/NumericSettingButton.hhv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 007   By: rhj    Date:  20-April-2006   DCS Number: 6181
//  Project:  PAV4
//  Description:
//      Added a functionality to change the Units text.
//
//  Revision: 006   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Modified constructor to take arguments for auxillary title text
//      and position.  This functionality will be used to plug-in the
//      "above PEEP" phrases to the "Pi" and "Psupp" setting buttons.
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/NumericSettingButton.hhv   1.5.1.0   07/30/98 10:17:36   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Colors.hh"

//@ Usage-Classes
#include "SettingButton.hh"
#include "NumericField.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class NumericSettingButton : public SettingButton
{
public:
	NumericSettingButton(Uint16 xOrg, Uint16 yOrg, Uint16 width,
				Uint16 height, ButtonType buttonType, Uint8 bevelSize,
				CornerType cornerType, BorderType borderType,
				TextFont::Size numberSize,
				Uint16 valueCenterX, Uint16 valueCenterY,
				StringId titleText, StringId unitsText,
				SettingId::SettingIdType settingId,
				SubScreen *pFocusSubScreen, StringId messageId,
				Boolean isMainSetting = FALSE,
				StringId auxTitleText = ::NULL_STRING_ID,
				Gravity  auxTitlePos  = ::GRAVITY_RIGHT);
	~NumericSettingButton(void);

	void setValue(Real32 value, Precision precision);
    void setUnitsText(StringId unitsTitle);
	void setLimitOffEnable(Boolean isEnabled);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
                                const SettingSubject*         pSubject);

	//@ Data-Member: buttonValue_
	// The Numeric Field which displays this button's setting value.
	NumericField buttonValue_;

	//@ Data-Member: isLimitOffEnabled_
	// TRUE if the OFF limit is used for this numeric setting
	Boolean isLimitOffEnabled_;

private:
	// these methods are purposely declared, but not implemented...
	NumericSettingButton(const NumericSettingButton&);	// not implemented...
	void   operator=(const NumericSettingButton&);	// not implemented...

    //@ Data-Member: unitsText_
    // A TextField which displays the units of the buttonValue_.
	TextField unitsText_;
};


#endif // NumericSettingButton_HH 
