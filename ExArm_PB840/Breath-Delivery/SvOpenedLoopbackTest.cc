#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: SvOpenedLoopbackTest - BD Safety Net Test for the detection
//                           of open safety valve loopback current OOR
//---------------------------------------------------------------------
//@ Interface-Description
//  The checkCriteria() method of this class is called from the
//  newCycle() method of the SafetyNetTestMediator class.  The
//  checkCriteria() method invokes methods in the SafetyValve class
//  to obtain the current state and the value of the safety valve
//  current.
//---------------------------------------------------------------------
//@ Rationale
//  Used to determine if the safety valve is stuck closed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The safety net test is defined in the checkCriteria() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SvOpenedLoopbackTest.ccv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: iv     Date: 09-Jun-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code when fault is detected.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  07-Nov-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "SvOpenedLoopbackTest.hh"

//@ Usage-Classes
#include "ValveRefs.hh"
#include "MiscSensorRefs.hh"
#include "Solenoid.hh"

#if defined ( SIGMA_SAFETY_NET_DEBUG )
#include <stdio.h>
#endif // defined ( SIGMA_SAFETY_NET_DEBUG )

//@ End-Usage

//@ Code...

static const Real32 MAX_SV_OPEN_CURRENT = 209.0F;

// wait up to 500MS for safety valve current to drop below 209.0 mA
static const Uint8 MAX_SV_OPEN_TIME_CRITERIA = 25;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SvOpenedLoopbackTest()
//
//@ Interface-Description
//  Default Constructor.  Passes two arguments to the base class
//  contructor to define the error code and the time criterion.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The backgndEventId_ and maxCyclesCriteriaMet_ data members are
//  set by the base class constructor.  The background event id is
//  a unique identifier that is placed in the diagnostic log if the
//  background check detects a problem.  The maximum Cycles that the
//  criteria can be met before the Background subsystem is notified
//  is MAX_SV_OPEN_TIME_CRITERIA.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

SvOpenedLoopbackTest::SvOpenedLoopbackTest( void ) :
SafetyNetTest( BK_SVO_CURRENT_OOR, MAX_SV_OPEN_TIME_CRITERIA )
{
    // $[TI1]
    CALL_TRACE("SvOpenedLoopbackTest(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SvOpenedLoopbackTest()
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

SvOpenedLoopbackTest::~SvOpenedLoopbackTest( void )
{
    CALL_TRACE("~SvOpenedLoopbackTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCriteria()
//
//@ Interface-Description
//  This method takes no arguments and returns the a BkEventName to
//  the caller.  If the background check criteria have been met for
//  the required number of cycles, the backgndEventId for the class
//  is returned; otherwise BK_NO_EVENT is returned to indicate to
//  the caller that the Background subsystem should not be notified.
//
//  This method invokes SafetyValve::getCurrentState to determine if
//  the safety valve is open.
//
//  This check is not performed if the safety valve is closed.
//
//  This method also calls SafetyValveCurrent::getValue() method for
//  the safety valve loopback signal to determine if it falls within
//  tolerance.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method takes no arguments and returns the a BkEventName to
//  This method first checks the status of the backgndCheckFailed_
//  data member.  If it is TRUE, the test is no longer run since
//  this Background event is not auto-resettable and the Background
//  subsystem only needs to be informed of an event once.  In this
//  case, BK_NO_EVENT is returned to the caller.
//
//  If the check has not already failed, the status of the
//  safety valve is checked.  If safety valve is open, the value
//  of the safety valve loopback signal is retrieved from the
//  LinearSensor objects and analyzed.
//
//  The following check is then made:
//
//      Safety Valve is OPEN
//      and
//      Safety Valve Current > MAX_SV_OPEN_CURRENT
//
//  If the condition is met, the numCyclesCriteriaMet_ data item is
//  incremented and compared to the maxCyclesCriteriaMet_ item to
//  determine if the background subsystem needs to be informed.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
SvOpenedLoopbackTest::checkCriteria( void )
{
    CALL_TRACE("SvOpenedLoopbackTest::checkCriteria( void )");

    BkEventName rtnValue = BK_NO_EVENT;

    // Don't do the check if it has already failed
    if ( ! backgndCheckFailed_ )
    {
    // $[TI1]

#if 0 // TODO E600_LL: need to be reviewed
        // Only do the check if safety valve is open
        if ( ( Solenoid::OPEN == RSafetyValve.getCurrentState() ) &&
             ( RSafetyValveCurrent.getValue() > MAX_SV_OPEN_CURRENT ) )
        {
        // $[TI1.1]

#if defined ( SIGMA_SAFETY_NET_DEBUG )
printf("\nSvOpenedLoopbackTest:\n");
printf("rSafetyValveCurrent.getValue() = %8.4f.\n", rSafetyValveCurrent.getValue() );
#endif // defined ( SIGMA_SAFETY_NET_DEBUG )

            if ( ++numCyclesCriteriaMet_ >= maxCyclesCriteriaMet_ )
            {
            // $[TI1.1.1]
                errorCode_ = (Uint16)(RSafetyValveCurrent.getValue());
                backgndCheckFailed_ = TRUE;
                rtnValue = backgndEventId_;
            }
            // $[TI1.1.2]
        }
        else // safety valve closed or current below threshold
        {
        // $[TI1.2]
            numCyclesCriteriaMet_ = 0;
        }
#endif
    }
    // $[TI2]

    return( rtnValue );
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SvOpenedLoopbackTest::SoftFault( const SoftFaultID  softFaultID,
                                 const Uint32       lineNumber,
                                 const char*        pFileName,
                                 const char*        pPredicate )
{
  CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, SVOPENEDLOOPBACKTEST,
                           lineNumber, pFileName, pPredicate );
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


