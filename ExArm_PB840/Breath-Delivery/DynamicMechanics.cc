#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================
// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DynamicMechanics - The class for dynamic mechanics data and 
//                           related calculations.
//---------------------------------------------------------------------
//@ Interface-Description
// This class provides the methods for collecting and calculating
// dynamic mechanics data including dynamic compliance and resistance
// as well as peak spontaneous inspired flow. newCycle() is called by the
// BD cycle timer to calculate and store intermediate dot products every 
// BD cycle. These intermediate dot products are then used to calculate
// dynamic resistance and compliance when update() is called at the end 
// of inspiration. Dynamics Mechanics is a feature subset of the 
// Respiratory Mechanics option.
//---------------------------------------------------------------------
//@ Rationale
// This class provides for storage and computation of dynmamic 
// mechanics data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//   n/a.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DynamicMechanics.ccv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  gdc   Date:  07-APR-2008    SCR Number: 6407
//  Project:  TREND2
//  Description:
//	Removed redefinition of countof macro to resolve compiler warning.
//
//  Revision: 004   By: gdc   Date:  23-Feb-2007    SCR Number: 6307
//  Project:  RESPM
//  Description:
//		Added missing RM requirement numbers.
//
//  Revision: 003   By: gdc   Date:  20-Feb-2007   SCR Number: 6345
//  Project:  RESPM
//  Description:
//		Removed C20/C related code.
//
//  Revision: 002   By: gdc   Date:  08-Nov-2006   SCR Number: 6309
//  Project:  RESPM
//  Description:
//		Changed to use inspiratory btps corrected lung flow.
//
//  Revision: 001   By: gdc   Date:  13-Oct-2006   SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM initial version.
//
//=====================================================================

#include "DynamicMechanics.hh"
#include "BreathRecord.hh"
#include "LungData.hh"
#include "BreathSet.hh"
#include "BreathMiscRefs.hh"
#include "Peep.hh"
#include "PendingContextHandle.hh"
#include "MathUtilities.hh"


#ifdef SIGMA_DEVELOPMENT
Int32 DynamicMechanics::RmDataIndex;

//  number of data points = MAX_INSP_TIME_MS / CYCLE_TIME_MS = 8000 / 5 = 1600
//   doesn't account for inspiratory pause
DynamicMechanics::RmDataType DynamicMechanics::RmData[1600];
#endif

//@ Constant: RESISTANCE_UPDATE_TIMEOUT
// resistance averaging restarts if this time elapses without an update
static const Int32 RESISTANCE_UPDATE_TIMEOUT = 120000;  // ms

//@ Constant: COMPLIANCE_UPDATE_TIMEOUT
// compliance averaging restarts if this time elapses without an update
static const Int32 COMPLIANCE_UPDATE_TIMEOUT = 120000;  // ms

// PSF averaging restarts if this time elapses without an update
static const Int32 PSF_UPDATE_TIMEOUT = 120000;  // ms

//@ Constant: DM_ALPHA
// Alpha filter coefficient used for smoothing Dynamic Mechanics data
static const Real32 DM_ALPHA = 0.625;

// $[RM24006] The following limits are to be applied to any single-breath result before averaging.

//@ Constant: MIN_COMPLIANCE
// Lower bound for compliance values 
static const Real32 MIN_COMPLIANCE = 0.0;

//@ Constant: MIN_RESISTANCE
// Lower bound for resistance values
static const Real32 MIN_RESISTANCE = 0.0;

//@ Constant: MAX_COMPLIANCE
// Upper bound for compliance values (Patient-Data-MAX + 10%)
static const Real32 MAX_COMPLIANCE = 220.0;

//@ Constant: MAX_RESISTANCE
// Upper bound for resistance values (Patient-Data-MAX + 10%)
static const Real32 MAX_RESISTANCE = 110.0;

//@ Constant: MAX_PEAK_SPONT_INSP_FLOW
// Upper bound for valid PEAK_SPONT_INSP_FLOW value  
static const Real32 MAX_PEAK_SPONT_INSP_FLOW = 220.0;

//@ Constant: MIN_PEAK_SPONT_INSP_FLOW
// Lower bound for valid PEAK_SPONT_INSP_FLOW value
static const Real32 MIN_PEAK_SPONT_INSP_FLOW = 0.0;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DynamicMechanics() 
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Initialize data members and call newBreath() to initialize the
//   intermediate dot products.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

DynamicMechanics::DynamicMechanics(void) :
	singleBreathCompliance_(0.0),
	singleBreathCompliance20_(0.0),
	singleBreathResistance_(0.0),
	compliance_(0.0),
	resistance_(0.0),
	peepSetting_(0.0),
	complianceAlpha_(0.0),
	resistanceAlpha_(0.0),
	psfAlpha_(0.0),
	complianceBreathCount_(0),
	resistanceBreathCount_(0),
	psfBreathCount_(0),
	timeSinceComplianceUpdate_(COMPLIANCE_UPDATE_TIMEOUT),
	timeSinceResistanceUpdate_(RESISTANCE_UPDATE_TIMEOUT),
	timeSincePsfUpdate_(PSF_UPDATE_TIMEOUT)
{
    CALL_TRACE("DynamicMechanics()");

	newBreath();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DynamicMechanics()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DynamicMechanics::~DynamicMechanics(void)
{
  CALL_TRACE("~DynamicMechanics()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//  Takes no arguments and returns nothing. Runs every BD cycle to
//  accumulate the intermdiate summations required for calculating
//  dynamic compliance and resistance. Called by BreathRecord to 
//  collect dynamic mechanics data every inspiratory cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// dynamic compliance and resistance intermediate summations
//
//    Qi     = Volumetric Flow
//    Vi     = Cumulative Delivered Volume
//    Pi     = Airway Pressure
//
//
//    Cdyn   =    sum(Vi * Qi) * sum(Vi * Qi) - sum(Vi * Vi) * sum(Qi * Qi)
//                ---------------------------------------------------------
//                sum(Vi * Qi) * sum(Pi * Qi) - sum(Qi * Qi) * sum(Pi * Vi)
//
//
//
//    Rdyn   =    sum(Vi * Qi) * sum(Pi * Vi) - sum(Vi * Vi) * sum(Pi * Qi)
//                ---------------------------------------------------------
//                sum(Vi * Qi) * sum(Vi * Qi) - sum(Vi * Vi) * sum(Qi * Qi)
//
//
// We'll calculate dot products for Cdyn and Rdyn every cycle of 
// inspiration even during the first cycle when technically no volume 
// has been delivered.
//
//---------------------------------------------------------------------
//@ PreCondition
//	(RBreathSet.getCurrentBreathRecord() != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
DynamicMechanics::newCycle(void)
{
    CALL_TRACE("newCycle()");
   
	timeSinceComplianceUpdate_ += ::CYCLE_TIME_MS;
	timeSinceResistanceUpdate_ += ::CYCLE_TIME_MS;
	timeSincePsfUpdate_        += ::CYCLE_TIME_MS;

	BreathRecord* pCurrentBreath = RBreathSet.getCurrentBreathRecord();
	CLASS_PRE_CONDITION(pCurrentBreath != NULL);

	BreathType breathType_ = pCurrentBreath->getBreathType();
	BreathPhaseType::PhaseType phaseType = pCurrentBreath->GetPhaseType(); 

	if ( (breathType_ == ::CONTROL || breathType_ == ::ASSIST) &&
		 (phaseType == BreathPhaseType::INSPIRATION ) )
	{
        // $[RM24002] The following dot products are to be computed during the inspiration 

		Int32 inspTime = (Int32)((RBreathSet.getCurrentBreathRecord())->getInspTime());

		if (inspTime == CYCLE_TIME_MS)
		{
			newBreath();

			// if isPeepChange() is TRUE then new PEEP will be established by the end 
			// of the next exhalation. So we don't change the baseline PEEP until 
			// the new PEEP has been established on the next breath.
			if (!RPeep.isPeepChange())
			{
				peepSetting_       = RPeep.getActualPeep();
			}
#ifdef SIGMA_DEVELOPMENT    		
			RmData[0].volume   = 0;
			RmData[0].flow     = 0;
			RmData[0].pressure = 0;
#endif			
		}

		Real32 flow        = RLungData.getInspLungBtpsFlow() / 60;  // liters/sec
		Real32 pressure    = RLungData.getLungPressure() - peepSetting_;
		Real32 deliveredVolume    = RLungData.getInspLungBtpsVolume() / 1000;  // liters

#ifdef SIGMA_DEVELOPMENT
    	RmDataIndex        = inspTime / ::CYCLE_TIME_MS;
		if (RmDataIndex < countof(RmData) )
		{
			RmData[RmDataIndex].volume   = deliveredVolume;
			RmData[RmDataIndex].flow     = flow;
			RmData[RmDataIndex].pressure = pressure;
		}
#endif		

    	sumVolumeDotFlow_      += deliveredVolume * flow;
    	sumVolumeDotVolume_    += deliveredVolume * deliveredVolume;
    	sumFlowDotFlow_        += flow * flow;
    	sumPressureDotVolume_  += pressure * deliveredVolume;
    	sumPressureDotFlow_    += pressure * flow;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
//  Called at the end of the inspiratory phase to calculate the 
//  dynamic compliance and resistance for the completed breath.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the current breath is a CONTROL or ASSIST breath then update
//  dynamic compliance and resistance. Otherwise, if the breath type 
//  is SPONT, then update the peak spontaneous flow.
//---------------------------------------------------------------------
//@ PreCondition
// (RBreathSet.getCurrentBreathRecord() != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
DynamicMechanics::update(void)
{
	BreathRecord* pCurrentBreath = RBreathSet.getCurrentBreathRecord();
	CLASS_PRE_CONDITION(pCurrentBreath != NULL);

	BreathType breathType_ = pCurrentBreath->getBreathType();
	Int32 inspTime = pCurrentBreath->getInspTime();

	if (breathType_ == ::CONTROL || breathType_ == ::ASSIST)
	{
		updateCompliance_();
		updateResistance_();
	}
	else if (breathType_ == ::SPONT)
	{
		updatePeakSpontInspFlow_();
	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath
//
//@ Interface-Description
//  Takes no arguments and returns nothing. Called to reinitialize
//  the intermediate dot products used in the calculation of 
//  dynamic compliance and resistance. Normally called during the 
//  first cycle of inspiration.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize the dot products and other data for the start
//  of a new inspiratory phase.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
DynamicMechanics::newBreath(void)
{
    CALL_TRACE("newBreath()");
   
#ifdef SIGMA_DEVELOPMENT
	RmDataIndex = 0;
#endif
	sumVolumeDotFlow_ = 0.0;
	sumVolumeDotVolume_ = 0.0;
	sumFlowDotFlow_ = 0.0;
	sumPressureDotVolume_ = 0.0;
	sumPressureDotFlow_ = 0.0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateCompliance_
//
//@ Interface-Description
//  Takes no arguments and returns nothing. Calculates and sets the 
//  dynamic compliance average.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//    Qi    = Volumetric Flow
//    Vi    = Delivered Volume
//    Pi    = Airway Pressure
//
//
//    Cdyn  = sum(Vi * Qi) * sum(Vi * Qi) - sum(Vi * Vi) * sum(Qi * Qi)
//            ---------------------------------------------------------
//            sum(Vi * Qi) * sum(Pi * Qi) - sum(Qi * Qi) * sum(Pi * Vi)
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DynamicMechanics::updateCompliance_(void)
{
	CALL_TRACE("updateCompliance_(void)");

    // $[RM24005] Calculate dynamic compliance...

	if (timeSinceComplianceUpdate_ > COMPLIANCE_UPDATE_TIMEOUT)
	{
        // $[RM12069] The alpha-filtered Cdyn average shall be reset when no mandatory breaths have been delivered during the last 2 minutes.
		complianceBreathCount_ = 0;
		complianceAlpha_ = 0.0;
	}

	Real32 numerator   = sumVolumeDotFlow_ * sumVolumeDotFlow_ - sumVolumeDotVolume_ * sumFlowDotFlow_;
	Real32 denominator = sumVolumeDotFlow_ * sumPressureDotFlow_ - sumFlowDotFlow_ * sumPressureDotVolume_;
	singleBreathCompliance_ = numerator / denominator * 1000.0F;

	// a divide by zero result is not included in the averages
	if ( !IsFpError(singleBreathCompliance_) )
	{
		singleBreathCompliance_ = MIN_VALUE(MAX_COMPLIANCE, singleBreathCompliance_);
		singleBreathCompliance_ = MAX_VALUE(MIN_COMPLIANCE, singleBreathCompliance_);

        // $[RM24007] compliance ratio to be displayed from the results of a single-breath (with limits applied).

        // $[RM24008] Filtered compliance...
		compliance_ = complianceAlpha_ * compliance_ 
							 + (1.0F - complianceAlpha_) * singleBreathCompliance_;
		complianceAlpha_ = DM_ALPHA;
		timeSinceComplianceUpdate_ = 0;
	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateResistance_
//
//@ Interface-Description
//  Takes no arguments and returns nothing. Calculates and sets the 
//  dynamic resistance average.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//    Qi    = Volumetric Flow
//    Vi    = Delivered Volume
//    Pi    = Airway Pressure
//
//
//    Rdyn  = sum(Vi * Qi) * sum(Pi * Vi) - sum(Vi * Vi) * sum(Pi * Qi)
//            ---------------------------------------------------------
//            sum(Vi * Qi) * sum(Vi * Qi) - sum(Vi * Vi) * sum(Qi * Qi)
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
DynamicMechanics::updateResistance_(void)
{
	CALL_TRACE("updateResistance_(void)");

    // [RM24004] Calculate dynamic resistance.

	if (timeSinceResistanceUpdate_ > RESISTANCE_UPDATE_TIMEOUT)
	{
        // $[RM12068] The alpha-filtered Rdyn average shall be reset when no mandatory byyreaths have been delivered within the last 2 minutes.
		resistanceBreathCount_ = 0;
		resistanceAlpha_ = 0.0;
	}

	Real32 numerator   = sumVolumeDotFlow_ * sumPressureDotVolume_ - sumVolumeDotVolume_ * sumPressureDotFlow_;
	Real32 denominator = sumVolumeDotFlow_ * sumVolumeDotFlow_ - sumVolumeDotVolume_ * sumFlowDotFlow_;
	singleBreathResistance_ = numerator / denominator;

	// divide by zero result is not included in averages
	if ( !IsFpError(singleBreathResistance_) )
	{
		singleBreathResistance_ = MIN_VALUE(MAX_RESISTANCE, singleBreathResistance_);
		singleBreathResistance_ = MAX_VALUE(MIN_RESISTANCE, singleBreathResistance_);

        // $[RM24008] Filtered resistance...
		resistance_ = resistanceAlpha_ * resistance_ 
							 + (1.0F - resistanceAlpha_) * singleBreathResistance_;
		resistanceAlpha_ = DM_ALPHA;
		timeSinceResistanceUpdate_ = 0;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePeakSpontInspFlow_
//
//@ Interface-Description
//  Takes no arguments and returns nothing. Calculates and sets the 
//  peak spontaneous flow average.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This method uses an exponential moving average to compute the 
//  peak spontaneous flow.
//
//    PSF =  alpha * (singleBreathPsf) + (1-alpha) * PSF
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
DynamicMechanics::updatePeakSpontInspFlow_(void)
{
	CALL_TRACE("updatePeakSpontInspFlow_(void)");

	if (timeSincePsfUpdate_ > PSF_UPDATE_TIMEOUT)
	{
        // $[RM12239] The alpha-filtered PSF average shall be reset when no spontaneous breaths have been delivered within the last 2 minutes.
		psfBreathCount_ = 0;
		psfAlpha_ = 0.0;
	}

	singleBreathPsf_ = BreathRecord::GetPeakInspiratoryFlow();

	singleBreathPsf_ = MIN_VALUE(MAX_PEAK_SPONT_INSP_FLOW, singleBreathPsf_);
	singleBreathPsf_ = MAX_VALUE(MIN_PEAK_SPONT_INSP_FLOW, singleBreathPsf_);

    // $[RM24015] The 1st-order exponential filter is used to compute a breath-by-breath average of PSF for display purposes
	peakSpontInspFlow_ = psfAlpha_ * peakSpontInspFlow_ + (1.0F - psfAlpha_) * singleBreathPsf_;
	psfAlpha_ = DM_ALPHA;
	timeSincePsfUpdate_ = 0;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
DynamicMechanics::SoftFault(const SoftFaultID  softFaultID,
						const Uint32       lineNumber,
						const char*        pFileName,
						const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, DYNAMICMECHANICS,
							lineNumber, pFileName, pPredicate);
}


