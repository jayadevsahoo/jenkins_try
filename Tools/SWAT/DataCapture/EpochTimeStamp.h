//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file EpochTimeStamp.h
//----------------------------------------------------------------------------
#ifndef EPOCH_TIMESTAMP_H
#define EPOCH_TIMESTAMP_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "gettimeofday.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------
///A EpochTimeStamp is used to capture the current epoch time.
class EpochTimeStamp
{
    public:

    /// @brief Constructor
    EpochTimeStamp();

    /// @brief Constructor
    ///
    /// @param rts Value to set timestamp to.
    EpochTimeStamp(EpochTimeStamp &rts);

    /// @brief Destructor
    ~EpochTimeStamp();


    /// @brief set EpochTimeStamp to ts
    ///
    /// @param ts A reference to a timestamp.
    void set(const EpochTimeStamp &ts);

    /// @brief set EpochTimeStamp current time.
    void set();


    /// @brief getEpochTimeAsDouble
    /// 
    /// @return floating point repersentation of time since epoch.
    double getEpochTimeAsDouble();

    private:
    struct timeval m_tv;


    /// @brief timeval2epoch
    /// 
    /// @param tv A pointer to a struct timeval. 
    /// 
    /// @return Double containing timestap value.
    inline double timeval2epoch(struct timeval *tv)
    {
	    return ( (double)tv->tv_usec) / ((double)1000000 )   + tv->tv_sec;
    }

    /// @brief timeval2epoch
    /// 
    /// @param tv A pointer to a struct timespec. 
    /// 
    /// @return Double containing timestap value.
    inline double timespec2epoch(struct timespec *ts)
    {
	    return ( (double)ts->tv_nsec) / ((double)1000000000 ) + ts->tv_sec;
    }
	
};

#endif
