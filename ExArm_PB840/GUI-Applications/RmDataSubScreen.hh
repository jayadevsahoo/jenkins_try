#ifndef RMDataSubScreen_HH
#define RMDataSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: RmDataSubScreen - Subscreen for displaying supplemental 
//                           Respiratory Mechanics Patient Data.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/RmDataSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: rhj    Date: 05-Apr-2011   SCR Number: 6759
//  Project:  PROX
//  Description:
//      Added the missing wye symbol for patient data items f/Vt 
//      and Vdot e spont  
//  
//  Revision: 003   By: gdc   Date:  11-Aug-2008    SCR Number: 6439
//  Project:  840S
//  Description:
//        Removed touch screen "drill-down" work-around.
//
//  Revision: 002   By: rhj   Date:  20-Feb-2007    SCR Number: 6345
//  Project:  RESPM
//  Description:
//        Removed C20/C.
//
//  Revision: 001   By: rhj   Date:  10-May-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//       RESPM Project Initial Version
//
//====================================================================

#include "BdEventTarget.hh"
#include "PatientDataTarget.hh"
#include "SubScreen.hh"
#include "GuiEventTarget.hh"

//@ Usage-Classes
#include "NumericField.hh"
#include "TouchableText.hh"
#include "ContextObserver.hh"
//@ End-Usage
#include "TextButton.hh"
#include "SubScreenArea.hh"
#include "MoreDataSubScreen.hh"
#include "PauseTypes.hh"
class SubScreenArea;

class RmDataSubScreen : public SubScreen,
                            public BdEventTarget,
                            public GuiEventTarget,
                            public PatientDataTarget,
                            public ContextObserver
{
public:
    RmDataSubScreen(SubScreenArea* pSubScreenArea);
    ~RmDataSubScreen(void);

    virtual void activate(void);
    virtual void deactivate(void);

    // ContextObserver virtual method...
    virtual void  batchSettingUpdate(
                             const Notification::ChangeQualifier qualifierId,
                             const ContextSubject*               pSubject,
                             const SettingId::SettingIdType      settingId
                                    );

    virtual void patientDataChangeHappened(
                            PatientDataId::PatientItemId patientDataId);

    virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

    virtual void bdEventHappened(EventData::EventId eventId,
                                    EventData::EventStatus eventStatus,
                                    EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);

    virtual void buttonDownHappened(Button* pButton,
                                    Boolean isByOperatorAction);


    static void SoftFault(const SoftFaultID softFaultID,
                          const Uint32      lineNumber,
                          const char*       pFileName  = NULL, 
                          const char*       pPredicate = NULL);

protected:

private:
    // these methods are purposely declared, but not implemented...
    RmDataSubScreen(void);                       // not implemented...
    RmDataSubScreen(const RmDataSubScreen&);  // not implemented...
    void operator=(const RmDataSubScreen&);      // not implemented...

    void  updateShowStates_(void);
    void  updateDataValue_ (const PatientDataId::PatientItemId  patientDataId);
    void  updateSettingApplicability_(void);

    void displayRstat_(PauseTypes::ComplianceState resistanceFlag);
    void displayCstat_(PauseTypes::ComplianceState complianceFlag);
    void updateVolumeLabel_(void);

    //@ Data-Member: backButton_
    // A Back button to allow transition from the RM Subscreen
    // back to the More data Subscreen.
    TextButton backButton_;

    //@ Data-Member:  dynamicCompliance{Label,Value,Unit}_
    // Label/Value/Unit for Dynamic Compliance.
    TouchableText  dynamicComplianceLabel_;
    NumericField   dynamicComplianceValue_;
    TextField      dynamicComplianceUnit_;

    //@ Data-Member:  dynamicResistance{Label,Value,Unit}_
    // Label/Value/Unit for Dynamic Resistance.
    TouchableText  dynamicResistanceLabel_;
    NumericField   dynamicResistanceValue_;
    TextField      dynamicResistanceUnit_;


    //@ Data-Member:  peakSpontFlow{Label,Value,Unit}_
    // Label/Value/Unit for Peak Spontaneous Flow.
    TouchableText  peakSpontFlowLabel_;
    NumericField   peakSpontFlowValue_;
    TextField      peakSpontFlowUnit_;


    //@ Data-Member:  peakExpFlow{Label,Value,Unit}_
    // Label/Value/Unit for Peak Expiratory Flow.
    TouchableText  peakExpFlowLabel_;
    NumericField   peakExpFlowValue_;
    TextField      peakExpFlowUnit_;

    //@ Data-Member:  endExpFlow{Label,Value,Unit}_
    // Label/Value/Unit for End Expiratory Flow.
    TouchableText  endExpFlowLabel_;
    NumericField   endExpFlowValue_;
    TextField      endExpFlowUnit_;

    //@ Data-Member:  spontFvRatio{Label,Value}_
    // Label/Value for Spontaneous Rapid/Shallow Breathing Index.
    TouchableText  spontFvRatioLabel_;
    NumericField   spontFvRatioValue_;

    //@ Data-Member:  pavIntrinsicPeep{Label,Value,Unit,Marker}_
    // Label/Value/Unit/Marker for the PAV Work of Breathing.
    TouchableText  intrinsicPeepLabel_;
    NumericField   intrinsicPeepValue_;
    TextField      intrinsicPeepUnit_;
    TextField      intrinsicPeepMarkerText_;

    //@ Data-Member:: notAvailableLabel_
    // Touchable text which contains the "Not Available" message.
    TouchableText  notAvailableLabel_;

    //@ Data-Member:: previousPData_
    // Flag indicates the previous patient data value.
    Int32 previousPData_;

    //@ Data-Member:: isInSpontMode_
    // Flag indicates that spont mode is active.
    Boolean  isInSpontMode_;

    //@ Data-Member:: isSpontTypeApplic_
    // Flag indicates that spont type is applicable.
    Boolean  isSpontTypeApplic_;

    //@ Data-Member:: isNivActive_
    // Flag indicates that NIV is active.
    Boolean  isNivActive_;         

    //@ Data-Member:: isBilevelActive_
    // Flag indicates that BILEVEL is active.
    Boolean  isBilevelActive_;

    //@ Data-Member:: isPaActive_
    // Flag indicates that PAV is active.
    Boolean  isPaActive_;

    //@ Data-Member:: isVcpActive_
    // Flag indicates that VCP is active.
    Boolean  isVcpActive_ ;

    //@ Data-Member:: isVsActive_
    // Flag indicates that VS is active.
    Boolean  isVsActive_;

    //@ Data-Member:  cStatic{Label,Value,Unit,Not Available,
    //                       Marker Text, Inadequate Text}_
    // Label/Value/Unit/ Not Available "****"/ 
    // Marker Text "()" / Inadequate Text "----"  for cstatic
    TouchableText  cStaticLabel_;
    NumericField   cStaticValue_;
    TextField      cStaticUnit_;
    TextField      cStaticNotAvail_;
    TextField      cStaticMarkerText_;
    TextField      cStaticInadequateText_;


    //@ Data-Member:  rStatic{Label,Value,Unit,Not Available,
    //                       Marker Text, Inadequate Text}_
    // Label/Value/Unit/ Not Available "****"/ 
    // Marker Text "()" / Inadequate Text "----"  for Rstatic
    TouchableText  rStaticLabel_;
    NumericField   rStaticValue_;
    TextField      rStaticUnit_;
    TextField      rStaticNotAvail_;
    TextField      rStaticMarkerText_;
    TextField      rStaticInadequateText_;

    //@ Data-Member: resistanceFlag_
    // A flag which stores the status of the value of Rstat.
    PauseTypes::ComplianceState resistanceFlag_;

    //@ Data-Member: complianceFlag_
    // A flag which stores the status of the value of Cstat.
    PauseTypes::ComplianceState complianceFlag_;

    //@ Data-Member: pressureStability_
    // A flag which stores the status of the value of PeepI.
    PauseTypes::ComplianceState pressureStability_;

    //@ Data-Member: vcpStatusMsg_
    // Status message displaying the current state of VCP.
    TextField vcpStatusMsg_;

    //@ Data-Member: vsStatusMsg_
    // Status message displaying the current state of VS.
    TextField vsStatusMsg_;

    //@ Data-Member: pavStatusMsg_
    // Status message displaying the current state of PAV.
    TextField pavStatusMsg_;

    //@ Data-Member:: isRmAvailable_
    // Flag indicates whether RM values can be displayed or not.
    Boolean  isRmAvailable_;

    //@ Data-Member:: cStatRstatInitiated_
    // Flag indicates whether Cstat or Rstat has been initiated or not.
    Boolean  cStatRstatInitiated_;

    //@ Data-Member:: peepiInitiated_
    // Flag indicates whether Peepi has been initiated or not.
    Boolean  peepiInitiated_;

	//@ Data-Member: isProxInop_
	// Is proximal board inoperative?
	Boolean isProxInop_;

	//@ Data-Member: isProxInStartup_
	// Is proximal board in startup mode?
    Boolean isProxInStartup_;


};


#endif // RMDataSubScreen_HH 


