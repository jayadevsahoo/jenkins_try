// UnitTest.h : Jayadev - This file is generated for Testing purpose only
// using VectorCAST tool

#pragma once

#include <string.h>
#include <winsock2.h>
#if 0
#define _WIN32_WCE 0x0550

//winuser.h
typedef struct tagNMHDR
{
    HWND  hwndFrom;
    UINT  idFrom;
    UINT  code;         // NM_ code
}   NMHDR;
typedef NMHDR FAR * LPNMHDR;

 
//winbase.h

#define GlobalAlloc(flags, cb)              LocalAlloc(flags, cb)
#define GlobalFree(handle)                  LocalFree(handle)
#define GlobalReAlloc(handle, cb, flags)    LocalReAlloc(handle, cb, LMEM_MOVEABLE)
//#define GlobalLock(lp)                      LocalLock(lp)
//#define GlobalHandle(lp)                    LocalHandle(lp)
//#define GlobalUnlock(hMem)                  LocalUnlock(hMem)
//#define GlobalSize(hMem)                    LocalSize(hMem)
//#define GlobalFlags(X)                      LocalFlags(X)
//#define LocalPtrHandle(lp)                  ((HLOCAL)LocalHandle(lp))
//#define LocalLockPtr(lp)                    ((BOOL)LocalLock(LocalPtrHandle(lp)))
//#define LocalUnlockPtr(lp)                  LocalUnlock(LocalPtrHandle(lp))
//#define LocalFreePtr(lp)                    (LocalUnlockPtr(lp), (BOOL)LocalFree(LocalPtrHandle(lp)))

#define GMEM_FIXED          LMEM_FIXED

HRSRC (*FindResourceEx)(hInstance, RT_STRING, MAKEINTRESOURCE( ((id>>4)+1) ), wLanguage);
HRESULT SafeArrayGetVartype(struct tagSAFEARRAY*, VARTYPE*);

#endif


