#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Area - This class represents an area as a rectangle.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is used to encapsulate the values defining an area (x1, y1, x2,
// y2) in a single place so they can be manipulated as a single unit.  This
// class provides methods to set and retrieve Area's private data, along with
// methods to get and set the width and height, to add and subtract areas, to
// determine overlap, containment, equality relationships between areas, and
// many others.
//
// The only private data carried around by this class are the coordinates of
// the upper left and lower right corners of the area.  The width and height of
// the area are derived from these coordinates.  The "location" of the area is
// confined by the coordinate of the upper left and lower right corners
// inclusively.  This class has no virtual functions and therefore no virtual
// table.  This class is not intended for use as a parent class.
//---------------------------------------------------------------------
//@ Rationale
// Many operations such as checking overlap or containment use x,y, width and
// height values.  The four values will often be grouped together so they can
// be passed around as a single parameter, instead of passing four parameters
// together.  For this reason this class is kept very "lightweight"; that is,
// the amount of class data is kept to a minimum, and none of its methods are
// virtual.  Because this is a basic, widely used class, it provides an
// opportunity to group (and thereby hide) a lot of functionality.  Therefore,
// a rich set of methods is provided.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class is implemented so as to minimize the memory used by an instance.
// For this reason there are no virtual functions, to avoid the overhead of a
// virtual table.  Since it will be used in performance-critical code, many of
// its less-complex methods are implemented as inline.
//
// The private data consists of the x-y coordinates of the upper left and lower
// right pixels, inclusive.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Area.ccv   25.0.4.0   19 Nov 2013 14:11:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 06-Dec-2006   SCR Number: 6237
//  Project:  Trending
//  Description:
//		Self correct malformed area during construction to
// 		avoid trifling assertions
//
//  Revision: 002   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Area.hh"


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Area
//
//@ Interface-Description
// This is a default constructor.  It simply sets the area's data to 0.
//---------------------------------------------------------------------
//@ Implementation-Description
// This default constructor sets the x-y coordinates of the area's
// upper left and lower right corners to 0.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Area::Area(void) :
	x1_(0),
	y1_(0),
	x2_(0),
	y2_(0)
{
	CALL_TRACE("Area::Area(void)");
	//			$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Area (x1, y1, x2, y2) [constructor]
//
//@ Interface-Description
// This constructor initializes the x-y coordinates of the area's upper
// left and lower right corners to the passed values.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just assign the private members to the arguments passed in. If the
// requested coordinates are invalid, construct a null area with all
// coordinates set to zero.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Area::Area(Int x1, Int y1, Int x2, Int y2) :
	x1_(x1),
	y1_(y1),
	x2_(x2),
	y2_(y2)
{
	CALL_TRACE("Area::Area(Int, Int, Int, Int)");
	if ( !((x1 <= x2) && (y1 <= y2)) )
	{
		// The diagnostic output below identifies the problem Area when
		// running in a development environment. There is no output
		// during production since all console output is disgarded.
		x1_ = 0;
		y1_ = 0;
		x2_ = 0;
		y2_ = 0;
	}
	//			$[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Area
//
//@ Interface-Description
// This is the copy constructor. It initializes an area with initial values
// which are the same as the given "otherArea".
//---------------------------------------------------------------------
//@ Implementation-Description
// Copy member data from "otherArea".
//---------------------------------------------------------------------
//@ PreCondition
//  otherArea.x1_ <= otherArea.x2_ && otherArea.y1_ <= otherArea.y2_
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Area::Area(const Area& otherArea)
{
	CALL_TRACE("Area::Area(const Area&)");
	SAFE_CLASS_PRE_CONDITION((otherArea.x1_ <= otherArea.x2_) &&
						(otherArea.y1_ <= otherArea.y2_));
	//			$[TI3]
	*this = otherArea;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Area
//
//@ Interface-Description
//  Destroy object. Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Area::~Area(void)
{
	CALL_TRACE("Area::~Area(void)");
	//			$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: set
//
//@ Interface-Description
// This method sets the x-y coordinates of the area's upper
// left and lower right corners to the passed values.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just assign the private members to the arguments passed in.
//---------------------------------------------------------------------
//@ PreCondition
//  "x1" <= "x2" && "y1" <= "y2"
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Area::set(Int x1, Int y1, Int x2, Int y2)
{
	CALL_TRACE("Area::set(Int, Int, Int, Int)");
	CLASS_PRE_CONDITION((x1 <= x2) && (y1 <= y2));

	//													 	$[TI1]
	x1_ = x1;
	y1_ = y1;
	x2_ = x2;
	y2_ = y2;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setX
//
//@ Interface-Description
// Moves the X coordinate of the area to the given "x" value.
// This refers to the X coordinate of the upper left corner.
// This method differs from the setX1() method in that it does not affect
// the width of the area; both the left and right edges of the area are
// affected.
//---------------------------------------------------------------------
//@ Implementation-Description
// Record the area width before making the change and then set
// it again afterwards.  This has the effect of making sure that x2_ is
// set to the correct value at the end of the function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Area::setX(Int x)
{
	CALL_TRACE("Area::setX(Int)");

	//													 	$[TI1]
	Int width = getWidth();
	x1_ = x;
	setWidth(width);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setY
//
//@ Interface-Description
// Moves the Y coordinate of the area to the given "y" value.
// This refers to the Y coordinate of the upper left corner.
// This method differs from the setY1() method in that it does not affect
// the height of the area; both the upper and lower edges of the area are
// affected.
//---------------------------------------------------------------------
//@ Implementation-Description
// Record the area height before making the change and then set
// it again afterwards.  This has the effect of making sure that y2_ is
// set to the correct value at the end of the function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Area::setY(Int y)
{
	CALL_TRACE("Area::setY(Int)");

	//													 	$[TI1]
	Int height = getHeight();
	y1_ = y;
	setHeight(height);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: moveBy
//
//@ Interface-Description
// Changes the relative location of this area by the given "xOffset" and
// "yOffset".
//---------------------------------------------------------------------
//@ Implementation-Description
// Only the location of the area changes -- the width and height remain
// unchanged.  For example, if x1,y1 position is 10,20 and moveBy(4,5)
// is called, then the resulting x1,y1 is 14,25.  Adds the given
// "xOffset" to x1_ and x2_.  Adds the given "yOffset" to y1_ and y2_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Area::moveBy(Int xOffset, Int yOffset)
{
	CALL_TRACE("Area::moveBy(Int, Int)");

	//			$[TI1]
	x1_ += xOffset;
	x2_ += xOffset;

	y1_ += yOffset;
	y2_ += yOffset;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: overlapsArea
//
//@ Interface-Description
// Returns TRUE if this area overlaps with (intersects) the given
// "otherArea".  If either area completely contains the other area, then
// the result is still TRUE.
//---------------------------------------------------------------------
//@ Implementation-Description
// There are basically two tests.  The first is for overlap in the x
// direction and the second is for overlap in the y direction.
//
// Note that this algorithm depends on the invariant which states that
// x1_ <= x2_ and y1_ <= y2_ being true.  If the invariant is ever
// changed then this code will need to be updated.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
Area::overlapsArea(const Area& otherArea) const
{
	CALL_TRACE("Area::OverlapsArea(const Area&) const");

	SAFE_CLASS_ASSERTION((x1_ <= x2_) && (y1_ <= y2_));

	SAFE_CLASS_ASSERTION((otherArea.x1_ <= otherArea.x2_) &&
						 (otherArea.y1_ <= otherArea.y2_));

	//									 	$[TI1], $[TI2]
	return ((x2_ >= otherArea.x1_) &&
			(x1_ <= otherArea.x2_) &&
			(y2_ >= otherArea.y1_) &&
			(y1_ <= otherArea.y2_));

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: makeOverlap
//
//@ Interface-Description
// If this area overlaps with the given "otherArea", returns TRUE and
// places the intersection area in "intersectionArea".  Otherwise, if
// this area does not overlap with "otherArea", returns FALSE, leaving
// "intersectionArea" unmodified.  Note that the second parameter
// "intersectionArea", passed as a reference, is used as an OUTPUT.
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the invariant assumption that x1_ <= x2_ and y1_ <= y2_ for any
// area, it turns out that the overlapping area (intersection) is
// defined by two points which are: (max (x1_, otherArea.x1_), max(y1,
// otherArea.y1_)) as the upper left point and (min(x2_, otherArea.x2_),
// min(y2, otherArea.y2_)) as the lower right point.
//---------------------------------------------------------------------
//@ PreCondition
//  otherArea.x1_ <= otherArea.x2_ && otherArea.y1_ <= otherArea.y2_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
Area::makeOverlap(const Area &otherArea, Area &intersectionArea) const
{
	CALL_TRACE("Area::makeOverlap(const Area&, Area&) const");
	SAFE_CLASS_PRE_CONDITION((otherArea.x1_ <= otherArea.x2_) &&
						(otherArea.y1_ <= otherArea.y2_));

	if (overlapsArea(otherArea))
	{	//												 	$[TI1]
		intersectionArea.set(MAX_VALUE(x1_, otherArea.x1_),
							 MAX_VALUE(y1_, otherArea.y1_),
							 MIN_VALUE(x2_, otherArea.x2_),
							 MIN_VALUE(y2_, otherArea.y2_));
		return TRUE;
	}

	//													 	$[TI2]
	return FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: makeMerge
//
//@ Interface-Description
// Computes the enclosing box of this area and the given "otherArea" and
// returns the result in "unionArea". Note that the second parameter 
// "unionArea", passed as a reference, is used as an OUTPUT.
//---------------------------------------------------------------------
//@ Implementation-Description
// Computes the combined bounding area of this area and the "otherArea",
// regardless of whether they overlap.
//---------------------------------------------------------------------
//@ PreCondition
//  otherArea.x1_ <= otherArea.x2_ && otherArea.y1_ <= otherArea.y2_
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Area::makeMerge(const Area& otherArea, Area& unionArea) const
{
	CALL_TRACE("Area::makeMerge(const Area&, Area&) const")
	SAFE_CLASS_PRE_CONDITION((otherArea.x1_ <= otherArea.x2_) &&
						(otherArea.y1_ <= otherArea.y2_));

	//													 	$[TI1]
	unionArea.set(MIN_VALUE(x1_, otherArea.x1_),
				  MIN_VALUE(y1_, otherArea.y1_),
				  MAX_VALUE(x2_, otherArea.x2_),
				  MAX_VALUE(y2_, otherArea.y2_));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: containsPoint
//
//@ Interface-Description
// Returns TRUE if the given "x" and "y" coordinates lie within this
// area.  Note that lying on the border of the area counts as being
// contained within the area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Basically the test is divided into two parts.  First a check is done
// to see if the x coordinate falls between the x coordinates of the
// area.  Second a check is made to see if the y coordinate falls
// between the first and second y coordinates.
//
// It should be noted that this algorithm depends on the invariant which
// states that x1_ <= x2_ and y1_ <= y2_ being true.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
Boolean
Area::containsPoint(Int x, Int y) const
{
	CALL_TRACE("Area::containsPoint(Int, Int) const");
	SAFE_CLASS_ASSERTION(x1_ <= x2_ && y1_ <= y2_);

	//													 	$[TI1], $[TI2]
	return ((x >= x1_) && (x <= x2_) &&
			(y >= y1_) && (y <= y2_));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: containsArea
//
//@ Interface-Description
// Returns TRUE if the given "otherArea" is completely contained within
// this area.  Areas may share one or more edges and still be contained
// within one another.  Hence if area A and area B are equal then A
// contains B and B contains A.
//---------------------------------------------------------------------
//@ Implementation-Description
// Check whether or not bounding box of "otherArea" fits within our
// bounding box.
//---------------------------------------------------------------------
//@ PreCondition
//  otherArea.x1_ <= otherArea.x2_ && otherArea.y1_ <= otherArea.y2_
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
Area::containsArea(const Area& otherArea) const
{
	CALL_TRACE("Area::containsArea(const Area&) const");
	SAFE_CLASS_PRE_CONDITION((otherArea.x1_ <= otherArea.x2_) &&
						(otherArea.y1_ <= otherArea.y2_));

	//													 	$[TI1], $[TI2]
	return ((x1_ <= otherArea.x1_) && (y1_ <= otherArea.y1_) &&
			(x2_ >= otherArea.x2_) && (y2_ >= otherArea.y2_));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: alignArea
//
//@ Interface-Description
// Initializes the "resultArea" with the given "width" and "height", then
// left, right, or center-justifies the "resultArea" within this area,
// based on the given "horzAlignment" value. Note that the last parameter
// "resultArea", passed as a reference, is used as an OUTPUT.
// Note that the passed width and height may define an area larger than
// this area.
//---------------------------------------------------------------------
//@ Implementation-Description
// The computation is carried out in three steps.  First, compute the
// *relative* X and Y coordinates for the "resultArea".  Second, set the
// width and height of the "resultArea".  Finally, move the "resultArea"
// to the *absolute* coordinates that apply to this area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Area::alignArea(Int width, Int height, Alignment horzAlignment,
				Area& resultArea) const
{
	CALL_TRACE("Area::alignArea(Int, Int, Alignment, Area&)");

	//
	// First, vertically center the "resultArea".  Note that this is
	// independent of the horizontal alignment parameter.
	//
	resultArea.setY(getHeight()/2 - height/2);

	//
	// Second, horizontally justify the "resultArea".
	//
	switch (horzAlignment)
	{													// $[TI1]
	case LEFT:											// $[TI1.1]
		resultArea.setX(0);
		break;
	case RIGHT:											// $[TI1.2]
		resultArea.setX(getWidth() - width);
		break;
	case CENTER:										// $[TI1.3]
		resultArea.setX(getWidth()/2 - width/2);
		break;
	default:
		CLASS_ASSERTION(FALSE);
		break;
	}

	//
	// Now set width and height.
	//
	resultArea.setWidth(width);
	resultArea.setHeight(height);

	//
	// Finally, move the entire area to put it in the same coordinate
	// system as this area.
	//
	resultArea.moveBy(x1_, y1_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: subtractArea
//
//@ Interface-Description
// Computes the intersection of the given "otherArea" and this area.  If
// removing the intersection from this area leaves a rectangle,
// modify this area to be that rectangle and return TRUE.  Otherwise, if
// the removal of the intersection from this area does not result in a
// rectangle or if this area is completely contained within "otherArea",
// then return FALSE without modifying this area.
//
// In other words, this method allows an area to be clipped off any of the
// four edges of this area - but clipping must occur along a single
// unbroken line.
//---------------------------------------------------------------------
//@ Implementation-Description
// The "subtraction" only occurs if the "otherArea" completely encloses
// exactly one edge of this area.  Thus, there are four cases to test --
// bottom edge, top edge, right edge, and left edge.  If any test comes
// up positive, then perform the subtraction and return TRUE.  Otherwise,
// just return FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	otherArea.x1_ <= otherArea.x2_ && otherArea.y1_ <= otherArea.y2_
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
Area::subtractArea(const Area& otherArea)
{
	CALL_TRACE("Area::subtractArea(const Area&)")
	SAFE_CLASS_PRE_CONDITION((otherArea.x1_ <= otherArea.x2_) &&
						(otherArea.y1_ <= otherArea.y2_));

	if ((x1_ >= otherArea.x1_) && (x2_ <= otherArea.x2_))
	{	//												 	$[TI1]
		//
		// otherArea can completely cover both the left and right edges of
		// this area (i.e., otherArea is "wider"), so check to see whether the
		// otherArea covers only a top or a bottom edge.
		//
		if ((y1_ < otherArea.y1_) && (y2_ >= otherArea.y1_ && y2_ <= otherArea.y2_))
		{	//											   $[TI1.1]
			//
			// otherArea covers only the bottom edge of this area
			//
			y2_ = otherArea.y1_ - 1;
			return TRUE;
		}
		else if ((y1_ >= otherArea.y1_ && y1_ <= otherArea.y2_) &&
				 (y2_ > otherArea.y2_))
		{	//											 	$[TI1.2]
			//
			// otherArea covers only the top edge of this area
			//
			y1_ = otherArea.y2_ + 1;
			return TRUE;
		}
		else
		{	//											 	$[TI1.3]
			//
			// otherArea completely contains or does not contain the upper
			// and lower edge of this area.
			//
			return FALSE;
		}
	}
	else if ((y1_ >= otherArea.y1_) && (y2_ <= otherArea.y2_))
	{	//													 $[TI2]
		//
		// otherArea can completely cover both the top and bottom edges of
		// this area (i.e., otherArea is "taller"), so check to see whether the
		// otherArea covers only a left or a right edge.
		//

		if ((x1_ < otherArea.x1_) && (x2_ >= otherArea.x1_ && x2_ <= otherArea.x2_))
		{	//												$[TI2.1]
			//
			// otherArea covers only the right edge of this area
			//
			x2_ = otherArea.x1_ - 1;
			return TRUE;
		}
		else if ((x1_ >= otherArea.x1_ && x1_ <= otherArea.x2_) && (x2_ > otherArea.x2_))
		{	//												$[TI2.2]
			//
			// otherArea covers only the left edge of this area
			//
			x1_ = otherArea.x2_ + 1;
			return TRUE;
		}
		else
		{	//												$[TI2.3]
			//
			// otherArea completely contains or does not contain the left and
			// right edge of this area.
			//
			return FALSE;
		}
	}
	else
	{	//													$[TI3]
		//
		// otherArea cannot completely cover an edge of this area.
		//
		return FALSE;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator==
//
//@ Interface-Description
// Returns TRUE if this area and the given "otherArea" are equivalent.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply compare the member data for equivalence.
//---------------------------------------------------------------------
//@ PreCondition
//	otherArea.x1_ <= otherArea.x2_ && otherArea.y1_ <= otherArea.y2_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
Area::operator==(const Area& otherArea) const
{
	CALL_TRACE("Area::operator==(const Area&) const");
	SAFE_CLASS_PRE_CONDITION((otherArea.x1_ <= otherArea.x2_) &&
						(otherArea.y1_ <= otherArea.y2_));

	//													 	$[TI1], $[TI2]
	return ((x1_ == otherArea.x1_) &&
			(x2_ == otherArea.x2_) &&
			(y1_ == otherArea.y1_) &&
			(y2_ == otherArea.y2_));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator!=
//
//@ Interface-Description
// Returns TRUE if this area and the given "otherArea" are not
// equivalent.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply compare the member data for non-equivalence.
//---------------------------------------------------------------------
//@ PreCondition
//	otherArea.x1_ <= otherArea.x2_ && otherArea.y1_ <= otherArea.y2_
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
Boolean
Area::operator!=(const Area& otherArea) const
{
	CALL_TRACE("Area::operator!=(const Area&) const");
	SAFE_CLASS_PRE_CONDITION((otherArea.x1_ <= otherArea.x2_) &&
						(otherArea.y1_ <= otherArea.y2_));

	//													 	$[TI1], $[TI2]

	return(!(*this == otherArea));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
// This operator allows areas to be assigned to one another and is
// equivalent to assigning the individual X's and Y's.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply copy all the member data.
//---------------------------------------------------------------------
//@ PreCondition
//	otherArea.x1_ <= otherArea.x2_ && otherArea.y1_ <= otherArea.y2_
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Area::operator=(const Area& otherArea)
{
	CALL_TRACE("Area::operator=(const Area&)");
	SAFE_CLASS_PRE_CONDITION((otherArea.x1_ <= otherArea.x2_) &&
						(otherArea.y1_ <= otherArea.y2_));

	if (&otherArea == this)								// $[TI1]
	{
		return;
	}
	//													$[TI2]	

	x1_ = otherArea.x1_;
	y1_ = otherArea.y1_;
	x2_ = otherArea.x2_;
	y2_ = otherArea.y2_;
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ("CLASS_PRE_CONDITION()", etc.). The "oftFaultID" and
//	"lineNumber" are essential pieces of information.  The "pFileName"
//	and "pPredicate" strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds its sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Area::SoftFault(const SoftFaultID  softFaultID,
				const Uint32       lineNumber,
				const char*        pFileName,
				const char*        pPredicate)
{
	CALL_TRACE("Area::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, AREA,
							lineNumber, pFileName, pPredicate);
}
