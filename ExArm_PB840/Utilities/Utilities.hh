
#ifndef Utilities_HH
#define Utilities_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Utilities - The Utilities Subsystem's Class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/Utilities.hhv   25.0.4.0   19 Nov 2013 14:35:16   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//
//   Revision: 001  By:  sah    Date:  19-10-93    DR Number: 
//   Project:  Sigma (R8027)
//   Description:
//        Initial version 
//  
//====================================================================

#include "SigmaTypes.hh"
#include "FaultHandlerMacros.hh"
#include "UtilityClassId.hh"

//@ Usage-Classes
//@ End-Usage


class Utilities
{
  public:
    static void  Initialize(void);

  private:
    Utilities(const Utilities&);		// not implemented...
    Utilities(void);				// not implemented...
    ~Utilities(void);				// not implemented...
    void  operator=(const Utilities&);		// not implemented...
};


// Inlined Methods...
#include "Utilities.in"


#endif // Utilities_HH 
