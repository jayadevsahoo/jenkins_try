#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  MinInspFlowSetting - Minimum Inspiratory Flow Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the minimum rate of
//  delivery (in liters-per-minute) of the specified "tidal volume" during
//  mandatory, volume-based breaths (VCV).  This class inherits from
//  'BoundedSetting' and provides the specific information needed for
//  representation of minimum inspiratory flow.  This information includes
//  the interval and range of this setting's values, this setting's response
//  to a Main Control Setting's transition (see 'acceptTransition()'), and
//  this batch setting's new-patient value (see 'getNewPatientValue()').
//
//  This class provides a static method for calculating minimum inspiratory
//  flow based on the value of other settings.  This calculation method
//  provides a standard way for all settings (as needed) to calculate a
//  minimum inspiratory flow value.
//
//  This class cannot be directly changed by the operator, yet it is not
//  fixed.  This setting is strictly changed via changes to peak inspiratory
//  flows, via a dependent relationship.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings, or non-fixed contraints.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/MinInspFlowSetting.ccv   25.0.4.0   19 Nov 2013 14:27:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: sah   Date:  16-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  modified 'getApplicability()' to base this tracking setting's
//         applicability purely on the applicability of the settings it
//         tracks
//      *  changed this setting from a "dependent setting" of peak
//         insp flow, to using the Observer/Subject mechanism as a
//         "tracking setting"; as a tracking setting, this now needs to
//         override some 'SettingObserver' methods ('valueUpdate()',
//         'doRetainAttachment()' and 'settingObserverInit()'), and no
//         longer needs dependent and transition methods
//         ('acceptPrimaryChange()' and 'acceptTransition()')
//
//  Revision: 005   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed now-unneeded 'calcNewValue()'
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.18.1.0) into Rev "BiLevel" (1.18.2.0)
//	Modified resolution to improve accuracies of calculations
//	based on this setting.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "MinInspFlowSetting.hh"
#include "MandTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02197] -- The setting's range ...
//  $[02199] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    0.8f,		// absolute minimum...
    0.0f,		// unused...
    HUNDREDTHS,		// unused...
    NULL
  };

static const BoundedInterval  INTERVAL_LIST_ =
  {
    10000.0f,		// absolute maximum...
    0.01f,		// resolution...
    HUNDREDTHS,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: MinInspFlowSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

MinInspFlowSetting::MinInspFlowSetting(void)
       : BatchBoundedSetting(SettingId::MIN_INSP_FLOW,
			     Setting::NULL_DEPENDENT_ARRAY_,
			     &::INTERVAL_LIST_,
			     NULL_SETTING_BOUND_ID,	// maxConstraintId...
			     NULL_SETTING_BOUND_ID)	// minConstraintId...
{
  CALL_TRACE("MinInspFlowSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~MinInspFlowSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

MinInspFlowSetting::~MinInspFlowSetting(void)
{
  CALL_TRACE("~MinInspFlowSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
MinInspFlowSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pPeakFlow =
			SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW);

  const Applicability::Id  PEAK_FLOW_APPLIC_ID =
				   pPeakFlow->getApplicability(qualifierId);

  return((PEAK_FLOW_APPLIC_ID != Applicability::INAPPLICABLE)
	  ? Applicability::VIEWABLE		// $[TI1]
	  : Applicability::INAPPLICABLE);	// $[TI2]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
MinInspFlowSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;
  
  newPatient = calcBasedOnSetting_(SettingId::PEAK_INSP_FLOW,
  				   BoundedRange::WARP_NEAREST,
				   BASED_ON_NEW_PATIENT_VALUES);

  return(newPatient);
}   // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to value changes of tidal volume and respiratory rate, by
//  re-calculating current value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02197]      -- equal to 'MAX(0.8, (Qmax * 0.1))'
//  $[02022]\g\   -- from-PC-to-VC transition rules
//  $[VC02006]\d\ -- from-VC-to-VC+ transition rules
//  $[VC02006]\i\ -- from-PC-to-VC+ transition rules
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::PEAK_INSP_FLOW)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
MinInspFlowSetting::valueUpdate(
			    const Notification::ChangeQualifier qualifierId,
			    const SettingSubject*               pSubject
				    )
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");
  SAFE_AUX_CLASS_PRE_CONDITION(
			(pSubject->getId() == SettingId::PEAK_INSP_FLOW),
		        pSubject->getId()
			      );

  if (qualifierId == Notification::ADJUSTED)
  {  // $[TI1] -- peak flow's adjusted value changed...
    const BoundedValue  MIN_INSP =
			 calcBasedOnSetting_(SettingId::PEAK_INSP_FLOW,
					     BoundedRange::WARP_NEAREST,
					     BASED_ON_ADJUSTED_VALUES);

    // store the value...
    setAdjustedValue(MIN_INSP);
  }  // $[TI2] -- don't care about accepted value changes...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
MinInspFlowSetting::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of apnea peak inspiratory flow,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to those (subject) settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
MinInspFlowSetting::settingObserverInit(void)
{
  CALL_TRACE("settingObserverInit()");

  attachToSubject_(SettingId::PEAK_INSP_FLOW, Notification::VALUE_CHANGED);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?  Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
MinInspFlowSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) !=
					    Applicability::INAPPLICABLE)
    {
      // is the the accepted minimum inspiratory flow value consistent
      // with the accepted peak inspiratory flow-based bound?...
      isValid = (BoundedValue(getAcceptedValue()) ==
		 calcBasedOnSetting_(SettingId::PEAK_INSP_FLOW,
				     BoundedRange::WARP_NEAREST,
				     BASED_ON_ACCEPTED_VALUES));
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?  Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
MinInspFlowSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean isValid;

  // is the adjusted minimum inspiratory flow value within its min
  // and max range?...
  isValid = BoundedSetting::isAdjustedValid();  // forward to base class...

  const Setting*  pMandType =
			  SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

  const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

  if (isValid  &&  MAND_TYPE_VALUE == MandTypeValue::VCV_MAND_TYPE)
  {
    // is the the adjusted minimum inspiratory flow value consistent
    // with the adjusted peak inspiratory flow-based bound?...
    isValid = (BoundedValue(Setting::getAdjustedValue()) ==
	       calcBasedOnSetting_(SettingId::PEAK_INSP_FLOW,
				   BoundedRange::WARP_NEAREST,
				   BASED_ON_ADJUSTED_VALUES));
  }

  return(isValid);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MinInspFlowSetting::SoftFault(const SoftFaultID  softFaultID,
			      const Uint32       lineNumber,
			      const char*        pFileName,
			      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  MIN_INSP_FLOW_SETTING, lineNumber,
			  pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnSettingId == SettingId::PEAK_INSP_FLOW  &&
//  (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//   basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
MinInspFlowSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
				       ) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

  const Setting*  pPeakInspFlow = 
			SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW);

  Real32  peakInspFlowValue;

  switch (basedOnCategory)
  {
  case BASED_ON_NEW_PATIENT_VALUES :		// $[TI1]
    // get the new-patient value of peak inspiratory flow...
    peakInspFlowValue = BoundedValue(pPeakInspFlow->getNewPatientValue()).value;
    break;
  case BASED_ON_ADJUSTED_VALUES :		// $[TI2]
    // get the "adjusted" value of peak inspiratory flow...
    peakInspFlowValue = BoundedValue(pPeakInspFlow->getAdjustedValue()).value;
    break;
  case BASED_ON_ACCEPTED_VALUES :
    // fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
    // get the "accepted" value of peak inspiratory flow...
    peakInspFlowValue = BoundedValue(pPeakInspFlow->getAcceptedValue()).value;
    break;
#endif // defined(SIGMA_DEVELOPMENT)
  default :
    // unexpected 'basedOnCategory' value..
    AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
    break;
  };

  BoundedValue  minInspFlow;

  // $[02019](1) -- formula for calculating minimum inspiratory flow...
  // $[02197]    -- equal to 'MAX(0.8, (Qmax * 0.1))'...
  minInspFlow.value = MAX_VALUE(0.8f,				// $[TI3]...
			        (peakInspFlowValue * 0.1f));	// $[TI4]

  // warp the calculated value to a "click" boundary...
  getBoundedRange().warpValue(minInspFlow, warpDirection, FALSE);

  return(minInspFlow);
}
