#ifndef ChangeStateMessage_HH
#define ChangeStateMessage_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  ChangeStateMessage - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/ChangeStateMessage.hhv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 

#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
#include "TaskControlMessage.hh"
#include "SigmaState.hh"
//@ End-Usage

class TaskAgent;

class ChangeStateMessage : public TaskControlMessage {
  public:

    ChangeStateMessage(  SigmaState state
                       , const TaskAgent * const pTaskAgent = NULL
                       , Uint32 sequence = 0);

    ChangeStateMessage(const ChangeStateMessage& rMessage);

    ~ChangeStateMessage();

    const char* nameOf(void) const;

    inline SigmaState           getState(void) const;
    inline const TaskAgent *    getPTaskAgent(void) const;
    inline Uint32               getSequenceNumber(void) const;
    void                        dispatch(const AppContext& context) const;

    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);

#ifdef SIGMA_UNIT_TEST
    friend class TaskAgent_UT;
#endif
 
  private:
    ChangeStateMessage();                       // declared only
    void operator=(const ChangeStateMessage&);  // declared only

    //@ Data-Member:    state_
    //  The ChangeStateMessage directs a transition to this target state.
    const enum SigmaState        state_;

    //@ Data-Member:    pTaskAgent_
    //  Pointer to the TaskAgent in the TaskAgentManager.  Used to update
    //  the state of the TaskAgent upon receiving the TaskReadyMessage.
    const TaskAgent * const   pTaskAgent_;
 
    //@ Data-Member:    sequenceNumber_
    //  The transition sequence number for this ChangeState directive.
    //  Used to discard messages from obsolete state transitions.
    const Uint32        sequenceNumber_;

};
 
#include "ChangeStateMessage.in"

#endif // ChangeStateMessage_HH
