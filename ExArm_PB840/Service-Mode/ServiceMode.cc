#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ServiceMode - SubSystem definitions
//---------------------------------------------------------------------
//@ Interface-Description
//	This class provides methods to initialize the Service Mode
//	subsystem, to determine if service and SST are required, and to
//	allow external access to certain Service Mode subsystem-level variables.
//	Since the class is used on both the BD and the GUI CPUs, preprocessor
//	directives are used to partition the code accordingly.
//---------------------------------------------------------------------
//@ Rationale
//	This class is required to set up the Service Mode subsystem, to
//	determine if service or SST is required, and to allow external
//	access to Service Mode variables.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09003]  $[09143]  $[10001]  $[10003]
//	In the initialization method, each object is constructed and placed
//	in memory.  Any object that needs initialization will execute its
//	own initialize() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	This is a static class and may not be constructed.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ServiceMode.ccv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 026   By: mnr   Date: 10-May-2010     SCR Number 6436
//  Project:  PROX
//  Description:
//      Do not set PROX SST test status to not applicable.
//
//  Revision: 025  By: mnr    Date: 24-Mar-2010     DR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX SST related updates.
//
//  Revision: 024  By: mnr    Date: 28-Dec-2009     DR Number: 6437
//  Project:  NEO
//  Description:
//      New force SST rule for Lockout option added.
//
//  Revision: 023  By: sah    Date: 22-Sep-1999     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      No longer need to store local copy of 'SoftwareOptions', now can
//      user global, static interface.  Also, now forcing SST if last
//      run with NEONATAL circuit and now option is disabled.
//
//  Revision: 022  By: dosman    Date: 26-Feb-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release.
//      Implemented changes to make unit test work for ATC.
//	Removed some SM_TEST unit test code which is no longer valid because
//      some of the data members in ServiceMode_UT are no longer there,
//	and because GSCircuitAttachedTest and ExhValveCommandTest no longer exist.
//
//  Revision: 021  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 020   By: syw   Date: 27-Aug-1998   DR Number:
//       Project:  BiLevel
//       Description:
//          BiLevel Initial Release.  Added DataKeyCopy instance.
//
//  Revision: 019  By:  syw    Date: 05-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//			BiLevel initial version.  Added GetSoftwareOptions() and
//			GetSoftwareOptions_() methods.  Set the softwareOptions
//			equal to the values received from the DataKeyAgent when
//			BdReadyCallback() is called.  Eliminate use of data key type
//			DEMO... use isSetFlashSerialNumber.
//
//  Revision: 018  By:  quf    Date: 15-Jan-1998    DR Number: DCS 2720
//       Project:  Sigma (840)
//       Description:
//			Instantiated and initialized RSecondExhPressureSensor object
//			here for use in SSt Exp Filter Test.
//
//  Revision: 017  By:  quf    Date: 21-Nov-1997    DR Number: DCS 2634
//       Project:  Sigma (840)
//       Description:
//			GUI novram serial number update now occurs within
//			ServiceMode::BdReadyCallback() (formerly done within
//			GuiApp::BdReadyCallback()).  CheckSerialNumber_() is now
//			defined for both BD and GUI.
//
//  Revision: 016  By:  quf    Date: 13-Nov-1997    DR Number: DCS 2631
//       Project:  Sigma (840)
//       Description:
//			Fixed IsServiceRequired() so that IS_EST_FAILURE is not
//			affected by use of demo data key, removed DemoDataKey_.
//
//  Revision: 015  By:  quf    Date: 05-Nov-1997    DR Number: DCS 2602
//       Project:  Sigma (840)
//       Description:
//			Fixed IsSstRequired() to force it TRUE if resistance or
//			compliance NOVRAM objects are corrupted.
//
//  Revision: 014  By:  quf    Date: 06-Oct-1997    DR Number: DCS 2451
//       Project:  Sigma (840)
//       Description:
//			Added methods CalculateIncreasedPriority(), IncreasePriority(),
//			and RestorePriority() to allow a temporary increase of the
//			SM Manager Task priority.
//
//  Revision: 013  By:  gdc    Date: 30-Sep-1997    DR Number: DCS 2513/2483
//       Project:  Sigma (840)
//       Description:
//			Provided TaskControlMessage callbacks on BD to provide
//          better communication failure detection/recovery and to
//          retrieve GUI FLASH serial number. Corrected initialization
//          of GUI SerialNumber object to use function static for
//          controlled initialization.
//
//  Revision: 012  By:  quf    Date: 26-Sep-1997    DR Number: DCS 2525
//       Project:  Sigma (840)
//       Description:
//			Changed enum name from EvCalStatus to TestStatus and added
//			FAILED status ID, fixed all references to name, fixed
//			IsServiceRequired().
//
//  Revision: 011  By:  quf    Date: 25-Sep-1997    DR Number: DCS 2410
//       Project:  Sigma (840)
//       Description:
//			Added atmospheric pressure xducer calibration.
//
//  Revision: 010  By:  gdc    Date: 23-Sep-1997    DR Number: DCS 2513
//       Project:  Sigma (840)
//       Description:
//			Added remote tests for service/manufacturing test.
//
//  Revision: 009  By:  quf    Date: 23-Sep-1997    DR Number: DCS 2385
//       Project:  Sigma (840)
//       Description:
//			Updated IsServiceRequired() and Initialize() for flow sensor
//			cal status.
//
//  Revision: 008  By:  quf    Date: 11-Sep-1997    DR Number: DCS 2289
//       Project:  Sigma (840)
//       Description:
//			Fixed IsServiceRequired() to ignore data key serial number
//			and to ignore EST status if demo data key is installed.
//
//  Revision: 007  By:  quf    Date: 01-Aug-1997    DR Number: DCS 2013
//       Project:  Sigma (840)
//       Description:
//			Added code for nurse call test to inform novram manager that
//          this is a GUI test.
//
//  Revision: 006  By:  quf    Date: 30-Jul-1997    DR Number: DCS 2205
//       Project:  Sigma (840)
//       Description:
//			Removed conditional compile of Exh Valve Velocity Transducer test.
//			Added code for EV vel xducer test to inform novram manager that
//          this is a BD test.
//
//  Revision: 005  By:  quf    Date:  16-Jul-97    DR Number: DCS 2066
//       Project:  Sigma (840)
//       Description:
//             Moved "SST required" calculation from ServiceModeAgent::isSstRequired()
//             to ServiceMode::IsSstRequired().
//
//  Revision: 004  By:  syw    Date:  03-Jul-97    DR Number: DCS 2279
//       Project:  Sigma (R8027)
//       Description:
//             Created instance of FlowSensorCalibration RFlowSensorCalibration.
//
//  Revision: 003  By:  quf    Date: 02-Jul-1997    DR Number: DCS 2107
//       Project:  Sigma (840)
//       Description:
//            Added method GetExhValveCalStatus(), which returns an enum
//            having one of three values: NEVER_RUN, IN_PROGRESS, or PASSED.
//            This will be used this to keep the EST button flat only for the
//            NEVER_RUN case.  IsExhValveCalRequired() and IsServiceRequired()
//            were modified to use the new three-state calibration-in-progress
//            novram flag.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "ForceVentInopTest.hh"
#include "SmManager.hh"
#include "SerialNumberInitialize.hh"
#include "SmTestId.hh"
#include "NovRamManager.hh"
#include "CalInfoRefs.hh"
#include "CalInfoFlashBlock.hh"
#include "CalInfoDuplication.hh"
#include "ChangeStateMessage.hh"
#include "TaskControlAgent.hh"

#if defined (SIGMA_DEVELOPMENT)
# include <stdio.h>
#endif // defined (SIGMA_DEVELOPMENT)

#if defined(SM_TEST)
# include "TestDriver.hh"
#endif  // defined(SM_TEST)

#if defined(SIGMA_BD_CPU)

# include "PatientCctTypeValue.hh"
# include "NetworkApp.hh"
# include "NetworkMsgId.hh"
# include "Task.hh"

# include "SmTasks.hh"
# include "SmFlowController.hh"
# include "SmPressureController.hh"
# include "SmExhValveController.hh"
# include "TestData.hh"
# include "ExhValveCalibration.hh"
# include "ComplianceCalibration.hh"
# include "FlowSensor.hh"
# include "SmSensorMediator.hh"
# include "SmUtilities.hh"
# include "InspResistanceTest.hh"
# include "SstFilterTest.hh"
# include "GasSupplyTest.hh"
# include "CircuitPressureTest.hh"
# include "ExhValveSealTest.hh"
# include "ExhValvePerformanceTest.hh"
# include "SafetySystemTest.hh"
# include "GeneralElectronicsTest.hh"
# include "ExhHeaterTest.hh"
# include "LeakTest.hh"
# include "FsCrossCheckTest.hh"
# include "BdAudioTest.hh"
# include "BdLampTest.hh"
# include "FlowSensorCalibration.hh"
# include "SmGasSupply.hh"
# include "BatteryTest.hh"
# include "ServiceModeNovramData.hh"
# include "AtmPresXducerCalibration.hh"
# include "ProxTest.hh"

# include "PhasedInContextHandle.hh"
# include "PendingContextHandle.hh"
# include "Post.hh"
# include "NominalLineVoltValue.hh"
# include "BdSettingValues.hh"

# include "BD_IO_Devices.hh"
# include "BDIOFlashMap.hh"
# include "RegisterRefs.hh"
# include "FlowSensorCalInfo.hh"
# include "MainSensorRefs.hh"
# include "ValveRefs.hh"
# include "ExhalationValve.hh"
# include "Fio2CalInfo.hh"
# include "Fio2SensorCalInfo.hh"
# include "SerialNumber.hh"
# include "DataKey.hh"
# include "VentObjectRefs.hh"
# include "Configuration.hh"
# include "BDIORefs.hh"

# include "BreathMiscRefs.hh"
# include "CircuitCompliance.hh"

# include "ResultEntryData.hh"
# include "TestResultId.hh"

// E600 BDIO # include "SmCompressorEeprom.hh"
# include "SmDatakey.hh"
# include "SmFlowTest.hh"
# include "CommDownMessage.hh"
# include "GuiReadyMessage.hh"

#ifdef SIGMA_UNIT_TEST
# include "ServiceMode_UT.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

// Global constant initialization

//@ Constant: EV_COUNTS_TO_MA
// constant to convert exh valve DAC counts to current in mA
const Real32 EV_COUNTS_TO_MA = 0.5125;

// Static data member initialization
Boolean ServiceMode::FlowSensorInfoValid_ = FALSE ;
Boolean ServiceMode::ExhValveCalValid_ = FALSE;
Boolean ServiceMode::VentInopTestInProgress_ = FALSE ;
Int32 ServiceMode::IncreasedPriority_ = 0;

#endif  // defined (SIGMA_BD_CPU)

# include "SoftwareOptions.hh"

#if defined (SIGMA_GUI_CPU)

# include "GuiIoTest.hh"
# include "DataKeyAgent.hh"
# include "ConfigurationAgent.hh"
# include "BdReadyMessage.hh"
# include "Configuration.hh"
# include "BDIORefs.hh"

//@ End-Usage

//@ Code...

// Static data member initialization
Boolean ServiceMode::DataKeyInstalled_ = FALSE ;

CalInfoFlashSignature::Signature ServiceMode::ExhValveSignature_ = (CalInfoFlashSignature::Signature)0 ;
CrcValue ServiceMode::ExhValveCrc_ = 0 ;

Uint32 ServiceMode::AirFlowSensorSN_ = 0 ;
CrcValue ServiceMode::AirFlowSensorCrc_ = 0 ;

Uint32 ServiceMode::O2FlowSensorSN_ = 0 ;
CrcValue ServiceMode::O2FlowSensorCrc_ = 0 ;

Uint32 ServiceMode::ExhFlowSensorSN_ = 0 ;
CrcValue ServiceMode::ExhFlowSensorCrc_ = 0 ;

Uint32 ServiceMode::ExhO2FlowSensorSN_ = 0 ;
CrcValue ServiceMode::ExhO2FlowSensorCrc_ = 0 ;

#ifdef SIGMA_UNIT_TEST
# include "ServiceMode_UT.hh"
#endif  // SIGMA_UNIT_TEST

#endif  // defined (SIGMA_GUI_CPU)

Boolean ServiceMode::ServiceComplete_ = FALSE;

//====================================================================
//
//   Assign the external references to the pre allocated memory location.
//
//====================================================================

//--------------------------------------------------------------------
// Service Mode Manager
Uint
	PSmManager[(sizeof(SmManager) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
SmManager& RSmManager =
	*((SmManager*) PSmManager) ;

#if defined(SM_TEST)
//--------------------------------------------------------------------
// Test Driver
Uint
	PTestDriver[(sizeof(TestDriver) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
TestDriver& RTestDriver =
	*((TestDriver*) PTestDriver) ;
#endif //  defined(SM_TEST)

#if defined(SIGMA_BD_CPU)

//--------------------------------------------------------------------
// service mode flow controller
Uint
	PSmFlowController[(sizeof(SmFlowController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
SmFlowController& RSmFlowController =
	*((SmFlowController*) PSmFlowController) ;

//--------------------------------------------------------------------
// service mode pressure controller
Uint
	PSmPressureController[(sizeof(SmPressureController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
SmPressureController& RSmPressureController =
	*((SmPressureController*) PSmPressureController) ;

//--------------------------------------------------------------------
// service mode exhalation valve controller
Uint
	PSmExhValveController[(sizeof(SmExhValveController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
SmExhValveController& RSmExhValveController =
	*((SmExhValveController*) PSmExhValveController) ;

//--------------------------------------------------------------------
// service mode test data
Uint
	PTestData[(sizeof(TestData) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
TestData& RTestData =
	*((TestData*) PTestData) ;

//--------------------------------------------------------------------
// Exhalation Valve calibration
Uint
	PExhValveCalibration[(sizeof(ExhValveCalibration) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
ExhValveCalibration& RExhValveCalibration =
	*((ExhValveCalibration*) PExhValveCalibration) ;

//--------------------------------------------------------------------
// Compliance calibration
Uint
	PComplianceCalibration[(sizeof(ComplianceCalibration) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
ComplianceCalibration& RComplianceCalibration =
	*((ComplianceCalibration*) PComplianceCalibration) ;

//--------------------------------------------------------------------
// Service Mode Sensor Mediator
Uint
	PSmSensorMediator[(sizeof(SmSensorMediator) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
SmSensorMediator& RSmSensorMediator =
	*((SmSensorMediator*) PSmSensorMediator) ;

//--------------------------------------------------------------------
// Service Mode Utilities
Uint
	PSmUtilities[(sizeof(SmUtilities) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
SmUtilities& RSmUtilities =
	*((SmUtilities*) PSmUtilities) ;

//--------------------------------------------------------------------
// Insp Resistance Test
Uint
	PInspResistanceTest[(sizeof(InspResistanceTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
InspResistanceTest& RInspResistanceTest =
	*((InspResistanceTest*) PInspResistanceTest) ;

//--------------------------------------------------------------------
// SST Filter Test
Uint
	PSstFilterTest[(sizeof(SstFilterTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
SstFilterTest& RSstFilterTest =
	*((SstFilterTest*) PSstFilterTest) ;

//--------------------------------------------------------------------
// Gas Supply Test
Uint
	PGasSupplyTest[(sizeof(GasSupplyTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
GasSupplyTest& RGasSupplyTest =
	*((GasSupplyTest*) PGasSupplyTest) ;

//--------------------------------------------------------------------
// Circuit Pressure Test
Uint
	PCircuitPressureTest[(sizeof(CircuitPressureTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
CircuitPressureTest& RCircuitPressureTest =
	*((CircuitPressureTest*) PCircuitPressureTest) ;

//--------------------------------------------------------------------
// Exhalation Valve Seal Test
Uint
	PExhValveSealTest[(sizeof(ExhValveSealTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
ExhValveSealTest& RExhValveSealTest =
	*((ExhValveSealTest*) PExhValveSealTest) ;

//--------------------------------------------------------------------
// Exhalation Valve Performance Test
Uint
	PExhValvePerformanceTest[(sizeof(ExhValvePerformanceTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
ExhValvePerformanceTest& RExhValvePerformanceTest =
	*((ExhValvePerformanceTest*) PExhValvePerformanceTest) ;

//--------------------------------------------------------------------
// Safety System Test
Uint
	PSafetySystemTest[(sizeof(SafetySystemTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
SafetySystemTest& RSafetySystemTest =
	*((SafetySystemTest*) PSafetySystemTest) ;

//--------------------------------------------------------------------
// General Electronics Test
Uint
	PGeneralElectronicsTest[(sizeof(GeneralElectronicsTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
GeneralElectronicsTest& RGeneralElectronicsTest =
	*((GeneralElectronicsTest*) PGeneralElectronicsTest) ;

//--------------------------------------------------------------------
// Exhalation Heater Test
Uint
	PExhHeaterTest[(sizeof(ExhHeaterTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
ExhHeaterTest& RExhHeaterTest =
	*((ExhHeaterTest*) PExhHeaterTest) ;

//--------------------------------------------------------------------
// Service Mode and Short Self Test Leak Test
Uint
	PLeakTest[(sizeof(LeakTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
LeakTest& RLeakTest =
	*((LeakTest*) PLeakTest) ;

//--------------------------------------------------------------------
// Flow Sensor Cross Check Test
// to that block of memory
Uint
	PFsCrossCheckTest[(sizeof(FsCrossCheckTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
FsCrossCheckTest& RFsCrossCheckTest =
	*((FsCrossCheckTest*) PFsCrossCheckTest) ;

//--------------------------------------------------------------------
// BD Lamp Test
Uint
	PBdLampTest[(sizeof(BdLampTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
BdLampTest& RBdLampTest =
	*((BdLampTest*) PBdLampTest) ;

//--------------------------------------------------------------------
// BD Audio Test
Uint
	PBdAudioTest[(sizeof(BdAudioTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
BdAudioTest& RBdAudioTest =
	*((BdAudioTest*) PBdAudioTest) ;

//--------------------------------------------------------------------
// Flow Sensor Calibration
Uint
	PFlowSensorCalibration[(sizeof(FlowSensorCalibration) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
FlowSensorCalibration& RFlowSensorCalibration =
	*((FlowSensorCalibration*) PFlowSensorCalibration) ;

//--------------------------------------------------------------------
// Gas Supply
Uint
	PSmGasSupply[(sizeof(SmGasSupply) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
SmGasSupply& RSmGasSupply =
	*((SmGasSupply*) PSmGasSupply) ;

//--------------------------------------------------------------------
// Battery test
Uint
	PBatteryTest[(sizeof(BatteryTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
BatteryTest& RBatteryTest =
	*((BatteryTest*) PBatteryTest) ;

//--------------------------------------------------------------------
// Prox test
Uint
	PProxTest[(sizeof(ProxTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
ProxTest& RProxTest =
	*((ProxTest*) PProxTest) ;

//--------------------------------------------------------------------
// Atmospheric Pressure Transducer Calibration
Uint
	PAtmPresXducerCalibration[(sizeof(AtmPresXducerCalibration) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
AtmPresXducerCalibration& RAtmPresXducerCalibration =
	*((AtmPresXducerCalibration*) PAtmPresXducerCalibration) ;
//--------------------------------------------------------------------
// exhalation pressure sensor (secondary adc channel)
Uint
    PSecondExhPressureSensor[(sizeof(PressureSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    PressureSensor& RSecondExhPressureSensor =
    *((PressureSensor*) PSecondExhPressureSensor) ;
//--------------------------------------------------------------------

#endif  // defined(SIGMA_BD_CPU)


#ifdef SIGMA_GUI_CPU

//--------------------------------------------------------------------
// GuiIoTest
Uint
        PGuiIoTest[(sizeof(GuiIoTest) + sizeof(Uint) - 1)
        / sizeof(Uint)];
GuiIoTest& RGuiIoTest =
        *((GuiIoTest*) PGuiIoTest) ;

//--------------------------------------------------------------------

#endif //SIGMA_GUI_CPU

//--------------------------------------------------------------------
// Serial Number Initialize
Uint
	PSerialNumberInitialize[(sizeof(SerialNumberInitialize) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
SerialNumberInitialize& RSerialNumberInitialize =
	*((SerialNumberInitialize*) PSerialNumberInitialize) ;

//--------------------------------------------------------------------
// Cal Info Duplication
Uint
	PCalInfoDuplication[(sizeof(CalInfoDuplication) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
CalInfoDuplication& RCalInfoDuplication =
	*((CalInfoDuplication*) PCalInfoDuplication) ;

//--------------------------------------------------------------------
// Force Vent Inop Test
Uint
	PForceVentInopTest[(sizeof(ForceVentInopTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
ForceVentInopTest& RForceVentInopTest =
	*((ForceVentInopTest*) PForceVentInopTest) ;

//--------------------------------------------------------------------

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
//	This static method takes no argument and returns no value.
//	It constructs all service mode sub-system objects and initializes
//	Service Mode data.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The first part of the method uses the placement new operator to
//	control the construction of the service mode objects.  The second
//	part of the method initializes service mode data.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	For every placement new operator, the address of the object created
//	is checked to have the proper address.
//@ End-Method
//=====================================================================
void
ServiceMode::Initialize(void)
{
// $[TI1]
	CALL_TRACE("ServiceMode::Initialize(void)") ;

// ***** Initialize Service Mode objects *****

#if defined(SIGMA_BD_CPU)
	//-----------------------------------------------------------------
	// service mode flow controller
	SmFlowController *pSmFlowController =
    	new (&RSmFlowController) SmFlowController() ;

	FREE_POST_CONDITION( pSmFlowController ==
    	&RSmFlowController, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// service mode pressure controller
	SmPressureController *pSmPressureController =
    	new (&RSmPressureController) SmPressureController() ;

	FREE_POST_CONDITION( pSmPressureController ==
    	&RSmPressureController, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// service mode exhalation valve controller
	SmExhValveController *pSmExhValveController =
    	new (&RSmExhValveController) SmExhValveController() ;

	FREE_POST_CONDITION( pSmExhValveController ==
    	&RSmExhValveController, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// service mode test data
	TestData *pTestData =
    	new (&RTestData) TestData() ;

	FREE_POST_CONDITION( pTestData ==
    	&RTestData, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Exhalation Valve calibration
   	ExhValveCalibration *pExhValveCalibration =
    	new (&RExhValveCalibration) ExhValveCalibration() ;

	FREE_POST_CONDITION( pExhValveCalibration ==
    	&RExhValveCalibration, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Compliance calibration
   	ComplianceCalibration *pComplianceCalibration =
    	new (&RComplianceCalibration) ComplianceCalibration() ;

	FREE_POST_CONDITION( pComplianceCalibration ==
    	&RComplianceCalibration, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Service Mode Sensor Mediator
   	SmSensorMediator *pSmSensorMediator =
    	new (&RSmSensorMediator) SmSensorMediator() ;

	FREE_POST_CONDITION( pSmSensorMediator ==
    	&RSmSensorMediator, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Service Mode Utilities
   	SmUtilities *pSmUtilities =
    	new (&RSmUtilities) SmUtilities() ;

	FREE_POST_CONDITION( pSmUtilities ==
    	&RSmUtilities, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Insp Resistance Test
   	InspResistanceTest *pInspResistanceTest =
    	new (&RInspResistanceTest) InspResistanceTest() ;

	FREE_POST_CONDITION( pInspResistanceTest ==
    	&RInspResistanceTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// SST Filter Test
   	SstFilterTest *pSstFilterTest =
    	new (&RSstFilterTest) SstFilterTest() ;

	FREE_POST_CONDITION( pSstFilterTest ==
    	&RSstFilterTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Gas Supply Test
   	GasSupplyTest *pGasSupplyTest =
    	new (&RGasSupplyTest) GasSupplyTest() ;

	FREE_POST_CONDITION( pGasSupplyTest ==
    	&RGasSupplyTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Circuit Pressure Test
   	CircuitPressureTest *pCircuitPressureTest =
    	new (&RCircuitPressureTest) CircuitPressureTest() ;

	FREE_POST_CONDITION( pCircuitPressureTest ==
    	&RCircuitPressureTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Exhalation Valve Seal Test
   	ExhValveSealTest *pExhValveSealTest =
    	new (&RExhValveSealTest) ExhValveSealTest() ;

	FREE_POST_CONDITION( pExhValveSealTest ==
    	&RExhValveSealTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Exhalation Valve Performance Test
	ExhValvePerformanceTest *pExhValvePerformanceTest =
    	new (&RExhValvePerformanceTest) ExhValvePerformanceTest() ;

	FREE_POST_CONDITION( pExhValvePerformanceTest ==
    	&RExhValvePerformanceTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Safety System Test
	SafetySystemTest *pSafetySystemTest =
    	new (&RSafetySystemTest) SafetySystemTest() ;

	FREE_POST_CONDITION( pSafetySystemTest ==
    	&RSafetySystemTest, SERVICE_MODE, SERVICEMODE) ;

	// TODO E600 MS put it back in when ported
	//-----------------------------------------------------------------
	// General Electronics Test
	/*
	GeneralElectronicsTest *pGeneralElectronicsTest =
    	new (&RGeneralElectronicsTest) GeneralElectronicsTest() ;

	FREE_POST_CONDITION( pGeneralElectronicsTest ==
    	&RGeneralElectronicsTest, SERVICE_MODE, SERVICEMODE) ;
		*/
	//-----------------------------------------------------------------
	// Exhalation Heater Test
	ExhHeaterTest *pExhHeaterTest =
    	new (&RExhHeaterTest) ExhHeaterTest() ;

	FREE_POST_CONDITION( pExhHeaterTest ==
    	&RExhHeaterTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Service Mode Leak Test
	LeakTest *pLeakTest =
    	new (&RLeakTest) LeakTest() ;

	FREE_POST_CONDITION( pLeakTest ==
    	&RLeakTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Flow Sensor Cross Check test
	FsCrossCheckTest *pFsCrossCheckTest =
    	new (&RFsCrossCheckTest) FsCrossCheckTest() ;

	FREE_POST_CONDITION( pFsCrossCheckTest ==
    	&RFsCrossCheckTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// BD Lamp Test
	BdLampTest *pBdLampTest =
    	new (&RBdLampTest) BdLampTest() ;

	FREE_POST_CONDITION( pBdLampTest ==
    	&RBdLampTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// BD Audio Test
	BdAudioTest *pBdAudioTest =
    	new (&RBdAudioTest) BdAudioTest() ;

	FREE_POST_CONDITION( pBdAudioTest ==
    	&RBdAudioTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Flow Sensor Calibration
	FlowSensorCalibration *pFlowSensorCalibration =
    	new (&RFlowSensorCalibration) FlowSensorCalibration() ;

	FREE_POST_CONDITION( pFlowSensorCalibration ==
    	&RFlowSensorCalibration, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Service Mode Gas Supply
	SmGasSupply *pSmGasSupply =
    	new (&RSmGasSupply) SmGasSupply() ;

	FREE_POST_CONDITION( pSmGasSupply ==
    	&RSmGasSupply, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Battery test
	BatteryTest *pBatteryTest =
    	new (&RBatteryTest) BatteryTest() ;

	FREE_POST_CONDITION( pBatteryTest ==
    	&RBatteryTest, SERVICE_MODE, SERVICEMODE) ;

	//-----------------------------------------------------------------
	// Prox test
	ProxTest *pProxTest =
    	new (&RProxTest) ProxTest() ;

	FREE_POST_CONDITION( pProxTest == &RProxTest, SERVICE_MODE, SERVICEMODE) ;

	//-----------------------------------------------------------------
	// Atmospheric Pressure Transducer Calibration
	AtmPresXducerCalibration *pAtmPresXducerCalibration =
    	new (&RAtmPresXducerCalibration) AtmPresXducerCalibration() ;

	FREE_POST_CONDITION( pAtmPresXducerCalibration ==
    	&RAtmPresXducerCalibration, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
    // Construct the Exhalation pressure sensor (secondary adc channel)
// E600 BDIO
#if 0
	PressureSensor *pSecondExhPressureSensor = new (&RSecondExhPressureSensor)
       PressureSensor(NULL, AdcChannels::EXH_PRESSURE_FILTERED, SERVICE_MODE_ALPHA) ;

    FREE_POST_CONDITION(pSecondExhPressureSensor ==
                 &RSecondExhPressureSensor, SERVICE_MODE, SERVICEMODE) ;
#endif
    //-----------------------------------------------------------------

    // Compressor Eeprom Service Functions
// E600 BDIO    SmCompressorEeprom::GetSmCompressorEeprom();

    //-----------------------------------------------------------------
    // Datakey Service Functions
    SmDatakey::GetSmDatakey();

    //-----------------------------------------------------------------
    // Remote Flow Test
    SmFlowTest::GetAirFlowTest();
    SmFlowTest::GetOxygenFlowTest();

#endif  // defined(SIGMA_BD_CPU)

	//-----------------------------------------------------------------
	// Service Mode Manager
	SmManager *pSmManager =
    	new (&RSmManager) SmManager() ;

	FREE_POST_CONDITION( pSmManager ==
    	&RSmManager, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Force Vent Inop Test
	ForceVentInopTest *pForceVentInopTest =
    	new (&RForceVentInopTest) ForceVentInopTest() ;

	FREE_POST_CONDITION( pForceVentInopTest ==
    	&RForceVentInopTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------

#if defined(SM_TEST)
	//-----------------------------------------------------------------
	// Test Driver
	TestDriver *pTestDriver =
    	new (&RTestDriver) TestDriver() ;

	FREE_POST_CONDITION( pTestDriver ==
    	&RTestDriver, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
#endif  // defined(SM_TEST)

#ifdef SIGMA_GUI_CPU
	//-----------------------------------------------------------------
	// GuiIoTest
	GuiIoTest *pGuiIoTest =
    	new (&RGuiIoTest) GuiIoTest() ;

	FREE_POST_CONDITION( pGuiIoTest ==
    	&RGuiIoTest, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
#endif //SIGMA_GUI_CPU

	//-----------------------------------------------------------------
	// Serial Number Initialize
	SerialNumberInitialize *pSerialNumberInitialize =
    	new (&RSerialNumberInitialize) SerialNumberInitialize() ;

	FREE_POST_CONDITION( pSerialNumberInitialize ==
    	&RSerialNumberInitialize, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------
	// Cal Info Duplication
	CalInfoDuplication *pCalInfoDuplication =
    	new (&RCalInfoDuplication) CalInfoDuplication() ;

	FREE_POST_CONDITION( pCalInfoDuplication ==
    	&RCalInfoDuplication, SERVICE_MODE, SERVICEMODE) ;
	//-----------------------------------------------------------------

// ***** Initialize Service Mode data *****
    GetGuiSerialNumber_();    // instantiate function static

	SmTestId::Initialize();
	CalInfoDuplication::Initialize();

#if defined(SIGMA_BD_CPU)
	// initialize nominal ac voltage
	DiscreteValue nominalVolt =
		PhasedInContextHandle::GetDiscreteValue(SettingId::NOMINAL_VOLT);

	// store the nominal ac voltage for use by the compressor
	// controller
	RSmGasSupply.setNominalVoltage(nominalVolt);

	// Update POST with default ac voltage in case this is 1st power-up
	Post::SetAcLineVoltage(
			(NominalLineVoltValue::NominalLineVoltValueId) nominalVolt);

	// Initialize the flow sensor and exh valve cal info valid flags
	ServiceMode::FlowSensorInfoValid_ = RCalInfoFlashBlock.getFlowSensorInfoValid();
	ServiceMode::ExhValveCalValid_ = RCalInfoFlashBlock.getExhValveCalValid();

	// Register to receive the network transmitted packets.
	NetworkApp::RegisterRecvMsg(SERVICE_MODE_MSG,
								  SmManager::RecvMsg);
	NetworkApp::RegisterRecvMsg(SERVICE_MODE_ACTION,
								 SmManager::RecvAction);

	// register callbacks for ac line voltage setting changes
	PendingContextHandle::RegisterSettingEvent(SettingId::NOMINAL_VOLT,
								ServiceMode::SettingEventHappened);

	// Tell NovRam which tests are not part of the
	// EST for the BD board.
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_KEYBOARD_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_KNOB_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_LAMP_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_AUDIO_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_TOUCH_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_SERIAL_PORT_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_NURSE_CALL_ID);

	RForceVentInopTest.initialize();
	// NOTE: initialize FlowSensorCalibration AFTER setting FlowSensorInfoValid_!
	RFlowSensorCalibration.initialize();

	// check the BD serial number and update novram if required
	CheckSerialNumber_();

	// initialize the secondary-channel exh pressure sensor filter
    RSecondExhPressureSensor.initValues(
								RSecondExhPressureSensor.getValue(), 0.0, 0.0,
								RSecondExhPressureSensor.getValue());

#endif  // defined(SIGMA_BD_CPU)

#if defined(SIGMA_GUI_CPU)

	// Tell NovRam which tests are not part of the
	// EST for the GUI board.
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GAS_SUPPLY_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_BD_LAMP_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_BD_AUDIO_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_FS_CROSS_CHECK_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_SAFETY_SYSTEM_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_EXH_VALVE_SEAL_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_EXH_VALVE_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_SM_LEAK_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_EXH_HEATER_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GENERAL_ELECTRONICS_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_BATTERY_ID);
	
	// Tell NovRam which tests are not part of the
	// SST for the GUI board.
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_EXPIRATORY_FILTER_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_CIRCUIT_LEAK_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_FLOW_SENSORS_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID);

#endif  // if defined(SIGMA_GUI_CPU)

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsServiceRequired()
//
//@ Interface-Description
//	This static method calculates if service is required.  No arguments,
//	returns TRUE if service is required, FALSE if not.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Determine if service is required based on the following conditions:
//	1. the existing overall EST result,
//	2. if flow sensor data in FLASH is valid,
//	3. if exh valve calibration data in FLASH is valid
//	4. if exh valve calibration is in progress,
//	5. if vent inop test result is a FAILURE,
//	6. if vent inop test is in progress,
//	7. if flow sensor calibration did not pass, and
//	8. if serial number has not been initialized for non-demo data key
//
//	On the BD, all of these conditions are evaluated to determine the
//	service required state, but on the GUI, only the first condition is
//	evaluated.  Return TRUE if service is required, FALSE if not.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
ServiceMode::IsServiceRequired( void)
{
	// TODO E600 MS. This is a hack. Remove when Calibration and EST are done.
	//return FALSE;

// TODO E600 MS Beginning of commented out code. Remove when EST and CALS are 
// complete
//#if 0
// $[TI1]
	CALL_TRACE("ServiceMode::IsServiceRequired( void)") ;

    ResultEntryData estOverallResult;

    NovRamManager::GetOverallEstResult( estOverallResult);
#if defined(SIGMA_UNIT_TEST) && defined(SIGMA_GUI_CPU)
	estOverallResult = ServiceMode_UT::EstOverallResult;
#endif  // defined(SIGMA_UNIT_TEST) && defined(SIGMA_GUI_CPU)

	const Boolean  IS_EST_FAILURE = 
			estOverallResult.getResultId() == MAJOR_TEST_FAILURE_ID ||
			estOverallResult.getResultId() == MINOR_TEST_FAILURE_ID ||
			estOverallResult.getResultId() == UNDEFINED_TEST_RESULT_ID ||
			estOverallResult.getResultId() == INCOMPLETE_TEST_RESULT_ID ;

#if defined(SIGMA_BD_CPU)
	const Boolean  IS_INVALID_FLOW_SENSOR =
							!RCalInfoFlashBlock.getFlowSensorInfoValid();

	const Boolean  IS_INVALID_EXH_CAL =
							!RCalInfoFlashBlock.getExhValveCalValid();

	const Boolean  IS_EXH_CAL_IN_PROGRESS =
			(ServiceModeNovramData::GetExhValveCalStatus() != ServiceMode::PASSED);

	const Boolean  IS_FS_CAL_FAILED =
			(ServiceModeNovramData::GetFlowSensorCalStatus() !=
												ServiceMode::PASSED);

	const Boolean  IS_VENT_INOP_MAJOR_FAILURE =
								  !ServiceModeNovramData::IsVentInopTestPassed();

	const Boolean  IS_VENT_INOP_TEST_IN_PROGRESS =
								ServiceMode::IsVentInopTestInProgress();
#endif  // defined(SIGMA_BD_CPU)


#if defined(SIGMA_DEVELOPMENT) && !defined(SIGMA_UNIT_TEST)
    if (IS_EST_FAILURE)
	{
	  cout << "SERVICE REQUIRED:  EST Overall Result = "
	  	   << estOverallResult.getResultId()  << endl;
	}

# if defined(SIGMA_BD_CPU)
    if (IS_INVALID_FLOW_SENSOR)
	{
	  cout << "SERVICE REQUIRED:  Invalid Flow Sensor Info!" << endl;
	}

    if (IS_INVALID_EXH_CAL)
	{
	  cout << "SERVICE REQUIRED:  Invalid Exh. Calibration Info!" << endl;
	}

    if (IS_EXH_CAL_IN_PROGRESS)
	{
	  cout << "SERVICE REQUIRED:  Invalid Exh. Calibration in progress!"
	  	   << endl;
	}

    if (IS_FS_CAL_FAILED)
	{
	  cout << "SERVICE REQUIRED:  Invalid Flow Sensor Calibration status!"
	  	   << endl;
	}

    if (IS_VENT_INOP_MAJOR_FAILURE)
	{
	  cout << "SERVICE REQUIRED:  Vent In-Op Test Major Failure!" << endl;
	}

    if (IS_VENT_INOP_TEST_IN_PROGRESS)
	{
	  cout << "SERVICE REQUIRED:  Vent In-Op Test In Progress!" << endl;
	}
# endif  // defined(SIGMA_BD_CPU)

#endif // defined(SIGMA_DEVELOPMENT) && !defined(SIGMA_UNIT_TEST)

#if defined(SIGMA_BD_CPU)
	return( IS_EST_FAILURE  			||
			IS_INVALID_FLOW_SENSOR		||
			IS_INVALID_EXH_CAL			||
			IS_EXH_CAL_IN_PROGRESS		||
			IS_FS_CAL_FAILED			||
# if defined(SIGMA_PRODUCTION)
			( BD_IO_Devices::GetSerialNumInitialSignature() &&
				SoftwareOptions::GetSoftwareOptions().isSetFlashSerialNumber()) ||
# endif // defined(SIGMA_PRODUCTION)
			IS_VENT_INOP_MAJOR_FAILURE	||
			IS_VENT_INOP_TEST_IN_PROGRESS );

#elif defined(SIGMA_GUI_CPU)
	return( IS_EST_FAILURE);

#endif  // defined(SIGMA_BD_CPU)

//#endif // TODO E600 MS end of commented out code.Remove when EST and CALS are complete
}


#if defined(SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsSstRequired()
//
//@ Interface-Description
//	This static BD-only method calculates if SST is required.  No arguments,
//	returns TRUE if SST is required, FALSE if not.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Determine if SST is required based on the existing overall SST result,
//	on a patient circuit change, and on the validity of the resistance
//	and compliance parameters stored in NOVRAM.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
ServiceMode::IsSstRequired( void)
{
	CALL_TRACE("ServiceMode::IsSstRequired( void)");

    ResultEntryData sstOverallResult;

    NovRamManager::GetOverallSstResult( sstOverallResult);

#ifdef SIGMA_UNIT_TEST
	sstOverallResult = ServiceMode_UT::BdSstOverallResult;
#endif  // SIGMA_UNIT_TEST

	const Boolean  IS_SST_FAILURE =
		sstOverallResult.getResultId() == MAJOR_TEST_FAILURE_ID ||
		sstOverallResult.getResultId() == MINOR_TEST_FAILURE_ID ||
		sstOverallResult.getResultId() == UNDEFINED_TEST_RESULT_ID ||
		sstOverallResult.getResultId() == INCOMPLETE_TEST_RESULT_ID;

	Boolean circuitChanged;
	if (NovRamManager::AreBatchSettingsInitialized())
	{
	// $[TI1]

		BdSettingValues acceptedSettingValues;
		NovRamManager::GetAcceptedBatchSettings(
											acceptedSettingValues);

		const DiscreteValue CURR_PATIENT_CCT_TYPE =
			acceptedSettingValues.getDiscreteValue(
										SettingId::PATIENT_CCT_TYPE);

		const DiscreteValue LAST_PATIENT_CCT_TYPE =
			NovRamManager::GetLastSstPatientCctType();

		// force SST if last circuit type was NEONATAL circuit, and the NeoMode
		// option is now disabled...
		circuitChanged = (   CURR_PATIENT_CCT_TYPE != LAST_PATIENT_CCT_TYPE
						|| ( LAST_PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT  &&
						     !SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE) )
						 //[02282]: For Lockout, only NEONATAL can be allowed for cct type
						|| ( LAST_PATIENT_CCT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT  &&
							 SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_LOCKOUT) )
						  );
	}
	else
	{
	// $[TI2]

		// If batch settings aren't available, force circuitChanged TRUE
		// to force an SST required state
		circuitChanged = TRUE;
	}
	// end else

	Resistance inspResistance;
	NovRamManager::GetInspResistance(inspResistance);
	Resistance exhResistance;
	NovRamManager::GetExhResistance(exhResistance);

	const Boolean INVALID_CIRCUIT_PARAMS =
							!inspResistance.isResistanceValid() ||
							!exhResistance.isResistanceValid() ||
							RCircuitCompliance.areValuesDefault();

	const Boolean IS_SST_REQUIRED = IS_SST_FAILURE || circuitChanged ||
									INVALID_CIRCUIT_PARAMS;

	return( IS_SST_REQUIRED);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SettingEventHappened()
//
//@ Interface-Description
//	This static BD-only method takes a SettingIdType and has no return value.
//	It is used to process a setting change to the nominal line voltage --
//	all other setting changes are ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the setting change is for nominal line voltage, get the new
//	line voltage value, update the SmGasSupply object, and update POST.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ServiceMode::SettingEventHappened (SettingId::SettingIdType id)
{

	if(id == SettingId::NOMINAL_VOLT)
	{
	// $[TI1]
		const DiscreteValue nominalLineVoltage =
			PendingContextHandle::GetDiscreteValue(SettingId::NOMINAL_VOLT);

		// Update SmGasSupply for compressor control
		RSmGasSupply.setNominalVoltage(nominalLineVoltage);

		// Update POST
		Post::SetAcLineVoltage(
			(NominalLineVoltValue::NominalLineVoltValueId) nominalLineVoltage);

#if defined(SIGMA_DEBUG)
		printf("NOMINAL_VOLT setting event happened\n");
#endif  // defined(SIGMA_DEBUG)

	}
	// $[TI2]
	// implied else: id is not NOMINAL_VOLT, so ignore it

#if defined(SIGMA_DEBUG)
	else
		printf("non-NOMINAL_VOLT setting event happened\n");
#endif  // defined(SIGMA_DEBUG)

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsExhValveCalRequired()
//
//@ Interface-Description
//	This static BD-only method has no argument and returns TRUE
//	if exhalation valve calibration is required, FALSE if not.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Determine if exh valve calib is required based on the ExhValveCalValid_
//	flag and the calib-in-progress novram flag.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
ServiceMode::IsExhValveCalRequired( void)
{
// $[TI1]
	CALL_TRACE("ServiceMode::IsExhValveCalRequired( void)") ;

	return( ExhValveCalValid_ == FALSE ||
			ServiceModeNovramData::GetExhValveCalStatus() != PASSED);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetExhValveCalStatus()
//
//@ Interface-Description
//	This static BD-only method has no argument and returns an enum
//	indicating exh valve calibration state.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Determine exh valve calib state based on the ExhValveCalValid_
//	flag and the calib-in-progress novram flag.
//---------------------------------------------------------------------
//@ PreCondition
//	EV cal status flag in novram must be one of the legal states.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceMode::TestStatus
ServiceMode::GetExhValveCalStatus( void)
{
	CALL_TRACE("ServiceMode::GetExhValveCalStatus( void)") ;

	TestStatus overallEvCalStatus;
	TestStatus novramEvCalStatus =
				ServiceModeNovramData::GetExhValveCalStatus();

	CLASS_PRE_CONDITION( novramEvCalStatus == NEVER_RUN ||
						 novramEvCalStatus == IN_PROGRESS ||
						 novramEvCalStatus == PASSED );

	if (novramEvCalStatus == NEVER_RUN && !ExhValveCalValid_)
	{
	// $[TI1]
		overallEvCalStatus = NEVER_RUN;
	}
	else if (novramEvCalStatus == PASSED && ExhValveCalValid_)
	{
	// $[TI2]
		overallEvCalStatus = PASSED;
	}
	else
	{
	// $[TI3]
		overallEvCalStatus = IN_PROGRESS;
	}
	// end else

	return( overallEvCalStatus);
}

#endif  // defined(SIGMA_BD_CPU)



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeStateCallback()
//
//@ Interface-Description
//	This static method takes a const reference ChangeStateMessage an
//	as an input and returns nothing.  It is invoked when the ChangeStateMessage
//	is received by the task that registered for it.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The current state is reported back to Sys-Init.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceMode::ChangeStateCallback(const ChangeStateMessage &rMessage)
{
// $[TI1]
	CALL_TRACE("ServiceMode::ChangeStateCallback(const ChangeStateMessage &rMessage)") ;

    TaskControlAgent::ReportTaskReady(rMessage);

#ifdef SIGMA_UNIT_TEST
	ServiceMode_UT::ChangeStateCallbackExecuted = TRUE;
#endif  // SIGMA_UNIT_TEST
}

#if defined(SIGMA_BD_CPU)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CommDownCallback()
//
//@ Interface-Description
//	This static method takes a const reference CommDownMessage
//	as input and returns nothing.  It is invoked when the CommDownMessage
//	is received by the task that registered for it.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The SmManager::CommsDown() function is called to report a
//  communication failure.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceMode::CommDownCallback(const CommDownMessage &rMessage)
{
// $[TI1]
	CALL_TRACE("ServiceMode::CommDownCallback(const CommDownMessage &rMessage)") ;
    SmManager::CommsDown();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiReadyCallback()
//
//@ Interface-Description
//	This static BD-only method takes a const reference GuiReadyMessage an
//	as an input and returns nothing.  It is invoked when the GuiReadyMessage
//	is received by the task that registered for it, initializing
//	static private data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize ServiceMode static private data members from the
//	data in the GuiReadyMessage.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceMode::GuiReadyCallback(const GuiReadyMessage &rMessage)
{
// $[TI1]
	CALL_TRACE("ServiceMode::GuiReadyCallback(const GuiReadyMessage &rMessage)") ;
    GetGuiSerialNumber_() = rMessage.getSerialNumber();
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  CalculateIncreasedPriority()
//
// Interface-Description
//	This static BD-only method is used to calculate and store the increased
//	priority of the SM manager task, which is used to temporarily increase
//	the SM manager task priority during time-critical tests.  No arguments,
//	no return value.
//---------------------------------------------------------------------
// Implementation-Description
//	Read the priority of the current task (i.e. the BD executive task),
//	add 1 to this and store this as the increased priority level for the
//	SM manager task.
//
//	NOTE: this method must ONLY be used in SmTasks::ServiceModeBackgroundCycle()
//	since it must read the priority of the BD executive task,
//	and it should only be executed in the first cycle.
//
//	NOTE: this method only stores the increased priority level for later
//	use during time-critical code running in the SM manager task -- the
//	methods IncreasePriority() and RestorePriority() are used to increase
//	the SM manager priority and restore it back to the base priority,
//	respectively.
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	none
// End-Method
//=====================================================================

void
ServiceMode::CalculateIncreasedPriority(void)
{
// $[TI1]
	CALL_TRACE("ServiceMode::CalculateIncreasedPriority(void)") ;

	IncreasedPriority_ = Task::GetPriority() + 1;
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  IncreasePriority()
//
// Interface-Description
//	This static BD-only method is used to temporarily increase the
//	the priority of the SM Manager Task.  No arguments, no return value.
//---------------------------------------------------------------------
// Implementation-Description
//	Increase the priority of the current task to IncreasedPriority_.
//
//	NOTE: this method must ONLY be used in the SM Manager Task, e.g.
//	DO NOT use it in SmFlowController::newCycle() or other code which
//	does not run in the SM Manager Task.
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	none
// End-Method
//=====================================================================

void
ServiceMode::IncreasePriority(void)
{
// $[TI1]
	CALL_TRACE("ServiceMode::IncreasePriority(void)") ;

	Task::SetPriority(IncreasedPriority_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  RestorePriority()
//
// Interface-Description
//	This static BD-only method is used to restore the base priority of
//	the SM Manager Task.  No arguments, no return value.
//---------------------------------------------------------------------
// Implementation-Description
//	Restore the SM Manager Task priority to the base priority.
//
//	NOTE: this method must ONLY be used in the SM Manager Task, e.g.
//	DO NOT use it in SmFlowController::newCycle() or other code which
//	does not run in the SM Manager Task.
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	none
// End-Method
//=====================================================================

void
ServiceMode::RestorePriority(void)
{
// $[TI1]
	CALL_TRACE("ServiceMode::RestorePriority(void)") ;

	const TaskInfo& rSmTaskInfo = Task::GetTaskBaseInfo();

	Task::SetPriority(rSmTaskInfo.GetBasePriority());
}

#endif // defined(SIGMA_BD_CPU)

#if defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdReadyCallback()
//
//@ Interface-Description
//	This static GUI-only method takes a const reference BdReadyMessage an
//	as an input and returns nothing.  It is invoked when the BdReadyMessage
//	is received by the task that registered for it, initializing
//	static private data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize ServiceMode static private data members from the
//	data stored in the BdReadyMessage.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceMode::BdReadyCallback(const BdReadyMessage &rMessage)
{
// $[TI1]
	CALL_TRACE("ServiceMode::BdReadyCallback(const BdReadyMessage &rMessage)") ;

#ifdef SIGMA_DEBUG
	printf( "in ServiceMode::BdReadyCallback(const BdReadyMessage &rMessage)\n") ;
	printf( "sizeof( BdReadyMessage) = %d\n", sizeof( BdReadyMessage)) ;
#endif // SIGMA_DEBUG

	const DataKeyAgent& rDataKeyAgent = rMessage.getDataKeyAgent() ;
	const ConfigurationAgent& rConfigurationAgent = rMessage.getConfigurationAgent() ;

	GetGuiSerialNumber_() = rDataKeyAgent.getGuiSerialNumber() ;
	DataKeyInstalled_ = rDataKeyAgent.isInstalled() ;

#ifdef SIGMA_DEBUG
	cout << "GuiSerialNumber from DataKey = " << GetGuiSerialNumber() << endl ;
	cout << "DataKeyInstalled_ = " << DataKeyInstalled_ << endl ;

//	rConfigurationAgent.dumpDataMembers() ;
#endif // SIGMA_DEBUG

	// copy data key GUI serial # to novram if required
	CheckSerialNumber_();

	ExhValveSignature_ = rConfigurationAgent.getExhValveSignature() ;
	ExhValveCrc_ = rConfigurationAgent.getExhValveCrc() ;

	AirFlowSensorSN_ = rConfigurationAgent.getAirFlowSensorSN() ;
	AirFlowSensorCrc_ = rConfigurationAgent.getAirFlowSensorCrc() ;

	O2FlowSensorSN_ = rConfigurationAgent.getO2FlowSensorSN() ;
	O2FlowSensorCrc_ = rConfigurationAgent.getO2FlowSensorCrc() ;

	ExhFlowSensorSN_ = rConfigurationAgent.getExhFlowSensorSN() ;
	ExhFlowSensorCrc_ = rConfigurationAgent.getExhFlowSensorCrc() ;

	ExhO2FlowSensorSN_ = rConfigurationAgent.getExhO2FlowSensorSN() ;
	ExhO2FlowSensorCrc_ = rConfigurationAgent.getExhO2FlowSensorCrc() ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AreBdAndGuiFlashTheSame()
//
//@ Interface-Description
//	This static GUI-only method has no arguments and returns TRUE
//	if BD and GUI flash store identical data, FALSE if data is
//	not identical or if GUI FLASH data is invalid.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If GUI FLASH data is invalid, return FALSE.
//	If GUI FLASH data is valid, then compare the checksums and exh valve
//	signature from the BD with the respective values of the GUI,
//	return TRUE if all values are identical or FALSE if not.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
ServiceMode::AreBdAndGuiFlashTheSame( void)
{
	CALL_TRACE("ServiceMode::AreBdAndGuiFlashTheSame( void)") ;

	Boolean rtnValue;

	
	Boolean flashOk = RCalInfoFlashBlock.isFlashOkay();

#ifdef SIGMA_UNIT_TEST
	flashOk = ServiceMode_UT::FlashOk;
#endif  // SIGMA_UNIT_TEST

	if (flashOk)
	{
	// $[TI1]
#ifdef SIGMA_DEBUG
		printf( "rCalInfoFlashBlock.isFlashOkay() = TRUE\n") ;
#endif // SIGMA_DEBUG
		if (ExhValveSignature_ == RSystemConfiguration.getExhValveSignature()
			&& ExhValveCrc_ == RSystemConfiguration.getExhValveCrc()
			&& AirFlowSensorCrc_ == RSystemConfiguration.getAirFlowSensorCrc()
			&& O2FlowSensorCrc_ == RSystemConfiguration.getO2FlowSensorCrc()
			&& ExhFlowSensorCrc_ == RSystemConfiguration.getExhFlowSensorCrc()
			&& ExhO2FlowSensorCrc_ == RSystemConfiguration.getExhO2FlowSensorCrc())
		{
		// $[TI1.1]
			rtnValue = TRUE;
		}
		else
		{
		// $[TI1.2]
			rtnValue = FALSE;
		}
		// end else
	}
	else
	{
	// $[TI2]
		rtnValue = FALSE;

#ifdef SIGMA_DEBUG
		printf( "rCalInfoFlashBlock.isFlashOkay() = FALSE\n") ;
#endif // SIGMA_DEBUG

	}
	// end else
	return( rtnValue);
}

#endif // defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetGuiSerialNumber()
//
// Interface-Description
//	This static method has no arguments and returns a const reference to the
//  GUI SerialNumber. On the GUI, this object is used to store the
//  GUI serial number from the DataKey. On the BD, this object is used
//  to store the GUI serial number from the GUI FLASH. This is the public
//  method which returns a const reference, the private member function
//  GetGuiSerialNumber_() returns a reference that can be modified.
//---------------------------------------------------------------------
// Implementation-Description
//	Returns the reference to the function static SerialNumber.
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	none
// End-Method
//=====================================================================

const SerialNumber&
ServiceMode::GetGuiSerialNumber(void)
{
// $[TI1]
	CALL_TRACE("ServiceMode::GetGuiSerialNumber( void)") ;

	return GetGuiSerialNumber_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ServiceMode::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("ServiceMode::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, SERVICEMODE,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  CheckSerialNumber_()
//
//@ Interface-Description
//	This static method is used to update the serial number stored
//	in novram.  No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update the novram serial # from the data key if these serial #'s
//	differ and FLASH serial # is valid (i.e. FLASH serial # is the
//	default or FLASH serial # is same as key serial #).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ServiceMode::CheckSerialNumber_( void)
{
	CALL_TRACE("ServiceMode::CheckSerialNumber_( void)");

	SerialNumber defaultSerialNumber;
	SerialNumber dataKeySerialNumber =
#ifdef SIGMA_BD_CPU
		RDataKey.getSerialNumber( DataKey::BD) ;
#else
		GetGuiSerialNumber();
#endif
	SerialNumber flashSerialNumber = RSystemConfiguration.getFlashSerialNumber() ;
	SerialNumber novRamSerialNumber ;

#ifdef SIGMA_UNIT_TEST
# ifdef SIGMA_BD_CPU
	// reset the flashSerialNumber_ to a value provided by the
	// unit test driver -- this way you don't have to write bogus
	// serial #'s into FLASH for unit testing
	flashSerialNumber = ServiceMode_UT::FlashSerialNumber;
# endif  // SIGMA_BD_CPU
#endif  // SIGMA_UNIT_TEST

	// first check if flash serial # is valid
	if (flashSerialNumber == defaultSerialNumber ||
		flashSerialNumber == dataKeySerialNumber)
	{
	// $[TI1]
		NovRamManager::GetUnitSerialNumber( novRamSerialNumber) ;

		// next check if novram and data key serial #'s mismatch
		if (novRamSerialNumber != dataKeySerialNumber)
		{
		// $[TI1.1]
			NovRamManager::UpdateUnitSerialNumber( dataKeySerialNumber) ;
		}
		// $[TI1.2]
		// implied else
	}
	// $[TI2]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetGuiSerialNumber_()
//
// Interface-Description
//	This static method has no arguments and returns a reference to the
//  GUI SerialNumber. On the GUI, this object is used to store the
//  GUI serial number from the DataKey. On the BD, this object is used
//  to store the GUI serial number from the GUI FLASH.
//---------------------------------------------------------------------
// Implementation-Description
//	Returns the reference to the function static SerialNumber.
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	none
// End-Method
//=====================================================================

SerialNumber&
ServiceMode::GetGuiSerialNumber_(void)
{
// $[TI1]
	CALL_TRACE("ServiceMode::GetGuiSerialNumber_( void)") ;

    static SerialNumber serialNumber;

	return serialNumber;
}
