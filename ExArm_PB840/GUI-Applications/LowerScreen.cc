#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LowerScreen - Contains and displays the
// five areas that constitute the Lower screen during normal ventilation (not
// Service mode): Main Settings Area, Lower Subscreen Area, Lower Screen Select
// Area, Message Area and Prompt Area.
//---------------------------------------------------------------------
//@ Interface-Description
// Only one instance of this class is created.  No parameters are
// needed to construct.  All areas of the screen which go to make up
// the Lower screen are created here.
//
// There are numerous methods provided which must be called when particular
// events occur.  It is then left up to the LowerScreen to decide what to do
// for each event.  Specifically, these methods are: beginNormalVentilation(),
// startNewPatientSetup(), samePatientSetupAccepted(),
// newPatientSetupScreen1Accepted(), newPatientSetupScreen3Accepted(),
// apneaSetupDeactivated(), reviewApneaSetup(), apneaSetupReviewed(),
// activityTimeout() and guiEventHappened().
//
// Some status information is provided by the methods isInPatientSetupMode(),
// apneaPatientSetupStatus() and apneaReviewStatus().
//
// LowerScreen is used as a container for the five objects that implement
// most of the Lower screen's functionality, i.e. MainSettingsArea,
// LowerSubScreenArea, LowerScreenSelectArea, PromptArea and MessageArea.
// There is an inline method provided to access pointers to each instance.
//
// The LowerScreen also manages the Restart button which allows the operator to
// restart the New Patient Setup sequence.  The buttonDownHappened() method
// is used to handle Restart button events.
//---------------------------------------------------------------------
//@ Rationale
// The main purpose of this class is to create and display the various
// areas of the Lower screen.  The areas are as follows: Main Settings
// Area, Lower Subscreen Area, Lower Screen Select Area, Message Area
// and Prompt Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// All Lower screen areas are created automatically and are then added to the
// Lower screen container causing them to be displayed as soon as the Lower
// screen is displayed (by itself being appended to GUI-Foundation's Lower
// basecontainer).
//
// On normal ventilation startup, however, the lower screen select area is
// blanked out and the Vent Startup subscreen automatically activated.  It is
// then up to the Vent Startup subscreen to call startSamePatientSetup() or
// startNewPatientSetup() (depending on which patient setup option is chosen by
// the operator).  This causes the next phase of the Same or New Patient Setup
// sequence to be entered.
//
// The Same Patient Setup mode is chosen by calling the
// samePatientSetupAccepted() method.  We then activate the Apnea Setup
// subscreen and display the Lower Screen Select Area.
//
// If the New Patient Setup sequence is entered then we activate the New
// Patient Setup subscreen (and blank the Main Settings Area).  When the
// operator presses the Continue button on that subscreen then the subscreen
// will call our newPatientSetupScreen1Accepted() method.  We then activate the
// Vent Setup subscreen (it will display in its special New Patient Setup
// mode).  When the operator accepts the Vent Setup settings then that
// subscreen calls our newPatientSetupScreen3Accepted() method.  We then
// activate the Apnea Setup subscreen and redisplay the Main Settings Area and
// the Lower Screen Select Area.
//
// When the Apnea Setup subscreen is first deactivated after the Same/New
// Patient subscreen was accepted then that screen will call our
// apneaSetupDeactivated() method which will signal the end of the special
// Apnea Patient Setup mode (where Apnea Setup must display special prompts
// advising the operator to check Apnea parameters).
//
// Note: the inline isInPatientSetupMode() and apneaPatientSetupStatus()
// methods are used by Vent and Apnea Setup in order to specially modify their
// behaviour for the Same/New Patient Setup sequence.
//
// During New Patient Setup, if the Restart button is pressed then this will be
// detected by the buttonDownHappened() method, which will then restart the
// Same/New Patient Setup sequence.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LowerScreen.ccv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 008   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
// 
//  Revision: 007   By: mjfullm    Date: 03/20/07     SCR Number: 6237
//  Project: Trend
//  Description:
// 		Added calls to TrendDataMgr to inform about patient setup
//
// 
//  Revision: 006   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 005  By:  hhd    Date: 10-May-1999  DCS Number: 5382
//  Project:  ATC
//  Description:
//		When restart button is commanded, make sure the flag isInPatientSetupMode_ is
//	turned on.
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003  By: yyy      Date: 23-Oct-1997  DR Number: 2578
//    Project:  Sigma (R8027)
//    Description:
//      Notify the AlarmLogSubScreen to update its display when new patient
//		setup is accepted.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2050
//    Project:  Sigma (R8027)
//    Description:
//      Reconditioned guiEventHappened() to handle the restart button.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "LowerScreen.hh"

//@ Usage-Classes
#include "UpperScreen.hh"
#include "MiscStrs.hh"
#include "SettingContextHandle.hh"
#include "GuiEventRegistrar.hh"
#include "NovRamManager.hh"
#include "Sound.hh"
#include "MemoryStructs.hh"
#include "AlarmSetupSubScreen.hh"
#include "ApneaSetupSubScreen.hh"
#include "VentSetupSubScreen.hh"
#include "VentStartSubScreen.hh"
#include "UpperSubScreenArea.hh"
#include "TrendDataMgr.hh"


//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 0;
static const Int32 CONTAINER_WIDTH_ = 640;
static const Int32 CONTAINER_HEIGHT_ = 480;
static const Int32 RESTART_BUTTON_X_ = 3;
static const Int32 RESTART_BUTTON_Y_ = 410;
static const Int32 RESTART_BUTTON_WIDTH_ = 85;
static const Int32 RESTART_BUTTON_HEIGHT_ = 43;
static const Int32 RESTART_BUTTON_BORDER_ = 3;

LowerScreen & LowerScreen::RLowerScreen = *((LowerScreen *)
											(((ScreenMemoryUnion *)::ScreenMemory)->normalVentMemory.lowerScreen));

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LowerScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the LowerScreen and all its contents.  No construction
// parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// All areas of the screen are created automatically.  The Lower
// screen is colored white and all areas are added to the Lower
// screen's container list.  We register for callbacks from the
// Restart button and initialize the Main Settings and Lower Screen Select
// areas.
// $[01001] Lower screen contains five areas ...
// $[01008] If the previous patient settings are not available ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LowerScreen::LowerScreen(void) :
restartButton_(RESTART_BUTTON_X_, RESTART_BUTTON_Y_,
			   RESTART_BUTTON_WIDTH_, RESTART_BUTTON_HEIGHT_,
			   Button::LIGHT, RESTART_BUTTON_BORDER_, Button::SQUARE,
			   Button::NO_BORDER,
			   MiscStrs::RESTART_BUTTON_TITLE),
	isInPatientSetupMode_(TRUE),
	apneaPatientSetupStatus_(APNEA_NO_SETUP),
	apneaReviewStatus_(NONE_CORRECTED)

	,subScreenUpperBorder_(3, 159, 637, 3)
	,subScreenLeftBorder_(0, 159, 3, 248)
	,subScreenRightBorder_(637, 159, 3, 248)
{
	CALL_TRACE("LowerScreen::LowerScreen(void)");

	// Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Default background color is White
	setFillColor(Colors::FRAME_COLOR);

	// Register for button callbacks to detect presses of the Restart button
	restartButton_.setButtonCallback(this);

	// Add the various areas to the screen, $[01001]
	addDrawable(&lowerSubScreenArea_);
	addDrawable(&mainSettingsArea_);
	addDrawable(&lowerScreenSelectArea_);
	addDrawable(&messageArea_);
	addDrawable(&promptArea_);

	addDrawable(&subScreenUpperBorder_);
	addDrawable(&subScreenLeftBorder_);
	addDrawable(&subScreenRightBorder_);

	subScreenUpperBorder_.setColor(Colors::MEDIUM_BLUE);
	subScreenUpperBorder_.setFillColor(Colors::MEDIUM_BLUE);
	subScreenUpperBorder_.setShow(FALSE);

	subScreenLeftBorder_.setColor(Colors::MEDIUM_BLUE);
	subScreenLeftBorder_.setFillColor(Colors::MEDIUM_BLUE);
	subScreenLeftBorder_.setShow(FALSE);

	subScreenRightBorder_.setColor(Colors::MEDIUM_BLUE);
	subScreenRightBorder_.setFillColor(Colors::MEDIUM_BLUE);
	subScreenRightBorder_.setShow(FALSE);

	// Initialize areas
	mainSettingsArea_.initialize();
	lowerScreenSelectArea_.initialize();

	// Register for GUI state change callbacks.
	GuiEventRegistrar::RegisterTarget(this);

	// Set the default prompt
	promptArea_.setDefaultPrompt(PromptStrs::TOUCH_A_BUTTON_D);

	// If Vent Startup is not completed or SettingsLockedOut happened due
	// to vent inop or vent timeout,  then automatically display the
	// Vent Startup subscreen.
	// $[01314] Following a powerfail recovery, the lower subscreen ...
	if (!GuiApp::IsVentStartupSequenceComplete() ||
		GuiApp::IsSettingsLockedOut())
	{  
		// We're in patient setup mode
		isInPatientSetupMode_ = TRUE;
	}
	else
	{ 
		// We're not in patient setup mode, 
		isInPatientSetupMode_ = FALSE;

		// Inform the TrendDataMgr that the vent has come up in same patient ventilation after a power fail.
		// This is necessary to continue trending following a power fail or reset condition.
		TrendDataMgr::PatientSetupCompleted( TRUE );
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LowerScreen()  [Destructor]
//
//@ Interface-Description
// The destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LowerScreen::~LowerScreen(void)	
{
	CALL_TRACE("LowerScreen::~LowerScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startNewPatientSetup
//
//@ Interface-Description
// Called from the Vent Startup subscreen when the operator
// chooses the New Patient Setup mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// We must remove the Main Settings Area, add the Restart button and activate
// the New Patient Setup subscreen.
//
// $[01014] When New Patient Setup is chosen, blank Main Settings,
//			display New Patient Setup subscreen and Restart button
//---------------------------------------------------------------------
//@ PreCondition
// We must currently be in Same/New Patient Setup mode.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void LowerScreen::startNewPatientSetup(void)
{
	CALL_TRACE("LowerScreen::startNewPatientSetup(void)");

	CLASS_PRE_CONDITION(isInPatientSetupMode_);

	// Blank the Main Settings Area
	mainSettingsArea_.setBlank(TRUE);

	// Add the Restart button
	addDrawable(&restartButton_);

	// Enter New Patient Setup starting at the Vent Setup Sub-Screen
	lowerSubScreenArea_.activateSubScreen(LowerSubScreenArea::GetVentSetupSubScreen(), NULL);
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: samePatientSetupAccepted
//
//@ Interface-Description
// Called from the Vent Startup subscreen when the user accepts Same
// Patient Setup.  We begin ventilation with the last patient's settings.
//---------------------------------------------------------------------
//@ Implementation-Description
// We must set the Main Settings Area buttons to the up position, display the
// Lower Screen Select Area, inform the Settings-Validation subsystem of the
// event and activate the Apnea Setup subscreen.  Inform the GuiEventRegistrar
// of the change in state (we're now finished with Vent Startup).
//
// $[01013] On Same Patient Accept display and do the following ...
// $[01133] After Same Patient setup display "Waiting for patient connect"
//---------------------------------------------------------------------
//@ PreCondition
// We must currently be in Same/New Patient Setup mode.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void LowerScreen::samePatientSetupAccepted(void)
{
	CALL_TRACE("LowerScreen::samePatientSetupAccepted(void)");

	CLASS_PRE_CONDITION(isInPatientSetupMode_);

	// Make the Accept sound
	Sound::Start(Sound::ACCEPT);

	// Inform Settings-Validation of event
	SettingContextHandle::AcceptSamePatientBatch();

	// Enable the Main Settings Area if Settings not locked out
	if (!GuiApp::IsSettingsLockedOut())
	{
		mainSettingsArea_.setToUp();
	}
	mainSettingsArea_.setBlank(FALSE);

	// Unblank Lower Screen Select Area
	lowerScreenSelectArea_.setBlank(FALSE);

	isInPatientSetupMode_ = FALSE;					// Not in patient setup mode
	apneaPatientSetupStatus_ = APNEA_SAME_PATIENT;	// Now setting up Apnea

	// Enter Apnea Setup subscreen
	lowerSubScreenArea_.activateSubScreen(
										 LowerSubScreenArea::GetApneaSetupSubScreen(),
										 lowerScreenSelectArea_.getApneaSetupTabButton());

	// Inform the Trend Manager that Same patient has been accepted
	TrendDataMgr::PatientSetupCompleted( TRUE );

	UpperScreen::RUpperScreen.patientSetupComplete();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newPatientSetupScreen3Accepted
//
//@ Interface-Description
// Called from the Vent Setup subscreen when New Patient Setup mode is active
// and its settings have been accepted.  We begin ventilation with the new
// patient's settings.
//---------------------------------------------------------------------
//@ Implementation-Description
// We must re-display the Main Settings Area, remove the Restart button,
// display the Lower Screen Select Area, clear the alarm history log and
// activate the Apnea Setup subscreen.  Inform the GuiEventRegistrar of the
// change in state (we're now finished with Vent Startup).
//
// $[01017] Review Apnea Settings after New Patient Setup accepted
//---------------------------------------------------------------------
//@ PreCondition
// We must currently be in Same/New Patient Setup mode, but not in Apnea
// Review mode.
//
// $[01133] After Same Patient setup display "Waiting for patient connect"
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void LowerScreen::newPatientSetupScreen3Accepted(void)
{
	CALL_TRACE("LowerScreen::newPatientSetupScreen3Accepted(void)");

	CLASS_PRE_CONDITION(isInPatientSetupMode_);
	CLASS_PRE_CONDITION(NONE_CORRECTED == apneaReviewStatus_);

	// Unblank the Main Settings Area
	if (!GuiApp::IsSettingsLockedOut())
	{													
		// Enable the Main Settings buttons if Settings not locked out
		mainSettingsArea_.setToUp();
	}													
	mainSettingsArea_.setBlank(FALSE);

	// Remove the Restart button
	removeDrawable(&restartButton_);

	// Unblank Lower Screen Select Area
	lowerScreenSelectArea_.setBlank(FALSE);

	// Not in New Patient Setup mode no more
	isInPatientSetupMode_ = FALSE;

	// Clear the Alarm History log.
	NovRamManager::ClearAlarmLog();

	// Indicate that Apnea Setup should display proper prompts after New Patient
	// Setup.
	apneaPatientSetupStatus_ = APNEA_NEW_PATIENT;

	// Enter Apnea Setup subscreen
	lowerSubScreenArea_.activateSubScreen(
										 LowerSubScreenArea::GetApneaSetupSubScreen(),
										 lowerScreenSelectArea_.getApneaSetupTabButton());

	// Inform the Trend Manager that New patient has been accepted
	TrendDataMgr::PatientSetupCompleted( FALSE );	// New patient

	UpperScreen::RUpperScreen.patientSetupComplete();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: apneaSetupDeactivated
//
//@ Interface-Description
// Called from the Apnea Setup subscreen when it is exited just after
// New or Same Patient Setup.  We reset the Apnea Patient Setup status value.
//---------------------------------------------------------------------
//@ Implementation-Description
// Reset the Apnea Patient Setup status flag.
//---------------------------------------------------------------------
//@ PreCondition
// We must currently be in Apnea Patient Setup mode, but not in Same/
// New Patient Setup mode or Apnea Review mode.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void LowerScreen::apneaSetupDeactivated(void)
{
	CALL_TRACE("LowerScreen::apneaSetupDeactivated(void)");

	CLASS_PRE_CONDITION(APNEA_NO_SETUP != apneaPatientSetupStatus_);
	CLASS_PRE_CONDITION(! isInPatientSetupMode_);
	CLASS_PRE_CONDITION(NONE_CORRECTED == apneaReviewStatus_);

	// Reset Apnea Patient Setup mode flag
	apneaPatientSetupStatus_ = APNEA_NO_SETUP;			
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reviewApneaSetup
//
//@ Interface-Description
// Given the 'apneaCorrectionStatus' value (returned from Settings
// subsystem), decide whether or not to auto-launch the Apnea Setup
// subscreen to force the user to review auto-adjusted apnea settings.
//---------------------------------------------------------------------
//@ Implementation-Description
// If auto-launch of Apnea Setup is necessary, set the 'apneaReviewStatus_'
// flag to 'apneaCorrectionStatus' and launch the Apnea Setup subscreen.  The
// Apnea Setup subscreen queries the review status to decide which prompts to
// display.
//
// $[01101] If apnea settings are auto-corrected then ...
//---------------------------------------------------------------------
//@ PreCondition
// There should not be an Apnea Setup review in progress, and we should
// not be in Same/New Patient Setup mode.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void LowerScreen::reviewApneaSetup(ApneaCorrectionStatus apneaCorrectionStatus)
{
	CALL_TRACE("LowerScreen::reviewApneaSetup(void)");
	CLASS_PRE_CONDITION(! isInPatientSetupMode_);
	CLASS_PRE_CONDITION(NONE_CORRECTED == apneaReviewStatus_);

	switch (apneaCorrectionStatus)
	{
	case NONE_CORRECTED:								
		//
		// Do nothing.
		//
		break;

	case APNEA_PI_CORRECTED:
	case APNEA_O2_CORRECTED:
	case BOTH_CORRECTED:								
		//
		// Leave a breadcrumb for the Apnea Setup subscreen so that it
		// can discover that it is in "review" mode.
		//
		apneaReviewStatus_ = apneaCorrectionStatus;

		// Auto-launch Apnea Setup subscreen
		lowerSubScreenArea_.activateSubScreen(
											 LowerSubScreenArea::GetApneaSetupSubScreen(),
											 lowerScreenSelectArea_.getApneaSetupTabButton());
		break;

	default:											// $[TI3]
		CLASS_ASSERTION(FALSE);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: apneaSetupReviewed
//
//@ Interface-Description
// Called from the Apnea Setup subscreen when the "review mode" for the
// auto-adjusted Apnea settings is active and the operator has exited
// the screen.  That signifies the end of the apnea review mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// Reset the 'apneaReviewStatus_' flag.
//---------------------------------------------------------------------
//@ PreCondition
// There should be an Apnea Setup review in progress, and we should
// not be in Same/New Patient Setup mode.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void LowerScreen::apneaSetupReviewed(void)
{
	CALL_TRACE("LowerScreen::apneaSetupReviewed(void)");

	CLASS_PRE_CONDITION(! isInPatientSetupMode_);
	CLASS_PRE_CONDITION(NONE_CORRECTED != apneaReviewStatus_);

	// Reset the apnea review status flag.
	apneaReviewStatus_ = NONE_CORRECTED;				
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activityTimeout
//
//@ Interface-Description
// Called from the New Patient Setup, Same Patient Setup, Vent Setup (when in
// new patient setup mode) or SST subscreens if the user interface has not been
// used for a period of time.  Go back to the Vent Startup subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// A timeout has the same effect as pressing the Restart button so
// that's what we'll do, press the Restart button down.
//---------------------------------------------------------------------
//@ PreCondition
// We must currently be in Same/New Patient Setup mode.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void LowerScreen::activityTimeout(void)
{
	CALL_TRACE("LowerScreen::activityTimeout(void)");

	CLASS_PRE_CONDITION(isInPatientSetupMode_);

	// Head back to Vent Startup subscreen.
	restartButton_.setToDown();							
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened
//
//@ Interface-Description
// Called when a change in the GUI App state happens.  Passed the id of the
// state which changed.  We react to changes in the states of settings lockout
// and settings transaction success.
//---------------------------------------------------------------------
//@ Implementation-Description
// React only to a settings lockout or settings transaction event.  If
// settings are locked out or the settings transaction failed and we are
// in the middle of a new patient setup then we must revert back to the
// vent startup subscreen.  We do this by pressing the restart button down
// and letting the inevitable callback do the rest.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void LowerScreen::guiEventHappened(GuiApp::GuiAppEvent eventId)
{
	CALL_TRACE("LowerScreen::guiEventHappened(GuiApp::GuiAppEvent eventId)");

	// If a settings lockout or settings transaction event occurred and either
	// settings are locked out or the settings transaction failed and the
	// restart button is visible (i.e. a new patient is being set up) then
	// reset back to vent startup (do this simply by pushing the restart
	// button down).
	if ((eventId != GuiApp::GUI_ONLINE_READY ||
		 (eventId == GuiApp::SETTINGS_TRANSACTION_SUCCESS &&
		  !GuiApp::IsSettingsTransactionSuccess())) &&
		restartButton_.isVisible())
	{											
		// Remove the Restart button
		removeDrawable(&restartButton_);
		restartButton_.setToUp();
	}											
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened [Virtual]
//
//@ Interface-Description
// Called when the Restart button is pressed down.  Passed a pointer to
// the Restart button, 'pButton'.  Go back to the Vent Startup subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Go back to the Vent Startup subscreen and remove the Restart button.
//---------------------------------------------------------------------
//@ PreCondition
// We must currently be in Same/New Patient Setup mode and pButton
// must be a pointer to the Restart button.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void LowerScreen::buttonDownHappened(Button *pButton, Boolean)
{
	CALL_TRACE("LowerScreen::buttonDownHappened(Button *pButton, Boolean)");

	CLASS_PRE_CONDITION(pButton == &restartButton_);
	CLASS_PRE_CONDITION(isInPatientSetupMode_);

	// Activate the Ventilator Startup subscreen
	lowerSubScreenArea_.activateSubScreen(
										 LowerSubScreenArea::GetVentStartSubScreen(), NULL);

	// Remove the Restart button
	removeDrawable(&restartButton_);
	restartButton_.setToUp();
	isInPatientSetupMode_ = TRUE;
	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void LowerScreen::SoftFault(const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LOWERSCREEN,
							lineNumber, pFileName, pPredicate);
}
