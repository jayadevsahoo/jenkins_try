#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  DiscoSensSetting - Disconnection Sensitivity Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the disconnection
//  sensitivity level.  This class inherits from 'BoundedSetting' and
//  provides the specific information needed for representation of 
//  disconnection sensitivity.  This information includes the interval
//  and range of this setting's values, and this batch setting's
//  new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, or dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DiscoSensSetting.ccv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//   
//  Revision: 011   By:   rhj    Date: 13-July-2010  SCR Number: 6581 
//  Project:  XENA2
//  Description:
//       Added an upper soft bound limit for neonatal when leakcomp
//       is enabled.
// 
//  Revision: 010   By:   rhj    Date: 12-May-2009  SCR Number: 6512
//  Project:  840S2
//  Description:
//       Modified the precision value of disconnect sensitivity
//       when circuit type is neonatal and vent type is NIV.
// 
//  Revision: 009   By:   rhj    Date: 23-Apr-2009  SCR Number: 6496
//  Project:  840S2
//  Description:
//       Changed the default values when a neonatal is the 
//       current circuit type.
// 
//  Revision: 008   By:   rhj    Date: 19-Mar-2009  SCR Number: 6496
//  Project:  840S2
//  Description:
//       Modified the disconnect sensitivity's resolution ranges 
//       and its default values.  
// 
//  Revision: 007   By:   rhj    Date: 19-Jan-2009  SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 006   By: gdc    Date: 18-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 005   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "DiscoSensSetting.hh"
#include "VentTypeValue.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "DiscreteSetting.hh"
#include "LeakCompEnabledValue.hh"
#include "PatientCctTypeValue.hh"
#include "BdEventRegistrar.hh"

//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

#define DEFAULT_MAX_VALUE  Real32(95.0f)

//  $[02277] -- The setting's range ...
//  $[02278] -- The setting's resolution ...
//  $[PX0205] -- The setting's softbound ...
static const BoundedInterval  LAST_NODE_ =
{
	1.0f,		// absolute minimum...
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};

static const BoundedInterval  NODE2_ =
{
	10.0f,		// change-point...
	0.5f,		// resolution...
	TENTHS,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE1_ =
{
	DEFAULT_MAX_VALUE,	// change-point...
	1.0f,		// resolution...
	ONES,		// precision...
	&::NODE2_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	DEFINED_UPPER_ALARM_LIMIT_OFF,	// absolute maximum...
	(DEFINED_UPPER_ALARM_LIMIT_OFF - DEFAULT_MAX_VALUE),	// resolution...
	ONES,		// precision...
	&::NODE1_
};



//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: DiscoSensSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, the value interval
//  is initialized.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscoSensSetting::DiscoSensSetting(void)
	: BatchBoundedSetting(SettingId::DISCO_SENS,
						  Setting::NULL_DEPENDENT_ARRAY_,
						  &::INTERVAL_LIST_,
						  DISCO_SENS_MAX_ID,	// maxConstraintId...
						  DISCO_SENS_MIN_ID)	// minConstraintId...
{
	CALL_TRACE("DiscoSensSetting()");


	// register this setting's soft bounds...
	registerSoftBound_(::DISCO_SENS_NEONATAL_SOFT_MAX_ID, Setting::UPPER);

}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~DiscoSensSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscoSensSetting::~DiscoSensSetting(void)	
{
	CALL_TRACE("~DiscoSensSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Disconnect sensitivity is ALWAYS changeable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	DiscoSensSetting::getApplicability(const Notification::ChangeQualifier) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	return(Applicability::CHANGEABLE);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant for the new patient value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	DiscoSensSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	BoundedValue  newPatient;

	// $[02273] The setting's new-patient value ...
	const DiscreteValue VENT_TYPE = 
		SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();

	const DiscreteValue  LEAK_COMP_ENABLED_VALUE =
		SettingsMgr::GetSettingPtr(SettingId::LEAK_COMP_ENABLED)->getAdjustedValue();

	const DiscreteValue  CIRCUIT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAcceptedValue();

	if (LEAK_COMP_ENABLED_VALUE == LeakCompEnabledValue::LEAK_COMP_ENABLED)
	{

		switch (CIRCUIT_TYPE)
		{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			newPatient.value     = 20.0F;
			newPatient.precision = ONES;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :
			newPatient.value     = 40.0F;
			newPatient.precision = ONES;
			break;
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			switch (VENT_TYPE)
			{
			case VentTypeValue::INVASIVE_VENT_TYPE :
				newPatient.value     = 2.0F;
				newPatient.precision = TENTHS;
				break;
			case VentTypeValue::NIV_VENT_TYPE :
				newPatient.value     = 5.0F;
				newPatient.precision = TENTHS;
				break;
			default :
				AUX_CLASS_ASSERTION_FAILURE( VENT_TYPE );
				break;
			}
			break;
		default:
			// Invalid CIRCUIT_TYPE Setting
			AUX_CLASS_ASSERTION_FAILURE( CIRCUIT_TYPE );
			break;
		}

	}
	else
	{

		switch (VENT_TYPE)
		{
		case VentTypeValue::INVASIVE_VENT_TYPE :
			newPatient.value     = 75.0f;
			newPatient.precision = ONES;
			break;
		case VentTypeValue::NIV_VENT_TYPE :
			newPatient.value     = DEFINED_UPPER_ALARM_LIMIT_OFF;
			newPatient.precision = ONES;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE( VENT_TYPE );
			break;
		}

	}


	return(newPatient);
}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NI02010]   -- transitioning from invasive to NIV
//  $[NI02011]   -- transitioning from NIV to invasive
//---------------------------------------------------------------------
//@ PreCondition
//  ((settingId == SettingId::VENT_TYPE)    ||
//   (settingId == SettingId::SUPPORT_TYPE) ||
//   (settingId == SettingId::MAND_TYPE) || 
//   (settingId == SettingId::MODE))
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
	DiscoSensSetting::acceptTransition(
									  const SettingId::SettingIdType settingId,
									  const DiscreteValue            newValue,
									  const DiscreteValue        currValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	AUX_CLASS_PRE_CONDITION( ((settingId == SettingId::VENT_TYPE) ||
							  (settingId == SettingId::SUPPORT_TYPE) ||
							  (settingId == SettingId::MAND_TYPE) || 
							  (settingId == SettingId::MODE)), 
							 settingId);

	const DiscreteValue  NEW_LEAK_COMP_VALUE =
		SettingsMgr::GetSettingPtr(SettingId::LEAK_COMP_ENABLED)->getAdjustedValue();

	const DiscreteValue  CURR_LEAK_COMP_VALUE =
		SettingsMgr::GetSettingPtr(SettingId::LEAK_COMP_ENABLED)->getAcceptedValue();

	const DiscreteValue  CIRCUIT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAcceptedValue();


	if (NEW_LEAK_COMP_VALUE != CURR_LEAK_COMP_VALUE ||
		((settingId == SettingId::VENT_TYPE) &&
		  ((CURR_LEAK_COMP_VALUE == LeakCompEnabledValue::LEAK_COMP_DISABLED) ||
		   ( (CURR_LEAK_COMP_VALUE == LeakCompEnabledValue::LEAK_COMP_ENABLED) && 
			 (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT )
		   )
		  )
		 )
	   )
	{
		setAdjustedValue(getNewPatientValue());
		updateConstraints_();
	}

	// activation of Transition Rule will be italicized if value has changed
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02277] - The setting's range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	DiscoSensSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	//-------------------------------------------------------------------
	// Determine maximum constraint...
	//-------------------------------------------------------------------

	const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();


	Real32  dummySoftMinValue = 0.0f;
	BoundedValue neoBasedMax;

	findSoftMinMaxValues_(dummySoftMinValue, neoBasedMax.value);

	BoundedRange::ConstraintInfo  maxConstraintInfo;
	BoundedRange::ConstraintInfo  minConstraintInfo;

	const DiscreteValue  CURRENT_VENT_TYPE =
		SettingsMgr::GetDiscreteSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();

	const DiscreteValue  LEAK_COMP_ENABLED_VALUE =
		SettingsMgr::GetSettingPtr(SettingId::LEAK_COMP_ENABLED)->getAdjustedValue();

	const DiscreteValue  CIRCUIT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAcceptedValue();


	if (LEAK_COMP_ENABLED_VALUE == LeakCompEnabledValue::LEAK_COMP_DISABLED)
	{
		// disconnect sensiticity is constrained to upper bound of 95% with
		// invasive vent type, otherwise the absolute max OFF is used
		if (CURRENT_VENT_TYPE == VentTypeValue::INVASIVE_VENT_TYPE)
		{	// $[TI1]
			BoundedValue ventTypeBasedMax;
			ventTypeBasedMax.value = 95.0f;

			BoundStatus  dummyStatus(getId());

			// reset this range's maximum constraint to allow for "warping" of a
			// new maximum value...
			getBoundedRange_().resetMaxConstraint();

			// warp the calculated value to a "click" boundary...
			getBoundedRange_().warpValue(ventTypeBasedMax);

			// the disconnect sensitivity is constrained by the vent type
			// therefore save its value and id as the maximum bound value and id...
			maxConstraintInfo.id    = DISCO_SENS_VENT_BASED_MAX_ID;
			maxConstraintInfo.value = ventTypeBasedMax.value;
		}
		else
		{	// $[TI2]
			// the absolute maximum is applicable, therefore save its value
			// and id as the maximum bound value and id...
			maxConstraintInfo.id    = DISCO_SENS_MAX_ID,	// maxConstraintId...
			maxConstraintInfo.value = ABSOLUTE_MAX;
		}
		maxConstraintInfo.isSoft = FALSE;

		minConstraintInfo.value  = 20.0F; // L/min $[02277] 

	}
	else
	{
               //  $[PX0205] -- The setting's softbound ...
		if ( isSoftBoundActive_(DISCO_SENS_NEONATAL_SOFT_MAX_ID) &&
	       (PatientCctTypeValue::NEONATAL_CIRCUIT == CIRCUIT_TYPE))
		{
			maxConstraintInfo.id    = DISCO_SENS_NEONATAL_SOFT_MAX_ID,  
			maxConstraintInfo.value = neoBasedMax.value;
			maxConstraintInfo.isSoft = TRUE;
			
		}
		else
		{
		maxConstraintInfo.id    = DISCO_SENS_MAX_ID,  // maxConstraintId...
		maxConstraintInfo.value = ABSOLUTE_MAX;
			maxConstraintInfo.isSoft = FALSE;

		}

		minConstraintInfo.value  = 1.0F; // L/min $[02277] 
	}


	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);


	//-------------------------------------------------------------------
	//  update minimum constraint...
	//-------------------------------------------------------------------
	minConstraintInfo.id     = DISCO_SENS_MIN_ID;
	minConstraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMinConstraint(minConstraintInfo);


}

//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Use this to return the absolute maximum value that is currently available
//  from this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02111] -- The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	DiscoSensSetting::getAbsoluteMaxValue_(void) const
{
	CALL_TRACE("getAbsoluteMaxValue_()");

	Real32  absoluteMaxValue;


	const DiscreteValue  LEAK_COMP_ENABLED_VALUE =
		SettingsMgr::GetSettingPtr(SettingId::LEAK_COMP_ENABLED)->getAdjustedValue();

	const DiscreteValue  CIRCUIT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAcceptedValue();

	if (LEAK_COMP_ENABLED_VALUE == LeakCompEnabledValue::LEAK_COMP_ENABLED)
	{
		switch (CIRCUIT_TYPE)
		{
		case PatientCctTypeValue::ADULT_CIRCUIT :
			absoluteMaxValue = 65.0F;  // absolute maximum...
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			absoluteMaxValue = 40.0F;  // absolute maximum...
			break;
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			absoluteMaxValue = 15.0F;  // absolute maximum...
			break;
		default:
			// Invalid CIRCUIT_TYPE Setting
			AUX_CLASS_ASSERTION_FAILURE( CIRCUIT_TYPE );
			break;
		}
	}
	else
	{
		absoluteMaxValue = DEFINED_UPPER_ALARM_LIMIT_OFF;  // absolute maximum...

	}

	return(absoluteMaxValue);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	DiscoSensSetting::SoftFault(const SoftFaultID  softFaultID,
								const Uint32       lineNumber,
								const char*        pFileName,
								const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							DISCO_SENS_SETTING, lineNumber, pFileName,
							pPredicate);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of 'this' objects primary setting values has changed, so the settings
//  value will be based on the new values, indicaticated by primaryId. 
//  If this setting's bound is violated by the primary setting's newly 
//  "adjusted" value, then the setting's value is "clipped" to that bound. 
//  Otherwise, a value of 'NULL' is returned to indicate no dependent 
//  bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02002] -- new dependent settings based on proposed primary settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
	DiscoSensSetting::acceptPrimaryChange(const SettingId::SettingIdType primaryId)
{
	CALL_TRACE("acceptPrimaryChange(primaryId)");

	// store the value...
	setAdjustedValue(getNewPatientValue());

	const DiscreteValue  CIRCUIT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAcceptedValue();

	if (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
	{
		activateSoftBound_(::DISCO_SENS_NEONATAL_SOFT_MAX_ID);
	}
	else
	{
		deactivateSoftBound_(::DISCO_SENS_NEONATAL_SOFT_MAX_ID);

	}

	return(NULL);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//   
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	DiscoSensSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
										    Real32& rSoftMaxValue) const
{
	CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

	const Real32  MAX_NEONATAL_LEAK_COMP_SOFT_BOUND = 10.0f;

	//-------------------------------------------------------------------
	// disable lower soft bound limit...
	//-------------------------------------------------------------------

	rSoftMinValue = ::LAST_NODE_.value;

	//-------------------------------------------------------------------
	// determine upper soft bound limit value...
	//-------------------------------------------------------------------

	rSoftMaxValue = MAX_NEONATAL_LEAK_COMP_SOFT_BOUND;
}  



