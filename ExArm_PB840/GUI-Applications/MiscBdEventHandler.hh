#ifndef MiscBdEventHandler_HH
#define MiscBdEventHandler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MiscBdEventHandler - Handles control of MiscBdEvent
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MiscBdEventHandler.hhv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd    Date:  19-JUL-95    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//             Integration baseline.
//====================================================================

#include "BdEventTarget.hh"
#include "GuiAppClassIds.hh"

class MiscBdEventHandler :	public BdEventTarget
{
public:
	MiscBdEventHandler(void);
	~MiscBdEventHandler(void);


	virtual void bdEventHappened(EventData::EventId evType,
					                EventData::EventStatus evStatus,
									EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);


	static void SoftFault(const SoftFaultID softFaultID,
                          const Uint32      lineNumber,
                          const char*       pFileName  = NULL,
                          const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	MiscBdEventHandler(const MiscBdEventHandler&);	// not implemented...
	void   operator=(const MiscBdEventHandler&);	// not implemented...
};


#endif // MiscBdEventHandler_HH
