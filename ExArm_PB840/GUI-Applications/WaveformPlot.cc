#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: WaveformPlot - A waveform plotting base class that handles drawing
// of the static elements of all plots (grid, x and y axis labels etc) and
// provides a common interface for drawing of the actual plot.
//---------------------------------------------------------------------
//@ Interface-Description
// The purpose of WaveformPlot is to handle all the basics of a waveform
// plot excepting the drawing of the plot itself.  All waveform plots are
// plotted on a grid.  Each grid line is labelled with the particular scale
// unit it depicts.  WaveformPlot also provides a way to label both the
// X and Y axes and also to reposition the X and Y axes which are normally
// displayed highlighted at the X and Y 0 markers.
//
// WaveformPlot is designed to be used as a base class.  Derived classes at a
// minimum need to define the update() and endPlot() methods.  The update()
// method is successively called to inform a plot class to draw the specified
// next segment of the plot and endPlot() is called after the final segment of
// the plot has been drawn allowing the plot class to do any tidy up needed.
//
// Other methods which can be redefined by derived classes are: activate()
// which is called before a plot object is displayed allowing the waveform
// grid and other drawables to be properly laid out in preparation for
// display, erase() which erases the plot (thought not the plot's grid)
// and setGridArea() which defines the area (x, y, width, height) within the
// plot's full area in which the grid itself is drawn.
//
// Before a plot should be displayed, the following methods should be called in
// order to set up the basic contents of a plot, i.e. the grid position and
// spacing, its numerical scale labels and the X and Y axis labels:
// setXLimits() to set the X axis scale values, setYLimits() to set the Y axis
// scale values, setYAxisValue() to set the position grid's highlighted Y axis
// (defaults to 0), setXUnitsString() to set the label for the X units and
// setYUnitsString() to set the label for the Y units.
//
// Once the above methods are called the plot is then prepared for display
// by calling activate().
//---------------------------------------------------------------------
//@ Rationale
// This class gathers all the functionality for displaying the basic
// drawables of a waveform plot in one place.
//---------------------------------------------------------------------
//@ Implementation-Description
// An important method is layoutGrid_().  When any features of WaveformPlot and
// the WaveformPlot is visible then this method is called.  It lays out the
// plot, spaces out the grid, determines and positions the numbers which will
// be displayed as the units along the X and Y axes etc.
//
// WaveformPlot is a Container, which contains all the graphics which goes
// up to make the waveform display.  The most important graphic is
// 'plotContainer_', a PlotContainer object, all waveform plotting is
// performed within this container.  This is a protected member which is
// accessible to derived classes so that they know where to draw the waveform.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// which verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformPlot.ccv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: rhj    Date:  01-Sept-2010    SCR Number: 6593
//  Project:  PROX
//  Description:
//     Added a generic message.
//
//  Revision: 007   By: rhj   Date:  16-July-2010    SCR Number: 6606
//  Project:  PROX
//  Description:
//     Added two distinct messagess when prox executes a purge or 
//     an autozero.
//
//  Revision: 006   By: gdc		Date: 28-May-2007   SCR Number:  6330 
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 005  By: jja		Date: 23-May-2002   DCS Number:  6010 
//  Project:  VCP
//  Description:
//      Change text color for auxYLabelBox_.setContentsColor for screen prints.
//
//  Revision: 004   By: sah    Date:  11-Jul-2000    DCS Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  changed constructor to take a boolean as to whether this plot is
//         an upper plot (plot #1) or a lower plot (plot #2)
//      *  added the shadow plot line (for histogram), along with the shadow
//         label box, as part of the new shadow trace functionality
//
//  Revision: 003   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "WaveformPlot.hh"
#include "MiscStrs.hh"

//@ Usage-Classes
#include "BaseContainer.hh"
//@ End-Usage

//@ Code...

static const Int32 X_VALUE_GAP_ = 7;
static const Int32 Y_VALUE_GAP_ = 4;
static const Int32 PLOT_LINE_THICKNESS_ = 1;
static const Int32 Y_VALUE_OFFSET_ = 5;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WaveformPlot  [Default Constructor]
//
//@ Interface-Description
// Default constructor for a WaveformPlot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize all data members.  Color and add all appropriate drawables.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformPlot::WaveformPlot(const Boolean isUpperPlot) :
					currAuxYUnits_(PatientDataId::NULL_PATIENT_DATA_ITEM),
					activeAuxYUnits_(PatientDataId::NULL_PATIENT_DATA_ITEM),
					upperXLimit_(0),
					lowerXLimit_(0),
					xDivision_(0),
					displayXValues_(TRUE),
					upperYLimit_(0),
					lowerYLimit_(0),
					yDivision_(0),
					displayPositiveYValues_(TRUE),
					lastXPixel_(0),
					lastYPixel_(0),
					xScaleFactor_(0.0),
					yScaleFactor_(0.0),
	                message_(MiscStrs::EMPTY_STRING),
					IS_UPPER_PLOT_(isUpperPlot)
{
	CALL_TRACE("WaveformPlot::WaveformPlot(DataStreamMaster *pDataStreamMaster)");

	xUnitsField_.setColor(Colors::WHITE);
	yUnitsField_.setColor(Colors::WHITE);
	addDrawable(&xUnitsField_);
	addDrawable(&yUnitsField_);
	addDrawable(&plotContainer_);

	for (Uint32 i = 0; i < MAX_GRID_LINES_; i++)
	{
		xGridLines_[i].setColor(Colors::MEDIUM_BLUE);
		yGridLines_[i].setColor(Colors::MEDIUM_BLUE);
	}

	xAxisLine_.setColor(Colors::LIGHT_BLUE);
	yAxisLine_.setColor(Colors::LIGHT_BLUE);	

	plotLine_.setPenWidth( PLOT_LINE_THICKNESS_ );		
	plotLine_.setShow(FALSE);   // not part of normal refresh

	auxPlotLine_.setColor(Colors::DARK_BLUE);
	auxPlotLine_.setShow(FALSE);   // not part of normal refresh

	// set initial colors to same as background (make "invisible")...
	auxYLabelBox_.addDrawable(&auxYLabelText_);
	auxYLabelBox_.setColor(Colors::DARK_BLUE);
	auxYLabelBox_.setFillColor(Colors::DARK_BLUE);
	auxYLabelBox_.setContentsColor(Colors::BLACK);
	auxYLabelBox_.setWidth(55);
	auxYLabelBox_.setHeight(20);
	auxYLabelBox_.setShow(FALSE);

    message_.setShow(FALSE);


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~WaveformPlot  [Destructor]
//
//@ Interface-Description
// Destroys a WaveformPlot.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformPlot::~WaveformPlot(void)
{
	CALL_TRACE("WaveformPlot::WaveformPlot(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Activates a WaveformPlot.  Should be called just before display of
// the plot.  Derived classes which redefine this method should always
// call this method first in the overridden method definition.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call layoutGrid_() to setup the plot's grid.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformPlot::activate(void)
{
	CALL_TRACE("WaveformPlot::activate(void)");

	layoutGrid_();										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
// This method must be defined in derived classes.  It is responsible for
// drawing successive segments of a waveform plot.  It is passed a
// reference to a WaveformIntervalIter.  This specifies the interval of
// waveform data that must be plotted.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: endPlot
//
//@ Interface-Description
// This method must be defined in derived classes.  It is called at the
// end of drawing a complete waveform plot and it allows the derived plot
// to do any tidy up needed to complete the waveform drawing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: erase
//
//@ Interface-Description
// Erases a waveform plot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Because the line segments of a waveform plot are not known to the
// GUI-Foundations drawable tree (they are drawn using PlotContainer's
// plotLine() method which bypasses the normal drawing scheme of
// GUI-Foundations) we must do a some funky stuff in order to erase the plot.
// We toggle the show flag of the plotContainer_ thus marking the
// plotContainer_ object as being changed and then call the base container's
// repaint() method thus causing a full redraw of plotContainer_ which will
// result in an erasing of the waveform plot.
//
// Ignore the request if this object is not being displayed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformPlot::erase(void)
{
	CALL_TRACE("WaveformPlot::erase(void)");

	if (isVisible())
	{													// $[TI1]
		plotContainer_.setShow(FALSE);
		plotContainer_.setShow(TRUE);
		getBaseContainer()->repaint();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setXLimits
//
//@ Interface-Description
// Sets the min and max limits of the X-axis.  Passed the following
// arguments:
// >Von
//	upperXLimit		The upper limit of the X axis.
//	lowerXLimit		The lower limit of the X axis.
//	xDivision		The division between grid lines on the X axis, i.e. a
//					grid line will be drawn every 'xDivision' amount between
//					'lowerXLimit' and 'upperXLimit'.
//	displayXValues	A flag indicating whether the X values which label each
//					vertical grid line should be displayed or not.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the passed in values and call gridChanged_() which will do what needs
// to be done with the changes.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformPlot::setXLimits(Int32 upperXLimit, Int32 lowerXLimit,
							Uint32 xDivision, Boolean displayXValues)
{
	CALL_TRACE("WaveformPlot::setXLimits(Int32 upperXLimit, Int32 lowerXLimit, "
							"Uint32 xDivision, Boolean displayXValues)");

	CLASS_PRE_CONDITION((lowerXLimit < upperXLimit) && (0 != xDivision));

	// Store new values
	upperXLimit_	= upperXLimit;
	lowerXLimit_	= lowerXLimit;
	xDivision_		= xDivision;
	displayXValues_ = displayXValues;

	// Note that the grid has changed
	gridChanged_();										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setYLimits
//
//@ Interface-Description
// Sets the min and max limits of the Y-axis.  Passed the following
// arguments:
// >Von
//	upperYLimit				The upper limit of the Y axis.
//	lowerYLimit				The lower limit of the Y axis.
//	yDivision				The division between grid lines on the Y axis, i.e.
//							a grid line will be drawn every 'yDivision' amount
//							between 'lowerYLimit' and 'upperYLimit'.
//	displayPositiveYValues	A flag indicating whether the Y value labels that
//							label the horizontal grid lines should be displayed
//							as all positive, i.e. a grid line at -20 will be
//							labelled 20, a grid line at -40 will be labelled
//							40 (note: needed for Flow-Time plots).
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the passed in values and call gridChanged_() which will do what needs
// to be done with the changes.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformPlot::setYLimits(Int32 upperYLimit, Int32 lowerYLimit,
							Uint32 yDivision, Boolean displayPositiveYValues)
{
	CALL_TRACE("WaveformPlot::setYLimits(Int32 upperYLimit, Int32 lowerYLimit, "
							"Uint32 yDivision, Boolean displayPositiveYValues)");

	CLASS_PRE_CONDITION((lowerYLimit < upperYLimit) && (0 != yDivision));

	// Store new values
	upperYLimit_			= upperYLimit;
	lowerYLimit_			= lowerYLimit;
	yDivision_				= yDivision;
	displayPositiveYValues_ = displayPositiveYValues;

	// Note that the grid has changed
	gridChanged_();										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setGridArea
//
//@ Interface-Description
// Sets the area occupied by the grid within the area of the WaveformPlot
// object itself.  The area is passed via x, y, width and height values.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the new area and call gridChanged_() which will do what needs
// to be done with the changes.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformPlot::setGridArea(Uint32 x, Uint32 y, Uint32 width, Uint32 height)
{
	CALL_TRACE("WaveformPlot::setGridArea(Uint32 x, Uint32 y, Uint32 width, "
											"Uint32 height)");

	lastXPixel_ = width - 1;
	lastYPixel_ = height - 1;

	plotContainer_.setX(x);
	plotContainer_.setY(y);
	plotContainer_.setWidth(width);
	plotContainer_.setHeight(height);

	auxYLabelBox_.setX(plotContainer_.getWidth() + 80);
	auxYLabelBox_.setY((IS_UPPER_PLOT_)
						? 5
						: (plotContainer_.getHeight() - 15));

	// Note that the grid has changed
	gridChanged_();										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setYAxisValue
//
//@ Interface-Description
// Sets the position of the highlighted Y axis.  This defaults to 0 but
// needs to be changed for some applications (e.g. indicating the current
// baseline pressure value on the Pressure-Volume plot).  Passed the floating
// point position of the new Y axis.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the new value and call gridChanged_() which will do what needs
// to be done with the changes.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformPlot::setYAxisValue(Real32 yAxisValue)
{
	CALL_TRACE("WaveformPlot::setYAxisValue(Real32 yAxisValue)");

	// Store new value
	yAxisValue_ = yAxisValue;

	// Note that the grid has changed
	gridChanged_();										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setXUnitsString
//
//@ Interface-Description
// Sets the position and contents of the string that labels the units of the
// X axis.  Passed the cheap text string and its x and y position within
// the WaveformPlot container.
//---------------------------------------------------------------------
//@ Implementation-Description
// Pass the parameters straight on to xUnitsField_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformPlot::setXUnitsString(StringId xUnitsString, Int32 x, Int32 y)
{
	CALL_TRACE("WaveformPlot::setXUnitsString(StringId xUnitsString, Int32 x, "
																	"Int32 y)");

	// Set contents and position of X units label
	xUnitsField_.setText(xUnitsString);
	xUnitsField_.setX(x);
	xUnitsField_.setY(y);								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setYUnitsStrings
//
//@ Interface-Description
// Sets the position and contents of the string that labels the units of the
// Y axis.  Passed the cheap text string and its x and y position within
// the WaveformPlot container.
//---------------------------------------------------------------------
//@ Implementation-Description
// Pass the parameters straight on to yUnitsField_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformPlot::setYUnitsString(StringId yUnitsString, Int32 x, Int32 y)
{
	CALL_TRACE("WaveformPlot::setYUnitsString(StringId yUnitsString, Int32 x, "
																	"Int32 y)");

	// Set contents and position of Y units label
	yUnitsField_.setText(yUnitsString);
	yUnitsField_.setX(x);
	yUnitsField_.setY(y);								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutGrid_
//
//@ Interface-Description
// Lays out the grid according to all the information passed to the
// setXLimits(), setYLimits(), setGridArea() and setYAxisValue() methods.
//---------------------------------------------------------------------
//@ Implementation-Description
// Basically, lay out the grid and all appropriate labels according to
// the values stored in various data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
WaveformPlot::layoutGrid_(void)
{
	CALL_TRACE("WaveformPlot::layoutGrid_(void)");


	wchar_t valueBuf[128];		// Temporary buffer for stuffing numbers into

	// Calculate scale factors (used for translating X and Y values from the
	// grid's coordinate system into on-screen pixel coordinates).
	calculateScaleFactors_();

	// Empty the plot of drawables before beginning the layout
	removeAllDrawables();
	plotContainer_.removeAllDrawables();
	addDrawable(&plotContainer_);

	// First thing we do is add and position the "Y grid lines", those
	// vertical lines parallel to the Y axis which mark the various divisions
	// of the X axis.  Calculate how many we need.
	Uint numYGridLines = (upperXLimit_ - lowerXLimit_) / xDivision_ + 1;
	CLASS_ASSERTION((numYGridLines > 1) &&
						(numYGridLines <= MAX_GRID_LINES_));

	Int32 xValue = lowerXLimit_;

	Int32 i = 0;
	for (i = 0; i < numYGridLines; i++, xValue += xDivision_)
	{
		// Calculate the X coordinate where the corresponding Y grid line will be
		// displayed.
		Int32 xPos = xValueToPoint_(xValue);

		// First check if we need to label the Y grid line with its
		// corresponding X value.
		if (displayXValues_)
		{												// $[TI1]
			// Put the X value into cheap text and set and position the
			// string which displays it.
			swprintf(valueBuf, L"{p=8,y=8:%d}", xValue);
			xValues_[i].setText(valueBuf);
			xValues_[i].setX(plotContainer_.getX() + xPos - xValues_[i].getWidth() / 2);
			xValues_[i].setY(plotContainer_.getY() + lastYPixel_
															+ X_VALUE_GAP_);

			// The first and last X value label should be displayed in white,
			// otherwise color it medium grey.
			if ((i == 0) || (i == numYGridLines - 1))
			{											// $[TI2]
				xValues_[i].setColor(Colors::WHITE);
			}
			else
			{											// $[TI3]
				xValues_[i].setColor(Colors::LIGHT_BLUE);
			}

			// Display the label
			addDrawable(&xValues_[i]);
		}												// $[TI4]

		// Position and add the Y grid line
		yGridLines_[i].setStartPoint(xPos, 0);
		yGridLines_[i].setEndPoint(xPos, lastYPixel_);
		plotContainer_.addDrawable(&yGridLines_[i]);
	}

	// If we're display X values then also display the X units label
	if (displayXValues_)
	{													// $[TI5]
		addDrawable(&xUnitsField_);
	}													// $[TI6]

	// Position Y axis
	Int32 yAxisXPos = xValueToPoint_(yAxisValue_);
	yAxisLine_.setStartPoint(yAxisXPos, 0);
	yAxisLine_.setEndPoint(yAxisXPos, lastYPixel_);

	// Now add and position the "X grid lines", those horizontal lines parallel
	// to the X axis which mark the various divisions of the Y axis.  Calculate
	// how many we need.
	Uint numXGridLines = (upperYLimit_ - lowerYLimit_) / yDivision_ + 1;
	CLASS_ASSERTION((numXGridLines > 1) &&
						(numXGridLines <= MAX_GRID_LINES_));

	Int32  yValue = lowerYLimit_;

	for (i = 0; i < numXGridLines; i++, yValue += yDivision_)
	{
		// Calculate the Y coordinate where the corresponding X grid line will be
		// displayed.
		Int32 yPos = yValueToPoint_(yValue);

		// Put the Y value into cheap text and set and position the
		// string which displays it.
		swprintf(valueBuf, L"{p=8,y=8:%d}",
				displayPositiveYValues_ && (yValue < 0) ? - yValue : yValue);
		yValues_[i].setText(valueBuf);
		yValues_[i].setX(plotContainer_.getX() - yValues_[i].getWidth()
												- Y_VALUE_GAP_);
		yValues_[i].setY(yPos + Y_VALUE_OFFSET_ - yValues_[i].getHeight() / 2);

		// The first and last Y value label should be displayed in white,
		// otherwise color it medium grey.
		if ((i == 0) || (i == numXGridLines - 1))
		{												// $[TI7]
			yValues_[i].setColor(Colors::WHITE);
		}
		else
		{												// $[TI8]
			yValues_[i].setColor(Colors::LIGHT_BLUE);
		}

		// Display the label
		addDrawable(&yValues_[i]);

		// Position and add the X grid line
		xGridLines_[i].setStartPoint(0, yPos);
		xGridLines_[i].setEndPoint(lastXPixel_, yPos);
		plotContainer_.addDrawable(&xGridLines_[i]);
	}

	// Display the Y units label
	addDrawable(&yUnitsField_);

	// Position the X-axis line
	Int32 xAxisYPos = yValueToPoint_(0.0);
	xAxisLine_.setStartPoint(0, xAxisYPos);
	xAxisLine_.setEndPoint(lastXPixel_, xAxisYPos);

	// Add X and Y axis at the end to make sure they are on top of
	// everything else
	plotContainer_.addDrawable(&xAxisLine_);
	plotContainer_.addDrawable(&yAxisLine_);
	
	// Add the plot line drawn using Direct Draw only
	plotContainer_.addDrawable(&plotLine_);
	plotContainer_.addDrawable(&auxPlotLine_);
	addDrawable(&auxYLabelBox_);


	message_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
	message_.setX( (plotContainer_.getWidth() /2) - 
							         (message_.getWidth()/2 ) );
	plotContainer_.addDrawable(&message_);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateScaleFactors_
//
//@ Interface-Description
// Calculates the scaling factors which are used to convert X and Y waveform
// data points into corresponding X and Y pixel coordinates within the
// waveform plot area (the grid).
//---------------------------------------------------------------------
//@ Implementation-Description
// Do the appropriate calculations.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformPlot::calculateScaleFactors_(void)
{
	CALL_TRACE("WaveformPlot::calculateScaleFactors_(void)");

	AUX_CLASS_PRE_CONDITION((lowerXLimit_ < upperXLimit_),
							lowerXLimit_);
	AUX_CLASS_PRE_CONDITION((lowerYLimit_ < upperYLimit_),
							lowerYLimit_);

	xScaleFactor_ = Real32(lastXPixel_) / (upperXLimit_ - lowerXLimit_);
	yScaleFactor_ = Real32(lastYPixel_) / (upperYLimit_ - lowerYLimit_);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformPlot::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, WAVEFORMPLOT,
									lineNumber, pFileName, pPredicate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayMessage
//
//@ Interface-Description
//  Displays a generic message
//---------------------------------------------------------------------
//@ Implementation-Description
// The first argument gets the input message which will be displayed
//  and the second argument allows to show or do not show the message.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void WaveformPlot::displayMessage(StringId msg, Boolean showMessage)
{

    // Do not display a message when 
	// the input argument msg is NULL
	if (msg == NULL)
	{
		return;
	}
    message_.setText(msg);
    message_.setShow(showMessage);
	message_.positionInContainer(GRAVITY_CENTER);
}


