#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  InspTimeSetting - Inspiratory Time Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the time (in milliseconds)
//  an inspiration lasts.  This class inherits from 'BatchBoundedSetting' and
//  provides the specific information needed for representation of
//  inspiratory time.  This information includes the interval and range of
//  this setting's values, this setting's response to a Main Control
//  Setting's transition (see 'acceptTransition()'), this setting's response
//  to a change of its primary setting (see 'acceptPrimaryChange()'), this
//  batch setting's new-patient value (see 'getNewPatientValue()'), and the
//  contraints and dependent settings of this setting.
//
//  This setting also dynamically monitors the constant parm setting,
//  and updates its applicability accordingly.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class also defines a 'calcDependentValues_()' method for updating
//  this setting's dependent settings, and an 'updateConstraints_()' method
//  for updating the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/InspTimeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 018   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 017   By: mnr    Date: 20-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Variable renamed according to coding standards.  
//
//  Revision: 016   By: mnr    Date: 10-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//	Enforce lower bound for Insp Time, because of lowered tidal volume.
//
//  Revision: 015   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 014  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added peak insp flow to array of dependent ids, for 'VC+'
//      *  added support for new 'VC+' mandatory type
//      *  added new transition rule requirement mappings
//      *  modified DEVELOPMENT-only test code to support testing in VC+
//      *  added new 'calcDependentValues_()' methods for determining list
//         of dependents
//
//  Revision: 013   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  added support for RAMP during new-patient calculation
//
//  Revision: 012   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  moved BiLevel-specific implementation to new PEEP-high-time
//	   setting
//	*  added new 'getApplicability()' method
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//	*  now an observer of constant parm setting, therefore 'valueUpdate()',
//	   'doRetainAttachment()' and 'settingObserverInit()' overridden from
//	   observer base class
//
//  Revision: 011   By: dosman    Date:  11-Nov-1998    DCS Number: 5252
//  Project:  BILEVEL
//  Description:
//	Added SRS requirement number where code carries out requirement.
//
//  Revision: 010   By: dosman    Date:  23-Sep-1998    DCS Number: BILEVEL 144
//  Project:  BILEVEL
//  Description:
//	Added code to assert if the transition rules cannot settle on a stable
//	value when trying to resolve dependent bounds.
//
//  Revision: 009   By: dosman    Date:  07-May-1998    DCS Number: 
//  Project:  BILEVEL
//  Description:
//	add & fix TIs
//
//  Revision: 008   By: dosman    Date:  29-Apr-1998    DCS Number: 4
//  Project:  BILEVEL
//  Description:
//	changed resolution of TI back to hundredths 
//
//  Revision: 007   By: dosman    Date:  29-Apr-1998    DCS Number: 34
//  Project:  BILEVEL
//  Project: Sigma (R8027)
//  Description:
//	Added explicit change of bound id for TH versus TI based on mode bilevel
//
//  Revision: 006   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 005   By: dosman    Date:  16-Jan-1998    DCS Number: 
//  Project:  BILEVEL
//  Description:
//    Initial BiLevel version.
//    Changed absolute max to reflect new,
//      less stringent max, for bilevel mode.
//    Used symbolic constants.
//    Changed acceptTransition() to handle transition 
//      from BiLevel mode and to BiLevel mode
//	(see Method Description for details).
//    Added assertion in updateConstraints_()
//      to check that absolute maximum is updated/correct
//      based on mode.
//    Added public virtual method updateAbsValues_()
//      (see Method Description for details)
//    Corrected new value of TI to 1000.0 msec in acceptTransition()
//    Changed constructor to initialize a DoubleIntervalRange
//      and ensured that the resolution rules we had earlier are
//      still kept, but only for the minimum (low value) resolution.
//    Fixed a hole in the implementation of the transition from
//      bilevel by using if-else instead of two if-statements
//      Added requirement numbers for tracing to SRS. 
//
//  Revision: 004   By: sah    Date:  31-Oct-1997    DCS Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	Added back the warping "down" of expiratory time-based bounds,
//	because "nearest" was still causing original problem.
//
//  Revision: 003   By: sah    Date:  20-Oct-1997    DCS Number: 2563
//  Project: Sigma (R8027)
//  Description:
//	Changed the calculation of insp. time from a "VCV" parameter in
//	'acceptPrimaryChange()', to "round" to the nearest resolution.
//	Changed the "warping" of all constraint values that were calculated
//	based on summations, to "NEAREST" rounding.  The "warping" for the
//	exp. time-based bound must remain as 'WARP_DOWN' while in VCV mode,
//	because of the "coarseness" of inspiratory time's resolution during
//	VCV.
//
//  Revision: 002   By: sah    Date:  10-Aug-1997    DCS Number: 2359
//  Project: Sigma (R8027)
//  Description:
//	Added warning message for future maintenance.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "InspTimeSetting.hh"
#include "FlowPatternValue.hh"
#include "MandTypeValue.hh"
#include "ConstantParmValue.hh"
#include "ModeValue.hh"
#include "SettingConstants.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "RespRateSetting.hh"
#include "ConstantParmSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02175] The setting's resolution ...
static const Real32  VCV_RESOLUTION_ = 20.0f;
static const Real32  PCV_RESOLUTION_ = 10.0f;


// This array is set up dynamically via this class's 'calcDependentValues_()',
// because inspiratory time doesn't have any dependents during 'SPONT' mode,
// but it has two dependents otherwise...
static SettingId::SettingIdType  ArrDependentSettingIds_[] =
  {
    SettingId::EXP_TIME,
    SettingId::IE_RATIO,
    SettingId::PEAK_INSP_FLOW,

    SettingId::NULL_SETTING_ID
  };


// $[02172] The setting's range ...
// $[02175] The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    200.0f,		// absolute minimum...
    0.0f,		// unused...
    TENS,		// unused...
    NULL
  };
static BoundedInterval  IntervalList_ =
  {
    8000.0f,		// absolute maximum...
    20.0f,		// resolution...
    TENS,		// precision...
    &::LAST_NODE_
  };



//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: InspTimeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Construct an instance of an inspiratory time setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

InspTimeSetting::InspTimeSetting(void)
       : BatchBoundedSetting(SettingId::INSP_TIME,
			     ::ArrDependentSettingIds_,
			     &::IntervalList_,
			     INSP_TIME_MAX_ID,		// maxConstraintId...
			     INSP_TIME_MIN_ID)		// minConstraintId...

{
  CALL_TRACE("InspTimeSetting()");
	isATimingSetting_ = TRUE;
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~InspTimeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

InspTimeSetting::~InspTimeSetting(void)
{
  CALL_TRACE("~InspTimeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01049] -- Ti shown in SPONT mode
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
InspTimeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pMode     = SettingsMgr::GetSettingPtr(SettingId::MODE);
  const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);
  const Setting*  pConstantParm =
			SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM);

  DiscreteValue  modeValue;
  DiscreteValue  mandTypeValue;
  DiscreteValue  constantParmValue;

  switch (qualifierId)
  {
  case Notification::ACCEPTED :		// $[TI1]
    modeValue         = pMode->getAcceptedValue();
    mandTypeValue     = pMandType->getAcceptedValue();
    constantParmValue = pConstantParm->getAcceptedValue();
    break;
  case Notification::ADJUSTED :		// $[TI2]
    modeValue         = pMode->getAdjustedValue();
    mandTypeValue     = pMandType->getAdjustedValue();
    constantParmValue = pConstantParm->getAdjustedValue();
    break;
  default :
    // unexpected qualifier id...
    AUX_CLASS_ASSERTION_FAILURE(qualifierId);
    break;
  }

  Applicability::Id  applicability;

  switch (modeValue)
  {
  case ModeValue::AC_MODE_VALUE :
  case ModeValue::SIMV_MODE_VALUE :		// $[TI3]
    switch (mandTypeValue)
    {
    case MandTypeValue::PCV_MAND_TYPE :
    case MandTypeValue::VCP_MAND_TYPE :		// $[TI3.1]
      applicability =
	    (constantParmValue == ConstantParmValue::INSP_TIME_CONSTANT)
	     ? Applicability::CHANGEABLE	// $[TI3.1.1]
	     : Applicability::VIEWABLE;		// $[TI3.1.2]
      break;
    case MandTypeValue::VCV_MAND_TYPE :		// $[TI3.2]
      applicability = Applicability::VIEWABLE;
      break;
    default :
      // unexpected mandatory type...
      AUX_CLASS_ASSERTION_FAILURE(mandTypeValue);
      break;
    }
    break;

  case ModeValue::SPONT_MODE_VALUE :	// $[TI4]
  case ModeValue::CPAP_MODE_VALUE :	
    switch (mandTypeValue)
    {
    case MandTypeValue::PCV_MAND_TYPE :
    case MandTypeValue::VCP_MAND_TYPE :		// $[TI4.1]
      applicability = Applicability::CHANGEABLE;
      break;
    case MandTypeValue::VCV_MAND_TYPE :		// $[TI4.2]
      applicability = Applicability::VIEWABLE;
      break;
    default :
      // unexpected mandatory type...
      AUX_CLASS_ASSERTION_FAILURE(mandTypeValue);
      break;
    }
    break;

  case ModeValue::BILEVEL_MODE_VALUE :		// $[TI5]
    applicability = Applicability::INAPPLICABLE;
    break;

  default :
    // unexpected mode...
    AUX_CLASS_ASSERTION_FAILURE(modeValue);
    break;
  }

  return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02173] -- this Setting's new-patient calculation...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
InspTimeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  // force to "VCV" resolution; this ensures that the resulting Ti will
  // be the same no matter what mandatory type current value...
  ::IntervalList_.resolution = ::VCV_RESOLUTION_;

  // calculate inspiratory time's new-patient value using the VCV
  // new-patient values (any VCV setting id will do)...
  BoundedValue  newPatientValue = calcBasedOnSetting_(
  						SettingId::TIDAL_VOLUME,
						BoundedRange::WARP_NEAREST,
						BASED_ON_NEW_PATIENT_VALUES
						       );

  // check the value to not fall below the lower bound
  const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
  newPatientValue.value = MAX_VALUE( ABSOLUTE_MIN, newPatientValue.value );

  const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

  const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

  switch (MAND_TYPE_VALUE)
  {
  case MandTypeValue::PCV_MAND_TYPE :
  case MandTypeValue::VCP_MAND_TYPE :  // $[TI1]
    // set to "PCV" resolution...
    ::IntervalList_.resolution = ::PCV_RESOLUTION_;
    break;
  case MandTypeValue::VCV_MAND_TYPE :  // $[TI2]
    // set to "VCV" resolution...
    ::IntervalList_.resolution = ::VCV_RESOLUTION_;
    break;
  default :
    // unexpected mand type...
    AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
    break;
  }

  return(newPatientValue);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned, and this setting's value is "clipped"
//  to that bound.  Otherwise, a value of 'NULL' is returned to indicate no
//  dependent bound was violated.
//
//  If inspiratory time is being changed by a VCV setting, this method
//  will automatically update inspiratory time's dependent settings (i.e.,
//  expiratory time and I:E ratio), this allows the VCV settings to notify
//  and monitor inspiratory time, only.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02002] -- new dependent settings based on proposed primary settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
InspTimeSetting::acceptPrimaryChange(const SettingId::SettingIdType primaryId)
{
  CALL_TRACE("acceptPrimaryChange(primaryId)");

  // update dynamic constraints...
  updateConstraints_();

  BoundStatus&  rBoundStatus = getBoundStatus_();

  // initialize to holding this setting's id...
  rBoundStatus.setViolationId(getId());

  BoundedValue  newInspTime;

  newInspTime = calcBasedOnSetting_(primaryId, BoundedRange::WARP_NEAREST,
				    BASED_ON_ADJUSTED_VALUES);

  // test 'newInspTime' against this setting's bounded range; the bound
  // violation state, if any, is returned in 'rBoundStatus', while
  // 'newInspTime' is "clipped" if a bound is violated...
  getBoundedRange_().testValue(newInspTime, rBoundStatus);

  // store the value...
  setAdjustedValue(newInspTime);

  const BoundStatus*  pBoundStatus;

  // if there is a bound violation return a pointer to this setting's
  // bound status, otherwise return 'NULL'...
  pBoundStatus = (rBoundStatus.isInViolation()) ? &rBoundStatus	// $[TI1]
						: NULL;		// $[TI2]

  return(pBoundStatus);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, toValue, fromValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'fromValue' to 'toValue'.
//
//  The only transitions that inspiratory time is interested in 
//  are 
//  (1) the transition of mandatory type;
//  (2) the transition of mode from "BiLevel". 
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[BL02000]\c\ -- from-BILEVEL-to-SPONT transition rules
//  $[BL02000]\f\ -- from-BILEVEL-to-AC/SIMV transition rules
//  $[02022]\i\   -- from-PC-to-VC transition rules
//  $[VC02006]\d\ -- from-VC-to-VC+ transition rules
//  $[VC02006]\k\ -- from-PC-to-VC+ transition rules
//---------------------------------------------------------------------
//@ PreCondition
//  ((settingId == SettingId::MODE  &&
//    fromValue == ModeValue::BILEVEL_MODE_VALUE) || 
//   settingId == SettingId::MAND_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
InspTimeSetting::acceptTransition(const SettingId::SettingIdType settingId,
				  const DiscreteValue            toValue,
				  const DiscreteValue            fromValue)
{
  CALL_TRACE("acceptTransition(settingId, toValue, fromValue)");

  // reset both constraints to ensure that "old" constraints won't interfere
  // with newly-calculated values...
  getBoundedRange_().resetMinConstraint();
  getBoundedRange_().resetMaxConstraint();

  const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
  BoundedValue  newInspTime;

  if (settingId == SettingId::MODE) 
  {   // $[TI1] -- transition initiated by mode change...
    // the only mode transition that TI is concerned with is a transition from
    // BiLevel...
    AUX_CLASS_PRE_CONDITION((fromValue == ModeValue::BILEVEL_MODE_VALUE),
			    fromValue);

    const Setting*  pPeepHighTime =
			SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH_TIME);

    const Real32  PEEP_HIGH_TIME_VALUE =
			 BoundedValue(pPeepHighTime->getAdjustedValue()).value;

    switch (toValue)
    {
    case ModeValue::SPONT_MODE_VALUE :				// $[TI1.1]
    case ModeValue::CPAP_MODE_VALUE :
      //--------------------------------------------------------------
      // mode transition from BILEVEL to SPONT...
      //--------------------------------------------------------------
      if (PEEP_HIGH_TIME_VALUE > 1000.0f) 
      {   // $[TI1.1.1]
	newInspTime.value = 1000.0f;
      }
      else
      {   // $[TI1.1.2]
	newInspTime.value = PEEP_HIGH_TIME_VALUE;
      }
      break;

    case ModeValue::AC_MODE_VALUE :
    case ModeValue::SIMV_MODE_VALUE :				// $[TI1.2]
      //--------------------------------------------------------------
      // mode transition from BILEVEL to A/C or SIMV...
      //--------------------------------------------------------------
      {
	const Setting*  pHlRatio =
			    SettingsMgr::GetSettingPtr(SettingId::HL_RATIO);
	const Setting*  pRespRate =
			    SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);

	const Real32  HL_RATIO_VALUE  =
			   BoundedValue(pHlRatio->getAcceptedValue()).value;
	const Real32  RESP_RATE_VALUE =
			   BoundedValue(pRespRate->getAcceptedValue()).value;

	if (RESP_RATE_VALUE < 16.0f  &&  PEEP_HIGH_TIME_VALUE > 1000.0f) 
	{   // $[TI1.2.1]
	  newInspTime.value = 1000.0f;
	}
	else if (RESP_RATE_VALUE >= 16.0f  &&  HL_RATIO_VALUE > 4.0f) 
	{   // $[TI1.2.2]
	  // based on clipping the H:L ratio to 4:1...
	  static const Real32  INSP_TIME_FACTOR_ = (4.0f / 5.0f);

	  const Real32  BREATH_PERIOD_VALUE =
	      RespRateSetting::GetBreathPeriod(BASED_ON_ADJUSTED_VALUES);

	  newInspTime.value = (INSP_TIME_FACTOR_ * BREATH_PERIOD_VALUE);
	}
	else
	{   // $[TI1.2.3]
	  newInspTime.value = PEEP_HIGH_TIME_VALUE;
	}
      }
      break;

    case ModeValue::BILEVEL_MODE_VALUE :
    default :
      // unexpected mode value...
      AUX_CLASS_ASSERTION_FAILURE(toValue);
      break;
    }

	// check the value to not fall below the lower bound
	newInspTime.value = MAX_VALUE( ABSOLUTE_MIN, newInspTime.value );

    // warp to a "click" boundary...
    getBoundedRange_().warpValue(newInspTime);

    // store the transition value into the adjusted context...
    setAdjustedValue(newInspTime);
  }
  else if (settingId == SettingId::MAND_TYPE) 
  {   // $[TI2] -- transition initiated by mandatory type change...
    switch (toValue)
    {
    case MandTypeValue::PCV_MAND_TYPE :		// $[TI2.1]
      //--------------------------------------------------------------
      // mandatory type transition to PC...
      //--------------------------------------------------------------
      // use a 10-ms resolution during PCV...
      ::IntervalList_.resolution = ::PCV_RESOLUTION_;
      break;

    case MandTypeValue::VCV_MAND_TYPE :		// $[TI2.2]
      //--------------------------------------------------------------
      // mandatory type transition to VC...
      //--------------------------------------------------------------
      // use a 20-ms resolution during VCV...
      ::IntervalList_.resolution = ::VCV_RESOLUTION_;

      // calculate inspiratory time from the "adjusted" VCV parameters...
      newInspTime = calcBasedOnSetting_(SettingId::TIDAL_VOLUME,
					BoundedRange::WARP_NEAREST,
					BASED_ON_ADJUSTED_VALUES);

	  // check the value to not fall below the lower bound
	  newInspTime.value = MAX_VALUE( ABSOLUTE_MIN, newInspTime.value );

      // store the transition value into the adjusted context...
      setAdjustedValue(newInspTime);
      break;

    case MandTypeValue::VCP_MAND_TYPE :		// $[TI2.3]
      //--------------------------------------------------------------
      // mandatory type transition to VC+...
      //--------------------------------------------------------------
      // use the resolution for PC...
      ::IntervalList_.resolution = ::PCV_RESOLUTION_;

      // calculate inspiratory time from the "adjusted" VCV parameters...
      newInspTime = calcBasedOnSetting_(SettingId::TIDAL_VOLUME,
					BoundedRange::WARP_NEAREST,
					BASED_ON_ADJUSTED_VALUES);

	  // check the value to not fall below the lower bound
	  newInspTime.value = MAX_VALUE( ABSOLUTE_MIN, newInspTime.value );

      // store the transition value into the adjusted context...
      setAdjustedValue(newInspTime);
      break;

    default:
      // unexpected mandatory type...
      AUX_CLASS_ASSERTION_FAILURE(toValue);
      break;
    }
  }
  else 
  {
    // unexpected 'settingId' value...
    AUX_CLASS_ASSERTION_FAILURE(settingId);
  }

  setForcedChangeFlag();
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to subject's value changes, by, possibly, updating this setting's
//  applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::CONSTANT_PARM)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
InspTimeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
			     const SettingSubject*               pSubject)
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");
  SAFE_AUX_CLASS_PRE_CONDITION((pSubject->getId() == SettingId::CONSTANT_PARM),
			       pSubject->getId());

  // update this instance's applicability...
  updateApplicability(qualifierId);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
InspTimeSetting::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Constant Parm setting,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
InspTimeSetting::settingObserverInit(void)
{
  CALL_TRACE("settingObserverInit()");

  // monitor changes in constant parm's value...
  attachToSubject_(SettingId::CONSTANT_PARM, Notification::VALUE_CHANGED);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
InspTimeSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) !=
					    Applicability::INAPPLICABLE)
    {
      Setting*  pExpTime = SettingsMgr::GetSettingPtr(SettingId::EXP_TIME);
      Setting*  pIeRatio = SettingsMgr::GetSettingPtr(SettingId::IE_RATIO);

      // forward on to inspiratory time's dependent settings...
      isValid = (pExpTime->isAcceptedValid()  &&
		 pIeRatio->isAcceptedValid());

      if (!isValid)
      {
	cout << "INVALID Inspiratory Time:\n";
	cout << *this << *pExpTime << *pIeRatio << endl;
      }

      const Setting*  pPeakInspFlow =
			SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW);

      if (isValid  &&
	  pPeakInspFlow->getApplicability(Notification::ACCEPTED) ==
						  Applicability::CHANGEABLE)
      {
	// Is the the adjusted inspiratory time value consistent with the
	// adjusted VCV parameters?...
	const Real32  INSP_TIME_VALUE = BoundedValue(getAcceptedValue()).value;
	const Real32  CALC_INSP_TIME_VALUE = calcBasedOnSetting_(
						SettingId::TIDAL_VOLUME,
						BoundedRange::WARP_NEAREST,
						BASED_ON_ACCEPTED_VALUES).value;

	isValid = (INSP_TIME_VALUE == CALC_INSP_TIME_VALUE);

	if (!isValid)
	{
	  const Setting*  pFlowPattern =
			SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
	  const Setting*  pMinInspFlow =
			SettingsMgr::GetSettingPtr(SettingId::MIN_INSP_FLOW);
	  const Setting*  pTidalVolume =
		       SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
	  const Setting*  pPlateauTime =
			 SettingsMgr::GetSettingPtr(SettingId::PLATEAU_TIME);

	  cout << "\nINVALID Inspiratory Time:\n";
	  cout << "CALC = " << CALC_INSP_TIME_VALUE << endl;
	  cout << *this << *pFlowPattern << *pMinInspFlow << *pPeakInspFlow
	       << *pPlateauTime << *pTidalVolume << endl;
	}
      }
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
InspTimeSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) !=
					    Applicability::INAPPLICABLE)
    {
      Setting*  pExpTime = SettingsMgr::GetSettingPtr(SettingId::EXP_TIME);
      Setting*  pIeRatio = SettingsMgr::GetSettingPtr(SettingId::IE_RATIO);

      // forward on to inspiratory time's dependent settings...
      isValid = (pExpTime->isAdjustedValid()  &&
      		 pIeRatio->isAdjustedValid());

      if (!isValid)
      {
	cout << "INVALID Inspiratory Time:\n";
	cout << *this << *pExpTime << *pIeRatio << endl;
      }

      const Setting*  pPeakInspFlow =
			SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW);

      if (isValid  &&
	  pPeakInspFlow->getApplicability(Notification::ADJUSTED) ==
						  Applicability::CHANGEABLE)
      {
	// Is the the adjusted inspiratory time value consistent with the
	// adjusted VCV parameters?...
	const Real32  INSP_TIME_VALUE = BoundedValue(getAdjustedValue()).value;
	const Real32  CALC_INSP_TIME_VALUE = calcBasedOnSetting_(
						SettingId::TIDAL_VOLUME,
						BoundedRange::WARP_NEAREST,
						BASED_ON_ADJUSTED_VALUES).value;

	isValid = (INSP_TIME_VALUE == CALC_INSP_TIME_VALUE);

	if (!isValid)
	{
	  const Setting*  pFlowPattern =
			SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
	  const Setting*  pMinInspFlow =
			SettingsMgr::GetSettingPtr(SettingId::MIN_INSP_FLOW);
	  const Setting*  pTidalVolume =
			SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
	  const Setting*  pPlateauTime =
			SettingsMgr::GetSettingPtr(SettingId::PLATEAU_TIME);

	  cout << "\nINVALID Inspiratory Time:\n";
	  cout << "CALC = " << CALC_INSP_TIME_VALUE << endl;
	  cout << *this << *pFlowPattern << *pMinInspFlow << *pPeakInspFlow
	       << *pPlateauTime << *pTidalVolume << endl;
	}
      }
    }
  }
  else
  {
    cout << "INVALID Inspiratory Time:\n";
    cout << *this << endl;
  }

  return(isValid);
}

#endif // defined (SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
InspTimeSetting::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  INSP_TIME_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02177] -- this setting's dependent settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
InspTimeSetting::calcDependentValues_(const Boolean isValueIncreasing)
{
  CALL_TRACE("calcDependentValues_(isValueIncreasing)");

  const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

  const DiscreteValue  MODE_VALUE = pMode->getAdjustedValue();

  Uint  idx = 0u;

  switch (MODE_VALUE)
  {
  case ModeValue::AC_MODE_VALUE :
  case ModeValue::SIMV_MODE_VALUE :		// $[TI1]
    {
      const Setting*  pMandType =
			    SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

      const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

      switch (MAND_TYPE_VALUE)
      {
      case MandTypeValue::PCV_MAND_TYPE :
      case MandTypeValue::VCV_MAND_TYPE :	// $[TI1.1]
	// all timing dependents are active here...
	ArrDependentSettingIds_[idx++] = SettingId::EXP_TIME;
	ArrDependentSettingIds_[idx++] = SettingId::IE_RATIO;
	break;
      case MandTypeValue::VCP_MAND_TYPE :	// $[TI1.2]
	// all dependents are active here...
	ArrDependentSettingIds_[idx++] = SettingId::EXP_TIME;
	ArrDependentSettingIds_[idx++] = SettingId::IE_RATIO;
	ArrDependentSettingIds_[idx++] = SettingId::PEAK_INSP_FLOW;
	break;
      default :
	AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
	break;
      }
    }
    break;
  case ModeValue::SPONT_MODE_VALUE :		// $[TI2]
  case ModeValue::CPAP_MODE_VALUE :
    // no dependents in 'SPONT' mode...
    break;
  case ModeValue::BILEVEL_MODE_VALUE :
  default :
    AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
    break;
  }

  // terminate with the null id...
  ArrDependentSettingIds_[idx] = SettingId::NULL_SETTING_ID;

  // forward on to base class...
  return(Setting::calcDependentValues_(isValueIncreasing));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//
//  WARNING:  any new bounds that are added to this method may also affect
//            the 'calcDependentValues()' method of RespRateSetting.  Please
//            make sure that the latter method is checked.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
InspTimeSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

  const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

  if (MAND_TYPE_VALUE == MandTypeValue::VCV_MAND_TYPE)
  {   // $[TI1]
    ::IntervalList_.resolution = ::VCV_RESOLUTION_;
  }
  else if (MAND_TYPE_VALUE == MandTypeValue::PCV_MAND_TYPE  ||
	   MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE)
  {   // $[TI2]
    ::IntervalList_.resolution = ::PCV_RESOLUTION_;
  }
  else
  {
    // invalid mandatory type...
    AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
  }

  //===================================================================
  // determine inspiratory time's maximum bound...
  //===================================================================

  BoundedRange::ConstraintInfo  maxConstraintInfo;

  maxConstraintInfo.id    = INSP_TIME_MAX_ID;
  maxConstraintInfo.value = getAbsoluteMaxValue_();

  const Setting*  pRespRate = SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);

  if (pRespRate->getApplicability(Notification::ADJUSTED) !=
						Applicability::INAPPLICABLE)
  {   // $[TI3] -- respiratory rate (and breath period) is applicable...
    const Real32  BREATH_PERIOD_BASED_MAX =
      (RespRateSetting::GetBreathPeriod(BASED_ON_ADJUSTED_VALUES) - 20.0f);

    if (BREATH_PERIOD_BASED_MAX < maxConstraintInfo.value)
    {   // $[TI3.1]
      // NOTE:   though this constraint will never be hit in a quiescent mode,
      //         it is possible with a LARGE knob delta to jump to an insp
      //         time value larger than this constraint, thereby messing
      //         up I:E ratio bound calculations; this prevents the screwed
      //         up I:E calculation scenario, and allows for I:E ratio to
      //         detect the bound violation, itself...
      maxConstraintInfo.value = BREATH_PERIOD_BASED_MAX;
#if defined(SIGMA_DEVELOPMENT)
      if (Settings_Validation::IsDebugOn(::EXP_TIME_SETTING))
      {
	cout << "\nITS::uC "
	     << " max==" << maxConstraintInfo.value
	     << endl;
      }
#endif  // defined(SIGMA_DEVELOPMENT)
    }   // $[TI3.2]
  }   // $[TI4] -- respiratory rate (and breath period) is NOT applicable...

  // this setting has no soft bounds...
  maxConstraintInfo.isSoft = FALSE;

  // update inspiratory time's maximum constraint info...
  getBoundedRange_().updateMaxConstraint(maxConstraintInfo);


  //===================================================================
  // determine inspiratory time's minimum bound...
  //===================================================================

  const Setting*  pPlateauTime =
			SettingsMgr::GetSettingPtr(SettingId::PLATEAU_TIME);
  const Real32  PLATEAU_TIME_VALUE =
  			BoundedValue(pPlateauTime->getAdjustedValue()).value;

  BoundedRange::ConstraintInfo  minConstraintInfo;

  minConstraintInfo.id    = INSP_TIME_MIN_ID;
  minConstraintInfo.value = getAbsoluteMinValue_();

  if (pPlateauTime->getApplicability(Notification::ADJUSTED) !=
					      Applicability::INAPPLICABLE  &&
      PLATEAU_TIME_VALUE > 0.0f)
  {   // $[TI5] -- plateau time is applicable, and plateau time is a factor...
    //===================================================================
    // calculate inspiratory time's minimum bound that is based on plateau
    // time's current value...
    //===================================================================

    BoundedValue  plateauTimeBasedMin;

    // calculate the plateau time-based minimum limit on inspiratory time
    // using the current plateau time value, and the absolute minimum value
    // of inspiratory time...
    plateauTimeBasedMin.value = PLATEAU_TIME_VALUE + minConstraintInfo.value;

    // "warp" to a resolution boundary...
    getBoundedRange_().resetMinConstraint();  // must do BEFORE "warping"...
    getBoundedRange_().warpValue(plateauTimeBasedMin, BoundedRange::WARP_NEAREST);

    minConstraintInfo.id    = INSP_TIME_MIN_BASED_PLAT_TIME_ID;
    minConstraintInfo.value = plateauTimeBasedMin.value;
  }   // $[TI6]

  // this setting has no minimum soft bounds...
  minConstraintInfo.isSoft = FALSE;

  // update inspiratory time's minimum constraint info...
  getBoundedRange_().updateMinConstraint(minConstraintInfo);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All new-patient calculations are to be based on the VCV setting values.
//
// $[02173] -- this Setting's new-patient calculation...
//---------------------------------------------------------------------
//@ PreCondition
//  ((basedOnSettingId == SettingId::TIDAL_VOLUME  ||
//    basedOnSettingId == SettingId::PEAK_INSP_FLOW  ||
//    basedOnSettingId == SettingId::PLATEAU_TIME  ||
//    basedOnSettingId == SettingId::FLOW_PATTERN)  &&
//   (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//    basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//			||
//  ((basedOnSettingId == SettingId::RESP_RATE  ||
//    basedOnSettingId == SettingId::IE_RATIO  ||
//    basedOnSettingId == SettingId::EXP_TIME)  &&
//    basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
InspTimeSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
				    ) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

  SettingId::SettingIdType  finalBasedOnSettingId;

  if (basedOnSettingId == SettingId::RESP_RATE)
  {   // $[TI1] -- convert 'RESP_RATE' to "based-on" id...
    const DiscreteValue  CONSTANT_PARM_VALUE =
			       ConstantParmSetting::GetValue(basedOnCategory);

    switch (CONSTANT_PARM_VALUE)
    {
    case ConstantParmValue::EXP_TIME_CONSTANT :    // $[TI1.1]
      // respiratory rate is being changed while holding expiratory time
      // constant, therefore calculate the new inspiratory time from the
      // current expiratory time...
      finalBasedOnSettingId = SettingId::EXP_TIME;
      break;
    case ConstantParmValue::IE_RATIO_CONSTANT :    // $[TI1.2]
      // respiratory rate is being changed while holding I:E ratio
      // constant, therefore calculate the new inspiratory time from the
      // current I:E ratio...
      finalBasedOnSettingId = SettingId::IE_RATIO;
      break;
    case ConstantParmValue::INSP_TIME_CONSTANT :
    case ConstantParmValue::TOTAL_CONSTANT_PARMS :
    default :
      // invalid value for 'CONSTANT_PARM_VALUE'...
      AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
      break;
    };
  }
  else
  {   // $[TI2]
    // use passed in parameter...
    finalBasedOnSettingId = basedOnSettingId;
  }

  BoundedValue  inspTime;

  switch (finalBasedOnSettingId)
  {
  //-------------------------------------------------------------------
  // $[02013](2) -- new insp. time while changing I:E ratio...
  // $[02017](1) -- new insp. time while keeping I:E ratio constant...
  //-------------------------------------------------------------------
  case SettingId::IE_RATIO :
    {   // $[TI3] -- base insp time's calculation on I:E ratio...
      // inspiratory time is to base its value on the "adjusted" I:E ratio,
      // only...
      AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
      			  basedOnCategory);

      const Setting*  pIeRatio =
			  SettingsMgr::GetSettingPtr(SettingId::IE_RATIO);

      // get the I:E ratio value from the Adjusted Context...
      const Real32  IE_RATIO_VALUE =
			    BoundedValue(pIeRatio->getAdjustedValue()).value;

      // get the breath period from the respiratory rate...
      const Real32  BREATH_PERIOD_VALUE =
			    RespRateSetting::GetBreathPeriod(basedOnCategory);

      Real32  eRatioValue;
      Real32  iRatioValue;

      if (IE_RATIO_VALUE < 0.0f)
      {   // $[TI3.1]
	// The I:E ratio value is negative, therefore we have 1:xx with
	// "xx" the expiratory portion of the ratio...
	iRatioValue = 1.0f;
	eRatioValue = -(IE_RATIO_VALUE);
      }
      else
      {   // $[TI3.2]
	// The I:E ratio value is positive, therefore we have xx:1 with
	// "xx" the inspiratory portion of the ratio...
	iRatioValue = IE_RATIO_VALUE;
	eRatioValue = 1.0f;
      }

      // calculate the insp time from the I:E ratio...
      inspTime.value = (iRatioValue * BREATH_PERIOD_VALUE) /
			   (iRatioValue + eRatioValue);
    }
    break;

  //-------------------------------------------------------------------
  // $[02012](2) -- new insp. time while changing exp. time...
  // $[02016](2) -- new insp. time while keeping exp. time constant...
  //-------------------------------------------------------------------
  case SettingId::EXP_TIME :
    {   // $[TI4] -- base insp time's calculation on exp time...
      // inspiratory time is to base its value on the "adjusted" expiratory
      // time, only...
      AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
      			  basedOnCategory);

      const Setting*  pExpTime =
			  SettingsMgr::GetSettingPtr(SettingId::EXP_TIME);

      // get the expiratory time value from the Adjusted Context...
      const Real32  EXP_TIME_VALUE =
      			BoundedValue(pExpTime->getAdjustedValue()).value;

      // get the breath period from the respiratory rate...
      const Real32  BREATH_PERIOD_VALUE =
			    RespRateSetting::GetBreathPeriod(basedOnCategory);

      // calculate inspiratory time...
      inspTime.value = BREATH_PERIOD_VALUE - EXP_TIME_VALUE;
    }
    break;

  //-------------------------------------------------------------------
  // $[02019](2) -- new insp. time while changing VCV values...
  //-------------------------------------------------------------------
  case SettingId::TIDAL_VOLUME :
  case SettingId::PEAK_INSP_FLOW :
  case SettingId::PLATEAU_TIME :
  case SettingId::FLOW_PATTERN :
    {   // $[TI5] -- base insp time's calculation on the VCV settings...
      // get pointers to each of the VCV parameters...
      const Setting*  pFlowPattern =
			 SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
      const Setting*  pPeakInspFlow =
			 SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW);
      const Setting*  pPlateauTime =
			 SettingsMgr::GetSettingPtr(SettingId::PLATEAU_TIME);
      const Setting*  pTidalVolume =
			 SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

      DiscreteValue  flowPatternValue;
      Real32         peakInspFlowValue;
      Real32         plateauTimeValue;
      Real32         tidalVolumeValue;

      switch (basedOnCategory)
      {
      case BASED_ON_NEW_PATIENT_VALUES :	// $[TI5.1]
		// get the new-patient values of each of the VCV parameters...
		flowPatternValue  = pFlowPattern->getNewPatientValue();
		peakInspFlowValue =
				  BoundedValue(pPeakInspFlow->getNewPatientValue()).value;
		plateauTimeValue  =
				  BoundedValue(pPlateauTime->getNewPatientValue()).value;
		tidalVolumeValue  =
				  BoundedValue(pTidalVolume->getNewPatientValue()).value;
		break;

      case BASED_ON_ADJUSTED_VALUES :		// $[TI5.2]
		// get the "adjusted" values of each of the VCV parameters...
		flowPatternValue  = pFlowPattern->getAdjustedValue();
		peakInspFlowValue =
				BoundedValue(pPeakInspFlow->getAdjustedValue()).value;
		plateauTimeValue  =
				BoundedValue(pPlateauTime->getAdjustedValue()).value;
		tidalVolumeValue  =
				BoundedValue(pTidalVolume->getAdjustedValue()).value;
		break;

      case BASED_ON_ACCEPTED_VALUES :
			// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
			// get the "accepted" values of each of the VCV parameters...
			flowPatternValue  = pFlowPattern->getAcceptedValue();
			peakInspFlowValue =
					BoundedValue(pPeakInspFlow->getAcceptedValue()).value;
			plateauTimeValue  =
					BoundedValue(pPlateauTime->getAcceptedValue()).value;
			tidalVolumeValue  =
					BoundedValue(pTidalVolume->getAcceptedValue()).value;
			break;
#endif // defined(SIGMA_DEVELOPMENT)

      default :
		// unexpected 'basedOnCategory' value..
		AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
		break;
      };

      switch (flowPatternValue)
      {
      case FlowPatternValue::SQUARE_FLOW_PATTERN : 
		{   // $[TI5.3] 
		  static const Real32  SQUARE_FACTOR_ = (60000.0f * 0.001f);
	
		  // $[02019]b -- formula for calculating inspiratory time from the
		  //	      VCV parameters...
		  inspTime.value = ((SQUARE_FACTOR_ * tidalVolumeValue) /
						peakInspFlowValue);
		}
		break;

      case FlowPatternValue::RAMP_FLOW_PATTERN :  
	  {   // $[TI5.4] 
		  static const Real32  RAMP_FACTOR_ = (60000.0f * 0.001f * 2.0f);
	
		  const Setting*  pMinInspFlow =
				   SettingsMgr::GetSettingPtr(SettingId::MIN_INSP_FLOW);
	
		  Real32  minInspFlowValue;
	
		  switch (basedOnCategory)
		  {
		  case BASED_ON_NEW_PATIENT_VALUES :	// $[TI5.4.2]
			// get the "new-patient" minimum insp flow value...
			minInspFlowValue =
				BoundedValue(pMinInspFlow->getNewPatientValue()).value;
			break;
		  case BASED_ON_ADJUSTED_VALUES :	// $[TI5.4.1]
			// get the "adjusted" minimum insp flow value...
			minInspFlowValue =
				BoundedValue(pMinInspFlow->getAdjustedValue()).value;
			break;
		  case BASED_ON_ACCEPTED_VALUES :
			// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
			// get the "accepted" minimum insp flow value...
			minInspFlowValue =
				BoundedValue(pMinInspFlow->getAcceptedValue()).value;
				break;
#endif // defined(SIGMA_DEVELOPMENT)
			default :
				// unexpected 'basedOnCategory' value..
				AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
				break;
		   };

	  // $[02019]b -- formula for calculating inspiratory time from the
	  //	      VCV parameters...
	  inspTime.value = ((RAMP_FACTOR_ * tidalVolumeValue) /
			    (peakInspFlowValue + minInspFlowValue));
	}
	break;

      default :
	AUX_CLASS_ASSERTION_FAILURE(flowPatternValue);
	break;
      };

      // $[02019]b -- add the plateau time...
      inspTime.value += plateauTimeValue;
    }
    break;

  default :
    // unexpected dependent setting id...
    AUX_CLASS_ASSERTION_FAILURE(finalBasedOnSettingId);
    break;
  };

  // place insp time on a "click" boundary...
  getBoundedRange().warpValue(inspTime, warpDirection, FALSE);

  return(inspTime);
}
