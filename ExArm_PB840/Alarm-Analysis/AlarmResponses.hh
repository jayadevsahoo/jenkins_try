#ifndef AlarmResponses_HH
#define AlarmResponses_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmResponses - Alarm-Analysis class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmResponses.hhv   25.0.4.0   19 Nov 2013 13:51:18   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "Alarm.hh"

//@ Usage-Classes
//@ End-Usage


class AlarmResponses {
  public:

    static void VILedOnPatDataOff(void); 
    static void VILedOffPatDataOn(void);

    static void BdAlarmOnGuiSetOff(void);
    static void BdAlarmOffGuiSetOn(void);

    static void LOILedOnBdAlarmOn(void);
    static void LOILedOffBdAlarmOff(void);

    static void GuiSetOffPatDataOff(void);
    static void GuiSetOnPatDataOn(void);

    static void LOILedOnBdAlarmOnGuiSetOffPatDataOff(void);
    static void LOILedOffBdAlarmOffGuiSetOnPatDataOn(void);

    static void LOILedOnBdAlarmOnGuiSetOff(void);
    static void LOILedOffBdAlarmOffGuiSetOn(void);

    static void GuiSetOff(void);
    static void GuiSetOn(void);

    static void PatDataOff(void);
    static void PatDataOn(void);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
  protected:

  private:
    AlarmResponses (void); // Declared only
    ~AlarmResponses(void); // Declared only

    inline static void LossOfInterfaceLedOn_(void);
    inline static void LossOfInterfaceLedOff_(void);
    inline static void BdAudibleAlarmOn_(void);
    inline static void BdAudibleAlarmOff_(void);
    inline static void PatientDataDisplayOn_(void);
    inline static void PatientDataDisplayOff_(void);
    inline static void GuiSettingChangesOn_(void);
    inline static void GuiSettingChangesOff_(void);
    inline static void VentInopLedOn_(void);
    inline static void VentInopLedOff_(void);
};


// Inlined methods
#include "AlarmResponses.in"


#endif // AlarmResponses_HH 
