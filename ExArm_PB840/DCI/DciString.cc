#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== C L A S S   D E S C R I P T I O N ====
//@ Class: DciString - Store a null-terminated character string with an
// associated validity tag
//---------------------------------------------------------------------
//@ Interface-Description
// This object holds a null-terminated character string of length <
// MAX_DCI_STRING_SIZE and a tag indicating whether it is valid or not
// (invalid, not supported, not in this release and to be determined).
//---------------------------------------------------------------------
//@ Rationale
// Need a string value in DCI that also has an indication of whether
// the string is valid or not.
//---------------------------------------------------------------------
//@ Implementation-Description
// Derive from base class, DciValueStatus, the validity tag.  Whenever
// we put() a value, provided it fits in the private storage space, the
// string is defined to be valid.
//---------------------------------------------------------------------
//@ Module-Decomposition
//	DciString [constructor]
//	~DciString [destructor]
//	get
//---------------------------------------------------------------------
//@ Fault-Handling
// CLASS_PRE_CONDITION is asserted if an attempt is made to put() a
// string of length >= MAX_DCI_STRING_SIZE.
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/DCI/vcssrc/DciString.ccv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 17-DEC-1997  DR Number: 2605
//  Project: Sigma (R8027)
//  Description:
//      Restructuring of DCI to fix several problems. 
//
//  Revision: 1.0	By: jdm	Date: 7/26/95	DR String: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "Sigma.hh"
#include "DciString.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DciString [constructor]
//
//@ Interface-Description
// Do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

DciString::DciString()
{
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DciString [destructor]
//
//@ Interface-Description
// Do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

DciString::~DciString()
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  get
//
//@ Interface-Description
// Return the stored string's status by name.  Optionally, return the
// string, if the user passed a buffer to hold it.
// NOTE: The burden is on the caller to verify that the value is valid prior
//	 to its use.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// Length of buffer pointed to by optional argument *must be* >=
// MAX_DCI_STRING_SIZE.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

DciValueStatus::DciValueStatusId
DciString::get(char * pString)
{
	DciValueStatus::DciValueStatusId status = getStatus();

	if ( pString != NULL )
	{						// $[TI1.1]
		*pString = NULL;
		if (isOk())
		{					// $[TI2.1]
			strcpy(pString, string_);
		}
							// $[TI2.2]
	}
							// $[TI1.2]
	return(status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// A fault handling assertion has been taken.
//---------------------------------------------------------------------
//@ PostCondition
// There is no return from this function.
//@ End-Method
//=====================================================================

void
DciString::SoftFault(const SoftFaultID softFaultID,
		const Uint32	lineNumber,
		const char *	pFileName,
		const char *	pPredicate)
{
	FaultHandler::SoftFault(softFaultID, DCI_DEVICE, DCI_STRING_CLASS,
		lineNumber, pFileName, pPredicate);
}
