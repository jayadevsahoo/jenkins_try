#ifndef FlashHardware_HH
#define FlashHardware_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Nellcor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: The class provides direct interfaces to the flash memory
//		hardware to write and read from.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/FlashHardware.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: rhj    Date:  11-April-2006    DR Number: 6192
//  Project:  XENA
//  Description:
//      Added support for the XENA Spansion Flash memory.
//      These changes only supports the new XENA GUI board not
//      the older GUI or BD boards.
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  Jan-30-96 DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "DownloadTypes.hh"
#include "DownloadError.hh"
#include "DownloadFault.hh"
#include "FlashMemCons.hh"

#define  NUM_POSSIBLE_SUB_BLOCKS 19
#define  TOTAL_POSSIBLE_NUM_SUB_BLOCKS  (NUM_POSSIBLE_SUB_BLOCKS * 2)

extern  void EnableFlashWrite(void);
extern  void DisableFlashWrite(void);

class FlashHardware
{
  public:
    FlashHardware(void);
    ~FlashHardware( void) ;	
	DownloadError burnFlashWord( Uint32 *writeAddress, const Uint32 data);
	DownloadError eraseFlashMem( Uint32 *start, Uint32 *end ) ;
    static void SoftFault( const FaultType	softFaultID,
				 				  const Uint32		lineNumber,
				 				  const char		*pFileName  = NULL, 
				 				  const char		*pPredicate = NULL);
    void BackupCheckSumTable( void) ;
    DownloadError RestoreCheckSumTable( void) ;
    DownloadError entireFlashErase( void) ;

  protected:

  private:
    FlashHardware( const FlashHardware&) ;		// not implemented...
    void operator=( const FlashHardware&) ;	// not implemented...

    //@ Data-Member:  pStart_
	//	Starting address of the system flash memory.
		Uint32 *pStart_;

    //@ Data-Member:  pEnd_
	//	Ending address of the system flash memory.
		Uint32 *pEnd_;

    //@ Data-Member:  SubBlockSizes_
    //  Sub block size.
    //  Block sizes of each blocks in Flash
    Int SubBlockSizes_[NUM_POSSIBLE_SUB_BLOCKS]; 

    //@ Data-Member:  pBlockAddr_
    //  Starting address of each memory block.
    Uint32 *pBlockAddr_[TOTAL_POSSIBLE_NUM_SUB_BLOCKS +1 ] ;

} ;


#endif // FlashHardware_HH 
