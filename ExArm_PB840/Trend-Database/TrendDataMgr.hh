#ifndef TrendDataMgr_HH
#define TrendDataMgr_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDataMgr - Main interface with the Trend database
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDataMgr.hhv   25.0.4.0   19 Nov 2013 14:34:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 002  By:  rhj    Date:  20-Jul-2007    SCR Number: 6390
//       Project:  Trend
//       Description:
//             Added a ResetDatabase method, which resets the database.
//
//  Revision: 001  By:  mjf    Date:  22-Aug-2006    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//=====================================================================

#include "MsgQueue.hh"              // OS-Foundation
#include "TimeStamp.hh"             // OS-Foundation
#include "TrendTimeScaleValue.hh"   // Setting-Validation
#include "TrendSelectValue.hh"      // Setting-Validation
#include "PatientDataId.hh"			// Patient-Data

#include "TrendEvents.hh"


// forward declare
class TrendDataSet;
class TrendValue;
class TrendDbPatientRepository;
class TrendDbSettingRepository;
class TrendDbTableBuffer;
class TrendDbApplicability;
class TrendDbEventReceiver;
class TrendDbControlBlock;


class TrendDataMgr
{
public:    

	//@ Type: DbConfigConst
	// Enum to hold the consistent values used in the database configuration
	enum DbConfigConst
	{
		// The Database version, any change in the database should update
		// this value.
		TREND_VERSION   = 1,

		// TrendDataSet values
		// The maximum number of columns (trend parameters)
		// and number of Rows (trend samples) returned in the set
		MAX_COLUMNS     = 10,
		MAX_ROWS        = 360,

		// The Compact flash layout of the database
		// The END_BLOCKS are used to make sure the database
		// Patient and Setting don't go over into unexpected blocks
		TREND_DATABASE_INFO_BLOCK_A = 1,
		TREND_DATABASE_INFO_BLOCK_B = 2,
		TREND_PATIENT_START_BLOCK   = 3,
		TREND_PATIENT_END_BLOCK     = 59999,  
		TREND_SETTING_START_BLOCK   = 60000,
		TREND_SETTING_END_BLOCK     = 60500,

		// Internal buffer sizes
		// All the tables share one read buffer, its allocated only once
		// All of the tables have there own write buffer, its allocated for each table
		TREND_READ_BUFFER_SIZE  =   91,
		TREND_WRITE_BUFFER_SIZE =   1,

		// Capacity of the Patient and Setting Databases
		// The patient database is based on the 1Hour table, other patient tables
		// are derived from the 1Hour table.
		// Satisfies requirement: [TR01140] ... data for ... 72 hours, 10 seconds ...
		// (72 Hours * 60 Minutes * 60 seconds/ 10 second frame = 25,920) 
		TREND_PATIENT_DB_CAPACITY = 25920,
		TREND_SETTING_DB_CAPACITY =   500,

		// The number of columns for the Patient tables and settings table
		// Note that the sample record (row) size can't exceed the block size (512 bytes)
		TREND_PATIENT_MAX_SLOTS = 48,
		TREND_SETTING_MAX_SLOTS = 31
	};


	//@ Type: TableId
	//  Unique Ids for each database table
	enum TableId
	{
		PATIENT_LOW_TABLE_ID = 0,
		PATIENT_1HR_TABLE_ID = TrendTimeScaleValue::TREND_TIME_SCALE_1HR,
		PATIENT_2HR_TABLE_ID = TrendTimeScaleValue::TREND_TIME_SCALE_2HR,
		PATIENT_4HR_TABLE_ID = TrendTimeScaleValue::TREND_TIME_SCALE_4HR,
		PATIENT_8HR_TABLE_ID = TrendTimeScaleValue::TREND_TIME_SCALE_8HR,
		PATIENT_12HR_TABLE_ID = TrendTimeScaleValue::TREND_TIME_SCALE_12HR,
		PATIENT_24HR_TABLE_ID = TrendTimeScaleValue::TREND_TIME_SCALE_24HR,
		PATIENT_48HR_TABLE_ID = TrendTimeScaleValue::TREND_TIME_SCALE_48HR,
		PATIENT_72HR_TABLE_ID = TrendTimeScaleValue::TREND_TIME_SCALE_72HR,
		PATIENT_HIGH_TABLE_ID = PATIENT_72HR_TABLE_ID,

		SETTING_LOW_TABLE_ID = PATIENT_HIGH_TABLE_ID + 1,
		SETTING_TABLE_ID = SETTING_LOW_TABLE_ID,
		SETTING_HIGH_TABLE_ID = SETTING_TABLE_ID,

		TOTAL_TABLE_ID_VALUES = SETTING_HIGH_TABLE_ID + 1
	};


	//@ Type: TrendId
	// The first Trend Ids come from the TrendSelectValue enum
	// Events are not selectable and have no entry in TrendSelectValues
	// create an Id which will be unique from TrendSelectValues
	enum TrendId
	{
		SAMPLE_USER_EVENTS_ID = TrendSelectValue::TOTAL_TREND_SELECT_VALUES + 1,
		SAMPLE_AUTO_EVENTS_ID = SAMPLE_USER_EVENTS_ID + 1,

		MAX_TREND_ID
	};


	//@ Type: RequestStatus
	// Enum returned from the Request Data call
	enum RequestStatus
	{
		// request has been put on the queue to process
		REQUEST_ACCEPTED,

		// manager is busy processing another request
		MANAGER_BUSY,

		// manager is not enabled
		MANAGER_NOT_ENABLED                    
	};


	// --------------------------------------------------------------
	// Methods called from the clients (Gui App)

	static TrendDataMgr & GetTrendDataMgr( void );
	static RequestStatus RequestData( TrendDataSet & trendDataSet );
	static void PostEvent( TrendEvents::EventId event );
	static void PostEventValue( TrendEvents::EventId event, const TrendValue & value );
	static Boolean IsEnabled( void );
	static Boolean IsInitialized( void );
	static void PatientSetupCompleted( Boolean isSamePatient );
	static Uint32 GetTableCapacity( TrendTimeScaleValue::TrendTimeScaleValueId timescale );
	static Uint16 GetTableFrequency( TrendTimeScaleValue::TrendTimeScaleValueId timescale );
	static Uint32 GetEndOfDataPosition( void );
	static void ResetDatabase(void);



	// --------------------------------------------------------------
	// Methods called from the other trend agents

	static void Initialize( void );
	static void Disable( void );
	static void SystemContextChange( void );
	static const TrendDbPatientRepository & GetPatientRepository( void );
	static const TrendDbSettingRepository & GetSettingRepository( void );
	static const TrendDbApplicability & GetApplicability( void );
	static const TrendDbEventReceiver & GetEventReceiver( void );
	static const TrendDbControlBlock & GetControlBlock( void );
	static MemPtr GetReadBuffer( void );
	static void Task( void );

	static void ChangeHappened( const PatientDataId::PatientItemId dataId );

	//@ Data-Member: DebugMask_
	// Used to control amount of debug output
	static Uint8 DebugMask;

	//@ Data-Member: IsLockDatabase_
	// Used when with the development key to lock the database
	static Boolean IsLockDatabase;


private:

	// Allow unrestricted access from the Test Agent
	friend class TrendDbTestDataAgent;

	// private static construction from GetTrendDataMgr()
	TrendDataMgr();
	~TrendDataMgr();

	// no copy or assignment constructor
	TrendDataMgr( const TrendDataMgr& );		// not implemented
	void operator=( const TrendDataMgr& );		// not implemented


	//@ Type: InitState
	// Value used to sync the initialization and startup code
	enum InitState
	{
		// Wait for Sys-Init to init the task
		NON_INITIALIZED,

		// Wait for the init of the database
		INITIALIZATION_PENDING,

		// Server is initialized and ready
		INITIALIZED_IDLE,

		// Process request, wait for patient
		SAMPLE_IDLE,

		// Server is collecting trend data
		SAMPLE_RUNNING,

		// The Trend option is not installed
		NO_TREND_OPTION,

		// Server is in error, set from Disable()
		INIT_STATE_ERROR                       
	};

	void initRepositories_( void ); 
	TrendDbPatientRepository & getPatientRepository_( void );
	TrendDbSettingRepository & getSettingRepository_( void );
	TrendDbApplicability & getApplicability_( void );
	TrendDbEventReceiver & getEventReceiver_( void );
	TrendDbControlBlock & getControlBlock_( void );
	inline void setState_( InitState state );
	inline Boolean isState_( InitState state );
	inline Int32 getCurrentSequenceCount_( void );
	Int32 readRequest_( TrendDataSet* & pTrendDataSet, Uint16 timeout );
	void processRequest_( TrendDataSet* pTrendDataSet, Uint32 currentSeq );
	void sampleTrend_( Uint32 seqNumber, const TimeStamp & trendSampleTime );
	void signalGuiDataReady_( Uint32 dataSetAddr );
	void signalGuiManagerStatus_( void );
	Uint16 getTableFrequency_( TrendDataMgr::TableId tableId );
	Uint32 getTableCapacity_( TrendDataMgr::TableId tableId );
	void initializeSequenceCount_( void );
	void setNextSequenceCount_( void );


	//@ Data-Member: initState_
	// Initialization/Running flag
	InitState initState_;

	//@ Data-Member: readRequestQ_
	//  The request input Queue
	MsgQueue   readRequestQ_;

	//@ Data-Member: frameSeqNumber_
	//  Frame sequence count for the database
	Int32 frameSeqNumber_;

	//@ Data-Member: isInitRequestDatabase_
	//  Request that the database should be initialized
	Boolean isInitRequestDatabase_;

	//@ Data-Member: isResetRequestDatabase_
	//  Request that the Trend data be cleared and reset to empty
	//  Will only have effect if the TrendDataMgr is not running
	Boolean isResetRequestDatabase_;

	//@ Data-Member: isSystemContextChange_
	//  Boolean used to mark a change in system context
	Boolean isSystemContextChange_;

	//@ Data-Member: isDataRequestInProcess_
	//  Semaphore used to only allow one request at a time
	//  This is set True before the TrendDataSet is placed on the input queue
	//  and cleared after the request has been processed and before the Gui
	//  is signaled that the data is ready.  This is to allow another request
	//  to come in right after the signal and not to be rejected as busy.
	Boolean isDataRequestInProcess_;
};


// Inlined methods
#include "TrendDataMgr.in"


#endif // TrendDataMgr_HH
