#ifndef  BootFile_HH
#define	BootFile_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@  Filename: BootFile.hh - Provides I/O interfaces to a file that
//    contains a boot image.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/BootFile.hhv   25.0.4.0   19 Nov 2013 14:02:46   pvcs  $ 
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  <jhv>    Date:  05-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

#include "DownloadTypes.hh"
#include "DownloadError.hh"
#include "DownloadCons.hh"
#include "DownloadFault.hh"
#include <stdio.h>

enum BootFileCons
{
	MAX_PATH_ENTRIES = 30,
#if	defined(SIGMA_SUN)
	PATH_SEP_CHAR =	':',
	DIR_SEP_CHAR  = '/',
#elif defined(SIGMA_PC)
	PATH_SEP_CHAR =	';',
	DIR_SEP_CHAR  = '\\',
#endif //defined(SIGMA_PC)
	MAX_PATH_MEMORY= 1024
};

enum OpenMode 
{
	READ,
	WRITE,
	CREATE
};

class BootFile 
{
  public:
    BootFile();
    ~BootFile(void);
	static SigmaStatus InitLoadPath(void);
	SigmaStatus open(char *filename );
	SigmaStatus creat(char *filename );
	inline Boolean opened();
	inline char *getFileName();
	inline Int getFileSize();
	inline Int getCurrentBlockNum();
	Int read(Byte *pBuf, Int size);
	Int write(Byte *pBuf, Int size);
	void  close(void);

    static void SoftFault(const FaultType softFaultID,
		const Uint32      lineNumber,
		const char*       pFileName  = NULL, 
		const char*       pPredicate = NULL);

  protected:

  private:
    BootFile(const BootFile&);			// not implemented...
    void   operator=(const BootFile&);	// not implemented...

    //@ Data-Member:  LoadPath_
    // boot file load path search list
		static char	**LoadPath_ ;

	//@Data-Member: fileId_
	// file descriptor to the Sigma image file.
		FILE  *fileId_;

	//@Data-Member: fileName_
	// file descriptor to the Sigma image file.
		char fileName_[MAX_FILE_NAME_LENGTH];

	//@Data-Member: offSet_
	// current offset to this boot file where the next read data is.
		Int offSet_;

	//@Data-Member: fileSize_
	// size of this Boot file.
		Int fileSize_;

};

// Inlined methods...
#include "BootFile.in"

#endif // BootFile_HH 
