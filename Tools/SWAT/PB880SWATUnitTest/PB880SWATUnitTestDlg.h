
// PB880SWATUnitTestDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"


#define	WM_LUASCRIPT	WM_APP + 0x10
#define WM_WRITELOG		WM_APP + 0x11
#define WM_TOUCHED_POS  WM_APP + 0x12

#define	LUA_BEGIN		TRUE
#define LUA_END			FALSE



// CPB880SWATUnitTestDlg dialog
class CPB880SWATUnitTestDlg : public CDialog
{
// Construction
public:
	CPB880SWATUnitTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_PB880SWATUnitTest_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	void	WriteLog( const TCHAR* pLogMsg );

// Implementation
protected:
	HICON m_hIcon;

	BOOL	InitializeControls();
	static UINT TestScriptExecThreadProc( LPVOID lParam );
    static UINT TouchedPositionMonitorThreadProc( LPVOID lParam );

	LRESULT OnLuaScript(WPARAM wParam, LPARAM lParam);
	LRESULT OnLogMessage(WPARAM wParam, LPARAM lParam);
    LRESULT OnTouchedPos(WPARAM wParam, LPARAM lParam);
	
	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	// script file edit box
	CEdit m_editScriptFile;
	// script file name field
	CString m_strScriptFile;


	afx_msg void OnBnClickedOk();

	// list control for logs
	CListCtrl m_listLogs;
};

/*
typedef void (CPB880SWATUnitTestDlg::*PMFN_HOSTLOG)( CString* pLogStr );

class HostLogFunction
{
private:
	CPB880SWATUnitTestDlg*	pHostDlg_;
	PMFN_HOSTLOG				pLogFunc_;

public:
	HostLogFunction( CPB880SWATUnitTestDlg* pDlg, PMFN_HOSTLOG pLogFunc );

	void operator() ( CString* pLogStr );
};

struct lua_State;
class ScriptInterpreter
{
private:
	lua_State* L_;
	HostLogFunction logFunc_;

	int SetLuaPath( lua_State* L, const char* path );
	int host_log( lua_State* L );

public:

	ScriptInterpreter(const HostLogFunction& logFunc);
	~ScriptInterpreter(void);

	bool RunLuaFile(const TCHAR* fileName, CString& strMsg);
};
*/