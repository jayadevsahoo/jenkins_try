#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhalationValve - Exhalation valve driver for command and
//         feedback.
//---------------------------------------------------------------------
//@ Interface-Description
//      Provide interface to update the exhalation valve and to read
//      the valve current.  There are 2 modes of operation of
//      exhalation valve: SLOWEST and NORMAL.  The mode of
//      operation is determined by the controllers.  Methods are implemented
//		to output a command to the exhalation valve, to set the
//		ExhValveGainEntryTable, and to get the valve current applied to
//		the valve, to point to the calibration table, to get the command
//		to the valve, to open the valve, and to get the pressure command given
//		the current.  The later method provides support for monitoring
//		the integrity of the exhalation valve applied current as compared
//		to the desired command level.  An inverted table (pressure vs. current)
//		is stored based on the exhalation valve characteristics (current vs.
//		pressure).
//
//		This class provides support for the BD_CPU and the GUI_CPU.
//---------------------------------------------------------------------
//@ Rationale
//      Abstraction of exhalation valve.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The pressure command and damping mode are passed to the method
//      updateValve and converted into a valve command and damping
//      gain.  The two commands are then output to the DAC.  The method
//      getExhValveCurrent returns the measured current for the
//      exhalation valve.  The pointer to the calibration table will point
//		to either a ram address or flash.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ExhalationValve.ccv   25.0.4.0   19 Nov 2013 13:54:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009 By: sah    Date: 13-Jan-1999   DR Number: 5321, 5322
//  Project:  ATC
//	Description:
//		ATC initial release.
//		Added SLOWEST.  Delete SLOW, QUICK.
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 008  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 007 By: syw    Date: 02-Jan-1997   DR Number: DCS 1652
//  	Project:  Sigma (R8027)
//		Description:
//			Added cmdTarget < INVERSE_TABLE_SIZE condition to prevent infinite loop if
//			minCmd < 0.
//
//  Revision: 006 By: syw    Date: 19-Sep-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Limit the index into DampingGainTable to PRESSURE_RANGE.
//
//  Revision: 005 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Change invertedTable_ to accept current counts as an input instead
//			of DAC counts.  Added CPU specific access.
//
//  Revision: 004 By: syw    Date: 07-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Initialize invertedTable_ during construction.  Added pre-processor
//			to generate CPU	specific code.
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Changed value for TEMPERATURE_COEFFICIENT.  Added NORMALIZED_TEMP.
//			Changed rExhValveTable_ to pExhValveTable.  Removed
//			setExhValveGainEntryTable() method.  Round index to nearest integer
//			and obtain normalized command to compute compensated command in
//			updateValve() method since table comntains normalized temperature
//			commands.  Fix dampinGain for SLOW case.  Added deenergize() method.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================
#include "Sigma.hh"
#include "ExhalationValve.hh"

// TODO E600_LL: the 2 includes below are used for debug only;
// should be removed in the future
#include "BreathPhase.hh"

//@ Usage-Classes

// TODO E600_LL: Need to revisit the use of ExhValveTable.hh
#include "ExhValveTable.hh"

#if defined (SIGMA_BD_CPU)

#include "TemperatureSensor.hh"
#include "UtilityFunctions.h"
#include "CalInfoRefs.hh"
#include "CalInfoFlashBlock.hh"


#endif  // defined(SIGMA_BD_CPU)

//@ End-Usage

//@ Code...

#if defined (SIGMA_BD_CPU)

const Real32    TEMPERATURE_COEFFICIENT = 0.00094 ;
const Real32 	NORMALIZED_TEMP = 23.0 ;

#endif  // defined(SIGMA_BD_CPU)

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhalationValve()
//
//@ Interface-Description
//      Constructor.  It takes three arguments, exhalation valve
//      temperature sensor, exhalation valve calibration table, the valve
//		current sensor for the BD_CPU.  The GUI_CPU has only the exhalation
//		valve calibration table as arguments.  These values are used to
//		initialize the object.
//---------------------------------------------------------------------
//@ Implementation-Description
//      BD_CPU implementation:
//		The private data members, rExhValveTemperatureSensor_,
//      rExhValveTable_, and rExhValveCurrentSensor_ are initialized
//		with the arguments passed in.  pDampingGainTable_ is also
//	    initialized.
//
//      GUI_CPU implementation:
//		The ExhValveTable_ stores current counts vs. pressure.  The current
//		counts are converted to current in mAmps by using the gain and offset
//		values obtained	during EST.  The invertedTable_ array is determined by
//		traversing the ExhValveTable_ until the cmdTarget is encountered.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

ExhalationValve::ExhalationValve(ExhValveTable *pExhValveTable) :
        Psol(Psol::EXH_PSOL, 0), pExhValveTable_(pExhValveTable)                    // $[TI1.2]
{
	CALL_TRACE("ExhalationValve::ExhalationValve( \
				ExhValveTable *pExhValveTable)") ;

	const Uint32 CAL_ARRAY_SIZE = ExhValveTable::EXH_VALVE_DATA_ARRAY_SIZE * sizeof( Real32 );

	memcpy(CalValveTable.highToLowData.exhPressDrvSamples,		pExhValveTable->getPdrvTable(), CAL_ARRAY_SIZE);
	memcpy(CalValveTable.highToLowData.exhPressSamples,			pExhValveTable->getP2Table(),   CAL_ARRAY_SIZE);
	memcpy(CalValveTable.highToLowData.dacSamples,				pExhValveTable->getDacTable(),  CAL_ARRAY_SIZE);

	memcpy(CalValveTable.lowToHighData.exhPressDrvSamples,		pExhValveTable->getPdrvTable_l2h(), CAL_ARRAY_SIZE);
	memcpy(CalValveTable.lowToHighData.exhPressSamples,			pExhValveTable->getP2Table_l2h(),   CAL_ARRAY_SIZE);
	memcpy(CalValveTable.lowToHighData.dacSamples,				pExhValveTable->getDacTable_l2h(),  CAL_ARRAY_SIZE);
}

#else

ExhalationValve::ExhalationValve( ExhValveTable *pExhValveTable)
 	: Psol(Psol::EXH_PSOL, 0), pExhValveTable_(pExhValveTable)		// $[TI3]
{
	CALL_TRACE("ExhalationValve::ExhalationValve( ExhValveTable *pExhValveTable)") ;

	Uint16 pressIndex = 0 ;
	Uint16 cmdTarget = 0 ;
}

#endif  // defined(SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExhalationValve()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

ExhalationValve::~ExhalationValve( void)
{
	CALL_TRACE("ExhalationValve::~ExhalationValve( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValve()
//
//@ Interface-Description
//      This method takes two arguments, pressure command and damping mode.
//      The arguments are used to determine the valve command and damping
//      gain sent to the exhalation valve's DAC output.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The pressure command is compensated for temperature.  The damping
//      gain is determined from the pressure command and the mode. The
//      compensated command and damping gain are then outputed to the DAC.
//      All conversions from real to int are truncated, not rounded.
// $[04274]
//	$[TC04029]
//---------------------------------------------------------------------
//@ PreCondition
//      The damping mode must be SLOWEST or NORMAL.
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
#if defined (SIGMA_BD_CPU)
void
ExhalationValve::updateValve( Real32 pressureCommand , const DampingMode mode)
{
	CALL_TRACE("ExhalationValve::updateValve( Real32 pressureCommand, \
				const DampingMode mode)") ;
	UNUSED_SYMBOL(mode);	

	Real32 PdrvDac;
	Real32 Pdrive;

	LookUpFloat(&Pdrive, CalValveTable.highToLowData.exhPressDrvSamples, 
		pressureCommand, CalValveTable.highToLowData.exhPressSamples, 
		ExhValveTable::EXH_VALVE_DATA_ARRAY_SIZE-1);

	LookUpFloat(&PdrvDac, CalValveTable.highToLowData.dacSamples, 
		Pdrive, CalValveTable.highToLowData.exhPressDrvSamples, 
		ExhValveTable::EXH_VALVE_DATA_ARRAY_SIZE - 1);

    Psol::updatePsol((Uint16)PdrvDac);
	//TODO E600_LL:  changed to if statement for easy break during insp. or exh for development
	// will eliminate at some time in the future
	if (BreathPhase::GetCurrentBreathPhase()->getPhaseType() == BreathPhaseType::EXHALATION)
	{
		DEBUGMSG(0, (L"EXP"));
	}
	else
	{
		DEBUGMSG(0, (L"INSP"));
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValveL2h()
//
//@ Interface-Description
//      This method takes two arguments, pressure command and damping mode.
//      The arguments are used to determine the valve command and damping
//      gain sent to the exhalation valve's DAC output.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Output DAC value to exhalation valve based on pressure command.
// $[04274]
//	$[TC04029]
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void
ExhalationValve::updateValveL2h( Real32 pressureCommand , const DampingMode mode)
{
	CALL_TRACE("ExhalationValve::updateValve( Real32 pressureCommand, \
				const DampingMode mode)") ;

	float PdrvDac2;
	float Pdrive2;

	LookUpFloat(&Pdrive2, CalValveTable.lowToHighData.exhPressDrvSamples, 
		pressureCommand, CalValveTable.lowToHighData.exhPressSamples, 
		ExhValveTable::EXH_VALVE_DATA_ARRAY_SIZE - 1);

	LookUpFloat(&PdrvDac2, CalValveTable.lowToHighData.dacSamples, 
		Pdrive2, CalValveTable.lowToHighData.exhPressDrvSamples, 
		ExhValveTable::EXH_VALVE_DATA_ARRAY_SIZE - 1);

    Psol::updatePsol((Uint16)PdrvDac2);
	//TODO E600_LL:  changed to if statement for easy break during insp. or exh for development
	// will eliminate at some time in the future
	if (BreathPhase::GetCurrentBreathPhase()->getPhaseType() == BreathPhaseType::EXHALATION)
	{
		DEBUGMSG(0, (L"EXP"));
	}
	else
	{
		DEBUGMSG(0, (L"INSP"));
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPdrvFromPexh()
//
//@ Interface-Description
//		Helper API to look up, in calibration table, the drive pressure
//		value that corresponds to an exh pressure sensor value
//---------------------------------------------------------------------
//@ Implementation-Description
//		The exh pressure value is provided in the argument
//---------------------------------------------------------------------
//@ PreCondition
//      none.
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Real32 ExhalationValve::getPdrvFromPexh(Real32 Pexp)
{
	Real32 Pdrive;
	LookUpFloat(&Pdrive, CalValveTable.highToLowData.exhPressDrvSamples, 
		Pexp, CalValveTable.highToLowData.exhPressSamples, 
		ExhValveTable::EXH_VALVE_DATA_ARRAY_SIZE - 1);
	return Pdrive;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPdrvFromUdac()
//
//@ Interface-Description
//		Helper API to look up, in calibration table, the drive pressure
//		value that corresponds to a peep-controller feed-forward DAC input
//---------------------------------------------------------------------
//@ Implementation-Description
//		The peep-controller feed-forward input is provided in the argument
//---------------------------------------------------------------------
//@ PreCondition
//      none.
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Real32 ExhalationValve::getPdrvFromUdac(Real32 Udac)
{
	Real32 Pdrive;
	LookUpFloat(&Pdrive, CalValveTable.highToLowData.exhPressDrvSamples, 
		Udac, CalValveTable.highToLowData.dacSamples, 
		ExhValveTable::EXH_VALVE_DATA_ARRAY_SIZE - 1);
	return Pdrive;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getUdacFromPdrv()
//
//@ Interface-Description
//		Helper API to look up, in calibration table, the peep-controller
//		feed-forward DAC value that corresponds to a drive pressure value
//---------------------------------------------------------------------
//@ Implementation-Description
//		The drive pressure value is provided in the argument   
//---------------------------------------------------------------------
//@ PreCondition
//      none.
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Real32 ExhalationValve::getUdacFromPdrv(Real32 Pdrv)
{
	Real32 Udac;
	LookUpFloat(&Udac, CalValveTable.highToLowData.dacSamples, 
		Pdrv, CalValveTable.highToLowData.exhPressDrvSamples,  
		ExhValveTable::EXH_VALVE_DATA_ARRAY_SIZE - 1);
	return Udac;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: commandValve()
//
//@ Interface-Description
//		API tocommand the exh valve Psol by a DAC input value
//---------------------------------------------------------------------
//@ Implementation-Description
//    
//---------------------------------------------------------------------
//@ PreCondition
//      none.
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void ExhalationValve::commandValve(Uint32 Udac)
{
	Psol::updatePsol((Uint16)Udac);
}

//*****************************************************************************
//
//  Used by the calibration process to update the cal data
//
//*****************************************************************************

void ExhalationValve::UpdateCalData(ExhValveTable::ExhValveCalTable &exhValveCalTable)
{

	// Update in persistent storage
    RCalInfoFlashBlock.SaveExhValveData( exhValveCalTable );

}

#endif  // defined(SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deenergize()
//
//@ Interface-Description
//      This method takes no arguments and returns nothing.  This method is
//		called to output 0 current to the valve and 0 damping.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Write 0 to the exhalation valve DAC port and damping gain DAC port.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

void
ExhalationValve::deenergize( void)
{
	CALL_TRACE("ExhalationValve::deenergize( void)") ;
	Psol::updatePsol(0);
}

#endif  // defined(SIGMA_BD_CPU)

#if defined(SIGMA_GUI_CPU)

#ifdef SIGMA_DEBUG

void ExhalationValve::dumpInvertedTable( void)
{
	for (Uint16 ii=0; ii < INVERSE_TABLE_SIZE; ii++)
	{
		if (!(ii % 10))
			printf( "\n%4d  ", ii) ;
		printf( "%5.1f ", getInvertedTableEntry(ii)) ;
	}
}

#endif //SIGMA_DEBUG

#endif  // defined(SIGMA_GUI_CPU)



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ExhalationValve::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;

    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, EXHALATIONVALVE, lineNumber,
                             pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
