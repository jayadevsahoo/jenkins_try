#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  AtmPresXducerCalibration - calibrate the atmospheric
//          pressure transducer.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is used to calibrate the atmospheric pressure transducer
//	against an external sensor.
//---------------------------------------------------------------------
//@ Rationale
//	This class is required to perform periodic calibrations of the
//	atmospheric pressure transducer relative to an external measurement
//	device.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method readXducer() is used to read the atmospheric pressure
//	transducer, either as an uncorrected or corrected reading depending
//	on the parameter string sent with the test ID when test is invoked.  
//	The method runCalibration() is used to perform the calibration, i.e.
//	measure and store the pressure correction offset.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/AtmPresXducerCalibration.ccv   25.0.4.0   19 Nov 2013 14:23:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 16-Dec-1997    DR Number: DCS 2684
//       Project:  Sigma (840)
//       Description:
//			Mapped SRS requirement 08063.
//
//  Revision: 001  By:  quf    Date: 24-Sep-1997    DR Number: DCS 2410
//       Project:  Sigma (840)
//       Description:
//		 	Initial version (Integration baseline).
//
//=====================================================================

#ifdef SM_TEST
# define private public
# define protected public
# include <ctype.h>
#endif  // SM_TEST

#include "AtmPresXducerCalibration.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmConstants.hh"
#include "SmManager.hh"
#include "ServiceModeNovramData.hh"

#include "MiscSensorRefs.hh"
#include "LinearSensor.hh"
#include "Barometer_MPL115A2.h"
#include "AtmPressOffset.hh"

#include "NovRamManager.hh"

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: AtmPresXducerCalibration()  [Default Constructor]
//
//@ Interface-Description
//  Constructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================
AtmPresXducerCalibration::AtmPresXducerCalibration(void)
{
	CALL_TRACE("AtmPresXducerCalibration::AtmPresXducerCalibration(void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~AtmPresXducerCalibration
//
//@ Interface-Description
//	Destructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
AtmPresXducerCalibration::~AtmPresXducerCalibration(void)
{
	CALL_TRACE("AtmPresXducerCalibration::~AtmPresXducerCalibration(void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: readXducer
//
//@ Interface-Description
//	This method reads the atmospheric pressure transducer and sends
//	either the corrected or uncorrected reading to the GUI via
//	Service-Data.  One argument: pointer to parameter string.
//	No return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	$[08063]
//	If the input parameter string (stored in SmManager) is null, send
//	the corrected atm pressure xducer value to Service-Data, else send
//	the uncorrected value.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
void
AtmPresXducerCalibration::readXducer( const char * const pParameters )
{
	CALL_TRACE("AtmPresXducerCalibration::readXducer(void)");

	// read corrected atm pressure value and offset
	const Real32 CORRECTED_PRESSURE_CMH2O =
									RAtmosphericPressureSensor.getValue();
	const Real32 OFFSET_CMH2O = RAtmosphericPressureSensor.getOffset();

	// calculate the corrected and uncorrected pressures in mmHg
	const Real32 CORRECTED_PRESSURE_MMHG =
							CORRECTED_PRESSURE_CMH2O / CMH2O_PER_MMHG;
	const Real32 UNCORRECTED_PRESSURE_MMHG =
			(CORRECTED_PRESSURE_CMH2O - OFFSET_CMH2O) / CMH2O_PER_MMHG;

	if (strlen( pParameters ) == 0)
	{
	// $[TI1]
		// null parameter string: send corrected reading in mmHg
		RSmManager.sendTestPointToServiceData( CORRECTED_PRESSURE_MMHG);
	}
	else
	{
	// $[TI2]
		// non-null parameter string: send uncorrected reading in mmHg
		RSmManager.sendTestPointToServiceData( UNCORRECTED_PRESSURE_MMHG);
	}
	// end else
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runCalibration
//
//@ Interface-Description
//	This method performs the atmospheric pressure transducer calibration.
//	One argument: pointer to parameter string.  No return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	$[07074]
//	Read the input (externally-measured) atmospheric pressure value.
//	Read the uncompensated atmospheric pressure transducer value.
//	Calculate the offset.
//	If the offset is out of range, generate a FAILURE and store a
//	default offset of 0 in RAM and novram.
//	Else if offset is in range, store the offset in RAM and novram.
//
//	NOTE: It is the responsibility of the external pressure input source
//	(i.e. the GUI or the Service laptop) to ensure that the externally-
//	measured pressure value is within a valid range.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
void
AtmPresXducerCalibration::runCalibration( const char * const pParameters )
{
	CALL_TRACE("AtmPresXducerCalibration::runCalibration(void)");

	// get the externally-measured pressure
	Real32 externalAtmPressureMmHg;
	const Int32 RTN_VAL = sscanf(pParameters, "%f", &externalAtmPressureMmHg);

	// make sure we didn't get a null parameter string for the external
	// pressure input value
	CLASS_ASSERTION( RTN_VAL != EOF);

	// read the stored offset
	const Real32 OLD_OFFSET_CMH2O = RAtmosphericPressureSensor.getOffset();

	// read the atm pressure transducer uncorrected value
	const Real32 INTERNAL_ATM_PRESSURE_MMHG =
			(RAtmosphericPressureSensor.getValue() - OLD_OFFSET_CMH2O) /
														CMH2O_PER_MMHG ;

	// calculate new offset
	const Real32 NEW_OFFSET_MMHG =
				externalAtmPressureMmHg - INTERNAL_ATM_PRESSURE_MMHG;

	// check offset before storing
	Real32 newOffsetCmH2O;
	if ( ABS_VALUE(NEW_OFFSET_MMHG) > ATM_PRESSURE_ERROR_MMHG )
	{
	// $[TI1]
		// offset OOR
		RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_1);

		// if offset is OOR, set it to 0
		newOffsetCmH2O = 0;

		// set novram status flag to FAILED  -- displays status of
		// "not calibrated" message on the GUI
		ServiceModeNovramData::SetAtmPresXducerCalStatus(
										ServiceMode::FAILED);
	}
	else
	{
	// $[TI2]
		// if offset is in range, convert it to cmH2O before storing it
		newOffsetCmH2O = NEW_OFFSET_MMHG * CMH2O_PER_MMHG;

		// set novram status flag to PASSED -- displays status of
		// "calibrated" on the GUI
		ServiceModeNovramData::SetAtmPresXducerCalStatus(
										ServiceMode::PASSED);
	}
	// end else

	// store the new offset in novram and RAM
	AtmPressOffset atmPressOffset;
	atmPressOffset.setAtmPressOffset(newOffsetCmH2O);
	NovRamManager::UpdateAtmPressOffset(atmPressOffset);
	RAtmosphericPressureSensor.setOffset(newOffsetCmH2O);

	// send the corrected atm pressure xducer value using the new offset
	const Real32 CORRECTED_PRESSURE_MMHG = INTERNAL_ATM_PRESSURE_MMHG + 	
											newOffsetCmH2O / CMH2O_PER_MMHG;
	RSmManager.sendTestPointToServiceData(CORRECTED_PRESSURE_MMHG);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
AtmPresXducerCalibration::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("AtmPresXducerCalibration::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, ATMPRESXDUCERCALIBRATION,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
