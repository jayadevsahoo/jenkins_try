#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SummationBuffer - class to sum data in a buffer.
//---------------------------------------------------------------------
//@ Interface-Description
//	Methods are provided to manage the data in the buffers (reset, determine
//	if the buffer is full, and to put data in the buffer).  Methods are also
//	provided to obtain the sum, average, and the number of data points
//	in the buffer.
//---------------------------------------------------------------------
//@ Rationale
//	To encapsulate the summation of data.
//---------------------------------------------------------------------
//@ Implementation-Description
//	A circular buffer is used to store the data.  The number of data items
//	put into the buffer to be summed is maintained until the buffer is full.
//	The sum of the buffer is maintained as data is put into the buffer.
//	 $[BL04055] $[BL04056] $[BL04057]
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/SummationBuffer.ccv   10.7   08/17/07 09:43:40   pvcs  
//
//@ Modification-Log
//
//  Revision: 004  By: rhj     Date:  27-Oct-2010    SCR Number: 6694
//  Project:  PROX
//  Description:
//       Added getCurrentAverage() and getCurrentSum()
// 
//  Revision: 003  By: rpr     Date:  25-Oct-2010    SCR Number: 6694
//  Project:  PROX
//  Description:
//		When two floating point numbers that are close in value are subtracted 
//		from each other errors can occur.  Avoided this in updateBuffer.
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  05-Mar-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "SummationBuffer.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SummationBuffer()  
//
//@ Interface-Description
//		Constructor.  This method has a pointer to the buffer and the buffer
//		size as arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Initialize the data members.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

SummationBuffer::SummationBuffer( Real32 *pBuffer, Uint32 bufferSize)
: pBuffer_( pBuffer), bufferSize_( bufferSize)
{
	CALL_TRACE("SummationBuffer::SummationBuffer( Real32 *pBuffer, Uint32 bufferSize)") ;

	// $[TI1]

	index_ = 0 ;
	sum_ = 0.0 ;
	isBufferFull_ = FALSE ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SummationBuffer()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

SummationBuffer::~SummationBuffer(void)
{
	CALL_TRACE("SummationBuffer::~SummationBuffer(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAverage
//
//@ Interface-Description
//		This method has no arguments and returns the average of the data in the
//		buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Return the computed the average.  The average is based on the
//		number of valid data in the buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
SummationBuffer::getAverage( void) const
{
	CALL_TRACE("SummationBuffer::getAverage( void)") ;
	
	Real32 average = 0.0 ;

	if (isBufferFull_)
	{
		// $[TI1]
		average = sum_ / bufferSize_ ;
	}
	else
	{
		// $[TI2]
		
		if (index_ != 0)
		{
			// $[TI3]
			average = sum_ / index_ ;
		}	// implied else $[TI4]
	}

	return( average) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateBuffer()
//
//@ Interface-Description
//		This method has the data to be put in the buffer as an argument and
//		returns nothing. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		The data is added to the cumulative sum.  If the buffer is full,
//		the old data is removed from the sum.  The data is stored where the
//		old data resided.  Care is taken so that subtraction of the 
// 		two floating point numbers are not close to being equal to each other.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
SummationBuffer::updateBuffer( const Real32 data)
{
	CALL_TRACE("SummationBuffer::updateBuffer( const Real32 data)") ;

	if (isBufferFull_ == TRUE)
	{
		// $[TI1]
		sum_ = data + (sum_ - pBuffer_[index_]) ;
	}
	else
	{
		// $[TI2]
		sum_ += data ;
	}

	pBuffer_[index_] = data ;
	index_++ ;
		
	if (index_ >= bufferSize_)
	{
		// $[TI3]
		isBufferFull_ = TRUE ;
		index_ = 0 ;
	}	// implied else $[TI4]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SummationBuffer::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, SUMMATIONBUFFER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCurrentAverage
//
//@ Interface-Description
//		This method has no arguments and returns the current average of 
//      the data in the buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Return the computed the average.  The average is based on the
//		number of valid data in the buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
SummationBuffer::getCurrentAverage( void) 
{
	CALL_TRACE("SummationBuffer::getCurrentAverage( void)") ;

	Real32 average = 0.0f;

	if (isBufferFull_)
    {
		average = getCurrentSum() / bufferSize_ ;
	}
	else
	{
		
		if (index_ != 0)
		{
			average = getCurrentSum() / index_;
		}	
	}

	return( average) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCurrentSum
//
//@ Interface-Description
//		This method has no arguments and returns the current sum of 
//      the data in the buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Returns the computed sum.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32
SummationBuffer::getCurrentSum( void) 
{
	CALL_TRACE("SummationBuffer::getCurrentSum( void)") ;
	
    Real32 sum = 0.0f;

	if (isBufferFull_)
	{
        for (Uint8 index = 0; index < bufferSize_; index++)
        {
            sum += 	pBuffer_[index];
        }
	}
	else
	{
		
		if (index_ != 0)
		{
            for (Uint8 index = 0; index < index_; index++)
            {
                sum += 	pBuffer_[index];
            }
		}	
	}

	return( sum) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
