#ifndef VitalCapacitySubScreen_HH
#define VitalCapacitySubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VitalCapacitySubScreen - Subscreen for activating or 
//                                  deactivating a Vital Capacity 
//                                  Maneuver.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VitalCapacitySubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: rhj    Date: 10-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Added a message indicating a leak has been detected.
//
//  Revision: 002  By:  rhj	   Date:  08-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:     
//      Added a three min. time out for automatically accepting
//      a VC maneuver.
//
//  Revision: 001   By: rhj   Date:  10-May-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//       RESPM Project Initial Version
//
//====================================================================

#include "SubScreen.hh"

//@ Usage-Classes
#include "SubScreenTitleArea.hh"
#include "GuiAppClassIds.hh"
#include "TextButton.hh"
#include "BdEventTarget.hh"
#include "EventData.hh"
#include "AlarmOffKeyPanel.hh"
#include "Line.hh"
#include "SubScreenTitleArea.hh"
#include "TextField.hh"
#include "NumericField.hh"
#include "TouchableText.hh"
#include "PatientDataTarget.hh"
#include "TimeStamp.hh"
#include "GuiTimerTarget.hh"
#include "GuiTimerRegistrar.hh"

//@ End-Usage

class VitalCapacitySubScreen : 
    public SubScreen, 
    public BdEventTarget,
    public PatientDataTarget,
    public GuiTimerTarget
{
public:
    VitalCapacitySubScreen(SubScreenArea *pSubScreenArea);
    ~VitalCapacitySubScreen(void);

    // Overload SubScreen methods
    virtual void activate(void);
    virtual void deactivate(void);

    // BdEventTarget virtual method
    virtual void bdEventHappened(EventData::EventId eventId,
                                EventData::EventStatus eventStatus,
                                EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);


    // PatientDataTarget virtual method
    virtual void patientDataChangeHappened(
                                PatientDataId::PatientItemId patientDataId);

    // ButtonTarget virtual method
    virtual void buttonDownHappened(Button *pButton,
                                    Boolean byOperatorAction);
    virtual void buttonUpHappened(Button *pButton,
                                    Boolean byOperatorAction);

    static void SoftFault(const SoftFaultID softFaultID,
                          const Uint32      lineNumber,
                          const char*       pFileName  = NULL, 
                          const char*       pPredicate = NULL);

    // GuiTimerTarget virtual method
    virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);


protected:

private:
    // these methods are purposely declared, but not implemented...
    VitalCapacitySubScreen(void);                           // not implemented..
    VitalCapacitySubScreen(const VitalCapacitySubScreen&);  // not implemented..
    void operator=(const VitalCapacitySubScreen&);          // not implemented..
   
    void updatePanel_(EventData::EventStatus eventStatus,
                      EventData::EventPrompt eventPrompt);
    void updateStartCancelButtons_(Boolean isStartButtonFlat);

    void updatePromptArea_(Boolean isStartMsg);
    enum
    {
        NUM_DATA_PANELS = 3
    };

    //@ Data-Member: titleArea_
    // The sub-screen's title at the top of the screen
    SubScreenTitleArea titleArea_;

    //@ Data-Member: controlPanel_
    // A container for the maneuver prompts and controls
    Container controlPanel_;

    //@ Data-Member: dataPanel_
    // A container for presentation of the maneuver results data
    Container pDataPanel_[NUM_DATA_PANELS];

    //@ Data-Member: pDataTimeStamp_
    // TimeStamp field for the date and time of the VitalCapacity data results
    // we use this object to store the date and time instead of the TextField object since
    // TextField does not have an assigment operator as needed to age the data
    TimeStamp pDataTimeStamp_[NUM_DATA_PANELS];

    //@ Data-Member: pDataDateTime_
    // Text field for the date and time of the VitalCapacity data results
    TextField pDataDateTime_[NUM_DATA_PANELS];

    //@ Data-Member: pDataSymbol_
    // Text field for labeling the VitalCapacity data results
    TouchableText pDataSymbol_[NUM_DATA_PANELS];

    //@ Data-Member: pDataValue_
    // Numeric field for the VitalCapacity data values display
    NumericField pDataValue_[NUM_DATA_PANELS];

    //@ Data-Member: pDataUnit_
    // Text field for the VitalCapacity data units display
    TextField pDataUnits_[NUM_DATA_PANELS];

    //@ Data-Member: panelMsg_
    // A message which informs the user of the maneuver status.
    TextField panelMsg_;

    //@ Data-Member: panelTitle_
    // A title text for this control panel
    TextField panelTitle_;

    //@ Data-Member: startButton_
    // The start command button 
    TextButton startButton_;

    //@ Data-Member: cancelButton_
    // The cancel command button 
    TextButton cancelButton_;

    //@ Data-Member: notActivatedYet_
    // TRUE if the maneuver has not been activated on the GUI yet
    Boolean notActivatedYet_;

    //@ Data-Member: rejectDataButton_
    // The reject button to exclude result data from history log
    TextButton rejectDataButton_;

    //@ Data-Member: acceptDataButton_
    // The reject button to include result data in history log
    TextButton acceptDataButton_;

    //@ Data-Member: eventStatus_
    // Stores the current event status
    EventData::EventStatus eventStatus_;

    //@ Data-Member: leakDetectMsg_
    // A message which informs the user of that a leak is present
    TextField leakDetectMsg_;

};

#endif // VitalCapacitySubScreen_HH 
