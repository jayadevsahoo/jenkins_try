#pragma once
#ifndef SERVOS_GLOBAL_H
#define SERVOS_GLOBAL_H

#include "CalibrationTable.h"  // for NUM_CAL_IDS
#include "InspFlowCalibrationTable.h"
#include "ExhValveCalibrationTable.h"
#include "ExhFlowSensorCalibrationTable.h"
#include "TransducerCalibrationTable.h"
#include "A2dReadingsStructs.h"

extern InspValveCalibrationTable_t *pAirValve;
extern InspValveCalibrationTable_t *pO2Valve;
extern ExhFlowSensorCalibrationTable_t *pExhFlowSensorCal;
extern ExhValveCalibrationTable_t *pExhValve;
extern TransducerCalibrationTable_t *pP1Transducer;
extern TransducerCalibrationTable_t *pP2Transducer;
extern TransducerCalibrationTable_t *pPdrvTransducer;
extern TransducerCalibrationTable_t *pAirInletPressureTransducer;
extern TransducerCalibrationTable_t *pO2InletPressureTransducer;
extern CalibrationTable_t *CalibrationObj[NUM_CAL_IDS];
extern CommandValues_t CommandValues;

#endif