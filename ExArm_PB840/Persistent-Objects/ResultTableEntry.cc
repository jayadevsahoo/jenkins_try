#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: ResultTableEntry - Entry for a result table.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is an entry item for the result tables.  Instances of
//  this class contain result, and a time-stamp for the time of the
//  result information.  This class is used only by the result tables.
//---------------------------------------------------------------------
//@ Rationale
//  This is a common class to be used by all of the test result tables.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains a time-stamp for recording when a result is
//  modified, and a double-buffered instance of 'ResultEntryData' for
//  storing the result information.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  (isClear()  ||  isValid())
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/ResultTableEntry.ccv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Table
//
//  Revision: 003  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By:  sah    Date:  15-Dec-1997    DCS Number: 2684
//  Project:  Sigma (R8027)
//  Description:
//      Adding the mapping of [07076].
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "ResultTableEntry.hh"

//@ Usage-Classes...
#include "NovRamManager.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ResultTableEntry()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default log entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (isClear())
//@ End-Method
//=====================================================================

ResultTableEntry::ResultTableEntry(void)
{
  CALL_TRACE("ResultTableEntry()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~ResultTableEntry()  [Destructor]
//
//@ Interface-Description
//  Destroy this log entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ResultTableEntry::~ResultTableEntry(void)
{
  CALL_TRACE("~ResultTableEntry()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize()
//
//@ Interface-Description
//  This method initializes this entry by verifying that this entry is
//  in a valid state.  If the entry is NOT in a valid state, this entry
//  will be left in a "corrected" state, and 'NovRamStatus::ITEM_INITIALIZED'
//  is returned.  Otherwise, 'NovRamStatus::ITEM_VERIFIED' is returned.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

NovRamStatus::InitializationId
ResultTableEntry::initialize(void)
{
  CALL_TRACE("initialize()");

  NovRamStatus::InitializationId  initStatusId;

  initStatusId = resultData_.initialize();

  if (initStatusId == NovRamStatus::ITEM_INITIALIZED)
  {   // $[TI1] -- 'resultData_' had to be fully initialized...
    // set this entry to the "corrected" ("cleared") state...
    clear();
  }   // $[TI2] -- 'resultData_' is valid...

  return(initStatusId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isClear()  [const]
//
//@ Interface-Description
//  Is this entry currently "cleared"?
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  An entry is considered "clear" if its result is either "NOT APPLICABLE"
//  or "UNDEFINED".
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

Boolean
ResultTableEntry::isClear(void) const
{
  CALL_TRACE("isClear()");

  ResultEntryData  newData;
  
  // initialize 'newData' with the current data...
  resultData_.getData(newData);

  return(newData.getResultOfCurrRun() <= ::UNDEFINED_TEST_RESULT_ID);
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  clear()  [const]
//
//@ Interface-Description
//  "Clear" this entry.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  (isClear())
//@ End-Method 
//===================================================================== 

void
ResultTableEntry::clear(void)
{
  CALL_TRACE("clear()");

  // run the default constructor on this instance...
  new (this) ResultTableEntry();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  startingTest()
//
//@ Interface-Description
//  Record the starting of this test.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[07076] -- if a test is stopped prior to completion, its status shall
//              be INCOMPLETE
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ResultTableEntry::startingTest(void)
{
  CALL_TRACE("startingTest()");

  ResultEntryData  newData;
  
  // initialize 'newData' with the current data...
  resultData_.getData(newData);

  if (newData.isTestCompleted()  ||
      newData.getResultOfCurrRun() >= newData.getResultOfPrevRun())
  {   // $[TI1]
    // inialize the "previous" values to the "current" values...
    newData.saveCurrValues();
  }   // $[TI2] -- do NOT save the "current" to the "previous"...

  // set up the new "current" values...
  newData.setCompletionFlag(FALSE);
  newData.setResultOfCurrRun(::INCOMPLETE_TEST_RESULT_ID);
  newData.resetTimeOfCurrResult();
  newData.setTestStartSeqNumber(NovRamManager::GetNewSequenceNumber());

  // update the buffered result data...
  resultData_.updateData(newData);

  SAFE_CLASS_ASSERTION((isValid()));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  intermediateResultLogged(resultOfIntermediateStep)
//
//@ Interface-Description
//  Record the fact that an intermediate test result was logged for this
//  test.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ResultTableEntry::intermediateResultLogged(
				const TestResultId resultOfIntermediateStep
					  )
{
  CALL_TRACE("intermediateResultLogged(resultOfIntermediateStep)");

  ResultEntryData  newData;

  // initialize 'newData' with the current data...
  resultData_.getData(newData);
  SAFE_CLASS_ASSERTION((!newData.isTestCompleted()));

  if (resultOfIntermediateStep > newData.getResultOfCurrRun())
  {   // $[TI1]
    newData.setResultOfCurrRun(resultOfIntermediateStep);
    newData.resetTimeOfCurrResult();

    // update the buffered result data...
    resultData_.updateData(newData);
  }   // $[TI2] -- new result is less severe than previous result...

  SAFE_CLASS_ASSERTION((isValid()));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  completedTest()
//
//@ Interface-Description
//  Record the completion of this test.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ResultTableEntry::completedTest(void)
{
  CALL_TRACE("completedTest()");

  ResultEntryData  newData;
  
  // initialize 'newData' with the current data...
  resultData_.getData(newData);
  SAFE_CLASS_ASSERTION((!newData.isTestCompleted()));

  newData.setCompletionFlag(TRUE);

  if (newData.getResultOfCurrRun() == ::INCOMPLETE_TEST_RESULT_ID)
  {   // $[TI1]
    newData.setResultOfCurrRun(::PASSED_TEST_ID);
  }   // $[TI2] -- failure status already logged...

  newData.resetTimeOfCurrResult();

  // update the buffered result data...
  resultData_.updateData(newData);

  SAFE_CLASS_ASSERTION((isValid()));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setToNotApplicable(testNumber)
//
//@ Interface-Description
//  Set the status of this test to "NOT APPLICABLE", because it represents
//  a GUI test.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ResultTableEntry::setToNotApplicable(void)
{
  CALL_TRACE("setToNotApplicable()");

  ResultEntryData  newData;
  
  // initialize 'newData' with the current data...
  resultData_.getData(newData);

  newData.setResultOfCurrRun(::NOT_APPLICABLE_TEST_RESULT_ID);
  newData.setResultOfPrevRun(::NOT_APPLICABLE_TEST_RESULT_ID);

  // update the buffered result data...
  resultData_.updateData(newData);

  SAFE_CLASS_ASSERTION((isValid()));
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  override()
//
//@ Interface-Description
//  Override this test's "MINOR" test result, if so.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ResultTableEntry::override(void)
{
  CALL_TRACE("override()");

  ResultEntryData  newData;

  // initialize 'newData' with the current data...
  resultData_.getData(newData);

  if (newData.getResultOfCurrRun() == ::MINOR_TEST_FAILURE_ID)
  {   // $[TI1]
    SAFE_CLASS_ASSERTION((newData.isTestCompleted()));

    newData.setResultOfCurrRun(::OVERRIDDEN_TEST_ID);

    // update the buffered result data...
    resultData_.updateData(newData);
  }   // $[TI2] -- this test is NOT a minor failure...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(tableEntry)  [Assignment Operator]
//
//@ Interface-Description
//  Copy 'tableEntry' into this log entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ResultTableEntry::operator=(const ResultTableEntry& tableEntry)
{
  CALL_TRACE("operator=(tableEntry)");
  CLASS_PRE_CONDITION((!NovRamManager::IsNonVolatileAddr(this)));

  ResultEntryData  otherData;

  // get "other" data...
  tableEntry.resultData_.getData(otherData);

  // store "other" data into this instance...
  resultData_.updateData(otherData);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                              [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ResultTableEntry::SoftFault(const SoftFaultID  softFaultID,
			    const Uint32       lineNumber,
			    const char*        pFileName,
			    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, RESULT_TABLE_ENTRY,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Friend Functions...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function: operator<<(ostr, tableEntry)  [friend]
//
// Interface-Description
//  Dump the contents of 'tableEntry' out to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Free-Function
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const ResultTableEntry& tableEntry)
{
  CALL_TRACE("operator<<(ostr, tableEntry)");

  ResultEntryData  resultData;
  
  tableEntry.resultData_.getData(resultData);

  ostr << "ResultTableEntry:\n"
       << "  resultData_ = " << resultData << endl;

  return(ostr);
}

#endif // defined(SIGMA_DEVELOPMENT)
