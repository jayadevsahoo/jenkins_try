#ifndef	UDPSocket_HH
#define	UDPSocket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//      Copyright (c) 1995-1996, Nellcor Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: UDPSocket - Class definition that provides 
//  interface to a UDP socket that can broadcast data and receive
//  broadcast data.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/UDPSocket.hhv   25.0.4.0   19 Nov 2013 14:02:52   pvcs  $ 
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  05-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================


#include "DownloadTypes.hh"
#include "DownloadFault.hh"
#include "NetSocket.hh"
#include "DownloadPortIds.hh"

#define CLASSA_IP_ADDR   	0x00000000
#define CLASSB_IP_ADDR   	0x80000000
#define CLASSC_IP_ADDR   	0xc0000000
#define IP_ADDR_TYPE_MASK   0xe0000000
#define CLASSA_NET_ADDR_MASK    0xff000000
#define CLASSB_NET_ADDR_MASK    0xffff0000
#define CLASSC_NET_ADDR_MASK    0xffffff00
#define LOCAL_NET_BROADCAST_ADDR   0xffffffff

class UDPSocket : public NetSocket 
{

  public:

    UDPSocket(void);
    ~UDPSocket(void);
	virtual Int  open( PortId port, int nListens );
	Int broadcastData( char *buf, Uint nbytes, PortId udpport );
	int readDataFrom(void *buf, int size, sockaddr *from);
	void computeBroadcastAddr(Uint32 hostIP);
	Int avoidRouting( void );
    static void SoftFault(const FaultType softFaultID,
		const Uint32      lineNumber,
		const char*       pFileName  = NULL, 
		const char*       pPredicate = NULL);
	inline  Boolean inClassC( Uint ip_addr );
	inline  Boolean inClassB( Uint ip_addr );
	inline  Boolean inClassA( Uint ip_addr );
#ifdef SIGMA_UNIT_TEST
	Int callTransmit(char *buf, Int nbytes);
	Int callReceiveFrom(char *buf, Int nbytes, sockaddr *from);
	Int callReceive(char *pData, Int nbytes);
	void setSocketIdOnly(Int socketId);
#endif // SIGMA_UNIT_TEST

  protected:
	virtual Int transmit_(char *buf, Int nbytes);
	virtual Int receive_(char *pData, Int nbytes);
	Int receiveFrom_(char *buf, Int nbytes, sockaddr *from);

  private:
    UDPSocket(const UDPSocket& );	// not implemented...
    void   operator=(const UDPSocket& );	// not implemented...

    //@ Data-Member:  destAddr_;
    // The destination address 
	Saddr destAddr_;

};

// include inline method codes.
#include "UDPSocket.in"

#endif // UDPSocket_HH 
