#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
//@ Filename: Post_Library.cc - Library of functions common to 
//                              all POST Phases.
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains functions used by Phase 1 (Kernel) and Phases
//  2 & 3 POST.  This module is linked separately with the Kernel in
//  PROM and with the POST phases in Flash.  They share the same source
//  but not the same object code.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  See module Interface-Description and function Interface-Descriptions.
//---------------------------------------------------------------------
//@ Fault-Handling
//    The standard POST fault handling mechanisms apply.
//---------------------------------------------------------------------
//@ Restrictions
//    None
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/Post_Library.ccv   25.0.4.0   19 Nov 2013 14:16:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 006     By: erm      Date: 09-May-2003  DR Number: 6054  
//   Project: AVER
//   Description: Allowed Analog Device post Channel Id to log
//
//  Revision: 005   By: Gary Cederquist  Date: 08-MAY-1998  DR Number: 5084
//    Project:   Sigma   (R8027)
//    Description:
//       SERVICE_REQUIRED condition no longer sets the POST failure flag.
//       POST nows logs SERVICE_REQUIRED as a MINOR POST erro and then
//       stops POST execution as it does for major MAJOR POST errors.
//
//  Revision: 004   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project:   Sigma   (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 003    By: Gary Cederquist  Date: 11-JUN-1997  DR Number: 2183
//    Project: Sigma (R8027)
//    Description:
//       Added sense information as part of auxiliary results.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-MAY-1997  DR Number: 2085
//    Project: Sigma (R8027)
//    Description:
//       Added auxiliary result logging.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================


#include "Post_Library.hh"

//@ Usage-Classes
#include "PostErrors.hh"
#include "BdIoUtil.hh"
#include "CpuDevice.hh"
#include "GuiIoUtil.hh"
#include "PostNovram.hh"
#include "AuxResultLog.hh"
//@ End-Usage


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: SetTestResult
//
//@ Interface-Description
//    This function sets the test result in the POST test results log
//    for the current test in progress to the specified result.  This
//    allows multiple functions and exception handlers to fail a test
//    without having knowledge of the current test.  The result
//    parameter is written to the POST results log indexed by the test
//    number of the test in progress.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
 
void
SetTestResult(Uint8 result)
{
   //  since this function may be called prior to the NOVRAM test,
   //  do not log the test result if the NOVRAM version codes don't match
   if (   PKernelNovram->versionCode == KERNEL_NOVRAM_VERSION 
       || PKernelNovram->versionCode == 0 )
   {
      // $[TI1.1]
      if (PKernelNovram->lastTestInProgress >= countof(PKernelNovram->results))
      {
         // $[TI2.1]
         SetErrorCode(POST_ERROR_INTERNAL_FAILURE);
         MajorPostFailure();
      }
         // $[TI2.2]
   
      if ( result > PKernelNovram->results[PKernelNovram->lastTestInProgress] )
      {
         // $[TI3.1]
          PKernelNovram->results[PKernelNovram->lastTestInProgress] = result;
      }
         // $[TI3.2]
   
      if ( result == MAJOR )
      {
         // $[TI4.1]
          PKernelNovram->postFailed = TRUE;
      }
         // $[TI4.2]
   }
   // $[TI1.2]
}
 
 
//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: MajorPostFailure
//
//@ Interface-Description
//    Stops POST testing when called.  This function should be used only
//    for MAJOR failures since there is no return from this function.
//
//    Upon entry, this function disables interrupts so an interrupt
//    cannot resume POST processing or cascade another error.  On the
//    BD CPU, this function sounds the audible alarm, activates the
//    safe state and issues VENT-INOP.  On the GUI, it activates the
//    remote alarm and the SAAS high urgency alarm.  It then continues
//    to strobe the watchdog timer indifinitely while alternately
//    displaying the POST step and error code in the diagnostic LEDs.
//
//  $[11080] $[11081]
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description.  This function does not depend on the
//    time of day clock for any timing purposes since the clock may have
//    failed.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
 
//  for loop count for cached code - determined empirically using 
//  emulator on a 24MHz target - actual delay time not important since
//  this only affects the rate at which the diagnostic LEDs change from
//  displaying the test step to displaying the error code. 
#define LOOPS_PER_MS  (4000)

void 
MajorPostFailure(void)
{
  //  DO NOT ISSUE PRINT STATEMENTS FROM THIS FUNCTION
  //  USED BY NMI HANDLERS POSSIBLY PRIOR TO PRINT INITIALIZATION

    DISABLE_INTERRUPTS();

    //  set shutdown state so we go through long POST the next cycle
    PKernelNovram->shutdownState = ACSWITCH;
 
    //  reset rolling thunder so the current error doesn't cause a 
	//  a failure of the rolling thunder count on the next power-up
    PKernelNovram->rollingThunderCount = 0;
 
    //  log only test failures and internal failures
    //  the exception handler has already logged the exception
    if( PKernelNovram->lastTestInProgress == POST_ERROR_ANALOG_DEVICES )
     {
#ifdef E600_840_TEMP_REMOVED
         AuxResult  analogDeviceAuxResult(  PKernelNovram->lastTestInProgress
                             , 0
                             , *NMI_REGISTER
                             , PKernelNovram->errorCode
                             , (Byte*)GetSense()  );
 
        AuxResultLog::GetPostAuxResultLog().add(analogDeviceAuxResult);
        SetErrorCode(POST_ERROR_TEST_FAILURE);
#endif     
     }
    else if (    PKernelNovram->errorCode == POST_ERROR_TEST_FAILURE
             ||  PKernelNovram->errorCode == POST_ERROR_INTERNAL_FAILURE )
     {
#ifdef E600_840_TEMP_REMOVED
        // $[TI1.1]
        AuxResult  auxResult(  PKernelNovram->lastTestInProgress
                             , 0
                             , *NMI_REGISTER
                             , PKernelNovram->errorCode
                             , (Byte*)GetSense()  );
 
        AuxResultLog::GetPostAuxResultLog().add(auxResult);
#endif
	}
        // $[TI1.2]

    if ( CpuDevice::IsBdCpu() )
    {
        // $[TI2.1]
        ActivateVentInop();
    }
    else
    {
        // $[TI2.2]
        SwitchSoundAlarm( TRUE );
    }

    SwitchAlarms( TRUE );
    SwitchLeds( TRUE );

    //  loop indefinitely, flash LED between last test step
    //  and the error code

UNIT_TEST_POINT(POSTUT_42_1)

    for (;;)
    {
        CpuDevice::LightDiagnosticLED(PKernelNovram->lastTestInProgress);

        // delay for 2 seconds
        for (Uint j=2000*LOOPS_PER_MS; j>0; j--)
        {
            CpuDevice::StrobeWatchdog();
        }
        CpuDevice::LightDiagnosticLED(PKernelNovram->errorCode);

        // delay for 2 seconds
        for (Uint k=2000*LOOPS_PER_MS; k>0; k--)
        {
            CpuDevice::StrobeWatchdog();
        }
    }
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: SwitchAlarms
//
//@ Interface-Description
//  Activates or deactivates alarms according to the boolean state
//  parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  Turns on the audible alarm.  On the BD, the audible alarm is turned
//  on through the Analog Interface.  On the GUI, the remote alarm is
//  activated or deactivated.
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
 
void
SwitchAlarms(Boolean state)
{
   if ( CpuDevice::IsBdCpu() )
   {
       // $[TI1.1]
       SwitchBdAudioAlarm( state );
   }
   else
   {
       // $[TI1.2]
       SwitchRemoteAlarm( state );
   }
}
 
//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: SwitchLeds
//
//@ Interface-Description
//  Activates or deactivates LEDS according to the boolean state
//  parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the appropriate CPU specific function to switch the LEDs 
//  on or off.
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
 
void
SwitchLeds(Boolean state)
{
   if ( CpuDevice::IsBdCpu() )
   {
       // $[TI1.1]
       SwitchBdLeds( state );
   }
   else
   {
       // $[TI1.2]
       SwitchGuiLeds( state );
   }
}
 
