 
#ifndef FlowPatternSetting_HH
#define FlowPatternSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  FlowPatternSetting - Flow Pattern Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/FlowPatternSetting.hhv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  18-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added 'acceptTransition()' for supporting flow pattern's new
//         VC+-to-VC and VC-to-VC+ Transition Rules
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  04-Jun-1997    DCS Number: 2188
//  Project: Sigma (R8027)
//  Description:
//	As part of the fixes for this DCS, I'm removing the special
//	handling done by this class so that a global solution can be
//	made in the base class ('DiscreteSetting').
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
//@ End-Usage


class FlowPatternSetting : public BatchDiscreteSetting
{
  public:
    FlowPatternSetting(void);
    virtual ~FlowPatternSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
				   const DiscreteValue            newValue,
				   const DiscreteValue            currValue);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif // defined (SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual Boolean  calcDependentValues_  (const Boolean isValueIncreasing);
    virtual void     processBoundViolation_(void);

  private:
    FlowPatternSetting(const FlowPatternSetting&);	// not implemented...
    void  operator=(const FlowPatternSetting&);	        // not implemented...
};


#endif // FlowPatternSetting_HH 
