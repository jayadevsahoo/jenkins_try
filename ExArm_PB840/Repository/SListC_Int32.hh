 
#ifndef SListC_Int32_HH
#define SListC_Int32_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SListC_Int32 - Singly-Linked List of 'Int32' Items.
//--------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSListC.H_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class  Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "SNodeC_Int32.hh"
#include "Heap.hh"
//@ End-Usage


class SListC_Int32
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&                  ostr,
				       const SListC_Int32& sList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

    //@ Friend:  SIterC_Int32
    // This list's corresponding iterator needs access to the protected
    // members of this list.
    friend class  SIterC_Int32;

    //@ Friend:  SListR_Int32
    // The singly-linked reference list uses a singly-linked list of
    // void*'s, and needs access to it's protected members.
    friend class  SListR_Int32;

  public:
    virtual ~SListC_Int32(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);

    SigmaStatus  search(const Int32&      item,
			const SearchFrom startFrom = FROM_FIRST);

    void  insert (const Int32&          newItem,
		  const InsertionPlace insertPlace = AFTER_CURRENT);
    void  append (const Int32& newItem);
    void  prepend(const Int32& newItem);

    SigmaStatus  removeCurrent(void);
    SigmaStatus  removeItem(const Int32& item);
    void         clearList(void);

    inline const Int32&  currentItem(void) const;
    inline Int32&        currentItem(void);

    void  operator=(const SListC_Int32& sList);

    inline Boolean  operator==(const SListC_Int32& sList) const;
    inline Boolean  operator!=(const SListC_Int32& sList) const;

    static void  SoftFault(const SoftFaultID softFaultID,
                           const Uint32      lineNumber,
                           const char*       pFileName = NULL,
                           const char*       pBoolTest = NULL);

  protected:
    inline SListC_Int32(Heap* pHeap);

    // return a pointer to the link that is equivalent to 'item', starting
    // the search from 'pStart'...
    const SNodeC_Int32* findNode_(const Int32&        item,
				 const SNodeC_Int32* pStartNode) const;

    inline Boolean  isLegalNode_(const SNodeC_Int32* pNode) const;

    inline const SNodeC_Int32*  getFirst_  (void) const;
    inline const SNodeC_Int32*  getCurrent_(void) const;

    inline SNodeC_Int32*  getFirst_  (void);
    inline SNodeC_Int32*  getCurrent_(void);

    inline void  setCurrent_(SNodeC_Int32* pNewCurr);

    Boolean  isEquivTo_(const SListC_Int32& sList) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    SListC_Int32(const SListC_Int32&);	// not implemented...
    SListC_Int32(void);				// not implemented...

    inline void  deallocNode_(SNodeC_Int32*& pOldNode);

    SNodeC_Int32* getPrevNode_(const SNodeC_Int32* pStart) const;

    //@ Data-Member:  rHeapOfNodes_
    // A reference to this list's heap of nodes.
    Heap&  rHeapOfNodes_;

    //@ Data-Member:  pFirst_
    // A pointer to the first node in this list.
    SNodeC_Int32*  pFirst_;

    //@ Data-Member:  pCurrent_
    // A pointer to the current node of this list.
    SNodeC_Int32*  pCurrent_;
};


// Inlined methods...
#include "SListC_Int32.in"


#endif  // SListC_Int32_HH
