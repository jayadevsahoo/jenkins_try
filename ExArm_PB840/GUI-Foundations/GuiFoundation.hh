#ifndef GuiFoundation_HH
#define GuiFoundation_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class:  GuiFoundation - GUI-Foundations subsystem class.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/GuiFoundation.hhv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 013   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 012   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 011   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 010   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 009  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 008  By: sah       Date: 03-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid.
//
//  Revision: 007  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 006  By: yyy      Date: 03-Apr-1997  DCR Number: 675
//    Project:  Sigma (R8027)
//    Description:
//      Make initialization of file id numbers explicit.
//
//  Revision: 005  By: mpm      Date: 29-Mar-1996  DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added RequestDisplayAccess(), ReleaseDisplayAccess(), SaveDisplayContext(),
//      RestoreDisplayContext() and SetDrawingArea() to allow multiple tasks to
//      share access to the VGA display for drawing purposes.
//
//  Revision: 004  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 003  By: yyy      Date: 18-Oct-1995  DCR Number: 594
//    Project:  Sigma (R8027)
//    Description:
//      Removed the #ifdef SIGMA_DEBUG statement for arrayDebugFlags[].
//
//  Revision: 002  By: yyy      Date: 02-Jun-1995  DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added two methods RequestDisplayAccess() and ReleaseDisplayAccess().
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================
#include "Sigma.hh"

#if defined(SIGMA_DEBUG)
#	include "BitUtilities.hh"
#endif // defined(SIGMA_DEBUG)

//@ Type: GuiFoundationClassId
// Ids of all of the classes/files of this subsystem.  Note that
// developers should add class enums alphabetically.
enum GuiFoundationClassId
{
	ADJUSTPANEL=0,
	AREA=1,
	BASECONTAINER=2,
	GUI_FOUNDATION_BITMAP=3,  
	BOX=4,
	BUTTON=5,
	COLORS=6,
	CONTAINER=7,
	DRAWABLE=8,
	IMAGE=9,
	KEYPANEL=10,
	GUI_LED=11,
	LINE=12,
	NUMERIC_FIELD=13,
	PLOTCONTAINER=14,
	SOUND=15,
	TEXT_FIELD=16,
	TEXT_FONT=17,
	TOUCH=18,
	TRIANGLE=19,

	GUI_FOUNDATION_CLASS=20,
	ALPHANUMERICFIELD=21,
	CLOCK_NUMERIC_FIELD=22,
	NUM_GUI_FOUNDATION_CLASSES
};

//@ Usage-Classes
class BaseContainer;
//@ End-Usage

class GuiFoundation
{
public:

	enum
	{
		//@ Constant: SCREEN_HEIGHT_PIXELS
		// This constant is the number of rows of pixels there are on the
		// VGA screen.
		SCREEN_HEIGHT_PIXELS = 480,

		//@ Constant: SCREEN_WIDTH_PIXELS
		// This constant is the number of columns of pixels there are on the
		// VGA screen.
		SCREEN_WIDTH_PIXELS = 640

	};

    static void Initialize(void);
	static BaseContainer & GetUpperBaseContainer(void);
	static BaseContainer & GetLowerBaseContainer(void);
	static BaseContainer * GetCurrentDisplayContainerPtr(void);
	
#if defined( SIGMA_GUIPC_CPU )
	static Boolean		   IsInitialized(void);	
	static void			   InvalidateBaseContainers(void);
#endif // SIGMA_GUIPC_CPU

	static void SwitchDisplays(void);
	static void SwitchToDisplay(BaseContainer * pBaseContainer);

#if defined SIGMA_DEBUG
    static inline Boolean IsDebugOn(const GuiFoundationClassId moduleId);

    static inline void TurnDebugOn (const GuiFoundationClassId moduleId);
    static inline void TurnDebugOff(const GuiFoundationClassId moduleId);

    static void TurnAllDebugOn (void);
    static void TurnAllDebugOff(void);
#endif // SIGMA_DEBUG

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL,
						  const char*       pPredicate = NULL);

private:
    // these methods are purposely declared, but not implemented...
    GuiFoundation(void);					// not implemented...
    GuiFoundation(const GuiFoundation&);	// not implemented...
    ~GuiFoundation(void);					// not implemented...
    void operator=(const GuiFoundation&);	// not implemented...


    // Constant:  NUM_WORDS_
    // The number of words needed in 'arrDebugFlags_[]' to contain all of
    // this subsystem's debug flags.
    enum
    {
        NUM_WORDS = (::NUM_GUI_FOUNDATION_CLASSES + sizeof(Uint)-1) / sizeof(Uint)
    };

    // Data-Member:  PCurrentDisplayContainer_
    // Pointer to the currently displayed base container - single screen
	static BaseContainer * PCurrentDisplayContainer_;

    // Data-Member:  DisplayAccessNesting_
    // A number which tracks the nesting of calls to the RequestDisplayAccess()
    // and ReleaseDisplayAccess() methods.  It is incremented on calls to
    // RequestDisplayAccess() and decremented on calls to
    // ReleaseDisplayAccess().  Only when its value is the initial 0 will
    // calls to VGARequestAccess() and VGAReleaseAccess() be made.  This
    // allows a task to get access to the display and to subsequently call
    // methods that also try to get access to the display.  The latter
    // calls are ignored, so allowing processing to continue.
    static Uint DisplayAccessNesting_;

    // Data-Member:  arrDebugFlags_
    // A static array of debug flags (one bit each) to be used by this
    // subsystem's code.
    static Uint  arrDebugFlags_[GuiFoundation::NUM_WORDS];

#if defined( SIGMA_GUIPC_CPU )
	// Data-Member: GuiFoundationInitialized_
	// A flag to indicate if the Gui Foundation is initialized
	static Boolean	GuiFoundationInitialized_;
#endif // SIGMA_GUIPC_CPU

};

// Inlined methods...
#include "GuiFoundation.in"

#endif // GuiFoundation_HH
