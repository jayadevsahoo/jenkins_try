//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsConfigPressureBased.hh
/// @brief Definition of the GainsConfigPressureBased class
//----------------------------------------------------------------------------
#ifndef GAINSCONFIGPRESSUREBASED_HH_
#define GAINSCONFIGPRESSUREBASED_HH_

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "GainsConfig.hh"
#include "PressureBasedPhase.hh"
#include "GainsNames.hh"
#include "SoftFaultId.hh"
#include "SigmaTypes.hh"

namespace GainsManagerNS
{
    //----------------------------------------------------------------------------
    // C L A S S   D E C L A R A T I O N
    // Declare this class as part of the GainsManagerNS namespace
    //----------------------------------------------------------------------------
    /// @class GainsConfigPressureBased
    /// @brief Uses the PressureBasedPhase base class to set and get the gains
    /// to a collection of PBP derived classes.
    ///
    /// This class is for handling a class of controllers that are pressure
    /// based - PressureBasedPhase.
    //-------------------------------------------------------------------------
    class GainsConfigPressureBased;
};

//-------------------------------------
// C L A S S    D E F I N I T I O N
//-------------------------------------
class GainsManagerNS::GainsConfigPressureBased : public GainsManagerNS::GainsConfig
{
public:
	//--------------------------------------------------------------------
	/// Constructor
	//--------------------------------------------------------------------
    GainsConfigPressureBased(void);

    //--------------------------------------------------------------------
    /// Constructor
    ///
    /// param[in] name - the name of the gain to control
    //--------------------------------------------------------------------
	GainsConfigPressureBased( GainsNames name);

    //--------------------------------------------------------------------
    /// Copy constructor
    ///
    /// @param[in] cb - GainsConfigPressureBased to copy
    //--------------------------------------------------------------------
	GainsConfigPressureBased(const GainsConfigPressureBased& cb );

    //--------------------------------------------------------------------
    /// Destructor
    //--------------------------------------------------------------------
    virtual ~GainsConfigPressureBased(void);

    //--------------------------------------------------------------------
    /// Assignment operator
    ///
    /// @param rhs - Right hand side.
    ///
    /// @return A reference to a GainsConfigPressureBased.
    //--------------------------------------------------------------------
    GainsConfigPressureBased& operator=(const GainsConfigPressureBased &rhs);

    //--------------------------------------------------------------------
    /// @see GainsConfig
    //--------------------------------------------------------------------
    virtual GainsConfigOpStatus distribute(void);

    //--------------------------------------------------------------------
    /// @see GainsConfig
    //--------------------------------------------------------------------
    virtual GainsConfigOpStatus load(void);

    //--------------------------------------------------------------------
    /// @see GainsConfig
    //--------------------------------------------------------------------
    virtual GainsConfigOpStatus setup(void);

    //--------------------------------------------------------------------
    /// Accessor method
    /// @return the name of the PressureBasedPhase object that this object will
    //          effect
    //--------------------------------------------------------------------
    GainsManagerNS::PressureBasedPhases getPhase(void) const;

    //--------------------------------------------------------------------
    /// Accessor method
    /// @return the gain within the PressureBasedPhase object that
    /// this object will effect
    //--------------------------------------------------------------------
    GainsManagerNS::PressureBasedPhaseGainsNames getGainType(void) const;

private:
    /// Base class pointer to the derived object that this object will effect
    PressureBasedPhase* m_pbpPtr;
    /// Name of the PressureBasedPhase object that this object will effect
    GainsManagerNS::PressureBasedPhases m_breathPhase;
    /// Name of the gain within the PressureBasedPhase object that
    /// this object will effect
    GainsManagerNS::PressureBasedPhaseGainsNames m_gainName;
};

#endif // GAINSCONFIGPRESSUREBASED_HH_
