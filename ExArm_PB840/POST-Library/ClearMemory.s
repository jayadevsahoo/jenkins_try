;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================


;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: ClearMemory.s - Fast memory clear operation
;
;---------------------------------------------------------------------
;@ Interface-Description
;  Free Functions: 
;     ClearMemory - Zeros the specified address range.
;---------------------------------------------------------------------
;@ Rationale
;   POST must set memory to zeros during short or long POST.  The 
;   CellIntegrityTest zeros memory for long POST.  ClearMemory zeros
;   memory without performing a memory test for short POST.
;---------------------------------------------------------------------
;@ Implementation-Description
;   Fills the memory range specified with zeros.
;---------------------------------------------------------------------
;@ Fault-Handling
;---------------------------------------------------------------------
;  Restrictions
;---------------------------------------------------------------------
;@ Invariants
;    None
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
; @(#) $Header:   /840/Baseline/POST-Library/vcssrc/ClearMemory.s_v   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
;
;@ Modification-Log
;
;  Revision 003   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
;    Project:   Sigma   (R8027)
;    Description:
;       Added testable item annotations.
;
;  Revision: 002    By: Gary Cederquist  Date: 16-MAY-1997  DR Number: 2121
;    Project: Sigma (R8027)
;    Description:
;       Removed assembler options specified in source in favor of command
;       line options.
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================

               INCLUDE kernel.inc

               SECTION code,,C

               ALIGN   16
ClearPattern:  DCB.L   8,$00000000
regList:       REG     d2/d3/d4/d5/d6/a1/a2/a5 

;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: ClearMemory - A very fast memory clear
;
;@ Interface-Description
;
; Implementation note: This module is designed to fill 256 bytes each time
;                      through the inner loop.  Therefore; the loop count
;                      for 16 Mbytes fits in 16 bits
;                       (16384*1024/256 = 65,566). 65536 is loop count + 1
;                      If the number of bytes cleared each time through the
;                      loop is reduced or the amount of memory cleared is 
;                      increased; be careful that the iteration loop count
;                      does not overflow 16 bits.
;
;                      Remember; the dbne instruction exits the loop when
;                                the or.l's have a non-zero result, or the
;                                loop count transitions to -1 ($FFFF).
;                                Therefore an initial value of $FFFF will
;                                do 65536 loops, or 65536*256 = 16 Mbytes.
;
;    C++ Callable:
;       Boolean   ClearMemory(Uint32 * pStart, Uint32 length);
;
;    INPUT:
;       pStart - starting address 
;       nbytes - number of bytes to clear (multiple of 256)
;
;    OUTPUT:
;       returns TRUE for a bad parameter, otherwise FALSE
;
;---------------------------------------------------------------------
;@ Implementation-Description
;
;
;  REGISTER USAGE
;
;  Register Modified: d0,d1,d2,d3,d4,d5,d6,d7,a0,a1,a2,a4,a5,a6
;
;---------------------------------------------------------------------
;@ PreCondition
;    None
;---------------------------------------------------------------------
;@ PostCondition
;    None
;
;@ End-Free-Function
;=====================================================================
               XDEF  _ClearMemory__FPUiUi

; Boolean ClearMemory(Uint32 * pStart, Uint32 length) {
_ClearMemory__FPUiUi:

                link.w  a6,#0
                movem.l d2-d7/a2-a5,-(sp)

                move.l  12(a6),d0        ;second argument - length
                movea.l 8(a6),a0         ;first argument - address

                move.l  d0,d1

                andi.l  #$000000ff,d1    ;if not a multiple of 256, fail
                bne     badParameter     ; // $[TI1.1]  failed
                                         ; // $[TI1.2]  passed

                asr.l   #8,d0            ;loop does 256 bytes each iteration
                tst.l   d0               ;if(clearing less than 256 bytes) fail
                beq     badParameter     ; // $[TI2.1]  failed
                                         ; // $[TI2.2]  passed

                subq.l  #1,d0            ;loop_count = requested_loops - 1
                move.l  d0,d1
                andi.l  #$ffff0000,d1    ;if(loop_count > 16 bits) fail
                bne     badParameter     ; // $[TI3.1]  failed
                                         ; // $[TI3.2]  passed

                movem.l ClearPattern,regList  ;fill registers with zeros

FillLoop:       STROBE_WATCHDOG          ;keep the watchdog from timing out
                movem.l regList,(a0)     ;fill 256 bytes with the pattern
                movem.l regList,(32,a0)
                movem.l regList,(64,a0)
                movem.l regList,(96,a0)
                movem.l regList,(128,a0)
                movem.l regList,(160,a0)
                movem.l regList,(192,a0)
                movem.l regList,(224,a0)

                add.l   #$100,a0         ; increment address by 256 

                dbne    d0,FillLoop      ; decrement and loop

                moveq.l #0,d0
                jmp     endClear

badParameter:   
                moveq.l #1,d0

endClear:
                movem.l -40(a6),d2-d7/a2-a5
                unlk    a6
                rts
; }

                END
