#ifndef TimeTick_HH
#define TimeTick_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TimeTick - Provides interface to the VRTX 32 real Time
//    tick value. Allows relative time difference comparison and
//    doesn't rely on the Time of date clock.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Download/vcssrc/TimeTick.hhv   10.7   08/17/07 09:49:42   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  02-13-95    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "DownloadTypes.hh"
#include "DownloadClassIds.hh"
#include "DownloadFault.hh"

#define WRAP_AROUND_TICK  0xFFFFFFFF

class  TimeTick
{
  public:
	TimeTick(void);
	void now(void);
	unsigned long getPassedTime(void);
	inline Boolean isValidTime(void) const;
#ifdef SIGMA_DEVELOPMENT
	inline void invalidateTime(void);
	unsigned long diffTime(const TimeTick&);
#endif // SIGMA_DEVELOPMENT
    static void SoftFault(const FaultType softFaultID,
				 const Uint32      lineNumber,
				 const char*       pFileName  = NULL, 
				 const char*       pPredicate = NULL);
    ~TimeTick(void); 

  private:
	TimeTick(const TimeTick&);          // not implemented...
	void   operator=(const TimeTick&);  // not implemented...

//@ Data_Member: timeTick_
//   contains a VRTX time tick value.
     unsigned long timeTick_;
};
 
// Inlined methods...
#include "TimeTick.in"

#endif // TimeTick_HH
