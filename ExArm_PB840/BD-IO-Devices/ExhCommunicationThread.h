//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: ExhCommunicationThread_t - Active object which communicates with
//			exhalation flow sensor to command autozero and accquire data.
//*****************************************************************************
//
//  ExhCommunicationThread.h - Header file for the ExhCommunicationThread_t class.
//
//*****************************************************************************

#ifndef EXH_COMM_THREAD_H

#define EXH_COMM_THREAD_H

#include "ExhCommunication.h"


enum ExhCommState_t
{
	STATE_FEX_STATUS,
	STATE_FEX_MSB,
	STATE_FEX_LSB,
	STATE_AUTOZERO_STATUS,
	STATE_FEX_STATUS_BYTE1,
	STATE_FEX_SERIAL_NO,
};

enum ExhAutozeroState_t
{
	STATE_AUTOZERO_INIT,
	STATE_AUTOZERO_CLOSE_VALVE,
	STATE_AUTOZERO_WAIT,
	STATE_AUTOZERO_REQUEST,
	STATE_AUTOZERO_DELAY,
	STATE_AUTOZERO_EXTRA_CMD,
	STATE_AUTOZERO_OFFSET,
	STATE_AUTOZERO_PENDING,
	STATE_AUTOZERO_COMPLETED
};

class ExhCommunicationThread_t
{
    public:
        static void Initialize();                           // Called prior to starting the thread
        static void Thread();

        static uint32_t ThreadProcedure(void *ProcedureArg);
        static bool RequestAutozero();
        static ExhAutozeroState_t GetAutozeroState()
		{
			return AutozeroRequestState;
		}

		static ExhSensorBoard_t			SensorBoard;

    protected:

        static ExhCommunication_t      ExhCommunication;
        static ExhCommState_t          ExhCommunicationState;
        static ExhAutozeroState_t      AutozeroRequestState;
		static uint16_t				   FexRawData;
        static uint8_t                 FexStatus;
        static bool                    bFexDataValid;
        static bool                    bAutozeroRequested;
		static uint8_t				   BytesToReceive;

    private:
        ExhCommunicationThread_t();                 // Never use
        ~ExhCommunicationThread_t(void);
};

#endif

