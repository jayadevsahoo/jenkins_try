#include "stdafx.h"

// ****************************************************************************
//
//  UtilityFunctions.cpp - This contains misc global functions which are not
//          associated with a class.
//
// ****************************************************************************
#include "DebugAssert.h"
#include "FileUtilities.hh"
#include "CrcUtilities.hh"


// This macro is taken from the old e600 code (also HT70). It was intended to
// store data in the debug log, for now it just writes to the debug serial port.
#define DEBUG_OUTPUT(Message)  (RETAILMSG(true, (Message)))

#define DOWNLOAD_REQUEST_FILE           _T("\\FlashFX Disk\\Startup\\ADSCopy\\DOWNLOAD.REQ")
#define SYSTEM_RESTART_EXE              _T("\\Windows\\Restart.exe")


// ****************************************************************************
//
//  InitiateDownload - Create a file, DOWNLOAD.REQ, which informs the Startup
//      program that a download is requested. Then restart the vent.
//
// ****************************************************************************

bool InitiateDownload()
{
    HANDLE RegFile = CreateFile(DOWNLOAD_REQUEST_FILE,
                                GENERIC_WRITE,
                                0,
                                NULL,
                                CREATE_ALWAYS,
                                FILE_ATTRIBUTE_NORMAL,
                                NULL);

    if(RegFile == INVALID_HANDLE_VALUE)
    {
        return FALSE;
    }

    // We care what we write. The startup program looks for the content of this file when performing software upgrade.
    BYTE   Data[] = "PB880_Upgrade";
    DWORD  Written;

    WriteFile(RegFile, Data, sizeof(Data), &Written, NULL);

    CloseHandle(RegFile);

    InitiateSystemRestart();                                // If this returns, we have a failure
    return false;
}


// ****************************************************************************
//
//  InitiateSystemRestart -
//
// ****************************************************************************

void InitiateSystemRestart()
{
//    EventsThread.SaveEventBuffers();
	PROCESS_INFORMATION pi;
    CreateProcess(SYSTEM_RESTART_EXE, NULL, NULL, 0, FALSE, 0, 0, 0, NULL, &pi);
}



// ****************************************************************************
//
//  TrapDebugAssert -
//
// ****************************************************************************

void TrapDebugAssert(TCHAR *SourceFile, uint32_t SourceLine)
{
    TCHAR DebugOutput[500];
    _stprintf(DebugOutput, _T("ASSERT failure, Source File=%s Line=%d\n"), SourceFile, SourceLine);
    DEBUG_OUTPUT(DebugOutput);
}



// ****************************************************************************
//
//  TrapDebugAssert -
//
// ****************************************************************************

void TrapDebugAssert(TCHAR *Message, TCHAR *SourceFile, uint32_t SourceLine)
{
    DEBUG_OUTPUT(Message);

    TCHAR DebugOutput[500];
    _stprintf(DebugOutput, _T("ASSERT failure, Source File=%s Line=%d\n"), SourceFile, SourceLine);
    DEBUG_OUTPUT(DebugOutput);
}


//******************************************************************
//
//  FileExists - Return TRUE if the file exists, FALSE if not.
//
//******************************************************************

bool FileExists(LPCTSTR FileName)
{
    DWORD Attributes = GetFileAttributes(FileName);

    if ((Attributes == INVALID_FILE_ATTRIBUTES) && ((GetLastError() == ERROR_PATH_NOT_FOUND) || (GetLastError() == ERROR_FILE_NOT_FOUND)))
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

//******************************************************************
//
//  DeleteDirectory - Return TRUE if the the directory specified by
//                    SourcePath can be deleted, FALSE if not.
//                    The length of SourcePath string is limited to
//                    MAX_PATH (260).
//******************************************************************
bool DeleteDirectory(LPCTSTR SourcePath)
{
   HANDLE FindHandle;                     // a handle returned by FindFirstFile and FindNextFile function
   WIN32_FIND_DATA FindFileData;

   TCHAR DirPath[MAX_PATH];
   TCHAR FileName[MAX_PATH];

   DEBUG_ASSERT(_tcslen(SourcePath) >= 1 && _tcslen(SourcePath) <= (MAX_PATH-1)); // check to see the SourcePath length is valid or not, -1 is to consider of the null character at the end of the SourcePath string

   if(!FileExists(SourcePath))  // if the SourcePath does not exist, return false immediatelly!
   {
       return FALSE;
   }
   // if SourcePath is already ended with a '\' charater, don't attach a '\' to the end
   if(SourcePath[_tcslen(SourcePath)-1] == '\\')
   {
       _tcscpy(DirPath,SourcePath);
       _tcscat(DirPath,L"*");       // attach * to search all files
       _tcscpy(FileName,SourcePath);
   }
   else
   {
       _tcscpy(DirPath,SourcePath);
       _tcscat(DirPath,L"\\*");   // attach * to search all files
       _tcscpy(FileName,SourcePath);
       _tcscat(FileName,L"\\");
   }
   // try to find the first file under SourcePath directory
   FindHandle = FindFirstFile(DirPath,&FindFileData);

   if(FindHandle == INVALID_HANDLE_VALUE) // no files is found under SourcePath directory, just delete the empty SourcePath directory
   {
       return((RemoveDirectory(SourcePath))?TRUE:FALSE);
   }
   _tcscpy(DirPath,FileName);    // after calling FindFirstFile, we need to set DirPath to be the same as SourcePath

   bool Search = TRUE;

   while(Search) // loop until we find an entry
   {
       if(_tcscmp(FindFileData.cFileName,L".") == 0) // ignore the . directory/file name
       {
            continue;
       }
        _tcscat(FileName,FindFileData.cFileName);

       // do we find a directory?
       if((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
       {
            // we have found a directory, recursely call the DeleteDirectory function
            if(!DeleteDirectory(FileName))
            {
                FindClose(FindHandle);

                return FALSE;           // directory couldn't be deleted
            }
            _tcscpy(FileName,DirPath);  // set FileName to be the same as the SourcePath, so it can be used to concatenate a file/directory name in the next search
       }
       else // do we find a file?
       {
            if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_READONLY) // if the file is read only, change it to normal
            {
                SetFileAttributes(FileName, FindFileData.dwFileAttributes | FILE_ATTRIBUTE_NORMAL);
            }

            if(!DeleteFile(FileName))    // try to delete the file found
            {
                FindClose(FindHandle);

                return FALSE;           // file couldn't be deleted
            }
             _tcscpy(FileName,DirPath); // set FileName to be the same as the SourcePath, so it can be used to concatenate a file/directory name in the next search
       }

       if(!FindNextFile(FindHandle,&FindFileData)) // continue to check to see if we can find any files / sub directories
       {
         if(GetLastError() == ERROR_NO_MORE_FILES)
         {
            Search = FALSE;
         }
         else
         {
            // some error occurred; close the handle and return FALSE
            FindClose(FindHandle);
            return FALSE;
         }
       }
   }

   FindClose(FindHandle);                  // close the file handle

   return (RemoveDirectory(SourcePath)?TRUE:FALSE);     // remove the empty directory

}


//********************************************************************
//
//  ValidFileCrc -
//  This function returns true if the 32-bit CRC calculated from
//  the FileName matches with the CRC read from the CrcFileName.
//  Otherwise, this function shall return false.
//
//********************************************************************
bool ValidFileCrc(LPCTSTR FileName, LPCTSTR CrcFileName)
{
    // open the file to calculate the CRC
    HANDLE FileHandle = CreateFile(FileName,
                                   GENERIC_READ,
                                   0,
                                   NULL,
                                   OPEN_EXISTING,
                                   FILE_ATTRIBUTE_NORMAL,
                                   NULL);

    if(FileHandle == INVALID_HANDLE_VALUE)
    {
        return false;
    }

    // open the Crc file
    HANDLE CrcFileHandle = CreateFile(CrcFileName,
                                      GENERIC_READ,
                                      0,
                                      NULL,
                                      OPEN_EXISTING,
                                      FILE_ATTRIBUTE_NORMAL,
                                      NULL);

    if(CrcFileHandle == INVALID_HANDLE_VALUE)
    {
        CloseHandle(FileHandle);

        return false;
    }

    DWORD FileSize    = GetFileSize(FileHandle,NULL);     // find out the file size
    DWORD CrcFileSize = GetFileSize(CrcFileHandle,NULL);  // find out the Crc file size
    CrcValue CrcFromFile   = 0;
    CrcValue CalculatedCrc = 0;

    // handle the empty file case
    if(FileSize == 0 || CrcFileSize == 0)
    {
        CloseHandle(FileHandle);

        CloseHandle(CrcFileHandle);

        return false;
    }

    // allocate a memory to hold the data read from the file
    BYTE* Buffer = new BYTE[FileSize];

    if(!Buffer)
    {
        CloseHandle(FileHandle);

        CloseHandle(CrcFileHandle);

        RETAILMSG(TRUE, (_T("Failed to allocate memory to check crc\r\n")));

        return false;
    }

    DWORD BytesRead;

    // read data from the file into memory, so we can calculate the crc
    ReadFile(FileHandle,Buffer,FileSize,&BytesRead,NULL);

    // incorrect number of bytes read?
    if(BytesRead != FileSize)
    {
        if(Buffer != NULL)
        {
            delete [] Buffer;
        }

        CloseHandle(FileHandle);

        CloseHandle(CrcFileHandle);

        return false;
    }

    // this file is no longer needed, close it
    CloseHandle(FileHandle);

    // read the stored crc into the local variable CrcFromFile
    ReadFile(CrcFileHandle,&CrcFromFile,CrcFileSize,&BytesRead,NULL);

    // incorrect number of bytes read?
    if(BytesRead != CrcFileSize)
    {
        if(Buffer != NULL)
        {
            delete [] Buffer;
        }

        CloseHandle(CrcFileHandle);

        return false;
    }

    CalculatedCrc = ::CalculateCrc((BYTE *)Buffer,FileSize);

    // compare the crc here
    if(CalculatedCrc != CrcFromFile)
    {
        if(Buffer != NULL)
        {
            delete [] Buffer;
        }

        CloseHandle(CrcFileHandle);

        return false;
    }

    // deallocate the buffer
    if(Buffer != NULL)
    {
        delete [] Buffer;
    }

    // the crc file is no longer needed, close it
    CloseHandle(CrcFileHandle);

    //RETAILMSG(TRUE, (_T("The file %s passed the validation!\r\n"), FileName, GetLastError()));
    //RETAILMSG(TRUE, (_T("The file %s passed the validation!\r\n"), CrcFileName, GetLastError()));

    return true;

}



