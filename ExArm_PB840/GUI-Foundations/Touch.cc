#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Touch -  This class directs touch-related events to the touched
//  drawable.
//---------------------------------------------------------------------
//@ Interface-Description
// The purpose of this static class is to manage the identification and
// notification of touch targets.
//
// This class collaborates with the screen management framework when it needs
// to locate the touched Drawable.  It starts the search by calling the
// findTouchedDrawable() method of the BaseContainer class.
//
// It calls the touch(), leave() and release() methods of the appropriate
// Drawable as the corresponding events occur. The touch() method is called
// once each time a finger enters a Drawable either by sliding in from the
// side, or by touching the screen from above. The leave() method is called
// when the finger slides out of the Drawable.  The release() method is called
// when the finger is removed from the screen while still within that Drawable.
//
// Touch screen events are passed to this class via the TouchEvent() or the
// ReleaseEvent() methods.  
// The application can turn Touch functionality off and on by calling the
// Enable() and Disable() methods of this class.
//
// This module contains all knowledge of the mapping from touch screen
// coordinates to VGA screen coordinates.
//---------------------------------------------------------------------
//@ Rationale
// This class was created in order to centralize all of the logic required for
// converting between touch screen and VGA coordinates and for dispatching
// touch events to Drawables.
//---------------------------------------------------------------------
//@ Implementation-Description
// All members of this class are static.  Instances of this class cannot be
// created.  Organizing the class in this way uses the class mechanism to
// implement what is essentially a module. This is legitimate in this case
// since the addition of more touch screens is extremely unlikely.
//
// There are two types of touch methods in this class, TouchEvent() and
// ReleaseEvent().
// The external caller calls the TouchEvent() method when a Touch event occured.
// A pair of (X,Y) coordinates are passed to this method as the parameter for
// this method.
// The external caller calls the ReleaseEvent() method when a Release event
// occured.
// 
// For details regarding the detection of touch events, refer to the
// GUI-IO-Devices subsystem SDS.
//
// For more information on how touch events are routed to this class, refer to
// the GUI-Applications subsystem SDS and the High Level Design Specification.
//
// The touch event method shall be used whenever a user's finger makes contact
// with the screen.  The processing of the touch event depends upon whether
// or not a "focus" object currently exists.
//
// If there is no focus or if the focus was cleared to null because a leave
// event occurred, then a search is made of the display tree to identify which
// Drawable has been touched.  The responsibility for performing this search is
// given to the Container class.  The search is started by calling the
// Container::findTouchedDrawable() method of the appropriate BaseContainer.
// If a touched Drawable is found then the touch() method of that Drawable is
// called.
//
// If there is a focus, then a check is made to see if the touch
// coordinates still fall within that Drawable. If not, then the leave()
// function of that Drawable is called to notify it that it is losing the touch
// focus, and the focus is set to null.  This means that the user's finger has
// been dragged out of the Drawable without being lifted.
//
// If there is a focus Drawable and the touch position is still within that
// Drawable, then we know that the finger has moved but has not left the area
// of the Drawable. In this case there is nothing more to do since these slide
// events do not get propagated to the Drawable.  
//
// The release event method shall be used whenever a user's finger is lifted
// from the screen.  If there is a focus, then the release() function of that
// Drawable is called to notify it that it is losing the touch focus, and the
// focus is set to null.
//
// This class includes a private enum defining constants HORIZONTAL_TOUCH_RANGE
// and VERTICAL_TOUCH_RANGE. They allow a "touch area" to be constructed around
// the point on the screen which was calculated from the touch screen
// coordinates.  A Drawable is considered to be touched if it overlaps the
// touch area.  If the touch area overlaps more than one Drawable, then none
// are considered to be touched.
//
// The constants TOUCHSCREEN_WIDTH and TOUCHSCREEN_HEIGHT are used to convert
// touch coordinates to VGA coordinates.  Constants SCREEN_HEIGHT_PIXELS and
// SCREEN_WIDTH_PIXELS define the number of rows and columns of pixels on the
// VGA screen; these are defined in class GuiFoundation.  This class handles
// conversion of touch screen locations into locations on the upper or lower
// VGA screen.  The design of this class assumes that there are 2 screens - the
// number of screens is not a generic constant.
//
// The touch class does not make use of a general callback mechanism. It is
// assumed that the object which will require notification due to a touch must
// be a Drawable, since that is the only type of object which can exist in the
// drawing tree.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Touch.ccv   25.0.4.0   19 Nov 2013 14:11:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 005   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 004   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 003   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 002  By: yyy      Date: 06-Feb-1997   DCR Number: 1526
//    Project:  Sigma (R8027)
//    Description:
//      Changed TouchEvent() and ReleaseEvent() from void to Int16 type
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Touch.hh"

//@ Usage-Classes
#include "BaseContainer.hh"
#include "Container.hh"
#include "OsUtil.hh"
#include "SoftwareOptions.hh"
//@ End-Usage

//@ Code...

// Initialize static data members
Drawable *Touch::PFocus_ = NULL;
Boolean Touch::Enabled_ = FALSE;
Int16 Touch::TouchScreen_ = Touch::NO_SCREEN_TOUCHED;


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize    [static]
//
//@ Interface-Description
// This function initializes the touch data members and any internal
// data structures that are required for touch management.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply initializes the Enabled_ attribute to TRUE and the PFocus_ to NULL.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
Touch::Initialize()
{
	CALL_TRACE("Touch::Initialize()");
	//								$[TI1]
	Touch::Enabled_ = TRUE;
	Touch::PFocus_ = NULL;

}





//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TouchEvent    [static]
//
//@ Interface-Description
// This function handles a touch event at the passed x, y location. The x,y
// point is in touch screen coordinates. It is the responsibility of this
// function to convert that point to VGA coordinates.
// This is one of the primary interfaces from GUI-Foundations to other
// subsystems and the outside world.
//---------------------------------------------------------------------
//@ Implementation-Description
// The ratio of TOUCHSCREEN_WIDTH to the width of a VGA screen gives us the
// factor by which a touch x coordinate must be multiplied to convert to VGA
// pixels.
//
// For the Y coordinate the conversion factor is a little different since touch
// screen Y coordinates stretch over 2 screens. So the conversion factor is the
// ratio of TOUCHSCREEN_HEIGHT / 2 to the height of one VGA screen.  The design
// of this method assumes that there are 2 screens - the number of screens
// is not a generic constant.
//
// Once this routine has established the point on the VGA screen where the
// event took place, it builds an Area around that point using the constants
// HORIZONTAL_TOUCH_RANGE and VERTICAL_TOUCH_RANGE.
//
// If there is a focus then a check is made to see if the touch event still
// overlaps with that Drawable. If not then the Leave() function is called and
// the focus is set to null. This corresponds to the finger sliding out of the
// Drawable.
//
// If there is a focus and the check for an overlap succeeds then we know that
// the finger has moved but has not left the area of the Drawable. In this case
// there is nothing more to do since these slide events do not get propagated
// to the Drawable.
//
// If there is no focus or if the focus was cleared to null because a Leave
// occured then a search is made of the drawing tree to find a Drawable which
// has been touched. If one is found then Touch() is called on that Drawable.
//
// A Drawable's touch sensitive area is restricted by its neighbours during the
// check for the initial touch, but this restriction dissappears for subsequent
// touch events within that drawable, until the finger is lifted or dragged
// out.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Int16
Touch::TouchEvent(Int16 x, Int16 y)
{
	CALL_TRACE("Touch::TouchEvent(Int16 x, Int16 y)");

	// Do stuff only if the touch screen has not been disabled
	if (!Touch::Enabled_)
	{	//												$[TI1]
		return(NO_SCREEN_TOUCHED);
	}
	//													$[TI2]

	Int16 currentTouchedScreen;
	
	// This will point at the GuiFoundation::GetUpperBaseContainer or the
	// GuiFoundation::GetLowerBaseContainer.
	BaseContainer *pRootContainer = NULL;

	{
	  // establish whether the point is upper or lower screen.
	  if (y < GuiFoundation::SCREEN_HEIGHT_PIXELS)
	  {	//													$[TI3]
		  // Upper screen was touched.
		  pRootContainer = &GuiFoundation::GetUpperBaseContainer();
		  currentTouchedScreen = UPPER_SCREEN_TOUCHED;
	  }
	  else
	  {	//													$[TI4]		
		  // Lower screen was touched.
		  y -= GuiFoundation::SCREEN_HEIGHT_PIXELS;
		  pRootContainer = &GuiFoundation::GetLowerBaseContainer();
		  currentTouchedScreen = LOWER_SCREEN_TOUCHED;
	  }
	}

	// Convert arguments passed in to VGA coordinates.

	Int16 vgaX = x;

	Int16 vgaY = y;

	// Create the touchArea
	// 
	Area touchArea(vgaX , vgaY , vgaX , vgaY );

    TouchScreen_ = NO_SCREEN_TOUCHED;
	
	// Check if there is a drawable that already has the touch focus
	if (Touch::PFocus_ != NULL)
	{	//													$[TI5]
		// The following is a unusual procudure of the touch event which
		// occures when the baseContainer is changed.  
		//    First, the user press one button,
		//    Second, at the same touch coordinate but on a different
		//    baseContainer, the user touch the screen again.  At this time,
		//    the user is touching the screen with both hands.
		//    Third, release the first finger.
		//    Fourth, drag the second finger across the screen.
		// To prevent this to happen, the baseContainer of the touched focus
		// need to be verified.
		//
		// Or the finger might have moved away from the focus object on the
		// same baseContainer.
		//    We check for overlap here because it is possible that
		//    the x,y touch point has moved since the last time this
		//    function was called, and the finger may have moved away
		//    from the focus object.
		//
		if ((pRootContainer != Touch::PFocus_->getBaseContainer()) ||
		    !(Touch::PFocus_->overlapsRealArea(touchArea)))
		{	//												$[TI5.1]	
			// Finger has moved away from drawable so inform the drawable
			// that it has been left.
			Touch::PFocus_->leave(x, y);
			Touch::PFocus_ = NULL;

			// Remember which screen had been touched.
			TouchScreen_ = currentTouchedScreen;
		}
		//													$[TI15.2]
	}
	//														$[TI6]
														

	// If we have no focus then see if a sensitive drawable was touched.
	if (Touch::PFocus_ == NULL)
	{	//													$[TI7]	
		// A reference to this variable is passed to the findTouchedDrawable()
		//  call below.  It increments this variable whenever it finds
		//  a sensitive Drawable that overlaps with the touchArea.
		Int numHits = 0;

		//////////////////////////////////////////////////////////////
		// NOTE: the following line is explicitly casting away const
		// type of the returned pointer to Drawable!  This is in
		// violation of the C++ coding standard - but it is needed
		// in this case because if the pointer was const, it would
		// not be possible to call the non-const member functions
		// such as touch().
		//////////////////////////////////////////////////////////////
			
		Touch::PFocus_ =
			(Drawable*)(pRootContainer->
						  findTouchedDrawable(touchArea, numHits));

		// If a sensitive drawable was found then inform it of the touch
		if (Touch::PFocus_ != NULL)
		{	//												$[TI7.1]
			Touch::PFocus_->touch(x, y);

			// If we havn't done any screen change, then now is the
			// time to update the changed screen
			if (TouchScreen_ == NO_SCREEN_TOUCHED)
			{   //                                   $[TI7.1.1]
				TouchScreen_ = currentTouchedScreen;
			}
			//                                       $[TI7.1.2]
		}
		//													$[TI7.2]
	}
	//														$[TI8]
    
	return(TouchScreen_);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReleaseEvent    [static]
//
//@ Interface-Description
// This function is called when the user release the finger from the touch
// screen.  In this case the x,y position is irrelevant since it is guaranteed
// to be the same as the last touch event.  This is one of the primary
// interfaces from GUI-Foundations to other subsystems and the outside world.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// If there is a PFocus_ then call the release() function on that Drawable,
// and then set the PFocus_ to null.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Int16
Touch::ReleaseEvent(Int16 x, Int16 y)
{
	CALL_TRACE("Touch::ReleaseEvent(Int16 x, Int16 y)");

	Int16 touchScreen = NO_SCREEN_TOUCHED;

	// Do stuff only if the touch screen has not been disabled
	if (Touch::Enabled_)
	{	//													$[TI1]

		// If a drawable currently has the touch focus then inform it that it
		// is not being touched anymore.
		if (Touch::PFocus_ != NULL)
		{	//												$[TI1.1]
			Touch::PFocus_->release();
			Touch::PFocus_ = NULL;

			// Remember to do a screen update based on the previous
			// touched screen.
			touchScreen = TouchScreen_;
		}

	}	//													$[TI1.2]
	//														$[TI2]

	return(touchScreen);
}



//=====================================================================
//
// Private methods
//
//=====================================================================


#if defined SIGMA_UNIT_TEST

//=====================================================================
//
// Public methods
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPFocus
//
//@ Interface-Description
// This function returns the address of the currently focused drawable object.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The address of the currently focused drawable object is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Drawable *
Touch::getPFocus(void)
{
	CALL_TRACE("Touch::getPFocus(void");

	return(PFocus_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getEnabled
//
//@ Interface-Description
// This function returns the Enabled_ status of the Touch class.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The value of the currently set Enabled_ status is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
Touch::getEnabled(void)
{
	CALL_TRACE("Touch::getEnabled(void");

	return(Enabled_);
}

#endif	// SIGMA_UNIT_TEST
								 


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
Touch::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, TOUCH, lineNumber,
							pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
