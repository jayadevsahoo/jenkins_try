/*****************************************************************************/
/*   Module Name:    dbapi.c                                            
/*   Purpose:      Database API functions                                
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 Selected Reports to preview or 
/* 	 delete  functions are added.			Lakshmi(CGS)   	  5 Dec,2000
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <toolbox.h>
#include <utility.h>

#include "72500.h"
#include "840cmds.h" // VENTDATA Vent
#include "840msgs.h"
#include "cpvt_db.h"
#include "cpvtmain.h"
#include "dbapi.h"
#include "ldb.h"
#include "MsgPopup.h"
#include "registry.h"

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#define DAO_PROCEED_IF_OK if(!DAO_OK_to_transact) return -1;

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

static test_run_struct CurrTestInfo;
static int CurrSequence = -1;
static int LastSequence = -1;
static int CurrOATest   = -1;
static int DAO_OK_to_transact = 0;
struct SSNdate SNDate={0,"",""};  
/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     IsReportAvailable                                                   
/*                                                                           
/* Purpose:  See if a test sequence has been run
/*                                          
/*                                                                           
/* Example Call:   static void IsReportAvailable(void);                                    
/*                                                                           
/* Input:    input1 -	description          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Returns 1 if a test has been run, 0 otherwise                                                        
/*                                                                           
/*****************************************************************************/
int IsReportAvailable(void)
{
	if(LastSequence == -1){
		return 0;
	}

	return 1;
}


/*****************************************************************************/
/* Name:     ShowJetError                                                   
/*                                                                           
/* Purpose:  Displays an error message for the Jet interface
/*                                          
/*                                                                           
/* Example Call:   static void ShowJetError();                                    
/*                                                                           
/* Input:    input1 -	description          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void ShowJetError()
{
	CPVTMessagePopup(LDBf(Ldb_Database_error), LDBf(Ldb_Database_error_msg), 
	                      OK_ONLY_DLG, NO_HELP);

	return;
}


/*****************************************************************************/
/* Name:     SetupDB_Jet                                                   
/*                                                                           
/* Purpose:  Initializes the Jet interface and adds a sequence record
/*                                          
/*                                                                           
/* Example Call:   int SetupDB_Jet(void);                                    
/*                                                                           
/* Input:    <none>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Sequence ID                                                         
/*                                                                           
/*****************************************************************************/
int SetupDB_Jet(void)
{
	int ID;
	
	DAO_PROCEED_IF_OK
	CurrTestInfo.set = 0;
	ID = AllocSeqID();
	CurrSequence = ID;

	return ID;
}


/*****************************************************************************/
/* Name:     SetupJetOATest                                                   
/*                                                                           
/* Purpose:  allocates a overall test record to obtain an ID
/*                                          
/*                                                                           
/* Example Call:   int SetupJetOATest(void);                                    
/*                                                                           
/* Input:    none 
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Test ID                                                         
/*                                                                           
/*****************************************************************************/
int SetupJetOATest(void)
{
 	int ID;

 	DAO_PROCEED_IF_OK
 	ID = AllocOATestID();
	if(ID < 0){
		ShowJetError();
	}
	CurrOATest = ID;

	return ID;
}


/*****************************************************************************/
/* Name:     WriteOATestDB_Jet                                                   
/*                                                                           
/* Purpose:  fills overall test information into previously allocated record
/*                                          
/*                                                                           
/* Example Call:   int WriteOATestDB_Jet(int index, int PF);                                    
/*                                                                           
/* Input:    index - this overall test's index in the list
/*			 PF	- pass/fail indicator 1 pass, 0 fail
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Id of updated record                                                       
/*                                                                           
/*****************************************************************************/
int WriteOATestDB_Jet(int index, int PF)
{
	int recID;

	DAO_PROCEED_IF_OK
	if(CurrOATest < 0){
		return -1;
	}
	if(PF == DB_SKIP){
		LoadTestInfo(index, PF, "", 0.0, 0.0, 0.0, 3);
		WriteTestDB_Jet();
	}	
	recID = AddOATest(CurrOATest, index, PF, CurrSequence);
	if(recID < 0){
		ShowJetError();
	}
	CurrOATest = -1;

	return recID;
}


/*****************************************************************************/
/* Name:     WriteSeqDB_Jet                                                   
/*                                                                           
/* Purpose:  Write the current sequence information the the Jet database
/*                                          
/*                                                                           
/* Example Call:   int WriteSeqDB_Jet(char* User, int PFtext, int ID);                                    
/*                                                                           
/* Input:    User - current user name
/*			 PFtest - pass/fail indicator
/*			 ID - record to update
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   TBD                                                         
/*                                                                           
/*****************************************************************************/
int WriteSeqDB_Jet(char* User, int PFtext, int ID)
{
	int recID;

	char cBufBDserial[VENT_SERIAL_NUMBER_MAX];
	char *BDserial = NULL;

	char cBufBDrunTimeHours[64];

	char cBufBDversion[VENT_VERSION_NUMBER_MAX];
	char *BDversion = NULL;

	char cBufBDpostVersion[POST_MAX];
	char *BDpostVersion = NULL;

	char cBufGUIserial[VENT_SERIAL_NUMBER_MAX];
	char *GUIserial = NULL;

	char cBufGUIpostVersion[POST_MAX];
	char *GUIpostVersion = NULL;

	char cBufSAASversion[SAAS_MAX];
	char *SAASversion = NULL;

    char cBufCompressorSN[VENT_SERIAL_NUMBER_MAX];
    char *CompressorSN = NULL;

	char cBufCompressorRunTimeHours[64];

	DAO_PROCEED_IF_OK
	if(CurrSequence < 0){
		return -1;
	}

	// extract ventilator serial ID from BD SN: string
	strcpy(cBufBDserial, Vent.Bd.serial);
	strtok(cBufBDserial, ":");
	BDserial = strtok(NULL, ":");

	// convert vent run-time hours to text for database
	sprintf(cBufBDrunTimeHours, "%d", (int)Vent.runTimeHours);

	// extract ventilator version ID from BD Ver: string
	strcpy(cBufBDversion, Vent.Bd.version);
	strtok(cBufBDversion, ":");
	BDversion = strtok(NULL, ":");

	// extract ventilator post version ID from BD Post Ver: string
	strcpy(cBufBDpostVersion, Vent.Bd.post);
	strtok(cBufBDpostVersion, ":");
	BDpostVersion = strtok(NULL, ":");

	// extract GUI serial ID from GUI SN: string
	strcpy(cBufGUIserial, Vent.Gui.serial);
	strtok(cBufGUIserial, ":");
	GUIserial = strtok(NULL, ":");

	// extract GUI post version ID from GUI Post Ver: string
	strcpy(cBufGUIpostVersion, Vent.Gui.post);
	strtok(cBufGUIpostVersion, ":");
	GUIpostVersion = strtok(NULL, ":");

	// extract SAAS version ID from Saas Ver: string
	strcpy(cBufSAASversion, Vent.saas);
	strtok(cBufSAASversion, ":");
	SAASversion = strtok(NULL, ":");

	// extract compressor serial number from CompressorSN: string
	strcpy(cBufCompressorSN, Vent.Compressor.serial);
	strtok(cBufCompressorSN, ":");
	CompressorSN = strtok(NULL, ":");

	// convert compressor run-time hours to text for database
	sprintf(cBufCompressorRunTimeHours, "%d", (int)Vent.Compressor.runTimeHours);

	recID = AddSequence(User, PFtext, CurrSequence, gUUTPN, BDserial, cBufBDrunTimeHours, BDversion, 
	                    BDpostVersion, GUIserial, GUIpostVersion, SAASversion, 
	                    CompressorSN, cBufCompressorRunTimeHours, 
	                    szSerialNumber, szVersionNumber, szCalDate, szNextCalDate, IsNextCalOverdue(szNextCalDate),
	                    gVentAssetNumber, gWorkOrderNumber);
	if(recID < 0){
		ShowJetError();
	}
	LastSequence = CurrSequence;
	CurrSequence = -1;

	return recID;
}


/*****************************************************************************/
/* Name:     LoadTestInfo                                                      
/*                                                                           
/* Purpose:  Loads the current test information to memory
/*	     This is done to maintain the same flow as the original reporting system                                          
/*                                                                           
/* Example Call:   int LoadTestInfo(int idx, char PF, char* fact, double min,
/*									double max, double rdg, int type);                                    
/*                                                                           
/* Input:    	idx - test index
/*				PF - Pass-fail
/*				fact - string from tfmtdisplay
/*				min - min value
/*				max - max value
/*				rdg - reading obtained during test
/*				type - test type (no values, single reading, limits reading)
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 1                                                         
/*                                                                           
/*****************************************************************************/
int LoadTestInfo(int idx, char PF, char* fact, double min, double max, double rdg, int type)
{
	CurrTestInfo.TestIndex = idx;
	CurrTestInfo.Fact = StrDup(fact);
	CurrTestInfo.MinRdg = min;
	CurrTestInfo.MaxRdg = max;
	CurrTestInfo.Rdg = rdg;
	CurrTestInfo.Type = type;
	CurrTestInfo.set = 1;
	if(PF == 'P'){
		CurrTestInfo.Pass_Fail = 1;
	}
	else{
		CurrTestInfo.Pass_Fail = 0;
	}

	return 1;
}


/*****************************************************************************/
/* Name:    WriteTestDB_Jet                                                    
/*                                                                           
/* Purpose:  Writes the current test information from memory to the Jet database
/*                                          
/*                                                                           
/* Example Call:   int WriteTestDB_Jet(void);                                    
/*                                                                           
/* Input:    <none>       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   record ID                                                       
/*                                                                           
/*****************************************************************************/
int WriteTestDB_Jet(void)
{
	int ID;

	DAO_PROCEED_IF_OK
	if(CurrTestInfo.set == 1){
		CurrTestInfo.set = 0;
	}
	else{
		return -1;
	}
	if(CurrSequence < 0){
		return -1;
	}
	ID = AddTestRun(CurrTestInfo.Pass_Fail,
	                CurrTestInfo.Fact,
	                CurrTestInfo.MinRdg,
	                CurrTestInfo.Rdg,
	                CurrTestInfo.MaxRdg,
	                CurrOATest,
	                CurrTestInfo.Type);
	if(ID < 0){
		ShowJetError();
	}

	return ID;
}


/*****************************************************************************/
/* Name:     WriteLogLine                                                   
/*                                                                           
/* Purpose:  adds a line to the log table
/*                                          
/*                                                                           
/* Example Call:   int WriteLogLine(char* line);                                    
/*                                                                           
/* Input:    line - line to add     
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   record ID added                                                  
/*                                                                           
/*****************************************************************************/
int WriteLogLine(char* line)
{
	int record = -1;

	DAO_PROCEED_IF_OK
	if(line != NULL){
		record = AddLogInfo(line, 0);
	}

	return record;
}


/*****************************************************************************/
/* Name:     ClearLog                                                   
/*                                                                           
/* Purpose:  deletes all log table entries
/*                                          
/*                                                                           
/* Example Call:   int ClearLog(void);                                    
/*                                                                           
/* Input:    <none>     
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int ClearLog(void)
{
	DAO_PROCEED_IF_OK
	ClearLogInfo();

	return 0;
}


/*****************************************************************************/
/* Name:     CompactJetDB                                                   
/*                                                                           
/* Purpose:  Compacts the jet database
/*                                          
/*                                                                           
/* Example Call:   void CompactJetDB(void);                                    
/*                                                                           
/* Input:    <none>     
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                        
/*                                                                           
/*****************************************************************************/
void CompactJetDB(void)
{
	CompactDB();

	return;
}


/*****************************************************************************/
/* Name:     CloseJetDB                                                   
/*                                                                           
/* Purpose:  Closes the jet database
/*                                          
/*                                                                           
/* Example Call:   int CloseJetDB(void);                                    
/*                                                                           
/* Input:    <none>     
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0                                                        
/*                                                                           
/*****************************************************************************/
int CloseJetDB(void)
{
	DAO_PROCEED_IF_OK
	CloseDAOConnect();	
	DAO_OK_to_transact = 0;

	return 0;
}


/*****************************************************************************/
/* Name:     OpenJetDB                                                   
/*                                                                           
/* Purpose:  Opens the jet database
/*                                          
/*                                                                           
/* Example Call:   void OpenJetDB(void);                                    
/*                                                                           
/* Input:    <none>     
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                        
/*                                                                           
/*****************************************************************************/
void OpenJetDB(void)
{
	OpenDAOConnect(LDBf(Ldb_DB_Name));	
	DAO_OK_to_transact = 1;

	return;
}


int ClearJetDB(void)
{
	DAO_PROCEED_IF_OK

	return ClearTables();
}


void CheckDBSize(void)
{
	long filesize;

	GetFileSize(LDBf(Ldb_DB_Name), &filesize);
	if(filesize > 2000000){
		CPVTMessagePopup(LDBf(Ldb_DB_Warning), LDBf(Ldb_DB_getting_big), OK_ONLY_DLG, 0);
	}

	return;
}

/*****************************************************************************/
/* Name:     DeleteSelectedSequence                                                   
/*                                                                           
/* Purpose:  to delete the selected reports from the database
/*                                          
/*                                                                           
/* Example Call:   int DeleteSelectedSequence(int SeqID);                                    
/*                                                                           
/* Input:    int
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Flag                                                         
/*                                                                           
/*****************************************************************************/
int DeleteSelectedSequence(int SeqID)
{
	int iStatus;
	
	DAO_PROCEED_IF_OK	
	iStatus=DeleteSequence(SeqID);
	if(!iStatus)
	{
		ShowJetError();
		return 0;
	}			 
	return 1;
}

/*****************************************************************************/
/* Name:     SelectJetSeqSN                                                   
/*                                                                           
/* Purpose:  Selects the records from the database.
/*                                          
/*                                                                           
/* Example Call:   struct SSNdate SelectJetSeqSN(int RecId);
/*                                                                           
/* Input:    int RecId
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           SNDate - A structure to hold Date serial Id of Equipment                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   struct SSNdate                                                         
/*                                                                           
/*****************************************************************************/
struct SSNdate SelectJetSeqSN(int RecId)
{
 	
    if(!DAO_OK_to_transact) return SNDate;
 	SNDate = SelectSeqID(RecId);
	if(SNDate.ID < 0){
		ShowJetError();
	}
	
	return SNDate;
}
