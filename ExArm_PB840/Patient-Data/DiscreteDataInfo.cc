#include "stdafx.h"

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DiscreteDataInfo - Data processing agent class for Discrete
//         breath item data types.
//	   
//---------------------------------------------------------------------
//@ Interface-Description
//
//>Von
//   Description:
//   ------------
//   DiscreteDataInfo is an agent class which processes and maintains
//   the latest data for a specifc discrete breath item type.
//   It contains information necessary to check out the validity
//   of a discrete value for a specfic data type.
//   It's a subclass of the 'BreathItemDataInfo' class, it inherits
//   most of the breath data processing features, double buffering,
//   data access methods.
//
//   Class Features:
//   --------------------
//    1. Maintains a minimum and a maximum values allowed for 
//       for a specific discrete breath data type.
//
//    2. Receives new data from the network and maintains an
//       active discrete data.
//
//   Class Key methods:
//   -----------------
//   1. modifyData_ -- updates the data of this agent to a new value.
//		              Guarantees data integrity through double
//					  buffering of the data.
//
//   2. isValid	-- Determines if a given value is a valid discrete
//      	   data value for this type of discrete breath type.	 
//---------------------------------------------------------------------
//@ Rationale
//    Purpose of this class is to provide only the unique features for
//    a discrete data data type beyond what 'BrethItemDataInfo' class
//    provides.
//---------------------------------------------------------------------
//@ Implementation-Description
//    A discrete breath data value has to be within the maximum
//    and minimum allowed ranges. A new data is stored into
//    a network buffer until the parent 'BreathItemDataInfo'
//    class's modifyData_ method updates it to be the active data.
//    
//>Voff
//---------------------------------------------------------------------
//@ Fault-Handling
//    
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/DiscreteDataInfo.ccv   25.0.4.0   19 Nov 2013 14:17:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: gdc   Date:  17-Jun-2005   SCR Number: 6170
//  Project:  NIV2
//  Description:
//      Modifications to support processing of NIV high spont 
//      insp time alert.
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  30-Jan-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================
#include "DiscreteDataInfo.hh"

//@ Usage-Classes
#include "PatientDataMgr.hh"
#include "PatientDataId.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DiscreteDataInfo( const PatientDataId::PatientItemId id,
//  const DiscreteValue min, const DiscreteValue mid ) [constructor]
//
//@ Interface-Description
//   Parent class consructor, BreathItemDataInfo() is called to initialize
//   two inherted member variables, index and PatientItem id type.
//   Initializes MIN_VALUE_ and MAX_VALUE_ member variables to minimum
//   number allowed and maximum number allowed for this data type.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	( PatientDataMgr::IsDiscreteId( id ) )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DiscreteDataInfo::DiscreteDataInfo( const PatientDataId::PatientItemId id,
	const DiscreteValue min, const DiscreteValue max)
    :BreathItemDataInfo(id),MIN_VALUE_(min),MAX_VALUE_(max)
{
  CALL_TRACE("DiscreteDataInfo()");
  CLASS_PRE_CONDITION( PatientDataMgr::IsDiscreteId( id ));
  data_[index_.active].dValue.timedOut = TRUE;
// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DiscreteDataInfo()  [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DiscreteDataInfo::~DiscreteDataInfo(void)
{
  CALL_TRACE("~DiscreteDataInfo()");
// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: modifyData_(void *pValue) [virtual]
//
//@ Interface-Description
//	Modifies Patient Data Manager's breath data buffer with a new
//      value and informs other subsystems there is new breath data item.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//      modifyData_ is called whenever new breath data is received
//      from the network. Get the buffer currently assigned to receive
//      a new network data and store the new data into it.
//      Call the parent class( BreathItemDataInfo)'s modifyData_ method
//      to make the new data to become active one and send breath item
//      data change notice.
//		$[03003] $[03006]
//---------------------------------------------------------------------
//@ PreCondition
//   ( pValue )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
DiscreteDataInfo::modifyData_( void *pValue)
{
Uint  rawValue;

  CLASS_PRE_CONDITION(pValue);

  rawValue= *((Uint  *) pValue);
  CLASS_ASSERTION(isValid( (DiscreteValue) rawValue));

  data_[index_.network].dValue.data = (DiscreteValue) rawValue;
  data_[index_.network].dValue.timedOut = FALSE;
  BreathItemDataInfo::modifyData_(pValue);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  setDataTimeout(void) 
//
//@ Interface-Description
//      Time out this data and send out a data change notice
//      to other interested subsystems if requested.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Set the timeout flag of the active data and call the
//      'PatientDataMgr::MakeItemCalls()' to send out data change notice.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none 
//@ End-Method
//=====================================================================

void
DiscreteDataInfo::setDataTimeout(void)
{
  CALL_TRACE("setDataTimeout_()");

  data_[index_.active].dValue.timedOut = TRUE;
  PatientDataMgr::MakeItemCalls( DATA_ID_ );  
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
DiscreteDataInfo::SoftFault(const SoftFaultID  softFaultID,
						    const Uint32       lineNumber,
						    const char*        pFileName,
						    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PATIENT_DATA,
  DISCRETE_DATA_INFO, lineNumber, pFileName, pPredicate);
}

#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
