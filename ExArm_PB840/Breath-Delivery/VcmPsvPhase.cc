#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VcmPsvPhase - Vital Capacity Maneuver Pressure Supported
//                       Inspiratory Phase
//---------------------------------------------------------------------
//@ Interface-Description
//
// This class is a derived from the PsvPhase which is itself derived
// from PressureBasedPhase. All methods in this class are virtual
// methods defined in the base classes. Since the Vital Capacity
// maneuver inspiratory phase is simply a pressure supported
// inspiratory phase with specific settings, this class uses the base
// class for establishing and controlling the inspiratory phase and
// uses vitual functions to override settings specific to the Vital
// Capacity inspiratory phase.
//
//---------------------------------------------------------------------
//@ Rationale
// This class implements the algorithms for the Vital Capacity 
// inspiratory phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// N/A
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VcmPsvPhase.ccv   25.0.4.0   19 Nov 2013 14:00:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc   Date:  23-Feb-2007    SCR Number: 6307
//  Project:  RESPM
//  Description:
//		Added missing RM requirement numbers.
//
//  Revision: 002   By: gdc   Date:  25-Oct-2006    SCR Number: 6296
//  Project:  RESPM
//  Description:
//		Changed to pressure expiratory trigger to cycle out of
// 		vital capacity inspiratory phase.
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//=====================================================================

#include "VcmPsvPhase.hh"

#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "BreathMiscRefs.hh"
#include "ControllersRefs.hh"

//@ Usage-Classes

#include "O2Mixture.hh"
#include "BreathTrigger.hh"
#include "DisconnectTrigger.hh"
#include "ControllersRefs.hh"
#include "PressureController.hh"
#include "ManeuverRefs.hh"
#include "VitalCapacityManeuver.hh"
#include "DeliveredFlowExpTrigger.hh"
#include "PressureExpTrigger.hh"

//@ End-Usage

//@ Code...

//@ Constant: MIN_SUPPORT_PRESSURE
// The minimum peep pressure to be maintained
extern const Real32 MIN_SUPPORT_PRESSURE ;

//@ Constant: MAX_VCMPSV_PHASE_TIME_
//	maximum time the Vital Capacity PSV phase can be active
//  used to set disconnect timer
static const Int32 MAX_VCMPSV_PHASE_TIME_ = 15000;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VcmPsvPhase()
//
//@ Interface-Description
//	Constructor.  This method has no arguments and returns nothing.
//	The RO2Mixture object should be constructed before this class is
//	constructed since this object registers for callbacks with the 
//  O2Mixture object.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Calls the base class PsvPhase constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VcmPsvPhase::VcmPsvPhase( void)
 : PsvPhase()
{
	CALL_TRACE("VcmPsvPhase( void)") ;

	// override INSPIRATION phaseType_ set in BreathPhase constructor
	BreathPhase::phaseType_ = BreathPhaseType::VITAL_CAPACITY_INSP;
	
	RO2Mixture.registerBreathPhaseCallBack( this) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VcmPsvPhase()
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

VcmPsvPhase::~VcmPsvPhase(void)
{
	CALL_TRACE("~VcmPsvPhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
// This method is passed a BreathTrigger& argument and has no return 
// value.  This method is called at the end of the VC inspiration 
// phase to wrap up the phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
VcmPsvPhase::relinquishControl( const BreathTrigger& trigger)
{
	CALL_TRACE("relinquishControl(BreathTrigger& trigger)");

	if (trigger.getId() == Trigger::PRESSURE_EXP)
	{
		// $[RM24011] terminate the VCM inspiratory phase when the delivered flow trigger...
	}
	else
	{
        // $[RM12062] A Vital Capacity maneuver shall be canceled if...
		// Any other trigger (e.g. high circuit pressure) cancels the maneuver
		// abnormal breath termination -- cancel  the maneuver
		Maneuver::CancelManeuver();
	}

    // $[RM12064] When a VC maneuver terminates, a PEEP Recovery breath shall be delivered, followed by a resumption of normal breath delivery. 

	// let the base class do the real work of relinquishing control
	PsvPhase::relinquishControl(trigger);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
// This method has no arguments and has no return value.  This method is 
// called at the beginning of the Vital Capacity inspiratory phase to 
// initialize the phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls VitalCapacityManeuver.activate() to change the maneuver state
// to active. Uses the base class PsvPhase::newBreath() to start
// the PSV inspiratory phase.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
VcmPsvPhase::newBreath(void)
{
    CALL_TRACE("newBreath(void)");
   	CLASS_ASSERTION(RVitalCapacityManeuver.getManeuverState() == PauseTypes::PENDING);

	// $[RM12207] The maneuver shall transition to active at the beginning of the next patient inspiratory effort signaled by the backup pressure trigger. 	

	// Activate the vital capacity maneuver
	RVitalCapacityManeuver.activate();


	// $[RM12063] When a Vital Capacity maneuver becomes active, the 840 shall deliver a spontaneous inspiration in response to patient effort

	// let the base class do the real work
	PsvPhase::newBreath();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
VcmPsvPhase::SoftFault( const SoftFaultID  softFaultID,
                   	 const Uint32       lineNumber,
					 const char*        pFileName,
		   			 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, VCMPSVPHASE,
                             lineNumber, pFileName, pPredicate) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineEffectivePressureAndBiasOffset_
//
//@ Interface-Description
// This method has no arguments and has no return value.  This method is
// called by the base class, PressureBasePhase, to determine the
// effective pressure and the bias offset. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The pressure support level for the Vital Capacity Maneuver 
// inspiratory phase is zero. The effective pressure is set to the 
// bias offset with a pressure support level of zero.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VcmPsvPhase::determineEffectivePressureAndBiasOffset_( void)
{
	CALL_TRACE("determineEffectivePressureAndBiasOffset_( void)") ;
	
	// $[RM12063] a: Psupp = 0
	effectivePressure_ = MIN_SUPPORT_PRESSURE;
}
		
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineFlowAccelerationPercent_
//
//@ Interface-Description
// This method has no arguments and has no return value.  This method is
// called by the base class, PressureBasePhase, to determine the
// flow acceleration percentage.
//---------------------------------------------------------------------
//@ Implementation-Description
// The flow acceleration percentage is set to 50% for the Vital Capacity
// Maneuver inspiratory phase.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
VcmPsvPhase::determineFlowAccelerationPercent_( void)
{
	CALL_TRACE("determineFlowAccelerationPercent_( void)") ;
	
	// $[RM12063] b: Rise Time = 50%
	fap_ = 50.0;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableExhalationTriggers_()
//
//@ Interface-Description
// This method has no arguments and has no return value.  This method is
// called by the base class, PressureBasePhase, to enable the
// triggers that will terminate inspiration.
//---------------------------------------------------------------------
//@ Implementation-Description
// Only the Delivered Flow Expiratory Trigger is enabled during the
// Vital Capacity Inspiratory Phase. The maneuver uses a timeout 
// to trigger out of the VCM inspiratory phase if the delivered flow
// trigger does not fire for some reason. The disconnect trigger is
// adjusted to not fire during the Vital Capacity inspiratory phase
// as well.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
VcmPsvPhase::enableExhalationTriggers_( void)
{
	CALL_TRACE("enableExhalationTriggers_( void)") ;
	
	if (elapsedTimeMs_ == 0)
	{
		// $[RM24011] Only the pressure based expiratory patient trigger shall be enabled during VCM insp...
		// do not enable the delivered flow patient trigger since
		// prematurely terminates the slow vital capacity inspiration
		RPressureExpTrigger.enable();

		// $[RM12084] During a VC maneuver, the INSPIRATION TOO LONG trigger and alarm shall be disabled.
		((BreathTrigger&)RBackupTimeExpTrigger).disable();

		// $[RM12240] During VCM insp, use the SPONT breath disconnect criteria
		RDisconnectTrigger.setTimeLimit( MAX_VCMPSV_PHASE_TIME_ ) ;
		RDisconnectTrigger.setFlowCmdLimit( RPressureController.getFlowCommandLimit()) ;
		
	}
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

